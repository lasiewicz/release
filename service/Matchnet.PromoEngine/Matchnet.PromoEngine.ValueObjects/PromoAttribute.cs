using System;

#region Matchnet references
using Matchnet.Content.ValueObjects.AttributeMetadata;
#endregion

namespace Matchnet.PromoEngine.ValueObjects
{	
	#region Promo Attribute types

	/// <summary>
	/// 
	/// </summary>
	public enum PromoAttributeType:int
	{
		/// <summary>
		/// 
		/// </summary>
		Simple=1,
		/// <summary>
		/// 
		/// </summary>
		Calculated=2
	}
	#endregion

	/// <summary>
	///  This class represents the attributes which are used in PromoPlan Engine
	///  Each attribute could be of  type of Native or Calculated. 
	///  Native attributes are simply the member attributes in the system
	///  calculated one's are introduced in PtomoPlan and are accessd through MemberMapperclass
	/// </summary>
	[Serializable]	
	public class PromoAttribute
	{

		#region Private members

		private int _attributeID;

		private DataType _dataType;

		private PromoAttributeType _attributeType;
		
		#endregion
		
		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		public PromoAttribute()
		{
			this._attributeID=Constants.NULL_INT;
			this._dataType= DataType.Number; // default to number
			this._attributeType=PromoAttributeType.Simple;	// default to simple type
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="attributeID"></param>
		/// <param name="dataType"></param>
		/// <param name="attributeType"></param>
		public PromoAttribute(int attributeID,DataType dataType, int attributeType)
		{
			this._attributeID=attributeID;
			this._dataType= dataType;
			this._attributeType= (attributeType==(int) PromoAttributeType.Calculated? PromoAttributeType.Calculated: PromoAttributeType.Simple);
			//			this._attributeType=attributeType;
		}

		#endregion

		#region Property methods
		/// <summary>
		/// 
		/// </summary>
		public int ID
		{
			get{return _attributeID;}
			set{_attributeID=value;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DataType DataType
		{
			get{return _dataType; }
			set{_dataType=value;}
		}
		/// <summary>
		/// 
		/// </summary>
		public PromoAttributeType  AttrubiteType
		{
			get{return _attributeType;}
			set{_attributeType=value;}
		}
		#endregion
	}
}
