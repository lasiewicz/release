using System;

namespace Matchnet.PromoEngine.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	public enum PromoApprovalStatus
	{
		/// <summary>
		/// 
		/// </summary>
		Pending = 1,
		/// <summary>
		/// 
		/// </summary>
		Approved = 2,
		/// <summary>
		/// 
		/// </summary>
		Rejected = 3 
	}
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class PromoApproval
	{
		private int promoID = Constants.NULL_INT;
		private int adminMemberID = Constants.NULL_INT;
		private int brandID = Constants.NULL_INT;
		private PromoApprovalStatus promoApprovalStatus;
		private DateTime updateDateTime = DateTime.MinValue;
		private DateTime insertDateTime = DateTime.MinValue;

		/// <summary>
		/// 
		/// </summary>
		public int PromoID
		{
			get
			{
				return promoID;
			}
			set
			{
				promoID = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int AdminMemberID
		{
			get
			{
				return adminMemberID;
			}
			set
			{
				adminMemberID = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int BrandID
		{
			get
			{
				return brandID;
			}
			set
			{
				brandID = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public PromoApprovalStatus PromoApprovalStatus
		{
			get
			{
				return promoApprovalStatus;
			}
			set
			{
				promoApprovalStatus = value;
			}
		}
		
		/// <summary>
		/// 
		/// </summary>
		public DateTime UpdateDateTime
		{
			get
			{
				return updateDateTime;
			}
			set
			{
				updateDateTime = value;
			}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDateTime
		{
			get
			{
				return insertDateTime;
			}
			set
			{
				insertDateTime = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public PromoApproval()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="adminMemberID"></param>
		/// <param name="brandID"></param>
		/// <param name="promoApprovalStatus"></param>
		public PromoApproval(int promoID, int adminMemberID, int brandID, PromoApprovalStatus promoApprovalStatus)
		: this (promoID, adminMemberID, brandID, promoApprovalStatus, DateTime.MinValue, DateTime.MinValue)
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="adminMemberID"></param>
		/// <param name="brandID"></param>
		/// <param name="promoApprovalStatus"></param>
		/// <param name="updateDateTime"></param>
		/// <param name="insertDateTime"></param>
		public PromoApproval(int promoID, int adminMemberID, int brandID, PromoApprovalStatus promoApprovalStatus, DateTime updateDateTime, DateTime insertDateTime)
		{
			this.promoID = promoID;
			this.adminMemberID = adminMemberID;
			this.brandID = brandID;
			this.promoApprovalStatus = promoApprovalStatus;
			this.updateDateTime = updateDateTime;
			this.insertDateTime = insertDateTime;
		}
	}
}
