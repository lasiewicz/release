using System;
using System.Collections;


namespace Matchnet.PromoEngine.ValueObjects
{
	#region Operator types
	/// <summary>
	/// Enum definition for available operators for PromoEngine expressions
	/// </summary>
	public enum OperatorType: int
	{
		/// <summary>
		/// 
		/// </summary>
		Equal=1,
		/// <summary>
		/// 
		/// </summary>
		LessThan=2,
		/// <summary>
		/// 
		/// </summary>
		LessThanEqual=4,
		/// <summary>
		/// 
		/// </summary>
		GreaterThan=8,
		/// <summary>
		/// 
		/// </summary>
		GreaterThanEqual=16,
		/// <summary>
		/// 
		/// </summary>
		Between=32

	};
	#endregion
	
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class Expression
	{
		#region Private Members
		
		private PromoAttribute _attribute;

		private OperatorType _operator;

		private ArrayList _values;

		#endregion
		
		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		public Expression()
		{
			_attribute= new PromoAttribute();
			_operator=0;
			_values= new ArrayList();
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="attribute"></param>
		/// <param name="Ooperator"></param>
		/// <param name="values"></param>
		public Expression(PromoAttribute attribute,OperatorType Ooperator, ArrayList values)
		{
			this._attribute=attribute;
			this._operator=Ooperator;
			this._values=values;

		}
		#endregion

		#region Property methods
		/// <summary>
		/// 
		/// </summary>
		public PromoAttribute Attribute
		{
			get{return _attribute ;}
			set{_attribute=value;}
		}
		/// <summary>
		/// 
		/// </summary>
		public OperatorType Operator
		{
			get{return _operator;}
			set{_operator=value;}
		}
		/// <summary>
		/// 
		/// </summary>
		public ArrayList Values
		{
			get{return _values;}
			set{_values=value;}
		}

		#endregion

	}

}