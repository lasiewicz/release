using System;

namespace Matchnet.PromoEngine.ServiceAdapters.TokenReplacement
{
	/// <summary>
	/// Summary description for RowHeaderTokenReplacer.
	/// </summary>
	public class RowHeaderTokenReplacer : ITokenReplacer
	{
		private int _duration = Constants.NULL_INT;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="duration"></param>
		public RowHeaderTokenReplacer(int duration)
		{
			_duration = duration;
		}
		#region ITokenReplacer Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="templateHtml"></param>
		/// <returns></returns>
		public string ReplaceTokens(string templateHtml)
		{
			return string.Format(templateHtml, _duration.ToString());
		}

		#endregion
	}
}
