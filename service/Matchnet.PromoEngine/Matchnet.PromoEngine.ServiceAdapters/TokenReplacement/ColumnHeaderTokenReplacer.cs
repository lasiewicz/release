using System;

namespace Matchnet.PromoEngine.ServiceAdapters.TokenReplacement
{
	/// <summary>
	/// Summary description for ColumnHeaderTokenReplacer.
	/// </summary>
	public class ColumnHeaderTokenReplacer : ITokenReplacer
	{
		/// <summary>
		/// 
		/// </summary>
		public ColumnHeaderTokenReplacer()
		{
			
		}
		#region ITokenReplacer Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="templateHtml"></param>
		/// <returns></returns>
		public string ReplaceTokens(string templateHtml)
		{
			return templateHtml;
		}

		#endregion
	}
}
