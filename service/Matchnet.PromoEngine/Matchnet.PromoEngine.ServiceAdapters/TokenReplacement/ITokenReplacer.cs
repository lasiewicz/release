using System;

namespace Matchnet.PromoEngine.ServiceAdapters.TokenReplacement
{
	/// <summary>
	/// Interface for token replacers for the resource templates
	/// </summary>
	public interface ITokenReplacer
	{
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		string ReplaceTokens(string templateHtml);
	}
}