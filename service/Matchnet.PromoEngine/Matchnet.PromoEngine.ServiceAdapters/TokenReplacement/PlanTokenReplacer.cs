using System;

using Matchnet.PromoEngine.ValueObjects;

namespace Matchnet.PromoEngine.ServiceAdapters.TokenReplacement
{
	/// <summary>
	/// Token replacer used for plans.
	/// </summary>
	public class PlanTokenReplacer : ITokenReplacer
	{
		private PromoPlan _promoPlan;
		private int _decimalPrecision;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="promoPlan"></param>
		public PlanTokenReplacer(PromoPlan promoPlan, int decimalPrecision)
		{
			_promoPlan = promoPlan;
			_decimalPrecision = decimalPrecision;
		}
		#region ITokenReplacer Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="templateHtml"></param>
		/// <returns></returns>
		public string ReplaceTokens(string templateHtml)
		{
			string formatString = "F" + _decimalPrecision.ToString();

			return string.Format(templateHtml, _promoPlan.InitialCostPerDuration.ToString(formatString),
				_promoPlan.InitialCost.ToString(formatString), _promoPlan.RenewCost.ToString(formatString), _promoPlan.InitialDuration.ToString());
		}

		#endregion
	}
}
