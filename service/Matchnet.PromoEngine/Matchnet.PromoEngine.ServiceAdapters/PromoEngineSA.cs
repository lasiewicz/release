using System;

using System.Collections.Generic;

using Quartz;

#region Matchnet references	
using Matchnet.RemotingClient;
using Matchnet.Exceptions;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.PromoEngine.ServiceDefinitions;
#endregion

namespace Matchnet.PromoEngine.ServiceAdapters
{
	/// <summary>
	/// 
	/// </summary>
	public class PromoEngineSA	: SABase
	{
		/// <summary>
		///  Singleton instance
		/// </summary>
		public static readonly PromoEngineSA Instance = new PromoEngineSA();
		
		// Constructor
		private PromoEngineSA()
		{
		}
		
		/// <summary>
		/// Returns an admin only promo for a given brand
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="brand"></param>
		/// <param name="paymentType"></param>
		/// <returns></returns>
		public Promo GetAdminPromo(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, Matchnet.Purchase.ValueObjects.PaymentType paymentType)
		{
			Promo  resultPromo=null;
			string uri=string.Empty;
			try
			{
				uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					resultPromo= getService(uri).GetAdminPromo(memberID,brand, paymentType);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex)
			{
				throw(new SAException("GetAdminPromo() error (uri: " + uri + ").  MemID="+ memberID.ToString()+ " BrandID=" + brand.BrandID.ToString() , ex));
			}
			return resultPromo; 
		}

        /// <summary>
        /// Returns a supervisor only promo.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="brand"></param>
        /// <param name="paymentType"></param>
        /// <returns></returns>
        public Promo GetSupervisorPromo(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, Matchnet.Purchase.ValueObjects.PaymentType paymentType)
        {
            Promo resultPromo = null;
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    resultPromo = getService(uri).GetSupervisorPromo(memberID, brand, paymentType);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("GetSupervisorPromo() error (uri: " + uri + ").  MemID=" + memberID.ToString() + " BrandID=" + brand.BrandID.ToString(), ex));
            }
            return resultPromo;
        }

		/// <summary>
		/// Checks to see if a member qualifies for a specific promo.
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="brand"></param>
		/// <param name="promo"></param>
		/// <returns></returns>
		public bool QualifyPromo(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, Promo promo)
		{
			bool qualify = false;
			string uri=string.Empty;

			try
			{
				uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					qualify= getService(uri).QualifyPromo(memberID, brand, promo);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw(new SAException("QualifyPromo() error (uri: " + uri + ").  MemID="+ memberID.ToString()+ " BrandID=" + brand.BrandID.ToString() + " PromoID=" + promo.PromoID.ToString() , ex));
			}

			return qualify;
		}

		/// <summary>
		/// Returns Promo for CreditCardPayment
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="brand"></param>
		/// <returns></returns>
		public Promo GetMemberPromo(int memberID,Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
		{
			return GetMemberPromo(memberID, brand, Matchnet.Purchase.ValueObjects.PaymentType.CreditCard);
		}

        		/// <summary>
		/// Returns a promo that meets targeting requirements for the member. Only returns Member PromoType.
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="brand"></param>
		/// <param name="paymentType"></param>
		/// <returns></returns>
        public Promo GetMemberPromo(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand,
            Matchnet.Purchase.ValueObjects.PaymentType paymentType)
        {
            Promo resultPromo = null;
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    resultPromo = getService(uri).GetMemberPromo(memberID, brand, paymentType);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("GetMemberPromo() error (uri: " + uri + ").  MemID=" + memberID.ToString() + " BrandID=" + brand.BrandID.ToString(), ex));
            }
            return resultPromo; 
        }

	    /// <summary>
		/// Same as GetMemberPromo but supports passing in of PromoType argument.
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="brand"></param>
		/// <param name="paymentType"></param>
		/// <param name="promoType">Added to support promos for mobile</param>
		/// <returns></returns>
		public Promo GetMemberPromo(int memberID,Matchnet.Content.ValueObjects.BrandConfig.Brand brand,
			Matchnet.Purchase.ValueObjects.PaymentType paymentType, PromoType promoType)
		{
			Promo  resultPromo=null;
			string uri=string.Empty;
			try
			{
				uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					resultPromo= getService(uri).GetMemberPromo(memberID,brand, paymentType, promoType);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex)
			{
				throw(new SAException("GetMemberPromo() error (uri: " + uri + ").  MemID="+ memberID.ToString()+ " BrandID=" + brand.BrandID.ToString() , ex));
			}
			return resultPromo; 
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public PromoCollection GetPromos( int brandID)
		{

			PromoCollection resultPromo=null;
			string uri=string.Empty;
			try
			{
				uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					resultPromo= getService(uri).GetPromos(brandID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex)
			{
				throw(new SAException("GetPromos() error (uri: " + uri + "). BrandID=" + brandID.ToString() + ex.ToString() , ex));
			}
			return resultPromo;
		}

		/// <summary>
		/// Returns an active promo with matching PromoID  
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public Promo GetPromoByID(int promoID,int brandID)
		{
			Promo resultPromo=null;
			string uri=string.Empty;
			try
			{
				uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					resultPromo= getService(uri).GetPromoByID (promoID,brandID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex)
			{
				throw(new SAException("GetPromoByID() error (uri: " + uri + "). promoID=" + promoID.ToString() , ex));
			}
			return resultPromo;
		}

		/// <summary>
		/// Retrieves a promo even inactive one can come back if specified.
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="brandID"></param>
		/// <param name="activeOnly"></param>
		/// <returns></returns>
		public Promo GetPromoByID(int promoID, int brandID, bool activeOnly)
		{
			Promo resultPromo=null;
			string uri=string.Empty;
			try
			{
				uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					resultPromo= getService(uri).GetPromoByID (promoID,brandID, activeOnly);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex)
			{
				throw(new SAException("GetPromoByID() error (uri: " + uri + "). promoID=" + promoID.ToString() , ex));
			}
			return resultPromo;
		}

		/// <summary>
		/// Returns an active promo with matching PromoID with member specific UI metadata
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="brandID"></param>
		/// <param name="memberID"></param>
		/// <returns></returns>
		public Promo GetPromoByID(int promoID, int brandID, int memberID)
		{
			Promo resultPromo=null;
			string uri=string.Empty;
			try
			{
				uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					resultPromo= getService(uri).GetPromoByID (promoID,brandID,memberID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex)
			{
				throw(new SAException("GetPromoByID() error (uri: " + uri + "). promoID=" + promoID.ToString() , ex));
			}
			return resultPromo;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="brandID"></param>
		/// <param name="memberID"></param>
		/// <param name="activeOnly"></param>
		/// <returns></returns>
		public Promo GetPromoByID(int promoID,int brandID, int memberID, bool activeOnly)
		{
			Promo resultPromo=null;
			string uri=string.Empty;
			try
			{
				uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					resultPromo= getService(uri).GetPromoByID (promoID,brandID,memberID, activeOnly);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex)
			{
				throw(new SAException("GetPromoByID() error (uri: " + uri + "). promoID=" + promoID.ToString() , ex));
			}
			return resultPromo;
		}

		/// <summary>
		/// This method takes in substring of the Promo's description and returns all promos that match that.
		/// </summary>
		/// <param name="description"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public PromoCollection GetPromosByDescription(string description, int brandID)
		{
			PromoCollection resultPromos = null;
			string uri=string.Empty;
			try
			{
				uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					resultPromos = getService(uri).GetPromosByDescription(description, brandID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex)
			{
				throw(new SAException("GetPromosByDescription() error (uri: " + uri + "). brandID=" + brandID.ToString() , ex));
			}
			return resultPromos;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public PromoCollection GetActivePromos(int brandID)
		{
			PromoCollection resultPromo=null;
			string uri=string.Empty;
			try
			{
				uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					resultPromo= getService(uri).GetActivePromos(brandID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex)
			{
				throw(new SAException("GetActivePromos() error (uri: " + uri + "). BrandID=" + brandID.ToString() , ex));
			}
			return resultPromo;
		}
		/// <summary>
		/// Persists the PromoCollection to the following fields of each promo; EndDate, Description, StartDate, ListOrder, PageTitle.
		/// </summary>
		/// <param name="promos"></param>
		/// <param name="brandID"></param>
		public void SavePromotions(PromoCollection promos, int brandID)
		{
			string uri=string.Empty;
			try
			{
				uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					getService(uri).SavePromos(promos,brandID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex)
			{
				throw(new SAException("SavePromotions() error (uri: " + uri + "). BrandID=" + brandID.ToString() , ex));
			}
			
			return ;
		}
		
		/// <summary>
		/// This method should be only called by SparkWS.Subscription for updating promo approval status from PromoApproval sytem.
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="brandID"></param>
		/// <param name="approvalStatus"></param>
		public void SavePromoApproval(int promoID, int brandID, int approvalStatus)
		{
			string uri=string.Empty;
			try
			{
				uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					Promo promo = this.GetPromoByID(promoID, brandID, false);

					if (promo == null)
						throw new Exception("Promo not found. PromoID=" + promoID.ToString() + ", BrandID=" + brandID.ToString());

					promo.PromoApprovalStatus = (Matchnet.PromoEngine.ValueObjects.PromoApprovalStatus) approvalStatus;
					getService(uri).SavePromo(promo, brandID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex)
			{
				throw(new SAException("SavePromotions() error (uri: " + uri + "). PromoID=" + 
					promoID.ToString() + ", BrandID=" + brandID.ToString()  + ", PromoApprovalStatus=" + approvalStatus.ToString(), ex));
			}
		}


		/// <summary>
		/// This method adds a new promotion to site's promotion collection
		/// </summary>
		/// <remarks> In case of passing an existing promotion to this method, it tries to do an update.
		/// The update is restricted to only few fields.
		/// </remarks>
		public int  SavePromotion(Promo promo, int brandID)
		{
			string uri=string.Empty;
			int promoID = Constants.NULL_INT;
			try
			{
				uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					promoID = getService(uri).SavePromo(promo,brandID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex)
			{
				throw(new SAException("SavePromotion() error (uri: " + uri + "). BrandID=" + brandID.ToString() , ex));
			}
			
			return promoID;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public PromoAttributeCollection GetPromoAttributes()
		{
			PromoAttributeCollection  result=null;
			string uri=string.Empty;

			try
			{
				uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					result=getService(uri).GetPromoAttributes();
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex)
			{
				throw(new SAException("GetPromoAttributes() error (uri: " + uri + ")." , ex));
			}
			
			return result;	
		}

        public string[] GetRecurrenceJobNames()
        {
            string uri = string.Empty;
            string[] result;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    result = getService(uri).GetRecurrenceJobNames();
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("GetPromoAttributes() error (uri: " + uri + ").", ex));
            }

            return result;
        }

        public string[] GetRecurrenceTriggerNames()
        {
            string uri = string.Empty;
            string[] result;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    result = getService(uri).GetRecurrenceTriggerNames();
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("GetRecurrenceTriggerNames() error (uri: " + uri + ").", ex));
            }

            return result;
        }

        public List<JobDetail> ListJobDetails()
        {
            string uri = string.Empty;
            List<JobDetail> result;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    result = getService(uri).ListJobDetails();
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("ListJobDetails() error (uri: " + uri + ").", ex));
            }

            return result;
        }

		/// <summary>
		/// Determines if a promo has the necessary template info including promoplan, col and row templates.
		/// Use to differentiate between legacy promos that were created w/o any of the template info
		/// and new promos that have.
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public bool IsPromoTemplateDriven(int promoID, int brandID)
		{
			Promo promo = GetPromoByID(promoID, brandID);
			
			foreach(PromoPlan promoPlan in promo.PromoPlans)
			{
				// DB default value is set to 0 instead of null to prevent BL get code from blowing up.
				if (promoPlan.ResourceTemplateID == 0)
				{
					return false;
				}
			}

			if ((promo.RowHeaderResourceTemplateCollection.Count == 0) || 
				(promo.ColHeaderResourceTemplateCollection.Count == 0))
				return false;

			return true;
		}

		/// <summary>
		/// 
		/// </summary>
		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PROMOENGINESVC_SA_CONNECTION_LIMIT"));
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="uri"></param>
		/// <returns></returns>
		private IPromoEngineService getService(string uri)
		{
			try
			{
				return (IPromoEngineService)Activator.GetObject(typeof(IPromoEngineService), uri);
			}
			catch(Exception ex)
			{
				throw new SAException("Cannot activate remote service manager at " + uri, ex);
			}
		}		

		/// <summary>
		/// 
		/// </summary>
		/// <param name="serviceManagerName"></param>
		/// <returns></returns>
		private string GetServiceManagerUri(string serviceManagerName)
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(serviceManagerName);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PROMOENGINESVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					uri = "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}
				
				return uri;
			}
			catch(Exception ex)
			{
				throw new Exception("Cannot get configuration settings for remote service manager.", ex);
			}
		}
	}

}