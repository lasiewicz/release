using System;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.PromoEngine.ServiceDefinitions;
using Matchnet.PromoEngine.ValueObjects.ServiceDefinitions;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
using Matchnet.RemotingClient;
using Matchnet.CacheSynchronization.Context;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.PromoEngine.ServiceAdapters.TokenReplacement;

namespace Matchnet.PromoEngine.ServiceAdapters
{
	/// <summary>
	/// Summary description for ResourceTemplateSA.
	/// </summary>
	public class ResourceTemplateSA : SABase
	{
		/// <summary>
		///  Singleton instance
		/// </summary>
		public static readonly ResourceTemplateSA Instance = new ResourceTemplateSA();
		private Random _random = null;
		private Cache _cache = null;

		// Constructor
		private ResourceTemplateSA()
		{
			_cache = Cache.Instance;
			_random = new Random();
		}

		/// <summary>
		/// 
		/// </summary>
		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PROMOENGINESVC_SA_CONNECTION_LIMIT"));
		}

		/// <summary>
		/// Returns the entire collection of resource templates used for subscription promo plans. This is cached on both SA and BL level.
		/// Resource templates should be extracted at the SA level from the web tier for faster load and better performance.
		/// </summary>
		/// <returns></returns>
		public ResourceTemplateCollection GetResourceTemplateCollection()
		{
			ResourceTemplateCollection resourceTemplateCollection = null;
			string uri = string.Empty;
			
			try
			{
				resourceTemplateCollection = _cache.Get(ResourceTemplateCollection.RESOURCE_TEMPLATE_COLLECTION_CACHE_KEY) as ResourceTemplateCollection;

				if (resourceTemplateCollection == null)
				{
					uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_RESOURCETEMPLATE_NAME);
					Int32 cacheTTL = GenerateRandomTTL(Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PROMOENGINESVC_CACHE_TTL_RESOURCETEMPLATECOL_SA")));

					base.Checkout(uri);
					try
					{
						resourceTemplateCollection = getService(uri).GetResourceTemplateCollection(System.Environment.MachineName,
							Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)));
					}
					finally
					{
						base.Checkin(uri);
					}

					if (resourceTemplateCollection == null)
						throw new SAException("ResourceTemplateCollection cannot be null.");

					resourceTemplateCollection.CacheTTLSeconds = cacheTTL;
					_cache.Insert(resourceTemplateCollection);
				}

				return resourceTemplateCollection;
			}
			catch(Exception ex)
			{
				throw(new SAException("GetResourceTemplateCollection() error (uri: " + uri + "). " + ex.ToString() , ex));
			}
		}

		/// <summary>
		/// Returns a resource template based on the resource template ID.
		/// </summary>
		/// <param name="resourceTemplateID">ResourceTemplateID</param>
		/// <returns></returns>
		public ResourceTemplate GetResourceTemplateByID(int resourceTemplateID)
		{
			foreach(ResourceTemplate resourceTemplate in GetResourceTemplateCollection())
			{
				if (resourceTemplate.ResourceTemplateID == resourceTemplateID)
				{
					return resourceTemplate;
				}
			}

			return null;
		}
		public ResourceTemplate GetResourceTemplateByGroupDefault(int groupID)
		{
			foreach(ResourceTemplate resourceTemplate in GetResourceTemplateCollection())
			{
				if ((resourceTemplate.GroupID == groupID) && (resourceTemplate.GroupDefault == true))
				{
					return resourceTemplate;
				}
			}

			return null;
		}

		/// <summary>
		/// Returns the resource string with the tokens replaced.
		/// </summary>
		/// <param name="resourceTemplateID">ResourceTemplateID</param>
		/// <param name="tokenReplacer">Implementation of ITokenReplacer that will handle the token replacements</param>
		/// <returns></returns>
		public string GetResource(int resourceTemplateID, ITokenReplacer tokenReplacer)
		{
			string retVal = string.Empty;

			ResourceTemplate resourceTemplate = GetResourceTemplateByID(resourceTemplateID);

			if(resourceTemplate != null)
				retVal = tokenReplacer.ReplaceTokens(resourceTemplate.Content);

			return retVal;
		}

		/// <summary>
		/// For creating and updating a resource template.
		/// </summary>
		/// <param name="resourceTemplate">To create, pass in with default resource template ID.</param>
		/// <returns>Saved ResourceTemplate</returns>
		public ResourceTemplate SaveResourceTemplate(ResourceTemplate resourceTemplate)
		{
			ResourceTemplate resourceTemplateReturned = null;
			string uri=string.Empty;
			try
			{
				uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_RESOURCETEMPLATE_NAME);

				base.Checkout(uri);
				try
				{
					resourceTemplateReturned = getService(uri).SaveResourceTemplate(resourceTemplate);
					_cache.Remove(ResourceTemplateCollection.RESOURCE_TEMPLATE_COLLECTION_CACHE_KEY);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex)
			{
				throw(new SAException("SaveResourceTemplate() error (uri: " + uri + "). ResourceTemplateID=" + resourceTemplate.ToString() , ex));
			}
			return resourceTemplateReturned;
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="serviceManagerName"></param>
		/// <returns></returns>
		private string GetServiceManagerUri(string serviceManagerName)
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(serviceManagerName);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PROMOENGINESVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					uri = "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}
				
				return uri;
			}
			catch(Exception ex)
			{
				throw new Exception("Cannot get configuration settings for remote service manager.", ex);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="uri"></param>
		/// <returns></returns>
		private IResourceTemplateService getService(string uri)
		{
			try
			{
				return (IResourceTemplateService)Activator.GetObject(typeof(IResourceTemplateService), uri);
			}
			catch(Exception ex)
			{
				throw new SAException("Cannot activate remote service manager at " + uri, ex);
			}
		}		

		/// <summary>
		/// 
		/// Taken from Conent Service.
		/// 
		/// There is a fear that perhaps having all Content objects have the same TTL was causing
		/// a mass loading of Content objects by the web tier.  To alleviate this, we will be taking the TTL and 
		/// generating a random TTL that is between ttl and 2 x ttl.
		/// </summary>
		/// <param name="ttl"></param>
		/// <returns></returns>
		private int GenerateRandomTTL(int ttl)
		{
			return ttl + _random.Next(ttl / 2);
		}

	}
}
