
using System;
using System.Diagnostics;
using System.Collections.Generic;

using Quartz;

#region Matchnet references
	using Matchnet;
	using Matchnet.Exceptions;
	using Matchnet.Data.Hydra;
	using Matchnet.PromoEngine.BusinessLogic;
	using Matchnet.PromoEngine.ServiceDefinitions;
	using Matchnet.PromoEngine.ValueObjects;
	using Matchnet.Replication;
#endregion

namespace Matchnet.PromoEngine.ServiceManagers
{
	/// <summary>
	/// 
	/// </summary>
	public class PromoEngineSM: MarshalByRefObject, IPromoEngineService, IServiceManager, IBackgroundProcessor,IDisposable, IReplicationActionRecipient
		
	{
		
		private HydraWriter _hydraWriter;
		private PerformanceCounter _perfAllHitCount;
		private PerformanceCounter _perfMatchedHitCount;
		private Replicator _replicator = null;
		/// <summary>
		/// 
		/// </summary>
		public PromoEngineSM()
		{
			initPerfCounters();
			_hydraWriter = new HydraWriter(new string[]{"mnSubscription"});

			PromoEngineBL.Instance.MatchRequested += new PromoEngineBL.PromoEngineMatchRequestedEventHandler(PromoEngineBL_MatchRequested);  
			PromoEngineBL.Instance.MatchFound += new PromoEngineBL.PromoEngineMatchFoundEventHandler(PromoEngineBL_MatchFound);  
			//initializing replicator
			string replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PROMOENGINESVC_REPLICATION_OVERRIDE");			
			string machineName = System.Environment.MachineName;
			if (replicationURI.Length == 0)
			{
				replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(Matchnet.PromoEngine.ValueObjects.ServiceConstants.SERVICE_CONSTANT, Matchnet.PromoEngine.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME, machineName);				
			}

            if (!String.IsNullOrEmpty(replicationURI))
            {
                PromoEngineBL.Instance.ReplicationRequested += new PromoEngineBL.ReplicationEventHandler(MemberBL_ReplicationRequested);			

                _replicator = new Replicator(Matchnet.PromoEngine.ValueObjects.ServiceConstants.SERVICE_NAME);
                _replicator.SetDestinationUri(replicationURI);
                _replicator.Start();
            }
            else
            {
                System.Diagnostics.EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "PromoEngineSM constructor, replicator partner URL is null. Replicator not started.", System.Diagnostics.EventLogEntryType.Warning);
            }

			
			System.Diagnostics.Trace.WriteLine("PromoEngineSM constructor completed.");
		}
		
		/// <summary>
		/// keeps service alive!
		/// </summary>
		/// <returns></returns>
		public override object InitializeLifetimeService()

		{

			return null;

		}

		private void MemberBL_ReplicationRequested(IReplicationAction replicationAction)
		{
			System.Diagnostics.Debug.WriteLine("_replicator.Enqueue.");
			_replicator.Enqueue(replicationAction);
		}

		#region IPromoEngineService members
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="brand"></param>
		/// <param name="paymentType"></param>
		/// <returns></returns>
		public Promo GetAdminPromo(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, Matchnet.Purchase.ValueObjects.PaymentType paymentType)
		{
			try
			{
				return PromoEngineBL.Instance.GetAdminPromo(memberID, brand, paymentType);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetAdminPromo() error. ", ex);
			}	
		}
        /// <summary>
        /// Returns a supervisor only promo
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="brand"></param>
        /// <param name="paymentType"></param>
        /// <returns></returns>
        public Promo GetSupervisorPromo(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, Matchnet.Purchase.ValueObjects.PaymentType paymentType)
        {
            try
            {
                return PromoEngineBL.Instance.GetSupervisorPromo(memberID, brand, paymentType);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetSupervisorPromo() error. ", ex);
            }
        }
		/// <summary>
		/// Checks to see if a member qualifies for a specific promo.
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="brand"></param>
		/// <param name="promo"></param>
		/// <returns></returns>
		public bool QualifyPromo(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, Promo promo)
		{
			try
			{
				return PromoEngineBL.Instance.QualifyPromo(memberID, brand, promo);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "QualifyPromo() error", ex);
			}
		}
		/// <summary>
		/// This is the main method for getting promos from the full site.
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="brand"></param>
		/// <param name="isAdmin"></param>
		/// <returns></returns>
		public Promo GetMemberPromo(int memberID,Matchnet.Content.ValueObjects.BrandConfig.Brand brand,
						Matchnet.Purchase.ValueObjects.PaymentType paymentType)
		{			
			try
			{
			    return GetMemberPromo(memberID, brand, paymentType, PromoType.Member);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetMemberPromo() error. ", ex);
			}			
		}
        /// <summary>
        /// This overloaded method was to add a new type, mobile for mobile sites.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="brand"></param>
        /// <param name="isAdmin"></param>
        /// <returns></returns>
        public Promo GetMemberPromo(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand,
                        Matchnet.Purchase.ValueObjects.PaymentType paymentType, PromoType promoType)
        {
            try
            {
                return PromoEngineBL.Instance.GetMemberPromo(memberID, brand, paymentType, promoType);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetMemberPromo() error. ", ex);
            }
        }
		/// <summary>
		/// 
		/// </summary>		
		/// <param name="brandID"></param>
		/// <returns></returns>
		public PromoCollection GetPromos(int brandID)
		{
			
			try
			{
				return PromoEngineBL.Instance.GetPromos(brandID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetPromos() error. ", ex);
			}

		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public PromoCollection GetActivePromos(int brandID)
		{
			
			try
			{
				return PromoEngineBL.Instance.GetActivePromos(brandID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetActivePromos() error. ", ex);
			}

		}
		/// <summary>
		/// Returns an active promo with matching PromoID  
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public Promo GetPromoByID(int promoID,int brandID)
		{
			try
			{
				return PromoEngineBL.Instance.GetPromoByID(promoID,brandID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetPromoByID() error. ", ex);
			}
		}
		
		/// <summary>
		/// Returns a promo by ID even inactive can be retrieved if specified.
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="brandID"></param>
		/// <param name="activeOnly"></param>
		/// <returns></returns>
		public Promo GetPromoByID(int promoID, int brandID, bool activeOnly)
		{
			try
			{
				return PromoEngineBL.Instance.GetPromoByID(promoID, brandID, activeOnly);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetPromoByID() error. ", ex);
			}
		}

		/// <summary>
		/// Returns an active promo with matching PromoID with member specific UI metadata
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="brandID"></param>
		/// <param name="memberID"></param>
		/// <returns></returns>
		public Promo GetPromoByID(int promoID, int brandID, int memberID)
		{
			try
			{
				return PromoEngineBL.Instance.GetPromoByID(promoID,brandID, memberID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetPromoByID() error. ", ex);
			}
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="brandID"></param>
		/// <param name="memberID"></param>
		/// <param name="activeOnly"></param>
		/// <returns></returns>
		public Promo GetPromoByID(int promoID, int brandID, int memberID, bool activeOnly)
		{
			try
			{
				return PromoEngineBL.Instance.GetPromoByID(promoID,brandID, memberID, activeOnly);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetPromoByID() error. ", ex);
			}
		}
		/// <summary>
		/// This method takes in substring of the Promo's description and returns all promos that match that.
		/// </summary>
		/// <param name="description"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public PromoCollection GetPromosByDescription(string description, int brandID)
		{
			try
			{
				return PromoEngineBL.Instance.GetPromosByDescription(description, brandID);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetPromosByDescription() error. ", ex);
			}
		}
		/// <summary>
		/// Persists the PromoCollection to the following fields of each promo; EndDate, Description, StartDate, ListOrder, PageTitle.
		/// </summary>
		/// <param name="promos"></param>
		/// <param name="brandID"></param>
		public void SavePromos(PromoCollection promos, int brandID)
		{
			try
			{
				PromoEngineBL.Instance.SavePromos(promos, brandID);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "SavePromos() error. ", ex);
			}
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="promo"></param>
		/// <param name="brandID"></param>
		public int SavePromo(Promo promo, int brandID)
		{
			try
			{
				return PromoEngineBL.Instance.SavePromo(promo,brandID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "SavePromo() error. ", ex);
			}
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public PromoAttributeCollection GetPromoAttributes()
		{
			try
			{
				return PromoEngineBL.Instance.GetPromoAttributes();
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetPromoAttributes() error. ", ex);
			}
			
		}

        public List<JobDetail> ListJobDetails()
        {
            try
            {
                return RecurrenceSchedulerBL.Instance.ListJobDetails();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetPromoAttributes() error. ", ex);
            }
        }

        public string[] GetRecurrenceJobNames()
        {
            try
            {
                return RecurrenceSchedulerBL.Instance.ListJobs();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetPromoAttributes() error. ", ex);
            }
        }

        public string[] GetRecurrenceTriggerNames()
        {
            try
            {
                return RecurrenceSchedulerBL.Instance.ListTriggers();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetPromoAttributes() error. ", ex);
            }
        }

		#endregion

		#region IServiceManager members
			/// <summary>
			/// 
			/// </summary>
			public void PrePopulateCache()
			{
				// no implementation at this time
			}
		

			/// <summary>
			/// IDisposable Members
			/// </summary>
				
			public void Dispose()
			{
			}
		#endregion 
		
		#region IBackgroundProcessor members
		/// <summary>
		/// 
		/// </summary>
		public void Start()
		{
			System.Diagnostics.Trace.WriteLine("PromoEngineSM start.");
			_hydraWriter.Start();

            RecurrenceSchedulerBL.Instance.Start();
		}
		/// <summary>
		/// 
		/// </summary>
		public void Stop()
		{
			System.Diagnostics.Trace.WriteLine("PromoEngineSM Stop.");
			if(_hydraWriter!=null)
			{
				_hydraWriter.Stop();
			}
			if(_replicator!=null)
			{
				_replicator.Stop();
			}

            RecurrenceSchedulerBL.Instance.Shutdown();
		}
		#endregion

		#region IReplicationActionRecipient member
		void IReplicationActionRecipient.Receive(IReplicationAction replicationAction)
		{
			try
			{				
				PromoEngineBL.Instance.PlayReplicationAction(replicationAction);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ValueObjects.ServiceConstants.SERVICE_NAME, "Failure processing replication object.", ex);
			}			
		}

		#endregion

		#region instrumentation (performance counters)
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public static CounterCreationData[] GetCounters()
		{	
			return new CounterCreationData [] {	new CounterCreationData( ServiceConstants.PERF_TOTALCOUNT_NAME, "Count of all requested Items.", PerformanceCounterType.NumberOfItems64),
												  new CounterCreationData(ServiceConstants.PERF_TOTAL_MATCH_COUNT_NAME, "Count of all requests that machted a promotion.", PerformanceCounterType.NumberOfItems64)};
		}

		private void initPerfCounters()
		{
			try
			{
				System.Diagnostics.Trace.WriteLine("PromoEngineSM, Start initPerfCounters");
				
				_perfAllHitCount= new PerformanceCounter(ServiceConstants.SERVICE_NAME, ServiceConstants.PERF_TOTALCOUNT_NAME, false);
				_perfMatchedHitCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, ServiceConstants.PERF_TOTAL_MATCH_COUNT_NAME, false);

				resetPerfCounters();
				System.Diagnostics.Trace.WriteLine("PromoEngineSM, Finish initPerfCounters");
			}
			catch
			{ System.Diagnostics.Trace.WriteLine("PromoEngineSM Exception in initPerfCounters");}
		}

		/// <summary>
		/// 
		/// </summary>
		public static void PerfCounterInstall()
		{
			try
			{
				System.Diagnostics.Trace.WriteLine("PromoEngineSM,Start PerfCounterInstall");
				
				CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
				ccdc.AddRange(PromoEngineSM.GetCounters());

				PerformanceCounterCategory.Create(ServiceConstants.SERVICE_NAME, ServiceConstants.SERVICE_NAME, ccdc);
				System.Diagnostics.Trace.WriteLine("PromoEngineSM,Finish PerfCounterInstall");
			}
			catch(Exception ex)
			{
					System.Diagnostics.Trace.WriteLine("PromoEngineSM Exception in PerfCounterInstall.");
					new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "PerfCounterInstall() error. ", ex);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public static void PerfCounterUninstall()
		{	
			PerformanceCounterCategory.Delete(ServiceConstants.SERVICE_NAME);
		}		
		/// <summary>
		/// 
		/// </summary>
		private void resetPerfCounters()
		{
			_perfAllHitCount.RawValue = 0;
			_perfMatchedHitCount.RawValue=0;
			System.Diagnostics.Trace.WriteLine("Perf counters reseted.");		
		}

		private void PromoEngineBL_MatchRequested()
		{
			_perfAllHitCount.Increment();
		}
		private void PromoEngineBL_MatchFound()
		{
			_perfMatchedHitCount.Increment();
		}

		

		#endregion
	}
}
