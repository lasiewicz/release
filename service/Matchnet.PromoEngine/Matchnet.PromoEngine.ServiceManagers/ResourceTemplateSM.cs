using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Exceptions;
using Matchnet.Data.Hydra;
using Matchnet.Replication;
using Matchnet.CacheSynchronization;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.PromoEngine.BusinessLogic;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
using Matchnet.PromoEngine.ValueObjects.ServiceDefinitions;

using System;
using System.Data;

namespace Matchnet.PromoEngine.ServiceManagers
{
	/// <summary>
	/// Summary description for PagePixelSM.
	/// </summary>
	public class ResourceTemplateSM : MarshalByRefObject, IServiceManager, IResourceTemplateService, IDisposable, IReplicationRecipient
	{
		#region constants
		private const string SERVICE_MANAGER_NAME = "ResourceTemplateSM";
		#endregion

		private HydraWriter hydraWriter; 
		internal static Replicator replicator = null;
		internal static Synchronizer synchronizer = null;
		
		/// <summary>
		/// 
		/// </summary>
		public ResourceTemplateSM()
		{
			// Hydrawriter for mnSubscription is already started by PromoEngineSM.
			ResourceTemplateBL.Instance.SynchronizationRequested += new Matchnet.PromoEngine.BusinessLogic.ResourceTemplateBL.SynchronizationEventHandler(ResourceTemplateBL_SynchronizationRequested);

			String machineName = System.Environment.MachineName;
			String overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
			if(overrideMachineName != null)
			{
				machineName = overrideMachineName;
			}

			String replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PROMOENGINESVC_REPLICATION_OVERRIDE");
			if (replicationURI.Length == 0)
			{
				replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(Matchnet.PromoEngine.ValueObjects.ServiceConstants.SERVICE_CONSTANT, SERVICE_MANAGER_NAME, machineName);
			}

            if (!String.IsNullOrEmpty(replicationURI))
            {
                ResourceTemplateBL.Instance.ReplicationRequested += new Matchnet.PromoEngine.BusinessLogic.ResourceTemplateBL.ReplicationEventHandler(ResourceTemplateBL_ReplicationRequested);

                replicator = new Replicator(SERVICE_MANAGER_NAME);
                replicator.SetDestinationUri(replicationURI);
                replicator.Start();
            }
            else
            {
                System.Diagnostics.EventLog.WriteEntry(Matchnet.PromoEngine.ValueObjects.ServiceConstants.SERVICE_NAME, "ResourceTemplateSM constructor, replicator partner URL is null. Replicator not started.", System.Diagnostics.EventLogEntryType.Warning);
            }

            

			synchronizer = new Synchronizer(ValueObjects.ServiceConstants.SERVICE_NAME);
			synchronizer.Start();
		}
		void IReplicationRecipient.Receive(IReplicable replicable)
		{
			try
			{
				ResourceTemplateBL.Instance.CacheReplicatedObject(replicable);
#if DEBUG
				System.Diagnostics.EventLog.WriteEntry("ResourceTemplateSM", "Item replicated.");
#endif
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ValueObjects.ServiceConstants.SERVICE_NAME, "Failure processing replication object.", ex);
			}
		}

		private void ResourceTemplateBL_ReplicationRequested(IReplicable replicableObject)
		{
			replicator.Enqueue(replicableObject);
		}

		private void ResourceTemplateBL_SynchronizationRequested(string key, System.Collections.Hashtable cacheReferences)
		{
#if DEBUG
			foreach( System.Collections.DictionaryEntry entry in cacheReferences)
			{
				System.Text.StringBuilder sb = new System.Text.StringBuilder();
				sb.Append("Synchronization(cache expiration on ResourceTemplateCollection)" + System.Environment.NewLine);
				sb.Append("Key:" + entry.Key.ToString() + " " + "Value:" + entry.Value.ToString() + System.Environment.NewLine);
				System.Diagnostics.EventLog.WriteEntry("ResourceTemplateSM", sb.ToString());
			}
#endif
			synchronizer.Enqueue(key, cacheReferences);
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <returns></returns>
		public ResourceTemplateCollection GetResourceTemplateCollection(string clientHostName, CacheReference cacheReference)
		{
			ResourceTemplateCollection resourceTemplateCollection = null;
	
			try
			{
				resourceTemplateCollection = ResourceTemplateBL.Instance.GetResourceTemplateCollection(clientHostName, cacheReference);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ValueObjects.ServiceConstants.SERVICE_NAME,"Failure getting resource template collection.", ex);
			} 

			return resourceTemplateCollection;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="resourceTemplate"></param>
		/// <returns></returns>
		public ResourceTemplate SaveResourceTemplate(ResourceTemplate resourceTemplate)
		{
			try
			{
				return ResourceTemplateBL.Instance.SaveResourceTemplate(resourceTemplate);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ValueObjects.ServiceConstants.SERVICE_NAME,"Failure saving resource template.", ex);
			}
		}

		#region IServiceManager Members
		public void PrePopulateCache()
		{
			// no implementation at this time
		}
		#endregion

		#region IDisposable Members
		public void Dispose()
		{
			if (replicator != null)
			{
				replicator.Stop();
			}

			if (synchronizer != null)
			{
				synchronizer.Stop();
			}

			if (hydraWriter != null)
			{
				hydraWriter.Stop();
			}
		}
		#endregion

		public override object InitializeLifetimeService()
		{
			return null;
		}
	}
}
