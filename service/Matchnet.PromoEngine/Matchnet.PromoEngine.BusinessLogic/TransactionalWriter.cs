
using System;
using System.Data;
using System.EnterpriseServices;

#region Matchnet references
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
#endregion

namespace Matchnet.PromoEngine.BusinessLogic
{
	/// <summary>
	/// Summary description for TransactionalWriter.
	/// </summary>
	[Transaction(TransactionOption.Required)]	
	public class TransactionalWriter: ServicedComponent
	{
/// <summary>
/// 
/// </summary>
		public TransactionalWriter()
		{}

		/// <summary>
		/// This method is used for submitting new ListOrders for a collection of promos.  It only saves the Promo information, whic
		/// does not include PromoPlans, Promo Expressions, and HeaderTemplate information. 
		/// </summary>
		/// <param name="promos"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public bool PersistPromos(PromoCollection promos, int brandID)
		{
			bool result = false;
			
			if(promos != null)
			{
				try
				{
					Command command;
					SyncWriter sw; 
					Exception ex;

					foreach(Promo promo in promos)
					{
						command =null;
						sw=null;
						ex=null;

						// Although we pass everything here, the SP only updates PromoApprovalStatus, EndDate, Description, StartDate, ListOrder, PageTitle
						command = new Command("mnSubscription", "dbo.up_Promo_Save", 0);
						command.AddParameter("@PromoID", SqlDbType.Int, ParameterDirection.Input, promo.PromoID);					
						command.AddParameter("@Description", SqlDbType.NVarChar, ParameterDirection.Input, promo.Description);
						command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, promo.StartDate);
						command.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, promo.EndDate);
						command.AddParameter("@ListOrder", SqlDbType.Int, ParameterDirection.Input, promo.ListOrder);
						command.AddParameter("@DefaultPlanID", SqlDbType.Int, ParameterDirection.Input, promo.DefaultPlanID);
						command.AddParameter("@BestValuePlanID", SqlDbType.Int, ParameterDirection.Input, promo.BestValuePlanID);
						command.AddParameter("@PageTitle", SqlDbType.NVarChar, ParameterDirection.Input, promo.PageTitle);
                        command.AddParameter("@LegalNote", SqlDbType.NVarChar, ParameterDirection.Input, promo.LegalNote);
						command.AddParameter("@BrandID", SqlDbType.Int, ParameterDirection.Input, brandID);
						command.AddParameter("@PaymentTypeID", SqlDbType.Int, ParameterDirection.Input, (int)promo.PaymentType);						
						command.AddParameter("@PromoApprovalStatus", SqlDbType.Int, ParameterDirection.Input, (int)promo.PromoApprovalStatus);
						command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, promo.AdminMemberID);
						command.AddParameter("@PromoTypeID", SqlDbType.SmallInt, ParameterDirection.Input, (int)promo.PromoType);
                        command.AddParameter("@Recurring", SqlDbType.Bit, ParameterDirection.Input, promo.Recurring);
                        if (promo.RecurrenceCronExpression != Constants.NULL_STRING)
                            command.AddParameter("@RecurrenceCronExpression", SqlDbType.NVarChar, ParameterDirection.Input, promo.RecurrenceCronExpression);
                        if (promo.RecurrenceActiveUntilDateTime != DateTime.MinValue)
                            command.AddParameter("@RecurrenceActiveUntilDateTime", SqlDbType.SmallDateTime, ParameterDirection.Input, promo.RecurrenceActiveUntilDateTime);
                        if (promo.RecurrenceDurationHours != Constants.NULL_INT)
                            command.AddParameter("@RecurrenceDurationHours", SqlDbType.Int, ParameterDirection.Input, promo.RecurrenceDurationHours);

						sw = new SyncWriter();					
						sw.Execute(command, null, out ex);
						sw.Dispose();

						if (ex != null)
						{
							throw ex;
						}
                        						
					}

					ContextUtil.SetComplete();

					// Successful save of PromoCollection
					result=true;
				}
				catch (Exception exception)
				{
					ContextUtil.SetAbort();
					throw exception;
				}
				finally
				{
				}
			}
			else
			{
				System.Diagnostics.EventLog.WriteEntry("PromoEngineBL.TransactionalWriter.PersistPromo()","failed saving PromoCollection. PromoCollection object is null!",System.Diagnostics.EventLogEntryType.Warning);
			}

			return result;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="headerResourceTemplate"></param>
		public void DeletePromoHeaderResourceTemplate(HeaderResourceTemplate headerResourceTemplate)
		{
			try
			{
				Exception ex;

				if (headerResourceTemplate == null)
					throw new BLException("HeaderResourceTemplate cannot be null.");

				Command command = new Command("mnSubscription", "up_PromoHeaderResourceTemplate_Delete", 0);

				command.AddParameter("@PromoID", SqlDbType.Int, ParameterDirection.Input, headerResourceTemplate.PromoID);
				command.AddParameter("@PromoHeaderResourceTemplateID", SqlDbType.Int, ParameterDirection.Input, headerResourceTemplate.HeaderResourceTemplateID);

				SyncWriter sw = new SyncWriter();
				sw.Execute(command, null, out ex);
				sw.Dispose();
				if (ex != null)
				{
					throw ex;
				}

				// Caching and replication should be handled by the parent object, Promo
			}
			catch(Exception ex)
			{
				throw new BLException("Error deleting header resource template." + ex.ToString(), ex);
			}
		}

		
		/// <summary>
		/// This should be called only when saving/updating a Promo.
		/// </summary>
		/// <param name="headerResourceTemplate"></param>
		/// <param name="duration"></param>
		/// <param name="durationType"></param>
		/// <param name="planType"></param>
		public void SavePromoHeaderResourceTemplate(HeaderResourceTemplate headerResourceTemplate, int planType, int premiumType, int duration, int durationType)
		{
			try
			{
				Exception ex;

				if (headerResourceTemplate == null)
					throw new BLException("HeaderResourceTemplate cannot be null.");

				Command command = new Command("mnSubscription", "up_PromoHeaderResourceTemplate_Save", 0);

				if (headerResourceTemplate.HeaderResourceTemplateID == Constants.NULL_INT)
					command.AddParameter("@PromoHeaderResourceTemplateID", SqlDbType.Int, ParameterDirection.Input, KeySA.Instance.GetKey("[PromoHeaderResourceTemplateID]"));
				else
					command.AddParameter("@PromoHeaderResourceTemplateID", SqlDbType.Int, ParameterDirection.Input, headerResourceTemplate.HeaderResourceTemplateID);

				command.AddParameter("@PromoID", SqlDbType.Int, ParameterDirection.Input, headerResourceTemplate.PromoID);
				command.AddParameter("@ResourceTemplateID", SqlDbType.Int, ParameterDirection.Input, headerResourceTemplate.ResourceTemplateID);
				command.AddParameter("@HeaderNumber", SqlDbType.SmallInt, ParameterDirection.Input, headerResourceTemplate.HeaderNumber);
				command.AddParameter("@HeaderType", SqlDbType.SmallInt, ParameterDirection.Input, (Int16)headerResourceTemplate.HeaderType);

				if ((planType != Constants.NULL_INT) &&  (premiumType != Constants.NULL_INT))
				{
					command.AddParameter("@PlanType", SqlDbType.Int, ParameterDirection.Input, planType);
					command.AddParameter("@PremiumType", SqlDbType.Int, ParameterDirection.Input, premiumType);
				}

				if (duration != Constants.NULL_INT && durationType != Constants.NULL_INT)
				{
					command.AddParameter("@Duration", SqlDbType.Int, ParameterDirection.Input, duration);
					command.AddParameter("@DurationType", SqlDbType.Int, ParameterDirection.Input, durationType);
				}

				SyncWriter sw = new SyncWriter();
				sw.Execute(command, null, out ex);
				sw.Dispose();
				if (ex != null)
				{
					throw ex;
				}

				// Caching and replication should be handled by the parent object, Promo
			}
			catch(Exception ex)
			{
				throw new BLException("Error saving header resource template." + ex.ToString(), ex);
			}
		}
		 

		/// <summary>
		/// When the approval status is pending, then make a new promo approval request queue to 
		/// PromoApprovalWS that is external(due to compliance) to Bedrock.
		/// This web service method is included inside the transaction to gurantee a message sent to the promoapproval system.
		/// </summary>
		/// <param name="brandID"></param>
		///<param name="promo"></param>
		private void SavePromoApprovalToWebService(Promo promo, int brandID)
		{
			try
			{
				if (promo.PromoApprovalStatus != PromoApprovalStatus.Pending)
					return;

				PromoApprovalWS.PromoApprovalWS service = new Matchnet.PromoEngine.BusinessLogic.PromoApprovalWS.PromoApprovalWS();

				Member.ServiceAdapters.Member subadmin = Member.ServiceAdapters.MemberSA.Instance.GetMember(
					promo.AdminMemberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);

				Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);

                service.SavePromoApproval(
                    promo.PromoID,
                    subadmin.MemberID,
                    subadmin.EmailAddress,
                    brandID,
                    brand.Uri,
                    (short)promo.PromoApprovalStatus);
			}
			catch(Exception ex)
			{
				throw new BLException("Error sending a promoapproval request to the promoapproval web service.", ex);
			}
		}


		/// <summary>
		///	This method persists a promo object to its corresponding tables in database.
		///	It uses a transaction to enforce data integrity (by rolling back)
		///	in case of failure of any of database writes.
		/// </summary>
		/// <param name="promo"></param>
		/// <param name="brandID"></param>
		/// <param name="promoID"></param>
		/// <returns></returns>
		/// <remarks>
		/// For a save new promo operation tasks 1-4 are going to be executed.
		///	For an update operation only task 1 is executed.
		///	</remarks>
		public bool PersistPromo(Promo promo, int brandID, out int promoID)
		{
			bool result=false;
			promoID = Constants.NULL_INT;
			
			if (promo!=null)
			{	
				if(promo.PromoPlans!=null && promo.Criteria!=null)
				{
					Command command;
					SyncWriter sw; 
					Exception ex;
					Int32 ExpressionID =0;
					Int32 PromoID = promo.PromoID;
				
					try
					{
					
						#region Task 1 Save Promotion
						if(PromoID<=0)	// if >0 it is an update
						{
							PromoID = KeySA.Instance.GetKey(PromoEngineBL.PRIMARYKEY_PROMOID);							
						}
						// Set the out parameter here
						promoID = PromoID;

						command =null;
						sw=null;
						ex=null;

						command = new Command("mnSubscription", "dbo.up_Promo_Save", 0);
						command.AddParameter("@PromoID", SqlDbType.Int, ParameterDirection.Input, PromoID);					
						command.AddParameter("@Description", SqlDbType.NVarChar, ParameterDirection.Input, promo.Description);
						command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, promo.StartDate);
						command.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, promo.EndDate);
						command.AddParameter("@ListOrder", SqlDbType.Int, ParameterDirection.Input, promo.ListOrder);
						command.AddParameter("@DefaultPlanID", SqlDbType.Int, ParameterDirection.Input, promo.DefaultPlanID);
						command.AddParameter("@BestValuePlanID", SqlDbType.Int, ParameterDirection.Input, promo.BestValuePlanID);
						command.AddParameter("@PageTitle", SqlDbType.NVarChar, ParameterDirection.Input, promo.PageTitle);
                        command.AddParameter("@LegalNote", SqlDbType.NVarChar, ParameterDirection.Input, promo.LegalNote);
						command.AddParameter("@BrandID", SqlDbType.Int, ParameterDirection.Input, brandID);
						command.AddParameter("@PaymentTypeID", SqlDbType.Int, ParameterDirection.Input, (int)promo.PaymentType);
						command.AddParameter("@PromoApprovalStatus", SqlDbType.Int, ParameterDirection.Input, (int)promo.PromoApprovalStatus);
						command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, promo.AdminMemberID);
						command.AddParameter("@PromoTypeID", SqlDbType.SmallInt, ParameterDirection.Input, (int)promo.PromoType);
						if (promo.UPSTemplateID != Constants.NULL_INT)
							command.AddParameter("@UPSTemplateID", SqlDbType.Int, ParameterDirection.Input, promo.UPSTemplateID);
                        command.AddParameter("@Recurring", SqlDbType.Bit, ParameterDirection.Input, promo.Recurring);
                        if (promo.RecurrenceCronExpression != Constants.NULL_STRING)
                            command.AddParameter("@RecurrenceCronExpression", SqlDbType.NVarChar, ParameterDirection.Input, promo.RecurrenceCronExpression);
                        if (promo.RecurrenceActiveUntilDateTime != DateTime.MinValue)
                            command.AddParameter("@RecurrenceActiveUntilDateTime", SqlDbType.SmallDateTime, ParameterDirection.Input, promo.RecurrenceActiveUntilDateTime);
                        if (promo.RecurrenceDurationHours != Constants.NULL_INT)
                            command.AddParameter("@RecurrenceDurationHours", SqlDbType.Int, ParameterDirection.Input, promo.RecurrenceDurationHours);
                          
						sw = new SyncWriter();					
						sw.Execute(command, null, out ex);
						sw.Dispose();

						if (ex != null)
						{
							throw ex;
						}

						#endregion
					
						#region Task 2 save Promo plans

						for (int i=0;i<promo.PromoPlans.Count;i++)
						{
							command =null;
							sw=null;
							ex=null;
							command = new Command("mnSubscription", "dbo.up_PromoPlan_Save", 0);
							command.AddParameter("@PromoID", SqlDbType.Int, ParameterDirection.Input, PromoID);
							command.AddParameter("@PlanID", SqlDbType.Int, ParameterDirection.Input, promo.PromoPlans[i].PlanID);
							command.AddParameter("@ListOrder", SqlDbType.Int, ParameterDirection.Input, promo.PromoPlans[i].ListOrder);
							command.AddParameter("@ColumnOrder", SqlDbType.Int, ParameterDirection.Input, promo.PromoPlans[i].ColumnGroup);					
							command.AddParameter("@ResourceTemplateID", SqlDbType.Int, ParameterDirection.Input, promo.PromoPlans[i].ResourceTemplateID);					
				
							sw = new SyncWriter();
							sw.Execute(command, null, out ex);
							sw.Dispose();

							if (ex != null)
							{
								throw ex;
							}
						}
						#endregion

						if (promo.PromoID<=0) // if it is a save operation vs update
						{
							#region Task 3-4 save Promo Expression

							foreach( Expression exp in promo.Criteria)
							{
								ExpressionID = KeySA.Instance.GetKey(PromoEngineBL.PRIMARYKEY_EXPRESSIONID); 
								command =null;
								sw=null;
								ex=null;

								command = new Command("mnSubscription", "dbo.up_PromoExpression_Save", 0);
								command.AddParameter("@ExpressionID", SqlDbType.Int, ParameterDirection.Input, ExpressionID);
								command.AddParameter("@PromoID", SqlDbType.Int, ParameterDirection.Input, PromoID);
								command.AddParameter("@AttributeID", SqlDbType.Int, ParameterDirection.Input, exp.Attribute.ID);					
								command.AddParameter("@OperationID", SqlDbType.SmallInt, ParameterDirection.Input, (int) exp.Operator);			


								sw = new SyncWriter();					
								sw.Execute(command, null, out ex);
								sw.Dispose();

								if (ex != null)
								{
									throw ex;
								}
								#region Task 4 save Promo Expression's values
								for (int i=0;i<exp.Values.Count;i++)
								{
									command =null;
									sw=null;
									ex=null;
									command = new Command("mnSubscription", "dbo.up_PromoExpressionvalueInt_Save", 0);
									command.AddParameter("@ExpressionID", SqlDbType.Int, ParameterDirection.Input, ExpressionID);

									if( exp.Attribute.DataType == Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Number  ||
										exp.Attribute.DataType == Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Mask ||
										exp.Attribute.DataType == Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Bit)
									{
								
										command.AddParameter("@Value", SqlDbType.Int, ParameterDirection.Input,(int)exp.Values[i] );
									}

									sw = new SyncWriter();
									sw.Execute(command, null, out ex);
									sw.Dispose();

									if (ex != null)
									{
										throw ex;
									}
							

								}
								#endregion

							}
							#endregion
						}

						#region Task 5 Save Promo Header Resource Templates

						foreach(HeaderResourceTemplate template in promo.RowHeaderResourceTemplateCollection)
						{
							if (template.MarkForDelete == true)
							{
								// PromoID maybe null when creating new. Set it to the newly created PromoID.
								template.PromoID = PromoID;
								DeletePromoHeaderResourceTemplate(template);
							}
						}

						foreach(HeaderResourceTemplate template in promo.ColHeaderResourceTemplateCollection)
						{
							if (template.MarkForDelete == true)
							{
								// PromoID maybe null when creating new. Set it to the newly created PromoID.
								template.PromoID = PromoID;
								DeletePromoHeaderResourceTemplate(template);
							}
						}

						foreach(RowHeaderResourceTemplate template in promo.RowHeaderResourceTemplateCollection)
						{
							// PromoID maybe null when creating new. Set it to the newly created PromoID.
							template.PromoID = PromoID;
							SavePromoHeaderResourceTemplate(template, Constants.NULL_INT, Constants.NULL_INT, template.Duration, (int)template.DurationType);
						}

						foreach(ColHeaderResourceTemplate template in promo.ColHeaderResourceTemplateCollection)
						{
							// PromoID maybe null when creating new. Set it to the newly created PromoID.
							template.PromoID = PromoID;
							SavePromoHeaderResourceTemplate(template, (int)template.PlanType, (int)template.PremiumType, Constants.NULL_INT, Constants.NULL_INT);
						}
						#endregion

						#region Task Send a new Promo Approval if it's a new item. Should be set to pending status.
						promo.PromoID = PromoID;
						SavePromoApprovalToWebService(promo, brandID);
						#endregion

						ContextUtil.SetComplete();

						// Promo's been saved. Save headerresourc
						result=true;
					}
					catch (Exception exception)
					{
						ContextUtil.SetAbort();
						throw exception;
					}
					finally
					{
					}
				}
				else
				{
					System.Diagnostics.EventLog.WriteEntry("PromoEngineBL.TransactionalWriter.PersistPromo()","failed saving Promo.plans or criteria is null, Promo object has no plans/criteria!",System.Diagnostics.EventLogEntryType.Warning);
				}
			}
			else
			{
				System.Diagnostics.EventLog.WriteEntry("PromoEngineBL.TransactionalWriter.PersistPromo()","failed saving Promo.Promo object is null!",System.Diagnostics.EventLogEntryType.Warning);
			}
			return result;
		}
	}
}
