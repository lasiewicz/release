﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Quartz;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.Exceptions;

namespace Matchnet.PromoEngine.BusinessLogic
{
    /// <summary>
    /// Activates a promo based on its recurrence schedule
    /// </summary>
    public class RecurrenceJobActivate : IJob
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public void Execute(JobExecutionContext context)
        {
            try
            {

                string instName = context.JobDetail.Name;
                string instGroup = context.JobDetail.Group;

                JobDataMap dataMap = context.JobDetail.JobDataMap;

                int promoID = dataMap.GetInt("PromoID");
                int brandID = dataMap.GetInt("BrandID");

                ActivateRecurrence(promoID, brandID);
            }
            catch (Exception ex)
            {
                throw new BLException("RecurrenceJobActivate.Execute() Error", ex);
            }
        }

        private void ActivateRecurrence(int promoID, int brandID)
        {
            Promo promo = PromoEngineBL.Instance.GetPromoByID(promoID, brandID);

            List<string> items = promo.RecurrenceCronExpression.Split(' ').ToList<string>();

            DateTime kickOffTime = new DateTime(DateTime.Now.Year,
                DateTime.Now.Month,
                DateTime.Now.Day,
                Convert.ToInt16(items[2]), 0, 0);

            promo.RecurrenceActiveUntilDateTime = kickOffTime.AddHours(promo.RecurrenceDurationHours);

            PromoEngineBL.Instance.SavePromo(promo, brandID);

            System.Diagnostics.Debug.WriteLine(DateTime.Now + " executing.PromoID:" + promoID + " BrandID:" + brandID
                + " activated until:" + promo.RecurrenceActiveUntilDateTime.ToString("g"));
        }
    }
}
