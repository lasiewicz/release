﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Quartz;

namespace Matchnet.PromoEngine.BusinessLogic
{
    /// <summary>
    /// 
    /// </summary>
    public class RecurrenceJobDeactivate : IJob
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public void Execute(JobExecutionContext context)
        {
            string instName = context.JobDetail.Name;
            string instGroup = context.JobDetail.Group;

            JobDataMap dataMap = context.JobDetail.JobDataMap;

            int promoID = dataMap.GetInt("PromoID");
            int siteID = dataMap.GetInt("SiteID");

            System.Diagnostics.Debug.WriteLine(DateTime.Now + " executing.PromoID:" + promoID + " SiteID:" + siteID);
        }
    }
}
