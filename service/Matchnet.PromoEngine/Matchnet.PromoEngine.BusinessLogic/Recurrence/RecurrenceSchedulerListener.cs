﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Matchnet.Exceptions;

using Quartz;
using Quartz.Collection;
using Quartz.Core;
using Quartz.Impl;
using Quartz.Spi;
using Quartz.Util;
using Quartz.Xml;
using Quartz.Job;
using Quartz.Listener;
using Quartz.Plugin;

namespace Matchnet.PromoEngine.BusinessLogic
{
    public class RecurrenceSchedulerListener : ISchedulerListener    
    {
        #region ISchedulerListener Members

        public void JobScheduled(Trigger trigger)
        {
            System.Diagnostics.Trace.WriteLine("RecurrenceSchedulerListener JobScheduled. TriggerJobName:" + trigger.JobName);
        }

        public void JobUnscheduled(string triggerName, string triggerGroup)
        {
            System.Diagnostics.Trace.WriteLine("RecurrenceSchedulerListener JobUnScheduled. TriggerName:" + triggerName);

        }

        public void JobsPaused(string jobName, string jobGroup)
        {
        }

        public void JobsResumed(string jobName, string jobGroup)
        {

        }

        public void SchedulerError(string msg, SchedulerException cause)
        {
            throw new BLException("RecurrenceSchedulerListener SchedulerError " + msg, cause.UnderlyingException);
        }

        public void SchedulerShutdown()
        {
            System.Diagnostics.Trace.WriteLine("RecurrenceSchedulerListener Shutdown");
        }

        public void TriggerFinalized(Trigger trigger)
        {

        }

        public void TriggersPaused(string triggerName, string triggerGroup)
        {

        }

        public void TriggersResumed(string triggerName, string triggerGroup)
        {

        }

        #endregion
    }
}
