﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Quartz;
using Quartz.Collection;
using Quartz.Core;
using Quartz.Impl;
using Quartz.Spi;
using Quartz.Util;
using Quartz.Xml;
using Quartz.Job;
using Quartz.Listener;
using Quartz.Plugin;

using Matchnet.PromoEngine.ValueObjects;
using Matchnet.Exceptions;

namespace Matchnet.PromoEngine.BusinessLogic
{
    public class RecurrenceSchedulerBL
    {
        public readonly static RecurrenceSchedulerBL Instance = new RecurrenceSchedulerBL();
        
        ISchedulerFactory schedFactory = new StdSchedulerFactory();
        IScheduler scheduler;

        private const string JOB_GROUP_NAME = "PROMOJOBS";

        /// <summary>
        /// Singleton
        /// </summary>
        private RecurrenceSchedulerBL()
        {
        }

        public void AddNewSchedule(Promo promo, int brandID)
        {
            try
            {
                if (promo.RecurrenceCronExpression == Constants.NULL_STRING)
                    throw new BLException("AddNewSchedule() Recurrence Cron Expression cannot be null. PromoID:" + promo.PromoID.ToString());

                if (!scheduler.IsStarted)
                    scheduler.Start();

                string jobName = "RecurrenceJobActivate:" + promo.PromoID;
                JobDetail jobDetail = new JobDetail(jobName, JOB_GROUP_NAME, typeof(RecurrenceJobActivate));
                jobDetail.JobDataMap["PromoID"] = promo.PromoID;
                jobDetail.JobDataMap["BrandID"] = brandID;
                jobDetail.JobDataMap["CronExpression"] = promo.RecurrenceCronExpression;

                string triggerName = "RecurrenceCronTrigger" + promo.PromoID;
                CronTrigger cronTrigger = new CronTrigger();
                cronTrigger.Name = triggerName;
                cronTrigger.Group = JOB_GROUP_NAME;
                cronTrigger.StartTimeUtc = DateTime.Now;
                cronTrigger.CronExpression = new CronExpression(promo.RecurrenceCronExpression);

                // TBD
                // It's an update, remove the existing one and re-add.
                //if (scheduler.GetJobDetail("RecurrenceJobActivate:" + promo.PromoID, JOB_GROUP_NAME) != null)
                //{
                //    scheduler.UnscheduleJob(triggerName, JOB_GROUP_NAME);
                //}

                if (scheduler.GetTrigger(triggerName, JOB_GROUP_NAME) == null)
                {
                    scheduler.ScheduleJob(jobDetail, cronTrigger);
                }
            }
            catch (Exception ex)
            {
                throw new Matchnet.Exceptions.BLException("Error scheduling a new job. PromoID:" + promo.PromoID.ToString(), ex);
            }
        }

        public void UpdateSchedule()
        { 
            throw new NotImplementedException();
        }

        public List<JobDetail> ListJobDetails()
        {
            string[] jobNames;
            List<JobDetail> jobDetails = new List<JobDetail>();

            jobNames = scheduler.GetJobNames(JOB_GROUP_NAME);
            
            for (int i=0; i < jobNames.Length; i++)
            {
                jobDetails.Add(scheduler.GetJobDetail(jobNames[i], JOB_GROUP_NAME));
            }
            return jobDetails;
        }

        public string[] ListTriggers()
        {
            return scheduler.GetTriggerNames(JOB_GROUP_NAME);
        }

        public string[] ListJobs()
        {
            return scheduler.GetJobNames(JOB_GROUP_NAME);
        }

        public void ClearJobs()
        {
            string[] jobs = this.ListJobs();

            foreach (string job in jobs)
            {
                scheduler.DeleteJob(job, JOB_GROUP_NAME);
            }
        }

        public void Start()
        {
            scheduler = schedFactory.GetScheduler();
            RecurrenceSchedulerListener recurrenceSchedulerListener = new RecurrenceSchedulerListener();
            scheduler.AddSchedulerListener(recurrenceSchedulerListener);
            scheduler.Start();
        }

        public void Shutdown()
        {
            scheduler.Shutdown(true);
        }
    }
}
