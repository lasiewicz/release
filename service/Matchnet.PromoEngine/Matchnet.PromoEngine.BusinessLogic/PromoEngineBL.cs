using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

#region Matchnet references

using System.Linq;
using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.PromoEngine.ValueObjects.ReplicationActions;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Replication;

#endregion

namespace Matchnet.PromoEngine.BusinessLogic
{
	/// <summary>
	/// Summary description for PromoEngineBL.
	/// </summary>
	public class PromoEngineBL
	{
		/// <summary>
		/// Primary key for table Promo
		/// </summary>
		public const string PRIMARYKEY_PROMOID = "PromoID";
		/// <summary>
		/// Primary key for table PromoExpression
		/// </summary>
		public const string PRIMARYKEY_EXPRESSIONID = "ExpressionID";

		/// <summary>
		/// Exposing the singletone instance of PromoEngineBL.
		/// </summary>
		public readonly static PromoEngineBL Instance = new PromoEngineBL();

		private Matchnet.Caching.Cache _cache;
		
		
		#region Promo Engine Events 
		
		
		/// <summary>
		/// Event hander for any incoming macth request 
		/// </summary>
		public delegate void PromoEngineMatchRequestedEventHandler();
		/// <summary>
		/// This event is fired any time there's an incoming match request
		/// </summary>
		public event PromoEngineMatchRequestedEventHandler MatchRequested;
		/// <summary>
		/// Event handler for when a request finds a matching promo
		/// </summary>
		public delegate void PromoEngineMatchFoundEventHandler();
		/// <summary>
		/// This event fires when a request finds a matching promo to return 
		/// </summary>		
		public event PromoEngineMatchFoundEventHandler MatchFound;
		/// <summary>
		/// This event handles the cache synchronization between service instances
		/// </summary>
		public delegate void ReplicationEventHandler(IReplicationAction replicationAction);
		/// <summary>
		/// 
		/// </summary>
		public event ReplicationEventHandler ReplicationRequested;

		#endregion
		
		/// <summary>
		/// 
		/// </summary>
		private PromoEngineBL()
		{
			System.Diagnostics.Trace.WriteLine("PromoEngineBL constructor.");
			_cache = Matchnet.Caching.Cache.Instance;
		}
		
        /// <summary>
        /// Returns the most recent promo of the filtered promos.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="brand"></param>
        /// <param name="promoType"></param>
        /// <param name="paymentType"></param>
        /// <returns></returns>
        private Promo GetMostRecentPromo(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, PromoType promoType, PaymentType paymentType)
        {
            Promo resultPromo = null;

            if (brand == null)
                throw new System.ArgumentException("GetMostRecentPromo() invalid (null) parameter.\nbrand parameter is null.");

            try
            {
                var activePromos = GetActivePromos(brand.BrandID);
                PromoCollection promoCol = new PromoCollection(brand.BrandID);
                DateTime maxDate = DateTime.MinValue;

                if (activePromos == null)
                    return null;

                var topMostPromo = (from Promo p in activePromos
                                 where p.PromoApprovalStatus == PromoApprovalStatus.Approved &&
                                       p.PromoType == promoType &&
                                       p.PaymentType == paymentType
                                    orderby p.InsertDate descending
                                    select p);

                if (topMostPromo.FirstOrDefault() == null)
                    return null;

                resultPromo = (Promo) topMostPromo.First().Clone();
                resultPromo.PromoPlans = SetupPlanCollectionForDisplay(resultPromo.PromoPlans, brand.BrandID,
                                                                       resultPromo.BestValuePlanID, memberID);
            }
            catch (Exception ex)
            {
                throw new BLException("GetMostRecentPromo() error.", ex);
            }

            return resultPromo; 
        }

	    #region IPromoEngineService members
				
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public PromoCollection GetPromos(int brandID)
        {
            PromoCollection promoCollection = null;
            PromoCollection filteredPromoCollection = null;
            SqlDataReader dataReader = null;

            try
            {
                promoCollection = _cache.Get(PromoCollection.GetCacheKey(brandID)) as PromoCollection;

                if (promoCollection != null)
                {
                    return promoCollection;
                }

                if (promoCollection == null)
                {
                    // retrieve from database
                    PromoAttributeCollection attributes = null;
                    attributes = GetPromoAttributes();

                    Command command = new Command("mnSubscription", "dbo.up_Promo_List_All", 0);
                    command.AddParameter("@BrandID", SqlDbType.Int, ParameterDirection.Input, brandID);
                    dataReader = Client.Instance.ExecuteReader(command);

                    // column ordinal variables
                    Int32
                        ordinalPromoID = Constants.NULL_INT,
                        ordinalPaymentTypeID = Constants.NULL_INT,
                        ordinalDescription = Constants.NULL_INT,
                        ordinalStartDate = Constants.NULL_INT,
                        ordinalEndDate = Constants.NULL_INT,
                        ordinalListOrder = Constants.NULL_INT,
                        ordinalDefaultPlanID = Constants.NULL_INT,
                        ordinalBestValuePlanID = Constants.NULL_INT,
                        ordinalPageTitle = Constants.NULL_INT,
                        ordinalLegalNote = Constants.NULL_INT,
                        ordinalBrandID = Constants.NULL_INT,
                        ordinalPlanID = Constants.NULL_INT,
                        ordinalExpressionID = Constants.NULL_INT,
                        ordinalAttributeID = Constants.NULL_INT,
                        ordinalOperationID = Constants.NULL_INT,
                        ordinalValue = Constants.NULL_INT,
                        ordinalColumnOrder = Constants.NULL_INT,
                        ordinalResourceTemplateID = Constants.NULL_INT,
                        ordinalAdminMemberID = Constants.NULL_INT,
                        ordinalPromoApprovalStatus = Constants.NULL_INT,
                        ordinalPromoType = Constants.NULL_INT,
                        ordinalPromoInsertDate = Constants.NULL_INT,
                        ordinalUPSTemplateID = Constants.NULL_INT,
                        ordinalRecurring = Constants.NULL_INT,
                        ordinalRecurrenceActiveUntilDateTime = Constants.NULL_INT,
                        ordinalRecurrenceCronExpression = Constants.NULL_INT,
                        ordinalRecurrenceDurationHours = Constants.NULL_INT,
                        ordinalGiftTypeID = Constants.NULL_INT,
                        ordinalDisableOneClick = Constants.NULL_INT;


                    // loop control vriables
                    Int32 curPromoID = 0, curExpressionID = 0;

                    if (dataReader.HasRows)
                    {
                        // Promo columns ordinal 

                        ordinalPromoID = dataReader.GetOrdinal("PromoID");
                        ordinalPaymentTypeID = dataReader.GetOrdinal("PaymentTypeID");
                        ordinalDescription = dataReader.GetOrdinal("Description");
                        ordinalStartDate = dataReader.GetOrdinal("StartDate");
                        ordinalEndDate = dataReader.GetOrdinal("EndDate");
                        ordinalListOrder = dataReader.GetOrdinal("ListOrder");
                        ordinalDefaultPlanID = dataReader.GetOrdinal("DefaultPlanID");
                        ordinalBestValuePlanID = dataReader.GetOrdinal("BestValuePlanID");
                        ordinalPageTitle = dataReader.GetOrdinal("PageTitle");
                        ordinalLegalNote = dataReader.GetOrdinal("LegalNote");
                        ordinalBrandID = dataReader.GetOrdinal("BrandID");
                        ordinalAdminMemberID = dataReader.GetOrdinal("AdminMemberID");
                        ordinalPromoApprovalStatus = dataReader.GetOrdinal("PromoApprovalStatus");
                        ordinalPromoType = dataReader.GetOrdinal("PromoTypeID");
                        ordinalPromoInsertDate = dataReader.GetOrdinal("InsertDate");
                        ordinalUPSTemplateID = dataReader.GetOrdinal("UPSTemplateID");
                        ordinalRecurring = dataReader.GetOrdinal("Recurring");
                        ordinalRecurrenceActiveUntilDateTime = dataReader.GetOrdinal("RecurrenceActiveUntilDateTime");
                        ordinalRecurrenceCronExpression = dataReader.GetOrdinal("RecurrenceCronExpression");
                        ordinalRecurrenceDurationHours = dataReader.GetOrdinal("RecurrenceDurationHours");

                        // Expression columns ordinals 

                        ordinalExpressionID = dataReader.GetOrdinal("ExpressionID");
                        ordinalAttributeID = dataReader.GetOrdinal("AttributeID");
                        ordinalOperationID = dataReader.GetOrdinal("OperationID");

                        // Expression value column ordinal

                        ordinalValue = dataReader.GetOrdinal("Value");
                        ordinalGiftTypeID = dataReader.GetOrdinal("GiftTypeID");
                        ordinalDisableOneClick = dataReader.GetOrdinal("DisableOneClick");
                    }
                    Promo promo = null;
                    promoCollection = new PromoCollection(brandID);
                    filteredPromoCollection = new PromoCollection(brandID);
                    Expression expression = null;

                    #region Populate PromoCollection
                    
                    while (dataReader.Read())
                    {
                        // create Promo 
                        if (curPromoID != dataReader.GetInt32(ordinalPromoID))
                        {
                            if (promo != null)
                            {
                                if (expression != null)
                                {
                                    promo.Criteria.Add(expression);
                                    expression = null;
                                }
                                // Add promo to promo collection
                                promoCollection.Add(promo);
                            }
                            curPromoID = dataReader.GetInt32(ordinalPromoID);
                            PaymentType paymentType = (PaymentType)dataReader.GetInt32(ordinalPaymentTypeID);

                            promo = new Promo(curPromoID, paymentType, dataReader.GetString(ordinalDescription), dataReader.GetDateTime(ordinalStartDate),
                                dataReader.GetDateTime(ordinalEndDate), dataReader.GetInt32(ordinalListOrder), new ExpressionCollection(), new PromoPlanCollection(brandID),
                                dataReader.GetInt32(ordinalBestValuePlanID), dataReader.GetInt32(ordinalDefaultPlanID), dataReader.GetString(ordinalPageTitle),
                                (PromoApprovalStatus)dataReader.GetInt16(ordinalPromoApprovalStatus), dataReader.GetInt32(ordinalAdminMemberID),
                                (PromoType)dataReader.GetInt16(ordinalPromoType), dataReader.GetDateTime(ordinalPromoInsertDate));

                            if (!dataReader.IsDBNull(ordinalUPSTemplateID))
                            {
                                promo.UPSTemplateID = dataReader.GetInt32(ordinalUPSTemplateID);
                            }

                            promo.Recurring = Convert.ToBoolean(dataReader.GetBoolean(ordinalRecurring));

                            if (!dataReader.IsDBNull(ordinalRecurrenceActiveUntilDateTime))
                            {
                                promo.RecurrenceActiveUntilDateTime = dataReader.GetDateTime(ordinalRecurrenceActiveUntilDateTime);
                            }

                            if (!dataReader.IsDBNull(ordinalRecurrenceCronExpression))
                            {
                                promo.RecurrenceCronExpression = dataReader.GetString(ordinalRecurrenceCronExpression);
                            }

                            if (!dataReader.IsDBNull(ordinalRecurrenceDurationHours))
                            {
                                promo.RecurrenceDurationHours = dataReader.GetInt32(ordinalRecurrenceDurationHours);
                            }

                            if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PROMO_RECURRENCE")))
                            {
                                if ((promo.Recurring && promo.PromoApprovalStatus == PromoApprovalStatus.Approved)
                                    && promo.StartDate.CompareTo(DateTime.Now) < 0 && promo.EndDate.CompareTo(DateTime.Now) > 0)
                                {
//                                    System.Diagnostics.Trace.WriteLine("PromoEngineBL Recurrence JobScheduled. Promo:" + promo.PromoID);
                                    RecurrenceSchedulerBL.Instance.AddNewSchedule(promo, brandID);
                                }
                            }
                            if (!dataReader.IsDBNull(ordinalGiftTypeID))
                            {
                                promo.GiftTypeID = dataReader.GetInt32(ordinalGiftTypeID);
                            }

                            if(!dataReader.IsDBNull(ordinalDisableOneClick))
                            {
                                promo.DisableOneClick = dataReader.GetBoolean(ordinalDisableOneClick);
                            }

                            if (!dataReader.IsDBNull(ordinalLegalNote))
                            {
                                promo.LegalNote = dataReader.GetString(ordinalLegalNote);
                            }
                        }

                        if (dataReader.GetInt32(ordinalExpressionID) == curExpressionID)
                        {
                            expression.Values.Add(dataReader.GetInt32(ordinalValue));
                        } 
                        else
                        {
                            if (expression != null)
                            {
                                promo.Criteria.Add(expression);
                            }
                            // create Expression
                            expression = new Expression(attributes.FindByID(dataReader.GetInt32(ordinalAttributeID)), (OperatorType)Enum.Parse(typeof(OperatorType), dataReader.GetInt16(ordinalOperationID).ToString(), true), new ArrayList());
                            expression.Values.Add(dataReader.GetInt32(ordinalValue));
                        }
                        curExpressionID = dataReader.GetInt32(ordinalExpressionID);
                    }
                    if (promo != null)
                    {
                        promo.Criteria.Add(expression);
                        // Add promo to promo collection
                        promoCollection.Add(promo);
                    }
                    #endregion

                    #region Populate Promo Plans

                    // Populating promos' plans
                    if (dataReader.NextResult())
                    {
                        if (dataReader.HasRows)
                        {
                            ordinalPromoID = dataReader.GetOrdinal("PromoID");
                            // Plan columns ordinals 
                            ordinalPlanID = dataReader.GetOrdinal("PlanID");
                            ordinalListOrder = dataReader.GetOrdinal("ListOrder");
                            ordinalColumnOrder = dataReader.GetOrdinal("ColumnOrder");
                            ordinalResourceTemplateID = dataReader.GetOrdinal("ResourceTemplateID");
                        }
                        int index = -1;
                        curPromoID = 0;
                        int listOrder = Constants.NULL_INT;
                        int columnOrder = Constants.NULL_INT;

                        while (dataReader.Read())
                        {
                            if (curPromoID != dataReader.GetInt32(ordinalPromoID))
                            {
                                index = promoCollection.IndexOf(dataReader.GetInt32(ordinalPromoID));
                                curPromoID = dataReader.GetInt32(ordinalPromoID);
                            }
                            if (index >= 0)
                            {
                                // Create a new Promo Plan based on a Plan to the PromoPlanCollection.
                                ///TODO: add another overloaded method to GetPlan to accept brandID and PlanType
                                ///Make sure when a plan is added to Promo that it's the same payment type as the Promo.
                                Purchase.ValueObjects.Plan plan = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(dataReader.GetInt32(ordinalPlanID), brandID);

                                // There are plans that don't exist in the DB..
                                if (plan != null)
                                {
                                    PromoPlan planToAdd = new PromoPlan(plan);

                                    // Override the row order of these plans to display correctly in the plan group display
                                    // Column order will be currently bound to the site default plan column order so this is already populated
                                    // Row header constant will be currently bound to the site default plan row header so this is already populated
                                    // Column header constant will be currently bound to the site default plan row header so this is already populated
                                    listOrder = dataReader.GetInt32(ordinalListOrder);
                                    // MPR-261 - Do not increment listOrder, retrieve it as is.
                                    //listOrder++;
                                    planToAdd.ListOrder = listOrder;
                                    columnOrder = dataReader.GetInt32(ordinalColumnOrder);
                                    planToAdd.ColumnGroup = columnOrder;
                                    planToAdd.ResourceTemplateID = dataReader.GetInt32(ordinalResourceTemplateID);
                                    // Override this value with the payment type that's defined for the promo.
                                    planToAdd.PlanResourcePaymentTypeID = promo.PaymentType;
                                    promoCollection[index].PromoPlans.Add(planToAdd);
                                }
                                else
                                {
                                    if (promoCollection[index].EndDate > DateTime.Now) //only log active promos
                                    {
                                        //either plan is missing or plan has expired
                                        new Matchnet.Exceptions.ServiceBoundaryException(Matchnet.PromoEngine.ValueObjects.ServiceConstants.SERVICE_NAME, "PromoEngineBL: Missing or expired plan. PromoID=" + curPromoID.ToString() + ", PlanID=" + dataReader.GetInt32(ordinalPlanID).ToString(), (int)System.Diagnostics.EventLogEntryType.Warning);
                                    }
                                }
                                //promoCollection[index].PromoPlans.Add(Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(dataReader.GetInt32(ordinalPlanID),brandID, (PlanType.PromotionalPlan | PlanType.Regular), PaymentType.CreditCard));	
                            }
                        }
                    }

                    #endregion

                    #region Populate Promo Header ResourceTemplates

                    // Get ColumnHeaderResourceTemplateCollection
                    if (dataReader.NextResult())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                Int32 headerResourceTemplateID = Convert.ToInt32(dataReader["PromoHeaderResourceTemplateID"]);
                                Int32 promoID = Convert.ToInt32(dataReader["PromoID"]);
                                Int32 resourceTemplateID = Convert.ToInt32(dataReader["ResourceTemplateID"]);
                                Int16 headerNumber = Convert.ToInt16(dataReader["HeaderNumber"]);
                                Int16 headerType = Convert.ToInt16(dataReader["HeaderType"]);
                                PlanType planType = (PlanType)Convert.ToInt32(dataReader["PlanType"]);
                                PremiumType premiumType = (PremiumType)Convert.ToInt32(dataReader["PremiumType"]);
                                DateTime UpdateDateTime = Convert.ToDateTime(dataReader["UpdateDateTime"]);
                                DateTime InsertDateTime = Convert.ToDateTime(dataReader["InsertDateTime"]);

                                int index = promoCollection.IndexOf(promoID);

                                HeaderResourceTemplateType headerTypeEnum = (HeaderResourceTemplateType)headerType;
                                ColHeaderResourceTemplate colHeaderResourceTemplate =
                                new ColHeaderResourceTemplate(headerResourceTemplateID, promoID, resourceTemplateID, headerNumber, planType, premiumType, UpdateDateTime, InsertDateTime);
                                promoCollection[index].ColHeaderResourceTemplateCollection.Add(colHeaderResourceTemplate);
                            }
                        }

                    }
                    // Get RowHeaderResourceTemplateCollection
                    if (dataReader.NextResult())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                Int32 headerResourceTemplateID = Convert.ToInt32(dataReader["PromoHeaderResourceTemplateID"]);
                                Int32 promoID = Convert.ToInt32(dataReader["PromoID"]);
                                Int32 resourceTemplateID = Convert.ToInt32(dataReader["ResourceTemplateID"]);
                                Int16 headerNumber = Convert.ToInt16(dataReader["HeaderNumber"]);
                                Int16 headerType = Convert.ToInt16(dataReader["HeaderType"]);
                                Int16 duration = Convert.ToInt16(dataReader["Duration"]);
                                DurationType durationType = (DurationType)Convert.ToInt16(dataReader["DurationType"]);
                                DateTime UpdateDateTime = Convert.ToDateTime(dataReader["UpdateDateTime"]);
                                DateTime InsertDateTime = Convert.ToDateTime(dataReader["InsertDateTime"]);

                                int index = promoCollection.IndexOf(promoID);

                                HeaderResourceTemplateType headerTypeEnum = (HeaderResourceTemplateType)headerType;
                                RowHeaderResourceTemplate rowHeaderResourceTemplate =
                                    new RowHeaderResourceTemplate(headerResourceTemplateID, promoID, resourceTemplateID, headerNumber, duration, durationType, UpdateDateTime, InsertDateTime);

                                promoCollection[index].RowHeaderResourceTemplateCollection.Add(rowHeaderResourceTemplate);
                            }
                        }
                    }
                    #endregion

                    // Filter out any promos with missing resource collections (Old records)
                    foreach (Promo p in promoCollection)
                    {
                        if (p.ColHeaderResourceTemplateCollection.Count == 0 ||
                            p.RowHeaderResourceTemplateCollection.Count == 0)
                        {

                        }
                        else
                        {
                            filteredPromoCollection.Add(p);
                        }
                    }

                    // Add to cache
                    if (filteredPromoCollection.Count > 0)
                    {
                        _cache.Add(filteredPromoCollection);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new BLException("GetPromos() error. (BrandID: " + brandID.ToString() + ")", ex);
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }

            return filteredPromoCollection;
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public PromoCollection GetActivePromos(int brandID)
		{
			PromoCollection result= new PromoCollection(brandID);
			try
			{
				PromoCollection promos=GetPromos(brandID);
				foreach(Promo promo in promos)
				{
					// Check if the plan is not expired
					if (promo.StartDate.CompareTo(DateTime.Now)<0 && promo.EndDate.CompareTo(DateTime.Now)>0)
					{
						result.Add(promo);
					}
				}
			} 
			catch(Exception ex)
			{
				throw new BLException("GetActivePromos() error. (BrandID: " + brandID.ToString() + ")", ex);
			}

			return result;
		}
		/// <summary>
		/// For a given brand, admin only promo is returned.  If there are more than 1 active then the one with later InsertDate wins.
		/// </summary>
		/// <param name="brand"></param>
		/// <param name="paymentType"></param>
		/// <returns></returns>
		public Promo GetAdminPromo(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, PaymentType paymentType)
		{
		    return GetMostRecentPromo(memberID, brand, PromoType.AdminOnly, paymentType);
		}
        /// <summary>
        /// Returns a supervisor only promo
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="brand"></param>
        /// <param name="paymentType"></param>
        /// <returns></returns>
        public Promo GetSupervisorPromo(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, PaymentType paymentType)
        {
            return GetMostRecentPromo(memberID, brand, PromoType.SupervisorOnly, paymentType);
        }
		/// <summary>
		/// Checks to see if a member qualifies for a specific promo.
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="brand"></param>
		/// <param name="promo"></param>
		/// <returns></returns>
		public bool QualifyPromo(int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, Promo promo)
		{
			bool qualify = false;

			if (brand == null)
			{
				throw new System.ArgumentException("QualifyPromo() invalid (null) parameter.\nbrand parameter is null."); 
			}
			if (promo == null)
			{
				throw new System.ArgumentException("QualifyPromo() invalid (null) parameter.\npromo parameter is null."); 
			}

			try
			{
				Member.ServiceAdapters.Member member = Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID,Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
				if (member ==null)
				{
					throw new System.ArgumentException("QualifyPromo() could not find member.(MemberId="+ memberID.ToString()+")"); 
				}

                MemberAttributeMapper mam = new MemberAttributeMapper();
                mam.refreshedSubStatus = false;
				qualify = ExpressionCalculator.CriteriaMatch(member, promo.Criteria, brand, mam, promo.PromoID);
			}
			catch (Exception ex)
			{
				throw new BLException("QualifyPromo() error.",ex); 
			}

			return qualify;
		}

	    /// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="brand"></param>
		/// <param name="paymentType"></param>
		/// <returns></returns>
		public Promo GetMemberPromo(int memberID,Matchnet.Content.ValueObjects.BrandConfig.Brand brand, PaymentType paymentType, PromoType promoType)
		{
			Promo  resultPromo=null;
			
			// firing the match request event 
			MatchRequested();

			if ( brand==null)
			{
				throw new System.ArgumentException("GetMemberPromo() invalid (null) parameter.\nbrand parameter is null."); 
			}
			try
			{
				Member.ServiceAdapters.Member member = Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID,Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
				if (member ==null)
				{
					throw new System.ArgumentException("GetMemberPromo() could not find member.(MemberId="+ memberID.ToString()+")"); 
				}

				PromoCollection activePromos= GetActivePromos(brand.BrandID);

				PromoCollection promoCol = new PromoCollection(brand.BrandID);

				// Only return active and approved promos.  With addition of PromoType, only return "member" promo type
				foreach(Promo promo in activePromos)
				{
                    if (promo.PromoApprovalStatus == PromoApprovalStatus.Approved && promo.PromoType == promoType)
                    {
                        if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PROMO_RECURRENCE")))
                        {
                            if ((promo.Recurring == false) ||
                                (promo.Recurring && promo.RecurrenceActiveUntilDateTime.CompareTo(DateTime.Now) > 0))
                            {
                                promoCol.Add(promo);
                            }
                        }
                        else
                            promoCol.Add(promo);
                    }
				}

				if (promoCol!=null)
				{
                    MemberAttributeMapper mam = new MemberAttributeMapper();
                    mam.refreshedSubStatus = false;
					foreach (Promo p in promoCol)
					{
						if(ExpressionCalculator.CriteriaMatch(member,p.Criteria,brand, mam, p.PromoID) && p.PaymentType == paymentType)
						{
							// Make a deep copy of the cached promo to avoid referencing the cached promo
							resultPromo = (Promo) p.Clone();
							//resultPromo=p;
							break;
						}
					}
				}
				if (resultPromo!=null)
				{
					// firing the match found event 
					MatchFound();

					// Now using the copy of the cached promo instead 
					// Otherwise, member specific data such as credit amount and purchase mode will be cached and could
					// possibly be altered after the cached promo is modified correctly for a member but before the cached
					// promo is used by an application 
					// In this case, the member may have a credit amount and purchase mode set by another member  
					
					resultPromo.PromoPlans = SetupPlanCollectionForDisplay(resultPromo.PromoPlans, brand.BrandID, resultPromo.BestValuePlanID, memberID);
				}
			}
			catch (Exception ex)
			{
				throw new BLException("GetMemberPromo() error.",ex); 
			}
			return resultPromo; 
		}

		#region Ported Methods from Purchase for Subscription Admin Project

		/// <summary>
		/// Ported from PurchaseSM
		/// </summary>
		/// <param name="sourcePlans"></param>
		/// <param name="brandID"></param>
		/// <param name="bestValuePlanID"></param>
		/// <param name="memberID"></param>
		/// <returns></returns>
		private PromoPlanCollection SetupPlanCollectionForDisplay(PromoPlanCollection sourcePlans, 
			Int32 brandID, 
			Int32 bestValuePlanID,
			Int32 memberID)
		{
			try
			{
				ResetBestValuePlanInPlanCollection(sourcePlans, bestValuePlanID);
				return PreparePlanCollectionForDisplay(sourcePlans, brandID, false, memberID, false, false);
			}
			catch(Exception ex)
			{
				throw new BLException("Error in SetupPlanCollectionForDisplay. " + ex.ToString(), ex);
			}
		}	
	
		/// <summary>
		/// Check if the member is allowed to purchase the selected plan given his remaining credit amount     
		/// </summary>
		/// <param name="plan">Plan being checked</param>
		/// <param name="remainingCreditAmount">Credit amount for the member being checked</param>
		/// <returns name="bool">Returns true if the remaining credit amount does not exceed the purchase rules for a particular payment type or false if the remaining credit amount does exceed the purchase rules for a particular payment type</returns>
		public bool IsPurchaseAllowedWithRemainingCredit(Plan plan, decimal remainingCreditAmount)
		{
			bool isPurchaseAllowedWithRemainingCredit = false;

			// Member is upgrading his plan so allow remaining credit to be applied  
			if (plan.PlanResourcePaymentTypeID == PaymentType.CreditCard)
			{
				// For credit card payment allow the member to upgrade to this plan only
				// if the cost of the plan is greater than the remaining credit
				isPurchaseAllowedWithRemainingCredit = (plan.InitialCost > remainingCreditAmount);
			}
			else if (plan.PlanResourcePaymentTypeID == PaymentType.Check)
			{
				// For check payment allow the member to upgrade to this plan only
				// if the net cost of the plan after applying the remaining credit 
				// is greater than $100.00
				isPurchaseAllowedWithRemainingCredit = ((plan.InitialCost - remainingCreditAmount) > 100.00m);
			}

			return isPurchaseAllowedWithRemainingCredit;
		}

		/// <summary>
		/// Ported from PurchaseBL
		/// </summary>
		/// <param name="sourcePlans"></param>
		/// <param name="planID"></param>
		private void ResetBestValuePlanInPlanCollection(PromoPlanCollection sourcePlans, Int32 planID)
		{
			// Allows overriding the best value plan returned from promo engine
			// Without this, the best value plan would be set when getting the
			// site default plans.  
			foreach (PromoPlan plan in sourcePlans)
			{
				if (plan != null)
				{
					if (plan.PlanID == planID)
					{
						plan.BestValueFlag = true;
					}
					else
					{
						plan.BestValueFlag = false;
					}
				}
			}
		}

		/// <summary>
		/// Ported from PurchaseBL
		/// Sets up the collection of plans for display
		/// </summary>
		/// <param name="sourcePlans">Original plan collection</param>
		/// <param name="brandID">Current Brand ID</param>
		/// <param name="showSiteDefaultPlansOnly">Show only the site default plans only</param>
		/// <param name="memberID">Current Member ID</param>
		/// <param name="allowAllColumnOrders">Show plans with any column order</param>
		/// <param name="clonePlanCollection">Requires cloning the original plan collection</param>
		/// <returns name="PlanDisplayValidation">Returns the plan collection for display</returns>
		public PromoPlanCollection PreparePlanCollectionForDisplay(PromoPlanCollection sourcePlans, Int32 brandID, bool showSiteDefaultPlansOnly, Int32 memberID, bool allowAllColumnOrders, bool clonePlanCollection)
		{
			/*
				Does 2 things
				Filters out all plans that will not be displayed to the customers
				Any plan with ColumnOrder >= 99 will be removed
				If showSiteDefaultPlansOnly is true, then only show plans with a plan type
				of site default.  However, if showSiteDefaultPlansOnly is false, then possibly 
				include all plan types.    			
			*/			

			if (clonePlanCollection)
			{
				sourcePlans = (PromoPlanCollection) sourcePlans.Clone();
			}

			PromoPlanCollection arrangedPlans = null;
			decimal remainingCredit = Constants.NULL_DECIMAL;
			bool checkedRemainingCredit = false;
			Matchnet.Content.ValueObjects.BrandConfig.Brand brand = null;

			if (sourcePlans != null)
			{
				foreach (PromoPlan plan in sourcePlans)
				{
					if (((plan.ColumnGroup < 99 && !allowAllColumnOrders) 
						|| (allowAllColumnOrders))  
						&& ((showSiteDefaultPlansOnly && ((plan.PlanTypeMask & PlanType.SiteDefault) == PlanType.SiteDefault))
						|| (!showSiteDefaultPlansOnly)))
					{
						if (arrangedPlans == null)
						{
							arrangedPlans = new PromoPlanCollection(brandID);
						}

						if (!checkedRemainingCredit)
						{
							brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);

							// Calculate the remaining credit for a member just once
							//remainingCredit = (Purchase.ServiceAdapters.PurchaseSA.Instance.GetRemainingCredit(memberID, brand)).TotalCreditedAmount;
                            Spark.Common.Adapter.OrderHistoryServiceAdapter orderHistoryServiceAdapter = new Spark.Common.Adapter.OrderHistoryServiceAdapter();
                            try
                            {
                                remainingCredit = orderHistoryServiceAdapter.GetProxyInstanceForBedrock().GetRemainingCredit(memberID, brand.Site.SiteID);
                            }
                            finally
                            {
                                orderHistoryServiceAdapter.CloseProxyInstance();
                            }
                            checkedRemainingCredit = true;
						}

						// Set the remaining credit for the plan
						plan.CreditAmount = remainingCredit;

						// Set the purchase mode for the plan
						Plan planForPurchaseMode = Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(plan.PlanID, brandID);
						plan.PurchaseMode = Purchase.ServiceAdapters.PurchaseSA.Instance.GetPurchaseMode(memberID, planForPurchaseMode, brand); 

						if (Plan.IsPurchaseModeValidForCredit(plan.PurchaseMode))
						{
							plan.EnabledForMember = this.IsPurchaseAllowedWithRemainingCredit(plan, remainingCredit);
						}
						else
						{
							// Member is not upgrading his plan so no remaining credit will be applied
							plan.EnabledForMember = true;
						}
									
						arrangedPlans.Add(plan);
					}		
				}
			}

			return arrangedPlans;
		}


		#endregion

		/// <summary>
		/// Returns an active promo with matching PromoID  
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public Promo GetPromoByID(int promoID,int brandID)
		{
			Promo result=null;
			try
			{
				PromoCollection promos=GetActivePromos(brandID);
				if (promos!=null)
				{
					result=promos.FindByID(promoID);
				}
			} 
			catch(Exception ex)
			{
				throw new BLException("GetPromoByID() error. (PromoID: " + promoID.ToString() + "BrandID=" + brandID.ToString() + ")", ex);
			}

			return result;
		}
		
		/// <summary>
		/// Returns a promo by ID.  Have the option to search through active ones only.
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="brandID"></param>
		/// <param name="activeOnly"></param>
		/// <returns></returns>
		public Promo GetPromoByID(int promoID, int brandID, bool activeOnly)
		{
			Promo result = null;
			try
			{
				PromoCollection promos = null;
				
				if(activeOnly)
					promos = GetActivePromos(brandID);
				else
					promos = GetPromos(brandID);

				if(promos != null)
					result = promos.FindByID(promoID);
			}
			catch(Exception ex)
			{
				throw new BLException("GetPromoByID() error. (PromoID: " + promoID.ToString() + "BrandID=" + brandID.ToString() + ")", ex);
			}
			
			return result;
		}

		/// <summary>
		/// Returns an active promo with matching PromoID  
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="brandID"></param>
		/// <param name="memberID"></param>
		/// <returns></returns>
		public Promo GetPromoByID(int promoID, int brandID, int memberID, bool activeOnly)
		{
			Promo result=null;
			try
			{
				PromoCollection promos;
				
				if (activeOnly)
				{
					promos = GetActivePromos(brandID);
				}
				else
				{
					promos = GetPromos(brandID);
				}
				
				if (promos!=null)
				{
					Promo findPromo = promos.FindByID(promoID);

					if(findPromo != null)
					{
						result = (Promo)findPromo.Clone();
						//result = promos.FindByID(promoID);
						result.PromoPlans = SetupPlanCollectionForDisplay(result.PromoPlans, brandID, result.BestValuePlanID, memberID);
					}
				}
			} 
			catch(Exception ex)
			{
				throw new BLException("GetPromoByID() error. (PromoID: " + promoID.ToString() + "BrandID=" + brandID.ToString() + ")", ex);
			}

			return result;
		}

		/// <summary>
		/// Returns an active promo with matching PromoID  
		/// </summary>
		/// <param name="promoID"></param>
		/// <param name="brandID"></param>
		/// <param name="memberID"></param>
		/// <returns></returns>
		public Promo GetPromoByID(int promoID, int brandID, int memberID)
		{
			Promo result=null;
			try
			{
				PromoCollection promos=GetActivePromos(brandID);
				if (promos!=null)
				{
					Promo findPromo = promos.FindByID(promoID);

					if(findPromo != null)
					{
						result = (Promo)findPromo.Clone();
						//result = promos.FindByID(promoID);
						result.PromoPlans = SetupPlanCollectionForDisplay(result.PromoPlans, brandID, result.BestValuePlanID, memberID);
					}
				}
			} 
			catch(Exception ex)
			{
				throw new BLException("GetPromoByID() error. (PromoID: " + promoID.ToString() + "BrandID=" + brandID.ToString() + ")", ex);
			}

			return result;
		}

		/// <summary>
		/// This method takes in substring of the Promo's description and returns all promos that match that.
		/// </summary>
		/// <param name="description"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public PromoCollection GetPromosByDescription(string description, int brandID)
		{
			PromoCollection promoCol = null;
			SqlDataReader dataReader = null;

			try
			{
				// Get the Promo information only from the DB (Data only from the Promo table).
				// Not like our up_Promo_List_All, which gets back everything related to promos.
				Command command = new Command("mnSubscription", "dbo.up_Promo_Search",0);
				command.AddParameter("@BrandID", SqlDbType.Int, ParameterDirection.Input, brandID);
				command.AddParameter("@SearchString", SqlDbType.NVarChar, ParameterDirection.Input, description);
				dataReader = Client.Instance.ExecuteReader(command);

				// column ordinal variables
				Int32
					ordinalPromoID = Constants.NULL_INT;

				if (dataReader.HasRows)
				{
					// Promo columns ordinal
					ordinalPromoID = dataReader.GetOrdinal("PromoID");
				}

				// We just need to collect all the PromoIDs, then grab the fully populated Promos by calling
				// GetPromoByID
				ArrayList promoIDs = new ArrayList();
				while(dataReader.Read())
				{
					promoIDs.Add(dataReader.GetInt32(ordinalPromoID));
				}

				// Start populating the actual Promos here
				promoCol = new PromoCollection(brandID);
				Promo promoToAdd = null;
				foreach(int promoID in promoIDs)
				{
					// Since the store proc only searches the description column, we can get back an ID that is considered "old"
					// which means promos returned from GetPromos() won't include that promo which will make GetPromoByID come
					// back with nothing.  Check for this case before adding it here.
					promoToAdd = null;
					promoToAdd = GetPromoByID(promoID, brandID, false);
					if(promoToAdd != null)
                        promoCol.Add(promoToAdd);
				}
			}
			catch(Exception ex)
			{
				throw new BLException("GetPromosByDescription() error. (Description: " + description + "BrandID=" + brandID.ToString() + ")", ex);
			}
			finally
			{
				if(dataReader != null)
					dataReader.Close();
			}
			
			return promoCol;
		}

		/// <summary>
		/// This is to prevent overwriting of a valid state that cannot be overwritten.
		/// For example, if promoapproval is set to approved or rejected do not set it to pending.
		/// </summary>
		/// <param name="promos"></param>
		/// <param name="brandID"></param>
		private PromoCollection PreventImmutableValueOverWrite(PromoCollection promos, int brandID)
		{
			PromoCollection filteredPromos = new PromoCollection(brandID);

			foreach (Promo promo in promos)
			{
				Promo p = GetPromoByID(promo.PromoID, brandID, false);

				if (p.PromoApprovalStatus != PromoApprovalStatus.Pending)
				{
					promo.PromoApprovalStatus = p.PromoApprovalStatus;
				}

				filteredPromos.Add(promo);
			}

			return filteredPromos;
		}

		/// <summary>
		/// Updates the promos in the PromoCollection object.  Only these fields are updated; EndDate, Description, StartDate, ListOrder,
		/// PageTitle.
		/// </summary>
		/// <param name="promos"></param>
		/// <param name="brandID"></param>
		public void SavePromos(PromoCollection promos, int brandID)
		{
			TransactionalWriter tw= new TransactionalWriter();
			try
			{
				promos = PreventImmutableValueOverWrite(promos, brandID);

				// if succeeded then invalidate cached collection
				if (tw.PersistPromos(promos,brandID))
				{	

					string cacheKey=PromoCollection.GetCacheKey(brandID);
					// Expire cache on the current instance
					_cache.Remove(cacheKey);
					// replicate the cache to other instances cache
					if (cacheKey.Length>0)
					{
						ExpirePromoCollection actionItem= new ExpirePromoCollection(cacheKey);
						ReplicationRequested(actionItem);
					}
				}
			}
			catch (Exception ex)
			{
				throw new BLException(string.Format("SavePromos() error. BrandID:{0})" ,brandID.ToString() ), ex);
			}
		}

		/// <summary>
		/// Persists a promo object into database
		/// </summary>
		/// <param name="promo"></param>
		/// <param name="brandID"></param>
		public int SavePromo(Promo promo, int brandID)
		{
			TransactionalWriter tw= new TransactionalWriter();
			int promoID = Constants.NULL_INT;
			try
			{
				int outPromoID;
				// if succeeded then invalidate cached collection
				if (tw.PersistPromo(promo,brandID, out outPromoID))
				{	
					string cacheKey=PromoCollection.GetCacheKey(brandID);
					// Expire cache on the current instance
					_cache.Remove(cacheKey);
					// replicate the cache to other instances cache
					if (cacheKey.Length>0)
					{
                        try
                        {
                            ExpirePromoCollection actionItem = new ExpirePromoCollection(cacheKey);
                            ReplicationRequested(actionItem);
                        }
                        catch (Exception ex)
                        {
                            new BLException("SavePromo() Error triggering replication");
                        }
					}

					// outPromoID is only meaningful if the insert/update was a success
					promoID = outPromoID;
				}
			}
			catch (Exception ex)
			{
				throw new BLException(string.Format("SavePromo() error. BrandID:{0})" ,brandID.ToString() ), ex);
			}

			return promoID;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public PromoAttributeCollection GetPromoAttributes()
		{

			PromoAttributeCollection  attributes =null;
			SqlDataReader dataReader = null;
			try
			{
				attributes = _cache.Get(PromoAttributeCollection.ATTRIBUTES_CACHE_KEY) as PromoAttributeCollection;
			
				if (attributes  == null)
				{
					// retrieve from database

					Int32
							ordinalAttributeID = Constants.NULL_INT,
							ordinalDataType = Constants.NULL_INT,
							ordinalAttributeTypeID= Constants.NULL_INT;
					
					Command command = new Command("mnSubscription", "dbo.up_PromoAttribute_List_All",0);					
					dataReader = Client.Instance.ExecuteReader(command);
					if (dataReader.HasRows)
					{
						// PromoAttributes columns ordinal 

						ordinalAttributeID = dataReader.GetOrdinal("AttributeID");
						ordinalDataType = dataReader.GetOrdinal("DataType");
						ordinalAttributeTypeID= dataReader.GetOrdinal("AttributeTypeID");
						
						attributes = new PromoAttributeCollection();
					}
					while(dataReader.Read())
					{
						attributes.Add( new PromoAttribute(dataReader.GetInt32(ordinalAttributeID),(DataType)Enum.Parse(typeof(DataType),  dataReader.GetString(ordinalDataType), true),dataReader.GetInt16(ordinalAttributeTypeID) ));
					}
					// Add to cache
					_cache.Add( attributes);
				}
			}
			catch (Exception ex)
			{
				throw new BLException("GetPromoAttributes() error. ", ex);
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
			return attributes;
		}
		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="replicationAction"></param>
		public void PlayReplicationAction(IReplicationAction replicationAction)
		{
			switch (replicationAction.GetType().Name)
			{
				case "ExpirePromoCollection":
					 
					ExpirePromoCollection expirePromoCollectionAction = replicationAction as ExpirePromoCollection;
					_cache.Remove(expirePromoCollectionAction.CacheKey);
					break;
			}
		}	
	}
}
