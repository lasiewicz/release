using System;
using System.Data.SqlClient;
using System.Web.Caching;
using System.Collections;
using System.Data;

using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.PromoEngine.BusinessLogic
{
	/// <summary>
	/// Summary description for ResourceTemplateBL.
	/// </summary>
	public class ResourceTemplateBL
	{
		/// <summary>
		/// 
		/// </summary>
		public readonly static ResourceTemplateBL Instance = new ResourceTemplateBL();

		private Matchnet.Caching.Cache _cache;

		private CacheItemRemovedCallback expireCallback;
		/// <summary>
		/// 
		/// </summary>
		public delegate void ReplicationEventHandler(IReplicable replicableObject);
		/// <summary>
		/// 
		/// </summary>
		public event ReplicationEventHandler ReplicationRequested;
		/// <summary>
		/// 
		/// </summary>
		public delegate void SynchronizationEventHandler(string key, Hashtable cacheReferences);
		/// <summary>
		/// 
		/// </summary>
		public event SynchronizationEventHandler SynchronizationRequested;

		private ResourceTemplateBL()
		{
			System.Diagnostics.Trace.WriteLine("ResourceTemplateBL constructor.");
			_cache = Matchnet.Caching.Cache.Instance;
			expireCallback = new CacheItemRemovedCallback(this.ExpireCallback);
		}

		private void ExpireCallback(string key, object value, CacheItemRemovedReason callbackreason)
		{
			switch (value.GetType().Name)
			{
				case "ResourceTemplateCollection":
					ResourceTemplateCollection resourceTemplateCollection = value as ResourceTemplateCollection;
					  SynchronizationRequested(key, resourceTemplateCollection.ReferenceTracker.Purge(Constants.NULL_STRING));
					break;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="replicableObject"></param>
		public void CacheReplicatedObject(IReplicable replicableObject)
		{
			switch (replicableObject.GetType().Name)
			{
				case "ResourceTemplateCollection":
					CacheResourceTemplateCollection((ResourceTemplateCollection)replicableObject);
					
					break;

				default:
					throw new Exception("invalid replication object type received (" + replicableObject.GetType().ToString() + ")");
			}
		}

		private void CacheResourceTemplateCollection(ResourceTemplateCollection resourceTemplateCollection) 
		{
			_cache.Insert(resourceTemplateCollection, expireCallback);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <returns></returns>
		public ResourceTemplateCollection GetResourceTemplateCollection(string clientHostName, CacheReference cacheReference)
		{
			ResourceTemplateCollection  resourceTemplateCollection = null;
			SqlDataReader dataReader = null;

			try
			{
				resourceTemplateCollection = _cache.Get(ResourceTemplateCollection.RESOURCE_TEMPLATE_COLLECTION_CACHE_KEY) as ResourceTemplateCollection;
			
				if (resourceTemplateCollection  == null)
				{
					resourceTemplateCollection = new ResourceTemplateCollection();

					Command command = new Command("mnSubscription", "dbo.up_ResourceTemplate_List",0);
					dataReader = Client.Instance.ExecuteReader(command);

					if (dataReader.HasRows)
					{
						while (dataReader.Read())
						{
							ResourceTemplate resourceTemplate = new ResourceTemplate(
								Convert.ToInt32(dataReader["ResourceTemplateID"]),
								Convert.ToString(dataReader["Content"]),
								Convert.ToString(dataReader["Description"]),
								Convert.ToInt32(dataReader["GroupID"]),
								Convert.ToBoolean(dataReader["GroupDefault"]),
								(ResourceTemplateType)Convert.ToInt32(dataReader["ResourceTemplateTypeID"]),
								Convert.ToDateTime(dataReader["InsertDateTime"]),
								Convert.ToDateTime(dataReader["UpdateDateTime"]));

							resourceTemplateCollection.Add(resourceTemplate);
						}
					}

					CacheResourceTemplateCollection(resourceTemplateCollection);
					ReplicationRequested(resourceTemplateCollection);
					SynchronizationRequested(ResourceTemplateCollection.RESOURCE_TEMPLATE_COLLECTION_CACHE_KEY, resourceTemplateCollection.ReferenceTracker.Purge(Constants.NULL_STRING));
				}

				if (resourceTemplateCollection == null || resourceTemplateCollection.Count == 0)
				{
					throw new BLException("Resource template collection cannot be empty.");
				}

				if (clientHostName != null && cacheReference != null)
				{
					resourceTemplateCollection.ReferenceTracker.Add(clientHostName, cacheReference);
					System.Diagnostics.Trace.WriteLine("Promo: Added a reference to resourcetemplatecollection. " + clientHostName);
				}

				return resourceTemplateCollection;
			}
			catch(Exception ex)
			{
				throw new BLException("Error populating resource template collection from the database. " + ex.ToString(), ex);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="resourceTemplate"></param>
		public ResourceTemplate SaveResourceTemplate(ResourceTemplate resourceTemplate)
		{
			try
			{
				// New Resource Template
				if (resourceTemplate.ResourceTemplateID == Constants.NULL_INT || resourceTemplate.ResourceTemplateID == 0)
				{
					resourceTemplate.ResourceTemplateID = KeySA.Instance.GetKey("ResourceTemplateID");
				}

				Command command = new Command("mnSubscription", "up_ResourceTemplate_Save", 0);

				command.AddParameter("@ResourceTemplateID", SqlDbType.Int, ParameterDirection.Input, resourceTemplate.ResourceTemplateID);
				command.AddParameter("@Content", SqlDbType.NVarChar, ParameterDirection.Input, resourceTemplate.Content);
				command.AddParameter("@Description", SqlDbType.NVarChar, ParameterDirection.Input, resourceTemplate.Description);
				if (resourceTemplate.GroupID != Constants.NULL_INT)
					command.AddParameter("@GroupID", SqlDbType.NVarChar, ParameterDirection.Input, resourceTemplate.GroupID);
				command.AddParameter("@GroupDefault", SqlDbType.Bit, ParameterDirection.Input, resourceTemplate.GroupDefault);
				command.AddParameter("@ResourceTemplateTypeID", SqlDbType.Int, ParameterDirection.Input, (int)resourceTemplate.ResourceTemplateType);

				Exception ex;
				SyncWriter sw = new SyncWriter();
				sw.Execute(command, null, out ex);
				sw.Dispose();
				if (ex != null)
				{
					throw ex;
				}

				_cache.Remove(ResourceTemplateCollection.RESOURCE_TEMPLATE_COLLECTION_CACHE_KEY);

				ResourceTemplateCollection resourceTemplateCollection = GetResourceTemplateCollection(null, null);

				return resourceTemplate;
			}
			catch(Exception ex)
			{
				throw new BLException("Error saving resource template." + ex.ToString(), ex);
			}
		}
	}
}
