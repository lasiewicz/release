using System;
#region Matchnet references
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.Exceptions;
using Spark.Logging;
using System.Text;
#endregion
namespace Matchnet.PromoEngine.BusinessLogic
{

	/// <summary>
	/// This class contains functionalities to evaluate PromoEngine expressions
	/// </summary>
	public class ExpressionCalculator
	{
        private const string CLASS_NAME = "ExpressionCalculator";
		/// <summary>
		/// Returns a true/false result indicating if the passed member macthed the promo criteria or not
		/// </summary>
		/// <param name="member"></param>
		/// <param name="criteria"></param>
		/// <param name="brand"></param>
		/// <returns></returns>
		public static bool  CriteriaMatch(Matchnet.Member.ServiceAdapters.Member  member, ExpressionCollection criteria, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, MemberAttributeMapper mam, int promoID)
		{
			bool  result =true;

            int criteriaCount = 0;
            if (criteria != null)
            {
                criteriaCount = criteria.Count;
            }

            RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, "Start CriteriaMatch() - promoID: " + promoID.ToString() + ", criterias: " + criteriaCount.ToString() + ", member: " + member.MemberID.ToString() + ", brand: " + brand.BrandID.ToString(),
                null);
			try
			{
				foreach (Expression exp in criteria)
				{
					if (!EvalExpression(member,exp,brand, mam, promoID))
					{
						result=false;
						break;
					}
				}

                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, "End CriteriaMatch() - " + result.ToString().ToUpper() + ", promoID: " + promoID.ToString() + ", member: " + member.MemberID.ToString() + ", brand: " + brand.BrandID.ToString(),
                        null);
			}
			catch(Exception ex)
			{
				throw new BLException("ExpressionCalculator.CriteriaMatch() error." , ex);
			}
			return result;
		}
		/// <summary>
		/// Evaluates a single expression against attributes of the passed member object. Returns true/false depending on the match result. 
		/// </summary>
		/// <param name="member"></param>
		/// <param name="expression"></param>
		/// <param name="brand"></param>
		/// <returns></returns>
		private static bool EvalExpression(Matchnet.Member.ServiceAdapters.Member member, Expression expression,Matchnet.Content.ValueObjects.BrandConfig.Brand brand, MemberAttributeMapper mam, int promoID)
		{            
			bool expResult=false;
            StringBuilder sbExpressionValues = new StringBuilder();
            string memberValue = "";
			if (expression.Values!=null)
			{
				switch (expression.Operator)
				{
					case OperatorType.Equal : //  Equal
					switch(expression.Attribute.DataType)
					{
						case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Mask :
						case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Bit :
							Int32 intMemberMaskValue=MemberAttributeMapper.GetMemberAttributeValueInt(member,expression.Attribute,brand, mam);
                            memberValue = intMemberMaskValue.ToString();
                            for( int i=0; i< expression.Values.Count;i++)
							{
                                sbExpressionValues.Append(expression.Values[i].ToString() + ";");
								expResult= expResult || ((intMemberMaskValue & Int32.Parse( expression.Values[i].ToString()))>0 ? true: false);
                                if (expResult)
                                {
                                    break;
                                }
							}
							break;
						case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Number  :
							Int32 intMemberValue=MemberAttributeMapper.GetMemberAttributeValueInt(member,expression.Attribute,brand, mam);
                            memberValue = intMemberValue.ToString();
                            for( int i=0; i< expression.Values.Count;i++)
							{
                                sbExpressionValues.Append(expression.Values[i].ToString() + ";");
								expResult= expResult || (intMemberValue==Int32.Parse( expression.Values[i].ToString()));
                                if (expResult)
                                {
                                    break;
                                }
							}
							break;
							/*case  Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Date:
									member.GetAttributeDate(brand,attribute.ID);		
									break;     
								
								*/
					}
						break;

					case OperatorType.GreaterThan : // >
					switch(expression.Attribute.DataType)
					{
						case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Number  :
							Int32 intMemberValue=MemberAttributeMapper.GetMemberAttributeValueInt(member,expression.Attribute,brand, mam);
                            memberValue = intMemberValue.ToString();
                            for( int i=0; i< expression.Values.Count;i++)
							{
                                sbExpressionValues.Append(expression.Values[i].ToString() + ";");
								expResult= expResult || (intMemberValue > Int32.Parse( expression.Values[i].ToString()));
                                if (expResult)
                                {
                                    break;
                                }
							}
							break;
							/*case  Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Date:
											member.GetAttributeDate(brand,attribute.ID);		
											break;     
										
										*/
					}
						break;
					case OperatorType.GreaterThanEqual: //>=
					switch(expression.Attribute.DataType)
					{
						case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Number  :
							Int32 intMemberValue=MemberAttributeMapper.GetMemberAttributeValueInt(member,expression.Attribute,brand, mam);
                            memberValue = intMemberValue.ToString();
                            for( int i=0; i< expression.Values.Count;i++)
							{
                                sbExpressionValues.Append(expression.Values[i].ToString() + ";");
								expResult= expResult || (intMemberValue >= Int32.Parse( expression.Values[i].ToString()));
                                if (expResult)
                                {
                                    break;
                                }
							}
							break;
							/*case  Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Date:
											member.GetAttributeDate(brand,attribute.ID);		
											break;     
										
										*/
					}
						break;
					case  OperatorType.LessThan : //<
					switch(expression.Attribute.DataType)
					{
						case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Number  :
							Int32 intMemberValue=MemberAttributeMapper.GetMemberAttributeValueInt(member,expression.Attribute,brand, mam);
                            memberValue = intMemberValue.ToString();
                            for( int i=0; i< expression.Values.Count;i++)
							{
                                sbExpressionValues.Append(expression.Values[i].ToString() + ";");
								expResult= expResult || (intMemberValue < Int32.Parse( expression.Values[i].ToString()));
                                if (expResult)
                                {
                                    break;
                                }
							}
							break;
							/*case  Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Date:
											member.GetAttributeDate(brand,attribute.ID);		
											break;     
										
										*/
					}
						break;
					case OperatorType.LessThanEqual : //<=
					switch(expression.Attribute.DataType)
					{
						case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Number  :
							Int32 intMemberValue=MemberAttributeMapper.GetMemberAttributeValueInt(member,expression.Attribute,brand, mam);
                            memberValue = intMemberValue.ToString();
                            for( int i=0; i< expression.Values.Count;i++)
							{
                                sbExpressionValues.Append(expression.Values[i].ToString() + ";");
								expResult= expResult || (intMemberValue <= Int32.Parse( expression.Values[i].ToString()));
                                if (expResult)
                                {
                                    break;
                                }
							}
							break;
							/*case  Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Date:
											member.GetAttributeDate(brand,attribute.ID);		
											break;     
										
										*/
					}
						break;
				}

                if (!expResult)
                {
                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, "EvalExpression() - FAILED " + expression.Operator.ToString() + ", promoID: " + promoID.ToString() + ", promo attribute: " + expression.Attribute.ID.ToString() + ", promo criteria value: " + sbExpressionValues.ToString() + ", member value: " + memberValue.ToString() + ", member " + member.MemberID.ToString() + ", brand " + brand.BrandID.ToString(),
                        null);
                }
			}
			return  expResult;
		}

	}
}
