using System;

#region Matchnet references

using Matchnet.PromoEngine.ValueObjects;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Purchase.ServiceAdapters;
using System.IO;
using Spark.Logging;
using Matchnet.Member.ServiceAdapters;

#endregion
namespace Matchnet.PromoEngine.BusinessLogic
{	
	/// <summary>
	/// Eum representation of PromoEngine calculated attributes 
	/// </summary>
	public enum Attributes:int
	{	
        /// <summary>
        /// Price target ID
        /// </summary>
        PriceTargetID = 744,
		/// <summary>
		/// 
		/// </summary>
		SubscriptionStatus = 1000,
		/// <summary>
		/// 
		/// </summary>
		PrivillageEndDays = 1001,
		/// <summary>
		/// 
		/// </summary>
		DaysFromRegistration = 1002,
		/// <summary>
		/// 
		/// </summary>
		Age = 1003,
		/// <summary>
		/// 
		/// </summary>
		Country = 1004,
		/// <summary>
		/// 
		/// </summary>
		State = 1005,
		/// <summary>
		/// 
		/// </summary>
		City = 1006,
		/// <summary>
		/// 
		/// </summary>
		DMA = 1007,
		/// <summary>
		/// 
		/// </summary>
		DaysToBirthDate=1008,
		/// <summary>
		/// 
		/// </summary>
		LastDigitOfMemberID = 1009,
		/// <summary>
		/// 
		/// </summary>
		BirthYear = 1010,
		/// <summary>
		/// This attribute is used to indicate that a promo actually has no criterion
		/// </summary>
		NoCriterion = 1011,
        DeviceOS = 1012
	}

	/// <summary>
	/// 
	/// </summary>
	public class MemberAttributeMapper
	{
        private const string CLASS_NAME = "MemberAttributeMapper";
        public bool NUnit = false;
        public bool refreshedSubStatus = false;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="member"></param>
		/// <param name="attribute"></param>
		/// <param name="brand"></param>
		/// <returns></returns>
		public static int GetMemberAttributeValueInt(Matchnet.Member.ServiceAdapters.Interfaces.IMember member,PromoAttribute attribute,Matchnet.Content.ValueObjects.BrandConfig.Brand brand, MemberAttributeMapper mam)
		{
			int result=Constants.NULL_INT;
			if(attribute.AttrubiteType== PromoAttributeType.Calculated)
			{
				int regionID=member.GetAttributeInt(brand,"RegionID",0);

				switch  (attribute.ID)
				{  
					case	(int)Attributes.SubscriptionStatus : //1000
						Matchnet.Purchase.ValueObjects.MemberSubscriptionStatus memsubstatus= PurchaseSA.Instance.GetMemberSubStatus(member.MemberID,brand.Site.SiteID ,member.GetAttributeDate(brand,"SubscriptionExpirationDate",DateTime.MinValue), mam.refreshedSubStatus);
                        mam.refreshedSubStatus = true;
                        result= (int)  memsubstatus.Status;
						break;
					case	(int)Attributes.PrivillageEndDays : //1001
						DateTime dtSubEndDate= member.GetAttributeDate(brand,"SubscriptionExpirationDate",DateTime.MinValue);
						result = ((TimeSpan )dtSubEndDate.Subtract(DateTime.Now)).Days;
						break;
					case	(int)Attributes.DaysFromRegistration : //1002
						DateTime memberRegDate = member.GetAttributeDate(brand, "BrandInsertDate", System.DateTime.MinValue);			
						result = ((TimeSpan )DateTime.Now.Subtract(memberRegDate)).Days;								
						break;
					case	(int)Attributes.Age :					//1003
						result=GetAge(member.GetAttributeDate(brand,"BirthDate",DateTime.MinValue));
						break;
					case	(int)Attributes.Country :				//1004
						RegionLanguage regionCountry = RegionSA.Instance.RetrievePopulatedHierarchy(regionID,Constants.NULL_INT);
						if (regionCountry!=null)
						{
							if( regionCountry.CountryRegionID>0 )
							{
								result=regionCountry.CountryRegionID;
							}
						}
						break;
					case	(int)Attributes.State:					//1005
						RegionLanguage regionState = RegionSA.Instance.RetrievePopulatedHierarchy(regionID,Constants.NULL_INT);           
						if (regionState !=null)
						{
							if( regionState.StateRegionID>0 )
							{
								result=regionState.StateRegionID;
							}
						}
						break;			
					case	(int)Attributes.City :					//1006
						RegionLanguage regionCity = RegionSA.Instance.RetrievePopulatedHierarchy(regionID,Constants.NULL_INT);
						if (regionCity !=null)
						{
							if( regionCity.CityRegionID>0 )
							{
								result=regionCity.CityRegionID;
							}
						}
						break;
					case	(int)Attributes.DMA :					//1007
						DMA memDma= RegionSA.Instance.GetDMAByZipRegionID(regionID);	
						if (memDma!=null)
						{
							result=memDma.DMAID;
						}
						break;
					case	(int)Attributes.DaysToBirthDate:		//1008
						DateTime memberBirthDate = member.GetAttributeDate(brand, "BirthDate", System.DateTime.MinValue);			
						DateTime nextBDate= DateTime.MinValue;
						if (memberBirthDate != System.DateTime.MinValue)
						{	
							// initializing the next Birth day date 
							if (System.DateTime.IsLeapYear(memberBirthDate.Year) && !System.DateTime.IsLeapYear(DateTime.Now.Year) && 
								(memberBirthDate.Month == 2) && (memberBirthDate.Day == 29))
							{
								// Leap year, set to March 1 instead of Feb 29
								nextBDate  = new DateTime(DateTime.Now.Year, 3, 1);
							}
							else
							{	
								nextBDate= new DateTime(DateTime.Now.Year,memberBirthDate.Month,memberBirthDate.Day);
							}
						}
						result = ((TimeSpan )nextBDate.Subtract(DateTime.Now)).Days;								
						/*
							// This code re-adjusts the days to birth day date to a positive number which is the next birth day date
							if (result <0)
							{
								nextBDate= new DateTime(DateTime.Now.Year+1,memberBirthDate.Month,memberBirthDate.Day);
								result = ((TimeSpan )nextBDate.Subtract(DateTime.Now)).Days;	
							}*/					
						break;
					case (int)Attributes.LastDigitOfMemberID: // 1009
						result = member.MemberID % 10;
						break;
					case (int)Attributes.BirthYear:	// 1010
						DateTime memberBirthDate2 = member.GetAttributeDate(brand, "BirthDate", System.DateTime.MinValue);
						if(memberBirthDate2 != System.DateTime.MinValue)
						{
							result = memberBirthDate2.Year;
						}
						break;
					case (int)Attributes.NoCriterion: // 1011
						// Since this attribute is used to indicate that a promo has no criterion, this will always return 1 for a member
						result = 1;
						break;
                    case (int)Attributes.DeviceOS: //1012
                        result = GetDeviceOS(member, attribute, brand, mam);
                        break;
				}		
			}
			else // native/simple attribute
			{
                if (mam.NUnit)
                {
                    result = member.GetAttributeInt(brand, attribute.ID.ToString(), Constants.NULL_INT);
                }
                else
                {
                    result = (member as Matchnet.Member.ServiceAdapters.Member).GetAttributeInt(brand, attribute.ID, Constants.NULL_INT);
                }
			}				
			return result;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="member"></param>
		/// <param name="attribute"></param>
		/// <param name="brand"></param>
		/// <returns></returns>
		public static DateTime  GetMemberAttributeValueDate(Matchnet.Member.ServiceAdapters.Member member,PromoAttribute attribute,Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
		{
			DateTime result=DateTime.MinValue;
			if(attribute.AttrubiteType== PromoAttributeType.Calculated)
			{
				// no calculated attributes implemented yet
		
			}
			else // native/simple attribute
			{
				result=member.GetAttributeDate(brand,attribute.ID);	
			}				
			return result;
		}

		private static int GetAge(DateTime birthDate)
		{	
			if (birthDate == DateTime.MinValue) return 0;
			return (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
		}

        private static int GetDeviceOS(Matchnet.Member.ServiceAdapters.Interfaces.IMember member, PromoAttribute attribute, Matchnet.Content.ValueObjects.BrandConfig.Brand brand, MemberAttributeMapper mam)
        {
            int result = Constants.NULL_INT;

            try
            {
                result = (int)MemberSA.Instance.GetRegisteredDevice(member, brand);
            }
            catch (Exception ex)
            {
                //likely error would be invalid xml structure (if it ever changes)
                //this error should not throw an exception but should return back null int so the user will fail to match this promo and move on to next
                result = Constants.NULL_INT;
                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, "MemberAttributeMapper.GetDeviceOS() - ERROR: " + ex.Message,
                        null);
            }

            return result;
        }
	}
}
