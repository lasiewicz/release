using System;
using System.Collections.Generic;
using System.Text;

using Matchnet;
using Matchnet.Exceptions;

using Spark.Configuration.BusinessLogic;
using Spark.Configuration.ValueObjects;
using Spark.Configuration.ValueObjects.ServiceDefinitions;
using Spark.Configuration.ValueObjects.Products;


namespace Spark.Configuration.ServiceManagers
{
    public class ProductSM : MarshalByRefObject, IProductService, IServiceManager, IDisposable
    {
        public ProductSM()
        {
        }

        public void PrePopulateCache()
        {
        }

        public ProductBase GetProduct(Int32 productID)
        {
            try
            {
                return ProductBL.GetProduct(productID);
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(ConfigurationServiceConstants.SERVICE_NAME, "Failure getting Product. ProductID: " + productID.ToString() + ")", ex));
            }
        }

        public Package GetPackage(Int32 packageID)
        {
            try
            {
                return ProductBL.GetPackage(packageID);
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(ConfigurationServiceConstants.SERVICE_NAME, "Failure getting Package. PackageID: " + packageID.ToString() + ")", ex));
            }
        }

        public List<ProductBase> GetProducts(Int32 packageID)
        {
            try
            {
                return ProductBL.GetProducts(packageID);
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(ConfigurationServiceConstants.SERVICE_NAME, "Failure getting Products for a package. PackageID: " + packageID.ToString() + ")", ex));
            }
        }

        public void Dispose()
        {
        }
    }
}
