using System;
using System.Collections.Generic;
using System.Text;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Caching;


using Spark.Configuration.ValueObjects;
using Spark.Configuration.ValueObjects.ServiceDefinitions;
using Spark.Configuration.ValueObjects.Products;


namespace Spark.Configuration.ServiceAdapters
{
    public class ProductSA : SABase
    {
        #region Singleton
        public static readonly ProductSA Instance = new ProductSA();

        private ProductSA()
        {
        }
        #endregion

        public Package GetPackage(Int32 packageID)
        {
            string uri = null;
            Package package = Matchnet.Caching.Cache.Instance.Get(Package.GetCacheKey(packageID)) as Package;

            if (package == null)
            {
                try
                {
                    uri = getServiceManagerUri();
                    package = getService(uri).GetPackage(packageID);
                    Matchnet.Caching.Cache.Instance.Insert(package);
                }
                catch (Exception ex)
                {
                    throw new SAException("Unable to get retrieve package.(uri: " + uri + ").", ex);
                }
            }

            return package;
        }

        public ProductBase GetProduct(Int32 productID)
        {
            string uri = null;
            ProductBase product = Matchnet.Caching.Cache.Instance.Get(ProductBase.GetCacheKey(productID)) as ProductBase;

            try
            {
                uri = getServiceManagerUri();
                product = getService(uri).GetProduct(productID);
                Matchnet.Caching.Cache.Instance.Insert(product);
            }
            catch (Exception ex)
            {
                throw new SAException("Unable to retrieve product (uri: " + uri + ").", ex);
            }
            return product;
        }

        public List<ProductBase> GetProducts(Int32 packageID)
        {
            string uri = null;
            //List<ProductBase> products = Matchnet.Caching.Cache.Instance.Get( Ask Garry what to use for cachekey

            try
            {
                uri = getServiceManagerUri();
                List<ProductBase> products = getService(uri).GetProducts(packageID);
                return products;
            }
            catch (Exception ex)
            {
                throw new SAException("Unable to retrieve products (uri: " + uri + ").", ex);
            }
        }

        private IProductService getService(string uri)
        {
            try
            {
                return (IProductService)Activator.GetObject(typeof(IProductService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }


        private string getServiceManagerUri()
        {
            try
            {   
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ValueObjects.ConfigurationServiceConstants.SERVICE_NAME, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(ConfigurationServiceConstants.SERVICE_MANAGER_NAME_PRODUCT);
                string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SP_CONFIGSVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }



        protected override void GetConnectionLimit()
        {
            //todo
            throw new Exception("The method or operation is not implemented.");
        }
    }
}
