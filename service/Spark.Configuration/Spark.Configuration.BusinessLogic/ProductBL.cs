using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

using Matchnet;
using Matchnet.Data;
using Matchnet.Data.Exceptions;
using Matchnet.Exceptions;

using Spark.Configuration.ValueObjects;
using Spark.Configuration.ValueObjects.ServiceDefinitions;
using Spark.Configuration.ValueObjects.Products;

namespace Spark.Configuration.BusinessLogic
{
    public class ProductBL
    {
        public static readonly ProductBL Instance = new ProductBL();

        private ProductBL()
        {
        }


        public static ProductBase GetProduct(int productID)
        {
            ProductBase productBase = null;
            SqlDataReader dataReader = null;

            try
            {
                try
                {
                    Command command = new Command("mnSystem", "dbo.up_Product_List_ById", productID);
                    command.AddParameter("@ProductID", SqlDbType.Int, ParameterDirection.Input, productID);
                    dataReader = Client.Instance.ExecuteReader(command);

                    bool colLookupDone = false;
                    Int32 ordinalProductTypeID = Constants.NULL_INT;
                    Int32 ordinalProductName = Constants.NULL_INT;
                    Int32 ordinalDisplayText = Constants.NULL_INT;
                    Int32 ordinalCurrencyID = Constants.NULL_INT;
                    Int32 ordinalPrice = Constants.NULL_INT;
                    Int32 ordinalRolloverProductID = Constants.NULL_INT;
                    Int32 ordinalServiceTypeID = Constants.NULL_INT;
                    Int32 ordinalDurationID = Constants.NULL_INT;
                    Int32 ordinalDurationTypeID = Constants.NULL_INT;
                    Int32 ordinalUnits = Constants.NULL_INT;
                    Int32 ordinalActive = Constants.NULL_INT;

                    //Nullable DB fields
                    Int32 durationID = Constants.NULL_INT;
                    DurationType durationTypeID = DurationType.None;
                    Int32 units = Constants.NULL_INT;
                    ServiceType serviceTypeID = ServiceType.None;


                    while (dataReader.Read())
                    {
                        if (colLookupDone)
                        {
                            ordinalProductTypeID = dataReader.GetOrdinal("ProductTypeID");
                            ordinalProductName = dataReader.GetOrdinal("ProductName");
                            ordinalDisplayText = dataReader.GetOrdinal("DisplayText");
                            ordinalCurrencyID = dataReader.GetOrdinal("CurrencyID");
                            ordinalPrice = dataReader.GetOrdinal("Price");
                            ordinalRolloverProductID = dataReader.GetOrdinal("RolloverProductID");
                            ordinalServiceTypeID = dataReader.GetOrdinal("ServiceTypeID");
                            ordinalDurationID = dataReader.GetOrdinal("DurationID");
                            ordinalDurationTypeID = dataReader.GetOrdinal("DurationTypeID");
                            ordinalUnits = dataReader.GetOrdinal("Units");
                            ordinalActive = dataReader.GetOrdinal("Active");
                            colLookupDone = true;
                        }

                        ProductType productTypeID = (ProductType)dataReader.GetInt32(ordinalProductTypeID);
                        String productName = dataReader.GetString(ordinalProductName);
                        String displayText = dataReader.GetString(ordinalDisplayText);
                        Int32 currencyID = dataReader.GetInt32(ordinalCurrencyID);
                        Decimal price = dataReader.GetDecimal(ordinalPrice);
                        Int32 rolloverProductID = dataReader.GetInt32(ordinalRolloverProductID);
                        if (!dataReader.IsDBNull(ordinalServiceTypeID))
                        {
                            serviceTypeID = (ServiceType)dataReader.GetInt32(ordinalServiceTypeID);
                        }
                        if (!dataReader.IsDBNull(ordinalDurationID))
                        {
                            durationID = dataReader.GetInt32(ordinalDurationID);
                        }
                        if (!dataReader.IsDBNull(ordinalDurationTypeID))
                        {
                            durationTypeID = (DurationType)dataReader.GetInt32(ordinalDurationTypeID);
                        }
                        if (!dataReader.IsDBNull(ordinalUnits))
                        {
                            units = dataReader.GetInt32(ordinalUnits);
                        }

                        Boolean active = dataReader.GetBoolean(ordinalActive);


                        switch (productTypeID)
                        {
                            case ProductType.Service:
                                productBase = new ProductService(productID,
                                    productTypeID,
                                    productName,
                                    displayText,
                                    currencyID,
                                    price,
                                    rolloverProductID,
                                    durationID,
                                    durationTypeID,
                                    serviceTypeID,
                                    active);
                                break;
                            case ProductType.Asset:
                                productBase = new ProductAsset(productID,
                                    productTypeID,
                                    productName,
                                    displayText,
                                    currencyID,
                                    price,
                                    units,
                                    active);
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw(new BLException("ProductBL error has occured trying to retrieve a Product. (ProductID: " + productID.ToString() + ")",ex));
                }

                return productBase;

            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }

        }

        public static List<ProductBase> GetProducts(int packageID)
        {
            SqlDataReader dataReader = null;
            ProductBase productBase = null;
            List<ProductBase> products = null;

            try
            {
                try
                {
                    Command command = new Command("mnSystem", "dbo.up_Product_List_ByPackageId", packageID);
                    command.AddParameter("@Packageid", SqlDbType.Int, ParameterDirection.Input, packageID);
                    dataReader = Client.Instance.ExecuteReader(command);

                    if (dataReader.HasRows)
                    {
                        bool colLookupDone = false;
                        Int32 ordinalProductID = Constants.NULL_INT;
                        Int32 ordinalProductTypeID = Constants.NULL_INT;
                        Int32 ordinalProductName = Constants.NULL_INT;
                        Int32 ordinalDisplayText = Constants.NULL_INT;
                        Int32 ordinalCurrencyID = Constants.NULL_INT;
                        Int32 ordinalPrice = Constants.NULL_INT;
                        Int32 ordinalRolloverProductID = Constants.NULL_INT;
                        Int32 ordinalServiceTypeID = Constants.NULL_INT;
                        Int32 ordinalDurationID = Constants.NULL_INT;
                        Int32 ordinalDurationTypeID = Constants.NULL_INT;
                        Int32 ordinalUnits = Constants.NULL_INT;
                        Int32 ordinalActive = Constants.NULL_INT;

                        //Nullable DB fields
                        ServiceType serviceTypeID = ServiceType.None;
                        Int32 durationID = Constants.NULL_INT;
                        DurationType durationTypeID = DurationType.None;
                        Int32 units = Constants.NULL_INT;

                        while (dataReader.Read()) 
                        {
                            if (colLookupDone)
                            {
                                ordinalProductID = dataReader.GetOrdinal("ProductID");
                                ordinalProductTypeID = dataReader.GetOrdinal("ProductTypeID");
                                ordinalProductName = dataReader.GetOrdinal("ProductName");
                                ordinalDisplayText = dataReader.GetOrdinal("DisplayText");
                                ordinalCurrencyID = dataReader.GetOrdinal("CurrencyID");
                                ordinalPrice = dataReader.GetOrdinal("Price");
                                ordinalRolloverProductID = dataReader.GetOrdinal("RolloverProductID");
                                ordinalServiceTypeID = dataReader.GetOrdinal("ServiceTypeID");
                                ordinalDurationID = dataReader.GetOrdinal("DurationID");
                                ordinalDurationTypeID = dataReader.GetOrdinal("DurationTypeID");
                                ordinalUnits = dataReader.GetOrdinal("Units");
                                ordinalActive = dataReader.GetOrdinal("Active");
                                colLookupDone = true;
                            }

                            Int32 productID = dataReader.GetInt32(ordinalProductID);
                            ProductType productTypeID = (ProductType)dataReader.GetInt32(ordinalProductTypeID);
                            String productName = dataReader.GetString(ordinalProductName);
                            String displayText = dataReader.GetString(ordinalDisplayText);
                            Int32 currencyID = dataReader.GetInt32(ordinalCurrencyID);
                            Decimal price = dataReader.GetDecimal(ordinalPrice);
                            Int32 rolloverProductID = dataReader.GetInt32(ordinalRolloverProductID);

                            if (!dataReader.IsDBNull(ordinalServiceTypeID))
                            {
                                serviceTypeID = (ServiceType)dataReader.GetInt32(ordinalServiceTypeID);
                            }
                            if (!dataReader.IsDBNull(ordinalDurationID))
                            {
                                durationID = dataReader.GetInt32(ordinalDurationID);
                            }
                            if (!dataReader.IsDBNull(ordinalDurationTypeID))
                            {
                                durationTypeID = (DurationType)dataReader.GetInt32(ordinalDurationTypeID);
                            }
                            if (!dataReader.IsDBNull(ordinalUnits))
                            {
                                units = dataReader.GetInt32(ordinalUnits);
                            }

                            Boolean active = dataReader.GetBoolean(ordinalActive);

                            switch (productTypeID)
                            {
                                case ProductType.Service:
                                    productBase = new ProductService(productID,
                                        productTypeID,
                                        productName,
                                        displayText,
                                        currencyID,
                                        price,
                                        rolloverProductID,
                                        durationID,
                                        durationTypeID,
                                        serviceTypeID,
                                        active);
                                    break;
                                case ProductType.Asset:
                                    productBase = new ProductAsset(productID,
                                        productTypeID,
                                        productName,
                                        displayText,
                                        currencyID,
                                        price,
                                        units,
                                        active);
                                    break;
                            }
                            products.Add(productBase);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw (new BLException("ProductBL error has occured trying to retrieve a Package. (PackageID: " + packageID.ToString() + ")", ex));
                }
                return products;
            }

            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }
            
        }

        public static Package GetPackage(int packageID)
        {
            Package package = null;
            SqlDataReader dataReader = null;
            List<ProductBase> products = null;

            package = Matchnet.Caching.Cache.Instance.Get(Package.GetCacheKey(packageID)) as Package;

            if (package == null)
            {
                try
                {
                    try
                    {
                        Command command = new Command("mnSystem", "dbo.up_Package_List_ById", packageID);
                        command.AddParameter("@PackageID", SqlDbType.Int, ParameterDirection.Input, packageID);
                        dataReader = Client.Instance.ExecuteReader(command);

                        bool colLookupDone = false;
                        Int32 ordinalPackageID = Constants.NULL_INT;
                        Int32 ordinalOfferID = Constants.NULL_INT;
                        Int32 ordinalPackageName = Constants.NULL_INT;
                        Int32 ordinalDisplayText = Constants.NULL_INT;
                        Int32 ordinalCurrencyID = Constants.NULL_INT;
                        Int32 ordinalPrice = Constants.NULL_INT;
                        Int32 ordinalDiscountID = Constants.NULL_INT;
                        Int32 ordinalDiscountTypeID = Constants.NULL_INT;
                        Int32 ordinalActive = Constants.NULL_INT;

                        while (dataReader.Read())
                        {
                            if (colLookupDone)
                            {
                                ordinalPackageID = dataReader.GetOrdinal("PackageID");
                                ordinalOfferID = dataReader.GetOrdinal("OfferID");
                                ordinalPackageName = dataReader.GetOrdinal("PackageName");
                                ordinalDisplayText = dataReader.GetOrdinal("DisplayText");
                                ordinalCurrencyID = dataReader.GetOrdinal("CurrencyID");
                                ordinalPrice = dataReader.GetOrdinal("Price");
                                ordinalDiscountID = dataReader.GetOrdinal("DiscountID");
                                ordinalDiscountTypeID = dataReader.GetOrdinal("DiscountTypeID");
                                ordinalActive = dataReader.GetOrdinal("Active");
                                colLookupDone = true;
                            }
                        }

                        Int32 offerID = dataReader.GetInt32(ordinalOfferID);
                        String packageName = dataReader.GetString(ordinalPackageName);
                        String displayText = dataReader.GetString(ordinalDisplayText);
                        Int32 currencyID = dataReader.GetInt32(ordinalCurrencyID);
                        Decimal price = dataReader.GetDecimal(ordinalPrice);
                        Int32 discountID = dataReader.GetInt32(ordinalDiscountID);
                        Int32 discountTypeID = dataReader.GetInt32(ordinalDiscountTypeID);
                        Boolean active = dataReader.GetBoolean(ordinalActive);

                        //Get the products for this package
                        products = ProductBL.GetProducts(packageID);

                        package = new Package(packageID,
                            offerID,
                            packageName,
                            displayText,
                            currencyID,
                            price,
                            discountID,
                            discountTypeID,
                            active,
                            products);
                        
                    }
                    catch (Exception ex)
                    {
                        throw (new BLException("ProductBL error has occured trying to retrieve a Package. (PackageID: " + packageID.ToString() + ")", ex));
                    }
                }
                finally
                {
                    if (dataReader != null)
                    {
                        dataReader.Close();
                    }
                }

                Matchnet.Caching.Cache.Instance.Insert(package);
            }

            return package;
        }
    }
}
