using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;

using Matchnet.RemotingServices;

namespace Spark.Configuration.Service
{
    public partial class ConfigurationService : RemotingServiceBase
    {
        public ConfigurationService()
        {
            InitializeComponent();
            RemotingServiceBase.ServiceConstant = Spark.Configuration.ValueObjects.ConfigurationServiceConstants.SERVICE_CONSTANT;
        }

        protected override void RegisterServiceManagers()
        {
            base.RegisterServiceManager(new Spark.Configuration.ServiceManagers.ProductSM());
            base.RegisterServiceManagers();
        }
    }
}
