using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;

namespace Spark.Configuration.Service
{
    static class Program
    {
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] { new ConfigurationService() };
            ServiceBase.Run(ServicesToRun);
        }
    }
}