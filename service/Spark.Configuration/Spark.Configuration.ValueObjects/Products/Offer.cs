using System;
using System.Collections.Generic;
using System.Text;

using Matchnet;

namespace Spark.Configuration.ValueObjects.Products
{
    [Serializable]
    public class Offer
    {
        private Int32 _offerID = Constants.NULL_INT;
        private string _code = Constants.NULL_STRING;
        private string _url = Constants.NULL_STRING;

        public Offer (Int32 offerId)
        {
            _offerID = offerId;
        }

        public Int32 OfferID
        {
            get
            {
                return _offerID;
            }
        }

        public string Code
        {
            get
            {
                return _code;
            }
        }

        public string Url
        {
            get
            {
                return _url;
            }
        }
    
    }

    
}
