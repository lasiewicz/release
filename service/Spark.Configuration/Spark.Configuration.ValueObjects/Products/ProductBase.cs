using System;
using System.Collections.Generic;
using System.Text;

using Matchnet;

namespace Spark.Configuration.ValueObjects.Products
{
    [Serializable]
    public enum ProductType : int
    {
        Service = 1,
        Asset = 2,
        OneTime = 3
    };


    [Serializable]
    public abstract class ProductBase : ICacheable
    {
        private const string CACHE_KEY_PREFIX = "ProductBase";
        private int _cacheTTLSeconds = 60 * 60;

        protected Int32 _productID = Constants.NULL_INT;
        protected ProductType _productType;
        protected String _productName = Constants.NULL_STRING;
        protected String _displayText = Constants.NULL_STRING;
        protected Int32 _currencyID = Constants.NULL_INT;
        protected Decimal _price = Constants.NULL_DECIMAL;
        protected Boolean _active = false;

        public ProductBase(Int32 productID,
            ProductType productType,
            String productName,
            String displayText,
            Int32 currencyID,
            Decimal price,
            Boolean active)
        {
            _productID = productID;
            _productType = productType;
            _productName = productName;
            _displayText = displayText;
            _currencyID = currencyID;
            _price = price;
            _active = active;
        }

        public Int32 ProductID
        {
            get
            {
                return _productID;
            }
        }

        public ProductType ProductType
        {
            get 
            {
                return _productType;   
            }
        }

        public String ProductName
        {
            get
            {
                return _productName;
            }
        }

        public String DisplayText
        {
            get
            {
                return _displayText;
            }
        }

        public Int32 CurrencyID
        {
            get
            {
                return _currencyID;
            }
        }

        public Decimal Price
        {
            get
            {
                return _price;
            }
        }

        public Boolean Active
        {
            get
            {
                return _active;
            }
        }

        #region ICacheable Members

        public CacheItemMode CacheMode
        {
            get
            {
                return CacheItemMode.Absolute;
            }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get
            {
                return CacheItemPriorityLevel.Default;
            }
            set
            {
            }
        }

        public int CacheTTLSeconds
        {
            get
            {
                return _cacheTTLSeconds;
            }
            set
            {
                _cacheTTLSeconds = value;
            }
        }

        public string GetCacheKey()
        {
            return ProductBase.GetCacheKey(_productID);
        }

        public static string GetCacheKey(int productID)
        {
            return CACHE_KEY_PREFIX + productID.ToString();
        }

        #endregion

    }
}
