using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using Matchnet;

namespace Spark.Configuration.ValueObjects.Products
{
    [Serializable]
    public class Package : ICacheable
    {
		private const string CACHE_KEY_PREFIX = "Package";
		private int _cacheTTLSeconds = 60 * 60;

        private Int32 _packageID;
        private Int32 _offerID = Constants.NULL_INT;
        private String _packageName = Constants.NULL_STRING;
        private String _displayText = Constants.NULL_STRING;
        private Int32 _currencyID = Constants.NULL_INT;
        private Decimal _price = Constants.NULL_DECIMAL;
        private Int32 _discountID = Constants.NULL_INT;
        private Int32 _discountTypeID = Constants.NULL_INT;
        private Boolean _active = false;
        private List<ProductBase> _products;

        public Package(Int32 packageID,
            Int32 offerID,
            String packageName,
            String displayText,
            Int32 currencyID,
            Decimal price,
            Int32 discountID,
            Int32 discountTypeID,
            Boolean active,
            List<ProductBase> products)
        {
            _packageID = packageID;
            _offerID = offerID;
            _packageName = packageName;
            _displayText = displayText;
            _currencyID = currencyID;
            _price = price;
            _discountID = discountID;
            _discountTypeID = discountTypeID;
            _active = active;
            _products = products;
        }

        
        public Int32 PackageID
        {
            get
            {
                return _packageID;
            }
        }

        public ProductBase this[Int32 index]
        {
            get
            {
                return _products[index];
            }
        }

        public Int32 ProductCount
        {
            get
            {
                return _products.Count;        
            }
        }

        public Int32 OfferID
        {
            get
            {
                return _offerID;
            }
        }

        public string PackageName
        {
            get
            {
                return _packageName;
            }
        }

        public string DisplayText
        {
            get
            {
                return _displayText;
            }
        }

        public Int32 CurrencyID
        {
            get
            {
                return _currencyID;
            }
        }

        public decimal Price
        {
            get
            {
                return _price;
            }
        }

        public Int32 DiscountID
        {
            get
            {
                return _discountID;
            }
        }

        public Int32 DiscountTypeID
        {
            get
            {
                return _discountTypeID;
            }
        }

        public bool Active
        {
            get
            {
                return _active;
            }
        }


        #region ICacheable Members
        public CacheItemMode CacheMode
        {
            get
            {
                return CacheItemMode.Absolute; 
            }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get
            {
                return CacheItemPriorityLevel.Default;
            }
            set
            {
            }
        }

        public int CacheTTLSeconds
        {
            get
            {
                return _cacheTTLSeconds;
            }
            set
            {
                _cacheTTLSeconds = value;
            }
        }


        public string GetCacheKey()
        {
            return Package.GetCacheKey(_packageID);
        }


        public static string GetCacheKey(Int32 packageID)
        {
            return CACHE_KEY_PREFIX + packageID.ToString();
        }
        #endregion
    }
}
