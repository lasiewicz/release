using System;
using System.Collections.Generic;
using System.Text;

using Matchnet;

namespace Spark.Configuration.ValueObjects.Products
{
    public class ProductAsset : ProductBase
    {

        protected Int32 _units = Constants.NULL_INT;

        public ProductAsset(Int32 productID,
            ProductType productType,
            String productName,
            String displayText,
            Int32 currencyID,
            Decimal price,
            Int32 units,
            Boolean active)
            : base(productID, productType, productName, displayText, currencyID, price, active)
        {
            _units = units;
        }

        public new Int32 ProductID
        {
            get
            {
                return base._productID;
            }
        }

        public new ProductType ProductType
        {
            get
            {
                return base._productType;
            }
        }

        public new String ProductName
        {
            get
            {
                return base._productName;
            }
        }

        public new String DisplayText
        {
            get
            {
                return base._displayText;
            }
        }

        public new Int32 CurrencyID
        {
            get
            {
                return base._currencyID;
            }
        }

        public new decimal Price
        {
            get
            {
                return base.Price;
            }
        }

        public Int32 Units
        {
            get
            {
                return _units;
            }
        }

        public new Boolean Active
        {
            get
            {
                return base._active;
            }
        }
    }
}
