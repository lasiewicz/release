using System;
using System.Collections.Generic;
using System.Text;

using Matchnet;

namespace Spark.Configuration.ValueObjects.Products
{
    [Flags]
    public enum ServiceType : int
    {
        //We should probably redo these servicetypes
        None = 0,
        Regular = 1,
        OneTimeOnly = 2,
        AuthOnly = 4,
        TerminateImmediately = 8,
        FreeTrialWelcome = 16,
        Registration = 32,
        Installment = 64
    }

    
    [Serializable]
    public class ProductService : ProductBase
    {
        protected Int32 _rolloverProductID = Constants.NULL_INT;
        protected Int32 _duration = Constants.NULL_INT;
        protected DurationType _durationType;
        protected ServiceType _serviceType = ServiceType.Regular;

        public ProductService(Int32 productID,
            ProductType productType,
            String productName,
            String displayText,
            Int32 currencyID,
            Decimal price,
            Int32 rolloverProductID,
            Int32 duration,
            DurationType durationType,
            ServiceType serviceType,
            Boolean active)
            : base(productID, productType, productName, displayText, currencyID, price, active)
        {
            _rolloverProductID = rolloverProductID;
            _duration = duration;
            _durationType = durationType;
            _serviceType = serviceType;
        }


        public new Int32 ProductID
        {
            get
            {
                return base._productID;
            }
        }

        public new ProductType ProductType
        {
            get
            {
                return base._productType;
            }
        }

        public new String ProductName
        {
            get
            {
                return base._productName;
            }
        }

        public new String DisplayText
        {
            get
            {
                return base._displayText;
            }
        }

        public new Int32 CurrencyID
        {
            get
            {
                return base._currencyID;
            }
        }

        public new decimal Price
        {
            get
            {
                return base.Price;
            }
        }

        public Int32 Duration
        {
            get
            {
                return Duration;
            }
        }

        public DurationType DurationType
        {
            get
            {
                return DurationType;
            }
        }

        public ServiceType ServiceType
        {
            get
            {
                return _serviceType;
            }        
        }

        public new Boolean Active
        {
            get
            {
                return base._active;
            }
        }


    }
}
