using System;
using System.Collections.Generic;
using System.Text;

namespace Spark.Configuration.ValueObjects.Products
{
    public class DecrementableAsset : ProductBase
    {
        public DecrementableAsset(Int32 productID) : base(productID)
        {
        }
    }
}
