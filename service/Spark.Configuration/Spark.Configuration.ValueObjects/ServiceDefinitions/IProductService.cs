using System;
using System.Collections.Generic;
using System.Text;

using Spark.Configuration.ValueObjects.Products;

namespace Spark.Configuration.ValueObjects.ServiceDefinitions
{
    public interface IProductService
    {
        ProductBase GetProduct(int productID);
        List<ProductBase> GetProducts(int packageID);
        Package GetPackage(int packageID);
    }

}
