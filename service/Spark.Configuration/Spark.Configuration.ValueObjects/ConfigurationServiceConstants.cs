using System;
using System.Collections.Generic;
using System.Text;

namespace Spark.Configuration.ValueObjects
{
    public class ConfigurationServiceConstants
    {
        private ConfigurationServiceConstants()
        {
        }

        public const string SERVICE_NAME = "Spark.Configuration.Service";
        public const string SERVICE_CONSTANT = "SP_CONFIG_SVC";
        public const string SERVICE_MANAGER_NAME_PRODUCT = "ProductSM";
    }
}
