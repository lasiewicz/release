using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Matchnet.List.ServiceManagers;
using Matchnet.List.ValueObjects;


namespace Matchnet.List.Service
{
	public class ListService : Matchnet.RemotingServices.RemotingServiceBase
	{
		private System.ComponentModel.Container components = null;
		private ListSM _listSM;
		private QuotaSM _quotaSM;

		public ListService()
		{
			InitializeComponent();
		}

		static void Main()
		{
		    //System.Diagnostics.Debugger.Launch();


			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new ListService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			new ServiceBoundaryException(ListSM.SERVICE_NAME, "An unhandled exception occured.", e.ExceptionObject as Exception);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = ListSM.SERVICE_NAME;
			base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ListSM.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
		}

		protected override void RegisterServiceManagers()
		{
			try
			{
				_listSM = new ListSM();
				_quotaSM = new QuotaSM();

				base.RegisterServiceManager(_listSM);
				base.RegisterServiceManager(_quotaSM);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ListSM.SERVICE_NAME, "Error occurred when starting the Windows Service", ex);
			}
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}

				if (_listSM != null)
				{
					_listSM.Dispose();
				}

				if (_quotaSM != null)
				{
					_quotaSM.Dispose();
				}
			}
			base.Dispose( disposing );
		}
	}
}
