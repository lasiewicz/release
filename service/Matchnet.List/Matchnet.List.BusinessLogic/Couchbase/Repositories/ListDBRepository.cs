﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;
using Matchnet.Data;
using Matchnet.Exceptions;
using Matchnet.List.ValueObjects;
using Spark.Logging;

namespace Matchnet.List.BusinessLogic.Couchbase.Repositories
{
    public class ListDBRepository
    {

        public ClickMask GetClickMask(ListInternal listInternal, Int32 targetMemberID, Int32 communityID, Int32 siteID)
        {
            SqlDataReader reader = null;
            try
            {
                Command command = new Command("mnList", "dbo.up_YNMList_Get", listInternal.MemberID);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, listInternal.MemberID);
                command.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                command.AddParameter("@ListTypeID", SqlDbType.TinyInt, ParameterDirection.Input, (Byte)HotListType.YNM);

                reader = Client.Instance.ExecuteReader(command);

                PopulateListInternalFromDataReader(listInternal, reader);

                ClickMask clickMask = listInternal.GetClickMask(communityID, siteID, targetMemberID);

                //If no votes between these members, insert placeholder vote so this method does not get called again
                if (clickMask == ClickMask.None)
                {
                    listInternal.AddClick(communityID, siteID, targetMemberID, ClickDirectionType.Sender, ClickListType.None, DateTime.Now, false);
                }

                return clickMask;
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured while retrieving click mask.", ex));
            }
            finally
            {
                if (reader != null)
                {
                    if (reader.IsClosed != true)
                    { reader.Close(); }

                }

            }
        }


        public ListInternal GetListInternalMutualYesFromDB(ListInternal listInternal, int communityId)
        {
            const string methodName = "GetListInternalMutualYesFromDB()";
            SqlDataReader reader = null;
            try
            {

                Command command = new Command("mnList", "dbo.up_YNMList_MutualYes", listInternal.MemberID);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, listInternal.MemberID);
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityId);
                command.AddParameter("@YNMTypeID", SqlDbType.TinyInt, ParameterDirection.Input, (Byte)ClickListType.Yes);

                reader = Client.Instance.ExecuteReader(command);

                PopulateListInternalFromDataReader(listInternal, reader);

                listInternal.MutualYesLoaded = true;

                return listInternal;
            }
            catch (Exception ex)
            {
                var message = string.Format("BL error occured while retrieving in {0}. memberId:{1}, communityId:{2}", methodName, listInternal.MemberID, communityId);
                throw (new BLException(message, ex));
            }
            finally
            {
                if (reader != null)
                {
                    if (reader.IsClosed != true)
                    { reader.Close(); }

                }

            }
        }
     
        public void GetMatchesFromSqlRepo(ListInternal listInternal)
        {
            SqlDataReader dataReader = null;

            try
            {
                Command command = new Command("mnAlert", "dbo.up_MatchMailSchedule_List_Service", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, listInternal.MemberID);
                dataReader = Client.Instance.ExecuteReader(command);

                bool colLookupDone = false;
                Int32 ordinalUpdateDate = Constants.NULL_INT;
                Int32 ordinalDomainID = Constants.NULL_INT;
                Int32 ordinalMemberIDList = Constants.NULL_INT;

                while (dataReader.Read())
                {
                    if (!colLookupDone)
                    {
                        ordinalUpdateDate = dataReader.GetOrdinal("UpdateDate");
                        ordinalDomainID = dataReader.GetOrdinal("DomainID");
                        ordinalMemberIDList = dataReader.GetOrdinal("MemberIDList");

                        colLookupDone = true;
                    }

                    if (!dataReader.IsDBNull(ordinalUpdateDate) && !dataReader.IsDBNull(ordinalMemberIDList))
                    {
                        string[] memberIDList = dataReader.GetString(ordinalMemberIDList).ToString().Split(',');
                        int communityID = dataReader.GetInt32(ordinalDomainID);
                        DateTime actionDate = dataReader.GetDateTime(ordinalUpdateDate);

                        for (Int32 memberIDNum = 0; memberIDNum < memberIDList.Length; memberIDNum++)
                        {
                            Double targetMemberID = 0;

                            if (Double.TryParse(memberIDList[memberIDNum], NumberStyles.Integer, NumberFormatInfo.InvariantInfo, out targetMemberID))
                            {
                                listInternal.AddListMember(HotListCategory.YourMatches,
                                    communityID,
                                    (int)targetMemberID,
                                    actionDate,
                                    false);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when retrieving matches data.", ex));
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }
        }

        public void GetListsFromSqlRepo(ListInternal listInternal, Int32[] communityIDList)
        {
            SqlDataReader dataReader = null;
            SqlParameterCollection sqlParameters;

            try
            {
                StringBuilder sb = new StringBuilder();
                for (Int32 communityIDNum = 0; communityIDNum < communityIDList.Length; communityIDNum++)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(",");
                    }
                    sb.Append(communityIDList[communityIDNum]);
                }

                Command command = new Command("mnList", "dbo.up_YNMList_List_Service_MaxCount", listInternal.MemberID);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, listInternal.MemberID);
                command.AddParameter("@DomainIDs", SqlDbType.VarChar, ParameterDirection.Input, sb.ToString());
                command.AddParameter("@ListTypeID", SqlDbType.TinyInt, ParameterDirection.Input, 0);
                command.AddParameter("@MaxYNMVoteCount", SqlDbType.Int, ParameterDirection.Input, ListBL.Instance.MaxYNMVoteCount);
                command.AddParameter("@YNMVotesLoaded", SqlDbType.Bit, ParameterDirection.Output);

                dataReader = Client.Instance.ExecuteReader(command, out sqlParameters);

                PopulateListInternalFromDataReader(listInternal, dataReader);
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when retrieving matches data.", ex));
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }

            listInternal.YNMVotesLoaded = Conversion.CBool(sqlParameters["@YNMVotesLoaded"].Value);
        }

        private void PopulateListInternalFromDataReader(ListInternal listInternal, SqlDataReader dataReader)
        {
            HotListCategory category;
            string n;
            bool colLookupDone = false;
            Int32 ordinalListTypeID = Constants.NULL_INT;
            Int32 ordinalDomainID = Constants.NULL_INT;
            Int32 ordinalPrivateLabelID = Constants.NULL_INT;
            Int32 ordinalTargetMemberID = Constants.NULL_INT;
            Int32 ordinalDirectionFlag = Constants.NULL_INT;
            Int32 ordinalYNMTypeID = Constants.NULL_INT;
            Int32 ordinalDateTime = Constants.NULL_INT;

            while (dataReader.Read())
            {
                if (!colLookupDone)
                {
                    ordinalListTypeID = dataReader.GetOrdinal("ListTypeID");
                    ordinalDomainID = dataReader.GetOrdinal("DomainID");
                    ordinalPrivateLabelID = dataReader.GetOrdinal("PrivateLabelID");
                    ordinalTargetMemberID = dataReader.GetOrdinal("TargetMemberID");
                    ordinalDirectionFlag = dataReader.GetOrdinal("DirectionFlag");
                    ordinalYNMTypeID = dataReader.GetOrdinal("YNMTypeID");
                    ordinalDateTime = dataReader.GetOrdinal("DateTime");

                    colLookupDone = true;
                }

                Int32 siteID = dataReader.GetInt32(ordinalPrivateLabelID);

                switch (siteID)
                {
                    case 1:
                        siteID = 101;
                        break;

                    case 2:
                        siteID = 102;
                        break;

                    case 3:
                        siteID = 103;
                        break;

                    case 12:
                        siteID = 112;
                        break;

                }

                switch (dataReader.GetByte(ordinalListTypeID))
                {
                    case ((byte)HotListType.YNM):
                        if (!dataReader.IsDBNull(ordinalYNMTypeID))
                        {
                            Int32 domainID = dataReader.GetInt32(ordinalDomainID);
                            Int32 targetMemberID = dataReader.GetInt32(ordinalTargetMemberID);
                            ClickDirectionType clickDirection = (ClickDirectionType)Enum.Parse(typeof(ClickDirectionType), dataReader.GetInt32(ordinalDirectionFlag).ToString());
                            ClickListType clickListType = (ClickListType)Enum.Parse(typeof(ClickListType), dataReader.GetInt32(ordinalYNMTypeID).ToString());
                            DateTime dateTime = dataReader.GetDateTime(ordinalDateTime);

                            listInternal.AddClick(dataReader.GetInt32(ordinalDomainID),
                                siteID,
                                dataReader.GetInt32(ordinalTargetMemberID),
                                (ClickDirectionType)Enum.Parse(typeof(ClickDirectionType), dataReader.GetInt32(ordinalDirectionFlag).ToString()),
                                (ClickListType)Enum.Parse(typeof(ClickListType), dataReader.GetInt32(ordinalYNMTypeID).ToString()),
                                dataReader.GetDateTime(ordinalDateTime),
                                false);
                        }
                        break;

                    case ((byte)HotListType.ViewProfile):
                        n = dataReader.GetDataTypeName(ordinalDirectionFlag);

                        category = ListInternal.MapListCategory(HotListType.ViewProfile,
                            (HotListDirection)Enum.Parse(typeof(HotListDirection),
                            dataReader.GetInt32(ordinalDirectionFlag).ToString()));

                        listInternal.AddListMember(category,
                            dataReader.GetInt32(ordinalDomainID),
                            dataReader.GetInt32(ordinalTargetMemberID),
                            dataReader.GetDateTime(ordinalDateTime),
                            false);
                        break;

                    case ((byte)HotListType.ReportAbuse):
                        n = dataReader.GetDataTypeName(ordinalDirectionFlag);

                        category = ListInternal.MapListCategory(HotListType.ReportAbuse,
                            HotListDirection.OnYourList);

                        listInternal.AddListMember(category,
                            dataReader.GetInt32(ordinalDomainID),
                            dataReader.GetInt32(ordinalTargetMemberID),
                            dataReader.GetDateTime(ordinalDateTime),
                            false);
                        break;
                }
            }
        }

        public void GetHotListFromSqlRepo(ListInternal listInternal)
        {
            SqlDataReader dataReader = null;

            try
            {
                var command = new Command("mnListMember", "dbo.up_List_List", listInternal.MemberID);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, listInternal.MemberID);
                dataReader = Client.Instance.ExecuteReader(command);

                var colLookupDone = false;
                var ordinalGroupID = Constants.NULL_INT;
                var ordinalMemberID = Constants.NULL_INT;
                var ordinalTargetMemberID = Constants.NULL_INT;
                var ordinalDirectionFlag = Constants.NULL_INT;
                var ordinalUpdateDate = Constants.NULL_INT;
                var ordinalCategoryID = Constants.NULL_INT;
                var ordinalListTypeID = Constants.NULL_INT;
                var ordinalTeaseID = Constants.NULL_INT;
                var ordinalListText = Constants.NULL_INT;

                while (dataReader.Read())
                {
                    if (!colLookupDone)
                    {
                        ordinalGroupID = dataReader.GetOrdinal("GroupID");
                        ordinalMemberID = dataReader.GetOrdinal("MemberID");
                        ordinalTargetMemberID = dataReader.GetOrdinal("TargetMemberID");
                        ordinalDirectionFlag = dataReader.GetOrdinal("DirectionFlag");
                        ordinalUpdateDate = dataReader.GetOrdinal("UpdateDate");
                        ordinalCategoryID = dataReader.GetOrdinal("CategoryID");
                        ordinalListTypeID = dataReader.GetOrdinal("ListTypeID");
                        ordinalTeaseID = dataReader.GetOrdinal("TeaseID");
                        ordinalListText = dataReader.GetOrdinal("ListText");
                        colLookupDone = true;
                    }

                    var communityID = dataReader.GetInt32(ordinalGroupID);
                    var sourceMemberID = dataReader.GetInt32(ordinalMemberID);
                    var targetMemberID = dataReader.GetInt32(ordinalTargetMemberID);
                    var directionFlag = (Int32)dataReader.GetByte(ordinalDirectionFlag);
                    var actionDate = dataReader.GetDateTime(ordinalUpdateDate);

                    var category = HotListCategory.Default;
                    var teaseID = Constants.NULL_INT;
                    var comment = Constants.NULL_STRING;

                    if (!dataReader.IsDBNull(ordinalCategoryID))
                    {
                        category =
                            (HotListCategory)
                                Enum.Parse(typeof(HotListCategory), dataReader.GetInt32(ordinalCategoryID).ToString());
                    }
                    else
                    {
                        HotListDirection direction;

                        if (directionFlag == 1)
                        {
                            direction = HotListDirection.OnYourList;
                        }
                        else
                        {
                            direction = HotListDirection.OnTheirList;
                        }

                        category =
                            ListInternal.MapListCategory(
                                (HotListType)Enum.Parse(typeof(HotListType), dataReader.GetByte(ordinalListTypeID).ToString()),
                                direction);
                    }

                    if (!dataReader.IsDBNull(ordinalTeaseID))
                    {
                        teaseID = dataReader.GetInt32(ordinalTeaseID);
                    }

                    if (!dataReader.IsDBNull(ordinalListText))
                    {
                        comment = dataReader.GetString(ordinalListText);
                    }

                    if (teaseID != Constants.NULL_INT)
                    {
                        listInternal.AddListMember(category,
                            communityID,
                            targetMemberID,
                            actionDate,
                            teaseID,
                            false);
                    }
                    else if (comment != Constants.NULL_STRING)
                    {
                        listInternal.AddListMember(category,
                            communityID,
                            targetMemberID,
                            actionDate,
                            comment,
                            false);
                    }
                    else
                    {
                        listInternal.AddListMember(category,
                            communityID,
                            targetMemberID,
                            actionDate,
                            false);
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when retrieving hotlist data.", ex));
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }
        }


        /// <summary>
        /// Save Viral Status Mask
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="targetMemberID"></param>
        /// <param name="brand"></param>
        /// <param name="viralStatus"></param>
        /// <remarks>
        /// The up_YNMList_Status_Save stored procedure does an 
        /// </remarks>
        public void SaveViralStatus(Int32 memberID, Int32 targetMemberID, int communityId, int viralStatus)
        {
            // Update member's list.
            var command = new Command("mnList", "up_YNMList_Status_Save", memberID);

            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, communityId);
            command.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
            command.AddParameter("@MailMask", SqlDbType.TinyInt, ParameterDirection.Input, viralStatus);

            try
            {
                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ListBL.SERVICE_CONSTANT, "ViralMailBL", "SaveViralStatus RecipientMember " + memberID.ToString(), ex, null);
                throw new BLException("Error saving viral status. [MemberID:" + memberID +
                    ", TargetmemberID: " + targetMemberID +
                    ", CommunityID" + communityId +
                    ", MailMask" + viralStatus +
                    ex);
            }

            // Update target member's list.
            command = new Command("mnList", "up_YNMList_Status_Save", targetMemberID);

            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
            command.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, communityId);
            command.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@MailMask", SqlDbType.TinyInt, ParameterDirection.Input, viralStatus);

            try
            {
                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ListBL.SERVICE_CONSTANT, "ViralMailBL", "SaveViralStatus TargetMember " + targetMemberID.ToString(), ex, null);
                throw new BLException("Error saving viral status. [MemberID:" + memberID +
                    ", TargetmemberID: " + targetMemberID +
                    ", CommunityID" + communityId +
                    ", MailMask" + viralStatus +
                    ex);
            }
        }

        public void SaveYnmList(int communityID, int siteID, int memberID, int targetMemberID, DateTime actionDate,
            int teaseID, int senderMemberID, HotListType listType)
        {
            var command = new Command("mnList", "dbo.up_YNMList_Save", memberID);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, communityID);
            command.AddParameter("@SenderMemberID", SqlDbType.Int, ParameterDirection.Input, senderMemberID);
            command.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
            command.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, siteID);
            command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, actionDate);
            command.AddParameter("@ListTypeID", SqlDbType.TinyInt, ParameterDirection.Input, (Int32)listType);

            if (listType == HotListType.ReportAbuse && teaseID != Constants.NULL_INT)
            {
                command.AddParameter("@YNMTypeID", SqlDbType.TinyInt, ParameterDirection.Input, teaseID);
            }

            Client.Instance.ExecuteAsyncWrite(command);
        }

        public void SaveMemberList(HotListCategory category, int communityID, int memberID, int targetMemberID,
            string comment, int teaseID, HotListType listType, HotListDirection direction)
        {
            var command = new Command("mnListMember", "dbo.up_List_Save", memberID);
            command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, communityID);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
            command.AddParameter("@ListTypeID", SqlDbType.Int, ParameterDirection.Input, (int)listType);
            command.AddParameter("@DirectionFlag", SqlDbType.Bit, ParameterDirection.Input,
                (direction == HotListDirection.OnYourList) ? true : false);
            command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);

            if ((int)category > 0)
            {
                command.AddParameter("@CategoryID", SqlDbType.Int, ParameterDirection.Input, (int)category);
            }
            if (comment != null)
            {
                command.AddParameter("@ListText", SqlDbType.NVarChar, ParameterDirection.Input, comment);
            }
            if (teaseID != Constants.NULL_INT)
            {
                command.AddParameter("@TeaseID", SqlDbType.Int, ParameterDirection.Input, teaseID);
            }

            Client.Instance.ExecuteAsyncWrite(command);
        }

        public void DeleteList(int communityID, int memberID, int targetMemberID, HotListDirection direction, HotListType listType)
        {
            var command = new Command("mnListMember", "dbo.up_List_Delete", memberID);
            command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, communityID);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
            command.AddParameter("@DirectionFlag", SqlDbType.Bit, ParameterDirection.Input,
                (direction == HotListDirection.OnYourList) ? 1 : 0);
            command.AddParameter("@ListTypeID", SqlDbType.Int, ParameterDirection.Input, (Int32)listType);
            Client.Instance.ExecuteAsyncWrite(command);
        }

        public void DeleteListStp(int communityID, int memberID, int targetMemberID, HotListDirection direction)
        {
            var command = new Command("mnList", "dbo.up_ListSTP_Delete", memberID);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
            command.AddParameter("@DirectionFlag", SqlDbType.Bit, ParameterDirection.Input,
                (direction == HotListDirection.OnYourList) ? 1 : 0);
            command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, communityID);
            command.AddParameter("@ListTypeID", SqlDbType.Int, ParameterDirection.Input, 2);
            command.AddParameter("@DeleteDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
            Client.Instance.ExecuteAsyncWrite(command);
        }

        public void SaveYNMListDB(int communityID, int siteID, int memberID, int targetMemberID, ClickListType type,
            DateTime actionDate, int senderMemberID)
        {
            var command = new Command("mnList", "dbo.up_YNMList_Save", memberID);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
            command.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, communityID);
            command.AddParameter("@SenderMemberID", SqlDbType.Int, ParameterDirection.Input, senderMemberID);
            command.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
            command.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, siteID);
            command.AddParameter("@YNMTypeID", SqlDbType.Int, ParameterDirection.Input, (Int32) type);
            command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, actionDate);
            Client.Instance.ExecuteAsyncWrite(command);
        }
    }
}
