﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Matchnet.Content.ValueObjects.Quotas;
using Matchnet.List.BusinessLogic.Couchbase.Common;
using Matchnet.List.BusinessLogic.Couchbase.Interfaces;
using Matchnet.List.BusinessLogic.Couchbase.Models;
using Matchnet.List.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Matchnet.List.BusinessLogic.Couchbase.Repositories
{
    /// <summary>
    /// Represents a list repository using a couchbase bucket as repository
    /// </summary>
    /// <remarks>
    /// In order to succesfully use this repository we need to make sure the following views exist in the couchbase cluster.
    /// 
    /// Design document:
    ///  lists
    /// 
    /// ViewName: 
    /// 
    /// by_memberid
    /// 
    /// JS Code:
    /// function (doc, meta) {
    ///  if(doc.type && doc.type=="list")
    ///  {
    ///    emit(doc.mId, null);
    ///  }
    /// }
    ///
    /// ViewName: 
    /// 
    /// by_memberid_targetmemberid_communityid_listypeid
    /// 
    /// JS Code:
    /// 
    /// function (doc, meta) {
    ///  if(doc.type && doc.type=="list")
    ///  {
    ///    emit([doc.mId, doc.cId, doc.ltId], null);
    ///  }
    /// }
    ///
    /// </remarks>
    public class ListNoSqlRepository : NoSqlRepositoryBase<ListItem>, IListRepository
    {
        private const string ServiceApplicationName = "Couchbase";
        private const string ClassIdentifier = "MemberListNoSqlRepository";
        private const string ListDesignDocument = "lists"; 
        private const string ListViewByMemberId = "by_memberid";
        private const string ListViewByMemberIdCommunityAndCategory = "by_memberid_communityid_categoryid";
        private const string ListViewByMemberIdCommunityAndCategoryCount = "by_memberid_communityid_categoryid_count";
        private const string ListViewByMemberIdCommunityAndListType = "by_memberid_communityid_listtypeid";
        private const string ListViewByMemberIdCommunityAndListTypeCount = "by_memberid_communityid_listtypeid_count";
        private const string ListViewByMemberIdCommunityAndAllListTypes = "by_memberid_communityid_alllisttypeids";
        private const int GlobalMaxBeforeQuotaFiltering = 10000;
        private const int DefaultDurationTime = 60;

        //TODO: determined by a runtime setting
        public bool IsDev { get { return false; } }

        /// <summary>
        /// Gets all the list items for a given member.
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns>A list containing all the listItem</returns>
        /// <remarks> We should do some performance analysis with this method as it has the potential risk to fetch too many items</remarks>
        public List<ListItem> GetListView(int memberId)
        {
            var startDate = DateTime.UtcNow.AddDays(1);
            var startKey = new object[] { memberId, new object[]{ startDate.Year,startDate.Month,startDate.Day,99,99,99 }};
            var endDate = DateTime.UtcNow.AddYears(-20);
            var endKey = new object[] { memberId, new object[] { endDate.Year, endDate.Month, endDate.Day, 99, 99, 99 } };
            var query = CouchbaseAdapter.Instance.Bucket.CreateQuery(ListDesignDocument, ListViewByMemberId)
                    .StartKey(startKey)
                    .EndKey(endKey)
                    .Limit(GlobalMaxBeforeQuotaFiltering)
                    .Development(IsDev)
                    .Desc()
                    .ConnectionTimeout(DefaultDurationTime);

            var result = CouchbaseAdapter.Instance.Bucket.Query<dynamic>(query);

//            return result.Rows.Select(row => Get(row.Id)).ToList();
//            return Get(result.Rows.Select(row => row.Id).ToArray());
            return result.Rows.Select(row => Reconstitute(row.Id, (object)row.Key, (object)row.Value)).ToList();
        }

        
        /// <summary>
        /// Gets a list of ListItems using the ListViewByMemberIdCommunityAndListType as the index to fetch them from.
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="communityId"></param>
        /// <param name="listType"></param>
        /// <returns></returns>
        public List<ListItem> GetListView(int memberId, int communityId, HotListType listType)
        {
            var startDate = DateTime.UtcNow.AddDays(1);
            var endDate = DateTime.UtcNow.AddYears(-20);
            string listView = string.Empty;
            object[] startKey;
            object[] endKey;
            if(0 == (int)listType)
            {
                listView = ListViewByMemberIdCommunityAndAllListTypes;
                startKey = new object[]{memberId, communityId, new object[] {startDate.Year, startDate.Month, startDate.Day, 99, 99, 99}};
                endKey = new object[] {memberId, communityId, new object[] {endDate.Year, endDate.Month, endDate.Day, 99, 99, 99}};
            } else
            {
                listView = ListViewByMemberIdCommunityAndListType;
                startKey = new object[] { memberId, communityId, listType, new object[] {startDate.Year, startDate.Month, startDate.Day, 99, 99, 99}};
                endKey = new object[] { memberId, communityId, listType, new object[] { endDate.Year, endDate.Month, endDate.Day, 99, 99, 99 } };                
            }

            var query = CouchbaseAdapter.Instance.Bucket.CreateQuery(ListDesignDocument, listView)
                    .StartKey(startKey)
                    .EndKey(endKey)
                    .Limit(GlobalMaxBeforeQuotaFiltering)
                    .Development(IsDev)
                    .Desc()
                    .ConnectionTimeout(DefaultDurationTime);
            
            var result = CouchbaseAdapter.Instance.Bucket.Query<dynamic>(query);

            return result.Rows.Select(row => Reconstitute(row.Id, (object)row.Key, (object)row.Value)).ToList();
        }

        public int GetListCount(int memberId, int communityId, HotListType listType)
        {
            var startDate = DateTime.UtcNow.AddDays(1);
            var startKey = new object[] { memberId, communityId, listType, new object[] { startDate.Year, startDate.Month, startDate.Day, 99, 99, 99 } };
            var endDate = DateTime.UtcNow.AddYears(-20);
            var endKey = new object[] { memberId, communityId, listType, new object[] { endDate.Year, endDate.Month, endDate.Day, 99, 99, 99 } };
            var query = CouchbaseAdapter.Instance.Bucket.CreateQuery(ListDesignDocument, ListViewByMemberIdCommunityAndListTypeCount)
                    .StartKey(startKey)
                    .EndKey(endKey)
                    .Limit(GlobalMaxBeforeQuotaFiltering)
                    .Development(IsDev).Desc()
                    .ConnectionTimeout(DefaultDurationTime);

            var result = CouchbaseAdapter.Instance.Bucket.Query<dynamic>(query);

            return  (null != result && result.StatusCode == HttpStatusCode.OK && null != result.Rows && null != result.Rows.First()) ?
                (int)result.Rows.First().Value : 0;
        }

        /// <summary>
        /// Gets a list of ListItems using the ListViewByMemberIdCommunityAndCategory as the index to fetch them from.
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="communityId"></param>
        /// <param name="listCategory"></param>
        /// <returns></returns>
        public List<ListItem> GetListView(int memberId, int communityId, HotListCategory listCategory)
        {
            var startDate = DateTime.UtcNow.AddDays(1);
            var startKey = new object[] { memberId, communityId, listCategory, new object[] { startDate.Year, startDate.Month, startDate.Day, 99, 99, 99 } };
            var endDate = DateTime.UtcNow.AddYears(-20);
            var endKey = new object[] { memberId, communityId, listCategory, new object[] { endDate.Year, endDate.Month, endDate.Day, 99, 99, 99 } };
            var query = CouchbaseAdapter.Instance.Bucket.CreateQuery(ListDesignDocument, ListViewByMemberIdCommunityAndCategory)
                    .StartKey(startKey)
                    .EndKey(endKey)
                    .Limit(GlobalMaxBeforeQuotaFiltering)
                    .Development(IsDev)
                    .Desc()
                    .ConnectionTimeout(DefaultDurationTime);

            var result = CouchbaseAdapter.Instance.Bucket.Query<dynamic>(query);

            return result.Rows.Select(row => Reconstitute(row.Id, (object)row.Key, (object)row.Value)).ToList();
        }

        public int GetListCount(int memberId, int communityId, HotListCategory listCategory)
        {
            var startDate = DateTime.UtcNow.AddDays(1);
            var startKey = new object[] { memberId, communityId, listCategory, new object[] { startDate.Year, startDate.Month, startDate.Day, 99, 99, 99 } };
            var endDate = DateTime.UtcNow.AddYears(-20);
            var endKey = new object[] { memberId, communityId, listCategory, new object[] { endDate.Year, endDate.Month, endDate.Day, 99, 99, 99 } };
            var query = CouchbaseAdapter.Instance.Bucket.CreateQuery(ListDesignDocument, ListViewByMemberIdCommunityAndCategoryCount)
                    .StartKey(startKey)
                    .EndKey(endKey)
                    .Limit(GlobalMaxBeforeQuotaFiltering)
                    .Development(IsDev)
                    .Desc()
                    .ConnectionTimeout(DefaultDurationTime);

            var result = CouchbaseAdapter.Instance.Bucket.Query<dynamic>(query);

            return (null != result && result.StatusCode == HttpStatusCode.OK && null != result.Rows && null != result.Rows.First()) ?
                (int)result.Rows.First().Value : 0;
        }

        ///  <summary>
        ///  As of right now there is no easy way to regenerate the key for the document to delete in a deterministic way hence this method will try to get 
        ///  all the documents from the ListViewByMemberIdCommunityAndCategory and then delete them.
        ///  </summary>
        /// <param name="communityId"></param>
        /// <param name="memberId"></param>
        /// <param name="targetMemberId"></param>
        /// <param name="listType"></param>
        /// <param name="direction"></param>
        /// <param name="category"></param>
        /// <remarks>
        ///  To be able to invoke the delete method define in the abstract class we will need to regenerate the following key:
        /// 
        ///  var deletekey = string.Format("{0}_{1}_{2}_{3}_{4}_{5}_{6}_{7}", "list", 
        ///   communityId, 
        ///   model.SiteId, 
        ///   model.CategoryId, 
        ///   model.ListTypeId, 
        ///   model.Direction, 
        ///   model.MemberId, 
        ///   model.TargetMemberId);
        ///  </remarks>
        public void Delete(int communityId, int memberId, int targetMemberId, HotListType listType, HotListDirection direction, HotListCategory category=HotListCategory.Default)
        {
            //build list of keys to delete
            List<string> keys = new List<string>();

            //first, remove item with 0 siteId
            var removeItem = new ListItem()
            {
                SiteId = 0,
                CommunityId = communityId,
                ListTypeId = (int)listType,
                CategoryId = (int)category,
                Direction = (int)direction,
                MemberId = memberId,
                TargetMemberId = targetMemberId,
            };
            keys.Add(BuildKey(removeItem));

            //second, remove items with all site ids for community
            var brandsByCommunity = BrandConfigSA.Instance.GetBrandsByCommunity(communityId);
            foreach (var site in brandsByCommunity.GetSites())
            {
                removeItem.SiteId = site.SiteID;
                keys.Add(BuildKey(removeItem));
            }

            //multi-threaded remove
            Remove(keys.ToArray());
        }

        /// <summary>
        /// Saves or updates a new ListItem document.
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="siteId"></param>
        /// <param name="listType"></param>
        /// <param name="memberId"></param>
        /// <param name="targetMemberId"></param>
        /// <param name="teaseId"></param>
        /// <param name="actionDate"></param>
        public void Save(int communityId, int siteId, HotListType listType, int memberId, int targetMemberId, int teaseId, DateTime actionDate)
        {
            var savingItem = new ListItem()
            {
                SiteId = siteId,
                CommunityId = communityId,
                ListTypeId = (int) listType,
                MemberId = memberId,
                TargetMemberId = targetMemberId,
                ActionDate = actionDate,
                TeaseId = teaseId
            };

            Save(savingItem);
        }


        /// <summary>
        /// Saves or updates a ListItem document
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="category"></param>
        /// <param name="listType"></param>
        /// <param name="direction"></param>
        /// <param name="memberId"></param>
        /// <param name="targetMemberId"></param>
        /// <param name="comment"></param>
        /// <param name="teaseId"></param>
        /// <param name="actionDate"></param>
        public void Save(int communityId, HotListCategory category, HotListType listType, HotListDirection direction, int memberId, int targetMemberId, string comment, int teaseId, DateTime actionDate)
        {
            var savingItem = new ListItem()
            {
                CommunityId = communityId,
                CategoryId = (int)category,
                ListTypeId = (int) listType,
                Direction = (int) direction,
                MemberId = memberId,
                TargetMemberId = targetMemberId,
                ListText = comment,
                TeaseId = teaseId,
                ActionDate = actionDate
            };

            Save(savingItem);
        }

        /// <summary>
        /// Get a list Item
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <remarks>
        /// The server doesn't pass back the _id in the JSON so we set it after fetching the document
        /// </remarks>
        public override ListItem Get(string id)
        {
            var result = CouchbaseAdapter.Instance.Bucket.GetDocument<ListItem>(id);

            if (result.Success)
            {
                result.Content.Id = id; 
            }

            return result.Content;
        }

        /// <summary>
        /// Get list of ListItems. Performance improvement over single Get() call
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public override List<ListItem> Get(string[] ids)
        {
            var parallelOptions = new ParallelOptions();
            parallelOptions.MaxDegreeOfParallelism = 10;
            var result = CouchbaseAdapter.Instance.Bucket.Get<ListItem>(ids, parallelOptions);
            //set the ids in all of the ListItems
            foreach (string key in result.Keys)
            {
                result[key].Value.Id = key;
            }
            return result.Values.Select(v => v.Value).ToList();
        }

        /// <summary>
        /// Uses data from ViewRow to reconstitute ListItems. Dramatically increased performance
        /// versus both Get() calls for ListItem.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override ListItem Reconstitute(string id, object key, object value)
        {
            var strings = id.Split(new string[] {"_"}, StringSplitOptions.None);
            var array=(JArray) key;
            JArray dateTimeInts = array.Last.Value<JArray>();
            var valueArray = (JArray) value;
            var item = new ListItem()
            {
                Id = id,
                ActionDate = new DateTime(dateTimeInts[0].Value<int>(),
                    dateTimeInts[1].Value<int>(),
                    dateTimeInts[2].Value<int>(),
                    dateTimeInts[3].Value<int>(),
                    dateTimeInts[4].Value<int>(),
                    dateTimeInts[5].Value<int>()),
                CommunityId = Int32.Parse(strings[1]),
                SiteId = Int32.Parse(strings[2]),
                CategoryId = Int32.Parse(strings[3]),
                ListTypeId = Int32.Parse(strings[4]),
                Direction = Int32.Parse(strings[5]),
                MemberId = Int32.Parse(strings[6]),
                TargetMemberId = Int32.Parse(strings[7]),
                ListText = valueArray[0].Value<string>(),
                MailMask = valueArray[1].Value<int>(),
                ClickType = valueArray[2].Value<int>(),
                TeaseId = valueArray[3].Value<int>()
            };
            return item;
        }

        public override void Remove(string docKey)
        {
            CouchbaseAdapter.Instance.Bucket.Remove(docKey);
        }

        public override void Remove(string[] docKeys)
        {
            var parallelOptions = new ParallelOptions();
            parallelOptions.MaxDegreeOfParallelism = 10;
            CouchbaseAdapter.Instance.Bucket.Remove(docKeys, parallelOptions);
        }

        /// <summary>
        /// The format of the key should guaranteed the uniqueness of the item, otherwise it would override data.
        /// </summary>
        /// <param name="model">a ListItem object</param>
        /// <returns></returns>
        public override string BuildKey(ListItem model)
        {
            return string.Format("{0}_{1}_{2}_{3}_{4}_{5}_{6}_{7}", 
                model.Type, 
                model.CommunityId, 
                model.SiteId, 
                model.CategoryId, 
                model.ListTypeId, 
                model.Direction, 
                model.MemberId,
                model.TargetMemberId);
        }

        /// <summary>
        /// Override for the Store method, when saving ListItem objects we try to determine
        /// the duration time based on the community, for this to work we need to have override entries in the
        /// QuotaCommunity table, we will also need to add a new column to this table to support the duration value,
        ///
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        protected override ulong Store(ListItem model)
        {

            QuotaDefinition quotaDefinition = DetermineExpirationTime((HotListCategory) model.CategoryId, model.CommunityId);
            //TODO: Determine whether list has passed threshold (time based or count based). Queue up item for async list pruning.
            var result = CouchbaseAdapter.Instance.Bucket.Upsert(BuildKey(model), model);
            return result.Success ? result.Cas : 0;
        }


        /// <summary>
        /// This method will return the duration time based on QuotaType, 
        /// hence we need to map to QuotaType and CategoryTypes
        /// </summary>
        /// <param name="category"></param>
        /// <param name="communityId"></param>
        /// <returns></returns>
        protected QuotaDefinition DetermineExpirationTime(HotListCategory category, int communityId)
        {
            QuotaType quotaType;

            switch (category)
            {
                case HotListCategory.Default:
                case HotListCategory.WhoAddedYouToTheirFavorites:
                     quotaType = QuotaType.FavoriteList;
                    break;
                case HotListCategory.MembersYouViewed:
                case HotListCategory.WhoViewedYourProfile:
                case HotListCategory.MembersYouIMed:
                case HotListCategory.WhoIMedYou:
                    quotaType = QuotaType.ViewProfileList;
                    break;
                case HotListCategory.IgnoreList:
                    quotaType = QuotaType.IgnoreList;
                    break;
                case HotListCategory.MembersYouEmailed:
                    quotaType = QuotaType.SentEmail;
                    break;
                case HotListCategory.MembersYouSentAllAccessEmail:
                    quotaType = QuotaType.EmailList;
                    break;
                case HotListCategory.MembersYouTeased:
                    quotaType = QuotaType.Tease;
                    break;
                case HotListCategory.ReportAbuse:
                    quotaType = QuotaType.ReportAbuse;
                    break;
                case HotListCategory.ExcludeFromList:
                    quotaType = QuotaType.ExcludeFromList;
                    break;
                case HotListCategory.ExcludeList:
                    quotaType = QuotaType.ExcludeList;
                    break;
                case HotListCategory.MembersYouViewedInPushFlirts:
                    quotaType = QuotaType.ViewInPushFlirtsList;
                    break;
                default:
                    quotaType = QuotaType.FavoriteList;
                    break;
            }

            var quotaDefinitions = QuotaDefinitionSA.Instance.GetQuotaDefinitions();

            return quotaDefinitions.GetQuotaDefinition(quotaType, communityId);
        }




    }
}
