﻿using System.Collections.Generic;
using Matchnet.List.BusinessLogic.Couchbase.Models;

namespace Matchnet.List.BusinessLogic.Couchbase.Repositories
{
    /// <summary>
    /// Defines the basic operations a generic _noSqlRepository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class NoSqlRepositoryBase<T> where T : CouchbaseModelBase
    {
        static NoSqlRepositoryBase()
        {
        }

        /// <summary>
        /// This simply works as a wrapper for the store method, the store method should only handle the data insertion
        /// any business logic could be actually added to the overrided implementation of the Save method.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual ulong Save(T model)
        {
            return Store(model);
        }

        /// <summary>
        /// Gets a document by it's given id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public abstract T Get(string id);

        /// <summary>
        /// Gets list of documents by ids. Uses .Net parallel for to get docs.
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public abstract List<T> Get(string[] ids);

        /// <summary>
        /// If id, key, and value from ViewRow contains all data necessary to create object in cache then use this 
        /// method to reconstitute the object so that you don't have to retrieve it from Couchbase again using the id. 
        /// This is a _significant_ performance improvement over both Get() calls.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public abstract T Reconstitute(string id, object key, object value);

        /// <summary>
        /// Remove a document by it's given id.
        /// </summary>
        /// <param name="docKey"></param>
        /// <remarks>
        /// Key Value must be calculated in a deterministic way for to this method to be useful.
        /// </remarks>
        public abstract void Remove(string docKey);

        /// <summary>
        /// Removes documents by their keys.
        /// </summary>
        /// <param name="docKeys"></param>
        /// <remarks>
        /// Key Value must be calculated in a deterministic way for to this method to be useful.
        /// </remarks>
        public abstract void Remove(string[] docKeys);

        /// <summary>
        /// The format of the key should guaranteed the uniqueness of the item, othewise it would override data by usign an existing document.
        /// </summary>
        /// <param name="model">a T object</param>
        /// <returns></returns>
        public abstract string BuildKey(T model);


        /// <summary>
        /// Forces the subclass to implement the storing mechanism for T
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <remarks>
        /// When implementing a subclass make sure to consider expiration times.
        /// </remarks>
        protected abstract ulong Store(T model);

        
    }
}
