﻿using System;
using System.Linq;
using Couchbase;
using Couchbase.Configuration.Client;
using Couchbase.Core;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.List.BusinessLogic.Couchbase.Repositories;

namespace Matchnet.List.BusinessLogic.Couchbase.Common
{
    /// <summary>
    /// Provides an easy way to access the client, without having to re instanciate the Couchbase client
    /// </summary>
    /// <remarks>
    /// This adapter assumes the existence of the NoSQL bucket with no password being set, bucket names are case sensitive
    /// </remarks>
    public class CouchbaseAdapter : SingletonBase<CouchbaseAdapter>
    {
        private readonly IBucket _bucket;
        private readonly Cluster _cluster;
        private const string NoSqlBucketName = "NoSQL";
        private const string CouchbaseServerUri = "http://{0}:{1}/pools";
        

        private CouchbaseAdapter()
        {
            var webBucket = RuntimeSettings.Instance.GetMembaseConfigByBucketFromSingleton(NoSqlBucketName.ToLower());
            var servers = webBucket.Servers.Select(server => new Uri(String.Format(CouchbaseServerUri, server.ServerIP, server.ServerPort))).ToList();

            var config = new ClientConfiguration
            {               
                Servers = servers
            };

            _cluster = new Cluster(config);
            _bucket = _cluster.OpenBucket(NoSqlBucketName);
        }

        ~CouchbaseAdapter()
        {
            if (_bucket != null && _cluster!= null)
            {
                _cluster.CloseBucket(_bucket);
            }
        }

        /// <summary>
        /// Represents the bucket loaded from the configuration 
        /// </summary>
        public IBucket Bucket
        {
            get
            {
                return _bucket;
            }
        }
    }

    /// <summary>
    /// Singleton that wraps functionatily supported by the NoSQL List repository
    /// </summary>
    public class ListNoSqlAdapter : SingletonBase<ListNoSqlRepository>
    {
        private readonly ListNoSqlRepository _repository;

        private ListNoSqlAdapter()
        {
            _repository = new ListNoSqlRepository();
        }

        /// <summary>
        /// A ListItem couchbase repository.
        /// </summary>
        public ListNoSqlRepository Repository
        {
            get { return _repository; }
        }
    }

    /// <summary>
    /// Singleton that wraps functionatily supported by the DB List repository
    /// </summary>
    public class ListDBAdapter : SingletonBase<ListDBRepository>
    {
        private readonly ListDBRepository _repository;

        private ListDBAdapter()
        {
            _repository = new ListDBRepository();
        }

        /// <summary>
        /// A ListItem couchbase repository.
        /// </summary>
        public ListDBRepository Repository
        {
            get { return _repository; }
        }
    }

}
