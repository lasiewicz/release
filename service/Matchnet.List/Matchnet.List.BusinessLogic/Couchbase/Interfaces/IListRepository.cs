﻿using System;
using System.Collections.Generic;
using Matchnet.List.BusinessLogic.Couchbase.Models;
using Matchnet.List.ValueObjects;

namespace Matchnet.List.BusinessLogic.Couchbase.Interfaces
{
    /// <summary>
    /// This interface defines the required operations to Save, Delete and query list data
    /// </summary>
    public interface IListRepository
    {
        /// <summary>
        /// Gets list count based on memberId, communityId and the listType
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="communityId"></param>
        /// <param name="listType"></param>
        /// <returns></returns>
        int GetListCount(int memberId, int communityId, HotListType listType);

        /// <summary>
        /// Gets list count based on memberId, communityId and the list category
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="communityId"></param>
        /// <param name="listCategory"></param>
        /// <returns></returns>
        int GetListCount(int memberId, int communityId, HotListCategory listCategory);

        /// <summary>
        /// Gets list data based on memberId, communityId and the listType
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="communityId"></param>
        /// <param name="listType"></param>
        /// <returns></returns>
        List<ListItem> GetListView(int memberId, int communityId, HotListType listType);

        /// <summary>
        /// Gets list data based on memberId, communityId and the list category
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="communityId"></param>
        /// <param name="listCategory"></param>
        /// <returns></returns>
        List<ListItem> GetListView(int memberId, int communityId, HotListCategory listCategory);
        
        /// <summary>
        /// Get All the listitems associated to a memberId
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns></returns>
        List<ListItem> GetListView(int memberId);

        /// <summary>
        /// Save a list item
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="siteId"></param>
        /// <param name="listType"></param>
        /// <param name="memberId"></param>
        /// <param name="targetMemberId"></param>
        /// <param name="teaseId"></param>
        /// <param name="actionDate"></param>
        void Save(int communityId, int siteId, HotListType listType, int memberId, int targetMemberId, int teaseId, DateTime actionDate);

        /// <summary>
        /// Save a list item
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="category"></param>
        /// <param name="listType"></param>
        /// <param name="direction"></param>
        /// <param name="memberId"></param>
        /// <param name="targetMemberId"></param>
        /// <param name="comment"></param>
        /// <param name="teaseId"></param>
        /// <param name="actionDate"></param>
        void Save(int communityId, HotListCategory category, HotListType listType, HotListDirection direction,
            int memberId, int targetMemberId, string comment, int teaseId, DateTime actionDate);

        /// <summary>
        /// Deletes a list item based on the passed parameters
        /// </summary>
        /// <param name="communityId"></param>
        /// <param name="memberId"></param>
        /// <param name="targetMemberId"></param>
        /// <param name="listType"></param>
        /// <param name="direction"></param>
        /// <param name="category"></param>
        void Delete(int communityId, int memberId, int targetMemberId, HotListType listType, HotListDirection direction, HotListCategory category);
    }
}
