﻿using System;
using Newtonsoft.Json;

namespace Matchnet.List.BusinessLogic.Couchbase.Models
{
    /// <summary>
    /// Class used to represent a given list item with a key as:
    /// siteId_communityId_listCategoryId_listTypeId_direction_memberId
    /// </summary>
    /// <remarks>
    /// The short names defined in the JsonProperty attributes allow us to use less space when saving listItems
    /// </remarks>
    public class ListItem : CouchbaseModelBase
    {
        /// <summary>
        /// SiteId
        /// </summary>
        [JsonProperty("sId")]
        public int SiteId { get; set; }

        /// <summary>
        /// CommunityId, in the legacy code is also refer as groupId and in some cases domainId
        /// </summary>
        [JsonProperty("cId")] 
        public int CommunityId{ get; set; }

        /// <summary>
        /// CategoryId
        /// </summary>
        [JsonProperty("ctId")]
        public int CategoryId { get; set; }

        /// <summary>
        /// Represents the listType, refer to the HotListType enum
        /// </summary>
        [JsonProperty("ltId")]
        public int ListTypeId { get; set; } 

        /// <summary>
        /// Indicates the list item direction 0 = none, 1 = OnYourList, 2 = OnTheirList where the Sender = 1, Recipient = 0
        /// </summary>
        [JsonProperty("dir")]
        public int Direction { get; set; } 

        /// <summary>
        /// MemberId
        /// </summary>
        [JsonProperty("mId")]
        public int MemberId { get; set; }

        /// <summary>
        /// TargetMemberId
        /// </summary>
        [JsonProperty("tgId")]
        public int TargetMemberId { get; set; }

        /// <summary>
        /// ActionDate
        /// </summary>
        [JsonProperty("actD")]
        public DateTime ActionDate { get; set; }

        /// <summary>
        /// TeaseId
        /// </summary>
        [JsonProperty("teaId")]
        public int TeaseId { get; set; }

        /// <summary>
        /// MailMask
        /// </summary>
        [JsonProperty("mmsk")]
        public int MailMask { get; set; }

        /// <summary>
        /// Represents a note or comment
        /// </summary>
        [JsonProperty("txt")]
        public string ListText { get; set; }

        /// <summary>
        /// YesNoMaybe Click Type, possible values:  0 = none, 1 = yes, 2 = no, 3 = maybe
        /// </summary>
        [JsonProperty("cli")] 
        public int ClickType { get; set; }

        //TODO: should we have different list models?
        public override string Type
        {
            get { return "list"; }
        }

    }
}
