﻿using System;
using Newtonsoft.Json;

namespace Matchnet.List.BusinessLogic.Couchbase.Models
{
    /// <summary>
    /// Define the required properties for a valid couchbase document
    /// </summary>
    [Serializable]
    public abstract class CouchbaseModelBase
    {
        /// <summary>
        /// All documents are stored with a reserved “_id” property that derives its value from its key. 
        /// </summary>
        [JsonProperty(PropertyName = "_id")]
        public string Id { get; set; }

        /// <summary>
        /// Type is used to filter when creating views, it also allow us to have different document types in the same bucket.
        /// </summary>
        [JsonProperty("type")]
        public abstract string Type { get; }

    }
}
