using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Caching;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Data;
using Matchnet.Exceptions;
using Matchnet.List.BusinessLogic.Couchbase.Common;
using Matchnet.List.BusinessLogic.Couchbase.Models;
using Matchnet.List.ValueObjects;
using Matchnet.List.ValueObjects.ReplicationActions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects.ServiceDefinitions;
using Matchnet.ExternalMail.ValueObjects;
using System.Collections.Generic;
using System.Diagnostics;
using Spark.Logging;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Spark.PushNotifications.Processor.ServiceAdapter;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Content.ValueObjects.SystemEvents;
using Matchnet.List.BusinessLogic.Couchbase.Repositories;
using Spark.ActivityRecording.Processor.ValueObjects;
using Spark.ActivityRecording.Processor.ServiceAdapter;

namespace Matchnet.List.BusinessLogic
{
	public class ListBL : MarshalByRefObject
	{
		
		public readonly static ListBL Instance = new ListBL();
        public const string SERVICE_CONSTANT = "LIST_SVC";
        public const string CLASS_NAME = "ListBL";

		private Matchnet.Caching.Cache _cache;
		private CacheItemRemovedCallback _expireCallback;
		private string _expiredCountLock = "";
		private int _expiredCount = 0;
        private Dictionary<int, Brand> _brands = new Dictionary<int, Brand>();
        private object _brandsLock = new object();

        #region Test properties
        public bool NUnit { get; set; }
        public IMember NUnit_Member { get; set; }
        public IMember NUnit_TargetMember { get; set; }
        public SearchPreferenceCollection NUnit_MemberSearchPreferenceCollection { get; set; }
        #endregion

        #region Events
        public delegate void ReplicationEventHandler(IReplicable replicableObject);
		public event ReplicationEventHandler ReplicationRequested;

		public delegate void ReplicationActionEventHandler(IReplicationAction replicationAction);
		public event ReplicationActionEventHandler ReplicationActionRequested;

		public delegate void SynchronizationEventHandler(string key, Hashtable cacheReferences);
		public event SynchronizationEventHandler SynchronizationRequested;

		public delegate void ListRequestEventHandler(bool cacheHit, Int32 count);
		public event ListRequestEventHandler ListRequested;

		public delegate void ListAddEventHandler();
		public event ListAddEventHandler ListAdded;

		public delegate void ListRemoveEventHandler();
		public event ListRemoveEventHandler ListRemoved;

		public delegate void ListSaveEventHandler();
		public event ListSaveEventHandler ListSaved;
        #endregion

        private IExternalMailSA _externalMailService;

	    public IExternalMailSA ExternalMailService
	    {
	        get
	        {
                if (null == _externalMailService)
                {
                    return ExternalMail.ServiceAdapters.ExternalMailSA.Instance;
                }
	            return _externalMailService;
	        }
	        set { _externalMailService = value; }
	    }

        private static ISettingsSA _settingsService = null;
        public static ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService)
                {
                    return RuntimeSettings.Instance;
                }
                return _settingsService;
            }
            set { _settingsService = value; }
        }

	    private ListBL()
		{
			_cache = Matchnet.Caching.Cache.Instance;
			_expireCallback = new CacheItemRemovedCallback(this.ExpireCallback);
            NUnit = false;
		}

		
		#region lists

	    public ListInternal GetListInternal(string clientHostName,
	        CacheReference cacheReference,
	        Int32 memberID,
	        Int32[] communityIDList)
	    {
            return GetListInternal(clientHostName, cacheReference, memberID, communityIDList, EnableListFetchingInParallel);
        }


        /// <summary>
        /// This method supports fetching from multiple repositories when fetching in Parallel
        /// </summary>
        /// <param name="clientHostName"></param>
        /// <param name="cacheReference"></param>
        /// <param name="memberID"></param>
        /// <param name="communityIDList"></param>
        /// <param name="processInParallel"></param>
        /// <returns></returns>
	    public ListInternal GetListInternal(string clientHostName,
			CacheReference cacheReference,
			Int32 memberID,
			Int32[] communityIDList, bool processInParallel)
		{
			try
			{
				bool cacheHit = true;

				var listInternal = _cache.Get(ListInternal.GetCacheKey(memberID)) as ListInternal;

				if (listInternal == null)
				{
					cacheHit = false;
                    
                    listInternal = processInParallel ? LoadListInternalInParallel(memberID, communityIDList) : loadListInternal(memberID, communityIDList);
                    
                    InsertMemberList(listInternal);
				}

				if (clientHostName != null && cacheReference != null)
				{
					listInternal.ReferenceTracker.Add(clientHostName,
						cacheReference);
				}


    			ListRequested(cacheHit, listInternal.TotalCount);
	
                return listInternal;
			}
			catch (Exception ex)
			{
				throw(new BLException(string.Format("GetListinternal: error occured when retrieving member list: {0}:{1}", ex.Message, ex.StackTrace), ex.InnerException));
			}
		}


	    public bool EnableListFetchingInParallel
        {
            get
            {
                var returnValue = false;

                try
                {
                    Boolean.TryParse(SettingsService.GetSettingFromSingleton(Matchnet.List.ValueObjects.ListServiceConstants.ENABLE_LIST_FETCHING_IN_PARALLEL),
                        out returnValue);
                }
                catch (Exception ex)
                {
                    RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME, "Failed to get ENABLE_LIST_FETCHING_IN_PARALLEL defaulting to false", null);
                }

                return returnValue;
            }
        }


        private ListRepository GetDefaultRepository(int siteId)
        {
            var returnValue = (int)ListRepository.Sql;

            try
            {
                var site = BrandConfigSA.Instance.GetSiteByID(siteId);

                if (site != null && site.Community != null)
                {
                    var communityId = site.Community.CommunityID;
                    Int32.TryParse(SettingsService.GetSettingFromSingleton(Matchnet.List.ValueObjects.ListServiceConstants.LIST_DEFAULT_REPOSITORY, communityId, siteId), out returnValue);
                }
                else
                {
                    RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME, string.Format("GetDefaultRepositoryBySiteId: Failed to get the communityId for site : {0}, trying fetching setting value with NULL_INT", siteId), null);
                    Int32.TryParse(SettingsService.GetSettingFromSingleton(Matchnet.List.ValueObjects.ListServiceConstants.LIST_DEFAULT_REPOSITORY, Constants.NULL_INT, siteId), out returnValue);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME, string.Format("GetDefaultRepositoryBySiteId: Failed to get LIST_DEFAULT_REPOSITORY defaulting to SQL Ex: {0}", ex.Message), null);
            }

            return (ListRepository)returnValue;
        }


        private ListRepository GetDefaultRepositoryByCommunityId(int communityId)
        {
            var returnValue = (int)ListRepository.Sql;

            try
            {
                Int32.TryParse(SettingsService.GetSettingFromSingleton(Matchnet.List.ValueObjects.ListServiceConstants.LIST_DEFAULT_REPOSITORY, communityId), out returnValue);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME, string.Format("GetDefaultRepositoryByCommunityId: Failed to get LIST_DEFAULT_REPOSITORY defaulting to SQL Ex: {0}", ex.Message), null);
            }

            return (ListRepository)returnValue;
        }



        private bool DisableNotifications(int siteId)
        {
            var returnValue = false;

            try
            {
                var site = BrandConfigSA.Instance.GetSiteByID(siteId);

                if (site != null && site.Community != null)
                {
                    var communityId = site.Community.CommunityID;
                    bool.TryParse(SettingsService.GetSettingFromSingleton(Matchnet.List.ValueObjects.ListServiceConstants.LIST_DISABLE_NOTIFICATIONS, communityId, siteId), out returnValue);
                }
                else
                {
                    RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME, string.Format("GetDefaultRepositoryBySiteId: Failed to get the communityId for site : {0}, trying fetching setting value with NULL_INT", siteId), null);
                    bool.TryParse(SettingsService.GetSettingFromSingleton(Matchnet.List.ValueObjects.ListServiceConstants.LIST_DISABLE_NOTIFICATIONS, Constants.NULL_INT, siteId), out returnValue);
                }
               
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME, string.Format("DisableNotificationsBySiteId: Failed to get LIST_SEND_NOTIFICATIONS defaulting to true Ex: {0}", ex.Message), null);
            }

            return returnValue;
        }


        private bool IsMingleCommunity(int communityId)
        {
            var returnValue = false;

            try
            {
                bool.TryParse(SettingsService.GetSettingFromSingleton(Matchnet.List.ValueObjects.ListServiceConstants.LIST_ISMINGLECOMMUNITY, communityId), out returnValue);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME, string.Format("Failed to get LIST_SEND_NOTIFICATIONS defaulting to true Ex: {0}", ex.Message), null);
            }

            return returnValue;
        }


	    private ListStoringBehavior GetStoringBehaviorByCommunity(int communityId)
        {
            var returnValue = (int)ListStoringBehavior.WriteToSql;

            try
            {
                Int32.TryParse(SettingsService.GetSettingFromSingleton(Matchnet.List.ValueObjects.ListServiceConstants.LIST_DEFAULT_STORING_BEHAVIOR, communityId), out returnValue);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME, string.Format("Failed to get LIST_DEFAULT_STORING_BEHAVIOR defaulting to SQL, Exception: {0}", ex.Message), null);
            }

            return (ListStoringBehavior)returnValue;
        }


	    private ListInternal loadListInternal(int memberID, int[] communityIDList)
		{
			var listInternal = new ListInternal(memberID);
            var sw = new Stopwatch();

            //Iterating through all the communities if ALL of them indicate Couchbase then use it, otherwise default back to SQL
            //Override settings will need to be added for all the communities
	        var useNoSql = false;
            
            if(communityIDList.Count()>0)
                useNoSql = communityIDList.All(i => GetDefaultRepositoryByCommunityId(i) != ListRepository.Sql);

	        sw.Start();

			GetHotlists(listInternal, useNoSql); 
			GetMatches(listInternal);
            GetLists(listInternal, communityIDList, useNoSql);

            sw.Stop();

            RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME,
                string.Format("LoadListInternal executed in {0} ms for member {1} :", sw.ElapsedMilliseconds, memberID), null);

			return listInternal;
		}

        private ListInternal LoadListInternalInParallel(int memberID, int[] communityIDList)
        {
            const string methodName = "LoadListInternalInParallel()";

            if (communityIDList == null) throw new ArgumentException("communityIDList", methodName);

            var lists = new ListInternal(memberID);
            var sw = new Stopwatch();

            //Iterating through all the communities if ALL of them indicate Couchbase then use it, otherwise default back to SQL
            //Override settings will need to be added for all the communities
            var useNoSql = false;
            
            if(communityIDList.Count()>0)
                useNoSql = communityIDList.All(i => GetDefaultRepositoryByCommunityId(i) != ListRepository.Sql);

            sw.Start();

            try
            {
                Parallel.Invoke(
                    () => GetHotlists(lists, useNoSql),
                    () => GetMatches(lists),
                    () => GetLists(lists, communityIDList, useNoSql)
                    );
            }
            catch (AggregateException aggregateException)
            {
                var message = string.Format("Bl exception occured in {0}. MemberId:{1}", methodName, memberID);
                // catch exception that bubbles up from Parallel Invoke
                if (aggregateException.InnerExceptions != null && aggregateException.InnerExceptions.Count > 0)
                {
                    var exception = aggregateException.InnerExceptions[0];
                    throw new BLException(message, exception);
                }

                throw new BLException(message);
            }

            lists.Sort(); 
            sw.Stop();

            RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME, string.Format("LoadListInternalInParallel executed in {0} ms for member {1} :", sw.ElapsedMilliseconds, memberID), null);

            return lists;
        }


        private void GetHotlists(ListInternal listInternal, bool useNoSql)
		{
            if (useNoSql)
                GetHotListFromNoSqlRepo(listInternal);
		    else
                ListDBAdapter.Instance.GetHotListFromSqlRepo(listInternal);
		}

        private void GetHotListFromNoSqlRepo(ListInternal listInternal)
        {
            var currentList = ListNoSqlAdapter.Instance.GetListView(listInternal.MemberID);
            
            if (currentList != null && currentList.Any())
            {
                foreach (var currentListItem in currentList)
                {
                    var category = HotListCategory.Default;

                    if (currentListItem.CategoryId > 0)
                    {
                        category = (HotListCategory) currentListItem.CategoryId;
                    }
                    else if (currentListItem.Direction != 1)
                    {
                        const HotListDirection defaultDirection = HotListDirection.OnTheirList;
                        category = ListInternal.MapListCategory((HotListType) currentListItem.ListTypeId, defaultDirection);
                    }

                    if (currentListItem.TeaseId != Constants.NULL_INT)
                    {
                        listInternal.AddListMember(category,
                            currentListItem.CommunityId,
                            currentListItem.TargetMemberId,
                            currentListItem.ActionDate,
                            currentListItem.TeaseId,
                            false);
                    }
                    else if (currentListItem.ListText != Constants.NULL_STRING)
                    {
                        listInternal.AddListMember(category,
                            currentListItem.CommunityId,
                            currentListItem.TargetMemberId,
                            currentListItem.ActionDate,
                            currentListItem.ListText,
                            false);
                    }
                    else
                    {
                        listInternal.AddListMember(category,
                            currentListItem.CommunityId,
                            currentListItem.TargetMemberId,
                            currentListItem.ActionDate,
                            false);
                    }
                }
            }
        }


        /// <summary>
        /// Matches do not get populated by the list service, we will continue fetching it from our SQL repo.
        /// </summary>
        /// <param name="listInternal"></param>
	    private void GetMatches(ListInternal listInternal)
	    {
            ListDBAdapter.Instance.GetMatchesFromSqlRepo(listInternal);
	    }


	    private void GetLists(ListInternal listInternal, Int32[] communityIDList, bool useNoSql)
	    {
            if(useNoSql)
                GetListsFromNoSqlRepo(listInternal, communityIDList);
            else
                ListDBAdapter.Instance.GetListsFromSqlRepo(listInternal, communityIDList);
	    }

        private void GetListsFromNoSqlRepo(ListInternal listInternal, int[] communityIDList)
        {
            foreach (var communityId in communityIDList)
            {
                var listItems = ListNoSqlAdapter.Instance.GetListView(listInternal.MemberID, communityId, (HotListType)0);
                foreach (var listItem in listItems)
                {
                    var siteId = MapSiteId(listItem.SiteId);

                    switch (listItem.ListTypeId)
                    {
                        case ((byte) HotListType.YNM):
                            if (listItem.CommunityId != 0)
                            {
                                listInternal.AddClick(listItem.CommunityId, siteId, listItem.TargetMemberId,
                                    (ClickDirectionType) listItem.Direction, (ClickListType) listItem.ListTypeId,
                                    listItem.ActionDate, false);
                            }
                            break;

                        case ((byte) HotListType.ViewProfile):
                            var viewProfileCategory = ListInternal.MapListCategory(HotListType.ViewProfile,
                                (HotListDirection) listItem.Direction);

                            listInternal.AddListMember(viewProfileCategory, listItem.CommunityId,
                                listItem.TargetMemberId,
                                listItem.ActionDate, false);

                            break;

                        case ((byte) HotListType.ReportAbuse):
                            var reportAbuseCategory = ListInternal.MapListCategory(HotListType.ReportAbuse,
                                (HotListDirection) listItem.Direction);

                            listInternal.AddListMember(reportAbuseCategory, listItem.CommunityId,
                                listItem.TargetMemberId, listItem.ActionDate, false);
                            break;
                    }
                }
            }
        }

	    private static int MapSiteId(int siteId)
	    {
            switch (siteId)
	        {
	            case 1:
	                siteId = 101;
	                break;
	            case 2:
	                siteId = 102;
	                break;
	            case 3:
	                siteId = 103;
	                break;
	            case 12:
	                siteId = 112;
	                break;
	        }
	        
            return siteId;
	    }


		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		public ListCountCollection GetListCounts(Int32 memberID, Int32 communityID, Int32 siteID)
		{
		    var counts = new ListCountCollection();
			var listInternal = GetListInternal(null, null, memberID, new Int32[] {communityID});

			foreach (HotListCategory category in Enum.GetValues(typeof(HotListCategory)))
			{
			    var count = 0;
			    listInternal.GetListMembers(category, communityID, siteID, 1, 1000000, out count);
				counts[category] = count;
			}

			return counts;
		}

        public ArrayList GetListSinceDate(Int32 memberID, Int32 communityID, Int32 siteID, HotListCategory category, DateTime sinceDate)
        {
            var count = 0;
            var listInternal = GetListInternal(null, null, memberID, new Int32[] { communityID });
            var listMembers = listInternal.GetListMembersSinceDate(category, communityID, siteID, 1, 1000000, sinceDate, out count);

            return listMembers;
        }

		public void AddListMember(string clientHostName, HotListCategory category, Int32 communityID, Int32 siteID, Int32 memberID,
			Int32 targetMemberID, DateTime actionDate, string comment, Int32 teaseID, DateTime timeStamp)
		{
			HotListType listType;
			HotListDirection direction;
			ListInternal.MapListTypeDirection(category, out listType, out direction);

		    if (IsMingleCommunity(communityID) && timeStamp != DateTime.MinValue)
		        actionDate = timeStamp;


		    if (memberID != 0 && targetMemberID != 0)
		    {
		        try
		        {
		            if (listType == HotListType.ViewProfile || listType == HotListType.ReportAbuse)
		            {
		                Int32 senderMemberID;

		                if (direction == HotListDirection.OnYourList)
		                {
		                    senderMemberID = memberID;
		                }
		                else
		                {
		                    if (listType == HotListType.ReportAbuse)
		                    {
		                        throw new Exception("Invalid listType/direction combination (" + listType.ToString() + "/" + direction.ToString() + ").");
		                    }
		                    senderMemberID = targetMemberID;
		                    targetMemberID = memberID;
		                }

                        switch (GetStoringBehaviorByCommunity(communityID))
		                {
                            case ListStoringBehavior.WriteToSql:
		                        ListDBAdapter.Instance.SaveYnmList(communityID, siteID, memberID, targetMemberID, actionDate, teaseID, senderMemberID, listType);
		                        break;
                            case ListStoringBehavior.WriteToCouchbase:
                                ListNoSqlAdapter.Instance.Save(communityID, siteID, listType, memberID, targetMemberID, teaseID, actionDate);
                                break;
                            case ListStoringBehavior.WriteToAll:
                                ListDBAdapter.Instance.SaveYnmList(communityID, siteID, memberID, targetMemberID, actionDate, teaseID, senderMemberID, listType);
                                ListNoSqlAdapter.Instance.Save(communityID, siteID, listType, memberID, targetMemberID, teaseID, actionDate);
		                        break;
                            default:
                                ListDBAdapter.Instance.SaveYnmList(communityID, siteID, memberID, targetMemberID, actionDate, teaseID, senderMemberID, listType);
		                        break;
		                }

		                addListMember(category, listType, direction, communityID, siteID, memberID,
		                    targetMemberID != memberID ? targetMemberID : senderMemberID, actionDate, comment,
		                    teaseID, true, clientHostName);
		            }
		            else
		            {
                        switch (GetStoringBehaviorByCommunity(communityID))
		                {
                            case ListStoringBehavior.WriteToSql:
                                ListDBAdapter.Instance.SaveMemberList(category, communityID, memberID, targetMemberID, comment, teaseID, listType, direction);
		                        break;
                            case ListStoringBehavior.WriteToCouchbase:
                                ListNoSqlAdapter.Instance.Save(communityID, category, listType, direction, memberID,
		                        targetMemberID, comment, teaseID, actionDate);
		                        break;
                            case ListStoringBehavior.WriteToAll:
                                ListDBAdapter.Instance.SaveMemberList(category, communityID, memberID, targetMemberID, comment, teaseID, listType, direction);
                                ListNoSqlAdapter.Instance.Save(communityID, category, listType, direction, memberID,
		                        targetMemberID, comment, teaseID, actionDate);
		                        break;
                            default:
		                        ListDBAdapter.Instance.SaveMemberList(category, communityID, memberID, targetMemberID, comment, teaseID, listType, direction);
		                        break;
		                }

		                addListMember(category, listType, direction, communityID, siteID, memberID, targetMemberID, actionDate, comment, teaseID, true, clientHostName);
		            }

		            ListSaved();
		        }
		        catch (Exception ex)
		        {
		            throw (new BLException(
		                "BL error occured when saving list entry. (category: " + ((Int32) category).ToString() + ", " +
		                "communityID: " + communityID.ToString() + ", " +
		                "siteID: " + siteID.ToString() + ", " +
		                "memberID: " + memberID.ToString() + ", " +
		                "targetMemberID: " + targetMemberID.ToString() + ", " +
		                "actionDate: " + actionDate.ToString() + ", " +
		                "comment: " + comment + ", " +
		                "teaseID: " + teaseID.ToString() + ")", ex));
		        }
		    }
		}

	  

	    private void addListMember(HotListCategory category,
			Int32 communityID,
			Int32 siteID,
			Int32 memberID,
			Int32 targetMemberID,
			DateTime actionDate,
			string comment,
			Int32 teaseID,
			bool replicate,
			string clientHostName)
		{
			HotListType listType;
			HotListDirection direction;
			ListInternal.MapListTypeDirection(category, out listType, out direction);

			addListMember(category,
				listType,
				direction,
				communityID,
				siteID,
				memberID,
				targetMemberID,
				actionDate,
				comment,
				teaseID,
				replicate,
				clientHostName);
		}
			

		private void addListMember(HotListCategory category,
			HotListType listType,
			HotListDirection direction,
			Int32 communityID,
			Int32 siteID,
			Int32 memberID,
			Int32 targetMemberID,
			DateTime actionDate,
			string comment,
			Int32 teaseID,
			bool replicate,
			string clientHostName)
		{
			ListInternal listInternal = _cache.Get(ListInternal.GetCacheKey(memberID)) as ListInternal;

			if (listInternal != null)
			{
				switch (listType)
				{
                    case HotListType.Friend:
                        listInternal.AddListMember(category,
                            communityID,
                            targetMemberID,
                            actionDate,
                            comment,
                            false);
                        if (AreNotificationsEnabled(communityID) && replicate == true && category == HotListCategory.Default)
                            recordActivity(memberID, targetMemberID, (int)HotListType.Friend, communityID, siteID, Matchnet.List.ValueObjects.ListServiceConstants.ADD_FAVORITE_ACTION_NAME);
                        break;
					
					case HotListType.Tease:
						listInternal.AddListMember(category,
							communityID,
							targetMemberID,
							actionDate,
							teaseID,
							false);
						break;

				  default:
						listInternal.AddListMember(category,
							communityID,
							targetMemberID,
							actionDate,
							comment,
							false);
						break;
				}
			}

			if (replicate && listInternal != null)
			{
				ReplicationActionRequested(new AddListMemberAction(category,
					communityID,
					siteID,
					memberID,
					targetMemberID,
					actionDate,
					comment,
					teaseID));
			}

			if (listInternal != null)
			{
				SynchronizationRequested(listInternal.GetCacheKey(),
					listInternal.ReferenceTracker.Purge(clientHostName));
			}
		}


		public void RemoveListMember(string clientHostName,
			HotListCategory category,
			Int32 communityID,
			Int32 memberID,
			Int32 targetMemberID)
		{
			HotListType listType;
			HotListDirection direction;


			ListInternal.MapListTypeDirection(category, out listType, out direction);
			try
			{
				if (listType == HotListType.ViewProfile)
				{
                    if (GetDefaultRepositoryByCommunityId(communityID) == ListRepository.Sql)
                        ListDBAdapter.Instance.DeleteListStp(communityID, memberID, targetMemberID, direction);
				    else
                        ListNoSqlAdapter.Instance.Delete(communityID, memberID, targetMemberID, listType, direction, category);
				}
				else
				{
                    if (GetDefaultRepositoryByCommunityId(communityID) == ListRepository.Sql)
				    {
                        ListDBAdapter.Instance.DeleteList(communityID, memberID, targetMemberID, direction, listType);
                        HotListDirection reverseDirection = (direction == HotListDirection.OnYourList
                            ? HotListDirection.OnTheirList
                            : HotListDirection.OnYourList);
                        ListDBAdapter.Instance.DeleteList(communityID, targetMemberID, memberID, reverseDirection, listType);      
				    }
				}

				RemoveListMember(category,
					communityID,
					memberID,
					targetMemberID,
					true,
					clientHostName);


                //AR for HotListCategory.Default
                if (AreNotificationsEnabled(communityID) && category == HotListCategory.Default)
                    recordActivity(memberID, targetMemberID, (int)HotListType.Friend, communityID, Int32.MinValue, Matchnet.List.ValueObjects.ListServiceConstants.DELETE_FAVORITE_ACTION_NAME);

                //map the category for the opposite direction
			    category = ListInternal.MapListCategory(listType, HotListDirection.OnTheirList);
                
                RemoveListMember(category,
                    communityID,
                    targetMemberID,
                    memberID,
                    true,
                    clientHostName);

                if (ListSaved != null)
                    ListSaved();
			}
			catch (Exception ex)
			{
				throw(new BLException("BL error occured when deleting list entry.", ex));

                RollingFileLogger.Instance.LogError(SERVICE_CONSTANT, CLASS_NAME, ex, null);
			}
		}

	    private void RemoveListMember(HotListCategory category,
			Int32 communityID,
			Int32 memberID,
			Int32 targetMemberID,
			bool replicate,
			string clientHostName)
		{
			var listInternal = _cache.Get(ListInternal.GetCacheKey(memberID)) as ListInternal;

	        if (listInternal != null)
	        {
	            listInternal.RemoveListMember(category, communityID, targetMemberID);
	        }

	        if (replicate)
			{
				ReplicationActionRequested(new RemoveListMemberAction(category,
					communityID,
					memberID,
					targetMemberID));
			}

			if (listInternal != null)
			{
				SynchronizationRequested(listInternal.GetCacheKey(),
					listInternal.ReferenceTracker.Purge(clientHostName));
			}
		}

        private void recordActivity(int memberId, int targetMemberId, int actionType, int communityID, int siteID, string actionName)
        {
            var captionParameters = new Dictionary<string, string>
                                            {
                                                {CaptionConstants.ACTION, actionName },
                                                {CaptionConstants.COMMUNITY_ID, communityID.ToString() }
                                            };
            //Calling system is hard coded to Web as per the JIRA request.
            ActivityRecordingProcessorAdapter.Instance.RecordActivity(memberId, targetMemberId, actionType, siteID, CallingSystem.Web, captionParameters);
        }

		public void AddClick(string clientHostName,
			Int32 communityID,
			Int32 siteID,
			Int32 memberID,
			Int32 targetMemberID,
			ClickDirectionType direction,
			ClickListType type,
			DateTime actionDate)
		{
			try
			{
				Int32 senderMemberID;

				if (direction == ClickDirectionType.Sender)
				{
					senderMemberID = memberID;
				}
				else
				{
					senderMemberID = targetMemberID;
					targetMemberID = memberID;
				}

                var savingItem = new ListItem()
                {
                    CommunityId = communityID,
                    SiteId = siteID,
                    ListTypeId = (int)HotListType.YNM,
                    MemberId = memberID,
                    TargetMemberId = targetMemberID,
                    ClickType = (int)type,
                    ActionDate = actionDate,

                };

                switch (GetStoringBehaviorByCommunity(communityID))
			    {
			        case ListStoringBehavior.WriteToSql:
			            ListDBAdapter.Instance.SaveYNMListDB(savingItem.CommunityId, savingItem.SiteId, savingItem.MemberId, savingItem.TargetMemberId, (ClickListType)savingItem.ClickType, savingItem.ActionDate, senderMemberID);
	    		        break;
                    case ListStoringBehavior.WriteToCouchbase:
			            ListNoSqlAdapter.Instance.Save(savingItem);
		    	        break;
                    case ListStoringBehavior.WriteToAll:
			            ListDBAdapter.Instance.SaveYNMListDB(savingItem.CommunityId, savingItem.SiteId, savingItem.MemberId, savingItem.TargetMemberId, (ClickListType)savingItem.ClickType, savingItem.ActionDate, senderMemberID);
                        ListNoSqlAdapter.Instance.Save(savingItem);
			        break;
			    }

			    if (AreNotificationsEnabled(communityID) && direction == ClickDirectionType.Recipient && type == ClickListType.Yes)
				{
					Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
					Int32 frequency = member.GetAttributeInt(communityID,
						Constants.NULL_INT,
						Constants.NULL_INT,
						"GotAClickEmailPeriod",
						0);

					if (frequency > 0 && member.IsEligibleForEmail(communityID))
					{
						Command commandViral = new Command("mnAlert", "dbo.up_ViralMailSchedule_Save", 0);
						commandViral.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
						commandViral.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
						commandViral.AddParameter("@Frequency", SqlDbType.Int, ParameterDirection.Input, frequency);
						commandViral.AddParameter("@LastYesVoteDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
						commandViral.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
						Client.Instance.ExecuteAsyncWrite(commandViral);
					}
				}

				addClick(communityID,
					siteID,
					memberID,
					senderMemberID,
					targetMemberID,
					direction,
					type,
					actionDate,
					true,
					clientHostName);
			}
			catch (Exception ex)
			{
				throw(new BLException("BL error occured when saving click vote.\r\n" +
					"communityID = " + communityID.ToString() + "\r\n" + 
					"memberID = " + memberID.ToString() + "\r\n" + 
					"targetMemberID = " + targetMemberID.ToString() + "\r\n" + 
					"siteID = " + siteID.ToString() + "\r\n" + 
					"direction = " + direction.ToString() + "\r\n" + 
					"type = " + type.ToString() + "\r\n" + 
					"dateTime = " + actionDate.ToString(), ex));
			}

            if (ListSaved != null)
                ListSaved();
		}


	    private void addClick(Int32 communityID,
			Int32 siteID,
			Int32 memberID,
			Int32 senderMemberID,
			Int32 targetMemberID,
			ClickDirectionType direction,
			ClickListType type,
			DateTime actionDate,
			bool replicate,
			string clientHostName)
		{
			ListInternal listInternal = _cache.Get(ListInternal.GetCacheKey(memberID)) as ListInternal;

			if (listInternal != null)
			{
				listInternal.AddClick(communityID,
					siteID,
					memberID == targetMemberID ? senderMemberID : targetMemberID,
					direction,
					type,
					actionDate,
					false);
			}

			if (replicate)
			{
				ReplicationActionRequested(new AddClickAction(communityID,
					siteID,
					memberID,
					senderMemberID,
					targetMemberID,
					direction,
					type,
					actionDate));
			}

			if (listInternal != null)
			{
				SynchronizationRequested(listInternal.GetCacheKey(),
					listInternal.ReferenceTracker.Purge(clientHostName));
			}
		}


		private void InsertMemberList(ListInternal listInternal)
		{
			InsertMemberList(listInternal, true);
		}


		private void InsertMemberList(ListInternal listInternal, bool replicate)
		{
			listInternal.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("LISTSVC_CACHE_TTL_SVC"));
			_cache.Insert(listInternal, _expireCallback);
			
            ListAdded();

            if (replicate)
			{
				ReplicationRequested(listInternal);
			}		
		}

		#endregion

		#region categories
		public StandardCategoryCollection GetStandardListCategories(Int32 siteID)
		{
			StandardCategoryCollection standardCategories = _cache.Get(StandardCategoryCollection.GetCacheKey(siteID)) as StandardCategoryCollection;

			if (standardCategories == null)
			{
				standardCategories = new StandardCategoryCollection(siteID);
				Command command = new Command("mnSystem", "dbo.up_ListCategory_List", 0);
				command.AddParameter("@SiteID", SqlDbType.VarChar, ParameterDirection.Input, siteID);
				DataTable dt = Client.Instance.ExecuteDataTable(command);

				for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
				{
					DataRow row = dt.Rows[rowNum];
					standardCategories.Add(new StandardCategory(Convert.ToInt32(row["ListCategoryID"]), row["ResourceKey"].ToString()));
				}

				_cache.Insert(standardCategories);
			}
		
			return standardCategories;
		}

		
		//temp
		public CustomCategoryCollection GetCustomListCategories(string clientHostName,
			CacheReference cacheReference,
			Int32 memberID,
			Int32 communityID)
		{
			try
			{
				CustomCategoryCollection categories = _cache.Get(CustomCategoryCollection.GetCacheKey(memberID, communityID)) as CustomCategoryCollection;

				if (categories == null)
				{
					SqlDataReader dataReader = null;

					try
					{
						Command command = new Command("mnListMember", "dbo.up_Category_List", memberID);
						command.AddParameter("@MemberID", SqlDbType.VarChar, ParameterDirection.Input, memberID);
						command.AddParameter("@GroupID", SqlDbType.VarChar, ParameterDirection.Input, communityID);
						dataReader = Client.Instance.ExecuteReader(command);

						categories = new CustomCategoryCollection(memberID, communityID);

						bool colLookupDone = false;
						Int32 ordinalCategoryID = Constants.NULL_INT;
						Int32 ordinalDescription = Constants.NULL_INT;

						while (dataReader.Read())
						{
							if (!colLookupDone)
							{
								ordinalCategoryID = dataReader.GetOrdinal("CategoryID");
								ordinalDescription = dataReader.GetOrdinal("Description");

								colLookupDone = true;
							}

							categories.Add(new CustomCategory(dataReader.GetInt32(ordinalCategoryID), dataReader.GetString(ordinalDescription)));
						}
					}
					finally
					{
						if (dataReader != null)
						{
							dataReader.Close();
						}
					}
						
					InsertCustomCategoryCollection(categories);
				}

				if (clientHostName != null && cacheReference != null)
				{
					categories.ReferenceTracker.Add(clientHostName,
						cacheReference);
				}

				return categories;
			}
			catch (Exception ex)
			{
				throw new BLException("Error retrieving custom list categories.", ex);
			}
		}



		public void SaveListCategory(string clientHostName,
			Int32 categoryID,
			Int32 communityID,
			Int32 memberID,
			string description)
		{
			try
			{
				CustomCategoryCollection categories = GetCustomListCategories(clientHostName,
					null,
					memberID,
					communityID);

				Command command = new Command("mnListMember", "dbo.up_Category_Save", memberID);
				command.AddParameter("@CategoryID", SqlDbType.Int, ParameterDirection.Input, categoryID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, communityID);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@Description", SqlDbType.NVarChar, ParameterDirection.Input, description);
				Client.Instance.ExecuteAsyncWrite(command);

				categories.Add(new CustomCategory(categoryID, description));

				ReplicationRequested(categories);

				SynchronizationRequested(categories.GetCacheKey(),
					categories.ReferenceTracker.Purge(Constants.NULL_STRING));
			}
			catch (Exception ex)
			{
				throw(new BLException("BL error occured when creating category.", ex));
			}
		}
		

		public void DeleteListCategory(string clientHostName,
			Int32 categoryID,
			Int32 communityID,
			Int32 memberID)
		{
			try
			{
				CustomCategoryCollection categories = GetCustomListCategories(clientHostName,
					null,
					memberID,
					communityID);

				Command command = new Command("mnListMember", "dbo.up_Category_Delete", memberID);
				command.AddParameter("@CategoryID", SqlDbType.Int, ParameterDirection.Input, categoryID);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, communityID);
				Client.Instance.ExecuteAsyncWrite(command);

				categories.Remove(categoryID);

				ReplicationRequested(categories);

				SynchronizationRequested(categories.GetCacheKey(),
					categories.ReferenceTracker.Purge(Constants.NULL_STRING));
			}
			catch (Exception ex)
			{
				throw(new BLException("BL error occured while deleting category.", ex));
			}
		}

		
		private void InsertCustomCategoryCollection(CustomCategoryCollection customCategoryCollection)
		{
			InsertCustomCategoryCollection(customCategoryCollection, true);
		}

			
		private void InsertCustomCategoryCollection(CustomCategoryCollection customCategoryCollection, bool replicate)
		{
			customCategoryCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("LISTSVC_CACHE_TTL_SVC"));
			_cache.Insert(customCategoryCollection, _expireCallback);

			if (replicate)
			{
				ReplicationRequested(customCategoryCollection);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="targetMemberID"></param>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		public ClickMask GetClickMask(Int32 memberID, Int32 targetMemberID, Int32 communityID, Int32 siteID)
		{
            ListInternal listInternal = GetListInternal(null, null, memberID, new Int32[] { communityID });
		    return ListDBAdapter.Instance.GetClickMask(listInternal, targetMemberID, communityID, siteID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <returns></returns>
		public ListInternal GetListInternalMutualYes(Int32 memberID, Int32 communityID)
		{
		    return GetDefaultRepositoryByCommunityId(communityID) == ListRepository.Sql ? GetListInternalMutualYesFromDB(memberID, communityID) : GetListInternalMutualYesFromNoSQL(memberID, communityID);
		}

	    public ListInternal GetListInternalMutualYesFromDB(int memberID, int communityID)
	    {
            ListInternal listInternal = GetListInternal(null, null, memberID, new Int32[] { communityID });
	        return ListDBAdapter.Instance.GetListInternalMutualYesFromDB(listInternal, communityID);
	    }

	    //TODO: Verify with Matt this functionality
        public ListInternal GetListInternalMutualYesFromNoSQL(int memberId, int communityId)
        {
            var listInternal = GetListInternal(null, null, memberId, new Int32[] { communityId });

            var ynmListItems = ListNoSqlAdapter.Instance.GetListView(memberId, communityId, HotListType.YNM);
            foreach (var item in ynmListItems)
                PopulateListInternalFromListItems(item, listInternal);

            var viewProfileListItems = ListNoSqlAdapter.Instance.GetListView(memberId, communityId, HotListType.ViewProfile);
            foreach (var item in viewProfileListItems)
                PopulateListInternalFromListItems(item, listInternal);

            var reportAbuseListItems = ListNoSqlAdapter.Instance.GetListView(memberId, communityId, HotListType.ReportAbuse);
            foreach (var item in reportAbuseListItems)
                PopulateListInternalFromListItems(item, listInternal);

            return listInternal;
        }

	    private static void PopulateListInternalFromListItems(ListItem item, ListInternal listInternal)
	    {
	        item.SiteId = MapSiteId(item.SiteId);
	        switch (item.ListTypeId)
	        {
	            case ((int) HotListType.YNM):
	                listInternal.AddClick(item.CommunityId, item.SiteId, item.TargetMemberId,
	                    (ClickDirectionType) item.Direction, (ClickListType) item.ClickType, item.ActionDate, false);
	                break;
	            case ((int) HotListType.ViewProfile):
	                var category = ListInternal.MapListCategory(HotListType.ViewProfile, (HotListDirection) item.Direction);
	                listInternal.AddListMember(category, item.CommunityId, item.TargetMemberId, item.ActionDate, false);
	                break;
	            case ((int) HotListType.ReportAbuse):
	                var cat = ListInternal.MapListCategory(HotListType.ReportAbuse, HotListDirection.OnYourList);
	                listInternal.AddListMember(cat, item.CommunityId, item.TargetMemberId, item.ActionDate, false);
	                break;
	        }
	    }

	    #endregion

		#region Communication with ExternalMail
		
		public void SendHotListNotification(Matchnet.List.ValueObjects.HotListCategory cat, int targetMemberID, int memberID, int brandID)
		{
			try
			{
				switch(cat)
				{
					case Matchnet.List.ValueObjects.HotListCategory.Default:
						ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendHotListedNotification(targetMemberID, memberID, brandID);
						break;
					case Matchnet.List.ValueObjects.HotListCategory.MutualYes:
                        //note: member is the person being notified that targetmember made a match
						ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendMutualYesNotification(memberID, targetMemberID, brandID);

                        // Send push notification
                        PushNotificationsProcessorAdapter.Instance.SendSystemEvent(new MutualYesEvent()
                        {
                            BrandId = brandID,
                            EventCategory = SystemEventCategory.MemberGenerated,
                            EventType = SystemEventType.BothSaidYes,
                            MemberId = targetMemberID,
                            TargetMemberId = memberID, //send notification to member
                            NotificationTypeId = PushNotificationTypeID.MutualYes,
                            AppGroupId = AppGroupID.JDateAppGroup
                        });


						break;
				}
			}
			catch(Exception ex)
			{
				throw new BLException("Failed to send HotList notification ListType: " + cat.ToString(), ex);
			}
		}

		public void SendMessmoMessage(int communityID,
			int targetBrandID,
			int sendingBrandID,
			int targetMemberID,
			int memberID,
			Matchnet.List.ValueObjects.HotListCategory cat)
		{
			try
			{
				if(ExternalMail.ServiceAdapters.ExternalMailSA.Instance.IsMessmoMessageRequired(targetMemberID, communityID))
				{
					ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendMessmoMessageFromList(communityID, targetBrandID, sendingBrandID, targetMemberID, memberID, cat);
				}
			}
			catch(Exception ex)
			{
				throw new BLException("Failed to send Messmo Message", ex);
			}
		}

        public ProfileViewedEmailAlertResult SendProfileViewedAlert(HotListCategory category,
			Int32 communityID,
			Int32 siteID,
			Int32 memberID,
			Int32 targetMemberID)
        {
            ProfileViewedEmailAlertResult alertResult = new ProfileViewedEmailAlertResult();
            alertResult.EmailAlertStatus = ProfileViewedEmailAlertStatus.None;
            bool sentAlert = false;
            try
            {
                RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME,
                string.Format("Starting SendProfileViewedAlert - category {0}, community: {1}, siteID: {2}, memberID: {3}, targetMemberID: {4}",
                category,
                communityID,
                siteID,
                memberID,
                targetMemberID),
                null);
                
                if (category == HotListCategory.WhoViewedYourProfile && memberID != targetMemberID && memberID > 0 && targetMemberID > 0)
                {
                    Brand brand = GetBrandBySite(siteID);
                    if (brand != null)
                    {
                        bool enableProfileViewedAlerts = IsProfileViewedEmailEnabled(communityID, siteID);
                        if (enableProfileViewedAlerts)
                        {                            
                            IMember member = NUnit ? NUnit_Member : MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                            if (member != null)
                            {
                                //check for opt-in
                                int maskEmailAlert = member.GetAttributeInt(brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, 0);
                                if ((maskEmailAlert & (int)EmailAlertMask.ProfileViewedAlertOptOut) != (int)EmailAlertMask.ProfileViewedAlertOptOut)
                                {
                                    bool triggerProfileViewedEmail = false;
                                    int minHoursInterval = GetMinIntervalHours(brand);
                                    int minRegistrationDays = GetMinRegistrationDays(brand);
                                    string previousTargetMemberIDs = "";
                                    string[] previousTargetMemberIDList = null;
                                    DateTime lastProfileViewedEmailDate = DateTime.MinValue;

                                    //"memberID" - profile viewed
                                    //check last login
                                    DateTime lastLoginDate = NUnit ? member.GetAttributeDate(brand, "BrandLastLogonDate", DateTime.MinValue) : (member as Matchnet.Member.ServiceAdapters.Member).GetLastLogonDate(communityID);
                                    int lastLoginDays = (int)Math.Floor(DateTime.Now.Subtract(lastLoginDate).TotalDays);
                                    int minLoginDays = GetMinLoginDays(brand);
                                    int maxLoginDays = GetMaxLoginDays(brand);
                                    if (lastLoginDays >= minLoginDays && lastLoginDays <= maxLoginDays)
                                    {
                                        triggerProfileViewedEmail = true;

                                        //check registered not within past 14 days (setting)
                                        DateTime registrationDate = member.GetAttributeDate(brand, "BrandInsertDate", DateTime.MinValue);
                                        int registrationDays = 0;
                                        if (registrationDate > DateTime.MinValue)
                                        {
                                            registrationDays = (int)Math.Floor(DateTime.Now.Subtract(registrationDate).TotalDays);
                                        }
                                        if (registrationDays < minRegistrationDays)
                                        {
                                            triggerProfileViewedEmail = false;
                                            alertResult.EmailAlertStatus = ProfileViewedEmailAlertStatus.FailedRegistration;
                                            RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME,
                                                "SendProfileViewedAlert FAILED - Member " + memberID.ToString() + " registered only " + registrationDays.ToString() + " days ago.  Needs to be at least " + minRegistrationDays.ToString() + " days",
                                                null);
                                        }

                                        if (triggerProfileViewedEmail)
                                        {
                                            //check last triggered alert email sent (attribute)
                                            lastProfileViewedEmailDate = member.GetAttributeDate(brand, "ProfileViewedAlertLastSentDate", DateTime.MinValue);
                                            
                                            int lastSentHours = minHoursInterval;
                                            if (lastProfileViewedEmailDate > DateTime.MinValue)
                                            {
                                                lastSentHours = (int)Math.Floor(DateTime.Now.Subtract(lastProfileViewedEmailDate).TotalHours);
                                            }

                                            if (lastSentHours >= minHoursInterval)
                                            {
                                                //check not cause of email trigger in past 5 emails (attribute)
                                                previousTargetMemberIDs = member.GetAttributeText(brand, "ProfileViewedAlertViewerMemberIDList");                                                
                                                if (!string.IsNullOrEmpty(previousTargetMemberIDs))
                                                {
                                                    previousTargetMemberIDList = previousTargetMemberIDs.Split(new char[] { ',' });
                                                    foreach (string s in previousTargetMemberIDList)
                                                    {
                                                        if (s == targetMemberID.ToString())
                                                        {
                                                            triggerProfileViewedEmail = false;
                                                            alertResult.EmailAlertStatus = ProfileViewedEmailAlertStatus.FailedMemberAlreadyTriggeredRecently;
                                                            RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME,
                                                                "SendProfileViewedAlert FAILED - Target member " + targetMemberID.ToString() + " already part of previous sent list. " + previousTargetMemberIDs,
                                                                null);
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                triggerProfileViewedEmail = false;
                                                alertResult.EmailAlertStatus = ProfileViewedEmailAlertStatus.FailedMinIntervalPerEmail;
                                                RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME,
                                                    "SendProfileViewedAlert FAILED - Member " + memberID.ToString() + " last sent profiled viewed alert email " + lastSentHours + " hours ago, no more than 1 within " + minHoursInterval.ToString() + " hours",
                                                    null);
                                            }
                                        }

                                        if (triggerProfileViewedEmail)
                                        {
                                            //"targetMemberID" - profile viewer
                                            IMember targetMember = NUnit ? NUnit_TargetMember : MemberSA.Instance.GetMember(targetMemberID, MemberLoadFlags.None);

                                            //check profile not hidden
                                            //note: self-suspend and suspend are already done in ExternalMail
                                            if (triggerProfileViewedEmail)
                                            {
                                                int hideMask = targetMember.GetAttributeInt(brand, "HideMask");
                                                if ((hideMask & 1) == 1)
                                                {
                                                    triggerProfileViewedEmail = false;
                                                    alertResult.EmailAlertStatus = ProfileViewedEmailAlertStatus.FailedHidden;
                                                    RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME,
                                                        "SendProfileViewedAlert FAILED - Target member " + targetMemberID.ToString() + " has profile set to hidden. HIDEMASK",
                                                        null);
                                                }
                                            }

                                            //check fraud
                                            if (triggerProfileViewedEmail)
                                            {
                                                int ftaApproved = targetMember.GetAttributeInt(brand, "FTAApproved");
                                                if (ftaApproved != 1)
                                                {
                                                    triggerProfileViewedEmail = false;
                                                    alertResult.EmailAlertStatus = ProfileViewedEmailAlertStatus.FailedFraud;
                                                    RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME,
                                                        "SendProfileViewedAlert FAILED - Target member " + targetMemberID.ToString() + " did not pass fraud check. FTAApproved.",
                                                        null);
                                                }
                                            }

                                            SearchPreferenceCollection memberSearchPreferenceCollection = null;
                                            if (triggerProfileViewedEmail)
                                            {
                                                memberSearchPreferenceCollection = NUnit ? NUnit_MemberSearchPreferenceCollection : SearchPreferencesSA.Instance.GetSearchPreferences(memberID, communityID);
                                            }

                                            //check match - gender
                                            if (triggerProfileViewedEmail)
                                            {
                                                int targetGenderMask = targetMember.GetAttributeInt(brand, "gendermask", 0);
                                                if (memberSearchPreferenceCollection != null && !string.IsNullOrEmpty(memberSearchPreferenceCollection["gendermask"]))
                                                {
                                                    int memberGenderMaskPref = Convert.ToInt32(memberSearchPreferenceCollection["gendermask"]);
                                                    if (targetGenderMask != memberGenderMaskPref)
                                                    {
                                                        triggerProfileViewedEmail = false;
                                                        alertResult.EmailAlertStatus = ProfileViewedEmailAlertStatus.FailedGenderMatch;
                                                        RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME,
                                                            "SendProfileViewedAlert FAILED - Target member " + targetMemberID.ToString() + " did not pass gender matching check. Target gender did not match member's search preference. Member search gendermask " + memberGenderMaskPref.ToString() + ", Target gendermask " + targetGenderMask.ToString(),
                                                            null);
                                                    }
                                                }
                                            }

                                            //check match - religion (e.g. "any" or is jewish)
                                            if (triggerProfileViewedEmail)
                                            {
                                                string religionAttribute = "religion";

                                                //jdate, cupid, jewishmingle
                                                if (communityID == 3 || communityID == 10 || siteID == 9171)
                                                {
                                                    religionAttribute = "jdatereligion";
                                                }

                                                int targetReligion = 0;
                                                int memberReligionMaskPref = 0;
                                                targetReligion = targetMember.GetAttributeInt(brand, religionAttribute, 0);
                                                if (memberSearchPreferenceCollection != null && !string.IsNullOrEmpty(memberSearchPreferenceCollection["jdatereligion"]))
                                                {
                                                    memberReligionMaskPref = Convert.ToInt32(memberSearchPreferenceCollection[religionAttribute]);
                                                }

                                                if (targetReligion == 0 || (memberReligionMaskPref != 0 && ((memberReligionMaskPref & targetReligion) != targetReligion)))
                                                {
                                                    triggerProfileViewedEmail = false;
                                                    alertResult.EmailAlertStatus = ProfileViewedEmailAlertStatus.FailedReligionMatch;
                                                    RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME,
                                                        "SendProfileViewedAlert FAILED - Target member " + targetMemberID.ToString() + " did not pass religion matching check. Target religion did not match member's search preference. Member search religion " + memberReligionMaskPref.ToString() + ", Target religion " + targetReligion.ToString(),
                                                        null);
                                                }
                                            }

                                            //check list count since last triggered email greater than 0
                                            int numberOfMembersWhoViewedSinceLastEmail = 0;
                                            if (triggerProfileViewedEmail && !NUnit)
                                            {
                                                DateTime sinceDate = DateTime.Now.AddHours(-minHoursInterval);
                                                if (lastProfileViewedEmailDate > sinceDate)
                                                {
                                                    sinceDate = lastProfileViewedEmailDate;
                                                }
                                                ArrayList listSinceDate = GetListSinceDate(memberID, communityID, siteID, category, sinceDate);
                                                if (null != listSinceDate)
                                                {
                                                    numberOfMembersWhoViewedSinceLastEmail = listSinceDate.Count;
                                                }

                                                if (numberOfMembersWhoViewedSinceLastEmail < 1)
                                                {
                                                    triggerProfileViewedEmail = false;
                                                    alertResult.EmailAlertStatus = ProfileViewedEmailAlertStatus.FailedListCount;
                                                    RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME,
                                                        "SendProfileViewedAlert FAILED - Member " + memberID.ToString() + " has not been viewed by another member since last viewed you email sent.",
                                                        null);
                                                }
                                            }

                                            if (triggerProfileViewedEmail)
                                            {
                                                //trigger email
                                                sentAlert = true;

                                                if (!NUnit)
                                                {
                                                    sentAlert = ExternalMailService.SendViewedYouEmail(memberID, targetMemberID, numberOfMembersWhoViewedSinceLastEmail, brand.BrandID);
                                                }

                                                if (sentAlert)
                                                {
                                                    alertResult.EmailAlertStatus = ProfileViewedEmailAlertStatus.Success;

                                                    //update target member in past emails
                                                    string newViewerMemberIDList = "";
                                                    if (string.IsNullOrEmpty(previousTargetMemberIDs))
                                                    {
                                                        newViewerMemberIDList = targetMemberID.ToString();
                                                    }
                                                    else if (previousTargetMemberIDList.Length < 5)
                                                    {
                                                        newViewerMemberIDList = previousTargetMemberIDs + "," + targetMemberID.ToString();
                                                    }
                                                    else
                                                    {
                                                        newViewerMemberIDList = previousTargetMemberIDs.Substring(previousTargetMemberIDs.IndexOf(",") + 1) + "," + targetMemberID.ToString();
                                                    }
                                                    member.SetAttributeText(brand, "ProfileViewedAlertViewerMemberIDList", newViewerMemberIDList, Member.ValueObjects.TextStatusType.Auto);

                                                    //update last triggered alert email sent date
                                                    member.SetAttributeDate(brand, "ProfileViewedAlertLastSentDate", DateTime.Now);
                                                    if (!NUnit)
                                                    {
                                                        MemberSA.Instance.SaveMember(member);
                                                    }

                                                    RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME,
                                                        "SendProfileViewedAlert Success - Member " + memberID.ToString() + ", Target member " + targetMemberID.ToString() + ", Site " + siteID.ToString() + ", ProfileViewedAlertViewerMemberIDList " + newViewerMemberIDList,
                                                        null);
                                                }
                                                else
                                                {
                                                    alertResult.EmailAlertStatus = ProfileViewedEmailAlertStatus.FailedExternalMail;
                                                    RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME,
                                                        "SendProfileViewedAlert FAILED - Member " + memberID.ToString() + ", Target member " + targetMemberID.ToString() + ", Site " + siteID.ToString() + ". External mail service returned false.",
                                                        null);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        alertResult.EmailAlertStatus = ProfileViewedEmailAlertStatus.FailedLastLogin;
                                        RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME,
                                        "SendProfileViewedAlert FAILED - Member " + memberID.ToString() + " last log in " + lastLoginDays.ToString() + " days ago (" + lastLoginDate.ToString() + "), not within range of " + minLoginDays.ToString() + " to " + maxLoginDays.ToString() + " days",
                                        null);
                                    }
                                }
                                else
                                {
                                    alertResult.EmailAlertStatus = ProfileViewedEmailAlertStatus.FailedOptIn;
                                    RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME,
                                        "SendProfileViewedAlert FAILED - Member " + memberID.ToString() + " not opted in.  EmailAlertMask value " + maskEmailAlert.ToString(),
                                        null);
                                }
                            }
                        }
                    }
                }
                else
                {
                    alertResult.EmailAlertStatus = ProfileViewedEmailAlertStatus.FailedWhoViewedYourProfileCategory;
                    RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME,
                                        "SendProfileViewedAlert FAILED - Not WhoViewedYourProfile category",
                                        null);
                }

            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME,
                    "SendProfileViewedAlert ERROR - Member " + memberID.ToString() + ". Exception message - " + ex.Message,
                    null);
                throw new BLException("Failed to send Profile Viewed Alert email" + ex.Message, ex);
            }

            return alertResult;
        }

		#endregion
		
		private void ExpireCallback(string key, object value, CacheItemRemovedReason callbackreason) 
		{
			switch (value.GetType().Name)
			{
				case "ListInternal":
					ListRemoved();
					ListInternal listInternal = value as ListInternal;
					SynchronizationRequested(listInternal.GetCacheKey(),
						listInternal.ReferenceTracker.Purge(Constants.NULL_STRING));
					IncrementExpirationCounter();
					break;

				case "CustomCategoryCollection":
					CustomCategoryCollection customCategoryCollection = value as CustomCategoryCollection;
					SynchronizationRequested(customCategoryCollection.GetCacheKey(),
						customCategoryCollection.ReferenceTracker.Purge(Constants.NULL_STRING));
					IncrementExpirationCounter();
					break;
			}
		}


		private void IncrementExpirationCounter()
		{
			lock (_expiredCountLock)
			{
				_expiredCount++;

                if (_expiredCount > Matchnet.List.ValueObjects.ListServiceConstants.GC_THRESHOLD)
				{
					_expiredCount = 0;
					GC.Collect();
				}
			}
		}


		public void CacheReplicatedObject(IReplicable replicableObject)
		{
			switch (replicableObject.GetType().Name)
			{
				case "ListInternal":
					InsertMemberList((ListInternal)replicableObject, false);
					break;

				case "CustomCategoryCollection":
					InsertCustomCategoryCollection((CustomCategoryCollection)replicableObject, false);
					break;

				case "Quotas":
					QuotaBL.Instance.CacheReplicatedObject(replicableObject);
					break;

				default:
					throw new Exception("invalid replication object type received (" + replicableObject.GetType().ToString() + ")");
			}
		}


		public void PlayReplicationAction(IReplicationAction replicationAction)
		{
			switch (replicationAction.GetType().Name)
			{
				case "AddClickAction":
					AddClickAction addClickAction = replicationAction as AddClickAction;
					addClick(addClickAction.CommunityID,
						addClickAction.SiteID,
						addClickAction.MemberID,
						addClickAction.SenderMemberID,
						addClickAction.TargetMemberID,
						addClickAction.Direction,
						addClickAction.Type,
						addClickAction.ActionDate,
						false,
						null);
					break;

				case "AddListMemberAction":
					AddListMemberAction addListMemberAction = replicationAction as AddListMemberAction;
					addListMember(addListMemberAction.Category,
						addListMemberAction.CommunityID,
						addListMemberAction.SiteID,
						addListMemberAction.MemberID,
						addListMemberAction.TargetMemberID,
						addListMemberAction.ActionDate,
						addListMemberAction.Comment,
						addListMemberAction.TeaseID,
						false,
						null);
					break;

				case "RemoveListMemberAction":
					RemoveListMemberAction removeListMemberAction = replicationAction as RemoveListMemberAction;
					RemoveListMember(removeListMemberAction.Category,
						removeListMemberAction.CommunityID,
						removeListMemberAction.MemberID,
						removeListMemberAction.TargetMemberID,
						false,
						null);
					break;
			}
		}

        private Brand GetBrandBySite(int siteID)
        {
            Brand brand = null;
            if (!_brands.ContainsKey(siteID))
            {
                lock (_brandsLock)
                {
                    if (!_brands.ContainsKey(siteID))
                    {
                        Brands brands = BrandConfigSA.Instance.GetBrandsBySite(siteID);

                        //default brand should have the smallest brandID
                        Brand defaultBrand = null;
                        foreach (Brand b in brands)
                        {
                            if (defaultBrand == null)
                            {
                                defaultBrand = b;
                            }
                            else if (b.BrandID < defaultBrand.BrandID)
                            {
                                defaultBrand = b;
                            }
                        }
                        _brands.Add(siteID, defaultBrand);
                    }
                }
            }

            brand = _brands[siteID];

            return brand;
        }

        #region Settings related
        public Int32 MaxYNMVoteCount
		{
			get
			{
				return Conversion.CInt(SettingsService.GetSettingFromSingleton("LISTSVC_MAX_YNM_VOTE_COUNT"));
			}
		}

        private bool IsProfileViewedEmailEnabled(int communityID, int siteID)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = SettingsService.GetSettingFromSingleton("ENABLE_VIEWED_YOUR_PROFILE_EMAIL", communityID, siteID, Constants.NULL_INT);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        private int GetMinLoginDays(Brand brand)
        {
            int value = 0;
            try
            {
                value = Convert.ToInt32(SettingsService.GetSettingFromSingleton("PROFILE_VIEWED_ALERT_LAST_LOGIN_MIN_DAYS", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
            }
            catch (Exception ex)
            {
                //setting missing
                value = 2;
            }

            return value;

        }

        private int GetMaxLoginDays(Brand brand)
        {
            int value = 0;
            try
            {
                value = Convert.ToInt32(SettingsService.GetSettingFromSingleton("PROFILE_VIEWED_ALERT_LAST_LOGIN_MAX_DAYS", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
            }
            catch (Exception ex)
            {
                //setting missing
                value = 2;
            }

            return value;

        }

        private int GetMinIntervalHours(Brand brand)
        {
            int value = 0;
            try
            {
                value = Convert.ToInt32(SettingsService.GetSettingFromSingleton("PROFILE_VIEWED_ALERT_MIN_HOURS_INTERVAL", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
            }
            catch (Exception ex)
            {
                //setting missing
                value = 2;
            }

            return value;

        }

        private int GetMinRegistrationDays(Brand brand)
        {
            int value = 0;
            try
            {
                value = Convert.ToInt32(SettingsService.GetSettingFromSingleton("PROFILE_VIEWED_ALERT_VIEWER_MIN_REGISTRATION_DAYS", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
            }
            catch (Exception ex)
            {
                //setting missing
                value = 2;
            }

            return value;

        }

        #endregion




	    /// <summary>
        /// Saves the viralMaskStatus to DB
        /// </summary>
        /// <param name="clientHostName"></param>
        /// <param name="communityId"></param>
        /// <param name="memberId"></param>
        /// <param name="targetMemberId"></param>
        /// <param name="alertEmailSent"></param>
        /// <param name="clickEmailSent"></param>
        public void SaveViralStatus(string clientHostName, int communityId, int memberId, int targetMemberId, bool alertEmailSent, bool clickEmailSent)
        {
            var viralMaskStatus = 0;

            if (alertEmailSent)
                viralMaskStatus = viralMaskStatus | 2;
            if (clickEmailSent)
                viralMaskStatus = viralMaskStatus | 4;

            ListDBAdapter.Instance.SaveViralStatus(memberId, targetMemberId, communityId, viralMaskStatus);
        }

        bool AreNotificationsEnabled(int communityId)
        {
            var returnValue = true;
            try
            {
                Boolean.TryParse(RuntimeSettings.Instance.GetSettingFromSingleton(Matchnet.List.ValueObjects.ListServiceConstants.LIST_NOTIFICATIONS_ENABLED, communityId), out returnValue);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME, "Failed to get LIST_NOTIFICATIONS_ENABLED setting." + ex.Message, null);
            }
            return returnValue;
        }
    }
}
