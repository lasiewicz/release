using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.Caching;

using Matchnet;
using Matchnet.Data;
using Matchnet.Data.Exceptions;
using Matchnet.Exceptions;
using Matchnet.List.ValueObjects.Quotas;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Content.ValueObjects.Quotas;


namespace Matchnet.List.BusinessLogic
{
	public class QuotaBL : MarshalByRefObject
	{
		private const int GC_THRESHOLD = 10000;

		public readonly static QuotaBL Instance = new QuotaBL();

		private Matchnet.Caching.Cache _cache;
		private CacheItemRemovedCallback _expireCallback;
		private string _expiredCountLock = "";
		private int _expiredCount = 0;

		public delegate void ReplicationEventHandler(IReplicable replicableObject);
		public event ReplicationEventHandler ReplicationRequested;

		public delegate void SynchronizationEventHandler(string key, Hashtable cacheReferences);
		public event SynchronizationEventHandler SynchronizationRequested;
		
		public delegate void QuotaRequestEventHandler(bool cacheHit);
		public event QuotaRequestEventHandler QuotaRequested;

		public delegate void QuotaSaveEventHandler();
		public event QuotaSaveEventHandler QuotaSaved;


		private QuotaBL()
		{
			_cache = Matchnet.Caching.Cache.Instance;
			_expireCallback = new CacheItemRemovedCallback(this.expireCallback);
		}


		public Quotas GetQuotas(Int32 communityID,
			Int32 memberID)
		{
			try
			{
				Quotas quotas = _cache.Get(Quotas.GetCacheKey(communityID, memberID)) as Quotas;
				bool cacheHit = true;

				if (quotas == null)
				{
					cacheHit = false;
					quotas = new Quotas(communityID, memberID);

					SqlDataReader dataReader = null;

					try
					{
						Command command = new Command("mnMember", "dbo.up_QuotaLog_List", memberID);
						command.Parameters.Add(new Parameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, (object) communityID));
						command.Parameters.Add(new Parameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, (object) memberID));
						dataReader = Client.Instance.ExecuteReader(command);


						bool colLookupDone = false;
						Int32 ordinalQuotaTypeID = Constants.NULL_INT;
						Int32 ordinalInsertDate = Constants.NULL_INT;

						while (dataReader.Read())
						{
							if (!colLookupDone)
							{
								ordinalQuotaTypeID = dataReader.GetOrdinal("QuotaTypeID");
								ordinalInsertDate = dataReader.GetOrdinal("InsertDate");

								colLookupDone = true;
							}

							QuotaType quotaType = (QuotaType)Enum.Parse(typeof(QuotaType), dataReader.GetInt32(ordinalQuotaTypeID).ToString());
							quotas[quotaType].AddItem(dataReader.GetDateTime(ordinalInsertDate));
						}

						quotas.Sort();
					}
					finally
					{
						if (dataReader != null)
						{
							dataReader.Close();
						}
					}

					insertQuotas(quotas);
				}

				QuotaRequested(cacheHit);
				return quotas;
			}
			catch (Exception ex)
			{
				throw new BLException("Error loading quotas (communityID: " + communityID.ToString() + ", memberID: " + memberID.ToString() + ").", ex);
			}
		}
		

		public void AddQuotaItem(Int32 communityID,
			Int32 memberID,
			QuotaType quotaType,
			DateTime dateTime)
		{
			try
			{
				Quotas quotas = GetQuotas(communityID, memberID);
				quotas[quotaType].AddItem(dateTime);

				Command command = new Command("mnListMember", "dbo.up_QuotaLog_Save", memberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, communityID);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@QuotaTypeID", SqlDbType.Int, ParameterDirection.Input, (Int32)quotaType);
				command.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, dateTime);
				Client.Instance.ExecuteAsyncWrite(command);

				QuotaSaved();
			}
			catch (Exception ex)
			{
				throw new BLException("Error saving quota item (communityID: " + communityID.ToString() + ", memberID: " + memberID.ToString() + ", quotaType: " + quotaType.ToString() + ").", ex);
			}
		}


		private void insertQuotas(Quotas quotas)
		{
			insertQuotas(quotas, true);
		}


		private void insertQuotas(Quotas quotas, bool replicate)
		{
			quotas.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("LISTSVC_CACHE_TTL_SVC"));
			_cache.Insert(quotas, _expireCallback);

			if (replicate)
			{
				ReplicationRequested(quotas);
			}
		}

		
		private void expireCallback(string key, object value, CacheItemRemovedReason callbackreason) 
		{
			switch (value.GetType().Name)
			{
				case "Quotas":
					Quotas quotas = value as Quotas;
					SynchronizationRequested(quotas.GetCacheKey(),
						quotas.ReferenceTracker.Purge(Constants.NULL_STRING));
					incrementExpirationCounter();
					break;
			}
		}


		private void incrementExpirationCounter()
		{
			lock (_expiredCountLock)
			{
				_expiredCount++;

				if (_expiredCount > GC_THRESHOLD)
				{
					_expiredCount = 0;
					GC.Collect();
				}
			}
		}


		public void CacheReplicatedObject(IReplicable replicableObject)
		{
			insertQuotas((Quotas)replicableObject, false);
		}

	}
}
