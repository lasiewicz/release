using System;
using System.Diagnostics;

using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.List.BusinessLogic;
using Matchnet.List.ValueObjects.Quotas;
using Matchnet.List.ServiceDefinitions;
using Matchnet.Replication;
using Matchnet.CacheSynchronization;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Content.ValueObjects.Quotas;


namespace Matchnet.List.ServiceManagers
{
	public class QuotaSM : MarshalByRefObject, IServiceManager, IQuotaService, IDisposable
	{
		private const string SERVICE_MANAGER_NAME = "QuotaSM";
		public const string SERVICE_CONSTANT = "LIST_SVC";
		public const string SERVICE_NAME = "Matchnet.List.Service";

		private PerformanceCounter _perfRequestsSec;
		private PerformanceCounter _perfHitRate;
		private PerformanceCounter _perfHitRate_Base;
		private PerformanceCounter _perfSavesSec;

		public QuotaSM()
		{
			initPerfCounters();
			QuotaBL.Instance.QuotaRequested += new Matchnet.List.BusinessLogic.QuotaBL.QuotaRequestEventHandler(Instance_QuotaRequested);
			QuotaBL.Instance.QuotaSaved += new Matchnet.List.BusinessLogic.QuotaBL.QuotaSaveEventHandler(Instance_QuotaSaved);
		}


		public void PrePopulateCache()
		{
		}


		public override object InitializeLifetimeService()
		{
			return null;
		}


		public Quotas GetQuotas(Int32 communityID,
			Int32 memberID)
		{
			try
			{
				return QuotaBL.Instance.GetQuotas(communityID, memberID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(SERVICE_NAME,
					"Error loading quotas (communityID: " + communityID.ToString() + ", memberID: " + memberID.ToString() + ").",
					ex);
			}
		}


		public void AddQuotaItem(Int32 communityID,
			Int32 memberID,
			QuotaType quotaType,
			DateTime dateTime)
		{
			try
			{
				QuotaBL.Instance.AddQuotaItem(communityID,
					memberID,
					quotaType,
					dateTime);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(SERVICE_NAME,
					"Error saving quota item (communityID: " + communityID.ToString() + ", memberID: " + memberID.ToString() + ", quotaType: " + quotaType.ToString() + ").",
					ex);
			}
		}


		public static CounterCreationData[] GetCounters()
		{	
			return new CounterCreationData [] {new CounterCreationData("Quota Requests/second", "Quota Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
														 new CounterCreationData("Quota Hit Rate", "Quota Hit Rate", PerformanceCounterType.RawFraction),
														 new CounterCreationData("Quota Hit Rate_Base","Quota Hit Rate_Base", PerformanceCounterType.RawBase),
														 new CounterCreationData("Quota Saves/second", "Quota Saves/second", PerformanceCounterType.RateOfCountsPerSecond32)};
		}


		private void initPerfCounters()
		{
			_perfRequestsSec = new PerformanceCounter(SERVICE_NAME, "Quota Requests/second", false);
			_perfHitRate = new PerformanceCounter(SERVICE_NAME, "Quota Hit Rate", false);
			_perfHitRate_Base = new PerformanceCounter(SERVICE_NAME, "Quota Hit Rate_Base", false);
			_perfSavesSec = new PerformanceCounter(SERVICE_NAME, "Quota Saves/second", false);

			resetPerfCounters();
		}


		private void resetPerfCounters()
		{
			_perfRequestsSec.RawValue = 0;
			_perfHitRate.RawValue = 0;
			_perfHitRate_Base.RawValue = 0;
			_perfSavesSec.RawValue = 0;
		}

		
		private void Instance_QuotaRequested(bool cacheHit)
		{
			_perfRequestsSec.Increment();
			_perfHitRate_Base.Increment();

			if (cacheHit)
			{
				_perfHitRate.Increment();
			}
		}


		private void Instance_QuotaSaved()
		{
			_perfSavesSec.Increment();
		}
		
		#region IDisposable Members
		public void Dispose()
		{
		}
		#endregion

	}
}
