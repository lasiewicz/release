using System;
using System.Diagnostics;

using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.List.BusinessLogic;
using Matchnet.List.ValueObjects;
using Matchnet.List.ServiceDefinitions;
using Matchnet.Replication;
using Matchnet.CacheSynchronization;
using Matchnet.CacheSynchronization.ValueObjects;
using System.Text;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.List.ServiceManagers
{
	public class ListSM : MarshalByRefObject, IServiceManager, IListService, IReplicationRecipient, IReplicationActionRecipient, IDisposable
	{
		private const string SERVICE_MANAGER_NAME = "ListSM";
		public const string SERVICE_CONSTANT = "LIST_SVC";
		public const string SERVICE_NAME = "Matchnet.List.Service";

		private PerformanceCounter _perfCount;
		private PerformanceCounter _perfRequestsSec;
		private PerformanceCounter _perfRequestItemsSec;
		private PerformanceCounter _perfHitRate;
		private PerformanceCounter _perfHitRate_Base;
		private PerformanceCounter _perfSavesSec;

		private HydraWriter _hydraWriter = null;
		private Replicator _replicator = null;
		private Synchronizer _synchronizer = null;

		public void StartWriter()
		{
			_hydraWriter.Start();
		}


		public void StopWriter()
		{
			_hydraWriter.Stop();
		}


		public void StartReplicator()
		{
			_replicator.Start();
		}


		public void StopReplicator()
		{
			_replicator.Stop();
		}

		public ListSM()
		{
			//perf
			initPerfCounters();
			ListBL.Instance.ListRequested += new Matchnet.List.BusinessLogic.ListBL.ListRequestEventHandler(ListBL_ListRequested);
			ListBL.Instance.ListAdded += new Matchnet.List.BusinessLogic.ListBL.ListAddEventHandler(ListBL_ListAdded);
			ListBL.Instance.ListRemoved += new Matchnet.List.BusinessLogic.ListBL.ListRemoveEventHandler(ListBL_ListRemoved);
			ListBL.Instance.ListSaved += new Matchnet.List.BusinessLogic.ListBL.ListSaveEventHandler(ListBL_ListSaved);

			//replication
			ListBL.Instance.ReplicationRequested += new Matchnet.List.BusinessLogic.ListBL.ReplicationEventHandler(ListBL_ReplicationRequested);
			ListBL.Instance.ReplicationActionRequested += new Matchnet.List.BusinessLogic.ListBL.ReplicationActionEventHandler(ListBL_ReplicationActionRequested);
			QuotaBL.Instance.ReplicationRequested += new Matchnet.List.BusinessLogic.QuotaBL.ReplicationEventHandler(ListBL_ReplicationRequested);

			//synchronization
			ListBL.Instance.SynchronizationRequested += new Matchnet.List.BusinessLogic.ListBL.SynchronizationEventHandler(ListBL_SynchronizationRequested);
			QuotaBL.Instance.SynchronizationRequested += new Matchnet.List.BusinessLogic.QuotaBL.SynchronizationEventHandler(ListBL_SynchronizationRequested);

			string machineName = System.Environment.MachineName;

			string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");

			if(overrideMachineName != null)
			{
				machineName = overrideMachineName;
			}

			_hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[]{"mnList", "mnListMember", "mnAlert"});
			_hydraWriter.Start();

			string replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("LISTSVC_REPLICATION_OVERRIDE");
			if (replicationURI.Length == 0)
			{
				replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(SERVICE_CONSTANT, SERVICE_MANAGER_NAME, machineName);
			}
			_replicator = new Replicator(SERVICE_NAME);
			_replicator.SetDestinationUri(replicationURI);
			_replicator.Start();

			EventLog.WriteEntry(SERVICE_NAME,
				"Replicating to " + replicationURI,
				EventLogEntryType.Information);

			_synchronizer = new Synchronizer(SERVICE_NAME);
			_synchronizer.Start();
		}


		public void PrePopulateCache()
		{
		}


		public override object InitializeLifetimeService()
		{
			return null;
		}


		public Replicator Replicator
		{
			get
			{
				return _replicator;
			}
		}


		public Synchronizer Synchronizer
		{
			get
			{
				return _synchronizer;
			}
		}


		#region list
		public ListInternal GetListInternal(string clientHostName,
			CacheReference cacheReference,
			Int32 memberID,
			Int32[] communityIDList)
		{
			try 
			{
				return ListBL.Instance.GetListInternal(clientHostName,
					cacheReference,
					memberID,
					communityIDList);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Failure getting MemberList.", ex);
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		public ListCountCollection GetListCounts(Int32 memberID, Int32 communityID, Int32 siteID)
		{
			try 
			{
				return ListBL.Instance.GetListCounts(memberID,
					communityID,
					siteID);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving List counts.", ex);
			}
		}


		public void SaveListCategory(string clientHostName,
			Int32 categoryID,
			Int32 communityID,
			Int32 memberID,
			string comment)
		{
			try 
			{
				ListBL.Instance.SaveListCategory(clientHostName,
					categoryID,
					communityID,
					memberID,
					comment);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Failure saving list category.", ex);
			}
		}

        //added for backwards compatability
	    public void AddListMember(string clientHostName,
	        HotListCategory category,
	        Int32 communityID,
	        Int32 siteID,
	        Int32 memberID,
	        Int32 targetMemberID,
	        DateTime actionDate,
	        string comment,
	        Int32 teaseID)
	    {
            AddListMember(clientHostName, category, communityID, siteID, memberID, targetMemberID, actionDate, comment, teaseID, DateTime.MinValue);
	    }

	    public void AddListMember(string clientHostName,
			HotListCategory category,
			Int32 communityID,
			Int32 siteID,
			Int32 memberID,
			Int32 targetMemberID,
			DateTime actionDate,
			string comment,
			Int32 teaseID,
            DateTime timeStamp)
		{
			try 
			{
				ListBL.Instance.AddListMember(clientHostName,
					category,
					communityID,
					siteID,
					memberID,
					targetMemberID,
					actionDate,
					comment,
					teaseID,
                    timeStamp);

                 //Disable the notifications for mingle migration
			    if (AreNotificationsEnabled(communityID))
			    {
			        if (category == HotListCategory.WhoViewedYourProfile && memberID != targetMemberID && memberID > 0 &&
			            targetMemberID > 0)
			        {
			            StringBuilder sbParams = new StringBuilder();
			            try
			            {
			                sbParams.Append(category.ToString());
			                sbParams.Append("," + communityID.ToString());
			                sbParams.Append("," + siteID.ToString());
			                sbParams.Append("," + memberID.ToString());
			                sbParams.Append("," + targetMemberID.ToString());

			                ListBL.Instance.SendProfileViewedAlert(category, communityID, siteID, memberID, targetMemberID);
			            }
			            catch (Exception sendProfileViewedAlertEx)
			            {
			                //this error should not cause issues for the add list member feature, just need to log it to be alerted of issues
			                new ServiceBoundaryException(SERVICE_NAME,
			                    "SendProfileViewedAlert(" + sbParams.ToString() + ") error:" + sendProfileViewedAlertEx.Message,
			                    sendProfileViewedAlertEx);
			            }
			        }
			    }
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Failure saving list entry.", ex);
			}
		}


		public void RemoveListMember(string clientHostName,
			HotListCategory category,
			Int32 communityID,
			Int32 memberID,
			Int32 targetMemberID)
		{
			try 
			{
				ListBL.Instance.RemoveListMember(clientHostName,
					category,
					communityID,
					memberID,
					targetMemberID);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Failure saving list entry.", ex);
			}
		}


		public void AddClick(string clientHostName,
			Int32 communityID,
			Int32 memberID,
			Int32 targetMemberID,
			Int32 siteID,
			ClickDirectionType direction,
			ClickListType type,
			DateTime actionDate)
		{
			try 
			{
				ListBL.Instance.AddClick(clientHostName,
					communityID,
					memberID,
					targetMemberID,
					siteID,
					direction,
					type,
					actionDate);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Failure saving click entry.", ex);
			}
		}

		public ClickMask GetClickMask(Int32 memberID, Int32 targetMemberID, Int32 communityID, Int32 siteID)
		{
			try 
			{
				return ListBL.Instance.GetClickMask(memberID,
					targetMemberID,
					communityID,
					siteID);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving click entry.", ex);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <returns></returns>
		public ListInternal GetListInternalMutualYes(Int32 memberID, Int32 communityID)
		{
			try 
			{
				return ListBL.Instance.GetListInternalMutualYes(memberID, communityID);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving mutual yes list.", ex);
			}
		}

		#endregion

		#region category
		//temp
		public StandardCategoryCollection GetStandardListCategories(Int32 siteID)
		{
			try
			{
				return ListBL.Instance.GetStandardListCategories(siteID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Error retrieving standard list categories.", ex);
			}
		}

		public CustomCategoryCollection GetCustomListCategories(Int32 memberID,
			Int32 communityID)
		{
			return GetCustomListCategories(null,
				null,
				memberID,
				communityID);
		}


		public CustomCategoryCollection GetCustomListCategories(string clientHostName,
			CacheReference cacheReference,
			Int32 memberID,
			Int32 communityID)
		{
			try
			{
				return ListBL.Instance.GetCustomListCategories(clientHostName,
					cacheReference,
					memberID,
					communityID);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Error retrieving custom list categories.", ex);
			}
		}


		//temp
		public void DeleteListCategory(Int32 categoryID,
			Int32 communityID,
			Int32 memberID)
		{
			DeleteListCategory(null,
				categoryID,
				communityID,
				memberID);
		}


		public void DeleteListCategory(string clientHostName,
			Int32 categoryID,
			Int32 communityID,
			Int32 memberID)
		{
			try
			{
				ListBL.Instance.DeleteListCategory(clientHostName,
					categoryID,
					communityID,
					memberID);
			}
			catch (Exception ex)
			{
				throw(new ServiceBoundaryException(SERVICE_NAME, "Error occured while deleting category.", ex));
			}
		}


        public void SaveViralStatus(string clientHostName, int communityId, int memberId, int targetMemberId,
           bool alertEmailSent, bool clickEmailSent)
        {
            try
            {
                ListBL.Instance.SaveViralStatus(clientHostName, communityId, memberId, targetMemberId,
                    alertEmailSent, clickEmailSent);
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(SERVICE_NAME, "Error occured while saving viral status.", ex));
            }
        }



		#endregion

		#region Communication with ExternalMail 
		public void SendSMSAlert(Matchnet.List.ValueObjects.HotListCategory hotlistCat,
			int receiverMemberID,
			int senderMemberID,
			int senderBrandID,
			string targetPhoneNumber,
			bool isFriend,
			Matchnet.Content.ValueObjects.BrandConfig.Brand receiverBrand)
		{
            // This method is deprecated and will be removed once all ListSA clients are updated with latest version.
		}

		public void SendHotListNotification(Matchnet.List.ValueObjects.HotListCategory cat, int targetMemberID, int memberID, int brandID)
		{
			try
			{
				ListBL.Instance.SendHotListNotification(cat, targetMemberID, memberID, brandID);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Failure sending HotList notification for HotListCategory: " + cat.ToString(), ex);
			}
		}

		public void SendMessmoMessage(int communityID,
			int targetBrandID,
			int sendingBrandID,
			int targetMemberID,
			int memberID,
			Matchnet.List.ValueObjects.HotListCategory cat)
		{
			try
			{
				ListBL.Instance.SendMessmoMessage(communityID,
					targetBrandID,
					sendingBrandID,
					targetMemberID,
					memberID,
					cat);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Failure sending Messmo Message for HotListCategory: " + cat.ToString(), ex);
			}
		}
		#endregion

		void IReplicationRecipient.Receive(IReplicable replicable)
		{
			try
			{
				ListBL.Instance.CacheReplicatedObject(replicable);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Failure processing replication object.", ex);
			}
		}


		public void Receive(IReplicationAction replicationAction)
		{
			ListBL.Instance.PlayReplicationAction(replicationAction);
		}

		
		private void ListBL_ReplicationRequested(IReplicable replicableObject)
		{
			_replicator.Enqueue(replicableObject);
		}


		private void ListBL_ReplicationActionRequested(IReplicationAction replicationAction)
		{
			_replicator.Enqueue(replicationAction);
		}


		private void ListBL_SynchronizationRequested(string key, 
			System.Collections.Hashtable cacheReferences)
		{
			_synchronizer.Enqueue(key, cacheReferences);
		}


		#region instrumentation
		public static void PerfCounterInstall()
		{	
			CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
			ccdc.AddRange(new CounterCreationData [] {	
														 new CounterCreationData("List Items", "List Items", PerformanceCounterType.NumberOfItems64),
														 new CounterCreationData("List Requests/second", "List Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
														 new CounterCreationData("List Request Items/second", "List Request Items/second", PerformanceCounterType.RateOfCountsPerSecond32),
														 new CounterCreationData("List Hit Rate", "List Hit Rate", PerformanceCounterType.RawFraction),
														 new CounterCreationData("List Hit Rate_Base","List Hit Rate_Base", PerformanceCounterType.RawBase),
														 new CounterCreationData("List Saves/second", "List Saves/second", PerformanceCounterType.RateOfCountsPerSecond32),
			});

			ccdc.AddRange(QuotaSM.GetCounters());
			PerformanceCounterCategory.Create(SERVICE_NAME, SERVICE_NAME, ccdc);
		}


		public static void PerfCounterUninstall()
		{	
			PerformanceCounterCategory.Delete(SERVICE_NAME);
		}


		private void initPerfCounters()
		{
			_perfCount = new PerformanceCounter(SERVICE_NAME, "List Items", false);
			_perfRequestsSec = new PerformanceCounter(SERVICE_NAME, "List Requests/second", false);
			_perfRequestItemsSec = new PerformanceCounter(SERVICE_NAME, "List Request Items/second", false);
			_perfHitRate = new PerformanceCounter(SERVICE_NAME, "List Hit Rate", false);
			_perfHitRate_Base = new PerformanceCounter(SERVICE_NAME, "List Hit Rate_Base", false);
			_perfSavesSec = new PerformanceCounter(SERVICE_NAME, "List Saves/second", false);

			resetPerfCounters();
		}


		private void resetPerfCounters()
		{
			_perfCount.RawValue = 0;
			_perfRequestsSec.RawValue = 0;
			_perfRequestItemsSec.RawValue = 0;
			_perfHitRate.RawValue = 0;
			_perfHitRate_Base.RawValue = 0;
			_perfSavesSec.RawValue = 0;
		}


		private void ListBL_ListRequested(bool cacheHit, Int32 count)
		{
			_perfRequestsSec.Increment();
			_perfRequestItemsSec.IncrementBy(count);
			_perfHitRate_Base.Increment();

			if (cacheHit)
			{
				_perfHitRate.Increment();
			}
		}


		private void ListBL_ListAdded()
		{
			_perfCount.Increment();
		}

		
		private void ListBL_ListRemoved()
		{
			_perfCount.Decrement();
		}


		private void ListBL_ListSaved()
		{
			_perfSavesSec.Increment();
		}
		#endregion

        bool AreNotificationsEnabled(int communityId)
        {
            var returnValue = true;
            try
            {
                Boolean.TryParse(RuntimeSettings.Instance.GetSettingFromSingleton(Matchnet.List.ValueObjects.ListServiceConstants.LIST_NOTIFICATIONS_ENABLED, communityId), out returnValue);
            }
            catch (Exception ex)
            {
                //No Log4net instance to log the exception so supressing.
            }
            return returnValue;
        }
		#region IDisposable Members
		public void Dispose()
		{
			if (_replicator != null)
			{
				_replicator.Stop();
			}

			if (_synchronizer != null)
			{
				_synchronizer.Stop();
			}


			if (_hydraWriter != null)
			{
				_hydraWriter.Stop();
			}

			resetPerfCounters();
		}
		#endregion
	}
}
