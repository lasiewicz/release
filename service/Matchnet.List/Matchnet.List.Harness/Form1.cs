using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Diagnostics;
using System.Threading;


using Matchnet.List.BusinessLogic;
using Matchnet.List.ServiceManagers;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.List.ServiceDefinitions;
using Matchnet.List.ValueObjects.ServiceDefinitions;
using Matchnet.RemotingServices.ValueObjects.ServiceDefinitions;

namespace Matchnet.List.Harness
{
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox txtMemberID;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.txtMemberID = new System.Windows.Forms.TextBox();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(8, 8);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			this.button1.Text = "button1";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// txtMemberID
			// 
			this.txtMemberID.Location = new System.Drawing.Point(96, 8);
			this.txtMemberID.Name = "txtMemberID";
			this.txtMemberID.TabIndex = 1;
			this.txtMemberID.Text = "48338001";
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(8, 56);
			this.button2.Name = "button2";
			this.button2.TabIndex = 2;
			this.button2.Text = "button2";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(8, 96);
			this.button3.Name = "button3";
			this.button3.TabIndex = 3;
			this.button3.Text = "button3";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(528, 309);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.txtMemberID);
			this.Controls.Add(this.button1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}
		#endregion

		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			//Int32[] memberIDs = new Int32[]{10317144, 45322948, 46539460, 8222588, 49045408, 45302224};
			Int32[] memberIDs = new Int32[]{23259144, 988268688, 988267566};

			foreach (Int32 memberID in memberIDs)
			{
				try
				{
                    IHotList list = ListSA.Instance.GetList(memberID, ListLoadFlags.IngoreSACache);
					Matchnet.List.ValueObjects.ClickMask mask= list.GetClickMask(3,103,200000);
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.ToString());
				}
			}

			Thread.Sleep(3000);

			/*foreach (Int32 memberID in memberIDs)
			{
				((ICacheManagerService)Activator.GetObject(typeof(ICacheManagerService), "tcp://172.16.1.131:43000/CacheManagerSM.rem")).Remove("L" + memberID.ToString());
			}
*/

			/*
			Console.WriteLine(ListSA.Instance.GetList(23248462).IsHotListed(HotListType.ReportAbuse,
				HotListDirection.OnYourList,
				1,
				988272102).ToString());
			(

			/*
			StreamReader sr = new StreamReader(@"c:\mbrs.txt");
			string[] mbrs = sr.ReadToEnd().Replace("\r","").Split('\n');

			foreach (string mbr in mbrs)
			{
				Matchnet.List.ServiceAdapters.ListSA.Instance.AddListMember(HotListCategory.MembersYouViewed,
					3,
					103,
					23248462,
					Convert.ToInt32(mbr),
					Constants.NULL_STRING,
					Constants.NULL_INT,
					true,
					false);

				//Thread.Sleep(50);

				//return;
			}

			Console.WriteLine(list.GetListItemDetail(HotListCategory.Default,
				10,
				15,
				42050916).Comment);


			Console.WriteLine("hitIt");
			((IListService)Activator.GetObject(typeof(IListService), "tcp://192.168.3.76:43000/ListSM.rem")).GetListInternal(null,
				null,
				1,
				new Int32[]{1});
			*/

			/*
			Thread t = new Thread(new ThreadStart(hitIt));
			t.Start();
			Thread.Sleep(5000);
			t.Abort();
			*/

				
			//Matchnet.List.ServiceAdapters.List list = ListSA.Instance.GetList(41568720);
			/*
			Console.WriteLine(list.GetClickMask(1, 101, 46026524));
			Console.WriteLine(list.GetCount(HotListCategory.MutualYes,
				1,
				101));

			ListInternal listInternal = ListBL.Instance.GetListInternal(null,
				null,
				23248462,
				new Int32[]{1,2,3,10,12});

			BinaryFormatter formatter = new BinaryFormatter();
			FileStream fs = new FileStream(@"c:\temp\ListInternal.bin", FileMode.Create);
			formatter.Serialize(fs, listInternal);
			fs.Close();

			FileStream fs2 = new FileStream(@"c:\temp\ListInternal.bin", FileMode.Open);
			listInternal = formatter.Deserialize(fs2) as ListInternal;
			fs2.Close();

			Console.WriteLine(listInternal.MemberID);
			*/

			/*
			IListService listService = (IListService)Activator.GetObject(typeof(IListService), "tcp://172.16.1.132:43000/ListSM.rem");
			listService.StopReplicator();
			*/


			/*
			IListService listService = (IListService)Activator.GetObject(typeof(IListService), "tcp://172.16.0.31:43000/ListSM.rem");

			ListInternal list = listService.GetListInternal(null,
				null,
				50531520,
				new Int32[]{1,2,10,12});

			DateTime beginTime = DateTime.Now;

			MemoryStream ms = new MemoryStream();
			BinaryFormatter formatter = new BinaryFormatter();
			formatter.Serialize(ms, list);
			//formatter.Deserialize(ms);
			ms.Close();

			Console.WriteLine(DateTime.Now.Subtract(beginTime).TotalSeconds);
			*/

			/*
			ListSM sm = new ListSM();

			Int32[] memberIDs = new Int32[]{5446,100766,2215162,4443610,5531187,6087252,8754720,8833030,16000206,
											   23246972,23247878,23248078,23248258,23248462,32435230,100000106,100000118,100000128,100000146,
											   100000152,100000156,100000164,100000166,100000169,100000176,100000177,100000180,100000184,100000185,
											   100000188,100000195,100000196,100000198,100000206,100000211,100000212,100000214,100000215,100000218,
											   100000220,100000227,100000228,100000232,100000234,100000235,100000253,100000258,100000268,100000282,
											   100000285,100000288,100000302,100000304,100000307,100000308,100000310,100000311,100000314,100000323,
											   100000328,100000330,100000333,100000336,100000347,100000373,100000383,100000402,100000405,100000410,
											   100000413,100000414,100000443,100000448,100000451,100000466,100000484,100000488,100000490,100000496,
											   100000508,100000512,100000513,100000518,100000521,100000529,100000542,100000548,100000551,100000557,
											   100000558,100000564,100000568,100000576,100000582,100000591,100000596,100000598,100000610,100000628,
											   100000631,100000636,100000652,100000653,100000661,100000682,100000693,100000696,100000706,100000708,
											   100000713,100000719,100000731,100000741,100000743,100000744,100000747,100000751,100000752,100000755,
											   100000756,100000759,100000766,100000769,100000772,100000774,100000782,100000788,100000790,100000791,
											   100000795,100000797,100000814,100000824,100000826,100000830,100000838,100000843,100000846,100000847,
											   100000849,100000852,100000864,100000881,100000882,100000890,100000896,100000900,100000906,100000910,
											   100000925,100000932,100000940,100000943,100000944,100000946,100000950,100000954,100000967,100000971,
											   100000972,100000976,100000978,100000979,100000984,100001012,100001018,100001381,100001556,100002238,
											   100002244,100002560,100002591,100002748,100003120,100003166,100003177,100003180,100003190,100003198,
											   100003217,100003220,100003223,100003243,100003258,100003273,100003276,100003278,100003281,100003296,
											   100003299,100003336,100003354,100003371,100003465,100005379,100019806,100019809,100019851,100019941,
											   100019992,100020007,100020058,100020082,100020085,100020178,100020190,100020202,100020319,100020322,
											   100020325,100020328,100020334,100020343,100020346,100020361,987655068,987655072,987655366,987655380,
											   987655914,987656440,987656484,987657052,987657084,987657262,987657346,987657444,987657506,987657552,
											   987657554,987657556,987657564,987657584,987657702,987994670,988264630,988264660,988264854,988264912,
											   988264920,988265244,988265246,988265342,988265502,988265622,988265678,988265822,988266510,988267562,
											   988268014,988268128,988268158,988268296,988268308,988268362,988268688,988269478,988269500,988269562,
											   988269718,988270882,988270908,988271464,988271822,988271852,988272102,988272118,988272266,988272306,
											   988272336,988272426,988272446,988272454,988272526,988272594,988272660,988272664,988272684,988272688,
											   988272720,988272768,988272778,988272828,988272936,988273312,988273314,988273316};

			for (Int32 memberNum = 0; memberNum < memberIDs.Length; memberNum++)
			{
				sm.GetListInternal(null, null, memberIDs[memberNum], new Int32[]{1,2,10,12}, false);
			}
			*/

			/*
			DateTime beginTime = DateTime.Now;
			int memberID = Int32.Parse(txtMemberID.Text) ;
			Matchnet.List.ServiceAdapters.List list = ListSA.Instance.GetList(23248462, ListLoadFlags.None);
			*/

			/*
			for (Int32 i = 0; i < 100000; i++)
			{
				list.IsHotListed(HotListCategory.Default,
					1,
					1);
			}
			*/


			//			MemoryStream ms = new MemoryStream();
//			BinaryFormatter formatter = new BinaryFormatter();
//
//			formatter.Serialize(ms,lst.);
			

			
			
			



			/*
			ListSA.Instance.GetListQuotas();
			return;


			//load test for profiling
			Int32[] memberIDs = new Int32[]{5446,100766,2215162,4443610,5531187,6087252,8754720,8833030,16000206,
				23246972,23247878,23248078,23248258,23248462,32435230,100000106,100000118,100000128,100000146,
				100000152,100000156,100000164,100000166,100000169,100000176,100000177,100000180,100000184,100000185,
				100000188,100000195,100000196,100000198,100000206,100000211,100000212,100000214,100000215,100000218,
				100000220,100000227,100000228,100000232,100000234,100000235,100000253,100000258,100000268,100000282,
				100000285,100000288,100000302,100000304,100000307,100000308,100000310,100000311,100000314,100000323,
				100000328,100000330,100000333,100000336,100000347,100000373,100000383,100000402,100000405,100000410,
				100000413,100000414,100000443,100000448,100000451,100000466,100000484,100000488,100000490,100000496,
				100000508,100000512,100000513,100000518,100000521,100000529,100000542,100000548,100000551,100000557,
				100000558,100000564,100000568,100000576,100000582,100000591,100000596,100000598,100000610,100000628,
				100000631,100000636,100000652,100000653,100000661,100000682,100000693,100000696,100000706,100000708,
				100000713,100000719,100000731,100000741,100000743,100000744,100000747,100000751,100000752,100000755,
				100000756,100000759,100000766,100000769,100000772,100000774,100000782,100000788,100000790,100000791,
				100000795,100000797,100000814,100000824,100000826,100000830,100000838,100000843,100000846,100000847,
				100000849,100000852,100000864,100000881,100000882,100000890,100000896,100000900,100000906,100000910,
				100000925,100000932,100000940,100000943,100000944,100000946,100000950,100000954,100000967,100000971,
				100000972,100000976,100000978,100000979,100000984,100001012,100001018,100001381,100001556,100002238,
				100002244,100002560,100002591,100002748,100003120,100003166,100003177,100003180,100003190,100003198,
				100003217,100003220,100003223,100003243,100003258,100003273,100003276,100003278,100003281,100003296,
				100003299,100003336,100003354,100003371,100003465,100005379,100019806,100019809,100019851,100019941,
				100019992,100020007,100020058,100020082,100020085,100020178,100020190,100020202,100020319,100020322,
				100020325,100020328,100020334,100020343,100020346,100020361,987655068,987655072,987655366,987655380,
				987655914,987656440,987656484,987657052,987657084,987657262,987657346,987657444,987657506,987657552,
				987657554,987657556,987657564,987657584,987657702,987994670,988264630,988264660,988264854,988264912,
				988264920,988265244,988265246,988265342,988265502,988265622,988265678,988265822,988266510,988267562,
				988268014,988268128,988268158,988268296,988268308,988268362,988268688,988269478,988269500,988269562,
				988269718,988270882,988270908,988271464,988271822,988271852,988272102,988272118,988272266,988272306,
				988272336,988272426,988272446,988272454,988272526,988272594,988272660,988272664,988272684,988272688,
				988272720,988272768,988272778,988272828,988272936,988273312,988273314,988273316};

			DateTime startTime = DateTime.Now;

			for (Int32 i = 0; i < 100; i++)
			{
				foreach (Int32 memberID in memberIDs)
				{
					ListSA.Instance.GetList(memberID, ListLoadFlags.IngoreSACache);
					IListService listService = (IListService)Activator.GetObject(typeof(IListService), "tcp://dv-gpeterson:43000/ListSM.rem");
					listService.GetListInternal(null, null, memberID, new Int32[]{1,2,10,12}, false);
				}
			}

			MessageBox.Show(DateTime.Now.Subtract(startTime).TotalSeconds.ToString());
			*/

			Console.WriteLine("done");
		}


		private void hitIt()
		{
			Console.WriteLine("hitIt");
			((IListService)Activator.GetObject(typeof(IListService), "tcp://localhost:43000/ListSM.rem")).GetListInternal(null,
				null,
				1,
				new Int32[]{1});
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
		
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			
			int[] mbr=new int[]{23251608,23255376,23259144,23262912,23266680};
		   //=new testmember(23251608);

			for (int i=0; i < mbr.Length;i++)
			{
				testmember tmrb=new testmember(mbr[i]);
				Thread t1=new Thread(new ThreadStart(tmrb.LoadList));
				Thread t2=new Thread(new ThreadStart(tmrb.AddClick));
				
				t1.Start();
				t2.Start();
				
			}
			
				
			

		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			int[] mbr=new int[]{23251608,23255376,23259144,23262912,23266680};
			//=new testmember(23251608);

			for (int i=0; i < mbr.Length;i++)
			{
				testmember tmrb=new testmember(mbr[i]);
				Thread t1=new Thread(new ThreadStart(tmrb.LoadList));
				Thread t2=new Thread(new ThreadStart(tmrb.GetClickMask));
				
				t1.Start();
				t2.Start();
				
			}
		}

	
	}
}
