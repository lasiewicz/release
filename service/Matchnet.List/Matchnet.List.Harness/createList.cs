using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Diagnostics;
using System.Threading;


using Matchnet.List.BusinessLogic;
using Matchnet.List.ServiceManagers;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.List.ServiceDefinitions;
using Matchnet.RemotingServices.ValueObjects.ServiceDefinitions;

namespace Matchnet.List.Harness
{
	/// <summary>
	/// Summary description for createList.
	/// </summary>
	public class createList : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public createList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(112, 32);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			this.button1.Text = "button1";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// createList
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(480, 273);
			this.Controls.Add(this.button1);
			this.Name = "createList";
			this.Text = "createList";
			this.Load += new System.EventHandler(this.createList_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void createList_Load(object sender, System.EventArgs e)
		{
		
		}

		private void button1_Click(object sender, System.EventArgs e)
		{

			int imbr=0;
			for(int i=0;i<=12000;i++)
			{
				imbr+=1;
				Matchnet.List.ServiceAdapters.ListSA.Instance.AddListMember(HotListCategory.MembersClickedYes,
					3,
					103,
					23251608,
					imbr,
					Constants.NULL_STRING,
					Constants.NULL_INT,
                    DateTime.MinValue,
					true,
					false);

				//Thread.Sleep(50);

				//return;
			}

			
			}
	}
}
