#region

using System;
using Matchnet.Caching;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Quotas;
using Matchnet.Exceptions;
using Matchnet.List.ServiceAdapters.Interfaces;
using Matchnet.List.ServiceDefinitions;
using Matchnet.List.ValueObjects.Quotas;

#endregion

namespace Matchnet.List.ServiceAdapters
{
    public class QuotaSA : IAddQuotaItem
    {
        private const string SERVICE_MANAGER_NAME = "QuotaSM";
        private const string SERVICE_CONSTANT = "LIST_SVC";

        public static readonly QuotaSA Instance = new QuotaSA();

        private readonly Cache _cache;

        private QuotaSA()
        {
            _cache = Cache.Instance;
        }


        public bool IsAtQuota(Int32 communityID, Int32 memberID, QuotaType quotaType)
        {
            Int32 count;
            return IsAtQuota(communityID, memberID, quotaType, out count);
        }


        public bool IsAtQuota(Int32 communityID, Int32 memberID, QuotaType quotaType, out Int32 count)
        {
            var quotaDefinition = QuotaDefinitionSA.Instance.GetQuotaDefinitions().GetQuotaDefinition(quotaType, communityID);
            var minDateTime = DateTime.Now.AddMinutes(-Duration.GetMinutes(quotaDefinition.DurationType, quotaDefinition.Duration));
            count = GetQuotas(communityID, memberID)[quotaType].GetItemCount(minDateTime);
            return (count >= quotaDefinition.MaxAllowed);
        }

        /// <summary>
        ///     Verify the tease count had met the quota
        /// </summary>
        /// <param name="communityID"></param>
        /// <param name="siteID"></param>
        /// <param name="memberID"></param>
        /// <param name="quotaType"></param>
        /// <param name="count"></param>
        /// <param name="teaseMaxAllow"></param>
        /// <returns></returns>
        public bool IsTeaseAtQuota(Int32 communityID, Int32 siteID, Int32 memberID, QuotaType quotaType, out Int32 count, out Int32 teaseMaxAllow)
        {
            try
            {
                var quotaDefinition = QuotaDefinitionSA.Instance.GetQuotaDefinitions().GetQuotaDefinition(quotaType, communityID);
                teaseMaxAllow = quotaDefinition.MaxAllowed; // This portion was setting driven before, modified to follow the right pattern.
                var minDateTime = DateTime.Now.AddMinutes(-Duration.GetMinutes(quotaDefinition.DurationType, quotaDefinition.Duration));
                count = GetQuotas(communityID, memberID)[quotaType].GetItemCount(minDateTime);
                return (count >= teaseMaxAllow);
            }
            catch (Exception ex)
            {
                throw new SAException(ToString() + ".IsTeaseAtQuota() - " + ex.Message);
            }
        }


        public void AddQuotaItem(Int32 communityID, Int32 memberID, QuotaType quotaType)
        {
            var uri = "";
            try
            {
                var dateTime = DateTime.Now;
                uri = getServiceManagerUri(memberID); 
                var quotas = GetQuotas(communityID, memberID);
                getService(uri).AddQuotaItem(communityID, memberID, quotaType, dateTime);
                quotas[quotaType].AddItem(dateTime);
            }
            catch (Exception ex)
            {
                throw new SAException("Error saving quota item (uri: " + uri + ", communityID: " + communityID + ", memberID: " + memberID + ", quotaType: " + quotaType + ").", ex);
            }
        }


        private Quotas GetQuotas(Int32 communityID, Int32 memberID)
        {
            var uri = "";

            try
            {
                var quotas = _cache.Get(Quotas.GetCacheKey(communityID, memberID)) as Quotas;

                if (quotas != null) return quotas;
                uri = getServiceManagerUri(memberID);
                quotas = getService(uri).GetQuotas(communityID, memberID);
                quotas.CacheTTLSeconds = Convert.ToInt32(RuntimeSettings.GetSetting("LISTSVC_CACHE_TTL_SA"));
                _cache.Insert(quotas);

                return quotas;
            }
            catch (Exception ex)
            {
                throw new Exception("Error loading quotas (uri: " + uri + ", communityID: " + communityID + ", memberID: " + memberID + ").", ex);
            }
        }


        private IQuotaService getService(string uri)
        {
            try
            {
                return (IQuotaService) Activator.GetObject(typeof (IQuotaService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }


        private string getServiceManagerUri(int memberID)
        {
            try
            {
                var uri = AdapterConfigurationSA.GetServicePartition(SERVICE_CONSTANT, PartitionMode.Calculated, memberID).ToUri(SERVICE_MANAGER_NAME);
                var overrideHostName = RuntimeSettings.GetSetting("LISTSVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Length <= 0) return uri;
                var uriBuilder = new UriBuilder(new Uri(uri));
                return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }
    }
}