using System;
using System.Collections;
using System.Data;

using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.List.ServiceDefinitions;
using Matchnet.List.ValueObjects.ServiceDefinitions;
using Matchnet.Member.ServiceAdapters;
using Matchnet.CacheSynchronization.Context;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Content.ServiceAdapters; 
using Matchnet.Content.ValueObjects.Quotas;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.List.ServiceAdapters.Interfaces;
using Spark.Logging;

namespace Matchnet.List.ServiceAdapters
{
	/// <summary>
	/// 
	/// </summary>
	[Flags]
	public enum ListLoadFlags
	{
		/// <summary></summary>
		None,

		/// <summary>Should always be used when member is loading his own list. Forces a list reload from database if cached copy was not loaded by the member that owns it.</summary>
		IsSelf,

		/// <summary>Ignore local cache.</summary>
		IngoreSACache
	}

	/// <summary>
	/// List service adapter
	/// </summary>
    public class ListSA : IListAddMember
	{
		private const string SERVICE_MANAGER_NAME = "ListSM";
		private const string SERVICE_CONSTANT = "LIST_SVC";
        private const string CLASS_NAME = "ListSA";
		private const int HIDEMASK_HIDEHOTLISTS = 2;
        private const string LIST_QUOTAS_ENABLED = "LIST_QUOTAS_ENABLED";
        private const string LIST_NOTIFICATIONS_ENABLED = "LIST_NOTIFICATIONS_ENABLED";
        private const string FAVORITED_NOTIFICATIONS_ENABLED = "FAVORITED_NOTIFICATIONS_ENABLED";



		/// <summary>
		/// Singleton instance
		/// </summary>
		public static readonly ListSA Instance = new ListSA();

		private Cache _cache;

		private ListSA()
		{
			_cache = Cache.Instance;
		}

        private static ISettingsSA _settingsService = null;
        public static ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService)
                {
                    return RuntimeSettings.Instance;
                }
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        private bool AreQuotasEnabled(int communityId)
        {
            var returnValue = true;
            
            try
            {
                Boolean.TryParse(SettingsService.GetSettingFromSingleton(Matchnet.List.ValueObjects.ListServiceConstants.LIST_QUOTAS_ENABLED, communityId), out returnValue);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME, "Failed to get LIST_QUOTAS_ENABLED defaulting to true", null);
            }

            return returnValue;
        }


        private bool AreNotificationsEnabled(int communityId)
        {
            var returnValue = true;

            try
            {
                Boolean.TryParse(SettingsService.GetSettingFromSingleton(Matchnet.List.ValueObjects.ListServiceConstants.LIST_NOTIFICATIONS_ENABLED, communityId), out returnValue);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogInfoMessage(SERVICE_CONSTANT, CLASS_NAME, "Failed to get LIST_NOTIFICATIONS_ENABLED defaulting to true", null);
            }

            return returnValue;
        }

        private bool IsFavoritedNotificationEnabled(int communityId)
        {
            bool returnValue = true;
            Boolean.TryParse(SettingsService.GetSettingFromSingleton(FAVORITED_NOTIFICATIONS_ENABLED, communityId), out returnValue);

            return returnValue;
        }


		/// <summary>
		/// Retreives a list for a given member
		/// </summary>
		/// <param name="memberID"></param>
		/// <returns></returns>
        public List GetList(Int32 memberID)
		{
			return GetList(memberID, ListLoadFlags.None);
		}

       
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="loadFlags"></param>
		/// <returns></returns>
        public List GetList(Int32 memberID,
			ListLoadFlags loadFlags)
		{
			try
			{
				if (memberID < 1)
				{
					throw new ArgumentException("value must be greater than zero", "memberID");
				}

				return new List(getListInternal(memberID, loadFlags));
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve member list.", ex));
			}
		}

        /// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		public ListCountCollection GetListCounts(Int32 memberID, Int32 communityID, Int32 siteID)
		{
			string uri = string.Empty;

			try
			{
				uri = getServiceManagerUri(memberID);

				return getService(uri).GetListCounts(memberID, communityID, siteID);
			}
			catch (Exception ex)
			{
				throw new SAException("Cannot retrieve list counts, uri: " + uri, ex);
			}	
		}


        public ListSaveResult ReportAbuser(Int32 communityID,
            Int32 siteID,
            Int32 memberID,
            Int32 targetMemberID,
            Int32 reasonID)
        {
            return AddListMember(HotListCategory.ReportAbuse,
                communityID,
                siteID,
                memberID,
                targetMemberID,
                null,
                reasonID,
                DateTime.MinValue,
                false,
                false);
        }



		public ListSaveResult ReportAbuser(Int32 communityID,
			Int32 siteID,
			Int32 memberID,
			Int32 targetMemberID,
			Int32 reasonID,
            DateTime timeStamp)
		{
			return AddListMember(HotListCategory.ReportAbuse,
				communityID,
				siteID,
				memberID,
				targetMemberID,
				null,
				reasonID,
                timeStamp,
				false,
				false);
		}

	    public ListSaveResult AddListMember(HotListCategory category,
	        Int32 communityID,
	        Int32 siteID,
	        Int32 memberID,
	        Int32 targetMemberID,
	        string comment,
	        Int32 teaseID,
	        bool saveReciprocal,
	        bool ignoreQuota,
	        bool notificationsEnabled)
	    {
	        return AddListMember(category, communityID, siteID, memberID, targetMemberID, comment, teaseID, DateTime.UtcNow,
	            saveReciprocal, ignoreQuota, notificationsEnabled);
	    }
        



	    public ListSaveResult AddListMember(HotListCategory category,
            Int32 communityID,
            Int32 siteID,
            Int32 memberID,
            Int32 targetMemberID,
            string comment,
            Int32 teaseID,
            DateTime timeStamp,
            bool saveReciprocal,
            bool ignoreQuota,
            bool notificationsEnabled)
        {
            string uri = string.Empty;
            DateTime actionDate = DateTime.Now;

            HotListType listType;
            HotListDirection direction;
            var result = new ListSaveResult {Status = ListActionStatus.Success};

            try
            {
                var listInternal = getListInternal(memberID, ListLoadFlags.None);

                if (memberID < 1)
                {
                    throw new ArgumentException("value must be greater than zero", "memberID");
                }

                if (targetMemberID < 1)
                {
                    throw new ArgumentException("value must be greater than zero", "targetMemberID");
                }

                //	If the list is a voluntary list, then we need to remove the user from
                //	all other voluntary lists because a user can only be on a single
                //	voluntary list.  We'll clear all of them (rather than just one) because
                //	the legacy data might have people on multiple lists.
                if (IsListVoluntaryList((int)category))
                {
                    if (category != HotListCategory.Default && listInternal.IsHotListed(HotListCategory.Default, communityID, targetMemberID))
                    {
                        RemoveListMember(HotListCategory.Default, communityID, memberID, targetMemberID);
                    }

                    CustomCategoryCollection categories = GetCustomListCategories(memberID, communityID);

                    for (Int32 categoryNum = 0; categoryNum < categories.Count; categoryNum++)
                    {
                        CustomCategory customCategory = categories[categoryNum];
                        HotListCategory currentCategory = (HotListCategory)Enum.Parse(typeof(HotListCategory), customCategory.CategoryID.ToString());
                        if (currentCategory != category && listInternal.IsHotListed(currentCategory, communityID, targetMemberID))
                        {
                            RemoveListMember(currentCategory, communityID, memberID, targetMemberID);
                        }
                    }
                }

                ListInternal.MapListTypeDirection(category, out listType, out direction);

                var maxAllowed = Constants.NULL_INT;

                if (!ignoreQuota && AreQuotasEnabled(communityID))
                {
                    QuotaDefinitions quotaDefinitions = QuotaDefinitionSA.Instance.GetQuotaDefinitions();
                    QuotaDefinition quotaDefinition = null;

                    var quotaCounter = Constants.NULL_INT;
                    switch (category)
                    {
                        case HotListCategory.Default:
                        case HotListCategory.WhoAddedYouToTheirFavorites:
                            maxAllowed = quotaDefinitions.GetQuotaDefinition(QuotaType.FavoriteList, communityID).MaxAllowed;
                            break;

                        case HotListCategory.MembersYouViewed:
                        case HotListCategory.WhoViewedYourProfile:
                            maxAllowed = quotaDefinitions.GetQuotaDefinition(QuotaType.ViewProfileList, communityID).MaxAllowed;
                            break;

                        case HotListCategory.MembersYouIMed:
                        case HotListCategory.WhoIMedYou:
                            maxAllowed = quotaDefinitions.GetQuotaDefinition(QuotaType.ViewProfileList, communityID).MaxAllowed;
                            break;

                        case HotListCategory.IgnoreList:
                            maxAllowed = quotaDefinitions.GetQuotaDefinition(QuotaType.IgnoreList, communityID).MaxAllowed;
                            break;

                        case HotListCategory.MembersYouEmailed:
                        case HotListCategory.MembersYouSentAllAccessEmail:
                            //check quota
                            quotaDefinition = quotaDefinitions.GetQuotaDefinition(QuotaType.SentEmail, communityID);
                            if (QuotaSA.Instance.IsAtQuota(communityID, memberID, QuotaType.SentEmail, out quotaCounter))
                            {
                                result.Status = ListActionStatus.ExceededMaxAllowed;
                            }
                            else
                            {
                                QuotaSA.Instance.AddQuotaItem(communityID, memberID, QuotaType.SentEmail);
                                quotaCounter++;
                            }

                            result.MaxAllowed = quotaDefinition.MaxAllowed;
                            result.Counter = quotaCounter;

                            //trim list
                            maxAllowed = quotaDefinitions.GetQuotaDefinition(QuotaType.EmailList, communityID).MaxAllowed;
                            break;

                        case HotListCategory.WhoEmailedYou:
                        case HotListCategory.MembersWhoSentYouAllAccessEmail:
                            maxAllowed = quotaDefinitions.GetQuotaDefinition(QuotaType.EmailList, communityID).MaxAllowed;
                            break;

                        case HotListCategory.MembersYouTeased:
                            //check quota
                            int teaseMaxCount;
                            if (QuotaSA.Instance.IsTeaseAtQuota(communityID, siteID, memberID, QuotaType.Tease, out quotaCounter, out teaseMaxCount))
                            {
                                result.Status = ListActionStatus.ExceededMaxAllowed;
                            }
                            else
                            {
                                QuotaSA.Instance.AddQuotaItem(communityID, memberID, QuotaType.Tease);
                                quotaCounter++;
                            }

                            result.MaxAllowed = teaseMaxCount;
                            result.Counter = quotaCounter;

                            //trim list
                            maxAllowed = quotaDefinitions.GetQuotaDefinition(QuotaType.TeaseList, communityID).MaxAllowed;
                            break;

                        case HotListCategory.WhoTeasedYou:
                            maxAllowed = quotaDefinitions.GetQuotaDefinition(QuotaType.TeaseList, communityID).MaxAllowed;
                            break;

                        case HotListCategory.ReportAbuse:
                            quotaDefinition = quotaDefinitions.GetQuotaDefinition(QuotaType.ReportAbuse, communityID);
                            if (QuotaSA.Instance.IsAtQuota(communityID, memberID, QuotaType.ReportAbuse, out quotaCounter))
                            {
                                result.Status = ListActionStatus.ExceededMaxAllowed;
                            }
                            else
                            {
                                QuotaSA.Instance.AddQuotaItem(communityID, memberID, QuotaType.ReportAbuse);
                                quotaCounter++;
                            }
                            break;
                        case HotListCategory.ExcludeFromList:
                            maxAllowed = quotaDefinitions.GetQuotaDefinition(QuotaType.ExcludeFromList, communityID).MaxAllowed;
                            if (listInternal.GetCount(HotListCategory.ExcludeFromList, communityID, siteID) >= maxAllowed)
                            {
                                result.Status = ListActionStatus.ExceededMaxAllowed;
                            }
                            break;
                        case HotListCategory.ExcludeList:
                            maxAllowed = quotaDefinitions.GetQuotaDefinition(QuotaType.ExcludeList, communityID).MaxAllowed;
                            if (listInternal.GetCount(HotListCategory.ExcludeList, communityID, siteID) >= maxAllowed)
                            {
                                result.Status = ListActionStatus.ExceededMaxAllowed;
                            }
                            break;
                        case HotListCategory.MembersYouViewedInPushFlirts:
                            maxAllowed = quotaDefinitions.GetQuotaDefinition(QuotaType.ViewInPushFlirtsList, communityID).MaxAllowed;
                            break;

                        //case HotListCategory.
                            //TODO: We need to add quotas for the new quotatypes 
                        
                    }
                }

                if (result.Status == ListActionStatus.Success)
                {
                    if (maxAllowed > 0 || maxAllowed == Constants.NULL_INT)
                    {
                        Int32[] removedList = listInternal.Trim(category,
                            communityID,
                            siteID,
                            maxAllowed - 1);

                        Int32 i = 0;
                        for (Int32 removedNum = 0; removedNum < removedList.Length; removedNum++)
                        {
                            RemoveListMember(category,
                                communityID,
                                memberID,
                                removedList[removedNum]);
                            i++;

                            if (i >= 10)
                            {
                                break;
                            }
                        }
                    }

                    uri = getServiceManagerUri(memberID);

                    if (DateTime.MinValue != timeStamp)
                    {
                        getService(uri).AddListMember(System.Environment.MachineName,
                            category,
                            communityID,
                            siteID,
                            memberID,
                            targetMemberID,
                            actionDate,
                            comment,
                            teaseID,
                            timeStamp);
                    }
                    else
                    {
                        getService(uri).AddListMember(System.Environment.MachineName,
                            category,
                            communityID,
                            siteID,
                            memberID,
                            targetMemberID,
                            actionDate,
                            comment,
                            teaseID);
                    }

                    switch (listType)
                    {
                        case HotListType.Friend:
                            listInternal.AddListMember(category,
                                communityID,
                                targetMemberID,
                                actionDate,
                                comment,
                                true);
                            break;

                        case HotListType.Tease:
                            listInternal.AddListMember(category,
                                communityID,
                                targetMemberID,
                                actionDate,
                                teaseID,
                                true);
                            break;

                        default:
                            listInternal.AddListMember(category,
                                communityID,
                                targetMemberID,
                                actionDate,
                                comment,
                                true);
                            break;
                    }

                    //save reciprocal
                    if (saveReciprocal && listType == HotListType.Friend)
                    {
                        saveReciprocal = IsFavoritedNotificationEnabled(communityID);
                    }

                    if (direction == HotListDirection.OnYourList
                        && (
                        (listType == HotListType.Email || listType == HotListType.IM || listType == HotListType.Tease)
                            || (saveReciprocal && (listType == HotListType.Friend || listType == HotListType.ViewProfile || listType == HotListType.AllAccessEmail || listType == HotListType.ConsolidatedExclude || listType == HotListType.Spark))))
                    {
                        //ignoreQuota = true;
                        HotListCategory categoryReciprocal = ListInternal.MapListCategory(listType, HotListDirection.OnTheirList);
                        AddListMember(categoryReciprocal, communityID, siteID, targetMemberID, memberID, comment, teaseID, timeStamp, false, ignoreQuota);
                    }

                    // ExcludeListInternal updates
                    if (listType == HotListType.ExcludeList)
                    {
                        AddListMember(HotListCategory.ExcludeListInternal, communityID, siteID, memberID, targetMemberID, comment, teaseID, timeStamp, false, ignoreQuota);
                    }

                    if (listType == HotListType.ExcludeFromList)
                    {
                        AddListMember(HotListCategory.ExcludeListInternal, communityID, siteID, targetMemberID, memberID, comment, teaseID, timeStamp, false, ignoreQuota);
                    }

                    if (notificationsEnabled && AreNotificationsEnabled(communityID))
                    {
                        #region Hotlisted Email Alert Notification
                        if (category == HotListCategory.Default && saveReciprocal)
                        {
                            // Is this site configured to send out this alert?
                            bool send = Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(
                                "ENABLE_HOTLISTED_EMAIL_NOTIFICATION", communityID, siteID));

                            if (send)
                            {
                                Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                                Matchnet.Member.ServiceAdapters.Member targetMember = MemberSA.Instance.GetMember(targetMemberID, MemberLoadFlags.None);

                                // Get Brand information
                                int brandID = Constants.NULL_INT;
                                member.GetLastLogonDate(communityID, out brandID);
                                Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);

                                // If member has hidemask set for "Hide When You View or Hot List Members", then don't send email
                                if (!((member.GetAttributeInt(brand, "HideMask", 0) & HIDEMASK_HIDEHOTLISTS) == HIDEMASK_HIDEHOTLISTS))
                                {
                                    // Send out if recipient's seeking gender matches this member's gender
                                    if (GetGender(targetMember, brand) == GetSeekingGender(member, brand))
                                    {
                                        SendHotListNotification(Matchnet.List.ValueObjects.HotListCategory.Default, targetMemberID, memberID, brandID);
                                    }
                                }
                            }
                        }
                        #endregion

                        // Target member and brand info
                        Matchnet.Member.ServiceAdapters.Member targetMember2 = MemberSA.Instance.GetMember(targetMemberID, MemberLoadFlags.None);
                        int targetBrandID = Constants.NULL_INT;
                        targetMember2.GetLastLogonDate(communityID, out targetBrandID);
                        Brand targetBrand = BrandConfigSA.Instance.GetBrandByID(targetBrandID);

                        // Sender member and brand info
                        Matchnet.Member.ServiceAdapters.Member sendingMember = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                        int sendingBrandID = Constants.NULL_INT;
                        sendingMember.GetLastLogonDate(communityID, out sendingBrandID);
                        Brand sendingBrand = BrandConfigSA.Instance.GetBrandByID(sendingBrandID);

                        #region Messmo Messaging
                        // will not modify anything since Toolbar uses them as well.
                        if ((category == HotListCategory.Default || category == HotListCategory.MembersYouViewed) &&  sendingMember.EmailAddress != null)
                        {
                            if (saveReciprocal)
                            {
                                if (!((sendingMember.GetAttributeInt(sendingBrand, "HideMask", 0) & HIDEMASK_HIDEHOTLISTS) == HIDEMASK_HIDEHOTLISTS))
                                {
                                    SendMessmoMessage(communityID, targetBrandID, sendingBrandID, targetMemberID, memberID, category);
                                }
                            }
                        }
                        #endregion

                        #region Toolbar notification attribute
                        Constants.NotificationType notification = Constants.NotificationType.NoNewNotification;
                        if (targetBrand != null && sendingMember.EmailAddress != null) //only try to send the notification mask if the brand for the targetMemberId was actually found
                        {
                            if (saveReciprocal)
                            {
                                if (!((sendingMember.GetAttributeInt(targetBrand, "HideMask", 0) & HIDEMASK_HIDEHOTLISTS) == HIDEMASK_HIDEHOTLISTS))
                                {
                                    if (category == HotListCategory.Default)
                                        notification = Constants.NotificationType.HotList;
                                    else if (category == HotListCategory.MembersYouViewed)
                                        notification = Constants.NotificationType.ProfileViews;

                                    setNotificationMask(targetMember2.MemberID, targetBrand.Site.Community.CommunityID,
                                        targetBrand.Site.SiteID, targetBrandID, notification);
                                }
                            }
                        }

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot save member list (uri: " + uri + ")", ex));
            }

            return result;
        }
        
        /// <summary>
		/// Adds a member to a list.
		/// </summary>
		/// <param name="category"></param>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="memberID"></param>
		/// <param name="targetMemberID"></param>
		/// <param name="comment"></param>
		/// <param name="teaseID"></param>
		/// <param name="saveReciprocal"></param>
		/// <param name="ignoreQuota"></param>
		/// <param name="async"></param>
		/// <returns></returns>
		public ListSaveResult AddListMember(HotListCategory category,
			Int32 communityID,
			Int32 siteID,
			Int32 memberID,
			Int32 targetMemberID,
			string comment,
			Int32 teaseID,
            DateTime timeStamp,
			bool saveReciprocal,
			bool ignoreQuota)
		{
            return AddListMember(category,
                communityID,
                siteID,
                memberID,
                targetMemberID,
                comment,
                teaseID,
                timeStamp,
                saveReciprocal,
                ignoreQuota,
                true);
		}

        /// <summary>
        /// This method is kept for legacy purposes
        /// </summary>
        /// <returns></returns>
        public ListSaveResult AddListMember(HotListCategory category,
        Int32 communityID,
        Int32 siteID,
        Int32 memberID,
        Int32 targetMemberID,
        string comment,
        Int32 teaseID,
        bool saveReciprocal,
        bool ignoreQuota)
        {
            return AddListMember(category,
                communityID,
                siteID,
                memberID,
                targetMemberID,
                comment,
                teaseID,
                DateTime.MinValue,
                saveReciprocal,
                ignoreQuota,
                true);
        }

		public void SendHotListNotification(Matchnet.List.ValueObjects.HotListCategory cat, int targetMemberID, int memberID, int brandID)
		{
			string uri = "";

			try
			{
				uri = getServiceManagerUri(memberID);
				getService(uri).SendHotListNotification(cat,
					targetMemberID,
					memberID,
					brandID);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot send HotList notification (uri: " + uri + ")", ex));
			}
		}

		public void SendMessmoMessage(int communityID,
			int targetBrandID,
			int sendingBrandID,
			int targetMemberID,
			int memberID,
			Matchnet.List.ValueObjects.HotListCategory cat)
		{
			string uri = "";

			try
			{
				uri = getServiceManagerUri(memberID);
				getService(uri).SendMessmoMessage(communityID,
					targetBrandID,
					sendingBrandID,
					targetMemberID,
					memberID,
					cat);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot send Messmo Message (uri: " + uri + ")", ex));
			}
		}
		
		/// <summary>
		/// Removes a member from a list.
		/// </summary>
		/// <param name="category"></param>
		/// <param name="communityID"></param>
		/// <param name="memberID"></param>
		/// <param name="targetMemberID"></param>
		public void RemoveListMember(HotListCategory category,
			Int32 communityID,
			Int32 memberID,
			Int32 targetMemberID)
		{
			removeListMember(category,
				communityID,
				memberID,
				targetMemberID,
				true);
		}


        public void SaveViralStatus(int communityId, int memberId, int targetMemberId,
            bool alertEmailSent, bool clickEmailSent)
        {
            var uri = string.Empty;

            try
            {
                if (memberId < 1)
                {
                    throw new ArgumentException("value must be greater than zero", "memberId");
                }

                uri = getServiceManagerUri(memberId);

                getService(uri)
                    .SaveViralStatus(System.Environment.MachineName, communityId, memberId,
                        targetMemberId,
                        alertEmailSent, clickEmailSent);
            }
            catch (Exception ex)
            {
                throw new SAException("Error occured while saving viral status. (uri = " + uri + ")", ex);
            }
        }

		private void removeListMember(HotListCategory category,
			Int32 communityID,
			Int32 memberID,
			Int32 targetMemberID,
			bool manageInternalList)
        {
			string uri = "";

            try
            {
                HotListType listType;
                HotListDirection direction;



                var listInternal = getListInternal(memberID, ListLoadFlags.None);

                uri = getServiceManagerUri(memberID);
                getService(uri).RemoveListMember(Environment.MachineName,
                    category,
                    communityID,
                    memberID,
                    targetMemberID);

                listInternal.RemoveListMember(category,
                    communityID,
                    targetMemberID);

                //now get the target member's list
                listInternal = getListInternal(targetMemberID, ListLoadFlags.None);

                //map the reciprocal category
                ListInternal.MapListTypeDirection(category, out listType, out direction);
                category = ListInternal.MapListCategory(listType, HotListDirection.OnTheirList);

                //remove from target member's list
                listInternal.RemoveListMember(category,
                    communityID,
                    targetMemberID);

				if(manageInternalList)
                {
					if(category == HotListCategory.ExcludeFromList)
                    {
                        // check to see if the member is on the target member's ExcludeList
                        // if yes: target member does not want to see the member anyways. do nothing.
                        // if no: the member originally triggered the ExcludeListInternal add, so delete the member from the target member's ExcludeListInternal
						if(!GetList(targetMemberID).IsHotListed(HotListCategory.ExcludeList, communityID, memberID))
                        {
                            removeListMember(HotListCategory.ExcludeListInternal, communityID, targetMemberID, memberID, false);
                        }
                    }
					else if(category == HotListCategory.ExcludeList)
                    {
                        // check to see if the target member's ExcludeFromList contains the member
                        // if yes: target member does not want the member to see the target member, do nothing
                        // if no: member added the target member to his ExcludeListInternal, so remove
						if(!GetList(targetMemberID).IsHotListed(HotListCategory.ExcludeFromList, communityID, memberID))
                        {
                            removeListMember(HotListCategory.ExcludeListInternal, communityID, memberID, targetMemberID, false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
				throw(new SAException("Cannot retrieve member list (uri: " + uri + ")", ex));
            }
        }

	    /// <summary>
	    /// Adds/updates a YNM vote.
	    /// </summary>
	    /// <param name="communityID"></param>
	    /// <param name="siteID"></param>
	    /// <param name="languageID"></param>
	    /// <param name="memberID"></param>
	    /// <param name="targetMemberID"></param>
	    /// <param name="type"></param>
	    public void AddClick(Int32 communityID,
	        Int32 siteID,
	        Int32 languageID,
	        Int32 memberID,
	        Int32 targetMemberID,
	        ClickListType type)
	    {
            DateTime actionDate = DateTime.Now;
            AddClick(communityID, siteID, languageID, memberID, targetMemberID, type, actionDate);
	    }

	    /// <summary>
		/// Adds/updates a YNM vote.
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="languageID"></param>
		/// <param name="memberID"></param>
		/// <param name="targetMemberID"></param>
		/// <param name="type"></param>
		public void AddClick(Int32 communityID,
			Int32 siteID,
			Int32 languageID,
			Int32 memberID,
			Int32 targetMemberID,
            ClickListType type, DateTime actionDate)
		{
			string uri = "";

			try
			{
				if (memberID < 1)
				{
					throw new ArgumentException("value must be greater than zero", "memberID");
				}

				if (targetMemberID < 1)
				{
					throw new ArgumentException("value must be greater than zero", "targetMemberID");
				}

				uri = getServiceManagerUri(memberID);

				getService(uri).AddClick(System.Environment.MachineName,
					communityID,
					siteID,
					memberID,
					targetMemberID,
					ClickDirectionType.Sender,
					type,
					actionDate);

				uri = "";

				//update local cached list
				List list = new List(getListInternal(memberID, ListLoadFlags.None));

				list.AddClick(communityID,
					siteID,
					targetMemberID,
					ClickDirectionType.Sender,
					type,
					actionDate,
					true);

				//update remote cached list/db
				uri = getServiceManagerUri(targetMemberID);
				getService(uri).AddClick(System.Environment.MachineName,
					communityID,
					siteID,
					targetMemberID,
					memberID,
					ClickDirectionType.Recipient,
					type,
					actionDate);

				//update local cached recipient list
				ListInternal listInternalInverse = _cache.Get(ListInternal.GetCacheKey(targetMemberID)) as ListInternal;
				if (listInternalInverse != null)
				{
					List listInverse = new List(listInternalInverse);
					listInverse.AddClick(communityID,
						siteID,
						memberID,
						ClickDirectionType.Recipient,
						type,
						actionDate,
						true);
				}

                if (AreNotificationsEnabled(communityID))
                {
				    //determine mutual yes
				    ClickMask clickMask = list.GetClickMask(communityID,
					    siteID,
					    targetMemberID);
				    if ((clickMask & ClickMask.MemberYes) == ClickMask.MemberYes
					    && (clickMask & ClickMask.TargetMemberYes) == ClickMask.TargetMemberYes)
				    {
					    Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
					    member.SetAttributeInt(communityID,
						    Constants.NULL_INT,
						    Constants.NULL_INT,
						    "HasNewMutualYes",
						    1);
					    MemberSA.Instance.SaveMember(member);

					    Matchnet.Member.ServiceAdapters.Member targetMember = MemberSA.Instance.GetMember(targetMemberID, MemberLoadFlags.None);
					    targetMember.SetAttributeInt(communityID,
						    Constants.NULL_INT,
						    Constants.NULL_INT,
						    "HasNewMutualYes", 
						    1);
					    MemberSA.Instance.SaveMember(targetMember);

				        Int32 brandID;
				        if (member.IsEligibleForEmail(communityID))
				        {
				            member.GetLastLogonDate(communityID, out brandID);
				            SendHotListNotification(Matchnet.List.ValueObjects.HotListCategory.MutualYes, targetMemberID, memberID,
				                brandID);
				        }

				        if (targetMember.IsEligibleForEmail(communityID))
				        {
				            targetMember.GetLastLogonDate(communityID, out brandID);
				            SendHotListNotification(Matchnet.List.ValueObjects.HotListCategory.MutualYes, memberID, targetMemberID,
				                brandID);
				        }
				    }
                }
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve member list (uri: " + uri + ")", ex));
			}
		}


		/// <summary>
		/// Deletes a hot list category.
		/// </summary>
		/// <param name="categoryID">The Id of the category to delete.</param>
		/// <param name="memberID">The member Id of the user that is deleting the category.</param>
		/// <param name="communityID"></param>
		public void DeleteListCategory(Int32 categoryID,
			Int32 communityID,
			Int32 memberID)
		{
			string uri = "";

			try
			{
				if (memberID < 1)
				{
					throw new ArgumentException("value must be greater than zero", "memberID");
				}

				uri = getServiceManagerUri(memberID);
				getService(uri).DeleteListCategory(System.Environment.MachineName,
					categoryID,
					communityID,
					memberID);

				GetCustomListCategories(memberID, communityID).Remove(categoryID);
			}
			catch (Exception ex)
			{
				throw new SAException("Error occured while deleting category. (uri = " + uri + ")", ex);
			}
		}


		/// <summary>
		/// Retrieves a list of custom user-created list names.
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <returns></returns>
		public CustomCategoryCollection GetCustomListCategories(Int32 memberID,
			Int32 communityID)
		{
			string uri = "";

			try
			{
				if (memberID < 1)
				{
					throw new ArgumentException("value must be greater than zero", "memberID");
				}

				CustomCategoryCollection categories = _cache.Get(CustomCategoryCollection.GetCacheKey(memberID, communityID)) as CustomCategoryCollection;

				if (categories == null)
				{
					Int32 cacheTTL = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("LISTSVC_CACHE_TTL_SA"));

					uri = getServiceManagerUri(memberID);
					categories = getService(uri).GetCustomListCategories(System.Environment.MachineName,
						Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
						memberID,
						communityID);
					categories.CacheMode = Matchnet.CacheItemMode.Absolute;
					categories.CacheTTLSeconds = cacheTTL;
					_cache.Insert(categories);
				}

				return categories;
			}
			catch (Exception ex)
			{
				throw new SAException("Error retrieving custom list categories. (uri = " + uri + ")", ex);
			}
		}


		/// <summary>
		/// Creates a custom hotlist category.
		/// </summary>
		/// <param name="categoryID"></param>
		/// <param name="communityID"></param>
		/// <param name="memberID"></param>
		/// <param name="description"></param>
		public CustomCategoryResult SaveListCategory(Int32 categoryID,
			Int32 communityID,
			Int32 memberID,
			string description)
		{
			string uri = "";

			try
			{
				if (memberID < 1)
				{
					throw new ArgumentException("value must be greater than zero", "memberID");
				}

				CustomCategoryCollection categories = GetCustomListCategories(memberID, communityID);

				//check quota
				if (categories.Count >= QuotaDefinitionSA.Instance.GetQuotaDefinitions().GetQuotaDefinition(QuotaType.CustomListCategories, communityID).MaxAllowed)
				{
					return new CustomCategoryResult(CustomCategoryResultStatus.MaximumCategoriesReached,
						Constants.NULL_INT);
				}

				//check for dupe name
				if (categories.ContainsCategory(description))
				{
					return new CustomCategoryResult(CustomCategoryResultStatus.ExistingCategoryName,
						Constants.NULL_INT);
				}

				//get PK if category is new
				if (categoryID == Constants.NULL_INT)
				{
					categoryID = KeySA.Instance.GetKey("CategoryID");
				}

				uri = getServiceManagerUri(memberID);
				getService(uri).SaveListCategory(System.Environment.MachineName,
					categoryID,
					communityID,
					memberID,
					description);

				categories.Add(new CustomCategory(categoryID, description));

				return new CustomCategoryResult(CustomCategoryResultStatus.Success, categoryID);
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot save list category (uri: " + uri + ")", ex));
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteID"></param>
		/// <returns></returns>
		public StandardCategoryCollection GetStandardListCategories(Int32 siteID)
		{
			string uri = "";

			try
			{
				StandardCategoryCollection standardCategories = _cache.Get(StandardCategoryCollection.GetCacheKey(siteID)) as StandardCategoryCollection;

				if (standardCategories == null)
				{
					uri = getServiceManagerUri();
					standardCategories = getService(uri).GetStandardListCategories(siteID);
					_cache.Insert(standardCategories);
				}

				return standardCategories;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve standard list categories (uri: " + uri + ").", ex));
			}
		}


		public ClickMask GetClickMask(Int32 memberID, Int32 targetMemberID, Int32 communityID, Int32 siteID)
		{
			ClickMask clickMask = ClickMask.None;
			string uri = "";

			try
			{
				uri = getServiceManagerUri(memberID);

				clickMask = getService(uri).GetClickMask(memberID, targetMemberID, communityID, siteID);
			}
			catch (Exception ex)
			{
				new SAException("Cannot retrieve click vote (uri: " + uri + ")", ex);
			}

			return clickMask;
		}


		public ListInternal GetListInternalMutualYes(Int32 memberID, Int32 communityID)
		{
			string uri = "";

			try
			{
				uri = getServiceManagerUri(memberID);

				return getService(uri).GetListInternalMutualYes(memberID, communityID);
			}
			catch (Exception ex)
			{
				throw new SAException("Cannot retrieve click vote (uri: " + uri + ")", ex);
			}
		}



		

		private ListInternal getListInternal(Int32 memberID,
			ListLoadFlags loadFlags)
		{
			ListInternal listInternal = null;
			string uri = "";

			try
			{
				Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
 
				if ((loadFlags & ListLoadFlags.IngoreSACache) != ListLoadFlags.IngoreSACache)
				{
					listInternal = _cache.Get(ListInternal.GetCacheKey(memberID)) as ListInternal;
				}

				if (listInternal == null)
				{
					uri = getServiceManagerUri(memberID);

					Int32 cacheTTL = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("LISTSVC_CACHE_TTL_SA"));

					//DateTime beginTime = DateTime.Now;
					listInternal = getService(uri).GetListInternal(System.Environment.MachineName,
						Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
						memberID,
						member.GetCommunityIDList());
					/*
					Double diff = DateTime.Now.Subtract(beginTime).TotalMilliseconds;

					if (diff > 2000)
					{
						System.Diagnostics.Trace.WriteLine("__getListInternal(" + memberID.ToString() + ") " + diff.ToString());
					}
					*/

					listInternal.CacheMode = Matchnet.CacheItemMode.Absolute;
					listInternal.CacheTTLSeconds = cacheTTL;
					_cache.Insert(listInternal);
				}

			}
			catch (Exception ex)
			{
				new SAException("Cannot retrieve member list (uri: " + uri + ")", ex);
				return new ListInternal(memberID);
			}

			return listInternal;
		}


		private IListService getService(string uri)
		{
			try
			{
				return (IListService)Activator.GetObject(typeof(IListService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}


		private string getServiceManagerUri(int memberID)
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Calculated, memberID).ToUri(SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("LISTSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}


		private string getServiceManagerUri()
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("LISTSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}


		/// <summary>
		/// Determines if a HotListID is for a voluntary list (ie default, blocked, or a custom list)
		/// </summary>
		/// <param name="listID"></param>
		/// <returns></returns>
		public static bool IsListVoluntaryList(int listID)
		{
			bool retVal = false;
		
			if(listID >= 0)
			{
				//	Favorites or custom
				retVal = true;
			} 

			return(retVal);
		}

		
		private GenderMask GetSeekingGender(Matchnet.Member.ServiceAdapters.Member member, Brand brand) 
		{
			Int32 genderMask = member.GetAttributeInt(brand, "GenderMask", Constants.NULL_INT);
			
			if ((genderMask & (Int16)GenderMask.SeekingMale) == (Int16)GenderMask.SeekingMale) 
				return GenderMask.Male;
			else if ((genderMask & (Int16)GenderMask.SeekingFemale) == (Int16)GenderMask.SeekingFemale) 
				return GenderMask.Female;
			else if ((genderMask & (Int16)GenderMask.SeekingFTM) == (Int16)GenderMask.SeekingFTM) 
				return GenderMask.FTM;
			else if ((genderMask & (Int16)GenderMask.SeekingMTF) == (Int16)GenderMask.SeekingMTF) 
				return GenderMask.MTF;
			else
				return GenderMask.Female;
		}

		private GenderMask GetGender(Matchnet.Member.ServiceAdapters.Member member, Brand brand) 
		{
			Int32 genderMask = member.GetAttributeInt(brand, "GenderMask", Constants.NULL_INT);

			if ((genderMask & (Int16)GenderMask.Male) == (Int16)GenderMask.Male)
				return GenderMask.Male;
			else if ((genderMask & (Int16)GenderMask.Female) == (Int16)GenderMask.Female)
				return GenderMask.Female;
			else if ((genderMask & (Int16)GenderMask.FTM) == (Int16)GenderMask.FTM)
				return GenderMask.FTM;
			else if ((genderMask & (Int16)GenderMask.MTF) == (Int16)GenderMask.MTF)
				return GenderMask.MTF;
			else
				return GenderMask.Female;
		}

		#region toolbarnotification
		private void setNotificationMask(int memberid, int communityid, int siteid, int brandid, Constants.NotificationType notificationtype)
		{
			try
			{
				Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
				int notificationMask=member.GetAttributeInt(communityid,siteid,brandid,"HasNewNotificationMask",0);
				if((notificationMask & (int) notificationtype) != (int) notificationtype)
				{
					notificationMask=notificationMask | (int) notificationtype;
					member.SetAttributeInt(communityid,siteid,brandid,"HasNewNotificationMask",notificationMask);
					Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);
				}
			}
			catch(Exception ex)
			{
				ex=null;
			}
		}
		
		#endregion
		
		private bool IsHotListedMember(int targetMemberID, int senderMemberID, Brand brand)
		{
			bool result = false;

            IHotList list = GetList(senderMemberID, ListLoadFlags.None);
			if (list == null)
			{
			    throw new Exception("Could not load hotlist.  Cannot continue");
			}

			result = list.IsHotListed(HotListCategory.WhoAddedYouToTheirFavorites, brand.Site.Community.CommunityID, targetMemberID);
			if(result == false)
			{
				list = GetList(targetMemberID, ListLoadFlags.None);
				CustomCategoryCollection ccc = GetCustomListCategories(targetMemberID, brand.Site.Community.CommunityID);
				if(ccc.Count>0)
				{
					foreach(CustomCategory cc in ccc)
					{
						result = list.IsHotListed((HotListCategory)cc.CategoryID, brand.Site.Community.CommunityID, senderMemberID);
						if(result == true)
						{
							break;
						}
					}
				}
			}

			return result;
		}
		private bool IsEmailedMember(int targetMemberID, int senderMemberID, Brand brand)
		{
			bool result = false;

            IHotList list = GetList(targetMemberID, ListLoadFlags.None);
			
			result = list.IsHotListed(HotListCategory.MembersYouEmailed, brand.Site.Community.CommunityID, senderMemberID);
			
			return result;
		}
	}
}
