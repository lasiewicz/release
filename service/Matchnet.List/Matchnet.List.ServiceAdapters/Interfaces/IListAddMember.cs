﻿#region

using System;
using Matchnet.List.ValueObjects;

#endregion

namespace Matchnet.List.ServiceAdapters.Interfaces
{
    /// <summary>
    ///     Using an adapter to not break the existing interface. Adding for DI
    /// </summary>
// ReSharper disable once InconsistentNaming
    public interface IListAddMember : IListSA
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="category"></param>
        /// <param name="communityID"></param>
        /// <param name="siteID"></param>
        /// <param name="memberID"></param>
        /// <param name="targetMemberID"></param>
        /// <param name="comment"></param>
        /// <param name="teaseID"></param>
        /// <param name="saveReciprocal"></param>
        /// <param name="ignoreQuota"></param>
        /// <returns></returns>
        ListSaveResult AddListMember(HotListCategory category,
            Int32 communityID,
            Int32 siteID,
            Int32 memberID,
            Int32 targetMemberID,
            string comment,
            Int32 teaseID,
            bool saveReciprocal,
            bool ignoreQuota);
    }
}