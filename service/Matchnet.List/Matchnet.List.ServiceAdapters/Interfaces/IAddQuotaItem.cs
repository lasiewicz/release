﻿#region

using System;
using Matchnet.Content.ValueObjects.Quotas;

#endregion

namespace Matchnet.List.ServiceAdapters.Interfaces
{
    /// <summary>
    ///     Added for implementing DI
    /// </summary>
    public interface IAddQuotaItem
    {
        void AddQuotaItem(Int32 communityID, Int32 memberID, QuotaType quotaType);
    }
}