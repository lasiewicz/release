﻿#region

using System;
using Matchnet.List.ValueObjects;

#endregion

namespace Matchnet.List.ServiceAdapters.Interfaces
{
    public interface IListSA
    {
        List GetList(Int32 memberID);

        void AddClick(Int32 communityID,
            Int32 siteID,
            Int32 languageID,
            Int32 memberID,
            Int32 targetMemberID,
            ClickListType type);
    }
}