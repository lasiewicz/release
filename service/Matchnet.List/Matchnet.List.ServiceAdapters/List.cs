using System;
using System.Collections;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.List.ValueObjects;
using Matchnet.List.ValueObjects.ServiceDefinitions;


namespace Matchnet.List.ServiceAdapters
{
	/// <summary>
	/// Representation of list entries
	/// </summary>
    public class List : IHotList
	{
		ListInternal _listInternal;

		internal List(ListInternal listInternal)
		{
			_listInternal = listInternal;
			_listInternal.Sort();
		}


		/// <summary>
		/// Determines if a member has been added to a list.
		/// </summary>
		/// <param name="category"></param>
		/// <param name="communityID"></param>
		/// <param name="targetMemberID"></param>
		/// <returns></returns>
		public bool IsHotListed(HotListCategory category,
			int communityID,
			int targetMemberID)
		{
			return _listInternal.IsHotListed(category,
				communityID,
				targetMemberID);
		}


		public void Sort(HotListCategory category, int communityID, int siteID)
		{

			try
			{
				_listInternal.Sort(category,communityID, siteID);

				}
			catch(Exception ex)
			{}
		}

		

		public bool IsAtQuota(Int32 communityID,
			HotListType hotListType)
		{
			//ListQuota listQuota;

			//todo: all types
			switch (hotListType)
			{
				case HotListType.Email:
					/*
					listQuota = ListSA.Instance.GetListQuotas().GetQuota(communityID, HotListType.Email);
					return _listInternal.QuotaCheck(HotListCategory.MembersYouEmailed,
						communityID,
						Constants.NULL_INT,
						DateTime.Now.AddDays(-listQuota.MaxAllowedDuration),
						listQuota.MaxAllowed);
					*/
					break;
			}

			return false;
		}
		
		/// <summary>
		/// Determines if a member has been added to a list.
		/// </summary>
		/// <param name="hotListType"></param>
		/// <param name="direction"></param>
		/// <param name="communityID"></param>
		/// <param name="targetMemberID"></param>
		/// <returns></returns>
		public bool IsHotListed(HotListType hotListType,
			HotListDirection direction,
			int communityID,
			int targetMemberID)
		{
			HotListCategory category = ListInternal.MapListCategory(hotListType, direction);

			return _listInternal.IsHotListed(category,
				communityID,
				targetMemberID);
		}


		/// <summary>
		/// Retreives details of a list entry.
		/// </summary>
		/// <param name="category"></param>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="targetMemberID"></param>
		/// <returns></returns>
		public ListItemDetail GetListItemDetail(HotListCategory category,
			int communityID,
			int siteID,
			int targetMemberID)
		{
			return _listInternal.GetListItemDetail(category,
				communityID,
				siteID,
				targetMemberID);
		}


		/// <summary>
		/// Retreives a collection of member IDs that belong to a list.
		/// </summary>
		/// <param name="category"></param>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="startRow"></param>
		/// <param name="pageSize"></param>
		/// <param name="rowCount"></param>
		/// <returns>ArrayList of member IDs</returns>
		public ArrayList GetListMembers(HotListCategory category,
			int communityID, 
			int siteID,
			int startRow,
			int pageSize,
			out int rowCount)
		{
			_listInternal.Sort(category, communityID,  siteID);
			if (category == HotListCategory.MutualYes && !_listInternal.MutualYesLoaded)
			{
				_listInternal = ListSA.Instance.GetListInternalMutualYes(_listInternal.MemberID, communityID);
			}

			return _listInternal.GetListMembers(category,
				communityID, 
				siteID,
				startRow,
				pageSize,
				out rowCount);
		}

		/// <summary>
		/// It returns the first list the target member is on. If the target member is on multiple lists, this might not be the right behavior you want.
		/// </summary>
		/// <param name="targetMemberID"></param>
		/// <param name="communityID"></param>
		/// <returns></returns>
		public Int32 DetermineVoluntaryList(Int32 targetMemberID, Int32 communityID)
		{
			int retVal = Constants.NULL_INT;

			CustomCategoryCollection customCategories = ListSA.Instance.GetCustomListCategories(_listInternal.MemberID, communityID);
			for (Int32 categoryNum = 0; categoryNum < customCategories.Count; categoryNum++)
			{
				CustomCategory category = customCategories[categoryNum];

				if(IsHotListed((HotListCategory)category.CategoryID, communityID, targetMemberID))
				{
					retVal = category.CategoryID;
					break;
				}
			}

			if(IsHotListed(Matchnet.List.ValueObjects.HotListCategory.IgnoreList, communityID, targetMemberID))
			{
				retVal = (int)Matchnet.List.ValueObjects.HotListCategory.IgnoreList;
			} 
			else if(IsHotListed(Matchnet.List.ValueObjects.HotListCategory.Default, communityID, targetMemberID))
			{
				retVal = (int)Matchnet.List.ValueObjects.HotListCategory.Default;
			}
			else if(IsHotListed(Matchnet.List.ValueObjects.HotListCategory.ExcludeFromList, communityID, targetMemberID))
			{
				retVal = (int)Matchnet.List.ValueObjects.HotListCategory.ExcludeFromList;
			}
			else if(IsHotListed(Matchnet.List.ValueObjects.HotListCategory.ExcludeList, communityID, targetMemberID))
			{
				retVal = (int)Matchnet.List.ValueObjects.HotListCategory.ExcludeList;
			}

			return retVal;
		}


		public bool IsMemberOfVoluntaryList(Int32 targetMemberID, Int32 communityID)
		{
			int listID;

			listID = DetermineVoluntaryList(targetMemberID, communityID);
			if(listID != Constants.NULL_INT)
			{
				return true;
			}
			
			return false;
		}


		/// <summary>
		/// Retreives the count of members that belong to a list.
		/// </summary>
		/// <param name="category"></param>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		public Int32 GetCount(HotListCategory category,
			Int32 communityID,
			Int32 siteID)
		{
			if (category == HotListCategory.MutualYes && !_listInternal.MutualYesLoaded)
			{
				_listInternal = ListSA.Instance.GetListInternalMutualYes(_listInternal.MemberID, communityID);
			}

			return _listInternal.GetCount(category,
				communityID,
				siteID);
		}


		/// <summary>
		/// Retreives the vote status of a given member.
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="targetMemberID"></param>
		/// <returns></returns>
		public ClickMask GetClickMask(int communityID,
			int siteID,
			int targetMemberID)
		{
			bool voteFound;
			ClickMask cachedClickMask = _listInternal.GetClickMask(communityID, siteID, targetMemberID, out voteFound);

			if (!voteFound && !_listInternal.YNMVotesLoaded)
			{
				return ListSA.Instance.GetClickMask(_listInternal.MemberID, targetMemberID, communityID, siteID);
			}

			return cachedClickMask;
		}

		public void AddClick(Int32 communityID,
			Int32 siteID,
			Int32 targetMemberID,
			ClickDirectionType direction,
			ClickListType voteType,
			DateTime actionDate,
			bool sort)
		{
			//Make sure we have the ClickMask for this user cached locally before we try to update it
			GetClickMask(communityID, siteID, targetMemberID);

			_listInternal.AddClick(communityID,
				siteID,
				targetMemberID,
				direction,
				voteType,
				actionDate,
				sort);
		}
	}
}
