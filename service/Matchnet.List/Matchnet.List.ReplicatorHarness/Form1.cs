using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using Matchnet.Replication;

namespace Matchnet.List.ReplicatorHarness
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(8, 8);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(280, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "start replicator";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 37);
			this.Controls.Add(this.button1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private const string SERVICE_MANAGER_NAME = "ListSM";
		public const string SERVICE_CONSTANT = "LIST_SVC";
		public const string SERVICE_NAME = "Matchnet.List.Service";
		private bool _running = false;
		private Replicator _replicator;

		private void button1_Click(object sender, System.EventArgs e)
		{
			if (!_running)
			{
				button1.Enabled = false;
				string replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(SERVICE_CONSTANT, SERVICE_MANAGER_NAME, System.Environment.MachineName);
				_replicator = new Replicator(SERVICE_NAME);
				_replicator.SetDestinationUri(replicationURI);
				_replicator.Start();
				button1.Text = "stop replicator";
				_running = true;
				button1.Enabled = true;
			}
			else
			{
				button1.Enabled = false;
				_replicator.Stop();
				button1.Text = "start replicator";
				_running = false;
				button1.Enabled = true;
			}
		}	
	}
}
