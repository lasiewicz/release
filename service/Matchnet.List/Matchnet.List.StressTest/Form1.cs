using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Threading;

using System.Xml;
using Matchnet.List.ValueObjects;
using Matchnet.List.ServiceDefinitions;
using System.IO;


namespace Matchnet.List.StressTest
{
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			InitializeComponent();
		}


		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(8, 8);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			this.button1.Text = "load";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(96, 8);
			this.button2.Name = "button2";
			this.button2.TabIndex = 1;
			this.button2.Text = "save";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		string[] _memberIDs;


		private void button1_Click(object sender, System.EventArgs e)
		{
			StreamReader sr = new StreamReader(System.Environment.CurrentDirectory + @"\mbrs.txt");
			_memberIDs = sr.ReadToEnd().Replace("\r","").Split('\n');
			sr.Close();
			Thread[] t = new Thread[20];

			for (Int32 i = 0; i < t.Length; i++)
			{
				t[i] = new Thread(new ThreadStart(loadCycle));
				t[i].Start();
			}
		}

		private Int32 getMemberID()
		{
			string s = _memberIDs[new System.Random().Next(0, _memberIDs.Length - 2)];
			return Convert.ToInt32(s);
		}

		private void loadCycle()
		{
			while (true)
			{
				try
				{
					Int32 memberID = getMemberID();

					IListService listService = (IListService)Activator.GetObject(typeof(IListService), "tcp://172.16.1.131:43000/ListSM.rem");
					listService.GetListInternal(null,
						null,
						memberID,
						new Int32[]{1,2,3,10,12});

					listService.GetCustomListCategories(null,null,memberID,12);

					Thread.Sleep(400);

					memberID = getMemberID();

					listService = (IListService)Activator.GetObject(typeof(IListService), "tcp://172.16.1.132:43000/ListSM.rem");
					listService.GetListInternal(null,
						null,
						memberID,
						new Int32[]{1,2,3,10,12});

					listService.GetCustomListCategories(null,null,memberID,12);

					Thread.Sleep(400);

					/*
					listService = (IListService)Activator.GetObject(typeof(IListService), "tcp://172.16.0.32:43000/ListSM.rem");
					listService.GetListInternal(null,
						null,
						getMemberID(),
						new Int32[]{2,10,12},
						false);
					*/
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.ToString());
				}
			}
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			Thread[] t = new Thread[20];

			for (Int32 i = 0; i < t.Length; i++)
			{
				t[i] = new Thread(new ThreadStart(saveCycle));
				t[i].Start();
			}
		}

		private void saveCycle()
		{
			ClickListType clickType = ClickListType.No;

			while (true)
			{
				try
				{

					IListService listService = (IListService)Activator.GetObject(typeof(IListService), "tcp://172.16.1.131:43000/ListSM.rem");

					listService.AddListMember(null,
						HotListCategory.Default,
						12,
						112,
						8578217,
						10385796,
						DateTime.Now,
						Guid.NewGuid().ToString(),
						Constants.NULL_INT, DateTime.MinValue);


					listService.AddClick(null,
						12,
						112,
						8578217,
						10385796,
						ClickDirectionType.Sender,
						clickType,
						DateTime.Now);

					Thread.Sleep(500);

					listService = (IListService)Activator.GetObject(typeof(IListService), "tcp://172.16.1.132:43000/ListSM.rem");

					listService.AddListMember(null,
						HotListCategory.Default,
						12,
						112,
						8578217,
						10385796,
						DateTime.Now,
						Guid.NewGuid().ToString(),
						Constants.NULL_INT, 
                        DateTime.MinValue);


					listService.AddClick(null,
						12,
						112,
						8578217,
						10385796,
						ClickDirectionType.Sender,
						clickType,
						DateTime.Now);

					Thread.Sleep(500);

					/*
					listService = (IListService)Activator.GetObject(typeof(IListService), "tcp://172.16.0.32:43000/ListSM.rem");
					listService.AddListMember(null,
						HotListCategory.Default,
						12,
						112,
						8578217,
						10385796,
						DateTime.Now,
						Guid.NewGuid().ToString(),
						Constants.NULL_INT,
						false);

					listService.AddClick(null,
						12,
						112,
						8578217,
						10385796,
						ClickDirectionType.Sender,
						clickType,
						DateTime.Now);
					*/

					if (clickType == ClickListType.No)
					{
						clickType = ClickListType.Maybe;
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.ToString());
				}
			}
		}
	}
}
