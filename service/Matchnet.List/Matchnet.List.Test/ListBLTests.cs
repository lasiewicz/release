﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading;
using NUnit.Framework;
using Matchnet.List.ValueObjects;
using Matchnet.List.BusinessLogic;
using Matchnet.Member.ServiceAdapters.Mocks;
using Matchnet.Search.ValueObjects;
using NUnit.Framework.Constraints;

namespace Matchnet.List.Test
{
    [TestFixture]
    public class ListBLTests
    {
        private const string ClientHostName = "ListBLTester";
        private int _communityId;
        private int _siteId;
        private int _teaseId;
        private int _memberId;
        private int _targetMemberId;
        private IEnumerable<int> _membersInPartition;
        private Stack<int> _stack;
        private bool _verboseOutput;
        private Random _random = new Random();
        
        [TestFixtureSetUp]
        public void SetUp()
        {
            ListBL.Instance.NUnit = true;
            const string membersInPartition1 = "456,870400,872012,4044138,6087252,7475716,8754720,10059924,16000084,16000114,16000214,23247084,23247094,23247492,23247500,23247520,23247590,23247594,23247678,23247684,23247686,23247688,23247690,23247692,23247696,23247698,23247702,23247704,23247706,23247708,23247712,23247714,23247718,23247720,23247722,23247724,23247728,23247730,23247732,23247736,23247738,23247740,23247742,23247748,23247750,23247752,23247754,23247756,23247758,23247760,23247762,23247764,23247766,23247768,23247770,23247778,23247780,23247786,23247808,23247810,23247812,23247814,23247816,23247818,23247820,23247822,23247824,23247826,23247828,23247846,23247848,23247852,23247854,23247856,23247858,23247860,23247862,23247864,23247866,23247868,23247870,23247884,23247890,23247894,23247906,23247910,23247916,23247918,23247920,23247922,23247926,23247932,23247934,23247936,23247938,23247940,23247942,23247944,23247946,23247948,23247950,23247952,23247954,23247956,23247958,23247960,23247962,23247964,23247968,23247970,23247972,23247974,23247980,23247986,23247988,23247990,23247992,23247994,23247996,23247998,23248006,23248014,23248016,23248018,23248020,23248022,23248026,23248028,23248044,23248046,23248048,23248050,23248052,23248060,23248064,23248080,23248100,23248102,23248138,23248160,23248172,23248174,23248176,23248180,23248184,23248188,23248190,23248192,23248196,23248200,23248202,23248204,23248206,23248208,23248212,23248214,23248216,23248218,23248222,23248234,23248236,23248238,23248250,23248252,23248254,23248260,23248262,23248270,23248272,23248274,23248276,23248278,23248280,23248282,23248284,23248286,23248288,23248290,23248292,23248294,23248296,23248298,23248300,23248302,23248304,23248306,23248308,23248310,23248312,23248314,23248316,23248318,23248320,23248322,23248324,23248326,23248328,23248332,23248334,23248336,23248338,23248340,23248342,23248344,23248346,23248348,23248350,23248352,23248354,23248356,23248358,23248360,23248362,23248364,23248366,23248368,23248370,23248372,23248374,23248376,23248378,23248380,23248382,23248384,23248386,23248388,23248390,23248392,23248394,23248396,23248398,23248400,23248402,23248404,23248406,23248408,23248410,23248412,23248414,23248416,23248418,23248422,23248428,23248430,23248434,23248436,23248438,23248440,23248442,23248444,23248446,23248454,23248456,23248458,23248460,23248464,23248466,23248476,23248478,23248480,23248484,23248486,23248488,23248492,23248494,23248498,23248500,23248502,23248510,23248512,23248514,23248516,23248518,23248520,23248522,23248524,23248526,23248530,23248532,23248534,23248550,23248552,23248554,23248556,23248558,23248564,23248566,23248570,23248578,23248584,23248588,23248590,23248592,23248596,23248600,23248604,23248638,23248642,23248670,23248678,23248708,23248714,23248726,23248730,23248736,23248758,23248770,23248772,23248774,23248776,23248800,23248824,23248848,23248872,23248896,23248920,23248944,23248968,23248992,23249016,23249040,23249064,23249088,23249112,23249136,23249160,23249184,23249208,23249232,23249256,23249280,23249304,23249328,23249352,2324937";

            _stack = new Stack<int>();
            _communityId = 3;
            _siteId = 103;
            _teaseId = 1;
            _memberId = 16000210; //16000206

            _targetMemberId = 16000208;
            _verboseOutput = false;

            (membersInPartition1 ?? "").Split(',').Select<string, int>(int.Parse).ToList().ForEach(p=>_stack.Push(p));
            _membersInPartition = (membersInPartition1 ?? "").Split(',').Select<string, int>(int.Parse);

            ListBL.Instance.ListSaved += Instance_ListSaved;
            ListBL.Instance.ListAdded += Instance_ListAdded;
            ListBL.Instance.ListRemoved += Instance_ListRemoved;
            ListBL.Instance.ListRequested += Instance_ListRequested;
            ListBL.Instance.ReplicationActionRequested += Instance_ReplicationActionRequested;
            ListBL.Instance.ReplicationRequested += Instance_ReplicationRequested;
            ListBL.Instance.SynchronizationRequested += Instance_SynchronizationRequested;


        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            ListBL.Instance.NUnit = false;
            ListBL.Instance.ListSaved -= Instance_ListSaved;
            ListBL.Instance.ListAdded -= Instance_ListAdded;
            ListBL.Instance.ListRemoved -= Instance_ListRemoved;
            ListBL.Instance.ListRequested -= Instance_ListRequested;
            ListBL.Instance.ReplicationActionRequested -= Instance_ReplicationActionRequested;
            ListBL.Instance.ReplicationRequested -= Instance_ReplicationRequested;
            ListBL.Instance.SynchronizationRequested -= Instance_SynchronizationRequested;
        }


        [Test]
        
        public void AddListMember_Fail()
        {
            var initialTotalCount = 0;
            var results = ListBL.Instance.GetListInternal(ClientHostName, null, _memberId, new[] { _communityId });

            initialTotalCount = results.TotalCount;

            ListBL.Instance.AddListMember(ClientHostName, HotListCategory.Default, _communityId, _siteId, _memberId, 0, DateTime.UtcNow, GenerateRandomComment(), _teaseId, DateTime.UtcNow);

            results = ListBL.Instance.GetListInternal(ClientHostName, null, _memberId, new[] { _communityId });

            Assert.IsTrue(results.TotalCount == initialTotalCount);
        }


        [Test]
        public void AddListMember_Success()
        {
            var initialTotalCount = 0;
            var results = ListBL.Instance.GetListInternal(ClientHostName, null, _memberId, new[] { _communityId });
            ListBL.Instance.RemoveListMember(ClientHostName, HotListCategory.Default, _communityId, _memberId, _targetMemberId);

            Thread.Sleep(1000);

            initialTotalCount = results.TotalCount;

            ListBL.Instance.AddListMember(ClientHostName, HotListCategory.Default, _communityId, _siteId, _memberId, _targetMemberId, DateTime.UtcNow, GenerateRandomComment(), _teaseId, DateTime.UtcNow);

            results = ListBL.Instance.GetListInternal(ClientHostName, null, _memberId, new[] { _communityId });

            Assert.IsTrue(results.TotalCount == initialTotalCount+1);

        }


        [Test]
        public void AddListMemberCouchbase_Success()
        {
            var initialTotalCount = 0;
            var results = ListBL.Instance.GetListInternal(ClientHostName, null, _memberId, new[] { _communityId });
            ListBL.Instance.RemoveListMember(ClientHostName, HotListCategory.Default, _communityId, _memberId, _targetMemberId);

            

            initialTotalCount = results.TotalCount;

            ListBL.Instance.AddListMember(ClientHostName, HotListCategory.Default, _communityId, _siteId, _memberId, _targetMemberId, DateTime.UtcNow, GenerateRandomComment(), _teaseId, DateTime.UtcNow);

            results = ListBL.Instance.GetListInternal(ClientHostName, null, _memberId, new[] { _communityId });

            Assert.IsTrue(results.TotalCount == initialTotalCount + 1);

        }


        [Test]
        public void GetListCounts_Success()
        {
            var countCollection = ListBL.Instance.GetListCounts(_memberId, _communityId, _siteId);

            Console.WriteLine();
            foreach (HotListCategory category in Enum.GetValues(typeof(HotListCategory)))
                Console.WriteLine(string.Format("Category {0} has {1} items", category.ToString(), countCollection[category]));

            Assert.Pass();
        }

        [Test]
        public void CompareListInternalCounts_Success()
        {
            var random = new Random();
            var addedMembers = new Dictionary<int, List<int>>();

            PrintCounts("Counts before adding members");

            foreach (HotListCategory category in Enum.GetValues(typeof(HotListCategory)))
            {
                for (var i = 0; i <= 4; i++)
                {

                    var targetMemberId = GetValidMemberId();
                    
                    ListBL.Instance.AddListMember(ClientHostName, category, _communityId, _siteId, _memberId,
                        targetMemberId, DateTime.UtcNow, GenerateRandomComment(), _teaseId, DateTime.UtcNow);

                    if(!addedMembers.ContainsKey((int)category))
                        addedMembers.Add((int)category, new List<int>());

                    addedMembers[(int)category].Add(targetMemberId);
                }
            }

            var sw = new Stopwatch();

            sw.Start();
            var listFetchedSequencially = ListBL.Instance.GetListInternal(ClientHostName, null, _memberId, new[] {_communityId});
            sw.Stop();

            
            Console.WriteLine(string.Format("{0}Getting items sequencially took {1} ms",Environment.NewLine,  sw.ElapsedMilliseconds));

            sw.Reset();
            sw.Start();
            var listFetchedInParallel = ListBL.Instance.GetListInternal(ClientHostName, null, _memberId, new[] { _communityId }, true);
            sw.Stop();

            Console.WriteLine(string.Format("Getting items in parallel took {0} ms", sw.ElapsedMilliseconds));

            foreach (var category in addedMembers.Keys)
            {
                foreach (var addedMemberId in addedMembers[category])
                {
                    var itemDetailA = listFetchedSequencially.GetListItemDetail((HotListCategory) category, _communityId, _siteId,
                        addedMemberId);

                    var itemDetailB = listFetchedInParallel.GetListItemDetail((HotListCategory)category, _communityId, _siteId,
                        addedMemberId);

                    if (itemDetailA != null && itemDetailB != null) {
                        if (_verboseOutput)
                        {
                            Console.WriteLine(string.Format("ActionDate A: {0} ActionDate B: {1}",
                                itemDetailA.ActionDate, itemDetailB.ActionDate));
                            Console.WriteLine(string.Format("Comment A: {0} == Comment B: {1}", itemDetailA.Comment,
                                itemDetailB.Comment));
                        }

                        Assert.IsTrue(itemDetailA.ActionDate == itemDetailB.ActionDate);
                        Assert.IsTrue(itemDetailA.Comment == itemDetailB.Comment );
                    }
                    else
                    {
                        //both should be null, otherwise this would mean one item got fetched by one list and not the other.
                        Assert.IsTrue(itemDetailA==itemDetailB);
                        Console.WriteLine("Couldn't fetch itemDetails");
                    }
                }
            }

            Assert.IsTrue(listFetchedSequencially.TotalCount == listFetchedInParallel.TotalCount);

            //Console.WriteLine("Sleeping for 5 secs");
            //Thread.Sleep(5000);

            PrintCounts("Counts after adding members");

            //Do some clean up only for this unit test as is good to have data for other tests
            foreach (var category in addedMembers.Keys)
            {
                foreach (var addedMemberId in addedMembers[category])
                {
                    ListBL.Instance.RemoveListMember(ClientHostName, (HotListCategory)category, _communityId, _memberId, addedMemberId);
                }
            }

            PrintCounts("Counts after cleaning up");
        }

        [Test]
        public void GetListInternalInParallel_Stress()
        {
            const int threads = 100;
            var averageTimes = new List<long>();

            ThreadFactory workers = new ThreadFactory(threads, () =>
            {
                long totalTime = 0;

                for (var i = 0; i < threads*5; i++)
                {
                    var sw = new Stopwatch();

                    sw.Reset();
                    sw.Start();
                    var listFetchedInParallel = ListBL.Instance.GetListInternal(ClientHostName, null,
                        GetRandomMemberId(), new[] {_communityId}, true);
                    sw.Stop();
                    totalTime = totalTime + sw.ElapsedMilliseconds;

                }

                averageTimes.Add(totalTime);

            });

            workers.StartWorking();

            var index = 0;
            long finalAverage = 0;
            foreach (long value in averageTimes) { 
                Console.WriteLine(string.Format("The average response time for thread {0} calls was {1}", index, value));
                finalAverage = finalAverage + value;
                index++;
            }

            Console.WriteLine(string.Format("Average for {0} threads was {1} ms", threads, finalAverage / threads));

            Assert.Pass();

        }

        [Test]
        public void GetListInternal_Stress()
        {
            const int threads = 100;
            var averageTimes = new List<long>();

            ThreadFactory workers = new ThreadFactory(threads, () =>
            {
                long totalTime = 0;

                for (var i = 0; i < threads*5; i++)
                {
                    var sw = new Stopwatch();

                    sw.Reset();
                    sw.Start();
                    var listFetchedInParallel = ListBL.Instance.GetListInternal(ClientHostName, null,
                        GetRandomMemberId(), new[] { _communityId }, false);
                    sw.Stop();
                    totalTime = totalTime + sw.ElapsedMilliseconds;

                }

                averageTimes.Add(totalTime);

            });

            workers.StartWorking();

            var index = 0;

            long finalAverage = 0;
            foreach (long value in averageTimes)
            {
                Console.WriteLine(string.Format("The average response time for thread {0} calls was {1}", index, value));
                finalAverage = finalAverage + value;
                index++;
            }

            Console.WriteLine(string.Format("Average for {0} threads was {1} ms", threads, finalAverage / threads));

            Assert.Pass();

        }



        private void PrintCounts(string header)
        {
            var countCollection = ListBL.Instance.GetListCounts(_memberId, _communityId, _siteId);
            
            Console.WriteLine(Environment.NewLine+ header + Environment.NewLine);
            
            foreach (HotListCategory category in Enum.GetValues(typeof (HotListCategory)))
                Console.WriteLine(string.Format("Category {0} has {1} items", category.ToString(), countCollection[category]));
        }

        [Test]
        public void SendProfileViewedAlert_Success()
        {
            SetSendProfileViewedAlertMockMemberData();
            ProfileViewedEmailAlertResult value = ListBL.Instance.SendProfileViewedAlert(HotListCategory.WhoViewedYourProfile, 3, 103, ListBL.Instance.NUnit_Member.MemberID, ListBL.Instance.NUnit_TargetMember.MemberID);
            Assert.IsTrue(value.EmailAlertStatus == ProfileViewedEmailAlertStatus.Success);
        }

        [Test]
        public void SendProfileViewedAlert_OptOutCondition()
        {
            SetSendProfileViewedAlertMockMemberData();
            //opt out
            (ListBL.Instance.NUnit_Member as MockMember).SetAttributeInt("AlertEmailMask", 16);
            ProfileViewedEmailAlertResult value = ListBL.Instance.SendProfileViewedAlert(HotListCategory.WhoViewedYourProfile, 3, 103, ListBL.Instance.NUnit_Member.MemberID, ListBL.Instance.NUnit_TargetMember.MemberID);
            Assert.IsTrue(value.EmailAlertStatus == ProfileViewedEmailAlertStatus.FailedOptIn);
        }

        [Test]
        public void SendProfileViewedAlert_LastLoginCondition()
        {
            SetSendProfileViewedAlertMockMemberData();
            //logged in one day ago
            (ListBL.Instance.NUnit_Member as MockMember).SetAttributeDate("BrandLastLogonDate", DateTime.Now.Subtract(new TimeSpan(1, 0, 0, 0)));
            ProfileViewedEmailAlertResult value = ListBL.Instance.SendProfileViewedAlert(HotListCategory.WhoViewedYourProfile, 3, 103, ListBL.Instance.NUnit_Member.MemberID, ListBL.Instance.NUnit_TargetMember.MemberID);
            Assert.IsTrue(value.EmailAlertStatus == ProfileViewedEmailAlertStatus.FailedLastLogin);
        }

        [Test]
        public void SendProfileViewedAlert_RegistrationCondition()
        {
            SetSendProfileViewedAlertMockMemberData();
            //registered 7 days ago
            (ListBL.Instance.NUnit_Member as MockMember).SetAttributeDate("BrandInsertDate", DateTime.Now.Subtract(new TimeSpan(7, 0, 0, 0)));
            ProfileViewedEmailAlertResult value = ListBL.Instance.SendProfileViewedAlert(HotListCategory.WhoViewedYourProfile, 3, 103, ListBL.Instance.NUnit_Member.MemberID, ListBL.Instance.NUnit_TargetMember.MemberID);
            Assert.IsTrue(value.EmailAlertStatus == ProfileViewedEmailAlertStatus.FailedRegistration);
        }

        [Test]
        public void SendProfileViewedAlert_LastAlertSentCondition()
        {
            SetSendProfileViewedAlertMockMemberData();
            //last received email 2 hours ago
            (ListBL.Instance.NUnit_Member as MockMember).SetAttributeDate("ProfileViewedAlertLastSentDate", DateTime.Now.Subtract(new TimeSpan(2, 0, 0)));
            ProfileViewedEmailAlertResult value = ListBL.Instance.SendProfileViewedAlert(HotListCategory.WhoViewedYourProfile, 3, 103, ListBL.Instance.NUnit_Member.MemberID, ListBL.Instance.NUnit_TargetMember.MemberID);
            Assert.IsTrue(value.EmailAlertStatus == ProfileViewedEmailAlertStatus.FailedMinIntervalPerEmail);
        }

        [Test]
        public void SendProfileViewedAlert_ViewerStatusCondition()
        {
            SetSendProfileViewedAlertMockMemberData();
            //hidden
            (ListBL.Instance.NUnit_TargetMember as MockMember).SetAttributeInt("HideMask", 1);
            ProfileViewedEmailAlertResult value = ListBL.Instance.SendProfileViewedAlert(HotListCategory.WhoViewedYourProfile, 3, 103, ListBL.Instance.NUnit_Member.MemberID, ListBL.Instance.NUnit_TargetMember.MemberID);
            Assert.IsTrue(value.EmailAlertStatus == ProfileViewedEmailAlertStatus.FailedHidden);

            SetSendProfileViewedAlertMockMemberData();
            //fraud
            (ListBL.Instance.NUnit_TargetMember as MockMember).SetAttributeInt("FTAApproved", 0);
            ProfileViewedEmailAlertResult value2 = ListBL.Instance.SendProfileViewedAlert(HotListCategory.WhoViewedYourProfile, 3, 103, ListBL.Instance.NUnit_Member.MemberID, ListBL.Instance.NUnit_TargetMember.MemberID);
            Assert.IsTrue(value2.EmailAlertStatus == ProfileViewedEmailAlertStatus.FailedFraud);
        }

        [Test]
        public void SendProfileViewedAlert_MatchCondition()
        {
            SetSendProfileViewedAlertMockMemberData();
            //gender match
            ListBL.Instance.NUnit_MemberSearchPreferenceCollection["gendermask"] = "9";
            ProfileViewedEmailAlertResult value = ListBL.Instance.SendProfileViewedAlert(HotListCategory.WhoViewedYourProfile, 3, 103, ListBL.Instance.NUnit_Member.MemberID, ListBL.Instance.NUnit_TargetMember.MemberID);
            Assert.IsTrue(value.EmailAlertStatus == ProfileViewedEmailAlertStatus.FailedGenderMatch);

            SetSendProfileViewedAlertMockMemberData();
            //no religion pref specified
            ListBL.Instance.NUnit_MemberSearchPreferenceCollection["jdatereligion"] = "";
            ListBL.Instance.NUnit_MemberSearchPreferenceCollection["religion"] = "0";
            ProfileViewedEmailAlertResult value2 = ListBL.Instance.SendProfileViewedAlert(HotListCategory.WhoViewedYourProfile, 3, 103, ListBL.Instance.NUnit_Member.MemberID, ListBL.Instance.NUnit_TargetMember.MemberID);
            Assert.IsTrue(value2.EmailAlertStatus == ProfileViewedEmailAlertStatus.Success);

            SetSendProfileViewedAlertMockMemberData();
            //specific religion match
            ListBL.Instance.NUnit_MemberSearchPreferenceCollection["jdatereligion"] = "2";
            ListBL.Instance.NUnit_MemberSearchPreferenceCollection["religion"] = "2";
            ProfileViewedEmailAlertResult value3 = ListBL.Instance.SendProfileViewedAlert(HotListCategory.WhoViewedYourProfile, 3, 103, ListBL.Instance.NUnit_Member.MemberID, ListBL.Instance.NUnit_TargetMember.MemberID);
            Assert.IsTrue(value3.EmailAlertStatus == ProfileViewedEmailAlertStatus.FailedReligionMatch);
        }

        private void SetSendProfileViewedAlertMockMemberData()
        {
            //creates successful member and target member
            MockMember member = new MockMember();
            member.MemberID = 1000;
            member.SetAttributeDate("BrandLastLogonDate", DateTime.Now.Subtract(new TimeSpan(5, 0 , 0, 0)));
            member.SetAttributeDate("BrandInsertDate", DateTime.Now.Subtract(new TimeSpan(21, 0, 0, 0)));
            ListBL.Instance.NUnit_Member = member;

            SearchPreferenceCollection memberSearchPreferenceCollection = new SearchPreferenceCollection();
            
            memberSearchPreferenceCollection["gendermask"] = "6";
            memberSearchPreferenceCollection["jdatereligion"] = "1";
            memberSearchPreferenceCollection["religion"] = "1";
            ListBL.Instance.NUnit_MemberSearchPreferenceCollection = memberSearchPreferenceCollection;

            MockMember targetMember = new MockMember();
            targetMember.MemberID = 2000;
            targetMember.SetAttributeInt("HideMask", 0);
            targetMember.SetAttributeInt("FTAApproved", 1);
            targetMember.SetAttributeInt("gendermask", 6);
            targetMember.SetAttributeInt("jdatereligion", 1);
            targetMember.SetAttributeInt("religion", 1);
            ListBL.Instance.NUnit_TargetMember = targetMember;

        }



        private void SetInternalListMockMemberData()
        {
            //creates successful member and target member
            MockMember member = new MockMember();
            member.MemberID = 1000;
            member.SetAttributeDate("BrandLastLogonDate", DateTime.Now.Subtract(new TimeSpan(5, 0, 0, 0)));
            member.SetAttributeDate("BrandInsertDate", DateTime.Now.Subtract(new TimeSpan(21, 0, 0, 0)));

            ListBL.Instance.NUnit_Member = member;

            MockMember targetMember = new MockMember();
            targetMember.MemberID = 2000;
            targetMember.SetAttributeInt("HideMask", 0);
            targetMember.SetAttributeInt("FTAApproved", 1);
            targetMember.SetAttributeInt("gendermask", 6);
            targetMember.SetAttributeInt("jdatereligion", 1);
            targetMember.SetAttributeInt("religion", 1);
            ListBL.Instance.NUnit_TargetMember = targetMember;

        }

        public string GenerateRandomComment(int minWords=10, int maxWords=500, int minSentences=1, int maxSentences=3, int numParagraphs=1)
        {

        var words = new[]{"lorem", "ipsum", "spark", "sit", "christian", "consectetuer",
        "adipiscing", "elit", "jdate", "mingle", "nonummy", "ux", "euismod",
        "tincidunt", "cool", "marcet", "date", "magna", "date", "super"};

            var rand = new Random();
            int numSentences = rand.Next(maxSentences - minSentences)
                + minSentences + 1;
            int numWords = rand.Next(maxWords - minWords) + minWords + 1;

            string result = string.Empty;

            for (int p = 0; p < numParagraphs; p++)
            {
                for (int s = 0; s < numSentences; s++)
                {
                    for (int w = 0; w < numWords; w++)
                    {
                        if (w > 0) { result += " "; }
                        result += words[rand.Next(words.Length)];
                    }
                }
            }

            return result;
        }



        void Instance_SynchronizationRequested(string key, System.Collections.Hashtable cacheReferences)
        {
            if(_verboseOutput)
                Console.WriteLine("Synchronization requested.");
        }

        void Instance_ReplicationRequested(IReplicable replicableObject)
        {
            if (_verboseOutput)
                Console.WriteLine("Replication requested.");
        }

        void Instance_ReplicationActionRequested(IReplicationAction replicationAction)
        {
            if (_verboseOutput) 
                Console.WriteLine("Replication action requested.");
        }

        void Instance_ListRequested(bool cacheHit, int count)
        {
            if (_verboseOutput) 
                Console.WriteLine("List requested.");
        }

        void Instance_ListRemoved()
        {
            if (_verboseOutput) 
                Console.WriteLine("List removed.");
        }

        void Instance_ListAdded()
        {
            if (_verboseOutput) 
                Console.WriteLine("List added.");
        }

        void Instance_ListSaved()
        {
            if (_verboseOutput)
                Console.WriteLine("List saved.");
        }


        private int GetValidMemberId()
        {
            return _stack.Pop();
        }

        private int GetRandomMemberId()
        {
            return _membersInPartition.OrderBy(x => _random.Next()).First();
        }

    }


    public class ThreadFactory
    {
        int workersCount;
        private List<Thread> threads = new List<Thread>();

        public ThreadFactory(int threadCount, Action action)
        {
            for (int i = 0; i < threadCount; i++)
            {
                var worker = new Thread(() => action());
                threads.Add(worker);
            }
        }

        public void StartWorking()
        {
            Console.WriteLine(string.Format("Starting with {0} threads", threads.Count()));

            foreach (var thread in threads)
            {
                thread.Start();
            }

            foreach (var thread in threads)
            {
                thread.Join();
            }
        }
    }



}
