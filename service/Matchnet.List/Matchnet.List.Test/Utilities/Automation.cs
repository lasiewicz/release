﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.List.Test
{
    public static class Utilities
    {
        public static string GenerateRandomText(int minWords = 10, int maxWords = 500, int minSentences = 1, int maxSentences = 3, int numParagraphs = 1)
        {
            var words = new[]{"lorem", "ipsum", "spark", "sit", "christian", "consectetuer",
        "adipiscing", "elit", "jdate", "mingle", "nonummy", "ux", "euismod",
        "tincidunt", "cool", "marcet", "date", "magna", "date", "super"};

            var rand = new Random();
            var numSentences = rand.Next(maxSentences - minSentences)
                + minSentences + 1;
            var numWords = rand.Next(maxWords - minWords) + minWords + 1;

            var result = string.Empty;

            for (var p = 0; p < numParagraphs; p++)
            {
                for (var s = 0; s < numSentences; s++)
                {
                    for (var w = 0; w < numWords; w++)
                    {
                        if (w > 0) { result += " "; }
                        result += words[rand.Next(words.Length)];
                    }
                }
            }

            return result;
        }

        public static IEnumerable<T> GetEnumerationValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

    }

    public static class EnumerableExtensions
    {
        public static T PickRandom<T>(this IEnumerable<T> source)
        {
            return source.PickRandom(1).Single();
        }

        public static IEnumerable<T> PickRandom<T>(this IEnumerable<T> source, int count)
        {
            return source.Shuffle().Take(count);
        }

        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
        {
            return source.OrderBy(x => Guid.NewGuid());
        }
    }
}
