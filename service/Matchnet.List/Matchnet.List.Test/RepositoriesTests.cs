﻿using System.Security.Permissions;
using System.Security.Policy;
using Matchnet.List.BusinessLogic;
using Matchnet.List.BusinessLogic.Couchbase.Common;
using Matchnet.List.BusinessLogic.Couchbase.Repositories;
using Matchnet.List.ValueObjects;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Matchnet.List.BusinessLogic.Couchbase.Models;

namespace Matchnet.List.Test
{
    [TestFixture]
    public class RepositoriesTests
    {
        private const int CommunityId = 20;
        private bool enableDocLoaderMethods;

        [TestFixtureSetUp]
        public void SetUp()
        {
        }

        [TestFixtureTearDown]
        public void TearDown()
        {

        }

        [Test, Description("Verify items are being fetch while applying quota values.")]
        public void GetListMember_Enforcequota()
        {
            const int memberId = 10;
            const int communitId = 30; //CM
            //const int teaseId = 100;

            var memberListRepo = new ListNoSqlRepository();

            //Console.WriteLine(string.Format("Adding two items to the list for member: {0}", memberId));

            //memberListRepo.Save(HotListCategory.Default, communitId, memberId, targetMemberId,
            //    "This is just some text", teaseId, HotListType.ViewProfile, HotListDirection.OnTheirList);
            //memberListRepo.SaveMemberList(HotListCategory.Default, communitId, memberId, targetMemberId + 1,
            //    "This is just some text", teaseId, HotListType.ViewProfile, HotListDirection.OnYourList);

//            var list1 = memberListRepo.GetListView(memberId, communitId, HotListCategory.Default);
//            Console.WriteLine("Get lists for HotListCategory.Default:");
//            foreach (ListItem item in list1)
//            {
//                Console.WriteLine("\t\t"+item.Id);
//            }
//
//            var list2 = memberListRepo.GetListView(memberId,communitId,(HotListType)0);
//            Console.WriteLine("Get lists for all list types:");
//            foreach (ListItem item in list2)
//            {
//                Console.WriteLine("\t\t" + item.Id);
//            }

            var list3 = memberListRepo.GetListView(memberId);
            Console.WriteLine("Get all lists for member:");
            foreach (ListItem item in list3)
            {
                Console.WriteLine("\t\t" + item.Id);
            }

            //Assert.IsNotNull(list);

            //var itemCount = list.Items.Count();

            //Console.WriteLine(string.Format("List for memberId: {0} has : {1} items ", memberId, itemCount));

            
            //Console.WriteLine("Removing added items");

            //memberListRepo.DeleteListStp(communitId, memberId, targetMemberId, HotListDirection.OnTheirList);
            //memberListRepo.DeleteListStp(communitId, memberId, targetMemberId + 1, HotListDirection.OnYourList);

            //PrintList(memberListRepo, memberId);

            //Assert.IsTrue(memberListRepo.GetList(memberId).Items.Count() == itemCount - 2);
        }


        [Test, Description("Insert ")]
        public void AddListMember_InsertDocuments()
        {
            var memberListRepo = new ListNoSqlRepository();
            const int numberOfIterations = 1000;
            const int communityId = 30;
            const int teaseId = 1;
            for (var memberId = 10; memberId < 20; memberId++)
            {
                var rand = new Random(memberId);
                Parallel.For(1, numberOfIterations, i =>
                {
                    var category = (HotListCategory) Enum.Parse(typeof (HotListCategory), (rand.Next(43)*-1) + "");
//                    var type = (HotListType) Enum.Parse(typeof (HotListType), rand.Next(25) + "");
                    var type = (HotListType)0;

                    memberListRepo.Save(communityId, category, type,
                        HotListDirection.OnYourList, memberId, i + 100, string.Empty, teaseId, DateTime.UtcNow.AddMonths(rand.Next(12) * -1));

                });
            }
            Thread.Sleep(30000);
        }

        [Test, Description("Generates new list items for the same memberId, with all the possible combinations, this method is useful to determine the size of a single document")]
        public void SaveMemberList_all_combinations()
        {
            const int memberId = 145828;
            var memberListRepo = new ListNoSqlRepository();
            var targetMembersIds = Enumerable.Range(200, 300).ToList();
            var teaseIds = Enumerable.Range(1, 10).ToList();

            foreach (var category in Utilities.GetEnumerationValues<HotListCategory>())
                foreach (var hotlistType in Utilities.GetEnumerationValues<HotListType>())
                    foreach (var direction in Utilities.GetEnumerationValues<HotListDirection>())
                        memberListRepo.Save(CommunityId,category,hotlistType, direction, memberId, targetMembersIds.PickRandom(),
                            Utilities.GenerateRandomText(1, 30), teaseIds.PickRandom(), DateTime.UtcNow);
        }

        [Test, Description("Adds a new listItem and verifies it is fetched.")]
        public void AddListMember_Success()
        {
            const int memberId = 1152228814;
//            const int targetMemberId = 22881458;
            const int targetMemberId = 411;
            const int communityId = 30;

            var memberListRepo = new ListNoSqlRepository();

            memberListRepo.Save(communityId, HotListCategory.ExcludeList, HotListType.Jmeter, HotListDirection.OnYourList, memberId, targetMemberId, string.Empty, 0, DateTime.UtcNow);

            var list = memberListRepo.GetListView(memberId);

            Assert.IsNotNull(list);

            var foundItem = list.FirstOrDefault(
                p =>
                    p.CategoryId == (int)HotListCategory.ExcludeList && p.CommunityId == communityId && p.MemberId == memberId && p.TargetMemberId == targetMemberId &&
                    p.ListTypeId == (int)HotListType.Jmeter && p.Direction == (int)HotListDirection.OnYourList);

            Assert.IsNotNull(foundItem);
        }


        [Test, Description("Save items to a list, remove it and verifies the counts")]
        public void SaveMemberList_add_then_remove_items()
        {
            var millisecondsTimeout = 2000;
            const int memberId = 145827;
            const int targetMemberId = 22881458;
            const int communityId = 20; //CM
            const int teaseId = 100;
            var memberListRepo = new ListNoSqlRepository();
            var list = memberListRepo.GetListView(memberId);
            var itemCount1 = list.Count();

            Console.WriteLine("Adding two items to the list for member: {0}", memberId);
            memberListRepo.Save(communityId, HotListCategory.MembersYouViewed, HotListType.ViewProfile, HotListDirection.OnYourList, memberId, targetMemberId, "simple text", teaseId, DateTime.UtcNow);
            memberListRepo.Save(communityId, HotListCategory.Default, HotListType.Friend, HotListDirection.OnYourList, memberId, targetMemberId, "simple text", teaseId, DateTime.UtcNow);
            Thread.Sleep(millisecondsTimeout);
            list = memberListRepo.GetListView(memberId);
            var itemCount2 = list.Count();
            Assert.AreEqual(itemCount1+2, itemCount2);

            Console.WriteLine(string.Format("List for memberId: {0} has : {1} items ", memberId, itemCount2));

            Console.WriteLine("Removing two items from the list for member: {0}", memberId);
            memberListRepo.Delete(communityId, memberId, targetMemberId, HotListType.Friend, HotListDirection.OnYourList, HotListCategory.Default);
            memberListRepo.Delete(communityId, memberId, targetMemberId, HotListType.ViewProfile, HotListDirection.OnYourList, HotListCategory.MembersYouViewed);
            Thread.Sleep(millisecondsTimeout);
            list = memberListRepo.GetListView(memberId);
            var itemCount3 = list.Count();
            Assert.AreEqual(itemCount1, itemCount3);

        }


        [Test, Description("Save items to a YNMlist, remove it and verifies the counts")]
        public void SaveYNMList_add_then_remove_items()
        {
            var millisecondsTimeout = 2500;
            const int memberId = 145827;
            const int targetMemberId = 22881458;
            const int communityId = 20; //CM
            const int siteId = 9081;
            const int teaseId = 100;
            
            var memberListRepo = new ListNoSqlRepository();
            var list = memberListRepo.GetListView(memberId);
            var itemCount1 = list.Count();

            Console.WriteLine("Adding 1 item to the list for member: {0}", memberId);
            memberListRepo.Save(communityId, siteId, HotListType.YNM, memberId, targetMemberId, teaseId, DateTime.UtcNow);
            Thread.Sleep(millisecondsTimeout);
            list = memberListRepo.GetListView(memberId);
            var itemCount2 = list.Count();
            Assert.AreEqual(itemCount1 + 1, itemCount2);


            Console.WriteLine(string.Format("List for memberId: {0} has : {1} items ", memberId, itemCount2));

            Console.WriteLine("Removing two items from the list for member: {0}", memberId);
            memberListRepo.Delete(communityId, memberId, targetMemberId, HotListType.YNM, HotListDirection.None);
            Thread.Sleep(millisecondsTimeout);
            list = memberListRepo.GetListView(memberId);
            var itemCount3 = list.Count();
            Assert.AreEqual(itemCount1, itemCount3);

//            var result = list.FirstOrDefault(p => p.CommunityId == communityId && p.SiteId == siteId && p.ListTypeId == (int)HotListType.YNM && p.MemberId == memberId && p.TargetMemberId == targetMemberId && p.TeaseId == teaseId);
//
//            Assert.IsNotNull(result);
        }

        private static void PrintCounts(int memberId, int communityId, int siteId)
        {
            var countCollection = ListBL.Instance.GetListCounts(memberId, communityId, siteId);

            foreach (HotListCategory category in Enum.GetValues(typeof(HotListCategory)))
                Console.WriteLine(string.Format("Category {0} has {1} items", category.ToString(), countCollection[category]));
        }
       
    }
}
