using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;


namespace Matchnet.List.ValueObjects
{
	/// <summary>
	/// Summary description for StandardCategoryCollection.
	/// </summary>
	[Serializable]
	public class StandardCategoryCollection : IValueObject, ICacheable, ISerializable, IEnumerable
	{
		private const string CACHE_KEY = "HL_STD_CAT";
		private Int32 _siteID;
		private ArrayList _list;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteID"></param>
		public StandardCategoryCollection(Int32 siteID)
		{
			_siteID = siteID;
			_list = new ArrayList();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected StandardCategoryCollection(SerializationInfo info, StreamingContext context)
		{
			_siteID = info.GetInt32("siteID");
			_list = (ArrayList)info.GetValue("list", typeof(ArrayList));
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("siteID", _siteID);
			info.AddValue("list", _list);
		}

		
		/// <summary>
		/// Adds a new standard category to the Standard Category Collection
		/// </summary>
		/// <param name="standardCategory">The category to be added.</param>
		/// <returns>The index at which the category was added.</returns>
		public int Add(StandardCategory standardCategory)
		{
			return _list.Add(standardCategory);
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return 3600;
			}
			set
			{
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return Matchnet.CacheItemMode.Absolute;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return Matchnet.CacheItemPriorityLevel.Normal;
			}
			set
			{
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return StandardCategoryCollection.GetCacheKey(_siteID);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteID"></param>
		/// <returns></returns>
		public static string GetCacheKey(Int32 siteID)
		{
			return CACHE_KEY + siteID.ToString();
		}
		#endregion

		#region IEnumerable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public IEnumerator GetEnumerator()
		{
			return _list.GetEnumerator();
		}

		#endregion
	}
}
