using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Threading;

namespace Matchnet.List.ValueObjects
{
	[Serializable]
	internal class ListInstance : ISerializable
	{
		private ReaderWriterLock _lock;
        private ArrayList _items;
		private Hashtable _index;

		public ListInstance()
		{
			_lock = new ReaderWriterLock();
            _items = new ArrayList();
			_index = new Hashtable();
		}


        internal ArrayList Items
		{
			get
			{
				return _items;
			}
		}


		protected ListInstance(SerializationInfo info, StreamingContext context)
		{
			_lock = new ReaderWriterLock();
            _items = (ArrayList)info.GetValue("items", typeof(ArrayList));
			_index = (Hashtable)info.GetValue("index", typeof(Hashtable));
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("items", _items);
			info.AddValue("index", _index);
		}

		
		public ListMember Find(Int32 targetMemberID)
		{
			_lock.AcquireReaderLock(-1);
			try
			{
				return _index[targetMemberID] as ListMember;
			}
			finally
			{
				_lock.ReleaseLock();
			}
		}


		public void Add(ListMember listMember, bool sort)
		{
			ListMember listMemberOld = Find(listMember.MemberID);

			_lock.AcquireWriterLock(-1);
			try
			{
				if (listMemberOld != null)
				{
					listMemberOld = listMember;
					if (sort)
					{
						_items.Sort();
					}
				}
				else
				{
					_items.Insert(0, listMember);
					_index.Add(listMember.MemberID,
						listMember);
				}
			}
			finally
			{
				_lock.ReleaseLock();
			}
		}


		public Int32[] Trim(Int32 length)
		{
			if (length < 0)
			{
				length = _items.Count;
			}

			if (_items.Count > length)
			{
				this.Sort();

				_lock.AcquireWriterLock(-1);
				try
				{
					Int32[] removedList = new Int32[_items.Count - length];

					Int32 i = 0;
					while (_items.Count > length)
					{
						ListMember listMember = _items[_items.Count - 1] as ListMember;
						removedList[i] = listMember.MemberID;
						i++;
						_index.Remove(listMember.MemberID);
						_items.RemoveAt(_items.Count - 1);
					}

					return removedList;
				}
				finally
				{
					_lock.ReleaseLock();
				}
			}

			return new Int32[0];
		}


		public void Remove(Int32 targetMemberID)
		{
			ListMember listMember = Find(targetMemberID);

			_lock.AcquireWriterLock(-1);
			try
			{
				if (listMember != null)
				{
					_items.Remove(listMember);
					_index.Remove(targetMemberID);
				}
			}
			finally
			{
				_lock.ReleaseLock();
			}
		}


		public ArrayList Get(Int32 startRow,
			Int32 pageSize,
			out Int32 rowCount)
		{
			ArrayList result = new ArrayList();

			_lock.AcquireReaderLock(-1);
			try
			{
				rowCount = _items.Count;

				for (Int32 listMemberNum = startRow - 1; listMemberNum < (startRow + pageSize - 1); listMemberNum++)
				{
					if (listMemberNum >= _items.Count)
					{
						break;
					}

					result.Add(((ListMember)_items[listMemberNum]).MemberID);
				}
			}
			finally
			{
				_lock.ReleaseLock();
			}

			return result;
		}

        public ArrayList Get(Int32 startRow,
            Int32 pageSize,
            DateTime sinceDate,
            out Int32 rowCount)
        {
            ArrayList result = new ArrayList();

            _lock.AcquireReaderLock(-1);
            try
            {
                rowCount = 0;

                for (Int32 listMemberNum = startRow - 1; listMemberNum < (startRow + pageSize - 1); listMemberNum++)
                {
                    if (listMemberNum >= _items.Count)
                    {
                        break;
                    }

                    ListMember listMember = ((ListMember) _items[listMemberNum]);
                    if (sinceDate < listMember.ActionDate)
                    {
                        result.Add(listMember.MemberID);
                        rowCount++;
                    }
                    else
                    {
                        //we can break here since lists are sorted by action date
                        break;
                    }
                }
            }
            finally
            {
                _lock.ReleaseLock();
            }

            return result;
        }


	    public void Sort()
		{
			_lock.AcquireWriterLock(-1);
			try
			{
				_items.Sort();
			}
			finally
			{
				_lock.ReleaseLock();
			}
		}


		public Int32 Count
		{
			get
			{
				_lock.AcquireReaderLock(-1);
				try
				{
					return _items.Count;
				}
				finally
				{
					_lock.ReleaseLock();
				}
			}
		}
	}
}
