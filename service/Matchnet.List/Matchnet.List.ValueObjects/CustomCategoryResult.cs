using System;
using System.Runtime.Serialization;
using System.Security.Permissions;


namespace Matchnet.List.ValueObjects
{
	/// <summary>
	/// The result of an attempt 
	/// </summary>
	[Serializable]
	public enum CustomCategoryResultStatus : int
	{
		/// <summary>
		/// A succesful modification to the hot list categories has been made.
		/// </summary>
		Success = 0,

		/// <summary>
		/// The operation failed because the maximum number of categories has been reached.
		/// </summary>
		MaximumCategoriesReached = 519163,

		/// <summary>
		/// The operation failed because there is an existing category of that name.
		/// </summary>
		ExistingCategoryName = 520133,

		/// <summary>
		/// The operation failed.
		/// </summary>
		Failure = 1
	}


	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class CustomCategoryResult : ISerializable
	{
		CustomCategoryResultStatus _status;
		Int32 _categoryID = Constants.NULL_INT;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="status"></param>
		/// <param name="categoryID"></param>
		public CustomCategoryResult(CustomCategoryResultStatus status,
			Int32 categoryID)
		{
			_status = status;
			_categoryID = categoryID;
		}


		protected CustomCategoryResult(SerializationInfo info, StreamingContext context)
		{
			_status = (CustomCategoryResultStatus)info.GetValue("status", typeof(CustomCategoryResultStatus));
			_categoryID = info.GetInt32("categoryID");
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("status", _status);
			info.AddValue("categoryID", _categoryID);
		}

		
		/// <summary>
		/// 
		/// </summary>
		public CustomCategoryResultStatus Status
		{
			get
			{
				return _status;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 CategoryID
		{
			get
			{
				return _categoryID;
			}
		}
	}
}
