using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace Matchnet.List.ValueObjects
{
	/// <summary>
	/// Used by ExternalMail for retrieving counts of each List without having to retrieve all List data.
	/// </summary>
	[Serializable]
	public class ListCountCollection : IValueObject, ISerializable
	{
		private Hashtable _counts = new Hashtable();

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		public ListCountCollection()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		protected ListCountCollection(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream((byte[])info.GetValue("bytearray", typeof(byte[])));
			BinaryReader br = new System.IO.BinaryReader(ms);

			Int32 count = br.ReadInt32();

			for (Int32 i = 0; i < count; i++)
			{
				HotListCategory category = (HotListCategory) br.ReadInt32();
				this[category] = br.ReadInt32();
			}
		}
		#endregion

		/// <summary>
		/// 
		/// </summary>
		public Int32 this[HotListCategory category]
		{
			get
			{
				return (Int32) _counts[category];
			}
			set
			{
				_counts[category] = value;
			}
		}

		#region ISerializable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			MemoryStream ms = new MemoryStream(4 + 8 * _counts.Count);
			BinaryWriter bw = new BinaryWriter(ms);

			bw.Write(_counts.Count);

			foreach (HotListCategory category in _counts.Keys)
			{
				bw.Write((Int32) category);
				bw.Write(this[category]);
			}

			info.AddValue("bytearray", ms.ToArray());
		}

		#endregion
	}
}
