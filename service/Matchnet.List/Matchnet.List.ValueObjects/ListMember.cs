using System;
using System.Runtime.Serialization;
using System.Security.Permissions;


namespace Matchnet.List.ValueObjects
{
	[Serializable]
	internal class ListMember : IComparable, ISerializable
	{
		protected int _communityID;
		protected int _memberID;
		protected DateTime _actionDate;

		internal ListMember()
		{
		}

		public ListMember(int communityID, int memberID, DateTime actionDate)
		{
			_communityID = communityID;
			_memberID = memberID;
			_actionDate = actionDate;
		}


		protected ListMember(SerializationInfo info, StreamingContext context)
		{
			_communityID = info.GetInt32("communityID");
			_memberID = info.GetInt32("memberID");
			_actionDate = info.GetDateTime("actionDate");
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("communityID", _communityID);
			info.AddValue("memberID", _memberID);
			info.AddValue("actionDate", _actionDate);
		}

		
		public int CommunityID
		{
			get
			{
				return _communityID;
			}
		}


		public int MemberID
		{
			get
			{
				return _memberID;
			}
		}


		public DateTime ActionDate
		{
			get
			{
				return _actionDate;
			}
		}

		internal void setActionDate(DateTime actionDate)
		{
			_actionDate = actionDate;
		}

		#region IComparable Members

		public Int32 CompareTo(object obj)
		{
			ListMember listMember = obj as ListMember;
			Int32 result = listMember.ActionDate.CompareTo(this.ActionDate);
			if (result == 0)
			{
				return listMember.MemberID.CompareTo(this.MemberID);
			}

			return result;
		}

		#endregion
	}
}
