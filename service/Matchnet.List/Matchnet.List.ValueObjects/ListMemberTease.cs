using System;
using System.Runtime.Serialization;
using System.Security.Permissions;


namespace Matchnet.List.ValueObjects
{
	[Serializable]
	internal class ListMemberTease : ListMember, ISerializable
	{
		private int _teaseID;

		public ListMemberTease(int communityID,
			int memberID,
			DateTime actionDate,
			int teaseID)
		{
			base._communityID = communityID;
			base._memberID = memberID;
			base._actionDate = actionDate;
			_teaseID = teaseID;
		}


		protected ListMemberTease(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_teaseID = info.GetInt32("teaseID");
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("teaseID", _teaseID);
		}

		
		public int TeaseID
		{
			get
			{
				return _teaseID;
			}
		}


	}
}
