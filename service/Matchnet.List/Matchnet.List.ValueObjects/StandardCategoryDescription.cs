using System;

namespace Matchnet.List.ValueObjects
{
	/// <summary>
	/// Summary description for StandardCategoryType.
	/// </summary>
	[Serializable]
	public enum StandardCategoryDescription
	{
		/// <summary></summary>
		WhoHasViewedYourProfile			= 601896,
		/// <summary></summary>
		WhoHasAddedYouToTheirHotList	= 601898,
		/// <summary></summary>
		WhoHasTeasedYou					= 601900,
		/// <summary></summary>
		WhoHasEmailedYou				= 601902,
		/// <summary></summary>
		WhoHasIMdYou					= 601904,
		/// <summary></summary>
		WhomYouHaveTeased				= 601906,
		/// <summary></summary>
		WhomYouHaveSentEmailsTo			= 601908,
		/// <summary></summary>
		WhomYouHaveIMd					= 601910,
		/// <summary></summary>
		WhomYouHaveViewed				= 601912,
		/// <summary></summary>
		YourMatchesSentViaEmail			= 601914,
		/// <summary></summary>
		WhomYouHaveIgnored				= 601916,
		/// <summary></summary>
		YouSaidYesToEachOther			= 602300,
		/// <summary></summary>
		WhomYouHaveVoiceMailed			= 602428,
		/// <summary></summary>
		WhoHasVoiceMailedYou			= 602430,
		/// <summary></summary>
		WhoHasListenedToYourVoiceMail	= 602486,
		/// <summary></summary>
		WhoseVoiceMailYouListenedTo		= 602488
	}
}
