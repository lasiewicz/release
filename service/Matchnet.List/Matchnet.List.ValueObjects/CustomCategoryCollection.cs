using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet.CacheSynchronization.Tracking;


namespace Matchnet.List.ValueObjects
{
	/// <summary>
	/// Summary description for CategoryCollection.
	/// </summary>
	[Serializable]
	public class CustomCategoryCollection : IValueObject, ICacheable, IReplicable, ISerializable, IEnumerable
	{
		private int _cacheTTLSeconds = 60 * 60;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Sliding;
		private Int32 _memberID;
		private Int32 _communityID;
		private ArrayList _list;

		[NonSerialized]
		private ReferenceTracker _referenceTracker;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		public CustomCategoryCollection(Int32 memberID, Int32 communityID) : base()
		{
			_list = new ArrayList();
			_memberID = memberID;
			_communityID = communityID;
		}


		protected CustomCategoryCollection(SerializationInfo info, StreamingContext context)
		{
			_cacheTTLSeconds = info.GetInt32("cacheTTLSeconds");
			_cachePriority = (Matchnet.CacheItemPriorityLevel)info.GetValue("cachePriority", typeof(Matchnet.CacheItemPriorityLevel));
			_cacheItemMode = (Matchnet.CacheItemMode)info.GetValue("cacheItemMode", typeof(Matchnet.CacheItemMode));
			_memberID = info.GetInt32("memberID");
			_communityID = info.GetInt32("communityID");
			_list = (ArrayList)info.GetValue("list", typeof(ArrayList));
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("cacheTTLSeconds", _cacheTTLSeconds);
			info.AddValue("cachePriority", _cachePriority);
			info.AddValue("cacheItemMode", _cacheItemMode);
			info.AddValue("memberID", _memberID);
			info.AddValue("communityID", _communityID);
			info.AddValue("list", _list);
		}

		
		public CustomCategory this[Int32 index]
		{
			get
			{
				return (CustomCategory)_list[index];
			}
		}


		/// <summary>
		/// Adds a new category to the Category Collection
		/// </summary>
		/// <param name="customCategory">The category to be added.</param>
		public void Add(CustomCategory customCategory)
		{
			CustomCategory existingCategory = Get(customCategory.CategoryID);
			if (existingCategory != null)
			{
				existingCategory.Description = customCategory.Description;
			}
			else
			{
				_list.Add(customCategory);
			}
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="description"></param>
		/// <returns></returns>
		public bool ContainsCategory(string description)
		{
			description = description.ToLower().Trim();

			Int32 count = _list.Count;
			for (Int32 index = 0; index < count; index++)
			{
				if (((CustomCategory)_list[index]).Description == description)
				{
					return true;
				}
			}

			return false;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="categoryID"></param>
		/// <returns></returns>
		public CustomCategory Get(Int32 categoryID)
		{
			CustomCategory customCategory = null;
			Int32 count = _list.Count;

			for (Int32 index = 0; index < count; index++)
			{
				CustomCategory current = (CustomCategory)_list[index];
				if (current.CategoryID == categoryID)
				{
					customCategory = current;
				}
			}

			return customCategory;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="categoryID"></param>
		public void Remove(Int32 categoryID)
		{
			Int32 count = _list.Count;
			for (Int32 index = 0; index < count; index++)
			{
				if (((CustomCategory)_list[index]).CategoryID == categoryID)
				{
					_list.RemoveAt(index);
					return;
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public ReferenceTracker ReferenceTracker
		{
			get
			{
				if (_referenceTracker == null)
				{
					_referenceTracker = new ReferenceTracker();
				}

				return _referenceTracker;
			}
		}


		public Int32 Count
		{
			get
			{
				return _list.Count;
			}
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return _cacheItemMode;
			}
			set
			{
				_cacheItemMode = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return CustomCategoryCollection.GetCacheKey(_memberID, _communityID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <returns></returns>
		public static string GetCacheKey(Int32 memberID, Int32 communityID)
		{
			return "~LISTCATEGORIES^" + memberID.ToString() + "|" + communityID.ToString();
		}
		#endregion

		#region IReplicable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(this.GetCacheKey());
		}

		#endregion

		#region IEnumerable Members

		public IEnumerator GetEnumerator()
		{
			return _list.GetEnumerator();
		}

		#endregion
	}
}
