using System;
using System.Runtime.Serialization;
using System.Security.Permissions;


namespace Matchnet.List.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class Vote : ISerializable
	{
		private ClickListType _voteType;
		//private DateTime _actionDate;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="voteType"></param>
		/// <param name="actionDate"></param>
		public Vote(ClickListType voteType, DateTime actionDate)
		{
			_voteType = voteType;
			//_actionDate = actionDate;
		}


		protected Vote(SerializationInfo info, StreamingContext context)
		{
			_voteType = (ClickListType)info.GetValue("voteType", typeof(ClickListType));
			//_actionDate = info.GetDateTime("actionDate");
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("voteType", _voteType);
			//info.AddValue("actionDate", _actionDate);
		}

		
		/// <summary>
		/// 
		/// </summary>
		public ClickListType VoteType
		{
			get
			{
				return _voteType;
			}
			set
			{
				_voteType = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/*
		public DateTime ActionDate
		{
			get
			{
				return _actionDate;
			}
			set
			{
				_actionDate = value;
			}
		}
			*/
	}
}
