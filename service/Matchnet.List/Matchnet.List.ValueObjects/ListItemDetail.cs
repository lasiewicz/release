using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet;

namespace Matchnet.List.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class ListItemDetail : ISerializable
	{
		private DateTime _actionDate;
		private string _comment;
		private int _teaseID;
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="actionDate"></param>
		/// <param name="comment"></param>
		/// <param name="teaseID"></param>
		public ListItemDetail(DateTime actionDate,
			string comment,
			int teaseID)
		{
			_actionDate = actionDate;
			_comment = comment;
			_teaseID = teaseID;
		}


		protected ListItemDetail(SerializationInfo info, StreamingContext context)
		{
			_actionDate = info.GetDateTime("actionDate");
			_comment = info.GetString("comment");
			_teaseID = info.GetInt32("teaseID");
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("actionDate", _actionDate);
			info.AddValue("comment", _comment);
			info.AddValue("teaseID", _teaseID);
		}

		
		/// <summary>
		/// 
		/// </summary>
		public DateTime ActionDate
		{
			get
			{
				return _actionDate;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string Comment
		{
			get
			{
				return _comment;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public int TeaseID
		{
			get
			{
				return _teaseID;
			}
		}

	}
}
