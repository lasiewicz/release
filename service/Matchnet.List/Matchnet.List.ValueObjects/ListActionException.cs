using System;
using Matchnet.Exceptions;
using System.Runtime.Serialization;
using System.Security.Permissions;


namespace Matchnet.List.ValueObjects
{
	/// <summary>
	/// Summary description for ListActionException.
	/// </summary>
	public class ListActionException: ExceptionBase, ISerializable
	{
		private ListSaveResult _listSaveResult;

		/// <summary>
		/// A custom exception for handling and describing failures related to List operations.
		/// </summary>
		/// <param name="pListSaveResult"></param>
		public ListActionException(ListSaveResult listSaveResult)
		{
			_listSaveResult = listSaveResult;
		}

		protected ListActionException(SerializationInfo info, StreamingContext context)
		{
			_listSaveResult = (ListSaveResult)info.GetValue("listSaveResult", typeof(ListSaveResult));
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("listSaveResult", _listSaveResult);
		}

		
		/// <summary>
		/// A read-only attribute representing the ListSaveResult associated with this list action exception condition.
		/// </summary>
		public ListSaveResult ListSaveResult
		{
			get
			{
				return _listSaveResult;
			}
		}
	}
}
