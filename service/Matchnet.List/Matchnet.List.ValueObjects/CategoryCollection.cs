using System;

namespace Matchnet.List.ValueObjects
{
	/// <summary>
	/// Summary description for CategoryCollection.
	/// </summary>
	public class CategoryCollection : System.Collections.CollectionBase, IValueObject, ICacheable
	{
		public CategoryCollection()
		{
			
		}

		/// <summary>
		/// Adds a new category to the Category Collection
		/// </summary>
		/// <param name="category">The category to be added.</param>
		/// <returns>The index at which the category was added.</returns>
		public int Add(Category category)
		{
			return base.InnerList.Add(category);
		}

		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get
			{
				// TODO:  Add CategoryCollection.CacheTTLSeconds getter implementation
				return 0;
			}
			set
			{
				// TODO:  Add CategoryCollection.CacheTTLSeconds setter implementation
			}
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				// TODO:  Add CategoryCollection.CacheMode getter implementation
				return new Matchnet.CacheItemMode ();
			}
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				// TODO:  Add CategoryCollection.CachePriority getter implementation
				return new Matchnet.CacheItemPriorityLevel ();
			}
			set
			{
				// TODO:  Add CategoryCollection.CachePriority setter implementation
			}
		}

		public string GetCacheKey()
		{
			// TODO:  Add CategoryCollection.GetCacheKey implementation
			return null;
		}

		#endregion
	}
}
