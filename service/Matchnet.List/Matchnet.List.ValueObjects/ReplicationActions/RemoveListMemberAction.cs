using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet;


namespace Matchnet.List.ValueObjects.ReplicationActions
{
	[Serializable]
	public class RemoveListMemberAction : IReplicationAction, ISerializable
	{
		private HotListCategory _category;
		private Int32 _communityID;
		private Int32 _memberID;
		private Int32 _targetMemberID;


		public RemoveListMemberAction(HotListCategory category,
			Int32 communityID,
			Int32 memberID,
			Int32 targetMemberID)
		{
			_category = category;
			_communityID = communityID;
			_memberID = memberID;
			_targetMemberID = targetMemberID;
		}

		
		protected RemoveListMemberAction(SerializationInfo info, StreamingContext context)
		{
			_category = (HotListCategory)info.GetValue("category", typeof(HotListCategory));
			_communityID = info.GetInt32("communityID");
			_memberID = info.GetInt32("memberID");
			_targetMemberID = info.GetInt32("targetMemberID");
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("category", _category);
			info.AddValue("communityID", _communityID);
			info.AddValue("memberID", _memberID);
			info.AddValue("targetMemberID", _targetMemberID);
		}

		
		public HotListCategory Category
		{
			get
			{
				return _category;
			}
		}


		public Int32 CommunityID
		{
			get
			{
				return _communityID;
			}
		}


		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}


		public Int32 TargetMemberID
		{
			get
			{
				return _targetMemberID;
			}
		}
	}
}
