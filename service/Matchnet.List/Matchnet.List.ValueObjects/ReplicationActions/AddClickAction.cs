using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet;


namespace Matchnet.List.ValueObjects.ReplicationActions
{
	[Serializable]
	public class AddClickAction : IReplicationAction, ISerializable
	{
		private Int32 _communityID;
		private Int32 _siteID;
		private Int32 _memberID;
		private Int32 _targetMemberID;
		private Int32 _senderMemberID;
		private ClickDirectionType _direction;
		private ClickListType _type;
		private DateTime _actionDate;


		public AddClickAction(Int32 communityID,
			Int32 siteID,
			Int32 memberID,
			Int32 senderMemberID,
			Int32 targetMemberID,
			ClickDirectionType direction,
			ClickListType type,
			DateTime actionDate)
		{
			_communityID = communityID;
			_siteID = siteID;
			_memberID = memberID;
			_senderMemberID = senderMemberID;
			_targetMemberID = targetMemberID;
			_direction = direction;
			_type = type;
			_actionDate = actionDate;
		}


		protected AddClickAction(SerializationInfo info, StreamingContext context)
		{
			_communityID = info.GetInt32("communityID");
			_siteID = info.GetInt32("siteID");
			_memberID = info.GetInt32("memberID");
			_senderMemberID = info.GetInt32("senderMemberID");
			_targetMemberID = info.GetInt32("targetMemberID");
			_direction = (ClickDirectionType)info.GetValue("direction", typeof(ClickDirectionType));
			_type = (ClickListType)info.GetValue("type", typeof(ClickListType));
			_actionDate = info.GetDateTime("actionDate");
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("communityID", _communityID);
			info.AddValue("siteID", _siteID);
			info.AddValue("memberID", _memberID);
			info.AddValue("senderMemberID", _senderMemberID);
			info.AddValue("targetMemberID", _targetMemberID);
			info.AddValue("direction", _direction);
			info.AddValue("type", _type);
			info.AddValue("actionDate", _actionDate);
		}

		
		public Int32 CommunityID
		{
			get
			{
				return _communityID;
			}
		}


		public Int32 SiteID
		{
			get
			{
				return _siteID;
			}
		}


		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}


		public Int32 SenderMemberID
		{
			get
			{
				return _senderMemberID;
			}
		}


		public Int32 TargetMemberID
		{
			get
			{
				return _targetMemberID;
			}
		}


		public ClickDirectionType Direction
		{
			get
			{
				return _direction;
			}
		}


		public ClickListType Type
		{
			get
			{
				return _type;
			}
		}


		public DateTime ActionDate
		{
			get
			{
				return _actionDate;
			}
		}
	}
}
