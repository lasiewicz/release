using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet;


namespace Matchnet.List.ValueObjects.ReplicationActions
{
	[Serializable]
	public class AddListMemberAction : IReplicationAction, ISerializable
	{
		private HotListCategory _category;
		private Int32 _communityID;
		private Int32 _siteID;
		private Int32 _memberID;
		private Int32 _targetMemberID;
		private DateTime _actionDate;
		private string _comment;
		private Int32 _teaseID;

		
		public AddListMemberAction(HotListCategory category,
			Int32 communityID,
			Int32 siteID,
			Int32 memberID,
			Int32 targetMemberID,
			DateTime actionDate,
			string comment,
			Int32 teaseID)
		{
			_category = category;
			_communityID = communityID;
			_siteID = siteID;
			_memberID = memberID;
			_targetMemberID = targetMemberID;
			_actionDate = actionDate;
			_comment = comment;
			_teaseID = teaseID;
		}

		
		protected AddListMemberAction(SerializationInfo info, StreamingContext context)
		{
			_category = (HotListCategory)info.GetValue("category", typeof(HotListCategory));
			_communityID = info.GetInt32("communityID");
			_siteID = info.GetInt32("siteID");
			_memberID = info.GetInt32("memberID");
			_targetMemberID = info.GetInt32("targetMemberID");
			_actionDate = info.GetDateTime("actionDate");
			_comment = info.GetString("comment");
			_teaseID = info.GetInt32("teaseID");
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("category", _category);
			info.AddValue("communityID", _communityID);
			info.AddValue("siteID", _siteID);
			info.AddValue("memberID", _memberID);
			info.AddValue("targetMemberID", _targetMemberID);
			info.AddValue("actionDate", _actionDate);
			info.AddValue("comment", _comment);
			info.AddValue("teaseID", _teaseID);
		}

		
		public HotListCategory Category
		{
			get
			{
				return _category;
			}
		}

	
		public Int32 CommunityID
		{
			get
			{
				return _communityID;
			}
		}


		public Int32 SiteID
		{
			get
			{
				return _siteID;
			}
		}


		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}


		public Int32 TargetMemberID
		{
			get
			{
				return _targetMemberID;
			}
		}


		public DateTime ActionDate
		{
			get
			{
				return _actionDate;
			}
		}


		public string Comment
		{
			get
			{
				return _comment;
			}
		}


		public Int32 TeaseID
		{
			get
			{
				return _teaseID;
			}
		}
	}
}
