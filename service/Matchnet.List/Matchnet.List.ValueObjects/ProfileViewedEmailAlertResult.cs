﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.List.ValueObjects
{
    public class ProfileViewedEmailAlertResult
    {
        public ProfileViewedEmailAlertStatus EmailAlertStatus { get; set; }
    }

    public enum ProfileViewedEmailAlertStatus
    {
        None,
        Success,
        FailedWhoViewedYourProfileCategory,
        FailedEnabled,
        FailedOptIn,
        FailedLastLogin,
        FailedMinIntervalPerEmail,
        FailedMemberAlreadyTriggeredRecently,
        FailedRegistration,
        FailedHidden,
        FailedFraud,
        FailedGenderMatch,
        FailedReligionMatch,
        FailedListCount,
        FailedExternalMail
        
    }
}
