﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.List.ValueObjects
{

    public class ListServiceConstants
    {
        public const string LIST_QUOTAS_ENABLED = "LIST_QUOTAS_ENABLED";
        public const string ENABLE_LIST_FETCHING_IN_PARALLEL = "ENABLE_LIST_FETCHING_IN_PARALLEL"; //At some point we should consider deprecate this setting 
        public const string LIST_DEFAULT_REPOSITORY = "LIST_DEFAULT_REPOSITORY"; //0 = DB, 1 = Couchbase (indicates where to fetch the data from)
        public const string LIST_DISABLE_NOTIFICATIONS = "LIST_DISABLE_NOTIFICATIONS"; // false/true
        public const string LIST_DEFAULT_STORING_BEHAVIOR = "LIST_DEFAULT_STORING_BEHAVIOR"; //0 = Write list to SQLDB, 1 = Write list to Couchbase, 2 = Write list to both SQLDB and couchbase
        public const string LIST_ISMINGLECOMMUNITY = "LIST_ISMINGLECOMMUNITY";
        public const string ADD_FAVORITE_ACTION_NAME = "Add Favorite";
        public const string DELETE_FAVORITE_ACTION_NAME = "Delete Favorite";
        public const Int32 MAIL_STATUS_VIRAL_MAIL_SENT = 2;
        public const string LIST_NOTIFICATIONS_ENABLED = "LIST_NOTIFICATIONS_ENABLED";
        public const int GC_THRESHOLD = 10000;
    }
}
