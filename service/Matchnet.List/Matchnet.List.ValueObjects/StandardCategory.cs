using System;
using System.Runtime.Serialization;
using System.Security.Permissions;


namespace Matchnet.List.ValueObjects
{
	/// <summary>
	/// Summary description for Category.
	/// </summary>
	[Serializable]
	public class StandardCategory : ISerializable
	{
		private Int32 _listCategoryID;
		private string	_resourcekey;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="listCategoryID"></param>
		/// <param name="resourceKey"></param>
		public StandardCategory(Int32 listCategoryID,
			string resourceKey)
		{
			_listCategoryID = listCategoryID;
			_resourcekey = resourceKey;
		}


		protected StandardCategory(SerializationInfo info, StreamingContext context)
		{
			_listCategoryID = info.GetInt32("listCategoryID");
			_resourcekey = info.GetString("resourcekey");
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("listCategoryID", _listCategoryID);
			info.AddValue("resourcekey", _resourcekey);
		}

		
		/// <summary>
		/// 
		/// </summary>
		public int ListCategoryID
		{
			get
			{
				return _listCategoryID;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string ResourceKey
		{
			get
			{
				return _resourcekey;
			}
		}
	}
}
