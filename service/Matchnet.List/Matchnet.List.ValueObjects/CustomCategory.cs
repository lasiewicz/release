using System;
using System.Runtime.Serialization;
using System.Security.Permissions;


namespace Matchnet.List.ValueObjects
{
	/// <summary>
	/// Summary description for Category.
	/// </summary>
	[Serializable]
	public class CustomCategory : ISerializable
	{
		private int _categoryID;
		private string _description;

		protected CustomCategory(SerializationInfo info, StreamingContext context)
		{
			_categoryID = info.GetInt32("categoryID");
			_description = info.GetString("description");
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("categoryID", _categoryID);
			info.AddValue("description", _description);
		}

		
		/// <summary>
		/// The category Id of the category
		/// </summary>
		public int CategoryID
		{
			get
			{
				return _categoryID;
			}
			set
			{
					_categoryID = value;
			}
		}


		/// <summary>
		/// The description or name of the category
		/// </summary>
		public string Description
		{
			get
			{
				return _description;
			}
			set
			{
				_description = value;
			}
		}


		#region Constructors
		/// <summary>
		/// Creates a new instance of a Category
		/// </summary>
		/// <param name="categoryID">The category ID of the category to be created.</param>
		/// <param name="description">The description of the category to be created.</param>
		public CustomCategory(int categoryID, string description)
		{
			_categoryID = categoryID;
			_description = description;
		}
		#endregion
	}
}
