using System;
using System.Runtime.Serialization;
using System.Security.Permissions;


namespace Matchnet.List.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class ListSaveResult : ISerializable
	{
		private ListActionStatus _status = ListActionStatus.Success;
		private int _counter;
		private int _maxAllowed;
		private int _maxAllowedDurationDays;

		public ListSaveResult()
		{
		}

		protected ListSaveResult(SerializationInfo info, StreamingContext context)
		{
			_status = (ListActionStatus)info.GetValue("status", typeof(ListActionStatus));
			_counter = info.GetInt32("counter");
			_maxAllowed = info.GetInt32("maxAllowed");
			_maxAllowedDurationDays = info.GetInt32("maxAllowedDurationDays");
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("status", _status);
			info.AddValue("counter", _counter);
			info.AddValue("maxAllowed", _maxAllowed);
			info.AddValue("maxAllowedDurationDays", _maxAllowedDurationDays);
		}

		
		/// <summary>
		/// 
		/// </summary>
		public ListActionStatus Status
		{
			get
			{
				return _status;
			}
			set
			{
				_status = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int Counter 
		{
			get
			{
				return _counter;
			}
			set
			{
				_counter = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int MaxAllowed 
		{
			get
			{
				return _maxAllowed;
			}
			set
			{
				_maxAllowed = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int MaxAllowedDurationDays 
		{
			get
			{
				return _maxAllowedDurationDays;
			}
			set
			{
				_maxAllowedDurationDays = value;
			}
		}
	}
}
