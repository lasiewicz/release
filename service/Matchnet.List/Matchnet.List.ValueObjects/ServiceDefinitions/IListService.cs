using System;

using Matchnet.List.ValueObjects;
using Matchnet.CacheSynchronization.ValueObjects;


namespace Matchnet.List.ServiceDefinitions
{
	/// <summary>
	/// 
	/// </summary>
	public interface IListService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <param name="memberID"></param>
		/// <param name="communityIDList"></param>
		/// <returns></returns>
		ListInternal GetListInternal(string clientHostName,
			CacheReference cacheReference,
			Int32 memberID,
			Int32[] communityIDList);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		ListCountCollection GetListCounts(Int32 memberID, Int32 communityID, Int32 siteID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="categoryID"></param>
		/// <param name="communityID"></param>
		/// <param name="memberID"></param>
		/// <param name="description"></param>
		[System.Runtime.Remoting.Messaging.OneWay()]
		void SaveListCategory(string clientHostName,
			Int32 categoryID,
			Int32 communityID,
			Int32 memberID,
			string description);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientHostName"></param>
        /// <param name="category"></param>
        /// <param name="siteID"></param>
        /// <param name="communityID"></param>
        /// <param name="memberID"></param>
        /// <param name="targetMemberID"></param>
        /// <param name="actionDate"></param>
        /// <param name="comment"></param>
        /// <param name="teaseID"></param>
        /// <param name="ignoreQuota"></param>
        /// <returns></returns>
        [System.Runtime.Remoting.Messaging.OneWay()]
        void AddListMember(string clientHostName,
            HotListCategory category,
            Int32 communityID,
            Int32 siteID,
            Int32 memberID,
            Int32 targetMemberID,
            DateTime actionDate,
            string comment,
            Int32 teaseID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="category"></param>
		/// <param name="siteID"></param>
		/// <param name="communityID"></param>
		/// <param name="memberID"></param>
		/// <param name="targetMemberID"></param>
		/// <param name="actionDate"></param>
		/// <param name="comment"></param>
		/// <param name="teaseID"></param>
		/// <param name="ignoreQuota"></param>
		/// <returns></returns>
		[System.Runtime.Remoting.Messaging.OneWay()]
		void AddListMember(string clientHostName,
			HotListCategory category,
			Int32 communityID,
			Int32 siteID,
			Int32 memberID,
			Int32 targetMemberID,
			DateTime actionDate,
			string comment,
			Int32 teaseID,
            DateTime timeStamp);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="category"></param>
		/// <param name="communityID"></param>
		/// <param name="memberID"></param>
		/// <param name="targetMemberID"></param>
		[System.Runtime.Remoting.Messaging.OneWay()]
		void RemoveListMember(string clientHostName,
			HotListCategory category,
			Int32 communityID,
			Int32 memberID,
			Int32 targetMemberID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="memberID"></param>
		/// <param name="targetMemberID"></param>
		/// <param name="direction"></param>
		/// <param name="type"></param>
		/// <param name="actionDate"></param>
		/// <returns></returns>
		[System.Runtime.Remoting.Messaging.OneWay()]
		void AddClick(string clientHostName,
			Int32 communityID,
			Int32 siteID,
			Int32 memberID,
			Int32 targetMemberID,
			ClickDirectionType direction,
			ClickListType type,
			DateTime actionDate);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientHostName"></param>
        /// <param name="communityID"></param>
        /// <param name="siteID"></param>
        /// <param name="memberID"></param>
        /// <param name="targetMemberID"></param>
        /// <param name="direction"></param>
        /// <param name="type"></param>
        /// <param name="actionDate"></param>
        /// <returns></returns>
        [System.Runtime.Remoting.Messaging.OneWay()]
        void SaveViralStatus(string clientHostName,
            int communityId, int memberId, int targetMemberId,
            bool alertEmailSent, bool clickEmailSent);


		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteID"></param>
		/// <returns></returns>
		StandardCategoryCollection GetStandardListCategories(Int32 siteID);
			

		/// <summary>
		/// 
		/// </summary>
		///	<param name="clientHostName"></param>
		///	<param name="cacheReference"></param>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <returns></returns>
		CustomCategoryCollection GetCustomListCategories(string clientHostName,
			CacheReference cacheReference,
			Int32 memberID,
			Int32 communityID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="targetMemberID"></param>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		ClickMask GetClickMask(Int32 memberID,
			Int32 targetMemberID,
			Int32 communityID,
			Int32 siteID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <returns></returns>
		ListInternal GetListInternalMutualYes(Int32 memberID, Int32 communityID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="categoryID"></param>
		/// <param name="communityID"></param>
		/// <param name="memberID"></param>
		[System.Runtime.Remoting.Messaging.OneWay()]
		void DeleteListCategory(string clientHostName,
			Int32 categoryID,
			Int32 communityID,
			Int32 memberID);
		
		[System.Runtime.Remoting.Messaging.OneWay()]
		void SendSMSAlert(Matchnet.List.ValueObjects.HotListCategory hotlistCat,
			int receiverMemberID,
			int senderMemberID,
			int senderBrandID,
			string targetPhoneNumber,
			bool isFriend,
			Matchnet.Content.ValueObjects.BrandConfig.Brand receiverBrand);

		[System.Runtime.Remoting.Messaging.OneWay()]
		void SendHotListNotification(Matchnet.List.ValueObjects.HotListCategory cat, int targetMemberID, int memberID, int brandID);

		[System.Runtime.Remoting.Messaging.OneWay()]
		void SendMessmoMessage(int communityID,
			int targetBrandID,
			int sendingBrandID,
			int targetMemberID,
			int memberID,
			Matchnet.List.ValueObjects.HotListCategory cat);
	}
}