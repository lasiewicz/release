﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.List.ValueObjects.ServiceDefinitions
{
    public interface IHotList
    {
        bool IsHotListed(HotListCategory category, int communityID, int targetMemberID);
        bool IsAtQuota(Int32 communityID, HotListType hotListType);
        void Sort(HotListCategory category, int communityID, int siteID);
        bool IsHotListed(HotListType hotListType, HotListDirection direction, int communityID, int targetMemberID);
        ListItemDetail GetListItemDetail(HotListCategory category,
                                         int communityID,
                                         int siteID,
                                         int targetMemberID);
        ArrayList GetListMembers(HotListCategory category,
                                 int communityID,
                                 int siteID,
                                 int startRow,
                                 int pageSize,
                                 out int rowCount);
        Int32 DetermineVoluntaryList(Int32 targetMemberID, Int32 communityID);
        bool IsMemberOfVoluntaryList(Int32 targetMemberID, Int32 communityID);
        Int32 GetCount(HotListCategory category, Int32 communityID, Int32 siteID);
        ClickMask GetClickMask(int communityID, int siteID, int targetMemberID);
        void AddClick(Int32 communityID,
                      Int32 siteID,
                      Int32 targetMemberID,
                      ClickDirectionType direction,
                      ClickListType voteType,
                      DateTime actionDate,
                      bool sort);
    }
}
