using System;

using Matchnet.List.ValueObjects.Quotas;
using Matchnet.Content.ValueObjects.Quotas;


namespace Matchnet.List.ServiceDefinitions
{
	public interface IQuotaService
	{
		Quotas GetQuotas(Int32 communityID, Int32 memberID);

		void AddQuotaItem(Int32 communityID,
			Int32 memberID,
			QuotaType quotaType,
			DateTime dateTime);
		}
}
