using System;
using System.Collections;

namespace Matchnet.List.ValueObjects.Quotas
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class ListQuotas : IValueObject, ICacheable
	{
		/// <summary>
		/// 
		/// </summary>
		public const string CACHE_KEY = "ListQuotas";
		private int _cacheTTLSeconds = 60 * 60;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Sliding;

		private Hashtable _quotas;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="quotas"></param>
		public ListQuotas(Hashtable quotas)
		{
			_quotas = quotas;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="listType"></param>
		/// <returns></returns>
		public ListQuota GetQuota(Int32 communityID,
			HotListType listType)
		{
			return _quotas[ListQuota.GetKey(communityID, listType)] as ListQuota;
		}


		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return _cacheItemMode;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return CACHE_KEY;
		}

		#endregion
	}
}
