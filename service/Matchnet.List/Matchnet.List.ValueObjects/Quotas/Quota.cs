using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;


namespace Matchnet.List.ValueObjects.Quotas
{
	[Serializable]
	public class Quota : ISerializable
	{
		private ArrayList _quotaItems;

		public Quota()
		{
			_quotaItems = new ArrayList();
		}


		protected Quota(SerializationInfo info, StreamingContext context)
		{
			_quotaItems = (ArrayList)info.GetValue("quotaItems", typeof(ArrayList));
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("quotaItems", _quotaItems);
		}

		
		public void AddItem(DateTime dateTime)
		{
			_quotaItems.Add(dateTime);
		}


		internal void Sort()
		{
			_quotaItems.Sort();
		}


		public Int32 GetItemCount(DateTime minDateTime)
		{
			Int32 itemCount = 0;

			for (Int32 itemNum = _quotaItems.Count - 1; itemNum >= 0; itemNum--)
			{
				if (((DateTime)_quotaItems[itemNum]) > minDateTime)
				{
					itemCount++;
				}
				else
				{
					break;
				}
			}
			
			return itemCount;
		}
	}
}
