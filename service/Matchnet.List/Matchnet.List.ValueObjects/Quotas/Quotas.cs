using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet;
using Matchnet.CacheSynchronization.Tracking;
using Matchnet.Content.ValueObjects.Quotas;


namespace Matchnet.List.ValueObjects.Quotas
{
	[Serializable]
	public class Quotas : ISerializable, IValueObject, ICacheable, IReplicable
	{
		private int _cacheTTLSeconds = 60 * 60;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Sliding;
		private Int32 _communityID;
		private Int32 _memberID;
		private Hashtable _quotas;

		[NonSerialized]
		private ReferenceTracker _referenceTracker;

		public Quotas(Int32 communityID,
			Int32 memberID)
		{
			_communityID = communityID;
			_memberID = memberID;
			_quotas = new Hashtable();
		}


		protected Quotas(SerializationInfo info, StreamingContext context)
		{
			_cacheTTLSeconds = info.GetInt32("cacheTTLSeconds");
			_cachePriority = (Matchnet.CacheItemPriorityLevel)info.GetValue("cachePriority", typeof(Matchnet.CacheItemPriorityLevel));
			_cacheItemMode = (Matchnet.CacheItemMode)info.GetValue("cacheItemMode", typeof(Matchnet.CacheItemMode));
			_communityID = info.GetInt32("communityID");
			_memberID = info.GetInt32("memberID");
			_quotas = (Hashtable)info.GetValue("quotas", typeof(Hashtable));
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("cacheTTLSeconds", _cacheTTLSeconds);
			info.AddValue("cachePriority", _cachePriority);
			info.AddValue("cacheItemMode", _cacheItemMode);
			info.AddValue("communityID", _communityID);
			info.AddValue("memberID", _memberID);
			info.AddValue("quotas", _quotas);
		}

		
		public Quota this[QuotaType quotaType]
		{
			get
			{
				Quota quota = _quotas[quotaType] as Quota;
				if (quota == null)
				{
					quota = new Quota();
					_quotas.Add(quotaType, quota);
				}

				return quota;
			}
			set
			{
				_quotas[quotaType] = value;
			}
		}

		public string GetCacheKey()
		{
			return Quotas.GetCacheKey(_communityID,
				_memberID);
		}


		public void Sort()
		{
			IDictionaryEnumerator de = _quotas.GetEnumerator();

			while (de.MoveNext())
			{
				((Quota)de.Value).Sort();
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <returns></returns>
		public static string GetCacheKey(Int32 communityID,
			Int32 memberID)
		{
			return "Q" + communityID.ToString() + "|" + memberID.ToString();
		}


		/// <summary>
		/// 
		/// </summary>
		public ReferenceTracker ReferenceTracker
		{
			get
			{
				if (_referenceTracker == null)
				{
					_referenceTracker = new ReferenceTracker();
				}

				return _referenceTracker;
			}
		}


		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return _cacheItemMode;

			}
			set
			{
				_cacheItemMode = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}


		#endregion

		#region IReplicable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}

		#endregion
	}
}
