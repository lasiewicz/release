using System;

using Matchnet;

namespace Matchnet.List.ValueObjects.Quotas
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class ListQuota : IValueObject
	{
		private Int32 _communityID;
		private HotListType _listType; 
		private Int32 _maxAllowed;
		private Int32 _maxAllowedDuration;
		private Int32 _maxCategories;
		private Int32 _growthLimit;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="listType"></param>
		/// <param name="maxAllowed"></param>
		/// <param name="maxAllowedDuration"></param>
		/// <param name="maxCategories"></param>
		/// <param name="growthLimit"></param>
		public ListQuota(Int32 communityID,
			HotListType listType,
			Int32 maxAllowed,
			Int32 maxAllowedDuration,
			Int32 maxCategories,
			Int32 growthLimit)
		{
			_communityID = communityID;
			_listType = listType;
			_maxAllowed = maxAllowed;
			_maxAllowedDuration = maxAllowedDuration;
			_maxCategories = maxCategories;
			_growthLimit = growthLimit;
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 MaxAllowed
		{
			get
			{
				return _maxAllowed;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 MaxAllowedDuration
		{
			get
			{
				return _maxAllowedDuration;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 MaxCategories
		{
			get
			{
				return _maxCategories;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 GrowthLimit
		{
			get
			{
				return _growthLimit;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetKey()
		{
			return ListQuota.GetKey(_communityID,
				_listType);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="listType"></param>
		/// <returns></returns>
		public static string GetKey(Int32 communityID,
			HotListType listType)
		{
			//ignore communityID for now, database does not support per-community quotas
			return listType.ToString();
			//return communityID.ToString() + "^" + listType.ToString();
		}
	}
}
