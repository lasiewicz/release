#region

using System;

#endregion

/// <summary>
///   Summary description for ListSaveStatus.
/// </summary>
[Serializable]
public enum ListActionStatus
{
    /// <summary>
    /// Success in list action taken
    /// </summary>
    Success = 0,
    /// <summary>
    /// The category passed is not category owner
    /// </summary>
    NotCategoryOwner = 1, // 519389
    /// <summary>
    /// The member has exceeded the maximum allowed
    /// </summary>
    ExceededMaxAllowed = 2, // 519388
    /// <summary>
    /// The member has exceeded maximum allowed for target member
    /// </summary>
    ExceededMaxAllowedForMember = 3, // 519431
    /// <summary>
    /// The member has already teased the target member
    /// </summary>
    HasAlreadyTeasedMember = 4,
    /// <summary>
    /// The member has already added the target member
    /// </summary>
    HasAlreadyAddedMemberToList = 5
}