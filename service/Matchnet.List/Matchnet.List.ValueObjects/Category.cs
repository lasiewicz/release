using System;

namespace Matchnet.List.ValueObjects
{
	/// <summary>
	/// Summary description for Category.
	/// </summary>
	[Serializable]
	public class Category
	{
		#region Private Members
		private int _categoryID;
		private string _description;
		#endregion

		#region Public Properties
		
		/// <summary>
		/// The category Id of the category
		/// </summary>
		public int CategoryID
		{
			get{return _categoryID;}
			set{_categoryID = value;}
		}

		/// <summary>
		/// The description or name of the category
		/// </summary>
		public string Description
		{
			get{return _description;}
			set{_description = value;}
		}

		#endregion

		#region Constructors
		/// <summary>
		/// Creates a new instance of a Category
		/// </summary>
		/// <param name="categoryID">The category ID of the category to be created.</param>
		/// <param name="description">The description of the category to be created.</param>
		public Category(int categoryID, string description)
		{
			_categoryID = categoryID;
			_description = description;
		}
		#endregion
	}
}
