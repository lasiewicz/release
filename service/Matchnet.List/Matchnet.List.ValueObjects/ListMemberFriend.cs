using System;
using System.Runtime.Serialization;
using System.Security.Permissions;


namespace Matchnet.List.ValueObjects
{
	[Serializable]
	internal class ListMemberFriend : ListMember, ISerializable
	{
		private HotListCategory _category;
		private string _comment = string.Empty;

		public ListMemberFriend(int communityID,
			int memberID,
			DateTime actionDate,
			HotListCategory category,	
			string comment)
		{
			base._communityID = communityID;
			base._memberID = memberID;
			base._actionDate = actionDate;
			_category = category;
			_comment = comment;
		}


		protected ListMemberFriend(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_category = (HotListCategory)info.GetValue("category", typeof(HotListCategory));
			_comment = info.GetString("comment");
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("category", _category);
			info.AddValue("comment", _comment);
		}

		
		public HotListCategory Category
		{
			get
			{
				return _category;
			}
		}


		public string Comment
		{
			get
			{
				return _comment;
			}
		}


		internal void setComment(string comment)
		{
			_comment = comment;
		}
	}
}
