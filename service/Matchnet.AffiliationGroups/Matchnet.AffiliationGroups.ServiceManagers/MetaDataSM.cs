﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Diagnostics;
using System.Collections;
using System.IO;

using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;

using Matchnet.Replication;
using Matchnet.CacheSynchronization;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.AffiliationGroups.ValueObjects;
using Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions;
using Matchnet.AffiliationGroups.BusinessLogic;
using Matchnet.Queuing;
using System.Messaging;
namespace Matchnet.AffiliationGroups.ServiceManagers
{
    public class MetaDataSM : MarshalByRefObject, IMetaDataService, IServiceManager, IDisposable
    {
        private const string MODULE_NAME = "MetaDataSM";
        public MetaDataSM()
        {
            string functionName = "MetaDataSM";
            try
            {
                ServiceTrace.Instance.Trace(functionName, "Begin constructor");
                
                              
                ServiceTrace.Instance.Trace(functionName, "End constructor");


            }
            catch (Exception ex)
            { ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true); }
        }

        #region IMetaDataService implementation
        public AffiliationCategorySiteCollection GetSiteAffiliationCategories(int siteid)
        {
            try
            {
                return MetaDataBL.Instance.GetSiteAffiliationCategories(siteid);
            }
            catch (Exception ex)
            { throw (ex); }
        }
        public AffiliationCategorySite GetAffiliationCategory(int categorysiteid)
        {
            try
            {
                return MetaDataBL.Instance.GetAffiliationCategory(categorysiteid);
            }
            catch (Exception ex)
            { throw (ex); }
        }
        public AffiliationCategoryOptionCollection GetAffiliationCategoryOptions(int categorysiteid)
        {
            try
            {
                return MetaDataBL.Instance.GetAffiliationCategoryOptions(categorysiteid);
            }
            catch (Exception ex)
            { throw (ex); }
        }
        public AffiliationAttributeCollection GetAttributeCollection()
        {
            try
            {
                return MetaDataBL.Instance.GetAttributeCollection();
            }
            catch (Exception ex)
            { throw (ex); }
        }
        #endregion

        #region IServiceManager
        //avoid GC
        public override object InitializeLifetimeService()
        {
            return null;
        }

        public void PrePopulateCache()
        {
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            Matchnet.Caching.Cache.Instance.Clear();

          
            //resetPerfCounters();
        }
        #endregion
    }
}
