﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;

using Matchnet.Replication;
using Matchnet.CacheSynchronization;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.AffiliationGroups.ValueObjects;
using Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions;
using Matchnet.AffiliationGroups.BusinessLogic;
using Matchnet.Queuing;
namespace Matchnet.AffiliationGroups.ServiceManagers
{
      public class AffiliationGroupSM : MarshalByRefObject, IReplicationRecipient, IAffiliationGroupService, IServiceManager, IDisposable, IAffiliationGroupMember,IMetaDataService
    {
          private const string MODULE_NAME = "AffiliationGroupSM";
        private HydraWriter _hydraWriter;
        private Replicator _replicator = null;
        private Synchronizer _synchronizer = null;

       
        #region perf counters

        private PerformanceCounter _perfGroupsMembersCachedRequestsSec;
        private PerformanceCounter _perfGroupsMembersRequestsSec;
        private PerformanceCounter _perfAddGroupsMembersRequestsSec;
        private PerformanceCounter _perfMembersResultSec;

        private PerformanceCounter _perfGroupsRequestsSec;
        private PerformanceCounter _perfGroupsCachedRequestsSec;
        private PerformanceCounter _perfGroupsResultSec;
        #endregion
        public AffiliationGroupSM()
        {
            string functionName = "AffiliationGroupSM";
            string machineName="";
            try
            {
                ServiceTrace.Instance.Trace(functionName, "Begin constructor");

                ServiceTrace.Instance.Trace(functionName, "Starting HydraWriter");
                _hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[] { "mnContent","mnPremiumServices" });
                _hydraWriter.Start();        
 
                 AffiliationGroupBL.Instance.SynchronizationRequested += new AffiliationGroupBL.SynchronizationEventHandler(AffiliationGroupBL_SynchronizationRequested);
                //replication
                AffiliationGroupBL.Instance.ReplicationRequested += new AffiliationGroupBL.ReplicationEventHandler(AffiliationGroupBL_ReplicationRequested);

                //synchronization
                AffiliationGroupMemberBL.Instance.SynchronizationRequested += new AffiliationGroupMemberBL.SynchronizationEventHandler(AffiliationGroupMemberBL_SynchronizationRequested);
                //replication
                AffiliationGroupMemberBL.Instance.ReplicationRequested += new AffiliationGroupMemberBL.ReplicationEventHandler(AffiliationGroupMemberBL_ReplicationRequested);
              
                machineName = System.Environment.MachineName;
                string replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("AFFILIATIONGROUPSSVC_REPLICATION_OVERRIDE");
              
                if (replicationURI.Length == 0)
                {
                    //string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
                    //if (overrideMachineName != null)
                    //{
                    //    machineName = overrideMachineName;
                    //}
                  
                    replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_CONSTANT,Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_MEMBER_MANAGER_NAME, machineName);
                }
                if (!String.IsNullOrEmpty(replicationURI))
                {
                    ServiceTrace.Instance.Trace(functionName, "Got replication URI:" + replicationURI);

                    _replicator = new Replicator(Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME);
                    _replicator.SetDestinationUri(replicationURI);
                    _replicator.Start();
                    ServiceTrace.Instance.Trace(functionName, "Started replicator");
                }
                else
                {
                    ServiceTrace.Instance.Trace(functionName, "Got empty replication URI");
                 
                }

                _synchronizer = new Synchronizer(Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME);
                _synchronizer.Start();

                AffiliationGroupBL.Instance.AffiliationGroupsRequested += new AffiliationGroupBL.AffiliationGroupsRequestEventHandler(AffiliationGroupBL_GroupRequested);

                AffiliationGroupMemberBL.Instance.AffiliationGroupMembersRequested += new AffiliationGroupMemberBL.AffiliationGroupMembersRequestEventHandler(AffiliationGroupMemberBL_MembersRequested);
                AffiliationGroupMemberBL.Instance.AddGroupMemberRequested += new AffiliationGroupMemberBL.AffiliationGroupAddMemberRequestEventHandler(AffiliationGroupMemberBL_MembersAdded);
                initPerfCounters();
                ServiceTrace.Instance.Trace(functionName, "Started synchronizer");
                ServiceTrace.Instance.Trace(functionName, "End constructor");

            }
            catch (Exception ex)
            { ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true); }
        }

        #region AffiliationGroupSM implementation
        public AffiliationGroupsCollection GetAffiliationGroups(int categorysiteid)
        {
            try{

                return AffiliationGroupBL.Instance.GetAffiliationGroups(categorysiteid);
            }catch(Exception ex)
            {throw(ex);}
        }

        public AffiliationGroup GetAffiliationGroup(int groupid)
        {
            try
            {

                return AffiliationGroupBL.Instance.GetGroup(groupid);
            }
            catch (Exception ex)
            { throw (ex); }
        }
        public AffiliationGroup SaveGroup(AffiliationGroup group)
        {
         try{

               return  AffiliationGroupBL.Instance.SaveGroup(group);
            }catch(Exception ex)
            {throw(ex);}
        }

        public void DeleteGroup(int categorysiteid,int groupid)
        {
            try
            {

                AffiliationGroupBL.Instance.DeleteGroup(categorysiteid,groupid);
            }
            catch (Exception ex)
            { throw (ex); }
        }
        #endregion

        #region IAffiliationGroupMember implementation
        public List<int> GetGroupMembers(int affiliationgroupid, string clientHostName)
        {
            string functionName = "GetGroupMembers";
            try
            {
                return AffiliationGroupMemberBL.Instance.GetGroupMembers(affiliationgroupid);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                throw (ex);
            }

        }
        public void AddGroupMember(int brandid, int categorysiteid, int groupid, int memberid, string clientHostName, CacheReference reference)
        {
            string functionName = "AddGroupMember";
            try
            {
                AffiliationGroupMemberBL.Instance.AddGroupMember(brandid, categorysiteid, groupid, memberid, clientHostName, reference);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                throw (ex);
            }

        }
        public void SaveAffiliationMember(int brandid, int memberid)
        {
            string functionName = "SaveAffiliationMember";
            try
            {
                AffiliationGroupMemberBL.Instance.SaveAffiliationMember(brandid, memberid);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                throw (ex);
            }
        }
        public void DeleteGroupMember(int brandid, int groupid, int categorysiteid, int memberid, string clientHostName, CacheReference reference)
        {
            string functionName = "DeleteGroupMember";
            try
            {
                AffiliationGroupMemberBL.Instance.DeleteGroupMember(brandid, groupid, categorysiteid, memberid, clientHostName, reference);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                throw (ex);
            }
        }
        public void DeleteMemberGroups(int brandid, int categorysiteid, int memberid)
        {
            string functionName = "DeleteMemberGroups";
            try
            {
                AffiliationGroupMemberBL.Instance.DeleteMemberGroups(brandid,  categorysiteid, memberid);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                throw (ex);
            }
        }
        public AffiliationMemberGroupsList GetMemberGroups(int siteid, int memberid, string clientHostName, CacheReference reference)
        {
            string functionName = "GetMemberGroups";
            try
            {
                return AffiliationGroupMemberBL.Instance.GetMemberGroups(siteid, memberid, clientHostName, reference);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                throw (ex);
            }
        }
        #endregion

        #region IMetaDataService implementation
        public AffiliationCategorySiteCollection GetSiteAffiliationCategories(int siteid)
        {
            try
            {
                return MetaDataBL.Instance.GetSiteAffiliationCategories(siteid);
            }
            catch (Exception ex)
            { throw (ex); }
        }
        public AffiliationCategorySite GetAffiliationCategory(int categorysiteid)
        {
            try
            {
                return MetaDataBL.Instance.GetAffiliationCategory(categorysiteid);
            }
            catch (Exception ex)
            { throw (ex); }
        }
        public AffiliationCategoryOptionCollection GetAffiliationCategoryOptions(int categorysiteid)
        {
            try
            {
                return MetaDataBL.Instance.GetAffiliationCategoryOptions(categorysiteid);
            }
            catch (Exception ex)
            { throw (ex); }
        }
        public AffiliationAttributeCollection GetAttributeCollection()
        {
            try
            {
                return MetaDataBL.Instance.GetAttributeCollection();
            }
            catch (Exception ex)
            { throw (ex); }
        }
        #endregion

        #region IServiceManager
        //avoid GC
        public override object InitializeLifetimeService()
        {
            return null;
        }

        public void PrePopulateCache()
        {
        }
        #endregion

        #region synchronization/replication events

        private void AffiliationGroupBL_ReplicationRequested(IReplicable replicableObject)
        {
            if (_replicator != null)
                _replicator.Enqueue(replicableObject);
        }


        private void AffiliationGroupBL_SynchronizationRequested(string key, System.Collections.Hashtable cacheReferences)
        {
            if (_synchronizer != null)
                _synchronizer.Enqueue(key, cacheReferences);
        }


        private void AffiliationGroupMemberBL_ReplicationRequested(IReplicable replicableObject)
        {
            if (_replicator != null)
                _replicator.Enqueue(replicableObject);
        }


        private void AffiliationGroupMemberBL_SynchronizationRequested(string key, System.Collections.Hashtable cacheReferences)
        {
            if (_synchronizer != null)
                _synchronizer.Enqueue(key, cacheReferences);
        }
        #endregion

        #region IReplicationRecipient

        void IReplicationRecipient.Receive(IReplicable replicableObject)
        {
            if (replicableObject.GetType() == typeof(AffiliationGroupsCollection))
                AffiliationGroupBL.Instance.InsertCacheGroups((AffiliationGroupsCollection)replicableObject, false, false);
            else if (replicableObject.GetType() == typeof(AffiliationGroup))
                AffiliationGroupBL.Instance.InsertCacheGroup((AffiliationGroup)replicableObject, false, false);
            else if (replicableObject.GetType() == typeof(AffiliationMemberGroupsList))
                AffiliationGroupMemberBL.Instance.InsertCacheMemberGroups((AffiliationMemberGroupsList)replicableObject, false, false,null);
        }


        #endregion

        private void initPerfCounters()
        {
            string servicename = Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME;
            _perfGroupsRequestsSec = new PerformanceCounter(servicename, "Groups Requests/second", false);
            _perfGroupsCachedRequestsSec = new PerformanceCounter(servicename, "Groups Cached Requests/second", false);
           
            _perfGroupsResultSec = new PerformanceCounter(servicename, "Groups Results/second", false);

            _perfGroupsMembersRequestsSec = new PerformanceCounter(servicename, "Groups Members Requests/second", false);
            _perfGroupsMembersCachedRequestsSec = new PerformanceCounter(servicename, "Groups Members Cached Requests/second", false);
            _perfAddGroupsMembersRequestsSec = new PerformanceCounter(servicename, "Groups Add Members Requests/second", false);

            _perfMembersResultSec = new PerformanceCounter(servicename, "Groups Members Results/second", false);

        }


        private void resetPerfCounters()
        {
            _perfGroupsRequestsSec.RawValue = 0;
            _perfGroupsCachedRequestsSec.RawValue = 0;
     
            _perfGroupsResultSec.RawValue = 0;

            _perfGroupsMembersRequestsSec.RawValue = 0;
            _perfGroupsMembersCachedRequestsSec.RawValue = 0;
            _perfAddGroupsMembersRequestsSec.RawValue = 0;

            _perfMembersResultSec.RawValue = 0;


        }

        private void AffiliationGroupBL_GroupRequested(bool cacheHit, int count)
        {
            try
            {

                _perfGroupsRequestsSec.Increment();
                if (cacheHit)
                    _perfGroupsCachedRequestsSec.Increment();
                _perfGroupsResultSec.IncrementBy((long)count);
                //_perfSpotlightResultItemsSec.IncrementBy((long)count);
            }
            catch (Exception ex)
            { }

        }

        private void AffiliationGroupMemberBL_MembersRequested(bool cache, int count)
        {
            try
            {

                _perfGroupsMembersRequestsSec.Increment();
                if (cache)
                    _perfGroupsMembersCachedRequestsSec.Increment();

                _perfMembersResultSec.IncrementBy((long)count);
            }
            catch (Exception ex)
            { }


        }


        private void AffiliationGroupMemberBL_MembersAdded()
        {
            try
            {

                _perfAddGroupsMembersRequestsSec.Increment();
            }
            catch (Exception ex)
            { }

        }

        public static void PerfCounterInstall()
        {
            try
            {
                ServiceTrace.Instance.DebugTrace("AffiliationGroupMemberSM", "Start PerfCounterInstall");
                CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
                ccdc.AddRange(new CounterCreationData[] {	
															 new CounterCreationData("Groups Requests/second", "Groups Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
                                                             new CounterCreationData("Groups Cached Requests/second", "Groups Cached Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
															 new CounterCreationData("Groups Members Cached Requests/second", "Groups Members Cached Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
															 new CounterCreationData("Groups Members Requests/second","Groups Members Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
															 new CounterCreationData("Groups Add Members Requests/second", "Groups Add Members Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
                                                             new CounterCreationData("Groups Members Results/second", "Groups Members Results/second", PerformanceCounterType.RateOfCountsPerSecond32),
                                                              new CounterCreationData("Groups Results/second", "Groups Results/second", PerformanceCounterType.RateOfCountsPerSecond32)
																													 
														 });

                PerformanceCounterCategory.Create(AffiliationGroups.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME, AffiliationGroups.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME, ccdc);
                ServiceTrace.Instance.DebugTrace("AffiliationGroupMemberSM", "Finish PerfCounterInstall");
            }
            catch (Exception ex)
            { ServiceTrace.Instance.Trace("AffiliationGroupMemberSM", ex, null); }
        }


        public static void PerfCounterUninstall()
        {
            PerformanceCounterCategory.Delete(AffiliationGroups.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME);
        }

        #region IDisposable Members
        public void Dispose()
        {
            Matchnet.Caching.Cache.Instance.Clear();
            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }

            if (_replicator != null)
            {
                _replicator.Stop();
            }

            if (_synchronizer != null)
            {
                _synchronizer.Stop();
            }

          
            //resetPerfCounters();
        }
        #endregion
    }
}

