﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Diagnostics;
using System.Collections;
using System.IO;

using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;

using Matchnet.Replication;
using Matchnet.CacheSynchronization;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.AffiliationGroups.ValueObjects;
using Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions;
using Matchnet.AffiliationGroups.BusinessLogic;
using Matchnet.Queuing;
using System.Messaging;
using Matchnet.CacheSynchronization.ValueObjects;
namespace Matchnet.AffiliationGroups.ServiceManagers
{
    public class AffiliationGroupMemberSM : MarshalByRefObject,  IServiceManager,  IDisposable
    {
        private const string MODULE_NAME = "AffiliationGroupMemberSM";
        private HydraWriter _hydraWriter;
        private Replicator _replicator = null;
        private Synchronizer _synchronizer = null;
        //QueueProcessor _queueProcessor = null;
        #region perf counters
        private PerformanceCounter _perfMemberUpdateRequestsSec;
        private PerformanceCounter _perfMemberRequestsSec;
        private PerformanceCounter _perfCachedMemberRequestsSec;
        
        #endregion

        public AffiliationGroupMemberSM()
        {
            string functionName = "SpotlightMemberSM";
            try
            {
                ServiceTrace.Instance.Trace(functionName, "Begin constructor");
                string machineName = "";

                ServiceTrace.Instance.Trace(functionName, "Register synchronization/replication events");
                //synchronization
                AffiliationGroupMemberBL.Instance.SynchronizationRequested += new AffiliationGroupMemberBL.SynchronizationEventHandler(AffiliationGroupMemberBL_SynchronizationRequested);
                //replication
                AffiliationGroupMemberBL.Instance.ReplicationRequested += new AffiliationGroupMemberBL.ReplicationEventHandler(AffiliationGroupMemberBL_ReplicationRequested);

                //ServiceTrace.Instance.Trace(functionName, "Starting queue processor");
               // _queueProcessor = new QueueProcessor();
                //_queueProcessor.Start();

                ServiceTrace.Instance.Trace(functionName, "Starting HydraWriter");
                _hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[] { "mnPremiumServices" });
                _hydraWriter.Start();

                ServiceTrace.Instance.Trace(functionName, "Getting replication URI");
                string replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("AFFILIATIONGROUPSSVC_REPLICATION_OVERRIDE");
              
                if (replicationURI.Length == 0)
                {
                    string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
                    if (overrideMachineName != null)
                    {
                        machineName = overrideMachineName;
                    }
                    replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_CONSTANT,Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_MEMBER_MANAGER_NAME, machineName);
                }
                if (!String.IsNullOrEmpty(replicationURI))
                { 
                    ServiceTrace.Instance.Trace(functionName, "Got replication URI:" + replicationURI);

                    _replicator = new Replicator(Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME);
                    _replicator.SetDestinationUri(replicationURI);
                    _replicator.Start();
                    ServiceTrace.Instance.Trace(functionName, "Started replicator");
                }
                else
                {
                    ServiceTrace.Instance.Trace(functionName, "Got empty replication URI" ); 
                }



                _synchronizer = new Synchronizer(Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions.ServiceConstants.SERVICE_NAME);
                _synchronizer.Start();
                ServiceTrace.Instance.Trace(functionName, "Started synchronizer");

               // ServiceTrace.Instance.Trace(functionName, "Registering perf counters events");
                //SpotlightMemberBL.Instance.SpotlightMemberRequested += new SpotlightMemberBL.SpotlightMemberRequestEventHandler(SpotlightMemberBL_SpotlightMemberRequested);
                //SpotlightMemberBL.Instance.SpotlightMemberUpdated += new SpotlightMemberBL.SpotlightMemberUpdateEventHandler(SpotlightMemberBL_SpotlightMemberUpdated);
                //SpotlightSearchBL.Instance.SpotlightSearchRequested += new SpotlightSearchBL.SpotlightSearchRequestEventHandler(SpotlightSearchBL_SpotlightSearchRequested);
                //initPerfCounters();

                              
                ServiceTrace.Instance.Trace(functionName, "End constructor");


            }
            catch (Exception ex)
            { ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true); }
        }
        
        #region IServiceManager
        //avoid GC
        public override object InitializeLifetimeService()
        {
            return null;
        }

        public void PrePopulateCache()
        {
        }
        #endregion

        #region IAffiliationGroupMember implementation
        public List<int> GetGroupMembers(int affiliationgroupid)
        {
            string functionName = "GetGroupMembers";
            try
            {
                return AffiliationGroupMemberBL.Instance.GetGroupMembers(affiliationgroupid);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                throw (ex);
            }

        }
        public void AddGroupMember(int brandid, int categoryid, int groupid, int memberid, string clientHostName, CacheReference reference)
        {
            string functionName = "AddGroupMember";
            try
            {
                AffiliationGroupMemberBL.Instance.AddGroupMember(brandid, categoryid, groupid, memberid, clientHostName,reference);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                throw (ex);
            }

        }
        public void SaveAffiliationMember(int brandid, int memberid)
        {
            string functionName = "SaveAffiliationMember";
            try
            {
                AffiliationGroupMemberBL.Instance.SaveAffiliationMember(brandid, memberid);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                throw (ex);
            }
        }
        public void DeleteGroupMember(int brandid, int groupid, int categorysiteid, int memberid, string clientHostName, CacheReference reference)
        {
            string functionName = "DeleteGroupMember";
            try
            {
                AffiliationGroupMemberBL.Instance.DeleteGroupMember(brandid, groupid, categorysiteid, memberid, clientHostName,reference);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                throw (ex);
            }
        }
        public AffiliationMemberGroupsList GetMemberGroups(int siteid,int memberid,string clientHostName, CacheReference reference)
        {
            string functionName = "GetMemberGroups";
            try
            {
                return AffiliationGroupMemberBL.Instance.GetMemberGroups(siteid, memberid, clientHostName, reference);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
                throw (ex);
            }
        }
        #endregion

   

        #region synchronization/replication events

        private void AffiliationGroupMemberBL_ReplicationRequested(IReplicable replicableObject)
        {
            if (_replicator != null)
                _replicator.Enqueue(replicableObject);
        }


        private void AffiliationGroupMemberBL_SynchronizationRequested(string key, System.Collections.Hashtable cacheReferences)
        {
            if (_synchronizer != null)
                _synchronizer.Enqueue(key, cacheReferences);
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            Matchnet.Caching.Cache.Instance.Clear();

            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }

            if (_replicator != null)
            {
                _replicator.Stop();
            }

            if (_synchronizer != null)
            {
                _synchronizer.Stop();
            }


            //resetPerfCounters();
        }
        #endregion
    }

}
