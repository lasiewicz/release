﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;


namespace Matchnet.AffiliationGroups
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
        private void ProjectInstaller_AfterInstall(object sender, InstallEventArgs e)
        {
            Matchnet.AffiliationGroups.ServiceManagers.AffiliationGroupSM.PerfCounterInstall();

        }

        private void ProjectInstaller_BeforeUninstall(object sender, InstallEventArgs e)
        {
            Matchnet.AffiliationGroups.ServiceManagers.AffiliationGroupSM.PerfCounterUninstall();
        }
    }
}
