﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.AffiliationGroups.ServiceManagers;
using Matchnet.AffiliationGroups.ValueObjects;
using Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions;
namespace Matchnet.AffiliationGroups
{
    public partial class AffiliationGroupsService : Matchnet.RemotingServices.RemotingServiceBase
    {
        private AffiliationGroupSM affiliationGroupSM;
        //private AffiliationGroupMemberSM affiliationGroupMemberSM;
        //private MetaDataSM metaDataSM;

        public AffiliationGroupsService()
        {
            InitializeComponent();
        }


        protected override void RegisterServiceManagers()
        {
            try
            {
                System.Diagnostics.Trace.Write("Starting service:" + ServiceConstants.SERVICE_CONSTANT);
               // affiliationGroupMemberSM = new AffiliationGroupMemberSM();
                affiliationGroupSM = new AffiliationGroupSM();
              //  metaDataSM = new MetaDataSM();
              ///  base.RegisterServiceManager(affiliationGroupMemberSM);
                base.RegisterServiceManager(affiliationGroupSM);
              //  base.RegisterServiceManager(metaDataSM);

            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceName, "Error occurred when starting the Windows Service", ex);
            }
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "An unhandled exception occured.", e.ExceptionObject as Exception);
        }
        protected override void OnStart(string[] args)
        {
            try
            {
                //System.Threading.Thread.Sleep(30000);
                base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
                base.OnStart(args);
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(ex.Source, "Exception on service start:" + ex.Message));
            }
        }

        protected override void OnStop()
        {
            try
            {
                //if(affiliationGroupMemberSM != null)
                //    affiliationGroupMemberSM.Dispose();
                if(affiliationGroupSM != null)
                    affiliationGroupSM.Dispose();
                //if (metaDataSM != null)
                //    metaDataSM.Dispose();
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(ex.Source, "Exception on service stop:" + ex.Message));
            }

        }
    }
}

