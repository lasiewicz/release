﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Matchnet.AffiliationGroups.BusinessLogic;
using Matchnet.AffiliationGroups.ValueObjects;
using Matchnet.AffiliationGroups.ServiceManagers;
namespace TestClient1
{
    public partial class TestBL : Form
    {
        AffiliationGroupSM groupSM = null;
        public TestBL()
        {
            InitializeComponent();
            groupSM = new AffiliationGroupSM();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AffiliationGroup group = new AffiliationGroup();
            group.CategorySiteID = 1001;
            group.ActiveFlag = true;
            group.About = "Lakers, 2009 NBA champions";
            group.Name = "Los Angeles Lakers";
            group.Head = "General Manager Mitch Kupchak";
            group.URL = "http://www.nba.com/lakers";
            group.Address1 = "555 N. Nash Street";
            group.Address2="";
            group.City="El Segundo";
            group.State="CA";
            group.PostalCode="90245";
            group.Phone = "";
            group.Email = "feedback@la-lakers.com";
            group.Image = "image.jpg";
            groupSM.SaveGroup(group);
        }

        private void button2_Click(object sender, EventArgs e)
        {
         //  AffiliationGroupMemberSM sm = new AffiliationGroupMemberSM();
           // sm.AddGroupMember(Int32.Parse(txtBrandUD.Text), Int32.Parse(txtCategoryID.Text), Int32.Parse(txtGroupID.Text), Int32.Parse(txtMemberID.Text), System.Environment.MachineName, null);
        }

        private void TestBL_Load(object sender, EventArgs e)
        {
           
        }
    }
}
