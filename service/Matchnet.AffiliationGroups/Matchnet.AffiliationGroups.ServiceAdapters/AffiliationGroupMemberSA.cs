﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Matchnet;
using Matchnet.Geo;
using Matchnet.Caching;
using Matchnet.Configuration;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.AffiliationGroups.ValueObjects;
using Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions;
using Matchnet.CacheSynchronization.Context;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
namespace Matchnet.AffiliationGroups.ServiceAdapters
{
    public class AffiliationGroupMemberSA:SABase
    {
               private Cache _cache;
       string testURI = "";
       public static readonly AffiliationGroupMemberSA Instance = new AffiliationGroupMemberSA();


       private AffiliationGroupMemberSA()
       {
            _cache = Cache.Instance;
       }

       #region service implementation

    
     
       public List<int> GetGroupMembers(int affiliationgroupid)
       {
           try
           {
               AffiliationGroupMemberList members = (AffiliationGroupMemberList)_cache.Get(AffiliationGroupMemberList.GetCacheKey(affiliationgroupid));
               if (members != null)
                   return members.Members;
               List<int> list = null;
               string uri = getServiceManagerUri();
               base.Checkout(uri);
               try
               {
                   list = getService(uri).GetGroupMembers(affiliationgroupid, System.Environment.MachineName);
                  
               }
               finally
               {
                   base.Checkin(uri);
               }

               if (list != null)
               {
                   members = new AffiliationGroupMemberList();
                   members.AffiliationGroupID = affiliationgroupid;
                   members.Members = list;
                   _cache.Insert(members);

               }
              
               return members.Members;
           }
           catch (Exception ex)
           { throw new SAException(ex); }

       }

       public List<int> GetGroupMembers(int affiliationgroupid, int page, int pagesize, out int total)
       {
           try
           {

               List<int> members = GetGroupMembers(affiliationgroupid);
               List<int> list = null;
               int startPoint=0;
                if (members == null)
                {   total=0;
                    return null;
                }

              
			   startPoint=pagesize *(page - 1);
               if (startPoint > members.Count - 1)
                   startPoint = members.Count - 1;

               if (startPoint < 0)
               {
                   total = 0;
                   return null;
               }
               int count = members.Count - startPoint < pagesize ? members.Count - startPoint : pagesize;
               list = members.GetRange(startPoint, count);

               total = list.Count;
               return list;

           }
           catch (Exception ex)
           { throw new SAException(ex); }

       }

       public AffiliationMemberGroupsList GetMemberGroups(int siteid, int memberid)
       {
           try
           {
               AffiliationMemberGroupsList membergroups = (AffiliationMemberGroupsList)_cache.Get(AffiliationMemberGroupsList.GetCacheKey(siteid, memberid));
               Int32 cacheTTL = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("AFFILIATIONGROUPSSVC_CACHE_TTL_SA"));
               if (membergroups != null)
                   return membergroups;
               AffiliationMemberGroupsList list = null;
               string uri = getServiceManagerUri();
               base.Checkout(uri);
               try
               {
                   list = getService(uri).GetMemberGroups(siteid,  memberid, System.Environment.MachineName,Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)));

               }
               finally
               {
                   base.Checkin(uri);
               }

               if (list != null)
               {

                   _cache.Insert(list);

               }

               return list;
           }
           catch (Exception ex)
           { throw new SAException(ex); }

       }

       public void AddGroupMember(int brandid, int categorysiteid, int groupid, int memberid)
       {
           try
           {
              
               string uri = getServiceManagerUri();
               base.Checkout(uri);
               try
               {
                   Int32 cacheTTL = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("AFFILIATIONGROUPSSVC_CACHE_TTL_SA"));
                   getService(uri).AddGroupMember(brandid, categorysiteid, groupid, memberid, System.Environment.MachineName,Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)));
                   Brand b = BrandConfigSA.Instance.GetBrandByID(brandid);
                   _cache.Remove(AffiliationMemberGroupsList.GetCacheKey(b.Site.SiteID, memberid));
                   _cache.Remove(AffiliationGroupMemberList.GetCacheKey(groupid));
                   try
                   {
                       AffiliationMemberGroupsList list = GetMemberGroups(b.Site.SiteID, memberid);
                       if (list == null)
                           list = new AffiliationMemberGroupsList();
                       if (!list.IfGroupMember(categorysiteid, groupid))
                       {
                           list.Add(categorysiteid, groupid);

                       }
                       _cache.Insert(list);
                   }
                   catch (Exception ex) { }
               }
               finally
               {
                   base.Checkin(uri);
               }

           }
           catch (Exception ex)
           { throw new SAException(ex); }

       }

       public void DeleteMemberGroups(int siteid, int brandid, int categorysiteid, int memberid)
       {
           try
           {

            
               string uri = getServiceManagerUri();
               base.Checkout(uri);
               try
               {
                    getService(uri).DeleteMemberGroups(brandid, categorysiteid, memberid);

               }
               finally
               {
                   base.Checkin(uri);
               }

               _cache.Remove(AffiliationMemberGroupsList.GetCacheKey(siteid, memberid));
               try
               {
                   Brand b = BrandConfigSA.Instance.GetBrandByID(brandid);
                   AffiliationMemberGroupsList list = GetMemberGroups(b.Site.SiteID, memberid);
                   if (list != null)
                   {
                       list.Groups.Clear();
                       _cache.Insert(list);
                   }
               }
               catch (Exception ex) { }
           }
           catch (Exception ex)
           { throw new SAException(ex); }


       }

       #endregion


       #region SABase implementation
       protected override void GetConnectionLimit()
       {
           base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("AFFILIATIONGROUPSSVC_SA_CONNECTION_LIMIT"));
       }
       #endregion SABase implementation


       #region service proxy

       private IAffiliationGroupMember getService(string uri)
       {
         
           try
           {
               return (IAffiliationGroupMember)Activator.GetObject(typeof(IAffiliationGroupMember), uri);
           }
           catch (Exception ex)
           {
               throw (new SAException("Cannot activate remote service manager at " + uri, ex));
           }
       }


       private string getServiceManagerUri()
       {
           string uri = "";
           try
           {
               if (!String.IsNullOrEmpty(testURI))
                   uri = testURI;
               else
               {
                   uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(ServiceConstants.SERVICE_GROUP_MANAGER_NAME);
               }
               string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(ServiceConstants.SERVICE_SA_HOST_OVERRIDE);

               if (overrideHostName.Length > 0)
               {
                   UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                   return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
               }

               return uri;
           }
           catch (Exception ex)
           {
               throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
           }
       }


       public String TestURI
       {
           get { return testURI; }
           set { testURI = value; }
       }
       #endregion
    }
}
