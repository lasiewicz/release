﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Matchnet;
using Matchnet.Geo;
using Matchnet.Caching;
using Matchnet.Configuration;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.AffiliationGroups.ValueObjects;
using Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions;

namespace Matchnet.AffiliationGroups.ServiceAdapters
{
      public class MetaDataSA : SABase
    {
       private Cache _cache;
       string testURI = "";
       public static readonly MetaDataSA Instance = new MetaDataSA();


       private MetaDataSA()
       {
            _cache = Cache.Instance;
       }

       #region service implementation
       public AffiliationCategorySiteCollection GetSiteAffiliationCategories(int siteid)
       {
           try
           {
               AffiliationCategorySiteCollection coll=(AffiliationCategorySiteCollection)_cache.Get(AffiliationCategorySiteCollection.GetCacheKey(siteid));
               if (coll != null)
                   return coll;
               string uri = getServiceManagerUri();
               base.Checkout(uri);
               try
               {
                   coll = getService(uri).GetSiteAffiliationCategories(siteid);
               }
               finally
               {
                   base.Checkin(uri);
               }

               if (coll != null)
                   _cache.Insert(coll);

               return coll;
           }
           catch (Exception ex)
           { throw new SAException(ex); }

       }

       public AffiliationCategorySite GetAffiliationCategory(int categorysiteid)
       {
           try
           {
               AffiliationCategorySite category = (AffiliationCategorySite)_cache.Get(AffiliationCategorySite.GetCacheKey(categorysiteid));
               if (category != null)
                   return category;
               string uri = getServiceManagerUri();
               base.Checkout(uri);
               try
               {
                   category = getService(uri).GetAffiliationCategory(categorysiteid);
               }
               finally
               {
                   base.Checkin(uri);
               }

               if (category != null)
                   _cache.Insert(category);

               return category;
           }
           catch (Exception ex)
           { throw new SAException(ex); }

       }

       public AffiliationCategoryOptionCollection GetAffiliationCategoryOptions(int categorysiteid)
       {
           try
           {
               AffiliationCategoryOptionCollection coll = (AffiliationCategoryOptionCollection)_cache.Get(AffiliationCategoryOptionCollection.GetCacheKey(categorysiteid));
               if (coll != null)
                   return coll;
               string uri = getServiceManagerUri();
               base.Checkout(uri);
               try
               {
                   coll = getService(uri).GetAffiliationCategoryOptions(categorysiteid);
               }
               finally
               {
                   base.Checkin(uri);
               }

               if (coll != null)
                   _cache.Insert(coll);

               return coll;
           }
           catch (Exception ex)
           { throw new SAException(ex); }

       }

       public AffiliationAttributeCollection GetAttributeCollection()
       {
           try
           {
               AffiliationAttributeCollection coll = (AffiliationAttributeCollection)_cache.Get(AffiliationAttributeCollection.CACHEKEY);
               if (coll != null)
                   return coll;
               string uri = getServiceManagerUri();
               base.Checkout(uri);
               try
               {
                   coll = getService(uri).GetAttributeCollection();
               }
               finally
               {
                   base.Checkin(uri);
               }

               if (coll != null)
                   _cache.Insert(coll);

               return coll;
           }
           catch (Exception ex)
           { throw new SAException(ex); }

       }

       #endregion


       #region SABase implementation
       protected override void GetConnectionLimit()
       {
           base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("AFFILIATIONGROUPSSVC_SA_CONNECTION_LIMIT"));
       }
       #endregion SABase implementation


       #region service proxy

       private IMetaDataService getService(string uri)
       {
         
           try
           {
               return (IMetaDataService)Activator.GetObject(typeof(IMetaDataService), uri);
           }
           catch (Exception ex)
           {
               throw (new SAException("Cannot activate remote service manager at " + uri, ex));
           }
       }


       private string getServiceManagerUri()
       {
           string uri = "";
           try
           {
               if (!String.IsNullOrEmpty(testURI))
                   uri = testURI;
               else
               {
                   uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(ServiceConstants.SERVICE_GROUP_MANAGER_NAME);
               }
               string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(ServiceConstants.SERVICE_SA_HOST_OVERRIDE);

               if (overrideHostName.Length > 0)
               {
                   UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                   return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
               }

               return uri;
           }
           catch (Exception ex)
           {
               throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
           }
       }


       public String TestURI
       {
           get { return testURI; }
           set { testURI = value; }
       }
       #endregion
    }
}
