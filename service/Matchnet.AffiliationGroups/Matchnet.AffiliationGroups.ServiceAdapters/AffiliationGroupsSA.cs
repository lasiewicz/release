﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Matchnet;
using Matchnet.Geo;
using Matchnet.Caching;
using Matchnet.Configuration;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.AffiliationGroups.ValueObjects;
using Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions;
namespace Matchnet.AffiliationGroups.ServiceAdapters
{
    public class AffiliationGroupsSA:SABase
    {
       private Cache _cache;
       string testURI = "";
       public static readonly AffiliationGroupsSA Instance = new AffiliationGroupsSA();


       private AffiliationGroupsSA()
       {
            _cache = Cache.Instance;
       }

       #region service implementation


     
       public AffiliationGroupsCollection GetAffiliationGroups(int categorysiteid)
       {
           try
           {
               if (categorysiteid <= 0)
               {
                   throw(new SAException("Affiliation Group Category cannot be less or equal 0: " + categorysiteid.ToString()));
               }
               AffiliationGroupsCollection groups = (AffiliationGroupsCollection)_cache.Get(AffiliationGroupsCollection.GetCacheKey(categorysiteid));
               if (groups != null)
                   return groups;
               string uri = getServiceManagerUri();
               base.Checkout(uri);
               try
               {
                   groups = getService(uri).GetAffiliationGroups(categorysiteid);
               }
               finally
               {
                   base.Checkin(uri);
               }

               if (groups != null)
                   _cache.Insert(groups);

               return groups;
           }
           catch (Exception ex)
           { throw new SAException(ex); }

       }


       public AffiliationGroup GetAffiliationGroup(int groupid)
       {
           try
           {
               if (groupid <= 0)
               {
                   return null;
               }
               AffiliationGroup group = (AffiliationGroup)_cache.Get(AffiliationGroup.GetCacheKey(groupid));
               if (group != null)
                   return group;
               string uri = getServiceManagerUri();
               base.Checkout(uri);
               try
               {
                   group = getService(uri).GetAffiliationGroup(groupid);
               }
               finally
               {
                   base.Checkin(uri);
               }

               if (group != null)
                   _cache.Insert(group);

               return group;
           }
           catch (Exception ex)
           { throw new SAException(ex); }

       }
     


       public AffiliationGroup SaveGroup(AffiliationGroup group)
       {
           try
           {
              
               string uri = getServiceManagerUri();
               base.Checkout(uri);
               try
               {
                   bool newgroup = false;
                   if (group.AffiliationGroupID <= 0)
                       newgroup = true;
                   
                   group= getService(uri).SaveGroup(group);
                 

                  _cache.Insert(group);
                   //don't want to deal with internal objects and soo, after all how many people will edit groups
                  _cache.Remove(AffiliationGroupsCollection.GetCacheKey(group.CategorySiteID));
                  return group;
               }
               finally
               {
                   base.Checkin(uri);
               }

           }
           catch (Exception ex)
           { throw new SAException(ex); }

       }


       public void DeleteGroup(int categorysiteid,int groupid)
       {
           try
           {

               string uri = getServiceManagerUri();
               base.Checkout(uri);
               try
               {
                   bool newgroup = false;
                    getService(uri).DeleteGroup(categorysiteid,groupid);


                 
                   //don't want to deal with internal objects and soo, after all how many people will edit groups
                   _cache.Remove(AffiliationGroupsCollection.GetCacheKey(categorysiteid));
                  
               }
               finally
               {
                   base.Checkin(uri);
               }

           }
           catch (Exception ex)
           { throw new SAException(ex); }

       }


       public void AddGroupToCategory(AffiliationGroup group)
       {
           try
           {
               AffiliationGroupsCollection groups = GetAffiliationGroups(group.CategorySiteID);
               groups.Groups.Add(group);
               _cache.Insert(groups);

           }catch(Exception ex)
               {throw new SAException(ex); }
       }

       //public List<AffiliationGroup> GetTopGroups(int categorysiteid, int start, int limit)
       //{
       //    try
       //    {
       //        List<AffiliationGroup> list = null;
       //        AffiliationGroupsCollection groups = (AffiliationGroupsCollection)_cache.Get(AffiliationGroupsCollection.GetCacheKey(categorysiteid,"top"));
       //        if(groups!=null)
       //        {
       //            return groups.GetGroups(start, limit);
                   
       //        }
       //        groups = GetAffiliationGroups(categorysiteid);
       //        List<AffiliationGroup> toplist = (from g in groups.Groups
       //                                          where g.AffiliationGroupID > 0
       //                                          orderby g.MembershipCount descending
       //                                          select g).ToList<AffiliationGroup>();
               
       //        groups.SortType="top";
       //        _cache.Insert(groups);
               
       //        list = toplist.GetRange(start, limit);
               

       //        return list;
               
       //    }
       //    catch (Exception ex)
       //    { throw new SAException(ex); }

       //}

       //public List<AffiliationGroup> GetNewGroups(int categorysiteid, int start, int limit)
       //{
       //    try
       //    {
       //        List<AffiliationGroup> list = null;

       //        AffiliationGroupsCollection groups = (AffiliationGroupsCollection)_cache.Get(AffiliationGroupsCollection.GetCacheKey(categorysiteid, "new"));
       //        if (groups != null)
       //        {
       //            return groups.GetGroups(start, limit);

       //        }
       //        groups = GetAffiliationGroups(categorysiteid);
       //        List<AffiliationGroup> newlist = (from g in groups.Groups
       //                                          where g.AffiliationGroupID > 0
       //                                          orderby g.CreatedDate descending
       //                                          select g).ToList<AffiliationGroup>(); ;

       //        groups.SortType = "new";
       //        _cache.Insert(groups);

               
       //        list = newlist.GetRange(start, limit);
               
       //        return list;
       //    }
       //    catch (Exception ex)
       //    { throw new SAException(ex); }

       //}

       #endregion


       #region SABase implementation
       protected override void GetConnectionLimit()
       {
           base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("AFFILIATIONGROUPSSVC_SA_CONNECTION_LIMIT"));
       }
       #endregion SABase implementation


       #region service proxy

       private IAffiliationGroupService getService(string uri)
       {
         
           try
           {
               return (IAffiliationGroupService)Activator.GetObject(typeof(IAffiliationGroupService), uri);
           }
           catch (Exception ex)
           {
               throw (new SAException("Cannot activate remote service manager at " + uri, ex));
           }
       }


       private string getServiceManagerUri()
       {
           string uri = "";
           try
           {
               if (!String.IsNullOrEmpty(testURI))
                   uri = testURI;
               else
               {
                   uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(ServiceConstants.SERVICE_GROUP_MANAGER_NAME);
               }
               string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(ServiceConstants.SERVICE_SA_HOST_OVERRIDE);

               if (overrideHostName.Length > 0)
               {
                   UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                   return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
               }

               return uri;
           }
           catch (Exception ex)
           {
               throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
           }
       }


       public String TestURI
       {
           get { return testURI; }
           set { testURI = value; }
       }
       #endregion
    }
}
