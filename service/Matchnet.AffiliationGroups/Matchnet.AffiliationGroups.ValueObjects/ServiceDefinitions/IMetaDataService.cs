﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.AffiliationGroups.ValueObjects;

namespace Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions
{
    public interface IMetaDataService
    {
         AffiliationCategorySiteCollection GetSiteAffiliationCategories(int siteid);
         AffiliationCategorySite GetAffiliationCategory(int categorysiteid);
         AffiliationCategoryOptionCollection GetAffiliationCategoryOptions(int categorysiteid);
         AffiliationAttributeCollection GetAttributeCollection();


    }
}
