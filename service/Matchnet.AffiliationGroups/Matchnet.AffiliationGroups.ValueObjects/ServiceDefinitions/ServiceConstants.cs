﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions
{
    public class ServiceConstants
    {
        public const string SERVICE_CONSTANT = "AFFILIATIONGROUPS_SVC";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_NAME = "Matchnet.AffiliationGroups.Service";
        /// <summary>
        /// 
        /// </summary>

        public const string SERVICE_SA_HOST_OVERRIDE = "AFFILIATIONGROUPSSVC_SA_HOST_OVERRIDE";

        public const string SERVICE_METADATA_MANAGER_NAME = "MetaDataSM";

        public const string SERVICE_MEMBER_MANAGER_NAME = "AffiliationGroupMemberSM";
        public const string SERVICE_GROUP_MANAGER_NAME = "AffiliationGroupSM";

        public const double DEFAULT_PARTITION_ANGLE = 0.1;
    }
}
