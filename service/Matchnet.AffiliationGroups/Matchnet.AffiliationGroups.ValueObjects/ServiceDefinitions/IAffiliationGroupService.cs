﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions
{
    public interface IAffiliationGroupService
    {
        
        AffiliationGroupsCollection GetAffiliationGroups(int categorysiteid);
        AffiliationGroup GetAffiliationGroup(int groupid);
        AffiliationGroup SaveGroup(AffiliationGroup group);
        void DeleteGroup(int categorysiteid,int groupid);
    }
}
