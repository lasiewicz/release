﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.CacheSynchronization.ValueObjects;
namespace Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions
{
    public interface IAffiliationGroupMember
    {
        List<int> GetGroupMembers(int affiliationgroupid, string clientHostName);
        void AddGroupMember(int brandid, int categorysiteid, int groupid, int memberid, string clientHostName, CacheReference reference);
        void SaveAffiliationMember(int brandid, int memberid);
        void DeleteGroupMember(int brandid, int groupid, int categorysiteid, int memberid, string clientHostName, CacheReference reference);
        AffiliationMemberGroupsList GetMemberGroups(int siteid, int memberid, string clientHostName, CacheReference reference);
        void DeleteMemberGroups(int brandid, int categorysiteid, int memberid);
    }
}
