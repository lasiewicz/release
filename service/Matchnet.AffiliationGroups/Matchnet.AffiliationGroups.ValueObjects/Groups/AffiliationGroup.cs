﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Matchnet.CacheSynchronization.Tracking;

namespace Matchnet.AffiliationGroups.ValueObjects
{
    #region class AffiliationGroup
    [Serializable]
    public class AffiliationGroup : IValueObject, ICacheable,IReplicable
    {
        int _affiliationgroupid;
        int _categorysiteid;
        bool _activeflag;
        string _name = null;
        string _head = null;
        string _address1 = null;
        string _address2 = null;
        string _city = null;
        string _postalcode = null;
        string _state = null;
        string _phone = null;
        string _email = null;
        string _url = null;
        string _about = null;
        string _image = null;
        int _membershipcount;
        DateTime _createddate;
        DateTime _updatedate;

        List<AffiliationGroupOption> _options;
        List<AttributeValue> _groupattibutes;

        public int AffiliationGroupID
        {
            get { return _affiliationgroupid; }
            set { _affiliationgroupid = value; }
        }


        public int CategorySiteID
        {
            get { return _categorysiteid; }
            set { _categorysiteid = value; }
        }


        public bool ActiveFlag
        {
            get { return _activeflag; }
            set { _activeflag = value; }
        }


        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }


        public string Head
        {
            get { return _head; }
            set { _head = value; }
        }


        public string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }


        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }


        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }
        public string PostalCode
        {
            get { return _postalcode; }
            set { _postalcode = value; }
        }


        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }


        public string URL
        {
            get { return _url; }
            set { _url = value; }
        }


        public string About
        {
            get { return _about; }
            set { _about = value; }
        }

        public string Image
        {
            get { return _image; }
            set { _image = value; }
        }


        public Int32 MembershipCount
        {
            get { return _membershipcount; }
            set { _membershipcount = value; }
        }

        public DateTime CreatedDate
        {
            get { return _createddate; }
            set { _createddate = value; }
        }


        public DateTime UpdateDate
        {
            get { return _updatedate; }
            set { _updatedate = value; }
        }

        public List<AttributeValue> Attributes
        {
            get { return _groupattibutes; }
            set { _groupattibutes = value; }
        }

        public List<AffiliationGroupOption> Options
        {
            get { return _options; }
            set { _options = value; }
        }
        #region ICacheable Members

        private int _cacheTTLSeconds = 300;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEY = "AffiliationGroup_{0}";
        private string cachekey = "";
        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }



        public string GetCacheKey()
        {
            return String.Format(CACHEKEY, _affiliationgroupid);
        }

        public static string GetCacheKey(int _affiliationgroupid)
        {
            return String.Format(CACHEKEY, _affiliationgroupid);
        }



        #endregion
        #region collection methods
        public AffiliationGroupOption GetOption(int optionid)
        {
             AffiliationGroupOption option = null;
            try
            {
                option = (from o in _options
                        where o.AffiliationCategoryOptionID == optionid
                          select o).ToList<AffiliationGroupOption>()[0];

                return option;

            }
            catch (Exception ex)
            { return null; }

        }

        public AttributeValue GetAttributeValue(int id)
        {
            AttributeValue val = null;
            try
            {
                val = (from v in _groupattibutes
                          where v.AttributeCategorySiteID == id
                       select v).ToList<AttributeValue>()[0];

                return val;

            }
            catch (Exception ex)
            { return null; }

        }

        public void AddOption(int optionid, int optionvalueid )
        {
            try
            {
                bool newOpt=false;
                AffiliationGroupOption option = GetOption(optionid);
                if (option == null)
                {
                    option = new AffiliationGroupOption();
                    newOpt = true;
                }

                option.AffiliationGroupID = _affiliationgroupid;
                option.AffiliationCategoryOptionID = optionid;
                option.AffiliationCategoryOptionValueID = optionvalueid;

                if (_options == null)
                    _options = new List<AffiliationGroupOption>();
                if (newOpt)
                    _options.Add(option);
                
            }
            catch (Exception ex)
            { }

        }

        public void SetAttributeIntValue(int attrcatid, int value)
        {
            try
            { bool newAtr=false;
                if (_groupattibutes == null)
                    _groupattibutes = new List<AttributeValue>();

                AttributeValue v = GetAttributeValue(attrcatid);
                if (v == null)
                    { v = new AttributeValue(); newAtr = true; }
                v.AffiliationGroupID = _affiliationgroupid;
                v.AttributeCategorySiteID = attrcatid;
                v.ValueInt = value;
                if (newAtr)
                    _groupattibutes.Add(v);
            }
            catch (Exception ex)
            { }
        }

        public void SetAttributeTextValue(int attrcatid, string value)
        {
            try
            {
                bool newAtr = false;
                if (_groupattibutes == null)
                    _groupattibutes = new List<AttributeValue>();

                AttributeValue v = GetAttributeValue(attrcatid);
                if (v == null)
                { v = new AttributeValue(); newAtr = true; }
                v.AffiliationGroupID = _affiliationgroupid;
                v.AttributeCategorySiteID = attrcatid;
                v.ValueText = value;
                if (newAtr)
                    _groupattibutes.Add(v);
            }
            catch (Exception ex)
            { }
        }

        public void SetAttributeDateValue(int attrcatid, DateTime value)
        {
            try
            {
                bool newAtr = false;
                if (_groupattibutes == null)
                    _groupattibutes = new List<AttributeValue>();

                AttributeValue v = GetAttributeValue(attrcatid);
                if (v == null)
                { v = new AttributeValue(); newAtr = true; }
                v.AffiliationGroupID = _affiliationgroupid;
                v.AttributeCategorySiteID = attrcatid;
                v.ValueDate = value;
                if (newAtr)
                    _groupattibutes.Add(v);
            }
            catch (Exception ex)
            { }
        }
        #endregion
        #region IReplicable and ReferenceTracking Members
        [NonSerialized]
        private ReferenceTracker _referenceTracker;

        public ReferenceTracker ReferenceTracker
        {
            get
            {
                if (_referenceTracker == null)
                {
                    _referenceTracker = new ReferenceTracker();
                }

                return _referenceTracker;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            return new ReplicationPlaceholder(GetCacheKey());
        }

        #endregion

    }

    #endregion


    #region class AffiliationGroupsCollection
    [Serializable]
    public class AffiliationGroupsCollection:IValueObject,ICacheable,IReplicable
    {
        int _categorysiteid;
        List<AffiliationGroup> _groups=null;
        List<AffiliationGroup> _topgroups = null;
        List<AffiliationGroup> _alphagroups = null;

     
        public int CategorySiteID
        {
            get { return _categorysiteid; }
            set { _categorysiteid = value; }
        }

        public List<AffiliationGroup> Groups
        {
            get { return _groups; }
            set { _groups = value; }
        }
        public List<AffiliationGroup> TopGroups
        {
            get { return _topgroups; }
            set { _topgroups = value; }
        }
        public List<AffiliationGroup> AlphaGroups
        {
            get { return GetAlphaGroups(0); }
            set { _topgroups = value; }
        }

        #region collection methods

        public AffiliationGroup GetGroup(int groupid)
        {
            AffiliationGroup group = null;
            try
            {
                group = (from g in _groups
                         where g.AffiliationGroupID == groupid
                         select g).ToList<AffiliationGroup>()[0];
                return group;
            }
            catch (Exception ex)
            { return null; }
        }

        public List<AffiliationGroup> GetAlphaGroups(int limit)
        {
            List<AffiliationGroup> list = null;
            try
            {
                if (_alphagroups == null)
                {
                    _alphagroups = (from g in _groups where g.AffiliationGroupID > 0 orderby g.Name select g).ToList<AffiliationGroup>();
                }
                if (limit > _groups.Count || limit==0)
                { limit = _groups.Count; }
            
                // _groups.Sort(delegate(AffiliationGroup g1, AffiliationGroup g2) { return g2.CreatedDate.CompareTo(g1.CreatedDate); });
                if (limit > 0)
                {
                    list = _alphagroups.GetRange(0, limit);
                }
                else
                {
                    list = _alphagroups.ToList<AffiliationGroup>();
                }
                return list;
            }
            catch (Exception ex)
            { return null; }
        }
        public List<AffiliationGroup> GetNewGroups(int limit)
        {
            List<AffiliationGroup> list = null;
            try
            {
                if (limit > _groups.Count)
                { limit = _groups.Count; }
             // _groups.Sort(delegate(AffiliationGroup g1, AffiliationGroup g2) { return g2.CreatedDate.CompareTo(g1.CreatedDate); });
              if (limit > 0 )
              {
                  list = _groups.GetRange(0, limit);
              }
              else
              {
                  list=_groups.ToList<AffiliationGroup>();
              }
              return list;
            }
            catch (Exception ex)
            { return null; }
        }

        public List<AffiliationGroup> GetNewGroups(int start, int limit)
        {
            List<AffiliationGroup> list = null;
            try
            {
                if (start > _groups.Count - 1)
                    start = _groups.Count - 1;

                if (limit > _groups.Count - start)
                    limit = _groups.Count - start;
                
                _groups.Sort(delegate(AffiliationGroup g1, AffiliationGroup g2) { return g2.CreatedDate.CompareTo(g1.CreatedDate); });
                if (limit > 0)
                {
                    list = _groups.GetRange(start, limit);
                }
                else
                {
                    list = _groups.ToList<AffiliationGroup>();
                }
                return list;
            }
            catch (Exception ex)
            { return null; }
        }
        public List<AffiliationGroup> GetTopGroups(int limit)
        {
            List<AffiliationGroup> list = null;
            try
            {
                if (limit > _topgroups.Count)
                { limit = _topgroups.Count; }
               
                if (limit > 0)
                {
                    list = _topgroups.GetRange(0, limit);
                }
                else
                {
                    list = _topgroups.ToList<AffiliationGroup>();
                }
                return list;
            }
            catch (Exception ex)
            { return null; }
        }

        public List<AffiliationGroup> GetTopGroups(int start, int limit)
        {
            List<AffiliationGroup> list = null;
            try
            {
                if (start > _topgroups.Count - 1)
                    start = _topgroups.Count - 1;

                if (limit > _topgroups.Count - start)
                    limit = _topgroups.Count - start;

               // _groups.Sort(delegate(AffiliationGroup g1, AffiliationGroup g2) { return g2.MembershipCount.CompareTo(g1.MembershipCount); });
                if (limit > 0)
                {
                    list = _topgroups.GetRange(start, limit);
                }
                else
                {
                    list = _topgroups.ToList<AffiliationGroup>();
                }
                
                return list;
            }
            catch (Exception ex)
            { return null; }
        }


        public List<AffiliationGroup> GetGroups(int limit)
        {
            List<AffiliationGroup> list = null;
            try
            {
                if (limit > _groups.Count)
                { limit = _groups.Count; }
               
                if (limit > 0)
                {
                    list = _groups.GetRange(0, limit);
                }
                else
                {
                    list = _groups.ToList<AffiliationGroup>();
                }
                return list;
            }
            catch (Exception ex)
            { return null; }
        }

        public List<AffiliationGroup> GetGroups(int start, int limit)
        {
            List<AffiliationGroup> list = null;
            try
            {
                if (start > _groups.Count - 1)
                    start = _groups.Count - 1;

                if (limit > _groups.Count - start)
                    limit = _groups.Count - start;

             
                if (limit > 0)
                {
                    list = _groups.GetRange(start, limit);
                }
                else
                {
                    list = _groups.ToList<AffiliationGroup>();
                }

                return list;
            }
            catch (Exception ex)
            { return null; }
        }

        #endregion
        #region ICacheable Members

        private int _cacheTTLSeconds = 300;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEY = "AffiliationGroupsCollection_{0}";
        private string cachekey = "";
        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }



        public string GetCacheKey()
        {

            string key = CACHEKEY;
            //if (!String.IsNullOrEmpty(SortType))
            //{
            //    key = key + "_SORT_" + SortType.ToUpper();
            //}
            return String.Format(key, _categorysiteid);
        }
        public static string GetCacheKey(int categorysiteid)
        {
            string key = CACHEKEY;
            //if (!String.IsNullOrEmpty(sorttype))
            //{
            //    key = key + "_SORT_" + sorttype.ToUpper();
            //}
            return String.Format(key, categorysiteid);
        }

      


        #endregion

        #region IReplicable and ReferenceTracking Members
        [NonSerialized]
        private ReferenceTracker _referenceTracker;

        public ReferenceTracker ReferenceTracker
        {
            get
            {
                if (_referenceTracker == null)
                {
                    _referenceTracker = new ReferenceTracker();
                }

                return _referenceTracker;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            return new ReplicationPlaceholder(GetCacheKey());
        }

        #endregion
    }

    [Serializable]
    public class AffiliationCategoryGroupsList : IValueObject, ICacheable, IReplicable
    {
        int _categorysiteid;
        List<int> _groups = null;

        public int CategorySiteID
        {
            get { return _categorysiteid; }
            set { _categorysiteid = value; }
        }

        public List<int> Groups
        {
            get { return _groups; }
            set { _groups = value; }
        }


        #region collection methods

        public int IfContainsGroup(int groupid)
        {
            int group ;
            try
            {
                group = (from g in _groups
                         where g == groupid
                         select g).ToList<int>()[0];
                return group;
            }
            catch (Exception ex)
            { return 0; }
        }

     
        #endregion
        #region ICacheable Members

        private int _cacheTTLSeconds = 300;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEY = "AffiliationCategoryGroupsList_{0}";
        private string cachekey = "";
        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }



        public string GetCacheKey()
        {
            return String.Format(CACHEKEY, _categorysiteid);
        }

        public static string GetCacheKey(int categorysiteid)
        {
            return String.Format(CACHEKEY, categorysiteid);
        }



        #endregion

        #region IReplicable and ReferenceTracking Members
        [NonSerialized]
        private ReferenceTracker _referenceTracker;

        public ReferenceTracker ReferenceTracker
        {
            get
            {
                if (_referenceTracker == null)
                {
                    _referenceTracker = new ReferenceTracker();
                }

                return _referenceTracker;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            return new ReplicationPlaceholder(GetCacheKey());
        }

        #endregion
    }


    #endregion
    
}
