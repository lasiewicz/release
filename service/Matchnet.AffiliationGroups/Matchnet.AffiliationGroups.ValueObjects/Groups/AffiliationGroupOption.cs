﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.AffiliationGroups.ValueObjects
{
    #region class AffiliationGroupOption
    [Serializable]
    public class AffiliationGroupOption
    {
        int _affiliationgroupoptionid;
        int _affiliationgroupid;
        int _affiliationcategoryoptionid;
        int _affiliationcategoryoptionvalueid;
        DateTime _createddate;
        DateTime _updatedate;

        public int AffiliationGroupOptionID
        {
            get { return _affiliationgroupoptionid; }
            set { _affiliationgroupoptionid = value; }
        }


        public int AffiliationGroupID
        {
            get { return _affiliationgroupid; }
            set { _affiliationgroupid = value; }
        }

        public int AffiliationCategoryOptionID
        {
            get { return _affiliationcategoryoptionid; }
            set { _affiliationcategoryoptionid = value; }
        }

        public int AffiliationCategoryOptionValueID
        {
            get { return _affiliationcategoryoptionvalueid; }
            set { _affiliationcategoryoptionvalueid = value; }
        }


        public DateTime CreatedDate
        {
            get { return _createddate; }
            set { _createddate = value; }
        }


        public DateTime UpdateDate
        {
            get { return _updatedate; }
            set { _updatedate = value; }
        }


    }
    #endregion

}
