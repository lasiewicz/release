﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using Matchnet.CacheSynchronization.Tracking;
namespace Matchnet.AffiliationGroups.ValueObjects
{
    #region class AffiliationMember
    [Serializable]
    public class AffiliationMember : IValueObject, ICacheable
    {
        int _affiliationmemberid;
        int _memberid;
        int _siteid;
        DateTime _birthdate;
        int _gendermask;
        int _boxx;
        int _boxy;
        int _regionid;
        int _depth1regionid;
        int _depth2regionid;
        int _depth3regionid;
        int _depth4regionid;
        Double _longitude ;
        Double _latitude ;
        bool _hasphotoflag;
        DateTime _createddate;
        DateTime _updatedate;

        public int AffiliationMemberID
        {
            get { return _affiliationmemberid; }
            set { _affiliationmemberid = value; }
        }

        public int MemberID
        {
            get { return _memberid; }
            set { _memberid = value; }
        }

        public int SiteID
        {
            get { return _siteid; }
            set { _siteid = value; }
        }


        public DateTime BirthDate
        {
            get { return _birthdate; }
            set { _birthdate = value; }
        }


        public int GenderMask
        {
            get { return _gendermask; }
            set { _gendermask = value; }
        }


        public int BoxX
        {
            get { return _boxx; }
            set { _boxx = value; }
        }


        public int BoxY
        {
            get { return _boxy; }
            set { _boxy = value; }
        }


        public int RegionID
        {
            get { return _regionid; }
            set { _regionid = value; }
        }


        public int Depth1RegionID
        {
            get { return _depth1regionid; }
            set { _depth1regionid = value; }
        }


        public int Depth2RegionID
        {
            get { return _depth2regionid; }
            set { _depth2regionid = value; }
        }


        public int Depth3RegionID
        {
            get { return _depth3regionid; }
            set { _depth3regionid = value; }
        }


        public int Depth4RegionID
        {
            get { return _depth4regionid; }
            set { _depth4regionid = value; }
        }


        public Double Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }


        public Double Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }


        public bool HasPhotoFlag
        {
            get { return _hasphotoflag; }
            set { _hasphotoflag = value; }
        }


        public DateTime CreatedDate
        {
            get { return _createddate; }
            set { _createddate = value; }
        }


        public DateTime UpdateDate
        {
            get { return _updatedate; }
            set { _updatedate = value; }
        }

        #region ICacheable Members

        private int _cacheTTLSeconds = 300;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEY = "AffiliationMember_{0}_{1}";
        private string cachekey = "";
        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }



        public string GetCacheKey()
        {
            return String.Format(CACHEKEY, _siteid,_memberid);
        }

        public static string GetCacheKey(int siteid,int memberid)
        {
            return String.Format(CACHEKEY,siteid, memberid);
        }



        #endregion

    }
    #endregion


    #region class AffiliationGroupMember
    [Serializable]
    public class AffiliationGroupMember
    {
        int _affiliationgroupid;
        int _affiliationgroupmemberid;
        int _memberid;

        DateTime _createddate;
        DateTime _updatedate;

        public int AffiliationGroupMemberID
        {
            get { return _affiliationgroupmemberid; }
            set { _affiliationgroupmemberid = value; }
        }


        public int AffiliationGroupID
        {
            get { return _affiliationgroupid; }
            set { _affiliationgroupid = value; }
        }


        public int MemberID
        {
            get { return _memberid; }
            set { _memberid = value; }
        }


       
        public DateTime CreatedDate
        {
            get { return _createddate; }
            set { _createddate = value; }
        }


        public DateTime UpdateDate
        {
            get { return _updatedate; }
            set { _updatedate = value; }
        }


    }
    #endregion

    [Serializable]
    public class AffiliationGroupMemberList : IValueObject, ICacheable
    {
        int _affiliationgroupid;

        List<int> _members = null;
        public int AffiliationGroupID
        {
            get { return _affiliationgroupid; }
            set { _affiliationgroupid = value; }
        }

        public List<int> Members
        {
            get { return _members; }
            set { _members = value; }
        }


        #region collection methods
        public bool IfMemberExistsInGroup(int memberid)
        {
            bool exists = false;
            try
            {
               int member = (from m in _members
                            where m == memberid
                            select m).ToList<int>()[0];

                exists = member > 0;
                return exists;
            }
            catch (Exception ex) { throw ex; }
        }

      
        #endregion
        #region ICacheable Members

        private int _cacheTTLSeconds = 300;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEY = "AffiliationGroupMemberList_{0}";
        private string cachekey = "";
        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }



        public string GetCacheKey()
        {
            return String.Format(CACHEKEY, _affiliationgroupid);
        }

        public static string GetCacheKey(int affiliationgroupid)
        {
            return String.Format(CACHEKEY, affiliationgroupid);
        }



        #endregion
    }
    [Serializable]
    public class AffiliationMemberGroupsList : IValueObject, ICacheable,IReplicable
    {
        int _memberid;
        int _siteid;
      
        List<AffiliationCategoryGroupsList> _groups = null;
        public int MemberID
        {
            get { return _memberid; }
            set { _memberid = value; }
        }
        public int SiteID
        {
            get { return _siteid; }
            set { _siteid = value; }
        }
      
        public List<AffiliationCategoryGroupsList> Groups
        {
            get { return _groups; }
            set { _groups = value; }
        }

        #region collection methods
        public bool IfCategoryMember(int categorysiteid)
        {
            bool exists = false;
            try
            {

                AffiliationCategoryGroupsList categorygroups = (from g in _groups
                                                                where g.CategorySiteID == categorysiteid
                                                                select g).ToList<AffiliationCategoryGroupsList>()[0];


                return exists = categorygroups !=null;
            }
            catch (Exception ex) { return false; }
        }
        public bool IfGroupMember(int categorysiteid,int groupid)
        {
            bool exists = false;
            try
            {

                AffiliationCategoryGroupsList categorygroups = (from g in _groups
                              where g.CategorySiteID == categorysiteid
                              select g).ToList<AffiliationCategoryGroupsList>()[0];

                int group=(from g in categorygroups.Groups
                            where g==groupid
                               select g).ToList<int>()[0];
                return exists = group > 0;
            }
            catch (Exception ex) { return false; }
        }

        public AffiliationCategoryGroupsList GetCategoryGroups(int categorysiteid)
        {
            try
            {
                AffiliationCategoryGroupsList categorygroups = (from g in _groups
                                                                where g.CategorySiteID == categorysiteid
                                                                select g).ToList<AffiliationCategoryGroupsList>()[0];
                return categorygroups;
            }
            catch (Exception ex)
            { return null; }
        }
        public void Add(int categorysiteid, int groupid)
        {
            try
            {
                if (_groups == null)
                    _groups = new List<AffiliationCategoryGroupsList>();
                if(!IfCategoryMember(categorysiteid))
                {
                    AffiliationCategoryGroupsList list=new AffiliationCategoryGroupsList();
                    list.CategorySiteID=categorysiteid;
                    list.Groups=new List<int>();
                    _groups.Add(list);
                }

                if(!IfGroupMember(categorysiteid,groupid))
                {
                    AffiliationCategoryGroupsList l=GetCategoryGroups(categorysiteid);
                    
                    l.Groups.Add(groupid);
                    

                }

            }
            catch (Exception ex) { }
        }

        public void Remove(int categorysiteid, int groupid)
        {
            try
            {
                if (_groups == null)
                    return;
               

                if (IfGroupMember(categorysiteid, groupid))
                {
                    AffiliationCategoryGroupsList l = GetCategoryGroups(categorysiteid);

                    l.Groups.Remove(groupid);


                }

            }
            catch (Exception ex) { }
        }
        
        #endregion
        #region ICacheable Members

        private int _cacheTTLSeconds = 300;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEY = "AffiliationMemberGroupsList_{0}_{1}";
        private string cachekey = "";
   
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }



        public string GetCacheKey()
        {
            return String.Format(CACHEKEY,_siteid,  _memberid);
        }

        public static string GetCacheKey(int siteid, int memberid)
        {
            return String.Format(CACHEKEY, siteid, memberid);
        }



        #endregion

        #region IReplicable and ReferenceTracking Members
        [NonSerialized]
        private ReferenceTracker _referenceTracker;

        public ReferenceTracker ReferenceTracker
        {
            get
            {
                if (_referenceTracker == null)
                {
                    _referenceTracker = new ReferenceTracker();
                }

                return _referenceTracker;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            return new ReplicationPlaceholder(GetCacheKey());
        }

        #endregion
    }


 
}
