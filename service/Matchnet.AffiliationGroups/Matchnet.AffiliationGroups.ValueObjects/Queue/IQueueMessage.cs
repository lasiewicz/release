﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.AffiliationGroups.ValueObjects
{
    public enum QueueAction : int
    {
        addgroupmember,
        deletegroupmember,
        updatemember
    }

    public interface IQueueMessage
    {
        int Attempts { get; set; }
        DateTime LastAttemptDateTime { get; set; }
        List<string> Errors { get; set; }
        QueueAction Action { get; set; }

    }

    public class MemberMessage : IQueueMessage
    {
        String errlogformat = "Attempts:{0},LastAttempt:{1}, Errs#:{3}, Last Error:{4}";
        string tostringformat = "Memberid:[0},AffiliationGroupID:{1},BrandID:{2}, Stats:{3}";
        public int MemberID
        {
            get;set; 
        }

        public int AffiliationGroupID
        {
            get;set; 
        }

        public int BrandID
        {
            get;set; 
        }
        public int CategorySiteID
        {
            get;
            set;
        }
        
        #region IQueueMember

        public QueueAction Action
        {
            get;
            set;
        }
        public int Attempts
        {
            get;
            set;
        }

        public DateTime LastAttemptDateTime
        {
            get;
            set;
        }

        public List<string> Errors
        {
            get;
            set;
        }

        public override string ToString()
        {
            string lasterr = Errors[Errors.Count - 1];
            string stats=String.Format(errlogformat, Attempts, LastAttemptDateTime, Errors.Count, lasterr);
            string msg = String.Format(tostringformat, MemberID, AffiliationGroupID, BrandID,stats);
            return msg;
        }
        #endregion
    }

}
