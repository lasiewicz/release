﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.AffiliationGroups.ValueObjects
{
    #region class AffiliationCategorySite
    [Serializable]
    public class AffiliationCategorySite : IValueObject, ICacheable
    {
        int _categorysiteid;
        int _categoryid;
        int _siteid;
        string _name = null;
        string _description = null;
        string _imagepath = null;
        string _defaultLogo = null;
        DateTime _createddate;
        DateTime _updatedate;

        AffiliationAttributeCategoryCollection _attributes=new AffiliationAttributeCategoryCollection();
        AffiliationCategoryOptionCollection _options;

        public int CategorySiteID
        {
            get { return _categorysiteid; }
            set { _categorysiteid = value; }
        }


        public int CategoryID
        {
            get { return _categoryid; }
            set { _categoryid = value; }
        }


        public int SiteID
        {
            get { return _siteid; }
            set { _siteid = value; }
        }


        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }


        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public string ImagePath
        {
            get { return _imagepath; }
            set { _imagepath = value; }
        }
        public string DefaultLogo
        {
            get { return _defaultLogo; }
            set { _defaultLogo = value; }
        }

        public AffiliationAttributeCategoryCollection Attributes
        {
            get { return _attributes; }
            set { _attributes = value; }
        }

        public AffiliationCategoryOptionCollection Options
        {
            get { return _options; }
            set { _options = value; }
        }

        public DateTime CreatedDate
        {
            get { return _createddate; }
            set { _createddate = value; }
        }


        public DateTime UpdateDate
        {
            get { return _updatedate; }
            set { _updatedate = value; }
        }


        #region ICacheable Members

        private int _cacheTTLSeconds = 300;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEY = "AffiliationCategorySite_{0}";
        private string cachekey = "";
        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }



        public string GetCacheKey()
        {
            return String.Format(CACHEKEY, _categorysiteid);
        }


        public static string GetCacheKey(int siteid)
        {
            return String.Format(CACHEKEY, siteid);
        }

        #endregion

    }
    #endregion

    #region class AffiliationCategorySiteCollection
    [Serializable]
    public class AffiliationCategorySiteCollection : IValueObject, ICacheable
    {
        List<AffiliationCategorySite> categorySites;
        int _siteID;
        public List<AffiliationCategorySite> CategorySites
        {
            get { return categorySites; }
            set { categorySites = value; }
        }

        public Int32 SiteID
        {
            get { return _siteID; }
            set { _siteID = value; }
        }

        #region collection methods

        public AffiliationCategorySite GetCategorySiteByName(string name)
        {
            AffiliationCategorySite category = null;
            try
            {
                category = (from c in categorySites
                        where c.Name == name
                        select c).ToList<AffiliationCategorySite>()[0];

                return category;

            }
            catch (Exception ex)
            { return null; }


        }
        #endregion

        #region ICacheable Members

        private int _cacheTTLSeconds = 300;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEY = "AffiliationCategorySiteCollection_{0}";
        private string cachekey = "";
        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }



        public string GetCacheKey()
        {
            return String.Format(CACHEKEY,_siteID);
        }


        public static string GetCacheKey(int siteid)
        {
            return String.Format(CACHEKEY, siteid);
        }

        #endregion


    }

    #endregion
}
