﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.AffiliationGroups.ValueObjects
{
    #region class AffiliationCategoryOption
    [Serializable]
    public class AffiliationCategoryOption
    {
        int _affiliationcategoryoptionid;
        int _categorysiteid;
        string _name = null;
        string _description = null;
        bool _activeflag;
        DateTime _createddate;
        DateTime _updatedate;
        public int AffiliationCategoryOptionID
        {
            get { return _affiliationcategoryoptionid; }
            set { _affiliationcategoryoptionid = value; }
        }
        List<AffiliationCategoryOptionValue> _optionValues;

        public int CategorySiteID
        {
            get { return _categorysiteid; }
            set { _categorysiteid = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }


        public bool ActiveFlag
        {
            get { return _activeflag; }
            set { _activeflag = value; }
        }


        public List<AffiliationCategoryOptionValue> OptionValues
        {
            get { return _optionValues; }
            set { _optionValues = value; }
        }


        public DateTime CreatedDate
        {
            get { return _createddate; }
            set { _createddate = value; }
        }


        public DateTime UpdateDate
        {
            get { return _updatedate; }
            set { _updatedate = value; }
        }
        public AffiliationCategoryOptionValue GetOptionValue(int optionvalueid)
        {
            AffiliationCategoryOptionValue optionvalue = null;
            try
            {
                optionvalue = (from o in _optionValues
                          where  o.AffiliationCategoryOptionValueID==optionvalueid
                          select o).ToList<AffiliationCategoryOptionValue>()[0];

                return optionvalue;

            }
            catch (Exception ex)
            { return null; }


        }

    }
    #endregion
    
    #region class AffiliationCategoryOptionValue
    [Serializable]
    public class AffiliationCategoryOptionValue
    {
        int _affiliationcategoryoptionvalueid;
        int _affiliationcategoryoptionid;
        string _description = null;
        int _value;
        int _listorder;
        DateTime _createddatetime;
        DateTime _updatedatetime;
        public int AffiliationCategoryOptionValueID
        {
            get { return _affiliationcategoryoptionvalueid; }
            set { _affiliationcategoryoptionvalueid = value; }
        }


        public int AffiliationCategoryOptionID
        {
            get { return _affiliationcategoryoptionid; }
            set { _affiliationcategoryoptionid = value; }
        }


        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }


        public int Value
        {
            get { return _value; }
            set { _value = value; }
        }


        public int ListOrder
        {
            get { return _listorder; }
            set { _listorder = value; }
        }


        public DateTime CreatedDateTime
        {
            get { return _createddatetime; }
            set { _createddatetime = value; }
        }


        public DateTime UpdateDateTime
        {
            get { return _updatedatetime; }
            set { _updatedatetime = value; }
        }


    }
    #endregion

    #region class AffiliationCategoryOptionCollection
    [Serializable]
    public class AffiliationCategoryOptionCollection : IValueObject, ICacheable
    {
        List<AffiliationCategoryOption> _options;
        int _categorySiteID;

        public List<AffiliationCategoryOption> Options
        {
            get { return _options; }
            set { _options = value; }
        }

        public int CategorySiteID
        {
            get { return _categorySiteID; }
            set { _categorySiteID = value; }
        }
        #region collection methods

        public AffiliationCategoryOption GetOption(string name)
        {
            AffiliationCategoryOption option = null;
            try
            {
                option = (from o in _options
                        where o.Name.ToLower() == name.ToLower()
                          select o).ToList<AffiliationCategoryOption>()[0];

                return option;

            }
            catch (Exception ex)
            { return null; }

            
        }
        public AffiliationCategoryOption GetOption(int id)
        {
            AffiliationCategoryOption option = null;
            try
            {
                option = (from o in _options
                          where o.AffiliationCategoryOptionID ==  id
                          select o).ToList<AffiliationCategoryOption>()[0];

                return option;

            }
            catch (Exception ex)
            { return null; }


        }

        #endregion

        #region ICacheable Members

        private int _cacheTTLSeconds = 300;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEY = "AffiliationCategoryOptionCollection_{0}";
        private string cachekey = "";
        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }



        public string GetCacheKey()
        {
            return String.Format(CACHEKEY,_categorySiteID);
        }

        public static string GetCacheKey(int categorySiteID)
        {
            return String.Format(CACHEKEY, categorySiteID);
        }



        #endregion


    }

    #endregion


}
