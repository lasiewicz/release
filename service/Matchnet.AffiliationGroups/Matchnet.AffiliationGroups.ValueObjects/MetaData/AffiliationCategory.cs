﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.AffiliationGroups.ValueObjects
{
    #region class AffiliationCategory
    [Serializable]
    public class AffiliationCategory
    {
        int _categoryid;
        string _description = null;
      
        DateTime _createddate;
        DateTime _updatedate;
        bool _activeflag;

        AffiliationCategoryOptionCollection _options;
        AffiliationAttributeCollection _attributes;
        public int CategoryID
        {
            get { return _categoryid; }
            set { _categoryid = value; }
        }


        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        

        public DateTime CreatedDate
        {
            get { return _createddate; }
            set { _createddate = value; }
        }


        public DateTime UpdateDate
        {
            get { return _updatedate; }
            set { _updatedate = value; }
        }


        public bool ActiveFlag
        {
            get { return _activeflag; }
            set { _activeflag = value; }
        }


    }
    #endregion

    
    #region class AffiliationCategoryCollection
    [Serializable]
    public class AffiliationCategoryCollection : IValueObject, ICacheable
    {
        List<AffiliationCategory> _categories;
        int _siteID;

        public List<AffiliationCategory> Categories
        {
            get { return _categories; }
            set { _categories = value; }
        }

        public Int32 SiteID
        {
            get { return _siteID; }
            set { _siteID = value; }
        }


        #region collection methods

        public AffiliationCategory GetAffiliationCategory(string description, bool active)
        {
            AffiliationCategory cat = null;
            try
            {
                cat = (from c in _categories
                       where c.Description.ToLower() == description.ToLower() && c.ActiveFlag == active
                       select c).ToList<AffiliationCategory>()[0];

                return cat;

            }
            catch (Exception ex)
            { return null; }


        }


        public AffiliationCategory GetAffiliationCategory(int id, bool active)
        {
            AffiliationCategory cat = null;
            try
            {
                cat = (from c in _categories
                       where c.CategoryID == id && c.ActiveFlag == active
                       select c).ToList<AffiliationCategory>()[0];

                return cat;

            }
            catch (Exception ex)
            { return null; }


        }

       #endregion

        #region ICacheable Members

        private int _cacheTTLSeconds = 300;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEYFORMAT = "AffiliationCategoryCollection_{0}";
        private string cachekey = "";
        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }



        public string GetCacheKey()
        {
            return String.Format(CACHEKEYFORMAT, _siteID);
        }



        #endregion


    }

    #endregion
}
