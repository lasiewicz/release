﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Matchnet;
using Matchnet.Caching;
using Matchnet.CacheSynchronization.Tracking;

namespace Matchnet.AffiliationGroups.ValueObjects
{
    public enum AffiliationAttributeType:int
    {
        INT=1,
        TXT=2,
        DATE=3

    }

    #region class AffiliationAttribute
    [Serializable]
    public class AffiliationAttribute:IValueObject
    {
        int _attributeid;
        int _attributetype;
        string _attributename = null;
        DateTime _createddate;
        DateTime _updatedate;
        public int AttributeID
        {
            get { return _attributeid; }
            set { _attributeid = value; }
        }


        public int AttributeType
        {
            get { return _attributetype; }
            set { _attributetype = value; }
        }


        public string AttributeName
        {
            get { return _attributename; }
            set { _attributename = value; }
        }


        public DateTime CreatedDate
        {
            get { return _createddate; }
            set { _createddate = value; }
        }


        public DateTime UpdateDate
        {
            get { return _updatedate; }
            set { _updatedate = value; }
        }


    }

    #endregion

    #region class AffiliationAttributeCategorySite
    [Serializable]
    public class AffiliationAttributeCategorySite:IValueObject
    {
        int _attributecategorysiteid;
        int _attributeid;
        int _categorysiteid;
        string _description = null;
        DateTime _createddate;
        DateTime _updatedate;
        public int AttributeCategorySiteID
        {
            get { return _attributecategorysiteid; }
            set { _attributecategorysiteid = value; }
        }


        public int AttributeID
        {
            get { return _attributeid; }
            set { _attributeid = value; }
        }

        public int CategorySiteID
        {
            get { return _categorysiteid; }
            set { _categorysiteid = value; }
        }


        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }


        public DateTime CreatedDate
        {
            get { return _createddate; }
            set { _createddate = value; }
        }


        public DateTime UpdateDate
        {
            get { return _updatedate; }
            set { _updatedate = value; }
        }


    }
    #endregion

    #region class AttributeValue
    [Serializable]
    public class AttributeValue
    {
        int _affiliationgroupid;
        int _attributecategorysiteid;
        string _valuetext = null;
        int _valueint=Constants.NULL_INT;
        DateTime _valuedate=DateTime.MinValue;
        DateTime _createdddate;
        DateTime _updatedate;
        public int AffiliationGroupID
        {
            get { return _affiliationgroupid; }
            set { _affiliationgroupid = value; }
        }


        public int AttributeCategorySiteID
        {
            get { return _attributecategorysiteid; }
            set { _attributecategorysiteid = value; }
        }


        public string ValueText
        {
            get { return _valuetext; }
            set { _valuetext = value; }
        }


        public int ValueInt
        {
            get { return _valueint; }
            set { _valueint = value; }
        }

        public DateTime ValueDate
        {
            get { return _valuedate; }
            set { _valuedate = value; }
        }

        public DateTime CreatedDDate
        {
            get { return _createdddate; }
            set { _createdddate = value; }
        }


        public DateTime UpdateDate
        {
            get { return _updatedate; }
            set { _updatedate = value; }
        }

        public override string ToString()
        {
            if (_valueint > Constants.NULL_INT)
                return _valueint.ToString();
            else if (_valuedate > DateTime.MinValue)
                return _valuedate.ToLongTimeString();
            else if (_valuetext != null)
                return _valuetext;

            return "";


        }
    }
    #endregion


    #region class AffiliationAttributeCollection
    [Serializable]
    public class AffiliationAttributeCollection : IValueObject, ICacheable
    {
        List<AffiliationAttribute> _attributes;

        public List<AffiliationAttribute> Attributes
        {
            get { return _attributes; }
            set { _attributes = value; }
        }


        #region collection methods

        public AffiliationAttribute GetAttribute(string name)
        {
            AffiliationAttribute attr =null;
            try
            {
                attr = (from a in _attributes
                        where a.AttributeName.ToLower() == name.ToLower()
                        select a).ToList<AffiliationAttribute>()[0];

                return attr;

            }
            catch (Exception ex)
            { return null; }


        }


        public AffiliationAttribute GetAttribute(int  id)
        {
            AffiliationAttribute attr = null;
            try
            {
                attr = (from a in _attributes
                        where a.AttributeID == id
                        select a).ToList<AffiliationAttribute>()[0];

                return attr;

            }
            catch (Exception ex)
            { return null; }


        }


        public List<AffiliationAttribute> GetAttributesByType(AffiliationAttributeType attrtype)
        {
            List<AffiliationAttribute> list = null;
            try
            {
                list = (from a in _attributes
                        where a.AttributeType == (int)attrtype
                        select a).ToList<AffiliationAttribute>();

                return list;

            }
            catch (Exception ex)
            { return null; }


        }

       
        #endregion

        #region ICacheable Members

        private int _cacheTTLSeconds = 300;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        public const string CACHEKEY = "AffiliationAttributeCollection";
        private string cachekey = "";
        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }



        public string GetCacheKey()
        {
            return CACHEKEY;
        }

       




        #endregion


    }

    #endregion


    #region class AffiliationAttributeCategorySiteCollection
    [Serializable]
    public class AffiliationAttributeCategoryCollection : IValueObject, ICacheable
    {
    
       List<AffiliationAttributeCategorySite> _attributes;
        int _categorysiteid;
        public  List<AffiliationAttributeCategorySite> CategorySiteAttributes
        {
            get { return _attributes; }
            set { _attributes = value; }
        }

        public int CategorySiteID
        {
            get { return _categorysiteid; }
            set { _categorysiteid = value; }
        }
        
        #region collection methods


        #endregion

        #region ICacheable Members

        private int _cacheTTLSeconds = 300;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEY = "AffiliationAttributeCategoryCollection";
        private string cachekey = "";
        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }



        public string GetCacheKey()
        {
            return String.Format( CACHEKEY,_categorysiteid);
        }
         public static string GetCacheKey(int categorysiteid)
        {
            return String.Format( CACHEKEY,categorysiteid);
        }



        #endregion


    }

    #endregion

}
