﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Caching;
using System.Collections;
using Matchnet.Data;
using Matchnet.Data.Configuration;
using Matchnet.Data.Hydra;
using Matchnet.Data.Exceptions;
using Matchnet.Configuration;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.AttributeMetadata;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.Member.ServiceAdapters;

using Matchnet.AffiliationGroups.ValueObjects;
using Matchnet.Geo;


using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.CacheSynchronization.Tracking;
namespace Matchnet.AffiliationGroups.BusinessLogic
{
    public class AffiliationGroupMemberBL
    {

        const string DATA_FORMAT = "memberid:{0}, siteid:{1}";
        const string MODULE_NAME = "AffiliationGroupMemberBL";
        private Matchnet.Caching.Cache _cache;

        #region singleton implementation
        public readonly static AffiliationGroupMemberBL Instance = new AffiliationGroupMemberBL();

        private AffiliationGroupMemberBL()
        {
            _cache = Matchnet.Caching.Cache.Instance;
            expireMemberGroups = new CacheItemRemovedCallback(this.ExpireMemberGroupsCallback);
        }

        #endregion

        #region synchronization/replication
        public delegate void ReplicationEventHandler(IReplicable replicableObject);
        public event ReplicationEventHandler ReplicationRequested;

        public delegate void SynchronizationEventHandler(string key, Hashtable cacheReferences);
        public event SynchronizationEventHandler SynchronizationRequested;


        private CacheItemRemovedCallback expireMemberGroups;

        private void ExpireMemberGroupsCallback(string key, object value, CacheItemRemovedReason callbackreason)
        {
            //MemberRemoved();

            AffiliationMemberGroupsList groups = value as AffiliationMemberGroupsList;
            if (groups != null)
            {
                ReplicationRequested(groups);
                SynchronizationRequested(groups.GetCacheKey(),
                    groups.ReferenceTracker.Purge(Constants.NULL_STRING));
                // IncrementExpirationCounter();
            }
          
        }


        public void InsertCacheMemberGroups(AffiliationMemberGroupsList groups, bool replicate, bool synchronize,string clientHostName)
        {
            string functionName = "InsertCacheMemberGroups";
            string trace = "";
            try
            {
                if (groups == null)
                    return;

                trace = String.Format(DATA_FORMAT, groups.MemberID, groups.SiteID);
                _cache.Insert(groups);
                if (replicate)
                {
                    ReplicationRequested(groups);
                }
                if (synchronize)
                {
                    SynchronizationRequested(groups.GetCacheKey(),
                    groups.ReferenceTracker.Purge(clientHostName));
                }
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, groups, null, trace, true);
            }
        }
        #endregion

        #region perfomance counters events
        public delegate void AffiliationGroupMembersRequestEventHandler(bool cacheHit, int count);
        public event AffiliationGroupMembersRequestEventHandler AffiliationGroupMembersRequested;

        public delegate void AffiliationGroupAddMemberRequestEventHandler();
        public event AffiliationGroupAddMemberRequestEventHandler AddGroupMemberRequested;



        #endregion

        
        #region public methods
        public List<int> GetGroupMembers(int affiliationgroupid)
        {
            AffiliationGroupMemberList members = null;

            DataSet ds = null;
            try
            {
                members = (AffiliationGroupMemberList)_cache.Get(AffiliationGroupMemberList.GetCacheKey(affiliationgroupid));
                if (members != null)
                {
                    int count = 0;
                    if (members.Members != null)
                        count = members.Members.Count;

                    AffiliationGroupMembersRequested(true, count);
                    return members.Members;


                }
                Command comm = new Command();
                comm.LogicalDatabaseName = "mnPremiumServices";
                comm.StoredProcedureName = "up_AffiliationGroupMembers_List";
                comm.AddParameter("@affiliationgroupid", SqlDbType.Int, ParameterDirection.Input, affiliationgroupid);

                ds = Client.Instance.ExecuteDataSet(comm);
                //with photos
                DataTable dt1 = ds.Tables[0];
                //no photos
                DataTable dt2 = ds.Tables[1];

                members = new AffiliationGroupMemberList();
                members.Members = new List<int>();

                members.AffiliationGroupID = affiliationgroupid;
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    DataRow rs = dt1.Rows[i];
                    AffiliationGroupMember member = createAffiliationGroupMember(rs);
                    members.Members.Add(member.MemberID);
                }
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    DataRow rs = dt2.Rows[i];
                    AffiliationGroupMember member = createAffiliationGroupMember(rs);
                    members.Members.Add(member.MemberID);
                }

                _cache.Insert(members);
                AffiliationGroupMembersRequested(false, members.Members.Count);
                return members.Members;

            }
            catch (Exception ex)
            {
                throw new BLException(ex);

            }




        }
        public AffiliationMemberGroupsList GetMemberGroups(int siteid, int memberid, string clientHostName, CacheReference reference)
        {
            AffiliationMemberGroupsList groups = null;

            SqlDataReader rs = null;
            try
            {
                groups = (AffiliationMemberGroupsList)_cache.Get(AffiliationMemberGroupsList.GetCacheKey(siteid, memberid));
                if (groups != null)
                    return groups;

                Command comm = new Command();
                comm.LogicalDatabaseName = "mnPremiumServices";
                comm.StoredProcedureName = "up_AffiliationMemberGroups_List";
                comm.AddParameter("@siteid", SqlDbType.Int, ParameterDirection.Input, siteid);
                comm.AddParameter("@memberid", SqlDbType.Int, ParameterDirection.Input, memberid);

                rs = Client.Instance.ExecuteReader(comm);
                groups = new AffiliationMemberGroupsList();
                groups.MemberID = memberid;
                groups.SiteID = siteid;
                groups.Groups = new List<AffiliationCategoryGroupsList>();
             
                while (rs.Read())
                {   
                    int groupid = Conversion.CInt(rs["affiliationgroupid"]);
                    int categorysiteid= Conversion.CInt(rs["categorysiteid"]);
                   
                    groups.Add(categorysiteid, groupid);

                }

               
                if (clientHostName != null && reference != null)
                {
                    groups.ReferenceTracker.Add(clientHostName, reference);
                }
                _cache.Insert(groups);
                return groups;
            }
            catch (Exception ex)
            {
                throw new BLException(ex);

            }
            finally
            {
                if (rs != null)
                    rs.Dispose();
            }



        }
        public void AddGroupMember(int brandid, int categorysiteid, int groupid, int memberid, string clienthostName, CacheReference reference)
        {
            try
            {
                Matchnet.Content.ValueObjects.BrandConfig.Brand brand=Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandid);
                AffiliationGroupMember member = new AffiliationGroupMember();
                member.MemberID = memberid;
                member.AffiliationGroupID = groupid;
                AffiliationMemberGroupsList groups = GetMemberGroups(brand.Site.SiteID, memberid, clienthostName, reference);

                AffiliationCategoryGroupsList list= groups.GetCategoryGroups(categorysiteid);
                if (list != null)
                {
                    list.Groups.Clear();
                }

                groups.Add(categorysiteid, groupid);
                saveGroupMember(brandid, member);
                SaveAffiliationMember(brandid, memberid);
                if (clienthostName != null && reference != null)
                {
                    groups.ReferenceTracker.Add(clienthostName, reference);
                }
                InsertCacheMemberGroups(groups, true, true, clienthostName);

                Matchnet.Member.ServiceAdapters.Member member1 = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
                int hasgroups = member1.GetAttributeInt(brand, "HasAffiliationGroup");
                if (hasgroups != 1)
                {
                    member1.SetAttributeInt(brand, "HasAffiliationGroup", 1);
                    Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(member1);
                }
                _cache.Remove(AffiliationGroupMemberList.GetCacheKey(groupid));
                AddGroupMemberRequested();
              
              
            }
            catch (Exception ex)
            {
                throw new BLException(ex);

            }

        }
        public void SaveAffiliationMember(int brandid, int memberid)
        {
            try
            {
                Brand brand = BrandConfigSA.Instance.GetBrands().GetBrand(brandid);
                AffiliationMember member = getAffiliationMember(brand.Site.SiteID, memberid);
                if (member == null)
                {
                    member = new AffiliationMember();
                    member.AffiliationMemberID = KeySA.Instance.GetKey("AffiliationMemberID");
                    member.SiteID = brand.Site.SiteID;
                    member.MemberID = memberid;
                    getMemberUpdate(member, brandid);

                    saveMember(member);
                }

                _cache.Insert(member);

            }
            catch (Exception ex)
            {
                throw new BLException(ex);

            }

        }
        public void DeleteGroupMember(int brandid, int groupid, int categorysiteid, int memberid, string clientHostName, CacheReference reference)
        {
            try
            {

                Command comm = new Command();
                comm.LogicalDatabaseName = "mnPremiumServices";
                comm.StoredProcedureName = "up_AffiliationGroupMember_Delete";


                comm.AddParameter("@affiliationgroupid", SqlDbType.Int, ParameterDirection.Input, groupid);
                comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberid);

                Client.Instance.ExecuteAsyncWrite(comm);

                Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandid);
                AffiliationMemberGroupsList groups = GetMemberGroups(brand.Site.SiteID,  memberid, clientHostName, null);
                groups.Remove(categorysiteid, groupid);
                if (clientHostName != null && reference != null)
                {
                    groups.ReferenceTracker.Add(clientHostName, reference);
                }
                InsertCacheMemberGroups(groups, true, true,clientHostName);
                if (groups.Groups.Count == 0)
                {

                    Matchnet.Member.ServiceAdapters.Member member1 = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
                    int hasgroups = member1.GetAttributeInt(brand, "HasAffiliationGroup");
                    if (hasgroups > 0)
                    {
                        member1.SetAttributeInt(brand, "HasAffiliationGroup", 0);
                        Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(member1);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new BLException(ex);

            }

        }
        public void DeleteMemberGroups(int brandid, int categorysiteid, int memberid)
        {
            try
            {

                Command comm = new Command();
                comm.LogicalDatabaseName = "mnPremiumServices";
                comm.StoredProcedureName = "up_AffiliationCategoryMember_Delete";


                comm.AddParameter("@categorysiteid", SqlDbType.Int, ParameterDirection.Input, categorysiteid);
                comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberid);

                Client.Instance.ExecuteAsyncWrite(comm);
                Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandid);
               // AffiliationMemberGroupsList groups = (AffiliationMemberGroupsList)_cache.Get(AffiliationMemberGroupsList.GetCacheKey(siteid, memberid));
              
                _cache.Remove(AffiliationMemberGroupsList.GetCacheKey(brand.Site.SiteID, memberid));
                

                Matchnet.Member.ServiceAdapters.Member member1 = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);
                int hasgroups = member1.GetAttributeInt(brand, "HasAffiliationGroup");
                if (hasgroups > 0)
                {
                    member1.SetAttributeInt(brand, "HasAffiliationGroup", 0);
                    Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(member1);
                }
                
            }
            catch (Exception ex)
            {
                throw new BLException(ex);

            }

        }

        #endregion

        #region private methods
        private void saveGroupMember(int brandid,  AffiliationGroupMember member)
        {
            try
            {
                if (member.AffiliationGroupMemberID <= 0)
                    member.AffiliationGroupMemberID = KeySA.Instance.GetKey("AffiliationGroupMemberID");
                Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandid);
                Command comm = new Command();
                comm.LogicalDatabaseName = "mnPremiumServices";
                comm.StoredProcedureName = "up_AffiliationGroupMember_Save";

                comm.AddParameter("@AffiliationGroupMemberID", SqlDbType.Int, ParameterDirection.Input, member.AffiliationGroupMemberID);
                comm.AddParameter("@affiliationgroupid", SqlDbType.Int, ParameterDirection.Input, member.AffiliationGroupID);
                comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, member.MemberID);
                comm.AddParameter("@Site", SqlDbType.Int, ParameterDirection.Input, brand.Site.SiteID);
            
                Client.Instance.ExecuteAsyncWrite(comm);
            }
            catch (Exception ex)
            {
                throw new BLException(ex);

            }

        }
        private AffiliationMember getAffiliationMember(int siteid, int memberid)
        {
            SqlDataReader rs = null;
            try
            {
                AffiliationMember member = (AffiliationMember)_cache.Get(AffiliationMember.GetCacheKey(siteid, memberid));
                if (member != null)
                { return member; }

                Command comm = new Command();
                comm.LogicalDatabaseName = "mnPremiumServices";
                comm.StoredProcedureName = "up_AffiliationMember_Get";

                comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberid);
                comm.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteid);
                rs = Client.Instance.ExecuteReader(comm);

                if (!rs.Read())
                { return null; }
                member = new AffiliationMember();
                member.AffiliationMemberID = (int)(rs["affiliationmemberid"] != Convert.DBNull ? rs["affiliationmemberid"] : 0);
                member.MemberID = (int)(rs["memberid"] != Convert.DBNull ? rs["memberid"] : 0);
                member.SiteID = (int)(rs["siteid"] != Convert.DBNull ? rs["siteid"] : 0);
                member.BirthDate = (DateTime)(rs["birthdate"] != Convert.DBNull ? rs["birthdate"] : new DateTime());
                member.GenderMask = (int)(rs["gendermask"] != Convert.DBNull ? rs["gendermask"] : 0);
                member.BoxX = (int)(rs["boxx"] != Convert.DBNull ? rs["boxx"] : 0);
                member.BoxY = (int)(rs["boxy"] != Convert.DBNull ? rs["boxy"] : 0);
                member.RegionID = (int)(rs["regionid"] != Convert.DBNull ? rs["regionid"] : 0);
                member.Depth1RegionID = (int)(rs["depth1regionid"] != Convert.DBNull ? rs["depth1regionid"] : 0);
                member.Depth2RegionID = (int)(rs["depth2regionid"] != Convert.DBNull ? rs["depth2regionid"] : 0);
                member.Depth3RegionID = (int)(rs["depth3regionid"] != Convert.DBNull ? rs["depth3regionid"] : 0);
                member.Depth4RegionID = (int)(rs["depth4regionid"] != Convert.DBNull ? rs["depth4regionid"] : 0);
                member.Longitude = Conversion.CDouble(rs["longitude"].ToString());
                member.Latitude = Conversion.CDouble(rs["latitude"].ToString());
                member.HasPhotoFlag = (bool)(rs["hasphotoflag"] != Convert.DBNull ? rs["hasphotoflag"] : false);
                member.CreatedDate = (DateTime)(rs["createddate"] != Convert.DBNull ? rs["createddate"] : DateTime.MinValue);
                member.UpdateDate = (DateTime)(rs["updatedate"] != Convert.DBNull ? rs["updatedate"] : DateTime.MinValue);
                _cache.Insert(member);
                return member;
            }
            catch (Exception ex)
            {
                throw new BLException(ex);
            }
            finally
            {
                if (rs != null)
                    rs.Dispose();
            }

        }
        private void saveMember(AffiliationMember member)
        {
            try
            {


                Command comm = new Command();
                comm.LogicalDatabaseName = "mnPremiumServices";
                comm.StoredProcedureName = "up_AffiliationMember_Save";

                comm.AddParameter("@AffiliationMemberID", SqlDbType.Int, ParameterDirection.Input, member.AffiliationMemberID);
                comm.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, member.SiteID);
                // comm.AddParameter("@affiliationgroupid", SqlDbType.Int, ParameterDirection.Input,member.AffiliationGroupID);
                comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, member.MemberID);
                if (member.BirthDate > DateTime.MinValue)
                    comm.AddParameter("@BirthDate", SqlDbType.DateTime, ParameterDirection.Input, member.BirthDate);
                if (member.GenderMask > 0)
                    comm.AddParameter("@GenderMask", SqlDbType.Int, ParameterDirection.Input, member.MemberID);
                if (member.BoxX > 0)
                    comm.AddParameter("@BoxX", SqlDbType.Int, ParameterDirection.Input, member.BoxX);
                if (member.BoxY > 0)
                    comm.AddParameter("@BoxY", SqlDbType.Int, ParameterDirection.Input, member.BoxY);
                if (member.RegionID > 0)
                    comm.AddParameter("@RegionID", SqlDbType.Int, ParameterDirection.Input, member.RegionID);
                if (member.Depth1RegionID > 0)
                    comm.AddParameter("@Depth1RegionID", SqlDbType.Int, ParameterDirection.Input, member.Depth1RegionID);
                if (member.Depth2RegionID > 0)
                    comm.AddParameter("@Depth2RegionID", SqlDbType.Int, ParameterDirection.Input, member.Depth2RegionID);
                if (member.Depth3RegionID > 0)
                    comm.AddParameter("@Depth3RegionID", SqlDbType.Int, ParameterDirection.Input, member.Depth3RegionID);
                if (member.Depth4RegionID > 0)
                    comm.AddParameter("@Depth4RegionID", SqlDbType.Int, ParameterDirection.Input, member.Depth4RegionID);
                if (member.Longitude > 0)
                    comm.AddParameter("@Longitude", SqlDbType.Decimal, ParameterDirection.Input, member.Longitude);
                if (member.Latitude > 0)
                    comm.AddParameter("@Latitude", SqlDbType.Decimal, ParameterDirection.Input, member.Latitude);

                comm.AddParameter("@HasPhotoFlag", SqlDbType.Int, ParameterDirection.Input, member.HasPhotoFlag ? 1 : 0);
                Client.Instance.ExecuteAsyncWrite(comm);
            }
            catch (Exception ex)
            {
                throw new BLException(ex);

            }

        }
        private void getMemberUpdate(AffiliationMember amember, int brandid)
        {
            try
            {
                int memberid = amember.MemberID;

                Matchnet.Member.ServiceAdapters.Member member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, MemberLoadFlags.None);

                Brand brand = BrandConfigSA.Instance.GetBrands().GetBrand(brandid);
                Site site = brand.Site;


                amember.RegionID = member.GetAttributeInt(site.Community.CommunityID, site.SiteID, brandid, "RegionID", Constants.NULL_INT);
                amember.BirthDate = member.GetAttributeDate(site.Community.CommunityID, site.SiteID, brandid, "BirthDate", DateTime.MinValue);
                amember.GenderMask = member.GetAttributeInt(site.Community.CommunityID, site.SiteID, brandid, "GenderMask", Constants.NULL_INT);
                amember.HasPhotoFlag = member.HasApprovedPhoto(site.Community.CommunityID);
                populateLocation(site, amember);

            }
            catch (Exception ex) { throw ex; }
        }
        private AffiliationGroupMember createAffiliationGroupMember(DataRow rs)
        {
            AffiliationGroupMember member = new AffiliationGroupMember();
            int memberid = Conversion.CInt(rs["memberid"]);
            int groupid = Conversion.CInt(rs["affiliationgroupid"]);
            int groupmemberid = Conversion.CInt(rs["affiliationgroupmemberid"]);
            DateTime createddate = Conversion.CDateTime(rs["createddate"]);
            member.AffiliationGroupID = groupid;
            member.AffiliationGroupMemberID = groupmemberid;
            member.MemberID = memberid;
            member.CreatedDate = createddate;
            return member;

        }
        private void populateLocation(Content.ValueObjects.BrandConfig.Site site, AffiliationMember member)
        {
            Matchnet.Content.ValueObjects.Region.RegionLanguage region = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrievePopulatedHierarchy(member.RegionID, site.LanguageID);

            if (region == null)
            { throw new Exception("Invalid RegionID in  AffiliateMember update update: " + member.RegionID); }

            Geo.Coordinates regioncoordinates = new Geo.Coordinates((double)region.Latitude, (double)region.Longitude);
            Geo.GeoBox geo = new Geo.GeoBox(regioncoordinates, Matchnet.AffiliationGroups.ValueObjects.ServiceDefinitions.ServiceConstants.DEFAULT_PARTITION_ANGLE, 0, false);

            member.Latitude = (double)region.Latitude;
            member.Longitude = (double)region.Longitude;
            member.BoxX = geo.SearchBox.Key.X;
            member.BoxY = geo.SearchBox.Key.Y;
            member.Depth1RegionID = region.Depth1RegionID;
            member.Depth2RegionID = region.StateRegionID;
            member.Depth3RegionID = region.CityRegionID;
            member.Depth4RegionID = region.PostalCodeRegionID;
        }

        private void deleteGroupMember(int brandid, int groupid, int memberid)
        {
            try
            {

                Command comm = new Command();
                comm.LogicalDatabaseName = "mnPremiumServices";
                comm.StoredProcedureName = "up_AffiliationGroupMember_Delete";


                comm.AddParameter("@affiliationgroupid", SqlDbType.Int, ParameterDirection.Input, groupid);
                comm.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberid);

                Client.Instance.ExecuteAsyncWrite(comm);

              
            }
            catch (Exception ex)
            {
                throw new BLException(ex);

            }

        }

        #endregion
    }
}
