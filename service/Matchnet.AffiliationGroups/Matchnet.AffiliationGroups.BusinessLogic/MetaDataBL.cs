﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Matchnet.Data;
using Matchnet.Data.Configuration;
using Matchnet.Data.Hydra;
using Matchnet.Data.Exceptions;
using Matchnet.Configuration;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Exceptions;

using Matchnet.AffiliationGroups.ValueObjects;
namespace Matchnet.AffiliationGroups.BusinessLogic
{
    public class MetaDataBL
    {
        private Matchnet.Caching.Cache _cache;

        #region singleton implementation
        public readonly static MetaDataBL Instance = new MetaDataBL();

        private MetaDataBL()
		{
            _cache = Matchnet.Caching.Cache.Instance;
        }
        #endregion

        public AffiliationCategorySiteCollection GetSiteAffiliationCategories(int siteid)
        {
            AffiliationCategorySiteCollection coll = null;
            try
            {
                coll =(AffiliationCategorySiteCollection) _cache.Get(AffiliationCategorySiteCollection.GetCacheKey(siteid));
                if (coll != null)
                    return coll;

                Command comm=new Command();
                comm.LogicalDatabaseName = "mnPremiumServices";
                comm.StoredProcedureName = "up_AffiliationCategorySite_List";
                comm.AddParameter("@SITEID", SqlDbType.Int, ParameterDirection.Input, siteid);
                using (SqlDataReader rs = Client.Instance.ExecuteReader(comm))
                {
                    coll = new AffiliationCategorySiteCollection();
                    coll.CategorySites = new List<AffiliationCategorySite>();
                    while (rs.Read())
                    {
                        AffiliationCategorySite s = new AffiliationCategorySite();
                        s.SiteID = siteid;
                        s.CategorySiteID=Conversion.CInt(rs["categorysiteid"]);
                        s.CategoryID = Conversion.CInt(rs["categoryid"]);
                        s.Name = rs["name"].ToString();
                        s.Description = rs["description"].ToString();
                        s.ImagePath = (rs["imagepath"] == null) ? "" : rs["imagepath"].ToString();
                        s.DefaultLogo = (rs["DefaultLogo"] == null) ? "" : rs["imagepath"].ToString();
                        coll.CategorySites.Add(s);
                        
                    }
                }
                if (coll != null)
                {
                    _cache.Add(coll);
                }
                return coll;


            }
            catch (Exception ex)
            {
                throw new BLException(ex);

            }




        }

        public AffiliationCategorySite GetAffiliationCategory(int categorysiteid)
        {
            AffiliationCategorySite category = null;
            DataSet ds = null;
            try
            {
                category = (AffiliationCategorySite)_cache.Get(AffiliationCategorySite.GetCacheKey(categorysiteid));
                if (category != null)
                    return category;

                Command comm = new Command();
                comm.LogicalDatabaseName = "mnPremiumServices";
                comm.StoredProcedureName = "up_AffiliationCategorySite_Get";
                comm.AddParameter("@CATEGORYSITEID", SqlDbType.Int, ParameterDirection.Input, categorysiteid);
                
                ds = Client.Instance.ExecuteDataSet(comm);
                //categorysite
                DataTable dt1 = ds.Tables[0];
                //attribute
                DataTable dt2 = ds.Tables[1];
                //option
                DataTable dt3 = ds.Tables[2];
                //optionvalues
                DataTable dt4 = ds.Tables[3];

                if (dt1.Rows.Count <= 0)
                    return null;

                DataRow r = dt1.Rows[0];
                category = new AffiliationCategorySite();

                category.CategorySiteID = (int) (r["categorysiteid"]!=Convert.DBNull ? r["categorysiteid"] : 0);
                category.CategoryID = (int) (r["categoryid"]!=Convert.DBNull ? r["categoryid"] : 0);
                category.SiteID = (int) (r["siteid"]!=Convert.DBNull ? r["siteid"] : 0);
                category.Name = (string) (r["name"]!=Convert.DBNull ? r["name"] : "");
                category.Description = (string) (r["description"]!=Convert.DBNull ? r["description"] : "");
                category.ImagePath = (r["imagepath"] == null) ? "" : r["imagepath"].ToString();
                category.DefaultLogo = (r["DefaultLogo"] == null) ? "" : r["DefaultLogo"].ToString();
                category.Attributes = new AffiliationAttributeCategoryCollection();
                category.Attributes.CategorySiteID = categorysiteid;
                for (int l = 0; l < dt2.Rows.Count; l++)
                {
                    AffiliationAttributeCategorySite a = new AffiliationAttributeCategorySite();
                    DataRow rs2 = dt2.Rows[l];
                    a.AttributeID = (int) (rs2["attributeid"]!=Convert.DBNull ? rs2["attributeid"] : 0);
                    a.AttributeCategorySiteID = (int)(rs2["attributecategorysiteid"] != Convert.DBNull ? rs2["attributecategorysiteid"] : 0);
                    a.CategorySiteID = categorysiteid;
                    a.Description = (string) (rs2["description"]!=Convert.DBNull ? rs2["description"] : "");
                    if (category.Attributes.CategorySiteAttributes == null)
                        category.Attributes.CategorySiteAttributes = new List<AffiliationAttributeCategorySite>();

                    category.Attributes.CategorySiteAttributes.Add(a);
                }
                

                AffiliationCategoryOptionCollection coll = new AffiliationCategoryOptionCollection();
                coll.Options = new List<AffiliationCategoryOption>();
                for (int i = 0; i < dt3.Rows.Count; i++)
                {
                    DataRow rs = dt3.Rows[i];
                    AffiliationCategoryOption o = new AffiliationCategoryOption();
                    o.OptionValues = new List<AffiliationCategoryOptionValue>();
                    o.AffiliationCategoryOptionID = (int)(rs["affiliationcategoryoptionid"] != Convert.DBNull ? rs["affiliationcategoryoptionid"] : 0);
                    o.CategorySiteID = (int)(rs["categorysiteid"] != Convert.DBNull ? rs["categorysiteid"] : 0);
                    o.Name = (string)(rs["name"] != Convert.DBNull ? rs["name"] : "");
                    o.Description = (string)(rs["description"] != Convert.DBNull ? rs["description"] : "");
                    o.ActiveFlag = (bool)(rs["activeflag"] != Convert.DBNull ? rs["activeflag"] : false);
                    DataRow[] optionvalues = dt4.Select("affiliationcategoryoptionid=" + o.AffiliationCategoryOptionID.ToString());
                    for (int k = 0; k < optionvalues.Length; k++)
                    {
                        AffiliationCategoryOptionValue v = new AffiliationCategoryOptionValue();
                        DataRow rs2 = optionvalues[k];
                        v.AffiliationCategoryOptionValueID = (int)(rs2["affiliationcategoryoptionvalueid"] != Convert.DBNull ? rs2["affiliationcategoryoptionvalueid"] : 0);
                        v.AffiliationCategoryOptionID = (int)(rs2["affiliationcategoryoptionid"] != Convert.DBNull ? rs2["affiliationcategoryoptionid"] : 0);
                        v.Description = (string)(rs2["description"] != Convert.DBNull ? rs2["description"] : "");
                        v.Value = (int)(rs2["value"] != Convert.DBNull ? rs2["value"] : 0);
                        v.ListOrder = (int)(rs2["listorder"] != Convert.DBNull ? rs2["listorder"] : 0);
                        o.OptionValues.Add(v);
                    }
                    coll.Options.Add(o);
                }

                category.Options = coll;
                return category;

                }
            catch (Exception ex)
            {
                throw new BLException(ex);

            }




        }

        public AffiliationCategoryOptionCollection GetAffiliationCategoryOptions(int categorysiteid)
        {
            AffiliationCategoryOptionCollection coll = null;
            DataSet ds = null;
            try
            {
                coll = (AffiliationCategoryOptionCollection)_cache.Get(AffiliationCategoryOptionCollection.GetCacheKey(categorysiteid));
                if (coll != null)
                    return coll;

                Command comm = new Command();
                comm.LogicalDatabaseName = "mnPremiumServices";
                comm.StoredProcedureName = "up_AffiliationCategoryOption_List";
                comm.AddParameter("@categorysiteid", SqlDbType.Int, ParameterDirection.Input, categorysiteid);

                ds = Client.Instance.ExecuteDataSet(comm);
                //options
                DataTable dt1 = ds.Tables[0];
                DataTable dt2 = ds.Tables[1];
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    DataRow rs = dt1.Rows[i];
                    AffiliationCategoryOption o = new AffiliationCategoryOption();
                    o.AffiliationCategoryOptionID = (int)(rs["affiliationcategoryoptionid"] != Convert.DBNull ? rs["affiliationcategoryoptionid"] : 0);
                    o.CategorySiteID = (int)(rs["categorysiteid"] != Convert.DBNull ? rs["categorysiteid"] : 0);
                    o.Description = (string)(rs["description"] != Convert.DBNull ? rs["description"] : "");
                    o.ActiveFlag = (bool)(rs["activeflag"] != Convert.DBNull ? rs["activeflag"] : false);
                    DataRow[] optionvalues = dt2.Select("affiliationcategoryoptionid=" + o.AffiliationCategoryOptionID.ToString());
                    for (int k = 0; k < optionvalues.Length; k++)
                    {
                        AffiliationCategoryOptionValue v = new AffiliationCategoryOptionValue();
                        DataRow rs2 = optionvalues[k];
                        v.AffiliationCategoryOptionValueID = (int)(rs2["affiliationcategoryoptionvalueid"] != Convert.DBNull ? rs2["affiliationcategoryoptionvalueid"] : 0);
                        v.AffiliationCategoryOptionID = (int)(rs2["affiliationcategoryoptionid"] != Convert.DBNull ? rs2["affiliationcategoryoptionid"] : 0);
                        v.Description = (string)(rs2["description"] != Convert.DBNull ? rs2["description"] : "");
                        v.Value = (int)(rs2["value"] != Convert.DBNull ? rs2["value"] : 0);
                        v.ListOrder = (int)(rs2["listorder"] != Convert.DBNull ? rs2["listorder"] : 0);
                        o.OptionValues.Add(v);
                    }
                    coll.Options.Add(o);
                }
                if (coll != null)
                {
                    _cache.Add(coll);
                }
                return coll;


            }
            catch (Exception ex)
            {
                throw new BLException(ex);

            }
            finally
            {
                if (ds != null)
                    ds.Dispose();
            }




        }

        public AffiliationAttributeCollection GetAttributeCollection()
        {
            AffiliationAttributeCollection coll = null;
            try
            {
                coll = (AffiliationAttributeCollection)_cache.Get(AffiliationAttributeCollection.CACHEKEY);
                if (coll != null)
                    return coll;

                Command comm = new Command();
                comm.LogicalDatabaseName = "mnPremiumServices";
                comm.StoredProcedureName = "up_AffiliationAttribute_List";
                
                using (SqlDataReader rs = Client.Instance.ExecuteReader(comm))
                {
                    coll = new AffiliationAttributeCollection();
                    while (rs.Read())
                    {
                        AffiliationAttribute a = new AffiliationAttribute();
                        a.AttributeName = rs["AttributeName"].ToString();
                        a.AttributeType = Conversion.CInt(rs["AttributeType"]);
                        a.AttributeID = Conversion.CInt(rs["AttributeID"]);
                        if (coll.Attributes == null)
                            coll.Attributes = new List<AffiliationAttribute>();

                        coll.Attributes.Add(a);

                    }
                }
                if (coll != null)
                {
                    _cache.Add(coll);
                }
                return coll;


            }
            catch (Exception ex)
            {
                throw new BLException(ex);

            }




        }
        
       

    }
}
