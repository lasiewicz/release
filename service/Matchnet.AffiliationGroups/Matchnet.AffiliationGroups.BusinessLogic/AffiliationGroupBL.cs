﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Web.Caching;

using Matchnet.Data;
using Matchnet.Data.Configuration;
using Matchnet.Data.Hydra;
using Matchnet.Data.Exceptions;
using Matchnet.Configuration;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;

using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.CacheSynchronization.Tracking;

using Matchnet.AffiliationGroups.ValueObjects;
namespace Matchnet.AffiliationGroups.BusinessLogic
{
    public class AffiliationGroupBL
    {


        const string DATA_FORMAT = "categoryid:{0}, key:{1}";
        const string MODULE_NAME = "AffiliationGroupBL";
        //for now max attr # to 10 for speed dev
        public const int MAX_ATTRIBUTE_NUMBER=10;
        public const int MAX_OPTION_NUMBER=10;
        private Matchnet.Caching.Cache _cache;

        #region synchronization/replication
        public delegate void ReplicationEventHandler(IReplicable replicableObject);
        public event ReplicationEventHandler ReplicationRequested;

        public delegate void SynchronizationEventHandler(string key, Hashtable cacheReferences);
        public event SynchronizationEventHandler SynchronizationRequested;

        public delegate void AffiliationGroupsRequestEventHandler(bool cacheHit,  int count);
        public event AffiliationGroupsRequestEventHandler AffiliationGroupsRequested;

        private CacheItemRemovedCallback expireGroups;

        private void ExpireGroupsCallback(string key, object value, CacheItemRemovedReason callbackreason)
        {
            AffiliationGroupsCollection groups = value as AffiliationGroupsCollection;
            if (groups != null)
            {
                ReplicationRequested(groups);
                SynchronizationRequested(groups.GetCacheKey(),
                groups.ReferenceTracker.Purge(Constants.NULL_STRING));
                // IncrementExpirationCounter();
            }
        }


        public void InsertCacheGroups(AffiliationGroupsCollection groups, bool replicate, bool synchronize)
        {
            string functionName = "InsertCacheGroups";
            string trace = "";
            try
            {
                if (groups == null)
                    return;

                trace = String.Format(DATA_FORMAT, groups.CategorySiteID, groups.GetCacheKey());
                _cache.Insert(groups);
                if (replicate)
                {
                    ReplicationRequested(groups);
                }
                if (synchronize)
                {
                    SynchronizationRequested(groups.GetCacheKey(),
                    groups.ReferenceTracker.Purge(Constants.NULL_STRING));
                }
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, groups, null, trace, true);
            }
        }


        public void InsertCacheGroup(AffiliationGroup group, bool replicate, bool synchronize)
        {
            string functionName = "InsertCacheGroup";
            string trace = "";
            try
            {
                if (group == null)
                    return;

                trace = String.Format(DATA_FORMAT, group.CategorySiteID, group.GetCacheKey());
                _cache.Insert(group);
                if (replicate)
                {
                    ReplicationRequested(group);
                }
                if (synchronize)
                {
                    SynchronizationRequested(group.GetCacheKey(),
                    group.ReferenceTracker.Purge(Constants.NULL_STRING));
                }
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, group, null, trace, true);
            }
        }
        #endregion

        #region singleton implementation
        public readonly static AffiliationGroupBL Instance = new AffiliationGroupBL();

        private AffiliationGroupBL()
		{
            _cache = Matchnet.Caching.Cache.Instance;
            expireGroups = new CacheItemRemovedCallback(this.ExpireGroupsCallback);
        }

        public AffiliationGroupsCollection GetAffiliationGroups(int categorysiteid)
        {
            AffiliationGroupsCollection groups = null;
            DataSet ds = null;
            try
            {
                groups = (AffiliationGroupsCollection)_cache.Get(AffiliationGroupsCollection.GetCacheKey(categorysiteid));
                if (groups != null)
                {
                    int count = 0;
                    if (groups.Groups != null)
                        count = groups.Groups.Count;
                    AffiliationGroupsRequested(true, count);
                    return groups;

                }
                Command comm = new Command();
                comm.LogicalDatabaseName = "mnPremiumServices";
                comm.StoredProcedureName = "up_AffiliationGroups_List";
                comm.AddParameter("@CATEGORYSITEID", SqlDbType.Int, ParameterDirection.Input, categorysiteid);

                ds = Client.Instance.ExecuteDataSet(comm);
                //groups
                DataTable dt1 = ds.Tables[0];
                //attributes
                DataTable dt2 = ds.Tables[1];
                //options
                DataTable dt3 = ds.Tables[2];

                if (dt1.Rows.Count <= 0)
                    return null;
                               
                groups = new AffiliationGroupsCollection();
                groups.CategorySiteID = categorysiteid;
                groups.Groups = new List<AffiliationGroup>();
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                     AffiliationGroup group = new AffiliationGroup();
                    DataRow rs = dt1.Rows[i];
                    group.AffiliationGroupID = (int) (rs["affiliationgroupid"]!=Convert.DBNull ? rs["affiliationgroupid"] : 0);
                    group.CategorySiteID = (int) (rs["categorysiteid"]!=Convert.DBNull ? rs["categorysiteid"] : 0);
                    group.ActiveFlag = (bool) (rs["activeflag"]!=Convert.DBNull ? rs["activeflag"] : false);
                    group.Name = (string) (rs["name"]!=Convert.DBNull ? rs["name"] : "");
                    group.Head = (string) (rs["head"]!=Convert.DBNull ? rs["head"] : "");
                    group.Address1 = (string) (rs["address1"]!=Convert.DBNull ? rs["address1"] : "");
                    group.Address2 = (string) (rs["address2"]!=Convert.DBNull ? rs["address2"] : "");
                    group.City = (string) (rs["city"]!=Convert.DBNull ? rs["city"] : "");
                    group.State = (string)(rs["state"] != Convert.DBNull ? rs["state"] : "");
                    group.PostalCode = (string) (rs["postalcode"]!=Convert.DBNull ? rs["postalcode"] : "");
                    group.Email = (string) (rs["email"]!=Convert.DBNull ? rs["email"] : "");
                    group.URL = (string) (rs["url"]!=Convert.DBNull ? rs["url"] : "");
                    group.About = (string) (rs["about"]!=Convert.DBNull ? rs["about"] : "");
                    group.Image = (string) (rs["image"]!=Convert.DBNull ? rs["image"] : "");
                    group.MembershipCount = (int) (rs["membershipcount"]!=Convert.DBNull ? rs["membershipcount"] : 0);
                    group.CreatedDate = (DateTime) (rs["createddate"]!=Convert.DBNull ? rs["createddate"] : new DateTime());
                    group.UpdateDate = (DateTime)(rs["updatedate"] != Convert.DBNull ? rs["updatedate"] : new DateTime());


                    DataRow[] attr = dt2.Select("affiliationgroupid=" + group.AffiliationGroupID.ToString());
                    group.Attributes = new List<AttributeValue>();
                    if (attr != null && attr.Length > 0)
                    {
                        for (int k = 0; k < attr.Length; k++)
                        {
                            AttributeValue v = new AttributeValue();
                            DataRow rs1 = attr[k];
                            v.AffiliationGroupID = (int)(rs1["affiliationgroupid"] != Convert.DBNull ? rs1["affiliationgroupid"] : 0);
                            v.AttributeCategorySiteID = (int)(rs1["attributecategorysiteid"] != Convert.DBNull ? rs1["attributecategorysiteid"] : 0);
                            v.ValueText = (string)(rs1["valuetext"] != Convert.DBNull ? rs1["valuetext"] : "");
                            v.ValueInt = (int)(rs1["valueint"] != Convert.DBNull ? rs1["valueint"] : 0);
                            v.ValueDate = (DateTime)(rs1["valuedate"] != Convert.DBNull ? rs1["valuedate"] : new DateTime());
                            group.Attributes.Add(v);
                        }
                    }
                    DataRow[] opts = dt3.Select("affiliationgroupid=" + group.AffiliationGroupID.ToString());
                    group.Options = new List<AffiliationGroupOption>();
                    if (opts != null && opts.Length > 0)
                    {
                        for (int k = 0; k < attr.Length; k++)
                        {
                            AffiliationGroupOption v = new AffiliationGroupOption();
                            DataRow rs1 = opts[k];
                            v.AffiliationGroupOptionID = (int)(rs1["affiliationgroupoptionid"] != Convert.DBNull ? rs1["affiliationgroupoptionid"] : 0);
                            v.AffiliationGroupID = (int)(rs1["affiliationgroupid"] != Convert.DBNull ? rs1["affiliationgroupid"] : 0);
                            v.AffiliationCategoryOptionValueID = (int)(rs1["affiliationcategoryoptionvalueid"] != Convert.DBNull ? rs1["affiliationcategoryoptionvalueid"] : 0);
                            group.Options.Add(v);
                        }
                    }
                    groups.Groups.Add(group);
                }

                List<AffiliationGroup> toplist = (from g in groups.Groups
                                                  where g.AffiliationGroupID > 0
                                                  orderby g.MembershipCount descending
                                                  select g).ToList<AffiliationGroup>();
                groups.TopGroups = toplist;

                InsertCacheGroups(groups,true,true);

                AffiliationGroupsRequested(true, groups.Groups.Count);
                return groups;
               
            }
            catch (Exception ex)
            {
                throw new BLException(ex);

            }




        }

        public AffiliationGroup GetGroup(int groupid)
        {
            AffiliationGroup group = null;
            DataSet ds = null;
            try
            {
                group = (AffiliationGroup)_cache.Get(AffiliationGroup.GetCacheKey(groupid));
                if (group != null)
                    return group;

                Command comm = new Command();
                comm.LogicalDatabaseName = "mnPremiumServices";
                comm.StoredProcedureName = "up_AffiliationGroup_List";
                comm.AddParameter("@AffiliationGroupID", SqlDbType.Int, ParameterDirection.Input, groupid);

                ds = Client.Instance.ExecuteDataSet(comm);
                //groups
                DataTable dt1 = ds.Tables[0];
                //attributes
                DataTable dt2 = ds.Tables[1];
                //options
                DataTable dt3 = ds.Tables[2];

                if (dt1.Rows.Count <= 0)
                    return null;

                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    group = new AffiliationGroup();
                    DataRow rs = dt1.Rows[i];
                    group.AffiliationGroupID = (int)(rs["affiliationgroupid"] != Convert.DBNull ? rs["affiliationgroupid"] : 0);
                    group.CategorySiteID = (int)(rs["categorysiteid"] != Convert.DBNull ? rs["categorysiteid"] : 0);
                    group.ActiveFlag = (bool)(rs["activeflag"] != Convert.DBNull ? rs["activeflag"] : false);
                    group.Name = (string)(rs["name"] != Convert.DBNull ? rs["name"] : "");
                    group.Head = (string)(rs["head"] != Convert.DBNull ? rs["head"] : "");
                    group.Address1 = (string)(rs["address1"] != Convert.DBNull ? rs["address1"] : "");
                    group.Address2 = (string)(rs["address2"] != Convert.DBNull ? rs["address2"] : "");
                    group.City = (string)(rs["city"] != Convert.DBNull ? rs["city"] : "");
                    group.State = (string)(rs["state"] != Convert.DBNull ? rs["state"] : "");
                    group.PostalCode = (string)(rs["postalcode"] != Convert.DBNull ? rs["postalcode"] : "");
                    group.Email = (string)(rs["email"] != Convert.DBNull ? rs["email"] : "");
                    group.URL = (string)(rs["url"] != Convert.DBNull ? rs["url"] : "");
                    group.About = (string)(rs["about"] != Convert.DBNull ? rs["about"] : "");
                    group.Image = (string)(rs["image"] != Convert.DBNull ? rs["image"] : "");
                    group.MembershipCount = (int)(rs["membershipcount"] != Convert.DBNull ? rs["membershipcount"] : 0);
                    group.CreatedDate = (DateTime)(rs["createddate"] != Convert.DBNull ? rs["createddate"] : new DateTime());
                    group.UpdateDate = (DateTime)(rs["updatedate"] != Convert.DBNull ? rs["updatedate"] : new DateTime());


                    DataRow[] attr = dt2.Select("affiliationgroupid=" + group.AffiliationGroupID.ToString());
                    group.Attributes = new List<AttributeValue>();
                    for (int k = 0; k < attr.Length; k++)
                    {
                        AttributeValue v = new AttributeValue();
                        DataRow rs1 = attr[k];
                        v.AffiliationGroupID = (int)(rs1["affiliationgroupid"] != Convert.DBNull ? rs1["affiliationgroupid"] : 0);
                        v.AttributeCategorySiteID = (int)(rs1["attributecategorysiteid"] != Convert.DBNull ? rs1["attributecategorysiteid"] : 0);
                        v.ValueText = (string)(rs1["valuetext"] != Convert.DBNull ? rs1["valuetext"] : "");
                        v.ValueInt = (int)(rs1["valueint"] != Convert.DBNull ? rs1["valueint"] : 0);
                        v.ValueDate = (DateTime)(rs1["valuedate"] != Convert.DBNull ? rs1["valuedate"] : new DateTime());
                        group.Attributes.Add(v);
                    }

                    DataRow[] opts = dt3.Select("affiliationgroupid=" + group.AffiliationGroupID.ToString());
                    group.Options = new List<AffiliationGroupOption>();
                    for (int k = 0; k < opts.Length; k++)
                    {
                        AffiliationGroupOption v = new AffiliationGroupOption();
                        DataRow rs1 = opts[k];
                        v.AffiliationGroupOptionID = (int)(rs1["affiliationgroupoptionid"] != Convert.DBNull ? rs1["affiliationgroupoptionid"] : 0);
                        v.AffiliationCategoryOptionID = (int)(rs1["AffiliationCategoryOptionID"] != Convert.DBNull ? rs1["AffiliationCategoryOptionID"] : 0);
                        v.AffiliationGroupID = (int)(rs1["affiliationgroupid"] != Convert.DBNull ? rs1["affiliationgroupid"] : 0);
                        v.AffiliationCategoryOptionValueID = (int)(rs1["affiliationcategoryoptionvalueid"] != Convert.DBNull ? rs1["affiliationcategoryoptionvalueid"] : 0);
                        group.Options.Add(v);
                    }

                  
                }
                _cache.Insert(group);
                return group;

            }
            catch (Exception ex)
            {
                throw new BLException(ex);

            }




        }

        public AffiliationGroup SaveGroup(AffiliationGroup group)
        {
            try
            {
                if (group == null)
                    return group;

                Command comm = new Command();
                comm.LogicalDatabaseName = "mnPremiumServices";
                comm.StoredProcedureName = "up_AffiliationGroup_Save";

                if (group.AffiliationGroupID <= 0)
                {
                    group.AffiliationGroupID=KeySA.Instance.GetKey("AffiliationGroupID");

                }
                comm.AddParameter("@AffiliationGroupID", SqlDbType.Int, ParameterDirection.Input, group.AffiliationGroupID);
                comm.AddParameter("@CategorySiteID", SqlDbType.Int, ParameterDirection.Input, group.CategorySiteID);
                comm.AddParameter("@ActiveFlag", SqlDbType.Int, ParameterDirection.Input, group.ActiveFlag);
                comm.AddParameter("@Name", SqlDbType.NVarChar, ParameterDirection.Input, group.Name);
                comm.AddParameter("@Head", SqlDbType.NVarChar, ParameterDirection.Input, group.Head);
                comm.AddParameter("@Address1", SqlDbType.NVarChar, ParameterDirection.Input, group.Address1);
                comm.AddParameter("@Address2", SqlDbType.NVarChar, ParameterDirection.Input, group.Address2);
                comm.AddParameter("@City", SqlDbType.NVarChar, ParameterDirection.Input, group.City);
                comm.AddParameter("@State", SqlDbType.NVarChar, ParameterDirection.Input, group.State);
                comm.AddParameter("@Phone", SqlDbType.NVarChar, ParameterDirection.Input, group.Phone);
                comm.AddParameter("@PostalCode", SqlDbType.NVarChar, ParameterDirection.Input, group.PostalCode);
                comm.AddParameter("@Email", SqlDbType.NVarChar, ParameterDirection.Input, group.Email);
                comm.AddParameter("@URL", SqlDbType.NVarChar, ParameterDirection.Input, group.URL);
                comm.AddParameter("@About", SqlDbType.NVarChar, ParameterDirection.Input, group.About);
                comm.AddParameter("@Image", SqlDbType.NVarChar, ParameterDirection.Input, group.Image);
                comm.AddParameter("@MembershipCount", SqlDbType.NVarChar, ParameterDirection.Input, group.MembershipCount);

                Client.Instance.ExecuteAsyncWrite(comm);
                comm = null;

               
                int start = 0;
                if (group.Attributes != null && group.Attributes.Count > 0)
                {
                    int finish = group.Attributes.Count < MAX_ATTRIBUTE_NUMBER ? group.Attributes.Count : MAX_ATTRIBUTE_NUMBER;

                    for (int i = start; i < finish; i++)
                    {
                        Command attrcomm = new Command();
                     
                        attrcomm.LogicalDatabaseName = "mnPremiumServices";
                        attrcomm.StoredProcedureName = "up_AffiliateAttributeValue_Save_Multiple";
                        attrcomm.AddParameter("@AffiliationGroupID", SqlDbType.Int, ParameterDirection.Input, group.AffiliationGroupID);
                        for (int k = 1; k <= finish; k++)
                        {
                            AttributeValue v = group.Attributes[k-1];

                            attrcomm.AddParameter(String.Format("@AttributeCategorySiteID{0}", k), SqlDbType.Int, ParameterDirection.Input, v.AttributeCategorySiteID);
                            if(!String.IsNullOrEmpty(v.ValueText))
                                attrcomm.AddParameter(String.Format("@ValueText{0}", k), SqlDbType.NVarChar, ParameterDirection.Input, v.ValueText);
                            if(v.ValueInt > Constants.NULL_INT)
                                attrcomm.AddParameter(String.Format("@ValueInt{0}", k), SqlDbType.Int, ParameterDirection.Input, v.ValueInt);
                            if(v.ValueDate > DateTime.MinValue)
                                attrcomm.AddParameter(String.Format("@ValueDate{0}", k), SqlDbType.DateTime, ParameterDirection.Input, v.ValueDate);

                        }

                        Client.Instance.ExecuteAsyncWrite(attrcomm);
                        start += finish;
                        finish = group.Attributes.Count - start < MAX_ATTRIBUTE_NUMBER ? group.Attributes.Count - start : MAX_ATTRIBUTE_NUMBER;
                    }
                }
                if (group.Options != null && group.Options.Count >0)
                {
                    start = 0;
                    int finish = group.Options.Count < MAX_OPTION_NUMBER ? group.Options.Count : MAX_OPTION_NUMBER;


                    for (int i = start; i < finish; i++)
                    {
                        Command optcomm = new Command();
                       

                        optcomm.LogicalDatabaseName = "mnPremiumServices";
                        optcomm.StoredProcedureName = "up_AffiliationGroupOption_Save_Multiple";
                        optcomm.AddParameter("@AffiliationGroupID", SqlDbType.Int, ParameterDirection.Input, group.AffiliationGroupID);
                        for (int k = 1; k <= finish; k++)
                        {
                            AffiliationGroupOption v = group.Options[k-1];
                            if (v.AffiliationGroupOptionID <= 0)
                            {
                                v.AffiliationGroupOptionID = KeySA.Instance.GetKey("AffiliationGroupOptionID");

                            }
                            optcomm.AddParameter(String.Format("@AffiliationGroupOptionID{0}", k), SqlDbType.Int, ParameterDirection.Input, v.AffiliationGroupOptionID);
                            optcomm.AddParameter(String.Format("@AffiliationCategoryOptionID{0}", k), SqlDbType.Int, ParameterDirection.Input, v.AffiliationCategoryOptionID);
                            optcomm.AddParameter(String.Format("@AffiliationCategoryOptionValueID{0}", k), SqlDbType.Int, ParameterDirection.Input, v.AffiliationCategoryOptionValueID);
                        }

                        Client.Instance.ExecuteAsyncWrite(optcomm);
                        start += finish;
                        finish = group.Options.Count - start < MAX_OPTION_NUMBER ? group.Options.Count - start : MAX_OPTION_NUMBER;
                    }
                    
                }

                InsertCacheGroup(group, true, true);
                _cache.Remove(AffiliationGroupsCollection.GetCacheKey(group.CategorySiteID));
                AffiliationGroupsCollection coll = GetAffiliationGroups(group.CategorySiteID);
               
                return group;

            }
            catch (Exception ex)
            { throw new BLException(ex); }




        }

        public void DeleteGroup(int categorysiteid,int groupid)
        {
         
            try
            {
              
                Command comm = new Command();
                comm.LogicalDatabaseName = "mnPremiumServices";
                comm.StoredProcedureName = "up_AffiliationGroup_Delete";
                 AffiliationGroupsCollection coll = GetAffiliationGroups(categorysiteid);
                AffiliationGroup group= coll.GetGroup(groupid);
                comm.AddParameter("@AffiliationGroupID", SqlDbType.Int, ParameterDirection.Input, groupid);

                Client.Instance.ExecuteAsyncWrite(comm);

                if(group != null)
                 {
                     coll.Groups.Remove(group);
                     InsertCacheGroups(coll, true, true);
                 }
            }
            catch (Exception ex)
            {
                throw new BLException(ex);

            }




        }

        #endregion
    }
}
