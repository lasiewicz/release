﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Messaging;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;

using Matchnet.AffiliationGroups.ValueObjects;
namespace Matchnet.AffiliationGroups.BusinessLogic
{
    public class QueueProcessor
    {
        const string MODULE_NAME = "AffiliationGroups.QueueProcessor";
        public const string QUEUE_PATH = @".\private$\AffiliationGroups";
        bool _runnable;

        private MessageQueue premiumMemberQueue;
        private Thread[] threads;

        public void Start()
        {
            const string functionName = "Start";
            try
            {
                startThreads();
            }
            catch (Exception ex)
            {

                //PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);

            }

        }


        public void Stop()
        {
            const string functionName = "Stop";
            try
            {
                stopThreads();
            }
            catch (Exception ex)
            {

                //PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);

            }

        }

        private void startThreads()
        {
            const string functionName = "startThreads";
            try
            {
                premiumMemberQueue = new MessageQueue(QUEUE_PATH);
                premiumMemberQueue.Formatter = new BinaryMessageFormatter();

                Int16 threadCount = Convert.ToInt16(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("AFFILIATIONGROUPSSVC_THREAD_COUNT"));
                threads = new Thread[threadCount];

                for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
                {
                    Thread t = new Thread(new ThreadStart(processQueue));
                    t.Name = "ProcessMemberThread" + threadNum.ToString();
                    t.Start();
                    threads[threadNum] = t;
                    _runnable = true;
                }
            }
            catch (Exception ex)
            {
               // PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }

        }
        
        private void stopThreads()
        {
            const string functionName = "stopThreads";
            _runnable = false;
            try
            {
                Int16 threadCount = (Int16)threads.Length;

                for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
                {
                    threads[threadNum].Join(10000);
                }

               // PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName, "Stopped all SpotlightMember threads");

            }
            catch (Exception ex)
            {
               // PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);

            }

        }

        public static void Enqueue(IQueueMessage message)
        {
            const string functionName = "Enqueue";
            try
            {
                if (!MessageQueue.Exists(QUEUE_PATH))
                {
                    MessageQueue.Create(QUEUE_PATH, true);
                }

                MessageQueue queue = new MessageQueue(QUEUE_PATH);
                queue.Formatter = new BinaryMessageFormatter();
                MessageQueueTransaction trans = new MessageQueueTransaction();

                try
                {
                    trans.Begin();
                    queue.Send(message, trans);

                    trans.Commit();
                }
                finally
                {
                    trans.Dispose();
                }

            }
            catch (Exception ex)
            {
               // PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
        }

        private void processQueue()
        {
            const string functionName = "processQueue";
            try
            {
                while (_runnable)
                {
                    processQueueTransaction();
                }

            }

            catch (Exception ex)
            {

              //  PremiumServiceSearch.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);

            }


        }

        private void processQueueTransaction()
        {
            MessageQueueTransaction tran = null;

            try
            {
                tran = new MessageQueueTransaction();
                tran.Begin();
                IQueueMessage msg = receive(tran);

                processMessage(msg, tran);
                tran.Commit();
                tran.Dispose();
                tran = null;
            }
            catch (Exception ex)
            {
                attemptRollback(tran);
                Thread.Sleep(5000);
            }
        }

        private void processMessage(IQueueMessage msg, MessageQueueTransaction tran)
        {

            try
            {
                if (msg == null)
                { throw (new Exception("Queue message is null")); }

                MemberMessage membermsg = (MemberMessage)msg;
                switch (membermsg.Action)
                { case QueueAction.addgroupmember:
                        AffiliationGroupMemberBL.Instance.AddGroupMember(membermsg.BrandID, membermsg.AffiliationGroupID,membermsg.CategorySiteID, membermsg.MemberID,null,null);
                        break;
                case QueueAction.updatemember:
                        AffiliationGroupMemberBL.Instance.SaveAffiliationMember(membermsg.BrandID,membermsg.MemberID);
                        break;
                case QueueAction.deletegroupmember:
                        AffiliationGroupMemberBL.Instance.DeleteGroupMember(membermsg.BrandID, membermsg.AffiliationGroupID, membermsg.CategorySiteID,membermsg.MemberID, null,null);
                        break;


                }

                
            }
            catch (Exception ex)
            {
                attemptResend(tran, msg, ex);
            }
        }

        private IQueueMessage receive(MessageQueueTransaction tran)
        {
            IQueueMessage msg = null;
            try
            {

                msg = (IQueueMessage)premiumMemberQueue.Receive(tran).Body;
                return msg;
            }
            catch (MessageQueueException mex)
            { throw new Exception("Message queue error. [Error:" + mex.Message + "]", mex); }
        }

        private void attemptResend(MessageQueueTransaction tran, IQueueMessage msg, Exception ex)
        {
            try
            {
                if (msg == null)
                {
                    attemptRollback(tran);
                    return;
                }

                if (tran.Status == MessageQueueTransactionStatus.Pending)
                {
                    tran.Commit();

                    msg.Errors.Add(ex.Source + ", " + ex.Message);
                    msg.Attempts += 1;
                    msg.LastAttemptDateTime = DateTime.Now;
                    if (msg.Errors.Count >= 5)
                    {
                        BLException blex= new BLException("Error processing queue message :" + msg.ToString());
                    }
                    Enqueue(msg);
                }
            }
            catch (Exception e)
            {
                string message = "Exception while trying to resend msg: " + ex.Message + ", StackTrace: " + ex.StackTrace;
                throw (new Exception(message));
            }
        }

        private void attemptRollback(MessageQueueTransaction tran)
        {
            try
            {
                if (tran != null)
                {
                    if (tran.Status == MessageQueueTransactionStatus.Pending)
                    {
                        tran.Abort();
                    }
                    tran.Dispose();
                    tran = null;
                }
            }
            catch (Exception ex)
            {
                string message = "Exception while trying to abort transaction: " + ex.Message + ", StackTrace: " + ex.StackTrace;
            }
        }


    }
}
