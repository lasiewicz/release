﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using Matchnet.RemotingServices;
using Matchnet.ActivityRecording.ServiceManagers;

namespace Matchnet.ActivityRecording.Service
{
    public partial class ActivityRecording : RemotingServiceBase
    {
        private ActivityRecordingSM m_activityRecordingSM;

        public ActivityRecording()
        {
            InitializeComponent();
        }

        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new ActivityRecording() 
			};
            ServiceBase.Run(ServicesToRun);
        }

        protected override void RegisterServiceManagers()
        {
            System.Diagnostics.Trace.WriteLine("ActivityRecording, Start RegisterServiceManagers");

            m_activityRecordingSM = new ActivityRecordingSM();
            base.RegisterServiceManager(m_activityRecordingSM);
                        
            base.RegisterServiceManagers();
        }
    }
}
