﻿using System;
using Matchnet.ActivityRecording.ValueObjects;
using Matchnet.ActivityRecording.ValueObjects.ServiceDefinitions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Data;
using Matchnet.Exceptions;

namespace Matchnet.ActivityRecording.BusinessLogic
{
    public class ActivityRecordingBL : IActivityRecordingService
    {
        /// <summary>
        /// Singleton instance
        /// </summary>
        public readonly static ActivityRecordingBL Instance = new ActivityRecordingBL();

        private ActivityRecordingBL()
        {
            System.Diagnostics.Trace.Write("ActivityRecordingBL constructor.");
        }

        public void RecordActivity(int memberID, int targetMemberID, int siteID, int actionTypeID, int callingSystemID, string activityCaption)
        {
            try
            {
                int key = KeySA.Instance.GetKey("ActivityID");
                Command command = new Command("mnActivityRecording", "up_Activity_Insert", 0);

                command.AddParameter("@ActivityID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, key);
                command.AddParameter("@MemberID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, memberID);
                command.AddParameter("@TargetMemberID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, targetMemberID);
                command.AddParameter("@SiteID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, siteID);
                command.AddParameter("@ActionTypeID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, actionTypeID);
                command.AddParameter("@CallingSystemID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, callingSystemID);
                command.AddParameter("@ActivityCaption", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, activityCaption);

                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw new BLException("Error recording an activity. " + ex.ToString(), ex);
            }           
        }

        public void LogApiRequest(string originIP, string clientIP, string url, int httpStatusCode, int httpSubStatusCode, string method, int appID, int memberID, long totalEllapsedRequestTime, int brandID, string host, string machineName, string actionName, string formDataXML, string requestBodyXML)
        {
            try
            {
                Command command = new Command("mnMonitoring", "up_InsertApiWebLog", 0);

                command.AddParameter("@OriginIP", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, originIP);
                command.AddParameter("@ClientIP", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, clientIP);
                command.AddParameter("@Url", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, url);
                command.AddParameter("@HttpStatusCode", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, httpStatusCode);
                command.AddParameter("@HttpSubStatusCode", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, httpSubStatusCode);
                command.AddParameter("@HttpMethod", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, method);
                command.AddParameter("@ApplicationID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, appID);
                command.AddParameter("@MemberID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, memberID);
                command.AddParameter("@TotalRequestTimeInMs", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, totalEllapsedRequestTime);
                command.AddParameter("@InsertDate", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, DateTime.Now);
                command.AddParameter("@BrandID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, brandID);
                command.AddParameter("@MachineName", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, machineName);
                command.AddParameter("@Host", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, host);
                command.AddParameter("@ActionName", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, actionName);
                command.AddParameter("@FormData", System.Data.SqlDbType.Xml, System.Data.ParameterDirection.Input, formDataXML);
                command.AddParameter("@RequestBody", System.Data.SqlDbType.Xml, System.Data.ParameterDirection.Input, requestBodyXML);
                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw new BLException("Error logging api web request. " + ex.ToString(), ex);
            }    
        }


        public void RecordRegistrationStart(string regSessionID, string regFormFactor, int regApplication, 
            int siteID, int scenarioID, int ipAddress, DateTime insertDate)
        {
            if (string.IsNullOrEmpty(regSessionID))
            {
                //creating this exception will log the error
                var exception = new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Registration start with empty registration sessionid");
                return;
            }
            
            try
            {
                Command command = new Command("mnRegFlow", "up_RegTrackingHeader_Insert", 0);

                command.AddParameter("@RegSessionGUID", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, regSessionID);
                command.AddParameter("@RegFormFactor", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, regFormFactor);
                command.AddParameter("@RegApplication", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, regApplication);
                command.AddParameter("@SiteID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, siteID);
                command.AddParameter("@ScenarioID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, scenarioID);
                command.AddParameter("@IPAddress", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, ipAddress);
                command.AddParameter("@InsertDate", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, insertDate);

                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw new BLException("Error recording a registration start. " + ex.ToString(), ex);
            }    
        }

        public void RecordRegistrationComplete(string regSessionID, int memberID, DateTime updateDate)
        {
            if (string.IsNullOrEmpty(regSessionID))
            {
                //creating this exception will log the error
                var exception = new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Registration complete with empty registration sessionid");
                return;
            }
            
            try
            {
                Command command = new Command("mnRegFlow", "up_RegTrackingHeader_Update", 0);

                command.AddParameter("@RegSessionGUID", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, regSessionID);
                command.AddParameter("@MemberID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, memberID);
                command.AddParameter("@UpdateDate", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, updateDate);

                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw new BLException("Error recording a registration finish. " + ex.ToString(), ex);
            }   
        }

        public void RecordRegistrationStep(string regSessionID, int stepID, string stepDetails, DateTime insertDate)
        {
            if (string.IsNullOrEmpty(regSessionID))
            {
                //creating this exception will log the error
                var exception = new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Registration step with empty registration sessionid");
                return;
            }
            
            try
            {
                Command command = new Command("mnRegFlow", "up_RegTrackingDetails_Insert", 0);

                command.AddParameter("@RegSessionGUID", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, regSessionID);
                command.AddParameter("@StepID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, stepID);
                command.AddParameter("@StepDetails", System.Data.SqlDbType.Xml, System.Data.ParameterDirection.Input, stepDetails);
                command.AddParameter("@InsertDate", System.Data.SqlDbType.DateTime, System.Data.ParameterDirection.Input, insertDate);

                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw new BLException("Error recording a registration step. " + ex.ToString(), ex);
            }   
        }
    }
}
