﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.ActivityRecording.ValueObjects;
using Matchnet.ActivityRecording.ValueObjects.ServiceDefinitions;

namespace Matchnet.ActivityRecording.ServiceAdapters
{
    public class ActivityRecordingSA : SABase, IActivityRecordingSA
    {
        // good ole' singleton instance
        public static readonly ActivityRecordingSA Instance = new ActivityRecordingSA();

        private ActivityRecordingSA()
        { }

        public void RecordActivity(int memberID, int targetMemberID, int siteID, Matchnet.ActivityRecording.ValueObjects.Types.ActionType actionType,
            Matchnet.ActivityRecording.ValueObjects.Types.CallingSystem callingSystem, string activityCaption)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).RecordActivity(memberID, targetMemberID, siteID, (int)actionType, (int)callingSystem, activityCaption);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("RecordActivity() error (uri: {0}).  MemberID: {1} ActionTypeID: {2} CallingSystemID {3}", uri, memberID, (int)actionType, (int)callingSystem), ex));
            }
        }

        public void LogApiRequest(string originIP, string clientIP, string url, int httpStatusCode, int httpSubStatusCode, string method, int appID, int memberID, long totalEllapsedRequestTime, int brandID, string host, string machineName, string actionName, string formDataXML, string requestBodyXML)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).LogApiRequest(originIP, clientIP, url, httpStatusCode, httpSubStatusCode, method, appID, memberID, totalEllapsedRequestTime, brandID, host, machineName, actionName, formDataXML, requestBodyXML);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("LogApiRequest() error (uri: {0}).", uri), ex));
            }            
        }


        public void RecordRegistrationStart(string regSessionID, string regFormFactor, int regApplication, 
            int siteID, int scenarioID, int ipAddress, DateTime insertDate)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).RecordRegistrationStart(regSessionID, regFormFactor, regApplication, siteID, scenarioID, ipAddress, insertDate);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("RecordRegistrationStart() error (uri: {0}).  RegSessionID: {1}", uri, regSessionID), ex));
            }
        }

        public void RecordRegistrationComplete(string regSessionID, int memberID, DateTime updateDate)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).RecordRegistrationComplete(regSessionID, memberID, updateDate);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("RecordRegistrationStart() error (uri: {0}).  RegSessionID: {1} MemberID: {1}", uri, regSessionID, memberID.ToString()), ex));
            }
        }

        public void RecordRegistrationStep(string regSessionID, int stepID, Dictionary<string, object> stepDetails, DateTime insertDate)
        {
            string uri = string.Empty;
            try
            {
                if(stepDetails == null || stepDetails.Count == 0)
                {
                    throw new ArgumentException("StepDetails must be supplied");
                }
                
                var stepDetailsXML = ReadStepDetailsIntoXML(stepID, stepDetails);
                
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).RecordRegistrationStep(regSessionID, stepID, stepDetailsXML, insertDate);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("RecordRegistrationStart() error (uri: {0}).  RegSessionID: {1} StepID: {1}", uri, regSessionID, stepID.ToString()), ex));
            }
        }

        private string ReadStepDetailsIntoXML(int stepID, Dictionary<string, object> stepDetails)
        {
            var xml = new StringBuilder();
            xml.Append(string.Format("<StepDetails><StepID>{0}</StepID><Details>", stepID.ToString()));

            foreach(var key in stepDetails.Keys)
            {
                object value = stepDetails[key] ?? "null";
                xml.Append(string.Format("<Detail><Field>{0}</Field><Value>{1}</Value></Detail>", key,
                                         SecurityElement.Escape(value.ToString())));
            }

            xml.Append("</Details></StepDetails>");

            return xml.ToString();
        }

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ACTIVITYRECORDINGSVC_SA_CONNECTION_LIMIT"));
        }

        private IActivityRecordingService getService(string uri)
        {
            try
            {
                return (IActivityRecordingService)Activator.GetObject(typeof(IActivityRecordingService), uri);
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot activate remote service manager at " + uri, ex);
            }
        }

        private string GetServiceManagerUri(string serviceManagerName)
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(serviceManagerName);
                string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ACTIVITYRECORDINGSVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    uri = "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot get configuration settings for remote service manager.", ex);
            }
        }

    }
}
