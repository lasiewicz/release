﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using Matchnet.ActivityRecording.ValueObjects.ServiceDefinitions;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.ActivityRecording.BusinessLogic;
using Matchnet.ActivityRecording.ValueObjects;

namespace Matchnet.ActivityRecording.ServiceManagers
{
    public class ActivityRecordingSM : MarshalByRefObject, IActivityRecordingService, IServiceManager, IBackgroundProcessor, IDisposable
    {
        private HydraWriter m_hydraWriter;
        private PerformanceCounter m_perfAllHitCount;

        public ActivityRecordingSM()
        {
            initPerfCounters();
            m_hydraWriter = new HydraWriter(new string[] { "mnActivityRecording", "mnRegFlow", "mnMonitoring"});

            System.Diagnostics.Trace.WriteLine("ActivityRecordingSM contstructor completed.");
        }

        /// <summary>
        /// keep the object alive indefinitely
        /// </summary>
        /// <returns></returns>
        public override object InitializeLifetimeService()
        {
            return null;
        }

        #region IActivityRecordingService Members

        public void LogApiRequest(string originIP, string clientIP, string url, int httpStatusCode, int httpSubStatusCode, string method, int appID, int memberID, long totalEllapsedRequestTime, int brandID, string host, string machineName, string actionName, string formDataXML, string requestBodyXML)
        {
            MiscUtils.FireAndForget(o =>
                                    ActivityRecordingBL.Instance.LogApiRequest(originIP, clientIP, url, httpStatusCode,
                                                                               httpSubStatusCode, method, appID,
                                                                               memberID, totalEllapsedRequestTime, brandID, host, machineName, actionName, formDataXML, requestBodyXML),
                                    ServiceConstants.SERVICE_MANAGER_NAME, "Could not log api request!");
        }

        public void RecordActivity(int memberID, int targetMemberID, int siteID, int actionTypeID, int callingSystemID, string activityCaption)
        {
            try
            {
                ActivityRecordingBL.Instance.RecordActivity(memberID, targetMemberID, siteID, actionTypeID, callingSystemID, activityCaption);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "RecordActivity() error. ", ex);
            }
        }   

        public void RecordRegistrationStart(string regSessionID, string regFormFactor, int regApplication, 
            int siteID, int scenarioID, int ipAddress, DateTime insertDate)
        {
            try
            {
                ActivityRecordingBL.Instance.RecordRegistrationStart(regSessionID, regFormFactor, regApplication, siteID, scenarioID, ipAddress, insertDate);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "RecordActivity() error. ", ex);
            }
        }

        public void RecordRegistrationComplete(string regSessionID, int memberID, DateTime updateDate)
        {
            try
            {
                ActivityRecordingBL.Instance.RecordRegistrationComplete(regSessionID, memberID, updateDate);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "RecordActivity() error. ", ex);
            }
        }

        public void RecordRegistrationStep(string regSessionID, int stepID, string stepDetails, DateTime insertDate)
        {
            try
            {
                ActivityRecordingBL.Instance.RecordRegistrationStep(regSessionID, stepID, stepDetails, insertDate);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "RecordActivity() error. ", ex);
            }
        }

        #endregion

        #region IServiceManager Members

        public void PrePopulateCache()
        {
            // no implementation
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            
        }

        #endregion

        #region IBackgroundProcessor Members

        public void Start()
        {
            System.Diagnostics.Trace.WriteLine("ActivityRecordingSM start.");
            m_hydraWriter.Start();
        }

        public void Stop()
        {
            System.Diagnostics.Trace.WriteLine("ActivityRecordingSM stop.");

            if (m_hydraWriter != null)
            {
                m_hydraWriter.Stop();
            }

        }

        #endregion

        #region Instrumentation
        private static CounterCreationData[] GetCounters()
        {
            return new CounterCreationData[] { new CounterCreationData(ServiceConstants.PERF_TOTALCOUNT_NAME, "Counter fo all incoming requests.", PerformanceCounterType.NumberOfItems64) };
        }

        public static void PerfCounterInstall()
        {
            try
            {
                System.Diagnostics.Trace.WriteLine("ActivityRecordingSM,Start PerfCounterInstall");

                CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
                ccdc.AddRange(GetCounters());

                PerformanceCounterCategory.Create(ServiceConstants.SERVICE_NAME, ServiceConstants.SERVICE_NAME,
                    PerformanceCounterCategoryType.SingleInstance, ccdc);

                System.Diagnostics.Trace.WriteLine("ActivityRecordingSM,Finish PerfCounterInstall");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("ActivityRecordingSM Exception in PerfCounterInstall.");
                new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "PerfCounterInstall() error. ", ex);
            }
        }
                
        public static void PerfCounterUninstall()
        {
            PerformanceCounterCategory.Delete(ServiceConstants.SERVICE_NAME);
        }	

        private void initPerfCounters()
        {
            try
            {
                System.Diagnostics.Trace.WriteLine("ActivityRecordingSM, Start initPerfCounters");

                m_perfAllHitCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, ServiceConstants.PERF_TOTALCOUNT_NAME, false);
                resetPerfCounters();

                System.Diagnostics.Trace.WriteLine("ActivityRecordingSM, Finish initPerfCounters");
            }   
            catch
            {
                System.Diagnostics.Trace.WriteLine("ActivityRecordingSM exception in initPerfCounters()");
            }
        }

        private void resetPerfCounters()
        {
            m_perfAllHitCount.RawValue = 0;            
            System.Diagnostics.Trace.WriteLine("Perf counters reseted.");
        }

        #endregion
    }
}
