﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ActivityRecording.ValueObjects
{
    public class ServiceConstants
    {
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_CONSTANT = "ACTIVITYRECORDING_SVC";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_NAME = "Matchnet.ActivityRecording.Service";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_MANAGER_NAME = "ActivityRecordingSM";
        /// <summary>
        /// Performance counter name for total hits.
        /// </summary>
        public const string PERF_TOTALCOUNT_NAME = "Total hit count";       
    }
}
