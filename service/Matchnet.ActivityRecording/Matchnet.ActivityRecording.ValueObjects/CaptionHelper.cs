﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.ActivityRecording.ValueObjects.Types;

namespace Matchnet.ActivityRecording.ValueObjects
{
    public class CaptionHelper
    {
        public string GetCaption(ActionType actionType, Dictionary<string, string> parameters)
        {
            var captionBuilder = new StringBuilder();
            var captionRoot = GetCaptionRoot(actionType);

            captionBuilder.Append("<");
            captionBuilder.Append(captionRoot);
            captionBuilder.Append(">");
            BuildCaptionBody(parameters, captionBuilder);
            captionBuilder.Append("</");
            captionBuilder.Append(captionRoot);
            captionBuilder.Append(">");

            return captionBuilder.ToString();
        }

        private void BuildCaptionBody(Dictionary<string, string> parameters, StringBuilder stringBuilder)
        {
            foreach(var element in parameters)
            {
                stringBuilder.Append("<");
                stringBuilder.Append(element.Key);
                stringBuilder.Append(">");
                stringBuilder.Append(element.Value);
                stringBuilder.Append("</");
                stringBuilder.Append(element.Key);
                stringBuilder.Append(">");
            }
        }

        private string GetCaptionRoot(ActionType actionType)
        {
            switch(actionType)
            {
                case ActionType.LoginArrivedAtClientSystem:
                    return CaptionRootConstants.LOGINARRIVEDATCLIENTSYSTEM;
                case ActionType.LoginArrivedAtSUA:
                    return CaptionRootConstants.LOGINARRIVEDATSUA;
                case ActionType.LoginAttemptedLogin:
                    return CaptionRootConstants.LOGINATTEMPTEDLOGIN;
                case ActionType.LoginFraudCheck:
                    return CaptionRootConstants.LOGINFRAUDCHECK;
                case ActionType.LoginAuthentication:
                    return CaptionRootConstants.LOGINAUTHENTICATION;
                case ActionType.LoginSentBackToClient:
                    return CaptionRootConstants.LOGINSENTBACKTOCLIENT;
                case ActionType.LoginArrivedAtPasswordResetRequest:
                    return CaptionRootConstants.LOGINARRIVEDATPASSWORDRESETREQUEST;
                case ActionType.LoginPasswordResetRequestSubmitted:
                    return CaptionRootConstants.LOGINPASSWORDRESETREQUESTSUBMITTED;
                case ActionType.LoginArrivedAtPasswordReset:
                    return CaptionRootConstants.LOGINARRIVEDATPASSWORDRESET;
                case ActionType.LoginAttemptedPasswordReset:
                    return CaptionRootConstants.LOGINATTEMPTEDLOGIN;
                case ActionType.IOSInAppPurchaseReceipt:
                    return CaptionRootConstants.IOSINAPPPURCHASERECEIPT;
                case ActionType.IOSInAppPurchaseStatus:
                    return CaptionRootConstants.IOSINAPPPURCHASESTATUS;
                case ActionType.AndroidInAppBillingReceipt:
                    return CaptionRootConstants.ANDROIDINAPPBILLINGRECEIPT;
                case ActionType.AndroidInAppBillingStatus:
                    return CaptionRootConstants.ANDROIDINAPPBILLINGSTATUS;
                case ActionType.AppInstall:
                    return CaptionRootConstants.APPINSTALL;
                case ActionType.InApplicationPurchaseSuccess:
                    return CaptionRootConstants.INAPPPURCHASESUCCESS;
                default:
                    return string.Empty;

            }
        }
    }
}
