using System;

namespace Matchnet.ActivityRecording.ValueObjects.ServiceDefinitions
{
    public interface  IActivityRecordingService
    {
        void LogApiRequest(string originIP, string clientIP, string url, int httpStatusCode, int httpSubStatusCode, string method, int appID, int memberID, long totalEllapsedRequestTime, int brandID, string host, string machineName, string actionName, string formDataXML, string requestBodyXML);
        void RecordActivity(int memberID, int targetMemberID, int siteID, int actionTypeID, int callingSystemID, string activityCaption);
        void RecordRegistrationStart(string regSessionID, string regFormFactor, int regApplication, int siteID, int scenarioID, int ipAddress, DateTime insertDate);
        void RecordRegistrationComplete(string regSessionID, int memberID, DateTime updateDate);
        void RecordRegistrationStep(string regSessionID, int stepID, string stepDetails, DateTime insertDate);
    }
}
