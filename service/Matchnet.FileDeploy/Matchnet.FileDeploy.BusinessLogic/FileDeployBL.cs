using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;

using ICSharpCode.SharpZipLib.Zip;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.Servers;
using Matchnet.FileDeploy.ValueObjects;

namespace Matchnet.FileDeploy.BusinessLogic
{
    public class FileDeployBL
    {
        #region Constructor
        public static FileDeployBL Instance = new FileDeployBL();

        private FileDeployBL()
        {
        }
        #endregion

        #region Public Methods

        public FileDeploymentResultCollection DeployFiles(FileCollection fileCollection, ServerType serverType)
        {
            System.Diagnostics.Trace.WriteLine("Begin DeployFiles");
            //TODO: Get server list from config service
            string[] servers = getServerList(serverType);

            FileDeploymentResultCollection results = new FileDeploymentResultCollection();
            foreach (File file in fileCollection)
            {
                results.Add(new FileDeploymentResult(file.RelativePath));
            }

            foreach (string serverName in servers)
            {
                bool backupSucceeded = true;

                try
                {
                    backupFiles(fileCollection, serverName);
                }
                catch (Exception ex)
                {
                    //If backup fails, add a failure result for this server to each file 
                    backupSucceeded = false;

                    string errorText = "Error backing up files on server: " + ex.Message;
                    DeploymentResult deploymentResult = new DeploymentResult(serverName, DeploymentResultStatus.Failure, errorText);
                    foreach (File file in fileCollection)
                    {
                        results[file.RelativePath].Add(deploymentResult);
                    }
                }

                if (backupSucceeded)
                {
                    foreach (File file in fileCollection)
                    {
                        DeploymentResult deploymentResult = copyFile(file, serverName);
                        results[file.RelativePath].Add(deploymentResult);

                        if (deploymentResult.Status == DeploymentResultStatus.Success)
                        {
                            try
                            {
                                invalidateCache(file, serverName);
                            }
                            catch (Exception ex)
                            {
                                deploymentResult.Status = DeploymentResultStatus.Failure;
                                deploymentResult.ErrorText = ex.ToString();
                            }
                        }
                    }
                }
            }
            System.Diagnostics.Trace.WriteLine("End DeployFiles");
            return results;
        }

        #endregion

        #region Private Methods

        private string[] getServerList(ServerType serverType)
        {
            string settingName = string.Empty;

            if ((serverType & ServerType.Production) == ServerType.Production)
            {
                settingName = "ServerListProduction";
            }
            else if ((serverType & ServerType.ContentStaging) == ServerType.ContentStaging)
            {
                settingName = "ServerListContentStaging";
            }
            else
            {
                settingName = "ServerListStaging";
            }
            

            return System.Configuration.ConfigurationManager.AppSettings[settingName].Split(new char[] { ',' });
        }

        private void backupFiles(FileCollection fileCollection, string serverName)
        {
            System.Diagnostics.Trace.WriteLine("Begin backupFiles");
            ZipOutputStream zipOutputStream = null;

            try
            {
                string backupDirectory = getAbsolutePath(serverName, BackupPath, string.Empty);
                if (!System.IO.Directory.Exists(backupDirectory))
                {
                    System.IO.Directory.CreateDirectory(backupDirectory);
                }
                string zipFileName = getAbsolutePath(serverName, BackupPath, @"\" + DateTime.Now.ToString("yyyy-MM-dd hhmmss") + ".zip");
                System.IO.FileStream zipFileStream = new System.IO.FileStream(zipFileName, System.IO.FileMode.CreateNew);
                zipOutputStream = new ZipOutputStream(zipFileStream);

                foreach (File file in fileCollection)
                {
                    //Read the file into a byte array
                    System.IO.FileStream fileStream = null;
                    byte[] bytes = null;
                    try
                    {
                        string filePath = getAbsolutePath(serverName, DestinationPath, file.RelativePath);

                        if (System.IO.File.Exists(filePath))
                        {
                            fileStream = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                            bytes = new byte[fileStream.Length];
                            fileStream.Read(bytes, 0, bytes.Length);
                        }
                        else if (file.FileType != FileType.Image)
                        {
                            throw new ApplicationException("Unable to backup file " +  filePath + " because the file does not exist.");
                        }
                    }
                    finally
                    {
                        if (fileStream != null)
                        {
                            fileStream.Close();
                        }
                    }

                    if (bytes != null)
                    {
                        //Add to the zip file
                        ZipEntry entry = new ZipEntry(ZipEntry.CleanName(file.RelativePath));
                        zipOutputStream.PutNextEntry(entry);
                        zipOutputStream.Write(bytes, 0, bytes.Length);
                    }
                }

                zipOutputStream.Finish();
            }
            finally
            {
                if (zipOutputStream != null)
                {
                    zipOutputStream.Close();
                }
                System.Diagnostics.Trace.WriteLine("End backupFiles");
            }
        }

        private DeploymentResult copyFile(File file, string serverName)
        {
            System.Diagnostics.Trace.WriteLine("Begin copyFile");
            DeploymentResultStatus status = DeploymentResultStatus.Success;
            string errorText = string.Empty;

            try
            {
                string directoryPath = getAbsolutePath(serverName, DestinationPath, string.Empty);
                if (!System.IO.Directory.Exists(directoryPath))
                {
                    System.IO.Directory.CreateDirectory(directoryPath);
                }

                file.Write(getAbsolutePath(serverName, DestinationPath, file.RelativePath));
            }
            catch (Exception ex)
            {
                status = DeploymentResultStatus.Failure;
                errorText = "Error writing file to server: " + ex.Message;
            }
            System.Diagnostics.Trace.WriteLine("End copyFile");
            return new DeploymentResult(serverName, status, errorText);
        }

        private void invalidateCache(File file, string serverName)
        {
            System.Diagnostics.Trace.WriteLine("Begin invalidateCache");
            string relativeUrl = string.Empty;

            switch (file.FileType)
            {
                case FileType.Resource:
                    relativeUrl = "/CacheManager.aspx?ResourceFileName=" + file.Name + "&ResourceFileFullPath=" + DestinationPath + file.RelativePath;
                    break;
                case FileType.Image:
                    relativeUrl = "/CacheManager.aspx?ImageFileName=" + file.Name;
                    break;
            }

            asyncRequest(serverName, relativeUrl);
            System.Diagnostics.Trace.WriteLine("End invalidateCache");
        }

        private void asyncRequest(string serverName, string relativeUrl)
        {
            //Also get https
            if (!serverName.EndsWith("443"))
            {
                asyncRequest(serverName + ":443", relativeUrl);
            }

            string requestString = "http://" + serverName + relativeUrl;
            WebRequest request = WebRequest.Create(requestString);
            request.Timeout = 5000;
            request.BeginGetResponse(new AsyncCallback(handleResxHttpResponse), request);
        }

        private void handleResxHttpResponse(IAsyncResult result)
        {
            StringBuilder sbOutput = new StringBuilder();
            EventLogEntryType entryType = EventLogEntryType.Information;

            try
            {
                WebRequest request = (WebRequest)result.AsyncState;
                sbOutput.Append("Expiring cache via URL " + request.RequestUri + "." + Environment.NewLine);

                WebResponse response = request.EndGetResponse(result);

                System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream());
                sbOutput.Append(sr.ReadToEnd());
            }
            catch (Exception ex)
            {
                sbOutput.Append(ex.ToString());
                entryType = EventLogEntryType.Error;
            }

            EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, sbOutput.ToString(), entryType);
        }

        #endregion

        #region File path determination

        private string getAbsolutePath(string serverName, string pathRoot, string relativePath)
        {
            return @"\\" + serverName + @"\" + pathRoot.Replace(":", "$") + relativePath;
        }

        private string DestinationPath
        {
            get
            {
                //return Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("FILEDEPLOYSVC_DESTINATION_PATH");
                return cleanPath(System.Configuration.ConfigurationManager.AppSettings["DestinationPath"]);
            }
        }

        private string BackupPath
        {
            get
            {
                //return Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("FILEDEPLOYSVC_BACKUP_PATH");
                return cleanPath(System.Configuration.ConfigurationManager.AppSettings["BackupPath"]);
            }
        }

        private string cleanPath(string dirtyPath)
        {
            return dirtyPath.TrimEnd('\\');
        }

        #endregion
    }
}
