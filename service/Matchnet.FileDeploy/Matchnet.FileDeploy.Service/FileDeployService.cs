using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.ServiceProcess;

using Matchnet.Exceptions;
using Matchnet.FileDeploy.ServiceManagers;
using Matchnet.FileDeploy.ValueObjects;


namespace Matchnet.FileDeploy.Service
{
    public class FileDeployService : ServiceBase
    {
        private System.ComponentModel.Container components = null;
        private FileDeploySM _fileDeploySM = null;
        private Int32 _servicePort;

        public FileDeployService()
        {
            _servicePort = Conversion.CInt(System.Configuration.ConfigurationManager.AppSettings["ServicePort"]);

            InitializeComponent();
        }

        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = ServiceConstants.SERVICE_NAME;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        protected override void OnStart(string[] args)
        {
            BinaryServerFormatterSinkProvider serverSink = new BinaryServerFormatterSinkProvider();
            serverSink.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;
            System.Runtime.Remoting.Channels.Tcp.TcpServerChannel channel = new System.Runtime.Remoting.Channels.Tcp.TcpServerChannel("", _servicePort, serverSink);

            try
            {
                ChannelServices.UnregisterChannel(channel);
            }
            catch
            {
                // Do nothing
            }

            try
            {
                ChannelServices.RegisterChannel(channel);
            }
            catch (Exception ex)
            {
                ServiceBoundaryException sbEx = new ServiceBoundaryException(this.ServiceName, "Error occurred when attempting to register channel on port " + _servicePort.ToString() + ".  " + ex.ToString(), ex);
            }

            registerServiceManagers();
            base.OnStart(args);
        }

        private void registerServiceManagers()
        {
            try
            {
                _fileDeploySM = new FileDeploySM();
                registerServiceManager(_fileDeploySM);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occurred when starting the Windows Service, see details:" + ex.Message);
            }
        }

        private void registerServiceManager(MarshalByRefObject serviceManager)
        {
            Type type = serviceManager.GetType();

            try
            {
                System.Runtime.Remoting.RemotingServices.Marshal(serviceManager, type.Name + ".rem");
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(this.ServiceName, "System could not register " + type.Name + " as a WellKnownObject (on Administration Port: " + _servicePort.ToString() + ").  Please view inner exception.", ex);
            }
        }
    }
}
