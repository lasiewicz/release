using System;
using System.Collections.Generic;
using System.Text;

using Matchnet.Configuration.ValueObjects.Servers;
using Matchnet.Exceptions;
using Matchnet.FileDeploy.ValueObjects;
using Matchnet.RemotingClient;

namespace Matchnet.FileDeploy.ServiceAdapters
{
    public class FileDeploySA : SABase
    {
        public const string SERVICE_MANAGER_NAME = "FileDeploySM";
        public static FileDeploySA Instance = new FileDeploySA();

        private FileDeploySA()
        {
        }

        public FileDeploymentResultCollection DeployFiles(FileCollection fileCollection, ServerType serverType, string hostName)
        {
            string uri = string.Empty;

            try
            {
                uri = getServiceManagerUri(hostName);

                base.Checkout(uri);
                try
                {
                    return getService(uri).DeployFiles(fileCollection, serverType);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot deploy files (uri: " + uri + ")", ex));
            }
        }

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("FILEDEPLOYSVC_SA_CONNECTION_LIMIT"));
        }

        private string getServiceManagerUri()
        {
            return getServiceManagerUri(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("FILEDEPLOYSVC_SA_HOST_OVERRIDE"));
        }

        private string getServiceManagerUri(string overrideHostName)
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(SERVICE_MANAGER_NAME);

                if (overrideHostName.Trim().Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

        private IFileDeployService getService(string uri)
        {
            try
            {
                return (IFileDeployService) Activator.GetObject(typeof(IFileDeployService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }
    }
}
