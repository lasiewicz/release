using System;
using System.IO;
using System.Runtime.Serialization;

namespace Matchnet.FileDeploy.ValueObjects
{
    [Serializable]
    public class File //: ISerializable
    {
        private string _relativePath;
        private Byte[] _bytes;
        private FileType _fileType;

        public File(string absolutePath, string relativePath, FileType fileType)
        {
            _relativePath = relativePath;
            _bytes = loadFile(absolutePath);
            _fileType = fileType;
        }


        private Byte[] loadFile(string filePath)
        {
            FileStream fs = null;
            try
            {
                fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                Byte[] bytes = new Byte[fs.Length];
                fs.Read(bytes, 0, (Int32)fs.Length);
                return bytes;
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                }
            }
        }

        public string Name
        {
            get
            {
                return _relativePath.Substring(_relativePath.LastIndexOf('\\') + 1);
            }
        }

        public string RelativePath
        {
            get
            {
                return _relativePath;
            }
        }

        public FileType FileType
        {
            get
            {
                return _fileType;
            }
        }

        public void Write(string destinationPath)
        {
            FileStream fs = null;
            try
            {
                string destinationFolder = destinationPath.Substring(0, destinationPath.LastIndexOf(Path.DirectorySeparatorChar));
                if (!Directory.Exists(destinationFolder))
                {
                    Directory.CreateDirectory(destinationFolder);
                }

                fs = new FileStream(destinationPath, FileMode.Create);
                fs.Write(_bytes, 0, _bytes.Length);
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                }
            }
        }

        /*#region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            
        }

        #endregion*/
    }
}
