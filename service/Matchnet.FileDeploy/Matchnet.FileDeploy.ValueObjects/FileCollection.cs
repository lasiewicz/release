using System;
using System.Collections;
//using System.Collections.Generic;
using System.Text;

namespace Matchnet.FileDeploy.ValueObjects
{
    [Serializable]
    public class FileCollection : IEnumerable//<File>
    {
        //private Dictionary<string, File> _files;
        private Hashtable _files;

        public FileCollection()
        {
            //_files = new Dictionary<string, File>();
            _files = new Hashtable();
        }

        public void Add(File file)
        {
            _files.Add(file.RelativePath, file);
        }

        /*#region IEnumerable<File> Members

        IEnumerator<File> IEnumerable<File>.GetEnumerator()
        {
            return _files.Values.GetEnumerator();
        }

        #endregion*/

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _files.Values.GetEnumerator();
        }

        #endregion
    }
}
