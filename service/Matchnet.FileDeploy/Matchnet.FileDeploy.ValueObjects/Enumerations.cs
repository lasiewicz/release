using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.FileDeploy.ValueObjects
{
    public enum FileType
    {
        Resource,
        Image
    }

    public enum DeploymentResultStatus
    {
        Success,
        Failure
    }
}
