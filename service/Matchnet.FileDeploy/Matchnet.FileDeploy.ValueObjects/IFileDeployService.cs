using System;
using System.Collections.Generic;
using System.Text;

using Matchnet.Configuration.ValueObjects.Servers;

namespace Matchnet.FileDeploy.ValueObjects
{
    public interface IFileDeployService
    {
        FileDeploymentResultCollection DeployFiles(FileCollection fileCollection, ServerType serverType);
    }
}
