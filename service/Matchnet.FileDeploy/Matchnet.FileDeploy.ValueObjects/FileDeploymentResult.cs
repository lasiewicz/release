using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.FileDeploy.ValueObjects
{
    [Serializable]
    public class FileDeploymentResult : IEnumerable<DeploymentResult>
    {
        Dictionary<string, DeploymentResult> _deploymentResults;
        private string _fileName;

        public FileDeploymentResult(string fileName)
        {
            _deploymentResults = new Dictionary<string,DeploymentResult>();
            _fileName = fileName;
        }

        public void Add(DeploymentResult deploymentResult)
        {
            _deploymentResults.Add(deploymentResult.ServerName, deploymentResult);
        }

        public void Append(FileDeploymentResult fileDeploymentResult)
        {
            foreach (DeploymentResult result in fileDeploymentResult)
            {
                Add(result);
            }
        }


        public string FileName
        {
            get
            {
                return _fileName;
            }
        }

        #region IEnumerable<DeploymentResult> Members

        IEnumerator<DeploymentResult> IEnumerable<DeploymentResult>.GetEnumerator()
        {
            return _deploymentResults.Values.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _deploymentResults.Values.GetEnumerator();
        }

        #endregion
    }
}
