using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.FileDeploy.ValueObjects
{
    [Serializable]
    public class DeploymentResult
    {
        string _serverName;
        DeploymentResultStatus _status;
        string _errorText;

        public DeploymentResult(string serverName, DeploymentResultStatus status, string errorText)
        {
            _serverName = serverName;
            _status = status;
            _errorText = errorText;
        }

        public string ServerName
        {
            get
            {
                return _serverName;
            }
            set
            {
                _serverName = value;
            }
        }

        public DeploymentResultStatus Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public string ErrorText
        {
            get
            {
                return _errorText;
            }
            set
            {
                _errorText = value;
            }
        }

        public override string ToString()
        {
            return "Server: " + ServerName + ", Status: " + Status + (Status == DeploymentResultStatus.Failure ? ", Error: " + ErrorText : string.Empty);
        }
    }
}
