using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.FileDeploy.ValueObjects
{
    [Serializable]
    public class FileDeploymentResultCollection : IEnumerable, IEnumerable<FileDeploymentResult>
    {
        Dictionary<string, FileDeploymentResult> _fileDeploymentResults;

        public FileDeploymentResultCollection()
        {
            _fileDeploymentResults = new Dictionary<string, FileDeploymentResult>();
        }

        public void Add(FileDeploymentResult fileDeploymentResult)
        {
            _fileDeploymentResults.Add(fileDeploymentResult.FileName, fileDeploymentResult);
        }

        public void Append(FileDeploymentResultCollection fileDeploymentResultCollection)
        {
            foreach (FileDeploymentResult fileDeploymentResult in fileDeploymentResultCollection)
            {
                if (!_fileDeploymentResults.ContainsKey(fileDeploymentResult.FileName))
                {
                    Add(fileDeploymentResult);
                }
                else
                {
                    FileDeploymentResult existingResult = _fileDeploymentResults[fileDeploymentResult.FileName];
                    existingResult.Append(fileDeploymentResult);
                }
            }
        }

        public FileDeploymentResult this[string fileName]
        {
            get
            {
                return _fileDeploymentResults[fileName];
            }
            set
            {
                _fileDeploymentResults[fileName] = value;
            }
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return _fileDeploymentResults.Values.GetEnumerator();
        }

        #endregion

        #region IEnumerable<FileDeploymentResult> Members

        IEnumerator<FileDeploymentResult> IEnumerable<FileDeploymentResult>.GetEnumerator()
        {
            return _fileDeploymentResults.Values.GetEnumerator();
        }

        #endregion
    }
}
