using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Matchnet.Configuration.ValueObjects.Servers;
using Matchnet.FileDeploy.ServiceAdapters;
using Matchnet.FileDeploy.ValueObjects;

namespace Matchnet.FileDeploy.Harness
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FileCollection fileCollection = new FileCollection();
            fileCollection.Add(new File(@"C:\temp\src\foo.txt", @"\foo.txt", FileType.Resource));
            FileDeploymentResultCollection results = FileDeploySA.Instance.DeployFiles(fileCollection, ServerType.Web | ServerType.Staging, Environment.MachineName);

            foreach (FileDeploymentResult fileResult in results)
            {
                foreach (DeploymentResult result in fileResult)
                {
                    Console.WriteLine(result.ToString());
                }
            }
        }
    }
}