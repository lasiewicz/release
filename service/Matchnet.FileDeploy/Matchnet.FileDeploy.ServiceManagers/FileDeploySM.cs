using System;
using System.Collections.Generic;
using System.Text;

using Matchnet.Configuration.ValueObjects.Servers;
using Matchnet.FileDeploy.BusinessLogic;
using Matchnet.FileDeploy.ValueObjects;

namespace Matchnet.FileDeploy.ServiceManagers
{
    public class FileDeploySM : MarshalByRefObject, IServiceManager, IFileDeployService, IDisposable
    {
        #region Private Variables
        private const string SERVICE_MANAGER_NAME = "FileDeploySM";
        #endregion

        #region Constructors
        public FileDeploySM()
        {
        }

        #endregion

        #region IFileDeployService Implementation
        public FileDeploymentResultCollection DeployFiles(FileCollection fileCollection, ServerType serverType)
        {
            return FileDeployBL.Instance.DeployFiles(fileCollection, serverType);
        }
        #endregion


        public override object InitializeLifetimeService()
        {
            return null;
        }


        public void Dispose()
        {
        }


        public void PrePopulateCache()
        {
            // no implementation at this time
        }
    }
}
