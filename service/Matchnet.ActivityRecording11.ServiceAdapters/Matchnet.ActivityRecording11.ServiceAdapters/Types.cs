using System;

namespace Matchnet.ActivityRecording11.ServiceAdapters
{
	public enum ActionType : int
	{
		None = 0,
		YNM = 1,
		ViewProfile =2,
		Favorites = 3,
		Email = 4,
		IM = 5,
		TeaseFlirt = 6,
		Matches = 7,
		Ignore = 8,
		VoiceProfile = 9,
		VoiceMail = 10,
		ReportAbuse = 11,
		eCard = 12,
		ReadMessage = 13,
		DeleteMessage = 14,
		ReplyToMessage = 15,
		SendAMessage = 16,
		Login = 17,
		Logout  = 18,
		StopSMS = 19,
		AnswerQuestion = 20,
		EditAnswer = 21,
		RemoveAnswer = 22,
		MailQuestion = 23,
		SendPhotoApproval = 24,
		SendToFriend = 25,
		SendNewMailAlert = 26,
		SendMutualMail = 27,
		SendPhotoRejection = 28,
		SendOptOutNotification = 29,
		SendHotListAlert = 30,
		SendSubscriptionConfirmation = 31,
		SendActivationLetter = 32,
		SendForgotPassword = 33,
		SendEmailChangeConfirmation = 34,
		SendMessageBoardInstantNotification = 35,
		SendMessageBoardDailyUpdates = 36,
		SendEcardToFriend = 37,
		SendRenewalFailureNotification = 38,
		SendMatchMail = 39,
		SendClickMail = 40,
		SendEcardToMember = 41,
		SendPaymentProfileNotification = 42,
		SendMatchMeterInvitation = 43,
		SendMatchMeterCongrats = 44,
		SendNewMemberMail = 45,
		SendPhotoCaptionRejectedMail = 46,
		SendMemberColorCode =47,
		SendMemberColorCodeQuizInvite = 48,
		SendColorCodeQuizConfirmation = 49,
		SendMemberQuestion = 50,
		SendOneOffPurchaseConfirmation = 51,
		SendJDateMobileRegistrationConfirmation = 52
	}

	public enum CallingSystem : int
	{
		Messmo = 1,
		QuestionAnswer = 2,
		ExternalMail = 3,
		BulkMail = 4
	}
}
