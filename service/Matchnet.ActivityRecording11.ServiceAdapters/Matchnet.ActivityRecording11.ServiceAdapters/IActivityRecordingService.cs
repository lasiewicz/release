using System;

namespace Matchnet.ActivityRecording.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public interface IActivityRecordingService
	{
		void RecordActivity(int memberID, int targetMemberID, int siteID, int actionTypeID, int callingSystemID, string activityCaption);
	}
}
