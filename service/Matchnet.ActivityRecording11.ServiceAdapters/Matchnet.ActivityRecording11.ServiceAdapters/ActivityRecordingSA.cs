using System;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.ActivityRecording.ValueObjects.ServiceDefinitions;

namespace Matchnet.ActivityRecording11.ServiceAdapters
{
	/// <summary>
	/// Summary description for ActivityRecordingSA.
	/// </summary>
	public class ActivityRecordingSA : SABase
	{
		public static readonly ActivityRecordingSA Instance = new ActivityRecordingSA();

		private ActivityRecordingSA()
		{ }

		public void RecordActivity(int memberID, int targetMemberID, int siteID, ActionType actionType,
			CallingSystem callingSystem, string activityCaption)
		{
			string uri = string.Empty;
			try
			{
				uri = GetServiceManagerUri("ActivityRecordingSM");
				base.Checkout(uri);
				try
				{
					getService(uri).RecordActivity(memberID, targetMemberID, siteID, (int)actionType, (int)callingSystem, activityCaption);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw (new SAException(string.Format("RecordActivity() error (uri: {0}).  MemberID: {1} ActionTypeID: {2} CallingSystemID {3}", uri, memberID, (int)actionType, (int)callingSystem), ex));
			}
		}

		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ACTIVITYRECORDINGSVC_SA_CONNECTION_LIMIT"));
		}

		private IActivityRecordingService getService(string uri)
		{
			try
			{
				return (IActivityRecordingService)Activator.GetObject(typeof(IActivityRecordingService), uri);
			}
			catch (Exception ex)
			{
				throw new SAException("Cannot activate remote service manager at " + uri, ex);
			}
		}

		private string GetServiceManagerUri(string serviceManagerName)
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition("ACTIVITYRECORDING_SVC", Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(serviceManagerName);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ACTIVITYRECORDINGSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					uri = "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch (Exception ex)
			{
				throw new Exception("Cannot get configuration settings for remote service manager.", ex);
			}
		}
	}
}
