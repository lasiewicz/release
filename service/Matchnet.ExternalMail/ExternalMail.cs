using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Matchnet.ExternalMail.ServiceManagers;
using Matchnet.ExternalMail.ValueObjects;


namespace Matchnet.ExternalMail.Service
{
	public class ExternalMail : Matchnet.RemotingServices.RemotingServiceBase
	{
		private System.ComponentModel.Container components = null;
		private ExternalMailSM _externalMailSM;

		public ExternalMail()
		{
			InitializeComponent();
		}

		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new ExternalMail() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = ServiceConstants.SERVICE_NAME;
			base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}

			if (_externalMailSM != null)
			{
				_externalMailSM.Dispose();
			}

			base.Dispose( disposing );
		}


		protected override void RegisterServiceManagers()
		{
			try
			{
				_externalMailSM = new ExternalMailSM();
				base.RegisterServiceManager(_externalMailSM);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceName, "Error occurred when starting the Windows Service", ex);
			}
		}
	}
}
