<%@ Register TagPrefix="cc" Namespace="Matchnet.ExternalMail.Templates.CommonControls" Assembly="Matchnet.ExternalMail.Templates" %>
<%@ Page language="c#" Codebehind="Site100.aspx.cs" AutoEventWireup="false" Inherits="Matchnet.ExternalMail.Templates.RegConfirmation.Site100" %>
<html>
	<head>
		<title></title>
	</head>
	<body topmargin="0" leftmargin="0" bgcolor="#ffffff">
		<form id="Form1" method="post" runat="server">


<table cellpadding="0" cellspacing="0" border="0" width="500">
	<tr>
		<td><img src="((HEADERIMG))" width="620" height="193"></td>
	</tr>
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="550">
				<tr>
					
          <td valign="top"> <img src="((NICKIMG))" width="21" height="46"></td>
					<td><img src="((TRANSIMG))" height="1" width="70"></td>
					<td><font face="verdana" size="2">

					<table cellpadding="0" cellspacing="0" border="0" width="531"><tr><td width="531" valign="bottom"><font face="verdana" size="2">Dear Member:</font></td></tr></table>
					<br>

					<table cellpadding="0" cellspacing="0" border="0" width="530">
						<tr>
							
                <td> <font size="2" face="Verdana, Arial, Helvetica, sans-serif">Please verify your email address to make sure that you receive all messages from ((PLDESCRIPTION)). </font></p> 
                  <p><a href="((VERIFYLINK))">
                            <center><img src="((VERIFYIMG))" border="0" width="193" height="27"></CENTER></a></p>
						

					 
                  <p><font size="2" face="Verdana, Arial, Helvetica, sans-serif">You 
                    can also go to "verify email" in the bottom menu at <a href="((SITELINK))">((SITELINK))</a> 
                    and copy this code:</font><br>
                   <input type=text size=80 value="((VERIFYCODE))"></p>

					 
                  <p><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Happy 
                    dating!</font></p>
                  <p><font size="2" face="Verdana, Arial, Helvetica, sans-serif">- 
                    The team at ((PLBRANDNAME))</font></p>
                  
					 </td>
						</tr>
					</table>


					
          </td>
			</tr>
		</table>

<br>

		<table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#F3F3F3">
			<tr>
				<td align="center">

					<table width="480">
						<tr>
							
                <td>
                  <p><span style="{font-family: arial; font-size: 10px; line-height: 10px; color: #000000}"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
                    This email is sent to you in connection with your membership 
                    at ((PLBRANDNAME)). To <br>change your mail settings or stop receiving 
                    our email, click here: <br>
                    <a target="_blank" href="((EMAILSETTINGSLINK))">((EMAILSETTINGSLINK)) 
                    </a> </font> </span></p>
                  <span style="{font-family: arial; font-size: 10px; line-height: 10px; color: #000000}"><p><font size="1" face="Verdana, Arial, Helvetica, sans-serif"> 
                    ((PLBRANDNAME)) is owned and operated by: <br>
                    Spark Networks plc | 8383 Wilshire Boulevard, Suite 800 | Beverly 
                    Hills, CA 90211, USA</font><br>
					</span>
							</td>
						</tr>
					</table>

				</td>
			</tr>
		</table>

		</td>
	</tr>
</table>

		</form>
	</body>
</html>
