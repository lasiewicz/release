<%@ Register TagPrefix="cc" Namespace="Matchnet.ExternalMail.Templates.CommonControls" Assembly="Matchnet.ExternalMail.Templates" %>
<%@ Page language="c#" Codebehind="CheckSubConfirmBase.cs" AutoEventWireup="false" Inherits="Matchnet.ExternalMail.Templates.CheckSubConfirm.CheckSubConfirmBase" %>
<html>
	<head>
		<title>Check Subscription Confirmation</title>
	</head>
	<body dir="ltr">
		<form id="Form1" method="post" runat="server">
			
Dear <asp:Label id="lblName" runat="server" />,<br />

<p>
	Thank you for your order 
</p>

<p>
Your transaction at www.spark.com was authorized on date <asp:Label id="lblTransactionDate" runat="server" />. 
For your records, the item(s) purchased include(s): 
<ul>
	<li>
		<asp:Label id="lblPlan" runat="server"/>
	</li>
</ul>
</p>

<p>
Payment for this transaction will be processed electronically, debiting 
the Total amount stated above from the account of, <asp:Label id="lblAccountHolder" runat="server" />, 
on, or soon after, two business days from the date you authorized this 
transaction. You gave your authorization on date <asp:Label id="lblAuthorizationDate" runat="server" /> to electronically 
debit your account when you clicked on the "I Authorize This Transaction" 
button. IMPORTANT: Make sure you void the check <asp:Label id="lblCheckNumber" runat="server" /> submitted for this 
transaction, so as to avoid re-presentment of the same check elsewhere. 
</p>

<p>
Please be advised that this transaction should appear on your next bank 
statement in the electronic debit section (where you normally see your 
ATM transactions). The transaction information should include 
1) the merchant's name, 2) the posting date, 3) the check number you 
entered at the time of the transaction, and 4) the amount of the 
transaction as stated above. 
</p>

<p>
If you have any need to contact us regarding your transaction (including 
to correct any erroneous information or to request a refund), you may do 
so by calling our customer care team (toll free in the USA) at 877-453-3861,
or e-mailing us at <asp:Label id="lblContactEmail" runat="server" />.
</p>

Sincerely,<br />
Spark.com Membership Services<br />
<br />
<p>
Spark.com - Free and premium memberships, detailed
profiles, photos, essays and more. The online place
to comfortably meet and find real romance.
</p>

<p>
***The Guaranteed Monthly Renewal Rate will apply for credit card purchases only at the expiration of your initial Subscription Term and will automatically continue as a monthly recurring transaction unless or until you choose a different subscription plan or cancel your subscription.
</p>



		</form>
	</body>
</html>
