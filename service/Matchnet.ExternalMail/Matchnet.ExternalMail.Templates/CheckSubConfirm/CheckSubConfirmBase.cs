using System;
using System.Collections.Specialized;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Subscription.ServiceAdapters;
using Matchnet.Subscription.ValueObjects;

namespace Matchnet.ExternalMail.Templates.CheckSubConfirm
{
	/// <summary>
	/// Summary description for CheckSubConfirmBase.
	/// </summary>
	public class CheckSubConfirmBase : TemplateBase
	{
		private Matchnet.Content.ValueObjects.BrandConfig.Brand _brand;
		protected Matchnet.Member.ServiceAdapters.Member _member;
		private int _memberID;
		private int _memberPaymentID;
		private string _planDescription;

		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Label lblPlan;
		protected System.Web.UI.WebControls.Label lblAccountHolder;
		protected System.Web.UI.WebControls.Label lblAuthorizationDate;
		protected System.Web.UI.WebControls.Label lblCheckNumber;
		protected System.Web.UI.WebControls.Label lblTransactionDate;
		protected System.Web.UI.WebControls.Label lblContactEmail;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			_brand = BrandConfigSA.Instance.GetBrands().GetBrand(Convert.ToInt32(Request.QueryString["BrandID"]));
			_memberID = Convert.ToInt32(Request.QueryString["MemberID"]);
			_member = MemberSA.Instance.GetMember(_memberID, MemberLoadFlags.None);
			_memberPaymentID = Convert.ToInt32(Request.QueryString["MemberPaymentID"]);
			_planDescription = Request.QueryString["PlanDescription"];

			NameValueCollection nvc = MemberPaymentSA.Instance.GetMemberPayment(_memberID, _brand.Site.Community.CommunityID, _memberPaymentID);
			string firstName = string.Empty;
			string lastName = string.Empty;
			string checkNumber = string.Empty;
				
			if ( nvc != null && nvc.Count > 0 )
			{
				if ( nvc["FirstName"] != null )
				{
					firstName = nvc["FirstName"];
				}
							
				if ( nvc["LastName"] != null )
				{
					lastName = nvc["LastName"];
				}

				if ( nvc["CheckNumber"] != null )
				{
					checkNumber = nvc["CheckNumber"];
				}
			}
			
			string name = firstName + " " + lastName;
			string date = DateTime.Now.ToString();

			lblName.Text = name;
			lblPlan.Text = _planDescription;
			lblAccountHolder.Text = name;
			lblTransactionDate.Text = date;
			lblAuthorizationDate.Text = date;
			lblCheckNumber.Text = checkNumber;
			lblContactEmail.Text = "support@" + _brand.Uri;
			

			base.MessageInfo.FromAddress = "support@" + _brand.Uri;
			base.MessageInfo.FromName = "support@" + _brand.Uri;
			base.MessageInfo.ToAddress = "comments@" + _brand.Uri;
			base.MessageInfo.Subject = "Telecheck Confirmation";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

	}
}
