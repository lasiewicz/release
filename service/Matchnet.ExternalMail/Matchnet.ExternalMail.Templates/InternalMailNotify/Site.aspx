<%@ Register TagPrefix="cc" TagName="StandardHeader" Src="../CommonControls/StandardHeader.ascx" %>
<%@ Register TagPrefix="cc" TagName="StandardFooter" Src="../CommonControls/StandardFooter.ascx" %>
<%@ Page language="c#" Codebehind="InternalMailNotifyBase.cs" AutoEventWireup="false" Inherits="Matchnet.ExternalMail.Templates.InternalMailNotify.InternalMailNotifyBase" %>
<html>
	<head>
		<title>Check Subscription Confirmation</title>
	</head>
	<body dir="ltr">
		<form id="Form1" method="post" runat="server">
			<cc:StandardHeader id="header" runat="server" />
			
			<table width="640" cellspacing="1" cellpadding="0" border="0" bgcolor="#999999">
				<tr>
					<td>
						<table width="100%" cellpadding="0" border="0" cellspacing="0" bgcolor="#FFFFFF">
							<tr>
								<td bgcolor="#24408F"><img src="JD_logo.gif" width="99" height="45" hspace="8" alt="JDate.com" /></td>
								<td bgcolor="#24408F" align="right"><img src="messagealert.gif" width="130" height="45" hspace="8" alt="Click&trade;! Alert" /></td>
							</tr>
							<tr>
								<td align="center" colspan="2" bgcolor="#FFFFFF">
									<table width="100%" cellpadding="16" cellspacing="0" border="0">
										<tr>
											<td width="40%" align="right" valign="top">
												<br />
												<p><img src="new_message2.gif" width="200" height="137" alt="You have new messages!" /></p></td>
											<td width="60%" valign="top">
												<br />
												<font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#333333"><p><strong><a href="#"><font color="#174495">Login</font></a> to see your new messages!</strong><br />
												You have <strong>47</strong> unread messages.</p>
												<p><a href="#"><img src="view_inbox.gif" width="140" height="20" border="0" alt="View your inbox" /></a></p>
												<p>Remember, you must be a Premium Member to communicate with others. If you are not a Premium Member yet, <strong><a href="#"><font color="#174495">upgrade today</font></a></strong></p></font>
												<br />
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<cc:StandardFooter id="footer" runat="server" />
		</form>
	</body>
</html>
