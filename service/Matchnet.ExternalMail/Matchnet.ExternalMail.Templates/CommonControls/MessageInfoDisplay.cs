using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using Matchnet;
using Matchnet.ExternalMail.ValueObjects;

namespace Matchnet.ExternalMail.Templates.CommonControls
{
	//[DefaultProperty("Text"), 
	//ToolboxData("<{0}:Link runat=server></{0}:Link>")]
	public class MessageInfoDisplay : System.Web.UI.WebControls.WebControl
	{
		private MessageInfo _messageInfo;
		

		public MessageInfoDisplay()
		{
			_messageInfo = new MessageInfo();
		}


		protected override void Render(HtmlTextWriter output)
		{
			output.Write("<!--\r\n");
			output.Write("MessageInfo:");

			MemoryStream ms = new MemoryStream();
			BinaryFormatter bf = new BinaryFormatter();
			bf.Serialize(ms, _messageInfo);
			output.Write(System.Convert.ToBase64String(ms.ToArray()));

			output.Write("\r\n-->\r\n");
		}


		public MessageInfo MessageInfo
		{
			get
			{
				return _messageInfo;
			}
		}
	}
}
