using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

using Matchnet;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;


namespace Matchnet.ExternalMail.Templates.CommonControls
{
	[DefaultProperty("Text"), 
		ToolboxData("<{0}:Link runat=server></{0}:Link>")]
	public class Txt : System.Web.UI.WebControls.WebControl
	{
		private string _text;
		private string _href = Constants.NULL_STRING;
		private bool _isSecure = false;
		private string _resourceName;
	
		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public string Text 
		{
			get
			{
				if(_text == null || _text == string.Empty)
				{
					int brandID = Convert.ToInt32(this.Page.Request.QueryString["BrandID"]);
					Brand brand = BrandConfigSA.Instance.GetBrands().GetBrand(brandID);
					object caller = this.Parent;
					if(caller is System.Web.UI.HtmlControls.HtmlForm)
					{
						//	We are calling from a page, not a nested user control.  This is kind of
						//	hack, but since there should only be on HtmlForm per page (at the top level)
						//	it should work all right.
						caller = this.Page;
					}
					return(Localizer.GetStringResource(ResourceName, caller, brand));
				}
				return(_text);
			}

			set
			{
				_text = value;
			}
		}

		public string ResourceName
		{
			get { return(_resourceName); }
			set { _resourceName = value; }
		}


		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public string Href 
		{
			get
			{
				return _href;
			}

			set
			{
				_href = value;
			}
		}


		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public bool IsSecure 
		{
			get
			{
				return _isSecure;
			}

			set
			{
				_isSecure = value;
			}
		}


		protected override void Render(HtmlTextWriter output)
		{
			if (_href == Constants.NULL_STRING)
			{
				output.Write(Text);
			}
			else
			{
				UriBuilder uri = new UriBuilder();

				Brand brand = BrandConfigSA.Instance.GetBrands().GetBrand(Convert.ToInt32(Context.Request.QueryString["BrandID"]));

				if (!IsSecure)
				{
					uri.Scheme = "http://";
					string host = brand.Site.DefaultHost;
					if (host.Length > 0)
					{
						host = host + ".";
					}
					uri.Host = host + brand.Uri;
					uri.Path = _href;
				}
				else
				{
					uri.Scheme = "https://";
					uri.Host = brand.Site.SSLUrl;
					uri.Path = _href;
				}

				output.Write("<a href=\"");
				output.Write(uri.ToString());
				output.Write("\">");
				output.Write(Text);
				output.Write("</a>");
			}
		}
	}
}
