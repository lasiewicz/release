using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Resources;
using System.Text;
using System.Web;
using System.Web.Caching;
using Matchnet.Content.ValueObjects.BrandConfig;
// using Matchnet.Web.Framework.Configuration;

namespace Matchnet.ExternalMail.Templates.CommonControls
{
	/// <summary>
	/// Summary description for LocalizedResourceSet.
	/// </summary>
	internal sealed class LocalizedResourceSet
	{
		private ArrayList				_searchTables;

		internal static LocalizedResourceSet GetControlResourceSet(string resourceSetBaseName, Brand brand, StringBuilder trace)
		{
			string		searchKey;

			ArrayList searchTables = new ArrayList();

			trace.Append("Control resource search order:").Append(Environment.NewLine);;

			// type + culture + brand
			searchKey	= GetSearchKey(resourceSetBaseName, CultureInfo.CurrentCulture, brand.Uri);
			AddSearchTable(searchKey, trace, searchTables);

			// type + culture + site
			searchKey	= GetSearchKey(resourceSetBaseName, CultureInfo.CurrentCulture, brand.Site.Name);
			AddSearchTable(searchKey, trace, searchTables);

			// type + culture + community
			searchKey	= GetSearchKey(resourceSetBaseName, CultureInfo.CurrentCulture, brand.Site.Community.Name);
			AddSearchTable(searchKey, trace, searchTables);

			// type + culture
			searchKey	= GetSearchKey(resourceSetBaseName, CultureInfo.CurrentCulture);
			AddSearchTable(searchKey, trace, searchTables);

			// type + brand
			searchKey	= GetSearchKey(resourceSetBaseName, brand.Uri);
			AddSearchTable(searchKey, trace, searchTables);

			// type + site
			searchKey	= GetSearchKey(resourceSetBaseName, brand.Site.Name);
			AddSearchTable(searchKey, trace, searchTables);

			// type + community
			searchKey	= GetSearchKey(resourceSetBaseName, brand.Site.Community.Name);
			AddSearchTable(searchKey, trace, searchTables);

			// type
			searchKey	= GetSearchKey(resourceSetBaseName);
			AddSearchTable(searchKey, trace, searchTables);

			return new LocalizedResourceSet(searchTables);
		}

		/// <summary>
		///		Get the search table matching the supplied key. If the table contains 0 records, ignore it,
		///		otherwise add to searchTables list.
		/// </summary>
		/// <param name="searchKey"></param>
		/// <param name="trace"></param>
		/// <param name="searchTables"></param>
		private static void AddSearchTable(string searchKey, StringBuilder trace, ArrayList searchTables)
		{
			Hashtable searchTable = GetSearchTable(searchKey);

			// since brand / site / community names can overlap, we may request the same
			// search table multiple times. if that happens, bail
			if (searchTables.Contains(searchTable))
			{
				return;
			}

			// trace.AppendFormat(@"<a href=""file://C:\Matchnet\Bedrock\Web\bedrock.matchnet.com\{0}.resx"" target=""_blank"">C:\Matchnet\...\{0}.resx</a> [{1} resource(s)", searchKey, searchTable.Count);
			if (searchTable.Count > 0)
			{
				searchTables.Add(searchTable);
			}
			else
			{
				trace.Append(", ignoring");
			}
			trace.Append("]").Append(Environment.NewLine);
		}

		internal static LocalizedResourceSet GetGlobalResourceSet(string resourceSetBaseName, Brand brand, StringBuilder trace)
		{
			string		searchKey;

			ArrayList searchTables = new ArrayList();

			trace.Append("Global resource search order:").Append(Environment.NewLine);;

			// culture + brand
			searchKey	= GetSearchKey(resourceSetBaseName, CultureInfo.CurrentCulture, brand.Uri);
			AddSearchTable(searchKey, trace, searchTables);

			// culture + site
			searchKey	= GetSearchKey(resourceSetBaseName, CultureInfo.CurrentCulture, brand.Site.Name);
			AddSearchTable(searchKey, trace, searchTables);

			// culture + community
			searchKey	= GetSearchKey(resourceSetBaseName, CultureInfo.CurrentCulture, brand.Site.Community.Name);
			AddSearchTable(searchKey, trace, searchTables);

			// culture
			searchKey	= GetSearchKey(resourceSetBaseName, CultureInfo.CurrentCulture);
			AddSearchTable(searchKey, trace, searchTables);

			// brand
			searchKey	= GetSearchKey(resourceSetBaseName, brand.Uri);
			AddSearchTable(searchKey, trace, searchTables);

			// site
			searchKey	= GetSearchKey(resourceSetBaseName, brand.Site.Name);
			AddSearchTable(searchKey, trace, searchTables);

			// community
			searchKey	= GetSearchKey(resourceSetBaseName, brand.Site.Community.Name);
			AddSearchTable(searchKey, trace, searchTables);

			// default
			searchKey	= GetSearchKey(resourceSetBaseName);
			AddSearchTable(searchKey, trace, searchTables);

			return new LocalizedResourceSet(searchTables);
		}

		internal string GetString(string key)
		{
			foreach (Hashtable resourceTable in _searchTables)
			{
				if (resourceTable.ContainsKey(key))
				{
					return (string) resourceTable[key];
				}
			}

			return null;
		}

		private static string GetSearchKey(string resourceSetBaseName)
		{
			return GetSearchKey(resourceSetBaseName, CultureInfo.InvariantCulture, null);
		}

		private static string GetSearchKey(string resourceSetBaseName, string brandUri)
		{
			return GetSearchKey(resourceSetBaseName, CultureInfo.InvariantCulture, brandUri);
		}

		private static string GetSearchKey(string resourceSetBaseName, CultureInfo currentCulture)
		{
			return GetSearchKey(resourceSetBaseName, currentCulture, null);
		}

		private static string GetSearchKey(string resourceSetBaseName, CultureInfo currentCulture, string brandUri)
		{
			string key = resourceSetBaseName;

			// add culture
			if (currentCulture != CultureInfo.InvariantCulture)
			{
				key += "." + currentCulture.Name;
			}

			// add brand
			if (null != brandUri && brandUri.Length > 0)
			{
				key += "." + brandUri;
			}

			return key;
		}

		private static Hashtable GetSearchTable(string key)
		{
			return GetSearchTableHelper(key, 0);
		}

		private static Hashtable GetSearchTableHelper(string key, int recursionCount)
		{
			key = key.ToLower();

			Hashtable searchTable = null;

			// make sure we are running under the asp.net runtime
			if (null == HttpRuntime.Cache)
			{
				throw new Exception("LocalizedResourceSet requires the support of ASP.NET and cannot run outside of it.");
			}

			// look up the search table in the cache, load if not found
			if (null == HttpRuntime.Cache[key])
			{
				lock(typeof(LocalizedResourceSet))
				{
					searchTable = (Hashtable) HttpRuntime.Cache[key];
					if (null == searchTable)
					{
						// get full path to the resource file
						string resourceFilePath = Path.Combine(HttpContext.Current.Server.MapPath("~/"), key) + ".resx";

						// load the resource file into a Hashtable
						searchTable = LoadSearchTable(resourceFilePath);
						Debug.Assert(null != searchTable);

						// add new table to the cache. if the resource file exists, set up file dependency.
						// if the resource files does not exist, set up the dependency on the root folder.
						// TODO: check whether a root dependency is triggered on a change in subfolder;
						CacheDependency fileDependency;
						if (File.Exists(resourceFilePath))
						{
							fileDependency = new CacheDependency(resourceFilePath);
						}
						else
						{
							fileDependency = new CacheDependency(HttpContext.Current.Server.MapPath("~/"));
						}
						HttpRuntime.Cache.Insert(key, searchTable, fileDependency, DateTime.MaxValue, TimeSpan.Zero);
					}
					else
					{
						return searchTable;
					}
				}
			}
			else
			{
				searchTable = (Hashtable) HttpRuntime.Cache[key];

				// its highly unlikely but possible that the key expired before we got here. check!
				if (null == searchTable)
				{
					// there is a chance that high system load on the same key
					// may force us to recurse out of control here. This is a
					// check measure to ensure this condition does not go unnoticed.
					//	TOFIX - Determine an appropriate maxRecursionCount - dcornell
					// int maxRecursionCount = MatchnetConfiguration.Settings.Localization.MaxRecursionCount;
					int maxRecursionCount = 100;
					if (recursionCount >= maxRecursionCount)
					{
						throw new Exception(string .Format("Max recursion count exceeded while retrieving resource ({0}).", key));
					}

					searchTable = GetSearchTableHelper(key, recursionCount + 1);
				}
			}

			Debug.Assert(null != searchTable);
			return searchTable;
		}

		private static Hashtable LoadSearchTable(string resourceFilePath)
		{
			Hashtable searchTable = new Hashtable();

			// if file not found, bail
			if (! File.Exists(resourceFilePath))
			{
				System.Diagnostics.Trace.WriteLine(string.Format("Resource file not found ({0}).", resourceFilePath));
				return searchTable;
			}

			// create a ResXResourceReader for the resource file
			using (ResXResourceReader reader = new ResXResourceReader(resourceFilePath))
			{
				// iterate through the resources and load the contents into the Hashtable
				foreach (DictionaryEntry entry in reader)
				{
					searchTable.Add(entry.Key, entry.Value);
				}

				// close the reader
				reader.Close();
			}

			return searchTable;
		}

		private LocalizedResourceSet(ArrayList searchTables)
		{
			_searchTables = searchTables;
		}
	}
}
