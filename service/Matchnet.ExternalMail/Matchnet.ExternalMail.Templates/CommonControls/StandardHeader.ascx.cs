using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.ExternalMail.Templates.CommonControls
{
	/// <summary>
	///		Summary description for CommonHeader.
	/// </summary>
	public class StandardHeader : UserControl
	{
		private int _memberID;
		private int _brandID;
		private Matchnet.Member.ServiceAdapters.Member _member;
		private Brand _brand;
		protected Label lblMemberName;

		private void Page_Load(object sender, System.EventArgs e)
		{
			_memberID = Convert.ToInt32(Request.QueryString["MemberID"]);
			_member = MemberSA.Instance.GetMember(_memberID, MemberLoadFlags.None);

			_brandID = Convert.ToInt32(Request.QueryString["BrandID"]);
			_brand = BrandConfigSA.Instance.GetBrands().GetBrand(_brandID);

			lblMemberName.Text = _member.Username;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion

	}
}
