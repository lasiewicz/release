using System;
using System.Collections;

namespace Matchnet.ExternalMail.Templates.CommonControls
{
	public class HeaderCollection : ICollection
	{
		private ArrayList _Collection = new ArrayList();
		
		public bool IsSynchronized
		{
			get
			{
				return _Collection.IsSynchronized;
			}
		}

		public int Count
		{
			get
			{
				return _Collection.Count;
			}
		}

		public void CopyTo(Array array, int index)
		{
			_Collection.CopyTo(array, index);
		}

		public object SyncRoot
		{
			get
			{
				return _Collection.SyncRoot;
			}
		}

		public IEnumerator GetEnumerator()
		{
			throw new System.InvalidOperationException("Use GetHeaderEnumerator instead");
		}

		public HeaderCollectionEnumerator GetHeaderEnumerator() 
		{
			return new HeaderCollectionEnumerator(this);
		}

		public Header this[int index]
		{
			get
			{
				return (Header)_Collection[index];
			}
		}

		public void Add(Header header) 
		{
			_Collection.Add(header);
		}
	}
}
