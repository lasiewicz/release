using System;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;

// using Matchnet.Web.Framework;
// using Matchnet.Web.Framework.Configuration;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.ExternalMail.Templates.CommonControls
{
	/// <summary>
	///		Localizer class manager the retrieval of resources at runtime.
	/// </summary>
	public sealed class Localizer
	{
		private static readonly string RESOURCES_SUBFOLDER = "_Resources";
		private static readonly string ROOT_NAMESPACE_FOLDER = "Matchnet.ExternalMail.Templates".Replace('.', Path.DirectorySeparatorChar);

		/// <summary>
		///		Prevent instantiation
		/// </summary>
		private Localizer() {}

		/// <summary>
		///		Retrieve a string resource.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="caller"></param>
		/// <param name="brand"></param>
		/// <returns>
		///		Returns a localized string or null if resource is not defined.
		/// </returns>
		public static string GetStringResource(string key, object caller, Brand brand)
		{
			return GetStringResource(key, caller, brand, null);
		}

		/// <summary>
		///		Retrieve a string resource.
		/// </summary>
		/// <param name="key">Resource key.</param>
		/// <param name="caller">The class that the resource belongs to.</param>
		/// <param name="brand">Site brand name.</param>
		/// <param name="tokenMap">String expansion tokens (name/value pairs).</param>
		/// <returns>
		///		Returns a localized string or null if resource is not defined.
		/// </returns>
		public static string GetStringResource(string key, object caller, Brand brand, StringDictionary tokenMap)
		{
			return GetFormattedStringResource(key, caller, brand, tokenMap, null);
		}

		/// <summary>
		///		Retrieve a string resource, use it as format template, and populate with values from the supplied argument array.
		/// </summary>
		/// <param name="key">Resource key.</param>
		/// <param name="caller">The class that the resource belongs to.</param>
		/// <param name="brand">Site brand name.</param>
		/// <param name="tokenMap">String expansion tokens (name/value pairs).</param>
		/// <param name="args">String replacement arguments.</param>
		/// <returns>
		///		Returns a localized string or null if resource is not defined.
		/// </returns>
		internal static string GetFormattedStringResource(string key, object caller, Brand brand, StringDictionary tokenMap, string[] args)
		{
			// TODO: bring back resource hints ("[{0}]{1}", resourceName, value)
			
			string	resourceSetBaseName;

			// set up the resource set base name (to be used in lookup)
			Control callingControl = GetCallingPageOrUserControl(caller);
			if (null != callingControl)
			{
				Type type = callingControl.GetType();
				resourceSetBaseName = ConvertTypeToResourcePath(type);
			}
			else
			{
				resourceSetBaseName = Path.Combine(RESOURCES_SUBFOLDER, "Global");
			}

			// get the resource set
			LocalizedResourceSet resourceSet;
			StringBuilder trace = new StringBuilder();
			if (null != callingControl)
			{
				resourceSet = LocalizedResourceSet.GetControlResourceSet(resourceSetBaseName, brand, trace);
			}
			else
			{
				resourceSet = LocalizedResourceSet.GetGlobalResourceSet(resourceSetBaseName, brand, trace);
			}

			// find the resource string
			string resourceString = resourceSet.GetString(key);

			// if configured, indicate that the resource string was not found
			//	TOFIX - Determine whether or not to show bugs as on the live site - dcornell
			// if (null == resourceString && MatchnetConfiguration.Settings.Localization.ShowMissingResources)
			if(null == resourceString && false)
			{
				return ShowResourceHint("<img src=\"http://static.matchnet.com/misc/dev/bug.gif\">", key, trace);
			}
			else if (null == resourceString)
			{
				return resourceString;
			}

			// perform token expansion
			resourceString = ExpandTokens(resourceString, tokenMap);

			// should show resource hints
			bool showResourceHints = GetShowResourceHints();

			// if there is no string replacing to do, we are done
			if (0 == resourceString.Length || null == args || 0 == args.Length)
			{
				return (showResourceHints) ? ShowResourceHint(resourceString, key, trace) : resourceString;
			}

			// replace occurrences of "%s" with values from the agrs array
			string formattedString = Format(resourceString, args);

			// perform token expansion again just in case any were introduced in the formatting step
			formattedString = ExpandTokens(formattedString, tokenMap);

			return (showResourceHints) ? ShowResourceHint(formattedString, key, trace) : formattedString;
		}

		internal static string ConvertTypeToResourcePath(Type type)
		{
			string typeName = type.Name;
			if (typeName.EndsWith("_aspx") || typeName.EndsWith("_ascx") || typeName.EndsWith("_asmx"))
			{
				typeName = typeName.Substring(0, typeName.Length - 5) + "." + typeName.Substring(typeName.Length - 4);
			}
			string path = Path.Combine(Path.Combine(type.BaseType.Namespace.Replace('.', Path.DirectorySeparatorChar), RESOURCES_SUBFOLDER), typeName);

			// strip the root namespace from they path, since the file structure ignores it
			if (path.StartsWith(ROOT_NAMESPACE_FOLDER))
			{
				path = path.Substring(ROOT_NAMESPACE_FOLDER.Length + 1);
			}

			return path;
		}

		private static bool GetShowResourceHints()
		{
			string resourceHints = HttpContext.Current.Request.QueryString["ResourceHints"];
			return (null != resourceHints && resourceHints.Length > 0 && resourceHints == "1");
		}

		private static string ShowResourceHint(string resourceValue, string key, StringBuilder trace)
		{
			return string.Format(
				"<div style=\"display:inline;\" onmouseover=\"document.getElementById('popup_{1}').style.visibility = 'visible'; return true;\" onmouseout=\"document.getElementById('popup_{1}').style.visibility = 'hidden'; return true;\" style=\"font-weight: normal;\">{0}" +
				"<div id=\"popup_{1}\" class=\"resourceHint\"><b>{2}</b><br/><br/>{3}</div></div>", resourceValue, Guid.NewGuid(), key, trace.Replace(Environment.NewLine, "<br />"));
		}

		/// <summary>
		///		Try to determine the Page or UserControl that this resource request ultimately
		///		comes from. Recurse up the Control hierarchy if necessary.
		/// </summary>
		/// <param name="caller"></param>
		/// <returns></returns>
		private static Control GetCallingPageOrUserControl(object caller)
		{
			if (null == caller || !(caller is Control))
			{
				return null;
			}

			if (! (caller is UserControl || caller is Page))
			{
				return GetCallingPageOrUserControl(((Control) caller).Parent);
			}

			return (Control) caller;
		}

		/// <summary>
		///		Substitute occurrences of "%s" in the format string with values from the arguments array.
		/// </summary>
		/// <param name="formatString">Format string.</param>
		/// <param name="args">Arguments array.</param>
		/// <returns></returns>
		private static string Format(string formatString, string[] args)
		{
			int pos = formatString.IndexOf("%s");
			int lastPos = 0;
			int counter = 0;

			if (pos < 0) 
			{
				return formatString;
			}

			if (args == null || 0 == args.Length) 
			{
				return formatString;
			}
			
			StringBuilder builder = new StringBuilder();
	
			while (pos > -1) 
			{
				builder.Append(formatString.Substring(lastPos, pos - lastPos));
				if (counter < args.Length) 
				{
					builder.Append(args[counter]);
					counter++;
				} 
				else 
				{
					break;
				}
				lastPos = pos + 2;
				if (lastPos > formatString.Length) 
				{
					break;
				}
				pos = formatString.IndexOf("%s", lastPos);
			}
			if (lastPos < formatString.Length) 
			{
				builder.Append(formatString.Substring(lastPos));
			}

			return builder.ToString();
		}

		/// <summary>
		///		For every token (key) value found in the source string, replace it with the corresponding
		///		expansion (value).
		/// </summary>
		/// <param name="source"></param>
		/// <param name="tokenMap"></param>
		/// <returns></returns>
		public static string ExpandTokens(string source, StringDictionary tokenMap)
		{
			// validate tokenMap exists
			if (null == tokenMap)
			{
				return source;
			}

			// expand tokens
			string result = source;
			foreach(DictionaryEntry entry in tokenMap)
			{
				string key = string.Format("(({0}))", entry.Key.ToString().ToUpper(CultureInfo.InvariantCulture));
				result = result.Replace(key, entry.Value.ToString());
			}

			return result;
		}

		internal static string ExpandImageTokens(string source, Brand brand)
		{
			int pos = source.IndexOf("((image:");
			int lastPos = 0;
			int endPos = 0;
			string fileName = string.Empty;
			string token = string.Empty;
			
			if (pos < 0) 
			{
				return source;
			}

			StringBuilder builder = new StringBuilder();
	
			while (pos > -1) 
			{
				
				builder.Append(source.Substring(lastPos, pos - lastPos));
				
				endPos = source.IndexOf("))", pos);
				
				if (endPos > -1)
				{
					token = source.Substring(pos + 2, endPos - pos - 2);

					int colon1 = token.IndexOf(":");
					int colon2 = token.LastIndexOf(":");
					
					if (colon1 != -1 && colon2 != -1)
					{
						fileName = token.Substring(colon2 + 1, token.Length - colon2 - 1);
						builder.Append(Image.GetURLFromFilename(fileName));
					}

					lastPos = endPos + 2;
					if (lastPos > source.Length) 
					{
						break;
					}
					pos = source.IndexOf("((image:", lastPos);
				}
				else
					break;

			}
			if (lastPos < source.Length) 
			{
				builder.Append(source.Substring(lastPos));
			}

			return builder.ToString();
		}
	}
}
