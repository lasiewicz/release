#region System References
using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Text;
using System.IO;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
#endregion

namespace Matchnet.ExternalMail.Templates.CommonControls
{
	public class Image : System.Web.UI.WebControls.Image
	{
		#region Private/Protected Fields
		private string	_FileName = String.Empty;
		private string	_NavigateUrl = String.Empty;
		private string	_ImageUrl = String.Empty;
		private Unit	_Hspace = Unit.Empty;
		private Unit	_Vspace = Unit.Empty;
		private Unit	_Border = Unit.Empty;
		private string	_defaultTitleResourceConstant;
		private string	_TitleResourceConstant;
		private string	_defaultResourceConstant;
		private string	_ResourceConstant;
		private bool	_RollOver;
		private Brand	_Brand;
		private App		_App = App.None;
		#endregion

		#region Properties

		public App App
		{
			get { return(_App); }
			set { _App = value; }
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public virtual string FileName 
		{
			get
			{
				return _FileName;
			}

			set
			{
				_FileName = value;
			}
		}

		public Brand Brand
		{
			get
			{
				if(_Brand != null)
				{
					return(_Brand);
				}
				int brandID = Convert.ToInt32(this.Page.Request.QueryString["BrandID"]);
				_Brand = BrandConfigSA.Instance.GetBrands().GetBrand(brandID);

				return(_Brand);
			}
		}
		
		private string _NavigateURLClass;
		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public string NavigateURLClass 
		{
			get
			{
				return _NavigateURLClass;
			}

			set
			{
				_NavigateURLClass = value;
			}
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public override string ImageUrl
		{
			get
			{
				if (_ImageUrl == null || _ImageUrl == String.Empty)
				{
					_ImageUrl = GetUrl(FileName, (int)_App, false);

					//If we don't get back an Image URL, return a dummy path to avoid requests to Default.aspx
					if (_ImageUrl == null || _ImageUrl == string.Empty)
					{
						_ImageUrl = ".gif";
					}
					_ImageUrl = LinkHref(_ImageUrl, true);
				}

				return(_ImageUrl);
			}
			set
			{
				_ImageUrl = value;
			}
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public string NavigateUrl 
		{
			get
			{
				return LinkHref(_NavigateUrl, true);
			}

			set
			{
				_NavigateUrl = value;
			}
		}

		/// <summary>
		/// This method is unorganized and has wavering logic.  It is a good candidate for scrapping or rewriting
		/// </summary>
		/// <param name="href"></param>
		/// <param name="checkForHTTPS"></param>
		/// <returns></returns>
		private string LinkHref(string href, bool checkForHTTPS)
		{
			if ( href == null || href.Length == 0 )
			{
				return(string.Empty);
			}

			if ( HttpContext.Current == null )
			{
				return(href);
			}

			string returnValue = href;

			/*
			if (checkForHTTPS && href.ToLower().IndexOf("javascript:") == -1)
			{
				if ((HttpContext.Current.Request["SERVER_PORT_SECURE"].Equals("1")))
				{
					if ((Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_DEVELOPMENT_MODE"))))
					{
						string host = HttpContext.Current.Request.Url.Host.Split(new Char[] {'.'})[0];

						if ( host.ToLower() == "localhost" )
						{
							returnValue = ("http://" + host) + returnValue;
						}
						else
						{
							returnValue = ("http://" + host + "." + Brand.Uri) + returnValue;
						}
					}
					else
					{
					*/
						returnValue = ("http://" + Brand.Site.DefaultHost + "." + Brand.Uri) + returnValue;
					/*
					}
				}
			}
			*/

			/*
			if (g.PersistLayoutTemplate == true && g.LayoutTemplate != LayoutTemplate.Standard && g.LayoutTemplate != LayoutTemplate.PopupOnce)
			{
				string delim;

				if (href.IndexOf("?") != -1)
				{
					delim = "&";
				}
				else
				{
					delim = "?";
				}
				returnValue += delim + "LayoutTemplateID=" + ((int) g.LayoutTemplate).ToString();
			}
			*/

			return(returnValue);
		}


		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public Unit Hspace 
		{
			get
			{
				return _Hspace;
			}

			set
			{
				_Hspace = value;
			}
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public Unit Vspace 
		{
			get
			{
				return _Vspace;
			}

			set
			{
				_Vspace = value;
			}
		}


		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public Unit Border
		{
			get
			{
				return _Border;
			}

			set
			{
				_Border = value;
			}
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public string ResourceConstant
		{
			get
			{
				return _ResourceConstant;
			}

			set
			{
				_ResourceConstant = value;
			}
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public string TitleResourceConstant
		{
			get
			{
				return _TitleResourceConstant;
			}

			set
			{
				_TitleResourceConstant = value;
			}
		}

		[Bindable(true), 
		Category("Appearance"), 
		DefaultValue("")] 
		public bool RollOver 
		{
			get
			{
				return _RollOver;
			}

			set
			{
				_RollOver = value;
			}
		}

		protected string DefaultTitleResourceConstant
		{
			get { return _defaultTitleResourceConstant; }
			set { _defaultTitleResourceConstant = value; }
		}

		protected string DefaultResourceConstant
		{
			get { return _defaultResourceConstant; }
			set { _defaultResourceConstant = value; }
		}

		#endregion

		#region Public Methods
		
		public Image()
		{
	
		}

		public string GetUrl(string filename, int applicationID, bool fullyQualified) 
		{
			string path = string.Empty;
			
			string key = "IMAGE:" + filename + ":" + applicationID + ":" + Brand.BrandID;
			//	TOFIX - Implement caching - dcornell
			//	path = System.Convert.ToString(Matchnet.CachingTemp.Cache.GetInstance().Get(key));

			if (path == string.Empty)
			{
				path = FileSrc(filename, applicationID);
				if(path.Length > 0) 
				{
					path = path.Replace(@"\", "/");
				}
				//	TOFIX - Implement caching - dcornell
				//	Matchnet.CachingTemp.Cache.GetInstance().Insert(key, path, null, DateTime.Now.AddMinutes(5), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.High, null);
			}

			if(fullyQualified) 
			{
				if((Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_URL")).IndexOf("http://") == -1) 
				{
					path = Brand.Site.DefaultHost + Brand.Uri + path;
				} 
			}

			return path;
		}

		public static string GetURLFromFilename(string filename)
		{
			Image img = new Image();
			img.FileName = filename;
			return img.ImageUrl;
		}

		#endregion

		#region Protected Methods

		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			if (ImageUrl.Length > 0) 
			{
				if ((null != _ResourceConstant && _ResourceConstant.Length > 0) || (null != _defaultResourceConstant && _defaultResourceConstant.Length > 0))
				{
					string resourceValue;
					if (null == _ResourceConstant || _ResourceConstant.Length == 0)
					{
						resourceValue = GetResourceValue(_defaultResourceConstant, null);
					}
					else
					{
						resourceValue = GetResourceValue(_ResourceConstant, this);
					}
					writer.AddAttribute(HtmlTextWriterAttribute.Alt,System.Web.HttpUtility.HtmlEncode(resourceValue));
				}

					// Issue #8178 klandrus 10.22.04, decision is now to display nothing when resourceconstant or titleresourceconstant is null
					// 6162 & 6213 lcarter 07.19.04 if no alt text, use FileName
				else if( (base.AlternateText.Length == 0) && (FileName.Length != 0) )
				{
					writer.AddAttribute(HtmlTextWriterAttribute.Alt, string.Empty);
				}
				// 6162 & 6213 end
			}
			else
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Alt, FileName);
			}

			// Write Title attribute (only if either a Default or actual title resource constant was specified)
			if ((null != _TitleResourceConstant && _TitleResourceConstant.Length > 0) || (null != _defaultTitleResourceConstant && _defaultTitleResourceConstant.Length > 0))
			{
				string titleResourceValue;
				if(_TitleResourceConstant != null && _TitleResourceConstant.Length == 0)
				{
					titleResourceValue = GetResourceValue(_defaultTitleResourceConstant, null);
				}
				else
				{
					titleResourceValue = GetResourceValue(_TitleResourceConstant, this);
				}
				writer.AddAttribute(HtmlTextWriterAttribute.Title, System.Web.HttpUtility.HtmlEncode(titleResourceValue));
			}

			if ((_Border != Unit.Empty) && (_NavigateUrl != String.Empty)) { writer.AddAttribute(HtmlTextWriterAttribute.Border,Border.ToString()); }
			if (_Hspace != Unit.Empty) { writer.AddAttribute("hspace",_Hspace.Value.ToString()); }
			if (_Vspace != Unit.Empty) { writer.AddAttribute("vspace",_Hspace.Value.ToString()); }
			if (_RollOver == true)
			{
				writer.AddAttribute("onmouseover","this.src='" + GetMouseOverSrc(ImageUrl) + "';");
				writer.AddAttribute("onmouseout","this.src='" + ImageUrl + "';");
			}

			base.AddAttributesToRender (writer);
		}

		private string GetResourceValue(string resourceConstant, Control caller)
		{
			if (resourceConstant == null || resourceConstant.Length == 0)
			{
				return string.Empty;
			}
			//	TOFIX - Replace this call once resources have been ported over - dcornell
			//	return _g.GetResource(resourceConstant, caller);
			return(string.Empty);
		}

		public override void RenderBeginTag(HtmlTextWriter writer)
		{
			DataBind();
			if ( _NavigateUrl != String.Empty)
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Class,_NavigateURLClass);
				writer.AddAttribute(HtmlTextWriterAttribute.Href,_NavigateUrl);
				writer.RenderBeginTag(HtmlTextWriterTag.A);
			}
			base.RenderBeginTag (writer);
		}

		public override void RenderEndTag(HtmlTextWriter writer)
		{
			if ( _NavigateUrl != String.Empty)
			{
				base.RenderEndTag (writer);
				writer.RenderEndTag(); // A
			}
			else
			{
				base.RenderEndTag (writer);
			}
		}

		#endregion

		#region Private Methods

		private string FileSrc(string filename, int applicationID) 
		{
			string path = String.Empty;
			bool found = false;
			
			string[] delims = new string[] {"Community", "Site", "Brand", "p", "d", "l"};
			string[] vals = new string[] {Brand.Site.Community.Name, Brand.Site.Name, GetPathFromUri(Brand.Uri), Brand.BrandID.ToString(), Brand.Site.Community.CommunityID.ToString(), Brand.Site.LanguageID.ToString()};
			for(int i = 0; i < delims.Length; i++) 
			{
				path = @"\" + delims[i] + @"\" + vals[i] + @"\" + filename;
				if(System.IO.File.Exists((Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_ROOT")) + path)) 
				{
					found = true;
					break;
				}

				if(applicationID != Constants.NULL_INT) 
				{
					path = @"\" + delims[i] + @"\" + vals[i] + @"\" + applicationID.ToString() + @"\" + filename;
					if(System.IO.File.Exists(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_ROOT") + path))
					{
						found = true;
						break;
					}
				}
			}

			if(!found) 
			{
				path = @"\" + applicationID.ToString() + @"\" + filename;
				found = System.IO.File.Exists((Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_ROOT")) + path);
			}
			
			if(!found) 
			{
				path = @"\" + filename;
				//Regardless of whether the image exists, we'll return a path including the filename.
				//This will be logged as a 404 in IIS logs.
			}

			path = path.Replace(@"\",@"/");
			path = (Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IMG_URL")) + path;

			return path;
		}

		private string GetPathFromUri(string pUri)
		{
			return pUri.Replace(".", "-");
		}

		private string GetMouseOverSrc(string fileSrc)
		{
			string filePath = fileSrc.Substring(0, fileSrc.LastIndexOf("/") + 1);
			return filePath + Path.GetFileNameWithoutExtension(fileSrc) + "h" + Path.GetExtension(fileSrc);
		}

		#endregion

	} // class Image

	public enum App : int
	{
		None = 0,
		Home = 1,
		Logon = 2,
		Subscription = 3,
		MemberProfile = 7,
		Chat = 12,
		Article = 13,
		ContactUs = 14,
		SendToFriend = 16,
		MemberServices = 18,
		IM = 19,
		Search = 22,
		MembersOnline = 23,
		HotList = 26,
		Email = 27,
		Events = 28,
		Termination = 32,
		Captcha = 50,
		Tease = 51,
		MemberPhotos = 52,
		Error = 53,
		Offer = 62,
		Telephony = 63,
		Resource = 507,
		MemberSearch = 22,
		Report = 523,
		FreeTextApproval = 525,
		MemberList = 26,
		EditProfile = 539,
		PixelAdministrator = 545,
		Options = 549
	}
}
