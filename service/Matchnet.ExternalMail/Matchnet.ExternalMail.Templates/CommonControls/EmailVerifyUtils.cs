using System;

using Matchnet.Security;

namespace Matchnet.ExternalMail.Templates.CommonControls
{
	/// <summary>
	/// Summary description for EmailVerifyUtils.
	/// </summary>
	public class EmailVerifyUtils
	{
		private EmailVerifyUtils()
		{

		}

		/// <summary>
		/// "Encrypt" an email address for verification.  This is located
		/// here because it is needed by a couple of different emails.
		/// 
		/// TODO - This should be placed in a location where it is accessible
		/// to both the templates that currently need access to it (this email
		/// and the RegistrationVerification template).  Ideally it would also
		/// go where the DecryptQueryString methods lives because it makes
		/// sense to keep them side by side, however, dependency issues between
		/// projects make this tricky.  Perhaps put it in Matchnet.SharedLib?
		/// -dcornell
		/// </summary>
		/// <param name="strEmailAddress">string to encrypt</param>
		/// <returns>encrypted string</returns>
		public static string EncryptQueryString(string strEmailAddress)
		{
			string mENCRYPT_PREFIX = "zX";
			string mENCRYPT_KEY = "kdfajdsf";
			string mENCRYPT_DELIM = "^";

			// akxString.Encrypt objEncrypt = new akxString.EncryptClass();
			// objEncrypt.Initialize(mENCRYPT_KEY);
			// return mENCRYPT_PREFIX + objEncrypt.Encode(strEmailAddress + mENCRYPT_DELIM + DateTime.Now);

			string valueToEncrypt = strEmailAddress + mENCRYPT_DELIM + DateTime.Now;
			string retVal = Crypto.Encrypt(mENCRYPT_KEY, valueToEncrypt);

			return(mENCRYPT_PREFIX + retVal);
		}
	}
}
