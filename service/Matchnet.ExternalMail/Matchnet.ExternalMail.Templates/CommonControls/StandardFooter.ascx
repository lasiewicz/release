<%@ Control Language="c#" AutoEventWireup="false" Codebehind="StandardFooter.ascx.cs" Inherits="Matchnet.ExternalMail.Templates.CommonControls.StandardFooter" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="cc" Namespace="Matchnet.ExternalMail.Templates.CommonControls" Assembly="Matchnet.ExternalMail.Templates" %>

<table width="640" cellpadding="8" cellspacing="0" border="0">
	<tr>
		<td align="center">
			<font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#999999">
				<cc:Txt id="txtContent" runat="server" ResourceName="TXT_CONTENT" />
			</font>
		</td>
	</tr>
</table>