using System;

namespace Matchnet.ExternalMail.Templates.CommonControls
{
	/// <summary>
	/// A simple Name / Value container to be used for
	/// rendering Email Headers.
	/// </summary>
	public class Header
	{
		private string _Name;
		private string _Value;

		/// <summary>
		/// The name of the Header, without the ":" suffix
		/// </summary>
		public string Name 
		{
			get 
			{
				return _Name;
			}
			set
			{
				_Name = value;
			}
		}

		/// <summary>
		/// The value of the Header.
		/// </summary>
		public string Value 
		{
			get 
			{
				return _Value;
			}
			set 
			{
				_Value = value;
			}
		}
	}
}
