<%@ Control Language="c#" AutoEventWireup="false" Codebehind="StandardHeader.ascx.cs" Inherits="Matchnet.ExternalMail.Templates.CommonControls.StandardHeader" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="cc" Namespace="Matchnet.ExternalMail.Templates.CommonControls" Assembly="Matchnet.ExternalMail.Templates" %>

<table width="640" cellpadding="0" cellspacing="0" border="0" style="margin-bottom: 2px;">
	<tr>
		<td><strong><font face="Verdana, Arial, Helvetica, sans-serif" size="2"><cc:Txt id="hi" ResourceName="HI" runat="server" /><asp:Label id="lblMemberName" runat="server" />!</font></strong></td>
		<td align="right"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><font color="#174495"><cc:Txt id="txtEmailSettings" ResourceName="TXT_EMAIL_SETTINGS" Href="/Applications/MemberServices/EmailSettings.aspx" runat="server" /></font> | <font color="#174495"><cc:Txt id="txtPasswordHelp" ResourceName="TXT_PASSWORD_HELP" Href="/Applications/Logon/RetrievePassword.aspx" runat="server" /></font> | <font color="#174495"><cc:Txt id="txtLogin" ResourceName="TXT_LOGIN" Href="/Applications/Logon/Logon.aspx" runat="server" /></font></font></td>
	</tr>
</table>