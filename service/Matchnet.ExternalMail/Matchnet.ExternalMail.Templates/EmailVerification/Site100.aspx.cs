using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

using Matchnet.ExternalMail.Templates.CommonControls;

namespace Matchnet.ExternalMail.Templates.EmailVerification
{
	public class Site100 : TemplateBase
	{		
		protected Brand _brand;
		protected Matchnet.Member.ServiceAdapters.Member _member;
		protected System.Web.UI.WebControls.Label lblUsername;
		protected System.Web.UI.WebControls.HyperLink lnkVerifyEmailLink;
		protected System.Web.UI.WebControls.Label lblVerifyCode;

		private void Page_Load(object sender, System.EventArgs e)
		{
			_brand = BrandConfigSA.Instance.GetBrands().GetBrand(Convert.ToInt32(Request.QueryString["BrandID"]));
			_member = MemberSA.Instance.GetMember(Convert.ToInt32(Convert.ToInt32(Request.QueryString["MemberID"])), MemberLoadFlags.None);

			string strEmailAddress;
			string strVerifyCode;
			string strVerifyLink;

			strEmailAddress = _member.EmailAddress;
			strVerifyCode = EmailVerifyUtils.EncryptQueryString(strEmailAddress);
			strVerifyLink = "http://www.spark.com/Applications/MemberServices/VerifyEmail.aspx?VerifyCode=" + strVerifyCode;

			lblUsername.Text = _member.Username;
			lnkVerifyEmailLink.Text = strVerifyLink;
			lnkVerifyEmailLink.NavigateUrl = strVerifyLink;
			lblVerifyCode.Text = strVerifyCode;

			base.MessageInfo.FromAddress = "VerifyEmail@spark.com";
			base.MessageInfo.FromName = "VerifyEmail@spark.com";
			base.MessageInfo.ToAddress = _member.EmailAddress;
			base.MessageInfo.Subject = "Activate Your Membership";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

	}
}
