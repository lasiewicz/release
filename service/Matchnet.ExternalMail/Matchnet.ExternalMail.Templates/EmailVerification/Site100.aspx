<%@ Register TagPrefix="cc" Namespace="Matchnet.ExternalMail.Templates.CommonControls" Assembly="Matchnet.ExternalMail.Templates" %>
<%@ Page language="c#" Codebehind="Site100.aspx.cs" AutoEventWireup="false" Inherits="Matchnet.ExternalMail.Templates.EmailVerification.Site100" %>
<html>
	<head>
		<title>Your matches from
			<cc:txt runat="server" id="txtTitleSiteName" text="[txtTitleSiteName]" /></title>
	</head>
	<body dir="ltr">
		<form id="Form1" method="post" runat="server">
			
Dear <asp:Label id="lblUsername" runat="server" />,<br />
<br />
To ACTIVATE YOUR MEMBERSHIP please click the link <br />
<br />
<asp:HyperLink id="lnkVerifyEmailLink" runat="server" /><br />
<br />
If the link above is not underlined in blue or if after clicking it does<br />
not take you to a web page, follow these steps:<br />
<br />
1. Make sure your computer is connected to the internet and open <br /> 
	Internet browser<br /><br />
2. Go to http://www.spark.com/Applications/MemberServices/MemberServices.aspx
3. Now scroll down to the very bottom of the page and click the "Verify Email" link<br />
<br />
<br />
4. Then copy and paste the following code into the space provided and click the "Submit" button:"<br />
<br />
<asp:Label id="lblVerifyCode" runat="server" /><br />
<br />
5. Once activated, you'll see a confirmation message on the screen.<br />
<br />
Remember, you must have a valid email address to send and receive<br />
contacts to other members so don't wait - ACTIVATE TODAY!<br />
<br />
The Team at Spark.com<br />
<br />
		</form>
	</body>
</html>
