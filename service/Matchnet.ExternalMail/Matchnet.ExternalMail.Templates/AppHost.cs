using System;
using System.Web;
using System.Web.Hosting;
using System.IO;
using System.Threading;
using System.Diagnostics;

namespace Matchnet.ExternalMail.Templates
{
	public class AppHost : MarshalByRefObject
	{
		public string ProcessRequest(string page, string queryString) 
		{
			StringWriter sw = new StringWriter();
			SimpleWorkerRequest wr = new SimpleWorkerRequest(page, queryString, sw);

			/*
			System.Diagnostics.Trace.WriteLine(wr.GetAppPath());
			System.Diagnostics.Trace.WriteLine(wr.GetFilePath());
			System.Diagnostics.Trace.WriteLine(wr.GetFilePathTranslated());
			*/

			HttpRuntime.ProcessRequest(wr);

			return sw.ToString();
		}
	}
}
