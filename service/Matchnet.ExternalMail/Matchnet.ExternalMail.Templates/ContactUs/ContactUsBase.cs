using System;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.ExternalMail.Templates.ContactUs
{
	/// <summary>
	/// Summary description for ContactUsBase.
	/// </summary>
	public class ContactUsBase : TemplateBase
	{
		private Brand _brand;
		private string _username;
		private string _fromEmailAddress;
		private string _sMemberID;
		private string _browser;
		private string _remoteIP;
		private string _reason;
		private string _userComment;

		protected System.Web.UI.WebControls.Label lblUserName;
		protected System.Web.UI.WebControls.Label lblEmailAddress;
		protected System.Web.UI.WebControls.Label lblMemberID;
		protected System.Web.UI.WebControls.Label lblBrowser;
		protected System.Web.UI.WebControls.Label lblRemoteIP;
		protected System.Web.UI.WebControls.Label lblReason;
		protected System.Web.UI.WebControls.Label lblComment;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			_brand = BrandConfigSA.Instance.GetBrands().GetBrand(Convert.ToInt32(Request.QueryString["BrandID"]));
			_username = Request["UserName"];
			_fromEmailAddress = Request.QueryString["FromEmailAddress"];
			_sMemberID = Request.QueryString["SMemberID"];
			_browser = Request.QueryString["Browser"];
			_remoteIP = Request.QueryString["RemoteIP"];
			_reason = Request.QueryString["Reason"];
			_userComment = Request.QueryString["UserComment"];

			lblUserName.Text = _username;
			lblEmailAddress.Text = _fromEmailAddress;
			lblMemberID.Text = _sMemberID;
			lblBrowser.Text = _browser;
			lblRemoteIP.Text = _remoteIP;
			lblReason.Text = _reason;
			lblComment.Text = _userComment;

			base.MessageInfo.FromAddress = _fromEmailAddress;
			base.MessageInfo.FromName = _fromEmailAddress;
			base.MessageInfo.ToAddress = "comments@" + _brand.Uri;
			base.MessageInfo.Subject = _reason;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
