using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.Templates.CommonControls;


namespace Matchnet.ExternalMail.Templates
{
	public class TemplateBase : System.Web.UI.Page
	{
		private MessageInfoDisplay _messageInfoDisplay;

		public TemplateBase()
		{
			_messageInfoDisplay = new MessageInfoDisplay();
			this.Controls.AddAt(0, _messageInfoDisplay);
		}


		public MessageInfo MessageInfo
		{
			get
			{
				return _messageInfoDisplay.MessageInfo;
			}
		}
	}
}
