using System;
using System.Collections;
using System.Diagnostics;
using System.Text;
using System.Web;

using Matchnet.Lib;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Region;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;

using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;

using Matchnet.ExternalMail.Templates.CommonControls;
using Matchnet.ExternalMail.ValueObjects.Impulse;

using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;

using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;


namespace Matchnet.ExternalMail.Templates.StormPostGeneral
{
	/// <summary>
	/// Summary description for StormPostGeneralBase.
	/// </summary>
	public class StormPostGeneralBase : TemplateBase
	{
		const string TAB = "\t";

		private string[] SEARCH_FIELDS_FOR_URL = new string[] {	"SearchTypeID", "Distance", "MinAge", "MaxAge",
													"MinHeight", "MaxHeight", "EducationLevel", "Religion",
													"Ethnicity", "SmokingHabits", "DrinkingHabits", "HasPhotoFlag",
													"MajorType", "RelationshipMask", "RelationShipStatus" };

		protected System.Web.UI.WebControls.Literal litContent;

		private int _emailType;

		private int _brandID;
		private Brand _brand;

		private int _memberID = 0;

		/// <summary>
		/// This can be set and will then be used in making the x-headers.
		/// Defaults to 0
		/// </summary>
		public int MemberID
		{
			get
			{
				return(_memberID);
			}
			set
			{
				_memberID = value;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			_emailType = Convert.ToInt32(Request.QueryString["EmailType"]);

			_brandID = Convert.ToInt32(Request.QueryString["BrandID"]);
			_brand = BrandConfigSA.Instance.GetBrands().GetBrand(_brandID);

			int privateLabelID = DeterminePrivateLabelIDFromSiteID(_brand.Site.SiteID);

			StringBuilder contentBuilder = new StringBuilder();
			BuildContent(contentBuilder);

			//	Set up x-header info
			//	First the old-school x-headers

			Classification classification = new Classification();
			//	classification.Populate(_emailType, MemberID, privateLabelID);
			classification.Populate(DetermineActualEmailType(_emailType), MemberID, privateLabelID);
			HeaderCollectionEnumerator enumerator = classification.Headers.GetHeaderEnumerator();
			while (enumerator.MoveNext()) 
			{
				Header header = (Header)enumerator.Current;
				base.MessageInfo.Headers.Add(header.Name, header.Value);
			}

			//	Now the StormPost Grey Mail specific x-header
			int numProfiles = DetermineProfileCount(_emailType);
			base.MessageInfo.Headers.Add("X-MsgType", privateLabelID + "-" + DetermineActualEmailType(_emailType) + "-" + numProfiles);

			//	Done with the headers, now write out actual content

			string myContent = contentBuilder.ToString();
			litContent.Text = myContent;
		}

		private int DetermineActualEmailType(int emailType)
		{
			int retVal = emailType;

			if(emailType == (int)EmailType.PhotoRejection)
			{
				//	This is a hack, but we will send both as photoapproval and set the profile
				//	count to 1 for the rejection (which is hacked in elsewhere)
				retVal = (int)EmailType.PhotoApproval;
			}

			return(retVal);
		}

		private int DeterminePrivateLabelIDFromSiteID(int siteID)
		{
			//	TODO - Fill in the rest of these - dcornell

			int retVal = 1;

			switch(siteID)
			{
				case 15:	//	Cupid.co.il
					retVal = 15;
					break;
				case 112:	//	CollegeLuv.com
					retVal = 12;
					break;
				case 102:	//	Glimpse.com
					retVal = 2;
					break;
				case 103:	//	JDate.com
					retVal = 3;
					break;
				case 101:	//	AmericanSingles.com
					retVal = 1;
					break;
			}

			return(retVal);
		}

		private int DetermineProfileCount(int emailType)
		{			
			int retVal = 0;

			if(	emailType == (int)EmailType.MatchNewsLetter ||
				emailType == (int)EmailType.ViralNewsLetter)
			{
				string memberIDList = Request.QueryString["MemberIDList"];
				string[] memberIDs = memberIDList.Split(new char[] { ',' });
				retVal = memberIDs.Length;
			} 
			else if(emailType == (int)EmailType.PhotoRejection)
			{
				//	This is a vicious hack, but there are two templates used by type 16, and
				//	for the rejection we set the count to 1 rather than 0.  Not my idea.  I
				//	just do what I'm told.
				retVal = 1;
			}

			if(emailType == (int)EmailType.MatchNewsLetter)
			{
				//	For these the user's own profile counts as one apparently.
				retVal++;
			}

			return(retVal);
		}

		private void BuildContent(StringBuilder content)
		{
			try
			{
				switch((EmailType)_emailType)
				{
						//	Activation letter and EmailChangeConfirmation pass the same info to StormPost.
					case EmailType.ActivationLetter:
						BuildActivationLetter(content);
						break;
					case EmailType.EmailChangeConfirmation:
						BuildEmailVerificationLetter(content);
						break;
					case EmailType.ForgotPassword:
						BuildForgotPassword(content);
						break;
					case EmailType.PhotoApproval:
						BuildPhotoApproval(content);
						break;
					case EmailType.SendToFriend:
						BuildSendToFriend(content);
						break;
					case EmailType.NewMailAlert:
						BuildNewMailAlert(content);
						break;
					case EmailType.MatchNewsLetter:
						BuildMatchNewsLetter(content);
						break;
					case EmailType.MutualMail:
						BuildMutualMail(content);
						break;
					case EmailType.ViralNewsLetter:
						BuildViralNewsLetter(content);
						break;
					case EmailType.PhotoRejection:
						BuildPhotoRejection(content);
						break;
					default:
						throw new Exception("Invalid EmailType of: " + _emailType
								+ " could not be matched with a GreyMail Build method.  QueryString was: "
								+ Request.QueryString.ToString());
						break;
				}
			} 
			catch (Exception ex)
			{
				EventLog.WriteEntry(Matchnet.ExternalMail.ValueObjects.ServiceConstants.SERVICE_NAME,
					"Exception while processing impulse with query string '" + Request.QueryString.ToString()
						+ "'.  Exception message is: "
						+ ex.Message + " and stack trace is: " + ex.StackTrace,
					EventLogEntryType.Error);
				base.MessageInfo.DoSend = false;
			}
		}

		private void BuildActivationLetter(StringBuilder content)
		{
			_memberID = Convert.ToInt32(Request.QueryString["MemberID"]);

			Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(MemberID, MemberLoadFlags.None);
			string emailAddress = member.EmailAddress;
			string verifyCode = EmailVerifyUtils.EncryptQueryString(emailAddress);

			content.Append(Filter(emailAddress));
			content.Append(TAB);
			content.Append(Filter(verifyCode));

			base.MessageInfo.FromAddress = "Activation@" + _brand.Uri;
			base.MessageInfo.FromName = "Activation@" + _brand.Uri;
			base.MessageInfo.ToAddress = emailAddress;
			base.MessageInfo.ToName = emailAddress;
			base.MessageInfo.Subject = _brand.Uri + " Welcome";
		}

		private void BuildEmailVerificationLetter(StringBuilder content)
		{
			_memberID = Convert.ToInt32(Request.QueryString["MemberID"]);

			Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(MemberID, MemberLoadFlags.None);
			string emailAddress = member.EmailAddress;
			string verifyCode = EmailVerifyUtils.EncryptQueryString(emailAddress);

			content.Append(Filter(emailAddress));
			content.Append(TAB);
			content.Append(Filter(verifyCode));

			base.MessageInfo.FromAddress = "verify@" + _brand.Uri;
			base.MessageInfo.FromName = "verify@" + _brand.Uri;
			base.MessageInfo.ToAddress = emailAddress;
			base.MessageInfo.ToName = emailAddress;
			base.MessageInfo.Subject = "Email Verification";
		}

		private void BuildForgotPassword(StringBuilder content)
		{
			string emailAddress = Request.QueryString["EmailAddress"];
			string password = MemberSA.Instance.GetPassword(emailAddress);

			content.Append(Filter(emailAddress));
			content.Append(TAB);
			content.Append(Filter(password));

			base.MessageInfo.FromAddress = "memberservices@" + _brand.Uri;
			base.MessageInfo.FromName = "memberservices@" + _brand.Uri;
			base.MessageInfo.ToAddress = emailAddress;
			base.MessageInfo.ToName = emailAddress;
			base.MessageInfo.Subject = "Forgot Password";
		}

		private void BuildPhotoApproval(StringBuilder content)
		{
			_memberID = Convert.ToInt32(Request.QueryString["MemberID"]);
			Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(MemberID, MemberLoadFlags.None);
			string emailAddress = member.EmailAddress;

			content.Append(Filter(emailAddress));

			base.MessageInfo.FromAddress = "photos@" + _brand.Uri;
			base.MessageInfo.FromName = "photos@" + _brand.Uri;
			base.MessageInfo.ToAddress = emailAddress;
			base.MessageInfo.ToName = emailAddress;
			base.MessageInfo.Subject = _brand.Uri + " photo approved";
		}

		private void BuildSendToFriend(StringBuilder content)
		{
			string friendEmail = Request.QueryString["FriendEmail"];
			string friendName = Request.QueryString["FriendName"];
			string sentMemberID = Request.QueryString["SentMemberID"];
			string subject = Request.QueryString["Subject"];
			string userEmail = Request.QueryString["UserEmail"];

			content.Append(Filter(friendEmail));
			content.Append(TAB);
			content.Append(Filter(friendName));
			content.Append(TAB);
			content.Append(Filter(sentMemberID));

			base.MessageInfo.FromAddress = userEmail;
			base.MessageInfo.FromName = userEmail;
			base.MessageInfo.ToAddress = friendEmail;
			base.MessageInfo.ToName = friendName;
			base.MessageInfo.Subject = subject;
		}

		private void BuildNewMailAlert(StringBuilder content)
		{
			_memberID = Convert.ToInt32(Request.QueryString["MemberID"]);
			Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(MemberID, MemberLoadFlags.None);

			int fromMemberID = Convert.ToInt32(Request.QueryString["SentMemberID"]);
			Matchnet.Member.ServiceAdapters.Member fromMember = MemberSA.Instance.GetMember(fromMemberID, MemberLoadFlags.None);

			int emailID = Convert.ToInt32(Request.QueryString["EmailID"]);
			EmailMessage message = EmailMessageSA.Instance.RetrieveMessage(_brand.Site.Community.CommunityID,
				member.MemberID,
				emailID,
				string.Empty,
				EmailMessage.DIRECTION.NONE,
				false);
			if(message == null)
			{
				throw new Exception("EmailMessageSA returned a null message for CommunityID: " + _brand.Site.Community.CommunityID
					+ ", MemberID: " + member.MemberID
					+ ", EmailID: " + emailID);
			} 

			EmailFolder inbox = EmailFolderSA.Instance.RetrieveFolder(member.MemberID, _brand.Site.Community.CommunityID, (int)EmailFolder.SYSTEM_TABLES.INBOX, true);
			
			content.Append(Filter(member.EmailAddress));
			content.Append(TAB);

			content.Append(Filter(member.Username));
			content.Append(TAB);

			content.Append(Filter(message.InsertDate.ToString()));
			content.Append(TAB);

			content.Append(inbox.UnreadMessages);
			content.Append(TAB);

			content.Append(Filter(this.DetermineMemberThumbnail(fromMember)));
			content.Append(TAB);

			content.Append(Filter(fromMember.Username));
			content.Append(TAB);

			content.Append(this.DetermineMemberAge(fromMember));
			content.Append(TAB);

			content.Append(Filter(DetermineMemberSeeking(member)));
			content.Append(TAB);

			if(_brandID != 1002)	//	Glimpse is different.  Here is the normal way:
			{
				content.Append(Filter(this.DetermineMemberRegionDisplay(fromMember)));
				content.Append(TAB);

				content.Append(emailID);
			}
			else	//	And here is the Glimpse way:
			{
				content.Append(emailID);
				content.Append(TAB);

				content.Append(Filter(this.DetermineMemberRegionDisplay(fromMember)));
			}
			
			base.MessageInfo.FromAddress = "Communications@" + _brand.Uri;
			base.MessageInfo.FromName = "Communications@" + _brand.Uri;
			base.MessageInfo.ToAddress = member.EmailAddress;
			base.MessageInfo.ToName = member.EmailAddress;
			base.MessageInfo.Subject = "You have mail";
		}


		private void BuildMatchNewsLetter(StringBuilder content)
		{
			string sMemberID = Request.QueryString["MemberID"];
			_memberID = Convert.ToInt32(sMemberID);
			Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(MemberID, MemberLoadFlags.None);

			Matchnet.Member.ServiceAdapters.Member[] members;
			members = LoadMemberGroup();

			//	Start making the actual GreyMail content

			content.Append(Filter(member.EmailAddress));
			content.Append(TAB);

			foreach(Matchnet.Member.ServiceAdapters.Member currentMember in members)
			{
				content.Append(currentMember.MemberID);
				content.Append(TAB);
			}

			content.Append(MemberID);
			content.Append(TAB);

			foreach(Matchnet.Member.ServiceAdapters.Member currentMember in members)
			{
				content.Append(Filter(currentMember.Username));
				content.Append(TAB);
			}

			content.Append(Filter(member.Username));
			content.Append(TAB);

			foreach(Matchnet.Member.ServiceAdapters.Member currentMember in members)
			{
				int age = DetermineMemberAge(currentMember);

				content.Append(age);
				content.Append(TAB);
			}

			content.Append(DetermineMemberAge(member));
			content.Append(TAB);

			foreach(Matchnet.Member.ServiceAdapters.Member currentMember in members)
			{
				string regionDisplay = DetermineMemberRegionDisplay(currentMember);

				content.Append(Filter(regionDisplay));
				content.Append(TAB);
			}

			content.Append(Filter(DetermineMemberRegionDisplay(member)));
			content.Append(TAB);

			foreach(Matchnet.Member.ServiceAdapters.Member currentMember in members)
			{
				string thumbnail = DetermineMemberThumbnail(currentMember);

				content.Append(Filter(thumbnail));
				content.Append(TAB);
			}

			content.Append(DetermineMemberThumbnail(member));
			content.Append(TAB);

			content.Append(Filter(DetermineMemberAboutMe(member)));
			content.Append(TAB);

			content.Append(this.DetermineMemberHotListCount(member));
			content.Append(TAB);

			content.Append(this.DetermineMemberContactedCount(member));
			content.Append(TAB);

			content.Append(this.DetermineMemberTeasedCount(member));
			content.Append(TAB);

			content.Append(this.DetermineMemberIMCount(member));
			content.Append(TAB);

			content.Append(this.DetermineMemberViewedCount(member));
			content.Append(TAB);

			content.Append(Filter(DetermineSearchUrl(member)));
			content.Append(TAB);

			string hashedEmail = EmailVerifyUtils.EncryptQueryString(member.EmailAddress);
			content.Append(Filter(hashedEmail));

			base.MessageInfo.FromAddress = "YourMatches@" + _brand.Uri;
			base.MessageInfo.FromName = "YourMatches@" + _brand.Uri;
			base.MessageInfo.ToAddress = member.EmailAddress;
			base.MessageInfo.ToName = member.EmailAddress;
			base.MessageInfo.Subject = "Your matches from " + _brand.Uri;
		}


		private void BuildMutualMail(StringBuilder content)
		{
			_memberID = Convert.ToInt32(Request.QueryString["MemberID"]);
			Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(MemberID, MemberLoadFlags.None);

			int mutualMemberID = Convert.ToInt32(Request.QueryString["TargetMemberID"]);
			Matchnet.Member.ServiceAdapters.Member mutualMember = MemberSA.Instance.GetMember(mutualMemberID, MemberLoadFlags.None);

			content.Append(Filter(member.EmailAddress));
			content.Append(TAB);

			content.Append(Filter(member.Username));
			content.Append(TAB);

			content.Append(mutualMember.MemberID);
			content.Append(TAB);

			content.Append(Filter(this.DetermineMemberThumbnail(mutualMember)));
			content.Append(TAB);

			content.Append(Filter(mutualMember.Username));
			content.Append(TAB);

			content.Append(this.DetermineMemberAge(mutualMember));
			content.Append(TAB);

			content.Append(Filter(this.DetermineMemberRegionDisplay(mutualMember)));
			content.Append(TAB);

			content.Append(Filter(DetermineMemberAboutMe(mutualMember)));
			content.Append(TAB);

			content.Append(this.DetermineMemberUsernameText(member));
			content.Append(TAB);

			content.Append(this.DetermineMemberUsernameText(mutualMember));

			base.MessageInfo.FromAddress = "Communications@" + _brand.Uri;
			base.MessageInfo.FromName = "Communications@" + _brand.Uri;
			base.MessageInfo.ToAddress = member.EmailAddress;
			base.MessageInfo.ToName = member.EmailAddress;
			base.MessageInfo.Subject = "You Click";
		}

		private void BuildViralNewsLetter(StringBuilder content)
		{
			_memberID = Convert.ToInt32(Request.QueryString["MemberID"]);
			Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(MemberID, MemberLoadFlags.None);

			Matchnet.Member.ServiceAdapters.Member[] clickMembers = this.LoadMemberGroup();

			content.Append(Filter(member.EmailAddress));
			content.Append(TAB);

			foreach(Matchnet.Member.ServiceAdapters.Member currentMember in clickMembers)
			{
				content.Append(currentMember.MemberID);
				content.Append(TAB);
			}

			foreach(Matchnet.Member.ServiceAdapters.Member currentMember in clickMembers)
			{
				content.Append(Filter(this.DetermineMemberThumbnail(currentMember)));
				content.Append(TAB);
			}

			foreach(Matchnet.Member.ServiceAdapters.Member currentMember in clickMembers)
			{
				content.Append(Filter(currentMember.Username));
				content.Append(TAB);
			}

			foreach(Matchnet.Member.ServiceAdapters.Member currentMember in clickMembers)
			{
				content.Append(this.DetermineMemberAge(currentMember));
				content.Append(TAB);
			}

			foreach(Matchnet.Member.ServiceAdapters.Member currentMember in clickMembers)
			{
				content.Append(Filter(this.DetermineMemberRegionDisplay(currentMember)));
				content.Append(TAB);
			}

			foreach(Matchnet.Member.ServiceAdapters.Member currentMember in clickMembers)
			{
				content.Append(Filter(this.DetermineMemberAboutMe(currentMember)));
				content.Append(TAB);
			}

			content.Append(member.MemberID);
			content.Append(TAB);

			content.Append(Filter(member.Username));
			content.Append(TAB);

			content.Append(Filter(this.DetermineMemberUsernameText(member)));
			content.Append(TAB);

			foreach(Matchnet.Member.ServiceAdapters.Member currentMember in clickMembers)
			{
				content.Append(Filter(this.DetermineMemberUsernameText(currentMember)));
				content.Append(TAB);
			}
			//	chop off the trailing tab
			content.Remove(content.Length - 1, 1);

			base.MessageInfo.FromAddress = "click@" + _brand.Uri;
			base.MessageInfo.FromName = "click@" + _brand.Uri;
			base.MessageInfo.ToAddress = member.EmailAddress;
			base.MessageInfo.ToName = member.EmailAddress;
			base.MessageInfo.Subject = "Do You Click?";
		}

		private void BuildPhotoRejection(StringBuilder content)
		{
			_memberID = Convert.ToInt32(Request.QueryString["MemberID"]);
			Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(MemberID, MemberLoadFlags.None);
			string emailAddress = member.EmailAddress;
			string comment = Request.QueryString["Comment"];

			content.Append(Filter(emailAddress));
			content.Append(TAB);
			content.Append(Filter(comment));

			base.MessageInfo.FromAddress = "photos@" + _brand.Uri;
			base.MessageInfo.FromName = "photos@" + _brand.Uri;
			base.MessageInfo.ToAddress = emailAddress;
			base.MessageInfo.ToName = emailAddress;
			base.MessageInfo.Subject = _brand.Uri + " photo rejected";
		}

		private Matchnet.Member.ServiceAdapters.Member[] LoadMemberGroup()
		{
			//	Get an array of memberID ints
			string sMemberIDs = Request.QueryString["MemberIDList"];
			string[] aMemberIDs = sMemberIDs.Split(new char[] { ',' });
			int[] memberIDArray = new int[aMemberIDs.Length];
			for(int i = 0; i < memberIDArray.Length; i++)
			{
				memberIDArray[i] = Convert.ToInt32(aMemberIDs[i]);
			}

			//	Load up those members into a Member array
			Matchnet.Member.ServiceAdapters.Member[] members;
			members = new Matchnet.Member.ServiceAdapters.Member[memberIDArray.Length];
			for(int i = 0; i < memberIDArray.Length; i++)
			{
				members[i] = MemberSA.Instance.GetMember(memberIDArray[i], MemberLoadFlags.None);
			}

			return(members);
		}

		#region Methods to pull selected data from members

		private string DetermineMemberUsernameText(Matchnet.Member.ServiceAdapters.Member member)
		{
			string retVal = member.Username;

			return(retVal);
		}

		private string DetermineMemberAboutMe(Matchnet.Member.ServiceAdapters.Member member)
		{
			string retVal = member.GetAttributeTextApproved(_brand, "AboutMe", member.MemberID, "FREETEXT_NOT_APPROVED");
			if(retVal != null && retVal.Length > 30)
			{
				retVal = retVal.Substring(0, 27) + "...";
			}

			return(retVal);
		}

		private int DetermineMemberHotListCount(Matchnet.Member.ServiceAdapters.Member member)
		{
			int retVal = DetermineListCount(member, HotListCategory.Default);

			return(retVal);
		}

		private int DetermineMemberContactedCount(Matchnet.Member.ServiceAdapters.Member member)
		{
			int retVal = DetermineListCount(member, HotListCategory.MembersYouEmailed);

			return(retVal);
		}

		private int DetermineMemberTeasedCount(Matchnet.Member.ServiceAdapters.Member member)
		{
			int retVal = DetermineListCount(member, HotListCategory.MembersYouTeased);

			return(retVal);
		}

		private int DetermineMemberIMCount(Matchnet.Member.ServiceAdapters.Member member)
		{
			int retVal = DetermineListCount(member, HotListCategory.MembersYouIMed);

			return(retVal);
		}

		private int DetermineMemberViewedCount(Matchnet.Member.ServiceAdapters.Member member)
		{
			int retVal = DetermineListCount(member, HotListCategory.MembersYouViewed);

			return(retVal);
		}

		private int DetermineListCount(Matchnet.Member.ServiceAdapters.Member member, Matchnet.List.ValueObjects.HotListCategory listID)
		{
			int count;

			Matchnet.List.ServiceAdapters.List myList = ListSA.Instance.GetList(member.MemberID);
			ArrayList members = myList.GetListMembers(listID, _brand.Site.Community.CommunityID, _brand.Site.SiteID, 1, 1000000, out count);

			return(count);
		}

		private int DetermineMemberAge(Matchnet.Member.ServiceAdapters.Member member)
		{
			DateTime birthDate = member.GetAttributeDate(_brand, "Birthdate");
			int age = Matchnet.Lib.Util.Util.Age(birthDate);

			//	Age can't be more than 999 for the templates and sometimes bad data gives
			//	us a year as a return value, so for our purposes here we will just knock
			//	the value down to 999

			if(age >= 1000)
			{
				age = 999;
			}

			return(age);
		}

		public string DetermineMemberRegionDisplay(Matchnet.Member.ServiceAdapters.Member pMember)
		{
			int regionAttribute;
			string regionString = "";
			RegionLanguage regionLanguage;

			regionAttribute = pMember.GetAttributeInt(_brand, "regionid");
			if (regionAttribute > 0)
			{
				// regionLanguage = new RegionLanguage();
				// regionLanguage.PopulateHierarchy(regionAttribute, g.Brand.Site.LanguageID);
				regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionAttribute, _brand.Site.LanguageID);
				
				// Incrementally build up the regionString with commas where necessary
				// based on the contents being empty or not before and after each comma
				// This made for CI/CP and tested on CL/GL 
				// start WEL
				regionString = regionLanguage.CityName;

				if(regionString != String.Empty && regionLanguage.StateDescription != String.Empty)
				{
					regionString += ", ";
				}

				regionString += regionLanguage.StateDescription;
																   
				if(regionString != String.Empty && regionLanguage.CountryAbbreviation != String.Empty)
				{
					regionString += ", ";
				}
				
				regionString += regionLanguage.CountryAbbreviation;

				// end WEL
				
				if (regionString.Length > ConstantsTemp.MAX_REGION_STRING_LENGTH)
				{
					regionString = regionString.Substring(0, ConstantsTemp.MAX_REGION_STRING_LENGTH - 3) + "...";
				}
			}
			else
			{
				regionString = "N/A";
			}
			return regionString;
		}

		private string DetermineMemberThumbnail(Matchnet.Member.ServiceAdapters.Member member)
		{
			//	TODO - determine if this is the appropriate no-photo for Bedrock sites - dcornell
			string retVal = "http://" + _brand.Site.DefaultHost + "." + _brand.Uri + @"/img/d/1/7/nophoto70x91.gif";

			if (member.GetPhotos(_brand.Site.Community.CommunityID).Count > 0)
			{
				Photo photo = member.GetPhotos(_brand.Site.Community.CommunityID)[0];

				if (photo.IsApproved)
				{
					if (photo.IsPrivate)
					{
						
					}
					else
					{
						retVal = photo.ThumbFileWebPath;
					}
				}
			}

			return(retVal);
		}

		private string DetermineMemberSeeking(Matchnet.Member.ServiceAdapters.Member member)
		{
			SearchPreferenceCollection prefs  = SearchPreferencesSA.Instance.GetSearchPreferences(member.MemberID, _brand.Site.Community.CommunityID);

			int genderMask = Convert.ToInt32(prefs["GenderMask"]);
			int genderSelf = GetGenderMaskSelf(genderMask);
			int genderSeeking = GetGenderMaskSeeking(genderMask);
			string sGenderSelf = "GENDER" + genderSelf;
			string sGenderSeeking = "GENDER" + genderSeeking;

			string retVal = GenderText(_brandID, genderSelf) + SeekingText(_brandID) + GenderText(_brandID, genderSeeking);

			return(retVal);

		}

		private static string GenderText(int brandID, int genderMaskPart)
		{
				/*
			// Gender Mask
			public const int GENDERID_MALE = 1;
			public const int GENDERID_FEMALE = 2;
			public const int GENDERID_SEEKING_MALE = 4;
			public const int GENDERID_SEEKING_FEMALE = 8;
			public const int GENDERID_MTF = 16;
			public const int GENDERID_FTM = 32;
			public const int GENDERID_SEEKING_MTF = 64;
			public const int GENDERID_SEEKING_FTM = 128;
			*/

			string retVal = "[gender text]";

			if(genderMaskPart == 1 || genderMaskPart == 4)
			{
				retVal = MaleText(brandID);
			}
			else if(genderMaskPart == 2 || genderMaskPart == 8)
			{
				retVal = FemaleText(brandID);
			}
			else if(genderMaskPart == 16 || genderMaskPart == 64)
			{
				retVal = MaleToFemaleText(brandID);
			}
			else if(genderMaskPart == 32 || genderMaskPart == 128)
			{
				retVal = FemaleToMaleText(brandID);
			}

			return(retVal);
		}

		private static string MaleText(int brandID)
		{
			string retVal = "[male]";

			switch(DetermineLanguageFromBrandID(brandID))
			{
				case(Language.English):
					retVal = "Male";
					break;
				case(Language.German):
					retVal = "[german male]";
					break;
				case(Language.Hebrew):
					retVal = "[hebrew male]";
					break;
			}

			return(retVal);
		}

		private static string FemaleText(int brandID)
		{
			string retVal= "[female]";

			switch(DetermineLanguageFromBrandID(brandID))
			{
				case(Language.English):
					retVal = "Female";
					break;
				case(Language.German):
					retVal = "[german female]";
					break;
				case(Language.Hebrew):
					retVal = "[hebrew female]";
					break;
			}

			return(retVal);
		}

		private static string MaleToFemaleText(int brandID)
		{
			string retVal = "[male turning female]";

			switch(DetermineLanguageFromBrandID(brandID))
			{
				case(Language.English):
					retVal = "Male Turning Female";
					break;
				case(Language.German):
					retVal = "[german male turning female]";
					break;
				case(Language.Hebrew):
					retVal = "[hebrew male turning female]";
					break;
			}

			return(retVal);
		}

		private static string FemaleToMaleText(int brandID)
		{
			string retVal = "[female turning male]";

			switch(DetermineLanguageFromBrandID(brandID))
			{
				case(Language.English):
					retVal = "Female Turning Male";
					break;
				case(Language.German):
					retVal = "[german female turning male]";
					break;
				case(Language.Hebrew):
					retVal = "[hebrew female turning male]";
					break;
			}

			return(retVal);
		}

		private static string SeekingText(int brandID)
		{
			string retVal = " [seeking] ";;

			switch(DetermineLanguageFromBrandID(brandID))
			{
				case(Language.English):
					retVal = " seeking ";
					break;
				case(Language.German):
					retVal = " [german seeking] ";
					break;
				case(Language.Hebrew):
					retVal = " [hebrew seeking] ";
					break;
			}

			return(retVal);
		}

		private static Language DetermineLanguageFromBrandID(int brandID)
		{
			//	TODO - Refactor this to use the built-in language and make it
			//	the same code as is used in SMTPMessage.MailLanguage

			Language retVal = Language.English;

			switch(brandID)
			{
				case 1001:	//	AmericanSingles.com
					retVal = Language.English;
					break;
				case 1002:	//	Glimpse.com
					retVal = Language.English;
					break;
				case 1003:	//	JDate.com
					retVal = Language.English;
					break;
				case 1015:	//	Cupid.co.il
					retVal = Language.Hebrew;
					break;
				case 10012:	//	CollegeLuv.com
					retVal = Language.English;
					break;
			}

			return(retVal);
		}

		private string DetermineSearchUrl(Matchnet.Member.ServiceAdapters.Member member)
		{
			StringBuilder sb = new StringBuilder();
			SearchPreferenceCollection prefs  = SearchPreferencesSA.Instance.GetSearchPreferences(member.MemberID, _brand.Site.Community.CommunityID);

			sb.Append("/Applications/Search/SearchResults.aspx?");

			for(int i = 0; i < SEARCH_FIELDS_FOR_URL.Length; i++)
			{
				if(i > 0)
				{
					sb.Append("&");
				}
				sb.Append(HttpUtility.UrlEncode(SEARCH_FIELDS_FOR_URL[i]));
				sb.Append("=");
				sb.Append(HttpUtility.UrlEncode(prefs[SEARCH_FIELDS_FOR_URL[i]]));
			}

			//	Make the correct gender mask fields
			int genderMask = Convert.ToInt32(prefs["GenderMask"]);

			sb.Append("&GenderID=");
			sb.Append(GetGenderMaskSelf(genderMask));
			sb.Append("&SeekingGenderID=");
			sb.Append(GetGenderMaskSeeking(genderMask));

			return(sb.ToString());
		}

		public static int GetGenderMaskSelf(int genderMask)
		{
			int maskSelf = ConstantsTemp.GENDERID_MALE + ConstantsTemp.GENDERID_FEMALE + ConstantsTemp.GENDERID_MTF + ConstantsTemp.GENDERID_FTM;
			return genderMask & maskSelf;
		}

		public static int GetGenderMaskSeeking(int genderMask)
		{
			int maskSeeking = ConstantsTemp.GENDERID_SEEKING_MALE + ConstantsTemp.GENDERID_SEEKING_FEMALE + ConstantsTemp.GENDERID_SEEKING_MTF + ConstantsTemp.GENDERID_SEEKING_FTM;
			return genderMask & maskSeeking;
		}

		#endregion


		/// <summary>
		/// Remove any unneeded characters from the value (right now just tabs)
		/// </summary>
		/// <param name="theObject">object to convert to a sanitized string</param>
		/// <returns>sanitized version of the object's string representation</returns>
		private string Filter(object theObject)
		{
			string retVal = string.Empty;
			if(theObject != null)
			{
				retVal = theObject.ToString();
				retVal = retVal.Replace(TAB, string.Empty);
				retVal = retVal.Replace("'", "\\");
				retVal = retVal.Replace("\"", "\\\"");
			}

			return(retVal);
		}
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		
	}

	public enum Language
	{
		English = 1,
		German = 2,
		Hebrew = 3
	}
}
