using System;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

namespace Matchnet.ExternalMail.Templates.PhotoApproved
{
	/// <summary>
	/// Summary description for PhotoApprovedBase.
	/// </summary>
	public class PhotoApprovedBase : TemplateBase
	{
		private int _brandID;
		private Brand _brand;
		private int _memberID;
		private int _photoID;
		private Matchnet.Member.ServiceAdapters.Member _member;
		
		protected System.Web.UI.WebControls.Label lblBrandID;
		protected System.Web.UI.WebControls.Label lblMemberID;
		protected System.Web.UI.WebControls.Label lblPhotoID;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			_brandID = Convert.ToInt32(Request.QueryString["BrandID"]);
			_brand = BrandConfigSA.Instance.GetBrands().GetBrand(_brandID);
			_memberID = Convert.ToInt32(Request.QueryString["MemberID"]);
			_member = MemberSA.Instance.GetMember(_memberID, MemberLoadFlags.None);
			_photoID = Convert.ToInt32(Request.QueryString["PhotoID"]);

			lblBrandID.Text = _brandID.ToString();
			lblMemberID.Text = _memberID.ToString();
			lblPhotoID.Text = _photoID.ToString();

			base.MessageInfo.FromAddress = "memberservices@" + _brand.Uri;
			base.MessageInfo.FromName = "memberservices@" + _brand.Uri;
			base.MessageInfo.ToAddress = _member.EmailAddress;
			base.MessageInfo.Subject = "Member Information";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
