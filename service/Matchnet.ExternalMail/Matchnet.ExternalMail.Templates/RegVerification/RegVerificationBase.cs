using System;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

using Matchnet.ExternalMail.Templates.CommonControls;

namespace Matchnet.ExternalMail.Templates.RegVerification
{
	/// <summary>
	/// Summary description for RegVerificationBase.
	/// </summary>
	public class RegVerificationBase : TemplateBase
	{
		private int _brandID;
		private int _memberID;
		private Brand _brand;
		protected Matchnet.Member.ServiceAdapters.Member _member;

		protected System.Web.UI.WebControls.Label lblBrandDescription;
		protected System.Web.UI.WebControls.Label lblBrandName;
		protected System.Web.UI.WebControls.Label lblBrandName2;
		protected System.Web.UI.WebControls.Label lblBrandName3;
		protected System.Web.UI.WebControls.Label lblEmailSettings;
		protected System.Web.UI.WebControls.HyperLink lnkMemberServices;
		protected System.Web.UI.HtmlControls.HtmlAnchor lnkEmailSettings;
		protected System.Web.UI.WebControls.TextBox txtVerifyCode;

		public RegVerificationBase()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			_brandID = Convert.ToInt32(Request.QueryString["BrandID"]);
			_memberID = Convert.ToInt32(Request.QueryString["MemberID"]);
			_brand = BrandConfigSA.Instance.GetBrands().GetBrand(Convert.ToInt32(Request.QueryString["BrandID"]));
			_member = MemberSA.Instance.GetMember(_memberID, MemberLoadFlags.None);

			lblBrandDescription.Text = _brand.Site.Name;
			lblBrandName.Text = _brand.Site.Name;
			lblBrandName2.Text = _brand.Site.Name;
			lblBrandName3.Text = _brand.Site.Name;

			txtVerifyCode.Text = EmailVerifyUtils.EncryptQueryString(_member.EmailAddress);

			string emailUrl = "http://" + _brand.Site.DefaultHost + "." + _brand.Uri + "/Applications/MemberServices/EmailSettings.aspx";
			lblEmailSettings.Text = emailUrl;
			lnkEmailSettings.HRef = emailUrl;

			string memberServicesUrl = "http://" + _brand.Site.DefaultHost + "." + _brand.Uri  + "Applications/MemberServices/MemberServices.aspx";;
			lnkMemberServices.Text = memberServicesUrl;
			lnkMemberServices.Target = memberServicesUrl;

			base.MessageInfo.FromAddress = "VerifyEmail@" + _brand.Uri;
			base.MessageInfo.FromName = "VerifyEmail@" + _brand.Uri;
			base.MessageInfo.ToAddress = _member.EmailAddress;
			base.MessageInfo.Subject = "Activate your Membership";
		}
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

	}
}
