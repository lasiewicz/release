<%@ Register TagPrefix="cc" Namespace="Matchnet.ExternalMail.Templates.CommonControls" Assembly="Matchnet.ExternalMail.Templates" %>
<%@ Page language="c#" Codebehind="Site100.aspx.cs" AutoEventWireup="false" Inherits="Matchnet.ExternalMail.Templates.MatchMail.Site100" %>
<html>
	<head>
		<title>Your matches from
			<cc:txt runat="server" id="txtTitleSiteName" text="[txtTitleSiteName]" /></title>
	</head>
	<body dir="ltr">
		<form id="Form1" method="post" runat="server">
			<table cellspacing="0" cellpadding="0" width="526" border="0">
				<tr>
					<td valign="top" align="center" width="621">
						<table cellspacing="0" cellpadding="0" width="100%" border="0">
							<tr>
								<td>
									<b><font face="verdana" color="#0000ff" size="2">Welcome
											<cc:txt runat="server" id="txtUsernameWelcome" text="[txtUsernameWelcome]" />!</font></b></td>
								<td><p align="right"><font face="verdana" size="1"><cc:txt runat="server" id="txtSetFrequency" text="Set frequency" href="/default.asp?p=18090" />
											|
											<cc:txt runat="server" id="txtPasswordHelp" text="Password Help" href="/default.asp?p=2021" />
											|
											<cc:txt runat="server" id="txtLogin" text="Login" href="/default.asp?p=2000" /></font></p>
								</td>
							</tr>
						</table>
						<table cellspacing="1" cellpadding="0" width="135" bgcolor="#962a2a" border="0">
							<tr>
								<td valign="middle" align="center" width="623" bgcolor="#ffffff" colspan="2"><img src="http://stage02.spark.com/img/p/1/27/logo.gif" border="0" width="599"
										height="32"></td>
							</tr>
							<tr>
								<td valign="top" align="left" width="330" bgcolor="#ffffff" height="221" rowspan="2">
									<table bordercolor="#edede6" cellspacing="1" cellpadding="0" border="0" width="100%">
										<!-- begin matches -->
										<asp:repeater runat="server" id="MatchRepeater">
											<itemtemplate>
												<tr>
													<td>
														<table border="0" cellpadding="0" cellspacing="1" bgcolor="#d0cfc4" width="100%">
															<tr>
																<td>
																	<table cellspacing="0" cellpadding="2" width="100%" bgcolor="#f8f8f2" border="0">
																		<tr>
																			<td valign="middle" align="center" rowspan="4">
																				<table cellspacing="0" cellpadding="0" border="0">
																					<tr>
																						<td><a href="http://stage02.spark.com/default.asp?p=7070&amp;MemberID=988264972"><img src="http://stage02.spark.com/img/d/1/7/nophoto70x91.gif" border="0"></a></td>
																					</tr>
																					<tr>
																						<td><img src="http://stage02.spark.com/img/p/1/trans.gif"><a href="http://stage02.spark.com/default.asp?p=7070&amp;MemberID=988264972"><img src="http://stage02.spark.com/img/p/1/27/btn_viewProfile.gif" border="0"
																									width="80" height="16"></a></td>
																					</tr>
																				</table>
																			</td>
																			<td valign="middle" align="left" width="100%"><font face="verdana" size="2"><b>
																						<cc:txt runat="server" id="txtMatchUsername" text="[txtMatchUsername]" href="p=7070&MemberID=" /></b></font></td>
																		</tr>
																		<tr>
																			<td valign="middle" align="left" width="100%"><font face="verdana" size="2"><b>Age:</b>
																					<cc:txt runat="server" id="txtMatchAge" text="[txtMatchAge]" /></font></td>
																		</tr>
																		<tr>
																			<td valign="middle" align="left" width="100%"><font face="verdana" size="2"><b>From:</b>
																					<cc:txt runat="server" id="txtMatchRegion" text="[txtMatchRegion]" /></font></td>
																		</tr>
																		<tr>
																			<td valign="middle" align="left" width="100%"><font color="#0000ff" face="verdana" size="2">
																					<cc:txt runat="server" id="txtMatchMore" text="more &gt;&gt;" href="/default.asp?p=7070&MemberID=" /></font></td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</itemtemplate>
										</asp:repeater>
										<!-- end matches -->
									</table>
								</td>
								<td valign="top" align="center" width="318" bgcolor="#ffffff" height="1"><div align="center"><center>
											<table cellspacing="4" cellpadding="0" border="0">
												<tr>
													<td valign="middle" align="center" bgcolor="#ffffff"><div align="center"><center>
																<table bordercolor="#d0cfc4" cellspacing="0" bgcolor="#edede6" cellpadding="0" width="98%"
																	border="1">
																	<tr>
																		<td valign="middle" align="center"><p align="center"><img src="http://stage02.spark.com/img/p/1/27/head_howOthersSeeYou.gif"></p>
																		</td>
																	</tr>
																	<tr>
																		<td valign="middle" align="center">
																			<table border="0" cellpadding="0" cellspacing="1" bgcolor="#d0cfc4" width="100%">
																				<tr>
																					<td>
																						<table cellspacing="0" cellpadding="2" width="100%" bgcolor="#f8f8f2" border="0">
																							<tr>
																								<td valign="middle" align="center" rowspan="4">
																									<table cellspacing="0" cellpadding="0" border="0">
																										<tr>
																											<td><a href="http://stage02.spark.com/default.asp?p=7070&amp;MemberID=987657146"><img src="http://stage02.spark.com/img/d/1/7/nophoto70x91.gif" border="0"></a></td>
																										</tr>
																										<tr>
																											<td><img src="http://stage02.spark.com/img/p/1/trans.gif"><a href="http://stage02.spark.com/default.asp?p=7070&amp;MemberID=987657146"><img src="http://stage02.spark.com/img/p/1/27/btn_viewProfile.gif" border="0"
																														width="80" height="16"></a></td>
																										</tr>
																									</table>
																								</td>
																								<td valign="middle" align="left" width="100%"><font face="verdana" size="2"><b><cc:txt runat="server" id="txtYouUsername" text="[txtYouUsername]" href="/default.asp?p=7070&amp;MemberID=" /></b></font></td>
																							</tr>
																							<tr>
																								<td valign="middle" align="left" width="100%"><font face="verdana" size="2"><b>Age:</b>
																										<cc:txt runat="server" id="txtYouAge" text="[txtYouAge]" /></font></td>
																							</tr>
																							<tr>
																								<td valign="middle" align="left" width="100%"><font face="verdana" size="2"><b>From:</b>
																										<cc:txt runat="server" id="txtYouRegion" text="[txtYouRegion]" /></font></td>
																							</tr>
																							<tr>
																								<td valign="middle" align="left" width="100%"><font face="verdana" size="2"></font> <a href="http://stage02.spark.com/default.asp?p=7070&amp;MemberID=987657146">
																										<font color="#0000ff" face="verdana" size="2">more &gt;&gt;</font></a></td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																			<font face="verdana" size="2"><strong>Is it the best it can be?<br>
																					Would <i>you<i> go out with <i>you</i>?</strong></font> </I></I>
																		</td>
																	</tr>
																	<tr>
																		<td valign="middle" align="center"><font face="verdana" size="2">Take 5 minutes to 
																				spice up your profile today and improve your chances of catching someone's eye!</font></td>
																	</tr>
																</table>
															</center>
														</div>
													</td>
												</tr>
												<tr>
													<td valign="middle" align="center" bgcolor="#ffffff">
														<table cellpadding="0" width="100%" border="0">
															<tr>
																<td valign="middle" align="center"><center><a href="http://stage02.spark.com//Applications/MemberProfile/MemberPhotoUpload.aspx"><font face="verdana" size="1">Add/Update 
																			Photo</a></center>
																	</FONT></td>
																<td valign="middle" align="center"><center><a href="http://stage02.spark.com//default.asp?p=7080"><font face="verdana" size="1">Make 
																			your profile rock!</a></center>
																	</FONT></td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</center>
									</div>
								</td>
							</tr>
							<tr>
								<td align="center" width="50%" bgcolor="#ffffff" height="322">
									<table height="145" cellspacing="4" cellpadding="0" width="100%" border="0">
										<tr>
											<td valign="middle" align="center" width="100%" height="141"><p align="center"><a href="http://stage02.spark.com/default.asp?p=26000"><img src="http://stage02.spark.com/img/p/1/27/head_hotlists.gif" height="22"
															width="98" border="0"></a><br>
													<b><font face="verdana" size="1">Members you're interested in!</font></b><br>
													<br>
													<font face="verdana" size="2"><a href="http://stage02.spark.com/default.asp?p=26000">
															Members on your hot list [0]</a><br>
														<a href="http://stage02.spark.com/default.asp?p=26000&amp;CategoryID=-7">Members 
															you have contacted [0]</a><br>
														<a href="http://stage02.spark.com/default.asp?p=26000&amp;CategoryID=-6">Members 
															you have teased [0]</a><br>
														<a href="http://stage02.spark.com/default.asp?p=26000&amp;CategoryID=-8">Members 
															you have IM'd [0]</a><br>
														<a href="http://stage02.spark.com/default.asp?p=26000&amp;CategoryID=-9">Members 
															you have viewed [0]</a></font></p>
												<p align="center"><a href="http://stage02.spark.com/default.asp?p=18090"><img src="http://stage02.spark.com/img/p/1/27/teaser1.gif" height="48" width="291"
															border="0"></a><br>
													<font face="verdana" size="2"><b>Your Matches</b> emails give you<br>
														a quick peek at the members<br>
														that match your preferences.<br>
														<a href="http://stage02.spark.com/default.asp?p=18090">To change how 
															often you<br>
															receive your matches, click here</a></font></p>
												<p align="center"><a href="http://stage02.spark.com/default.asp?p=18150"><img src="http://stage02.spark.com/img/p/1/27/teaser2.gif" height="48" width="291"
															border="0"></a><br>
													<font face="verdana" size="2">Decide where your profile<br>
														shows up and who gets to see you.<br>
														<a href="http://stage02.spark.com/default.asp?p=18150">Click here for 
															your settings</a></font></p>
												<p align="center"><a href="http://stage02.spark.com/default.asp?GenderID=1&amp;SeekingGenderID=8&amp;GenderMask=6&amp;MinAge=18&amp;MaxAge=26&amp;HasPhotoFlag=1&amp;OrderBy=InsertDate+desc&amp;a=22500&amp;p=1000&amp;SearchTypeID=4&amp;Distance=60&amp;CountryRegionID=223&amp;StateRegionID=1538&amp;City=Beverly+Hills&amp;MinHeight=-2147483647&amp;MaxHeight=-2147483647&amp;SynagogueAttendance=-2147483647&amp;JDateEthnicity=-2147483647&amp;KeepKosher=-2147483647&amp;JDateReligion=-2147483647&amp;MaritalStatus=-2147483647&amp;EducationLevel=-2147483647&amp;Religion=-2147483647&amp;Ethnicity=-2147483647&amp;SmokingHabits=-2147483647&amp;DrinkingHabits=-2147483647&amp;SexualIdentityType=-2147483647&amp;BodyType=-2147483647&amp;Zodiac=-2147483647"><img src="http://stage02.spark.com/img/p/1/27/teaser3.gif" height="48" width="200"
															border="0"></a><br>
													<font face="verdana" size="2">Want to see who else we found for you?<br>
														<a href="http://stage02.spark.com/default.asp?GenderID=1&amp;SeekingGenderID=8&amp;GenderMask=6&amp;MinAge=18&amp;MaxAge=26&amp;HasPhotoFlag=1&amp;OrderBy=InsertDate+desc&amp;a=22500&amp;p=1000&amp;SearchTypeID=4&amp;Distance=60&amp;CountryRegionID=223&amp;StateRegionID=1538&amp;City=Beverly+Hills&amp;MinHeight=-2147483647&amp;MaxHeight=-2147483647&amp;SynagogueAttendance=-2147483647&amp;JDateEthnicity=-2147483647&amp;KeepKosher=-2147483647&amp;JDateReligion=-2147483647&amp;MaritalStatus=-2147483647&amp;EducationLevel=-2147483647&amp;Religion=-2147483647&amp;Ethnicity=-2147483647&amp;SmokingHabits=-2147483647&amp;DrinkingHabits=-2147483647&amp;SexualIdentityType=-2147483647&amp;BodyType=-2147483647&amp;Zodiac=-2147483647">
															Click here for MORE fabulous<br>
															profiles and photos</a></font></p>
											</td>
										</tr>
										<tr>
											<td align="center">&nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="100%" colspan="2">
									<table cellspacing="0" cellpadding="0" width="100%" bgcolor="#e6e6e6" border="0">
										<tr>
											<td width="572" bgcolor="#e6e6e6"><p align="center"><font face="verdana" size="2"><a href="http://stage02.spark.com/default.asp?p=18090">To 
															change your mail settings or stop receiving our email, <b>click here</b>.</a></font></p>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<p align="center"><font face="arial" color="#808080" size="1">Copyright � 2004 MatchNet 
								plc. All rights reserved. Spark.com is a registered trademark of 
								MatchNet plc.</font></p>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
