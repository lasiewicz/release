using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

using Matchnet.ExternalMail.Templates.CommonControls;

namespace Matchnet.ExternalMail.Templates.MatchMail
{
	public class Site101 : TemplateBase
	{		
		protected ArrayList _memberIDList;
		protected Brand _brand;
		protected Matchnet.Member.ServiceAdapters.Member _member;

		protected Txt txtTitleSiteName;
		protected Txt txtUsernameWelcome;
		protected Txt txtYouUsername;
		protected Txt txtYouAge;
		protected Txt txtYouRegion;
		protected Matchnet.ExternalMail.Templates.CommonControls.Txt txtSetFrequency;
		protected Matchnet.ExternalMail.Templates.CommonControls.Txt txtPasswordHelp;
		protected Matchnet.ExternalMail.Templates.CommonControls.Txt txtLogin;
		protected Repeater MatchRepeater;

		private void Page_Load(object sender, System.EventArgs e)
		{
			_memberIDList = new ArrayList(Request.QueryString["MemberIDList"].Split(','));
			_brand = BrandConfigSA.Instance.GetBrands().GetBrand(Convert.ToInt32(Request.QueryString["BrandID"]));
			//_member = MemberSA.Instance.GetMember(_brand.Site.Community.CommunityID,
			//	_brand.Site.SiteID,
			//	_brand.Site.LanguageID,
			//	Convert.ToInt32(Convert.ToInt32(Request.QueryString["MemberID"])),
			//	MemberLoadFlags.None);
			_member = MemberSA.Instance.GetMember(Convert.ToInt32(Convert.ToInt32(Request.QueryString["MemberID"])), MemberLoadFlags.None);

			txtTitleSiteName.Text = _brand.Site.Name;
			txtUsernameWelcome.Text = _member.Username;
			txtYouUsername.Text = _member.Username;
			txtYouUsername.Href = txtYouUsername.Href + _member.MemberID.ToString();

			//todo
			/*
			txtYouAge.Text = ;
			txtYouRegion.Text = ;
			*/
			
			MatchRepeater.DataSource = _memberIDList;
			MatchRepeater.DataBind();

			base.MessageInfo.FromAddress = "matches@americansingles.com";
			base.MessageInfo.FromName = "matches@americansingles.com";
			base.MessageInfo.ToAddress = _member.EmailAddress;
			base.MessageInfo.Subject = "Your matches from Americansingles.com";
		}

		private void MatchRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			Int32 memberID = Convert.ToInt32(e.Item.DataItem);
			//Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(_brand.Site.Community.CommunityID,
			//	_brand.Site.SiteID,
			//	_brand.Site.LanguageID,
			//	memberID,
			//	MemberLoadFlags.None);
			Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

			Txt txtMatchUsername = (Txt)e.Item.FindControl("txtMatchUsername");
			txtMatchUsername.Text = member.Username;
			txtMatchUsername.Href = txtMatchUsername.Href + member.MemberID.ToString() + "&prtid=" + PurchaseReasonType.YourMatchesEmail;

			Txt txtMatchAge = (Txt)e.Item.FindControl("txtMatchAge");
			//txtMatchAge.Text = //todo;

			Txt txtMatchRegion = (Txt)e.Item.FindControl("txtMatchRegion");
			//txtMatchRegion.Text = //todo;

			Txt txtMatchMore = (Txt)e.Item.FindControl("txtMatchMore");
			txtMatchMore.Href = txtMatchMore.Href + member.MemberID.ToString() + "&prtid=" + PurchaseReasonType.YourMatchesEmail;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.MatchRepeater.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.MatchRepeater_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

	}
}
