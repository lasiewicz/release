<%@ Register TagPrefix="cc" Namespace="Matchnet.ExternalMail.Templates.CommonControls" Assembly="Matchnet.ExternalMail.Templates" %>
<%@ Page language="c#" Codebehind="OptOutNotifyBase.cs" AutoEventWireup="false" Inherits="Matchnet.ExternalMail.Templates.OptOutNotify.OptOutNotifyBase" %>
<html>
	<head>
		<title>Opt Out</title>
	</head>
	<body dir="ltr">
		<form id="Form1" method="post" runat="server">
			
<br />
<p>
By clicking on the link below you can verify the opt out:

<asp:HyperLink id="lnkOptOut" runat="server" />
</p>

		</form>
	</body>
</html>
