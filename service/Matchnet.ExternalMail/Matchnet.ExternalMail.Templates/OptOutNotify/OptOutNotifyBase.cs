using System;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.ExternalMail.Templates.OptOutNotify
{
	/// <summary>
	/// Summary description for OptOutNotifyBase.
	/// </summary>
	public class OptOutNotifyBase : TemplateBase
	{
		private int _brandID;
		private Brand _brand;
		private string _email;
		private string _fromHost;

		protected System.Web.UI.WebControls.HyperLink lnkOptOut;

		private void Page_Load(object sender, System.EventArgs e)
		{
			
			_brandID = Convert.ToInt32(Request.QueryString["BrandID"]);
			_brand = BrandConfigSA.Instance.GetBrands().GetBrand(Convert.ToInt32(Request.QueryString["BrandID"]));
			_email = Request.QueryString["Email"];
			_fromHost = Request.QueryString["FromHost"];

			int rand = new Random().Next();
			
			string url = "http://" + _fromHost + "/OptOut/EmailConfirm.aspx?email=" + 
				_email + "&tag=" + rand.ToString();
			lnkOptOut.NavigateUrl = url;
			lnkOptOut.Text = "Click here";

			base.MessageInfo.FromAddress = "support@" + _brand.Uri;
			base.MessageInfo.FromName = "support@" + _brand.Uri;
			base.MessageInfo.ToAddress = _email;
			base.MessageInfo.Subject = "Opt Out response required";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
