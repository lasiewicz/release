using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

using Matchnet.ExternalMail.Templates.CommonControls;

namespace Matchnet.ExternalMail.Templates.CollegeLuvPromo
{
	public class CollegeLuvPromoBase : TemplateBase
	{		
		protected int _brandID;
		protected Brand _brand;
		protected int _memberID;

		protected Label lblBrandID;
		protected Label lblMemberID;
		protected Matchnet.Member.ServiceAdapters.Member _member;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			_brandID = Convert.ToInt32(Request.QueryString["BrandID"]);
			_brand = BrandConfigSA.Instance.GetBrands().GetBrand(_brandID);
			_memberID = Convert.ToInt32(Request.QueryString["MemberID"]);
			_member = MemberSA.Instance.GetMember(_memberID, MemberLoadFlags.None);

			lblBrandID.Text = _brandID.ToString();
			lblMemberID.Text = _memberID.ToString();

			base.MessageInfo.FromAddress = "memberservices@" + _brand.Uri;
			base.MessageInfo.FromName = "memberservices@" + _brand.Uri;
			base.MessageInfo.ToAddress = _member.EmailAddress;
			// base.MessageInfo.Subject = "How about CollegeLuv?";
			base.MessageInfo.Subject = Localizer.GetStringResource("SUBJECT", this, _brand);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

	}
}
