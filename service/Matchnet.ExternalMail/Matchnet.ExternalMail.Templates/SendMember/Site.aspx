<%@ Register TagPrefix="cc" Namespace="Matchnet.ExternalMail.Templates.CommonControls" Assembly="Matchnet.ExternalMail.Templates" %>
<%@ Page language="c#" Codebehind="SendMemberBase.cs" AutoEventWireup="false" Inherits="Matchnet.ExternalMail.Templates.SendMember.SendMemberBase" %>
<html>
	<head>
		<title></title>
	</head>
	<body topmargin="0" leftmargin="0" bgcolor="#ffffff">
		<form id="Form1" method="post" runat="server">

Dear <asp:Label id="lblToName" runat="server" />,<br />

<p>
<asp:Label id="lblMessage" runat="server" />
</p>

<p>
<asp:HyperLink id="lnkDestination" runat="server" />
</p>

<p>
<asp:Label id="lblBrandname" runat="server" /><br />
Online dating, friendship, chat, pictures, profiles and more!
</p>

		</form>
	</body>
</html>
