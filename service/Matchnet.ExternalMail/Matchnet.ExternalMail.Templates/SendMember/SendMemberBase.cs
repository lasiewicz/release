using System;
using System.Text;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.ExternalMail.Templates.SendMember
{
	/// <summary>
	/// Summary description for SendMemberBase.
	/// </summary>
	public class SendMemberBase : TemplateBase
	{
		private int _brandID;
		private Brand _brand;
		private string _friendName;
		private string _friendEmail;
		private string _userEmail;
		private int _sentMemberID;
		private string _subject;
		private string _message;

		protected System.Web.UI.WebControls.Label lblToName;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.HyperLink lnkDestination;
		protected System.Web.UI.WebControls.Label lblBrandname;

		private void Page_Load(object sender, System.EventArgs e)
		{
			_brandID = Convert.ToInt32(Request.QueryString["BrandID"]);
			_brand = BrandConfigSA.Instance.GetBrands().GetBrand(Convert.ToInt32(Request.QueryString["BrandID"]));

			_friendName = Request.QueryString["FriendName"];
			_friendEmail = Request.QueryString["FriendEmail"];
			_userEmail = Request.QueryString["UserEmail"];
			_sentMemberID = Convert.ToInt32(Request.QueryString["SentMemberID"]);
			_subject = Request.QueryString["Subject"];
			_message = Request.QueryString["Message"];

			lblToName.Text = _friendName;
			lblMessage.Text = _message;
			lblBrandname.Text = _brand.Site.Name;

			StringBuilder sb = new StringBuilder();

			sb.Append("http://");
			sb.Append(_brand.Site.DefaultHost);
			sb.Append(".");
			sb.Append(_brand.Uri);
			sb.Append("/Applications/MemberProfile/ViewProfile.aspx?MemberID=");
			sb.Append(_sentMemberID);
			string theUrl = sb.ToString();

			lnkDestination.Text = theUrl;
			lnkDestination.NavigateUrl = theUrl;

			base.MessageInfo.FromAddress = _userEmail;
			base.MessageInfo.FromName = _userEmail;
			base.MessageInfo.ToAddress = _friendEmail;
			base.MessageInfo.Subject = _subject;
		}
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		
	}
}
