using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

using Matchnet.ExternalMail.Templates.CommonControls;

namespace Matchnet.ExternalMail.Templates.RegConfirmation
{
	public class Site : TemplateBase
	{		
		protected Brand _brand;
		protected string _emailAddress;
		protected string _password;

		protected Label lblEmail;
		protected Label lblPassword;
		protected Label lblBrandName;
		// protected Matchnet.Member.ServiceAdapters.Member _member;

		private void Page_Load(object sender, System.EventArgs e)
		{
			_brand = BrandConfigSA.Instance.GetBrands().GetBrand(Convert.ToInt32(Request.QueryString["BrandID"]));
			_emailAddress = Request.QueryString["EmailAddress"];
			_password = MemberSA.Instance.GetPassword(_emailAddress);

			lblEmail.Text = _emailAddress;
			lblPassword.Text = _password;
			lblBrandName.Text = _brand.Site.Name;

			// _member = MemberSA.Instance.GetMember(Convert.ToInt32(Convert.ToInt32(Request.QueryString["MemberID"])), MemberLoadFlags.None);

			base.MessageInfo.FromAddress = "memberservices@spark.com";
			base.MessageInfo.FromName = "memberservices@spark.com";
			base.MessageInfo.ToAddress = _emailAddress;
			base.MessageInfo.Subject = "Member Information";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

	}
}
