using System;
using System.Collections;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Email.ValueObjects;
using Matchnet.Exceptions;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;
using Matchnet.ExternalMail.ValueObjects.ServiceDefinitions;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.RemotingClient;
using Matchnet.Configuration.ServiceAdapters;
using MSA = Matchnet.Member.ServiceAdapters;
using Matchnet.MembersOnline.ServiceAdapters;
using System.Collections.Generic;

namespace Matchnet.ExternalMail.ServiceAdapters
{
    /// <summary>
    /// 
    /// </summary>
    public class ExternalMailSA : SABase, IExternalMailSA
    {
        [Flags]
        public enum SMSAlertMask
        {
            None = 0,
            GotClick = 1,
            NewEmail = 2,
            NewECard = 4,
            HotListed = 8,
            NewFlirt = 16
        }

        /// <summary>
        /// 
        /// </summary>
        public static readonly ExternalMailSA Instance = new ExternalMailSA();

        #region Constructors

        private ExternalMailSA()
        {
        }

        #endregion
        private static ISettingsSA _settingsService;

        public static ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService) _settingsService = RuntimeSettings.Instance;
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        private const string EMAIL_VOLUME_REGULATION_BY_LAST_LOGGED_ON_DATE_SETTING =
            "EmailVolumeRegulationByLastLoggedonDate";

        private const string ATTRIBUTE_BRAND_LAST_LOGON_DATE = "BrandLastLogonDate";

        #region Public Methods

        /// <summary>
        /// Checks to see if the passed in SMS alert mask value is set or not
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="maskToCheck"></param>
        /// <returns></returns>
        public bool IsSMSAlertNeeded(int memberID, SMSAlertMask maskToCheck, Brand brand)
        {
            // If 4INFO is not even enabled on the target brand, no need to go further
            string fourInfoEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("4INFO_ENABLED",
                                                                                                       brand.Site.
                                                                                                           Community.
                                                                                                           CommunityID,
                                                                                                       brand.Site.SiteID,
                                                                                                       brand.BrandID);
            if (fourInfoEnabled.ToLower() == "false")
                return false;

            IMemberDTO member = GetIMemberDTO(memberID, MemberLoadFlags.None);
            bool isSmsAlertCapable = false;

            // Make sure there is a valid phone number that's confirmed
            string phoneNumber = member.GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID,
                                                         brand.BrandID, brand.Site.LanguageID, "SMSPhone",
                                                         Matchnet.Constants.NULL_STRING);
            bool isConfirmed = member.GetAttributeBool(brand.Site.Community.CommunityID, brand.Site.SiteID,
                                                       brand.BrandID, "SMSPhoneIsValidated");

            if (phoneNumber != Matchnet.Constants.NULL_STRING && phoneNumber != string.Empty && isConfirmed)
                isSmsAlertCapable = true;

            // Now check to see if the user wants to be notified for this type of alert
            int smsAlertPref = member.GetAttributeInt(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID,
                                                      "SMSAlertPreference", 0);

            return (((smsAlertPref & (int)maskToCheck) == (int)maskToCheck) && isSmsAlertCapable);
        }

        public bool IsSMSAlertNeeded(int memberID, Brand brand, SMSAlertsDefinitions.MessageType messageType,
                                     bool isFriend)
        {
            bool result = false;

            try
            {
                string sMSAlertsEnabled =
                    Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SMS_ALERTS_ENABLED",
                                                                                      brand.Site.Community.CommunityID,
                                                                                      brand.Site.SiteID, brand.BrandID);

                if (sMSAlertsEnabled.ToLower() == "true")
                {
                    result = true;
                }

                if (result)
                {

                    IMemberDTO member = GetIMemberDTO(memberID, MemberLoadFlags.None);
                    string phoneNumber = member.GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID,
                                                                 brand.BrandID, brand.Site.LanguageID, "SMSPhone",
                                                                 Matchnet.Constants.NULL_STRING);
                    bool isConfirmed = member.GetAttributeBool(brand.Site.Community.CommunityID, brand.Site.SiteID,
                                                               brand.BrandID, "SMSPhoneIsValidated");
                    bool isMemberOnline =
                        MembersOnline.ServiceAdapters.MembersOnlineSA.Instance.IsMemberOnline(
                            brand.Site.Community.CommunityID, memberID);
                    if (isConfirmed && phoneNumber != Matchnet.Constants.NULL_STRING && phoneNumber != string.Empty)
                    {
                        string uri = string.Empty;


                        uri = getServiceManagerUri();
                        base.Checkout(uri);
                        try
                        {
                            result = getService(uri).NewInboxItemReceived(memberID, brand.Site.SiteID, messageType,
                                                                          isFriend);
                            if (isMemberOnline)
                            // SMS Alerts on new email should not be sent if the target member is online.
                            {
                                result = false;
                            }
                        }
                        finally
                        {
                            base.Checkin(uri);
                        }

                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new SAException("Error using IsSMSAlertNeeded().  Message was: " + ex.Message, ex);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Email Volume Regulation.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="brand"></param>
        /// <returns>True - do not send</returns>
        public bool IsRegulatedByLastLogonDate(int memberID, Brand brand)
        {
            int regulationDays =
                Convert.ToInt32(
                    Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(
                        EMAIL_VOLUME_REGULATION_BY_LAST_LOGGED_ON_DATE_SETTING,
                        brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));

            if (regulationDays > 0)
            {
                IMemberDTO member = GetIMemberDTO(memberID, MemberLoadFlags.None);

                DateTime lastLoginDate =
                    Convert.ToDateTime(member.GetAttributeDate(brand, ATTRIBUTE_BRAND_LAST_LOGON_DATE, DateTime.MinValue));

                if (lastLoginDate == DateTime.MinValue)
                {
                    new ServiceBoundaryException(Matchnet.ExternalMail.ValueObjects.ServiceConstants.SERVICE_NAME,
                                                 "MemberID " + memberID.ToString() +
                                                 ": no brandlastlogondate attribute found. Email is not regulated.");

                    // No last login date is found for this member. Do not regulate.
                    return false;
                }
                else
                {
                    TimeSpan span = DateTime.Now.Subtract(lastLoginDate);

                    if (span.Days >= regulationDays)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                // If setting value is 0, then do not regulate.
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void GetConnectionLimit()
        {
            base.MaxConnections =
                Convert.ToByte(
                    Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_SA_CONNECTION_LIMIT"));
        }

        public bool SendFTAWarnings(int pMemberId, int pBrandId, string[] pWarnings)
        {
            return EnqueueMailObj(new FTAWarningsImpulse(pMemberId, pBrandId, pWarnings), pMemberId, pBrandId);
        }

        public bool SendAllAccessNudgeEmail(int pSenderMemberId, int pSenderBrandId, int pMessageListId,
                                            int pRecipientMemberId, int pRecipientBrandId, string pEmailSubject,
                                            string pEmailContent, DateTime pInitialMailTime)
        {
            return
                EnqueueMailObj(
                    new AllAccessEmailNudgeImpulse(pSenderMemberId, pSenderBrandId, pMessageListId, pRecipientMemberId,
                                                   pRecipientBrandId, pEmailSubject, pEmailContent, pInitialMailTime),
                    pRecipientMemberId, pRecipientBrandId);
        }

        public bool SendAllAccessInitialEmail(int pSenderMemberId, int pSenderBrandId, int pMessageListId,
                                              int pRecipientMemberId, int pRecipientBrandId, string pEmailSubject,
                                              string pEmailContent)
        {
            return
                EnqueueMailObj(
                    new AllAccessInitialEmailImpulse(pSenderMemberId, pSenderBrandId, pMessageListId, pRecipientMemberId,
                                                     pRecipientBrandId, pEmailSubject, pEmailContent),
                    pRecipientMemberId, pRecipientBrandId);
        }

        public bool SendAllAccessReplyToInitialEmail(int pSenderMemberId, int pSenderBrandId, int pMessageListId,
                                                     int pRecipientMemberId, int pRecipientBrandId, string pEmailSubject,
                                                     string pEmailContent)
        {
            return
                EnqueueMailObj(
                    new AllAccessReplyEmailImpulse(pSenderMemberId, pSenderBrandId, pMessageListId, pRecipientMemberId,
                                                   pRecipientBrandId, pEmailSubject, pEmailContent), pRecipientMemberId,
                    pRecipientBrandId);
        }

        public bool SendAllAccessNoThanksEmail(int pSenderMemberId, int pSenderBrandId, int pRecipientMemberId,
                                               int pRecipientBrandId)
        {
            return
                EnqueueMailObj(
                    new AllAccessNoThanksEmailImpulse(pSenderMemberId, pSenderBrandId, pRecipientMemberId,
                                                      pRecipientBrandId), pRecipientMemberId, pRecipientBrandId);
        }

        public bool SendAllAccessReturnEmail(int pSenderMemberId, int pSenderBrandId, int pRecipientMemberId,
                                             int pRecipientBrandId)
        {
            return
                EnqueueMailObj(
                    new AllAccessReturnEmailImpulse(pSenderMemberId, pSenderBrandId, pRecipientMemberId,
                                                    pRecipientBrandId), pRecipientMemberId, pRecipientBrandId);
        }

        public bool SendBOGONotificationEmail(int senderMemberId, int senderBrandID, string recipientEmailAddress,
                                              string giftPromo, string giftCode)
        {
            EnqueueMailObj(new BOGONotificationImpulse(senderMemberId, senderBrandID, recipientEmailAddress,
                                                       giftPromo, giftCode));
            return true;
        }

        public void SendProfileChangedConfirmationEmail(int memberId, int brandID, string emailAddress, string firstName,
                                                        string userName, List<string> fields)
        {
            Brand brand = Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
            if (brand != null && memberId > 0)
            {
                string isEnabled = "false";
                try
                {
                    isEnabled =
                        Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(
                            "ENABLE_PROFILE_CHANGE_CONFIRMATION", brand.Site.Community.CommunityID, brand.Site.SiteID,
                            brand.BrandID);
                }
                catch (Exception ex)
                {
                    //setting missing
                    isEnabled = "false";
                }

                if (isEnabled.ToLower().Trim() == "true")
                {
                    EnqueueMailObj(new ProfileChangedConfirmationImpulse(memberId, brandID, firstName, userName,
                                                                         emailAddress, fields));
                }
            }
        }

        /// <summary>
        /// Sends an email to users with credit card had a hard decline during subscription renewal period.
        /// </summary>
        /// <param name="siteID">Site id of the website the system is trying to renew.</param>
        /// <param name="userName">Username of the member.</param>
        /// <returns></returns>
        public bool SendRenewalFailureEmail(int siteID, string userName, string emailAddress)
        {
            EnqueueMailObj(new RenewalFailureNotificationImpulse(siteID, userName, emailAddress));
            return true;
        }

        /// <summary>
        /// Sends an email containing information about the Report Abuse to CS.
        /// Does not require good email address
        /// </summary>
        /// <returns></returns>
        public bool SendReportAbuseEmail(int brandID, string ReportContent)
        {
            EnqueueMailObj(new ReportAbuseImpulse(brandID, ReportContent));
            return true;
        }


        /// <summary>
        /// Sends a confirmation to the members that their registration has
        /// been successful
        /// and asking them to verify their email address
        /// 
        /// See SendVerificationLetter in ContextGlobal.cs:582
        /// Requires good email
        /// </summary>
        /// <returns></returns>
        public bool SendRegistrationVerification(int memberID, int brandID, int siteID)
        {
            return EnqueueMailObj(new RegVerificationImpulse(memberID, brandID), memberID, brandID);
        }


        /// <summary>
        /// Sends a request to the member to verify their email address.
        /// This can be called either as the result of a member action or
        /// an admin action.
        /// 
        /// See SendVerifyEmailLetter in ContextGlobal.cs:651
        /// Requires good email
        /// </summary>
        /// <param name="memberID">member ID to send the verification to</param>
        /// <param name="brandID">brand ID for the site to which the email verification pertains</param>
        /// <param name="isFromAdmin">whether or not the email was initiated by an administrator</param>
        public bool SendEmailVerification(int memberID, int brandID, bool isFromAdmin)
        {
            return EnqueueMailObj(new EmailVerificationImpulse(memberID, brandID), memberID, brandID);
        }


        /// <summary>
        /// Sends a request to the member to verify their email address.
        /// This is the result of a member action.
        /// 
        /// See SendVerifyEmailLetter in ContextGlobal.cs:651
        /// Requires good email
        /// </summary>
        /// <param name="memberID">member ID to send the verification to</param>
        /// <param name="brandID">brand ID for the site to which the email verification pertains</param>
        public bool SendEmailVerification(int memberID, int brandID)
        {
            return SendEmailVerification(memberID, brandID, false);
        }


        /// <summary>
        /// Sends the password for a given email address to that email address
        /// 
        /// See SendMemberPassword in ContextGlobal.cs:701
        /// Requires good email
        /// </summary>
        /// <param name="brandID">The brand id of the current brand</param>
        /// <param name="emailAddress">email address for the member who needs
        /// to receive their password</param>
        public bool SendPassword(int brandID, string emailAddress)
        {
            var brand = BrandConfigSA.Instance.GetBrandByID(brandID);
            int memberID = MemberSA.Instance.GetMemberIDByEmail(emailAddress, brand.Site.Community.CommunityID);
            return EnqueueMailObj(new PasswordMailImpulse(brandID, emailAddress), memberID, brandID);
        }

        /// <summary>
        /// Sends an email with the link to reset the member's password
        /// </summary>
        /// <param name="brandId"></param>
        /// <param name="memberId"></param>
        /// <param name="resetPasswordUrl">Reset password page link with the token value in the url</param>
        /// <returns></returns>
        public bool ResetPassword(int brandId, int memberId, string resetPasswordUrl)
        {
            return EnqueueMailObj(new ResetPasswordImpulse(brandId, memberId, resetPasswordUrl), memberId, brandId, true);
        }

        /// <summary>
        /// Send a notice after a successful check subscription payment.
        /// 
        /// See SendSuccessEmail in SubscribeByCheck.ascx.cs:354
        /// and SendSuccessEmail in SubscribeByDirectDebit.ascx.cs:276
        /// 
        /// This particular email does not appear to be contained in the Excel
        /// spreadsheet.
        /// Requires good email
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="brandID"></param>
        /// <param name="memberPaymentID"></param>
        /// <param name="planDescription"></param>
        public bool SendCheckSubscriptionConfirmation(int memberID, int brandID, int memberPaymentID,
                                                      string planDescription)
        {
            return EnqueueMailObj(new CheckSubConfirmImpulse(memberID, brandID, memberPaymentID, planDescription),
                                  memberID, brandID);
        }


        /// <summary>
        /// Not entirely sure what this email does but it appears to send an
        /// opt-out notice to a member that they perhaps then have to confirm
        /// via clicking on a link in an email?
        /// 
        /// See btnOptOut_Click in OptOut.ascx.cs:96
        /// NO LONGER IN USE.  Optout is handled by StormPost, this method is no longer in use
        /// Does not require good email address
        /// </summary>
        public void SendOptOutNotification(int brandID, string email, string fromHost)
        {
            EnqueueMailObj(new OptOutNotifyImpulse(brandID, email, fromHost));
        }


        /// <summary>
        /// Send an email to Spark.Net personnel based on a user ContactUs
        /// form submission.
        /// See Go_Click in ContactUs.ascx.cs:77
        /// Does not require good email address
        /// </summary>
        /// <param name="brandID">brand of the site that was the source of the contact</param>
        /// <param name="userName">user-supplied username</param>
        /// <param name="fromEmailAddress">user-supplied email address</param>
        /// <param name="sMemberID">user-supplied MemberID</param>
        /// <param name="browser">browser ID string from request</param>
        /// <param name="remoteIP">remote IP for the request</param>
        /// <param name="reason">drop-down selected reason</param>
        /// <param name="userComment">user free form comment</param>
        /// <param name="appDeviceInfo">The application device information.</param>
        /// <param name="contactUsEmail">to email address</param>
        public void SendContactUs(int brandID, string userName, string fromEmailAddress, string sMemberID,
                                  string browser, string remoteIP, string reason, string userComment, string appDeviceInfo = null, string contactUsEmail = null)
        {
            EnqueueMailObj(new ContactUsImpulse(brandID, userName, fromEmailAddress, sMemberID, browser, remoteIP,
                                                reason, userComment, appDeviceInfo, contactUsEmail));
        }

        /// <summary>
        /// For sending internal emails within the company. Use for admin related activities.
        /// Initially created for supporting subscription admin.
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="fromAddress"></param>
        /// <param name="toAddress"></param>
        /// <param name="toName"></param>
        /// <param name="body"></param>
        /// <param name="subject"></param>
        public void SendInternalCorpMail(int brandID, string fromAddress, string toAddress, string body, string toName,
                                         string subject)
        {
            EnqueueMailObj(new InternalCorpMail(brandID, fromAddress, toAddress, body, toName, subject));
        }


        /// <summary>
        /// Send a link to a member to a (potentially external) email address.
        /// 
        /// See btnSendToFriend_Click in SendToFriend.ascx.cs:138
        /// Does not require good email address
        /// </summary>
        /// <param name="friendName">user-supplied friend's name</param>
        /// <param name="friendEmail">user-supplied friend's email</param>
        /// <param name="userEmail">user's email address</param>
        /// <param name="sentMemberID">member ID whose info is being sent</param>
        /// <param name="subject">user-supplied subject</param>
        /// <param name="message">user-supplied message</param>
        /// <param name="brandID">source brandID</param>
        public bool SendMemberToFriend(string friendName, string friendEmail, string userEmail, int sentMemberID,
                                       string subject, string message, int brandID)
        {
             return SendMemberToFriend(friendName, friendEmail, userEmail, sentMemberID, subject, message,
                                                 brandID, Constants.NULL_INT);
            return true;
        }

        public bool SendMemberToFriend(string friendName, string friendEmail, string userEmail, int sentMemberID,
                                       string subject, string message, int brandID, int sendingMemberID)
        {
            
            EnqueueMailObj(new SendMemberImpulse(friendName, friendEmail, userEmail, sentMemberID, subject, message,
                                            brandID, sendingMemberID));
            return true;
        }

        /// <summary>
        /// Sends a member's color code link to friend
        /// </summary>
        /// <param name="friendName"></param>
        /// <param name="friendEmail"></param>
        /// <param name="senderEmail"></param>
        /// <param name="senderName"></param>
        /// <param name="senderMemberID"></param>
        /// <param name="subject"></param>
        /// <param name="message"></param>
        /// <param name="brandID"></param>
        /// <returns></returns>
        public bool SendColorCodeToFriend(string friendName,
                                          string friendEmail,
                                          string senderEmail,
                                          string senderName,
                                          int senderMemberID,
                                          string subject,
                                          string message,
                                          int brandID)
        {
            EnqueueMailObj(new SendMemberColorCodeImpulse(friendName, friendEmail, senderEmail, senderName,
                                                          senderMemberID, subject, message, brandID));
            return true;
        }

        public bool SendColorCodeQuizInvite(string recipientEmail,
                                            string recipientUserName,
                                            string senderEmail,
                                            int senderMemberID,
                                            string senderUserName,
                                            int brandID)
        {
            EnqueueMailObj(new SendMemberColorCodeQuizInviteImpulse(recipientEmail, recipientUserName, senderEmail,
                                                                    senderMemberID, senderUserName, brandID));
            return true;
        }

        public bool SendColorCodeQuizConfirmation(int memberID,
                                                  string emailAddress,
                                                  int brandID,
                                                  string color,
                                                  string memberUserName)
        {
            EnqueueMailObj(new ColorCodeQuizConfirmationImpulse(memberID, emailAddress, brandID, color, memberUserName));
            return true;
        }


        /// <summary>
        /// Send a promo email inviting a member to join the CollegeLuv site
        /// Requires good email
        /// </summary>
        /// <param name="memberID">member to send the email to</param>
        /// <param name="brandID">original brand where the user registered</param>
        public bool SendCollegeLuvPromo(int memberID, int brandID)
        {
            return EnqueueMailObj(new CollegeLuvPromoImpulse(memberID, brandID), memberID, brandID);
        }


        /// <summary>
        /// Send an email to a user when they clicked Yes for a member who has also
        /// clicked Yes for them.
        /// Requires good email
        /// </summary>
        /// <param name="memberID">member to receive the email</param>
        /// <param name="targetMemberID">other member who participated in the mutual yes</param>
        /// <param name="brandID">brand where the vote occurred</param>
        public bool SendMutualYesNotification(int memberID, int targetMemberID, int brandID)
        {
            return EnqueueMailObj(new MutualYesNotifyImpulse(memberID, targetMemberID, brandID), memberID, brandID);
        }


        /// <summary>
        /// Send an email to a user who has received an internal mail from another member.
        /// Requires good email
        /// </summary>
        public bool SendInternalMailNotification(EmailMessage emailMessage, int brandID, int unreadEmailCount)
        {
            brandID = CheckRecipientBrand(brandID, emailMessage.ToMemberID);

            return EnqueueMailObj(new InternalMailNotifyImpulse(emailMessage, brandID, unreadEmailCount),
                                  emailMessage.ToMemberID, brandID);
        }


        /// <summary>
        /// Send an email to a member to let them know that an administrator has reviewed
        /// and approved one of their uploaded photos.
        /// Requires good email
        /// </summary>
        /// <param name="memberID">member who owns the approved photo</param>
        /// <param name="photoID">identifier for photo that has been approved</param>
        /// <param name="brandID">brand where the photo was uploaded</param>
        /// <param name="approvalStatus"></param>
        public bool SendPhotoApprovedNotification(int memberID, int photoID, int brandID, string approvalStatus)
        {
            return EnqueueMailObj(new PhotoApprovedImpulse(memberID, photoID, brandID, approvalStatus), memberID,
                                  brandID);
        }

        /// Send an email to a member to let them know that an administrator has reviewed
        /// and approved one of their uploaded photos.
        /// Requires good email
        /// </summary>
        /// <param name="memberID">member who owns the approved photo</param>
        /// <param name="photoID">identifier for photo that has been approved</param>
        /// <param name="brandID">brand where the photo was uploaded</param>
        /// <param name="approvalStatus"></param>
        public bool SendPhotoApprovedNotification(int memberID, int photoID, int brandID, string approvalStatus,
                                                  bool sendCaptionRejectedNotification)
        {
            bool res = EnqueueMailObj(new PhotoApprovedImpulse(memberID, photoID, brandID, approvalStatus), memberID,
                                      brandID);
            if (sendCaptionRejectedNotification)
            {
                res = EnqueueMailObj(new PhotoCaptionRejectedImpulse(memberID, photoID, brandID), memberID, brandID);
            }
            return res;
        }

        /// Send an email to a member to let them know that an administrator has reviewed
        /// and rejected one of their  photos caption.
        /// Requires good email
        /// </summary>
        /// <param name="memberID">member who owns the approved photo</param>
        /// <param name="photoID">identifier for photo that has been approved</param>
        /// <param name="brandID">brand where the photo was uploaded</param>
        /// <param name="approvalStatus"></param>
        public bool SendPhotoCaptionRejectNotification(int memberID, int photoID, int brandID)
        {

            bool res = EnqueueMailObj(new PhotoCaptionRejectedImpulse(memberID, photoID, brandID), memberID, brandID);

            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID">member to receive the email</param>
        /// <param name="hotListedByMemberID">member who has hotlisted the member receiving the email</param>
        /// <param name="brandID">brand where the hot list adding occurred</param>
        /// <returns></returns>
        public bool SendHotListedNotification(int memberID, int hotListedByMemberID, int brandID)
        {
            brandID = CheckRecipientBrand(brandID, memberID);

            return EnqueueMailObj(new HotListedNotifyImpulse(memberID, hotListedByMemberID, brandID), memberID, brandID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID">member to receive the email</param>
        /// <param name="viewedByMemberID">member who has viewed the member</param>
        /// <param name="numberOfMembersWhoViewedSinceLastEmail">number of members who have viewed the member receiving the email</param>
        /// <param name="brandID">brand where the viewing occurred</param>
        /// <returns></returns>
        public bool SendViewedYouEmail(int memberID, 
            int viewedByMemberID,
            int numberOfMembersWhoViewedSinceLastEmail, 
            int brandID)
        {
            brandID = CheckRecipientBrand(brandID, memberID);
            return EnqueueMailObj(new ViewedYouEmailImpulse(memberID, viewedByMemberID, numberOfMembersWhoViewedSinceLastEmail, brandID), memberID, brandID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID">member to receive the email</param>
        /// <param name="requestedByMemberID">member who is sending the request email</param>
        /// <param name="brandID">brand where the request occurred</param>
        /// <returns></returns>
        public bool SendNoEssayRequestEmail(int memberID,
            int requestedByMemberID,
            int brandID)
        {
            brandID = CheckRecipientBrand(brandID, memberID);
            return EnqueueMailObj(new NoEssayRequestEmailImpulse(memberID, requestedByMemberID, brandID), memberID, brandID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="siteID"></param>
        /// <param name="brandID"></param>
        /// <param name="userName"></param>
        /// <param name="emailAddress"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="address1"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="postalCode"></param>
        /// <param name="dateOfPurchase"></param>
        /// <param name="planType"></param>
        /// <param name="currencyType"></param>
        /// <param name="initialCost"></param>
        /// <param name="initialDuration"></param>
        /// <param name="initialDurationType"></param>
        /// <param name="renewCost"></param>
        /// <param name="renewDuration"></param>
        /// <param name="renewDurationType"></param>
        /// <param name="last4CC"></param>
        /// <param name="creditCardTypeID"></param>
        /// <param name="confirmationNumber"></param>
        /// <returns></returns>
        public bool SendSubscriptionConfirmation(int memberID, int siteID, int brandID, string userName,
                                                 string emailAddress, string firstName, string lastName, string address1,
                                                 string city,
                                                 string state,
                                                 string postalCode,
                                                 DateTime dateOfPurchase,
                                                 int planType,
                                                 int currencyType,
                                                 decimal initialCost,
                                                 int initialDuration,
                                                 DurationType initialDurationType,
                                                 decimal renewCost,
                                                 int renewDuration,
                                                 DurationType renewDurationType,
                                                 string last4CC,
                                                 int creditCardTypeID,
                                                 string confirmationNumber)
        {
            return EnqueueMailObj(new SubscriptionConfirmationImpulse(memberID,
                                                                      siteID,
                                                                      brandID,
                                                                      userName,
                                                                      emailAddress,
                                                                      firstName,
                                                                      lastName,
                                                                      address1,
                                                                      city,
                                                                      state,
                                                                      postalCode,
                                                                      dateOfPurchase,
                                                                      planType,
                                                                      currencyType,
                                                                      initialCost,
                                                                      initialDuration,
                                                                      initialDurationType,
                                                                      renewCost,
                                                                      renewDuration,
                                                                      renewDurationType,
                                                                      last4CC,
                                                                      creditCardTypeID,
                                                                      confirmationNumber), memberID, brandID);
        }

        public bool SendSubscriptionConfirmation(int memberID, int siteID,
                                                 string firstName, string lastName, string address1,
                                                 string city,
                                                 string state,
                                                 string postalCode,
                                                 DateTime dateOfPurchase,
                                                 int planType,
                                                 int currencyType,
                                                 decimal initialCost,
                                                 int initialDuration,
                                                 DurationType initialDurationType,
                                                 decimal renewCost,
                                                 int renewDuration,
                                                 DurationType renewDurationType,
                                                 string last4CC,
                                                 int creditCardTypeID,
                                                 string confirmationNumber)
        {
            Matchnet.Content.ValueObjects.BrandConfig.Brands brands = BrandConfigSA.Instance.GetBrandsBySite(siteID);
            Matchnet.Content.ValueObjects.BrandConfig.Brand lowestBrand = null;
            int lowestBrandID = Constants.NULL_INT;
            foreach (Matchnet.Content.ValueObjects.BrandConfig.Brand brand in brands)
            {
                if (lowestBrandID == Constants.NULL_INT)
                {
                    lowestBrandID = brand.BrandID;
                }
                else if (brand.BrandID < lowestBrandID)
                {
                    // The assumption is that the lowest brand ID for all the brands
                    // of a site will provide the default brand ID for the site 
                    // In the case that the lowest brand ID is not the default brand 
                    // ID for the site, then the correct plan will still be retrieved.
                    // However, a collection of plans for a new brand ID will be cached.  
                    lowestBrandID = brand.BrandID;
                }
            }

            lowestBrand = Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(lowestBrandID);

            IMemberDTO member = GetIMemberDTO(memberID, MemberLoadFlags.None);

            int brandID = lowestBrand.BrandID;
            string userName = member.GetUserName(lowestBrand);
            string emailAddress = member.EmailAddress;

            return SendSubscriptionConfirmation(memberID, siteID, brandID, userName, emailAddress, firstName, lastName,
                                                address1, city, state, postalCode, dateOfPurchase, planType,
                                                currencyType, initialCost, initialDuration,
                                                initialDurationType, renewCost, renewDuration, renewDurationType,
                                                last4CC, creditCardTypeID, confirmationNumber);

        }

        public bool SendOneOffPurchaseConfirmation(int memberID,
                int siteID,
            //int brandID, 
            //string userName, 
            //string emailAddress, 
                string firstName,
                string lastName,
                DateTime dateOfPurchase,
            //int planType,
                int currencyType,
                decimal initialCost,
                int initialDuration,
                DurationType initialDurationType,
                string last4CC,
                int creditCardTypeID,
                string confirmationNumber)
        {
            Matchnet.Content.ValueObjects.BrandConfig.Brands brands = BrandConfigSA.Instance.GetBrandsBySite(siteID);
            Matchnet.Content.ValueObjects.BrandConfig.Brand lowestBrand = null;
            int lowestBrandID = Constants.NULL_INT;
            foreach (Matchnet.Content.ValueObjects.BrandConfig.Brand brand in brands)
            {
                if (lowestBrandID == Constants.NULL_INT)
                {
                    lowestBrandID = brand.BrandID;
                }
                else if (brand.BrandID < lowestBrandID)
                {
                    // The assumption is that the lowest brand ID for all the brands
                    // of a site will provide the default brand ID for the site 
                    // In the case that the lowest brand ID is not the default brand 
                    // ID for the site, then the correct plan will still be retrieved.
                    // However, a collection of plans for a new brand ID will be cached.  
                    lowestBrandID = brand.BrandID;
                }
            }
            lowestBrand = Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(lowestBrandID);

            IMemberDTO member = GetIMemberDTO(memberID, MemberLoadFlags.None);

            return EnqueueMailObj(new OneOffPurchaseConfirmationImpulse(memberID,
                siteID,
                lowestBrand.BrandID,
                member.GetUserName(lowestBrand),
                member.EmailAddress,
                firstName,
                lastName,
                dateOfPurchase,
                //planType,
                currencyType,
                initialCost,
                initialDuration,
                initialDurationType,
                last4CC,
                creditCardTypeID,
                confirmationNumber), memberID, lowestBrand.BrandID);
        }

        /// <summary>
        /// send payment profile confirmation
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="siteID"></param>
        /// <param name="brandID"></param>
        /// <param name="emailAddress"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="dateOfPurchase"></param>
        /// <param name="last4CC"></param>
        /// <param name="creditCardTypeID"></param>
        /// <param name="confirmationNumber"></param>
        /// <returns></returns>
        public bool SendPaymentProfileConfirmation(int memberID,
                                                    int siteID,
                                                    int brandID,
                                                    string emailAddress,
                                                    string firstName,
                                                    string lastName,
                                                    DateTime dateOfPurchase,
                                                    string last4CC,
                                                    string confirmationNumber)
        {
            return EnqueueMailObj(new PmtProfileConfirmationImpulse(memberID,
                                                                    siteID,
                                                                    brandID,
                                                                    emailAddress,
                                                                    firstName,
                                                                    lastName,
                                                                    dateOfPurchase,
                                                                    last4CC,
                                                                    confirmationNumber), memberID, brandID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="siteID"></param>
        /// <param name="memberID"></param>
        /// <param name="boardName"></param>
        /// <param name="topicSubject"></param>
        /// <param name="replyURL"></param>
        /// <param name="newReplies"></param>
        /// <param name="unsubscribeURL"></param>
        /// <returns></returns>
        public bool SendMessageBoardInstantNotification(int brandID, int siteID, int memberID, string boardName, string topicSubject, string replyURL, int newReplies, string unsubscribeURL)
        {
            return EnqueueMailObj(new MessageBoardInstantNotificationImpulse(brandID,
                                                                             siteID,
                                                                             memberID,
                                                                             boardName,
                                                                             topicSubject,
                                                                             replyURL,
                                                                             newReplies,
                                                                             unsubscribeURL), memberID, brandID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="siteID"></param>
        /// <param name="memberID"></param>
        /// <param name="boardName"></param>
        /// <param name="topicSubject"></param>
        /// <param name="newReplies"></param>
        /// <param name="boardID"></param>
        /// <param name="threadID"></param>
        /// <param name="lastMessageID"></param>
        /// <param name="unsubscribeURL"></param>
        /// <returns></returns>
        public bool SendMessageBoardDailyUpdates(int brandID, int siteID, int memberID, string boardName, string topicSubject, int newReplies, int boardID, int threadID, int lastMessageID, string unsubscribeURL)
        {
            return EnqueueMailObj(new MessageBoardDailyUpdatesImpulse(brandID,
                                                                      siteID,
                                                                      memberID,
                                                                      boardName,
                                                                      topicSubject,
                                                                      newReplies,
                                                                      boardID,
                                                                      threadID,
                                                                      lastMessageID,
                                                                      unsubscribeURL), memberID, brandID);
        }

        public bool SendEcardToMember(int brandID, int siteID, int senderMemberID
            , string cardUrl, string cardThumbnail, int recipientMemberID, DateTime sentDate)
        {
            return EnqueueMailObj(new EcardToMemberImpulse(brandID,
                siteID,
                senderMemberID,
                cardUrl,
                cardThumbnail,
                recipientMemberID,
                sentDate), senderMemberID, brandID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="siteID"></param>
        /// <param name="memberID"></param>
        /// <param name="senderUsername"></param>
        /// <param name="senderAge"></param>
        /// <param name="senderLocation"></param>
        /// <param name="senderThumbnail"></param>
        /// <param name="cardUrl"></param>
        /// <param name="cardThumbnail"></param>
        /// <param name="recipientEmailAddress"></param>
        /// <param name="recipientUsername"></param>
        /// <param name="sentDate"></param>
        /// <returns></returns>
        public bool SendEcardToFriend(int brandID, int siteID, int senderMemberID, string cardUrl
            , string cardThumbnail, string recipientEmailAddress, DateTime sentDate)
        {
            return EnqueueMailObj(new EcardToFriendImpulse(brandID,
                siteID,
                senderMemberID,
                cardUrl,
                cardThumbnail,
                recipientEmailAddress,
                sentDate), senderMemberID, brandID);
        }


        /// <summary>
        /// Send an email to a member to let them know that an administrator has reviewed
        /// and rejected one of their uploaded photos.
        /// Requires good email
        /// </summary>
        /// <param name="memberID">member who owns the rejected photo</param>
        /// <param name="photoID">identifier for photo that has been rejected</param>
        /// <param name="brandID">brand where the photo was uploaded</param>
        /// <param name="comment">administrator's comments about why the image was unacceptable</param>
        public bool SendPhotoRejectedNotification(int memberID, int photoID, int brandID, string comment)
        {
            return EnqueueMailObj(new PhotoRejectedImpulse(memberID, photoID, brandID, comment), memberID, brandID);
        }


        /// <summary>
        /// Send a viral email
        /// Requires good email
        /// </summary>
        /// <param name="brandID">brandid for the current brand</param>
        /// <param name="emailAddress">email address of the recipiient</param>
        /// <param name="miniprofileInfo">receipient's miniprofile</param>
        /// <param name="miniprofileInfoCollection">list of members to be presented to the recipient</param>
        /// <returns>true if success, false otherwise</returns>
        public bool SendViralEmail(int brandID, String emailAddress,
                                   MiniprofileInfo miniprofileInfo,
                                   MiniprofileInfoCollection miniprofileInfoCollection)
        {
            //			int memberID = MemberSA.Instance.GetMemberIDByEmail(emailAddress);
            return EnqueueMailObj(new ViralImpulse(brandID, emailAddress, miniprofileInfo, miniprofileInfoCollection), miniprofileInfo.MemberID, brandID);
        }


        /// <summary>
        /// Send a match email
        /// Requires good email
        /// </summary>
        /// <param name="brandID">brand for which the email is being sent</param>
        /// <param name="emailAddress">member who will receive the email</param>
        /// <param name="miniprofileInfo">member miniprofile who will receive the email</param>
        /// <param name="miniprofileInfoCollection">list of members to be presented to the recipient</param>
        public bool SendMatchMail(int brandID, String emailAddress,
                                  MiniprofileInfo miniprofileInfo,
                                  MiniprofileInfoCollection miniprofileInfoCollection)
        {
            //			int memberID = MemberSA.Instance.GetMemberIDByEmail(emailAddress);
            return EnqueueMailObj(new MatchMailImpulse(brandID, emailAddress, miniprofileInfo, miniprofileInfoCollection), miniprofileInfo.MemberID, brandID);
        }

        /// <summary>
        /// Send a "new members in your area" email
        /// Requires good email
        /// </summary>
        /// <param name="brandID">brand for which the email is being sent</param>
        /// <param name="emailAddress">member who will receive the email</param>
        /// <param name="miniprofileInfo">member miniprofile who will receive the email</param>
        /// <param name="miniprofileInfoCollection">list of members to be presented to the recipient</param>
        public bool SendNewMemberMail(int brandID, String emailAddress,
            MiniprofileInfo miniprofileInfo,
            MiniprofileInfoCollection miniprofileInfoCollection)
        {
            //			int memberID = MemberSA.Instance.GetMemberIDByEmail(emailAddress);
            return EnqueueMailObj(new NewMemberMailImpulse(brandID, emailAddress, miniprofileInfo, miniprofileInfoCollection), miniprofileInfo.MemberID, brandID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="receiverSiteID"></param>
        /// <param name="receiverMemberID"></param>
        /// <param name="senderMemberID"></param>
        /// <param name="senderBrandID"></param>
        /// <param name="receiverBrandID"></param>
        /// <param name="targetPhoneNumber"></param>
        /// <returns></returns>
        public bool SendNewEmailSMSAlert(int receiverSiteID, int receiverMemberID, int senderMemberID, int senderBrandID, int receiverBrandID, string targetPhoneNumber)
        {
            return EnqueueMailObj(new NewEmailSMSAlertImpulse(receiverBrandID, receiverSiteID, receiverMemberID, senderMemberID, targetPhoneNumber), senderMemberID, senderBrandID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="receiverSiteID"></param>
        /// <param name="receiverMemberID"></param>
        /// <param name="senderMemberID"></param>
        /// <param name="senderBrandID"></param>
        /// <param name="receiverBrandID"></param>
        /// <param name="targetPhoneNumber"></param>
        /// <returns></returns>
        public bool SendHotListedSMSAlert(int receiverSiteID, int receiverMemberID, int senderMemberID, int senderBrandID, int receiverBrandID, string targetPhoneNumber)
        {
            return EnqueueMailObj(new HotListedSMSAlertImpulse(receiverBrandID, receiverSiteID, receiverMemberID, senderMemberID, targetPhoneNumber), senderMemberID, senderBrandID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="receiverSiteID"></param>
        /// <param name="receiverMemberID"></param>
        /// <param name="senderMemberID"></param>
        /// <param name="senderBrandID"></param>
        /// <param name="receiverBrandID"></param>
        /// <param name="targetPhoneNumber"></param>
        /// <returns></returns>
        public bool SendNewFlirtSMSAlert(int receiverSiteID, int receiverMemberID, int senderMemberID, int senderBrandID, int receiverBrandID, string targetPhoneNumber)
        {
            return EnqueueMailObj(new NewFlirtSMSAlertImpulse(receiverBrandID, receiverSiteID, receiverMemberID, senderMemberID, targetPhoneNumber), senderMemberID, senderBrandID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="receiverSiteID"></param>
        /// <param name="receiverMemberID"></param>
        /// <param name="senderMemberID"></param>
        /// <param name="senderBrandID"></param>
        /// <param name="receiverBrandID"></param>
        /// <param name="targetPhoneNumber"></param>
        /// <returns></returns>
        public bool SendClickedSMSAlert(int receiverSiteID, int receiverMemberID, int senderMemberID, int senderBrandID, int receiverBrandID, string targetPhoneNumber)
        {
            return EnqueueMailObj(new ClickedSMSAlertImpulse(receiverBrandID, receiverSiteID, receiverMemberID, senderMemberID, targetPhoneNumber), senderMemberID, senderBrandID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="receiverSiteID"></param>
        /// <param name="receiverMemberID"></param>
        /// <param name="senderMemberID"></param>
        /// <param name="senderBrandID"></param>
        /// <param name="receiverBrandID"></param>
        /// <param name="targetPhoneNumber"></param>
        /// <returns></returns>
        public bool SendNewECardSMSAlert(int receiverSiteID, int receiverMemberID, int senderMemberID, int senderBrandID, int receiverBrandID, string targetPhoneNumber)
        {
            return EnqueueMailObj(new NewECardSMSAlertImpulse(receiverBrandID, receiverSiteID, receiverMemberID, senderMemberID, targetPhoneNumber), senderMemberID, senderBrandID);
        }

        /// <summary>
        /// Although the method name says Send, it's only doing the logging of a validation SMS send for now.
        /// </summary>
        /// <param name="siteID"></param>
        /// <param name="receivingMemberID"></param>
        /// <param name="sendingMemberID"></param>
        /// <param name="brandID"></param>
        /// <param name="targetPhoneNumber"></param>
        /// <returns></returns>
        public bool SendValidationSMSAlert(int siteID, int receivingMemberID, int sendingMemberID, int brandID, string targetPhoneNumber)
        {
            return EnqueueMailObj(new ValidationSMSAlertImpulse(brandID, siteID, receivingMemberID, sendingMemberID, targetPhoneNumber), sendingMemberID, brandID);
        }
        /// <summary>
        /// Sends the compatibility test invitation mail
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="memberName"></param>
        /// <param name="eMail"></param>
        /// <returns></returns>
        //		public bool SendCompatibilityTestInvitation(int brandID, string memberName, string eMail)
        //		{
        //			return  EnqueueMailObj(new CompatibilityTestInvitation(brandID, memberName, eMail));
        //		}

        public bool SendMatchMeterInvitationMail(int brandID, int siteID, int senderMemberID, string senderUsername, string emailAddress, string recipientEmailAddress)
        {
            return EnqueueMailObj(new MatchMeterInvitationMailImpulse(brandID, siteID, senderMemberID, senderUsername, emailAddress, recipientEmailAddress), senderMemberID, brandID);
        }
        public bool SendMatchMeterCongratsMail(int brandID, int siteID, int senderMemberID, string senderUsername, string emailAddr)
        {
            return EnqueueMailObj(new MatchMeterCongratsMailImpulse(brandID, siteID, senderMemberID, senderUsername, emailAddr), senderMemberID, brandID);
        }

        public bool SendAuthCodeSMSAlert(int brandID, int siteID, int receivingMemberID, string targetPhoneNumber, string authCode)
        {
            return EnqueueMailObj(new AuthCodeSMSAlertImpulse(brandID, siteID, receivingMemberID, targetPhoneNumber, authCode), receivingMemberID, brandID);
        }

        public bool SendRegistrationSuccessSMSAlert(int brandID, int siteID, int receivingMemberID, string targetPhoneNumber)
        {
            return EnqueueMailObj(new RegistrationSuccessSMSAlertImpulse(brandID, siteID, receivingMemberID, targetPhoneNumber), receivingMemberID, brandID);
        }

        /// <summary>
        /// Sends a member's quesiton and answer link to friend
        /// </summary>
        /// <param name="friendName"></param>
        /// <param name="friendEmail"></param>
        /// <param name="senderEmail"></param>
        /// <param name="senderName"></param>
        /// <param name="senderMemberID"></param>
        /// <param name="subject"></param>
        /// <param name="message"></param>
        /// <param name="brandID"></param>
        /// <param name="questionId"></param>
        /// <param name="questionText"></param>
        /// <returns></returns>
        public bool SendMemberQuestion(string friendName,
            string friendEmail,
            string senderEmail,
            string senderName,
            int senderMemberID,
            string subject,
            string message,
            int brandID,
            int questionId,
            string questionText)
        {
            EnqueueMailObj(new SendMemberQuestionAndAnswerImpulse(friendName, friendEmail, senderEmail, senderName, senderMemberID, subject, message, brandID, questionId, questionText));
            return true;
        }

        public bool SendFriendReferralEmail(string senderName,
            string message,
            string friendEmail,
            string prm,
            string lgid,
            string subject,
            int senderMemberID,
            int brandID)
        {
            EnqueueMailObj(new SendFriendReferralImpulse(friendEmail, senderName, senderMemberID, subject, message, brandID, prm, lgid));
            return true;
        }

        public bool UpdatePhonesList(int memberID, int brandID, string phoneNumber, SMSAlertsDefinitions.Action action)
        {
            bool result = false;
            string uri = string.Empty;
            try
            {
                uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    getService(uri).UpdatePhonesList(memberID, brandID, phoneNumber, action);
                    result = true;
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("Error using UpdatePhonesList()  (uri: " + uri + ").  Message was: " + ex.Message, ex);
            }
            return result;
        }

        public bool IsPhoneAvailable(int siteID, string phoneNumber)
        {
            bool result = false;
            string uri = string.Empty;
            try
            {
                uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    result = getService(uri).IsPhoneAvailable(siteID, phoneNumber);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("Error using UpdatePhonesList()  (uri: " + uri + ").  Message was: " + ex.Message, ex);
            }
            return result;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="mobileNumber"></param>
        /// <param name="memberID"></param>
        /// <param name="emailAddress"></param>
        /// <param name="brandID"></param>
        /// <returns></returns>
        public bool SendJDateMobileRegistrationConfirmation(string userName, string mobileNumber, int memberID, string emailAddress, int brandID)
        {
            return EnqueueMailObj(new JDateMobileRegistrationConfirmationImpulse(brandID, userName, mobileNumber, memberID, emailAddress), memberID, brandID);
        }

        public bool SendScheduledEmail(int groupid, int memberid, int emailscheduleid, string schedulename, string scheduleguid, string email, Hashtable details)
        {
            try
            {
                return EnqueueMailObj(new ScheduledNotificationImpulse(groupid, memberid, emailscheduleid, schedulename, scheduleguid, email, details), memberid, groupid);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void SendBetaFeedback(int brandID, string senderName, string senderEmailAddress, string senderMemberID, string browser, string remoteIP, string subject, string userComment, int feedbackTypeID)
        {
            EnqueueMailObj(new BetaFeedbackImpulse(brandID, senderName, senderEmailAddress, senderMemberID, browser, remoteIP, subject, userComment, feedbackTypeID));
        }

        public void SendBBEPasscodeEmail(int memberId, int brandID, string emailAddress, string passcode)
        {
            EnqueueMailObj(new BBEPasscodeImpulse(memberId, brandID, emailAddress, passcode));
        }

        public void SendSodbPhotoUploadEmail(int memberId, int brandId, string email, bool IsBHMember)
        {
            EnqueueMailObj(new SodbPhotoUploadImpulse(memberId, brandId, email,IsBHMember ));
        }

        #region Messmo Messaging
        /// <summary>
        /// Checks to see if the member wants to receive Messmo messages. This method does not check against message types,
        /// so technically this method can return true, but the member can end up not receiving anything.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="communityID"></param>
        /// <returns></returns>
        public bool IsMessmoMessageRequired(int memberID, int communityID)
        {
            if (!(Conversion.CBool(RuntimeSettings.GetSetting("MESSMO_ENABLED", communityID))))
            {
                return false;
            }

            bool isRequired = false;

            IMemberDTO member = GetIMemberDTO(memberID, MemberLoadFlags.None);

            if (member != null)
            {
                int brandID;
                member.GetLastLogonDate(communityID, out brandID);

                // if we can't get brandID by doing this, just don't bother with this whole Messmo stuff
                if (brandID != Constants.NULL_INT)
                {
                    Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);

                    // Messmo attributes are community scope, so we should get back the attribute as long as we get the community right with the brand object
                    int messmoCapable = member.GetAttributeInt(brand, "MessmoCapable", 0);
                    if (messmoCapable == 1)
                    {
                        // Business decided to not use MessmoAlertSetting.  The return value purely depends on MessmoCapable value.
                        isRequired = true;

                        //						int messmoAlertSetting = member.GetAttributeInt(brand, "MessmoAlertSetting", 0);
                        //						if(messmoAlertSetting != (int)Matchnet.HTTPMessaging.Constants.MessmoAlertSetting.DoNotSend)
                        //						{							
                        //							if(messmoAlertSetting == (int)Matchnet.HTTPMessaging.Constants.MessmoAlertSetting.Always)
                        //							{
                        //								isRequired = true;
                        //							}
                        //							else
                        //							{
                        //								// this means member wants to be notified when he is offline only
                        //								if(!Matchnet.MembersOnline.ServiceAdapters.MembersOnlineSA.Instance.IsMemberOnline(communityID, memberID))
                        //								{
                        //									isRequired = true;
                        //								}
                        //							}

                        //						}
                    }
                }
            }

            return isRequired;
        }

        /// <summary>
        /// Send an Member Question to Spark.Net personnel
        /// 
        /// </summary>
        /// <param name="brandID">brand of the site that was the source of the contact</param>
        /// <param name="userName">user-supplied username</param>
        /// <param name="fromFirstName">user-supplied first name</param>
        /// <param name="fromLastName">user-supplied last name</param>
        /// <param name="memberID">user-supplied memberID</param>
        /// <param name="browser">browser ID string from request</param>
        /// <param name="remoteIP">remote IP for the request</param>        
        /// <param name="questionText">user free form question text</param>
        /// <param name="isAnonymous">flag to indicate anonymity</param>
        /// 
        public void SendMemberGeneratedQuestion(int brandID, string userName, string fromFirstName, string fromLastName, int memberID, string questionText, bool isAnonymous)
        {
            EnqueueMailObj(new MemberQuestionImpulse(brandID, userName, fromFirstName, fromLastName, memberID, questionText, isAnonymous));
        }

        #region No longer used - see comments below
        // All the messages get sent now if they have their phone set up. The message type masks on the UI now determine if they want to be push notified or not.

        //		public bool IsMessmoMessageRequired(int memberID, int communityID, Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMsgType)
        //		{
        //			if(!(Conversion.CBool(RuntimeSettings.GetSetting("MESSMO_ENABLED", communityID))))
        //			{
        //				return false;
        //			}
        //
        //			bool isRequired = false;
        //			Matchnet.Content.ValueObjects.BrandConfig.Brand brand = null;
        //
        //			Matchnet.Member.ServiceAdapters.Member member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
        //
        //			if(member != null)
        //			{
        //				int brandID;
        //				member.GetLastLogonDate(communityID, out brandID);
        //
        //				// if we can't get brandID by doing this, just don't bother with this whole Messmo stuff
        //				if(brandID != Constants.NULL_INT)
        //				{
        //					brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
        //
        //					// Messmo attributes are community scope, so we should get back the attribute as long as we get the community right with the brand object
        //					int messmoCapable = member.GetAttributeInt(brand, "MessmoCapable", 0);
        //					if(messmoCapable == 1)
        //					{
        //						int messmoAlertSetting = member.GetAttributeInt(brand, "MessmoAlertSetting", 0);
        //						if(messmoAlertSetting != (int)Matchnet.HTTPMessaging.Constants.MessmoAlertSetting.DoNotSend)
        //						{							
        //							if(messmoAlertSetting == (int)Matchnet.HTTPMessaging.Constants.MessmoAlertSetting.Always)
        //							{
        //								isRequired = true;
        //							}
        //							else
        //							{
        //								// this means member wants to be notified when he is offline only
        //								if(!Matchnet.MembersOnline.ServiceAdapters.MembersOnlineSA.Instance.IsMemberOnline(communityID, memberID))
        //								{
        //									isRequired = true;
        //								}
        //							}
        //
        //						}
        //					}
        //				}
        //			}
        //
        //			// let's check against the MailType
        //			if(isRequired)
        //			{
        //				isRequired = false;
        //
        //                int memberAlertMask = member.GetAttributeInt(brand, "MessmoAlertMask", 0);
        //				
        //				Matchnet.HTTPMessaging.Constants.MessmoAlertMask messageAlertMask = Matchnet.HTTPMessaging.Utilities.MapToMessmoAlertMask(messmoMsgType);
        //				
        //				if((memberAlertMask & (int)messageAlertMask) > 0)
        //				{
        //					isRequired = true;
        //				}
        //			}
        //
        //            return isRequired;
        //		}
        #endregion

        public bool NotifyOfMessmoMessageArrival(int memberID, int brandID, Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMsgType)
        {
            bool notify = false;
            IMemberDTO member = GetIMemberDTO(memberID, MemberLoadFlags.None);
            Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);

            if (member != null && brand != null)
            {
                int memberAlertMask = member.GetAttributeInt(brand, "MessmoAlertMask", 0);
                Matchnet.HTTPMessaging.Constants.MessmoAlertMask messageAlertMask = Matchnet.HTTPMessaging.Utilities.MapToMessmoAlertMask(messmoMsgType);

                // Business decided to only allow push notification for inbox items.
                // For all other message types, notify flag value will be false
                if (messageAlertMask == Matchnet.HTTPMessaging.Constants.MessmoAlertMask.Message)
                {
                    if ((memberAlertMask & (int)messageAlertMask) > 0)
                    {
                        notify = true;
                    }
                }
            }

            return notify;
        }

        /// <summary>
        /// Called from Email svc. Sends a Messmo message to the member's mobile if it is required.
        /// </summary>
        /// <param name="sendResult"></param>
        /// <returns></returns>
        public bool SendMessmoMessageFromEmail(Matchnet.Email.ValueObjects.MessageSendResult sendResult)
        {
            Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMsgType = Matchnet.ExternalMail.ValueObjects.MessmoUtilities.MapMailTypeToMessmoMessageType(sendResult.EmailMessage.MailTypeID);

            // We do not check against the message type anymore
            //			if(!IsMessmoMessageRequired(sendResult.EmailMessage.ToMemberID, sendResult.EmailMessage.GroupID, messmoMsgType))
            //				return false;

            int recipientBrandID, senderBrandID, sendingMemberID;
            DateTime messageDate;
            string receivingMessmoSubID, messageBody, smartMessageTag, taggle;

            sendingMemberID = sendResult.EmailMessage.FromMemberID;
            int recipientMemberID = sendResult.EmailMessage.ToMemberID;

            // figure out the BrandID's
            IMemberDTO sendingMember = GetIMemberDTO(sendingMemberID, MemberLoadFlags.None);
            IMemberDTO recipientMember = GetIMemberDTO(recipientMemberID, MemberLoadFlags.None);

            if (sendingMember == null || recipientMember == null)
                return false;

            sendingMember.GetLastLogonDate(sendResult.EmailMessage.GroupID, out senderBrandID);
            recipientMember.GetLastLogonDate(sendResult.EmailMessage.GroupID, out recipientBrandID);

            if (senderBrandID == Constants.NULL_INT || recipientBrandID == Constants.NULL_INT)
                return false;

            // make sure the member has the Messmo subscriber ID although he should have it if this method was called
            receivingMessmoSubID = recipientMember.GetAttributeText(Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(recipientBrandID),
                "MessmoSubscriberID", string.Empty);

            if (receivingMessmoSubID == string.Empty)
                return false;

            Brand senderBrand = BrandConfigSA.Instance.GetBrandByID(senderBrandID);

            messageDate = sendResult.EmailMessage.InsertDate;
            Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMessageType = Matchnet.ExternalMail.ValueObjects.MessmoUtilities.MapMailTypeToMessmoMessageType(sendResult.EmailMessage.MailTypeID);
            // since Messmo doesn't have a subject field, we are including this in the body of the message
            messageBody = Matchnet.HTTPMessaging.Utilities.GetMessmoMessageBody(messmoMessageType, sendResult.EmailMessage.Message.MessageBody,
                sendResult.EmailMessage.Subject);

            smartMessageTag = Matchnet.HTTPMessaging.Utilities.GetMessmoSmartMessageTag(messmoMessageType, sendingMember.GetUserName(senderBrand));

            taggle = Matchnet.HTTPMessaging.Utilities.GetMessmoTaggleValue(sendingMemberID, recipientMemberID, messmoMessageType,
                sendResult.EmailMessage.GroupID, sendResult.RecipientMessageListID);

            bool notify = NotifyOfMessmoMessageArrival(sendResult.EmailMessage.ToMemberID, recipientBrandID, messmoMessageType);

            return SendMessmoMultiPartMessage(recipientBrandID, senderBrandID, sendingMemberID, messageDate, receivingMessmoSubID,
                messageBody, smartMessageTag, taggle, false, notify, messmoMsgType);
        }

        /// <summary>
        /// Called from the List Service to send a Messmo message to the member's mobile.
        /// </summary>
        /// <param name="communityID"></param>
        /// <param name="recipientBrandID"></param>
        /// <param name="senderBrandID"></param>
        /// <param name="recipientMemberID"></param>
        /// <param name="senderMemberID"></param>
        /// <param name="hotlistCat"></param>
        /// <returns></returns>
        public bool SendMessmoMessageFromList(int communityID, int recipientBrandID, int senderBrandID, int recipientMemberID, int senderMemberID,
            Matchnet.List.ValueObjects.HotListCategory hotlistCat)
        {
            // make sure the recipient wants to receive this even
            // we have to get the reciprocal HotListCategory since the member who initiates this isn't the one who is being notified.
            // the person who is receiving this message is the only relevant (Messmo) user thus the HotListCategory should be flipped.
            Matchnet.List.ValueObjects.HotListCategory reciprocalCat = Matchnet.ExternalMail.ValueObjects.MessmoUtilities.GetReciprocalHotListCategory(hotlistCat);
            Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMsgType = Matchnet.ExternalMail.ValueObjects.MessmoUtilities.MapHotListCategoryToMessmoMessageType(reciprocalCat);

            // We do not check against message types anymore.
            //			if(!IsMessmoMessageRequired(recipientMemberID, communityID, messmoMsgType))
            //				return false;

            string receivingMessmoSubID, smartMessageTag, taggle;

            IMemberDTO recipientMember = GetIMemberDTO(recipientMemberID, MemberLoadFlags.None);
            IMemberDTO senderMember = GetIMemberDTO(senderMemberID, MemberLoadFlags.None);

            // can't do anything if any one of the members is null
            if (recipientMember == null || senderMember == null)
                return false;

            Matchnet.Content.ValueObjects.BrandConfig.Brand recipientBrand = BrandConfigSA.Instance.GetBrandByID(recipientBrandID);
            Matchnet.Content.ValueObjects.BrandConfig.Brand senderBrand = BrandConfigSA.Instance.GetBrandByID(senderBrandID);

            receivingMessmoSubID = recipientMember.GetAttributeText(recipientBrand,
                "MessmoSubscriberID", string.Empty);

            smartMessageTag = Matchnet.HTTPMessaging.Utilities.GetMessmoSmartMessageTag(messmoMsgType, senderMember.GetUserName(senderBrand));

            taggle = Matchnet.HTTPMessaging.Utilities.GetMessmoTaggleValue(senderMemberID, recipientMemberID, messmoMsgType,
                recipientBrand.Site.Community.CommunityID, Constants.NULL_INT);

            string messageBody = Matchnet.HTTPMessaging.Utilities.GetMessmoMessageBody(messmoMsgType, string.Empty, string.Empty);

            bool notify = NotifyOfMessmoMessageArrival(recipientMemberID, recipientBrandID, messmoMsgType);

            return SendMessmoMultiPartMessage(recipientBrandID, senderBrandID, senderMemberID, DateTime.Now, receivingMessmoSubID,
                messageBody, smartMessageTag, taggle, true, notify, messmoMsgType);
        }


        /// <summary>
        /// Maps the MessmoMessageType to MessmoButtonType before calling the overloaded SendMessmoMultiPartMessage.  This is the ScheduledFeed service version where
        /// notify flag can be specified to disable it.
        /// </summary>
        /// <param name="recipientBrandID"></param>
        /// <param name="senderBrandID"></param>
        /// <param name="senderMemberID"></param>
        /// <param name="messageDate"></param>
        /// <param name="receivingMessmoSubID"></param>
        /// <param name="messageBody"></param>
        /// <param name="smartMessageTag"></param>
        /// <param name="taggle"></param>
        /// <param name="replace"></param>
        /// <param name="notify"></param>
        /// <param name="messmoMsgType"></param>
        /// <returns></returns>
        public bool SendMessmoMultiPartMessage(int recipientBrandID, int senderBrandID, int senderMemberID, DateTime messageDate, string receivingMessmoSubID,
            string messageBody, string smartMessageTag, string taggle, bool replace, bool notify, Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMsgType)
        {
            return sendMessmoMultiPartMessage(recipientBrandID, senderBrandID, senderMemberID, messageDate, receivingMessmoSubID,
                messageBody, smartMessageTag, taggle, replace, notify, Matchnet.HTTPMessaging.Utilities.GetButtonTypeFromMessmoMessageType(messmoMsgType));
        }

        /// <summary>
        /// Messmo API 2.20 - A Channel Sends a Multi-Part Message to a User.
        /// Notify flag can be manually set with this.
        /// </summary>
        /// <param name="recipientBrandID"></param>
        /// <param name="senderBrandID"></param>
        /// <param name="senderMemberID"></param>
        /// <param name="messageDate"></param>
        /// <param name="receivingMessmoSubID"></param>
        /// <param name="messageBody"></param>
        /// <param name="smartMessageTag"></param>
        /// <param name="taggle"></param>
        /// <param name="replace"></param>
        /// <param name="notify"></param>
        /// <param name="messmoButtonType"></param>
        /// <returns></returns>
        private bool sendMessmoMultiPartMessage(int recipientBrandID, int senderBrandID, int senderMemberID, DateTime messageDate, string receivingMessmoSubID,
            string messageBody, string smartMessageTag, string taggle, bool replace, bool notify, Matchnet.HTTPMessaging.Constants.MessmoButtonType messmoButtonType)
        {
            return EnqueueMailObj(new MessmoSendMultiPartMessageImpulse(recipientBrandID, senderBrandID, senderMemberID, messageDate, receivingMessmoSubID, messageBody,
                smartMessageTag, taggle, replace, notify, messmoButtonType), senderMemberID, senderBrandID);
        }

        /// <summary>
        /// This method isn't one of the Messmo API call.  It's a grouping of Messmo API calls that must be made right after the initial sign-up
        /// of Messmo's service.  This call will send the member's subcription end date and the initial batch of messages to Messmo.  There is a
        /// delay between where Messmo starts recognizing the Messmo subscriber ID and where we sign the member up for the first time.
        /// For this reason, this impulse is going to be a delayed impulse, meaning thread.sleep will be used.  According to Messmo, the maximum
        /// time for this delay is 1 minute.
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="timestamp"></param>
        /// <param name="memberID"></param>
        /// <param name="messmoSubscriberID"></param>
        /// <param name="status"></param>
        /// <param name="setDate"></param>
        /// <param name="mobileNumber"></param>
        public void SendInitialBatchOfMessmoMessages(int brandID, DateTime timestamp, int memberID, string messmoSubscriberID, string status,
             DateTime setDate, string mobileNumber)
        {
            EnqueueMailObj(new MessmoSendInitialBatchImpulse(brandID, timestamp, memberID, messmoSubscriberID, status, setDate, mobileNumber));
        }

        /// <summary>
        /// Messmo's API 2.5 - A Channel Sends Content to a User. This isn't being used in the beginning. This  can be used in the future to
        /// send simple messages (i.e. system message).
        /// </summary>	
        /// <param name="recipientBrandID">This is used only to validate if the site is enabled for ExternalMail</param>	
        /// <param name="emailDate"></param>
        /// <param name="emailBody"></param>
        /// <param name="receivingMessmoSubID"></param>
        /// <param name="taggle"></param>
        /// <returns></returns>
        public void SendMessmoMessage(int recipientBrandID, DateTime emailDate, string emailBody, string receivingMessmoSubID,
            string taggle)
        {
            EnqueueMailObj(new MessmoSendMessageImpulse(recipientBrandID, emailDate, emailBody, receivingMessmoSubID, taggle));
        }

        /// <summary>
        /// Messmo's API 2.2 call. Although the name suggests Subscription, this is really used to reset the password with Messmo.
        /// When our member changes his password, we have to let Messmo know, and their Subscribe API call is the only way to do so.
        /// 
        /// *The actual subscription process (from the web) calls the same Messmo API, but synchronously so the call wouldn't call
        ///  through this method.
        /// </summary>
        ///<param name="brandID">Brand where the Messmo subscripition request is made</param>
        ///<param name="memberID"></param>
        /// <param name="timestamp"></param>
        /// <param name="mobileNumber"></param>
        /// <param name="encryptedPassword"></param>
        /// <returns></returns>
        public bool SendMessmoSubscriptionRequest(int brandID, int memberID, DateTime timestamp, string mobileNumber, string encryptedPassword)
        {
            return EnqueueMailObj(new MessmoSubscribeImpulse(brandID, memberID, timestamp, mobileNumber, encryptedPassword), memberID, brandID);
        }
        //
        //		/// <summary>
        //		/// Messmo API 2.4 A Channel Unsubscribes a User
        //		/// </summary>
        //		/// <param name="brandID"></param>
        //		/// <param name="timestamp"></param>
        //		/// <param name="messmoSubscriberID"></param>
        //		/// <returns></returns>
        //		public void SendMessmoUnsubscribeRequest(int brandID, DateTime timestamp, int memberID)
        //		{
        //			EnqueueMailObj(new MessmoUnsubscribeImpulse(brandID, timestamp, memberID));
        //		}

        /// <summary>
        /// Messmo API 2.21 A Channel Sets a User's Transaction Status
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="timestamp"></param>
        /// <param name="messmoSubscriberID"></param>
        /// <param name="status"></param>
        /// <param name="setDate"></param>
        public void SendMessmoUserTransStatusRequest(int brandID, DateTime timestamp, string messmoSubscriberID, string status, DateTime setDate)
        {
            EnqueueMailObj(new MessmoTranStatusImpulse(brandID, timestamp, messmoSubscriberID, status, setDate));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="timestamp"></param>
        /// <param name="messmoSubscriberID"></param>
        /// <param name="status"></param>
        /// <param name="setDate"></param>
        public void SendMessmoUserTransStatusRequestBySiteID(int siteID, DateTime timestamp, string messmoSubscriberID, string status, DateTime setDate)
        {
            int brandID = GetDefaultBrandForSite(siteID);

            if (brandID != Constants.NULL_INT)
            {
                EnqueueMailObj(new MessmoTranStatusImpulse(brandID, timestamp, messmoSubscriberID, status, setDate));
            }
        }

        /// <summary>
        /// Messmo API 2.23 a Channel Provides Full Profile Details to Messmo
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="memberID"></param>
        /// <param name="timestamp"></param>
        public void SendMessmoFullProfileRequest(int brandID, int memberID, DateTime timestamp)
        {
            EnqueueMailObj(new MessmoFullProfileImpulse(brandID, memberID, timestamp, memberID.ToString()));
        }

        /// <summary>
        /// Messmo API 2.18 A Channel sends an SMS to a User
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="timestamp"></param>
        /// <param name="mobileNumber"></param>
        /// <param name="message"></param>
        public void SendMessmoSMSRequest(int brandID, DateTime timestamp, string mobileNumber, string message)
        {
            EnqueueMailObj(new MessmoSendSMSImpulse(brandID, timestamp, mobileNumber, message));
        }
        #endregion
        #endregion

        #region Private Methods
        /// <summary>
        /// Identifies the default brand for a given site.  In reality site-to-brand relationship is one-to-one, so this should
        /// be totally safe.
        /// </summary>
        /// <param name="siteID"></param>
        /// <returns></returns>
        private int GetDefaultBrandForSite(int siteID)
        {
            int brandID = Constants.NULL_INT;

            switch (siteID)
            {
                case 103:
                    brandID = 1003;
                    break;
            }

            return brandID;
        }

        /// <summary>
        /// Cancel the send if the member has been suspended for a variety of reasons.
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="MemberID"></param>
        /// <returns>true if the send was cancelled, false if not</returns>
        private bool IsValidEmail(int MemberID, int brandID, bool ignoreSelfSuspend)
        {
            bool retVal = true;
            
            Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);
            var logonOnlySite = Convert.ToBoolean(RuntimeSettings.GetSetting(SettingConstants.LOGON_ONLY_SITE, 
                brand.Site.Community.CommunityID, brand.Site.SiteID));
            
            if(logonOnlySite)
            {
                return retVal;
            }
            
            IMemberDTO member = GetIMemberDTO(MemberID, MemberLoadFlags.None);

            

            //	Has the use suspended him/her self?
            int selfSuspendedMember = member.GetAttributeInt(brand, "SelfSuspendedFlag", 0);

            //	Has the user been suspended by an admin or by stormpost?
            int globalStatusMask = member.GetAttributeInt(brand, "GlobalStatusMask", 0);

            // 1 is the value of 'admin suspended'
            bool adminSuspendedMember = Convert.ToBoolean((globalStatusMask & 1));

            //	Has the user been suspended for a bad email?
            // 2 is the value of 'bad email'
            bool badEmailSuspend = Convert.ToBoolean((globalStatusMask & 2));

            if (!ignoreSelfSuspend && selfSuspendedMember == 1)
            {
                retVal = false;
            }
            else if (adminSuspendedMember)
            {
                retVal = false;
            }
            else if (badEmailSuspend)
            {
                retVal = false;
            }

            return (retVal);
        }

        private bool EnqueueMailObj(ImpulseBase impulse, int MemberID, int brandID, bool ignoreSelfSuspend)
        {
            // Block All external email for date.co.uk brandid=1006

            if (impulse.GetType() == typeof(ScheduledNotificationImpulse))
            {
                ScheduledNotificationImpulse imp = (ScheduledNotificationImpulse)impulse;
                if (imp.EmailAddress == null)
                    return false;

                if (imp.MemberID <= 0)
                {
                    if (!String.IsNullOrEmpty(imp.EmailAddress))
                    {
                        EnqueueMailObj(impulse);
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    MemberID = imp.MemberID;
                    brandID = imp.BrandID;
                }


            }
            if (brandID != 1006)
            {
                if (IsValidEmail(MemberID, brandID, ignoreSelfSuspend))
                {
                    EnqueueMailObj(impulse);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }


        private bool EnqueueMailObj(ImpulseBase impulse, int MemberID, int brandID)
        {
            return EnqueueMailObj(impulse, MemberID, brandID, false);
        }


        /// <summary>
        /// Takes an object and stuffs it on the ExternalMail MSMQ queue.
        /// </summary>
        /// <param name="impulse">object to enqueue</param>
        private void EnqueueMailObj(ImpulseBase impulse)
        {
            string uri = string.Empty;

            try
            {
                uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    getService(uri).Enqueue(impulse);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw new SAException("Error queuing impulse object (uri: " + uri + ").  Message was: " + ex.Message, ex);
            }
        }


        private string getServiceManagerUri()
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ExternalMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(Matchnet.ExternalMail.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME);
                string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }


        private IExternalMailService getService(string uri)
        {
            try
            {
                return (IExternalMailService)Activator.GetObject(typeof(IExternalMailService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }


        /// <summary>
        /// See TT 16111.
        /// This function checks to make sure that the recipient belongs to the brand.
        /// If the recipient does not belong to the brand, then the first brand in the same Site, to which
        /// the recipient belongs, is returned as the brandID.  If the recipient does not belong to any brands
        /// in that Site, then the first brand in any Site in that Community is returned.
        /// If no such brand is found,
        /// then an error is thrown (This should never happen).
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="memberID"></param>
        private int CheckRecipientBrand(int brandID, int memberID)
        {
            IMemberDTO member = GetIMemberDTO(memberID, MemberLoadFlags.None);
            Brand brand = Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);

            // If the member is a member of this Brand, then return because we're done.
            if (member.GetAttributeDate(brand, "BrandInsertDate", DateTime.MinValue) != DateTime.MinValue)
                return brandID;

            Brands brands = null;
            // If the member is a member of this site, return the first brand
            // that we find.
            if (member.IsSiteMember(brand.Site.SiteID))
            {
                brands = BrandConfigSA.Instance.GetBrandsBySite(brand.Site.SiteID);
                foreach (Brand currBrand in brands)
                {
                    if (currBrand.BrandID != brand.BrandID) // Don't double check the brandID we already checked.
                    {
                        if (member.GetAttributeDate(currBrand, "BrandInsertDate", DateTime.MinValue) != DateTime.MinValue)
                            return currBrand.BrandID;
                    }
                }
            }

            brands = null;
            // Still haven't found a brandID?
            // Return the first brandID that we find for this community.
            // The member should always be a member of this community.
            brands = BrandConfigSA.Instance.GetBrandsByCommunity(brand.Site.Community.CommunityID);
            foreach (Brand currBrand in brands)
            {
                if (currBrand.Site.SiteID != brand.Site.SiteID) // Don't double check the brandIDs.
                {
                    DateTime currBrandInsertDate = member.GetAttributeDate(currBrand, "BrandInsertDate", DateTime.MinValue);
                    if (currBrandInsertDate != DateTime.MinValue)
                        return currBrand.BrandID;
                }
            }

            // We should never get here.
            throw new SAException("A valid Brand could not be found.");
        }

        public static IMemberDTO GetIMemberDTO(int memberid, MemberLoadFlags memberLoadFlags)
        {
            return GetIMemberDTO(MemberSA.Instance, memberid, memberLoadFlags);
        }

        public static IMemberDTO GetIMemberDTO(IGetMember memberService, int memberid, MemberLoadFlags memberLoadFlags)
        {
            string settingFromSingleton = SettingsService.GetSettingFromSingleton("ENABLE_MEMBER_DTO");
            bool isEnabled = (null != settingFromSingleton && Boolean.TrueString.ToLower().Equals(settingFromSingleton.ToLower()));
            if (isEnabled)
            {
                return memberService.GetMemberDTO(memberid, MemberType.MatchMail);
            }
            else
            {
                return memberService.GetMember(memberid, memberLoadFlags);
            }
        }

        #endregion
    }
}
