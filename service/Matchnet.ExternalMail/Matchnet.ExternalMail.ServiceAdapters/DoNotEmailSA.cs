using System;
using System.Collections;
using Matchnet;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects.DoNotEmail;
using Matchnet.ExternalMail.ValueObjects.ServiceDefinitions;


namespace Matchnet.ExternalMail.ServiceAdapters
{
	/// <summary>
	/// Summary description for DoNotEmailSA.
	/// </summary>
	public class DoNotEmailSA: SABase
	{
		public static readonly DoNotEmailSA Instance = new DoNotEmailSA();
		
		private DoNotEmailSA()
		{
		}
		
		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_SA_CONNECTION_LIMIT"));
		}

		public bool AddEmailAddress(int brandID, string emailAddress)
		{
			return AddEmailAddress(brandID, emailAddress, Constants.NULL_INT);
		}

		public bool AddEmailAddress(int brandID, string emailAddress, int memberID)
		{
			string uri = string.Empty;

			try
			{
				uri = getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					return getService(uri).AddEmailAddress(brandID, emailAddress, memberID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw new SAException("Error adding EmailAddress (uri: " + uri + ").  Message was: " + ex.Message, ex);
			}
		
		}

		public bool AddEmailAddressBySiteID(int siteID, string emailAddress)
		{
			string uri = string.Empty;

			try
			{
				uri = getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					return getService(uri).AddEmailAddressBySiteID(siteID, emailAddress);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw new SAException("Error adding EmailAddress (uri: " + uri + ").  Message was: " + ex.Message, ex);
			}
		
		}

		public bool RemoveEmailAddress(int siteID, string emailAddress)
		{
			string uri = string.Empty;

			try
			{
				uri = getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					return getService(uri).RemoveEmailAddress(siteID, emailAddress);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw new SAException("Error removing EmailAddress (uri: " + uri + ").  Message was: " + ex.Message, ex);
			}
		}

		public DoNotEmailEntry GetEntryByEmailAddress(string emailAddress)
		{
			string uri = string.Empty;
			DoNotEmailEntry entry = null;

			try
			{
				uri = getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					entry = getService(uri).GetEntryByEmailAddress(emailAddress);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw new SAException("Error getting DoNotEmailEntry by email address (uri: " + uri + ").  Message was: " + ex.Message, ex);
			}
			
			return entry;
		}

		public DoNotEmailEntry GetEntryBySiteAndEmailAddress(int siteID, string emailAddress)
		{
			string uri = string.Empty;
			DoNotEmailEntry entry = null;

			try
			{
				uri = getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					entry = getService(uri).GetEntryBySiteAndEmailAddress(siteID, emailAddress);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw new SAException("Error getting DoNotEmailEntry by site and email address (uri: " + uri + ").  Message was: " + ex.Message, ex);
			}
			
			return entry;
		}


		public ArrayList GetEntriesBySite(int siteID)
		{
			string uri = string.Empty;
			ArrayList entries = null;

			try
			{
				uri = getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					entries = getService(uri).GetEntriesBySite(siteID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw new SAException("Error getting DoNotEmail entries (uri: " + uri + ").  Message was: " + ex.Message, ex);
			}
			
			return entries;
		}

		public ArrayList GetEntries()
		{
			string uri = string.Empty;
			ArrayList entries = null;

			try
			{
				uri = getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					entries = getService(uri).GetEntries();
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw new SAException("Error getting DoNotEmail entries (uri: " + uri + ").  Message was: " + ex.Message, ex);
			}
			
			return entries;
		}


		public ArrayList Search(int siteID, string partialEmailAddress, int startRow, int pageSize, ref int totalRows )
		{
			string uri = string.Empty;
			ArrayList entries = null;

			try
			{
				uri = getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					entries = getService(uri).Search(siteID, partialEmailAddress, startRow, pageSize, ref totalRows);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw new SAException("Error getting DoNotEmail entries (uri: " + uri + ").  Message was: " + ex.Message, ex);
			}
			
			return entries;
		}

		public ArrayList GetSiteSummary()
		{
			string uri = string.Empty;
			ArrayList summaries = null;

			try
			{
				uri = getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					summaries = getService(uri).GetSiteSummary();
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw new SAException("Error getting summary (uri: " + uri + ").  Message was: " + ex.Message, ex);
			}
			
			return summaries;
		}

		private string getServiceManagerUri()
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ExternalMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(Matchnet.ExternalMail.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.", ex));
			}
		}

		private IDoNotEmailService getService(string uri)
		{
			try
			{
				return (IDoNotEmailService) Activator.GetObject(typeof (IDoNotEmailService), uri);
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

	}
}
