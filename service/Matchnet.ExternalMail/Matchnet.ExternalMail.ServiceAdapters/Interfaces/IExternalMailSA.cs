using System;
using System.Collections;
using System.Collections.Generic;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;
using Matchnet.ExternalMail.ValueObjects.ServiceDefinitions;

namespace Matchnet.ExternalMail.ServiceAdapters
{
    public interface IExternalMailSA
    {
        bool ResetPassword(int brandId, int memberId, string resetPasswordUrl);

        /// <summary>
        /// Checks to see if the passed in SMS alert mask value is set or not
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="maskToCheck"></param>
        /// <returns></returns>
        bool IsSMSAlertNeeded(int memberID, ExternalMailSA.SMSAlertMask maskToCheck, Brand brand);

        bool IsSMSAlertNeeded(int memberID, Brand brand, SMSAlertsDefinitions.MessageType messageType,
                                              bool isFriend);

        /// <summary>
        /// Email Volume Regulation.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="brand"></param>
        /// <returns>True - do not send</returns>
        bool IsRegulatedByLastLogonDate(int memberID, Brand brand);

        bool SendAllAccessNudgeEmail(int pSenderMemberId, int pSenderBrandId, int pMessageListId,
                                                     int pRecipientMemberId, int pRecipientBrandId, string pEmailSubject,
                                                     string pEmailContent, DateTime pInitialMailTime);

        bool SendAllAccessInitialEmail(int pSenderMemberId, int pSenderBrandId, int pMessageListId,
                                                       int pRecipientMemberId, int pRecipientBrandId, string pEmailSubject,
                                                       string pEmailContent);

        bool SendAllAccessReplyToInitialEmail(int pSenderMemberId, int pSenderBrandId, int pMessageListId,
                                                              int pRecipientMemberId, int pRecipientBrandId, string pEmailSubject,
                                                              string pEmailContent);

        bool SendAllAccessNoThanksEmail(int pSenderMemberId, int pSenderBrandId, int pRecipientMemberId,
                                                        int pRecipientBrandId);

        bool SendAllAccessReturnEmail(int pSenderMemberId, int pSenderBrandId, int pRecipientMemberId,
                                                      int pRecipientBrandId);

        bool SendBOGONotificationEmail(int senderMemberId, int senderBrandID, string recipientEmailAddress,
                                                       string giftPromo, string giftCode);

        void SendProfileChangedConfirmationEmail(int memberId, int brandID, string emailAddress, string firstName,
                                                                 string userName, List<string> fields);

        /// <summary>
        /// Sends an email to users with credit card had a hard decline during subscription renewal period.
        /// </summary>
        /// <param name="siteID">Site id of the website the system is trying to renew.</param>
        /// <param name="userName">Username of the member.</param>
        /// <returns></returns>
        bool SendRenewalFailureEmail(int siteID, string userName, string emailAddress);

        /// <summary>
        /// Sends an email containing information about the Report Abuse to CS.
        /// Does not require good email address
        /// </summary>
        /// <returns></returns>
        bool SendReportAbuseEmail(int brandID, string ReportContent);

        /// <summary>
        /// Sends a confirmation to the members that their registration has
        /// been successful
        /// and asking them to verify their email address
        /// 
        /// See SendVerificationLetter in ContextGlobal.cs:582
        /// Requires good email
        /// </summary>
        /// <returns></returns>
        bool SendRegistrationVerification(int memberID, int brandID, int siteID);

        /// <summary>
        /// Sends a request to the member to verify their email address.
        /// This can be called either as the result of a member action or
        /// an admin action.
        /// 
        /// See SendVerifyEmailLetter in ContextGlobal.cs:651
        /// Requires good email
        /// </summary>
        /// <param name="memberID">member ID to send the verification to</param>
        /// <param name="brandID">brand ID for the site to which the email verification pertains</param>
        /// <param name="isFromAdmin">whether or not the email was initiated by an administrator</param>
        bool SendEmailVerification(int memberID, int brandID, bool isFromAdmin);

        /// <summary>
        /// Sends a request to the member to verify their email address.
        /// This is the result of a member action.
        /// 
        /// See SendVerifyEmailLetter in ContextGlobal.cs:651
        /// Requires good email
        /// </summary>
        /// <param name="memberID">member ID to send the verification to</param>
        /// <param name="brandID">brand ID for the site to which the email verification pertains</param>
        bool SendEmailVerification(int memberID, int brandID);

        /// <summary>
        /// Sends the password for a given email address to that email address
        /// 
        /// See SendMemberPassword in ContextGlobal.cs:701
        /// Requires good email
        /// </summary>
        /// <param name="brandID">The brand id of the current brand</param>
        /// <param name="emailAddress">email address for the member who needs
        /// to receive their password</param>
        bool SendPassword(int brandID, string emailAddress);

        /// <summary>
        /// Send a notice after a successful check subscription payment.
        /// 
        /// See SendSuccessEmail in SubscribeByCheck.ascx.cs:354
        /// and SendSuccessEmail in SubscribeByDirectDebit.ascx.cs:276
        /// 
        /// This particular email does not appear to be contained in the Excel
        /// spreadsheet.
        /// Requires good email
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="brandID"></param>
        /// <param name="memberPaymentID"></param>
        /// <param name="planDescription"></param>
        bool SendCheckSubscriptionConfirmation(int memberID, int brandID, int memberPaymentID,
                                                               string planDescription);

        /// <summary>
        /// Not entirely sure what this email does but it appears to send an
        /// opt-out notice to a member that they perhaps then have to confirm
        /// via clicking on a link in an email?
        /// 
        /// See btnOptOut_Click in OptOut.ascx.cs:96
        /// NO LONGER IN USE.  Optout is handled by StormPost, this method is no longer in use
        /// Does not require good email address
        /// </summary>
        void SendOptOutNotification(int brandID, string email, string fromHost);

        /// <summary>
        /// Send an email to Spark.Net personnel based on a user ContactUs
        /// form submission.
        /// See Go_Click in ContactUs.ascx.cs:77
        /// Does not require good email address
        /// </summary>
        /// <param name="brandID">brand of the site that was the source of the contact</param>
        /// <param name="userName">user-supplied username</param>
        /// <param name="fromEmailAddress">user-supplied email address</param>
        /// <param name="sMemberID">user-supplied MemberID</param>
        /// <param name="browser">browser ID string from request</param>
        /// <param name="remoteIP">remote IP for the request</param>
        /// <param name="reason">drop-down selected reason</param>
        /// <param name="userComment">user free form comment</param>
        /// <param name="appDeviceInfo">The application device information.</param>
        /// <param name="contactUsEmail">to email address</param>
        void SendContactUs(int brandID, string userName, string fromEmailAddress, string sMemberID,
                                           string browser, string remoteIP, string reason, string userComment, string appDeviceInfo = null, string contactUsEmail = null);

        /// <summary>
        /// For sending internal emails within the company. Use for admin related activities.
        /// Initially created for supporting subscription admin.
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="fromAddress"></param>
        /// <param name="toAddress"></param>
        /// <param name="toName"></param>
        /// <param name="body"></param>
        /// <param name="subject"></param>
        void SendInternalCorpMail(int brandID, string fromAddress, string toAddress, string body, string toName,
                                                  string subject);

        /// <summary>
        /// Send a link to a member to a (potentially external) email address.
        /// 
        /// See btnSendToFriend_Click in SendToFriend.ascx.cs:138
        /// Does not require good email address
        /// </summary>
        /// <param name="friendName">user-supplied friend's name</param>
        /// <param name="friendEmail">user-supplied friend's email</param>
        /// <param name="userEmail">user's email address</param>
        /// <param name="sentMemberID">member ID whose info is being sent</param>
        /// <param name="subject">user-supplied subject</param>
        /// <param name="message">user-supplied message</param>
        /// <param name="brandID">source brandID</param>
        bool SendMemberToFriend(string friendName, string friendEmail, string userEmail, int sentMemberID,
                                                string subject, string message, int brandID);

        /// <summary>
        /// Sends a member's color code link to friend
        /// </summary>
        /// <param name="friendName"></param>
        /// <param name="friendEmail"></param>
        /// <param name="senderEmail"></param>
        /// <param name="senderName"></param>
        /// <param name="senderMemberID"></param>
        /// <param name="subject"></param>
        /// <param name="message"></param>
        /// <param name="brandID"></param>
        /// <returns></returns>
        bool SendColorCodeToFriend(string friendName,
                                                   string friendEmail,
                                                   string senderEmail,
                                                   string senderName,
                                                   int senderMemberID,
                                                   string subject,
                                                   string message,
                                                   int brandID);

        bool SendColorCodeQuizInvite(string recipientEmail,
                                                     string recipientUserName,
                                                     string senderEmail,
                                                     int senderMemberID,
                                                     string senderUserName,
                                                     int brandID);

        bool SendColorCodeQuizConfirmation(int memberID,
                                                           string emailAddress,
                                                           int brandID,
                                                           string color,
                                                           string memberUserName);

        /// <summary>
        /// Send a promo email inviting a member to join the CollegeLuv site
        /// Requires good email
        /// </summary>
        /// <param name="memberID">member to send the email to</param>
        /// <param name="brandID">original brand where the user registered</param>
        bool SendCollegeLuvPromo(int memberID, int brandID);

        /// <summary>
        /// Send an email to a user when they clicked Yes for a member who has also
        /// clicked Yes for them.
        /// Requires good email
        /// </summary>
        /// <param name="memberID">member to receive the email</param>
        /// <param name="targetMemberID">other member who participated in the mutual yes</param>
        /// <param name="brandID">brand where the vote occurred</param>
        bool SendMutualYesNotification(int memberID, int targetMemberID, int brandID);

        /// <summary>
        /// Send an email to a user who has received an internal mail from another member.
        /// Requires good email
        /// </summary>
        bool SendInternalMailNotification(EmailMessage emailMessage, int brandID, int unreadEmailCount);

        /// <summary>
        /// Send an email to a member to let them know that an administrator has reviewed
        /// and approved one of their uploaded photos.
        /// Requires good email
        /// </summary>
        /// <param name="memberID">member who owns the approved photo</param>
        /// <param name="photoID">identifier for photo that has been approved</param>
        /// <param name="brandID">brand where the photo was uploaded</param>
        /// <param name="approvalStatus"></param>
        bool SendPhotoApprovedNotification(int memberID, int photoID, int brandID, string approvalStatus);

        /// Send an email to a member to let them know that an administrator has reviewed
        /// and approved one of their uploaded photos.
        /// Requires good email
        /// </summary>
        /// <param name="memberID">member who owns the approved photo</param>
        /// <param name="photoID">identifier for photo that has been approved</param>
        /// <param name="brandID">brand where the photo was uploaded</param>
        /// <param name="approvalStatus"></param>
        bool SendPhotoApprovedNotification(int memberID, int photoID, int brandID, string approvalStatus,
                                                           bool sendCaptionRejectedNotification);

        /// Send an email to a member to let them know that an administrator has reviewed
        /// and rejected one of their  photos caption.
        /// Requires good email
        /// </summary>
        /// <param name="memberID">member who owns the approved photo</param>
        /// <param name="photoID">identifier for photo that has been approved</param>
        /// <param name="brandID">brand where the photo was uploaded</param>
        /// <param name="approvalStatus"></param>
        bool SendPhotoCaptionRejectNotification(int memberID, int photoID, int brandID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID">member to receive the email</param>
        /// <param name="hotListedByMemberID">member who has hotlisted the member receiving the email</param>
        /// <param name="brandID">brand where the hot list adding occurred</param>
        /// <returns></returns>
        bool SendHotListedNotification(int memberID, int hotListedByMemberID, int brandID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="siteID"></param>
        /// <param name="brandID"></param>
        /// <param name="userName"></param>
        /// <param name="emailAddress"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="address1"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="postalCode"></param>
        /// <param name="dateOfPurchase"></param>
        /// <param name="planType"></param>
        /// <param name="currencyType"></param>
        /// <param name="initialCost"></param>
        /// <param name="initialDuration"></param>
        /// <param name="initialDurationType"></param>
        /// <param name="renewCost"></param>
        /// <param name="renewDuration"></param>
        /// <param name="renewDurationType"></param>
        /// <param name="last4CC"></param>
        /// <param name="creditCardTypeID"></param>
        /// <param name="confirmationNumber"></param>
        /// <returns></returns>
        bool SendSubscriptionConfirmation(int memberID, int siteID, int brandID, string userName,
                                                          string emailAddress, string firstName, string lastName, string address1,
                                                          string city,
                                                          string state,
                                                          string postalCode,
                                                          DateTime dateOfPurchase,
                                                          int planType,
                                                          int currencyType,
                                                          decimal initialCost,
                                                          int initialDuration,
                                                          DurationType initialDurationType,
                                                          decimal renewCost,
                                                          int renewDuration,
                                                          DurationType renewDurationType,
                                                          string last4CC,
                                                          int creditCardTypeID,
                                                          string confirmationNumber);

        bool SendSubscriptionConfirmation(int memberID, int siteID,
                                                          string firstName, string lastName, string address1,
                                                          string city,
                                                          string state,
                                                          string postalCode,
                                                          DateTime dateOfPurchase,
                                                          int planType,
                                                          int currencyType,
                                                          decimal initialCost,
                                                          int initialDuration,
                                                          DurationType initialDurationType,
                                                          decimal renewCost,
                                                          int renewDuration,
                                                          DurationType renewDurationType,
                                                          string last4CC,
                                                          int creditCardTypeID,
                                                          string confirmationNumber);

        bool SendOneOffPurchaseConfirmation(int memberID,
                                                            int siteID,
                                                            //int brandID, 
                                                            //string userName, 
                                                            //string emailAddress, 
                                                            string firstName,
                                                            string lastName,
                                                            DateTime dateOfPurchase,
                                                            //int planType,
                                                            int currencyType,
                                                            decimal initialCost,
                                                            int initialDuration,
                                                            DurationType initialDurationType,
                                                            string last4CC,
                                                            int creditCardTypeID,
                                                            string confirmationNumber);

        /// <summary>
        /// send payment profile confirmation
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="siteID"></param>
        /// <param name="brandID"></param>
        /// <param name="emailAddress"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="dateOfPurchase"></param>
        /// <param name="last4CC"></param>
        /// <param name="creditCardTypeID"></param>
        /// <param name="confirmationNumber"></param>
        /// <returns></returns>
        bool SendPaymentProfileConfirmation(int memberID,
                                                            int siteID,
                                                            int brandID,
                                                            string emailAddress,
                                                            string firstName,
                                                            string lastName,
                                                            DateTime dateOfPurchase,
                                                            string last4CC,
                                                            string confirmationNumber);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="siteID"></param>
        /// <param name="memberID"></param>
        /// <param name="boardName"></param>
        /// <param name="topicSubject"></param>
        /// <param name="replyURL"></param>
        /// <param name="newReplies"></param>
        /// <param name="unsubscribeURL"></param>
        /// <returns></returns>
        bool SendMessageBoardInstantNotification(int brandID, int siteID, int memberID, string boardName, string topicSubject, string replyURL, int newReplies, string unsubscribeURL);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="siteID"></param>
        /// <param name="memberID"></param>
        /// <param name="boardName"></param>
        /// <param name="topicSubject"></param>
        /// <param name="newReplies"></param>
        /// <param name="boardID"></param>
        /// <param name="threadID"></param>
        /// <param name="lastMessageID"></param>
        /// <param name="unsubscribeURL"></param>
        /// <returns></returns>
        bool SendMessageBoardDailyUpdates(int brandID, int siteID, int memberID, string boardName, string topicSubject, int newReplies, int boardID, int threadID, int lastMessageID, string unsubscribeURL);

        bool SendEcardToMember(int brandID, int siteID, int senderMemberID
                                               , string cardUrl, string cardThumbnail, int recipientMemberID, DateTime sentDate);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="siteID"></param>
        /// <param name="memberID"></param>
        /// <param name="senderUsername"></param>
        /// <param name="senderAge"></param>
        /// <param name="senderLocation"></param>
        /// <param name="senderThumbnail"></param>
        /// <param name="cardUrl"></param>
        /// <param name="cardThumbnail"></param>
        /// <param name="recipientEmailAddress"></param>
        /// <param name="recipientUsername"></param>
        /// <param name="sentDate"></param>
        /// <returns></returns>
        bool SendEcardToFriend(int brandID, int siteID, int senderMemberID, string cardUrl
                                               , string cardThumbnail, string recipientEmailAddress, DateTime sentDate);

        /// <summary>
        /// Send an email to a member to let them know that an administrator has reviewed
        /// and rejected one of their uploaded photos.
        /// Requires good email
        /// </summary>
        /// <param name="memberID">member who owns the rejected photo</param>
        /// <param name="photoID">identifier for photo that has been rejected</param>
        /// <param name="brandID">brand where the photo was uploaded</param>
        /// <param name="comment">administrator's comments about why the image was unacceptable</param>
        bool SendPhotoRejectedNotification(int memberID, int photoID, int brandID, string comment);

        /// <summary>
        /// Send a viral email
        /// Requires good email
        /// </summary>
        /// <param name="brandID">brandid for the current brand</param>
        /// <param name="emailAddress">email address of the recipiient</param>
        /// <param name="miniprofileInfo">receipient's miniprofile</param>
        /// <param name="miniprofileInfoCollection">list of members to be presented to the recipient</param>
        /// <returns>true if success, false otherwise</returns>
        bool SendViralEmail(int brandID, String emailAddress,
                                            MiniprofileInfo miniprofileInfo,
                                            MiniprofileInfoCollection miniprofileInfoCollection);

        /// <summary>
        /// Send a match email
        /// Requires good email
        /// </summary>
        /// <param name="brandID">brand for which the email is being sent</param>
        /// <param name="emailAddress">member who will receive the email</param>
        /// <param name="miniprofileInfo">member miniprofile who will receive the email</param>
        /// <param name="miniprofileInfoCollection">list of members to be presented to the recipient</param>
        bool SendMatchMail(int brandID, String emailAddress,
                                           MiniprofileInfo miniprofileInfo,
                                           MiniprofileInfoCollection miniprofileInfoCollection);

        /// <summary>
        /// Send a "new members in your area" email
        /// Requires good email
        /// </summary>
        /// <param name="brandID">brand for which the email is being sent</param>
        /// <param name="emailAddress">member who will receive the email</param>
        /// <param name="miniprofileInfo">member miniprofile who will receive the email</param>
        /// <param name="miniprofileInfoCollection">list of members to be presented to the recipient</param>
        bool SendNewMemberMail(int brandID, String emailAddress,
                                               MiniprofileInfo miniprofileInfo,
                                               MiniprofileInfoCollection miniprofileInfoCollection);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="receiverSiteID"></param>
        /// <param name="receiverMemberID"></param>
        /// <param name="senderMemberID"></param>
        /// <param name="senderBrandID"></param>
        /// <param name="receiverBrandID"></param>
        /// <param name="targetPhoneNumber"></param>
        /// <returns></returns>
        bool SendNewEmailSMSAlert(int receiverSiteID, int receiverMemberID, int senderMemberID, int senderBrandID, int receiverBrandID, string targetPhoneNumber);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="receiverSiteID"></param>
        /// <param name="receiverMemberID"></param>
        /// <param name="senderMemberID"></param>
        /// <param name="senderBrandID"></param>
        /// <param name="receiverBrandID"></param>
        /// <param name="targetPhoneNumber"></param>
        /// <returns></returns>
        bool SendHotListedSMSAlert(int receiverSiteID, int receiverMemberID, int senderMemberID, int senderBrandID, int receiverBrandID, string targetPhoneNumber);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="receiverSiteID"></param>
        /// <param name="receiverMemberID"></param>
        /// <param name="senderMemberID"></param>
        /// <param name="senderBrandID"></param>
        /// <param name="receiverBrandID"></param>
        /// <param name="targetPhoneNumber"></param>
        /// <returns></returns>
        bool SendNewFlirtSMSAlert(int receiverSiteID, int receiverMemberID, int senderMemberID, int senderBrandID, int receiverBrandID, string targetPhoneNumber);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="receiverSiteID"></param>
        /// <param name="receiverMemberID"></param>
        /// <param name="senderMemberID"></param>
        /// <param name="senderBrandID"></param>
        /// <param name="receiverBrandID"></param>
        /// <param name="targetPhoneNumber"></param>
        /// <returns></returns>
        bool SendClickedSMSAlert(int receiverSiteID, int receiverMemberID, int senderMemberID, int senderBrandID, int receiverBrandID, string targetPhoneNumber);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="receiverSiteID"></param>
        /// <param name="receiverMemberID"></param>
        /// <param name="senderMemberID"></param>
        /// <param name="senderBrandID"></param>
        /// <param name="receiverBrandID"></param>
        /// <param name="targetPhoneNumber"></param>
        /// <returns></returns>
        bool SendNewECardSMSAlert(int receiverSiteID, int receiverMemberID, int senderMemberID, int senderBrandID, int receiverBrandID, string targetPhoneNumber);

        /// <summary>
        /// Although the method name says Send, it's only doing the logging of a validation SMS send for now.
        /// </summary>
        /// <param name="siteID"></param>
        /// <param name="receivingMemberID"></param>
        /// <param name="sendingMemberID"></param>
        /// <param name="brandID"></param>
        /// <param name="targetPhoneNumber"></param>
        /// <returns></returns>
        bool SendValidationSMSAlert(int siteID, int receivingMemberID, int sendingMemberID, int brandID, string targetPhoneNumber);

        /// <summary>
        /// Sends the compatibility test invitation mail
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="memberName"></param>
        /// <param name="eMail"></param>
        /// <returns></returns>
        //		public bool SendCompatibilityTestInvitation(int brandID, string memberName, string eMail)
        //		{
        //			return  EnqueueMailObj(new CompatibilityTestInvitation(brandID, memberName, eMail));
        //		}
        bool SendMatchMeterInvitationMail(int brandID, int siteID, int senderMemberID, string senderUsername, string emailAddress, string recipientEmailAddress);

        bool SendMatchMeterCongratsMail(int brandID, int siteID, int senderMemberID, string senderUsername, string emailAddr);
        bool SendAuthCodeSMSAlert(int brandID, int siteID, int receivingMemberID, string targetPhoneNumber, string authCode);
        bool SendRegistrationSuccessSMSAlert(int brandID, int siteID, int receivingMemberID, string targetPhoneNumber);

        /// <summary>
        /// Sends a member's quesiton and answer link to friend
        /// </summary>
        /// <param name="friendName"></param>
        /// <param name="friendEmail"></param>
        /// <param name="senderEmail"></param>
        /// <param name="senderName"></param>
        /// <param name="senderMemberID"></param>
        /// <param name="subject"></param>
        /// <param name="message"></param>
        /// <param name="brandID"></param>
        /// <param name="questionId"></param>
        /// <param name="questionText"></param>
        /// <returns></returns>
        bool SendMemberQuestion(string friendName,
                                                string friendEmail,
                                                string senderEmail,
                                                string senderName,
                                                int senderMemberID,
                                                string subject,
                                                string message,
                                                int brandID,
                                                int questionId,
                                                string questionText);

        bool SendFriendReferralEmail(string senderName,
                                                     string message,
                                                     string friendEmail,
                                                     string prm,
                                                     string lgid,
                                                     string subject,
                                                     int senderMemberID,
                                                     int brandID);

        bool UpdatePhonesList(int memberID, int brandID, string phoneNumber, SMSAlertsDefinitions.Action action);
        bool IsPhoneAvailable(int siteID, string phoneNumber);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="mobileNumber"></param>
        /// <param name="memberID"></param>
        /// <param name="emailAddress"></param>
        /// <param name="brandID"></param>
        /// <returns></returns>
        bool SendJDateMobileRegistrationConfirmation(string userName, string mobileNumber, int memberID, string emailAddress, int brandID);

        bool SendScheduledEmail(int groupid, int memberid, int emailscheduleid, string schedulename, string scheduleguid, string email, Hashtable details);
        void SendBetaFeedback(int brandID, string senderName, string senderEmailAddress, string senderMemberID, string browser, string remoteIP, string subject, string userComment, int feedbackTypeID);

        /// <summary>
        /// Checks to see if the member wants to receive Messmo messages. This method does not check against message types,
        /// so technically this method can return true, but the member can end up not receiving anything.
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="communityID"></param>
        /// <returns></returns>
        bool IsMessmoMessageRequired(int memberID, int communityID);

        /// <summary>
        /// Send an Member Question to Spark.Net personnel
        /// 
        /// </summary>
        /// <param name="brandID">brand of the site that was the source of the contact</param>
        /// <param name="userName">user-supplied username</param>
        /// <param name="fromFirstName">user-supplied first name</param>
        /// <param name="fromLastName">user-supplied last name</param>
        /// <param name="memberID">user-supplied memberID</param>
        /// <param name="browser">browser ID string from request</param>
        /// <param name="remoteIP">remote IP for the request</param>        
        /// <param name="questionText">user free form question text</param>
        /// <param name="isAnonymous">flag to indicate anonymity</param>
        /// 
        void SendMemberGeneratedQuestion(int brandID, string userName, string fromFirstName, string fromLastName, int memberID, string questionText, bool isAnonymous);

        bool NotifyOfMessmoMessageArrival(int memberID, int brandID, Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMsgType);

        /// <summary>
        /// Called from Email svc. Sends a Messmo message to the member's mobile if it is required.
        /// </summary>
        /// <param name="sendResult"></param>
        /// <returns></returns>
        bool SendMessmoMessageFromEmail(Matchnet.Email.ValueObjects.MessageSendResult sendResult);

        /// <summary>
        /// Called from the List Service to send a Messmo message to the member's mobile.
        /// </summary>
        /// <param name="communityID"></param>
        /// <param name="recipientBrandID"></param>
        /// <param name="senderBrandID"></param>
        /// <param name="recipientMemberID"></param>
        /// <param name="senderMemberID"></param>
        /// <param name="hotlistCat"></param>
        /// <returns></returns>
        bool SendMessmoMessageFromList(int communityID, int recipientBrandID, int senderBrandID, int recipientMemberID, int senderMemberID,
                                                       Matchnet.List.ValueObjects.HotListCategory hotlistCat);

        /// <summary>
        /// Maps the MessmoMessageType to MessmoButtonType before calling the overloaded SendMessmoMultiPartMessage.  This is the ScheduledFeed service version where
        /// notify flag can be specified to disable it.
        /// </summary>
        /// <param name="recipientBrandID"></param>
        /// <param name="senderBrandID"></param>
        /// <param name="senderMemberID"></param>
        /// <param name="messageDate"></param>
        /// <param name="receivingMessmoSubID"></param>
        /// <param name="messageBody"></param>
        /// <param name="smartMessageTag"></param>
        /// <param name="taggle"></param>
        /// <param name="replace"></param>
        /// <param name="notify"></param>
        /// <param name="messmoMsgType"></param>
        /// <returns></returns>
        bool SendMessmoMultiPartMessage(int recipientBrandID, int senderBrandID, int senderMemberID, DateTime messageDate, string receivingMessmoSubID,
                                                        string messageBody, string smartMessageTag, string taggle, bool replace, bool notify, Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMsgType);

        /// <summary>
        /// This method isn't one of the Messmo API call.  It's a grouping of Messmo API calls that must be made right after the initial sign-up
        /// of Messmo's service.  This call will send the member's subcription end date and the initial batch of messages to Messmo.  There is a
        /// delay between where Messmo starts recognizing the Messmo subscriber ID and where we sign the member up for the first time.
        /// For this reason, this impulse is going to be a delayed impulse, meaning thread.sleep will be used.  According to Messmo, the maximum
        /// time for this delay is 1 minute.
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="timestamp"></param>
        /// <param name="memberID"></param>
        /// <param name="messmoSubscriberID"></param>
        /// <param name="status"></param>
        /// <param name="setDate"></param>
        /// <param name="mobileNumber"></param>
        void SendInitialBatchOfMessmoMessages(int brandID, DateTime timestamp, int memberID, string messmoSubscriberID, string status,
                                                              DateTime setDate, string mobileNumber);

        /// <summary>
        /// Messmo's API 2.5 - A Channel Sends Content to a User. This isn't being used in the beginning. This  can be used in the future to
        /// send simple messages (i.e. system message).
        /// </summary>	
        /// <param name="recipientBrandID">This is used only to validate if the site is enabled for ExternalMail</param>	
        /// <param name="emailDate"></param>
        /// <param name="emailBody"></param>
        /// <param name="receivingMessmoSubID"></param>
        /// <param name="taggle"></param>
        /// <returns></returns>
        void SendMessmoMessage(int recipientBrandID, DateTime emailDate, string emailBody, string receivingMessmoSubID,
                                               string taggle);

        /// <summary>
        /// Messmo's API 2.2 call. Although the name suggests Subscription, this is really used to reset the password with Messmo.
        /// When our member changes his password, we have to let Messmo know, and their Subscribe API call is the only way to do so.
        /// 
        /// *The actual subscription process (from the web) calls the same Messmo API, but synchronously so the call wouldn't call
        ///  through this method.
        /// </summary>
        ///<param name="brandID">Brand where the Messmo subscripition request is made</param>
        ///<param name="memberID"></param>
        /// <param name="timestamp"></param>
        /// <param name="mobileNumber"></param>
        /// <param name="encryptedPassword"></param>
        /// <returns></returns>
        bool SendMessmoSubscriptionRequest(int brandID, int memberID, DateTime timestamp, string mobileNumber, string encryptedPassword);

        /// <summary>
        /// Messmo API 2.21 A Channel Sets a User's Transaction Status
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="timestamp"></param>
        /// <param name="messmoSubscriberID"></param>
        /// <param name="status"></param>
        /// <param name="setDate"></param>
        void SendMessmoUserTransStatusRequest(int brandID, DateTime timestamp, string messmoSubscriberID, string status, DateTime setDate);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="timestamp"></param>
        /// <param name="messmoSubscriberID"></param>
        /// <param name="status"></param>
        /// <param name="setDate"></param>
        void SendMessmoUserTransStatusRequestBySiteID(int siteID, DateTime timestamp, string messmoSubscriberID, string status, DateTime setDate);

        /// <summary>
        /// Messmo API 2.23 a Channel Provides Full Profile Details to Messmo
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="memberID"></param>
        /// <param name="timestamp"></param>
        void SendMessmoFullProfileRequest(int brandID, int memberID, DateTime timestamp);

        /// <summary>
        /// Messmo API 2.18 A Channel sends an SMS to a User
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="timestamp"></param>
        /// <param name="mobileNumber"></param>
        /// <param name="message"></param>
        void SendMessmoSMSRequest(int brandID, DateTime timestamp, string mobileNumber, string message);

        bool SendViewedYouEmail(int memberID, int viewedByMemberID, int numberOfMembersWhoViewedSinceLastEmail, int brandID);
        void SendBBEPasscodeEmail(int memberId, int brandID, string emailAddress, string passcode);
    }
}
