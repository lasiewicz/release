using System;

namespace Matchnet.ExternalMail.ValueObjects
{
	public class ServiceConstants
	{
		/// <summary>
		/// 
		/// </summary>
		public const string SERVICE_CONSTANT = "EXTMAIL_SVC";
		/// <summary>
		/// 
		/// </summary>
		public const string SERVICE_NAME = "Matchnet.ExternalMail.Service";
		/// <summary>
		/// 
		/// </summary>
		public const string SERVICE_MANAGER_NAME = "ExternalMailSM";
		
		private ServiceConstants()
		{
		}
	}

	public enum BulkMailType
	{
		/// <summary>
		/// Match Newsletter
		/// </summary>
		MatchMail = 1,
		/// <summary>
		/// a.k.a. ViralMail
		/// </summary>
		ClickMail = 2, 
		/// <summary>
		/// New Members in Your Area Newsletter
		/// </summary>
		NewMembers = 3
	}
}
