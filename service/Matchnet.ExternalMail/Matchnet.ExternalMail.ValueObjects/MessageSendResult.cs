using System;

namespace Matchnet.ExternalMail.ValueObjects
{
	/// <summary>
	/// Summary description for MessageSendResult.
	/// </summary>
	[Serializable]
	public class MessageSendResult
	{
		private MessageSendStatus _status;
		private string _message;

		/// <summary>
		/// Create a new result with a status and a message
		/// </summary>
		/// <param name="status">status</param>
		/// <param name="message">message</param>
		public MessageSendResult(MessageSendStatus status, string message)
		{
			_status = status;
			_message = message;
		}

		/// <summary>
		/// Create a new result with a status and a blank message
		/// </summary>
		/// <param name="status">status</param>
		public MessageSendResult(MessageSendStatus status) : this(status, string.Empty)
		{
			//	Nothing else to do
		}

		/// <summary>
		/// Status of the attempted send
		/// </summary>
		public MessageSendStatus Status
		{
			get { return(_status); }
		}

		/// <summary>
		/// Message
		/// </summary>
		public string Message
		{
			get { return(_message); }
		}

		/// <summary>
		/// Dummy success object to be used when fleshing out the API
		/// </summary>
		public static MessageSendResult Default
		{
			get { return(new MessageSendResult(MessageSendStatus.FailureSystem,
												"This is just a default return value - no email was actually sent because the method you called has not been implemented")); }
		}


		public static MessageSendResult Success 
		{
			get 
			{
				return new MessageSendResult(MessageSendStatus.Success);
			}
		}
	}

	public enum MessageSendStatus
	{
		Success = 1,
		FailureBadEmail = 2,
		FailureSystem = 3
	}
}
