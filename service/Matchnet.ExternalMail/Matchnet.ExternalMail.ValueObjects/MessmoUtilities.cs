using System;
using Matchnet.List.ValueObjects;

namespace Matchnet.ExternalMail.ValueObjects
{
	/// <summary>
	/// Summary description for MessmoUtilities.
	/// </summary>
	public class MessmoUtilities
	{		

		/// <summary>
		/// Maps the reciprocal HotListCategory only for some categories
		/// </summary>
		/// <param name="cat"></param>
		/// <returns></returns>
		public static HotListCategory GetReciprocalHotListCategory(HotListCategory cat)
		{		
			HotListCategory ret = HotListCategory.Default;
			
			switch(cat)
			{
				case HotListCategory.MembersYouViewed:
					ret = HotListCategory.WhoViewedYourProfile;
					break;
				case HotListCategory.Default:
					ret = HotListCategory.WhoAddedYouToTheirFavorites;
					break;
			}

			return ret;
		}

		/// <summary>
		/// Maps our List type to Messmo Message Type
		/// </summary>
		/// <param name="hotListCategory"></param>
		/// <returns></returns>
		public static Matchnet.HTTPMessaging.Constants.MessmoMessageType MapHotListCategoryToMessmoMessageType(Matchnet.List.ValueObjects.HotListCategory hotListCategory)
		{
			Matchnet.HTTPMessaging.Constants.MessmoMessageType ret = Matchnet.HTTPMessaging.Constants.MessmoMessageType.None;

			switch(hotListCategory)
			{
				case Matchnet.List.ValueObjects.HotListCategory.WhoViewedYourProfile:
					ret = Matchnet.HTTPMessaging.Constants.MessmoMessageType.ViewedProfile;
					break;
				case Matchnet.List.ValueObjects.HotListCategory.WhoAddedYouToTheirFavorites:
					ret = Matchnet.HTTPMessaging.Constants.MessmoMessageType.Hotlisted;
					break;
				case Matchnet.List.ValueObjects.HotListCategory.YourMatches:
					ret = Matchnet.HTTPMessaging.Constants.MessmoMessageType.Matches;
					break;
				case Matchnet.List.ValueObjects.HotListCategory.Default:
					ret = Matchnet.HTTPMessaging.Constants.MessmoMessageType.Favorites;
					break;
				case Matchnet.List.ValueObjects.HotListCategory.MembersYouViewed:
					ret = Matchnet.HTTPMessaging.Constants.MessmoMessageType.MembersYouViewed;
					break;
			}

			return ret;
		}

		/// <summary>
		/// Takes our Email MailType and maps the value to MessageMessageType. We do this since we don't send all of our mail types
		/// to Messmo
		/// </summary>
		/// <param name="mailType"></param>
		/// <returns></returns>
		public static Matchnet.HTTPMessaging.Constants.MessmoMessageType MapMailTypeToMessmoMessageType(Matchnet.Email.ValueObjects.MailType mailType)
		{
			Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMsgType = Matchnet.HTTPMessaging.Constants.MessmoMessageType.None;

			switch(mailType)
			{
				case Matchnet.Email.ValueObjects.MailType.Email:
					messmoMsgType = Matchnet.HTTPMessaging.Constants.MessmoMessageType.Email;
					break;
				case Matchnet.Email.ValueObjects.MailType.Tease:
					messmoMsgType = Matchnet.HTTPMessaging.Constants.MessmoMessageType.Flirt;
					break;
				case Matchnet.Email.ValueObjects.MailType.ECard:
					messmoMsgType = Matchnet.HTTPMessaging.Constants.MessmoMessageType.Ecard;
					break;
				case Matchnet.Email.ValueObjects.MailType.MissedIM:
					messmoMsgType = Matchnet.HTTPMessaging.Constants.MessmoMessageType.MissedIM;
					break;
				default:
					break;
			}

			return messmoMsgType;
		}
	}
}
