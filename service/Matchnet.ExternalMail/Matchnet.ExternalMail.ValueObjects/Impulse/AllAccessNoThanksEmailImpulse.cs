﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
    [Serializable]
    public class AllAccessNoThanksEmailImpulse : ImpulseBaseEmail
    {
        private int _senderMemberId;
        private int _senderBrandId;
        private int _recipientMemberId;

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_ALL_ACCESS_NO_THANKS"; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendAllAccessNoThanksEmail; }
        }

        public override int TargetMemberId
        {
            get { return _recipientMemberId; }
        }

        public override string EmailAddress
        {
            get { return Constants.NULL_STRING; }
        }

        public override int MemberIDForRandomSelection
        {
            get { return _recipientMemberId; }
        }

        public int SenderMemberId
        {
            get { return _senderMemberId; }
        }

        public int SenderBrandId
        {
            get { return _senderBrandId; }
        }

        public AllAccessNoThanksEmailImpulse(int pSenderMemberId, int pSenderBrandId, int pRecipientMemberId, int pRecipientBrandId)
        {
            this._senderMemberId = pSenderMemberId;
            this._senderBrandId = pSenderBrandId;
            this._recipientMemberId = pRecipientMemberId;
            this.BrandID = pRecipientBrandId;
        }

        public override string GetQueryString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(base.GetQueryString());
            sb.Append("&SenderMemberID=" + _senderMemberId.ToString());
            sb.Append("&SenderBrandID=" + _senderBrandId.ToString());

            return sb.ToString();
        }
    }
}
