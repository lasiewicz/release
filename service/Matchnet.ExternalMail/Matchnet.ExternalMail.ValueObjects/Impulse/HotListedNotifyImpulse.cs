using System;
using System.Text;
namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	[Serializable]
	public class HotListedNotifyImpulse : ImpulseBaseEmail
	{
		private int _memberID;
		private int _hotListedByMemberID;

		private HotListedNotifyImpulse()
		{
		}

		public HotListedNotifyImpulse(int memberID, int hotListedByMemberID, int brandID)
		{
			base.BrandID = brandID;
			_memberID = memberID;
			_hotListedByMemberID = hotListedByMemberID;
		}

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _memberID;
			}
		}


		public int MemberID
		{
			get { return(_memberID); }
		}

		public int HotListedByMemberID
		{
			get { return(_hotListedByMemberID); }
		}

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_HOT_LIST_ALERT"; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendHotListAlert; }
        }

        public override int TargetMemberId
        {
            get { return this.MemberID; }
        }

        public override string EmailAddress
        {
            get { return Constants.NULL_STRING; }
        }

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append(base.GetQueryString());
			sb.Append("&MemberID=");
			sb.Append(MemberID);
			sb.Append("&HotListedByMemberID=");
			sb.Append(HotListedByMemberID);

			return(sb.ToString());
		}

        
    }
}
