using System;
using System.Collections;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using Matchnet.ActivityRecording.ValueObjects.Types;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	[Serializable]
	public class MatchMailImpulse : MassMailImpulse, IByteSerializable, ISerializable
	{
        private const byte VERSION_001 = 1;
        private const byte VERSION_002 = 2;
		public const Int32 MATCH_COUNT = 6;

		#region Constructors
		/// <summary>
		/// Needed for deserialization
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MatchMailImpulse(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		public MatchMailImpulse(Int32 brandID, String recipientEmailAddress, 
			MiniprofileInfo recipientMiniprofileInfo,
			MiniprofileInfoCollection targetMiniprofileInfoCollection)
		{
			base.BrandID = brandID;
			base.RecipientEmailAddress = recipientEmailAddress;
			base.RecipientMiniprofileInfo = recipientMiniprofileInfo;
			base.TargetMiniprofileInfoCollection = targetMiniprofileInfoCollection;
		}
		#endregion

        public override ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return ActionType.SendMatchMail; }
        }

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_MATCH_MAIL"; }
        }

		#region Public Methods
		#region IByteSerializable Members

		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new BinaryReader(ms);

			byte version = br.ReadByte();

			switch (version)
			{
				case VERSION_001:

					this.BrandID = br.ReadInt32();
					this.RecipientEmailAddress = br.ReadString();
					MiniprofileInfo mini = new MiniprofileInfo();

					int miniLength = br.ReadInt32();
					mini.FromByteArray(br.ReadBytes(miniLength));
					this.RecipientMiniprofileInfo = mini;

					MiniprofileInfoCollection minis = new MiniprofileInfoCollection();
					int minisLength = br.ReadInt32();
					minis.FromByteArray(br.ReadBytes(minisLength));
					this.TargetMiniprofileInfoCollection = minis;

					break;
                case VERSION_002:

                    //Added SearchPrefType for "strict" or "relax"
                    this.BrandID = br.ReadInt32();
                    this.SearchPrefType = br.ReadString();
                    this.RecipientEmailAddress = br.ReadString();
                    MiniprofileInfo mini2 = new MiniprofileInfo();

                    int miniLength2 = br.ReadInt32();
                    mini2.FromByteArray(br.ReadBytes(miniLength2));
                    this.RecipientMiniprofileInfo = mini2;

                    MiniprofileInfoCollection minis2 = new MiniprofileInfoCollection();
                    int minisLength2 = br.ReadInt32();
                    minis2.FromByteArray(br.ReadBytes(minisLength2));
                    this.TargetMiniprofileInfoCollection = minis2;

                    break;
				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}

		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			byte version = VERSION_002;

			bw.Write(version);
			bw.Write(base.BrandID);
            bw.Write(base.SearchPrefType);
			writeString(bw, base.RecipientEmailAddress);
			
			byte[] recipientMiniprofileInfoBytes = base.RecipientMiniprofileInfo.ToByteArray();
			bw.Write(recipientMiniprofileInfoBytes.Length);
			bw.Write(recipientMiniprofileInfoBytes);

			byte[] targetMiniprofileInfoCollection = base.TargetMiniprofileInfoCollection.ToByteArray(); 
			bw.Write(targetMiniprofileInfoCollection.Length);
			bw.Write(targetMiniprofileInfoCollection);
			
			return ms.ToArray();
		}

		#endregion

		private static void writeString(BinaryWriter bw, string s)
		{
			if (s == null)
			{
				bw.Write(string.Empty);
			}
			else
			{
				bw.Write(s);
			}
		}

		#region ISerializable Members

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", this.ToByteArray());
		}

		#endregion
		#endregion
	}
}
