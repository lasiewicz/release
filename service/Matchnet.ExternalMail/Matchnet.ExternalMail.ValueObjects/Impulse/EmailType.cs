using System;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	public enum EmailType 
	{
		MatchNewsLetter = 1,
		MutualMail = 2,
		ViralNewsLetter = 3,
		NewMailAlert = 4,
		FreeTrial30 = 5,
		FreeTrial60 = 6,
		FreeTrial90 = 7,
		PremiumSubscriberWelcome = 8,
		CheckConfirmation = 9,
		ActivationLetter = 10,
		ForgotPassword = 11,
		PhotoAlbumRequest = 12,
		PhotoAlbumApprove = 13,
		SendToFriend = 14,
		CrossPromotion = 15,
		PhotoApproval = 16,
		ContactUs = 17,
		EventComments = 18,
		EmailChangeConfirmation = 19,
		Broadcast = 20,
		HotListedAlert = 21,
		ReportAbuse = 22,
		SubscriptionConfirmation = 23,
		MessageBoardInstantNotification = 24,
		MessageBoardDailyUpdates = 25,
		RenewalFailureNotification = 26,
		EcardToMember = 27,
		EcardToFriend = 28,
		PmtProfileConfirmation = 29,
		MatchMeterCongratsMail = 30,
		MatchMeterInvitationMail = 31,
		SendMemberColorCode = 32,
		ColorCodeQuizConfirmation = 33,
		SendMemberColorCodeQuizInvite = 34,
		OneOffPurchaseConfirmation = 35,
		SendMemberQuestionAndAnswer = 36,
		JDateMobileRegistrationConfirmation = 37,
        AllAccessNudgeEmail = 38,
        AllAccessInitialEmail = 39,
        AllAccessReplyEmail = 40,
        AllAccessNoThanksEmail = 41,
        AllAccessReturnEmail = 42,
        SendFriendReferral = 43,
		OptOutNotify = 69,
        RegistrationCapture=70,
        MemberQuestionMail = 75, 
        BetaFeedback = 76,
        PhotoRejection = 116,	//	This has to be treated like 16 when it is sent out,
								//	but the count gets sfet to 1 which makes other parts of the code really really clear :(
		PhotoCaptionRejection=117,
		NewMembersNewsLetter = 118,
        BOGONotification = 119,
        ProfileChangedConfirmation = 120,
        ViewedYouEmail = 121,
        BBEPasscodeEmail = 122,
        FTAWarnings = 123,
        SodbPhotoUpload = 124,
        NoEssayRequestEmail = 125,
		InternalCorpMail = 200,
		Unknown = 999
	}

	public class EmailTypeConverter 
	{
		public static int ToInt(ImpulseBase impulse) 
		{
			EmailType retVal = EmailType.Unknown;

			if(impulse is MatchMailImpulse)
			{
				retVal = EmailType.MatchNewsLetter;
			}
			else if (impulse is ViralImpulse)
			{
				retVal = EmailType.ViralNewsLetter;
			}
			else if(impulse is MutualYesNotifyImpulse)
			{
				retVal = EmailType.MutualMail;
			}
			else if(impulse is InternalMailNotifyImpulse)
			{
				retVal = EmailType.NewMailAlert;
			} 
			else if(impulse is HotListedNotifyImpulse)
			{
				retVal = EmailType.HotListedAlert;
			}
			else if(impulse is SendMemberImpulse)
			{
				retVal = EmailType.SendToFriend;
			}
			else if(impulse is EmailVerificationImpulse)
			{
				retVal = EmailType.EmailChangeConfirmation;
			}
			else if(impulse is PasswordMailImpulse)
			{
				retVal = EmailType.ForgotPassword;
			}
			else if(impulse is PhotoApprovedImpulse)
			{
				retVal = EmailType.PhotoApproval;
			}
			else if(impulse is PhotoRejectedImpulse)
			{
				retVal = EmailType.PhotoRejection;
			}
			else if(impulse is RegVerificationImpulse)
			{
				retVal = EmailType.ActivationLetter;
			}
			else if(impulse is ContactUsImpulse)
			{
				retVal = EmailType.ContactUs;
			}
			else if(impulse is OptOutNotifyImpulse)
			{
				retVal = EmailType.OptOutNotify;
			}
			else if(impulse is ReportAbuseImpulse)
			{
				retVal = EmailType.ReportAbuse;
			}
			else if(impulse is SubscriptionConfirmationImpulse)
			{
				retVal = EmailType.SubscriptionConfirmation;
			}
			else if(impulse is MessageBoardInstantNotificationImpulse)
			{
				retVal = EmailType.MessageBoardInstantNotification;
			}
			else if(impulse is MessageBoardDailyUpdatesImpulse)
			{
				retVal = EmailType.MessageBoardDailyUpdates;
			}
			else if(impulse is EcardToMemberImpulse)
			{
				retVal = EmailType.EcardToMember;
			}
			else if(impulse is EcardToFriendImpulse)
			{
				retVal = EmailType.EcardToFriend;
			}
			else if(impulse is RenewalFailureNotificationImpulse)
			{
				retVal = EmailType.RenewalFailureNotification;
			}
			else if(impulse is PmtProfileConfirmationImpulse)
			{
				retVal = EmailType.RenewalFailureNotification;
			}
			else if(impulse is MatchMeterInvitationMailImpulse)
			{
				retVal = EmailType.MatchMeterInvitationMail;
			}
			else if(impulse is MatchMeterCongratsMailImpulse)
			{
				retVal = EmailType.MatchMeterCongratsMail;
			}
			else if(impulse is InternalCorpMail)
			{
				retVal = EmailType.InternalCorpMail;
			}
			else if (impulse is SendMemberColorCodeImpulse)
			{
				retVal = EmailType.SendMemberColorCode;
			}
			else if (impulse is ColorCodeQuizConfirmationImpulse)
			{
				retVal = EmailType.ColorCodeQuizConfirmation;
			}
			else if (impulse is SendMemberColorCodeQuizInviteImpulse)
			{
				retVal = EmailType.SendMemberColorCodeQuizInvite;
			}
			else if (impulse is OneOffPurchaseConfirmationImpulse)
			{
				retVal = EmailType.OneOffPurchaseConfirmation;
			}
			else if (impulse is JDateMobileRegistrationConfirmationImpulse)
			{
				retVal = EmailType.JDateMobileRegistrationConfirmation;
			}
            else if (impulse is AllAccessEmailNudgeImpulse)
            {
                retVal = EmailType.AllAccessNudgeEmail;
            }
            else if (impulse is AllAccessInitialEmailImpulse)
            {
                retVal = EmailType.AllAccessInitialEmail;
            }
            else if (impulse is AllAccessReplyEmailImpulse)
            {
                retVal = EmailType.AllAccessReplyEmail;
            }
            else if (impulse is AllAccessNoThanksEmailImpulse)
            {
                retVal = EmailType.AllAccessNoThanksEmail;
            }
            else if (impulse is AllAccessReturnEmailImpulse)
            {
                retVal = EmailType.AllAccessReturnEmail;
            }
			else if (impulse is SendMemberQuestionAndAnswerImpulse)
			{
				retVal = EmailType.SendMemberQuestionAndAnswer;
            }
            else if (impulse is MemberQuestionImpulse)
            {
                retVal = EmailType.MemberQuestionMail;
            }
            else if (impulse is BetaFeedbackImpulse)
            {
                retVal = EmailType.BetaFeedback;
            }
            else if (impulse is ProfileChangedConfirmationImpulse)
            {
                retVal = EmailType.ProfileChangedConfirmation;
            }
            else if (impulse is ViewedYouEmailImpulse)
            {
                retVal=EmailType.ViewedYouEmail;
            }
            else if (impulse is FTAWarningsImpulse)
            {
                retVal = EmailType.FTAWarnings;
            }

			return((int)retVal);
		}
	}
}
