﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.ActivityRecording.ValueObjects.Types;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
    [Serializable]
    public class FTAWarningsImpulse : ImpulseBaseEmail
    {
        private int _memberId;
        private string[] _warnings;

        public string[] Warnings
        {
            get { return _warnings; }
        }

        #region Overrides of ImpulseBase

        public override int MemberIDForRandomSelection
        {
            get { return _memberId; }
        }

        #endregion

        #region Overrides of ImpulseBaseEmail

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_FTA_WARNINGS"; }
        }

        public override ActionType ActionType
        {
            get { return ActivityRecording.ValueObjects.Types.ActionType.SendFTAWarnings; }
        }

        public override int TargetMemberId
        {
            get { return _memberId; }
        }

        public override string EmailAddress
        {
            get { return Constants.NULL_STRING; }
        }

        #endregion

        public FTAWarningsImpulse(int pMemberId, int pBrandId, string[] pWarnings)
        {
            base.BrandID = pBrandId;
            _memberId = pMemberId;
            _warnings = pWarnings;
        }
    }
}
