using System;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for PhotoApprovedImpulse.
	/// </summary>
	[Serializable]
	public class PhotoRejectedImpulse : ImpulseBaseEmail
	{
		private int _memberID;
		private int _photoID;
		private string _comment;

		private PhotoRejectedImpulse()
		{
		}

		public PhotoRejectedImpulse(int memberID,
			int photoID,
			int brandID,
			string comment)
		{
			base.BrandID = brandID;
			_memberID = memberID;
			_photoID = photoID;
			_comment = comment;
		}

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _memberID;
			}
		}

		public int MemberID
		{
			get
			{
				return _memberID;
			}
		}

		public int PhotoID
		{
			get
			{
				return _photoID;
			}
		}

		public string Comment
		{
			get
			{
				return _comment;
			}
		}

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_PHOTO_REJECTION"; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendPhotoRejection; }
        }

        public override int TargetMemberId
        {
            get { return this.MemberID; }
        }

        public override string EmailAddress
        {
            get { return Constants.NULL_STRING; }
        }

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append(base.GetQueryString());
			sb.Append("&MemberID=");
			sb.Append(MemberID);
			sb.Append("&PhotoID=");
			sb.Append(PhotoID);
			sb.Append("&Comment=");
			sb.Append(Comment);

			return(sb.ToString());
		}

        
    }
}
