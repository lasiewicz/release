#region

using System;
using System.Text;
using System.Web;

#endregion

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
    /// <summary>
    ///     Summary description for ContactUsImpulse.
    /// </summary>
    [Serializable]
    public class ContactUsImpulse : ImpulseBase
    {
        private readonly string _browser;
        private readonly string _fromEmailAddress;
        private readonly string _reason;
        private readonly string _remoteIP;
        private readonly string _sMemberID;
        private readonly string _userComment;
        private readonly string _appDeviceInfo;
        private readonly string _userName;
        private readonly string _toEmailAddress;

        /// <summary>
        ///     Hide no arg constructor
        /// </summary>
        private ContactUsImpulse()
        {
        }

        /// <summary>
        ///     Create a full ContactUsImpulse
        /// </summary>
        /// <param name="brandID">brand of the site that was the source of the contact</param>
        /// <param name="userName">user-supplied username</param>
        /// <param name="fromEmailAddress">user-supplied email address</param>
        /// <param name="sMemberID">user-supplied MemberID</param>
        /// <param name="browser">browser ID string from request</param>
        /// <param name="remoteIP">remote IP for the request</param>
        /// <param name="reason">drop-down selected reason</param>
        /// <param name="userComment">user free form comment</param>
        /// <param name="appDeviceInfo">environment details of the app (i.e. version, OS, etc)</param>
        /// <param name="toEmailAddress">to Email Address</param>
        public ContactUsImpulse(int brandID,
            string userName,
            string fromEmailAddress,
            string sMemberID,
            string browser,
            string remoteIP,
            string reason,
            string userComment,
            string appDeviceInfo = null, 
            string toEmailAddress = null)
        {
            base.BrandID = brandID;
            _userName = userName;
            _fromEmailAddress = fromEmailAddress;
            _sMemberID = sMemberID;
            _browser = browser;
            _remoteIP = remoteIP;
            _reason = reason;
            _userComment = userComment;
            _appDeviceInfo = appDeviceInfo;
            _toEmailAddress = toEmailAddress;
        }

        public override Int32 MemberIDForRandomSelection
        {
            get
            {
                var memberID = Constants.NULL_INT;
                try
                {
                    memberID = int.Parse(_sMemberID);
                }
                catch
                {
                }
                return memberID;
            }
        }


        public string UserName
        {
            get { return (_userName); }
        }

        public string FromEmailAddress
        {
            get { return (_fromEmailAddress); }
        }

        public string SMemberID
        {
            get { return (_sMemberID); }
        }

        public string Browser
        {
            get { return (_browser); }
        }

        public string RemoteIP
        {
            get { return (_remoteIP); }
        }

        public string Reason
        {
            get { return (_reason); }
        }

        public string UserComment
        {
            get { return (_userComment); }
        }

        public string AppDeviceInfo
        {
            get { return (_appDeviceInfo); }
        }

        public string ToEmailAddress
        {
            get { return (_toEmailAddress); }
        }

        public override string GetQueryString()
        {
            var sb = new StringBuilder();

            sb.Append(base.GetQueryString());
            sb.Append("&UserName=");
            sb.Append(HttpUtility.UrlEncode(UserName));
            sb.Append("&FromEmailAddress=");
            sb.Append(HttpUtility.UrlEncode(FromEmailAddress));
            sb.Append("&SMemberID=");
            sb.Append(HttpUtility.UrlEncode(SMemberID));
            sb.Append("&Browser=");
            sb.Append(HttpUtility.UrlEncode(Browser));
            sb.Append("&RemoteIP=");
            sb.Append(HttpUtility.UrlEncode(RemoteIP));
            sb.Append("&Reason=");
            sb.Append(HttpUtility.UrlEncode(Reason));
            sb.Append("&UserComment=");
            sb.Append(HttpUtility.UrlEncode(UserComment));
            sb.Append("&AppDeviceInfo=");
            sb.Append(HttpUtility.UrlEncode(AppDeviceInfo));
            sb.Append("&ToEmailAddress=");
            sb.Append(HttpUtility.UrlEncode(ToEmailAddress));

            return (sb.ToString());
        }
    }
}