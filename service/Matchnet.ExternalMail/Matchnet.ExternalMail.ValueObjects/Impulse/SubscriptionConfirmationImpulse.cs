using System;
using System.Text;
using System.IO;
using System.Runtime.Serialization;

using Matchnet;
using Matchnet.ActivityRecording.ValueObjects.Types;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	[Serializable]
    public class SubscriptionConfirmationImpulse : ImpulseBaseEmail, ISerializable, IByteSerializable
	{
		private const byte VERSION_001 = 1;

		#region Private Variables
		private int _memberID = Constants.NULL_INT;
		private int _siteID = Constants.NULL_INT;
		private string _userName = Constants.NULL_STRING;
		private string _emailAddress = Constants.NULL_STRING;
		private string _firstName = Constants.NULL_STRING;
		private string _lastName = Constants.NULL_STRING;
		private string _address1 = Constants.NULL_STRING;
		private string _city = Constants.NULL_STRING;
		private string _state = Constants.NULL_STRING;
		private string _postalCode = Constants.NULL_STRING;
		private DateTime _dateOfPurchase =  new DateTime(DateTime.MinValue.Ticks);
		private int _planType = Constants.NULL_INT;
		private int _currencyType = Constants.NULL_INT;
		private decimal _initialCost = Constants.NULL_DECIMAL;
		private int _initialDuration = Constants.NULL_INT;
		private DurationType _initialDurationType;
		private decimal _renewCost = Constants.NULL_DECIMAL;
		private int _renewDuration = Constants.NULL_INT;
		private DurationType _renewDurationType;
		private string _last4CC = Constants.NULL_STRING;
		private int _creditCardTypeID = Constants.NULL_INT;
		private string _confirmationNumber = Constants.NULL_STRING;
		#endregion

		#region Public Properties
		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _memberID;
			}
		}

		public int MemberID
		{
			get
			{
				return _memberID;
			}
		}
		public int SiteID
		{
			get
			{
				return _siteID;
			}
		}
		public string UserName
		{
			get
			{
				return _userName;
			}
		}

		public override string EmailAddress
		{
			get
			{
				return _emailAddress;
			}
		}

		public string FirstName
		{
			get
			{
				return _firstName;
			}
		}

		public string LastName
		{
			get
			{
				return _lastName;
			}
		}

		public string Address1
		{
			get
			{
				return _address1;
			}
		}

		public string City
		{
			get
			{
				return _city;
			}
		}
		public string State
		{
			get
			{
				return _state;
			}
		}
		public string PostalCode
		{
			get
			{
				return _postalCode;
			}
		}
		public DateTime DateOfPurchase
		{
			get
			{
				return _dateOfPurchase;
			}
		}
		public int PlanType
		{
			get
			{
				return _planType;
			}
		}
		public int CurrencyType
		{
			get
			{
				return _currencyType;
			}
		}
		public decimal InitialCost
		{
			get
			{
				return _initialCost;
			}
		}
		public int InitialDuration
		{
			get
			{
				return _initialDuration;
			}
		}
		public DurationType InitialDurationType
		{
			get
			{
				return _initialDurationType;
			}
		}
		public decimal RenewCost
		{
			get
			{
				return _renewCost;
			}
		}
		public int RenewDuration
		{
			get
			{
				return _renewDuration;
			}
		}
		public DurationType RenewDurationType
		{
			get
			{
				return _renewDurationType;
			}
		}

		public string Last4CC
		{
			get
			{
				return _last4CC;
			}
			set
			{
				_last4CC = value;
			}
		}
		public int CreditCardTypeID
		{
			get
			{
				return _creditCardTypeID;
			}
			set
			{
				_creditCardTypeID = value;
			}
		}
		public string ConfirmationNumber
		{
			get
			{
				return _confirmationNumber;
			}
			set
			{
				_confirmationNumber = value;
			}
		}

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_SUBSCRIPTION_CONFIRMATION"; }
        }

        public override ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return ActionType.SendSubscriptionConfirmation; }
        }

        public override int TargetMemberId
        {
            get { return _memberID; }
        }
		#endregion

		#region Constructors
		/// <summary>
		/// Needed for deserialization
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SubscriptionConfirmationImpulse(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		public SubscriptionConfirmationImpulse(int memberID,
			int siteID,
			int brandID,
			string userName,
			string emailAddress,
			string firstName,
			string lastName,
			string address1,
			string city,
			string state,
			string postalCode,
			DateTime dateOfPurchase,
			int planType,
			int currencyType,
			decimal initialCost,
			int initialDuration,
			DurationType initialDurationType,
			decimal renewCost,
			int renewDuration,
			DurationType renewDurationType,
			string last4CC,
			int creditCardTypeID,
			string confirmationNumber)
		{
			this._memberID = memberID;
			this._siteID = siteID;
			this.BrandID = brandID;
			this._userName = userName;
			this._emailAddress = emailAddress;
			this._firstName = firstName;
			this._lastName = lastName;
			this._address1 = address1;
			this._city = city;
			this._state = state;
			this._postalCode = postalCode;
			this._dateOfPurchase = dateOfPurchase;
			this._planType = planType;
			this._currencyType = currencyType;
			this._initialCost = initialCost;
			this._initialDuration = initialDuration;
			this._initialDurationType = initialDurationType;
			this._renewCost = renewCost;
			this._renewDuration = renewDuration;
			this._renewDurationType = renewDurationType;
			this._last4CC = last4CC;
			this._creditCardTypeID = creditCardTypeID;
			this._confirmationNumber = confirmationNumber;
		}

		#endregion

		// Property not used in greymail. Need to get rid of it for good
		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append(base.GetQueryString());
			sb.Append("&MemberID=");
			sb.Append(this.MemberID.ToString());

			return(sb.ToString());
		}

		#region ISerializable Members
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", this.ToByteArray());
		}
		#endregion

		#region IByteSerializable Members

		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new BinaryReader(ms);

			byte version = br.ReadByte();

			switch (version)
			{
				case VERSION_001:
					_memberID = br.ReadInt32();
					_siteID = br.ReadInt32();
					BrandID = br.ReadInt32();
					_userName = br.ReadString();
					_emailAddress = br.ReadString();
					_firstName = br.ReadString();
					_lastName = br.ReadString();
					_address1 = br.ReadString();
					_city = br.ReadString();
					_state = br.ReadString();
					_postalCode = br.ReadString();
					_dateOfPurchase = new DateTime(br.ReadInt64());
					_planType = br.ReadInt32();
					_currencyType = br.ReadInt32();
					_initialCost = br.ReadDecimal();
					_initialDuration = br.ReadInt32();
					_initialDurationType = (DurationType) Enum.Parse(typeof(DurationType), br.ReadInt32().ToString());
					_renewCost = br.ReadDecimal();
					_renewDuration = br.ReadInt32();
					_renewDurationType = (DurationType) Enum.Parse(typeof(DurationType), br.ReadInt32().ToString());
					_last4CC = br.ReadString();
					_creditCardTypeID = br.ReadInt32();
					_confirmationNumber = br.ReadString();
					break;

				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}

		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			byte version = VERSION_001;

			bw.Write(version);
			bw.Write(_memberID);
			bw.Write(_siteID);
			bw.Write(BrandID);
			writeString(bw, _userName);
			writeString(bw, _emailAddress);
			writeString(bw, _firstName);
			writeString(bw, _lastName);
			writeString(bw, _address1);
			writeString(bw, _city);
			writeString(bw, _state);
			writeString(bw, _postalCode);
			bw.Write(_dateOfPurchase.Ticks);
			bw.Write(_planType);
			bw.Write(_currencyType);
			bw.Write(_initialCost);
			bw.Write(_initialDuration);
			bw.Write((int)_initialDurationType);
			bw.Write(_renewCost);
			bw.Write(_renewDuration);
			bw.Write((int)_renewDurationType);
			writeString(bw, _last4CC);
			bw.Write(_creditCardTypeID);
			writeString(bw, _confirmationNumber);
		
			return ms.ToArray();
		}

		private static void writeString(BinaryWriter bw, string s)
		{
			if (s == null)
			{
				bw.Write(string.Empty);
			}
			else
			{
				bw.Write(s);
			}
		}

		#endregion

    }
}
