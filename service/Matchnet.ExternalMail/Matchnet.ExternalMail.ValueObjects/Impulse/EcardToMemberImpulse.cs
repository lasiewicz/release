using System;
using System.IO;
using System.Runtime.Serialization;
using Matchnet.ActivityRecording.ValueObjects.Types;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Used by Spark WS.
	/// Mingle makes a call to Spark WS to trigger a new email which in turn uses this class.
	/// This is identical to EcardToFriend, but kept the impulses separate in case there is a divergence later.
	/// </summary>
	[Serializable]
    public class EcardToMemberImpulse : ImpulseBaseEmail, IByteSerializable, ISerializable
	{
		#region Private Constants
		private const byte VERSION_001 = 1;
		#endregion

		#region Private Variables
		private int _siteID;
		private string _senderUsername;
		private int _senderMemberID;
		private string _senderLocation;
		private string _senderThumbnail;
		private string _cardUrl;
		private string _cardThumbnail;
		private int _recipientMemberID;
		private DateTime _sentDate;
		#endregion

		#region Public Properties
		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _senderMemberID;
			}
		}


		public int SiteID
		{
			get { return _siteID;}	
			set { _siteID = value;}
		}
	

		/// <summary>
		/// 
		/// </summary>
		public int SenderMemberID
		{
			get { return _senderMemberID; }
			set { _senderMemberID = value; }
		}

		
		/// <summary>
		/// 
		/// </summary>
		public string CardUrl
		{
			get { return _cardUrl; }
			set { _cardUrl = value; }
		}


		/// <summary>
		/// 
		/// </summary>
		public string CardThumbnail
		{
			get { return _cardThumbnail; }
			set { _cardThumbnail = value; }
		}


		/// <summary>
		/// 
		/// </summary>
		public int RecipientMemberID
		{
			get { return _recipientMemberID; }
			set { _recipientMemberID = value; }
		}


		/// <summary>
		/// 
		/// </summary>
		public DateTime SentDate
		{
			get { return _sentDate; }
			set { _sentDate = value; }
		}

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_ECARD_TO_MEMBER"; }
        }

        public override ActionType ActionType
        {
            get { return ActionType.SendEcardToMember; }
        }

        public override int TargetMemberId
        {
            get { return _recipientMemberID; }
        }

        public override string EmailAddress
        {
            get { return Constants.NULL_STRING; }
        }
		#endregion

		#region Constructors
		/// <summary>
		/// Needed for deserialization
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected EcardToMemberImpulse(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		public EcardToMemberImpulse(int brandID, int siteID, int senderMemberID
			, string cardUrl, string cardThumbnail, int recipientMemberID
			, DateTime sentDate)
		{
			base.BrandID = brandID;
			this._siteID = siteID;
			this._senderMemberID = senderMemberID;
			this._cardUrl = cardUrl;
			this._cardThumbnail = cardThumbnail;
			this._recipientMemberID = recipientMemberID;
			this._sentDate = sentDate;
		}

		#endregion

		#region Private Methods
		private static void writeString(BinaryWriter bw, string s)
		{
			if (s == null)
			{
				bw.Write(string.Empty);
			}
			else
			{
				bw.Write(s);
			}
		}

		#endregion

		#region Public Methods
		#region IByteSerializable & ISerializable Methods
		/// <summary>
		/// Used for byte serialization
		/// </summary>
		/// <returns></returns>
		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			byte version = VERSION_001;

			bw.Write(version);
			bw.Write(base.BrandID);
			bw.Write(_siteID);
			writeString(bw, _senderUsername);
			bw.Write(_senderMemberID);
			writeString(bw, _senderLocation);
			writeString(bw, _senderThumbnail);
			writeString(bw, _cardUrl);
			writeString(bw, _cardThumbnail);
			bw.Write(_recipientMemberID);
			bw.Write(_sentDate.Ticks);

			return ms.ToArray();
		}

		/// <summary>
		/// Used for byte serialization
		/// </summary>
		/// <param name="bytes"></param>
		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new BinaryReader(ms);

			byte version = br.ReadByte();

			switch (version)
			{
				case VERSION_001:

					base.BrandID = br.ReadInt32();
					this._siteID = br.ReadInt32();
					this._senderUsername = br.ReadString();
					this._senderMemberID = br.ReadInt32();
					this._senderLocation = br.ReadString();
					this._senderThumbnail = br.ReadString();
					this._cardUrl = br.ReadString();
					this._cardThumbnail = br.ReadString();
					this._recipientMemberID = br.ReadInt32();
					// ToBinary() and FromBinary() only available in .Net 2.0.
					this._sentDate = DateTime.MinValue.AddTicks(br.ReadInt64());

					break;

				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}

		/// <summary>
		/// Used for serialization
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", this.ToByteArray());
		}
		#endregion
		#endregion

	    
	}
}


