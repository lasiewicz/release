using System;
using System.Text;
using System.Web;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Send color code profile to another member
	/// </summary>
	[Serializable]
	public class SendMemberQuestionAndAnswerImpulse : ImpulseBaseEmail
	{
		private string _friendName = "";
		private string _friendEmail = "";
		private string _senderEmail = "";
		private string _senderName = "";
		private int _senderMemberID = 0;
		private string _subject = "";
		private string _message = "";
		private int _questionID = 0;
		private string _questionText="";

		private SendMemberQuestionAndAnswerImpulse()
		{

		}

		public SendMemberQuestionAndAnswerImpulse(
			string friendName,
			string friendEmail,
			string senderEmail,
			string senderName,
			int senderMemberID,
			string subject,
			string message,
			int brandID,
			int questionID,
			string questionText)
		{
			if(friendEmail == null || string.Empty.Equals(friendEmail))
			{
				throw new Exception("friendEmail field cannot be null or blank");
			}
			else if(friendName == null || string.Empty.Equals(friendName))
			{
				throw new Exception("friendName field cannot be null or blank");
			}
			else if(senderEmail == null || string.Empty.Equals(senderEmail))
			{
				throw new Exception("senderEmail field cannot be null or blank");
			}
			else if(senderName == null || string.Empty.Equals(senderName))
			{
				throw new Exception("senderName field cannot be null or blank");
			}

			base.BrandID = brandID;
			_friendName = friendName;
			_friendEmail = friendEmail;
			_senderEmail = senderEmail;
			_senderMemberID = senderMemberID;
			_senderName = senderName;
			_subject = subject;
			_message = message;
			_questionID=questionID;
			_questionText=questionText;
		}

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _senderMemberID;
			}
		}

		public string FriendName
		{
			get { return(_friendName); }
		}

		public string FriendEmail
		{
			get { return(_friendEmail); }
		}

		public string SenderEmail
		{
			get { return(_senderEmail); }
		}

		public int SenderMemberID
		{
			get { return(_senderMemberID); }
		}

		public string SenderName
		{
			get { return(_senderName); }
		}

		public string Subject
		{
			get { return(_subject); }
		}

		public string Message
		{
			get { return(_message); }
		}

		public int QuestionId
		{
			get { return(_questionID);}
		}

		public string QuestionText
		{
			get { return(_questionText);}
		}

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_SEND_MEMBER_QUESTION"; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendMemberQuestion; }
        }

        public override int TargetMemberId
        {
            get { return Constants.NULL_INT; }
        }

        public override string EmailAddress
        {
            get { return this.FriendEmail; }
        }

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append(base.GetQueryString());
			sb.Append("&FriendName=");
			sb.Append(HttpUtility.UrlEncode(FriendName));
			sb.Append("&FriendEmail=");
			sb.Append(HttpUtility.UrlEncode(FriendEmail));
			sb.Append("&UserEmail=");
			sb.Append(HttpUtility.UrlEncode(SenderEmail));
			sb.Append("&SenderMemberID=");
			sb.Append(SenderMemberID);
			sb.Append("&Subject=");
			sb.Append(HttpUtility.UrlEncode(Subject));
			sb.Append("&Message=");
			sb.Append(HttpUtility.UrlEncode(Message));
			sb.Append("&QuestionId=");
			sb.Append(QuestionId);
			sb.Append("&QuestionText=");
			sb.Append(HttpUtility.UrlEncode(QuestionText));

			return(sb.ToString());
		}

        
    }
}
