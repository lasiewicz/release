using System;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for PasswordMailImpulse.
	/// </summary>
	[Serializable]
	public class PasswordMailImpulse : ImpulseBase
	{
		private string _emailAddress;

		private PasswordMailImpulse()
		{

		}

		/// <summary>
		/// Sends the password for the account referenced by the email address to that email address.
		/// </summary>
		/// <param name="brandID">brand where this impulse originated</param>
		/// <param name="emailAddress">email address to send the password to.  cannot be null or blank.</param>
		public PasswordMailImpulse(int brandID, string emailAddress)
		{
			if(emailAddress == null || string.Empty.Equals(emailAddress))
			{
				throw new Exception("emailAddress field cannot be null or blank");
			}

			base.BrandID = brandID;
			this.EmailAddress = emailAddress;
		}

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return Constants.NULL_INT;
			}
		}


		public string EmailAddress
		{
			get
			{
				return(_emailAddress);
			}
			set
			{
				_emailAddress = value;
			}
		}

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append(base.GetQueryString());
			sb.Append("&EmailAddress=");
			sb.Append(EmailAddress);

			return(sb.ToString());
		}
	}
}
