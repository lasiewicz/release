using System;
using System.IO;
using System.Runtime.Serialization;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Used by Spark WS.
	/// Mingle makes a call to Spark WS to trigger a new email which in turn uses this class.
	/// This is identical to EcardToFriend, but kept the impulses separate in case there is a divergence later.
	/// </summary>
	[Serializable]
	public class EcardToFriendImpulse : ImpulseBase, IByteSerializable, ISerializable
	{
		#region Private Constants
		private const byte VERSION_001 = 1;
		#endregion

		#region Private Variables
		private int _siteID;
		private int _senderMemberID;
		private string _cardUrl;
		private string _cardThumbnail;
		private string _recipientEmailAddress;
		private DateTime _sentDate;
		#endregion

		#region Public Properties
		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _senderMemberID;
			}
		}


		public int SiteID
		{
			get { return _siteID;}	
			set { _siteID = value;}
		}


		/// <summary>
		/// 
		/// </summary>
		public int SenderMemberID
		{
			get { return _senderMemberID; }
			set { _senderMemberID = value; }
		}

		
		/// <summary>
		/// 
		/// </summary>
		public string CardUrl
		{
			get { return _cardUrl; }
			set { _cardUrl = value; }
		}


		/// <summary>
		/// 
		/// </summary>
		public string CardThumbnail
		{
			get { return _cardThumbnail; }
			set { _cardThumbnail = value; }
		}


		/// <summary>
		/// 
		/// </summary>
		public string RecipientEmailAddress
		{
			get { return _recipientEmailAddress; }
			set { _recipientEmailAddress = value; }
		}


		/// <summary>
		/// 
		/// </summary>
		public DateTime SentDate
		{
			get { return _sentDate; }
			set { _sentDate = value; }
		}
		#endregion

		#region Constructors
		/// <summary>
		/// Needed for deserialization
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected EcardToFriendImpulse(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <param name="memberID"></param>
		/// <param name="senderUsername"></param>
		/// <param name="senderAge"></param>
		/// <param name="senderLocation"></param>
		/// <param name="senderThumbnail"></param>
		/// <param name="cardUrl"></param>
		/// <param name="cardThumbnail"></param>
		/// <param name="recipientUsername"></param>
		public EcardToFriendImpulse(int brandID, int siteID, int senderMemberID, string cardUrl
			, string cardThumbnail, string recipientEmailAddress
			, DateTime sentDate)
		{
			base.BrandID = brandID;
			this._siteID = siteID;
			this._senderMemberID = senderMemberID;
			this._cardUrl = cardUrl;
			this._cardThumbnail = cardThumbnail;
			this._recipientEmailAddress = recipientEmailAddress;
			this._sentDate = sentDate;
		}

		#endregion

		#region Private Methods
		private static void writeString(BinaryWriter bw, string s)
		{
			if (s == null)
			{
				bw.Write(string.Empty);
			}
			else
			{
				bw.Write(s);
			}
		}

		#endregion

		#region Public Methods
		#region IByteSerializable & ISerializable Methods
		/// <summary>
		/// Used for byte serialization
		/// </summary>
		/// <returns></returns>
		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			byte version = VERSION_001;

			bw.Write(version);
			bw.Write(base.BrandID);
			bw.Write(_siteID);
			bw.Write(_senderMemberID);
			writeString(bw, _cardUrl);
			writeString(bw, _cardThumbnail);
			writeString(bw, _recipientEmailAddress);
			bw.Write(_sentDate.Ticks);

			return ms.ToArray();
		}

		/// <summary>
		/// Used for byte serialization
		/// </summary>
		/// <param name="bytes"></param>
		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new BinaryReader(ms);

			byte version = br.ReadByte();

			switch (version)
			{
				case VERSION_001:

					base.BrandID = br.ReadInt32();
					this._siteID = br.ReadInt32();
					this._senderMemberID = br.ReadInt32();
					this._cardUrl = br.ReadString();
					this._cardThumbnail = br.ReadString();
					this._recipientEmailAddress = br.ReadString();
					// ToBinary() and FromBinary() only available in .Net 2.0.
					this._sentDate = DateTime.MinValue.AddTicks(br.ReadInt64());

					break;

				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}

		/// <summary>
		/// Used for serialization
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", this.ToByteArray());
		}
		#endregion
		#endregion
	}
}


