using System;
using System.Web;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Generic email type for sending emails internally. For administration only.
	/// </summary>
	[Serializable]
	public class InternalCorpMail : ImpulseBase
	{
		#region Private Variables
		
		private string fromAddress = Constants.NULL_STRING;
		private string toAddress = Constants.NULL_STRING;
		private string body = Constants.NULL_STRING;
		private string toName = Constants.NULL_STRING;
		private string subject = Constants.NULL_STRING;
		
		#endregion

		#region Public Properties

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return Constants.NULL_INT;
			}
		}

		public string FromEmailAdress
		{
			get
			{
				return fromAddress;
			}
			set
			{
				fromAddress = value;
			}
		}
		public string ToAddress
		{
			get
			{
				return toAddress;
			}
			set
			{
				ToAddress = value;
			}
		}
		public string Body
		{
			get
			{
				return body;
			}
			set
			{
				body = value;
			}
		}
		public string ToName
		{
			get
			{
				return toName;
			}
			set
			{
				toName = value;
			}
		}

		public string Subject
		{
			get
			{
				return subject;
			}
			set
			{
				subject = value;
			}
		}


		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandID"></param>
		/// <param name="fromAddress"></param>
		/// <param name="toAddress"></param>
		/// <param name="body"></param>
		/// <param name="toName"></param>
		/// <param name="subject"></param>
		public InternalCorpMail(int brandID, string fromAddress, string toAddress, string body, string toName, string subject)
		{
			base.BrandID = brandID;
			this.fromAddress = fromAddress;
			this.toAddress = toAddress;
			this.body = body;
			this.toName = toName;
			this.subject = subject;
		}

		
		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append(base.GetQueryString());
			sb.Append("&FromAddress=");
			sb.Append(HttpUtility.UrlEncode(this.fromAddress));
			sb.Append("&ToAddress=");
			sb.Append(HttpUtility.UrlEncode(this.toAddress));
			sb.Append("&Body=");
			sb.Append(HttpUtility.UrlEncode(this.body));
			sb.Append("&ToName=");
			sb.Append(HttpUtility.UrlEncode(this.toName));
			sb.Append("&Subject=");
			sb.Append(HttpUtility.UrlEncode(this.subject));

			return(sb.ToString());
		}
	}
}
