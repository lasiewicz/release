using System;
using System.Web;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
    /// Summary description for MemberQuestionImpulse.
	/// </summary>
	[Serializable]
	public class MemberQuestionImpulse : ImpulseBase
	{
		private string _userName;
		private string _fromFirstName;
        private string _fromLastName;
		private int _memberID;
		private string _questionText;
        private bool _isAnonymous;

		/// <summary>
		/// Hide no arg constructor
		/// </summary>
		private MemberQuestionImpulse()
		{

		}

		/// <summary>
        /// Create a full MemberQuestionImpulse
		/// </summary>
		/// <param name="brandID">brand of the site that was the source of the contact</param>
		/// <param name="userName">username</param>
        /// <param name="fromFirstName">user-supplied first name</param>
        /// <param name="fromLastName">user-supplied last name</param>
        /// <param name="memberID">memberID</param>
		/// <param name="questionText">user free form question text</param>
        /// <param name="isAnonymous">flag for anonymity</param>
        public MemberQuestionImpulse(int brandID,
								string userName,
                                string fromFirstName,
                                string fromLastName,
                                int memberID,
								string questionText,
                                bool isAnonymous)
		{
			base.BrandID = brandID;
			_userName = userName;
            _fromFirstName = fromFirstName;
            _fromLastName = fromLastName;
			_memberID = memberID;
            _questionText = questionText;
            _isAnonymous = isAnonymous;
		}

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
                int memberID = _memberID;
                return memberID;
			}
		}


		public string UserName
		{
			get { return(_userName); }
		}

		public string FromFirstName
		{
			get { return(_fromFirstName); }
		}

        public string FromLastName
        {
            get { return (_fromLastName); }
        }
        
        public int MemberID
		{
			get { return(_memberID); }
		}

		public string QuestionText
		{
			get { return(_questionText); }
		}

        public bool IsAnonymous
        {
            get { return (_isAnonymous); }
        }

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append(base.GetQueryString());
			sb.Append("&UserName=");
			sb.Append(HttpUtility.UrlEncode(UserName));
            sb.Append("&FromFirstName=");
            sb.Append(HttpUtility.UrlEncode(FromFirstName));
            sb.Append("&FromLastName=");
            sb.Append(HttpUtility.UrlEncode(FromLastName));
            sb.Append("&MemberID=");
			sb.Append(HttpUtility.UrlEncode(MemberID.ToString()));
            sb.Append("&QuestionText=");
            sb.Append(HttpUtility.UrlEncode(QuestionText));
            sb.Append("&IsAnonymous=");
            sb.Append(HttpUtility.UrlEncode(IsAnonymous.ToString()));

			return(sb.ToString());
		}
	}
}
