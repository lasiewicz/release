using System;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Messmo's API 2.5 - A Channel Sends Content to a User. This isn't being used in the beginning. This  can be used in the future to
	/// send simple messages (i.e. system message).
	/// </summary>
	[Serializable]
	public class MessmoSendMessageImpulse : MessmoImpulseBase
	{	
		private string _messageBody = Constants.NULL_STRING;
		private string _receivingMessmoSubID = Constants.NULL_STRING;
		private string _taggle = Constants.NULL_STRING;

		#region Properties
		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return Constants.NULL_INT;
			}
		}


		public DateTime MessageDate
		{
			get { return this.TimeStamp; }
		}

		public string MessageBody
		{
			get { return _messageBody; }
		}

		public string ReceivingMessmoSubID
		{
			get { return _receivingMessmoSubID; }
		}

		public string Taggle
		{
			get { return _taggle; }
		}
		#endregion

		public MessmoSendMessageImpulse()
		{
			
		}

		public MessmoSendMessageImpulse(Int32 brandID, DateTime messageDate, string messageBody, string receivingMessmoSubID, string taggle)
		{
			this.BrandID = brandID;			
			this.TimeStamp = messageDate;
			_messageBody = messageBody;
			_receivingMessmoSubID = receivingMessmoSubID;
			_taggle = taggle;
		}
	}
}
