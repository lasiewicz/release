using System;
using System.Collections;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for ImpulseBaseQueueItems.
	/// </summary>
	public class ImpulseBaseQueueItems : CollectionBase, ICollection	
	{
		public ImpulseBaseQueueItems()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public void Add(ImpulseBase item) 
		{
			List.Add(item);
		}

		public void Remove(int index)
		{
			if (index < List.Count && index >= 0)
			{
				List.RemoveAt(index); 
			}
		}

		public ImpulseBase this[int index]
		{
			get 
			{ 
				return List[index] as ImpulseBase; 
			}
			set 
			{
				List[index] = value;
			}
		}
	}
}
