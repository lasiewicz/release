using System;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Messmo API 2.21 A Channel Sets a User's Transaction Status
	/// </summary>
	[Serializable]
	public class MessmoTranStatusImpulse : MessmoImpulseBase
	{
		#region Private Members
		private string _messmoSubscriberID;
		private string _status;
		private DateTime _setDate;
		#endregion

		#region Properties
		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return Constants.NULL_INT;
			}
		}

		public string MessmoSubscriberID
		{
			get { return _messmoSubscriberID; }
		}

		public string Status
		{
			get { return _status; }
		}

		public DateTime SetDate
		{
			get { return _setDate; }
		}
		#endregion

		public MessmoTranStatusImpulse()
		{
		
		}

		public MessmoTranStatusImpulse(Int32 brandID, DateTime timestamp, string messmoSubscriberID, string status, DateTime setDate)
		{
			this.BrandID = brandID;
			this.TimeStamp = timestamp;
			this._messmoSubscriberID = messmoSubscriberID;
			this._status  = status;
			this._setDate = setDate;
		}
	}
}
