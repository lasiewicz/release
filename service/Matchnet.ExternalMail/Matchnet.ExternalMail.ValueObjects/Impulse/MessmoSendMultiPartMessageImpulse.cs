using System;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for MessmoSendMultiPartMessageImpulse.
	/// </summary>
	[Serializable]
	public class MessmoSendMultiPartMessageImpulse : MessmoImpulseBase
	{
		#region Private Members
		private Int32 _sendingMemberID = Constants.NULL_INT;
		private Int32 _senderBrandID = Constants.NULL_INT;
		private string _receivingMessmoSubID = Constants.NULL_STRING;
		private string _messageBody = Constants.NULL_STRING;
		private string _smartMessageTag = Constants.NULL_STRING;
		private string _taggle = Constants.NULL_STRING;
		private bool _replace = false;
		private bool _notify = true;
		private Matchnet.HTTPMessaging.Constants.MessmoButtonType _messmoButtonType = Matchnet.HTTPMessaging.Constants.MessmoButtonType.Reply;
		#endregion

		#region Properties
		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _sendingMemberID;
			}
		}

		public Int32 SenderBrandID
		{
			get { return _senderBrandID; }
		}

		public Int32 SendingMemberID
		{
			get { return _sendingMemberID; }
		}
		public DateTime MessageDate
		{
			get { return this.TimeStamp; }
		}
		
		public string ReceivingMessmoSubID
		{
			get { return _receivingMessmoSubID; }
		}

		public string MessageBody
		{
			get { return _messageBody; }
		}

		public string SmartMessageTag
		{
			get { return _smartMessageTag; }
		}

		public string Taggle
		{
			get { return _taggle; }
		}

		public bool Replace
		{
			get { return _replace; }
		}

		public bool Notify
		{
			get { return _notify; }
		}

		public  Matchnet.HTTPMessaging.Constants.MessmoButtonType MessmoButtonType
		{
			get { return _messmoButtonType; }
		}
		#endregion

		public MessmoSendMultiPartMessageImpulse(Int32 recipientBrandID, Int32 senderBrandID, Int32 sendingMemberID, DateTime messageDate,
			string receivingMessmoSubID, string messageBody, string smartMessageTag, string taggle, bool replace,
			Matchnet.HTTPMessaging.Constants.MessmoButtonType messmoButtonType)
		{
			this.BrandID = recipientBrandID;
            this.TimeStamp = messageDate;
			
			_senderBrandID = senderBrandID;
			_sendingMemberID = sendingMemberID;
			_receivingMessmoSubID = receivingMessmoSubID;
			_messageBody = messageBody;			
			_smartMessageTag = smartMessageTag;
			_taggle = taggle;
			_replace = replace;
			_messmoButtonType = messmoButtonType;
		}

		/// <summary>
		/// This is used in ScheduledFeed svc.  Users probably don't want to be notified in the middle of the night.  Use this to disable
		/// the notify flag.
		/// </summary>
		/// <param name="recipientBrandID"></param>
		/// <param name="senderBrandID"></param>
		/// <param name="sendingMemberID"></param>
		/// <param name="messageDate"></param>
		/// <param name="receivingMessmoSubID"></param>
		/// <param name="messageBody"></param>
		/// <param name="smartMessageTag"></param>
		/// <param name="taggle"></param>
		/// <param name="replace"></param>
		/// <param name="notify"></param>
		public MessmoSendMultiPartMessageImpulse(Int32 recipientBrandID, Int32 senderBrandID, Int32 sendingMemberID, DateTime messageDate,
			string receivingMessmoSubID, string messageBody, string smartMessageTag, string taggle, bool replace, bool notify,
			Matchnet.HTTPMessaging.Constants.MessmoButtonType messmoButtonType)
		{
			this.BrandID = recipientBrandID;
			this.TimeStamp = messageDate;
			
			_senderBrandID = senderBrandID;
			_sendingMemberID = sendingMemberID;
			_receivingMessmoSubID = receivingMessmoSubID;
			_messageBody = messageBody;			
			_smartMessageTag = smartMessageTag;
			_taggle = taggle;
			_replace = replace;
			_notify = notify;
			_messmoButtonType = messmoButtonType;
		}

		public MessmoSendMultiPartMessageImpulse()
		{
			
		}
	}
}
