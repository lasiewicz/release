using System;
using System.IO;
using System.Runtime.Serialization;
using Matchnet.ActivityRecording.ValueObjects.Types;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Used by Spark WS.
	/// Mingle makes a call to Spark WS to trigger a new email which in turn uses this class
	/// </summary>
	[Serializable]
    public class MessageBoardDailyUpdatesImpulse : ImpulseBaseEmail, IByteSerializable, ISerializable
	{
		#region Private Constants
		private const byte VERSION_001 = 1;
		#endregion

		#region Private Variables
		private int _siteID;
		private int _memberID;
		private string _boardName;
		private string _topicSubject;
		private int _newReplies;
		private int _boardID;
		private int _threadID;
		private int _lastMessageID;
		private string _unsubscribeURL;
		#endregion

		#region Public Properties
		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _memberID;
			}
		}


		public int SiteID
		{
			get { return _siteID;}	
			set { _siteID = value;}
		}
		
		/// <summary>
		/// 
		/// </summary>
		public int MemberID
		{
			get { return _memberID; }
			set { _memberID = value; }
		}


		/// <summary>
		/// 
		/// </summary>
		public string BoardName
		{
			get { return _boardName; }
			set { _boardName = value; }
		}


		/// <summary>
		/// 
		/// </summary>
		public string TopicSubject
		{
			get { return _topicSubject; }
			set { _topicSubject = value; }
		}


		/// <summary>
		/// 
		/// </summary>
		public int BoardID
		{
			get { return _boardID; }
			set { _boardID = value; }
		}

		
		/// <summary>
		/// 
		/// </summary>
		public int ThreadID
		{
			get { return _threadID; }
			set { _threadID = value; }
		}


		/// <summary>
		/// 
		/// </summary>
		public int LastMessageID
		{
			get { return _lastMessageID; }
			set { _lastMessageID = value; }
		}


		/// <summary>
		/// 
		/// </summary>
		public int NewReplies
		{
			get { return _newReplies; }
			set { _newReplies = value; }
		}


		/// <summary>
		/// 
		/// </summary>
		public string UnsubscribeURL
		{
			get { return _unsubscribeURL; }
			set { _unsubscribeURL = value; }
		}

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_MESSAGEBOARD_DAILY_UPDATES"; }
        }

        public override ActionType ActionType
        {
            get { return ActionType.SendMessageBoardDailyUpdates; }
        }

        public override int TargetMemberId
        {
            get { return _memberID; }
        }

        public override string EmailAddress
        {
            get { return Constants.NULL_STRING; }
        }
		#endregion

		#region Constructors
		/// <summary>
		/// Needed for deserialization
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessageBoardDailyUpdatesImpulse(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <param name="memberID"></param>
		/// <param name="boardName"></param>
		/// <param name="topicSubject"></param>
		/// <param name="newReplies"></param>
		/// <param name="boardID"></param>
		/// <param name="threadID"></param>
		/// <param name="lastMessageID"></param>
		/// <param name="unsubscribeURL"></param>
		public MessageBoardDailyUpdatesImpulse(int brandID, int siteID, int memberID, string boardName, string topicSubject, int newReplies, int boardID, int threadID, int lastMessageID, string unsubscribeURL)
		{
			base.BrandID = brandID;
			this._siteID = siteID;
			this._memberID = memberID;
			this._boardName = boardName;
			this._topicSubject = topicSubject;
			this._newReplies = newReplies;
			this._boardID = boardID;
			this._threadID = threadID;
			this._lastMessageID = lastMessageID;
			this._unsubscribeURL = unsubscribeURL;
		}

		#endregion

		#region Private Methods
		private static void writeString(BinaryWriter bw, string s)
		{
			if (s == null)
			{
				bw.Write(string.Empty);
			}
			else
			{
				bw.Write(s);
			}
		}

		#endregion

		#region Public Methods
		#region IByteSerializable & ISerializable Methods
		/// <summary>
		/// Used for byte serialization
		/// </summary>
		/// <returns></returns>
		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			byte version = VERSION_001;

			bw.Write(version);
			bw.Write(base.BrandID);
			bw.Write(_siteID);
			bw.Write(_memberID);
			writeString(bw, _boardName);
			writeString(bw, _topicSubject);
			bw.Write(_newReplies);
			bw.Write(_boardID);
			bw.Write(_threadID);
			bw.Write(_lastMessageID);
			writeString(bw, _unsubscribeURL);
			
			return ms.ToArray();
		}

		/// <summary>
		/// Used for byte serialization
		/// </summary>
		/// <param name="bytes"></param>
		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new BinaryReader(ms);

			byte version = br.ReadByte();

			switch (version)
			{
				case VERSION_001:

					base.BrandID = br.ReadInt32();
					this._siteID = br.ReadInt32();
					this._memberID = br.ReadInt32();
					this._boardName = br.ReadString();
					this._topicSubject = br.ReadString();
					this._newReplies = br.ReadInt32();
					this._boardID = br.ReadInt32();
					this._threadID = br.ReadInt32();
					this._lastMessageID = br.ReadInt32();
					this._unsubscribeURL = br.ReadString();

					break;

				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}

		/// <summary>
		/// Used for serialization
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", this.ToByteArray());
		}
		#endregion
		#endregion

	   
	}
}


