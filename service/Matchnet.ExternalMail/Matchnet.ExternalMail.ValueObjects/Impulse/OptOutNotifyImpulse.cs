using System;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for OptOutNotifyImpulse.
	/// </summary>
	[Serializable]
	public class OptOutNotifyImpulse : ImpulseBaseEmail
	{
		private string _email;
		private string _fromHost;

		private OptOutNotifyImpulse()
		{
		}

		public OptOutNotifyImpulse(int brandID, string email, string fromHost)
		{
			base.BrandID = brandID;
			_email = email;
			_fromHost = fromHost;
		}

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return Constants.NULL_INT;
			}
		}


		public string Email
		{
			get { return(_email); }
		}

		public string FromHost
		{
			get { return(_fromHost); }
		}

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_OPT_OUT_NOTIFICATION"; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendOptOutNotification; }
        }

        public override int TargetMemberId
        {
            get { return Constants.NULL_INT; }
        }

        public override string EmailAddress
        {
            get { return this.Email; }
        }
		
		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append(base.GetQueryString());
			sb.Append("&Email=");
			sb.Append(System.Web.HttpUtility.UrlEncode(Email));
			sb.Append("&FromHost=");
			sb.Append(System.Web.HttpUtility.UrlEncode(FromHost));

			return(sb.ToString());
		}


       
    }
}
