﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
    /// <summary>
    /// Class to represent email for Send to friend referral
    /// </summary>
    [Serializable]
    public class SendFriendReferralImpulse : ImpulseBaseEmail
    {
        private string _friendEmail = "";
        private string _senderName = "";
        private int _senderMemberID = 0;
        private string _subject = "";
        private string _message = "";
        private string _prm = "";
        private string _lgid = "";

        private SendFriendReferralImpulse()
        {

        }

        public SendFriendReferralImpulse(
            string friendEmail,
            string senderName,
            int senderMemberID,
            string subject,
            string message,
            int brandID,
            string prm,
            string lgid)
        {
            if (friendEmail == null || string.Empty.Equals(friendEmail))
            {
                throw new Exception("friendEmail field cannot be null or blank");
            }
            else if (senderName == null || string.Empty.Equals(senderName))
            {
                throw new Exception("senderName field cannot be null or blank");
            }

            base.BrandID = brandID;
            _friendEmail = friendEmail;
            _senderMemberID = senderMemberID;
            _senderName = senderName;
            _subject = subject;
            _message = message;
            _prm = prm;
            _lgid = lgid;
        }

        public override Int32 MemberIDForRandomSelection
        {
            get
            {
                return _senderMemberID;
            }
        }

        public string FriendEmail
        {
            get { return (_friendEmail); }
        }

        public int SenderMemberID
        {
            get { return (_senderMemberID); }
        }

        public string SenderName
        {
            get { return (_senderName); }
        }

        public string Subject
        {
            get { return (_subject); }
        }

        public string Message
        {
            get { return (_message); }
        }

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_SEND_FRIEND_REFERAL"; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendFriendReferralEmail; }
        }

        public override int TargetMemberId
        {
            get { return Constants.NULL_INT; }
        }

        public override string EmailAddress
        {
            get { return this.FriendEmail; }
        }

        public string PRM
        {
            get { return this._prm; }
        }

        public string LGID
        {
            get { return this._lgid; }
        }

        public override string GetQueryString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(base.GetQueryString());
            sb.Append("&FriendEmail=");
            sb.Append(HttpUtility.UrlEncode(FriendEmail));
            sb.Append("&SenderMemberID=");
            sb.Append(SenderMemberID);
            sb.Append("&Subject=");
            sb.Append(HttpUtility.UrlEncode(Subject));
            sb.Append("&Message=");
            sb.Append(HttpUtility.UrlEncode(Message));
            sb.Append("&prm=");
            sb.Append(HttpUtility.UrlEncode(_prm));
            sb.Append("&lgid=");
            sb.Append(HttpUtility.UrlEncode(_lgid));

            return (sb.ToString());
        }
    }
}
