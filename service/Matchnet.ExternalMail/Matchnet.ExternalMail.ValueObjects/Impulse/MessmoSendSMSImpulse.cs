using System;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Messmo API 2.18 A Channel Sends a SMS to a User
	/// </summary>
	[Serializable]
	public class MessmoSendSMSImpulse : MessmoImpulseBase
	{
		private string _mobileNumber;
		private string _message;

		#region Properties
		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return Constants.NULL_INT;
			}
		}

		public string MobileNumber
		{
			get { return _mobileNumber; }
		}

		public string Message
		{
			get { return _message; }
		}
		#endregion

		public MessmoSendSMSImpulse()
		{
			
		}

		public MessmoSendSMSImpulse(Int32 brandID, DateTime timestamp, string mobileNumber, string message)
		{
			this.BrandID = brandID;
			this.TimeStamp = timestamp;
            _mobileNumber = mobileNumber;
			_message = message;
		}
	}
}
