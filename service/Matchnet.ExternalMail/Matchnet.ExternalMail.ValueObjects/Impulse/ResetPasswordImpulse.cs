﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.ActivityRecording.ValueObjects.Types;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
    /// <summary>
    /// When member requests a password reset, this impulse is used to send them a link to change their password.
    /// </summary>
    [Serializable]
    public class ResetPasswordImpulse : ImpulseBaseEmail
    {
        private int _memberId;
        private string _resetPasswordUrl;

        public ResetPasswordImpulse(int brandId, int memberId, string resetPasswordUrl)
        {
            base.BrandID = brandId;
            _memberId = memberId;
            _resetPasswordUrl = resetPasswordUrl;
        }

        public override Int32 MemberIDForRandomSelection
        {
            get
            {
                return _memberId;
            }
        }

        public int MemberId
        {
            get { return _memberId; }
        }

        public string ResetPasswordUrl
        {
            get { return _resetPasswordUrl; }
        }

        public override string GetQueryString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(base.GetQueryString());
            sb.Append("&MemberID=");
            sb.Append(_memberId);
            sb.Append("&ResetPasswordUrl=");
            sb.Append(_resetPasswordUrl);

            return (sb.ToString());
        }


        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_FORGOT_PASSWORD"; }
        }

        public override ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return ActionType.SendForgotPassword; }
        }

        public override int TargetMemberId
        {
            get { return _memberId; }
        }

        public override string EmailAddress
        {
            get { return string.Empty; }
        }
    }
}
