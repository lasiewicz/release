using System;
using System.Text;
using System.Web;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Email for member to send an invite for another member to take color code quiz
	/// </summary>
	[Serializable]
	public class SendMemberColorCodeQuizInviteImpulse : ImpulseBase
	{
		private string _recipientEmail = "";
		private string _recipientUserName = "";
		private string _senderEmail = "";
		private int _senderMemberID = 0;
		private string _senderUserName = "";

		private SendMemberColorCodeQuizInviteImpulse()
		{

		}

		public SendMemberColorCodeQuizInviteImpulse(
			string recipientEmail,
			string recipientUserName,
			string senderEmail,
			int senderMemberID,
			string senderUserName,
			int brandID)
		{
			if(recipientEmail == null || string.Empty.Equals(recipientEmail))
			{
				throw new Exception("recipientEmail field cannot be null or blank");
			}
			else if(senderEmail == null || string.Empty.Equals(senderEmail))
			{
				throw new Exception("senderEmail field cannot be null or blank");
			}

			base.BrandID = brandID;
			_recipientEmail = recipientEmail;
			_recipientUserName = recipientUserName;
			_senderEmail = senderEmail;
			_senderMemberID = senderMemberID;
			_senderUserName = senderUserName;
		}

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _senderMemberID;
			}
		}

		public string RecipientEmail
		{
			get { return(_recipientEmail); }
		}

		public string RecipientUserName
		{
			get { return(_recipientUserName); }
		}

		public string SenderEmail
		{
			get { return(_senderEmail); }
		}

		public int SenderMemberID
		{
			get { return(_senderMemberID); }
		}

		public string SenderUserName
		{
			get { return(_senderUserName); }
		}

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append(base.GetQueryString());
			sb.Append("&RecipientEmail=");
			sb.Append(HttpUtility.UrlEncode(RecipientEmail));
			sb.Append("&SenderEmail=");
			sb.Append(HttpUtility.UrlEncode(SenderEmail));
			sb.Append("&SenderMemberID=");
			sb.Append(SenderMemberID);

			return(sb.ToString());
		}
	}
}
