using System;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for PassCallUserDataSMSAlertImpulse.
	/// </summary>
	public class PassCallUserDataSMSAlertImpulse : SMSAlertImpulseBase
	{
		private	string _Email;

		public string Email
		{
			get
			{
				return _Email;
			}
			set
			{
				_Email = value;
			}
		}

		public PassCallUserDataSMSAlertImpulse(int BrandID, int SiteID, int ReceivingMemberID, string TargetPhoneNumber,
			string Email)
		{
			this.BrandID = BrandID;
			this.SiteID = SiteID;
			this.ReceivingMemberID = ReceivingMemberID;
			this.TargetPhoneNumber = TargetPhoneNumber;
			this.Email = Email;
		}

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();

			// the following line should be instead of
			/*
			sb.Append("BrandID=");
			sb.Append(this.BrandID);
			sb.Append("&SiteID=");
			sb.Append(_siteID);
			sb.Append("&SendingMemberID=");
			sb.Append(_sendingMemberID);
			sb.Append("&ReceivingMemberID=");
			sb.Append(_receivingMemberID);
			sb.Append("&TargetPhoneNumber=");
			sb.Append(_targetPhoneNumber);
			*/

			sb.Append(base.GetQueryString());
			sb.Append("&Email=");
			sb.Append(this.Email);

			return sb.ToString();
		}
	}
}
