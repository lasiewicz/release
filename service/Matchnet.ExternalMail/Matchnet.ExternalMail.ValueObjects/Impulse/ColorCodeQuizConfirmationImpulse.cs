using System;
using System.Text;
using System.Web;
using Matchnet.ActivityRecording.ValueObjects.Types;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Code Code Quiz Confirmation when member has finished the quiz
	/// </summary>
	[Serializable]
    public class ColorCodeQuizConfirmationImpulse : ImpulseBaseEmail
	{
		private int _memberID = Constants.NULL_INT;
		private string _memberUserName = "";
		private string _emailAddress = Constants.NULL_STRING;
		private string _color = "";

		#region Public Properties
		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _memberID;
			}
		}


		public int MemberID
		{
			get
			{
				return _memberID;
			}
		}

	    public override string SettingConstant
	    {
            get { return "YESMAIL_PERCENT_COLOR_CODE_QUIZ_CONFIRM"; }
	    }

	    public override ActionType ActionType
	    {
            get { return ActionType.SendColorCodeQuizConfirmation; }
	    }

	    public override int TargetMemberId
	    {
	        get { return _memberID; }
	    }

	    public override string EmailAddress
		{
			get
			{
				return _emailAddress;
			}
		}

		public string MemberUserName
		{
			get
			{
				return _memberUserName;
			}
		}

		public string Color
		{
			get
			{
				return _color;
			}
		}
		#endregion

		public ColorCodeQuizConfirmationImpulse()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public ColorCodeQuizConfirmationImpulse(int memberID,
			string emailAddress,
			int brandID,
			string color,
			string memberUserName)
		{
			this._memberID = memberID;
			this._emailAddress = emailAddress;
			this._color = color;
			base.BrandID = brandID;
			this._memberUserName = memberUserName;
		}

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append(base.GetQueryString());
			sb.Append("&MemberID=");
			sb.Append(this.MemberID.ToString());

			return(sb.ToString());
		}

	}
}
