using System;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for RegVerificationImpulse.
	/// </summary>
	[Serializable]
	public class RegVerificationImpulse : ImpulseBaseEmail
	{
		private int _memberID;

		private RegVerificationImpulse()
		{
		}

		public RegVerificationImpulse(int memberID, int brandID)
		{
			base.BrandID = brandID;
			_memberID = memberID;
		}

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _memberID;
			}
		}


		public int MemberID
		{
			get { return(_memberID); }
		}

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_REG_VERIFICATION_MAIL"; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendActivationLetter; }
        }

        public override int TargetMemberId
        {
            get { return this.MemberID; }
        }

        public override string EmailAddress
        {
            get { return Constants.NULL_STRING; }
        }

        public override string GetQueryString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(base.GetQueryString());
            sb.Append("&MemberID=");
            sb.Append(MemberID);

            return (sb.ToString());
        }
    }
}
