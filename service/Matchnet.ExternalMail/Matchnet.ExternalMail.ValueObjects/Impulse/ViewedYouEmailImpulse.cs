﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
    [Serializable]
    public class ViewedYouEmailImpulse : ImpulseBaseEmail
    {
		private int _memberID;
		private int _viewedByMemberID;
        private int _numberOfMembersWhoViewedSinceLastEmail;

		private ViewedYouEmailImpulse()
		{
		}

        public ViewedYouEmailImpulse(int memberId, int viewedByMemberId, int numberOfMembersWhoViewedSinceLastEmail, int brandId)
        {
            _memberID = memberId;
            _viewedByMemberID = viewedByMemberId;
            _numberOfMembersWhoViewedSinceLastEmail = numberOfMembersWhoViewedSinceLastEmail;
            this.BrandID = brandId;
        }

        public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _memberID;
			}
		}

		public int MemberID
		{
			get { return(_memberID); }
		}

		public int ViewedByMemberID
		{
			get { return(_viewedByMemberID); }
		}

        public int NumberOfMembersWhoViewedSinceLastEmail
        {
            get { return _numberOfMembersWhoViewedSinceLastEmail; }
        }

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_VIEWED_YOU_EMAIL"; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendViewedYouEmail; }
        }

        public override int TargetMemberId
        {
            get { return this.MemberID; }
        }

        public override string EmailAddress
        {
            get { return Constants.NULL_STRING; }
        }

    }
}
