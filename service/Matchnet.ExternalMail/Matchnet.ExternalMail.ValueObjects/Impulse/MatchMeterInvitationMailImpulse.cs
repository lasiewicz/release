using System;
using System.Web;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for MatchMeterInvitationMailImpulse.
	/// </summary>
	[Serializable]
	public class MatchMeterInvitationMailImpulse : ImpulseBaseEmail
	{
		int _SiteID;
		int _MemberID;
		string _SenderUsername;
		string _EmailAddr;
		string _RecipientEmailAddress;

		public MatchMeterInvitationMailImpulse(int brandID, int siteID,int memberID, string senderUsername, string emailAddr, string recipientEmailAddress)
		{
			base.BrandID = brandID;
			_SiteID = siteID;
			_MemberID = memberID;
			_SenderUsername = senderUsername;
			_EmailAddr = emailAddr;
			_RecipientEmailAddress = recipientEmailAddress;
		}

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _MemberID;
			}
		}


		public int SiteID
		{
			get { return _SiteID;}
		}
		public int MemberID
		{
			get { return _MemberID; }
		}
		public string SenderUsername
		{
			get { return _SenderUsername; }
		}
		public string EmailAddr
		{
			get { return _EmailAddr; }
		}
		public string RecipientEmailAddress
		{
			get { return _RecipientEmailAddress; }
		}

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_MATCH_METER_INVITATION"; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendMatchMeterInvitation; }
        }

        public override int TargetMemberId
        {
            get { return Constants.NULL_INT; }
        }

        public override string EmailAddress
        {
            get { return this.RecipientEmailAddress; }
        }

        public override string GetQueryString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(base.GetQueryString());
            sb.Append("&SiteID=");
            sb.Append(HttpUtility.UrlEncode(SiteID.ToString()));
            sb.Append("&MemberID=");
            sb.Append(HttpUtility.UrlEncode(MemberID.ToString()));
            sb.Append("&SenderUsername=");
            sb.Append(HttpUtility.UrlEncode(SenderUsername));
            sb.Append("&EmailAddr=");
            sb.Append(HttpUtility.UrlEncode(EmailAddr));
            sb.Append("&RecipientEmailAddress=");
            sb.Append(HttpUtility.UrlEncode(RecipientEmailAddress));
            sb.Append("&S2FMemberID=");
            sb.Append(HttpUtility.UrlEncode(MemberID.ToString()));

            return (sb.ToString());
        }
    }
}
