using System;
using System.Text;
using System.IO;
using System.Runtime.Serialization;

using Matchnet;
using Matchnet.ActivityRecording.ValueObjects.Types;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	[Serializable]
    public class PmtProfileConfirmationImpulse : ImpulseBaseEmail, ISerializable, IByteSerializable
	{
		private const byte VERSION_001 = 1;

		#region Private Variables
		private int _memberID = Constants.NULL_INT;
		private int _siteID = Constants.NULL_INT;
		private string _emailAddress = Constants.NULL_STRING;
		private string _firstName = Constants.NULL_STRING;
		private string _lastName = Constants.NULL_STRING;
		private DateTime _dateOfPurchase =  new DateTime(DateTime.MinValue.Ticks);
		private string _last4CC = Constants.NULL_STRING;
		private string _confirmationNumber = Constants.NULL_STRING;
		#endregion

		#region Public Properties
		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _memberID;
			}
		}

		public int MemberID
		{
			get
			{
				return _memberID;
			}
		}
		public int SiteID
		{
			get
			{
				return _siteID;
			}
		}

	    public override string SettingConstant
	    {
            get { return "YESMAIL_PERCENT_PAYMENT_PROFILE_NOTIF"; }
	    }

	    public override ActionType ActionType
	    {
            get { return ActionType.SendPaymentProfileNotification; }
	    }

	    public override int TargetMemberId
	    {
	        get { return _memberID; }
	    }

	    public override string EmailAddress
		{
			get
			{
				return _emailAddress;
			}
		}

		public string FirstName
		{
			get
			{
				return _firstName;
			}
		}

		public string LastName
		{
			get
			{
				return _lastName;
			}
		}

		public DateTime DateOfPurchase
		{
			get
			{
				return _dateOfPurchase;
			}
		}
		
		public string Last4CC
		{
			get
			{
				return _last4CC;
			}
			set
			{
				_last4CC = value;
			}
		}
		
		public string ConfirmationNumber
		{
			get
			{
				return _confirmationNumber;
			}
			set
			{
				_confirmationNumber = value;
			}
		}
		#endregion

		#region Constructors
		/// <summary>
		/// Needed for deserialization
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PmtProfileConfirmationImpulse(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		public PmtProfileConfirmationImpulse(int memberID,
			int siteID,
			int brandID,
			string emailAddress,
			string firstName,
			string lastName,
			DateTime dateOfPurchase,
			string last4CC,
			string confirmationNumber)
		{
			this._memberID = memberID;
			this._siteID = siteID;
			this.BrandID = brandID;
			this._emailAddress = emailAddress;
			this._firstName = firstName;
			this._lastName = lastName;
			this._dateOfPurchase = dateOfPurchase;
			this._last4CC = last4CC;
			this._confirmationNumber = confirmationNumber;
		}

		#endregion

		// Property not used in greymail. Need to get rid of it for good
		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append(base.GetQueryString());
			sb.Append("&MemberID=");
			sb.Append(this.MemberID.ToString());

			return(sb.ToString());
		}

		#region ISerializable Members
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", this.ToByteArray());
		}
		#endregion

		#region IByteSerializable Members

		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new BinaryReader(ms);

			byte version = br.ReadByte();

			switch (version)
			{
				case VERSION_001:
					_memberID = br.ReadInt32();
					_siteID = br.ReadInt32();
					BrandID = br.ReadInt32();
					_emailAddress = br.ReadString();
					_firstName = br.ReadString();
					_lastName = br.ReadString();
					_dateOfPurchase = new DateTime(br.ReadInt64());
					_last4CC = br.ReadString();
					_confirmationNumber = br.ReadString();
					break;

				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}

		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			byte version = VERSION_001;

			bw.Write(version);
			bw.Write(_memberID);
			bw.Write(_siteID);
			bw.Write(BrandID);
			writeString(bw, _emailAddress);
			writeString(bw, _firstName);
			writeString(bw, _lastName);
			bw.Write(_dateOfPurchase.Ticks);
			writeString(bw, _last4CC);
			writeString(bw, _confirmationNumber);
		
			return ms.ToArray();
		}

		private static void writeString(BinaryWriter bw, string s)
		{
			if (s == null)
			{
				bw.Write(string.Empty);
			}
			else
			{
				bw.Write(s);
			}
		}

		#endregion
	}
}
