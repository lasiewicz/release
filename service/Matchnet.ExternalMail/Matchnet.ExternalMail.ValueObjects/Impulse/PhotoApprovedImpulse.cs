using System;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for PhotoApprovedImpulse.
	/// </summary>
	[Serializable]
	public class PhotoApprovedImpulse : ImpulseBaseEmail
	{
		private int _memberID;
		private int _photoID;
		private string _approveStatus;

		private PhotoApprovedImpulse()
		{
		}

		public PhotoApprovedImpulse(int memberID,
			int photoID,
			int brandID,
			string approveStatus)
		{
			base.BrandID = brandID;
			_memberID = memberID;
			_photoID = photoID;
			_approveStatus = approveStatus;
		}

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _memberID;
			}
		}

		public int MemberID
		{
			get
			{
				return _memberID;
			}
		}

		public int PhotoID
		{
			get
			{
				return _photoID;
			}
		}

		public string ApproveStatus
		{
			get
			{
				return _approveStatus;
			}
		}

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_PHOTO_APPROVAL"; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return ActivityRecording.ValueObjects.Types.ActionType.SendPhotoApproval; }
        }

        public override int TargetMemberId
        {
            get { return _memberID; }
        }

        public override string EmailAddress
        {
            get { return Constants.NULL_STRING; }
        }

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append(base.GetQueryString());
			sb.Append("&MemberID=");
			sb.Append(MemberID);
			sb.Append("&PhotoID=");
			sb.Append(PhotoID);
			sb.Append("&ApproveStatus=");
			sb.Append(ApproveStatus);

			return(sb.ToString());
		}

        
    }
}
