﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
    /// <summary>
    /// Black Board Eats passcode confirmation email
    /// </summary>
    [Serializable]
    public class BBEPasscodeImpulse : ImpulseBaseEmail
    {
        private int _MemberID;
        private string _Passcode;
        private string _EmailAddress;

        public BBEPasscodeImpulse(int memberID, int brandID, string emailAddress, string passcode)
        {
            _MemberID = memberID;
            _EmailAddress = emailAddress;
            _Passcode = passcode;
            base.BrandID = brandID;
        }

        public int MemberID { get { return _MemberID; } }
        public string Passcode { get { return _Passcode; } }

        public override Int32 MemberIDForRandomSelection
        {
            get
            {
                return MemberID;
            }
        }

        public override string SettingConstant
        {
            get { return YESMAIL_100_PERCENT; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return Matchnet.ActivityRecording.ValueObjects.Types.ActionType.BBEPasscodeEmail; }
        }

        public override int TargetMemberId
        {
            get { return this.MemberID; }
        }

        public override string EmailAddress
        {
            get { return _EmailAddress; }
        }

        public override string GetQueryString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(base.GetQueryString());
            sb.Append("&MemberID=");
            sb.Append(MemberID);
            sb.Append("&Passcode=" + _Passcode);

            return (sb.ToString());
        }
    }
}
