using System;
using System.Collections;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using Matchnet.ActivityRecording.ValueObjects.Types;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	[Serializable]
	public class NewMemberMailImpulse: MassMailImpulse, IByteSerializable, ISerializable
	{
		private const byte VERSION_001 = 1;
		
		protected NewMemberMailImpulse(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		public NewMemberMailImpulse(Int32 brandID, String recipientEmailAddress, 
			MiniprofileInfo recipientMiniprofileInfo,
			MiniprofileInfoCollection targetMiniprofileInfoCollection)
		{
			base.BrandID = brandID;
			base.RecipientEmailAddress = recipientEmailAddress;
			base.RecipientMiniprofileInfo = recipientMiniprofileInfo;
			base.TargetMiniprofileInfoCollection = targetMiniprofileInfoCollection;
		}

        public override ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return ActionType.SendNewMemberMail; }
        }

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_NEW_MEMBER_MAIL"; }
        }

		#region Public Methods
		#region IByteSerializable Members

		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new BinaryReader(ms);

			byte version = br.ReadByte();

			switch (version)
			{
				case VERSION_001:

					this.BrandID = br.ReadInt32();
					this.RecipientEmailAddress = br.ReadString();
					MiniprofileInfo mini = new MiniprofileInfo();

					int miniLength = br.ReadInt32();
					mini.FromByteArray(br.ReadBytes(miniLength));
					this.RecipientMiniprofileInfo = mini;

					MiniprofileInfoCollection minis = new MiniprofileInfoCollection();
					int minisLength = br.ReadInt32();
					minis.FromByteArray(br.ReadBytes(minisLength));
					this.TargetMiniprofileInfoCollection = minis;

					break;

				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}

		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			byte version = VERSION_001;

			bw.Write(version);
			bw.Write(base.BrandID);
			writeString(bw, base.RecipientEmailAddress);
			
			byte[] recipientMiniprofileInfoBytes = base.RecipientMiniprofileInfo.ToByteArray();
			bw.Write(recipientMiniprofileInfoBytes.Length);
			bw.Write(recipientMiniprofileInfoBytes);

			byte[] targetMiniprofileInfoCollection = base.TargetMiniprofileInfoCollection.ToByteArray(); 
			bw.Write(targetMiniprofileInfoCollection.Length);
			bw.Write(targetMiniprofileInfoCollection);
			
			return ms.ToArray();
		}

		#endregion

		private static void writeString(BinaryWriter bw, string s)
		{
			if (s == null)
			{
				bw.Write(string.Empty);
			}
			else
			{
				bw.Write(s);
			}
		}

		#region ISerializable Members

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", this.ToByteArray());
		}

		#endregion
		#endregion
	}
}
