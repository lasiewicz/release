using System;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for CollegeLuvPromoImpulse.
	/// </summary>
	[Serializable]
	public class CollegeLuvPromoImpulse : ImpulseBase
	{
		 private int _memberID;

		private CollegeLuvPromoImpulse()
		{

		}

		 public CollegeLuvPromoImpulse(int memberID, int brandID)
		 {
			base.BrandID = brandID;
			 _memberID = memberID;
		 }

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _memberID;
			}
		}


		 public int MemberID
		 {
			 get { return(_memberID); }
		 }
		 
		 public override string GetQueryString()
		 {
			 StringBuilder sb = new StringBuilder();
			
			 sb.Append(base.GetQueryString());
			 sb.Append("&MemberID=");
			 sb.Append(_memberID);

			 return(sb.ToString());
		 }
	}
}
