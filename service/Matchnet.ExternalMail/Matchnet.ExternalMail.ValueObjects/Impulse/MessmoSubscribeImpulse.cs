using System;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for MessmoSubscribeImpulse.
	/// </summary>
	[Serializable]
	public class MessmoSubscribeImpulse : MessmoImpulseBase
	{
		private Int32 _memberID = Constants.NULL_INT;
		private string _mobileNumber = Constants.NULL_STRING;
		private string _encryptedPassword = Constants.NULL_STRING;
		
		#region Properties
		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _memberID;
			}
		}

		public Int32 MemberID
		{
			get { return _memberID; }
		}

		public string MobileNumber
		{
			get { return _mobileNumber; }
		}

		public string EncryptedPassword
		{
			get { return _encryptedPassword; }
		}
		#endregion

		public MessmoSubscribeImpulse()
		{
			
		}

		public MessmoSubscribeImpulse(Int32 brandID, Int32 memberID, DateTime timestamp, string mobileNumber, string encryptedPassword)
		{
			this.BrandID = brandID;
			this.TimeStamp = timestamp;
			_memberID = memberID;
			_mobileNumber = mobileNumber;
			_encryptedPassword = encryptedPassword;			
		}
	}
}
