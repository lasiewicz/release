﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
    [Serializable]
    public class SodbPhotoUploadImpulse : ImpulseBaseEmail
    {
        private int memberId;
        private string emailAddress;
        private bool isBHMember;

        public SodbPhotoUploadImpulse(int memberId, int brandId, string emailAddress, bool isBHMember)
		{
			base.BrandID = brandId;
			this.memberId = memberId;
            this.emailAddress = emailAddress;
            this.isBHMember = isBHMember;
		}

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_SODB_PHOTO_UPLOAD"; }
        }

        public override ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return ActivityRecording.ValueObjects.Types.ActionType.SendSodbPhotoUpload; }
        }

        public override int TargetMemberId
        {
            get { return Constants.NULL_INT; }
        }

        public override string EmailAddress
        {
            get { return emailAddress; }
        }

        public override int MemberIDForRandomSelection
        {
            get { return memberId; }
        }

        public int MemberId
        {
            get { return memberId; }
        }

        public bool IsBHMember
        {
            get { return isBHMember; }
        }
    }
}
