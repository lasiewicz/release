using System;
using System.Text;

using Matchnet.Email.ValueObjects;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for SMSAlertImpulseBase.
	/// </summary>
	/// 
	[Serializable]
	public abstract class SMSAlertImpulseBase : ImpulseBase
	{
		private int _siteID;
		private int _receivingMemberID;
		private int _sendingMemberID;
		private string _targetPhoneNumber;

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _sendingMemberID;
			}
		}

		public int SiteID
		{
			get
			{
				return _siteID;
			}
			set
			{
				_siteID = value;
			}
		}

		public int ReceivingMemberID
		{
			get
			{
				return _receivingMemberID;
			}
			set
			{
				_receivingMemberID = value;
			}
		}

		public int SendingMemberID
		{
			get
			{
				return _sendingMemberID;
			}
			set
			{
				_sendingMemberID = value;
			}
		}

		public string TargetPhoneNumber
		{
			get
			{
				return _targetPhoneNumber;
			}
			set
			{
				_targetPhoneNumber = value;
			}
		}

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("BrandID=");
			sb.Append(this.BrandID);
			sb.Append("&SiteID=");
			sb.Append(_siteID);
			sb.Append("&SendingMemberID=");
			sb.Append(_sendingMemberID);
			sb.Append("&ReceivingMemberID=");
			sb.Append(_receivingMemberID);
			sb.Append("&TargetPhoneNumber=");
			sb.Append(_targetPhoneNumber);
			
			return sb.ToString();
		}
	}
}
