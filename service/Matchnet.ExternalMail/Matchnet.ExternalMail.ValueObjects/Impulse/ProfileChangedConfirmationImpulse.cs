﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
    /// <summary>
    /// Impulse to represent confirmation emails when certain profile data changes, this is sent
    /// as part of process to prevent fraud and hacked accounts
    /// </summary>
    [Serializable]
    public class ProfileChangedConfirmationImpulse : ImpulseBaseEmail
    {
        private int _MemberID;
        private string _FirstName;
        private string _Username;
        private string _EmailAddress;
        private List<string> _Fields;

        public ProfileChangedConfirmationImpulse(int memberID, int brandID, string firstName, string userName, string emailAddress, List<string> fields)
        {
            _MemberID = memberID;
            _FirstName = firstName;
            _Username = userName;
            _EmailAddress = emailAddress;
            _Fields = fields;
            base.BrandID = brandID;
        }

        public int MemberID { get { return _MemberID; } }
        public string FirstName { get { return _FirstName; } }
        public string Username { get { return _Username; } }
        public List<string> Fields { get { return _Fields; } }

        public override Int32 MemberIDForRandomSelection
        {
            get
            {
                return MemberID;
            }
        }

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_PROFILE_CHANGE_CONFIRMATION"; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendProfileChangedConfirmation; }
        }

        public override int TargetMemberId
        {
            get { return this.MemberID; }
        }

        public override string EmailAddress
        {
            get { return _EmailAddress; }
        }

        public override string GetQueryString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(base.GetQueryString());
            sb.Append("&MemberID=");
            sb.Append(MemberID);

            return (sb.ToString());
        }

    }
}
