using System;

using Matchnet.Email.ValueObjects;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for NewFlirtSMSAlertImpulse.
	/// </summary>
	/// 
	[Serializable]
	public class NewFlirtSMSAlertImpulse : SMSAlertImpulseBase
	{
		public NewFlirtSMSAlertImpulse(int BrandID, int SiteID, int ReceivingMemberID, int SendingMemberID, string TargetPhoneNumber)
		{
			this.BrandID = BrandID;
			this.SiteID = SiteID;
			this.ReceivingMemberID = ReceivingMemberID;
			this.SendingMemberID = SendingMemberID;
			this.TargetPhoneNumber = TargetPhoneNumber;
		}
	}
}
