﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
    [Serializable]
    public class BOGONotificationImpulse: ImpulseBaseEmail
    {
        private int _senderMemberId;
        private int _senderBrandId;
        private string _emailAddress;
        private string _giftPromo;
        private string _giftCode;
        

        public BOGONotificationImpulse(ScheduledNotificationImpulse scheduledNotificationImpulse)
        {
           if(scheduledNotificationImpulse.ScheduleName.ToLower() == "bogo")
           {
               _senderMemberId = scheduledNotificationImpulse.MemberID;
               _senderBrandId = scheduledNotificationImpulse.BrandID;
               _emailAddress = scheduledNotificationImpulse.EmailAddress;
               _giftPromo = scheduledNotificationImpulse.ScheduleDetail["GiftPromo"].ToString();
               _giftCode = scheduledNotificationImpulse.ScheduleDetail["GiftCode"].ToString();
               BrandID = scheduledNotificationImpulse.BrandID;
           }
           else
           {
               //this is not the right kind of scheduled notification, throw an error
               throw new ApplicationException("ScheduledNotification impulse is not tied to a 'BOGO' schedule.");
           }
        }

        public BOGONotificationImpulse(int memberId, int brandId, string emailAddress, string giftPromo, string giftCode)
        {
            _senderMemberId = memberId;
            _senderBrandId = brandId;
            _emailAddress = emailAddress;
            _giftPromo = giftPromo;
            _giftCode = giftCode;
            BrandID = brandId;
        }

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_BOGO_NOTIFICATION"; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendBOGONotification; }
        }

        public override int TargetMemberId
        {
            get { return Constants.NULL_INT; }
        }

        public override string EmailAddress
        {
            get { return _emailAddress; }
        }

        public override int MemberIDForRandomSelection
        {
            get { return _senderMemberId; }
        }

        public int SenderMemberId
        {
            get { return _senderMemberId; }
        }

        public int SenderBrandId
        {
            get { return _senderBrandId; }
        }

        public string GiftCode
        {
            get { return _giftCode; }
        }

        public string GiftPromo
        {
            get { return _giftPromo; }
        }
    }
}
