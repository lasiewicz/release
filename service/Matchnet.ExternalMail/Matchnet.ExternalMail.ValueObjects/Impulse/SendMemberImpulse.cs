using System;
using System.Text;
using System.Web;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for SendMemberImpulse.
	/// </summary>
	[Serializable]
	public class SendMemberImpulse : ImpulseBaseEmail
	{
		private string _friendName;
		private string _friendEmail;
		private string _userEmail;
		private int _sentMemberID;
		private string _subject;
		private string _message;
	    private int _sendingMemberID;

		private SendMemberImpulse()
		{

		}

		/// <param name="friendName">user-supplied friend's name.  cannot be null or blank.</param>
		/// <param name="friendEmail">user-supplied friend's email. cannot be null or blank</param>
		/// <param name="userEmail">user's email address.  cannot be null or blank</param>
		/// <param name="sentMemberID">member ID whose info is being sent</param>
		/// <param name="subject">user-supplied subject</param>
		/// <param name="message">user-supplied message</param>
		/// <param name="brandID">source brandID</param>
		public SendMemberImpulse(
			string friendName,
			string friendEmail,
			string userEmail,
			int sentMemberID,
			string subject,
			string message,
			int brandID)
		{
			if(friendEmail == null || string.Empty.Equals(friendEmail))
			{
				throw new Exception("friendEmail field cannot be null or blank");
			}
			else if(friendName == null || string.Empty.Equals(friendName))
			{
				throw new Exception("friendName field cannot be null or blank");
			}
			else if(userEmail == null || string.Empty.Equals(userEmail))
			{
				throw new Exception("userEmail field cannot be null or blank");
			}

			base.BrandID = brandID;
			_friendName = friendName;
			_friendEmail = friendEmail;
			_userEmail = userEmail;
			_sentMemberID = sentMemberID;
			_subject = subject;
			_message = message;
		}

        public SendMemberImpulse(
            string friendName,
            string friendEmail,
            string userEmail,
            int sentMemberID,
            string subject,
            string message,
            int brandID,
            int sendingMemberID)
            : this(friendName, friendEmail, userEmail, sentMemberID, subject, message, brandID)
       {
            _sendingMemberID = sendingMemberID;
        }

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _sentMemberID;
			}
		}

		public string FriendName
		{
			get { return(_friendName); }
		}

		public string FriendEmail
		{
			get { return(_friendEmail); }
		}

		public string UserEmail
		{
			get { return(_userEmail); }
		}

		public int SentMemberID
		{
			get { return(_sentMemberID); }
		}

		public string Subject
		{
			get { return(_subject); }
		}

		public string Message
		{
			get { return(_message); }
		}

	    public int SendingMemberID
	    {
	        get { return _sendingMemberID; }
	    }

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_SEND_MEMBER_TO_FRIEND"; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendToFriend; }
        }

        // Target is an external email address, we do not know/have the memberID of the target.
        // Set it as null_int to signal this.
        public override int TargetMemberId
        {
            get { return Constants.NULL_INT; }
        }

        public override string EmailAddress
        {
            get { return _friendEmail; }
        }

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append(base.GetQueryString());
			sb.Append("&FriendName=");
			sb.Append(HttpUtility.UrlEncode(FriendName));
			sb.Append("&FriendEmail=");
			sb.Append(HttpUtility.UrlEncode(FriendEmail));
			sb.Append("&UserEmail=");
			sb.Append(HttpUtility.UrlEncode(UserEmail));
			sb.Append("&SentMemberID=");
			sb.Append(SentMemberID);
			sb.Append("&Subject=");
			sb.Append(HttpUtility.UrlEncode(Subject));
			sb.Append("&Message=");
			sb.Append(HttpUtility.UrlEncode(Message));
            sb.Append("&SendingMemberID=");
            sb.Append(HttpUtility.UrlEncode(SendingMemberID.ToString()));

			return(sb.ToString());
		}
        
    }
}
