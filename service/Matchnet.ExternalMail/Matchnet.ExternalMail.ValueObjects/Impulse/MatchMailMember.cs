using System;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	public class MatchMailMember
	{
		private Int32 _memberID;
		private string _username;
		private DateTime _birthdate;
		private Int32 _regionID;
		private Int32 _genderMask;
		private string _aboutMe;
		private string _headline;
		private string _thumbPath;

		public MatchMailMember(Int32 memberID,
			string username,
			DateTime birthdate,
			Int32 regionID,
			Int32 genderMask,
			string aboutMe,
			string headline,
			string thumbPath)
		{
			_memberID = memberID;
			_username = username;
			_birthdate = birthdate;
			_regionID = regionID;
			_genderMask = genderMask;
			_aboutMe = aboutMe;
			_headline = headline;
			_thumbPath = thumbPath;
		}


		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}


		public string Username
		{
			get
			{
				return _username;
			}
		}


		public DateTime Birthdate
		{
			get
			{
				return _birthdate;
			}
		}


		public Int32 RegionID
		{
			get
			{
				return _regionID;
			}
		}


		public Int32 GenderMask
		{
			get
			{
				return _genderMask;
			}
		}


		public string AboutMe
		{
			get
			{
				return _aboutMe;
			}
		}


		public string Headline
		{
			get
			{
				return _headline;
			}
		}


		public string ThumbPath
		{
			get
			{
				return _thumbPath;
			}
		}
	}
}
