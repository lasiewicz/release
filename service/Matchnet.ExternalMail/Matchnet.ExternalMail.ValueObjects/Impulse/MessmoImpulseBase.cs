using System;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for MessmoImpulseBase.
	/// </summary>
	[Serializable]
	public abstract class MessmoImpulseBase : ImpulseBase
	{
		private DateTime _timestamp = new DateTime(DateTime.MinValue.Ticks);
	
		public DateTime TimeStamp
		{
			get { return _timestamp; }
			set { _timestamp = value; }
		}
	}
}
