using System;
using Matchnet.ActivityRecording.ValueObjects.Types;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	[Serializable]
    public abstract class MassMailImpulse : ImpulseBaseEmail
	{
		private String _recipientEmailAddress;
		private MiniprofileInfo _recipientMiniprofileInfo;
		private MiniprofileInfoCollection _targetMiniprofileInfoCollection;
        private String _SearchPrefType = "strict";
	    
        public String SearchPrefType
        {
            get { return _SearchPrefType; }
            set { _SearchPrefType = value; }
        }

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _recipientMiniprofileInfo.MemberID;
			}
		}


		public String RecipientEmailAddress
		{
			get
			{
				return _recipientEmailAddress;
			}
			set
			{
				_recipientEmailAddress = value;
			}
		}

		public MiniprofileInfo RecipientMiniprofileInfo 
		{
			get
			{
				return _recipientMiniprofileInfo;
			}
			set
			{
				_recipientMiniprofileInfo = value;
			}
		}

		public MiniprofileInfoCollection TargetMiniprofileInfoCollection
		{
			get
			{
				return _targetMiniprofileInfoCollection;
			}
			set
			{
				_targetMiniprofileInfoCollection = value;
			}
		}


        public abstract override string SettingConstant { get; }

        public abstract override ActivityRecording.ValueObjects.Types.ActionType ActionType { get; }

        public override int TargetMemberId
        {
            get { return _recipientMiniprofileInfo.MemberID; }
        }

        public override string EmailAddress
        {
            get { return _recipientEmailAddress; }
        }
    }
}
