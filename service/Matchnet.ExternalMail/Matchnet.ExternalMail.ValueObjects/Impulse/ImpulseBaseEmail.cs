﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.ActivityRecording.ValueObjects.Types;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
    [Serializable]
    public abstract class ImpulseBaseEmail : ImpulseBase
    {
        public abstract string SettingConstant { get; }
        public abstract ActionType ActionType { get; }
        public abstract int TargetMemberId { get; }
        public abstract string EmailAddress { get; }
        protected const string YESMAIL_100_PERCENT = "YESMAIL_100_PERCENT";

        public virtual string GetQueryString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(base.GetQueryString());
            sb.Append("&SettingConstant=" + SettingConstant == null ? string.Empty : SettingConstant);
            sb.Append("&ActionType=" + ((int) this.ActionType).ToString());
            sb.Append("&TargetMemberId=" + TargetMemberId.ToString());
            sb.Append("&EmailAddress=" + EmailAddress);

            return sb.ToString();
        }
    }
}
