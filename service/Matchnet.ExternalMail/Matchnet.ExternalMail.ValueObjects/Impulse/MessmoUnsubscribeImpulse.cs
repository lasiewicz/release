using System;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for MessmoUnsubscribeImpulse.
	/// </summary>
	[Serializable]
	public class MessmoUnsubscribeImpulse : MessmoImpulseBase
	{
		private int _memberID;

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _memberID;
			}
		}


		public int MemberID
		{
			get { return _memberID; }
		}

		public MessmoUnsubscribeImpulse()
		{
			
		}

		public MessmoUnsubscribeImpulse(Int32 brandID, DateTime timestamp, int memberID)
		{
			this.BrandID = brandID;
			this.TimeStamp = timestamp;
			this._memberID = memberID;
		}
	}
}
