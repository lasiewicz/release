using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using Matchnet;
using Matchnet.ActivityRecording.ValueObjects.Types;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Used by Spark WS.
	/// Mingle makes a call to Spark WS to trigger a new email which in turn uses this class
	/// </summary>
	[Serializable]
    public class MessageBoardInstantNotificationImpulse : ImpulseBaseEmail, IByteSerializable, ISerializable
	{
		#region Private Constants
		private const byte VERSION_001 = 1;
		#endregion

		#region Private Variables
		private int _siteID;
		private int _memberID;
		private string _boardName;
		private string _topicSubject;
		private string _replyURL;
		private int _newReplies;
		private string _unsubscribeURL;
		#endregion

		#region Public Properties
		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _memberID;
			}
		}


		public int SiteID
		{
			get { return _siteID; }
			set { _siteID = value; }
		}
		
		/// <summary>
		/// Recipient MemberID.
		/// </summary>
		public int MemberID
		{
			get { return _memberID; }
			set { _memberID = value; }
		}

		
		/// <summary>
		/// Name of the board the topic is in.
		/// </summary>
		public string BoardName
		{
			get { return _boardName; }
			set { _boardName = value; }
		}


		/// <summary>
		/// Name of the topic member is subscribing to.
		/// </summary>
		public string TopicSubject
		{
			get { return _topicSubject; }
			set { _topicSubject = value; }
		}


		/// <summary>
		/// URL to take the member to the page to reply.
		/// </summary>
		public string ReplyURL
		{
			get { return _replyURL; }
			set { _replyURL = value; }
		}


		/// <summary>
		///  Number of new replies added to the topic.
		/// </summary>
		public int NewReplies
		{
			get { return _newReplies; }
			set { _newReplies = value; }
		}


		/// <summary>
		/// URL to unsubscribe from the topic
		/// </summary>
		public string UnsubscribeURL
		{
			get { return _unsubscribeURL; }
			set { _unsubscribeURL = value; }
		}

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_MESSAGEBOARD_INSTANT_NOTIF"; }
        }

        public override ActionType ActionType
        {
            get { return ActionType.SendMessageBoardInstantNotification; }
        }

        public override int TargetMemberId
        {
            get { return _memberID; }
        }

        public override string EmailAddress
        {
            get { return Constants.NULL_STRING; }
        }
		#endregion

		#region Constructors
		/// <summary>
		/// Needed for deserialization
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessageBoardInstantNotificationImpulse(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandID"></param>
		/// <param name="siteID"></param>
		/// <param name="memberID"></param>
		/// <param name="boardName"></param>
		/// <param name="topicSubject"></param>
		/// <param name="replyURL"></param>
		/// <param name="newReplies"></param>
		/// <param name="unsubscribeURL"></param>
		public MessageBoardInstantNotificationImpulse(int brandID, int siteID, int memberID, string boardName, string topicSubject, string replyURL, int newReplies, string unsubscribeURL)
		{
			base.BrandID = brandID;
			this._siteID = siteID;
			this._memberID = memberID;
			this._boardName = boardName;
			this._topicSubject = topicSubject;
			this._replyURL = replyURL;
			this._newReplies = newReplies;
			this._unsubscribeURL = unsubscribeURL;
		}		
		#endregion

		#region Private Methods
		private static void writeString(BinaryWriter bw, string s)
		{
			if (s == null)
			{
				bw.Write(string.Empty);
			}
			else
			{
				bw.Write(s);
			}
		}
		#endregion

		#region Public Methods
		#region IByteSerializable & ISerializable Members
		/// <summary>
		/// Used by byte serialization
		/// </summary>
		/// <returns></returns>
		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			byte version = VERSION_001;

			bw.Write(version);
			bw.Write(base.BrandID);
			bw.Write(this._siteID);
			bw.Write(this._memberID);
			writeString(bw, this._boardName);
			writeString(bw, this._topicSubject);
			writeString(bw, this._replyURL);
			bw.Write(this._newReplies);
			writeString(bw, this._unsubscribeURL);
			
			return ms.ToArray();
		}

		/// <summary>
		/// Used by byte serialization
		/// </summary>
		/// <param name="bytes"></param>
		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new BinaryReader(ms);

			byte version = br.ReadByte();

			switch (version)
			{
				case VERSION_001:

					this.BrandID = br.ReadInt32();
					this._siteID = br.ReadInt32();
					this._memberID = br.ReadInt32();
					this._boardName = br.ReadString();
					this._topicSubject = br.ReadString();
					this._replyURL = br.ReadString();
					this._newReplies = br.ReadInt32();
					this._unsubscribeURL = br.ReadString();

					break;

				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}

		/// <summary>
		/// Used by serialization
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", this.ToByteArray());
		}
		#endregion
		#endregion

	    
	}
}