﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.IO;
using AjaxPro;
namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
    [Serializable]
    public class ScheduledNotificationImpulse:ImpulseBase, IByteSerializable, ISerializable
    {
        private const byte VERSION_001 = 1;
        public int MemberID { get; set; }
        public int EmailScheduleID { get; set; }
        public string ScheduleName { get; set; }
        public string EmailAddress { get; set; }
        public string ScheduleGUID { get; set; }
        public Hashtable ScheduleDetail { get; set; }

        public ScheduledNotificationImpulse(int groupid, int memberid, int emailscheduleid, string schedulename, string scheduleguid,string email, Hashtable details)
        {
            MemberID = memberid;
            BrandID = groupid;
            EmailScheduleID = emailscheduleid;
            EmailAddress = email;
            ScheduleName = schedulename;
            ScheduleDetail = details;
            ScheduleGUID = scheduleguid;

		}
        protected ScheduledNotificationImpulse(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}
     
		public override Int32 MemberIDForRandomSelection
		{
			get
			{
                if(MemberID > 0)
                    return MemberID;
                else
                    return EmailScheduleID;

			}
		}


	

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
            try
            {
                sb.Append(base.GetQueryString());
                sb.Append("Memberid=" + MemberID.ToString());
                sb.Append("EmailScheduleID=" + EmailScheduleID.ToString());
                sb.Append("ScheduleGUID=" + ScheduleGUID.ToString());
                sb.Append("EmailAddress=" + EmailAddress.ToString());
            }
            catch (Exception ex)
            {
            }


          

			return(sb.ToString());
		}


        #region IByteSerializable & ISerializable Methods
        /// <summary>
        /// Used for byte serialization
        /// </summary>
        /// <returns></returns>
        public byte[] ToByteArray()
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);

            byte version = VERSION_001;

            bw.Write(version);
            bw.Write(base.BrandID);
            bw.Write(MemberID);
            bw.Write(EmailScheduleID);
            writeString(bw, ScheduleGUID);
            writeString(bw, EmailAddress);
            writeString(bw, ScheduleName);
            writeString(bw, ToJSonString());
           
            return ms.ToArray();
        }

        /// <summary>
        /// Used for byte serialization
        /// </summary>
        /// <param name="bytes"></param>
        public void FromByteArray(byte[] bytes)
        {
            MemoryStream ms = new MemoryStream(bytes);
            BinaryReader br = new BinaryReader(ms);

            byte version = br.ReadByte();

            switch (version)
            {
                case VERSION_001:

                    base.BrandID = br.ReadInt32();
                    this.MemberID = br.ReadInt32();
                    this.EmailScheduleID = br.ReadInt32();
                    this.ScheduleGUID = br.ReadString();
                    this.EmailAddress = br.ReadString();
                    this.ScheduleName = br.ReadString();
                    string jsondetail = br.ReadString();
                    ScheduleDetail = FromJSonString(jsondetail);
                    
                    break;

                default:
                    throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
            }
        }

        /// <summary>
        /// Used for serialization
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("bytearray", this.ToByteArray());
        }

        private static void writeString(BinaryWriter bw, string s)
        {
            if (s == null)
            {
                bw.Write(string.Empty);
            }
            else
            {
                bw.Write(s);
            }
        }

        public Hashtable FromJSonString(string jsonstr)
        {
            return (Hashtable)JavaScriptDeserializer.DeserializeFromJson(jsonstr, typeof(Hashtable));

        }


        public string ToJSonString()
        {
            //_details = (Hashtable)JavaScriptDeserializer.DeserializeFromJson(jsonstr, typeof(Hashtable));
            StringBuilder bld = new StringBuilder();
            if (ScheduleDetail != null)
                JavaScriptSerializer.Serialize(ScheduleDetail, bld);

            return bld.ToString();

        }
        #endregion
    }
}
