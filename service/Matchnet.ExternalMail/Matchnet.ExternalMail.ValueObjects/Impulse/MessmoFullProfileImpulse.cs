using System;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Messmo API 2.23 a Channel Provides Full Profile Details to Messmo
	/// </summary>
	[Serializable]
	public class MessmoFullProfileImpulse : MessmoImpulseBase
	{
		#region Private Members
		private Int32 _memberID = Constants.NULL_INT;
		private string _taggleToken1 = Constants.NULL_STRING;
		#endregion

		#region Properties
		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _memberID;
			}
		}

		public Int32 MemberID
		{
			get { return _memberID; }
		}
		public string TaggleToken1
		{
			get { return _taggleToken1; }
		}
		public Int32 SenderBrandID
		{
			get { return this.BrandID; }
		}
		#endregion

		public MessmoFullProfileImpulse()
		{
			
		}

		public MessmoFullProfileImpulse(Int32 brandID, Int32 memberID, DateTime timestamp, string taggleToken1)
		{
			this.BrandID = brandID;
			this._memberID = memberID;
			this.TimeStamp = timestamp;
			this._taggleToken1 = taggleToken1;
		}
	}
}
