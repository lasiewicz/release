using System;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for MutualYesNotifyImpulse.
	/// </summary>
	[Serializable]
	public class MutualYesNotifyImpulse : ImpulseBaseEmail
	{
		private int _memberID;
		private int _targetMemberID;

		private MutualYesNotifyImpulse()
		{
		}

		public MutualYesNotifyImpulse(int memberID, int targetMemberID, int brandID)
		{
			base.BrandID = brandID;
			_memberID = memberID;
			_targetMemberID = targetMemberID;
		}

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _memberID;
			}
		}


		public int MemberID
		{
			get { return(_memberID); }
		}

		public int TargetMemberID
		{
			get { return(_targetMemberID); }
		}

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_MUTUAL_YES_MAIL"; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendMutualMail; }
        }

        public override int TargetMemberId
        {
            get { return this.MemberID; }
        }

        public override string EmailAddress
        {
            get { return Constants.NULL_STRING; }
        }

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append(base.GetQueryString());
			sb.Append("&MemberID=");
			sb.Append(MemberID);
			sb.Append("&TargetMemberID=");
			sb.Append(TargetMemberID);

			return(sb.ToString());
		}

        
    }
}
