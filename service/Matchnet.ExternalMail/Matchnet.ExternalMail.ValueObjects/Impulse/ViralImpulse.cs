using System;
using System.Text;
using Matchnet.ActivityRecording.ValueObjects.Types;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	[Serializable]
	public class ViralImpulse : MassMailImpulse
	{
		public const Int16 VIRAL_MEMBER_PROFILE_COUNT = 5;
        
		public ViralImpulse(Int32 brandID, String recipientEmailAddress, 
			MiniprofileInfo recipientMiniprofileInfo,
			MiniprofileInfoCollection targetMiniprofileInfoCollection)
		{
			base.BrandID = brandID;
			base.RecipientEmailAddress = recipientEmailAddress;
			base.RecipientMiniprofileInfo = recipientMiniprofileInfo;
			base.TargetMiniprofileInfoCollection = targetMiniprofileInfoCollection;
		}

        public override ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return ActionType.SendClickMail; }
        }

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_CLICK_MAIL"; }
        }
	}
}
