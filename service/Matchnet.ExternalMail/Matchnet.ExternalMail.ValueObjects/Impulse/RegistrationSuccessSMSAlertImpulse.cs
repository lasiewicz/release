using System;

using Matchnet.Email.ValueObjects;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
    /// Summary description for RegistrationSuccessSMSAlertImpulse.
	/// </summary>
	/// 
	[Serializable]
	public class RegistrationSuccessSMSAlertImpulse : SMSAlertImpulseBase
	{
        public RegistrationSuccessSMSAlertImpulse(int BrandID, int SiteID, int ReceivingMemberID, string TargetPhoneNumber)
		{
			this.BrandID = BrandID;
			this.SiteID = SiteID;
			this.ReceivingMemberID = ReceivingMemberID;
			this.TargetPhoneNumber = TargetPhoneNumber;
		}
	}
}
