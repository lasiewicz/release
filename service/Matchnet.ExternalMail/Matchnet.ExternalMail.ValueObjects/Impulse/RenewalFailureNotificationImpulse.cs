using System;
using System.IO;
using System.Runtime.Serialization;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Used by Purchase service to send out an email when a hard decline occurs.
	/// </summary>
	[Serializable]
	public class RenewalFailureNotificationImpulse: ImpulseBase, ISerializable, IByteSerializable
	{
		#region Constants
		private const byte VERSION_001 = 1;
		#endregion
		
		#region Private Variables
		private int _siteID = Constants.NULL_INT;
		private string _userName = Constants.NULL_STRING;
		private string _emailAddress = Constants.NULL_STRING;
		#endregion

		#region Public Properties
		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return Constants.NULL_INT;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int SiteID
		{
			get{return _siteID;}
		}
		
		/// <summary>
		/// 
		/// </summary>
		public string UserName
		{
			get
			{
				return _userName;
			}
		}
		
		/// <summary>
		/// 
		/// </summary>
		public string EmailAddress
		{
			get
			{
				return _emailAddress;
			}
		}
		#endregion
		
		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RenewalFailureNotificationImpulse(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteID"></param>
		/// <param name="userName"></param>
		/// <param name="emailAddress"></param>
		public RenewalFailureNotificationImpulse(int siteID, string userName, string emailAddress)
		{
			_siteID = siteID;
			_userName = userName;
			_emailAddress = emailAddress;
		}
		#endregion

		#region ISerializable Members
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", this.ToByteArray());
		}

		#endregion
		
		#region IByteSerializable Members
		
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			byte version = VERSION_001;

			bw.Write(version);
			bw.Write(BrandID);
			bw.Write(_siteID);
			writeString(bw, _userName);
			writeString(bw, _emailAddress);
		
			return ms.ToArray();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="bw"></param>
		/// <param name="s"></param>
		private static void writeString(BinaryWriter bw, string s)
		{
			if (s == null)
			{
				bw.Write(string.Empty);
			}
			else
			{
				bw.Write(s);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="bytes"></param>
		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new BinaryReader(ms);

			byte version = br.ReadByte();

			switch (version)
			{
				case VERSION_001:
					BrandID = br.ReadInt32();
					_siteID = br.ReadInt32();
					_userName = br.ReadString();
					_emailAddress = br.ReadString();
					
					break;

				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}
		
		#endregion
	}
}
