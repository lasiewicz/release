using System;

using Matchnet.Email.ValueObjects;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for NewEmailSMSAlertImpulse.
	/// </summary>
	/// 
	[Serializable]
	public class NewEmailSMSAlertImpulse : SMSAlertImpulseBase
	{
		public NewEmailSMSAlertImpulse(int BrandID, int SiteID, int ReceivingMemberID, int SendingMemberID, string TargetPhoneNumber)
		{
			this.BrandID = BrandID;
			this.SiteID = SiteID;
			this.ReceivingMemberID = ReceivingMemberID;
			this.SendingMemberID = SendingMemberID;
			this.TargetPhoneNumber = TargetPhoneNumber;
		}
	}
}
