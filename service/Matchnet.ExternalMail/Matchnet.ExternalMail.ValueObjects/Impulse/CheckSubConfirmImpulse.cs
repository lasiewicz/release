using System;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for CheckSubConfirmImpulse.
	/// </summary>
	[Serializable]
	public class CheckSubConfirmImpulse : ImpulseBase
	{
		private int _memberID;
		private int _memberPaymentID;
		private string _planDescription;

		private CheckSubConfirmImpulse()
		{
		}

		public CheckSubConfirmImpulse(int memberID, int brandID, int memberPaymentID, string planDescription)
		{
			base.BrandID = brandID;
			_memberID = memberID;
			_memberPaymentID = memberPaymentID;
			_planDescription = planDescription;
		}

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _memberID;
			}
		}


		public int MemberID
		{
			get { return(_memberID); }
		}

		public int MemberPaymentID
		{
			get { return(_memberPaymentID); }
		}

		public string PlanDescription
		{
			get { return(_planDescription); }
		}

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append(base.GetQueryString());
			sb.Append("&MemberID=");
			sb.Append(_memberID);
			sb.Append("&MemberPaymentID=");
			sb.Append(_memberPaymentID);
			sb.Append("&PlanDescription=");
			sb.Append(System.Web.HttpUtility.UrlEncode(PlanDescription));

			return(sb.ToString());
		}
	}
}
