using System;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// For now, this impulse shouldn't send anything.  This impulse is used for logging a sms validation send only.
	/// </summary>
	/// 
	[Serializable]
	public class ValidationSMSAlertImpulse : SMSAlertImpulseBase
	{
		private bool _sendValidationSMS = false;

		public bool SendValidationSMS
		{
			get
			{
				return _sendValidationSMS;
			}
			set
			{
				_sendValidationSMS = value;
			}
		}

		public ValidationSMSAlertImpulse(int BrandID, int SiteID, int ReceivingMemberID, int SendingMemberID, string TargetPhoneNumber)
		{
			this.BrandID = BrandID;
			this.SiteID = SiteID;
			this.ReceivingMemberID = ReceivingMemberID;
			this.SendingMemberID = SendingMemberID;
			this.TargetPhoneNumber = TargetPhoneNumber;
		}
	}
}
