using System;
using System.Text;

using Matchnet.Email.ValueObjects;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for InternalMailNotifyImpulse.
	/// </summary>
	[Serializable]
	public class InternalMailNotifyImpulse : ImpulseBaseEmail
	{
		private EmailMessage _emailMessage;
		private int _unreadEmailCount;

		private InternalMailNotifyImpulse()
		{
		}

		public InternalMailNotifyImpulse(EmailMessage emailMessage, int brandID, int unreadEmailCount)
		{
			base.BrandID = brandID;
			_emailMessage = emailMessage;
			_unreadEmailCount = unreadEmailCount;
		}
	
		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _emailMessage.FromMemberID;
			}
		}


		public int MemberID
		{
			get { return(_emailMessage.ToMemberID); }
		}

		public int SentMemberID
		{
			get { return(_emailMessage.FromMemberID); }
		}

		public int EmailID
		{
			get { return(_emailMessage.MailID); }
		}

		public DateTime InsertDate
		{
			get { return(_emailMessage.InsertDate); }
		}

		public int UnreadEmailCount
		{
			get { return(_unreadEmailCount); }
		}

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_NEW_MAIL_ALERT"; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendNewMailAlert; }
        }

        public override int TargetMemberId
        {
            get { return this.MemberID; }
        }

        public override string EmailAddress
        {
            get { return Constants.NULL_STRING; }
        }

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append(base.GetQueryString());
			sb.Append("&MemberID=");
			sb.Append(MemberID);
			sb.Append("&SentMemberID=");
			sb.Append(SentMemberID);
			sb.Append("&EmailID=");
			sb.Append(EmailID);
			sb.Append("&InsertDate=");
			sb.Append(InsertDate);
			sb.Append("&UnreadEmailCount=");
			sb.Append(UnreadEmailCount);

			return(sb.ToString());
		}

        
    }
}
