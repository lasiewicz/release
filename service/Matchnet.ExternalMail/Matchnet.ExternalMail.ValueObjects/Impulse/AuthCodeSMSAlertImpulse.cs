using System;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for AuthCodeSMSAlertImpulse.
	/// </summary>
	[Serializable]
	public class AuthCodeSMSAlertImpulse : SMSAlertImpulseBase
	{
		private string _AuthCode;

		public string AuthCode
		{
			get
			{
				return _AuthCode;
			}
			set 
			{
				_AuthCode = value;
			}
		}

		public AuthCodeSMSAlertImpulse(int BrandID, int SiteID, int ReceivingMemberID, string TargetPhoneNumber,
			string authCode)
		{
			this.BrandID = BrandID;
			this.SiteID = SiteID;
			this.ReceivingMemberID = ReceivingMemberID;
			this.TargetPhoneNumber = TargetPhoneNumber;
			this.AuthCode = authCode;
		}

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();

			// the following line should be instead of
			/*
			sb.Append("BrandID=");
			sb.Append(this.BrandID);
			sb.Append("&SiteID=");
			sb.Append(_siteID);
			sb.Append("&SendingMemberID=");
			sb.Append(_sendingMemberID);
			sb.Append("&ReceivingMemberID=");
			sb.Append(_receivingMemberID);
			sb.Append("&TargetPhoneNumber=");
			sb.Append(_targetPhoneNumber);
			*/

			sb.Append(base.GetQueryString());
			sb.Append("&AuthCode=");
			sb.Append(this.AuthCode);

			return sb.ToString();
		}
	}
}
