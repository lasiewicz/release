﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
    [Serializable]
    public class BetaFeedbackImpulse : ImpulseBase
    {
        private string _senderName;
		private string _senderEmailAddress;
		private string _senderMemberID;
		private string _browser;
		private string _remoteIP;
		private string _subject;
		private string _userComment;
        private int _feedbackTypeID;

		/// <summary>
		/// Hide no arg constructor
		/// </summary>
		private BetaFeedbackImpulse()
		{

		}

        public BetaFeedbackImpulse(int brandID,
								string senderName, 
								string senderEmailAddress,
								string senderMemberID,
								string browser,
								string remoteIP,
								string subject,
								string userComment,
                                int feedbackTypeID)
		{
			base.BrandID = brandID;
			_senderName = senderName;
			_senderEmailAddress = senderEmailAddress;
			_senderMemberID = senderMemberID;
			_browser = browser;
			_remoteIP = remoteIP;
			_subject = subject;
			_userComment = userComment;
            _feedbackTypeID = feedbackTypeID;
		}

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				int memberID = Constants.NULL_INT;
				try
				{
					memberID = int.Parse(_senderMemberID);
				}
				catch
				{}
				return memberID;
			}
		}


		public string SenderName
		{
			get { return(_senderName); }
		}

		public string SenderEmailAddress
		{
			get { return(_senderEmailAddress); }
		}

		public string SenderMemberID
		{
			get { return(_senderMemberID); }
		}

		public string Browser
		{
			get { return(_browser); }
		}

		public string RemoteIP
		{
			get { return(_remoteIP); }
		}

		public string Subject
		{
			get { return(_subject); }
		}

		public string UserComment
		{
			get { return(_userComment); }
		}

        public int FeedbackTypeID
        {
            get { return _feedbackTypeID; }
        }

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append(base.GetQueryString());
			sb.Append("&SenderName=");
			sb.Append(HttpUtility.UrlEncode(SenderName));
			sb.Append("&SenderEmailAddress=");
			sb.Append(HttpUtility.UrlEncode(SenderEmailAddress));
			sb.Append("&SenderMemberID=");
			sb.Append(HttpUtility.UrlEncode(SenderMemberID));
			sb.Append("&Browser=");
			sb.Append(HttpUtility.UrlEncode(Browser));
			sb.Append("&RemoteIP=");
			sb.Append(HttpUtility.UrlEncode(RemoteIP));
			sb.Append("&Subject=");
			sb.Append(HttpUtility.UrlEncode(Subject));
			sb.Append("&UserComment=");
			sb.Append(HttpUtility.UrlEncode(UserComment));
            sb.Append("&FeedbackTypeID=");
            sb.Append(FeedbackTypeID);

			return(sb.ToString());
		}
    }
}
