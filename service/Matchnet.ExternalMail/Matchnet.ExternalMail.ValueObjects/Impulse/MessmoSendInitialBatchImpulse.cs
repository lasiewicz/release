using System;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for MessmoSendInitialBatchImpulse.
	/// </summary>
	[Serializable]
	public class MessmoSendInitialBatchImpulse : MessmoImpulseBase
	{
		private int _memberID;
		private string _messmoSubscriberID;
		private string _status;
		private DateTime _setDate;
		private string _mobileNumber;

		#region Properties
		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _memberID;
			}
		}

		public int MemberID
		{
			get { return _memberID; }
		}	

		public string MessmoSubscriberID
		{
			get { return _messmoSubscriberID; }
		}

		public string Status
		{
			get { return _status; }
		}

		public DateTime SetDate
		{
			get { return _setDate; }
		}

		public string MobileNumber
		{
			get { return _mobileNumber; }
		}
		#endregion

		public MessmoSendInitialBatchImpulse()
		{
		
		}

		public MessmoSendInitialBatchImpulse(Int32 brandID, DateTime timestamp, int memberID, string messmoSubscriberID, string status,
			DateTime setDate, string mobileNumber)
		{
			this.BrandID = brandID;
			this.TimeStamp = timestamp;
			this._memberID = memberID;
			this._messmoSubscriberID = messmoSubscriberID;
			this._status = status;
			this._setDate = setDate;
			this._mobileNumber = mobileNumber;
		}

	}
}
