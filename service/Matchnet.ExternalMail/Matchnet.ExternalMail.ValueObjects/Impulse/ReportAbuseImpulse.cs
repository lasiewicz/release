using System;
using System.Web;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for ContactUsImpulse.
	/// </summary>
	[Serializable]
	public class ReportAbuseImpulse : ImpulseBase
	{
		private string _reportContent;

		/// <summary>
		/// Hide no arg constructor
		/// </summary>
		private ReportAbuseImpulse()
		{

		}

		/// <summary>
		/// Create a full ContactUsImpulse
		/// </summary>
		/// <param name="brandID">brand of the site that was the source of the contact</param>
		/// <param name="userName">user-supplied username</param>
		/// <param name="fromEmailAddress">user-supplied email address</param>
		/// <param name="sMemberID">user-supplied MemberID</param>
		/// <param name="browser">browser ID string from request</param>
		/// <param name="remoteIP">remote IP for the request</param>
		/// <param name="reason">drop-down selected reason</param>
		/// <param name="userComment">user free form comment</param>
		public ReportAbuseImpulse(
								int brandID,
								string ReportContent)
		{
			base.BrandID = brandID;
			_reportContent = ReportContent;
		}

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return Constants.NULL_INT;
			}
		}


		public string ReportContent
		{
			get { return(_reportContent); }
		}

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append(base.GetQueryString());
			sb.Append("&ReportContent=");
			sb.Append(HttpUtility.UrlEncode(ReportContent));

			return(sb.ToString());
		}
	}
}
