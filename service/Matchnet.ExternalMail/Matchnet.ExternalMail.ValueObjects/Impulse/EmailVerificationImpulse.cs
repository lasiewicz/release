using System;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for EmailVerificationImpulse.
	/// </summary>
	[Serializable]
	public class EmailVerificationImpulse : ImpulseBaseEmail
	{
		private int _memberID;

		private EmailVerificationImpulse()
		{

		}

		public EmailVerificationImpulse(int memberID, int brandID)
		{
			base.BrandID = brandID;
			this._memberID = memberID;
		}

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _memberID;
			}
		}


		public int MemberID
		{
			get { return(_memberID); }
			set { _memberID = value; }
		}

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_EMAIL_CHANGE_CONFIRMATION"; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendEmailChangeConfirmation; }
        }

        public override int TargetMemberId
        {
            get { return this.MemberID; }
        }

        public override string EmailAddress
        {
            get { return Constants.NULL_STRING; }
        }

        public override string GetQueryString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(base.GetQueryString());
            sb.Append("&MemberID=");
            sb.Append(MemberID);

            return (sb.ToString());
        }
    }
}
