using System;
using System.Web;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for MatchMeterCongratsMailImpulse.
	/// </summary>
	/// 

	[Serializable]
	public class MatchMeterCongratsMailImpulse : ImpulseBaseEmail
	{
		int _SiteID;
		int _MemberID;
		string _UserName;
		string _EmailAddr;

		public MatchMeterCongratsMailImpulse(int brandID, int siteID,int memberID , string userName, string emailAddr)
		{
			base.BrandID = brandID;
			_SiteID = siteID;
			_MemberID = memberID;
			_UserName = userName;
			_EmailAddr = emailAddr;
		}

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _MemberID;
			}
		}


		public int SiteID
		{
			get { return _SiteID; }
		}
		public int MemberID
		{
			get { return _MemberID; }
		}
		public string UserName
		{
			get { return _UserName; }
		}
		public string EmailAddr
		{
			get { return _EmailAddr; }
		}

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_MATCH_METER_CONGRATS"; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendMatchMeterCongrats; }
        }

        public override int TargetMemberId
        {
            get { return this.MemberID; }
        }

        public override string EmailAddress
        {
            get { return Constants.NULL_STRING; }
        }

        public override string GetQueryString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(base.GetQueryString());
            sb.Append("&SiteID=");
            sb.Append(HttpUtility.UrlEncode(SiteID.ToString()));
            sb.Append("&MemberID=");
            sb.Append(HttpUtility.UrlEncode(MemberID.ToString()));
            sb.Append("&UserName=");
            sb.Append(HttpUtility.UrlEncode(UserName));
            sb.Append("&EmailAddr=");
            sb.Append(HttpUtility.UrlEncode(EmailAddr));
            sb.Append("&RecipientEmailAddress=");
            sb.Append(HttpUtility.UrlEncode(EmailAddr));

            return (sb.ToString());
        }
    }
}
