using System;
using System.Text;
using Matchnet.ActivityRecording.ValueObjects.Types;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for JDateMobileRegistrationConfirmation.
	/// </summary>
	[Serializable]
    public class JDateMobileRegistrationConfirmationImpulse : ImpulseBaseEmail
	{
		private string m_userName;
		private string m_mobileNumber;
		private int m_memberID;
		private string m_emailAddress;

		#region Properties

		public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return m_memberID;
			}
		}


		public string UserName
		{
			get { return m_userName; }
		}
		public string MobileNumber
		{
			get { return m_mobileNumber; }
		}
		public int MemberID
		{
			get { return m_memberID; }
		}

	    public override string SettingConstant
	    {
            get { return "YESMAIL_PERCENT_JDATE_MOBILE_REG"; }
	    }

	    public override ActionType ActionType
	    {
	        get { return ActionType.SendJDateMobileRegistrationConfirmation; }
	    }

	    public override int TargetMemberId
	    {
	        get { return m_memberID; }
	    }

	    public override string EmailAddress
		{
			get { return m_emailAddress; }
		}
		#endregion

		private JDateMobileRegistrationConfirmationImpulse()
		{
		}

		public JDateMobileRegistrationConfirmationImpulse(int brandID, string userName, string mobileNumber, int memberID, string emailAddress)
		{
			this.BrandID = brandID;
			m_userName = userName;
			m_mobileNumber = mobileNumber;
			m_memberID = memberID;
			m_emailAddress = emailAddress;
		}

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();

			sb.Append(base.GetQueryString());
			sb.Append("&UserName=");
			sb.Append(UserName);
			sb.Append("&MobileNumber=");
			sb.Append(MobileNumber);
			sb.Append("&MemberID=");
			sb.Append(MemberID);
			sb.Append("&EmailAddress=");
			sb.Append(EmailAddress);
			
			return sb.ToString();
		}
	}
}
