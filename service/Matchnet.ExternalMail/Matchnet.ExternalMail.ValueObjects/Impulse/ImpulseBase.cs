using System;
using System.Text;


namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	[Serializable]
	public abstract class ImpulseBase : IValueObject
	{
		private Int32 _brandID;

		public Int32 BrandID
		{
			get
			{
				return _brandID;
			}
			set
			{
				_brandID = value;
			}
		}

		public abstract Int32 MemberIDForRandomSelection
		{
			get;
		}

        public int AttemptsCount { get; private set; }
        public DateTime FirstAttemptedUtc { get; private set; }        
        public virtual string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();
			
			sb.Append("BrandID=");
			sb.Append(_brandID);
			sb.Append("&EmailType=");
			sb.Append(EmailTypeConverter.ToInt(this));

			return sb.ToString();	
		}

        public ImpulseBase CopyWithSettingFirstAttemptUtc()
        {
            var ret = (ImpulseBase)MemberwiseClone();
            ret.FirstAttemptedUtc = DateTime.UtcNow;
            return ret;
        }
        
        public ImpulseBase CopyWithAttemptIncrement()
	    {
	        var ret = (ImpulseBase) MemberwiseClone();
	        ret.AttemptsCount++;
	        return ret;
	    }
    
        public ImpulseBase CopyWithAttemptReset()
        {
            var ret = (ImpulseBase)MemberwiseClone();
            ret.AttemptsCount = 0;
            return ret;
        }
    }
}
