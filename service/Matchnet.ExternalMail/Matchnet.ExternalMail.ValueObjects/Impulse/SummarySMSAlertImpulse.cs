using System;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
	/// <summary>
	/// Summary description for SummarySMSAlertImpulse.
	/// </summary>
	[Serializable]
	public class SummarySMSAlertImpulse : SMSAlertImpulseBase
	{
		private int _EmailCount;
		private int _EcardCount;
		private int _FlirtCount;

		public int EmailCount
		{
			get 
			{
				return _EmailCount;
			}
			set
			{
				_EmailCount= value;
			}
		}
		public int EcardCount
		{
			get
			{
				return _EcardCount;
			}
			set
			{
				_EcardCount = value;
			}
		}
		public int FlirtCount
		{
			get
			{
				return _FlirtCount;
			}
			set
			{
				_FlirtCount = value;
			}
		}

		public SummarySMSAlertImpulse(int BrandID, int SiteID, int ReceivingMemberID, string TargetPhoneNumber,
			int emailCount, int ecardCount, int flirtCount)
		{
			this.BrandID = BrandID;
			this.SiteID = SiteID;
			this.ReceivingMemberID = ReceivingMemberID;
			this.TargetPhoneNumber = TargetPhoneNumber;
			this.EmailCount = emailCount;
			this.EcardCount = ecardCount;
			this.FlirtCount = flirtCount;
		}

		public override string GetQueryString()
		{
			StringBuilder sb = new StringBuilder();

			// the following line should be instead of
			/*
			sb.Append("BrandID=");
			sb.Append(this.BrandID);
			sb.Append("&SiteID=");
			sb.Append(_siteID);
			sb.Append("&SendingMemberID=");
			sb.Append(_sendingMemberID);
			sb.Append("&ReceivingMemberID=");
			sb.Append(_receivingMemberID);
			sb.Append("&TargetPhoneNumber=");
			sb.Append(_targetPhoneNumber);
			*/

			sb.Append(base.GetQueryString());
			sb.Append("&EmailCount=");
			sb.Append(this.EmailCount);
			sb.Append("&EcardCount=");
			sb.Append(this.EcardCount);
			sb.Append("&FlirtCount");
			sb.Append(this.FlirtCount);

			return sb.ToString();
		}
	}
}
