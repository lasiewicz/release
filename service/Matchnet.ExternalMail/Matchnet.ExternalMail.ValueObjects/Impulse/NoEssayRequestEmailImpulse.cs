﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
    [Serializable]
    public class NoEssayRequestEmailImpulse : ImpulseBaseEmail
    {
        private int _memberID;
		private int _requestedByMemberID;

		private NoEssayRequestEmailImpulse()
		{
		}

        public NoEssayRequestEmailImpulse(int memberId, int requestedByMemberId, int brandId)
        {
            _memberID = memberId;
            _requestedByMemberID = requestedByMemberId;
            this.BrandID = brandId;
        }

        public override Int32 MemberIDForRandomSelection
		{
			get
			{
				return _memberID;
			}
		}

		public int MemberID
		{
			get { return(_memberID); }
		}

		public int RequestedByMemberID
		{
            get { return (_requestedByMemberID); }
		}

        public override string SettingConstant
        {
            get { return YESMAIL_100_PERCENT; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendNoEssayRequestEmail; }
        }

        public override int TargetMemberId
        {
            get { return this.MemberID; }
        }

        public override string EmailAddress
        {
            get { return Constants.NULL_STRING; }
        }

    }
}
