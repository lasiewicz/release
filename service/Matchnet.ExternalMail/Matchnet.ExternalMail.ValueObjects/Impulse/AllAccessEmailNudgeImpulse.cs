﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects.Impulse
{
    [Serializable]
    public class AllAccessEmailNudgeImpulse : ImpulseBaseEmail
    {
        private int _senderMemberId;
        private int _senderBrandId;
        private int _messageListId;
        private int _recipientMemberId;
        private string _emailSubject;
        private string _emailContent;
        private DateTime _initialMailTime;

        public int SenderMemberId
        {
            get { return _senderMemberId; }
        }

        public int SenderBrandId
        {
            get { return _senderBrandId; }
        }

        public int MessageListId
        {
            get { return _messageListId; }
        }

        public string EmailSubject
        {
            get { return _emailSubject; }
        }

        public string EmailContent
        {
            get { return _emailContent; }
        }

        public override string SettingConstant
        {
            get { return "YESMAIL_PERCENT_ALL_ACCESS_NUDGE"; }
        }

        public override Matchnet.ActivityRecording.ValueObjects.Types.ActionType ActionType
        {
            get { return Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendAllAccessEmailNudge; }
        }

        public override int TargetMemberId
        {
            get { return _recipientMemberId; }
        }

        public override string EmailAddress
        {
            get { return Constants.NULL_STRING; }
        }

        public override int MemberIDForRandomSelection
        {
            get { return _recipientMemberId; }
        }

        public AllAccessEmailNudgeImpulse(int pSenderMemberId, int pSenderBrandId, int pMessageListId, int pRecipientMemberId, int pRecipientBrandId, string pEmailSubject, string pEmailContent, DateTime pInitialMailTime)
        {
            this._senderMemberId = pSenderMemberId;
            this._senderBrandId = pSenderBrandId;
            this._messageListId = pMessageListId;
            this._recipientMemberId = pRecipientMemberId;
            this._emailSubject = pEmailSubject;
            this._emailContent = pEmailContent;
            this.BrandID = pRecipientBrandId;
            this._initialMailTime = pInitialMailTime;
        }

        public override string GetQueryString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(base.GetQueryString());
            sb.Append("&SenderMemberID=" + _senderMemberId.ToString());
            sb.Append("&SenderBrandID=" + _senderBrandId.ToString());
            sb.Append("&MessageListID=" + _messageListId.ToString());
            sb.Append("&EmailSubject=" + _emailSubject);
            sb.Append("&InitialMailTime=" + _initialMailTime.ToString());
            return sb.ToString();
        }
    }
}
