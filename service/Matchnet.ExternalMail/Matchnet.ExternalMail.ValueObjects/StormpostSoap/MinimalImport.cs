using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


namespace Matchnet.ExternalMail.ValueObjects.StormpostSoap
{
	/// <summary>
	/// Summary description for MinimalImport.
	/// </summary>
	[Serializable()]
	public class MinimalImport: IValueObject, IByteSerializable, ISerializable
	{

		private byte VERSION_001 = 1;
		int TemplateID;
		int CommunityID;

		/// <summary>
		/// Positional ordered set of strings that adhere to the template ID definition in stormpost for this community
		/// Current definition should have :
		/// EmailAddress, MemberID - in that order.
		/// </summary>
		string [] DataItems;
		public MinimalImport()
		{
		}

		public MinimalImport(int templateID, int communityID, string [] dataItems)
		{
			this.TemplateID = templateID;
			this.CommunityID = communityID;
			this.DataItems = dataItems;
		}

		#region IByteSerializable Members

		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new BinaryReader(ms);

			byte version = br.ReadByte();
			if (version != VERSION_001)
				throw new Exception("MinimalImport serialized version not supported " + version.ToString());

			TemplateID = br.ReadInt32();
			CommunityID = br.ReadInt32();
			int length = br.ReadInt32();
			DataItems = new string[length];
			for (int i = 0; i < length; i++)
				DataItems[i] = br.ReadString();
				
		}

		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream(128);
			BinaryWriter bw = new BinaryWriter(ms);
			
			bw.Write(this.VERSION_001);
			bw.Write(this.TemplateID);
			bw.Write(this.CommunityID);

			if (this.DataItems == null)
				bw.Write((int)0);
			else 
			{
				for (int i = 0; i < DataItems.Length; i++)
					bw.Write(DataItems[i]);
			}
			

			return ms.ToArray();
		}

		#endregion

		#region ISerializable Members

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", this.ToByteArray());
		}

		protected MinimalImport(SerializationInfo info,  StreamingContext context){
			FromByteArray(info.GetValue("bytearray", typeof(byte[])) as byte[]);
		}

		#endregion
	}
}
