using System;

namespace Matchnet.ExternalMail.ValueObjects.StormpostSoap
{
	public class Constants 
	{
		public const string ERROR_QUEUE_NAME = @".\Private$\externalmail_SOAP_error";
	}

	/// <summary>
	/// Used for SOAP api import calls to stormpost.
	/// </summary>
	public enum ImportTemplateType: int 
	{
		/// <summary>
		/// A template that includes only email address and memberID
		/// </summary>
		Minimal = 0
	}		

	public enum SendTemplateType: int 
	{
		SendTemplate_PasswordReminder = 1,
		SendTemplate_Activation = 2,
		SendTemplate_EmailChangeConfiration = 3,
		SendTemplate_MessageBoardInstantNotification = 4,
		SendTemplate_MessageBoardDailyUpdates = 5,
		SendTemplate_RenewalFailureNotification = 6,
		SendTemplate_EcardSendToMember = 7,
		SendTemplate_EcardSendToFriend = 8
	}		

}
