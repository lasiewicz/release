using System;

namespace Matchnet.ExternalMail.ValueObjects.BulkMail
{
	/// <summary>
	/// Summary description for Token.
	/// </summary>
	public class Token
	{
		string _name;
		string _value;

		public Token()
		{
			
		}

		public Token(string tokenName, string tokenValue)
		{
			_name = tokenName;
			_value = tokenValue;
		}

		public string Value
		{
			get
			{
				return _value;
			}
			set
			{
				_value = value;
			}
		}

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}
	}
}
