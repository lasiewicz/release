using System;
using System.Collections;
using System.Text;

namespace Matchnet.ExternalMail.ValueObjects
{
	[Serializable]
	public class MessageInfo
	{
		private bool _doSend = true;
		private string _fromAddress;
		private string _fromName;
		private string _toAddress;
		private string _toName;
		private string _subject;
		private Hashtable _headers;		
		
		public MessageInfo()
		{
			_headers = new Hashtable();
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("FromAddress=");
			sb.Append(FromAddress);
			sb.Append(",FromName=");
			sb.Append(FromName);
			sb.Append(",ToAddress=");
			sb.Append(ToAddress);
			sb.Append(",ToName=");
			sb.Append(ToName);
			sb.Append(",Subject=");
			sb.Append(Subject);
			sb.Append(",DoSend=");
			sb.Append(DoSend);

			return(sb.ToString());
		}

		public bool DoSend
		{
			get
			{
				return(_doSend);
			}
			set
			{
				_doSend = value;
			}
		}


		public string FromAddress
		{
			get
			{
				return _fromAddress;
			}
			set
			{
				 _fromAddress = value;
			}
		}


		public string FromName
		{
			get
			{
				return _fromName;
			}
			set
			{
				_fromName = value;
			}
		}


		public string ToAddress
		{
			get
			{
				return _toAddress;
			}
			set
			{
				_toAddress = value;
			}
		}


		public string ToName
		{
			get
			{
				return _toName;
			}
			set
			{
				_toName = value;
			}
		}


		public string Subject
		{
			get
			{
				return _subject;
			}
			set
			{
				_subject = value;
			}
		}


		public Hashtable Headers
		{
			get
			{
				return _headers;
			}
			set
			{
				_headers = value;
			}
		}
	}
}
