using System;

namespace Matchnet.ExternalMail.ValueObjects
{	
	public enum EmailAlertMask
	{
		GotClickAlert = 1,
		NewEmailAlert = 2,
		ECardAlert = 4,
		HotListedAlert = 8,
        ProfileViewedAlertOptOut = 16,
        NoEssayRequestEmail = 32
	};

	public class EmailAlertConstant
	{
		public const string ALERT_ATTRIBUTE_NAME = "AlertEmailMask";
		public const string YES_MAIL_FLAG = "UseYesMail";
		public const int ATTRIBUTE_ID = 510;
	}

	public enum NewsEventOfferMask
	{
		News = 1,
		Events = 2,
		Offers = 4
	};
}
