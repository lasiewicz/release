using System;
using Matchnet;

namespace Matchnet.ExternalMail.ValueObjects.DoNotEmail
{
	/// <summary>
	/// Summary description for DoNotEmailEntry.
	/// </summary>
	[Serializable]
	public class DoNotEmailEntry: ICacheable
	{
		private int _SiteID;
		private int _MemberID; 
		private string _EmailAddress;
		private DateTime _InsertDate;
		public static readonly string CACHE_PREPEND = "~DNE";
		public static readonly string CACHE_SEPARATOR = "~~";

		//ICacheable members
		private int _Ttl = 86400;
		CacheItemMode _Mode = CacheItemMode.Absolute;
		CacheItemPriorityLevel _Priority = CacheItemPriorityLevel.Default;

		
		public int SiteID
		{
			get { return _SiteID;}
			set {_SiteID = value;}
		}
	
		public int MemberID
		{
			get { return _MemberID;}
			set {_MemberID = value;}
		}

		public string EmailAddress
		{
			get {return _EmailAddress;}
			set {_EmailAddress = value;}
		}

		public DateTime InsertDate
		{
			get {return _InsertDate;}
			set {_InsertDate = value;}
		}


		public DoNotEmailEntry()
		{
		}

		public DoNotEmailEntry(int siteID, int memberID, string emailAddress, DateTime insertDate)
		{
			_SiteID = siteID;
			_MemberID = memberID;
			_EmailAddress = emailAddress.ToLower();
			_InsertDate = insertDate;
		}

		public DoNotEmailEntry(int siteID, string emailAddress)
		{
			_SiteID = siteID;
			_EmailAddress = emailAddress.ToLower();
		}

		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get
			{
				return _Ttl;
			}
			set
			{
				_Ttl = value;
			}
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return _Mode;
			}
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _Priority;
			}
			set
			{
				_Priority = value;
			}
		}

		public string GetCacheKey()
		{
			return _SiteID.ToString() + CACHE_SEPARATOR + _EmailAddress;
		}

		#endregion
	}
}
