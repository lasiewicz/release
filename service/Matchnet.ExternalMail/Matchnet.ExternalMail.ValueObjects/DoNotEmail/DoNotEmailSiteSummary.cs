using System;

namespace Matchnet.ExternalMail.ValueObjects.DoNotEmail
{
	/// <summary>
	/// Summary description for DoNotEmailSiteSummary.
	/// </summary>
	[Serializable]
	public class DoNotEmailSiteSummary
	{
		private int _SiteID;
		private string _SiteName;
		private int _Count;

		public int SiteID
		{
			get { return _SiteID;}
			set {_SiteID = value;}
		}

		public int Count
		{
			get { return _Count;}
			set {_Count = value;}
		}

		public string SiteName
		{
			get { return _SiteName;}
			set {_SiteName = value;}
		}
		
		public DoNotEmailSiteSummary()
		{

		}

		public DoNotEmailSiteSummary(int siteID, int count, string siteName)
		{
			_SiteID = siteID;
			_Count = count;
			_SiteName = siteName;
		}
	}
}
