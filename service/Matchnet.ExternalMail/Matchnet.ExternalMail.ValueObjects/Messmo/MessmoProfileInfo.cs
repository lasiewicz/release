using System;

namespace Matchnet.ExternalMail.ValueObjects.Messmo
{
	/// <summary>
	/// Summary description for MessmoProfileInfo.
	/// </summary>
	[Serializable]
	public class MessmoProfileInfo
	{
		Int32 _memberID = Constants.NULL_INT;
        string _userName = Constants.NULL_STRING;
		Int32 _genderMask = Constants.NULL_INT;
		DateTime _birthDate = new DateTime(DateTime.MinValue.Ticks);
		Int32 _regionID = Constants.NULL_INT;
		string _primaryPhotoThumbURL = Constants.NULL_STRING;
		string _aboutMe = Constants.NULL_STRING;
        Int32 _height = Constants.NULL_INT;
		Int32 _relationshipStatus = Constants.NULL_INT;
		Int32 _numberOfChildren = Constants.NULL_INT;
		Int32 _smokingHabits = Constants.NULL_INT;
		Int32 _education = Constants.NULL_INT;
		string _occupation = Constants.NULL_STRING;

		public MessmoProfileInfo()
		{
			
		}
	}
}
