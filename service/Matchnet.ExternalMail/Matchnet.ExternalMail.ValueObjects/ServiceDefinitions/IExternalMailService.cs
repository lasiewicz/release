using System;

using Matchnet.ExternalMail.ValueObjects.Impulse;


namespace Matchnet.ExternalMail.ValueObjects.ServiceDefinitions
{
	public interface IExternalMailService
	{
		void Enqueue(ImpulseBase impulse); 
		bool NewInboxItemReceived(int memberID, int siteID, SMSAlertsDefinitions.MessageType messageType, bool isFriend);
		void UpdatePhonesList(int memberID, int brandID, string phoneNumber, SMSAlertsDefinitions.Action action);
		bool IsPhoneAvailable(int siteID, string phoneNumber);
	}
}
