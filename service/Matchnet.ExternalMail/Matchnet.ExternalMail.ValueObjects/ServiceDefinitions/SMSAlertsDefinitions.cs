using System;

namespace Matchnet.ExternalMail.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for SMSAlertsDefinitions.
	/// </summary>
	public class SMSAlertsDefinitions
	{
		[Flags]
		public enum SMSAlertsPrefs
		{
			YouveClickedWithSomeone = 1,
			NewEmailReceived = 2,
			NewEcardReceived = 4,
			SomeoneHotlistedYou = 8,
			NewFlirtReceived = 16,
			/* The following are new for JDIL SMS Alerts */
			NewEmailFromFriends = 32, // member I emailed or tagged as favorites
			AllowSummary = 64,
			AllowSaturday = 128,
			PhaseTwoUser = 256
		}
		public enum MessageType
		{
			Email,
			Flirt, 
			Ecard
		}
		public enum Action 
		{
			Insert,
			Remove,
			Update
		}

		public SMSAlertsDefinitions()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
