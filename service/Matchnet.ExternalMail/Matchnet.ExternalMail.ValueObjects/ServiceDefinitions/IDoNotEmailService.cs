using System;
using System.Collections;
using Matchnet.ExternalMail.ValueObjects.DoNotEmail;

namespace Matchnet.ExternalMail.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IDoNotEmailService.
	/// </summary>
	public interface IDoNotEmailService
	{
		bool AddEmailAddress(int brandID, string emailAddress, int memberID);
		bool AddEmailAddressBySiteID(int siteID, string emailAddress);
		bool RemoveEmailAddress(int brandID, string emailAddress);
		DoNotEmailEntry GetEntryBySiteAndEmailAddress(int siteID, string emailAddress);
		DoNotEmailEntry GetEntryByEmailAddress(string emailAddress);
		ArrayList GetEntriesBySite(int siteID);
		ArrayList GetEntries();
		ArrayList Search(int siteID, string partialEmailAddress,int startRow, int pageSize, ref int totalRows);
		ArrayList GetSiteSummary();
	}
}
