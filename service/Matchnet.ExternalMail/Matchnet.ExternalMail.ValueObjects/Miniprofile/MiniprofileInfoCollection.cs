using System;
using System.Collections;
using System.Runtime.Serialization;
using System.IO;

namespace Matchnet.ExternalMail.ValueObjects.Miniprofile
{
	[Serializable]
	public class MiniprofileInfoCollection : CollectionBase, ISerializable, IByteSerializable
	{
		private const byte VERSION_001 = 1;

		#region Constructors
		/// <summary>
		/// Needed for deserialization
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MiniprofileInfoCollection(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		public MiniprofileInfoCollection()
		{
		}
		#endregion

		#region Public Methods
		public void Add(MiniprofileInfo miniprofileInfo)
		{     
			List.Add(miniprofileInfo);
		}       

		public void Remove(int index)
		{
			// Check to see if there is a Leaf at the supplied index.
			if (index > Count - 1 || index < 0) 
			{
				// Handle the error that occurs if the valid page index is       
				// not supplied.    
				// This exception will be written to the calling function             
				throw new Exception("Index out of bounds");            
			}        
			List.RemoveAt(index);       
		} 
		// Method implementation from the CollectionBase class
		public MiniprofileInfo Item(int Index) 
		{     
			// The appropriate item is retrieved from the List object and     
			// explicitly cast to the Leaf type, then returned to the      
			// caller.     
			return (MiniprofileInfo) List[Index]; 
		}

		#region ISerializable Members

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", this.ToByteArray());
		}

		#endregion

		#region IByteSerializable Members

		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new BinaryReader(ms);

			byte version = br.ReadByte();

			switch (version)
			{
				case VERSION_001:
					int length = br.ReadInt32();

					for (int i = 0; i < length; i++)
					{
						int miniLength = br.ReadInt32();
						MiniprofileInfo mini = new MiniprofileInfo();
						mini.FromByteArray(br.ReadBytes(miniLength));
						this.List.Add(mini);
					}

					break;

				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}

		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			byte version = VERSION_001;

			bw.Write(version);
			bw.Write(List.Count);

			foreach (object item in this.List)
			{
				MiniprofileInfo miniprofileInfo = (MiniprofileInfo) item;
				bw.Write(miniprofileInfo.ToByteArray().Length);
				bw.Write(miniprofileInfo.ToByteArray());
			}

			return ms.ToArray();
		}

		#endregion
		#endregion
	}
}
