using System;
using System.IO;
using System.Runtime.Serialization;

using Matchnet;

namespace Matchnet.ExternalMail.ValueObjects.Miniprofile
{
	[Serializable]
	public class MiniprofileInfo : IByteSerializable, ISerializable
	{
		private const byte VERSION_001 = 1;

		#region Private Variables
		Int32 _memberID = Constants.NULL_INT;
		String _userName = Constants.NULL_STRING;
		DateTime _birthDate = new DateTime(DateTime.MinValue.Ticks);
		Int32 _regionID = Constants.NULL_INT;
		String _thumbNailWebPath = Constants.NULL_STRING;
		String _aboutMe = Constants.NULL_STRING;
		Int32 _genderMask = Constants.NULL_INT;

		// Extra attributes for recipient member
		Int32 _religionID = Constants.NULL_INT;
		Int32 _JdateReligionID = Constants.NULL_INT;
		Int32 _ethnicityID = Constants.NULL_INT;
		Int32 _JdateEthnicityID = Constants.NULL_INT;
		Int32 _maritalStatusID = Constants.NULL_INT;
		Int32 _childrenCount = Constants.NULL_INT;
		Int32 _custodyID = Constants.NULL_INT;
		Boolean _isPayingMember = false;
		Int32 _globalStatusMask = Constants.NULL_INT;
		Boolean _hasApprovedPhotos = false;
		Int32 _entertainmentLocation = Constants.NULL_INT;
		Int32 _leisureActivity = Constants.NULL_INT;
		Int32 _cuisine = Constants.NULL_INT;
		Int32 _music = Constants.NULL_INT;
		Int32 _reading = Constants.NULL_INT;
		Int32 _physicalActivity = Constants.NULL_INT;
		Int32 _pets = Constants.NULL_INT;
		#endregion

		#region Public Properties
		public Int32 MemberID 
		{
			get
			{
				return _memberID;
			}
			set
			{
				_memberID = value;
			}
		}
		public String UserName 
		{
			get
			{
				return _userName;
			}
			set
			{
				_userName = value;
			}
		}

		public DateTime BirthDate 
		{
			get
			{
				return _birthDate;
			}
			set
			{
				_birthDate = value;
			}
		}
		public Int32 RegionID 
		{
			get
			{
				return _regionID;
			}
			set
			{
				_regionID = value;
			}
		}

		public String ThumbNailWebPath 
		{
			get
			{
				return _thumbNailWebPath;
			}
			set
			{
				_thumbNailWebPath = value;
			}
		}
		public String AboutMe 
		{
			get
			{
				return _aboutMe;
			}
			set
			{
				_aboutMe = value;
			}
		}

		public Int32 GenderMask 
		{
			get
			{
				return _genderMask;
			}
			set
			{
				_genderMask = value;
			}
		}
		

		public Int32 ReligionID
		{
			get
			{
				return _religionID;
			}
			set
			{
				_religionID = value;
			}
		}

		public Int32 JdateReligionID
		{
			get
			{
				return _JdateReligionID;
			}
			set
			{
				_JdateReligionID = value;
			}
		}

		public Int32 EthnicityID
		{
			get
			{
				return _ethnicityID;
			}
			set
			{
				_ethnicityID = value;
			}
		}
		public Int32 JdateEthnicityID
		{
			get
			{
				return _JdateEthnicityID;
			}
			set
			{
				_JdateEthnicityID = value;
			}
		}
		public Int32 MaritalStatusID
		{
			get
			{
				return _maritalStatusID;
			}
			set
			{
				_maritalStatusID = value;
			}
		}

		public Int32 ChildrenCount
		{
			get
			{
				return _childrenCount;
			}
			set
			{
				_childrenCount = value;
			}
		}

		public Int32 CustodyID
		{
			get
			{
				return _custodyID;
			}
			set
			{
				_custodyID = value;
			}
		}
		public Boolean IsPayingMember
		{
			get
			{
				return _isPayingMember;
			}
			set
			{
				_isPayingMember = value;
			}
		}
		public Int32 GlobalStatusMask
		{
			get
			{
				return _globalStatusMask;
			}
			set
			{
				_globalStatusMask = value;
			}
		}
		public Boolean HasApprovedPhotos
		{
			get
			{
				return _hasApprovedPhotos;
			}
			set
			{
				_hasApprovedPhotos = value;
			}
		}

		public Int32 EntertainmentLocation
		{
			get
			{
				return _entertainmentLocation;
			}
			set
			{
				_entertainmentLocation = value;
			}
		}

		public Int32 LeisureActivity
		{
			get
			{
				return _leisureActivity;
			}
			set
			{
				_leisureActivity = value;
			}
		}

		public Int32 Cuisine
		{
			get
			{
				return _cuisine;
			}
			set
			{
				_cuisine = value;
			}
		}

		public Int32 Music
		{
			get
			{
				return _music;
			}
			set
			{
				_music = value;
			}
		}

		public Int32 Reading
		{
			get
			{
				return _reading;
			}
			set
			{
				_reading = value;
			}
		}

		public Int32 PhysicalActivity
		{
			get
			{
				return _physicalActivity;
			}
			set
			{
				_physicalActivity = value;
			}
		}

		public Int32 Pets
		{
			get
			{
				return _pets;
			}
			set
			{
				_pets = value;
			}
		}


		#endregion

		#region Constructors
		/// <summary>
		/// Needed for deserialization
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MiniprofileInfo(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}
		public MiniprofileInfo()
		{
		}
		/// <summary>
		/// For populating miniprofile information
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="userName"></param>
		/// <param name="birthDate"></param>
		/// <param name="regionID"></param>
		/// <param name="thumbNailWebPath"></param>
		/// <param name="aboutMe"></param>
		public MiniprofileInfo(Int32 memberID, String userName, DateTime birthDate, Int32 regionID, String thumbNailWebPath, String aboutMe)
		{
			_memberID = memberID;
			_userName = userName;
			_birthDate = birthDate;
			_regionID = regionID;
			_thumbNailWebPath = thumbNailWebPath;
			_aboutMe = aboutMe;
		}

		/// <summary>
		/// For populating matchmail recipient member. Extra attributes are used for rendering banner ads.
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="userName"></param>
		/// <param name="birthDate"></param>
		/// <param name="regionID"></param>
		/// <param name="thumbNailWebPath"></param>
		/// <param name="aboutMe"></param>
		/// <param name="genderMask"></param>
		/// <param name="religionID"></param>
		/// <param name="JdateReligionID"></param>
		/// <param name="ethnicityID"></param>
		/// <param name="JDateEthnicityID"></param>
		/// <param name="maritalStatusID"></param>
		/// <param name="childrenCount"></param>
		/// <param name="custodyID"></param>
		/// <param name="isPayingMember"></param>
		/// <param name="globalStatusMask"></param>
		/// <param name="hasApprovedPhotos"></param>
		public MiniprofileInfo(Int32 memberID, String userName, DateTime birthDate, Int32 regionID, String thumbNailWebPath, String aboutMe, 
			Int32 genderMask, Int32 religionID, Int32 JdateReligionID, Int32 ethnicityID, Int32 JDateEthnicityID, Int32 maritalStatusID, 
			Int32 childrenCount, Int32 custodyID, Boolean isPayingMember, Int32 globalStatusMask, Boolean hasApprovedPhotos,
			Int32 entertainmentLocation, Int32 leisureActivity, Int32 cuisine, Int32 music, Int32 reading, Int32 physicalActivity, 
			Int32 pets)
		{
			_memberID = memberID;
			_userName = userName;
			_birthDate = birthDate;
			_regionID = regionID;
			_thumbNailWebPath = thumbNailWebPath;
			_aboutMe = aboutMe;

			_genderMask = genderMask;
			_religionID = religionID;
			_JdateReligionID = JdateReligionID;
			_ethnicityID = ethnicityID;
			_JdateEthnicityID = JDateEthnicityID;
			_maritalStatusID = maritalStatusID;
			_childrenCount = childrenCount;
			_custodyID = custodyID;
			_isPayingMember = isPayingMember;
			_globalStatusMask = globalStatusMask;
			_hasApprovedPhotos = hasApprovedPhotos;

			_entertainmentLocation = entertainmentLocation;
			_leisureActivity = leisureActivity ;
			_cuisine = cuisine;
			_music = music;
			_reading = reading;
			_physicalActivity = physicalActivity;
			_pets = pets;
		}
	

		#endregion

		#region IByteSerializable Members

		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new BinaryReader(ms);

			byte version = br.ReadByte();

			switch (version)
			{
				case VERSION_001:
					_aboutMe = br.ReadString();
					_birthDate = new DateTime(br.ReadInt64());
					_genderMask = br.ReadInt32();
					_memberID = br.ReadInt32();
					_regionID = br.ReadInt32();
					_thumbNailWebPath = br.ReadString();
					_userName = br.ReadString();

					_religionID = br.ReadInt32();
					_JdateReligionID = br.ReadInt32();
					_ethnicityID = br.ReadInt32();
					_JdateEthnicityID = br.ReadInt32();
					_maritalStatusID = br.ReadInt32();
					_childrenCount = br.ReadInt32();
					_custodyID = br.ReadInt32();
					_isPayingMember = br.ReadBoolean();
					_globalStatusMask = br.ReadInt32();
					_hasApprovedPhotos = br.ReadBoolean();
					_entertainmentLocation = br.ReadInt32();
					_leisureActivity = br.ReadInt32();
					_cuisine = br.ReadInt32();
					_music = br.ReadInt32();
					_reading = br.ReadInt32();
					_physicalActivity = br.ReadInt32();
					_pets = br.ReadInt32();

					break;

				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}

		public byte[] ToByteArray()
		{
			
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			byte version = VERSION_001;

			bw.Write(version);
			writeString(bw, _aboutMe);
			bw.Write(_birthDate.Ticks);
			bw.Write(_genderMask);
			bw.Write(_memberID);
			bw.Write(_regionID);
			writeString(bw, _thumbNailWebPath);
			writeString(bw, _userName);

			bw.Write(_religionID);
			bw.Write(_JdateReligionID);
			bw.Write(_ethnicityID);
			bw.Write(_JdateEthnicityID);
			bw.Write(_maritalStatusID);
			bw.Write(_childrenCount);
			bw.Write(_custodyID);
			bw.Write(_isPayingMember);
			bw.Write(_globalStatusMask);
			bw.Write(_hasApprovedPhotos);
			bw.Write(_entertainmentLocation);
			bw.Write(_leisureActivity);
			bw.Write(_cuisine);
			bw.Write(_music);
			bw.Write(_reading);
			bw.Write(_physicalActivity);
			bw.Write(_pets);

			return ms.ToArray();
		}

		private static void writeString(BinaryWriter bw, string s)
		{
			if (s == null)
			{
				bw.Write(string.Empty);
			}
			else
			{
				bw.Write(s);
			}
		}

		#endregion

		#region ISerializable Members

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", this.ToByteArray());
		}

		#endregion
	}
}
