using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.BusinessLogic;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;
using Matchnet.ExternalMail.ValueObjects.StormpostSoap;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.ExternalMail.Harness
{
	/// <summary>
	/// Summary description for TestMail.
	/// </summary>
	public class TestMail : Form
	{
		static void Main()
		{
			Application.Run(new TestMail());
		}
		

		#region private variables

		private Button submitEmail;
		private CheckBox chkInternalMail;
		private Label lblEmailTypes;
		private CheckBox chkVerifyEmail;
		private CheckBox chkSendMemberToFriend;
		private CheckBox chkSendPassword;
		private CheckBox chkPhotoApproval;
		private CheckBox chkRegistrationVerification;
		private CheckBox chkMatchMail;
		private CheckBox chkSendMutualYes;
		private CheckBox chkViralMail;
		private CheckBox chkPhotoRejection;
		private CheckBox chkSendAll;
		private CheckedListBox cblBrands;
		private TabControl tabControl1;
		private TabPage tpSendTest;
		private TabPage tpMetrics;
		private Button btnInstallCounters;
		private Button btnUninstallCounters;
		private Button btnInitializeCounters;
		private CheckBox UseBL;
		private CheckBox chkHotListedAlert;
		private CheckBox chkSubscriptionConfirmation;
		private Button ByteSerializationTest;
		private TabPage tpStormPostSoap;
		private Button btnSOAPImportBL;
		private TextBox txtOutput;
		private TextBox soapEmail;
		private TextBox soapMemberID;
		private CheckedListBox clbCommunityID;
		private Button btnSOAPSend;
		private GroupBox groupBox1;
		private Button button2;
		private Button StartQueueProcessing;
		private Button SendImportSendQueues;

        private Matchnet.ExternalMail.BusinessLogic.QueueProcessing.QueueProcessor _queueProcessor;
		private Label label1;
		private Label label2;
		private Button Repeat100Times;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		#endregion

		#region Class methods/setup

		public TestMail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			BindData();

			ExternalMailBL.PerfCounterUninstall();
			ExternalMailBL.PerfCounterInstall();
		}

		private void BindData()
		{
			/*
			Content.ValueObjects.BrandConfig.Sites siteList = BrandConfigSA.Instance.GetSites();

			foreach(Site site in siteList)
			{
				cblBrands.Items.Add(site.Name);
			}
			*/
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#endregion

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.submitEmail = new System.Windows.Forms.Button();
			this.chkInternalMail = new System.Windows.Forms.CheckBox();
			this.lblEmailTypes = new System.Windows.Forms.Label();
			this.chkVerifyEmail = new System.Windows.Forms.CheckBox();
			this.chkSendMemberToFriend = new System.Windows.Forms.CheckBox();
			this.chkSendPassword = new System.Windows.Forms.CheckBox();
			this.chkPhotoApproval = new System.Windows.Forms.CheckBox();
			this.chkRegistrationVerification = new System.Windows.Forms.CheckBox();
			this.chkMatchMail = new System.Windows.Forms.CheckBox();
			this.chkSendMutualYes = new System.Windows.Forms.CheckBox();
			this.chkViralMail = new System.Windows.Forms.CheckBox();
			this.chkPhotoRejection = new System.Windows.Forms.CheckBox();
			this.chkSendAll = new System.Windows.Forms.CheckBox();
			this.cblBrands = new System.Windows.Forms.CheckedListBox();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tpSendTest = new System.Windows.Forms.TabPage();
			this.ByteSerializationTest = new System.Windows.Forms.Button();
			this.chkSubscriptionConfirmation = new System.Windows.Forms.CheckBox();
			this.chkHotListedAlert = new System.Windows.Forms.CheckBox();
			this.UseBL = new System.Windows.Forms.CheckBox();
			this.tpMetrics = new System.Windows.Forms.TabPage();
			this.btnInitializeCounters = new System.Windows.Forms.Button();
			this.btnUninstallCounters = new System.Windows.Forms.Button();
			this.btnInstallCounters = new System.Windows.Forms.Button();
			this.tpStormPostSoap = new System.Windows.Forms.TabPage();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.Repeat100Times = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.StartQueueProcessing = new System.Windows.Forms.Button();
			this.SendImportSendQueues = new System.Windows.Forms.Button();
			this.btnSOAPSend = new System.Windows.Forms.Button();
			this.clbCommunityID = new System.Windows.Forms.CheckedListBox();
			this.soapMemberID = new System.Windows.Forms.TextBox();
			this.soapEmail = new System.Windows.Forms.TextBox();
			this.txtOutput = new System.Windows.Forms.TextBox();
			this.btnSOAPImportBL = new System.Windows.Forms.Button();
			this.tabControl1.SuspendLayout();
			this.tpSendTest.SuspendLayout();
			this.tpMetrics.SuspendLayout();
			this.tpStormPostSoap.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// submitEmail
			// 
			this.submitEmail.Location = new System.Drawing.Point(8, 368);
			this.submitEmail.Name = "submitEmail";
			this.submitEmail.Size = new System.Drawing.Size(144, 24);
			this.submitEmail.TabIndex = 0;
			this.submitEmail.Text = "Send Email";
			this.submitEmail.Click += new System.EventHandler(this.submitEmail_Click);
			// 
			// chkInternalMail
			// 
			this.chkInternalMail.Location = new System.Drawing.Point(8, 64);
			this.chkInternalMail.Name = "chkInternalMail";
			this.chkInternalMail.Size = new System.Drawing.Size(176, 24);
			this.chkInternalMail.TabIndex = 1;
			this.chkInternalMail.Text = "Internal Mail Notification";
			// 
			// lblEmailTypes
			// 
			this.lblEmailTypes.Location = new System.Drawing.Point(8, 8);
			this.lblEmailTypes.Name = "lblEmailTypes";
			this.lblEmailTypes.Size = new System.Drawing.Size(192, 23);
			this.lblEmailTypes.TabIndex = 2;
			this.lblEmailTypes.Text = "Email Types";
			// 
			// chkVerifyEmail
			// 
			this.chkVerifyEmail.Location = new System.Drawing.Point(8, 88);
			this.chkVerifyEmail.Name = "chkVerifyEmail";
			this.chkVerifyEmail.Size = new System.Drawing.Size(176, 24);
			this.chkVerifyEmail.TabIndex = 1;
			this.chkVerifyEmail.Text = "Verify Email";
			// 
			// chkSendMemberToFriend
			// 
			this.chkSendMemberToFriend.Location = new System.Drawing.Point(8, 112);
			this.chkSendMemberToFriend.Name = "chkSendMemberToFriend";
			this.chkSendMemberToFriend.Size = new System.Drawing.Size(176, 24);
			this.chkSendMemberToFriend.TabIndex = 1;
			this.chkSendMemberToFriend.Text = "Send Member To Friend";
			// 
			// chkSendPassword
			// 
			this.chkSendPassword.Location = new System.Drawing.Point(8, 136);
			this.chkSendPassword.Name = "chkSendPassword";
			this.chkSendPassword.Size = new System.Drawing.Size(176, 24);
			this.chkSendPassword.TabIndex = 1;
			this.chkSendPassword.Text = "Send Password";
			// 
			// chkPhotoApproval
			// 
			this.chkPhotoApproval.Location = new System.Drawing.Point(8, 160);
			this.chkPhotoApproval.Name = "chkPhotoApproval";
			this.chkPhotoApproval.Size = new System.Drawing.Size(176, 24);
			this.chkPhotoApproval.TabIndex = 1;
			this.chkPhotoApproval.Text = "Photo Approval";
			// 
			// chkRegistrationVerification
			// 
			this.chkRegistrationVerification.Location = new System.Drawing.Point(8, 184);
			this.chkRegistrationVerification.Name = "chkRegistrationVerification";
			this.chkRegistrationVerification.Size = new System.Drawing.Size(176, 24);
			this.chkRegistrationVerification.TabIndex = 1;
			this.chkRegistrationVerification.Text = "Registration Verification";
			// 
			// chkMatchMail
			// 
			this.chkMatchMail.Location = new System.Drawing.Point(8, 208);
			this.chkMatchMail.Name = "chkMatchMail";
			this.chkMatchMail.Size = new System.Drawing.Size(176, 24);
			this.chkMatchMail.TabIndex = 1;
			this.chkMatchMail.Text = "Match Mail";
			// 
			// chkSendMutualYes
			// 
			this.chkSendMutualYes.Location = new System.Drawing.Point(8, 232);
			this.chkSendMutualYes.Name = "chkSendMutualYes";
			this.chkSendMutualYes.Size = new System.Drawing.Size(176, 24);
			this.chkSendMutualYes.TabIndex = 1;
			this.chkSendMutualYes.Text = "Send Mutual Yes";
			// 
			// chkViralMail
			// 
			this.chkViralMail.Location = new System.Drawing.Point(8, 256);
			this.chkViralMail.Name = "chkViralMail";
			this.chkViralMail.Size = new System.Drawing.Size(176, 24);
			this.chkViralMail.TabIndex = 1;
			this.chkViralMail.Text = "Viral Mail";
			// 
			// chkPhotoRejection
			// 
			this.chkPhotoRejection.Location = new System.Drawing.Point(8, 280);
			this.chkPhotoRejection.Name = "chkPhotoRejection";
			this.chkPhotoRejection.Size = new System.Drawing.Size(176, 24);
			this.chkPhotoRejection.TabIndex = 1;
			this.chkPhotoRejection.Text = "Photo Rejection";
			// 
			// chkSendAll
			// 
			this.chkSendAll.Location = new System.Drawing.Point(8, 32);
			this.chkSendAll.Name = "chkSendAll";
			this.chkSendAll.Size = new System.Drawing.Size(176, 24);
			this.chkSendAll.TabIndex = 1;
			this.chkSendAll.Text = "SEND ALL";
			// 
			// cblBrands
			// 
			this.cblBrands.Location = new System.Drawing.Point(192, 56);
			this.cblBrands.Name = "cblBrands";
			this.cblBrands.Size = new System.Drawing.Size(136, 244);
			this.cblBrands.TabIndex = 3;
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tpSendTest);
			this.tabControl1.Controls.Add(this.tpMetrics);
			this.tabControl1.Controls.Add(this.tpStormPostSoap);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(616, 493);
			this.tabControl1.TabIndex = 4;
			// 
			// tpSendTest
			// 
			this.tpSendTest.Controls.Add(this.ByteSerializationTest);
			this.tpSendTest.Controls.Add(this.chkSubscriptionConfirmation);
			this.tpSendTest.Controls.Add(this.chkHotListedAlert);
			this.tpSendTest.Controls.Add(this.UseBL);
			this.tpSendTest.Controls.Add(this.chkSendMemberToFriend);
			this.tpSendTest.Controls.Add(this.chkVerifyEmail);
			this.tpSendTest.Controls.Add(this.lblEmailTypes);
			this.tpSendTest.Controls.Add(this.chkInternalMail);
			this.tpSendTest.Controls.Add(this.cblBrands);
			this.tpSendTest.Controls.Add(this.chkSendAll);
			this.tpSendTest.Controls.Add(this.chkPhotoRejection);
			this.tpSendTest.Controls.Add(this.chkViralMail);
			this.tpSendTest.Controls.Add(this.chkSendMutualYes);
			this.tpSendTest.Controls.Add(this.chkMatchMail);
			this.tpSendTest.Controls.Add(this.chkRegistrationVerification);
			this.tpSendTest.Controls.Add(this.chkPhotoApproval);
			this.tpSendTest.Controls.Add(this.chkSendPassword);
			this.tpSendTest.Controls.Add(this.submitEmail);
			this.tpSendTest.Location = new System.Drawing.Point(4, 22);
			this.tpSendTest.Name = "tpSendTest";
			this.tpSendTest.Size = new System.Drawing.Size(608, 467);
			this.tpSendTest.TabIndex = 0;
			this.tpSendTest.Text = "Send Test";
			this.tpSendTest.Click += new System.EventHandler(this.tpSendTest_Click);
			// 
			// ByteSerializationTest
			// 
			this.ByteSerializationTest.Location = new System.Drawing.Point(8, 408);
			this.ByteSerializationTest.Name = "ByteSerializationTest";
			this.ByteSerializationTest.Size = new System.Drawing.Size(144, 23);
			this.ByteSerializationTest.TabIndex = 7;
			this.ByteSerializationTest.Text = "Byte Serialization Test";
			this.ByteSerializationTest.Click += new System.EventHandler(this.ByteSerializationTest_Click);
			// 
			// chkSubscriptionConfirmation
			// 
			this.chkSubscriptionConfirmation.Location = new System.Drawing.Point(8, 328);
			this.chkSubscriptionConfirmation.Name = "chkSubscriptionConfirmation";
			this.chkSubscriptionConfirmation.Size = new System.Drawing.Size(176, 24);
			this.chkSubscriptionConfirmation.TabIndex = 6;
			this.chkSubscriptionConfirmation.Text = "Subscription Confirmation";
			// 
			// chkHotListedAlert
			// 
			this.chkHotListedAlert.Location = new System.Drawing.Point(8, 304);
			this.chkHotListedAlert.Name = "chkHotListedAlert";
			this.chkHotListedAlert.Size = new System.Drawing.Size(176, 24);
			this.chkHotListedAlert.TabIndex = 5;
			this.chkHotListedAlert.Text = "Hotlisted Alert";
			// 
			// UseBL
			// 
			this.UseBL.Location = new System.Drawing.Point(168, 368);
			this.UseBL.Name = "UseBL";
			this.UseBL.TabIndex = 4;
			this.UseBL.Text = "Use BL";
			// 
			// tpMetrics
			// 
			this.tpMetrics.Controls.Add(this.btnInitializeCounters);
			this.tpMetrics.Controls.Add(this.btnUninstallCounters);
			this.tpMetrics.Controls.Add(this.btnInstallCounters);
			this.tpMetrics.Location = new System.Drawing.Point(4, 22);
			this.tpMetrics.Name = "tpMetrics";
			this.tpMetrics.Size = new System.Drawing.Size(608, 467);
			this.tpMetrics.TabIndex = 1;
			this.tpMetrics.Text = "Metrics / Coutners";
			// 
			// btnInitializeCounters
			// 
			this.btnInitializeCounters.Location = new System.Drawing.Point(16, 80);
			this.btnInitializeCounters.Name = "btnInitializeCounters";
			this.btnInitializeCounters.Size = new System.Drawing.Size(152, 23);
			this.btnInitializeCounters.TabIndex = 2;
			this.btnInitializeCounters.Text = "Initialize Counters";
			this.btnInitializeCounters.Click += new System.EventHandler(this.btnInitializeCounters_Click);
			// 
			// btnUninstallCounters
			// 
			this.btnUninstallCounters.Location = new System.Drawing.Point(16, 48);
			this.btnUninstallCounters.Name = "btnUninstallCounters";
			this.btnUninstallCounters.Size = new System.Drawing.Size(152, 23);
			this.btnUninstallCounters.TabIndex = 1;
			this.btnUninstallCounters.Text = "Uninstall Coutners";
			this.btnUninstallCounters.Click += new System.EventHandler(this.btnUninstallCounters_Click);
			// 
			// btnInstallCounters
			// 
			this.btnInstallCounters.Location = new System.Drawing.Point(16, 16);
			this.btnInstallCounters.Name = "btnInstallCounters";
			this.btnInstallCounters.Size = new System.Drawing.Size(152, 23);
			this.btnInstallCounters.TabIndex = 0;
			this.btnInstallCounters.Text = "Install Coutners";
			this.btnInstallCounters.Click += new System.EventHandler(this.btnInstallCounters_Click);
			// 
			// tpStormPostSoap
			// 
			this.tpStormPostSoap.Controls.Add(this.label2);
			this.tpStormPostSoap.Controls.Add(this.label1);
			this.tpStormPostSoap.Controls.Add(this.groupBox1);
			this.tpStormPostSoap.Controls.Add(this.btnSOAPSend);
			this.tpStormPostSoap.Controls.Add(this.clbCommunityID);
			this.tpStormPostSoap.Controls.Add(this.soapMemberID);
			this.tpStormPostSoap.Controls.Add(this.soapEmail);
			this.tpStormPostSoap.Controls.Add(this.txtOutput);
			this.tpStormPostSoap.Controls.Add(this.btnSOAPImportBL);
			this.tpStormPostSoap.Location = new System.Drawing.Point(4, 22);
			this.tpStormPostSoap.Name = "tpStormPostSoap";
			this.tpStormPostSoap.Size = new System.Drawing.Size(608, 467);
			this.tpStormPostSoap.TabIndex = 2;
			this.tpStormPostSoap.Text = "StormPost SOAP";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(240, 8);
			this.label2.Name = "label2";
			this.label2.TabIndex = 11;
			this.label2.Text = "MemberID:";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 8);
			this.label1.Name = "label1";
			this.label1.TabIndex = 10;
			this.label1.Text = "EmailAddress:";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.Repeat100Times);
			this.groupBox1.Controls.Add(this.button2);
			this.groupBox1.Controls.Add(this.StartQueueProcessing);
			this.groupBox1.Controls.Add(this.SendImportSendQueues);
			this.groupBox1.Location = new System.Drawing.Point(8, 112);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(592, 104);
			this.groupBox1.TabIndex = 9;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "SOAP BL Full Process Test";
			// 
			// Repeat100Times
			// 
			this.Repeat100Times.Location = new System.Drawing.Point(24, 64);
			this.Repeat100Times.Name = "Repeat100Times";
			this.Repeat100Times.Size = new System.Drawing.Size(112, 23);
			this.Repeat100Times.TabIndex = 12;
			this.Repeat100Times.Text = "Repeat 100 times";
			this.Repeat100Times.Click += new System.EventHandler(this.Repeat100Times_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(424, 64);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(144, 23);
			this.button2.TabIndex = 11;
			this.button2.Text = "End Queue Processing";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// StartQueueProcessing
			// 
			this.StartQueueProcessing.Location = new System.Drawing.Point(424, 32);
			this.StartQueueProcessing.Name = "StartQueueProcessing";
			this.StartQueueProcessing.Size = new System.Drawing.Size(144, 23);
			this.StartQueueProcessing.TabIndex = 10;
			this.StartQueueProcessing.Text = "Start Queue Processing";
			this.StartQueueProcessing.Click += new System.EventHandler(this.StartQueueProcessing_Click);
			// 
			// SendImportSendQueues
			// 
			this.SendImportSendQueues.Location = new System.Drawing.Point(24, 32);
			this.SendImportSendQueues.Name = "SendImportSendQueues";
			this.SendImportSendQueues.Size = new System.Drawing.Size(160, 23);
			this.SendImportSendQueues.TabIndex = 9;
			this.SendImportSendQueues.Text = "Send Import/Send Queues";
			this.SendImportSendQueues.Click += new System.EventHandler(this.SendImportSendQueues_Click);
			// 
			// btnSOAPSend
			// 
			this.btnSOAPSend.Location = new System.Drawing.Point(16, 48);
			this.btnSOAPSend.Name = "btnSOAPSend";
			this.btnSOAPSend.Size = new System.Drawing.Size(144, 23);
			this.btnSOAPSend.TabIndex = 5;
			this.btnSOAPSend.Text = "Test Direct SOAP Send";
			this.btnSOAPSend.Click += new System.EventHandler(this.btnSOAPSend_Click);
			// 
			// clbCommunityID
			// 
			this.clbCommunityID.CheckOnClick = true;
			this.clbCommunityID.Items.AddRange(new object[]
				{
					"1",
					"2",
					"3",
					"9",
					"10",
					"12"
				});
			this.clbCommunityID.Location = new System.Drawing.Point(328, 48);
			this.clbCommunityID.MultiColumn = true;
			this.clbCommunityID.Name = "clbCommunityID";
			this.clbCommunityID.Size = new System.Drawing.Size(264, 49);
			this.clbCommunityID.TabIndex = 4;
			// 
			// soapMemberID
			// 
			this.soapMemberID.Location = new System.Drawing.Point(352, 8);
			this.soapMemberID.Name = "soapMemberID";
			this.soapMemberID.TabIndex = 3;
			this.soapMemberID.Text = "16000206";
			// 
			// soapEmail
			// 
			this.soapEmail.Location = new System.Drawing.Point(128, 8);
			this.soapEmail.Name = "soapEmail";
			this.soapEmail.TabIndex = 2;
			this.soapEmail.Text = "wlee@spark.net";
			// 
			// txtOutput
			// 
			this.txtOutput.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
				| System.Windows.Forms.AnchorStyles.Left)
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtOutput.Location = new System.Drawing.Point(8, 224);
			this.txtOutput.Multiline = true;
			this.txtOutput.Name = "txtOutput";
			this.txtOutput.Size = new System.Drawing.Size(592, 232);
			this.txtOutput.TabIndex = 1;
			this.txtOutput.Text = "";
			// 
			// btnSOAPImportBL
			// 
			this.btnSOAPImportBL.Location = new System.Drawing.Point(168, 48);
			this.btnSOAPImportBL.Name = "btnSOAPImportBL";
			this.btnSOAPImportBL.Size = new System.Drawing.Size(144, 23);
			this.btnSOAPImportBL.TabIndex = 0;
			this.btnSOAPImportBL.Text = "Test Direct SOAP Import";
			this.btnSOAPImportBL.Click += new System.EventHandler(this.btnSOAPImportBL_Click);
			// 
			// TestMail
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(616, 493);
			this.Controls.Add(this.tabControl1);
			this.Name = "TestMail";
			this.Text = "TestMail";
			this.tabControl1.ResumeLayout(false);
			this.tpSendTest.ResumeLayout(false);
			this.tpMetrics.ResumeLayout(false);
			this.tpStormPostSoap.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private void submitEmail_Click(object sender, EventArgs e)
		{
			ConsoleDriver console = new ConsoleDriver(Console.Out);

			bool allchecked = this.chkSendAll.Checked;

			console.testEmailVerification = this.chkVerifyEmail.Checked || allchecked;
			console.testInternalMailNotification = this.chkInternalMail.Checked || allchecked;
			console.testMatchMail = this.chkMatchMail.Checked || allchecked;
			console.testPhotoApproval = this.chkPhotoApproval.Checked || allchecked;
			console.testRegistrationVerification = this.chkRegistrationVerification.Checked || allchecked;
			console.testSendMemberToFriend = this.chkSendMemberToFriend.Checked || allchecked;
			console.testSendMutualYes = this.chkSendMutualYes.Checked || allchecked;
			console.testSendPassword = this.chkSendPassword.Checked || allchecked;
			console.testViralMail = this.chkViralMail.Checked || allchecked;
			console.testPhotoRejection = this.chkPhotoRejection.Checked || allchecked;
			console.testHotListedAlert = this.chkHotListedAlert.Checked || allchecked;
			console.testSubscriptionConfirmation = this.chkSubscriptionConfirmation.Checked || allchecked;

			console.DoRun(UseBL.Checked);
		}

		

		private void btnInstallCounters_Click(object sender, EventArgs e)
		{
			Metrics.InstallPerformanceCounters();
		}

		private void btnUninstallCounters_Click(object sender, EventArgs e)
		{
			Metrics.InstallPerformanceCounters();
		}

		private void btnInitializeCounters_Click(object sender, EventArgs e)
		{
			Sites sites = BrandConfigSA.Instance.GetSites();
			string[] instances = new string[sites.Count];
			for (int i = 0; i < sites.Count; i++)
			{
				instances[i] = sites[i].Name;
			}

			Metrics.Instance.InitializeCounters(instances);

			// now set some val's
			Metrics.Instance.ImpulseConsumed("JDate.co.il").Increment();
			Metrics.Instance.ImpulsesConsumedPerSecond("Date.ca").Increment();
			Metrics.Instance.ImpulsesFailed("Spark.com").Increment();


		}

		private void tpSendTest_Click(object sender, EventArgs e)
		{
		}

		private void ByteSerializationTest_Click(object sender, EventArgs e)
		{
			/* Subscription Confirmation
			Matchnet.ExternalMail.ValueObjects.Impulse.SubscriptionConfirmationImpulse impulse = new Matchnet.ExternalMail.ValueObjects.Impulse.SubscriptionConfirmationImpulse(
						16000206,
						101,
						1001,
						"won",
						"wlee@spark.net",
						"Won",
						"Lee",
						"690 Catalina",
						"Los Angeles",
						"CA",
						"90005",
						DateTime.Today,
						64,
						1,
						19.99m,
						3,
						Matchnet.DurationType.Month,
						15.99m,
						3,
						Matchnet.DurationType.Month,
						"3456",
						1,
						"767676767633");
			*/

			/* MatchMail */

			Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(16000206, MemberLoadFlags.None);
			Brand brand = BrandConfigSA.Instance.GetBrandByID(1001);

			MiniprofileInfo recipientMiniprofileInfo = new MiniprofileInfo(
				16000206,
				"juan",
				DateTime.Parse("08/03/1975"),
				203,
				@"http://dev.americansingles.com/Photo1000/2006/01/11/16/120005172.jpg?siteID=101",
				"about me  myself and i",
				member.GetAttributeInt(brand, "GenderMask", Constants.NULL_INT),
				member.GetAttributeInt(brand, "Religion", Constants.NULL_INT),
				member.GetAttributeInt(brand, "JdateReligion", Constants.NULL_INT),
				member.GetAttributeInt(brand, "Ethnicity", Constants.NULL_INT),
				member.GetAttributeInt(brand, "JDateEthnicity", Constants.NULL_INT),
				member.GetAttributeInt(brand, "MaritalStatus", Constants.NULL_INT),
				member.GetAttributeInt(brand, "ChildrenCount", Constants.NULL_INT),
				member.GetAttributeInt(brand, "Custody", Constants.NULL_INT),
				member.IsPayingMember(brand.Site.SiteID),
				member.GetAttributeInt(brand, "GlobalStatusMask", Constants.NULL_INT),
				member.HasApprovedPhoto(brand.Site.Community.CommunityID),0,0,0,0,0,0,0);

			MiniprofileInfoCollection minis = new MiniprofileInfoCollection();
			minis.Add(recipientMiniprofileInfo);
			minis.Add(recipientMiniprofileInfo);
			minis.Add(recipientMiniprofileInfo);
			minis.Add(recipientMiniprofileInfo);
			minis.Add(recipientMiniprofileInfo);
			minis.Add(recipientMiniprofileInfo);

			MatchMailImpulse impulse = new MatchMailImpulse(
				1001,
				"wlee@spark.net",
				recipientMiniprofileInfo,
				minis);

			impulse.FromByteArray(impulse.ToByteArray());

			Trace.WriteLine("done");

		}

		private void btnSOAPImportBL_Click(object sender, EventArgs e)
		{
			int communityID = Convert.ToInt32(clbCommunityID.SelectedItem);
			StormPostSoapBL.Instance.Import(
				ImportTemplateType.Minimal,
				communityID,
				new string[] {soapEmail.Text, soapMemberID.Text}
				);

			txtOutput.Text += "done" + Environment.NewLine;
		}

		private void btnSOAPSend_Click(object sender, EventArgs e)
		{
			txtOutput.Text += DateTime.Now.ToString("s") + Environment.NewLine;
			foreach (int i in new int[] {101, 102, 103, 112, 13, 15, 1840, 1934, 4, 5, 6, 7})
			{
				txtOutput.Text += DateTime.Now.ToString("s") + Environment.NewLine;
				///Activation letter takes NAME, VERIFICATION CODE, PASSWORD

				//StormPostSoapBL.Instance.Send(SendTemplateType.SendTemplate_Activation, i, "nuri@spark.net","abuarba","CODE_" + i.ToString(), "passwd_" + i.ToString()); 
				StormPostSoapBL.Instance.Send(SendTemplateType.SendTemplate_Activation, i, "wlee@spark.net", "CODE_" + i.ToString());
			}
		}

		private void SendImportSendQueues_Click(object sender, EventArgs e)
		{
			sendImportSendQueues();
		}

		private void sendImportSendQueues()
		{
			foreach (object community  in clbCommunityID.CheckedItems)
			{
				int communityID = Convert.ToInt32(community);
				int brandID = Constants.NULL_INT;
				int siteID = Constants.NULL_INT;

				switch (communityID)
				{
					case 1:
						brandID = 1001;
						siteID = 101;
						break;
					case 3:
						brandID = 1003;
						siteID = 103;
						break;
					case 9:
						brandID = 1009;
						break;
					case 10:
						brandID = 1015;
						break;
					case 12:
						brandID = 10012;
						siteID = 112;
						break;
				}

				RegVerificationImpulse regImpulse =
					new RegVerificationImpulse(Convert.ToInt32(soapMemberID.Text), brandID);
				ExternalMailBL.Instance.Enqueue(regImpulse);
				txtOutput.Text += DateTime.Now.ToString("s") + " Enqueued Registration Verification Email Impulse for Community:" + communityID + Environment.NewLine;

				PasswordMailImpulse passwordImpulse =
					new PasswordMailImpulse(brandID, soapEmail.Text);
				ExternalMailBL.Instance.Enqueue(passwordImpulse);
				txtOutput.Text += DateTime.Now.ToString("s") + " Enqueued Password Email Impulse for Community:" + communityID + Environment.NewLine;

				EmailVerificationImpulse emailverificationImpulse =
					new EmailVerificationImpulse(Convert.ToInt32(soapMemberID.Text), brandID);
				ExternalMailBL.Instance.Enqueue(emailverificationImpulse);
				txtOutput.Text += DateTime.Now.ToString("s") + " Enqueued Email Verification(Changed) Impulse for Community:" + communityID + Environment.NewLine;

				MessageBoardInstantNotificationImpulse messageBoardInstantNotificationImpulse =
					new MessageBoardInstantNotificationImpulse(brandID, siteID, Convert.ToInt32(soapMemberID.Text), "BoardName", "TopicSubject", "ReplyURL", 1, "UnsubscribeURL");
				ExternalMailBL.Instance.Enqueue(messageBoardInstantNotificationImpulse);
				txtOutput.Text += DateTime.Now.ToString("s") + " Enqueued MessageBoardInstantNotificationImpulse) Impulse for Community:" + communityID + Environment.NewLine;

				MessageBoardDailyUpdatesImpulse messageBoardDailyUpdatesImpulse =
					new MessageBoardDailyUpdatesImpulse(brandID, siteID, Convert.ToInt32(soapMemberID.Text), "BoardName", "TopicSubject", 1, 2, 3, 4, "UnsubscribeURL");
				ExternalMailBL.Instance.Enqueue(messageBoardDailyUpdatesImpulse);
				txtOutput.Text += DateTime.Now.ToString("s") + " Enqueued MessageBoardDailyUpdatesImpulse Impulse for Community:" + communityID + Environment.NewLine;

				RenewalFailureNotificationImpulse renewalFailureNotificationImpulse = new RenewalFailureNotificationImpulse(siteID, "Wonsta", "wlee@spark.net");
				ExternalMailBL.Instance.Enqueue(renewalFailureNotificationImpulse);
				txtOutput.Text += DateTime.Now.ToString("s") + " Enqueued RenewalFailureNotification Impulse for Community:" + communityID + Environment.NewLine;
			}
		}

		private void StartQueueProcessing_Click(object sender, EventArgs e)
		{
			Metrics.InstallPerformanceCounters();

			_queueProcessor = new Matchnet.ExternalMail.BusinessLogic.QueueProcessing.QueueProcessor();
			_queueProcessor.Start();

			txtOutput.Text += DateTime.Now.ToString("s") + " Started processor threads." + Environment.NewLine;
		}

		private void button2_Click(object sender, EventArgs e)
		{
			_queueProcessor.Stop();

			txtOutput.Text += DateTime.Now.ToString("s") + " Ended processor threads." + Environment.NewLine;
		}

		private void Repeat100Times_Click(object sender, EventArgs e)
		{
			for (int i = 0; i < 100; i ++)
			{
				sendImportSendQueues();
			}
		}
	}
}