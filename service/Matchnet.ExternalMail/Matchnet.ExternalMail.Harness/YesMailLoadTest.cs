using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Matchnet.ExternalMail.Harness
{
	/// <summary>
	/// Summary description for YesMailLoadTest.
	/// </summary>
	public class YesMailLoadTest : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBoxNumberOfCalls;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label labelResult1;
		private System.Windows.Forms.CheckBox checkBoxAsync;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public YesMailLoadTest()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label1 = new System.Windows.Forms.Label();
			this.textBoxNumberOfCalls = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.labelResult1 = new System.Windows.Forms.Label();
			this.checkBoxAsync = new System.Windows.Forms.CheckBox();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.checkBoxAsync);
			this.groupBox1.Controls.Add(this.labelResult1);
			this.groupBox1.Controls.Add(this.button1);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.textBoxNumberOfCalls);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(24, 24);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(544, 104);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(24, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(96, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Number of calls:";
			// 
			// textBoxNumberOfCalls
			// 
			this.textBoxNumberOfCalls.Location = new System.Drawing.Point(120, 24);
			this.textBoxNumberOfCalls.Name = "textBoxNumberOfCalls";
			this.textBoxNumberOfCalls.TabIndex = 1;
			this.textBoxNumberOfCalls.Text = "";
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(24, 48);
			this.label2.Name = "label2";
			this.label2.TabIndex = 2;
			this.label2.Text = "Async:";
			// 
			// button1
			// 
			this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.button1.Location = new System.Drawing.Point(224, 24);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 44);
			this.button1.TabIndex = 4;
			this.button1.Text = "Test";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// labelResult1
			// 
			this.labelResult1.BackColor = System.Drawing.Color.White;
			this.labelResult1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.labelResult1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.labelResult1.Location = new System.Drawing.Point(312, 30);
			this.labelResult1.Name = "labelResult1";
			this.labelResult1.Size = new System.Drawing.Size(168, 20);
			this.labelResult1.TabIndex = 5;
			// 
			// checkBoxAsync
			// 
			this.checkBoxAsync.Location = new System.Drawing.Point(120, 48);
			this.checkBoxAsync.Name = "checkBoxAsync";
			this.checkBoxAsync.TabIndex = 6;
			// 
			// YesMailLoadTest
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(712, 207);
			this.Controls.Add(this.groupBox1);
			this.Name = "YesMailLoadTest";
			this.Text = "YesMailLoadTest";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void button1_Click(object sender, System.EventArgs e)
		{
			labelResult1.Text = "Sending.......";
			LoadTest test = new LoadTest();
			test.DoTest(int.Parse(textBoxNumberOfCalls.Text.Trim()), checkBoxAsync.Checked, new TestCallBack(CallbackHandler));
		}

		public void CallbackHandler(object arg)
		{			
			labelResult1.Text = arg.ToString();
		}
	}
}
