using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Web.Hosting;
using System.IO;
using System.Text;

using Matchnet;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.ExternalMail.BusinessLogic;

namespace Matchnet.ExternalMail.Harness
{
	public class PreviewTool : System.Windows.Forms.Form
	{
		private AxSHDocVw.AxWebBrowser axWebBrowser1;
		private System.Windows.Forms.Button btnRender;
		private System.Windows.Forms.ListBox lstEmailType;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox MemberID;
		private System.Windows.Forms.TextBox MemberPlanID;
		private System.Windows.Forms.TextBox PlanDescription;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox RemoteIP;
		private System.Windows.Forms.TextBox Browser;
		private System.Windows.Forms.TextBox FromEmailAddress;
		private System.Windows.Forms.TextBox UserName;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox PhotoID;
		private System.Windows.Forms.TextBox FromHost;
		private System.Windows.Forms.TextBox Email;
		private System.Windows.Forms.TextBox TargetMemberID;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.TextBox MemberIDList;
		private System.Windows.Forms.TextBox SentMemberID;
		private System.Windows.Forms.TextBox UserComment;
		private System.Windows.Forms.TextBox Reason;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.TextBox Message;
		private System.Windows.Forms.TextBox Subject;
		private System.Windows.Forms.TextBox UserEmail;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.TextBox FriendEmail;
		private System.Windows.Forms.TextBox FriendName;
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.ComboBox BrandID;
		private Hashtable impulseMap;

		private static string[] CONTROLS_ALL = { "BrandID", "MemberID", "MemberPlanID", "PlanDescription", "UserName", "FromEmailAddress", "Browser", "RemoteIP", "Reason", "UserComment", "SentMemberID", "MemberIDList", "TargetMemberID", "Email", "FromHost", "PhotoID", "FriendName", "FriendEmail", "UserEmail", "Subject", "Message" };
		private static string[] CONTROLS_CHECK_SUB_CONFIRM = { "BrandID", "MemberID", "MemberPlanID", "PlanDescription" };
		private static string[] CONTROLS_COLLEGE_LUV_PROMO = { "BrandID", "MemberID" };
		private static string[] CONTROLS_CONTACT_US = { "BrandID", "MemberID", "UserName", "FromEmailAddress", "Browser", "RemoteIP", "Reason", "UserComment" };
		private static string[] CONTROLS_EMAIL_VERIFICATION = { "BrandID", "MemberID" };
		private static string[] CONTROLS_INTERNAL_MAIL_NOTIFY = { "BrandID", "MemberID", "SentMemberID" };
		private static string[] CONTROLS_MATCH_MAIL = { "BrandID", "MemberID", "MemberIDList" };
		private static string[] CONTROLS_MUTUAL_YES_NOTIFY = { "BrandID", "MemberID", "TargetMemberID" };
		private static string[] CONTROLS_OPT_OUT_NOTIFY = { "BrandID", "Email", "FromHost" };
		private static string[] CONTROLS_PASSWORD_MAIL = { "BrandID", "Email" };
		private static string[] CONTROLS_PHOTO_APPROVED = { "BrandID", "MemberID", "PhotoID" };
		private static string[] CONTROLS_REG_VERIFICATION = { "BrandID", "MemberID" };
		private static string[] CONTROLS_SEND_MEMBER = { "BrandID", "SentMemberID", "FriendName", "FriendEmail", "UserEmail", "Subject", "Message" };

		public PreviewTool()
		{
			InitializeComponent();

			SetUpImpulseMap();

			Brands theBrands = BrandConfigSA.Instance.GetBrands();
			
			BrandID.Items.Clear();
			BrandID.DisplayMember = "Uri";
			BrandID.ValueMember = "BrandID";

			foreach(Brand theBrand in theBrands)
			{
				BrandID.Items.Add(theBrand);
			}
		}

		private void SetUpImpulseMap()
		{
			impulseMap = new Hashtable();

			impulseMap.Add("CheckSubConfirm", CONTROLS_CHECK_SUB_CONFIRM);
			impulseMap.Add("CollegeLuvPromo", CONTROLS_COLLEGE_LUV_PROMO);
			impulseMap.Add("ContactUs", CONTROLS_CONTACT_US);
			impulseMap.Add("EmailVerification", CONTROLS_EMAIL_VERIFICATION);
			impulseMap.Add("InternalMailNotify", CONTROLS_INTERNAL_MAIL_NOTIFY);
			impulseMap.Add("MatchMail", CONTROLS_MATCH_MAIL);
			impulseMap.Add("MutualYesNotify", CONTROLS_MUTUAL_YES_NOTIFY);
			impulseMap.Add("OptOutNotify", CONTROLS_OPT_OUT_NOTIFY);
			impulseMap.Add("PasswordMail", CONTROLS_PASSWORD_MAIL);
			impulseMap.Add("PhotoApproved", CONTROLS_PHOTO_APPROVED);
			impulseMap.Add("RegVerification", CONTROLS_REG_VERIFICATION);
			impulseMap.Add("SendMember", CONTROLS_SEND_MEMBER);
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(PreviewTool));
			this.btnRender = new System.Windows.Forms.Button();
			this.axWebBrowser1 = new AxSHDocVw.AxWebBrowser();
			this.lstEmailType = new System.Windows.Forms.ListBox();
			this.label1 = new System.Windows.Forms.Label();
			this.MemberID = new System.Windows.Forms.TextBox();
			this.MemberPlanID = new System.Windows.Forms.TextBox();
			this.PlanDescription = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.RemoteIP = new System.Windows.Forms.TextBox();
			this.Browser = new System.Windows.Forms.TextBox();
			this.FromEmailAddress = new System.Windows.Forms.TextBox();
			this.UserName = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.PhotoID = new System.Windows.Forms.TextBox();
			this.FromHost = new System.Windows.Forms.TextBox();
			this.Email = new System.Windows.Forms.TextBox();
			this.TargetMemberID = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.MemberIDList = new System.Windows.Forms.TextBox();
			this.SentMemberID = new System.Windows.Forms.TextBox();
			this.UserComment = new System.Windows.Forms.TextBox();
			this.Reason = new System.Windows.Forms.TextBox();
			this.label18 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.Message = new System.Windows.Forms.TextBox();
			this.Subject = new System.Windows.Forms.TextBox();
			this.UserEmail = new System.Windows.Forms.TextBox();
			this.label22 = new System.Windows.Forms.Label();
			this.label23 = new System.Windows.Forms.Label();
			this.FriendEmail = new System.Windows.Forms.TextBox();
			this.FriendName = new System.Windows.Forms.TextBox();
			this.BrandID = new System.Windows.Forms.ComboBox();
			((System.ComponentModel.ISupportInitialize)(this.axWebBrowser1)).BeginInit();
			this.SuspendLayout();
			// 
			// btnRender
			// 
			this.btnRender.Location = new System.Drawing.Point(56, 736);
			this.btnRender.Name = "btnRender";
			this.btnRender.TabIndex = 0;
			this.btnRender.Text = "Render";
			this.btnRender.Click += new System.EventHandler(this.btnRender_Click);
			// 
			// axWebBrowser1
			// 
			this.axWebBrowser1.Enabled = true;
			this.axWebBrowser1.Location = new System.Drawing.Point(224, 112);
			this.axWebBrowser1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axWebBrowser1.OcxState")));
			this.axWebBrowser1.Size = new System.Drawing.Size(976, 592);
			this.axWebBrowser1.TabIndex = 1;
			// 
			// lstEmailType
			// 
			this.lstEmailType.Items.AddRange(new object[] {
															  "CheckSubConfirm",
															  "CollegeLuvPromo",
															  "ContactUs",
															  "EmailVerification",
															  "InternalMailNotify",
															  "MatchMail",
															  "MutualYesNotify",
															  "OptOutNotify",
															  "PasswordMail",
															  "PhotoApproved",
															  "RegVerification",
															  "SendMember"});
			this.lstEmailType.Location = new System.Drawing.Point(8, 24);
			this.lstEmailType.Name = "lstEmailType";
			this.lstEmailType.Size = new System.Drawing.Size(200, 82);
			this.lstEmailType.TabIndex = 2;
			this.lstEmailType.SelectedIndexChanged += new System.EventHandler(this.lstEmailType_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 16);
			this.label1.TabIndex = 3;
			this.label1.Text = "Email Type";
			// 
			// MemberID
			// 
			this.MemberID.Location = new System.Drawing.Point(104, 176);
			this.MemberID.Name = "MemberID";
			this.MemberID.TabIndex = 5;
			this.MemberID.Text = "";
			// 
			// MemberPlanID
			// 
			this.MemberPlanID.Location = new System.Drawing.Point(104, 200);
			this.MemberPlanID.Name = "MemberPlanID";
			this.MemberPlanID.TabIndex = 6;
			this.MemberPlanID.Text = "";
			// 
			// PlanDescription
			// 
			this.PlanDescription.Location = new System.Drawing.Point(104, 224);
			this.PlanDescription.Name = "PlanDescription";
			this.PlanDescription.TabIndex = 7;
			this.PlanDescription.Text = "";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 112);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(192, 16);
			this.label2.TabIndex = 8;
			this.label2.Text = "Brand";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(0, 176);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(100, 16);
			this.label3.TabIndex = 9;
			this.label3.Text = "MemberID";
			this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(0, 200);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(100, 16);
			this.label4.TabIndex = 10;
			this.label4.Text = "MemberPlanID";
			this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(0, 224);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(100, 16);
			this.label5.TabIndex = 11;
			this.label5.Text = "Plan Description";
			this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(0, 320);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(100, 16);
			this.label6.TabIndex = 19;
			this.label6.Text = "RemoteIP";
			this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(0, 296);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(100, 16);
			this.label7.TabIndex = 18;
			this.label7.Text = "Browser";
			this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(0, 272);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(100, 16);
			this.label8.TabIndex = 17;
			this.label8.Text = "FromEmailAddress";
			this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(0, 256);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(100, 16);
			this.label9.TabIndex = 16;
			this.label9.Text = "UserName";
			this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// RemoteIP
			// 
			this.RemoteIP.Location = new System.Drawing.Point(104, 320);
			this.RemoteIP.Name = "RemoteIP";
			this.RemoteIP.TabIndex = 15;
			this.RemoteIP.Text = "";
			// 
			// Browser
			// 
			this.Browser.Location = new System.Drawing.Point(104, 296);
			this.Browser.Name = "Browser";
			this.Browser.TabIndex = 14;
			this.Browser.Text = "";
			// 
			// FromEmailAddress
			// 
			this.FromEmailAddress.Location = new System.Drawing.Point(104, 272);
			this.FromEmailAddress.Name = "FromEmailAddress";
			this.FromEmailAddress.TabIndex = 13;
			this.FromEmailAddress.Text = "";
			// 
			// UserName
			// 
			this.UserName.Location = new System.Drawing.Point(104, 248);
			this.UserName.Name = "UserName";
			this.UserName.TabIndex = 12;
			this.UserName.Text = "";
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(0, 512);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(100, 16);
			this.label10.TabIndex = 35;
			this.label10.Text = "Photo ID";
			this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(0, 488);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(100, 16);
			this.label11.TabIndex = 34;
			this.label11.Text = "From Host";
			this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(0, 464);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(100, 16);
			this.label12.TabIndex = 33;
			this.label12.Text = "Email";
			this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(0, 448);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(100, 16);
			this.label13.TabIndex = 32;
			this.label13.Text = "Target Member ID";
			this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// PhotoID
			// 
			this.PhotoID.Location = new System.Drawing.Point(104, 512);
			this.PhotoID.Name = "PhotoID";
			this.PhotoID.TabIndex = 31;
			this.PhotoID.Text = "";
			// 
			// FromHost
			// 
			this.FromHost.Location = new System.Drawing.Point(104, 488);
			this.FromHost.Name = "FromHost";
			this.FromHost.TabIndex = 30;
			this.FromHost.Text = "";
			// 
			// Email
			// 
			this.Email.Location = new System.Drawing.Point(104, 464);
			this.Email.Name = "Email";
			this.Email.TabIndex = 29;
			this.Email.Text = "";
			// 
			// TargetMemberID
			// 
			this.TargetMemberID.Location = new System.Drawing.Point(104, 440);
			this.TargetMemberID.Name = "TargetMemberID";
			this.TargetMemberID.TabIndex = 28;
			this.TargetMemberID.Text = "";
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(0, 416);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(100, 16);
			this.label14.TabIndex = 27;
			this.label14.Text = "Member ID List";
			this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label15
			// 
			this.label15.Location = new System.Drawing.Point(0, 392);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(100, 16);
			this.label15.TabIndex = 26;
			this.label15.Text = "Sent Member ID";
			this.label15.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label16
			// 
			this.label16.Location = new System.Drawing.Point(0, 368);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(100, 16);
			this.label16.TabIndex = 25;
			this.label16.Text = "User Comment";
			this.label16.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label17
			// 
			this.label17.Location = new System.Drawing.Point(0, 352);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(100, 16);
			this.label17.TabIndex = 24;
			this.label17.Text = "Reason";
			this.label17.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// MemberIDList
			// 
			this.MemberIDList.Location = new System.Drawing.Point(104, 416);
			this.MemberIDList.Name = "MemberIDList";
			this.MemberIDList.TabIndex = 23;
			this.MemberIDList.Text = "";
			// 
			// SentMemberID
			// 
			this.SentMemberID.Location = new System.Drawing.Point(104, 392);
			this.SentMemberID.Name = "SentMemberID";
			this.SentMemberID.TabIndex = 22;
			this.SentMemberID.Text = "";
			// 
			// UserComment
			// 
			this.UserComment.Location = new System.Drawing.Point(104, 368);
			this.UserComment.Name = "UserComment";
			this.UserComment.TabIndex = 21;
			this.UserComment.Text = "";
			// 
			// Reason
			// 
			this.Reason.Location = new System.Drawing.Point(104, 344);
			this.Reason.Name = "Reason";
			this.Reason.TabIndex = 20;
			this.Reason.Text = "";
			// 
			// label18
			// 
			this.label18.Location = new System.Drawing.Point(0, 632);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(100, 16);
			this.label18.TabIndex = 47;
			this.label18.Text = "Message";
			this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label19
			// 
			this.label19.Location = new System.Drawing.Point(0, 608);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(100, 16);
			this.label19.TabIndex = 46;
			this.label19.Text = "Subject";
			this.label19.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label21
			// 
			this.label21.Location = new System.Drawing.Point(0, 592);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(100, 16);
			this.label21.TabIndex = 44;
			this.label21.Text = "User Email";
			this.label21.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// Message
			// 
			this.Message.Location = new System.Drawing.Point(104, 632);
			this.Message.Name = "Message";
			this.Message.TabIndex = 43;
			this.Message.Text = "";
			// 
			// Subject
			// 
			this.Subject.Location = new System.Drawing.Point(104, 608);
			this.Subject.Name = "Subject";
			this.Subject.TabIndex = 42;
			this.Subject.Text = "";
			// 
			// UserEmail
			// 
			this.UserEmail.Location = new System.Drawing.Point(104, 584);
			this.UserEmail.Name = "UserEmail";
			this.UserEmail.TabIndex = 40;
			this.UserEmail.Text = "";
			// 
			// label22
			// 
			this.label22.Location = new System.Drawing.Point(0, 560);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(100, 16);
			this.label22.TabIndex = 39;
			this.label22.Text = "Friend Email";
			this.label22.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// label23
			// 
			this.label23.Location = new System.Drawing.Point(0, 536);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(100, 16);
			this.label23.TabIndex = 38;
			this.label23.Text = "Friend Name";
			this.label23.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// FriendEmail
			// 
			this.FriendEmail.Location = new System.Drawing.Point(104, 560);
			this.FriendEmail.Name = "FriendEmail";
			this.FriendEmail.TabIndex = 37;
			this.FriendEmail.Text = "";
			// 
			// FriendName
			// 
			this.FriendName.Location = new System.Drawing.Point(104, 536);
			this.FriendName.Name = "FriendName";
			this.FriendName.TabIndex = 36;
			this.FriendName.Text = "";
			// 
			// BrandID
			// 
			this.BrandID.Location = new System.Drawing.Point(8, 128);
			this.BrandID.Name = "BrandID";
			this.BrandID.Size = new System.Drawing.Size(200, 21);
			this.BrandID.Sorted = true;
			this.BrandID.TabIndex = 48;
			// 
			// PreviewTool
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(928, 773);
			this.Controls.Add(this.BrandID);
			this.Controls.Add(this.label18);
			this.Controls.Add(this.label19);
			this.Controls.Add(this.label21);
			this.Controls.Add(this.Message);
			this.Controls.Add(this.Subject);
			this.Controls.Add(this.UserEmail);
			this.Controls.Add(this.label22);
			this.Controls.Add(this.label23);
			this.Controls.Add(this.FriendEmail);
			this.Controls.Add(this.FriendName);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.PhotoID);
			this.Controls.Add(this.FromHost);
			this.Controls.Add(this.Email);
			this.Controls.Add(this.TargetMemberID);
			this.Controls.Add(this.label14);
			this.Controls.Add(this.label15);
			this.Controls.Add(this.label16);
			this.Controls.Add(this.label17);
			this.Controls.Add(this.MemberIDList);
			this.Controls.Add(this.SentMemberID);
			this.Controls.Add(this.UserComment);
			this.Controls.Add(this.Reason);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.RemoteIP);
			this.Controls.Add(this.Browser);
			this.Controls.Add(this.FromEmailAddress);
			this.Controls.Add(this.UserName);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.PlanDescription);
			this.Controls.Add(this.MemberPlanID);
			this.Controls.Add(this.MemberID);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.lstEmailType);
			this.Controls.Add(this.axWebBrowser1);
			this.Controls.Add(this.btnRender);
			this.Name = "PreviewTool";
			this.Text = "ExternalMail Preview Tool";
			((System.ComponentModel.ISupportInitialize)(this.axWebBrowser1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		[STAThread]
		static void Main() 
		{
			Application.Run(new PreviewTool());
		}

		private void btnRender_Click(object sender, System.EventArgs e)
		{
			object myObject = new object();
			axWebBrowser1.Navigate("", ref myObject, ref myObject, ref myObject, ref myObject);

			//MatchMailImpulse matchMail = new MatchMailImpulse(1001, 23248462, new Int32[] {23248462,23248462,23248462});
			PasswordMailImpulse passwordMail = new PasswordMailImpulse(1017, "dan@denimgroup.com");
			EmailVerificationImpulse verifyEmail = new EmailVerificationImpulse(23248462, 1001);
			///ContactUsImpulse contactEmail = new ContactUsImpulse(1017, "DanCoTest", "dan@denimgroup.com", "12345678", "My Browser string", "192.168.20.10", "My Reason & My Reasons", "My User<>!@#$%&%# Comments have scary characterss in ()-_+={}[]\\|';:,<.>/? them");
			//StringBuilder sb = new StringBuilder();
			//for(int i = 0; i < 1000; i++)
			//{
			//	sb.Append("1234567890");
			//}
			// ContactUsImpulse contactEmail = new ContactUsImpulse(1017, "DanCoTest", "dan@denimgroup.com", "12345678", "My Browser string", "192.168.20.10", "My Reason & My Reasons", sb.ToString());
			RegVerificationImpulse regEmail = new RegVerificationImpulse(988268688, 1017);
			OptOutNotifyImpulse optOutEmail = new OptOutNotifyImpulse(1017, "dan@denimgroup.com", "www.spark.com");
			CheckSubConfirmImpulse checkEmail = new CheckSubConfirmImpulse(988268688, 1017, 123456789, "This is my plan description");
			SendMemberImpulse sendMemberEmail = new SendMemberImpulse("Dan Cornell", "to_dan@denimgroup.com", "dan@denimgroup.com", 12345, "Here is someone", "This is the message about someone", 1017);
			PhotoApprovedImpulse photoImpulse = new PhotoApprovedImpulse(988268688, 12345, 1017, "approved");
			//	TOFIX - get a demo emailID if this is going to be used again
			InternalMailNotifyImpulse internalMailImpulse = new InternalMailNotifyImpulse(988268688, 988268688, 0, 1017);
			MutualYesNotifyImpulse mutualYesImpulse = new MutualYesNotifyImpulse(988268688, 988268688, 1017);
			CollegeLuvPromoImpulse collegeImpulse = new CollegeLuvPromoImpulse(988268688, 1017);

			ImpulseBase theImpulse = GenerateImpulse();

			string fileName = Directory.GetCurrentDirectory() + @"\temp.htm";
			StreamWriter sw = new StreamWriter(fileName, false);
			// string messageBody = Composer.Instance.Compose(matchMail);
			// string messageBody = Composer.Instance.Compose(passwordMail);
			// string messageBody = Composer.Instance.Compose(verifyEmail);
			// string messageBody = Composer.Instance.Compose(contactEmail);
			// string messageBody = Composer.Instance.Compose(regEmail);
			// string messageBody = Composer.Instance.Compose(optOutEmail);
			// string messageBody = Composer.Instance.Compose(checkEmail);
			// string messageBody = Composer.Instance.Compose(sendMemberEmail);
			// string messageBody = Composer.Instance.Compose(photoImpulse);
			// string messageBody = Composer.Instance.Compose(internalMailImpulse);
			// string messageBody = Composer.Instance.Compose(mutualYesImpulse);
			// string messageBody = Composer.Instance.Compose(collegeImpulse);

			// string messageBody = Composer.Instance.Compose(theImpulse);
			MessageInfo messageInfo = Composer.Instance.Compose(theImpulse);

			sw.Write(messageInfo.Body);
			sw.Close();

			/*
			SMTPMessage message = new SMTPMessage("blackholeme@matchnet.com",
				"YourMatches@americansingles.com",
				"dan@denimgroup.com",
				"Dan Cornell",
				"ExternalMail.Harness test",
				Constants.NULL_STRING,
				messageBody);

			SMTPMail smtpMail = new SMTPMail(100);
			smtpMail.SmtpServer = "ironport6.matchnet.com";
			smtpMail.Send(message);
			*/

			axWebBrowser1.Navigate(fileName, ref myObject, ref myObject, ref myObject, ref myObject);
		}

		private ImpulseBase GenerateImpulse()
		{
			ImpulseBase retVal = null;

			string emailType = (string)lstEmailType.SelectedItem;

			switch(emailType)
			{
				case "CheckSubConfirm":
					retVal = new CheckSubConfirmImpulse(Convert.ToInt32(MemberID.Text),
														((Brand)BrandID.SelectedItem).BrandID, 
														Convert.ToInt32(MemberPlanID.Text),
														PlanDescription.Text);
					break;

				case "CollegeLuvPromo":
					retVal = new CollegeLuvPromoImpulse(Convert.ToInt32(MemberID.Text),
														((Brand)BrandID.SelectedItem).BrandID);
					break;

				case "ContactUs":
					retVal = new ContactUsImpulse(((Brand)BrandID.SelectedItem).BrandID,
													UserName.Text,
													FromEmailAddress.Text, 
													MemberID.Text,
													Browser.Text,
													RemoteIP.Text,
													Reason.Text,
													UserComment.Text);
					break;

				case "EmailVerification":
					retVal = new EmailVerificationImpulse(Convert.ToInt32(MemberID.Text),
															((Brand)BrandID.SelectedItem).BrandID);
					break;

				case "InternalMailNotify":
					retVal = new InternalMailNotifyImpulse(Convert.ToInt32(MemberID.Text),
															Convert.ToInt32(SentMemberID.Text),
															0,	//	TOFIX - add a param for the email ID if this is going to be used again
															((Brand)BrandID.SelectedItem).BrandID);
					break;

				case "MatchMail":
					ArrayList aMemberIDs = new ArrayList();
					foreach(string key in MemberIDList.Text.Split(new char[] {','}))
					{
						aMemberIDs.Add(Convert.ToInt32(key));
					}
					int[] memberIDs = new int[aMemberIDs.Count];
					for(int i = 0; i < memberIDs.Length; i++)
					{
						memberIDs[i] = (int)aMemberIDs[i];
					}
					/*
					retVal = new MatchMailImpulse(((Brand)BrandID.SelectedItem).BrandID,
												Convert.ToInt32(MemberID.Text),
												memberIDs);
					*/
					break;

				case "MutualYesNotify":
					retVal = new MutualYesNotifyImpulse(Convert.ToInt32(MemberID.Text),
														Convert.ToInt32(TargetMemberID.Text),
														Convert.ToInt32(BrandID.Text));
					break;

				case "OptOutNotify":
					retVal = new OptOutNotifyImpulse(Convert.ToInt32(BrandID.Text),
													Email.Text,
													FromHost.Text);
					break;

				case "PasswordMail":
					retVal = new PasswordMailImpulse(((Brand)BrandID.SelectedItem).BrandID,
													Email.Text);
					break;

				case "PhotoApproved":
					retVal = new PhotoApprovedImpulse(Convert.ToInt32(MemberID.Text),
														Convert.ToInt32(PhotoID.Text),
														((Brand)BrandID.SelectedItem).BrandID,
														"approved");
					break;

				case "RegVerification":
					retVal = new RegVerificationImpulse(Convert.ToInt32(MemberID.Text),
														((Brand)BrandID.SelectedItem).BrandID);
					break;

				case "SendMember":
					retVal = new SendMemberImpulse(FriendName.Text,
													FriendEmail.Text,
													UserEmail.Text,
													Convert.ToInt32(SentMemberID.Text),
													Subject.Text,
													Message.Text,
													((Brand)BrandID.SelectedItem).BrandID);
					break;

			}

			return(retVal);
		}

		private void SetControlsVisibility(string[] controlNames, bool isVisible)
		{
			foreach(string controlName in controlNames)
			{
				SetControlVisibility(controlName, isVisible);
			}
		}

		private void SetControlVisibility(string controlName, bool isVisible)
		{
			Control theControl = FindControl(controlName);
			theControl.Visible = isVisible;
		}

		private Control FindControl(string controlName)
		{
			Control retVal = null;

			foreach(Control myControl in this.Controls)
			{
				if(myControl.Name.Equals(controlName))
				{
					retVal = myControl;
					break;
				}
			}

			return(retVal);
		}

		private void lstEmailType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string newImpulse = (string)lstEmailType.SelectedItem;
			string[] controls = (string[])impulseMap[newImpulse];
			HideControls();
			SetControlsVisibility(controls, true);
		}

		private void HideControls()
		{
			SetControlsVisibility(CONTROLS_ALL, false);
		}
	}
}
