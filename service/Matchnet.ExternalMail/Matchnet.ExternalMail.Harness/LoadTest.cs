using System;
using System.Threading;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.BusinessLogic;
using MSA = Matchnet.Member.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.ExternalMail.ValueObjects.StormpostSoap;
using Config = Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.BusinessLogic.YesMailWebService;

namespace Matchnet.ExternalMail.Harness
{
	public class LoadTest
	{
		private int _threadCount = 40;
		private TestCallBack _testCallback;
		private int _numberOfCalls;
		public ManualResetEvent[] _mnuResets;


		public LoadTest()
		{
			_mnuResets = new ManualResetEvent[_threadCount];
		}

		public void DoTest(int numberOfCalls, bool async, TestCallBack callBack)
		{
			_testCallback = callBack;
			if(! async)
			{
				SyncTest(numberOfCalls);
			}
			else
			{
				AsyncTest(numberOfCalls);
			}
		}

		private void AsyncTest(int numberOfCalls)
		{	
			_numberOfCalls = numberOfCalls;
			Thread t = new Thread(new ThreadStart(DoAsyncTest));
			t.IsBackground = true;
			t.Start();
		}
		private void DoAsyncTest()
		{
			DateTime start = DateTime.Now;
			int itemsPerThread = _numberOfCalls / _threadCount;
			for(int i = 0; i < _threadCount; i++)
			{
				_mnuResets[i] = new ManualResetEvent(false);		
				
				AsyncTestHelper at = new AsyncTestHelper();
				at.ItemsPerThread = itemsPerThread;
				at.Index = i;
				at.MnuReset = _mnuResets[i];

				Thread t = new Thread(new ThreadStart(at.DoAsyncTest));
				t.IsBackground = true;
				t.Start();
			}

			bool done = WaitHandle.WaitAll(_mnuResets);
			if(done)
			{
				DateTime now = DateTime.Now;
				TimeSpan ts = now.Subtract(start);
				_testCallback(ts.Hours.ToString() + ":" + ts.Minutes.ToString() + ":" + ts.Seconds.ToString());
			}
		}
		private void SyncTest(int numberOfCalls)
		{
			DateTime start = DateTime.Now;
			for(int i = 0; i < numberOfCalls; i++)
			{
				SendForgotPasswordEmail();
			}
			DateTime end = DateTime.Now;

			TimeSpan ts = end.Subtract(start);
			_testCallback(ts.Hours.ToString() + ":" + ts.Minutes.ToString() + ":" + ts.Seconds.ToString());
		}

		private static void SendForgotPasswordEmail()
		{
			PasswordMailImpulse imp = new PasswordMailImpulse(1001, "qa1@spark.net");
			YesMailHandler.Instance.SendEmail(imp);
		}

		public class AsyncTestHelper
		{
			public int ItemsPerThread;
			public int Index;
			public ManualResetEvent MnuReset;
			
			public void DoAsyncTest()
			{
				for(int i = 0; i < ItemsPerThread; i++)
				{
					SendForgotPasswordEmail();	
				}
				MnuReset.Set();
			}
		}
	}

	public delegate void TestCallBack(object arg);
}
