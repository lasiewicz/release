using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.BusinessLogic;
using MSA = Matchnet.Member.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.ExternalMail.ValueObjects.StormpostSoap;
using Config = Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.BusinessLogic.YesMailWebService;

namespace Matchnet.ExternalMail.Harness
{
	/// <summary>
	/// Summary description for YesMailTest.
	/// </summary>
	public class YesMailTest : System.Windows.Forms.Form
	{
		private System.Collections.ArrayList id = new ArrayList();
		private System.Windows.Forms.Button buttonEcardToMember;
		private System.Collections.ArrayList email = new ArrayList();
		private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnComments;
        private System.Windows.Forms.Button btnEmptyQueue;
		private System.Windows.Forms.Button ButtonInternalCorpMail;
		private System.Collections.ArrayList brands = new ArrayList();

		#region

		static void Main()
		{
			Application.Run(new YesMailTest());
		}

		private System.Windows.Forms.Button buttonForgotPwd;
		private System.Windows.Forms.Button buttonPhotoApproval;
		private System.Windows.Forms.Button buttonMemberToFriend;
		private System.Windows.Forms.Button buttonNewMailAlert;
		private System.Windows.Forms.Button buttonMutualYesNotify;
		private System.Windows.Forms.Button buttonPhotoRejected;
		private System.Windows.Forms.Button buttonOptOutNotify;
		private System.Windows.Forms.Button buttonHotListedNotify;
		private System.Windows.Forms.Button buttonSubscriptionConfirmation;
		private System.Windows.Forms.Button buttonRegVerification;
		private System.Windows.Forms.Button buttonEmailChangeConfirmation;
		private System.Windows.Forms.Button buttonMessageBoardInstantNotification;
		private System.Windows.Forms.Button buttonMessageBoardDaily;
		private System.Windows.Forms.Button buttonEcardToFriend;
		private System.Windows.Forms.Button buttonRenewalFailureNotification;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;


		public YesMailTest()
		{

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			int[] brandsIdList = {1001, 1003, 1004, 1006, 1013, 1015, 10012, 1105};
			foreach(int brandId in brandsIdList)
			{
				brands.Add(BrandConfigSA.Instance.GetBrandByID(brandId));
			}

            id.Add(100066132);
//			id.Add(100047079);
//			id.Add(16000206);
//			id.Add(987656484);
//			id.Add(100042825);
//			id.Add(100064353);


            email.Add("jrho@spark.net");
            //email.Add("wlee@spark.net");
			//email.Add("lhalley@spark.net");
			//email.Add("lexxx1524@aol.com");
			//email.Add("come2cali4nia@aim.com");
			//email.Add("refron@Matchnet.com");
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#endregion

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.buttonForgotPwd = new System.Windows.Forms.Button();
			this.buttonHotListedNotify = new System.Windows.Forms.Button();
			this.buttonPhotoRejected = new System.Windows.Forms.Button();
			this.buttonNewMailAlert = new System.Windows.Forms.Button();
			this.buttonPhotoApproval = new System.Windows.Forms.Button();
			this.buttonSubscriptionConfirmation = new System.Windows.Forms.Button();
			this.buttonOptOutNotify = new System.Windows.Forms.Button();
			this.buttonMutualYesNotify = new System.Windows.Forms.Button();
			this.buttonMemberToFriend = new System.Windows.Forms.Button();
			this.buttonRegVerification = new System.Windows.Forms.Button();
			this.buttonEmailChangeConfirmation = new System.Windows.Forms.Button();
			this.buttonMessageBoardInstantNotification = new System.Windows.Forms.Button();
			this.buttonMessageBoardDaily = new System.Windows.Forms.Button();
			this.buttonEcardToFriend = new System.Windows.Forms.Button();
			this.buttonRenewalFailureNotification = new System.Windows.Forms.Button();
			this.buttonEcardToMember = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.btnComments = new System.Windows.Forms.Button();
			this.btnEmptyQueue = new System.Windows.Forms.Button();
			this.ButtonInternalCorpMail = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// buttonForgotPwd
			// 
			this.buttonForgotPwd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonForgotPwd.Location = new System.Drawing.Point(29, 18);
			this.buttonForgotPwd.Name = "buttonForgotPwd";
			this.buttonForgotPwd.Size = new System.Drawing.Size(182, 47);
			this.buttonForgotPwd.TabIndex = 0;
			this.buttonForgotPwd.Text = "Forgot Password";
			this.buttonForgotPwd.Click += new System.EventHandler(this.buttonForgotPwd_Click);
			// 
			// buttonHotListedNotify
			// 
			this.buttonHotListedNotify.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonHotListedNotify.Location = new System.Drawing.Point(250, 178);
			this.buttonHotListedNotify.Name = "buttonHotListedNotify";
			this.buttonHotListedNotify.Size = new System.Drawing.Size(182, 46);
			this.buttonHotListedNotify.TabIndex = 1;
			this.buttonHotListedNotify.Text = "Hot Listed Notify";
			this.buttonHotListedNotify.Click += new System.EventHandler(this.buttonHotListedNotify_Click);
			// 
			// buttonPhotoRejected
			// 
			this.buttonPhotoRejected.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonPhotoRejected.Location = new System.Drawing.Point(250, 125);
			this.buttonPhotoRejected.Name = "buttonPhotoRejected";
			this.buttonPhotoRejected.Size = new System.Drawing.Size(182, 46);
			this.buttonPhotoRejected.TabIndex = 2;
			this.buttonPhotoRejected.Text = "Photo Rejected";
			this.buttonPhotoRejected.Click += new System.EventHandler(this.buttonPhotoRejected_Click);
			// 
			// buttonNewMailAlert
			// 
			this.buttonNewMailAlert.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonNewMailAlert.Location = new System.Drawing.Point(250, 74);
			this.buttonNewMailAlert.Name = "buttonNewMailAlert";
			this.buttonNewMailAlert.Size = new System.Drawing.Size(182, 46);
			this.buttonNewMailAlert.TabIndex = 3;
			this.buttonNewMailAlert.Text = "New Mail Alert";
			this.buttonNewMailAlert.Click += new System.EventHandler(this.buttonNewMailAlert_Click);
			// 
			// buttonPhotoApproval
			// 
			this.buttonPhotoApproval.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonPhotoApproval.Location = new System.Drawing.Point(250, 18);
			this.buttonPhotoApproval.Name = "buttonPhotoApproval";
			this.buttonPhotoApproval.Size = new System.Drawing.Size(182, 47);
			this.buttonPhotoApproval.TabIndex = 4;
			this.buttonPhotoApproval.Text = "PhotoApproval";
			this.buttonPhotoApproval.Click += new System.EventHandler(this.buttonPhotoApproval_Click);
			// 
			// buttonSubscriptionConfirmation
			// 
			this.buttonSubscriptionConfirmation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonSubscriptionConfirmation.Location = new System.Drawing.Point(29, 231);
			this.buttonSubscriptionConfirmation.Name = "buttonSubscriptionConfirmation";
			this.buttonSubscriptionConfirmation.Size = new System.Drawing.Size(182, 46);
			this.buttonSubscriptionConfirmation.TabIndex = 5;
			this.buttonSubscriptionConfirmation.Text = "Sub Confirmation";
			this.buttonSubscriptionConfirmation.Click += new System.EventHandler(this.buttonSubscriptionConfirmation_Click);
			// 
			// buttonOptOutNotify
			// 
			this.buttonOptOutNotify.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonOptOutNotify.Location = new System.Drawing.Point(29, 178);
			this.buttonOptOutNotify.Name = "buttonOptOutNotify";
			this.buttonOptOutNotify.Size = new System.Drawing.Size(182, 46);
			this.buttonOptOutNotify.TabIndex = 6;
			this.buttonOptOutNotify.Text = " Opt Out Notify";
			this.buttonOptOutNotify.Click += new System.EventHandler(this.buttonOptOutNotify_Click);
			// 
			// buttonMutualYesNotify
			// 
			this.buttonMutualYesNotify.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonMutualYesNotify.Location = new System.Drawing.Point(29, 125);
			this.buttonMutualYesNotify.Name = "buttonMutualYesNotify";
			this.buttonMutualYesNotify.Size = new System.Drawing.Size(182, 46);
			this.buttonMutualYesNotify.TabIndex = 7;
			this.buttonMutualYesNotify.Text = "Mutual Yes Notify";
			this.buttonMutualYesNotify.Click += new System.EventHandler(this.buttonMutualYesNotify_Click);
			// 
			// buttonMemberToFriend
			// 
			this.buttonMemberToFriend.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonMemberToFriend.Location = new System.Drawing.Point(29, 72);
			this.buttonMemberToFriend.Name = "buttonMemberToFriend";
			this.buttonMemberToFriend.Size = new System.Drawing.Size(182, 46);
			this.buttonMemberToFriend.TabIndex = 8;
			this.buttonMemberToFriend.Text = "Member To Friend";
			this.buttonMemberToFriend.Click += new System.EventHandler(this.buttonMemberToFriend_Click);
			// 
			// buttonRegVerification
			// 
			this.buttonRegVerification.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonRegVerification.Location = new System.Drawing.Point(250, 231);
			this.buttonRegVerification.Name = "buttonRegVerification";
			this.buttonRegVerification.Size = new System.Drawing.Size(182, 46);
			this.buttonRegVerification.TabIndex = 9;
			this.buttonRegVerification.Text = "Registration Verification";
			this.buttonRegVerification.Click += new System.EventHandler(this.buttonRegVerification_Click);
			// 
			// buttonEmailChangeConfirmation
			// 
			this.buttonEmailChangeConfirmation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonEmailChangeConfirmation.Location = new System.Drawing.Point(461, 18);
			this.buttonEmailChangeConfirmation.Name = "buttonEmailChangeConfirmation";
			this.buttonEmailChangeConfirmation.Size = new System.Drawing.Size(182, 47);
			this.buttonEmailChangeConfirmation.TabIndex = 10;
			this.buttonEmailChangeConfirmation.Text = "Email Change";
			this.buttonEmailChangeConfirmation.Click += new System.EventHandler(this.buttonEmailChangeConfirmation_Click);
			// 
			// buttonMessageBoardInstantNotification
			// 
			this.buttonMessageBoardInstantNotification.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonMessageBoardInstantNotification.Location = new System.Drawing.Point(461, 72);
			this.buttonMessageBoardInstantNotification.Name = "buttonMessageBoardInstantNotification";
			this.buttonMessageBoardInstantNotification.Size = new System.Drawing.Size(182, 46);
			this.buttonMessageBoardInstantNotification.TabIndex = 11;
			this.buttonMessageBoardInstantNotification.Text = "Msg Brd Instant Notification";
			this.buttonMessageBoardInstantNotification.Click += new System.EventHandler(this.buttonMessageBoardInstantNotification_Click);
			// 
			// buttonMessageBoardDaily
			// 
			this.buttonMessageBoardDaily.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonMessageBoardDaily.Location = new System.Drawing.Point(461, 125);
			this.buttonMessageBoardDaily.Name = "buttonMessageBoardDaily";
			this.buttonMessageBoardDaily.Size = new System.Drawing.Size(182, 46);
			this.buttonMessageBoardDaily.TabIndex = 12;
			this.buttonMessageBoardDaily.Text = "Msg Brd Daily Update";
			this.buttonMessageBoardDaily.Click += new System.EventHandler(this.buttonMessageBoardDaily_Click);
			// 
			// buttonEcardToFriend
			// 
			this.buttonEcardToFriend.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonEcardToFriend.Location = new System.Drawing.Point(461, 178);
			this.buttonEcardToFriend.Name = "buttonEcardToFriend";
			this.buttonEcardToFriend.Size = new System.Drawing.Size(182, 46);
			this.buttonEcardToFriend.TabIndex = 13;
			this.buttonEcardToFriend.Text = "Ecard To Friend";
			this.buttonEcardToFriend.Click += new System.EventHandler(this.buttonEcardToFriend_Click);
			// 
			// buttonRenewalFailureNotification
			// 
			this.buttonRenewalFailureNotification.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonRenewalFailureNotification.Location = new System.Drawing.Point(461, 231);
			this.buttonRenewalFailureNotification.Name = "buttonRenewalFailureNotification";
			this.buttonRenewalFailureNotification.Size = new System.Drawing.Size(182, 46);
			this.buttonRenewalFailureNotification.TabIndex = 14;
			this.buttonRenewalFailureNotification.Text = "Renewal Failure Notification";
			this.buttonRenewalFailureNotification.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.buttonRenewalFailureNotification.Click += new System.EventHandler(this.buttonRenewalFailureNotification_Click);
			// 
			// buttonEcardToMember
			// 
			this.buttonEcardToMember.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.buttonEcardToMember.Location = new System.Drawing.Point(29, 286);
			this.buttonEcardToMember.Name = "buttonEcardToMember";
			this.buttonEcardToMember.Size = new System.Drawing.Size(182, 46);
			this.buttonEcardToMember.TabIndex = 15;
			this.buttonEcardToMember.Text = "Ecard To Member";
			this.buttonEcardToMember.Click += new System.EventHandler(this.buttonEcardToMember_Click);
			// 
			// button1
			// 
			this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.button1.Location = new System.Drawing.Point(250, 286);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(182, 46);
			this.button1.TabIndex = 16;
			this.button1.Text = "Use Load Test";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// btnComments
			// 
			this.btnComments.Location = new System.Drawing.Point(461, 286);
			this.btnComments.Name = "btnComments";
			this.btnComments.Size = new System.Drawing.Size(182, 46);
			this.btnComments.TabIndex = 17;
			this.btnComments.Text = "Comments";
			this.btnComments.Click += new System.EventHandler(this.btnComments_Click);
			// 
			// btnEmptyQueue
			// 
			this.btnEmptyQueue.Location = new System.Drawing.Point(278, 342);
			this.btnEmptyQueue.Name = "btnEmptyQueue";
			this.btnEmptyQueue.Size = new System.Drawing.Size(125, 27);
			this.btnEmptyQueue.TabIndex = 18;
			this.btnEmptyQueue.Text = "Empty Queue";
			this.btnEmptyQueue.Click += new System.EventHandler(this.btnEmptyQueue_Click);
			// 
			// ButtonInternalCorpMail
			// 
			this.ButtonInternalCorpMail.Location = new System.Drawing.Point(496, 344);
			this.ButtonInternalCorpMail.Name = "ButtonInternalCorpMail";
			this.ButtonInternalCorpMail.Size = new System.Drawing.Size(112, 23);
			this.ButtonInternalCorpMail.TabIndex = 19;
			this.ButtonInternalCorpMail.Text = "InternalCorpMail";
			this.ButtonInternalCorpMail.Click += new System.EventHandler(this.ButtonInternalCorpMail_Click);
			// 
			// YesMailTest
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
			this.ClientSize = new System.Drawing.Size(710, 368);
			this.Controls.Add(this.ButtonInternalCorpMail);
			this.Controls.Add(this.btnEmptyQueue);
			this.Controls.Add(this.btnComments);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.buttonEcardToMember);
			this.Controls.Add(this.buttonRenewalFailureNotification);
			this.Controls.Add(this.buttonEcardToFriend);
			this.Controls.Add(this.buttonMessageBoardDaily);
			this.Controls.Add(this.buttonMessageBoardInstantNotification);
			this.Controls.Add(this.buttonEmailChangeConfirmation);
			this.Controls.Add(this.buttonRegVerification);
			this.Controls.Add(this.buttonMemberToFriend);
			this.Controls.Add(this.buttonMutualYesNotify);
			this.Controls.Add(this.buttonOptOutNotify);
			this.Controls.Add(this.buttonSubscriptionConfirmation);
			this.Controls.Add(this.buttonPhotoApproval);
			this.Controls.Add(this.buttonNewMailAlert);
			this.Controls.Add(this.buttonPhotoRejected);
			this.Controls.Add(this.buttonHotListedNotify);
			this.Controls.Add(this.buttonForgotPwd);
			this.Name = "YesMailTest";
			this.Text = "YesMailTest";
			this.Load += new System.EventHandler(this.YesMailTest_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void buttonForgotPwd_Click(object sender, System.EventArgs e)
		{
			//sparkmailsettings@yahoo.com
			//refron@Matchnet.com
			//rescamilla@spark.net

            int[] siteIdList = {105};		
			//int[] siteIdList = {101, 112, 15, 13, 6, 103, 4};
						
			foreach(int si in siteIdList)
			{	
				Config.Brand b = this.GetBrand(si);
				foreach(string em in email)
				{
					PasswordMailImpulse imp = new PasswordMailImpulse(b.BrandID, em);
					YesMailHandler.Instance.SendEmail(imp);
				}
			}
			
			((Button)sender).ForeColor = Color.Green;
		}

		private void buttonPhotoApproval_Click(object sender, System.EventArgs e)
		{			
			int[] siteIdList = {6, 101, 112, 15, 13, 103, 4};
			foreach(int si in siteIdList)
			{
				Config.Brand b = this.GetBrand(si);
				
				foreach(Object i in id)
				{
					PhotoApprovedImpulse imp = new PhotoApprovedImpulse(int.Parse(i.ToString()), 11, b.BrandID, "approved");
					YesMailHandler.Instance.SendPhotoApproval(imp);
				}
			}
			
			((Button)sender).ForeColor = Color.Green;
		}

		private void buttonPhotoRejected_Click(object sender, System.EventArgs e)
		{
			int[] siteIdList = {13, 6, 103, 101, 4, 15, 112};
			foreach(int si in siteIdList)
			{
				Config.Brand b = this.GetBrand(si);
				foreach(object i in id)
				{
					PhotoRejectedImpulse imp = new PhotoRejectedImpulse(int.Parse(i.ToString()), 7000, b.BrandID, "Your photo is rejected by Steve.");
					YesMailHandler.Instance.SendPhotoRejection(imp);
				}
			}
			
			((Button)sender).ForeColor = Color.Green;
		}

		private void buttonHotListedNotify_Click(object sender, System.EventArgs e	)
		{
			//int[] siteIdList = { 4, 101, 13, 6, 103 };
			int[] siteIdList = {6};

			foreach(int sid in siteIdList)
			{
				Config.Brand b = this.GetBrand(sid);

				foreach(Object i in id)
				{
					HotListedNotifyImpulse imp = new HotListedNotifyImpulse(int.Parse(i.ToString()),
						int.Parse(i.ToString()), b.BrandID);
					YesMailHandler.Instance.SendHotListAlert(imp);
				}
			}										
			
			((Button)sender).ForeColor = Color.Green;
		}
		
		
		private void buttonRegVerification_Click(object sender, System.EventArgs e)
		{
			int[] siteIdList = {101, 112, 15, 13, 6, 103, 4};
			foreach(int si in siteIdList)
			{
				Config.Brand b = this.GetBrand(si);

				foreach(object i in id)
				{
					RegVerificationImpulse imp = new RegVerificationImpulse(int.Parse(i.ToString()), b.BrandID);
					YesMailHandler.Instance.SendActivationLetter(imp);
				}
			}			
			((Button)sender).ForeColor = Color.Green;
		}
		
		private void buttonEmailChangeConfirmation_Click(object sender, System.EventArgs e)
		{
			int[] siteIdList = {101, 112, 15, 13, 6, 103, 4};
			foreach(int si in siteIdList)
			{
				Config.Brand b = this.GetBrand(si);
				foreach(Object i in id)
				{
					EmailVerificationImpulse imp = new EmailVerificationImpulse(int.Parse(i.ToString()), b.BrandID);
					YesMailHandler.Instance.SendEmailChangeConfirmation(imp);
				}
			}
			
			((Button)sender).ForeColor = Color.Green;
		}

		private void buttonMemberToFriend_Click(object sender, System.EventArgs e)
		{
			int[] siteIdList = {15, 4, 101, 112, 13, 6, 103};
			foreach(int si in siteIdList)
			{
				Config.Brand b = this.GetBrand(si);
				foreach(string em in email)
				{
					SendMemberImpulse imp = new SendMemberImpulse(em.Substring(0, em.IndexOf("@")), em, "JOOMAX@JOOMAX.COM", 10000000, 
						"Wanna be my friend?", "CHECK OUT: WWW.JOOMAX.COM", b.BrandID);
					YesMailHandler.Instance.SendToFriend(imp);

				}
			}
			
			((Button)sender).ForeColor = Color.Green;
		}

		private void buttonMessageBoardInstantNotification_Click(object sender, System.EventArgs e)
		{
			int[] siteIdList = {101, 112, 13, 6, 103};
			
			foreach(int si in siteIdList)
			{
				Config.Brand b = this.GetBrand(si);
				foreach(object i in id)
				{
					MessageBoardInstantNotificationImpulse imp = new MessageBoardInstantNotificationImpulse(b.BrandID, si, int.Parse(i.ToString()), "Test board",
						"CHECK OUT WWW.JOOMAX.COM", "testurl", 9,
						"test");
					YesMailHandler.Instance.SendMessageBoardInstantNotification(imp);
				}
			}
			
			((Button)sender).ForeColor = Color.Green;
		}

		
		private void buttonMessageBoardDaily_Click(object sender, System.EventArgs e)
		{
			int[] siteIdList = {101, 112, 13, 6, 103};
			foreach(int si in siteIdList)
			{
				Config.Brand b = this.GetBrand(si);

				foreach(object i in id)
				{
					MessageBoardDailyUpdatesImpulse imp = new MessageBoardDailyUpdatesImpulse(b.BrandID, si, int.Parse(i.ToString()), "JOOMAX BOARD", "CHECK OUT WWW.JOOMAX.COM",
						0, 111, 111, 1, "test url");
					YesMailHandler.Instance.SendMessageBoardDailyUpdates(imp);
				}			
			}
			
			((Button)sender).ForeColor = Color.Green;
		}

		private void buttonEcardToFriend_Click(object sender, System.EventArgs e)
		{
			int[] siteIdList = {101, 13, 6, 103};
			foreach(int si in siteIdList)
			{
				Config.Brand b = this.GetBrand(si);
				foreach(string em in email)
				{
					EcardToFriendImpulse imp = new EcardToFriendImpulse(b.BrandID, si, 100000001, "", "", em, DateTime.Now);
					YesMailHandler.Instance.SendEcardToFriend(imp);
				}
			}
			
			((Button)sender).ForeColor = Color.Green;
		}
		private void buttonEcardToMember_Click(object sender, System.EventArgs e)
		{
			//int[] siteIdList = {101, 13, 6, 103};
			int[] siteIdList = {6};
			foreach(int si in siteIdList)
			{
				Config.Brand b = this.GetBrand(si);
				foreach(int i in id)
				{
					EcardToMemberImpulse imp = new EcardToMemberImpulse(b.BrandID, si, int.Parse(i.ToString()), "", "", int.Parse(i.ToString()), DateTime.Now);
					YesMailHandler.Instance.SendEcardToMember(imp);
				}
			}
			
			((Button)sender).ForeColor = Color.Green;
		}

		private void buttonRenewalFailureNotification_Click(object sender, System.EventArgs e)
		{
			int[] siteIdList = {101, 13, 6, 103};
			foreach(int si in siteIdList)
			{				
				foreach(string em in email)
				{
					int memberId = Member.ServiceAdapters.MemberSA.Instance.GetMemberIDByEmail(em);
					MSA.Member member = Member.ServiceAdapters.MemberSA.Instance.GetMember(memberId, Member.ServiceAdapters.MemberLoadFlags.None);

					RenewalFailureNotificationImpulse imp = new RenewalFailureNotificationImpulse(si, "logysis", em);
					YesMailHandler.Instance.SendRenewalFailureNotification(imp);
					//SoapHandler.Instance.Lather(imp);
				}
			}
			
			((Button)sender).ForeColor = Color.Green;
		}

		
		private void buttonMutualYesNotify_Click(object sender, System.EventArgs e)
		{
			int[] siteIdList = {112, 15, 13, 6, 103, 4, 101};
			foreach(int si in siteIdList)
			{
				Config.Brand b = this.GetBrand(si);
				foreach(object i in id)
				{
					MutualYesNotifyImpulse imp = new MutualYesNotifyImpulse(int.Parse(i.ToString()), int.Parse(i.ToString()), b.BrandID);
					YesMailHandler.Instance.SendMutualMail(imp);
				}			
			}
			
			((Button)sender).ForeColor = Color.Green;
		}

		private void buttonOptOutNotify_Click(object sender, System.EventArgs e)
		{
						
		}

		private void buttonSubscriptionConfirmation_Click(object sender, System.EventArgs e)
		{
			int[] siteIdList = {101, 13, 15, 103, 6, 4};
			foreach(int si in siteIdList)
			{
				Config.Brand b = this.GetBrand(si);
				foreach(string em in email)
				{
					SubscriptionConfirmationImpulse imp = new SubscriptionConfirmationImpulse(100000, si, b.BrandID, em.Substring(0, em.IndexOf("@")), em,
						"steve", "chen", "confidential", "alhambra", "CA", "91803",
						DateTime.Today, 1, 1, 50, 20, Matchnet.DurationType.Month, 100, 10,
						Matchnet.DurationType.Month, "1212", 1, "23rasdfasd");
					YesMailHandler.Instance.SendSubscriptionConfirmation(imp);
				}
			}
			
			((Button)sender).ForeColor = Color.Green;
		}
		private void buttonNewMailAlert_Click(object sender, System.EventArgs e)
		{
            int[] siteIdList = {105};
            //int[] siteIdList = {6, 101};
			foreach(int si in siteIdList)
			{
				foreach(object i in id)
				{	
                    Matchnet.Email.ValueObjects.MessageList list = new Matchnet.Email.ValueObjects.MessageList(1, int.Parse(i.ToString()), 
                        int.Parse(i.ToString()), false, 1, 1, 1, 
                        Matchnet.Email.ValueObjects.MessageStatus.New, 
                        Matchnet.Email.ValueObjects.MailType.Tease, DateTime.Now, 100);
                    /*
                    Matchnet.Email.ValueObjects.MessageList list = new Matchnet.Email.ValueObjects.MessageList(1, int.Parse(i.ToString()), 
						int.Parse(i.ToString()), false, 1, 1, 1, 
						Matchnet.Email.ValueObjects.MessageStatus.New, 
						Matchnet.Email.ValueObjects.MailType.Tease, DateTime.Now, 100);
                    */
					Matchnet.Email.ValueObjects.EmailMessage msg = new Matchnet.Email.ValueObjects.EmailMessage(list);
					InternalMailNotifyImpulse imp = new InternalMailNotifyImpulse(msg, this.GetBrand(si).BrandID, 9);
					YesMailHandler.Instance.SendNewMailAlert(imp);
				}
				((Button)sender).ForeColor = Color.Green;
			}
		}
		


		private void YesMailTest_Load(object sender, System.EventArgs e)
		{
//			this.buttonForgotPwd_Click(this.buttonForgotPwd, EventArgs.Empty);
//			this.buttonPhotoApproval_Click(this.buttonPhotoApproval, EventArgs.Empty);
//			this.buttonPhotoRejected_Click(this.buttonPhotoRejected, EventArgs.Empty);
//			this.buttonHotListedNotify_Click(this.buttonHotListedNotify, EventArgs.Empty);
//
//			this.buttonRegVerification_Click(this.buttonRegVerification, EventArgs.Empty);
//			this.buttonEmailChangeConfirmation_Click(this.buttonEmailChangeConfirmation, EventArgs.Empty);
//			this.buttonMemberToFriend_Click(this.buttonMemberToFriend, EventArgs.Empty);
//			this.buttonMessageBoardInstantNotification_Click(this.buttonMessageBoardInstantNotification, EventArgs.Empty);
//
//			this.buttonMessageBoardDaily_Click(this.buttonMessageBoardDaily, EventArgs.Empty);
//			this.buttonEcardToFriend_Click(this.buttonEcardToFriend, EventArgs.Empty);
//			this.buttonEcardToMember_Click(this.buttonEcardToMember, EventArgs.Empty);
//			this.buttonRenewalFailureNotification_Click(this.buttonRenewalFailureNotification, EventArgs.Empty);
//
//			this.buttonMutualYesNotify_Click(this.buttonMutualYesNotify,EventArgs.Empty);
//			this.buttonSubscriptionConfirmation_Click(this.buttonSubscriptionConfirmation, EventArgs.Empty);
//			this.buttonNewMailAlert_Click(this.buttonNewMailAlert, EventArgs.Empty);


		}
		private Config.Brand GetBrand(int siteId)
		{
			foreach(Config.Brand b in brands)
			{
				if(b.Site.SiteID == siteId)
					return b;
			}
			return null;
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			
		}

        private void btnComments_Click(object sender, System.EventArgs e)
        {
            ExternalMailBL.Instance.Enqueue(new ContactUsImpulse(1003,
                "kguy",
                "jrho@spark.net",
                "100066132",
                "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2; .NET CLR 1.1.4322; .NET CLR 2.0.50727; InfoPath.1)",
                "127.0.0.1",
                "Logging in",
                "change1212"));            
                        
            /*
            Matchnet.ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendContactUs(1105,
                "kguy",
                "jrho@spark.net",
                "100066132",
                "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2; .NET CLR 1.1.4322; .NET CLR 2.0.50727; InfoPath.1)",
                "127.0.0.1",
                "Logging in",
                "change1212");      
            */      
        }

        private void btnEmptyQueue_Click(object sender, System.EventArgs e)
        {
            Metrics.InstallPerformanceCounters();

            Matchnet.ExternalMail.BusinessLogic.QueueProcessing.QueueProcessor _queueProcessor = new Matchnet.ExternalMail.BusinessLogic.QueueProcessing.QueueProcessor();
            _queueProcessor.Start();        
        }

		private void ButtonInternalCorpMail_Click(object sender, System.EventArgs e)
		{
			Matchnet.ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendInternalCorpMail(1003,
				"wlee@spark.net", "wlee@spark.net", "yo mang", "Won", "InternalCorpMail Test~");
		}
	}
}
