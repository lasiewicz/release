using System;
using System.Collections;
using System.IO;
using Matchnet.ExternalMail.BusinessLogic;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;
using Matchnet.Email.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;

namespace Matchnet.ExternalMail.Harness
{
	/// <summary>
	/// Summary description for ConsoleDriver.
	/// </summary>
	public class ConsoleDriver
	{
		#region Configuration Variables

		// a list of brands that emails should be sent for
		//private int[] _brandIDs = { 1002,  1015, 10012 }; // GL CU CL
		private int[] _brandIDs =
		{	
			//1015,		//	Cupid.co.il
			//10012,	//	CollegeLuv.com
			//1002		//	Glimpse.com
			//1003,		//	JDate.com
			1001,		//	AmericanSingles.com
			//1013,		//	Date.ca
			//1006,		//		Date.co.uk
			//11934,	//	DatingResults.com
			//1004			//	JDate.co.il
			//10008,		//	MatchNet.com
			//1007,			//	MatchNet.com.au
			//1450,		//	SilverSingles.com
			//100		//	Spark.com
		};

		// a list of memberid's for users that should be sent emails.
		//private int[] _memberIDs = {  16000206, 100042825,987656484,100021132,100064677,988269718 };
		private int[] _memberIDs = {  16000206, 100042825,987656484,100021132,100064677,988269718 };
		//private int[] _memberIDs = {27029711}; //won's prod
		//	will lybrand		100003172 
		// rafi berkovich	988269718
		// dave kalman		100020769
		// justin gracer	100019971
		// sean smith		49053689 
		//private int[] _memberIDs = { 100013384 };	//	wlybrand@spark.net production
		// hudoba test		100041352, 100041361, 100041367, 100041346, 100019
		
		// a list of target member id's to show up for target users.  [0] is used for any single target emails
		//private int[] _targetMemberIDs = {  16000206, 100042825,987656484,100021132,100064677,988269718 };	//	dev		
		private int[] _targetMemberIDs = {  16000206, 16000206,16000206,16000206,16000206,16000206 };	//	dev	
		private int[] _emailNotificationIDs = {100003348, 100003366 ,100019950 ,100019866 ,100003154 ,100003480,100000943, 100019596, 100002558, 100002620};
		// private int[] _targetMemberIDs = {51770416, 43016993, 49337610, 42478651, 52439971, 42191387     }; // actual stage users
		// private int[] _targetMemberIDs = { 49244844, 10133912, 41098948, 50865823, 49298797, 44050860 };	//	staging
		 
		#endregion

		#region Boolean Internal test values
		
		private bool _testInternalMailNotification = false;
		private bool _testSendMemberToFriend = false;
		private bool _testEmailVerification = false;
		private bool _testSendPassword = false;
		private bool _testPhotoApproval = false;
		private bool _testRegistrationVerification = false;
		private bool _testMatchMail = false;
		private bool _testSendMutualYes = false;
		private bool _testViralMail = false;
		private bool _testPhotoRejection = false;
		private bool _testHotListedAlert = false;
		private bool _testSubscriptionConfirmation = false;
		private bool _testEcardToMember = false;
		private bool _testEcardToFriend = false;

		private bool useBL = false;
		
		#endregion

		#region Boolean Properties

		public bool testEmailVerification
		{
			get{return _testEmailVerification;}
			set{_testEmailVerification = value;}
		}
		public bool testInternalMailNotification
		{
			get {return _testInternalMailNotification;}
			set {_testInternalMailNotification = value;}
		}
		public bool testSendMemberToFriend
		{
			get {return _testSendMemberToFriend;}
			set {_testSendMemberToFriend = value;}
		}
		public bool testSendPassword
		{
			get {return _testSendPassword;}
			set {_testSendPassword = value;}
		}
		public bool testPhotoApproval
		{
			get {return _testPhotoApproval;}
			set {_testPhotoApproval = value;}
		}
		public bool testRegistrationVerification
		{
			get {return _testRegistrationVerification;}
			set {_testRegistrationVerification = value;}
		}
		public bool testMatchMail
		{
			get {return _testMatchMail;}
			set {_testMatchMail = value;}
		}
		public bool testSendMutualYes
		{
			get {return _testSendMutualYes;}
			set {_testSendMutualYes = value;}
		}
		public bool testViralMail
		{
			get {return _testViralMail;}
			set {_testViralMail = value;}
		}
		public bool testPhotoRejection
		{
			get {return _testPhotoRejection;}
			set {_testPhotoRejection = value;}
		}
		public bool testHotListedAlert
		{
			get
			{
				return _testHotListedAlert;
			}
			set
			{
				_testHotListedAlert = value;
			}
		}
		public bool testSubscriptionConfirmation
		{
			get
			{
				return _testSubscriptionConfirmation;
			}
			set
			{
				_testSubscriptionConfirmation = value;
			}
		}
		public bool testEcardToMember
		{
			get
			{
				return _testEcardToMember;
			}
			set
			{
				_testEcardToMember = value;
			}
		}
		public bool testEcardToFriend
		{
			get
			{
				return _testEcardToFriend;
			}
			set
			{
				_testEcardToFriend = value;
			}
		}
		#endregion

		#region Private variables
		private TextWriter _writer;

		private MiniprofileInfo[] _miniprofiles;

		private int memberID = 0;
		private int brandID = 0;
		IMemberDTO member = null;
		Content.ValueObjects.BrandConfig.Brand brand = null;
		ExternalMail.ValueObjects.Miniprofile.MiniprofileInfo miniprofile = null;

		private int sendCount = 0;

		
		private string[] REJECT_REASONS = new string[] {	"Rejected_BadFileFormat",		//	0
														   "Rejected_Copyright",			//	1
														   "Rejected_Blurry",				//	2
														   "Rejected_FileSize",			//	3
														   "Rejected_General",				//	4
														   "Rejected_UnknownMember",		//	5
														   "Rejected_Suggestive" };		//	6

		public TextWriter Writer
		{
			get { return(_writer); }
		}

		public ConsoleDriver(TextWriter writer)
		{
			_writer = writer;
		}
		#endregion

		#region Class master methods
		/// 
		/// The main entry point for the application.
		///     
		static void Main(string[] args)
		{
			Console.WriteLine("Starting ExternalMail / StormPost test run");

			ConsoleDriver myDriver = new ConsoleDriver(Console.Out);
			myDriver.DoRun();

			Console.WriteLine("Hit <ENTER> to exit");
			string str = Console.ReadLine();
		}

		public int DoRun()
		{
			return DoRun(false);
		}

		/// <summary>
		/// Execute a test run
		/// </summary>
		/// <param name="useBL">True - make direct calls to the BL class</param>
		/// <returns>zero (0) if successful, non-zero if not successful</returns>
		public int DoRun(bool useBL)
		{
			this.useBL = useBL;

			int retVal = 0;

			this.SetUpMiniprofileArray();

			// for each member in _memberIDs array
			foreach(int i in _memberIDs)
			{
				// for each brand in _brandIDs array
				foreach(int j in _brandIDs)
				{
					PerformTestRun(i, j);
				}
			}

			return(retVal);
		}

		private int PerformTestRun(int MemberID, int BrandID)
		{
            member = MailHandlerUtils.GetIMemberDTO(MemberID, Member.ServiceAdapters.MemberLoadFlags.None);
			brand = Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(BrandID);

			memberID = MemberID;
			brandID = BrandID;
			
			Member.ValueObjects.Photos.PhotoCommunity photocommunity = member.GetPhotos(brand.Site.Community.CommunityID);
			string filewebpath = String.Empty;
					
			foreach(Member.ValueObjects.Photos.Photo photo in photocommunity)
			{
				if (photo != null)
				{
					filewebpath = photo.ThumbFileWebPath;
				}
			}

			miniprofile = new Matchnet.ExternalMail.ValueObjects.Miniprofile.MiniprofileInfo(
				memberID,
				member.GetUserName(brand),
				member.GetAttributeDate(brand, "BirthDate"),
				member.GetAttributeInt(brand, "RegionID"),
				filewebpath,
				member.GetAttributeText(brand, "AboutMe", String.Empty),
				member.GetAttributeInt(brand, "GenderMask", Constants.NULL_INT), 
				member.GetAttributeInt(brand, "Religion", Constants.NULL_INT), 
				member.GetAttributeInt(brand, "JdateReligion", Constants.NULL_INT), 
				member.GetAttributeInt(brand, "Ethnicity", Constants.NULL_INT), 
				member.GetAttributeInt(brand, "JDateEthnicity", Constants.NULL_INT), 
				member.GetAttributeInt(brand, "MaritalStatus", Constants.NULL_INT), 
				member.GetAttributeInt(brand, "ChildrenCount", Constants.NULL_INT), 
				member.GetAttributeInt(brand, "Custody", Constants.NULL_INT), 
				member.IsPayingMember(brand.Site.SiteID), 
				member.GetAttributeInt(brand, "GlobalStatusMask", Constants.NULL_INT),
				member.HasApprovedPhoto(brand.Site.Community.CommunityID), 
				Constants.NULL_INT,
				Constants.NULL_INT,
				Constants.NULL_INT,
				Constants.NULL_INT,
				Constants.NULL_INT,
				Constants.NULL_INT,
				Constants.NULL_INT);

			Log("Starting test run for MemberID: " + MemberID + " and BrandID: " + BrandID);

			if(_testInternalMailNotification)
			{
				InternalMailNotification();
			}

			if(_testSendMemberToFriend)
			{
				SendMemberToFriend();
			}
			
			if(_testEmailVerification)
			{
				SendEmailVerification();
			}
			
			if(_testSendPassword)
			{
				SendPassword();
			}

			if(_testPhotoApproval)
			{
				SendPhotoApproval();			
			}

			if(_testRegistrationVerification)
			{
				SendRegistrationVerification();
			}

			if(_testMatchMail)
			{
				SendMatchMail();
			}

			if(_testSendMutualYes)
			{
				SendMutualYes();			
			}

			if(_testViralMail)
			{
				SendViralMail();
			}

			if(_testPhotoRejection)
			{
				SendPhotoRejection();		
			}			

			if (_testHotListedAlert)
			{
				SendHotListedAlert();
			}

			if (_testSubscriptionConfirmation)
			{
				SendSubscriptionConfirmation();
			}

			if (_testEcardToMember)
			{
				SendEcardToMember();
			}

			if (_testEcardToFriend)
			{
				SendEcardToFriend();
			}

			Log("Finished test run of " + sendCount + " messages for MemberID: " + memberID + " and BrandID: " + brandID);

			return(0);
		}
		#endregion

		#region Email Send Methods

		private bool SendMail(bool Value, string MailName)
		{
			if(Value)
			{
				sendCount++;
				Log("Sent: " + MailName);
				return true;
			}
			else
			{
				Log("Failed send, blacklisted.  " + MailName + ".");
				return false;
			}
		}

		private void InternalMailNotification()
		{
			try
			{
				foreach(int i in _emailNotificationIDs)
				{
					Int32 messageID = 50000585;
					MessageList messageList = new MessageList(0, i, memberID, false, brandID, (Int32) SystemFolders.Sent, messageID, MessageStatus.Show | MessageStatus.New, MailType.Email, DateTime.Now, 0);
					Message message = new Message(messageID, "Test Subject", "Test Body", DateTime.Now, DateTime.MinValue);
					EmailMessage emailMessage = new EmailMessage(messageList, message);

					SendMail(ExternalMailSA.Instance.SendInternalMailNotification(emailMessage, brandID, 4), "SendInternalMailNotification");
				}
			}
			catch(Exception ex)
			{
				Log("Exception during run for MemberID: " + memberID + " and BrandID: " + brandID
					+ ".  Message: " + ex.Message + ", StackTrace: " + ex.StackTrace);
			}
		}

		private void SendMemberToFriend()
		{
			if(this.brandID == 1015 || this.brandID == 1004)
			{
				try
				{
					//	Hebrew text
					SendMail(
						ExternalMailSA.Instance.SendMemberToFriend("דדדדדדדדדדדדדדדדדדדדדדדד", member.EmailAddress, "sender@spark.net", _targetMemberIDs[0], "send member subject", "send member message", brandID)
						, "SendMemberToFriend");
				}
				catch(Exception ex)
				{
					Log("Exception during run for MemberID: " + memberID + " and BrandID: " + brandID
						+ ".  Message: " + ex.Message + ", StackTrace: " + ex.StackTrace);
				}
			}
			else
			{
				try
				{
					//	English text
					SendMail(
						ExternalMailSA.Instance.SendMemberToFriend("MyFriendName", member.EmailAddress, "sender@spark.net", _targetMemberIDs[0], "send member subject", "send member message", brandID)
						, "SendMemberToFriend");
				}
				catch(Exception ex)
				{
					Log("Exception during run for MemberID: " + memberID + " and BrandID: " + brandID
						+ ".  Message: " + ex.Message + ", StackTrace: " + ex.StackTrace);
				}
			}
		}

		private void SendEmailVerification()
		{
			try
			{
				SendMail(SendMail(ExternalMailSA.Instance.SendEmailVerification(memberID, brandID), "EmailVerification"), "SendEmailVerification");
			}
			catch(Exception ex)
			{
				Log("Exception during run for MemberID: " + memberID + " and BrandID: " + brandID
					+ ".  Message: " + ex.Message + ", StackTrace: " + ex.StackTrace);
			}
		}

		private void SendPassword()
		{
			try
			{
				SendMail(ExternalMailSA.Instance.SendPassword(brandID, member.EmailAddress), "SendPassword");
			}
			catch(Exception ex)
			{
				Log("Exception during run for MemberID: " + memberID + " and BrandID: " + brandID
					+ ".  Message: " + ex.Message + ", StackTrace: " + ex.StackTrace);
			}
		}

		private void SendPhotoApproval()
		{
			try
			{
				SendMail(ExternalMailSA.Instance.SendPhotoApprovedNotification(memberID, 0, brandID, "photo approval approval status"), "SendPhotoApprovedNotification");
			}
			catch(Exception ex)
			{
				Log("Exception during run for MemberID: " + memberID + " and BrandID: " + brandID
					+ ".  Message: " + ex.Message + ", StackTrace: " + ex.StackTrace);
			}
		}

		private void SendRegistrationVerification()
		{
			try
			{
				SendMail(ExternalMailSA.Instance.SendRegistrationVerification(memberID, brandID, 0), "SendRegistrationVerification");
			}
			catch(Exception ex)
			{
				Log("Exception during run for MemberID: " + memberID + " and BrandID: " + brandID
					+ ".  Message: " + ex.Message + ", StackTrace: " + ex.StackTrace);
			}
		}

		private void SendMatchMail()
		{
			try
			{
				if (useBL)
				{
					ExternalMail.ValueObjects.Impulse.MatchMailImpulse matchMailImpulse
						= new ExternalMail.ValueObjects.Impulse.MatchMailImpulse(brandID, member.EmailAddress,
						miniprofile, LimitProfileCollection(_miniprofiles, 6));

					// YesMail BulkMail Test
					Matchnet.ExternalMail.BusinessLogic.YesMailHandler.Instance.SendMatchMail(matchMailImpulse);

					// Stormpost GreyMail Test
					//Matchnet.ExternalMail.BusinessLogic.Composer.Instance.Compose(matchMailImpulse);

					Log("Sent MatchMail (BL) w/ 6 profiles");
				}
				else
				{
					SendMail(ExternalMailSA.Instance.SendMatchMail(brandID, member.EmailAddress, miniprofile, LimitProfileCollection(_miniprofiles, 6)), "SendMatchMail with " + 6 + " profiles");
				}
			}
			catch(Exception ex)
			{
				Log("Exception during run for MemberID: " + memberID + " and BrandID: " + brandID
					+ ".  Message: " + ex.Message + ", StackTrace: " + ex.StackTrace);
			}
		}

		private void SendMutualYes()
		{
			try
			{
				SendMail(ExternalMailSA.Instance.SendMutualYesNotification(memberID, _targetMemberIDs[0], brandID), "MutualYesNotification");
			}
			catch(Exception ex)
			{
				Log("Exception during run for MemberID: " + memberID + " and BrandID: " + brandID
					+ ".  Message: " + ex.Message + ", StackTrace: " + ex.StackTrace);
			}
		}

		private void SendViralMail()
		{
			for(int i = 1; i <= 5; i++)
			{
				try
				{
					ExternalMail.ValueObjects.Impulse.ViralImpulse viralMailImpulse = new Matchnet.ExternalMail.ValueObjects.Impulse.ViralImpulse(
					brandID, member.EmailAddress, miniprofile, LimitProfileCollection(_miniprofiles, 5));

					// YesMail BulkMail Test
					Matchnet.ExternalMail.BusinessLogic.YesMailHandler.Instance.SendClickMail(viralMailImpulse);

					// Stormpost GreyMail Test
					//SendMail(ExternalMailSA.Instance.SendViralEmail(brandID, member.EmailAddress, miniprofile, LimitProfileCollection(_miniprofiles, i)), "SendViralEmail with " + i + " profiles");
				}
				catch(Exception ex)
				{
					Log("Exception during run for MemberID: " + memberID + " and BrandID: " + brandID
						+ ".  Message: " + ex.Message + ", StackTrace: " + ex.StackTrace);
				}
			}
		}

		private void SendPhotoRejection()
		{
			try
			{
				//foreach(string myReason in REJECT_REASONS)
				//{
					SendMail(ExternalMailSA.Instance.SendPhotoRejectedNotification(memberID, 0, brandID, "Rejected_Blurry"), "PhotoRejection");
				//}
			}
			catch(Exception ex)
			{
				Log("Exception during run for MemberID: " + memberID + " and BrandID: " + brandID
					+ ".  Message: " + ex.Message + ", StackTrace: " + ex.StackTrace);
			}
		}

		private void SendHotListedAlert()
		{
			try
			{
				if (useBL)
				{
					ExternalMail.ValueObjects.Impulse.HotListedNotifyImpulse hotListedNotifyImpulse
						= new ExternalMail.ValueObjects.Impulse.HotListedNotifyImpulse(16000206, 16000206, brandID);

					// This is just to DEBUG and write TRACEs. It is not sending anything out.
					Matchnet.ExternalMail.BusinessLogic.MessageInfo msg = Matchnet.ExternalMail.BusinessLogic.Composer.Instance.Compose(hotListedNotifyImpulse);
				}
				else
				{
					SendMail(ExternalMailSA.Instance.SendHotListedNotification(16000206, 16000206, 1001), "HotListed Alert");
					SendMail(ExternalMailSA.Instance.SendHotListedNotification(16000206, 16000206, 1003), "HotListed Alert");
				}
			}
			catch(Exception ex)
			{
				Log("Exception during run for MemberID: " + memberID + " and BrandID: " + brandID
					+ ".  Message: " + ex.Message + ", StackTrace: " + ex.StackTrace);
			}
		}

		private void SendSubscriptionConfirmation()
		{
			try
			{
				if (useBL)
				{
					for (int i = 0; i < _brandIDs.Length; i++)
					{
						Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(_brandIDs[i]);

						ExternalMail.ValueObjects.Impulse.SubscriptionConfirmationImpulse impulse =
							new Matchnet.ExternalMail.ValueObjects.Impulse.SubscriptionConfirmationImpulse(
							16000206,
							brand.Site.SiteID,
							brand.BrandID,
							"won",
							"wlee@spark.net",
							"Won",
							"Lee",
							"690 Catalina",
							"Los Angeles",
							"CA",
							"90005",
							DateTime.Today,
							64,
							1,
							19.99m,
							3,
							Matchnet.DurationType.Month,
							15.99m,
							3,
							Matchnet.DurationType.Month,
							"3456",
							1,
							"767676767633");

						Matchnet.ExternalMail.BusinessLogic.MessageInfo msg = Matchnet.ExternalMail.BusinessLogic.Composer.Instance.Compose(impulse);

					}

				}
				else
				{
					for (int i = 0; i < _brandIDs.Length; i++)
					{
						Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(_brandIDs[i]);

						SendMail(ExternalMailSA.Instance.SendSubscriptionConfirmation(
							16000206,
							brand.Site.SiteID,
							brand.BrandID,
							"won",
							"wlee@spark.net",
							"Won",
							"Lee",
							"690 Catalina",
							"Los Angeles",
							"CA",
							"90005",
							DateTime.Today,
							64,
							1,
							19.99m,
							3,
							Matchnet.DurationType.Month,
							15.99m,
							3,
							Matchnet.DurationType.Month,
							"3456",
							1,
							"767676767633"),
							"Subscription Confirmation ");
					}
				}
			}
			catch(Exception ex)
			{
				Log("Exception during run for MemberID: " + memberID + " and BrandID: " + brandID
					+ ".  Message: " + ex.Message + ", StackTrace: " + ex.StackTrace);
			}
		}

		private void SendEcardToMember()
		{
			const int senderMemberID = 988264912;
			const int recipientMemberID = 988264912;	// Subscribed
			//const int recipientMemberID = 100045465;	// Non-subscribed

			try
			{
				for (int i = 0; i < _brandIDs.Length; i++)
				{
					Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(_brandIDs[i]);

					if (brand.Site.SiteID != 103
						&& brand.Site.SiteID != 101
						&& brand.Site.SiteID != 6
						&& brand.Site.SiteID != 13)
						continue;

					if (useBL)
					{
						ExternalMail.ValueObjects.Impulse.EcardToMemberImpulse impulse = new ExternalMail.ValueObjects.Impulse.EcardToMemberImpulse(brand.BrandID
							, brand.Site.SiteID, senderMemberID
							, "blah/blah.html", "http://www.americansingles.com/Photo9000/2006/03/30/03/125029290.jpg?siteID=101"
							, recipientMemberID, DateTime.Now);

						Matchnet.ExternalMail.BusinessLogic.ExternalMailBL.Instance.Enqueue(impulse);
					}
					else
					{
						SendMail(ExternalMailSA.Instance.SendEcardToMember(brand.BrandID
							, brand.Site.SiteID, senderMemberID
							, "blah/blah.html", "http://www.americansingles.com/Photo9000/2006/03/30/03/125029290.jpg?siteID=101"
							, recipientMemberID
							, DateTime.Now), "EcardToMember");
					}
				}
			}
			catch(Exception ex)
			{
				Log("Exception during run for MemberID: " + memberID + " and BrandID: " + brandID
					+ ".  Message: " + ex.Message + ", StackTrace: " + ex.StackTrace);
			}
		}

		private void SendEcardToFriend()
		{
			const int senderMemberID = 988264912;
			const int recipientMemberID = 988264912;	// Subscribed
			//const int recipientMemberID = 100045465;	// Non-subscribed

			try
			{
				for (int i = 0; i < _brandIDs.Length; i++)
				{
					Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(_brandIDs[i]);

					if (brand.Site.SiteID != 103
						&& brand.Site.SiteID != 101
						&& brand.Site.SiteID != 6
						&& brand.Site.SiteID != 13)
						continue;

					if (useBL)
					{
						ExternalMail.ValueObjects.Impulse.EcardToFriendImpulse impulse = new ExternalMail.ValueObjects.Impulse.EcardToFriendImpulse(brand.BrandID
							, brand.Site.SiteID, senderMemberID
							, "blah/blah.html", "http://www.americansingles.com/Photo9000/2006/03/30/03/125029290.jpg?siteID=101"
							, "dlee@spark.net"
							, DateTime.Now);

						Matchnet.ExternalMail.BusinessLogic.ExternalMailBL.Instance.Enqueue(impulse);
					}
					else
					{
						SendMail(ExternalMailSA.Instance.SendEcardToFriend(brand.BrandID
							, brand.Site.SiteID, senderMemberID
							, "blah/blah.html", "http://www.americansingles.com/Photo9000/2006/03/30/03/125029290.jpg?siteID=101"
							, "dlee@spark.net"
							, DateTime.Now), "EcardToFriend");
					}
				}
			}
			catch(Exception ex)
			{
				Log("Exception during run for MemberID: " + memberID + " and BrandID: " + brandID
					+ ".  Message: " + ex.Message + ", StackTrace: " + ex.StackTrace);
			}
		}


		
		#endregion

		#region Helper methods
		private void Log(string str)
		{
			Writer.WriteLine(str);
		}

		private static int[] LimitArray(int[] theArray, int maxCount)
		{
			int[] retVal = new int[maxCount];

			if(maxCount > theArray.Length)
			{
				throw new Exception("Tried to make an array with " + maxCount 
					+ " elements out of an array that only had " + theArray.Length + " elements");
			}

			for(int i = 0; i < maxCount; i++)
			{
				retVal[i] = theArray[i];
			}

			return(retVal);
		}

		private static MiniprofileInfoCollection LimitProfileCollection(MiniprofileInfo[] theArray, int maxCount)
		{
			MiniprofileInfoCollection retVal = new MiniprofileInfoCollection();

			if(maxCount > theArray.Length)
			{
				throw new Exception("Tried to make an array with " + maxCount 
					+ " elements out of an array that only had " + theArray.Length + " elements");
			}

			for(int i = 0; i < maxCount; i++)
			{
				retVal.Add(theArray[i]);
			}

			return(retVal);
		}

		private void SetUpMiniprofileArray()
		{
			_miniprofiles = new MiniprofileInfo[6];

			for(int i = 0; i < 6; i++)
			{
				Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(_brandIDs[0]);

                IMemberDTO member = MailHandlerUtils.GetIMemberDTO(_targetMemberIDs[i], MemberLoadFlags.IngoreSACache);

				Member.ValueObjects.Photos.PhotoCommunity photocommunity = member.GetPhotos(brand.Site.Community.CommunityID);
				string filewebpath = String.Empty;
					
				foreach(Member.ValueObjects.Photos.Photo photo in photocommunity)
				{
					if (photo != null)
					{
						filewebpath = photo.ThumbFileWebPath;
					}
					else
					{
						filewebpath = Matchnet.EmailLibrary.EmailMemberHelper.DetermineBaseNoPhoto(brand);
					}
				}

				_miniprofiles[i] = new MiniprofileInfo(member.MemberID
					, member.GetUserName(brand)
					, member.GetAttributeDate(brand, "BirthDate")
					, member.GetAttributeInt(brand, "RegionID")
					, filewebpath
					, member.GetAttributeText(brand, "AboutMe"));
			}
		}
		#endregion
	}
}
