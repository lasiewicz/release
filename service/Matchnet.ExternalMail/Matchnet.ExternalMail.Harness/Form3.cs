using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Matchnet.ExternalMail.Harness
{
	/// <summary>
	/// Summary description for Form3.
	/// </summary>
	public class Form3 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button SendEcardToMember;
		private System.Windows.Forms.Button BuildMutualMail;
		private System.Windows.Forms.Button BuildNewMailAlert;
		private System.Windows.Forms.Button BuildHotListedAlert;
		private System.ComponentModel.Container components = null;

		public Form3()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}


		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.SendEcardToMember = new System.Windows.Forms.Button();
			this.BuildMutualMail = new System.Windows.Forms.Button();
			this.BuildNewMailAlert = new System.Windows.Forms.Button();
			this.BuildHotListedAlert = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// SendEcardToMember
			// 
			this.SendEcardToMember.Location = new System.Drawing.Point(16, 16);
			this.SendEcardToMember.Name = "SendEcardToMember";
			this.SendEcardToMember.Size = new System.Drawing.Size(176, 48);
			this.SendEcardToMember.TabIndex = 0;
			this.SendEcardToMember.Text = "SendEcardToMember";
			this.SendEcardToMember.Click += new System.EventHandler(this.SendEcardToMember_Click);
			// 
			// BuildMutualMail
			// 
			this.BuildMutualMail.Location = new System.Drawing.Point(208, 16);
			this.BuildMutualMail.Name = "BuildMutualMail";
			this.BuildMutualMail.Size = new System.Drawing.Size(176, 48);
			this.BuildMutualMail.TabIndex = 1;
			this.BuildMutualMail.Text = "BuildMutualMail";
			this.BuildMutualMail.Click += new System.EventHandler(this.BuildMutualMail_Click);
			// 
			// BuildNewMailAlert
			// 
			this.BuildNewMailAlert.Location = new System.Drawing.Point(16, 88);
			this.BuildNewMailAlert.Name = "BuildNewMailAlert";
			this.BuildNewMailAlert.Size = new System.Drawing.Size(176, 48);
			this.BuildNewMailAlert.TabIndex = 2;
			this.BuildNewMailAlert.Text = "BuildNewMailAlert";
			this.BuildNewMailAlert.Click += new System.EventHandler(this.BuildNewMailAlert_Click);
			// 
			// BuildHotListedAlert
			// 
			this.BuildHotListedAlert.Location = new System.Drawing.Point(208, 91);
			this.BuildHotListedAlert.Name = "BuildHotListedAlert";
			this.BuildHotListedAlert.Size = new System.Drawing.Size(176, 48);
			this.BuildHotListedAlert.TabIndex = 3;
			this.BuildHotListedAlert.Text = "BuildHotListedAlert";
			this.BuildHotListedAlert.Click += new System.EventHandler(this.BuildHotListedAlert_Click);
			// 
			// Form3
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(592, 231);
			this.Controls.Add(this.BuildHotListedAlert);
			this.Controls.Add(this.BuildNewMailAlert);
			this.Controls.Add(this.BuildMutualMail);
			this.Controls.Add(this.SendEcardToMember);
			this.Name = "Form3";
			this.Text = "Form3";
			this.ResumeLayout(false);

		}
		#endregion

		private void SendEcardToMember_Click(object sender, System.EventArgs e)
		{
			Matchnet.ExternalMail.ValueObjects.Impulse.EcardToMemberImpulse impulse = new Matchnet.ExternalMail.ValueObjects.Impulse.EcardToMemberImpulse(1001, 
						101, 100045255, "", "", 100045813, DateTime.Now);

			Matchnet.ExternalMail.BusinessLogic.SoapHandler.Instance.Lather(impulse);
		}

		private void BuildMutualMail_Click(object sender, System.EventArgs e)
		{
		
		}

		private void BuildNewMailAlert_Click(object sender, System.EventArgs e)
		{
		
		}

		private void BuildHotListedAlert_Click(object sender, System.EventArgs e)
		{
		
		}
	}
}
