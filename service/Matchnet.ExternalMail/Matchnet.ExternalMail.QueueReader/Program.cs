﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Newtonsoft.Json;

namespace Matchnet.ExternalMail.QueueReader
{
    class Program
    {
        static void Main(string[] args)
        {
            var queuePath = args[0].Contains(@"\")?args[0]:args[0] +  @"\private$\ExternalMail_soap_noretry";

            var mainQueue = new MessageQueue(queuePath);
            mainQueue.Formatter = new BinaryMessageFormatter();
            var messages = mainQueue.GetAllMessages();
            foreach(var message in messages)
            {
                Console.WriteLine("");
                Console.WriteLine(JsonConvert.SerializeObject(message.Body ?? "(null)"));
            } 
            Console.ReadLine();
        }
    }
}
