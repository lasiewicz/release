﻿using System;
using System.Collections;
using System.Collections.Generic;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.ExternalMail.MsTests
{
    [TestClass]
    public class jdate_co_il_ExternalMailSMTests
    {
   
        private int _brandId = 1004;
        private int _siteId = 4;
        private string _loginBaseUrl = "https://login.jdate.co.il";
        private readonly EmailGenerator _emailGenerator;
        
        // CONS-4016
        
        public jdate_co_il_ExternalMailSMTests()
        {
            _emailGenerator = new EmailGenerator(TargetConfigurationProvider.Instance.Get(Site.JDateCoIl, Environment.Stage, TargetType.TechnicalQA), _brandId, _siteId, _loginBaseUrl, 2265);
        }

        [TestMethod]
        public void ForgotPasswordSA()
        {
            _emailGenerator.ForgotPassword();
        }

        [TestMethod]
        public void SubscriptionConfirmation()
        {
            _emailGenerator.SubscriptionConfirmation();
        }

        [TestMethod]
        public void NewMailAlert()
        {
            _emailGenerator.NewMailAlert();
        }

        [TestMethod]
        public void NoEssayRequestEmail()
        {
            _emailGenerator.NoEssayRequestEmail();
        }

        [TestMethod]
        public void SendMemberQuestionAndAnswer()
        {

            _emailGenerator.SendMemberQuestionAndAnswer();
        }

        [TestMethod]
        public void EmailChangeConfirmation()
        {
            _emailGenerator.EmailChangeConfirmation();
        }


        [TestMethod]
        public void ViewedYouEmail()
        {
            _emailGenerator.ViewedYouEmail();
        }

        [TestMethod]
        public void EcardToMember()
        {
            _emailGenerator.EcardToMember();
        }

        [TestMethod]
        public void EcardToFriend()
        {
            _emailGenerator.EcardToFriend();
        }

        [TestMethod]
        public void PhotoCaptionRejection()
        {
            _emailGenerator.PhotoCaptionRejection();
        }

        [TestMethod]
        public void HotListedAlert()
        {
            _emailGenerator.HotListedAlert();
        }

        [TestMethod]
        public void MutualMailSA()
        {
            _emailGenerator.MutualMail();
        }

        [TestMethod]
        public void AllAccessReturnEmail()
        {
            _emailGenerator.AllAccessReturnEmail();
        }

        [TestMethod]
        public void PhotoApproval()
        {
            _emailGenerator.PhotoApproval();
        }

        [TestMethod]
        public void ActivationLetter()
        {
            _emailGenerator.ActivationLetter();
        }

        [TestMethod]
        public void SendToFriend()
        {
            _emailGenerator.SendToFriend();
        }
    }
}
