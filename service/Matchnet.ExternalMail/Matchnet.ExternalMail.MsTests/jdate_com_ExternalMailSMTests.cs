﻿using System;
using System.Collections;
using System.Collections.Generic;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.ExternalMail.MsTests
{
    [TestClass]
    public class jdate_com_ExternalMailSMTests
    {

        private int _brandId = 1003;
        private int _siteId = 103;
        private string _loginBaseUrl = "https://login.jdate.com";
        private readonly EmailGenerator _emailGenerator;

        public jdate_com_ExternalMailSMTests()
        {
            _emailGenerator = new EmailGenerator(TargetConfigurationProvider.Instance.Get(Site.JDateCom, Environment.Stage, TargetType.Developer), _brandId, _siteId, _loginBaseUrl, 2265);
        }

        /*
         * 
         * CONS-3836 
         * 
         * 
         */

        [TestMethod]
        public void ActivationLetter()
        {
            _emailGenerator.ActivationLetter();
        }

        [TestMethod]
        public void SubscriptionConfirmation()
        {
            _emailGenerator.SubscriptionConfirmation();
        }

        [TestMethod]
        public void ForgotPassword()
        {
            _emailGenerator.ForgotPassword();
        }

        [TestMethod]
        public void EmailChangeConfirmation()
        {
            _emailGenerator.EmailChangeConfirmation();
        }

        [TestMethod]
        public void NewMailAlert()
        {
            _emailGenerator.NewMailAlert();
        }

        [TestMethod]
        public void PhotoApproval()
        {
            _emailGenerator.PhotoApproval();
        }

        [TestMethod]
        public void PhotoRejection()
        {
            _emailGenerator.PhotoRejection();
        }

        [TestMethod]
        public void PhotoCaptionRejection()
        {
            _emailGenerator.PhotoCaptionRejection();
        }

        [TestMethod]
        public void MutualMail()
        {
            _emailGenerator.MutualMail();
        }

        [TestMethod]
        public void ViewedYouEmail()
        {
            _emailGenerator.ViewedYouEmail();
        }

        [TestMethod]
        public void SodbPhotoUpload()
        {
            _emailGenerator.SodbPhotoUpload();
        }

        [TestMethod]
        public void AllAccessReturnEmail()
        {
            _emailGenerator.AllAccessReturnEmail();
        }

        /*
         * 
         * CONS-3837, CONS-3838, CONS-3839, CONS-3840, CONS-3841 (disabled)
         * 
         * 
         */

        [TestMethod]
        public void ProfileChangedConfirmation()
        {
            _emailGenerator.ProfileChangedConfirmation();
        }

        [TestMethod]
        public void AllAccessInitialEmail()
        {
            _emailGenerator.AllAccessInitialEmail();
        }

        [TestMethod]
        public void AllAccessNudgeEmail()
        {
            _emailGenerator.AllAccessNudgeEmail();
        }

        [TestMethod]
        public void AllAccessReplyEmail()
        {
            _emailGenerator.AllAccessReplyEmail();
        }
    }
}
