﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.ExternalMail.MsTests
{
    enum Site
    {
        JDateCom,
        JDateCoIl,
        JDateCoUk,
        JDateCoFr,
        CupidCoIl,
        SparkCom,
        BlackSinglesCom
    }

    enum Target
    {
        Nick,
        Allyson,
        Rebekah,
        Don,
        Yossi,
        Einat,
        ASPARKX5_GMAIL_COM,
        ASPARKX_2013_YAHOO_COM,
        ASPARKX_2013_HOTMAIL_COM,
        ASPARKX7_WALLA_COM
    }

    enum TargetType
    {
        Developer,
        CreativeQA,
        TechnicalQA
    }

    enum Environment
    {
        Dev,
        Stage,
        Prod
    }

    class TargetConfiguration
    {
        public Target Target { get; set; }
        public TargetType TargetType { get; set; }
        public Environment Environment { get; set; }
        public string Email { get; set; }
        public int MemberId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int MailId { get; set; }
        public int OtherMemberId { get; set; }
        public string SecondEmail { get; set; }
        public string SecondUsername { get; set; }
        public IEnumerable<Site> Sites { get; set; }

    }

    class Emails
    {
        public const string NICK = "nrubell@spark.net";
        public const string ALLYSON = "alutz@spark.net";
        public const string REBEKAH = "rreid@spark.net";
        public const string DON = "dcarmichael@spark.net";
        public const string YOSSI = "ykoka@spark.net";
        public const string EINAT = "emichaeli@spark.net";
        public const string ASPARKX5_GMAIL_COM = "asparkx5@gmail.com";
        public const string ASPARKX_2013_YAHOO_COM = "asparkx2013@yahoo.com";
        public const string ASPARKX_2013_HOTMAIL_COM = "asparkx2013@hotmail.com";
        public const string ASPARKX7_WALLA_COM = "asparkx7@walla.com";


    }

    class MemberIds
    {
        public const int NICK_DEV = 101805229;
        public const int OTHER_DEV = 10059924;

        public const int NICK_STAGE = 1127808615;
        public const int ALLYSON_STAGE = 1127809996;
        public const int REBEKAH_STAGE = 1127816859;
        public const int DON_STAGE = 116688729;
        public const int YOSSI_STAGE = 1127779878;
        public const int EINAT_STAGE = 1127813356;

        public const int ASPARKX5_GMAIL_COM_STAGE = 109584459;
        public const int ASPARKX_2013_YAHOO_COM_STAGE = 1127796227;
        public const int ASPARKX_2013_HOTMAIL_COM_STAGE = 1127799444;
        public const int ASPARKX7_WALLA_COM_STAGE = 1127796230;


    }

    class Sites
    {
        public static IEnumerable<Site> All
        {
            get {
                return new List<Site>()
                {
                    Site.BlackSinglesCom,
                    Site.CupidCoIl,
                    Site.JDateCoIl,
                    Site.JDateCoFr,
                    Site.JDateCoUk,
                    Site.JDateCom,
                    Site.SparkCom
                };
            }
        }
        public static IEnumerable<Site> La
        {
            get
            {
                return new List<Site>()
                {
                    Site.BlackSinglesCom,
                    Site.JDateCoUk,
                    Site.JDateCom,
                    Site.SparkCom,
                    Site.JDateCoFr
                };
            }
        }
        public static IEnumerable<Site> Il
        {
            get
            {
                return new List<Site>()
                {
                    Site.CupidCoIl,
                    Site.JDateCoIl,
                    Site.JDateCoFr
                };
            }
        }
    }

    class TargetConfigurationsData
    {
        public static List<TargetConfiguration> GetAll()
        {
            return new List<TargetConfiguration>()
            {
                new TargetConfiguration() {Target = Target.Nick, TargetType = TargetType.Developer, Environment = Environment.Dev, Email = Emails.NICK, FirstName = "Nick", LastName = "Rubell", Username = "nick123", MailId = 562657690, MemberId = MemberIds.NICK_DEV, OtherMemberId = MemberIds.OTHER_DEV, SecondEmail = Emails.ALLYSON, SecondUsername = "allyson123", Sites = Sites.All},
                new TargetConfiguration() {Target = Target.Nick, TargetType = TargetType.Developer, Environment = Environment.Stage, Email = Emails.NICK, FirstName = "Nick", LastName = "Rubell", Username = "nick123", MailId = 562657690, MemberId = MemberIds.NICK_STAGE, OtherMemberId = MemberIds.ALLYSON_STAGE, SecondEmail = Emails.ALLYSON, SecondUsername = "allyson123", Sites = Sites.All},

                new TargetConfiguration() {Target = Target.Allyson, TargetType = TargetType.CreativeQA, Environment = Environment.Stage, Email = Emails.ALLYSON, FirstName = "Allyson", LastName = "Lutz", Username = "allyson123", MailId = 628430562, MemberId = MemberIds.ALLYSON_STAGE, OtherMemberId = MemberIds.NICK_STAGE, SecondEmail = Emails.NICK, SecondUsername = "nick123", Sites = Sites.La},
                new TargetConfiguration() {Target = Target.Rebekah, TargetType = TargetType.CreativeQA, Environment = Environment.Stage, Email = Emails.REBEKAH, FirstName = "Rebekah", LastName = "Reid", Username = "rebekah123", MailId = 628430562, MemberId = MemberIds.REBEKAH_STAGE, OtherMemberId = MemberIds.NICK_STAGE, SecondEmail = Emails.NICK, SecondUsername = "nick123", Sites = Sites.La},
                new TargetConfiguration() {Target = Target.Don, TargetType = TargetType.TechnicalQA, Environment = Environment.Stage, Email = Emails.DON, FirstName = "Don", LastName = "Carmichael", Username = "don123", MailId = 562661566, MemberId = MemberIds.DON_STAGE, OtherMemberId = MemberIds.NICK_STAGE, SecondEmail = Emails.NICK, SecondUsername = "nick123", Sites = Sites.La},

                new TargetConfiguration() {Target = Target.Yossi, TargetType = TargetType.CreativeQA, Environment = Environment.Stage, Email = Emails.YOSSI, FirstName = "Yossi", LastName = "K", Username = "yossi123", MailId = 562661566, MemberId = MemberIds.YOSSI_STAGE, OtherMemberId = MemberIds.NICK_STAGE, SecondEmail = Emails.NICK, SecondUsername = "nick123", Sites = Sites.Il},
                new TargetConfiguration() {Target = Target.Einat, TargetType = TargetType.CreativeQA, Environment = Environment.Stage, Email = Emails.EINAT, FirstName = "Einat", LastName = "Michaeli", Username = "einat123", MailId = 562661566, MemberId = MemberIds.EINAT_STAGE, OtherMemberId = MemberIds.NICK_STAGE, SecondEmail = Emails.NICK, SecondUsername = "nick123", Sites = Sites.Il},

                new TargetConfiguration() {Target = Target.ASPARKX5_GMAIL_COM, TargetType = TargetType.TechnicalQA, Environment = Environment.Stage, Email = Emails.ASPARKX5_GMAIL_COM, FirstName = "ASPARKX5", LastName = "GMAIL_COM", Username = "ASPARKX5_GMAIL_COM", MailId = 562661566, MemberId = MemberIds.ASPARKX5_GMAIL_COM_STAGE, OtherMemberId = MemberIds.NICK_STAGE, SecondEmail = Emails.NICK, SecondUsername = "nick123", Sites = Sites.Il},
                new TargetConfiguration() {Target = Target.ASPARKX_2013_YAHOO_COM, TargetType = TargetType.TechnicalQA, Environment = Environment.Stage, Email = Emails.ASPARKX_2013_YAHOO_COM, FirstName = "EiASPARKX_2013", LastName = "YAHOO_COM", Username = "ASPARKX_2013_YAHOO_COM", MailId = 562661566, MemberId = MemberIds.ASPARKX_2013_YAHOO_COM_STAGE, OtherMemberId = MemberIds.NICK_STAGE, SecondEmail = Emails.NICK, SecondUsername = "nick123", Sites = Sites.Il},
                new TargetConfiguration() {Target = Target.ASPARKX_2013_HOTMAIL_COM, TargetType = TargetType.TechnicalQA, Environment = Environment.Stage, Email = Emails.ASPARKX_2013_HOTMAIL_COM, FirstName = ".ASPARKX_2013", LastName = "HOTMAIL_COM", Username = "ASPARKX_2013_HOTMAIL_COM", MailId = 562661566, MemberId = MemberIds.ASPARKX_2013_HOTMAIL_COM_STAGE, OtherMemberId = MemberIds.NICK_STAGE, SecondEmail = Emails.NICK, SecondUsername = "nick123", Sites = Sites.Il},
                new TargetConfiguration() {Target = Target.ASPARKX7_WALLA_COM, TargetType = TargetType.TechnicalQA, Environment = Environment.Stage, Email = Emails.ASPARKX7_WALLA_COM, FirstName = "ASPARKX7", LastName = "WALLA_COM", Username = "ASPARKX7_WALLA_COM", MailId = 562661566, MemberId = MemberIds.ASPARKX7_WALLA_COM_STAGE, OtherMemberId = MemberIds.NICK_STAGE, SecondEmail = Emails.NICK, SecondUsername = "nick123", Sites = Sites.Il},

            };
        } 
    }



    class TargetConfigurationProvider
    {

        public static TargetConfigurationProvider Instance { get { return _instance; } }

        static readonly TargetConfigurationProvider _instance = new TargetConfigurationProvider();

        private readonly List<TargetConfiguration> _targetConfigurations = TargetConfigurationsData.GetAll();

        public IEnumerable<TargetConfiguration> Get(Site site, Environment environment, TargetType targetType)
        {
            return
                _targetConfigurations.Where(
                    x => x.Sites.Contains(site) && x.Environment == environment && x.TargetType == targetType);
        }
        
        public IEnumerable<TargetConfiguration> Get(Site site, Environment environment, TargetType targetType, Target target)
        {
            return
                _targetConfigurations.Where(
                    x => x.Sites.Contains(site) && x.Environment == environment && x.TargetType == targetType && x.Target == target);
        }
    
        public IEnumerable<TargetConfiguration> Get(Site site, Environment environment, Target target)
        {
            return
                _targetConfigurations.Where(
                    x => x.Sites.Contains(site) && x.Environment == environment && x.Target == target);
        }
    }
}
