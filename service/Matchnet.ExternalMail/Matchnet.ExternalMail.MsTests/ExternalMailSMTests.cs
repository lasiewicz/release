﻿using System;
using System.Collections;
using System.Collections.Generic;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.ExternalMail.MsTests
{
    [TestClass]
    public class ExternalMailSMTests
    {

        private int _brandId = 1003;
        private int _siteId = 103;

        // Nick (Stage)
        //private string _email = "nrubell@spark.net";
        //private int _memberId = 1127808615; // STAGE: 1127808615; // PROD: 151216048; //DEV: 101805229;
        //private string _username = "nrubell";
        //private string _firstName = "Nick";
        //private string _lastName = "Rubell";
        //private int _mailId = 562657690;
        //private int _otherMemberId = 1127809996;   //some test: 1127798264;/// 1127798264;//150487264;///1127808615;

        //// Dev
        private string _email = "nrubell@spark.net";
        private int _memberId = 101805229; // STAGE: 1127808615; // PROD: 151216048; //DEV: 101805229;
        private string _username = "nrubell";
        private string _firstName = "Nick";
        private string _lastName = "Rubell";
        private int _mailId = 562657690;
        private int _otherMemberId = 10059924;   //some test: 1127798264;/// 1127798264;//150487264;///1127808615;


        //849298953; //[mnImail16]
        //// DEV
        //private string _email = "nrubell@spark.net";
        //private int _memberId = 101805229; // STAGE: 1127808615; // PROD: 151216048; //DEV: 101805229;
        //private string _username = "nrubell";
        //private string _firstName = "Nick";
        //private string _lastName = "Rubell";
        //private int _mailId = 849298953; //[mnImail15]


        //// Allyson (Stage)
        //private string _email = "alutz@spark.net";
        //private int _memberId = 1127809996;////STAGE: 1127809996; ///150487264 i
        //private string _username = "Allyson123";
        //private string _firstName = "Allyson";
        //private string _lastName = "Lutz";
        //private int _mailId = 628430562;//849298953; //[mnImail16]
        // Don (Stage)
        //private string _email = "dcarmichael@spark.net";
        //private int _memberId = 116688729;
        //private string _username = "Don123";
        //private string _firstName = "Don";
        //private string _lastName = "Carmichael";

        // Stage:
        // Male: 1127812806
        // Female: 1127798264
        // Prod: 
        // Male: 150487264 
        // Female: 1127808615



        [TestMethod]
        public void TestMethod1()
        {
        }

        [TestMethod]
        public void MaropostRegistrationEmailViaSA()
        {
            var res = ExternalMailSA.Instance.SendRegistrationVerification(_memberId, _brandId, _siteId);
        }

        [TestMethod]
        public void SubscriptionConfirmationSA()
        {
            var res = ExternalMailSA.Instance.SendSubscriptionConfirmation(_memberId, _siteId, _brandId,
                _username, _email, _firstName, _lastName, "123 Address", "City1", "ST", "9000", DateTime.Today,
                            64,
                            1,
                            19.99m,
                            3,
                            Matchnet.DurationType.Month,
                            15.99m,
                            3,
                            Matchnet.DurationType.Month,
                            "3456",
                            1,
                            "767676767633");
        }
        [TestMethod]
        public void ResetPasswordSA()
        {
            var res = ExternalMailSA.Instance.ResetPassword(_brandId, _memberId, "http://www.jdate.com/?");
        }
        [TestMethod]
        public void EmailChangeConfirmationSA()
        {
            var res = ExternalMailSA.Instance.SendEmailVerification(_memberId, _brandId, false);
        }
        [TestMethod]
        public void NewMailAlertSA()
        {
            // Can be anything; Only memberId matters
            var messageList = new MessageList(1, _memberId, _memberId, true, 1, 1, 1, MessageStatus.New, MailType.Email, DateTime.Now, 100);
            var message = new EmailMessage(messageList);
            var res = ExternalMailSA.Instance.SendInternalMailNotification(message, _brandId, 4);
        }
        [TestMethod]
        public void PhotoApprovalSA()
        {
            var res = ExternalMailSA.Instance.SendPhotoApprovedNotification(_memberId, 1, _brandId, "Approved");
        }
        [TestMethod]
        public void PhotoRejectionSA()
        {
            var res = ExternalMailSA.Instance.SendPhotoRejectedNotification(_memberId, 1, _brandId, "Rejected test");
        }
        [TestMethod]
        public void PhotoCaptionRejectionSA()
        {
            var res = ExternalMailSA.Instance.SendPhotoCaptionRejectNotification(_memberId, 1, _brandId);
        }
        [TestMethod]
        public void MutualMailSA()
        {
            var res = ExternalMailSA.Instance.SendMutualYesNotification(_memberId, 1127798264, _brandId);
        }
        [TestMethod]
        public void ViewedYouEmailSA()
        {
            var res = ExternalMailSA.Instance.SendViewedYouEmail(_memberId, _memberId, 19, _brandId);
        }
        [TestMethod]
        public void SodbPhotoUploadSA()
        {
            ExternalMailSA.Instance.SendSodbPhotoUploadEmail(_memberId, _brandId, _email, false);
            ExternalMailSA.Instance.SendSodbPhotoUploadEmail(_memberId, _brandId, _email, true);
        }
        [TestMethod]
        public void AllAccessReturnEmailSA()
        {


            var res = ExternalMailSA.Instance.SendAllAccessReturnEmail(_memberId, _brandId, _otherMemberId, _brandId);
        }
        [TestMethod]
        public void ProfileChangedConfirmationTestSA()
        {

            ExternalMailSA.Instance.SendProfileChangedConfirmationEmail(_memberId, _brandId, _email, _firstName, _username, new List<string>()
                {
                    "Occupation", "Some Other Field"
                });

        }
        [TestMethod]
        public void AllAccessInitialEmailTestSA()
        {

            ExternalMailSA.Instance.SendAllAccessInitialEmail(_otherMemberId, _brandId, 0, _memberId, _brandId, "Sed ut perspiciatis unde omnis", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.");

        }
        [TestMethod]
        public void AllAccessNudgeEmailTestSA()
        {
            //var imp = new AllAccessEmailNudgeImpulse(_memberId, _brandId, _mailId, _otherMemberId, _brandId, "Subject", "Content", DateTime.Now);
            //MaropostHandler.Instance.SendEmail(imp);
            ExternalMailSA.Instance.SendAllAccessNudgeEmail(_otherMemberId, _brandId, _mailId, _memberId, _brandId, "Subject", "Content", DateTime.Now);
        }
        [TestMethod]
        public void AllAccessReplyEmailTestSA()
        {
            ExternalMailSA.Instance.SendAllAccessReplyToInitialEmail(_otherMemberId, _brandId, 0, _memberId, _brandId, "Subject", "Content");
        }
        [TestMethod]
        public void RegRecaptureTestSA()
        {
            ExternalMailSA.Instance.SendScheduledEmail(0, _memberId, _brandId, "sch name", "ffffs", _email, new Hashtable());
        }
    }
}
