﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;

namespace Matchnet.ExternalMail.MsTests
{
    class EmailGenerator
    {
        private List<TargetConfiguration> _configurations;
        private int _brandId;
        private int _siteId;
        private string _loginBaseUrl;
        private int _questionId;

        public EmailGenerator(IEnumerable<TargetConfiguration> configurations, int brandId, int siteId, string loginBaseUrl, int questionId)
        {
            _configurations = configurations.ToList();
            _brandId = brandId;
            _siteId = siteId;
            _loginBaseUrl = loginBaseUrl;
            _questionId = questionId;
        }

        public void ActivationLetter()
        {
            _configurations.ToList().ForEach(x => ExternalMailSA.Instance.SendRegistrationVerification(x.MemberId, _brandId, _siteId));
        }


        public void SubscriptionConfirmation()
        {
            _configurations.ToList().ForEach(
                x => ExternalMailSA.Instance.SendSubscriptionConfirmation(x.MemberId, _siteId, _brandId,
                    x.Username, x.Email, x.FirstName, x.LastName, "123 Address", "City1", "ST", "9000", DateTime.Today,
                            64,
                            1,
                            19.99m,
                            3,
                            Matchnet.DurationType.Month,
                            15.99m,
                            3,
                            Matchnet.DurationType.Month,
                            "3456",
                            1,
                            "767676767633")
                    );
        }

        public void ForgotPassword()
        {
            _configurations.ToList().ForEach(
                x => ExternalMailSA.Instance.ResetPassword(_brandId, x.MemberId, _loginBaseUrl +   "/password/reset/jdatecom?resettoken=43ed6ddd-7be7-4ffe-85cd-3710278d58b0&mid=113804686&clientid=1003&loginsessionid=52157bde-c29e-49f7-9c07-ada1907e0bb7")
                );
        }

        public void EmailChangeConfirmation()
        {
            _configurations.ToList().ForEach(x => ExternalMailSA.Instance.SendEmailVerification(x.MemberId, _brandId, false));
        }

        public void NewMailAlert()
        {
            // Can be anything; Only memberId matters
            _configurations.ToList().ForEach(x =>
            {
                var messageList = new MessageList(1, x.MemberId, x.MailId, true, 1, 1, 1, MessageStatus.New,
                    MailType.Email, DateTime.Now, 100);
                var message = new EmailMessage(messageList);
                var res = ExternalMailSA.Instance.SendInternalMailNotification(message, _brandId, 4);
            });
        }

        public void PhotoApproval()
        {
            _configurations.ToList().ForEach(x => ExternalMailSA.Instance.SendPhotoApprovedNotification(x.MemberId, 1, _brandId, "Approved"));
        }

        public void PhotoRejection()
        {
            _configurations.ToList().ForEach(x => ExternalMailSA.Instance.SendPhotoRejectedNotification(x.MemberId, 1, _brandId, "Rejected test"));
        }

        public void PhotoCaptionRejection()
        {
            _configurations.ToList().ForEach(x => ExternalMailSA.Instance.SendPhotoCaptionRejectNotification(x.MemberId, 1, _brandId));
        }

        public void MutualMail()
        {
            _configurations.ToList().ForEach(x => ExternalMailSA.Instance.SendMutualYesNotification(x.MemberId, x.OtherMemberId, _brandId));
        }

        public void ViewedYouEmail()
        {
            _configurations.ToList().ForEach(x => ExternalMailSA.Instance.SendViewedYouEmail(x.MemberId, x.OtherMemberId, 19, _brandId));
        }

        public void SodbPhotoUpload()
        {
            _configurations.ToList().ForEach(x =>
            {
                ExternalMailSA.Instance.SendSodbPhotoUploadEmail(x.MemberId, _brandId, x.Email, false);
                ExternalMailSA.Instance.SendSodbPhotoUploadEmail(x.MemberId, _brandId, x.Email, true);
            });
        }

        public void AllAccessReturnEmail()
        {
            _configurations.ToList().ForEach(x => ExternalMailSA.Instance.SendAllAccessReturnEmail(x.MemberId, _brandId, x.OtherMemberId, _brandId));
        }

        public void ProfileChangedConfirmation()
        {
            _configurations.ToList().ForEach(x=> 
                ExternalMailSA.Instance.SendProfileChangedConfirmationEmail(x.MemberId, _brandId, x.Email, x.FirstName, x.Username, new List<string>()
                    {
                        "Occupation", "Some Other Field"
                    }));
        }


        public void AllAccessInitialEmail()
        {
            _configurations.ToList().ForEach(x=> 
                ExternalMailSA.Instance.SendAllAccessInitialEmail(x.OtherMemberId, _brandId, 0, x.MemberId, _brandId, "Sed ut perspiciatis unde omnis", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.")
                );
        }

        public void AllAccessNudgeEmail()
        {
            _configurations.ToList().ForEach(x=> 
                ExternalMailSA.Instance.SendAllAccessNudgeEmail(x.OtherMemberId, _brandId, x.MailId, x.MemberId, _brandId, "Subject", "Content", DateTime.Now)
                );
        }

        public void AllAccessReplyEmail()
        {
            _configurations.ToList().ForEach(x=> ExternalMailSA.Instance.SendAllAccessReplyToInitialEmail(x.OtherMemberId, _brandId, 0, x.MemberId, _brandId, "Subject", "Content")
                );
        }

        public void NoEssayRequestEmail()
        {
            _configurations.ToList().ForEach(x=> 
                ExternalMailSA.Instance.SendNoEssayRequestEmail(x.MemberId, x.OtherMemberId, _brandId)
                );
        }

        public void SendMemberQuestionAndAnswer()
        {

            _configurations.ToList().ForEach(x => 
                ExternalMailSA.Instance.SendMemberQuestion("My Friend", x.Email, x.SecondEmail, x.FirstName, x.MemberId, "This Subject", "This Message", _brandId, _questionId, "Waht do you know about question text")
                );
        }

        public void EcardToMember()
        {
            _configurations.ToList().ForEach(x => 
                ExternalMailSA.Instance.SendEcardToMember(_brandId, _siteId, x.OtherMemberId, "http://www.google.com", "https://www.petfinder.com/wp-content/uploads/2012/11/dog-how-to-select-your-new-best-friend-thinkstock99062463.jpg", x.MemberId, DateTime.UtcNow)
                );
        }

        public void EcardToFriend()
        {
            _configurations.ToList().ForEach(x => 
                ExternalMailSA.Instance.SendEcardToFriend(_brandId, _siteId, x.OtherMemberId, "http://www.google.com", "https://www.petfinder.com/wp-content/uploads/2012/11/dog-how-to-select-your-new-best-friend-thinkstock99062463.jpg", x.Email, DateTime.UtcNow)
                );
        }

        public void HotListedAlert()
        {
            _configurations.ToList().ForEach(x => 
                ExternalMailSA.Instance.SendHotListedNotification(x.MemberId, x.OtherMemberId, _brandId)
                );
        }

        public void SendToFriend()
        {
            _configurations.ToList().ForEach(x => 
                ExternalMailSA.Instance.SendMemberToFriend("Friend", x.Email, x.SecondEmail, x.MemberId, "Subject", "Message", _brandId, x.OtherMemberId)
                );
        }

        public void ColorCodeQuizConfirmation()
        {
            _configurations.ToList().ForEach(x =>
                ExternalMailSA.Instance.SendColorCodeQuizConfirmation(x.MemberId, x.Email, _brandId, "Red", x.Username)
                );
        }

        public void SendMemberColorCodeQuizInvite()
        {
            _configurations.ToList().ForEach(x =>
                ExternalMailSA.Instance.SendColorCodeQuizInvite(x.Email, x.Username, x.SecondEmail, x.OtherMemberId, x.OtherMemberId.ToString(), _brandId)
                );
        }

        public void MatchMeterCongratsMail()
        {
            _configurations.ToList().ForEach(x =>
                ExternalMailSA.Instance.SendMatchMeterCongratsMail(_brandId, _siteId, x.OtherMemberId, x.OtherMemberId.ToString(), x.Email)
                );
        }

        public void MatchMeterInvitationMail()
        {
            _configurations.ToList().ForEach(x =>
                ExternalMailSA.Instance.SendMatchMeterInvitationMail(_brandId, _siteId, x.OtherMemberId, x.OtherMemberId.ToString(), x.SecondEmail, x.Email)
                );
        }

        public void SendMemberColorCode()
        {
            _configurations.ToList().ForEach(x =>
                ExternalMailSA.Instance.SendColorCodeToFriend(x.FirstName,x.Email,x.SecondEmail,x.OtherMemberId.ToString(),x.OtherMemberId, "Subject Lorem Ipsum","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",_brandId)
                );
        }



    }
}
