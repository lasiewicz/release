﻿using System;
using System.Collections;
using System.Collections.Generic;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.ExternalMail.MsTests
{
    [TestClass]
    public class jdate_co_fr_ExternalMailSMTests
    {

        private int _brandId = 1105;
        private int _siteId = 105;

        private string _loginBaseUrl = "https://login.jdate.com";
        private readonly EmailGenerator _emailGenerator;

        public jdate_co_fr_ExternalMailSMTests()
        {
            _emailGenerator = new EmailGenerator(TargetConfigurationProvider.Instance.Get(Site.JDateCoFr, Environment.Stage, TargetType.CreativeQA,Target.Rebekah), _brandId, _siteId, _loginBaseUrl, 2265);
        }

     
        [TestMethod]
        public void NewMailAlert()
        {
            _emailGenerator.NewMailAlert();
        }

        [TestMethod]
        public void SubscriptionConfirmation()
        {
            _emailGenerator.SubscriptionConfirmation();
        }

        [TestMethod]
        public void AllAccessReturnEmail()
        {
            _emailGenerator.AllAccessReturnEmail();
        }

        [TestMethod]
        public void MutualMail()
        {
            _emailGenerator.MutualMail();
        }

        [TestMethod]
        public void EcardToFriend()
        {

            _emailGenerator.EcardToFriend();
        }

        [TestMethod]
        public void EcardToMember()
        {
            _emailGenerator.EcardToMember();
        }

        [TestMethod]
        public void EmailChangeConfirmation()
        {
            _emailGenerator.EmailChangeConfirmation();
        }

        [TestMethod]
        public void ForgotPassword()
        {
            _emailGenerator.ForgotPassword();
        }

        [TestMethod]
        public void HotListedAlert()
        {
            _emailGenerator.HotListedAlert();
        }

        [TestMethod]
        public void PhotoApproval()
        {
            _emailGenerator.PhotoApproval();
        }

        [TestMethod]
        public void PhotoCaptionRejection()
        {
            _emailGenerator.PhotoCaptionRejection();
        }

        [TestMethod]
        public void PhotoRejection()
        {
            _emailGenerator.PhotoRejection();
        }

        [TestMethod]
        public void SendToFriend()
        {
            _emailGenerator.SendToFriend();
        }

        [TestMethod] 
        public void ActivationLetter()
        {
            _emailGenerator.ActivationLetter();
        }

        [TestMethod]
        public void ViewedYouEmail()
        {
            _emailGenerator.ViewedYouEmail();
        }
    }
}
