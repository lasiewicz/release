﻿using System;
using System.Collections;
using System.Collections.Generic;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.ExternalMail.MsTests
{
    [TestClass]
    public class blacksingles_com_ExternalMailSMTests
    {
        private int _brandId = 90510;
        private int _siteId = 9051;

        private string _loginBaseUrl = "https://login.jdate.com";
        private readonly EmailGenerator _emailGenerator;

        public blacksingles_com_ExternalMailSMTests()
        {
            _emailGenerator = new EmailGenerator(TargetConfigurationProvider.Instance.Get(Site.BlackSinglesCom, Environment.Stage, TargetType.Developer), _brandId, _siteId, _loginBaseUrl, 2265);
        }

        [TestMethod]
        void ActivationLetter()
        {
            _emailGenerator.ActivationLetter();
        }

        [TestMethod]
        void HotListedAlert()
        {
            _emailGenerator.HotListedAlert();
        }

        [TestMethod]
        void NewMailAlert()
        {
            _emailGenerator.NewMailAlert();
        }

        [TestMethod]
        void MutualMail()
        {
            _emailGenerator.MutualMail();
        }

        [TestMethod]
        void SendMemberColorCode()
        {
            _emailGenerator.SendMemberColorCode();
        }

        [TestMethod]
        void ColorCodeQuizConfirmation()
        {
            _emailGenerator.ColorCodeQuizConfirmation();
        }

        [TestMethod]
        void SendMemberColorCodeQuizInvite()
        {
            _emailGenerator.SendMemberColorCodeQuizInvite();
        }

        [TestMethod]
        void EcardToFriend()
        {
            _emailGenerator.EcardToFriend();
        }

        [TestMethod]
        void EcardToMember()
        {
            _emailGenerator.EcardToMember();
        }

        [TestMethod]
        void EmailChangeConfirmation()
        {
            _emailGenerator.EmailChangeConfirmation();
        }

        [TestMethod]
        void ForgotPassword()
        {
            _emailGenerator.ForgotPassword();
        }

        [TestMethod]
        void PhotoApproval()
        {
            _emailGenerator.PhotoApproval();
        }
        
        [TestMethod]
        void PhotoCaptionRejection()
        {
            _emailGenerator.PhotoCaptionRejection();
        }
        
        [TestMethod]
        void PhotoRejection()
        {
            _emailGenerator.PhotoRejection();
        }
        
        [TestMethod]
        void SodbPhotoUpload()
        {
            _emailGenerator.SodbPhotoUpload();
        }
        
        [TestMethod]
        void SubscriptionConfirmation()
        {
            _emailGenerator.SubscriptionConfirmation();
        }
    }
}
