﻿using System;
using System.Collections;
using System.Collections.Generic;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.ExternalMail.MsTests
{
    [TestClass]
    public class spark_com_ExternalMailSMTests
    {

        private int _brandId = 1001;
        private int _siteId = 101;

        private string _loginBaseUrl = "https://login.jdate.com";
        private readonly EmailGenerator _emailGenerator;

        public spark_com_ExternalMailSMTests()
        {
            _emailGenerator = new EmailGenerator(TargetConfigurationProvider.Instance.Get(Site.CupidCoIl, Environment.Stage, TargetType.Developer), _brandId, _siteId, _loginBaseUrl, 2265);
        }

        [TestMethod]
        public void SubscriptionConfirmation()
        {
            _emailGenerator.SubscriptionConfirmation();
        }

        [TestMethod]
        public void ForgotPassword()
        {
            _emailGenerator.ForgotPassword();
        }

        [TestMethod]
        public void PhotoRejection()
        {
            _emailGenerator.PhotoRejection();
        }

        [TestMethod]
        public void ActivationLetter()
        {
            _emailGenerator.ActivationLetter();
        }

        [TestMethod]
        public void PhotoApproval()
        {
            _emailGenerator.PhotoApproval();
        }

        [TestMethod]
        public void EmailChangeConfirmation()
        {
            _emailGenerator.EmailChangeConfirmation();
        }

        [TestMethod]
        public void MutualMail()
        {
            _emailGenerator.MutualMail();
        }

        [TestMethod]
        public void ColorCodeQuizConfirmation()
        {
            _emailGenerator.ColorCodeQuizConfirmation();
        }

        [TestMethod]
        public void SendMemberColorCodeQuizInvite()
        {
            _emailGenerator.SendMemberColorCodeQuizInvite();
        }

        [TestMethod]
        public void SendMemberColorCode()
        {
            _emailGenerator.SendMemberColorCode();
        }

        [TestMethod]
        public void EcardToFriend()
        {
            _emailGenerator.EcardToFriend();
        }

        [TestMethod]
        public void EcardToMember()
        {
            _emailGenerator.EcardToMember();
        }

        [TestMethod]
        public void HotListedAlert()
        {
            _emailGenerator.HotListedAlert();
        }

        [TestMethod]
        public void PhotoCaptionRejection()
        {
            _emailGenerator.PhotoCaptionRejection();
        }

        [TestMethod]
        public void SendMemberQuestionAndAnswer()
        {
            _emailGenerator.SendMemberQuestionAndAnswer();
        }

        [TestMethod]
        public void SendToFriend()
        {
            _emailGenerator.SendToFriend();
        }
    }
}
