﻿using System;
using System.Collections;
using System.Collections.Generic;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.ExternalMail.MsTests
{
    [TestClass]
    public class cupid_co_il_ExternalMailSMTests
    {
        private int _brandId = 1015;
        private int _siteId = 15;

        private string _loginBaseUrl = "https://login.jdate.com";
        private readonly EmailGenerator _emailGenerator;

        public cupid_co_il_ExternalMailSMTests()
        {
            _emailGenerator = new EmailGenerator(TargetConfigurationProvider.Instance.Get(Site.CupidCoIl, Environment.Stage, TargetType.Developer), _brandId, _siteId, _loginBaseUrl, 2265);
        }

        [TestMethod]
        public void SubscriptionConfirmation()
        {
            _emailGenerator.SubscriptionConfirmation();
        }

        [TestMethod]
        public void ForgotPassword()
        {
            _emailGenerator.ForgotPassword();
        }

        [TestMethod]
        public void EmailChangeConfirmation()
        {
            _emailGenerator.EmailChangeConfirmation();
        }

        [TestMethod]
        public void NewMailAlert()
        {
            _emailGenerator.NewMailAlert();
        }

        [TestMethod]
        public void PhotoApproval()
        {
            _emailGenerator.PhotoApproval();
        }

        [TestMethod]
        public void PhotoRejection()
        {
            _emailGenerator.PhotoRejection();
        }

        [TestMethod]
        public void ActivationLetter()
        {
            _emailGenerator.ActivationLetter();
        }

        [TestMethod]
        public void SendToFriend()
        {
            _emailGenerator.SendToFriend();
        }
    }
}
