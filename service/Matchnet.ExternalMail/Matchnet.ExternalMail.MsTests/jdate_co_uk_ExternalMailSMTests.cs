﻿using System;
using System.Collections;
using System.Collections.Generic;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.ExternalMail.MsTests
{
    [TestClass]
    public class jdate_co_uk_ExternalMailSMTests
    {
        private int _brandId = 1107;
        private int _siteId = 107;

        private string _loginBaseUrl = "https://login.jdate.com";
        private readonly EmailGenerator _emailGenerator;

        public jdate_co_uk_ExternalMailSMTests()
        {
            _emailGenerator = new EmailGenerator(TargetConfigurationProvider.Instance.Get(Site.JDateCoUk, Environment.Stage, Target.Don), _brandId, _siteId, _loginBaseUrl, 2265);
        }

        /*
         * 
         * CONS-3836 
         * 
         * 
         */

        [TestMethod]
        public void NewMailAlert()
        {
            _emailGenerator.NewMailAlert();
        }

        [TestMethod]
        public void AllAccessReturnEmail()
        {
            _emailGenerator.AllAccessReturnEmail();
        }

        [TestMethod]
        public void MutualMail()
        {
            _emailGenerator.MutualMail();
        }

        [TestMethod]
        public void ColorCodeQuizConfirmation()
        {
            _emailGenerator.ColorCodeQuizConfirmation();
        }
        
        [TestMethod]
        public void SendMemberColorCodeQuizInvite()
        {
            _emailGenerator.SendMemberColorCodeQuizInvite();
        }

        [TestMethod]
        public void EcardToMember()
        {
            _emailGenerator.EcardToMember();
        }

        [TestMethod]
        public void EmailChangeConfirmation()
        {
            _emailGenerator.EmailChangeConfirmation();
        }

        [TestMethod]
        public void HotListedAlert()
        {
            _emailGenerator.HotListedAlert();
        }

        [TestMethod]
        public void MatchMeterCongratsMail()
        {
            _emailGenerator.MatchMeterCongratsMail();
        }

        [TestMethod]
        public void PhotoApproval()
        {
            _emailGenerator.PhotoApproval();
        }

        [TestMethod]
        public void PhotoRejection()
        {
            _emailGenerator.PhotoRejection();
        }

        [TestMethod]
        public void SendToFriend()
        {
            _emailGenerator.SendToFriend();
        }

        [TestMethod]
        public void SubscriptionConfirmation()
        {
            _emailGenerator.SubscriptionConfirmation();
        }


        [TestMethod] 
        public void ActivationLetter()
        {
            _emailGenerator.ActivationLetter();
        }

        [TestMethod]
        public void ViewedYouEmail()
        {
            _emailGenerator.ViewedYouEmail();
        }


        [TestMethod]
        public void MatchMeterInvitationMail()
        {
            _emailGenerator.MatchMeterInvitationMail();
        }

        [TestMethod]
        public void PhotoCaptionRejection()
        {
            _emailGenerator.PhotoCaptionRejection();
        }
        //----------------------------------------------------

    }
}
