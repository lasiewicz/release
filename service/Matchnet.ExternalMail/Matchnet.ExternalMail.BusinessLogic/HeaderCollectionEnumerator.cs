using System;
using System.Collections;

namespace Matchnet.ExternalMail.BusinessLogic
{
	public class HeaderCollectionEnumerator : IEnumerator
	{
		private HeaderCollection _Collection;
		private int _Index = 0;
		private Header _Current;

		public HeaderCollectionEnumerator(HeaderCollection collection)
		{
			_Collection = collection;
		}

		public void Reset()
		{
			_Index = 0;
		}

		public object Current
		{
			get
			{
				return _Current;
			}
		}

		public string FullHeader 
		{
			get 
			{
				return _Current.Name + ": " + _Current.Value;
			}
		}

		public bool MoveNext()
		{
			if (_Index > _Collection.Count - 1) 
			{
				return false;
			} 
			else 
			{
				_Current = (Header)_Collection[_Index];
			}

			_Index++;
			return true;
		}
	}
}
