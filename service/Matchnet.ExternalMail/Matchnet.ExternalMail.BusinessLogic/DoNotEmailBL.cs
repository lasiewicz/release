using System;
using System.Diagnostics;
using System.Messaging;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Web.Caching;
using System.Diagnostics;
using System.Threading;
using System.Text;
using Matchnet.Caching;
using Matchnet.Data;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.DoNotEmail;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

namespace Matchnet.ExternalMail.BusinessLogic
{
	/// <summary>
	/// Summary description for DoNotEmailBL.
	/// </summary>
	public class DoNotEmailBL
	{
		#region Public Variables

		public static readonly DoNotEmailBL Instance = new DoNotEmailBL();

		public static PerformanceCounter AddedToList;
		public static PerformanceCounter RemovedFromList;
		public static PerformanceCounter BlockedSend;
		public static string HASH_CACHE_KEY = "~DNE_HASH";
		public static string HASH_ENTRY = "1";
		
		#endregion

		#region Private Variables

		private static bool countersInitialized = false;
		private Matchnet.Caching.Cache _Cache;

		private const string MMFREQUENCY_ATTRIBUTENAME = "MatchNewsletterPeriod";
		private const string CMFREQUENCY_ATTRIBUTENAME = "GotAClickEmailPeriod";
		private const string NEWSLETTER_ATTRIBUTENAME = "NewsletterMask";
		private Thread _loadThread;
		private int _cacheTTL;
		private bool _debugToErrorLog;
		private DateTime _lastCacheRefreshTime; 
		private bool _isReloadingCache = false;
		
		private EntryHash CachedEntries
		{
			get
			{
				reloadCacheIfNecessary();
				EntryHash hash = _Cache[HASH_CACHE_KEY] as EntryHash;
				if(hash == null)
				{
					hash = new EntryHash(_cacheTTL);
					ServiceBoundaryException ex = new ServiceBoundaryException(ValueObjects.ServiceConstants.SERVICE_NAME, "No entries in the DNE cache");
				}
				return hash;
			}
		}
		
		#endregion

		#region Public Methods

		public bool AddEmailAddress(int brandID, string emailAddress, int memberID)
		{
			bool success = false;
			int siteID;
			
			try
			{
				Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
				siteID = brand.Site.SiteID;
				
				DoNotEmailEntry entry = new DoNotEmailEntry(siteID, emailAddress);
				if(CachedEntries[entry.GetCacheKey()] == null)
				{
					CachedEntries.Add(entry.GetCacheKey(), HASH_ENTRY);
				
					Command command = new Command("mnXmail", "up_DoNotEmail_Add", 0);
					command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
					command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, emailAddress);
					command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);

					Matchnet.Data.Client.Instance.ExecuteAsyncWrite(command);
					success = true;
				}

				if(memberID > 0)
				{
					removeMemberEmailSettings(memberID, brand);
				}
			}
			catch(Exception ex)
			{
				throw new BLException("AddEmailAddress() failed.", ex);
			}

			return success;
		}

		public bool AddEmailAddressBySiteID(int siteID, string emailAddress)
		{
			bool success = false;
			
			try
			{
				DoNotEmailEntry entry = new DoNotEmailEntry(siteID, emailAddress);
				if(CachedEntries[entry.GetCacheKey()] == null)
				{
					CachedEntries.Add(entry.GetCacheKey(), HASH_ENTRY);
				
					Command command = new Command("mnXmail", "up_DoNotEmail_Add", 0);
					command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
					command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, emailAddress);
					command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, Constants.NULL_INT);

					Matchnet.Data.Client.Instance.ExecuteAsyncWrite(command);
					success = true;
				}
			}
			catch(Exception ex)
			{
				throw new BLException("AddEmailAddress() failed.", ex);
			}

			return success;
		}

		public bool RemoveEmailAddress(int siteID, string emailAddress)
		{
			bool success = false;
			
			try
			{
                if (emailAddress == null || emailAddress == String.Empty)
                {
                    new BLException("DNE RemoveEmailAddress Email Address is null or empty. SiteID:"
                       + siteID.ToString());

                    return success;
                }

				DoNotEmailEntry entry = new DoNotEmailEntry(siteID, emailAddress);
				if(CachedEntries[entry.GetCacheKey()] != null)
				{
					Command command = new Command("mnXmail", "up_DoNotEmail_Delete", 0);
					command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
					command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, emailAddress);
				
					Matchnet.Data.Client.Instance.ExecuteAsyncWrite(command);
				
					CachedEntries.Remove(entry.GetCacheKey());
					success = true;
				}
			}
			catch(Exception ex)
			{
				throw new BLException("RemoveEmailAddress() failed.", ex);
			}

			return success;
		}

		public DoNotEmailEntry GetEntryByEmailAddress(string emailAddress)
		{
			DoNotEmailEntry entry = null;
			
			try
			{
				string cacheKey = string.Empty;
				object obj = null;
				
				foreach(Site site in Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetSites())
				{
					cacheKey = (new DoNotEmailEntry(site.SiteID, emailAddress)).GetCacheKey();
					obj = CachedEntries[cacheKey];
					if(obj != null)
					{
						return new DoNotEmailEntry(site.SiteID, emailAddress);
						break;
					}
				}
				
			}
			catch(Exception ex)
			{
				throw new BLException(String.Format("GetEntryByEmailAddress() failed. EmailAddress {0}.", emailAddress), ex);
			}
			
			return entry;
		}

		public DoNotEmailEntry GetEntryBySiteAndEmailAddress(int siteID, string emailAddress)
		{
			DoNotEmailEntry entry = null;
			
			try
			{
				string cacheKey = (new DoNotEmailEntry(siteID, emailAddress)).GetCacheKey();
				object obj = CachedEntries[cacheKey];
				if(obj != null)
				{
					entry = new DoNotEmailEntry(siteID, emailAddress);
				}
			}
			catch(Exception ex)
			{
				throw new BLException(String.Format("GetEntryBySiteEmailAddress() failed. SiteID: {0} EmailAddress {1}.", siteID.ToString(), emailAddress), ex);
			}
			
			return entry;
		}

		public ArrayList GetEntriesBySite(int siteID)
		{
			ArrayList entries = new ArrayList();
			DoNotEmailEntry entry = null;
			string[] siteAndAddress;

			try
			{
				IDictionaryEnumerator de = CachedEntries.GetEnumerator();
				while(de.MoveNext())
				{
					siteAndAddress = de.Key.ToString().Split(DoNotEmailEntry.CACHE_SEPARATOR.ToCharArray());
					if(Convert.ToInt32(siteAndAddress[0]) == siteID)
					{
						
						entries.Add(siteAndAddress[2].ToString());
					}
				}

			}
			catch(Exception ex)
			{
				throw new BLException(String.Format("GetEntriesBySite() failed. SiteID: {0}", siteID.ToString()), ex);
			}
			
			return entries;
			
		}

		public ArrayList GetEntries()
		{
			ArrayList entries = new ArrayList();
			DoNotEmailEntry entry = null;
			
			try
			{
				IDictionaryEnumerator de = CachedEntries.GetEnumerator();
				while(de.MoveNext())
				{
					if(de.Key.ToString().IndexOf(DoNotEmailEntry.CACHE_PREPEND) > 0)
					{
						/*entry = new DoNotEmailEntry();
						entry.SiteID = reader.GetInt32(reader.GetOrdinal("SiteID"));
						entry.EmailAddress = reader.GetString(reader.GetOrdinal("EmailAddress"));
						entry.InsertDate = reader.GetDateTime(reader.GetOrdinal("InsertDate"));
						if(!reader.IsDBNull(reader.GetOrdinal("MemberID")))
						{
							entry.MemberID = reader.GetInt32(reader.GetOrdinal("MemberID"));
						}
						
						
						//entry = (DoNotEmailEntry)de.Value;
						entries.Add(entry);*/
					}
				}
			}
			catch(Exception ex)
			{
				throw new BLException("GetEntries() failed. ", ex);
			}
			
			return entries;
		}

		public ArrayList GetSiteSummary()
		{
			ArrayList summaries = new ArrayList();
			SqlDataReader reader = null;
			int siteID;
			string siteName;
			int count; 
			
			try
			{
				Command command = new Command("mnXmail", "up_DoNotEmail_ListSummary", 0);
				reader = Matchnet.Data.Client.Instance.ExecuteReader(command);
	
				while(reader.Read())
				{
					siteID = reader.GetInt32(reader.GetOrdinal("SiteID"));
					siteName = reader.GetString(reader.GetOrdinal("SiteName"));
					count = reader.GetInt32(reader.GetOrdinal("NumberEntries"));
					summaries.Add(new DoNotEmailSiteSummary(siteID, count, siteName));
				}
			}
			catch(Exception ex)
			{
				throw new BLException("GetEntries() failed. ", ex);
			}
			finally
			{
				if(reader != null && !reader.IsClosed)
				{
					reader.Close();
				}
			}

			return summaries;
		}

		public ArrayList Search(int siteID, string partialEmailAddress, int startRow, int pageSize, ref int totalRows)
		{
			ArrayList entries = new ArrayList();
			DoNotEmailEntry entry = null;
			SqlDataReader reader = null;
			int numRecords = 0;
			
			try
			{
				Command command = new Command("mnXmail", "up_DoNotEmail_ListbyPartialEmailAddress", 0);
				command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
				command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, partialEmailAddress);
				command.AddParameter("@StartRow", SqlDbType.Int, ParameterDirection.Input, startRow);
				command.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
				command.AddParameter("@TotalRows", SqlDbType.Int, ParameterDirection.Output, totalRows);
				
				SqlParameterCollection totalRowsCount;

				reader = Matchnet.Data.Client.Instance.ExecuteReader(command, out totalRowsCount);
								
				while(reader.Read())
				{
					entry = getDoNotEmailEntry(reader);
					entries.Add(entry);
					numRecords++;
				}
				
				reader.Close();
				totalRows = Conversion.CInt(totalRowsCount["@TotalRows"].Value);
			}
			catch(Exception ex)
			{
				throw new BLException(String.Format("Search() failed. SiteID: {0} EmailAddress: {1}", siteID.ToString(), partialEmailAddress), ex);
			}
			finally
			{	   
				if(reader != null && !reader.IsClosed)
				{
					reader.Close();
				}
			}
			
			return entries;
		}

		public void LoadCache()
		{
			reloadCacheIfNecessary();
		}



		#endregion

		#region Private Methods

		private DoNotEmailBL()
		{
			_lastCacheRefreshTime = DateTime.MinValue;
			_Cache = Matchnet.Caching.Cache.Instance;
			_cacheTTL = Conversion.CInt(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("DNE_REFRESH_CACHE_INTERVAL"));
			_debugToErrorLog = Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("DNE_ENABLE_LOGGING"));
		}

		private DoNotEmailEntry getDoNotEmailEntry(SqlDataReader reader)
		{
			DoNotEmailEntry entry = new DoNotEmailEntry();
			entry.SiteID = reader.GetInt32(reader.GetOrdinal("SiteID"));
			entry.EmailAddress = reader.GetString(reader.GetOrdinal("EmailAddress"));
			entry.InsertDate = reader.GetDateTime(reader.GetOrdinal("InsertDate"));
			if(!reader.IsDBNull(reader.GetOrdinal("MemberID")))
			{
				entry.MemberID = reader.GetInt32(reader.GetOrdinal("MemberID"));
			}
			
			return entry;
		}

		private void loadCache()
		{
			DoNotEmailEntry entry = null;
			SqlDataReader reader = null;
			EntryHash dneHash = new EntryHash(_cacheTTL);
			
			try
			{
				if(_debugToErrorLog) System.Diagnostics.EventLog.WriteEntry("dne", "starting cache load", System.Diagnostics.EventLogEntryType.Warning);
				Command command = new Command("mnXmail", "up_DoNotEmail_List", 0);
				reader = Data.Client.Instance.ExecuteReader(command);
				int ordinalEmail = reader.GetOrdinal("EmailAddress");
				int ordinalSite = reader.GetOrdinal("SiteID");

				while(reader.Read())
				{
					entry = new DoNotEmailEntry();
					entry.SiteID = reader.GetInt32(reader.GetOrdinal("SiteID"));
					entry.EmailAddress = reader.GetString(reader.GetOrdinal("EmailAddress"));
					
					dneHash.Add(entry.GetCacheKey(), "1");
				}

				if(_Cache[HASH_CACHE_KEY] != null)
				{
					_Cache.Remove(HASH_CACHE_KEY);
				}
				_Cache.Add(dneHash);
				_lastCacheRefreshTime = DateTime.Now;
				if(_debugToErrorLog) System.Diagnostics.EventLog.WriteEntry("dne", "finished cache load", System.Diagnostics.EventLogEntryType.Warning);

			}
			catch(Exception ex)
			{
				System.Diagnostics.EventLog.WriteEntry("dne", "error in cache load: " +  ex.Message, System.Diagnostics.EventLogEntryType.Warning);
				throw new BLException("loadCache() failed.", ex);
				
			}
			finally
			{	   
				_isReloadingCache = false;
				if(reader != null && !reader.IsClosed)
				{
					reader.Close();
				}
			}
		}

		private void reloadCacheIfNecessary()
		{
			if(!_isReloadingCache && _lastCacheRefreshTime.AddSeconds(_cacheTTL) <= DateTime.Now)
			{
				if(_debugToErrorLog) System.Diagnostics.EventLog.WriteEntry("dne", "reloading cache because of expiration", System.Diagnostics.EventLogEntryType.Warning);
				_isReloadingCache = true;
				_loadThread = new Thread(new ThreadStart(loadCache));
				_loadThread.Start();
			}
		}

		private void removeMemberEmailSettings(int memberID, Brand brand)
		{
			Member.ServiceAdapters.Member member = Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
			member.SetAttributeInt(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, MMFREQUENCY_ATTRIBUTENAME, 0);
			member.SetAttributeInt(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, CMFREQUENCY_ATTRIBUTENAME, 0);
			member.SetAttributeInt(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, 0);
			member.SetAttributeInt(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, NEWSLETTER_ATTRIBUTENAME, 0);
			Member.ServiceAdapters.MemberSA.Instance.SaveMember(member);
		}

		private string getCacheKey(int siteID, string emailAddress)
		{
			return siteID.ToString() + DoNotEmailEntry.CACHE_SEPARATOR + emailAddress;
		}
		#endregion

		public class EntryHash: Hashtable, ICacheable
		{
			#region ICacheable Members
			private int _cacheTTL;
			
			public EntryHash(int cacheTTL)
			{
				_cacheTTL = cacheTTL;
			}

			public int CacheTTLSeconds
			{
				get
				{
					return _cacheTTL;
				}
				set
				{
					_cacheTTL = value;
				}
			}

			public Matchnet.CacheItemMode CacheMode
			{
				get
				{
					return Matchnet.CacheItemMode.Sliding;
				}
			}

			public Matchnet.CacheItemPriorityLevel CachePriority
			{
				get
				{
					return Matchnet.CacheItemPriorityLevel.Normal;
				}
				set
				{
				}
			}

			public string GetCacheKey()
			{
				return DoNotEmailBL.HASH_CACHE_KEY;
			}

			#endregion
			

		}

	}
}

