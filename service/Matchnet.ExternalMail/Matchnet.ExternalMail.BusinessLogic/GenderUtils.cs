using System;

using Matchnet;

namespace Matchnet.ExternalMail.BusinessLogic
{
	/// <summary>
	///	This was copied over from the web solution and should probably be placed in a shared location.
	/// </summary>
	public class GenderUtils
	{
		private GenderUtils()
		{
			//	Static only, no instantiation
		}

		public static int GetGenderMaskSelf(int genderMask)
		{
			int maskSelf = (Int32)GenderMask.Male + (Int32)GenderMask.Female + (Int32)GenderMask.MTF + (Int32)GenderMask.FTM;
			return genderMask & maskSelf;
		}

		public static int GetGenderMaskSeeking(int genderMask)
		{
			int maskSeeking = (Int32)GenderMask.SeekingMale + (Int32)GenderMask.SeekingFemale + (Int32)GenderMask.MTF + (Int32)GenderMask.FTM;
			return genderMask & maskSeeking;
		}

		public static int GetGenderSeekingFromGender(int genderMask)
		{
			int retVal = 0;

			if (MaskContains(genderMask, (Int32)GenderMask.Male))
				retVal += (Int32)GenderMask.SeekingMale;
			if (MaskContains(genderMask, (Int32)GenderMask.Female))
				retVal += (Int32)GenderMask.SeekingFemale;
			if (MaskContains(genderMask, (Int32)GenderMask.MTF))
				retVal += (Int32)GenderMask.MTF;
			if (MaskContains(genderMask, (Int32)GenderMask.FTM))
				retVal += (Int32)GenderMask.FTM;

			return retVal;
		}

		public static int GetGenderFromGenderSeeking(int genderMask)
		{
			int retVal = 0;

			if (MaskContains(genderMask, (Int32)GenderMask.SeekingMale))
				retVal += (Int32)GenderMask.Male;
			if (MaskContains(genderMask, (Int32)GenderMask.SeekingFemale))
				retVal += (Int32)GenderMask.Female;
			if (MaskContains(genderMask, (Int32)GenderMask.MTF))
				retVal += (Int32)GenderMask.MTF;
			if (MaskContains(genderMask, (Int32)GenderMask.FTM))
				retVal += (Int32)GenderMask.FTM;

			return retVal;
		}

		/// <summary>
		/// Determines whether or not a given gender and seeking gender indicate
		/// a homosexual orientation
		/// </summary>
		/// <param name="gender"></param>
		/// <param name="seekingGender"></param>
		/// <returns></returns>
		public static bool IsHomosexual(int gender, int seekingGender)
		{
			bool retVal = false;

			if (
				(gender == (Int32)GenderMask.Male && seekingGender == (Int32)GenderMask.SeekingMale)
				||	(gender == (Int32)GenderMask.Female && seekingGender == (Int32)GenderMask.SeekingFemale) 
				||	(gender == (Int32)GenderMask.MTF && seekingGender == (Int32)GenderMask.SeekingMTF) 
				||	(gender == (Int32)GenderMask.FTM && seekingGender == (Int32)GenderMask.SeekingFTM) )
			{
				retVal = true;
			}

			return(retVal);
		}

		/// <summary>
		///	Determines whether or not a given gender mask indicate a
		///	homosexual orientation
		/// </summary>
		/// <param name="genderMask"></param>
		/// <returns></returns>
		public static bool IsMaskHomosexual(int genderMask)
		{
			int genderSelf = GetGenderMaskSelf(genderMask);
			int genderSeeking = GetGenderMaskSeeking(genderMask);

			return IsHomosexual(genderSelf, genderSeeking);
		}

		/// <summary>
		/// Determines whether or not a given gender and seeking gender indicate
		/// a heterosexual orientation
		/// </summary>
		/// <param name="gender"></param>
		/// <param name="seekingGender"></param>
		/// <returns></returns>
		public static bool IsHeterosexual(int gender, int seekingGender)
		{
			return(!IsHomosexual(gender, seekingGender));
		}

		/// <summary>
		///	Determines whether or not a given gender mask indicate a
		///	heterosexual orientation
		/// </summary>
		/// <param name="genderMask"></param>
		/// <returns></returns>
		public static bool IsMaskHeterosexual(int genderMask)
		{
			return(!IsMaskHomosexual(genderMask));
		}

		/// <summary>
		/// Apparently the gender mask is stored in such a way that if
		/// we want it to dispaly properly we have to flip it when we
		/// pull it from the preferences and before it gets put back
		/// into the preferences if and only if the user is a
		/// heterosexual.  This method encapsulates that functionality.
		/// </summary>
		/// <param name="genderMask"></param>
		/// <returns></returns>
		public static int FlipMaskIfHeterosexual(int genderMask)
		{
			int retVal = genderMask;

			if(IsMaskHeterosexual(genderMask))
			{
				int genderSelf = GetGenderMaskSelf(genderMask);
				int genderSeeking = GetGenderMaskSeeking(genderMask);

				genderSelf = GetGenderSeekingFromGender(genderSelf);
				genderSeeking = GetGenderFromGenderSeeking(genderSeeking);

				retVal = genderSelf + genderSeeking;
			}
				
			return (retVal);
		}

		public static bool MaskContains(int genderMask, int genderID)
		{
			return ((genderMask & genderID) == genderID);
		}
	}
}
