using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using Matchnet.ActivityRecording.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Exceptions;
using System.Web;
using System.Text;
using System.Globalization; 
using System.Net;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.BulkMail;
using Matchnet.Member.ValueObjects.Interfaces;
using MSA = Matchnet.Member.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.ExternalMail.ValueObjects.StormpostSoap;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.EmailLibrary;
using Matchnet.HTTPMessaging;
using Matchnet.HTTPMessaging.RequestTypes.MingleMailer;
using P1 = Matchnet.Purchase.ValueObjects;
using Matchnet.Email.ServiceAdapters;
using Matchnet.ExternalMail.BusinessLogic.Monitoring;
using Spark.Logger;
using Spark.Logging;
using ServiceConstants = Matchnet.ExternalMail.ValueObjects.ServiceConstants;

namespace Matchnet.ExternalMail.BusinessLogic
{
	/// <summary>
	/// Summary description for MingleMailerHandler.
	/// </summary>
	public class MingleMailerHandler : IMailHandler
	{
        private const string CLASS_NAME = "MingleMailerHandler";
        private const string PROFILE_FIELD_LABEL = "field_name";
	    
        private AllowedDomains _allowedDomains;
		#region CTOR
		private MingleMailerHandler()
		{
			_mapper = new MingleMailerTemplateMapper();
            _allowedDomains = new AllowedDomains(ServiceConstants.SERVICE_NAME);
		}

		#endregion

		#region Constants
		private const Int32 EMAIL_VERIFIED = 4;
		private const int XOR_SALT_ADD_VALUE = 42;
		private string[] SEARCH_FIELDS_FOR_URL = new string[]
			{
				"SearchTypeID", "Distance", "MinAge", "MaxAge",
				"MinHeight", "MaxHeight", "EducationLevel", "Religion",
				"Ethnicity", "SmokingHabits", "DrinkingHabits", "HasPhotoFlag",
				"MajorType", "RelationshipMask", "RelationShipStatus", "RegionID",
				"SearchOrderBy"
			};
		#endregion

		#region Member
		private ITemplateMapper _mapper;
		private static MingleMailerHandler _instance;
		private static Object _syncBlock = new object();
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(MingleMailerHandler));
		#endregion

		#region PRIVATE HELPER

        private bool isUserAlertSettingOn(IMemberDTO pMember, Brand pBrand)
        {
            AttributeGroup group = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(pBrand.Site.Community.CommunityID, EmailAlertConstant.ATTRIBUTE_ID);
            int mask = (int)EmailAlertMask.NewEmailAlert;
            int userEmailSetting = pMember.GetAttributeInt(pBrand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, int.Parse(group.DefaultValue));

            return (userEmailSetting & mask) == mask;
        }

		private void sendMingleMail(string templateName, string emailAddress, EnvParameterBase envParameter, Brand brand)
		{
            var tags = new CustomDataProvider(brand.BrandID,0,emailAddress).Get();
            Log.LogInfoMessage("Enter sendMingleMail - " + templateName,tags);

            bool allowedDomain = true;
            //put it hear to quick fix, though the better place will be router
            //though it's debatable because restriction is  for testing so we might want to test up to the actual send point
            try
            {
                allowedDomain = _allowedDomains.IsAllowedDomain(emailAddress);
            }
            catch (Exception ex)
            {
                return;
            }
            if (!allowedDomain)
                return;
			string mingleMailerPostUrl = RuntimeSettings.GetSetting("MINGLE_MAILER_POST_URL");
			string siteKey = RuntimeSettings.GetSetting("MINGLE_MAILER_SITE_KEY", brand.Site.Community.CommunityID, brand.Site.SiteID);
			
            SendMailRequest mailReq = new SendMailRequest(templateName, envParameter, emailAddress, siteKey);
			HttpStatusCode statusCode;
			try
			{
				string respResult = SMSProxy.SendPost(mailReq.GetPostData(), mingleMailerPostUrl, 60000, "application/x-www-form-urlencoded", out statusCode);
                Log.LogInfoMessage("sendMingleMail, sending to : " +  mingleMailerPostUrl + " post: " + mailReq.GetPostData()  +  envParameter.GetJsonString(),tags);
                if ((respResult.ToLower().IndexOf("\"error\"") > -1) || (respResult.ToLower().IndexOf("\"failure\"") > -1))
                {
                    // Temporary fix to get through the weekend
                    if (respResult.IndexOf("Do Not Email") == -1)
                        throw new Exception("Error response from mingle mailer:" + respResult);
                }
                else if (respResult.ToLower().StartsWith("error"))
                {
                    // These are errors from Mingle dev server (just logging to know)
                    try
                    {   // Attaching the stack trace
                        throw new Exception(
                            "Error sending a mail request to Mingle Mailer. [EmailAddress: " + emailAddress +
                            ", TemplateName: " +
                            templateName + ", BrandID: " + brand.BrandID.ToString() +
                            "] ***Data POSTED to MingleMailer --> " + mailReq.GetPostData() + "Mingle Response: " +
                            respResult + " from " + mingleMailerPostUrl);
                    }
                    catch (Exception ex)
                    {
                        Log.LogError("Exception in sendMingleMail, template = " + templateName, ex, tags);
                    }
                }
                Log.LogInfoMessage("Leave sendMingleMail, response " + respResult, tags);
                
			}
			catch(Exception ex)
			{
			    var message = "Error sending a mail request to Mingle Mailer. [EmailAddress: " + emailAddress +
			                  ", TemplateName: " +
			                  templateName + ", BrandID: " + brand.BrandID.ToString() + "] ***Data POSTED to MingleMailer --> " +
			                  mailReq.GetPostData();
                Log.LogError(message,ex,tags);
				throw new Exception(message, ex);
			}
		}

		private string getMingleMailerSiteShortName(Brand brand)
		{
			return RuntimeSettings.GetSetting("MINGLE_MAILER_SITE_SHORTNAME", brand.Site.Community.CommunityID, brand.Site.SiteID);
		}
		
		private string getPaidNonPaidString(IMemberDTO member, int siteID)
		{
			string ret = "NONPAID";

			if(member.IsPayingMember(siteID))
				ret = "PAID";

            return ret;
		}

		private string getSubscriptionStatusString(int subStatus)
		{
			P1.SubscriptionStatus sStatus = (P1.SubscriptionStatus)subStatus;

			string ret = string.Empty;

			switch(sStatus)
			{
				case P1.SubscriptionStatus.NonSubscriberNeverSubscribed:
					ret = "Registered Member";
					break;
				case P1.SubscriptionStatus.SubscribedFTS:
					ret = "First Time Subscriber";
					break;
				case P1.SubscriptionStatus.RenewalSubscribed:
					ret = "Renewal Subscriber";
					break;
				case P1.SubscriptionStatus.NonSubscriberExSubscriber:
					ret = "X-Subscriber";
					break;
				case P1.SubscriptionStatus.SubscribedWinBack:
					ret = "Win-Back Subscriber";
					break;
				case P1.SubscriptionStatus.RenewalWinBack:
					ret = "Win-Back Renewal";
					break;
			}

			return ret;
		}

		private string getGenderSelfString(int genderMask)
		{
			string ret = string.Empty;

			if( (genderMask & (int)GenderMask.Male) == (int)GenderMask.Male)
			{
				ret = "Male";
			}
			else if( (genderMask & (int)GenderMask.Female) == (int)GenderMask.Female)
			{
				ret = "Female";
			}

			return ret;
		}

        private DateTime getMemberBrandJoinDate(ImpulseBase impulse)
        {
            DateTime brandInsertDate = DateTime.MinValue;
            int communityID = Constants.NULL_INT;
            
            var brand = BrandConfigSA.Instance.GetBrandByID(impulse.BrandID);

            if (brand == null)
            {
                RenewalFailureNotificationImpulse renewalImpulse = (RenewalFailureNotificationImpulse)impulse;

                Sites oSites = BrandConfigSA.Instance.GetSites();
                foreach (Site oSite in oSites)
                {
                    if (renewalImpulse.SiteID == oSite.SiteID)
                    {
                        communityID = oSite.Community.CommunityID;
                        break;
                    }
                }
            }
            else
            {
                communityID = brand.Site.Community.CommunityID;
            }


            if (impulse is ImpulseBaseEmail)
            {
                var impulseBaseEmail = (ImpulseBaseEmail) impulse;

                if (impulseBaseEmail.TargetMemberId != Constants.NULL_INT)
                {
                    var member = MemberSA.Instance.GetMember(impulseBaseEmail.TargetMemberId, MemberLoadFlags.None);
                    brandInsertDate = member.GetAttributeDate(brand, "BrandInsertDate");
                }
            }
            else
            {
                var memberID = Constants.NULL_INT;

                // todo: mike list the impulse types that require manual process.
                if (impulse is PasswordMailImpulse)
                {
                    memberID = MemberSA.Instance.GetMemberIDByEmail(((PasswordMailImpulse)impulse).EmailAddress, communityID);
                   
                }
                else if (impulse is RenewalFailureNotificationImpulse)
                {
                    memberID = MemberSA.Instance.GetMemberIDByEmail(((RenewalFailureNotificationImpulse)impulse).EmailAddress, communityID);
                }
                else if (impulse is SendMemberColorCodeQuizInviteImpulse)
                {
                    memberID = MemberSA.Instance.GetMemberIDByEmail(((SendMemberColorCodeQuizInviteImpulse)impulse).RecipientEmail, communityID);
                }

                if (memberID != Constants.NULL_INT)
                {
                    var member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                    if (member != null)
                    {
                        brandInsertDate = member.GetAttributeDate(brand, "BrandInsertDate");
                    }
                }
            }
            

            return brandInsertDate;
        }
        #endregion

		#region PUBLIC METHOD
		
		public void SendEmail(ImpulseBase impulse)
		{
            var tags = new CustomDataProvider(impulse.BrandID).Get();
            Log.LogInfoMessage(string.Format("ClassName:{0}, Entering Method {1}", CLASS_NAME, "SendEmail"),tags);
            var logonOnlySite = false;  
            // Need to block sending certain email type for migration
            try
            {
            Log.LogInfoMessage("Enter SendEmail - " + impulse.GetQueryString(), tags);
            Brand brand=BrandConfigSA.Instance.GetBrandByID(impulse.BrandID);
            if(brand != null){
                logonOnlySite = Convert.ToBoolean(RuntimeSettings.GetSetting(SettingConstants.LOGON_ONLY_SITE,
                brand.Site.Community.CommunityID, brand.Site.SiteID));    
                
                int communityid=brand.Site.Community.CommunityID;
                    int siteid=brand.Site.SiteID;
                    

                    if (Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MIGRATION_MINGLE_EMAIL_BLOCK",communityid,siteid,brand.BrandID )))
                    {
                        if ((impulse is PhotoApprovedImpulse)
                            || (impulse is PhotoRejectedImpulse)
                            || (impulse is PhotoCaptionRejectedImpulse))
                        {
                            return;
                        }
                    }
              }
            }catch(Exception ex)
            {}

		    var brandInsertDate = DateTime.MinValue;
            if(!logonOnlySite)
            {
                brandInsertDate = getMemberBrandJoinDate(impulse);
            }
            
            // has no valid targetMemberID
            if (impulse is PhotoApprovedImpulse)
                this.SendPhotoApproval(impulse, brandInsertDate);

            else if (impulse is SendMemberImpulse)          
                this.SendToFriend(impulse);

            else if (impulse is InternalMailNotifyImpulse)
                this.SendNewMailAlert(impulse, brandInsertDate);

            else if (impulse is MutualYesNotifyImpulse)
                this.SendMutualMail(impulse, brandInsertDate);

            else if (impulse is PhotoRejectedImpulse)
                this.SendPhotoRejection(impulse, brandInsertDate);

                // has no valid targetMemberID, but maybe could obtain from email
            else if (impulse is OptOutNotifyImpulse)        
                this.SendOptOutNotification(impulse);

            else if (impulse is HotListedNotifyImpulse)
                this.SendHotListAlert(impulse, brandInsertDate);
            
                // todo: revisit - done
            else if (impulse is SubscriptionConfirmationImpulse)    
                this.SendSubscriptionConfirmation(impulse, brandInsertDate);
            
            else if (impulse is RegVerificationImpulse)
                this.SendActivationLetter(impulse, brandInsertDate);

                // with ResetPasswordImpulse, not sure if this is called    
            else if (impulse is PasswordMailImpulse)    
                this.SendForgotPassword(impulse, brandInsertDate);

            else if (impulse is ResetPasswordImpulse)
                this.SendResetPassword(impulse, brandInsertDate);

            else if (impulse is EmailVerificationImpulse)
                this.SendEmailChangeConfirmation(impulse, brandInsertDate);
            
                // todo: revisit - done
            else if (impulse is MessageBoardInstantNotificationImpulse)
                this.SendMessageBoardInstantNotification(impulse, brandInsertDate);

                // todo: revisit - done
            else if (impulse is MessageBoardDailyUpdatesImpulse)
                this.SendMessageBoardDailyUpdates(impulse, brandInsertDate);

                // has no valid targetMemberID
            else if (impulse is EcardToFriendImpulse)   
                this.SendEcardToFriend(impulse);

            else if (impulse is RenewalFailureNotificationImpulse)
                this.SendRenewalFailureNotification(impulse, brandInsertDate);

                // since this isn't being used brandInsertDate value be obtained, it will be DateTime.MinValue
                // this should never trigger since Match mail is handled by BulkMail svc only
            else if (impulse is MatchMailImpulse) 
                this.SendMatchMail(impulse, brandInsertDate);
            
                // todo: revisit - done
            else if (impulse is ViralImpulse)
                this.SendClickMail(impulse, brandInsertDate);

                // todo: revisit - done
            else if (impulse is EcardToMemberImpulse)
                this.SendEcardToMember(impulse, brandInsertDate);

                // todo: revisit - done
            else if (impulse is PmtProfileConfirmationImpulse)
                this.sendPaymentProfileNotification(impulse, brandInsertDate);

                // has no valid targetMemberID
            else if (impulse is MatchMeterInvitationMailImpulse) 
                this.sendMatchMeterInvitationMailImpulse(impulse);

            else if (impulse is MatchMeterCongratsMailImpulse)
                this.sendMatchMeterCongratsMailImpulse(impulse, brandInsertDate);

                // since this isn't being used brandInsertDate value be obtained, it will be DateTime.MinValue
                // this should never trigger since New Member mail is handled by BulkMail svc only
            else if (impulse is NewMemberMailImpulse) 
                this.SendNewMemberMail(impulse, brandInsertDate);

            else if (impulse is PhotoCaptionRejectedImpulse)
                this.SendPhotoCaptionRejectedMail(impulse, brandInsertDate);

            else if (impulse is SendMemberColorCodeImpulse) // has no valid targetMemberID
                this.SendMemberColorCode(impulse);

            else if (impulse is SendMemberColorCodeQuizInviteImpulse)
                this.SendMemberColorCodeQuizInvite(impulse, brandInsertDate);

                // todo: revisit - done
            else if (impulse is ColorCodeQuizConfirmationImpulse)
                this.SendColorCodeQuizConfirmation(impulse, brandInsertDate);
                
                // has no valid targetMemberID. seems to be a send to friend deal
            else if (impulse is SendMemberQuestionAndAnswerImpulse) 
                this.SendMemberQuestion(impulse);

                // todo: revisit - done
            else if (impulse is OneOffPurchaseConfirmationImpulse)
                this.SendOneOffPurchaseConfirmation(impulse, brandInsertDate);
            
                // todo: revisit - done
            else if (impulse is JDateMobileRegistrationConfirmationImpulse)
                this.SendJDateMobileRegistrationConfirmation(impulse, brandInsertDate);

            else if (impulse is AllAccessEmailNudgeImpulse)
                this.SendAllAccessNudgeEmail(impulse, brandInsertDate);

            else if (impulse is AllAccessInitialEmailImpulse)
                this.SendAllAccessInitialEmail(impulse, brandInsertDate);

            else if (impulse is AllAccessReplyEmailImpulse)
                this.SendAllAccessReplyEmail(impulse, brandInsertDate);

                // since this is a registration capture, we don't have a brandInsertDate for this case
            else if (impulse is ScheduledNotificationImpulse)
            {//right now there is only 1 scheduled email for the future will create method or case
                SendRegistrationCapture(impulse);
            }
                // no valid targetMemberId -> send to friend type
            else if (impulse is SendFriendReferralImpulse)
                this.SendFriendReferral(impulse);

            else if (impulse is ProfileChangedConfirmationImpulse)
                this.SendProfileChangedConfirmation(impulse, brandInsertDate);

            else if (impulse is FTAWarningsImpulse)
                this.SendFTAWarnings(impulse, brandInsertDate);

            Log.LogInfoMessage(string.Format("ClassName:{0}, Leaving Method {1}", CLASS_NAME, "SendEmail"), tags);
		}

	    private void SendFTAWarnings(ImpulseBase impulse, DateTime brandInsertDate)
	    {
	        var imp = (FTAWarningsImpulse) impulse;
            var member = MailHandlerUtils.GetIMemberDTO(imp.TargetMemberId, MemberLoadFlags.None);
            var brand = BrandConfigSA.Instance.GetBrandByID(imp.BrandID);

	        var firstName = member.GetAttributeText(brand, "SiteFirstName", string.Empty);
	        var lastName = member.GetAttributeText(brand, "SiteLastName", string.Empty);

            EnvParameterFTAWarnings envParm = new EnvParameterFTAWarnings(getMingleMailerSiteShortName(brand),
                member.MemberID.ToString(), member.EmailAddress, firstName, lastName, imp.Warnings, brand.Site.Name,
                brand.Uri, string.Format("memberservices@{0}", brand.Uri), brandInsertDate);

            sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.FTAWarnings),
                    member.EmailAddress, envParm, brand);

	    }

	    #region Ported over from GrayMail
		/// <summary>
		/// Send photo approval email.
		/// </summary>
		/// <param name="impulse"></param>
		public void SendPhotoApproval(ImpulseBase impulse, DateTime brandInsertDate)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			PhotoApprovedImpulse imp = (PhotoApprovedImpulse)impulse;
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.None);
			
			EnvParameterPhotoApproval envParm = new EnvParameterPhotoApproval(getMingleMailerSiteShortName(brand), member.EmailAddress,
				member.MemberID.ToString(), brandInsertDate);
				
			sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.PhotoApproval),
				member.EmailAddress, envParm, brand);
		}

	    /// <summary>
	    /// Send  SendPhotoCaptionRejectedMail email.
	    /// </summary>
	    /// <param name="impulse"></param>
	    /// <param name="brandInsertDate"></param>
	    public void SendPhotoCaptionRejectedMail(ImpulseBase impulse, DateTime brandInsertDate)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			PhotoCaptionRejectedImpulse imp = (PhotoCaptionRejectedImpulse)impulse;
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.None);

			EnvParameterPhotoCaptionRejected envParm = new EnvParameterPhotoCaptionRejected(getMingleMailerSiteShortName(brand),
				member.EmailAddress, member.MemberID.ToString(), brandInsertDate);

			sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.PhotoCaptionRejection),
				member.EmailAddress, envParm, brand);
		}

	    /// <summary>
	    /// Send member to friend email.  Friend can be off site.
	    /// </summary>
	    /// <param name="impulse"></param>
	    /// <param name="brandInsertDate"></param>
	    public void SendToFriend(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			SendMemberImpulse imp = (SendMemberImpulse)impulse;
			
			EnvParameterSendToFriend envParm = new EnvParameterSendToFriend(getMingleMailerSiteShortName(brand),
				imp.Message, imp.FriendName, imp.SentMemberID.ToString(), imp.FriendEmail);

			sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.SendToFriend),
				imp.FriendEmail, envParm, brand);

            try
            {
                var captionString =string.Format("<Mail><Recipient>{0}</Recipient><SendToFriend><MemberID>{1}</MemberID></SendToFriend></Mail>",
                        imp.FriendEmail, imp.SentMemberID.ToString());
                ActivityRecordingSA.Instance.RecordActivity(imp.SendingMemberID, Constants.NULL_INT, brand.Site.SiteID, ActivityRecording.ValueObjects.Types.ActionType.SendToFriend,
                   ActivityRecording.ValueObjects.Types.CallingSystem.BulkMail, captionString);
            }
            catch(Exception ex)
            {
                string message = "ActivityRecording threw an exception: " + ex.Message;
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, message, EventLogEntryType.Error);
            }
		}

		/// <summary>
		/// Send member's color code profile to friend
		/// </summary>
		/// <param name="impulse"></param>
		public void SendMemberColorCode(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			SendMemberColorCodeImpulse imp = (SendMemberColorCodeImpulse)impulse;
			
			EnvParameterColorCode envParm = new EnvParameterColorCode(getMingleMailerSiteShortName(brand),
				imp.Message, imp.FriendName, imp.SenderName.ToString(), imp.SenderMemberID.ToString(), imp.FriendEmail);

			sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.SendMemberColorCode),
				imp.FriendEmail, envParm, brand);
		}

        /// <summary>
        /// Send referral email to friend
        /// </summary>
        /// <param name="impulse"></param>
        public void SendFriendReferral(ImpulseBase impulse)
        {
            Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
            SendFriendReferralImpulse imp = (SendFriendReferralImpulse)impulse;

            EnvParameterFriendReferral envParm = new EnvParameterFriendReferral(getMingleMailerSiteShortName(brand),
                imp.Message, imp.SenderName.ToString(), imp.SenderMemberID.ToString(), imp.FriendEmail, imp.PRM, imp.LGID);

            sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.SendFriendReferral),
                imp.FriendEmail, envParm, brand);
        }

	    /// <summary>
	    /// Send member confirmation on color code quiz
	    /// </summary>
	    /// <param name="impulse"></param>
	    /// <param name="brandInsertDate"></param>
	    public void SendColorCodeQuizConfirmation(ImpulseBase impulse, DateTime brandInsertDate)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			ColorCodeQuizConfirmationImpulse imp = (ColorCodeQuizConfirmationImpulse)impulse;

			EnvParameterColorCodeQuizConfirm envParm = new EnvParameterColorCodeQuizConfirm(getMingleMailerSiteShortName(brand),
				imp.MemberUserName, imp.Color.ToString(), imp.MemberID.ToString(), imp.EmailAddress, brandInsertDate);

			sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.ColorCodeQuizConfirmation),
				imp.EmailAddress, envParm, brand);
		}

	    /// <summary>
	    /// Send color code quiz invite
	    /// </summary>
	    /// <param name="impulse"></param>
	    /// <param name="brandInsertDate"></param>
	    public void SendMemberColorCodeQuizInvite(ImpulseBase impulse, DateTime brandInsertDate)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			SendMemberColorCodeQuizInviteImpulse imp = (SendMemberColorCodeQuizInviteImpulse)impulse;
            int toMemberID = MemberSA.Instance.GetMemberIDByEmail(imp.RecipientEmail, brand.Site.Community.CommunityID);
            IMemberDTO toMember = MailHandlerUtils.GetIMemberDTO(toMemberID, MemberLoadFlags.None);
			
			EnvParameterColorCodeQuizInvite envParm = new EnvParameterColorCodeQuizInvite(getMingleMailerSiteShortName(brand),
				imp.RecipientUserName.ToString(), imp.SenderUserName.ToString(), imp.SenderMemberID.ToString(),
				toMemberID.ToString(), imp.RecipientEmail,
				getGenderSelfString(toMember.GetAttributeInt(brand, "GenderMask", 0)), brandInsertDate);

			sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.SendMemberColorCodeQuizInvite),
				imp.RecipientEmail, envParm, brand);
		}

	    /// <summary>
	    /// Send new mail alert email.
	    /// </summary>
	    /// <param name="impulse"></param>
	    /// <param name="brandInsertDate"></param>
	    public void SendNewMailAlert(ImpulseBase impulse, DateTime brandInsertDate)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			InternalMailNotifyImpulse imp = (InternalMailNotifyImpulse)impulse;
            IMemberDTO toMember = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.None);
            IMemberDTO fromMember = MailHandlerUtils.GetIMemberDTO(imp.SentMemberID, MemberLoadFlags.None);

			bool send = true;

			AttributeGroup group = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(brand.Site.Community.CommunityID, EmailAlertConstant.ATTRIBUTE_ID);
			
			int mask = (int)EmailAlertMask.NewEmailAlert;
			int userSetting = toMember.GetAttributeInt(brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, int.Parse(group.DefaultValue));
			if((mask & userSetting) != mask)
			{
				send = false;
			}

			if(send)
			{
				EnvParameterNewMailAlert envParm = new EnvParameterNewMailAlert(getMingleMailerSiteShortName(brand),
					toMember.GetUserName(brand), imp.UnreadEmailCount.ToString(), EmailMemberHelper.GetDateDisplay(imp.InsertDate, brand),
					EmailMemberHelper.DetermineMemberAge(fromMember, brand).ToString(), EmailMemberHelper.DetermineMemberRegionDisplay(fromMember, brand),
					fromMember.GetUserName(brand), EmailMemberHelper.DetermineMemberSeeking(brand, fromMember), EmailMemberHelper.DetermineMemberThumbnail(fromMember, brand),
					imp.EmailID.ToString(), toMember.MemberID.ToString(), toMember.EmailAddress, getPaidNonPaidString(toMember, brand.Site.SiteID), brandInsertDate);

				sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.NewMailAlert),
					toMember.EmailAddress, envParm, brand);
			}
		}

	    /// <summary>
	    /// Send mutual (both cliqued) email.
	    /// </summary>
	    /// <param name="impulse"></param>
	    /// <param name="brandInsertDate"></param>
	    public void SendMutualMail(ImpulseBase impulse, DateTime brandInsertDate)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			MutualYesNotifyImpulse imp = (MutualYesNotifyImpulse)impulse;
			
			// This is the member who initiated the click.
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.None);
			
			// This is the member who clicked back.
            IMemberDTO mutualMember = MailHandlerUtils.GetIMemberDTO(imp.TargetMemberID, MemberLoadFlags.None);

			bool send = true;

			AttributeGroup group = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(brand.Site.Community.CommunityID, EmailAlertConstant.ATTRIBUTE_ID);

			int mask = (int)EmailAlertMask.GotClickAlert;
			int userSetting = member.GetAttributeInt(brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, int.Parse(group.DefaultValue));
			if((mask & userSetting) != mask)
			{
				send = false;
			}

			if(send)
			{
				EnvParameterMutualMail envParm = new EnvParameterMutualMail(getMingleMailerSiteShortName(brand),
					member.GetUserName(brand), member.GetUserName(brand), mutualMember.GetUserName(brand), mutualMember.GetUserName(brand), mutualMember.MemberID.ToString(),
					EmailMemberHelper.DetermineMemberAboutMe(brand, mutualMember, member), EmailMemberHelper.DetermineMemberAge(mutualMember, brand).ToString(),
					EmailMemberHelper.DetermineMemberThumbnail(mutualMember, brand), EmailMemberHelper.DetermineMemberRegionDisplay(mutualMember, brand),
					member.MemberID.ToString(), member.EmailAddress, getPaidNonPaidString(member, brand.Site.SiteID), brandInsertDate);

				sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.MutualMail),
					member.EmailAddress, envParm, brand);
			}
		}

	    /// <summary>
	    /// Send user uploaded photo rejection email.
	    /// </summary>
	    /// <param name="impulse"></param>
	    /// <param name="brandInsertDate"></param>
	    public void SendPhotoRejection(ImpulseBase impulse, DateTime brandInsertDate)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			PhotoRejectedImpulse imp = (PhotoRejectedImpulse)impulse;
			StormPostGreyMail sp = new StormPostGreyMail("dummy=1", null);
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.None);

			EnvParameterPhotoRejection envParm = new EnvParameterPhotoRejection(getMingleMailerSiteShortName(brand),
				sp.GetPhotoDisapproveText(imp.Comment, brand.Site.LanguageID), member.MemberID.ToString(), member.EmailAddress, brandInsertDate);

			sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.PhotoRejection),
				member.EmailAddress, envParm, brand);
		}

		/// <summary>
		/// Send news letter opt out notification email.
		/// </summary>
		/// <param name="impulse"></param>
		public void SendOptOutNotification(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			OptOutNotifyImpulse imp = (OptOutNotifyImpulse)impulse;

			EnvParameterOptOutNotification envParm = new EnvParameterOptOutNotification(getMingleMailerSiteShortName(brand), imp.Email);

			sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.OptOutNotify),
				imp.Email, envParm, brand);
		}

	    /// <summary>
	    /// Send hot list email.
	    /// </summary>
	    /// <param name="impulse"></param>
	    /// <param name="brandInsertDate"></param>
	    public void SendHotListAlert(ImpulseBase impulse, DateTime brandInsertDate)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			HotListedNotifyImpulse imp = (HotListedNotifyImpulse)impulse;
			
			if (!Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_HOTLISTED_EMAIL_NOTIFICATION", brand.Site.Community.CommunityID, brand.Site.SiteID)))
			{
				return;
			}

            IMemberDTO hotListedByMember = MailHandlerUtils.GetIMemberDTO(imp.HotListedByMemberID, MemberLoadFlags.None);
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.None);
			
			bool send = true;

			AttributeGroup group = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(brand.Site.Community.CommunityID, EmailAlertConstant.ATTRIBUTE_ID);

			int mask = (int)EmailAlertMask.HotListedAlert;
			int userSetting = member.GetAttributeInt(brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, int.Parse(group.DefaultValue));
			if((mask & userSetting) != mask)
			{
				send = false;
			}

			if(send)
			{
				string genderString = getGenderSelfString(member.GetAttributeInt(brand, "GenderMask", 0));
//				string subStatus = getSubscriptionStatusString(member.GetAttributeInt(brand, "SubscriptionStatus", 0));
				string subStatus = getPaidNonPaidString(member, brand.Site.SiteID);
				
				EnvParameterHotListAlert envParm = new EnvParameterHotListAlert(getMingleMailerSiteShortName(brand),
					member.GetUserName(brand), hotListedByMember.GetUserName(brand), EmailMemberHelper.DetermineMemberAge(hotListedByMember, brand).ToString(),
					EmailMemberHelper.DetermineMemberSeeking(brand, hotListedByMember), EmailMemberHelper.DetermineMemberThumbnail(hotListedByMember, brand),
					EmailMemberHelper.DetermineMemberThumbnail(hotListedByMember, brand), member.MemberID.ToString(),
					genderString, subStatus, member.EmailAddress, brandInsertDate);

				sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.HotListedAlert),
					member.EmailAddress, envParm, brand);
			}
		}

	    /// <summary>
	    /// Send subscription confirmation email.
	    /// </summary>
	    /// <param name="impulse"></param>
	    /// <param name="brandInsertDate"></param>
	    public void SendSubscriptionConfirmation(ImpulseBase impulse, DateTime brandInsertDate)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			SubscriptionConfirmationImpulse imp = (SubscriptionConfirmationImpulse)impulse;
			
			string dateOfPurchase = "";
			if (impulse.BrandID == 1004)
				dateOfPurchase = imp.DateOfPurchase.AddHours(10).ToString("dd/MM/yyyy");
			else if (impulse.BrandID == 1013)
				dateOfPurchase = imp.DateOfPurchase.ToString("dd/MM/yyyy");
			else if (impulse.BrandID == 1107)
				dateOfPurchase = imp.DateOfPurchase.AddHours(8).ToString("dd/MM/yyyy");
			else if (impulse.BrandID == 1105)
				dateOfPurchase = imp.DateOfPurchase.AddHours(9).ToString("dd/MM/yyyy");			
			else
				dateOfPurchase = imp.DateOfPurchase.ToString("d");
			
			string initialCost = string.Empty;
			if (brand.Site.LanguageID == (int)Language.Hebrew)
			{
				initialCost = imp.InitialCost.ToString("F0");
			}
			else
			{
				initialCost = imp.InitialCost.ToString("F2");
			}

			EnvParameterSubscriptionConfirm envParm = new EnvParameterSubscriptionConfirm(getMingleMailerSiteShortName(brand),
				imp.LastName, imp.FirstName, dateOfPurchase, imp.RenewCost.ToString(), imp.RenewDuration.ToString(), imp.RenewDurationType.ToString(),
				initialCost, imp.InitialDuration.ToString(), imp.InitialDurationType.ToString(), imp.Last4CC, imp.PlanType.ToString(), imp.CreditCardTypeID.ToString(),
				imp.ConfirmationNumber.ToString(), imp.MemberID.ToString(), imp.EmailAddress, brandInsertDate);
			
			sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.SubscriptionConfirmation),
				imp.EmailAddress, envParm, brand);
			
		}

	    /// <summary>
	    /// Send one off purchase confirmation email.
	    /// </summary>
	    /// <param name="impulse"></param>
	    /// <param name="brandInsertDate"></param>
	    public void SendOneOffPurchaseConfirmation(ImpulseBase impulse, DateTime brandInsertDate)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			OneOffPurchaseConfirmationImpulse imp = (OneOffPurchaseConfirmationImpulse)impulse;
			
			string dateOfPurchase = "";
			if (impulse.BrandID == 1004)
				dateOfPurchase = imp.DateOfPurchase.AddHours(10).ToString("dd/MM/yyyy");
			else if (impulse.BrandID == 1013)
				dateOfPurchase = imp.DateOfPurchase.ToString("dd/MM/yyyy");
			else if (impulse.BrandID == 1107)
				dateOfPurchase = imp.DateOfPurchase.AddHours(8).ToString("dd/MM/yyyy");
			else if (impulse.BrandID == 1105)
				dateOfPurchase = imp.DateOfPurchase.AddHours(9).ToString("dd/MM/yyyy");			
			else
				dateOfPurchase = imp.DateOfPurchase.ToString("d");			
		
			string initialCost = string.Empty;
			if (brand.Site.LanguageID == (int)Language.Hebrew)
			{
				initialCost = imp.InitialCost.ToString("F0");
			}
			else
			{
				initialCost = imp.InitialCost.ToString("F2");
			}

			EnvParameterOneOffPurchaseConfirm envParm = new EnvParameterOneOffPurchaseConfirm(getMingleMailerSiteShortName(brand),
				imp.LastName, imp.FirstName, dateOfPurchase, initialCost, imp.InitialDuration.ToString(), imp.InitialDurationType.ToString(),
				imp.Last4CC, imp.CreditCardTypeID.ToString(), imp.ConfirmationNumber.ToString(), imp.MemberID.ToString(), imp.EmailAddress,
                brandInsertDate);

			sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.OneOffPurchaseConfirmation),
				imp.EmailAddress, envParm, brand);
			
		}

		public void sendPaymentProfileNotification(ImpulseBase impulse, DateTime brandInsertDate)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			PmtProfileConfirmationImpulse imp = (PmtProfileConfirmationImpulse)impulse;
			
			string dateOfPurchase = "";
			if (impulse.BrandID == 1004)
				dateOfPurchase = imp.DateOfPurchase.AddHours(10).ToString("dd/MM/yyyy");
			else if (impulse.BrandID == 1013)
				dateOfPurchase = imp.DateOfPurchase.ToString("dd/MM/yyyy");
			else if (impulse.BrandID == 1107)
				dateOfPurchase = imp.DateOfPurchase.AddHours(8).ToString("dd/MM/yyyy");
			else
				dateOfPurchase = imp.DateOfPurchase.ToString("d");			
		
			Site site = null;
			Sites coll = BrandConfigSA.Instance.GetSites();

			foreach(Site s in coll)
			{
				if(s.SiteID == imp.SiteID)
				{
					site = s;
					break;
				}
			}

			EnvParameterPaymentProfileNotification envParm = new EnvParameterPaymentProfileNotification(getMingleMailerSiteShortName(brand),
				imp.LastName, imp.FirstName, dateOfPurchase, imp.Last4CC, imp.ConfirmationNumber.ToString(), imp.MemberID.ToString(), imp.EmailAddress,
                brandInsertDate);

			sendMingleMail(_mapper.GetProviderTemplateID(site, EmailType.PmtProfileConfirmation),
				imp.EmailAddress, envParm, brand);
		}

		public void sendMatchMeterInvitationMailImpulse(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			MatchMeterInvitationMailImpulse imp = (MatchMeterInvitationMailImpulse)impulse;

			Site site = null;
			Sites coll = BrandConfigSA.Instance.GetSites();

			foreach(Site s in coll)
			{
				if(s.SiteID == imp.SiteID)
				{
					site = s;
					break;
				}
			}

			EnvParameterMatchMeterInvitation envParm = new EnvParameterMatchMeterInvitation(getMingleMailerSiteShortName(brand),
				imp.SenderUsername, imp.EmailAddr, imp.RecipientEmailAddress, imp.MemberID.ToString());

			sendMingleMail(_mapper.GetProviderTemplateID(site, EmailType.MatchMeterInvitationMail),
				imp.RecipientEmailAddress, envParm, brand);

		}

		public void sendMatchMeterCongratsMailImpulse(ImpulseBase impulse, DateTime brandInsertDate)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			MatchMeterCongratsMailImpulse imp = (MatchMeterCongratsMailImpulse)impulse;

			Site site = null;
			Sites coll = BrandConfigSA.Instance.GetSites();

			foreach(Site s in coll)
			{
				if(s.SiteID == imp.SiteID)
				{
					site = s;
					break;
				}
			}

			EnvParameterMatchMeterCongrats envParm = new EnvParameterMatchMeterCongrats(getMingleMailerSiteShortName(brand),
				imp.UserName, imp.EmailAddr, imp.EmailAddr, imp.MemberID.ToString(), imp.MemberID.ToString(), brandInsertDate);

			sendMingleMail(_mapper.GetProviderTemplateID(site, EmailType.MatchMeterCongratsMail),
				imp.EmailAddr, envParm, brand);

		}

	    /// <summary>
	    /// Formerly known as ViralMail.
	    /// </summary>
	    /// <param name="impulseBase"></param>
	    /// <param name="brandInsertDate"></param>
	    public void SendClickMail(ImpulseBase impulseBase, DateTime brandInsertDate)
		{
			int counter;

			ViralImpulse impulse = (ViralImpulse) impulseBase;

			MiniprofileInfo recipientMiniprofileInfo = impulse.RecipientMiniprofileInfo;
			MiniprofileInfoCollection clickMembers = impulse.TargetMiniprofileInfoCollection;

			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);

			EnvParameterClickMail envParm = new EnvParameterClickMail(getMingleMailerSiteShortName(brand),
				MailHandlerUtils.Filter(impulse.RecipientEmailAddress), brand.Site.SiteID.ToString(), recipientMiniprofileInfo.MemberID.ToString(),
				MailHandlerUtils.Filter(recipientMiniprofileInfo.UserName), brandInsertDate);

			foreach (MiniprofileInfo currentMember in clickMembers)
			{
				string memberID, thumbnail, username, age, location, nameText;

				memberID = currentMember.MemberID.ToString();
				
				string tempThumbnail = EmailMemberHelper.DetermineBaseNoPhoto(brand);
				if (currentMember.ThumbNailWebPath != Constants.NULL_STRING)
				{
					tempThumbnail = currentMember.ThumbNailWebPath;
				}
				thumbnail = MailHandlerUtils.Filter(tempThumbnail);
                
				username = MailHandlerUtils.Filter(currentMember.UserName);
				age = EmailMemberHelper.DetermineMemberAge(currentMember.BirthDate).ToString();
				location = MailHandlerUtils.Filter(EmailMemberHelper.DetermineMemberRegionDisplay(currentMember.RegionID, brand));
				
				string miniAboutMe = currentMember.AboutMe;
				if (miniAboutMe.Length > 18)
				{
					miniAboutMe = miniAboutMe.Substring(0, 18);
				}
				nameText = MailHandlerUtils.Filter(miniAboutMe);

				envParm.AddMiniProfile(new ClickMailMiniProfile(memberID, thumbnail, username, age, location, nameText));
			}

			sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.ViralNewsLetter),
				impulse.RecipientEmailAddress, envParm, brand);
		}

	    /// <summary>
	    /// Send MatchMail using BulkMail
	    /// </summary>
	    /// <param name="impulseBase"></param>
	    /// <param name="brandInsertDate"></param>
	    public void SendMatchMail(ImpulseBase impulseBase, DateTime brandInsertDate)
		{
			MatchMailImpulse impulse = (MatchMailImpulse) impulseBase;

			MiniprofileInfo recipientMiniprofileInfo = impulse.RecipientMiniprofileInfo;
			MiniprofileInfoCollection members = impulse.TargetMiniprofileInfoCollection;

			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);

			ListCountCollection listCountCollection = ListSA.Instance.GetListCounts(recipientMiniprofileInfo.MemberID, brand.Site.Community.CommunityID, brand.Site.SiteID);

			string emailAddress, siteID, memberID, username, userAge, memberLocation, finalRecipientThumbnail;
			emailAddress = MailHandlerUtils.Filter(impulse.RecipientEmailAddress);
			siteID = brand.Site.SiteID.ToString();
			memberID = impulse.RecipientMiniprofileInfo.MemberID.ToString();
			username = MailHandlerUtils.Filter(recipientMiniprofileInfo.UserName);
			userAge = EmailMemberHelper.DetermineMemberAge(recipientMiniprofileInfo.BirthDate).ToString();
			memberLocation = EmailMemberHelper.DetermineMemberRegionDisplay(recipientMiniprofileInfo.RegionID, brand);
			
			string recipientThumbnail = EmailMemberHelper.DetermineBaseNoPhoto(brand);

			if (recipientMiniprofileInfo.ThumbNailWebPath != Constants.NULL_STRING)
			{
				recipientThumbnail = recipientMiniprofileInfo.ThumbNailWebPath;
			}

			finalRecipientThumbnail = MailHandlerUtils.Filter(recipientThumbnail);

			#region Append HotList Tokens (You XXed others)

			string numberYouHotListed, numberYouContacted, numberYouTeased, numberYouIM, numberYouViewed;

			numberYouHotListed = listCountCollection[HotListCategory.Default].ToString();
            numberYouContacted = listCountCollection[HotListCategory.MembersYouEmailed].ToString();
			numberYouTeased = listCountCollection[HotListCategory.MembersYouTeased].ToString();
			numberYouIM = listCountCollection[HotListCategory.MembersYouIMed].ToString();
			numberYouViewed = listCountCollection[HotListCategory.MembersYouViewed].ToString();
			#endregion

			string memberSearchURL;
			memberSearchURL = MailHandlerUtils.Filter(MailHandlerUtils.DetermineSearchUrl(recipientMiniprofileInfo.MemberID,
				BrandConfigSA.Instance.GetBrandByID(impulse.BrandID).Site.Community.CommunityID));
			
			

			#region Append Reverse HotList Tokens (Who XXed you)
			string  numberHotlistedYou, numberContactedYou, numberTeasedYou, numberIMYou, numberViewedYou;

			numberHotlistedYou = listCountCollection[HotListCategory.WhoAddedYouToTheirFavorites].ToString();
			numberContactedYou = listCountCollection[HotListCategory.WhoEmailedYou].ToString();
			numberTeasedYou = listCountCollection[HotListCategory.WhoTeasedYou].ToString();
			numberIMYou = listCountCollection[HotListCategory.WhoIMedYou].ToString();
			numberViewedYou = listCountCollection[HotListCategory.WhoViewedYourProfile].ToString();

			#endregion

			#region V1.3 #17638 Additional tokens for banners			
			string religion, jdateReligion, ethnicity, jdateEthnicity, maritalStatus, childrenCount, custody, isPayingMember,
				finalIsVerifiedEmail, hasApprovedPhoto, gender, seekingGender, emailVerificationCode;

			religion = recipientMiniprofileInfo.ReligionID.ToString();
			jdateReligion = recipientMiniprofileInfo.JdateReligionID.ToString();
			ethnicity = recipientMiniprofileInfo.EthnicityID.ToString();
			jdateEthnicity = recipientMiniprofileInfo.JdateEthnicityID.ToString();
			maritalStatus = recipientMiniprofileInfo.MaritalStatusID.ToString();
			childrenCount = recipientMiniprofileInfo.ChildrenCount.ToString();
			custody = recipientMiniprofileInfo.CustodyID.ToString();
			isPayingMember = recipientMiniprofileInfo.IsPayingMember.ToString();
			
			//IsVerifiedEamil
			bool isVerifiedEmail = false;
			int globalStatusMask = recipientMiniprofileInfo.GlobalStatusMask;
			if ((globalStatusMask != Constants.NULL_INT) && ((globalStatusMask & EMAIL_VERIFIED) == EMAIL_VERIFIED))
			{
				isVerifiedEmail = true;
			}
			finalIsVerifiedEmail = isVerifiedEmail.ToString();

			hasApprovedPhoto = recipientMiniprofileInfo.HasApprovedPhotos.ToString();
			gender = MailHandlerUtils.GetGenderMaskSelf(recipientMiniprofileInfo.GenderMask).ToString();
			seekingGender = MailHandlerUtils.GetGenderMaskSeeking(recipientMiniprofileInfo.GenderMask).ToString();
			emailVerificationCode = EmailVerify.HashMemberID(recipientMiniprofileInfo.MemberID).ToString();

			#endregion

			// First create the EnvParameter object here
			EnvParameterMatchMail envParm = new EnvParameterMatchMail(getMingleMailerSiteShortName(brand),
				emailAddress, siteID, memberID, username, userAge, memberLocation, finalRecipientThumbnail, numberYouHotListed,
				numberYouContacted, numberYouTeased, numberYouIM, numberYouViewed, memberSearchURL, numberHotlistedYou, numberContactedYou,
				numberTeasedYou, numberIMYou, numberViewedYou, religion, jdateReligion, ethnicity, jdateEthnicity, maritalStatus, childrenCount,
				custody, isPayingMember, finalIsVerifiedEmail, hasApprovedPhoto, gender, seekingGender, emailVerificationCode, brandInsertDate);

			#region Add the mini profiles
			
			string mpMemberID, mpUsername, mpAge, mpRegionDisplay, mpThumbnail, mmvid;
			foreach(MiniprofileInfo currentMember in members)
			{
				mpMemberID = currentMember.MemberID.ToString();
				mpUsername = MailHandlerUtils.Filter(currentMember.UserName);
				
				// Age
				int age = EmailMemberHelper.DetermineMemberAge(currentMember.BirthDate);
				mpAge = age.ToString();
				
				mpRegionDisplay = MailHandlerUtils.Filter(EmailMemberHelper.DetermineMemberRegionDisplay(currentMember.RegionID, brand));
				
				// Thumbnail
				string tempThumbnail = EmailMemberHelper.DetermineBaseNoPhoto(brand);
				if (currentMember.ThumbNailWebPath != Constants.NULL_STRING)
				{
					tempThumbnail = currentMember.ThumbNailWebPath;
				}
				mpThumbnail = MailHandlerUtils.Filter(tempThumbnail);

				// Append View Profile MMVID (Enables hot listing w/o logging in)
				mmvid = MailHandlerUtils.GetMMVID(recipientMiniprofileInfo.MemberID, currentMember.MemberID).ToString();

				MatchMiniProfile miniProfile = new MatchMiniProfile(mpMemberID, mpThumbnail, mpUsername, mpAge, mpRegionDisplay, mmvid);

				if(Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MATCHMAIL_INCLUDE_MORE_DATA")))
				{
					miniProfile.matchtext =  MailHandlerUtils.GetMatchText(recipientMiniprofileInfo, currentMember, brand);
				}

				envParm.AddMiniProfile(miniProfile);
			}

			#endregion

			sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.MatchNewsLetter),
				impulse.RecipientEmailAddress, envParm, brand);
		}

	    /// <summary>
	    /// New Members in Your Area mail
	    /// </summary>
	    /// <param name="impulseBase"></param>
	    /// <param name="brandInsertDate"></param>
	    public void SendNewMemberMail(ImpulseBase impulseBase, DateTime brandInsertDate)
		{
			NewMemberMailImpulse impulse = (NewMemberMailImpulse) impulseBase;

			MiniprofileInfo recipientMiniprofileInfo = impulse.RecipientMiniprofileInfo;
			MiniprofileInfoCollection members = impulse.TargetMiniprofileInfoCollection;

			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			
			string emailAddress, nmiaTimestamp;
			emailAddress = MailHandlerUtils.Filter(impulse.RecipientEmailAddress);
			nmiaTimestamp = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss", DateTimeFormatInfo.InvariantInfo);

			EnvParameterNewMember envParm = new EnvParameterNewMember(getMingleMailerSiteShortName(brand),
				emailAddress, nmiaTimestamp, recipientMiniprofileInfo.MemberID.ToString(), impulse.RecipientEmailAddress, brandInsertDate);

			foreach (MiniprofileInfo currentMember in impulse.TargetMiniprofileInfoCollection)
			{
				string regionDisplay = EmailMemberHelper.DetermineMemberRegionDisplay(currentMember.RegionID, brand);
				string userName = currentMember.UserName;
				string age = MailHandlerUtils.GetAge(currentMember.BirthDate).ToString();
				string imageURL = currentMember.ThumbNailWebPath;
				string memberID = currentMember.MemberID.ToString();

				NewMemberMiniProfile miniProfile = new NewMemberMiniProfile(memberID, imageURL, userName, age, regionDisplay);
				
				envParm.AddMiniProfile(miniProfile);
			}

			sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.NewMembersNewsLetter),
				impulse.RecipientEmailAddress, envParm, brand);
		}

		
		#endregion

		#region Ported over from SoapHandler

	    /// <summary>
	    /// Send acount activation email.
	    /// </summary>
	    /// <param name="impulse"></param>
	    /// <param name="brandInsertDate"></param>
	    public void SendActivationLetter(ImpulseBase impulse, DateTime brandInsertDate)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			RegVerificationImpulse imp = (RegVerificationImpulse)impulse;

            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.IngoreSACache);
			
			// This is to prevent occasional errors we get from AllowedDomains() method call. Checking against Constants.NULL_STRING since that's the default for this property.
			// This could possibly happen when a new member's registration info has not been replicated.
			if (member != null && member.EmailAddress == Constants.NULL_STRING)
			{
				EventLog.WriteEntry("Matchnet.ExternalMail.Service", "Member SendActivationLetter Exception. EmailAddress is null. MemberID:" + member.MemberID.ToString(), EventLogEntryType.Error);
				return;
			}

			string emailAddress=member.EmailAddress;

			EnvParameterRegVerification envParm = new EnvParameterRegVerification(getMingleMailerSiteShortName(brand),
				EmailVerify.GetHashCode(emailAddress, imp.MemberID,brand), member.MemberID.ToString(), member.EmailAddress, brandInsertDate);

			sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.ActivationLetter),
				member.EmailAddress, envParm, brand);
			
		}

	    /// <summary>
	    /// Send password recovery email.
	    /// </summary>
	    /// <param name="impulse">PasswordMailImpulse</param>
	    /// <param name="brandInsertDate"></param>
	    public void SendForgotPassword(ImpulseBase impulse, DateTime brandInsertDate)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			PasswordMailImpulse imp = (PasswordMailImpulse)impulse;
			int memberID = Constants.NULL_INT;

            memberID = MemberSA.Instance.GetMemberIDByEmail(imp.EmailAddress, brand.Site.Community.CommunityID);

			// To prevent invalid email addresses from calling up Member Service which ends up in exceptions.
			if (memberID == Constants.NULL_INT)
			{
				EventLog.WriteEntry("Matchnet.ExternalMail.Service", "MemberID not found. [Email Address:" + imp.EmailAddress + "]");

				return;
			}

            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(memberID, MSA.MemberLoadFlags.None);
			string password = String.Empty;

			// Member service is throwing out errors on password retrival of some members. Just log and send a blank password for now.
			try
			{
				password = MemberSA.Instance.GetPassword(imp.EmailAddress);
			}
			catch (Exception ex)
			{
				EventLog.WriteEntry("Matchnet.ExternalMail.Service", "Member SendForgotPassword Exception." + ex.ToString(), EventLogEntryType.Error);
			}
			
			EnvParameterForgotPassword envParm = new EnvParameterForgotPassword(getMingleMailerSiteShortName(brand),
				imp.EmailAddress, password, memberID.ToString(), brandInsertDate);

			sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.ForgotPassword),
				imp.EmailAddress, envParm, brand);

		}

        private void SendResetPassword(ImpulseBase impulse, DateTime brandInsertDate)
        {

            Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
            var imp = (ResetPasswordImpulse)impulse;

            var basicLogonInfo = MemberSA.Instance.GetMemberBasicLogonInfo(imp.MemberId,
                                                                           brand.Site.Community.CommunityID);
            if (basicLogonInfo == null)
                return;

            var envParm = new EnvParameterForgotPassword(getMingleMailerSiteShortName(brand),
                basicLogonInfo.EmailAddress, HttpUtility.UrlEncode(imp.ResetPasswordUrl), imp.MemberId.ToString(), brandInsertDate);

            sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.ForgotPassword),
                basicLogonInfo.EmailAddress, envParm, brand);
        }

	    /// <summary>
	    /// Send Email update email.
	    /// </summary>
	    /// <param name="impulse">EmailVerificationImpulse</param>
	    /// <param name="brandInsertDate"></param>
	    public void SendEmailChangeConfirmation(ImpulseBase impulse, DateTime brandInsertDate)
		{			
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			EmailVerificationImpulse imp = (EmailVerificationImpulse)impulse;
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MSA.MemberLoadFlags.IngoreSACache);

			string emailAddress=member.EmailAddress;

			EnvParameterEmailChangeConfirm envParm = new EnvParameterEmailChangeConfirm(getMingleMailerSiteShortName(brand),
				EmailVerify.GetHashCode(emailAddress, imp.MemberID,brand), imp.MemberID.ToString(), emailAddress, brandInsertDate);

			sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.EmailChangeConfirmation),
				emailAddress, envParm, brand);
		}

	    /// <summary>
	    /// Send message board notification email.
	    /// </summary>
	    /// <param name="impulse"></param>
	    /// <param name="brandInsertDate"></param>
	    public void SendMessageBoardInstantNotification(ImpulseBase impulse, DateTime brandInsertDate)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			MessageBoardInstantNotificationImpulse imp = (MessageBoardInstantNotificationImpulse)impulse;
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.None);

			EnvParameterMsgBoardInstantNotification envParm = new EnvParameterMsgBoardInstantNotification(getMingleMailerSiteShortName(brand),
				imp.ReplyURL, imp.BoardName, member.GetUserName(brand), imp.TopicSubject, imp.UnsubscribeURL, imp.NewReplies.ToString(),
				imp.MemberID.ToString(), member.EmailAddress, brandInsertDate);

			Site site = null;
			Sites coll = BrandConfigSA.Instance.GetSites();

			foreach(Site s in coll)
			{
				if(s.SiteID == imp.SiteID)
				{
					site = s;
					break;
				}
			}

			sendMingleMail(_mapper.GetProviderTemplateID(site, EmailType.MessageBoardInstantNotification),
				member.EmailAddress, envParm, brand);
		}

	    /// <summary>
	    /// Send mesasge board daily updates email.
	    /// </summary>
	    /// <param name="impulse"></param>
	    /// <param name="brandInsertDate"></param>
	    public void SendMessageBoardDailyUpdates(ImpulseBase impulse, DateTime brandInsertDate)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			MessageBoardDailyUpdatesImpulse imp = (MessageBoardDailyUpdatesImpulse) impulse;
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.None);
			
			EnvParameterMsgBoardDailyUpdates envParm = new EnvParameterMsgBoardDailyUpdates(getMingleMailerSiteShortName(brand),
				imp.BoardName, member.GetUserName(brand), imp.TopicSubject, imp.BrandID.ToString(), imp.ThreadID.ToString(),
				imp.UnsubscribeURL, imp.NewReplies.ToString(), imp.LastMessageID.ToString(), imp.MemberID.ToString(), member.EmailAddress,
                brandInsertDate);
			
			Site site = null;
			Sites coll = BrandConfigSA.Instance.GetSites();

			foreach(Site s in coll)
			{
				if(s.SiteID == imp.SiteID)
				{
					site = s;
					break;
				}
			}

			sendMingleMail(_mapper.GetProviderTemplateID(site, EmailType.MessageBoardDailyUpdates),
				member.EmailAddress, envParm, brand);

		}

	    /// <summary>
	    /// Send ecard to member email.
	    /// </summary>
	    /// <param name="impulse"></param>
	    /// <param name="brandInsertDate"></param>
	    public void SendEcardToMember(ImpulseBase impulse, DateTime brandInsertDate)
		{				
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			EcardToMemberImpulse imp = (EcardToMemberImpulse)impulse;

            IMemberDTO sender = MailHandlerUtils.GetIMemberDTO(imp.SenderMemberID, MemberLoadFlags.None);
            IMemberDTO recipient = MailHandlerUtils.GetIMemberDTO(imp.RecipientMemberID, MemberLoadFlags.None);	
		
			bool send = true;
			
			AttributeGroup group = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(brand.Site.Community.CommunityID, EmailAlertConstant.ATTRIBUTE_ID);
			
			int mask = (int)EmailAlertMask.ECardAlert;
			int userSetting = recipient.GetAttributeInt(brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, int.Parse(group.DefaultValue));

			// Make sure user specified to recieve ecard notification email.
			if((mask & userSetting) != mask)
			{
				send = false;
			}

			if(send)
			{
				string senderThumbnail = "";
				if (Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_VIEW_MINIPROFILE_ECARD_TO_MEMBER", brand.Site.Community.CommunityID, imp.SiteID)) || recipient.IsPayingMember(imp.SiteID))
				{
					senderThumbnail = EmailMemberHelper.DetermineMemberThumbnail(sender, brand);
				}

				string ecardUrl = EmailMemberHelper.CreateEcardUrl(recipient, brand, imp.CardUrl);
				if (ecardUrl != Constants.NULL_STRING)
				{

					EnvParameterEcardToMember envParm = new EnvParameterEcardToMember(getMingleMailerSiteShortName(brand),
						ecardUrl, sender.GetUserName(brand), senderThumbnail, imp.CardThumbnail, recipient.GetUserName(brand), recipient.EmailAddress,
						EmailMemberHelper.GetDateDisplay(imp.SentDate, brand), EmailMemberHelper.DetermineMemberAge(sender, brand).ToString(),
						EmailMemberHelper.DetermineMemberRegionDisplay(sender, brand), recipient.MemberID.ToString(), recipient.EmailAddress, brandInsertDate);
				
					/*
					Site site = null;
					Sites coll = BrandConfigSA.Instance.GetSites();

					foreach(Site s in coll)
					{
						if(s.SiteID == imp.SiteID)
						{
							site = s;
							break;
						}
					}
					*/

					sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.EcardToMember),
						recipient.EmailAddress, envParm, brand);

					
				}
			}
		}

		/// <summary>
		/// Send ecard to off site friend email.
		/// </summary>
		/// <param name="impulse"></param>
		public void SendEcardToFriend(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			EcardToFriendImpulse imp = (EcardToFriendImpulse) impulse;
            IMemberDTO sender = MailHandlerUtils.GetIMemberDTO(imp.SenderMemberID, MemberLoadFlags.None);
			
			string ecardUrl = EmailMemberHelper.CreateEcardUrl(null, brand, imp.CardUrl);
			if (ecardUrl != Constants.NULL_STRING)
			{
				EnvParameterEcardToFriend envParm = new EnvParameterEcardToFriend(getMingleMailerSiteShortName(brand),
					ecardUrl, imp.CardThumbnail, sender.GetUserName(brand), imp.RecipientEmailAddress, EmailMemberHelper.GetDateDisplay(imp.SentDate, brand),
					EmailMemberHelper.DetermineMemberAge(sender, brand).ToString(), EmailMemberHelper.DetermineMemberThumbnail(sender, brand),
					EmailMemberHelper.DetermineMemberRegionDisplay(sender, brand), imp.RecipientEmailAddress);
				
				Site site = null;
				Sites coll = BrandConfigSA.Instance.GetSites();

				foreach(Site s in coll)
				{
					if(s.SiteID == imp.SiteID)
					{
						site = s;
						break;
					}
				}

				sendMingleMail(_mapper.GetProviderTemplateID(site, EmailType.EcardToFriend),
					imp.RecipientEmailAddress, envParm, brand);
			}
		}

	    /// <summary>
	    /// Send renewal failure notification email.
	    /// </summary>
	    /// <param name="impulse"></param>
	    /// <param name="brandInsertDate"></param>
	    public void SendRenewalFailureNotification(ImpulseBase impulse, DateTime brandInsertDate)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			Sites sites = BrandConfigSA.Instance.GetSites();
			RenewalFailureNotificationImpulse imp = (RenewalFailureNotificationImpulse) impulse;
			
			foreach(Site site in sites)
			{
				if(imp.SiteID == site.SiteID)
				{
					EnvParameterRenewalFailureNotification envParm = new EnvParameterRenewalFailureNotification(getMingleMailerSiteShortName(brand),
						imp.UserName, MemberSA.Instance.GetMemberIDByEmail(imp.EmailAddress, site.Community.CommunityID).ToString(), imp.EmailAddress, brandInsertDate);

					sendMingleMail(_mapper.GetProviderTemplateID(site, EmailType.RenewalFailureNotification),
						imp.EmailAddress, envParm, brand);
				}
			}
		}

		/// <summary>
		/// Send to friend Question of the Week
		/// </summary>
		/// <param name="impulse"></param>
		public void SendMemberQuestion(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			SendMemberQuestionAndAnswerImpulse imp = (SendMemberQuestionAndAnswerImpulse)impulse;
			
			EnvParameterMemberQuestion envParm = new EnvParameterMemberQuestion(getMingleMailerSiteShortName(brand),
				brand.Site.SiteID.ToString(), imp.FriendName, imp.SenderName.ToString(), imp.Message, imp.SenderMemberID.ToString(),
				imp.QuestionId.ToString(), imp.QuestionText, imp.FriendEmail);

			sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.SendMemberQuestionAndAnswer),
				imp.FriendEmail, envParm, brand);
		}


		#endregion

		public void SendJDateMobileRegistrationConfirmation(ImpulseBase impulse, DateTime brandInsertDate)
		{
			JDateMobileRegistrationConfirmationImpulse imp = (JDateMobileRegistrationConfirmationImpulse)impulse;
			Brand brand = MailHandlerUtils.GetBrand(imp.BrandID);

			if(brand != null)
			{
				EnvParameterJDateMobileRegConfirm envParm = new EnvParameterJDateMobileRegConfirm(getMingleMailerSiteShortName(brand),
					imp.UserName, imp.MobileNumber, imp.MemberID.ToString(), imp.EmailAddress, brandInsertDate);

				sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.JDateMobileRegistrationConfirmation),
					imp.EmailAddress, envParm, brand);
			}

		}

        public void SendAllAccessNudgeEmail(ImpulseBase pImpulse, DateTime brandInsertDate)
        {
            var imp = (AllAccessEmailNudgeImpulse)pImpulse;

            var recipient = MailHandlerUtils.GetIMemberDTO(imp.TargetMemberId, MemberLoadFlags.None);
            var recipientBrand = BrandConfigSA.Instance.GetBrandByID(imp.BrandID);
            
            bool send = isUserAlertSettingOn(recipient, recipientBrand);

            if (send)
            {
                var sender = MailHandlerUtils.GetIMemberDTO(imp.SenderMemberId, MemberLoadFlags.None);
                var senderBrand = BrandConfigSA.Instance.GetBrandByID(imp.SenderBrandId);

                var senderUsername = sender.GetUserName(senderBrand);
                var senderThumbnail = EmailMemberHelper.DetermineMemberThumbnail(sender, senderBrand);
                var senderAge = EmailMemberHelper.DetermineMemberAge(sender, senderBrand).ToString();
                //var senderSeeking = EmailMemberHelper.DetermineMemberSeeking(senderBrand, sender);
                var senderGender = EmailMemberHelper.DetermineMemberGender(senderBrand, sender);
                var senderLocation = EmailMemberHelper.DetermineMemberRegionDisplay(sender, senderBrand);
                var uid = EmailMemberHelper.DetermineUIDForPixel(imp.MessageListId, imp.TargetMemberId, recipientBrand.Site.Community.CommunityID);

                // get the date of the original vip message
                var emailMessage = EmailMessageSA.Instance.RetrieveMessage(recipient.MemberID, recipientBrand.Site.Community.CommunityID, imp.MessageListId);
                var mailDate = emailMessage.Message.InsertDate;

                var envParm = new EnvParameterAllAccessNudgeEmail(imp.MessageListId.ToString(), uid, imp.EmailSubject, imp.EmailContent, mailDate.ToString("dd/MM/yyyy"),
                    sender.MemberID.ToString(), senderUsername, senderThumbnail, senderAge, senderGender, senderLocation,
                    getMingleMailerSiteShortName(recipientBrand), recipient.MemberID.ToString(), recipient.EmailAddress, brandInsertDate);

                sendMingleMail(_mapper.GetProviderTemplateID(recipientBrand.Site, EmailType.AllAccessNudgeEmail), recipient.EmailAddress, envParm, recipientBrand);
            }

        }

        public void SendAllAccessInitialEmail(ImpulseBase pImpulse, DateTime brandInsertDate)
        {
            var imp = (AllAccessInitialEmailImpulse)pImpulse;

            var recipient = MailHandlerUtils.GetIMemberDTO(imp.TargetMemberId, MemberLoadFlags.None);
            var recipientBrand = BrandConfigSA.Instance.GetBrandByID(imp.BrandID);

            bool send = isUserAlertSettingOn(recipient, recipientBrand);

            if (send)
            {
                var sender = MailHandlerUtils.GetIMemberDTO(imp.SenderMemberId, MemberLoadFlags.None);
                var senderBrand = BrandConfigSA.Instance.GetBrandByID(imp.SenderBrandId);

                var senderUsername = sender.GetUserName(senderBrand);
                var senderThumbnail = EmailMemberHelper.DetermineMemberThumbnail(sender, senderBrand);
                var senderAge = EmailMemberHelper.DetermineMemberAge(sender, senderBrand).ToString();
                //var senderSeeking = EmailMemberHelper.DetermineMemberSeeking(senderBrand, sender);
                var senderGender = EmailMemberHelper.DetermineMemberGender(senderBrand, sender);
                var senderLocation = EmailMemberHelper.DetermineMemberRegionDisplay(sender, senderBrand);
                var uid = EmailMemberHelper.DetermineUIDForPixel(imp.MessageListId, imp.TargetMemberId, recipientBrand.Site.Community.CommunityID);

                var recipientPaidStatus = recipient.IsPayingMember(recipientBrand.Site.SiteID) ? "paid" : "nonpaid";

                var envParm = new EnvParameterAllAccessInitialEmail(imp.MessageListId.ToString(), uid, imp.EmailSubject, imp.EmailContent, recipientPaidStatus,
                    senderUsername, senderThumbnail, senderAge, senderGender, senderLocation, getMingleMailerSiteShortName(recipientBrand),
                    recipient.MemberID.ToString(), recipient.EmailAddress, brandInsertDate);

                sendMingleMail(_mapper.GetProviderTemplateID(recipientBrand.Site, EmailType.AllAccessInitialEmail), recipient.EmailAddress, envParm, recipientBrand);
            }
        }

        public void SendAllAccessReplyEmail(ImpulseBase pImpulse, DateTime brandInsertDate)
        {
            var imp = (AllAccessReplyEmailImpulse)pImpulse;
            
            var recipientBrand = BrandConfigSA.Instance.GetBrandByID(imp.BrandID);
            var recipient = MailHandlerUtils.GetIMemberDTO(imp.TargetMemberId, MemberLoadFlags.None);

            bool send = isUserAlertSettingOn(recipient, recipientBrand);

            if (send)
            {
                var sender = MailHandlerUtils.GetIMemberDTO(imp.SenderMemberId, MemberLoadFlags.None);
                var senderBrand = BrandConfigSA.Instance.GetBrandByID(imp.SenderBrandId);

                var senderUsername = sender.GetUserName(senderBrand);
                var senderThumbnail = EmailMemberHelper.DetermineMemberThumbnail(sender, senderBrand);
                var senderAge = EmailMemberHelper.DetermineMemberAge(sender, senderBrand).ToString();
                //var senderSeeking = EmailMemberHelper.DetermineMemberSeeking(senderBrand, sender);
                var senderGender = EmailMemberHelper.DetermineMemberGender(senderBrand, sender);
                var senderLocation = EmailMemberHelper.DetermineMemberRegionDisplay(sender, senderBrand);

                var envParm = new EnvParameterAllAccessReplyEmail(imp.MessageListId.ToString(), imp.EmailSubject, imp.EmailContent, senderUsername, senderThumbnail,
                    senderAge, senderGender, senderLocation, getMingleMailerSiteShortName(recipientBrand), recipient.MemberID.ToString(), recipient.EmailAddress,
                    brandInsertDate);

                sendMingleMail(_mapper.GetProviderTemplateID(recipientBrand.Site, EmailType.AllAccessReplyEmail), recipient.EmailAddress, envParm, recipientBrand);
            }
        }
        public void SendRegistrationCapture(ImpulseBase pImpulse)
        {
            var imp = (ScheduledNotificationImpulse)pImpulse;
            var tags = new CustomDataProvider(imp.BrandID,imp.MemberID,imp.EmailAddress).Get();
            Log.LogInfoMessage("Enter SendRegistrationCapture - " + pImpulse.GetQueryString(), tags);
            var brand = BrandConfigSA.Instance.GetBrandByID(imp.BrandID);
          
            if(brand == null)
                return;
            var envParm = new EnvParameterRegistrationCapture(brand.Site.SiteID,getMingleMailerSiteShortName(brand),  imp.EmailAddress,imp.ScheduleGUID);
            sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.RegistrationCapture), imp.EmailAddress, envParm, brand);
            Log.LogInfoMessage("Leave SendRegistrationCapture - " + pImpulse.GetQueryString(), tags);
        }

	    /// <summary>
	    /// Send profile changed confirmation email
	    /// </summary>
	    /// <param name="impulse"></param>
	    /// <param name="brandInsertDate"></param>
	    public void SendProfileChangedConfirmation(ImpulseBase impulse, DateTime brandInsertDate)
        {
            Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
            ProfileChangedConfirmationImpulse imp = (ProfileChangedConfirmationImpulse)impulse;

            if (imp.Fields.Count > 0)
            {
                EnvParameterProfileChangedConfirm envParm = new EnvParameterProfileChangedConfirm(getMingleMailerSiteShortName(brand),
                    imp.FirstName, imp.MemberID.ToString(), imp.EmailAddress, imp.Username, imp.Fields.ToArray(), brandInsertDate);

                sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.ProfileChangedConfirmation),
                    imp.EmailAddress, envParm, brand);
            }
        }
		#endregion

		#region PUBLIC PROPERTY
		public static MingleMailerHandler Instance
		{
			get
			{
				if(_instance == null)
				{
					lock(_syncBlock)
					{
						if(_instance == null)
						{
							_instance = new MingleMailerHandler();
						}
					}
				}
				return _instance;
			}
		}

		#endregion
	}
}
