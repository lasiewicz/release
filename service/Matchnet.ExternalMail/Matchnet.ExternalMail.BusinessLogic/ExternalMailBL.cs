using System;
using System.Diagnostics;
using System.Messaging;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;
using Matchnet.ExternalMail.BusinessLogic.Monitoring;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.ExternalMail.ValueObjects.DoNotEmail;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Spark.Logger;
using Spark.Logging;
using ServiceConstants = Matchnet.ExternalMail.ValueObjects.ServiceConstants;

namespace Matchnet.ExternalMail.BusinessLogic
{
	/// <summary>
	/// 
	/// </summary>
	public class ExternalMailBL
	{
		#region Public Variables

		public static readonly ExternalMailBL Instance = new ExternalMailBL();

		public static PerformanceCounter ReceivedFromQueue;
		public static PerformanceCounter NullMessageInfo;
		public static PerformanceCounter ComposeFailed;
		public static PerformanceCounter ProhibitedDomain;
		public static PerformanceCounter SuccessfulSend;
		public static PerformanceCounter UnsuccessfulSend;
		public static PerformanceCounter ErrorProcessingQueue;
		public static PerformanceCounter UncaughtException;
		public static PerformanceCounter AvgSOAPSendPerSec;
		public static PerformanceCounter AvgSOAPSendPerSecBase;
		public static PerformanceCounter AvgSOAPImportPerSec;
		public static PerformanceCounter AvgSOAPImportPerSecBase;

		#endregion

		#region Private Variables

		private static bool countersInitialized = false;
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(ExternalMailBL));

		#endregion

		#region Private Methods

		private ExternalMailBL()
		{
		}

		private bool RejectBecauseOfDNE(ImpulseBase impulse, int siteID)
		{

			bool reject = false;
			
			IMemberDTO member = null;
			DoNotEmailEntry entry = null;
			string emailAddress = string.Empty;
            try
            {
                if (impulse is CheckSubConfirmImpulse)
                {
                    member = MailHandlerUtils.GetIMemberDTO(((CheckSubConfirmImpulse) impulse).MemberID, MemberLoadFlags.None);
                    emailAddress = member.EmailAddress;
                }

                if (impulse is CollegeLuvPromoImpulse)
                {
                    member = MailHandlerUtils.GetIMemberDTO(((CollegeLuvPromoImpulse) impulse).MemberID, MemberLoadFlags.None);
                    emailAddress = member.EmailAddress;
                }

                if (impulse is EmailVerificationImpulse)
                {
                    member = MailHandlerUtils.GetIMemberDTO(((EmailVerificationImpulse) impulse).MemberID, MemberLoadFlags.None);
                    emailAddress = member.EmailAddress;
                }

                if (impulse is HotListedNotifyImpulse)
                {
                    member = MailHandlerUtils.GetIMemberDTO(((HotListedNotifyImpulse) impulse).MemberID, MemberLoadFlags.None);
                    emailAddress = member.EmailAddress;
                }

                if (impulse is MessageBoardDailyUpdatesImpulse)
                {
                    member = MailHandlerUtils.GetIMemberDTO(((MessageBoardDailyUpdatesImpulse) impulse).MemberID, MemberLoadFlags.None);
                    emailAddress = member.EmailAddress;
                }

                if (impulse is MessageBoardInstantNotificationImpulse)
                {
                    member = MailHandlerUtils.GetIMemberDTO(
                        ((MessageBoardInstantNotificationImpulse) impulse).MemberID, MemberLoadFlags.None);
                    emailAddress = member.EmailAddress;
                }

                if (impulse is PhotoApprovedImpulse)
                {
                    member = MailHandlerUtils.GetIMemberDTO(((PhotoApprovedImpulse) impulse).MemberID,MemberLoadFlags.None);
                    emailAddress = member.EmailAddress;
                }

                if (impulse is PhotoRejectedImpulse)
                {
                    member = MailHandlerUtils.GetIMemberDTO(((PhotoRejectedImpulse) impulse).MemberID, MemberLoadFlags.None);
                    emailAddress = member.EmailAddress;
                }

                if (impulse is EcardToFriendImpulse)
                {
                    emailAddress = ((EcardToFriendImpulse) impulse).RecipientEmailAddress;
                }

                if (impulse is MatchMailImpulse)
                {
                    emailAddress = ((MatchMailImpulse) impulse).RecipientEmailAddress;
                }

                if (impulse is MatchMeterInvitationMailImpulse)
                {
                    emailAddress = ((MatchMeterInvitationMailImpulse) impulse).RecipientEmailAddress;
                }

                if (impulse is ViralImpulse)
                {
                    emailAddress = ((ViralImpulse) impulse).RecipientEmailAddress;
                }

                if (impulse is EcardToMemberImpulse)
                {
                    member = MailHandlerUtils.GetIMemberDTO(((EcardToMemberImpulse) impulse).RecipientMemberID, MemberLoadFlags.None);
                    emailAddress = member.EmailAddress;
                }

                if (impulse is MatchMeterCongratsMailImpulse)
                {
                    emailAddress = ((MatchMeterCongratsMailImpulse) impulse).EmailAddr;
                }

                if (impulse is MutualYesNotifyImpulse)
                {
                    member = MailHandlerUtils.GetIMemberDTO(((MutualYesNotifyImpulse) impulse).TargetMemberID, MemberLoadFlags.None);
                    emailAddress = member.EmailAddress;
                }

                if (impulse is OptOutNotifyImpulse)
                {
                    emailAddress = ((OptOutNotifyImpulse) impulse).Email;
                }

                if (impulse is PmtProfileConfirmationImpulse)
                {
                    emailAddress = ((PmtProfileConfirmationImpulse) impulse).EmailAddress;
                }

                if (impulse is SubscriptionConfirmationImpulse)
                {
                    emailAddress = ((SubscriptionConfirmationImpulse) impulse).EmailAddress;
                }

                if (impulse is SendMemberImpulse)
                {
                    emailAddress = ((SendMemberImpulse) impulse).FriendEmail;
                }

                if (impulse is SendMemberColorCodeImpulse)
                {
                    emailAddress = ((SendMemberColorCodeImpulse) impulse).FriendEmail;
                }

                if (impulse is SendMemberColorCodeQuizInviteImpulse)
                {
                    emailAddress = ((SendMemberColorCodeQuizInviteImpulse) impulse).RecipientEmail;
                }

                if (impulse is ColorCodeQuizConfirmationImpulse)
                {
                    emailAddress = ((ColorCodeQuizConfirmationImpulse) impulse).EmailAddress;
                }

                if (impulse is OneOffPurchaseConfirmationImpulse)
                {
                    emailAddress = ((OneOffPurchaseConfirmationImpulse) impulse).EmailAddress;
                }

                if (impulse is JDateMobileRegistrationConfirmationImpulse)
                {
                    emailAddress = ((JDateMobileRegistrationConfirmationImpulse) impulse).EmailAddress;
                }

                if (impulse is InternalMailNotifyImpulse)
                {
                    member = MailHandlerUtils.GetIMemberDTO(((InternalMailNotifyImpulse) impulse).MemberID, MemberLoadFlags.None);
                    emailAddress = member.EmailAddress;
                }
                if (impulse is SodbPhotoUploadImpulse)
                {
                    emailAddress = ((SodbPhotoUploadImpulse) impulse).EmailAddress;
                }

                if (impulse is ImpulseBaseEmail)
                {
                    if (((ImpulseBaseEmail) impulse).TargetMemberId > 0)
                    {
                        var site = BrandConfigSA.Instance.GetSiteByID(siteID);
                        var logonOnlySite = Convert.ToBoolean(RuntimeSettings.GetSetting(SettingConstants.LOGON_ONLY_SITE,
                            site.Community.CommunityID, siteID));

                        if (logonOnlySite)
                        {
                            var basicLogonInfo =MemberSA.Instance.GetMemberBasicLogonInfo(((ImpulseBaseEmail) impulse).TargetMemberId,
                                                                          site.Community.CommunityID);
                            emailAddress = basicLogonInfo.EmailAddress;
                        }
                        else
                        {
                            member = MailHandlerUtils.GetIMemberDTO(((ImpulseBaseEmail)impulse).TargetMemberId, MemberLoadFlags.None);
                            emailAddress = member.EmailAddress;    
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new BLException("Error while looking up email address for DNE.", ex);
            }
			
			if(emailAddress != string.Empty)
			{
                try
                {
				entry = DoNotEmailBL.Instance.GetEntryBySiteAndEmailAddress(siteID, emailAddress);
                }
                catch (Exception ex)
                {
                    throw new BLException("Error retrieving from DNE", ex);
                }

				if(entry != null)
				{
					reject = true;
				}
			}

			return reject;


		}

		#endregion

		#region Public Methods

		/// <summary>
		/// 
		/// </summary>
		/// <param name="impulse"></param>
		public void Enqueue(ImpulseBase impulse)
		{
			string queuePath = null;

			try
			{
				Site site = null;

				/// HACK: In the cases where an unmatching brandID/siteID combination is passed in. 
				if(impulse is RenewalFailureNotificationImpulse || impulse is EcardToFriendImpulse || impulse is EcardToMemberImpulse 
					|| impulse is MessageBoardDailyUpdatesImpulse || impulse is MessageBoardInstantNotificationImpulse
					|| impulse is PmtProfileConfirmationImpulse)
				{
					int siteId = Constants.NULL_INT;

					if(impulse is RenewalFailureNotificationImpulse)
						siteId = ((RenewalFailureNotificationImpulse)impulse).SiteID;
					else if(impulse is EcardToFriendImpulse)
						siteId = ((EcardToFriendImpulse)impulse).SiteID;
					else if(impulse is EcardToMemberImpulse)
						siteId = ((EcardToMemberImpulse)impulse).SiteID;
					else if(impulse is MessageBoardDailyUpdatesImpulse)
						siteId = ((MessageBoardDailyUpdatesImpulse)impulse).SiteID;
					else if(impulse is PmtProfileConfirmationImpulse)
						siteId = ((PmtProfileConfirmationImpulse)impulse).SiteID;
					else 
						siteId = ((MessageBoardInstantNotificationImpulse)impulse).SiteID;

					Sites coll = BrandConfigSA.Instance.GetSites();

					foreach(Site s in coll)
					{
						if(s.SiteID == siteId)
						{
							site = s;
							break;
						}
					}		
				}
				else if(impulse is SMSAlertImpulseBase)
				{
					int siteId = Constants.NULL_INT;

					siteId = ((SMSAlertImpulseBase)impulse).SiteID;

					Sites coll = BrandConfigSA.Instance.GetSites();

					foreach(Site s in coll)
					{
						if(s.SiteID == siteId)
						{
							site = s;
							break;
						}
					}
				}
				else
				{
					Brand brand = BrandConfigSA.Instance.GetBrandByID(impulse.BrandID);
					site = brand.Site;
				}

				if (site == null)
				{
					throw new BLException("Site is not found. [BrandID:" + impulse.BrandID.ToString() + ", ImpulseType:" + 
						impulse.ToString());
				}
				//07/2009 SK replacing hardcoded switch with setting
				bool active = Conversion.CBool(RuntimeSettings.GetSetting("SITE_ACTIVE_FLAG", site.Community.CommunityID, site.SiteID));
				if(!active)
				{
						System.Diagnostics.EventLog.WriteEntry(ExternalMail.ValueObjects.ServiceConstants.SERVICE_NAME, 
							"Site:" + site.Name + " is not handled by this service.",
							System.Diagnostics.EventLogEntryType.Warning);
						return;
				}

                // 5/24/2013 ResetPasswordImpulse was added because when the email is asked for by the user, it's okay to bypass
                // the DNE.
			    var resetPasswordImpulse = impulse as ResetPasswordImpulse;
			    if (resetPasswordImpulse != null)
			    {
                    var tags = new CustomDataProvider(resetPasswordImpulse.BrandID).Get();
			        Log.LogInfoMessage(string.Format("Enqueue request for ResetPasswordImpulse received. DNE check will be DISABLED for this request: MemberID: {0} Site: {1}",
                                                                  resetPasswordImpulse.MemberId, site.Name),tags);
			    }

			    if (!(impulse is SMSAlertImpulseBase) && !(impulse is MessmoImpulseBase) && !(impulse is ResetPasswordImpulse))
				{
					if(RejectBecauseOfDNE(impulse, site.SiteID))
					{
						return;
					}
				}

				if(impulse is ViralImpulse || impulse is MatchMailImpulse || 
					impulse is ReportAbuseImpulse || impulse is ContactUsImpulse || impulse is InternalCorpMail 
                    || impulse is NewMemberMailImpulse || impulse is MemberQuestionImpulse
                    || impulse is BetaFeedbackImpulse)
				{
					// Above impulses always use Gray Queue.
                    queuePath = Matchnet.ExternalMail.BusinessLogic.QueueProcessing.QueueProcessor.GreyQueuePath;
				}
                //else if(impulse is SMSAlertImpulseBase)
                //{
                //    queuePath = QueueProcessor.SMSQueuePath;
                //}
                //else if(impulse is MessmoImpulseBase)
                //{
                //    if(impulse is MessmoSendInitialBatchImpulse || impulse is MessmoSendSMSImpulse)
                //    {
                //        queuePath = QueueProcessor.MessmoDelayedQueuePath;
                //    }
                //    else
                //    {
                //        queuePath = QueueProcessor.MessmoQueuePath;
                //    }
                //}
				else
				{
                    queuePath = Matchnet.ExternalMail.BusinessLogic.QueueProcessing.QueueProcessor.SoapQueuePath;
				}

				if (!MessageQueue.Exists(queuePath))
				{
					MessageQueue.Create(queuePath, true);
				}

				MessageQueue queue = new MessageQueue(queuePath);
				queue.Formatter = new BinaryMessageFormatter();
				MessageQueueTransaction trans = new MessageQueueTransaction();

				try
				{
					trans.Begin();
					queue.Send(impulse, trans);
					trans.Commit();
				}
				finally
				{
					trans.Dispose();
				}
			}
			catch (Exception ex)
			{
#if DEBUG
				Trace.Write(ex.ToString());
#endif
				string errorMessage = "Error enqueueing impulse object.";
				if(queuePath != null)
				{
					errorMessage = errorMessage + " QueuePath:" + queuePath.ToString();
				}
				throw new BLException(errorMessage, ex);
			}
		}

		#endregion

		#region Instrumentation Methods

		public static void PerfCounterInstall()
		{
			CounterCreationDataCollection ccdc = new CounterCreationDataCollection();

			ccdc.AddRange(new CounterCreationData[]
				{
					new CounterCreationData("Messages Received From Queue [IN]", "Total number of messages received from the queue", PerformanceCounterType.NumberOfItems32),
					new CounterCreationData("Null Message Info", "Null messages pulled from the queue (also tracks when exceptions were thrown waiting for messages but non arrived)", PerformanceCounterType.NumberOfItems32),
					new CounterCreationData("Compose Failed [OUT]", "Messages that were not sent because the compose failed - typically due to an exception after bad data in an impulse", PerformanceCounterType.NumberOfItems32),
					new CounterCreationData("Prohibited Domain [OUT]", "Messages that were not sent because of domain restrictions (typically only turned on in development mode)", PerformanceCounterType.NumberOfItems32),
					new CounterCreationData("Successful Send [OUT]", "SMTP message was successfully sent", PerformanceCounterType.NumberOfItems32),
					new CounterCreationData("Unsuccessful Send [OUT]", "SMTP message could not be sent due to protocol or load problems", PerformanceCounterType.NumberOfItems32),
					new CounterCreationData("Error Processing Queue [OUT]", "There was an unhandled exception inside the queue processing block", PerformanceCounterType.NumberOfItems32),
					new CounterCreationData("Uncaught Exception [OUT]", "There was an unhandled exception in the outer part of the processCycle loop", PerformanceCounterType.NumberOfItems32),
					new CounterCreationData("Avg SOAP Send/sec", "Average SOAP send duration time in seconds", PerformanceCounterType.AverageTimer32),
					new CounterCreationData("Avg SOAP Send/sec base", "Average SOAP send duration time in seconds base", PerformanceCounterType.AverageBase),
					new CounterCreationData("Avg SOAP Import/sec", "Average SOAP import duration time in seconds", PerformanceCounterType.AverageTimer32),
					new CounterCreationData("Avg SOAP Import/sec base", "Average SOAP import duration time in seconds base", PerformanceCounterType.AverageBase)
				});

			PerformanceCounterCategory.Create(ServiceConstants.SERVICE_NAME, ServiceConstants.SERVICE_NAME, ccdc);
		}


		public static void PerfCounterUninstall()
		{
			PerformanceCounterCategory.Delete(ServiceConstants.SERVICE_NAME);
		}


		public static void InitPerfCounters()
		{
			if (!countersInitialized)
			{
				ReceivedFromQueue = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Messages Received From Queue [IN]", false);
				NullMessageInfo = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Null Message Info", false);
				ComposeFailed = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Compose Failed [OUT]", false);
				ProhibitedDomain = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Prohibited Domain [OUT]", false);
				SuccessfulSend = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Successful Send [OUT]", false);
				UnsuccessfulSend = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Unsuccessful Send [OUT]", false);
				ErrorProcessingQueue = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Error Processing Queue [OUT]", false);
				UncaughtException = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Uncaught Exception [OUT]", false);
				AvgSOAPSendPerSec = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Avg SOAP Send/sec", false);
				AvgSOAPSendPerSecBase = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Avg SOAP Send/sec base", false);
				AvgSOAPImportPerSec = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Avg SOAP Import/sec", false);
				AvgSOAPImportPerSecBase = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Avg SOAP Import/sec base", false);

				ResetPerfCounters();

				EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Performance counters initialized", EventLogEntryType.Information);

				countersInitialized = true;
			}
		}


		private static void ResetPerfCounters()
		{
			ReceivedFromQueue.RawValue = 0;
			NullMessageInfo.RawValue = 0;
			ComposeFailed.RawValue = 0;
			ProhibitedDomain.RawValue = 0;
			SuccessfulSend.RawValue = 0;
			UnsuccessfulSend.RawValue = 0;
			ErrorProcessingQueue.RawValue = 0;
			UncaughtException.RawValue = 0;
			AvgSOAPSendPerSec.RawValue = 0;
			AvgSOAPSendPerSecBase.RawValue = 0;
			AvgSOAPImportPerSec.RawValue = 0;
			AvgSOAPImportPerSecBase.RawValue = 0;
		}

		#endregion
	}
}
