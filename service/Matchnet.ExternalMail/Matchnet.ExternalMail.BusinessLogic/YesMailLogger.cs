using System;
using System.IO;
using System.Data;
using System.Text;
using System.Diagnostics;
using System.Collections;
using System.Web.Services.Protocols;

using Matchnet.Data;
using Matchnet.Exceptions;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects;
using MSA = Matchnet.Member.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.ExternalMail.ValueObjects.StormpostSoap;
using Matchnet.ExternalMail.BusinessLogic.YesMailWebService;

namespace Matchnet.ExternalMail.BusinessLogic
{
	/// <summary>
	/// Summary description for YesMailLogger.
	/// </summary>
	public class YesMailLogger
	{
		private static bool _enabled = false;

		public static void YesMailEnqueue(string msg)
		{
			if(_enabled)
			{
				using(StreamWriter sw = new StreamWriter("C:\\YesMailEnqueue.txt", true))
				{
					sw.WriteLine(DateTime.Now.ToLongTimeString());
					sw.WriteLine("\r\n");
					sw.WriteLine(msg);
					sw.WriteLine("\r\n\r\n");
				}
			}			
		}
	
		public static void YesMailDequeue(string msg)
		{
			if(_enabled)
			{
				using(StreamWriter sw = new StreamWriter("C:\\YesMailDequeue.txt", true))
				{
					sw.WriteLine(DateTime.Now.ToLongTimeString());
					sw.WriteLine("\r\n");
					sw.WriteLine(msg);
					sw.WriteLine("\r\n\r\n");
				}
			}			
		}

		public static void Log (int memberId, Site site, string masterId, string recipientEmail, string[] token)
		{
			if(_enabled)
			{
				YesMailTemplateMapper mapper = new YesMailTemplateMapper();
				string siteName = site.Name;
				string emailType = 	mapper.GetEmailType(site, masterId);
				StreamWriter sw = new StreamWriter("c:\\yesmail.txt", true);		
				StringBuilder sb = new StringBuilder();
			
				sb.Append("------------");
				sb.Append(DateTime.Now.ToLongTimeString());
				sb.Append("-------------");
				sb.Append("\r\n");
				sb.Append(siteName.ToUpper());
				sb.Append(": ");
				sb.Append(emailType);
				sb.Append("\r\n");
				sb.Append("\t");
				sb.Append("memberId: ");
				sb.Append(memberId.ToString());
				sb.Append("\r\n");
				sb.Append("\t");
				sb.Append("siteId: ");
				sb.Append(site.SiteID.ToString());
				sb.Append("\r\n");
				sb.Append("\t");
				sb.Append("masterId: ");
				sb.Append(masterId);
				sb.Append("\r\n");
				sb.Append("\t");
				sb.Append("recipientEmail: ");
				sb.Append(recipientEmail);
				sb.Append("\r\n");
				sb.Append("\t");
				sb.Append("token: ");
				foreach(string s in token)
				{
					sb.Append(s);
					sb.Append(" ");
				}
			
				sw.WriteLine(sb.ToString());
				sw.Close();
			}			
		}

		public static void Log(string msg)
		{
			if(_enabled)
			{
				using(StreamWriter sw = new StreamWriter("c:\\yesmail.txt", true))
				{
					sw.WriteLine(msg);
				}
			}			
		}
	}
}
