using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;

using Matchnet.Caching;
using Matchnet.Data;


namespace Matchnet.ExternalMail.BusinessLogic
{
	/// <summary>
	/// TODO - This class needs to be refactored in order to make it match the new model of
	/// ValueObjects that is being used under Bedrock.  Right now it still uses the old
	/// style of a Populate method.
	/// </summary>
	public class Classification
	{
		int _Verified;
		int _PrivateLabelID;

		private const string HASH_SEED = "GotDimSum";
		private const string HEADER_PRIORITY = "X-MatchnetPriority";
		private const string HEADER_MAILTYPE = "X-MatchnetMailType";

		private int _EmailClassificationID;
		private int _MemberID;
		private string _EmailType;
		private int _DeliveryTypeID;
		private string _Account;
		private string _Signature;
		private HeaderCollection _HeaderCollection;
		private bool _isValidToSend = false;

		public Classification() 
		{
			_HeaderCollection = new HeaderCollection();
			_isValidToSend = false;
		}

		#region PUBLIC PROPERTIES

		public int EmailClassificationID 
		{
			get 
			{
				return _EmailClassificationID;
			}
		}

		public int MemberID 
		{
			get 
			{
				return _MemberID;
			}
		}	

		public string EmailType 
		{
			get 
			{
				return _EmailType;
			}
		}

		public int DeliveryTypeID 
		{
			get 
			{
				return _DeliveryTypeID;
			}
		}

		public bool Verified 
		{
			get 
			{
				return _Verified == 1;
			}
		}

		public string Signature 
		{
			get 
			{
				return _Signature;
			}
		}

		public string Account 
		{
			get 
			{
				return _Account;
			}
		}

		public HeaderCollection Headers 
		{
			get 
			{
				return _HeaderCollection;
			}
		}
		
		public bool IsValidToSend
		{
			get{return _isValidToSend;}
		}
		#endregion

		public void Populate(int emailClassificationID, int memberID, int privateLabelID)
		{
			_EmailClassificationID = emailClassificationID;
			_MemberID = memberID;
			_PrivateLabelID = privateLabelID;
			
			string cacheKey = "EMAILCLASSIFICATION:" + emailClassificationID;

			object val = Cache.Instance[cacheKey];

			if (val == null) 
			{
				Command command = new Command();
				command.LogicalDatabaseName = "mnSystem";
				command.StoredProcedureName = "up_EmailClassification_List";
				command.AddParameter("@EmailClassificationID", SqlDbType.Int, ParameterDirection.Input, emailClassificationID);
				DataTable table = Client.Instance.ExecuteDataTable(command);

				if (table != null && table.Rows.Count > 0)
				{
					DataRow row = table.Rows[0];
					_EmailType = Convert.ToString(row["Description"]);
					_Account = Convert.ToString(row["Account"]);
					_DeliveryTypeID = Convert.ToInt32(row["DeliveryTypeID"]);

					string valueToCache = ToCacheString();
					CacheableString emailInfo = new CacheableString(valueToCache, cacheKey);
					Cache.Instance.Add(emailInfo);
				}
			} 
			else 
			{
				CacheableString cachedObject = (CacheableString)val;
				FromCacheString(cachedObject.MyValue);
			}
			PopulateHeaders();
		}

		private string ToCacheString() 
		{
			return _EmailType + ":" + _DeliveryTypeID + ":" + _Account;
		}

		private void FromCacheString(string cacheString) 
		{
			string[] tokens = cacheString.Split(new char[] { ':' });
			_EmailType = tokens[0];
			_DeliveryTypeID = Convert.ToInt32(tokens[1]);
			_Account = tokens[2];
		}

		private void PopulateHeaders() 
		{
			Header priority = new Header();
			priority.Name = HEADER_PRIORITY;
			priority.Value = Convert.ToString(_DeliveryTypeID);
			_HeaderCollection.Add(priority);

			Header type = new Header();
			type.Name = HEADER_MAILTYPE;
			string prefix = MailTypePrefix();
			type.Value = prefix + "-" + GetSignature(prefix);
			_HeaderCollection.Add(type);
		}

		private string MailTypePrefix() 
		{
			return _PrivateLabelID + "-" + _MemberID + "-" + _EmailClassificationID + "-" + _Verified;
		}

		private string GetSignature(string prefix) 
		{
			MD5 md5 = MD5.Create();
			_Signature = BitConverter.ToString(md5.ComputeHash(Encoding.ASCII.GetBytes(HASH_SEED + prefix))).Replace("-",null);
			return _Signature;
		}

		public bool IsValidHeader(string header) 
		{
			int delimeterPos = header.LastIndexOf("-");
			string prefix = header.Substring(0, delimeterPos);
			string suffix = header.Substring(delimeterPos + 1);
			return GetSignature(prefix) == suffix;
		}

	}
}
