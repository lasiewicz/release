using System;
using System.IO;
using System.Data;
using System.Text;
using System.Diagnostics;
using System.Collections;
using System.Web.Services.Protocols;

using Matchnet.Data;
using Matchnet.Exceptions;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.BulkMail;
using MSA = Matchnet.Member.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.ExternalMail.ValueObjects.StormpostSoap;
using Matchnet.ExternalMail.BusinessLogic.YesMailWebService;
using Matchnet.EmailLibrary;
using Matchnet.ExternalMail.BusinessLogic.Monitoring;
using Spark.Logger;
using Spark.Logging;

namespace Matchnet.ExternalMail.BusinessLogic
{
	/// <summary>
	/// Yes Mail email provider.
	/// </summary>
	public class YesMailBL: IMailBL
    {
		#region Private Variables

	    private bool _isDebug = false;
        private const string CLASS_NAME = "YesMailBL";
		private MailTemplateMapper mapper = new MailTemplateMapper("YesMailMapping.xml");
		private Sites siteList = BrandConfigSA.Instance.GetSites();
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(YesMailBL));
		private SparkEmailMessagingService Service
		{
			get
			{
				return new SparkEmailMessagingService();			
			}
		}
		private enum ReturnCode
		{
			UnhandledError = 0,
			Success = 1,
			DatabaseDown = 10,
			SiteIdNotAvailable = 20,
			MemberIdNotAvailable = 30,
			MasterIdNotAvailable = 40,
			RecipientEmailNotAvailable = 50,
			TokenNotAvailable = 60,
		}

		private AllowedDomains _allowedDomains;

		#endregion

		#region Public Variables

		public static readonly YesMailBL Instance = new YesMailBL();

		/// <summary>
		/// Pipe delimiter YesMail has specified.
		/// </summary>
		public const string DELIMITER = @"|";
		public const string LINEDELIMITER = "^";

		/// <summary>
		/// Current bulk mail types
		/// </summary>



		#endregion

		#region Constructors

		private YesMailBL()
		{
			_allowedDomains = new AllowedDomains(ServiceConstants.SERVICE_NAME);
		}

	    public bool IsDebug
	    {
	        get { return _isDebug; }
	        set { _isDebug = value; }
	    }

	    #endregion

		#region Public Methods
		/// <summary>
		/// Handles sending bulk mails in the daily export format to mnFileExchanges..BulkMail
		/// </summary>
		/// <param name="bulkMailType"></param>
		/// <param name="memberID"></param>
		/// <param name="siteID"></param>
		/// <param name="emailAddress"></param>
		/// <param name="tokenValues"></param>
		public void SendUserExportData(BulkMailType bulkMailType, int memberID, int siteID, string emailAddress, ArrayList tokenValues)
		{
			string tokenKey = string.Empty;
			string tokenValue = string.Empty;
			string tokenDBValue = string.Empty;
			int lineNumber = 0;							
			
			//for ( int i = 0; i < tokenValues.Count; i++ )  
			foreach(Token token in tokenValues)
			{
				lineNumber++;
				tokenKey = token.Name;
				tokenValue = token.Value.Replace(Matchnet.Constants.NULL_INT.ToString(), String.Empty);

				Command command = new Command("mnFileExchange", "up_UserExportData_Insert", 0);

				command.AddParameter("@BulkMailTypeID", SqlDbType.Int, ParameterDirection.Input, (int)bulkMailType);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
				command.AddParameter("@LineOrderID", SqlDbType.Int, ParameterDirection.Input, lineNumber);
				command.AddParameter("@AttributeName", SqlDbType.VarChar, ParameterDirection.Input, tokenKey);
				command.AddParameter("@AttributeValue", SqlDbType.NVarChar, ParameterDirection.Input, tokenValue);

				try
				{
					if (_allowedDomains.IsAllowedDomain(emailAddress))
					{
						Client.Instance.ExecuteAsyncWrite(command);
					}
				}
				catch(Exception ex)
				{
					throw new Exception("Error writing Bulkmail entry. [MemberID:" + memberID.ToString() + 
						", SiteID:" + siteID.ToString() + ", EmailAddress:" + emailAddress, ex);
				}
			}

#if DEBUG
			string delimitedTokenValues = string.Empty;
			string delimitedTokenKeys = string.Empty;

			foreach(Token token in tokenValues)
			{
				delimitedTokenValues += Convert.ToString(token.Value).Replace(Matchnet.Constants.NULL_INT.ToString(), String.Empty) + DELIMITER;
				delimitedTokenKeys += Convert.ToString(token.Name).Substring(3, Convert.ToString(token.Name).Length - 3) + DELIMITER;
			}

			System.Diagnostics.Trace.WriteLine("-- begin tokens --");
			System.Diagnostics.Trace.WriteLine(delimitedTokenKeys);
			System.Diagnostics.Trace.WriteLine(delimitedTokenValues);
			System.Diagnostics.Trace.WriteLine("--end token values--");
#endif
		}



		/// <summary>
		/// Handles sending bulk mails to mnFileExchanges..BulkMail
		/// </summary>
		/// <param name="bulkMailType"></param>
		/// <param name="memberID"></param>
		/// <param name="siteID"></param>
		/// <param name="emailAddress"></param>
		/// <param name="tokenValues"></param>
		public void SendBulkMail(BulkMailType bulkMailType, int memberID, int siteID, string emailAddress, SortedList tokenValues)
		{
			Command command = new Command("mnFileExchange", "up_BulkMail_Save", 0);

			command.AddParameter("@BulkMailTypeID", SqlDbType.Int, ParameterDirection.Input, (int)bulkMailType);
			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
			command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, emailAddress);

			string delimitedTokenValues = string.Empty;
			IList tokenValueList = tokenValues.GetValueList();
			for (int i = 0; i < tokenValues.Count; i++)
			{
				// Per YesMail's request, we need to replace out null int value with an empty value.
				delimitedTokenValues += Convert.ToString(tokenValueList[i]).Replace(Matchnet.Constants.NULL_INT.ToString(), String.Empty) + DELIMITER;
			}
			delimitedTokenValues = delimitedTokenValues.Remove(delimitedTokenValues.Length - 1, 1);

#if DEBUG
			string delimitedTokenKeys = string.Empty;
			IList tokenKeyList = tokenValues.GetKeyList();
			for (int i = 0; i < tokenValues.Count; i++)
			{
				delimitedTokenKeys += Convert.ToString(tokenKeyList[i]).Substring(3, Convert.ToString(tokenKeyList[i]).Length - 3) + DELIMITER;
			}
			delimitedTokenKeys = delimitedTokenKeys.Remove(delimitedTokenKeys.Length - 1, 1);

			System.Diagnostics.Trace.WriteLine("-- begin tokens --");
			System.Diagnostics.Trace.WriteLine(delimitedTokenKeys);
			System.Diagnostics.Trace.WriteLine(delimitedTokenValues);
			System.Diagnostics.Trace.WriteLine("--end token values--");
#endif
			command.AddParameter("@TokenValues", SqlDbType.NVarChar, ParameterDirection.Input, delimitedTokenValues);

			try
			{
				if (_allowedDomains.IsAllowedDomain(emailAddress))
				{
					Client.Instance.ExecuteAsyncWrite(command);
				}
			}
			catch(Exception ex)
			{
				throw new Exception("Error writing Bulkmail entry. [MemberID:" + memberID.ToString() + 
					", SiteID:" + siteID.ToString() + ", EmailAddress:" + emailAddress + ", DelimitedTokens:" +
					delimitedTokenValues, ex);
			}
		}
		



		/// <summary>
		/// Handles all SOAP calls to YesMail.
		/// </summary>
		/// <param name="memberId"></param>
		/// <param name="siteId"></param>
		/// <param name="masterId"></param>
		/// <param name="recipientEmail"></param>
		/// <param name="token"></param>
		/// <param name="emailType"></param>
		public void SendTriggerMail(int memberId, int siteId, int brandId, string masterId, string recipientEmail, string[] token, string[] customFields, EmailType emailType)
		{
            // customFields is not used in YesMail
            var tags = new CustomDataProvider(brandId,memberId,recipientEmail).Get();
            Log.LogInfoMessage(string.Format("ClassName:{0}, Entering Method {1}", CLASS_NAME, "SendTriggerMail"),tags);

			int returnCode = 0;
			bool allowedDomain=false;
			try
			{
				allowedDomain = _allowedDomains.IsAllowedDomain(recipientEmail);
			}
			catch (Exception ex)
			{
				returnCode = (int)ReturnCode.RecipientEmailNotAvailable;
			}
			if (allowedDomain)
			{

				try
				{
					long startTime = DateTime.Now.Ticks;

                    Log.LogInfoMessage(
                        string.Format("About to call YesMail MemberID:{0}, SiteID: {1}, MasterID: {2}, RecipientEmail: {3}", memberId, siteId, masterId, recipientEmail),
                        tags);
					returnCode = this.Service.sendEmail(memberId, siteId, masterId, recipientEmail, token);

                    Log.LogInfoMessage(string.Format("Return code from YesMail: {0}", returnCode), tags);
#if DEBUG
                    if (!IsDebug)
                    {
#endif
                        ExternalMailBL.AvgSOAPSendPerSec.IncrementBy(DateTime.Now.Ticks - startTime);
                        ExternalMailBL.AvgSOAPSendPerSecBase.Increment();
#if DEBUG
                    }
#endif
				}
				catch (Exception ex)
				{
					if (ex.ToString().IndexOf("SAXParseException") > -1)
					{
						EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, ex.ToString(), EventLogEntryType.Error);
					}
					else
					{
						throw new Exception("Yesmail network error: " + ex.Message);
					}
				}
				this.ProcessReturnCode(returnCode, siteId, brandId, memberId, masterId, recipientEmail, emailType);
			}
			else
			{
				this.ProcessReturnCode(returnCode, siteId, brandId, memberId, masterId, recipientEmail, emailType);

			}

            Log.LogInfoMessage(string.Format("ClassName:{0}, Leaving Method {1}", CLASS_NAME, "SendTriggerMail"), tags);
		}



		#endregion		

		#region Private Methods

		private void ProcessReturnCode(int code, int siteID, int brandID, int memberID, string masterID, string email, EmailType emailType)
		{			
			string logMessage = "";
			ReturnCode retCode;
			int emailTypeID = Constants.NULL_INT;
			
			#region Param Validation

			if (email == null)
			{
				// We're only logging at this point. Assign emtpy value so this method doesn't blow up.
				email = String.Empty;
			}

			try
			{
				emailTypeID = (int)emailType;
			}
			catch(Exception ex)
			{
				throw new BLException("Unknown email type.", ex);
			}

			try
			{
				retCode = (ReturnCode)code;
			}
			catch(Exception ex)
			{
				throw new BLException("Unknown YesMail return code.", ex);
			}

			#endregion
			
			if(retCode != ReturnCode.Success)
			{
				if(retCode == ReturnCode.DatabaseDown)
				{
					logMessage = "Yesmail database down.";
				}
				else if(retCode == ReturnCode.SiteIdNotAvailable)
				{
					logMessage = "SiteID not available when calling Yesmail web service.";
				}
				else if(retCode == ReturnCode.MemberIdNotAvailable)
				{
					logMessage = "MemberID not available when calling Yesmail web service.";
				}
				else if(retCode == ReturnCode.MasterIdNotAvailable)
				{
					logMessage = "MasterId not available when calling Yesmail web service.";
				}
				else if(retCode == ReturnCode.RecipientEmailNotAvailable)
				{
					logMessage = "Recipient Email not available when calling Yesmail web service.";
				}
				else if(retCode == ReturnCode.UnhandledError)
				{
					logMessage = "Unhandled error occurred from Yesmail web servcie.";
				}
				else if(retCode == ReturnCode.TokenNotAvailable)
				{
					logMessage = "Token not available when calling Yesmail web service.";
				}

				logMessage += "\r\n";
				logMessage += "Site ID: " + siteID.ToString() + "\r\n";
				logMessage += "Member ID: " + memberID.ToString() + "\r\n";
				logMessage += "Master ID: " + masterID.ToString() + "\r\n";
				logMessage += "EmailType ID: " + emailTypeID.ToString() + "\r\n";
				logMessage += "Recipient Email: " + email.ToString();

                Log.LogInfoMessage(logMessage, new CustomDataProvider(brandID, memberID, email).Get());
				
    		}	
		}


		private Site GetSite(int siteId)
		{
			foreach(Site s in siteList)
			{
				if(s.SiteID == siteId)
					return s;
			}
			return null;
		}


		#endregion
	}
}
