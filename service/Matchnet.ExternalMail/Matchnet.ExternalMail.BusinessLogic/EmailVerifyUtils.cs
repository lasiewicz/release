using System;
using System.Security.Cryptography;

using Matchnet.Security;

namespace Matchnet.ExternalMail.BusinessLogic
{
	/// <summary>
	/// Summary description for EmailVerifyUtils.
	/// </summary>
	/// 
	public  enum HashCodeMask:int
	{
		fromMemberID=0,
		fromEmailAddress=1
	}
	public class EmailVerifyUtils
	{
		static char[] hexDigits = {
									  '0', '1', '2', '3', '4', '5', '6', '7',
									  '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

		private EmailVerifyUtils()
		{

		}

		/// <summary>
		/// Hash a memberID or email for verification purposes
		/// 
		/// TODO - Perhaps put it in Matchnet.SharedLib? -dcornell
		/// </summary>
		/// <param name="memberID">memberID to hash</param>
		/// <returns>hashed memberID</returns>
		
		public static string GetHashCode(string sIn, int memberID, Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
		{	string retVal="";
			HashCodeMask mask=HashCodeMask.fromMemberID;
			try
			{
				mask = (HashCodeMask)Enum.Parse(typeof(HashCodeMask), Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EMAIL_VERIFICATION_HASH_MASK", brand.Site.Community.CommunityID, brand.Site.SiteID));
			
			}
			catch(Exception ex)
			{

			}
			if(mask==HashCodeMask.fromEmailAddress)
			{
				retVal=HashString(sIn,memberID);	
			}
			else
			{
				retVal=HashMemberID(memberID);	
			}

			return retVal;
		}

		/// <summary>
		/// Hash a memberID for email verification purposes
		/// 
		/// TODO - Perhaps put it in Matchnet.SharedLib? -dcornell
		/// </summary>
		/// <param name="memberID">memberID to hash</param>
		/// <returns>hashed memberID</returns>
		
		public static string HashMemberID(int memberID)
		{
			string mENCRYPT_KEY = "kdfajdsf";

			MD5 md5 = new MD5CryptoServiceProvider();
			byte[] result = md5.ComputeHash(System.Text.Encoding.Unicode.GetBytes(mENCRYPT_KEY + memberID.ToString()));
			string retVal = ToHexString(result) + memberID.ToString();

			return(retVal);
		}

		public static string HashString(string sIn, int memberID)
		{
			string mENCRYPT_KEY = "kdfajdsf";

			MD5 md5 = new MD5CryptoServiceProvider();
			byte[] result = md5.ComputeHash(System.Text.Encoding.Unicode.GetBytes(mENCRYPT_KEY + sIn));
			string retVal = ToHexString(result) + memberID.ToString();

			return(retVal);
		}
		public static string ToHexString(byte[] bytes) 
		{
			char[] chars = new char[bytes.Length * 2];
			for (int i = 0; i < bytes.Length; i++) 
			{
				int b = bytes[i];
				chars[i * 2] = hexDigits[b >> 4];
				chars[i * 2 + 1] = hexDigits[b & 0xF];
			}
			return new string(chars);
		}
	}
}
