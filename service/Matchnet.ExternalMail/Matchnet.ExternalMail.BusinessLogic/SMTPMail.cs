using System;
using System.Collections;
using System.Text;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;
using Matchnet.ExternalMail.BusinessLogic.Monitoring;
using Spark.Logging;
using Matchnet.ExternalMail.ValueObjects;
using Spark.Logger;

namespace Matchnet.ExternalMail.BusinessLogic
{
    public class SMTPMail : IDisposable
    {
        private const int socketReadRetries = 500;
        private const int MAX_SEND_ATTEMPTS = 10;
        private string _smtpServer;
        private Socket _socket;
        private Int16 _maxPipelineSize;
        private int _pipelineCount;

        public SMTPMail(Int16 maxPipelineSize)
        {
            _maxPipelineSize = maxPipelineSize;
        }

        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(SMTPMail));
        public string SmtpServer
        {
            get
            {
                return _smtpServer;
            }
            set
            {
                if (value != _smtpServer)
                {
                    _smtpServer = value;
                    if (HealthyConnection)
                    {
                        Disconnect();
                    }
                }
            }
        }

        public bool HealthyConnection
        {
            get
            {
                if (_socket != null && _socket.Connected)
                {
                    return (true);
                }
                else
                {
                    return (false);
                }
            }
        }


        public bool Send(SMTPMessage message)
        {
            int retryCount = 0;
            bool successfulSend = false;
            ArrayList exceptionList = new ArrayList();

            lock (this)
            {
                while (!successfulSend && retryCount < MAX_SEND_ATTEMPTS)
                {
                    retryCount++;

                    try
                    {
                        ActualSend(message);
                        successfulSend = true;
                    }
                    catch (Exception ex)
                    {
                        exceptionList.Add(ex);
                    }
                }
            }

            if (!successfulSend)
            {
                LogInabilityToSend(exceptionList, message);
            }

            return (successfulSend);
        }

        private void LogInabilityToSend(ArrayList exceptionTrail, SMTPMessage message)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Unable to send message: [");
            sb.Append(message.ToString());
            sb.Append("].  Had ");
            sb.Append(exceptionTrail.Count);
            sb.Append(" failures.  ");

            int exceptionNum = 1;
            foreach (Exception ex in exceptionTrail)
            {
                sb.Append("Failure ");
                sb.Append(exceptionNum);
                sb.Append(": [");

                sb.Append("Message: ");
                sb.Append(ex.Message);
                sb.Append(", StackTrace: ");
                sb.Append(ex.StackTrace);
                sb.Append("[");

                exceptionNum++;
            }

            EventLog.WriteEntry(Matchnet.ExternalMail.ValueObjects.ServiceConstants.SERVICE_NAME, sb.ToString(), EventLogEntryType.Warning);
        }

        private void ActualSend(SMTPMessage message)
        {
            if (!HealthyConnection)
            {
                SendInitialize();
            }
            else
            {
                SendReset();
            }

            SendFrom(message.FromAddress);
            SendTo(message.ToAddress);
            SendDataPrep();
            SendActualData(message.GetData(), message.Language);

            _pipelineCount--;

            if (_pipelineCount == 0)
            {
                Disconnect();
            }
        }

        private void SendInitialize()
        {
            Connect();
            CheckResponse(SMTPResponse.CONNECT_SUCCESS);

            SendData(string.Format("EHLO {0}\r\n", Dns.GetHostName()));
            CheckResponse(SMTPResponse.GENERIC_SUCCESS);
        }

        private void SendReset()
        {
            SendData("RSET\r\n");
            CheckResponse(SMTPResponse.GENERIC_SUCCESS);
        }

        private void SendFrom(string fromAddress)
        {
            SendData(string.Format("MAIL From: {0}\r\n", fromAddress));
            CheckResponse(SMTPResponse.GENERIC_SUCCESS);
        }

        private void SendTo(string toAddress)
        {
            SendData(string.Format("RCPT TO: {0}\r\n", toAddress));
            CheckResponse(SMTPResponse.GENERIC_SUCCESS);
        }

        private void SendDataPrep()
        {
            SendData(("DATA\r\n"));
            CheckResponse(SMTPResponse.DATA_SUCCESS);
        }

        private void SendActualData(string actualData, Language language)
        {
            SendData(actualData, language);
            CheckResponse(SMTPResponse.GENERIC_SUCCESS);
        }


        private enum SMTPResponse : int
        {
            CONNECT_SUCCESS = 220,
            GENERIC_SUCCESS = 250,
            DATA_SUCCESS = 354,
            QUIT_SUCCESS = 221
        }


        private void Connect()
        {
            IPHostEntry IPhst = Dns.Resolve(_smtpServer);
            IPEndPoint endPt = new IPEndPoint(IPhst.AddressList[0], 25);
            _socket = new Socket(endPt.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            _socket.Connect(endPt);

            _pipelineCount = _maxPipelineSize;
        }


        public void Disconnect()
        {
            lock (this)
            {
                if (_socket != null && _socket.Connected)
                {
                    _socket.Close();
                }
                _socket = null;
            }
        }


        private void SendData(string msg)
        {
            SendData(msg, Language.English);
        }

        private void SendData(string msg, Language language)
        {
            byte[] _msg = null;

            switch (language)
            {
                case Language.English:
                    _msg = Encoding.GetEncoding(1252).GetBytes(msg);	//	windows-1252 (and iso-8859-1)
                    break;
                case Language.German:
                case Language.French:
                    _msg = Encoding.GetEncoding(1252).GetBytes(msg);	//	iso-8859-1 (and windows-1252)
                    break;
                case Language.Hebrew:
                    _msg = Encoding.GetEncoding(1255).GetBytes(msg);	//	windows-1255
                    // _msg = Encoding.UTF8.GetBytes(msg);
                    break;
                default:
                    _msg = Encoding.ASCII.GetBytes(msg);
                    break;
            }

            int numBytes = _socket.Send(_msg, 0, _msg.Length, SocketFlags.None);
        }


		private void CheckResponse(SMTPResponse response_expected )
		{
			string response;
			int responseCode;
			byte[] bytes = new byte[1024];
			int numTries = 0;
            var tags = new CustomDataProvider().Get();
			while (_socket.Available == 0 && numTries < socketReadRetries)
			{
				numTries++;
				System.Threading.Thread.Sleep(100);
			}
			if(numTries >= socketReadRetries)
			{
				//	Looks like the socket went bad because of too much latency.
				//	Recycle the connection and give up.


                var loggingInfo =
                    string.Format("Socket seems to have gone bad.  Attempted {0} times to get code: {1}.  Aborting email",
                        numTries, response_expected);

				Disconnect();
                Log.LogInfoMessage(loggingInfo, tags);
                throw (new Exception(loggingInfo));
            }

            int numBytes = _socket.Receive(bytes, 0, _socket.Available, SocketFlags.None);
            response = Encoding.ASCII.GetString(bytes);
            responseCode = Convert.ToInt32(response.Substring(0, 3));
            if (responseCode != (int)response_expected)
            {
                Disconnect();

                var loggingInfo = String.Format("unexpected response from server (expected {0}, received {1} with response code of {2}", response_expected.ToString(), response, responseCode);
			    try
			    {
			        throw new Exception(loggingInfo);
			    }
			    catch (Exception ex)
			    {
                    Log.LogError(loggingInfo, ex, tags);    
			    }

                throw new Exception(loggingInfo);
            }
        }


        #region IDisposable Members

        public void Dispose()
        {
            if (HealthyConnection)
            {
                SendData("QUIT\r\n");
            }
            Disconnect();
        }
        #endregion
    }
}