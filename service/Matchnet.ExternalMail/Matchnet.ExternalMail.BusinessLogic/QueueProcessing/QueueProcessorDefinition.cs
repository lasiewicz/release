﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Matchnet.ExternalMail.BusinessLogic.QueueProcessing
{
    public class QueueProcessorDefinition
    {
        public QueueProcessorDefinition(string queuePath, MessageQueue mainQueue, MessageQueue noretryQueue, MessageQueue noretryBadEmailsQueue)
        {
            QueuePath = queuePath;
            MainQueue = mainQueue;
            NoretryQueue = noretryQueue;
            NoretryBadEmailsQueue = noretryBadEmailsQueue;
        }
        public string QueuePath { get; private set; }
        public MessageQueue MainQueue { get; private set; }
        public MessageQueue NoretryQueue { get; private set; }
        public MessageQueue NoretryBadEmailsQueue { get; private set; }
    }
}
