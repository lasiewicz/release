﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Matchnet.EmailLibrary;
using Matchnet.ExternalMail.BusinessLogic.Monitoring;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Spark.Logger;
using Spark.Logging;

namespace Matchnet.ExternalMail.BusinessLogic.QueueProcessing
{
    class EmailHelper
    {
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(EmailHelper));
        public static void sendEmail(MessageInfo messageInfo, ImpulseBase impulse, SMTPMail smtpMail, string callingClassName)
        {
            var tags = new CustomDataProvider(impulse.BrandID,0, messageInfo.ToAddress).Get();
            var brandID = impulse.BrandID;
            var language = EmailMemberHelper.DetermineLanguageFromBrandID(brandID);
            SMTPMessage smtpMessage = null;

            try
            {
                smtpMessage = new SMTPMessage(messageInfo.FromAddress,
                    messageInfo.FromName,
                    messageInfo.ToAddress,
                    messageInfo.ToName,
                    messageInfo.Subject,
                    messageInfo.Body,
                    Constants.NULL_STRING,
                    language);

                foreach (string key in messageInfo.Headers.Keys)
                    smtpMessage.AddHeader(key, (string)messageInfo.Headers[key]);

            }
            catch (Exception ex)
            {
                var loggingInfo = string.Format("Exception while creating SMTPMessage object: {0} with stack trace: {1} Impulse query string that generated this message was: {2}",
                    ex.Message, ex.StackTrace, impulse.GetQueryString());
                Log.LogError(loggingInfo, ex, tags);
            }

            if (smtpMail.Send(smtpMessage))
            {
                ExternalMailBL.SuccessfulSend.Increment();

                var loggingInfo = string.Format("Successfully sent impulse email: [{0}] with query string: [{1}] and brandId [{2}]",
                        messageInfo.ToString(), impulse.GetQueryString(), brandID);
                
                Log.LogInfoMessage(loggingInfo, tags);
            }
            else
            {
                ExternalMailBL.UnsuccessfulSend.Increment();

                var loggingInfo = string.Format("Failed to send Impulse email: [{0}] with query string: [{1}] and brandId [{2}]",
                        messageInfo.ToString(), impulse.GetQueryString(), brandID);
                try
                { // attaching the stack
                    throw new Exception(loggingInfo);
                }
                catch (Exception ex)
                {
                    Log.LogError("Error in sendEmail", ex, tags);
                }
                var filename = @"C:\ExternalMailDeadLetter\ExternalMailDeadLetter" + Guid.NewGuid() + ".impulse";

                try
                {
                    //Then intention of this file is unclear, leaving here for now.
                    Stream outStream = File.OpenWrite(filename);
                    var formatter = new BinaryFormatter();
                    formatter.Serialize(outStream, impulse);
                    outStream.Close();

                    var info = string.Format("Unable to send email: [{0}] for impulse with query string: [{1}].  Stored out to file: [{2}]",
                        messageInfo.ToString(), impulse.GetQueryString(), filename);
                    try
                    { // attaching the stack
                        throw new Exception(info);
                    }
                    catch (Exception ex)
                    {
                        Log.LogError("Error in sendEmail", ex, tags);
                    }
                }
                catch (Exception ex)
                {
                    var info = string.Format("Unable to send email: [{0}] for impulse with query string: [{1}].  Also unable to store out to file: {2} because of Exception with message:  {3} and stack trace: {4}", messageInfo.ToString(), impulse.GetQueryString(), filename, ex.Message, ex.StackTrace);
                    Log.LogError(info, ex, tags);
                }
            }
        }
        public static bool SendStormPostEmail(ImpulseBase impulse, SMTPMail smtpMail, AllowedDomains allowedDomains, string callingClassName)
        {
            MessageInfo messageInfo = Composer.Instance.Compose(impulse);
            var tags = new CustomDataProvider(impulse.BrandID, 0, messageInfo.ToAddress).Get();
            if (messageInfo != null)
            {
                if (messageInfo.DoSend)
                {
                    Log.LogInfoMessage("messageInfo.DoSend was set to true", tags);
                    if (allowedDomains.IsAllowedDomain(messageInfo.ToAddress))
                    {
                        Log.LogInfoMessage("Domain was allowed", tags);
                        sendEmail(messageInfo, impulse, smtpMail, callingClassName);
                    }
                    else
                    {
                        Log.LogInfoMessage("Domain was NOT allowed, message was not sent", tags);
                    }
                }
                else
                {
                    ExternalMailBL.ComposeFailed.Increment();

                    var message = string.Format("MessageInfo DoSend flag was set to false.  Reason for cancelling send: {0}", messageInfo.CancelMessage);

                    Log.LogInfoMessage(message, tags);
                }

                return true;
            }
            else
            {
                ExternalMailBL.NullMessageInfo.Increment();

                Log.LogInfoMessage("Got a null messageInfo from Composer.", tags);
                return false;
            }
        }
    }
}
