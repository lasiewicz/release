﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Matchnet.ExternalMail.BusinessLogic.QueueProcessing
{
    public class QueueProcessorRuntimeInfo
    {
        public QueueProcessorRuntimeInfo(QueueProcessorDefinition definition, Thread[] threads)
        {
            Definition = definition;
            Threads = threads;
        }
        public QueueProcessorDefinition Definition { get; private set; }
        public Thread[] Threads { get; private set; }
    }
}
