﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Messaging;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.Content.ServiceAdapters;
using Matchnet.EmailLibrary;
using Matchnet.ExternalMail.BusinessLogic.Monitoring;
using MSA = Matchnet.Member.ServiceAdapters;

using Matchnet.HTTPMessaging;
using Matchnet.HTTPMessaging.RequestTypes;
using Matchnet.HTTPMessaging.ResponseTypes;

using Matchnet.ExternalMail.BusinessLogic.SMSProvider;
using Spark.Logger;
using Spark.Logging;

namespace Matchnet.ExternalMail.BusinessLogic.QueueProcessing
{
    public class QueueProcessor:IQueueProcessor
    {
        #region Public Constants
        public const string DEFAULT_GREY_QUEUE_PATH = @".\private$\ExternalMail";
        public const string DEFAULT_SOAP_QUEUE_PATH = @".\private$\ExternalMail_Soap";
        private const string GREY_THREAD_NAME_PREFIX = "GreyProcessThread";
        private const string SOAP_THREAD_NAME_PREFIX = "SoapProcessThread";
        private const int DEFAULT_EXTMAILSVC_BAD_MESSAGE_EXP_IN_MIN = 6*60;
        #endregion

        #region Private Constants
        private const string CLASS_NAME = "QueueProcessor";
        private const string LOG_NAME = "ExternalMail";
        private const int SLEEP_TIME = 500;
        private const int SLEEP_TIME_ERROR = 15000;
        private const int NO_MESSAGE_DISCONNECT_LIMIT = 6;

        #endregion

        #region Private Variables
        private static string _greyQueuePath = null;
        private static string _soapQueuePath = null;

        private HydraWriter _hydraWriter = null;

        private bool _runnable;


        private QueueProcessorRuntimeInfo _greyProcessorRuntimeInfo;
        private QueueProcessorRuntimeInfo _soapProcessorRuntimeInfo;

        private AllowedDomains _allowedDomains;


        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(QueueProcessor));

        #endregion

        #region Public Properties
        public static string GreyQueuePath
        {
            get
            {
                return getGreyQueuePath();
            }
        }


        public static string SoapQueuePath
        {
            get
            {
                return getSoapQueuePath();
            }
        }

        #endregion

        #region Public Constructors
        public QueueProcessor()
        {
            ExternalMailBL.InitPerfCounters();
            _allowedDomains = new AllowedDomains(ServiceConstants.SERVICE_NAME);
        }
        #endregion

        #region Public Methods
        public void Start()
        {
            _runnable = true;

            _hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[] { "mnFileExchange", "mnXmail" });
            _hydraWriter.Start();

            _greyProcessorRuntimeInfo = startThreads(getGreyQueuePath(), processGreyCycle, GREY_THREAD_NAME_PREFIX);
            _soapProcessorRuntimeInfo = startThreads(getSoapQueuePath(), processSoapCycle, SOAP_THREAD_NAME_PREFIX);
            EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Started processing GreyQueuePath:" + getGreyQueuePath() + ", SoapQueuePath:" + getSoapQueuePath() , EventLogEntryType.Information);
        }


        public void Stop()
        {
            _runnable = false;

            _hydraWriter.Stop();

            stopThreads(_greyProcessorRuntimeInfo.Threads, GREY_THREAD_NAME_PREFIX);
            stopThreads(_soapProcessorRuntimeInfo.Threads, SOAP_THREAD_NAME_PREFIX);
        }

        #endregion

        #region Private Methods

        private QueueProcessorRuntimeInfo startThreads(string queuePath, Action<QueueProcessorDefinition> process, string prefix)
        {
            if (!MessageQueue.Exists(queuePath))
            {
                MessageQueue.Create(queuePath, true);
            }
            if (!MessageQueue.Exists(queuePath + "_noretry"))
            {
                MessageQueue.Create(queuePath + "_noretry", true);
            }
            if (!MessageQueue.Exists(queuePath + "_noretry_bad_emails"))
            {
                MessageQueue.Create(queuePath + "_noretry_bad_emails", true);
            }
            var mainQueue = new MessageQueue(queuePath);
            mainQueue.Formatter = new BinaryMessageFormatter();

            var noretryQueue = new MessageQueue(queuePath + "_noretry");
            noretryQueue.Formatter = new BinaryMessageFormatter();

            var noretryBadEmailsQueue = new MessageQueue(queuePath + "_noretry_bad_emails");
            noretryBadEmailsQueue.Formatter = new BinaryMessageFormatter();

            Int16 threadCount = Convert.ToInt16(RuntimeSettings.GetSetting("EXTMAILSVC_THREAD_COUNT"));
            var threads = new Thread[threadCount];

            var definition = new QueueProcessorDefinition(queuePath, mainQueue, noretryQueue, noretryBadEmailsQueue);

            for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
            {
                Thread t = new Thread(new ThreadStart(()=>process(definition)));
                t.Name = prefix + threadNum.ToString();
                t.Start();
                threads[threadNum] = t;
            }

#if DEBUG
            System.Diagnostics.Trace.WriteLine("Stopped all soap threads");
#endif
            return new QueueProcessorRuntimeInfo(definition, threads);
        }


        private void stopThreads(Thread[] threads, string prefix)
        {
            Int16 threadCount = (Int16)threads.Length;

            for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
            {
                threads[threadNum].Join(10000);
            }

#if DEBUG
            System.Diagnostics.Trace.WriteLine("Stopped all " + prefix + " threads");
#endif
        }


        private static string getSoapQueuePath()
        {
            if (_soapQueuePath == null || string.Empty.Equals(_soapQueuePath))
            {
                _soapQueuePath = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_QUEUE_PATH") + "_Soap";

                if (_soapQueuePath == null || string.Empty.Equals(_soapQueuePath))
                {
                    _soapQueuePath = QueueProcessor.DEFAULT_SOAP_QUEUE_PATH;
                }
            }

            return _soapQueuePath;
        }


        private static string getGreyQueuePath()
        {
            if (_greyQueuePath == null || string.Empty.Equals(_greyQueuePath))
            {
                _greyQueuePath = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_QUEUE_PATH");

                if (_greyQueuePath == null || string.Empty.Equals(_greyQueuePath))
                {
                    _greyQueuePath = QueueProcessor.DEFAULT_SOAP_QUEUE_PATH;
                }
            }

            return _greyQueuePath;
        }

        //TODO: Refactor to a smaller method
        private void processGreyCycle(QueueProcessorDefinition definition)
        {
            try
            {
                var tags = new CustomDataProvider().Get();
                TimeSpan ts = new TimeSpan(0, 0, 10);

                int noMessageCount = 0;
                bool successful = false;

                SMTPMail smtpMail = new SMTPMail(100);
                smtpMail.SmtpServer = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_RELAY_HOST");

                while (_runnable)
                {
                    MessageQueueTransaction tran = new MessageQueueTransaction();

                    try
                    {
                        ImpulseBase impulse = null;
                        tran.Begin();

                        try
                        {
                            impulse = definition.MainQueue.Receive(ts, tran).Body as ImpulseBase;
                            //	INSTRUMENTATION - Impulse received from queue (IN)
                            ExternalMailBL.ReceivedFromQueue.Increment();
                        }
                        catch (MessageQueueException mEx)
                        {
                            if (mEx.Message != "Timeout for the requested operation has expired.")
                            {
                                noMessageCount++;
                                if (noMessageCount >= NO_MESSAGE_DISCONNECT_LIMIT)
                                {
                                    smtpMail.Disconnect();
                                }
                                throw new Exception("Message queue error [queuePath: " + _greyQueuePath + "].", mEx);
                            }
                        }

                        if (impulse != null)
                        {
                            Brand brand = BrandConfigSA.Instance.GetBrandByID(impulse.BrandID);

                            if ((impulse is ViralImpulse || impulse is MatchMailImpulse || impulse is NewMemberMailImpulse))
                            {
                                //								YesMailHandler.Instance.SendEmail(impulse);
                                MailRouter.Instance.SendEmail(impulse);

                                successful = true;
                            }
                            else if (impulse is ContactUsImpulse || impulse is ReportAbuseImpulse || impulse is InternalCorpMail || impulse is MemberQuestionImpulse
                                || impulse is BetaFeedbackImpulse)
                            {
                                // These are the mail types that still go through our SMTP.	

                                Log.LogInfoMessage(string.Format("About to send a StormPostEmail with type:{0} brandid: {1} queryString: {2}", impulse.GetType(), impulse.BrandID, impulse.GetQueryString()), tags);

                                successful = EmailHelper.SendStormPostEmail(impulse, smtpMail,_allowedDomains, CLASS_NAME);
                            }
                            else
                            {
                                var errorMessage = string.Format("Unknown impulse type passed into externalmail(greymail) queue. Only Match,Viral,ContactUs, ReportAbuse, and InternalCorpEmail are allowed. [ImpulseType: {0}, BrandID: {1}]",
                                    impulse.GetType().ToString(), impulse.BrandID);

                                throw new BLException(errorMessage);
                            }

                            if (successful)
                            {
                                tran.Commit();

                                ExternalMailBL.SuccessfulSend.Increment();
                            }
                        }
                        else
                        {
                            Thread.Sleep(SLEEP_TIME);
                        }

                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.IndexOf("does not contain a valid BinaryHeader") > -1)
                        {
                            tran.Commit();
                        }
                        else if (ex.Message.IndexOf("Can not find Yes mail mapping") > -1)
                        {
                            tran.Commit();
                        }
                        else
                        {
                            attemptRollback(tran);
                        }

                        ExternalMailBL.ErrorProcessingQueue.Increment();

                        Log.LogError("Error processing grey transaction. [QueuePath:" + _greyQueuePath + "]" + ex, ex, tags);

                        Thread.Sleep(SLEEP_TIME_ERROR);
                    }
                }

                smtpMail.Dispose();
            }
            catch (Exception ex)
            {
                ExternalMailBL.UncaughtException.Increment();

                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Uncaught Exception in processGreyCycle(): " + ex.ToString(), EventLogEntryType.Error);

                Thread.Sleep(SLEEP_TIME_ERROR);
            }
        }

     

        private void processSoapCycle(QueueProcessorDefinition definition)
        {
            try
            {
                while (_runnable)
                {
                    MessageQueueTransaction tran = new MessageQueueTransaction();

                    try
                    {
                        tran.Begin();

                        TimeSpan timeout = new TimeSpan(0, 0, 10);
                        ImpulseBase impulse = ReceiveImpulse(definition.MainQueue, timeout, tran);
                        ProcessImpulseAndCommit(definition,tran,impulse);
                    }
                    catch (Exception ex)
                    {
                        ExternalMailBL.ErrorProcessingQueue.Increment();
                        EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Error processing soap transaction. [QueuePath: " + definition.QueuePath + "]" + ex.ToString(), EventLogEntryType.Error);
                        Thread.Sleep(SLEEP_TIME_ERROR);
                    }
                    finally
                    {
                        if (tran != null)
                        {
                            tran.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalMailBL.UncaughtException.Increment();
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Uncaught Exception in processSoapCycle(): " + ex.ToString(), EventLogEntryType.Error);
                Thread.Sleep(SLEEP_TIME_ERROR);
            }
        }

        ImpulseBase ReceiveImpulse(MessageQueue queue, TimeSpan timeout, MessageQueueTransaction tran)
        {
            try
            {
                var ret = queue.Receive(timeout, tran).Body as ImpulseBase;
                ExternalMailBL.ReceivedFromQueue.Increment();
                return ret;
            }
            catch (MessageQueueException mex)
            {
                if (mex.Message != "Timeout for the requested operation has expired.")
                {
                    attemptRollback(tran);
                    throw new Exception("Message queue error. [Error:" + mex.Message + "]", mex);
                }
            }

            return null;
        }


        void ProcessImpulseAndCommit(QueueProcessorDefinition definition, MessageQueueTransaction tran, ImpulseBase impulse)
        {

            #region Process impulse
            if (impulse != null)
            {
                var tags = new CustomDataProvider(impulse.BrandID).Get();
                try
                {
                    Site site = CalculateSiteForSoapMessage(impulse);

                    if (site == null)
                    {
                        throw new BLException("Site is not found. [BrandID:" + impulse.BrandID.ToString() + ", ImpulseType:" +
                            impulse.ToString());
                    }

                    MailRouter.Instance.SendEmail(impulse);

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    handleInTransactionException(definition.MainQueue, definition.NoretryQueue, definition.NoretryBadEmailsQueue, tran, impulse, ex);
                }
            }
            else
            {
                tran.Commit();

                ExternalMailBL.SuccessfulSend.Increment();

                Thread.Sleep(SLEEP_TIME);
            }
            #endregion
        }

        Site CalculateSiteForSoapMessage(ImpulseBase impulse)
        {
            /// HACK: In the cases where an unmatching brandID/siteID combination is passed in. 
            if (impulse is RenewalFailureNotificationImpulse || impulse is EcardToFriendImpulse || impulse is EcardToMemberImpulse
                || impulse is MessageBoardDailyUpdatesImpulse || impulse is MessageBoardInstantNotificationImpulse
                || impulse is PmtProfileConfirmationImpulse)
            {
                int siteId = Constants.NULL_INT;

                if (impulse is RenewalFailureNotificationImpulse)
                    siteId = ((RenewalFailureNotificationImpulse)impulse).SiteID;
                else if (impulse is EcardToFriendImpulse)
                    siteId = ((EcardToFriendImpulse)impulse).SiteID;
                else if (impulse is EcardToMemberImpulse)
                    siteId = ((EcardToMemberImpulse)impulse).SiteID;
                else if (impulse is MessageBoardDailyUpdatesImpulse)
                    siteId = ((MessageBoardDailyUpdatesImpulse)impulse).SiteID;
                else if (impulse is PmtProfileConfirmationImpulse)
                    siteId = ((PmtProfileConfirmationImpulse)impulse).SiteID;
                else
                    siteId = ((MessageBoardInstantNotificationImpulse)impulse).SiteID;

                Sites coll = BrandConfigSA.Instance.GetSites();

                foreach (Site s in coll)
                {
                    if (s.SiteID == siteId)
                    {
                        return s;
                    }
                }
            }
            else
            {
                Brand brand = BrandConfigSA.Instance.GetBrandByID(impulse.BrandID);
                return brand.Site;
            }

            return null;
        }
        private void attemptRollback(MessageQueueTransaction tran)
        {
            try
            {
                if (tran.Status == MessageQueueTransactionStatus.Pending)
                {
                    tran.Abort();
                }
            }
            catch (Exception ex)
            {
                string message = "Exception while trying to abort transaction: " + ex.Message + ", StackTrace: " + ex.StackTrace;
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, message, EventLogEntryType.Warning);
            }
        }



        private void handleInTransactionException(MessageQueue queue, MessageQueue queueNoretry, MessageQueue queueNoretryBadEmails,MessageQueueTransaction tran, ImpulseBase imp, Exception e)
        {
            try
            {
                var tags = new CustomDataProvider(imp.BrandID).Get();
                tags["Final"] = "final";
                if (tran.Status == MessageQueueTransactionStatus.Pending)
                {
                    var isPermanent = IsPermanent(e);
                    var expInMins = 0;

                    var expInMinsStr = RuntimeSettings.GetSetting("EXTMAILSVC_BAD_MESSAGE_EXP_IN_MIN");
                    if (expInMinsStr == null || !int.TryParse(expInMinsStr, out expInMins))
                    {
                        expInMins = DEFAULT_EXTMAILSVC_BAD_MESSAGE_EXP_IN_MIN;
                    }

                    var messageAgeInMinutes = (DateTime.UtcNow - imp.FirstAttemptedUtc).TotalMinutes;
                    var isAttemptedForLongTime = imp.AttemptsCount > 10 &&  messageAgeInMinutes > expInMins;
                    if (IsPermanent(e) || isAttemptedForLongTime)
                    {
                        if (IsBadEmail(e))
                        {
                            queueNoretryBadEmails.Send(imp, tran);
                            tags["BadEmail"] = "BadEmail";
                        }
                        else
                        {
                            queueNoretry.Send(imp, tran);
                        }
                        
                        if (isAttemptedForLongTime)
                        {
                            var message = "Giving up, Timeout, attempted for " + messageAgeInMinutes +
                                          " minutes and " +
                                          (imp.AttemptsCount + 1) + " times.";

                            Log.LogError(message, e, tags);
                        }
                        else
                        {
                            var message = "Giving up. Permantent Error.";
                            Log.LogError(message, e, tags);
                        }
                    }
                    else
                    {
                        queue.Send(imp.CopyWithAttemptIncrement(), tran);
                        var message = "Retrying in future. Attempted for " + messageAgeInMinutes + " minutes and " + (imp.AttemptsCount + 1) +
                                      " times. Continuing attempts.";
                        Log.LogError(message, e, tags);
                    }
                    

                    tran.Commit();
                }
            }
            catch (Exception ex)
            {
                Log.LogError("Exception while trying to abort transaction",ex,new CustomDataProvider(imp.BrandID).Get());
            }
        }

        public bool IsPermanent(Exception e)
        {
            if (e is ServiceTransportException)
            {
                var se = e as ServiceTransportException;
                int[] permanentReturnCodes = new[] {400,401,402,403,404,405,406,413,414,415,498,499,501};
                return permanentReturnCodes.Any(x => se.StatusCode == x);
            }
            return true;
        }

        public bool IsBadEmail(Exception e)
        {
            return e.Message.Contains("Contact is invalid") ||
                   e.Message.Contains("This email is present in your global unsubcribes list");
        }

    #endregion
    }
}
