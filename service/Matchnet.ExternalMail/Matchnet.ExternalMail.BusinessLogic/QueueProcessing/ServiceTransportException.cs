﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.ExternalMail.BusinessLogic.QueueProcessing
{
    class ServiceTransportException: Exception
    {
        public int StatusCode { get; private set; }
        public ServiceTransportException(int statusCode, string message, Exception innerException):base(message,innerException)
        {
            StatusCode = statusCode;
        }
    }
}
