using System;
using System.IO;
using System.Xml.XPath;
using System.Reflection;
using System.Resources;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ValueObjects.Impulse;

namespace Matchnet.ExternalMail.BusinessLogic
{	
	public class SiteMapper
	{
		private XPathDocument _doc;
		private Object _syncBlock = new object();

        public SiteMapper(string configFile)
		{
		    _configFile = configFile;
			this.LoadDocument(configFile);
		}

		#region ITemplateMapper Members

	    private string _configFile;
		public string GetAccessToken(int siteId)
		{	
			XPathNavigator nav = _doc.CreateNavigator();
			XPathNodeIterator iterator = nav.Select(nav.Compile(string.Format("mapping/item[siteID='{0}']/apiToken", siteId)));

			if(iterator.MoveNext())
				return iterator.Current.Value;
			else
				throw new Exception("Can not find site mapping. Site id: " + siteId);
		}

        #endregion

		private void LoadDocument(string configFile)
		{
			if(_doc == null)
			{
				lock(_syncBlock)
				{
					if(_doc == null)
					{
						Assembly asm = Assembly.GetExecutingAssembly();
						string[] resNames = Assembly.GetExecutingAssembly().GetManifestResourceNames();
						for(int i = 0; i < resNames.Length; i++)
						{
							if(resNames[i].IndexOf(configFile) > -1)
							{
								Stream stream = asm.GetManifestResourceStream(resNames[i]);
								_doc = new XPathDocument(stream);
								stream.Close();
							}
						}
					}
				}
			}
		}
	}
}
