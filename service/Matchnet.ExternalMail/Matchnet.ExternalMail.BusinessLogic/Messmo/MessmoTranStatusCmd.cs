using System;
using System.Net;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.HTTPMessaging;
using Matchnet.HTTPMessaging.RequestTypes;

namespace Matchnet.ExternalMail.BusinessLogic.Messmo
{
	/// <summary>
	/// Messmo API 2.21 A Channel Sets a User's Transaction Status
	/// </summary>
	public class MessmoTranStatusCmd : MessmoCommandBase
	{
		private MessmoTranStatusImpulse _impulse;

		public MessmoTranStatusCmd(MessmoTranStatusImpulse impulse)
		{
			this._impulse = impulse;
		}

		public override string DoCommand()
		{
			MessmoTranStatusRequest req =  new MessmoTranStatusRequest("setTrans", MessmoKey, MessmoChannelNumber,
				Matchnet.HTTPMessaging.Utilities.GetMessmoFormatTimestamp(_impulse.TimeStamp), _impulse.MessmoSubscriberID, _impulse.Status,
				Matchnet.HTTPMessaging.Utilities.GetMessmoFormatTimestamp(_impulse.SetDate), MessmoAPIVersion);

			return MessmoHelper.PostMessmoRequest(req);
		}

	}
}
