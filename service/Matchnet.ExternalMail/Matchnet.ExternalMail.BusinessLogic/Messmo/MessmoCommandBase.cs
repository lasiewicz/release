using System;

namespace Matchnet.ExternalMail.BusinessLogic.Messmo
{
	/// <summary>
	/// Summary description for MessmoCommandBase.
	/// </summary>
	public abstract class MessmoCommandBase
	{
		#region Properties
		public string MessmoPostUrl
		{
			get
			{
				return Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MESSMO_POST_URL");
			}
		}

		public string MessmoKey
		{
			get
			{
				return Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MESSMO_KEY");			
			}
		}

		public string MessmoChannelNumber
		{
			get
			{
				return Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MESSMO_CHANNEL_NUMBER");
			}
		}

		public string MessmoAPIVersion
		{
			get
			{
				return "2.0";
			}
		}
		#endregion

		public abstract string DoCommand();		
	}
}
