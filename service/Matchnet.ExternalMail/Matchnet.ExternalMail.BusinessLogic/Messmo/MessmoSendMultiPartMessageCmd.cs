using System;
using System.Diagnostics;
using System.Net;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.HTTPMessaging;
using Matchnet.HTTPMessaging.RequestTypes;
using Matchnet.HTTPMessaging.ResponseTypes;

namespace Matchnet.ExternalMail.BusinessLogic.Messmo
{
	/// <summary>
	/// Messmo API 2.20 - A Channel Sends a Multi-Part Message to a User
	/// </summary>
	public class MessmoSendMultiPartMessageCmd : MessmoCommandBase	
	{
		private MessmoSendMultiPartMessageImpulse _impulse;

		public MessmoSendMultiPartMessageCmd(MessmoSendMultiPartMessageImpulse impulse)
		{
			_impulse = impulse;
		}

		public override string DoCommand()
		{
			// Make sure the sender's full profile was sent at least once
			MessmoHelper.SendFullProfileCheck(_impulse.SenderBrandID, _impulse.SendingMemberID);

			string attachmentUrl, attachmentType, supplementary, ret;

			attachmentUrl = MessmoHelper.GetProfilePhotoUrl(_impulse.SendingMemberID, _impulse.SenderBrandID, out attachmentType);
			supplementary = MessmoHelper.GetMemberMiniProfie(_impulse.SendingMemberID, _impulse.SenderBrandID, true);

			MessmoSendMultiPartMessageRequest req = new MessmoSendMultiPartMessageRequest("labelMsg", MessmoKey, MessmoChannelNumber,
				Matchnet.HTTPMessaging.Utilities.GetMessmoFormatTimestamp(_impulse.MessageDate), _impulse.ReceivingMessmoSubID, _impulse.MessageBody,
				attachmentUrl, attachmentType, _impulse.SmartMessageTag, _impulse.Taggle, supplementary, _impulse.Replace, _impulse.Notify,
				_impulse.MessmoButtonType, MessmoAPIVersion);

			string postData = req.GetPostData(true);

			HttpStatusCode statusCode;
			string respResult = SMSProxy.SendPost(postData, this.MessmoPostUrl, 60000, "application/x-www-form-urlencoded", out statusCode);
			
			if(statusCode == HttpStatusCode.OK)
			{
				ret = statusCode.ToString();
			}
			else
			{
				ret = statusCode.ToString() + ": " + respResult;
#if DEBUG
				Trace.WriteLine("SendMultiPartMessageCmd first attempt failure: " + ret);
#endif

				// Check to see if this is invalid photo URL, if so let's send the no photo gif and try again
				if(respResult.ToLower().IndexOf("invalid attachment url", 0) > -1)
				{
					req.AttachmentURL = MessmoHelper.GetSiteURL(_impulse.SenderBrandID) + MessmoHelper.NoPhotoImage;
					if(req.AttachmentURL.Length > 3)
					{
						req.AttachmentType = req.AttachmentURL.Substring(req.AttachmentURL.Length - 3, 3).ToLower();
						ret = MessmoHelper.PostMessmoRequest(req);
					}
				}
			}

#if DEBUG
					Trace.WriteLine("SendMultiPartMessageCmd final result: " + ret);
#endif

			return ret;
		}
	}
}
