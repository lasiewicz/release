using System;
using System.Collections;
using System.Diagnostics;
using System.Net;
using System.Threading;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.HTTPMessaging;
using Matchnet.HTTPMessaging.RequestTypes;
using Matchnet.HTTPMessaging.ResponseTypes;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.List.ValueObjects;

namespace Matchnet.ExternalMail.BusinessLogic.Messmo
{
	/// <summary>
	/// Summary description for MessmoSendInitialBatchCmd.
	/// </summary>
	public class MessmoSendInitialBatchCmd : MessmoCommandBase
	{
		private MessmoSendInitialBatchImpulse _impulse;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="impulse"></param>
		public MessmoSendInitialBatchCmd(MessmoSendInitialBatchImpulse impulse)
		{
			_impulse = impulse;
		}

		public override string DoCommand()
		{
			string ret = "ok";

			// This item should come off the delayed Messmo queue.  We need to put in some delay before we make any attempts because
			// it takes some time for Messmo to create an account.  During our initial sign-up, sign-up call and subsequent calls are made
			// right after another.  This is the subsequent call, thus put in some delay and allow for Messmo to create the account.
			#region Delay portion + sending of Member Tran Status (sub expiration date)
			int sleepTime = int.Parse(RuntimeSettings.GetSetting("MESSMO_INITIAL_BATCH_SLEEPTIME")) * 1000;
			int retryCount = int.Parse(RuntimeSettings.GetSetting("MESSMO_INITIAL_BATCH_RETRIES"));
			int actualCount = 0;
			bool sendTranStatusSuccess = false;
			
			MessmoTranStatusRequest req = null;
			DateTime startTime = DateTime.Now;
			for(int i=0; i < retryCount; i++)
			{
				actualCount = i;
				Thread.Sleep(sleepTime);

				if(i == 0)
				{
					req = new MessmoTranStatusRequest("setTrans", MessmoKey, MessmoChannelNumber, 
						Matchnet.HTTPMessaging.Utilities.GetMessmoFormatTimestamp(_impulse.TimeStamp), _impulse.MessmoSubscriberID, _impulse.Status,
						Matchnet.HTTPMessaging.Utilities.GetMessmoFormatTimestamp(_impulse.SetDate), MessmoAPIVersion);
				}

                string tranStatusResult = MessmoHelper.PostMessmoRequest(req);
				ret = tranStatusResult;

				if(tranStatusResult.ToLower() == "ok")
				{
					// we are done here break out of this
					sendTranStatusSuccess = true;
					break;
				}
				else
				{
					// we only want to retry if this was due to our timing issue. Something like:
					// InternalServerError: Active SubscriberId does not exist
					if(tranStatusResult.ToLower().IndexOf("active subscriberid does not exist") < 0)
					{
						// this is some other type of error, don't bother retrying
						break;
					}
				}
			}
			DateTime endTime = DateTime.Now;
			#endregion

			// return if we weren't able to send the TranStatus
			if(!sendTranStatusSuccess)
			{
				TimeSpan duration = endTime - startTime;
				int durationSecs = duration.Minutes * 60 + duration.Seconds;
				return ret + " Retries attempted: " + (actualCount + 1).ToString() + " Duration in seconds: " + durationSecs.ToString();
			}

			// we were able to get through the Tran Status Send phase, let's rest some time here because if we don't
			// Messmo has trouble keeping up
			int initialPauseTime = int.Parse(RuntimeSettings.GetSetting("MESSMO_INITIAL_BATCH_INTIAL_PAUSE")) * 1000;
			Thread.Sleep(initialPauseTime);

			// get member and brand object first
			IMemberDTO fromMember = null;
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(_impulse.MemberID, MemberLoadFlags.None);

			if(member == null)
			{
				ret = string.Format("Cannot find the member: {0}", _impulse.MemberID);
			}
			else
			{
				Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(_impulse.BrandID);

				// parameter we need to send a message to Messmo
				string receivingMessmoSubID = _impulse.MessmoSubscriberID;
				int senderBrandID, sendingMemberID;
				DateTime messageDate;
				string messageBody, smartMessageTag, taggle;
        
				#region Send messages from the Inbox first
				
				string messageCount = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MESSMO_INITIAL_MESSAGES_COUNT", brand.Site.Community.CommunityID,
					brand.Site.SiteID, brand.BrandID);

				int msgCountConfiguration = 0;

				if (messageCount != null)
					msgCountConfiguration = int.Parse(messageCount);

				// go get the messages from the member's inbox
				ArrayList messages = (ArrayList)Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance.RetrieveMessages(member.MemberID, brand.Site.Community.CommunityID,
					(int)SystemFolders.Inbox, 1, msgCountConfiguration, "InsertDate desc", brand);

				int i = 0;

				foreach (EmailMessage msg in messages)
				{
					if (i >= msgCountConfiguration)
						break;

					sendingMemberID = msg.FromMemberID;
					messageDate = msg.InsertDate;

					// retrieve the actual message; EmailMessage object from the collection object does not contain the actual message
					EmailMessage singleMsg = EmailMessageSA.Instance.RetrieveMessage(member.MemberID,
						msg.GroupID, msg.MemberMailID);

					if (singleMsg != null)
					{
						// retrieve the member object for From member since EmailMessage.FromUserName is no good
                        fromMember = MailHandlerUtils.GetIMemberDTO(msg.FromMemberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
						if (fromMember != null)
						{
							smartMessageTag = Matchnet.HTTPMessaging.Utilities.GetMessmoSmartMessageTag(Matchnet.HTTPMessaging.Constants.MessmoMessageType.Email, fromMember.GetUserName(brand));

							// We have to distinguish the message type here
							Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMsgType = Matchnet.ExternalMail.ValueObjects.MessmoUtilities.MapMailTypeToMessmoMessageType(msg.MailTypeID);
						
							taggle = Matchnet.HTTPMessaging.Utilities.GetMessmoTaggleValue(msg.FromMemberID, member.MemberID, messmoMsgType,
								brand.Site.Community.CommunityID, msg.MemberMailID);

							// Prepare the message body.  Since Messmo does not have a separate subject field, we are including the subject in the body
							messageBody =Matchnet.HTTPMessaging.Utilities.GetMessmoMessageBody(messmoMsgType, singleMsg.Content, singleMsg.Subject);
							
							// senderBrandID is used to retrieive the sender's profile information and photo. get it by grabbing the last logon
							// brandID.
							fromMember.GetLastLogonDate(brand.Site.Community.CommunityID, out senderBrandID);

							// couldn't find the member for some reason
							if(senderBrandID != Constants.NULL_INT)
							{								
								MessmoHelper.SendMessmoMultiPartMessage(brand.BrandID, senderBrandID, sendingMemberID, messageDate, receivingMessmoSubID, messageBody,
									smartMessageTag, taggle, false,
									MessmoHelper.NotifyOfMessmoMessageArrival(member, brand, messmoMsgType),
									messmoMsgType);
							}
						}
					}

					// get to the next message
					i++;
				}				
				#endregion

				#region Hot Lists
				// Hot Lists            
				List.ServiceAdapters.List list = List.ServiceAdapters.ListSA.Instance.GetList(member.MemberID, Matchnet.List.ServiceAdapters.ListLoadFlags.IsSelf);
				// most recent 10 profiles of who viewed the member				
				MessmoHelper.SendProfilesFromList(brand, member, list, HotListCategory.WhoViewedYourProfile, 10, receivingMessmoSubID);

				// most recent 10 profiles of who hot listed the member 
				MessmoHelper.SendProfilesFromList(brand, member, list, HotListCategory.WhoAddedYouToTheirFavorites, 10, receivingMessmoSubID);

				// most recent 5 your matches
				MessmoHelper.SendProfilesFromList(brand, member, list, HotListCategory.YourMatches, 5, receivingMessmoSubID);

				// most recent 20 your favorites
				MessmoHelper.SendProfilesFromList(brand, member, list, HotListCategory.Default, 20, receivingMessmoSubID);

				// Members you viewed (20 of them)
				MessmoHelper.SendProfilesFromList(brand, member, list, HotListCategory.MembersYouViewed, 20, receivingMessmoSubID);
				#endregion

				#region Members in your area in Try Catch block
				// A lot of problems occur with Search, but we don't want this queue item to stall the queue so put try catch around this area
				try
				{
					// 5 most recent members in your area            
					SearchPreferenceCollection searchPrefs = MessmoHelper.GetNewMemberInYourAreaSearchPrefs(brand, member);

					if (searchPrefs != null)
					{
						MatchnetQueryResults searchResults = null;
						ValidationResult validationResult = searchPrefs.Validate();

						if (validationResult.Status == PreferencesValidationStatus.Success)
						{
							searchResults = MemberSearchSA.Instance.Search(searchPrefs,
								brand.Site.Community.CommunityID,
								brand.Site.SiteID,
								0,
								5); // we only want 5 most recent

							ArrayList newMemberIDs = null;

							if (searchResults != null)
							{
								newMemberIDs = searchResults.ToArrayList();                        
							}

							if (newMemberIDs != null)
							{
#if DEBUG
								Trace.WriteLine("New members in your area count: " + newMemberIDs.Count.ToString());
#endif
								foreach (int newMemberID in newMemberIDs)
								{
                                    fromMember = MailHandlerUtils.GetIMemberDTO(newMemberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
									if (fromMember != null)
									{
										sendingMemberID = fromMember.MemberID;
										// since we are sending search results which do not exist as messages or list items, just set the messageDate as now
										messageDate = DateTime.Now;
										messageBody = Matchnet.HTTPMessaging.Utilities.GetMessmoMessageBody(Matchnet.HTTPMessaging.Constants.MessmoMessageType.NewMembersInArea, string.Empty, string.Empty);

										smartMessageTag = Matchnet.HTTPMessaging.Utilities.GetMessmoSmartMessageTag(Matchnet.HTTPMessaging.Constants.MessmoMessageType.NewMembersInArea,
											fromMember.GetUserName(brand));
										taggle = Matchnet.HTTPMessaging.Utilities.GetMessmoTaggleValue(fromMember.MemberID, member.MemberID,
											Matchnet.HTTPMessaging.Constants.MessmoMessageType.NewMembersInArea,
											brand.Site.Community.CommunityID,
											Constants.NULL_INT);

										// senderBrandID is used to retrieive the sender's profile information and photo. get it by grabbing the last logon
										// brandID.
										fromMember.GetLastLogonDate(brand.Site.Community.CommunityID, out senderBrandID);

										// couldn't find the member for some reason
										if(senderBrandID != Constants.NULL_INT)
										{										
											MessmoHelper.SendMessmoMultiPartMessage(brand.BrandID, senderBrandID, sendingMemberID, messageDate, receivingMessmoSubID,
												messageBody, smartMessageTag, taggle, true,
												MessmoHelper.NotifyOfMessmoMessageArrival(member, brand, Matchnet.HTTPMessaging.Constants.MessmoMessageType.NewMembersInArea),
												Matchnet.HTTPMessaging.Constants.MessmoMessageType.NewMembersInArea);
										}
									}
								}
							}
						}
					}
				}
				catch(Exception ex)
				{
					ret = "Search error occurred while trying to send New Members in Your Area: " + ex.Message;
				}
				#endregion

				string smsMessage = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MESSMO_SMS_MESSAGE");
				MessmoHelper.SendSMSToUser(_impulse.BrandID, DateTime.Now, _impulse.MobileNumber, smsMessage);
			}
			

			return ret;
		}

	}
}
