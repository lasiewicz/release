using System;
using System.Collections;
using System.Diagnostics;
using System.Net;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.HTTPMessaging;
using Matchnet.HTTPMessaging.RequestTypes;
using Matchnet.HTTPMessaging.ResponseTypes;
using Matchnet.Content.ValueObjects;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.List.ValueObjects;

namespace Matchnet.ExternalMail.BusinessLogic.Messmo
{
	/// <summary>
	/// Messmo's API 2.2 call. Although the name suggests Subscription, this is really used to reset the password with Messmo.
	/// When our member changes his password, we have to let Messmo know, and their Subscribe API call is the only way to do so.
	/// 
	/// *The actual subscription process (from the web) calls the same Messmo API, but synchronously so the call wouldn't call
	///  through this method.
	/// </summary>
	public class MessmoSubscribeCmd : MessmoCommandBase
	{
		MessmoSubscribeImpulse _impulse;

		/// <summary>
		/// Messmo's API 2.2 call. Although the name suggests Subscription, this is really used to reset the password with Messmo.
		/// When our member changes his password, we have to let Messmo know, and their Subscribe API call is the only way to do so.
		/// 
		/// *The actual subscription process (from the web) calls the same Messmo API, but synchronously so the call wouldn't call
		///  through this method.
		/// </summary>
		/// <param name="impulse"></param>
		public MessmoSubscribeCmd(MessmoSubscribeImpulse impulse)
		{
			_impulse = impulse;
		}

		/// <summary>
		/// Messmo's API 2.2 call. Although the name suggests Subscription, this is really used to reset the password with Messmo.
		/// When our member changes his password, we have to let Messmo know, and their Subscribe API call is the only way to do so.
		/// 
		/// *The actual subscription process (from the web) calls the same Messmo API, but synchronously so the call wouldn't call
		///  through this method.
		/// </summary>
		/// <returns></returns>
		public override string DoCommand()
		{
			string ret = string.Empty;

			MessmoSubscribeRequest req = new MessmoSubscribeRequest("on", MessmoKey, MessmoChannelNumber, Matchnet.HTTPMessaging.Utilities.GetMessmoFormatTimestamp(_impulse.TimeStamp),
				_impulse.MobileNumber, _impulse.EncryptedPassword, MessmoAPIVersion);

			string retFromMessmo = MessmoHelper.PostMessmoRequest(req);

			if(ret.ToLower() != "ok")
			{
				ret = string.Format("Password reset for member (MemberID: {0}) failed. Error code from the Messmo Server: [{1}]", _impulse.MemberID, retFromMessmo);
			}
			else
			{
				ret = "ok";
			}

			return ret;
		}
	
	}
}
