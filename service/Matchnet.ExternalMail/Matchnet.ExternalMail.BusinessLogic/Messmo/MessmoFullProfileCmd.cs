using System;
using System.Net;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.HTTPMessaging;
using Matchnet.HTTPMessaging.RequestTypes;
namespace Matchnet.ExternalMail.BusinessLogic.Messmo
{
	/// <summary>
	/// Messmo API 2.23 a Channel Provides Full Profile Details to Messmo
	/// </summary>
	public class MessmoFullProfileCmd : MessmoCommandBase
	{
		private MessmoFullProfileImpulse _impulse;

		public MessmoFullProfileCmd(MessmoFullProfileImpulse impulse)
		{
			this._impulse = impulse;
		}

		public override string DoCommand()
		{
			string attachmentUrl, attachmentType, supplementary, ret;
			
            attachmentUrl = MessmoHelper.GetProfilePhotoUrl(_impulse.MemberID, _impulse.BrandID, out attachmentType);
			supplementary = MessmoHelper.GetMemberFullProfile(_impulse.MemberID, _impulse.BrandID);
			
			MessmoFullProfileRequest req =  new MessmoFullProfileRequest("saveProfile", MessmoKey, MessmoChannelNumber,
				Matchnet.HTTPMessaging.Utilities.GetMessmoFormatTimestamp(_impulse.TimeStamp), attachmentUrl, attachmentType,
				_impulse.TaggleToken1, supplementary, MessmoAPIVersion);

			// Have all the data ready, send the request now
			string postData = req.GetPostData(true);

			HttpStatusCode statusCode;
			string respResult = SMSProxy.SendPost(postData, this.MessmoPostUrl, 60000, "application/x-www-form-urlencoded", out statusCode);

			if(statusCode == HttpStatusCode.OK)
			{
				ret = statusCode.ToString();
			}
			else
			{
				ret = statusCode.ToString() + ": " + respResult;	

				// Check to see if this is invalid photo URL, if so let's send the no photo gif and try again
				if(respResult.ToLower().IndexOf("invalid attachment url", 0) > -1)
				{
					req.AttachmentURL = MessmoHelper.GetSiteURL(_impulse.SenderBrandID) + MessmoHelper.NoPhotoImage;
					if(req.AttachmentURL.Length > 3)
					{
						req.AttachmentType = req.AttachmentURL.Substring(req.AttachmentURL.Length - 3, 3).ToLower();
						ret = MessmoHelper.PostMessmoRequest(req);
					}
				}
			}

			return ret;
		}

	}
}
