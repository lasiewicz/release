using System;
using System.Net;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.HTTPMessaging;
using Matchnet.HTTPMessaging.RequestTypes;

namespace Matchnet.ExternalMail.BusinessLogic.Messmo
{
	/// <summary>
	/// Messmo API 2.18 A Channel Sends a SMS to a User
	/// </summary>
	public class MessmoSendSMSCmd : MessmoCommandBase
	{
		private MessmoSendSMSImpulse _impulse;
        
		public MessmoSendSMSCmd(MessmoSendSMSImpulse impulse)
		{
			_impulse = impulse;
		}

		public override string DoCommand()
		{
			MessmoSendSMSRequest req =  new MessmoSendSMSRequest("SMS", MessmoKey, MessmoChannelNumber,
				Matchnet.HTTPMessaging.Utilities.GetMessmoFormatTimestamp(_impulse.TimeStamp), _impulse.MobileNumber, _impulse.Message, MessmoAPIVersion);

			return MessmoHelper.PostMessmoRequest(req);
		}
	}
}
