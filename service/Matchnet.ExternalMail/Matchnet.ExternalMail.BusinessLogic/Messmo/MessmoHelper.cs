using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.HTTPMessaging;
using Matchnet.HTTPMessaging.RequestTypes;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.List.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.Impulse;

namespace Matchnet.ExternalMail.BusinessLogic.Messmo
{
	public class MessmoHelper
	{
		#region Constants
		public const int GENDERID_FEMALE = 2;
		public const int GENDERID_FTM = 32;
		public const int GENDERID_MALE = 1;
		public const int GENDERID_MTF = 16;
		public const int GENDERID_SEEKING_FEMALE = 8;
		public const int GENDERID_SEEKING_FTM = 128;
		public const int GENDERID_SEEKING_MALE = 4;
		public const int GENDERID_SEEKING_MTF = 64;

		public const int REGIONID_USA = 223;
		public const int MAX_REGION_STRING_LENGTH = 40;

		public const string BEGIN_BLANK = "Blank";
//		public const string END_BLANK = "</Blank>";
		public const string FREETEXT_NOT_APPROVED = "My new essay is being approved by Customer Care. Check back soon to find out more about me.";
		#endregion

		public static string MessmoPostURL
		{
			get
			{
				return Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MESSMO_POST_URL");
			}
		}

		public static string NoPhotoImage
		{
			get
			{
				return Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("DEFAULT_NO_PHOTO_FILE_LOCATION");
			}
		}


		#region Enqueue Methods
		/// <summary>
		/// Sends a SMS to the user to confirm their subscription process. The user has to click on the URL within the message to
		/// actually activate their Messmo account.
		/// </summary>
		/// <param name="senderBrandID"></param>
		/// <param name="timestamp"></param>
		/// <param name="mobileNumber"></param>
		/// <param name="message"></param>
		public static void SendSMSToUser(int senderBrandID, DateTime timestamp, string mobileNumber, string message)
		{
			//ExternalMailBL.Instance.Enqueue(new MessmoSendSMSImpulse(senderBrandID, timestamp, mobileNumber, message, true));
			ExternalMailBL.Instance.Enqueue(new MessmoSendSMSImpulse(senderBrandID, timestamp, mobileNumber, message));
		}

		public static void SendMessmoMultiPartMessage(int recipientBrandID, int senderBrandID, int sendingMemberID, DateTime messageDate, string receivingMessmoSubID,
			string messageBody, string smartMessageTag, string taggle, bool replace, bool notify, Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMsgType )
		{
			sendMessmoMultiPartMessage(recipientBrandID, senderBrandID, sendingMemberID, messageDate, receivingMessmoSubID, messageBody, smartMessageTag,
										taggle, replace, notify, Matchnet.HTTPMessaging.Utilities.GetButtonTypeFromMessmoMessageType(messmoMsgType));
		}
		/// <summary>
		/// Send a multi part message to Messmo.  Pretty much all messages with member profile attached are considered multi part.
		/// </summary>
		/// <param name="recipientBrandID"></param>
		/// <param name="senderBrandID"></param>
		/// <param name="sendingMemberID"></param>
		/// <param name="messageDate"></param>
		/// <param name="receivingMessmoSubID"></param>
		/// <param name="messageBody"></param>
		/// <param name="smartMessageTag"></param>
		/// <param name="taggle"></param>

		private static void sendMessmoMultiPartMessage(int recipientBrandID, int senderBrandID, int sendingMemberID, DateTime messageDate, string receivingMessmoSubID,
			string messageBody, string smartMessageTag, string taggle, bool replace, bool notify, Matchnet.HTTPMessaging.Constants.MessmoButtonType messmoButtonType )
		{
			// TODO: check to see if the sender is on the receiver's ignore list only in some cases...?

			ExternalMailBL.Instance.Enqueue(new MessmoSendMultiPartMessageImpulse(recipientBrandID, senderBrandID, sendingMemberID, messageDate, receivingMessmoSubID,
				messageBody, smartMessageTag, taggle, replace, notify, messmoButtonType));
		}

		/// <summary>
		/// Use this to send Messmo member subscription status
		/// </summary>
		/// <param name="senderBrandID"></param>
		/// <param name="timestamp"></param>
		/// <param name="messmoSubscriberID"></param>
		/// <param name="status">2 possible valuess are: SetValidUntil & GetValidUntil</param>
		/// <param name="setDate">Subscription end date</param>
		public static void SendMessmoUserTransStatusRequest(int senderBrandID, DateTime timestamp, string messmoSubscriberID, string status, DateTime setDate)
		{
			ExternalMailBL.Instance.Enqueue(new MessmoTranStatusImpulse(senderBrandID, timestamp, messmoSubscriberID, status, setDate));
		}
		/// <summary>
		/// Use this method to send a member's full profile information within BL.
		/// </summary>
		/// <param name="brandID"></param>
		/// <param name="memberID"></param>
		/// <param name="timestamp"></param>
		public static void SendMessmoFullProfileRequest(int brandID, int memberID, DateTime timestamp)
		{
			ExternalMailBL.Instance.Enqueue(new MessmoFullProfileImpulse(brandID, memberID, timestamp, memberID.ToString()));
		}
		
		#endregion		

		public static bool NotifyOfMessmoMessageArrival(IMemberDTO member, Brand brand, HTTPMessaging.Constants.MessmoMessageType messmoMsgType)
		{
			bool notify = false;
						
			if(member != null && brand != null)
			{
				int memberAlertMask = member.GetAttributeInt(brand, "MessmoAlertMask", 0);
				Matchnet.HTTPMessaging.Constants.MessmoAlertMask messageAlertMask = Matchnet.HTTPMessaging.Utilities.MapToMessmoAlertMask(messmoMsgType);
				
				if((memberAlertMask & (int)messageAlertMask) > 0)
				{
					notify = true;
				}
			}

			return notify;
		}

		/// <summary>
		/// If we never sent a full profile information for this member before, send the full profile information.
		/// </summary>
		/// <param name="brandID"></param>
		/// <param name="memberID"></param>
		/// <param name="timestamp"></param>
		public static void SendFullProfileCheck(int brandID, int memberID)
		{
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(memberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
            Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
				
			if(member == null || brand == null)
				throw new ApplicationException("SendFullProfileCheck() Either member or brand is NULL");

			DateTime lastFullProfileSendDate = member.GetAttributeDate(brand, "MessmoFullProfileSendDate", DateTime.MinValue);

			// If full profile was never sent, let's send this
			if(lastFullProfileSendDate == DateTime.MinValue)
			{
			    Member.ServiceAdapters.Member memberObj = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
				SendMessmoFullProfileRequest(brandID, memberID, DateTime.Now);
				// Update the member attribute
                memberObj.SetAttributeDate(brand, "MessmoFullProfileSendDate", DateTime.Now);
				Matchnet.Member.ServiceAdapters.MemberSA.Instance.SaveMember(memberObj);
			}

			// If already sent we do nothing
		}

	    /// <summary>
	    /// Sends profiles from the specified list
	    /// </summary>
	    /// <param name="recipientBrand">member's brand</param>
	    /// <param name="member">member who owns the list</param>
	    /// <param name="list">list object</param>
	    /// <param name="hotListCat">hot list category</param>
	    /// <param name="sendCount">how many to send (most recent ones get sent)</param>
	    /// <param name="receivingMessmoSubID">Messmo subscription ID of the person to send these to</param>
	    public static void SendProfilesFromList(Brand recipientBrand, IMemberDTO member, List.ServiceAdapters.List list, HotListCategory hotListCat, int sendCount, string receivingMessmoSubID)
		{
			int sendingMemberID, senderBrandID;
			DateTime messageDate;
			string messageBody, smartMessageTag, taggle;
			IMemberDTO sendingMember = null;
			
			if (list != null)
			{
				int rowCount;

				ArrayList memberIDs = list.GetListMembers(hotListCat, recipientBrand.Site.Community.CommunityID,
					recipientBrand.Site.SiteID, 1, sendCount, out rowCount);

				int i = 0;
				foreach (int memberID in memberIDs)
				{
					if (i >= sendCount)
						break;

					sendingMemberID = memberID;
                    sendingMember = MailHandlerUtils.GetIMemberDTO(sendingMemberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
					
					if (sendingMember != null)
					{
						// MessmoSendMultiPartMessageCmd senderBrandID to grab the profile info and photo
						sendingMember.GetLastLogonDate(recipientBrand.Site.Community.CommunityID, out senderBrandID);

						if(senderBrandID != Constants.NULL_INT)
						{
							Matchnet.Content.ValueObjects.BrandConfig.Brand senderBrand = BrandConfigSA.Instance.GetBrandByID(senderBrandID);

							ListItemDetail itemDetail = list.GetListItemDetail(hotListCat, recipientBrand.Site.Community.CommunityID,
								recipientBrand.Site.SiteID, sendingMemberID);

							messageDate = itemDetail.ActionDate;
							
							Matchnet.HTTPMessaging.Constants.MessmoMessageType messmoMsgType = Matchnet.ExternalMail.ValueObjects.MessmoUtilities.MapHotListCategoryToMessmoMessageType(hotListCat);
							
							messageBody = Matchnet.HTTPMessaging.Utilities.GetMessmoMessageBody(messmoMsgType, string.Empty, string.Empty);
												
							smartMessageTag = Matchnet.HTTPMessaging.Utilities.GetMessmoSmartMessageTag(messmoMsgType, sendingMember.GetUserName(senderBrand));
							// this is not a real message in our system so don't pass the groupID or the messagelistID
							taggle = Matchnet.HTTPMessaging.Utilities.GetMessmoTaggleValue(sendingMemberID, member.MemberID, messmoMsgType,
								recipientBrand.Site.Community.CommunityID, Constants.NULL_INT);					

							SendMessmoMultiPartMessage(recipientBrand.BrandID, senderBrandID, sendingMemberID, messageDate, receivingMessmoSubID, messageBody,
								smartMessageTag, taggle, true,
								NotifyOfMessmoMessageArrival(member, recipientBrand, messmoMsgType),
								messmoMsgType);
						}
					}

					i++;
				}
			}
		}

		/// <summary>
		/// This is taken from BulkMail service. 
		/// </summary>
		/// <returns></returns>
		public static SearchPreferenceCollection GetNewMemberInYourAreaSearchPrefs(Brand brand, IMemberDTO member)
		{
			SearchPreferenceCollection newPrefs = null;

			int ageFactor = Convert.ToInt32(RuntimeSettings.GetSetting("NEW_MEMBER_EMAIL_AGE_FACTOR"));
			int distance = Convert.ToInt32(RuntimeSettings.GetSetting("NEW_MEMBER_EMAIL_DISTANCE"));

			int genderMask = member.GetAttributeInt(brand.Site.Community.CommunityID, 0, 0, "GenderMask", Constants.NULL_INT);
			int regionID = member.GetAttributeInt(brand.Site.Community.CommunityID, 0, 0, "RegionID", Constants.NULL_INT);
			DateTime birthDate = member.GetAttributeDate(brand.Site.Community.CommunityID, 0, 0, "Birthdate", DateTime.MinValue);

			if (genderMask != Constants.NULL_INT && regionID != Constants.NULL_INT && birthDate != DateTime.MinValue && Matchnet.GenderUtils.IsFlippable(genderMask))
			{
				newPrefs = new SearchPreferenceCollection();

				genderMask = GenderUtils.FlipMaskIfHeterosexual(genderMask); // Get seeking gender
				int[] agerange = AgeUtils.GetAgeRange(birthDate, Constants.NULL_INT, Constants.NULL_INT);
				newPrefs.Add("GenderMask", genderMask.ToString());
				newPrefs.Add("RegionID", regionID.ToString());
				newPrefs.Add("SearchTypeID", "4"); //Region
				newPrefs.Add("SearchOrderBy", "1"); // JoinDate
				newPrefs.Add("Distance", distance.ToString());
				newPrefs.Add("MinAge", (agerange[0] - ageFactor).ToString());
				newPrefs.Add("MaxAge", (agerange[1] + ageFactor).ToString());
			}

			return newPrefs;
		}	

		/// <summary>
		/// If you need to post to Messmo and do nothing with the body, use this method. This method does not return the body
		/// unless there was an error.
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		public static string PostMessmoRequest(MessmoRequest request)
		{
			string ret = string.Empty;

			string postData = request.GetPostData(true);
			HttpStatusCode statusCode;
			string respResult = SMSProxy.SendPost(postData, MessmoPostURL, 60000, "application/x-www-form-urlencoded", out statusCode);

			if(statusCode == HttpStatusCode.OK)
			{
				ret = statusCode.ToString();
			}
			else
			{
				ret = statusCode.ToString() + ": " + respResult;
			}

			return ret;
		}

		public static string GetMemberFullProfile(int memberID, int brandID)
		{
			string fullProfile = string.Empty;

			Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(memberID, MemberLoadFlags.IngoreSACache);

			StringWriter sw = null;
			XmlTextWriter writer = null;

			if(member != null)
			{
				sw = new StringWriter();
				writer = new XmlTextWriter(sw);		

				writer.WriteStartElement("Supplementary");

				// Mini profile info
				// Username
				writer.WriteStartElement(BEGIN_BLANK);
				writer.WriteString(member.GetUserName(brand));
				writer.WriteEndElement();
				
				// Gendermask
				int genderMask = member.GetAttributeInt(brand, "GenderMask");
				writer.WriteStartElement(BEGIN_BLANK);
				if(genderMask > 0)
				{					
					writer.WriteString(GetGenderMaskString(genderMask));
				}
				else
				{
					writer.WriteString("*");
				}				
				writer.WriteEndElement();

				// Birthday
				DateTime birthdate = member.GetAttributeDate(brand, "Birthdate", DateTime.MinValue);
				writer.WriteStartElement(BEGIN_BLANK);
				if(birthdate != DateTime.MinValue)
				{
					int age = GetAge(birthdate);
					if(age > 0)
					{						
						writer.WriteString(age.ToString());
					}
					else
					{
						writer.WriteString("*");
					}
				}
				else
				{
					writer.WriteString("*");
				}
				writer.WriteEndElement();

				// Region
				int regionID = member.GetAttributeInt(brand, "RegionID", -1);
				writer.WriteStartElement(BEGIN_BLANK);
				if(regionID > -1)
				{
					writer.WriteString(GetRegionString(regionID, 2, false, true, false));
				}
				else
				{
					writer.WriteString("*");
				}
				writer.WriteEndElement();
				//***End Mini profile info
					
				// relationship preference
				writer.WriteStartElement("For");
				writer.WriteString(GetResourceString(brand.BrandID, "RelationshipMask", member.GetAttributeInt(brand, "RelationshipMask", 0), 1));
				writer.WriteEndElement();

				// about me
				writer.WriteStartElement("About_Me");
				writer.WriteString(member.GetAttributeTextApproved(brand, "AboutMe", FREETEXT_NOT_APPROVED));
				writer.WriteEndElement();

				// height
				writer.WriteStartElement("Height");
				int height = member.GetAttributeInt(brand, "Height", 0);
				if(height > 0)
				{
					writer.WriteString(GetResourceString(brand.BrandID, "Height", height, 0));
				}
				writer.WriteEndElement();

				// relationship status
				writer.WriteStartElement(BEGIN_BLANK);
				writer.WriteString(GetResourceString(brand.BrandID, "RelationshipStatus", member.GetAttributeInt(brand, "RelationshipStatus", 0), 0));
				writer.WriteEndElement();

				// number of children
				writer.WriteStartElement("Children");
				writer.WriteString(member.GetAttributeInt(brand, "ChildrenCount", 0).ToString());
				writer.WriteEndElement();

				// smoking habits
				writer.WriteStartElement("I_smoke");
				writer.WriteString(GetResourceString(brand.BrandID, "SmokingHabits", member.GetAttributeInt(brand, "SmokingHabits", 0), 0));
				writer.WriteEndElement();

				// education
				writer.WriteStartElement("My_education");
				writer.WriteString(GetResourceString(brand.BrandID, "EducationLevel", member.GetAttributeInt(brand, "EducationLevel", 0), 0));
				writer.WriteEndElement();

				// occupation
				writer.WriteStartElement("Occupation");
				writer.WriteString(member.GetAttributeText(brand, "OccupationDescription"));
				writer.WriteEndElement();

				writer.WriteEndElement();	// end Supplementary tag
				
			}

			if(sw != null)
			{
				fullProfile = sw.ToString();
				if(writer != null)
				{
					writer.Close();
				}
			}

			return fullProfile;

		}

		/// <summary>
		/// Hits Bedrock to get the resource needed.
		/// </summary>
		/// <param name="brandID"></param>
		/// <param name="attributeName"></param>
		/// <param name="attributeValue"></param>
		/// <param name="isMask"></param>
		/// <returns></returns>
		private static string GetResourceString(Int32 brandID, string attributeName, Int32 attributeValue, Int32 isMask)
		{
			string ret = string.Empty;

			ret = SMSProxy.SendGet(string.Format("{0}?BrandID={1}&AttributeName={2}&AttributeValue={3}&IsMask={4}",
				RuntimeSettings.GetSetting("MESSMO_GETRESOURCE_URL"), brandID.ToString(), attributeName, attributeValue.ToString(), isMask.ToString()));

			return ret;
		}

		/// <summary>
		/// Messmo expects 4 fields regardless if we have them or not
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public static string GetMemberMiniProfie(int memberID, int brandID, bool insertSupplementaryTag)
		{
			string miniProfile = string.Empty;

			Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(memberID, MemberLoadFlags.None);

			StringWriter sw = null;
			XmlTextWriter writer = null;

			if(member != null)
			{
				sw = new StringWriter();
				writer = new XmlTextWriter(sw);				

				if(insertSupplementaryTag)
				{
					writer.WriteStartElement("Supplementary");
				}

				// Username
				writer.WriteStartElement(BEGIN_BLANK);
				writer.WriteString(member.GetUserName(brand));
				writer.WriteEndElement();
				
				// Gendermask
				int genderMask = member.GetAttributeInt(brand, "GenderMask");
				writer.WriteStartElement(BEGIN_BLANK);
				if(genderMask > 0)
				{					
					writer.WriteString(GetGenderMaskString(genderMask));
				}
				else
				{
					writer.WriteString("*");
				}				
				writer.WriteEndElement();

				// Birthday
				DateTime birthdate = member.GetAttributeDate(brand, "Birthdate", DateTime.MinValue);
				writer.WriteStartElement(BEGIN_BLANK);
				if(birthdate != DateTime.MinValue)
				{
					int age = GetAge(birthdate);
					if(age > 0)
					{						
						writer.WriteString(age.ToString());
					}
					else
					{
						writer.WriteString("*");
					}
				}
				else
				{
					writer.WriteString("*");
				}
				writer.WriteEndElement();

				// Region
				int regionID = member.GetAttributeInt(brand, "RegionID", -1);
				writer.WriteStartElement(BEGIN_BLANK);
				if(regionID > -1)
				{
					writer.WriteString(GetRegionString(regionID, 2, false, true, false));
				}
				else
				{
					writer.WriteString("*");
				}
				writer.WriteEndElement();

				if(insertSupplementaryTag)
				{
					writer.WriteEndElement();			
				}
			}

			if(sw != null)
			{
				miniProfile = sw.ToString();
				if(writer != null)
				{
					writer.Close();
				}
			}

			return miniProfile;
		}

		public static string GetProfilePhotoUrl(int memberID, int brandID, out string fileType)
		{
			string ret = string.Empty;
			fileType = string.Empty;

			Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(memberID, MemberLoadFlags.None);

			PhotoCommunity photos = member.GetPhotos(brand.Site.Community.CommunityID);
			Photo photo = null;
			Photo retVal = null;

			for (byte photoNum = 0; photoNum < photos.Count; photoNum++)
			{
				photo = photos[photoNum];
				if (photo.IsApproved)
				{
					if (photo.IsPrivate)
					{
						retVal = photo;  //This private photo will get returned if no non-private approved photos are found
					}
					else
					{
						retVal = photo; //We found a good photo, get out
						break;
					}
				}
			}
	
			if(retVal != null)
			{                
				ret = getSiteURL(brand) + photo.ThumbFileWebPath;
                				
				fileType = photo.ThumbFileWebPath.Substring(photo.ThumbFileWebPath.Length - 3, 3).ToLower();
			}
			else
			{
				// if we didn't find any photo, set the photo to no-photo-sm-54x70.gif
                ret = getSiteURL(brand) + RuntimeSettings.GetSetting("DEFAULT_NO_PHOTO_FILE_LOCATION");
				if(ret.Length > 3)
				{
					fileType = ret.Substring(ret.Length - 3, 3).ToLower();
				}
			}

			return ret;
		}
		public static string GetRegionString(int regionID, int languageID, bool showPostalCode, bool showAbbreviatedState, bool showAbbreviatedCountry)
		{
			string regionString = string.Empty;

			if (regionID > 0)
			{
				// regionLanguage = new RegionLanguage();
				// regionLanguage.PopulateHierarchy(regionAttribute, g.Brand.Site.LanguageID);
				RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, languageID);

				// Incrementally build up the regionString with commas where necessary
				// based on the contents being empty or not before and after each comma
				// This made for CI/CP and tested on CL/GL 
				// start WEL
				regionString = regionLanguage.CityName;

				string stateString = string.Empty;

				if (isDefaultRegion(regionLanguage))
				{
					if (showAbbreviatedState && regionLanguage.CountryRegionID == REGIONID_USA)
					{
						stateString = regionLanguage.StateAbbreviation;
					}
					else
					{
						stateString = regionLanguage.StateDescription;
					}
				}

				if (isNotBlankString(regionString) && isNotBlankString(stateString))
				{
					regionString += ", ";
				}
				regionString += stateString;

				if (!isDefaultRegion(regionLanguage))
				{
					string countryString = string.Empty;
					if (showAbbreviatedCountry)
					{
						countryString = regionLanguage.CountryAbbreviation;
					}
					else
					{
						countryString = regionLanguage.CountryName;
					}
					if (isNotBlankString(regionString) && isNotBlankString(countryString))
					{
						regionString += ", ";
					}
					regionString += countryString;
				}

				// end WEL

				if (regionString != null && regionString.Length > MAX_REGION_STRING_LENGTH)
				{
					regionString = regionString.Substring(0, MAX_REGION_STRING_LENGTH - 3) + "...";
				}
			}
			else
			{
				regionString = "N/A";
			}
			return regionString;
		}

		public static string GetGenderMaskString(int genderMask)
		{			
			string gender = GetGenderString(genderMask);
			string seeking = GetSeekingGenderString(genderMask);

			if (seeking == string.Empty || gender == string.Empty)
			{
				return string.Empty;
			}
			else
			{
				return gender + " seeking " + seeking;
			}
		}

		public static int GetAge(DateTime birthDate)
		{
			if (birthDate == DateTime.MinValue)
				return 0;

			return (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
		}

		public static string GetGenderString(int genderMask)
		{			
			string genderStr = string.Empty;

			if (genderMask < 0)
				return genderStr;

			if ((genderMask & GENDERID_MALE) == GENDERID_MALE)
				return "Man";

			if ((genderMask & GENDERID_FEMALE) == GENDERID_FEMALE)
				return "Woman";

			if ((genderMask & GENDERID_MTF) == GENDERID_MTF)
				return "MTF";

			if ((genderMask & GENDERID_FTM) == GENDERID_FTM)
				return "FTM";
			
			return genderStr;
		}

		public static string GetSeekingGenderString(int genderMask)
		{
			string seeking = string.Empty;

			if ((genderMask & GENDERID_SEEKING_FEMALE) == GENDERID_SEEKING_FEMALE)
				seeking = "Woman";

			if ((genderMask & GENDERID_SEEKING_MALE) == GENDERID_SEEKING_MALE)
			{
				if (seeking.Length > 0)
					seeking = seeking + ", ";

				seeking = seeking + "Man";
			}

			if ((genderMask & GENDERID_SEEKING_MTF) == GENDERID_SEEKING_MTF)
			{
				if (seeking.Length > 0)
					seeking = seeking + ", ";

				seeking = seeking + "MTF";
			}

			if ((genderMask & GENDERID_SEEKING_FTM) == GENDERID_SEEKING_FTM)
			{
				if (seeking.Length > 0)
					seeking = seeking + ", ";

				seeking = seeking + "FTM";
			}

			return seeking;
		}


		public static string GetSiteURL(Int32 brandID)
		{
			Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);

			if(brand != null)
			{
				return getSiteURL(brand);
			}

			return string.Empty;
		}

		private static string getSiteURL(Matchnet.Content.ValueObjects.BrandConfig.Brand brand)
		{
			string hostHeader = "www";			

			try
			{
				string env = RuntimeSettings.GetSetting("ENVIRONMENT_TYPE");
			
				if(env != null && env != string.Empty)
				{
					if(env.ToLower() == "dev")
					{
						hostHeader = "dev01";
					}
					else if(env.ToLower() == "stg")
					{
						hostHeader = "stg01";
					}
					else if(env.ToLower() == "stgv")
					{
						hostHeader = "stgv101";
					}
					else if (env.ToLower() == "stgv2")
					{
						hostHeader = "stgv201";
					}
				}
			}
			catch
			{
				// do nothing.  this is to safeguard against ENVIRONMENT_TYPE setting not being in prod DB
			}

			return string.Format("http://{0}.{1}", hostHeader, brand.Uri.ToLower());            
		}
		private static bool isDefaultRegion(Matchnet.Content.ValueObjects.Region.RegionLanguage regionLanguage)
		{			
			if (regionLanguage.CountryRegionID == REGIONID_USA)
			{
				return true;
			}
			return false;
		}

		private static bool isNotBlankString(string s)
		{
			if (s != String.Empty && s != null)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		
	}


	/// <summary>
	/// Taken from BulkMail service
	/// </summary>
	public class AgeUtils
	{
		public static int[] GetAgeRange(DateTime birthDate, int ageMin, int ageMax)
		{
			if (ageMin == Constants.NULL_INT || ageMax == Constants.NULL_INT)
			{
				int age = (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
				if (age >= Constants.MIN_AGE_RANGE && age <= 24)
				{
					ageMin = Constants.MIN_AGE_RANGE;
					ageMax = 24;
				}
				else if (age >= 25 && age <= 34)
				{
					ageMin = 25;
					ageMax = 34;
				}
				else if (age >= 35 && age <= 44)
				{
					ageMin = 35;
					ageMax = 44;
				}
				else if (age >= 45 && age <= 54)
				{
					ageMin = 45;
					ageMax = 54;
				}
				else if (age >= 55 && age <= Constants.MAX_AGE_RANGE)
				{
					ageMin = 55;
					ageMax = Constants.MAX_AGE_RANGE;
				}
				else
				{
					ageMin = 25;
					ageMax = 34;
				}
			}
			return new int[] { ageMin, ageMax };
		}

		public static int GetAge(DateTime birthDate)
		{
			return (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
		}
	}
}
