using System;
using System.Net;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.HTTPMessaging;
using Matchnet.HTTPMessaging.RequestTypes;
using Matchnet.Member.ValueObjects.Interfaces;

namespace Matchnet.ExternalMail.BusinessLogic.Messmo
{
	/// <summary>
	/// TODO: remove this file. we no longer do this via queues
	/// Messmo API 2.4 A Channel Unsubscribes a User
	/// </summary>
	public class MessmoUnsubscribeCmd : MessmoCommandBase
	{
		private MessmoUnsubscribeImpulse _impulse;

		public MessmoUnsubscribeCmd(MessmoUnsubscribeImpulse impulse)
		{
			_impulse = impulse;
		}

		public override string DoCommand()
		{
			string ret = string.Format("HTTP POST was not made. Unable to find the member: {0}", _impulse.MemberID);
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(_impulse.MemberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);

			if(member != null)
			{
				Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(_impulse.BrandID);
				string messmoSubID = member.GetAttributeText(brand, "MessmoSubscriberID", string.Empty);

				if(messmoSubID == string.Empty)
				{
					ret = string.Format("HTTP POST was not made. This member ({0}) does not have a MessmoSubcriberID", _impulse.MemberID);
				}
				else
				{
					MessmoUnsubscribeRequest req =  new MessmoUnsubscribeRequest("off", MessmoKey, MessmoChannelNumber,
						Matchnet.HTTPMessaging.Utilities.GetMessmoFormatTimestamp(_impulse.TimeStamp), messmoSubID, MessmoAPIVersion);

					ret = MessmoHelper.PostMessmoRequest(req);
				}
			}

			return ret;
		}

	}
}
