using System;
using Matchnet.ExternalMail.ValueObjects.Impulse;

namespace Matchnet.ExternalMail.BusinessLogic.Messmo
{
	/// <summary>
	/// Summary description for MessmoCommandMapper.
	/// </summary>
	public class MessmoCommandMapper
	{
		public static MessmoCommandBase GetMessmoCommand(MessmoImpulseBase impulse)
		{
			if(impulse is MessmoSendMessageImpulse)
			{
				return new MessmoSendMessageCmd((MessmoSendMessageImpulse)impulse);
			}
			else if(impulse is MessmoSubscribeImpulse)
			{
				return new MessmoSubscribeCmd((MessmoSubscribeImpulse)impulse);
			}
			else if(impulse is MessmoSendMultiPartMessageImpulse)
			{
				return new MessmoSendMultiPartMessageCmd((MessmoSendMultiPartMessageImpulse)impulse);
			}
			else if(impulse is MessmoFullProfileImpulse)
			{
				return new MessmoFullProfileCmd((MessmoFullProfileImpulse)impulse);
			}
			else if(impulse is MessmoSendSMSImpulse)
			{
				return new MessmoSendSMSCmd((MessmoSendSMSImpulse)impulse);
			}
			else if(impulse is MessmoTranStatusImpulse)
			{
				return new MessmoTranStatusCmd((MessmoTranStatusImpulse)impulse);
			}
			else if(impulse is MessmoUnsubscribeImpulse)
			{
				return new MessmoUnsubscribeCmd((MessmoUnsubscribeImpulse)impulse);
			}
			else if(impulse is MessmoSendInitialBatchImpulse)
			{
				return new MessmoSendInitialBatchCmd((MessmoSendInitialBatchImpulse)impulse);
			}


			return null;
		}
	}
}
