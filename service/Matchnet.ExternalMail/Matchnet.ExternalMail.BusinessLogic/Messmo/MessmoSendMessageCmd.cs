using System;
using System.Net;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.HTTPMessaging;
using Matchnet.HTTPMessaging.RequestTypes;
using Matchnet.HTTPMessaging.ResponseTypes;

namespace Matchnet.ExternalMail.BusinessLogic.Messmo
{
	/// <summary>
	/// Messmo's API 2.5 - A Channel Sends Content to a User. This isn't being used in the beginning. This  can be used in the future to
	/// send simple messages (i.e. system message).
	/// </summary>
	public class MessmoSendMessageCmd : MessmoCommandBase
	{
		MessmoSendMessageImpulse _impulse;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="impulse"></param>
		public MessmoSendMessageCmd(MessmoSendMessageImpulse impulse)
		{
			_impulse = impulse;
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override string DoCommand()
		{
			MessmoSendMessageRequest req = new MessmoSendMessageRequest("message", MessmoKey, MessmoChannelNumber, Matchnet.HTTPMessaging.Utilities.GetMessmoFormatTimestamp(_impulse.MessageDate),
				_impulse.ReceivingMessmoSubID, _impulse.MessageBody, _impulse.Taggle, MessmoAPIVersion);
			
			return MessmoHelper.PostMessmoRequest(req);
		}

	}
}
