using System;
using System.Diagnostics;

using Matchnet.Exceptions;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Member.ServiceAdapters;
using Matchnet.EmailLibrary;
using Matchnet.Member.ValueObjects.Interfaces;
using MSA = Matchnet.Member.ServiceAdapters;

using Matchnet.ExternalMail.ValueObjects.StormpostSoap;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.Impulse;

namespace Matchnet.ExternalMail.BusinessLogic
{
	public class SoapHandler
	{
		public static SoapHandler Instance = new SoapHandler();

		public const string STORMPOST_DO_NOT_SHOW_THUMBNAIL = "HTML";

		#region Constructors
		private SoapHandler()
		{
		}
		#endregion

		#region Public Methods
		/// <summary>
		/// Based on the imcoming impulse object, this method will determine Import & Send and execute
		/// </summary>
		/// <param name="impulse"></param>
		public void Lather(ImpulseBase impulse)
		{
			switch ((EmailType) EmailTypeConverter.ToInt(impulse))
			{
				case (EmailType.ActivationLetter):
					sendActivationLetter(impulse);
					break;
				
				case (EmailType.ForgotPassword):
					sendForgotPassword(impulse);
					break;

				case (EmailType.EmailChangeConfirmation):
					sendEmailChangeConfirmation(impulse);
					break;

				case (EmailType.MessageBoardInstantNotification):
					sendMessageBoardInstantNotification(impulse);
					break;

				case (EmailType.MessageBoardDailyUpdates):
					sendMessageBoardDailyUpdates(impulse);
					break;

				case (EmailType.EcardToMember):
					sendEcardToMember(impulse);
					break;

				case (EmailType.EcardToFriend):
					sendEcardToFriend(impulse);
					break;

				case (EmailType.RenewalFailureNotification):
					sendRenewalFailureNotification(impulse);
					break;
					
				default:
					throw new BLException("Impulse type " + EmailTypeConverter.ToInt(impulse) + " is not supported in soap handler.");
			}
		}	

		#endregion

		#region Private Methods
		// Make sure to name methods to match with EmailType Enum

		private void sendActivationLetter(ImpulseBase impulse)
		{
			int memberID = ((RegVerificationImpulse) impulse).MemberID;
			string verificationCode = String.Empty;

            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(memberID, MemberLoadFlags.None);	

			// Member svc may not have that member in time since registration was just completed.
			if (member == null)
			{
				EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Member not found in member service. Skipping this member. [MemberID:" + memberID.ToString() + "]", EventLogEntryType.Warning);

				return;	
			}

			try
			{
				verificationCode = EmailVerify.HashMemberID(memberID);
			}
			catch(Exception ex)
			{
				EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, ex.ToString(), EventLogEntryType.Warning);
			}

// This was overloading Stormpost. Turning off for now. 
//			StormPostSoapBL.Instance.Import(ImportTemplateType.Minimal, BrandConfigSA.Instance.GetBrandByID(impulse.BrandID).Site.Community.CommunityID,
//				new string[]{member.EmailAddress, memberID.ToString()});

			StormPostSoapBL.Instance.Send(SendTemplateType.SendTemplate_Activation, BrandConfigSA.Instance.GetBrandByID(impulse.BrandID).Site.SiteID, member.EmailAddress,
				new string[]{verificationCode});
		}


		private void sendForgotPassword(ImpulseBase impulse)
		{
			string emailAddress = ((PasswordMailImpulse) impulse).EmailAddress;
			string password = String.Empty;
		    var brand = BrandConfigSA.Instance.GetBrandByID(impulse.BrandID);

			try
			{
				password = MemberSA.Instance.GetPassword(emailAddress);
			}
			catch(Exception ex)
			{
				EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, ex.ToString(), EventLogEntryType.Warning);
			}

            int memberID = MemberSA.Instance.GetMemberIDByEmail(emailAddress, brand.Site.Community.CommunityID);

// This was overloading Stormpost. Turning off for now. 
//			StormPostSoapBL.Instance.Import(ImportTemplateType.Minimal, BrandConfigSA.Instance.GetBrandByID(impulse.BrandID).Site.Community.CommunityID,
//				new string[]{emailAddress, memberID.ToString()});

			StormPostSoapBL.Instance.Send(SendTemplateType.SendTemplate_PasswordReminder, brand.Site.SiteID, emailAddress,
				new string[]{password});
		}


		private void sendEmailChangeConfirmation(ImpulseBase impulse)
		{
			int memberID = ((EmailVerificationImpulse) impulse).MemberID;
			string verificationCode = String.Empty;

            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(memberID, MemberLoadFlags.None);

			try
			{
				verificationCode = EmailVerify.HashMemberID(memberID);
			}
			catch(Exception ex)
			{
				EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, ex.ToString(), EventLogEntryType.Warning);
			}

// This was overloading Stormpost. Turning off for now. 
//			StormPostSoapBL.Instance.Import(ImportTemplateType.Minimal, BrandConfigSA.Instance.GetBrandByID(impulse.BrandID).Site.Community.CommunityID,
//				new string[]{member.EmailAddress, memberID.ToString()});

			StormPostSoapBL.Instance.Send(SendTemplateType.SendTemplate_EmailChangeConfiration, BrandConfigSA.Instance.GetBrandByID(impulse.BrandID).Site.SiteID, member.EmailAddress,
				new string[]{verificationCode});
		}


		private void sendMessageBoardInstantNotification(ImpulseBase impulse)
		{
			MessageBoardInstantNotificationImpulse messageBoardImpulse = (MessageBoardInstantNotificationImpulse) impulse;

            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(messageBoardImpulse.MemberID, MemberLoadFlags.None);

			StormPostSoapBL.Instance.Send(SendTemplateType.SendTemplate_MessageBoardInstantNotification, messageBoardImpulse.SiteID, member.EmailAddress,
				new string[]{member.GetUserName(Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(impulse.BrandID)),
                    messageBoardImpulse.BoardName, messageBoardImpulse.TopicSubject, 
				messageBoardImpulse.ReplyURL, messageBoardImpulse.NewReplies.ToString(), messageBoardImpulse.UnsubscribeURL});
		}

		
		private void sendMessageBoardDailyUpdates(ImpulseBase impulse)
		{
			MessageBoardDailyUpdatesImpulse messageBoardImpulse = (MessageBoardDailyUpdatesImpulse) impulse;

            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(messageBoardImpulse.MemberID, MemberLoadFlags.None);

			StormPostSoapBL.Instance.Send(SendTemplateType.SendTemplate_MessageBoardDailyUpdates, messageBoardImpulse.SiteID, member.EmailAddress,
                new string[]{member.GetUserName(Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(impulse.BrandID)),
                    messageBoardImpulse.BoardName,  messageBoardImpulse.TopicSubject, 
				messageBoardImpulse.NewReplies.ToString(),  messageBoardImpulse.BoardID.ToString(),  messageBoardImpulse.ThreadID.ToString(),  messageBoardImpulse.LastMessageID.ToString(),
				messageBoardImpulse.UnsubscribeURL});
		}

		private void sendEcardToMember(ImpulseBase impulse)
		{
			EcardToMemberImpulse ecardToMember = (EcardToMemberImpulse) impulse;

			Brand brand = BrandConfigSA.Instance.GetBrands().GetBrand(ecardToMember.BrandID);

            IMemberDTO senderMember = MailHandlerUtils.GetIMemberDTO(ecardToMember.SenderMemberID, MemberLoadFlags.None);
			if (senderMember == null)
				throw(new BLException("Cannot get sender Member information for MemberID " + ecardToMember.SenderMemberID.ToString()));

            IMemberDTO recipientMember = MailHandlerUtils.GetIMemberDTO(ecardToMember.RecipientMemberID, MemberLoadFlags.None);
			if (recipientMember == null)
				throw(new BLException("Cannot get recipient Member information for MemberID " + ecardToMember.RecipientMemberID.ToString()));

			bool send = true;
			// Make sure UseYesMail is set to 'True' for the current brand.
			if(Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting(EmailAlertConstant.YES_MAIL_FLAG, brand.Site.Community.CommunityID, brand.Site.SiteID)))
			{				
				AttributeGroup group = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(brand.Site.Community.CommunityID, EmailAlertConstant.ATTRIBUTE_ID);
				
				int mask = (int)EmailAlertMask.ECardAlert;
				int userSetting = recipientMember.GetAttributeInt(brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, int.Parse(group.DefaultValue));

				// Make sure user specified to recieve ecard notification email.
				if((mask & userSetting) != mask)
				{
					send = false;
				}
			}

			if(send)
			{
				// It the recipient is a non-subscriber, then' for some sites we do not want to show the sender's thumbnail.
				string senderThumbnail = STORMPOST_DO_NOT_SHOW_THUMBNAIL;;
				if (Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_VIEW_MINIPROFILE_ECARD_TO_MEMBER", brand.Site.Community.CommunityID, ecardToMember.SiteID))
					|| recipientMember.IsPayingMember(ecardToMember.SiteID))
				{
					senderThumbnail = EmailMemberHelper.DetermineMemberThumbnail(senderMember, brand);
				}

				string ecardUrl = createEcardUrl(recipientMember, brand, ecardToMember.CardUrl);

				if (ecardUrl != Constants.NULL_STRING)
				{
					StormPostSoapBL.Instance.Send(SendTemplateType.SendTemplate_EcardSendToMember, ecardToMember.SiteID, recipientMember.EmailAddress,
						new string[]{senderMember.GetUserName(brand), EmailMemberHelper.DetermineMemberAge(senderMember, brand).ToString()
										, EmailMemberHelper.DetermineMemberRegionDisplay(senderMember, brand)
										, senderThumbnail, ecardUrl, ecardToMember.CardThumbnail, recipientMember.EmailAddress,
										recipientMember.GetUserName(brand), EmailMemberHelper.GetDateDisplay(ecardToMember.SentDate, brand)});
				}			
			}
		}

		private string createEcardUrl(IMemberDTO member, Brand brand, string cardUrl)
		{
			// Don't mess with an http.
			if (!cardUrl.ToLower().StartsWith("http"))
			{
				string devSubdomain = String.Empty;
				if (brand.Site.DefaultHost == "dev")
					devSubdomain = "dev.";

				cardUrl = String.Format("http://connect.{0}{1}/{2}", devSubdomain, brand.Uri, cardUrl);
			}
			return cardUrl;
/*	20070731	Modified by: RB
 * 			// HACK FOR TT 19893.  This should be temporary for less than a month (08/30/2006).
			if (cardUrl.IndexOf("jdate.co.il") == -1)
			{
				return cardUrl;
			}
			else
			{
				EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "MemberID " + member.MemberID.ToString()
					+ " will not be sent an Ecard ExternalMail to jdate.co.il (cardUrl: " + cardUrl
					+ ").", EventLogEntryType.Warning);
				return Constants.NULL_STRING;
			}
*/
		}

		private void sendEcardToFriend(ImpulseBase impulse)
		{
			EcardToFriendImpulse ecardToFriend = (EcardToFriendImpulse) impulse;

			Brand brand = BrandConfigSA.Instance.GetBrands().GetBrand(ecardToFriend.BrandID);

            IMemberDTO senderMember = MailHandlerUtils.GetIMemberDTO(ecardToFriend.SenderMemberID, MemberLoadFlags.None);
			if (senderMember == null)
				throw(new BLException("Cannot get sender Member information for MemberID " + ecardToFriend.SenderMemberID.ToString()));

			string ecardUrl = createEcardUrl(null, brand, ecardToFriend.CardUrl);

			if (ecardUrl != Constants.NULL_STRING)
			{
				StormPostSoapBL.Instance.Send(SendTemplateType.SendTemplate_EcardSendToFriend, ecardToFriend.SiteID, ecardToFriend.RecipientEmailAddress,
					new string[]{senderMember.GetUserName(brand), EmailMemberHelper.DetermineMemberAge(senderMember, brand).ToString()
									, EmailMemberHelper.DetermineMemberRegionDisplay(senderMember, brand)
									, EmailMemberHelper.DetermineMemberThumbnail(senderMember, brand)
									, ecardUrl, ecardToFriend.CardThumbnail, ecardToFriend.RecipientEmailAddress
									, EmailMemberHelper.GetDateDisplay(ecardToFriend.SentDate, brand)});
			}
		}
		
		private void sendRenewalFailureNotification(ImpulseBase impulse)
		{
			RenewalFailureNotificationImpulse renewalFailureNotificationImpulse = (RenewalFailureNotificationImpulse) impulse;
			
			StormPostSoapBL.Instance.Send(SendTemplateType.SendTemplate_RenewalFailureNotification,
			                              renewalFailureNotificationImpulse.SiteID,
			                              renewalFailureNotificationImpulse.EmailAddress,
			                              new string[] {renewalFailureNotificationImpulse.UserName});
			
		}
		#endregion
	}
}
