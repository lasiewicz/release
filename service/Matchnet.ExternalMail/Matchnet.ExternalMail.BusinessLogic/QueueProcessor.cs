using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Messaging;
using System.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.Content.ServiceAdapters;
using Matchnet.EmailLibrary;
using Matchnet.ExternalMail.BusinessLogic.QueueProcessing;
using MSA = Matchnet.Member.ServiceAdapters;

using Matchnet.HTTPMessaging;
using Matchnet.HTTPMessaging.RequestTypes;
using Matchnet.HTTPMessaging.ResponseTypes;

using Matchnet.ExternalMail.BusinessLogic.SMSProvider;
using Spark.Logging;

namespace Matchnet.ExternalMail.BusinessLogic
{
    [Obsolete("Use QueueProcessing.QueueProcessor instead", false)]
    public class QueueProcessor : IQueueProcessor
    {
        #region Public Constants
        public const string DEFAULT_GREY_QUEUE_PATH = @".\private$\ExternalMail";
        public const string DEFAULT_SOAP_QUEUE_PATH = @".\private$\ExternalMail_Soap";
        public const string DEFAULT_SMS_QUEUE_PATH = @".\private$\ExternalMail_SMS";
        public const string DEFAULT_MESSMO_QUEUE_PATH = @".\private$\ExternalMail_MESSMO";
        public const string DEFAULT_MESSMO_DELAYED_QUEUE_PATH = @".\private$\ExternalMail_MESSMO_DELAYED";
        public const string DEFAULT_BATCH_SMS_QUEUE_PATH = @".\private$\ExternalMail_BATCH_SMS";


        #endregion

        #region Private Constants
        private const string CLASS_NAME = "QueueProcessor";
        private const string LOG_NAME = "ExternalMail";
        private const int SLEEP_TIME = 500;
        private const int SLEEP_TIME_ERROR = 15000;
        private const int NO_MESSAGE_DISCONNECT_LIMIT = 6;

        #endregion

        #region Private Variables
        private static string _greyQueuePath = null;
        private static string _soapQueuePath = null;
        private static string _smsQueuePath = null;
        private static string _messmoQueuePath = null;
        private static string _messmoDelayedQueuePath = null;
        private static string _batchSmsQueuePath = null;

        private HydraWriter _hydraWriter = null;

        private bool _runnable;

        private Thread[] _greyThreads;
        private Thread[] _soapThreads;
        private Thread[] _smsThreads;
        private Thread[] _messmoThreads;
        private Thread[] _messmoDelayedThreads;
        private Thread[] _batchSmsThreads;

        private MessageQueue _greyQueue;
        private MessageQueue _soapQueue;
        private MessageQueue _smsQueue;
        private MessageQueue _messmoQueue;
        private MessageQueue _messmoDelayedQueue;
        private MessageQueue _batchSmsQueue;
        private AllowedDomains _allowedDomains;
        #endregion

        #region Public Properties
        public static string GreyQueuePath
        {
            get
            {
                return getGreyQueuePath();
            }
        }


        public static string SoapQueuePath
        {
            get
            {
                return getSoapQueuePath();
            }
        }

        public static string SMSQueuePath
        {
            get
            {
                return getSMSQueuePath();
            }
        }

        public static string MessmoQueuePath
        {
            get
            {
                return getMessmoQueuePath();
            }
        }

        public static string MessmoDelayedQueuePath
        {
            get
            {
                return getMessmoDelayedQueuePath();
            }
        }

        public static string BatchSMSQueuePath
        {
            get
            {
                return getBatchSMSQueuePath();
            }
        }
        #endregion

        #region Public Constructors
        public QueueProcessor()
        {
            ExternalMailBL.InitPerfCounters();
            _allowedDomains = new AllowedDomains(ServiceConstants.SERVICE_NAME);
        }
        #endregion

        #region Public Methods
        public void Start()
        {
            _runnable = true;

            _hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[] { "mnFileExchange", "mnXmail" });
            _hydraWriter.Start();

            startGreyThreads();
            startSoapThreads();
            startSMSThreads();
            startMessmoThreads();
            startMessmoDelayedThreads();
            startBatchSMSThreads();

            EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Started processing GreyQueuePath:" + getGreyQueuePath() + ", SoapQueuePath:" + getSoapQueuePath() + ", SMSQueuePath:" + getSMSQueuePath(), EventLogEntryType.Information);
        }


        public void Stop()
        {
            _runnable = false;

            _hydraWriter.Stop();

            stopGreyThreads();
            stopSoapThreads();
            stopSMSThreads();
            stopMessmoThreads();
            stopBatchSMSThreads();
        }

        #endregion

        #region Private Methods
        private void startGreyThreads()
        {
            _greyQueuePath = getGreyQueuePath();

            _greyQueue = new MessageQueue(_greyQueuePath);
            _greyQueue.Formatter = new BinaryMessageFormatter();

            Int16 threadCount = Convert.ToInt16(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_THREAD_COUNT"));
            _greyThreads = new Thread[threadCount];

            for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
            {
                Thread t = new Thread(new ThreadStart(processGreyCycle));
                t.Name = "GreyProcessThread" + threadNum.ToString();
                t.Start();
                _greyThreads[threadNum] = t;
            }

#if DEBUG
            System.Diagnostics.Trace.WriteLine("Stopped all grey threads");
#endif
        }


        private void startSoapThreads()
        {
            _soapQueuePath = getSoapQueuePath();

            _soapQueue = new MessageQueue(_soapQueuePath);
            _soapQueue.Formatter = new BinaryMessageFormatter();

            Int16 threadCount = Convert.ToInt16(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_THREAD_COUNT"));
            _soapThreads = new Thread[threadCount];

            for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
            {
                Thread t = new Thread(new ThreadStart(processSoapCycle));
                t.Name = "SoapProcessThread" + threadNum.ToString();
                t.Start();
                _soapThreads[threadNum] = t;
            }

#if DEBUG
            System.Diagnostics.Trace.WriteLine("Stopped all soap threads");
#endif
        }

        private void startSMSThreads()
        {
            _smsQueuePath = getSMSQueuePath();

            _smsQueue = new MessageQueue(_smsQueuePath);
            _smsQueue.Formatter = new BinaryMessageFormatter();

            Int16 threadCount = Convert.ToInt16(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_THREAD_COUNT"));
            _smsThreads = new Thread[threadCount];

            for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
            {
                Thread t = new Thread(new ThreadStart(processSMSCycle));
                t.Name = "SMSProcessThread" + threadNum.ToString();
                t.Start();
                _smsThreads[threadNum] = t;
            }

#if DEBUG
            System.Diagnostics.Trace.WriteLine("Started all SMS threads");
#endif
        }

        private void startMessmoThreads()
        {
            _messmoQueuePath = getMessmoQueuePath();

            _messmoQueue = new MessageQueue(_messmoQueuePath);
            _messmoQueue.Formatter = new BinaryMessageFormatter();

            Int16 threadCount = Convert.ToInt16(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_THREAD_COUNT"));
            _messmoThreads = new Thread[threadCount];

            for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
            {
                Thread t = new Thread(new ThreadStart(processMessmoCycle));
                t.Name = "MessmoProcessThread" + threadNum.ToString();
                t.Start();
                _messmoThreads[threadNum] = t;
            }
        }

        private void startMessmoDelayedThreads()
        {
            _messmoDelayedQueuePath = getMessmoDelayedQueuePath();

            _messmoDelayedQueue = new MessageQueue(_messmoDelayedQueuePath);
            _messmoDelayedQueue.Formatter = new BinaryMessageFormatter();

            Int16 threadCount = Convert.ToInt16(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_THREAD_COUNT"));
            _messmoDelayedThreads = new Thread[threadCount];

            for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
            {
                Thread t = new Thread(new ThreadStart(processMessmoDelayedCycle));
                t.Name = "MessmoDelayedProcessThread" + threadNum.ToString();
                t.Start();
                _messmoThreads[threadNum] = t;
            }
        }

        private void startBatchSMSThreads()
        {
            //_smsQueuePath = getSMSQueuePath();
            _batchSmsQueuePath = getBatchSMSQueuePath();

            if (!MessageQueue.Exists(_batchSmsQueuePath))
            {
                MessageQueue.Create(_batchSmsQueuePath, true);
            }

            //_smsQueue = new MessageQueue(_smsQueuePath);
            _batchSmsQueue = new MessageQueue(_batchSmsQueuePath);
            //_smsQueue.Formatter = new BinaryMessageFormatter();
            _batchSmsQueue.Formatter = new BinaryMessageFormatter();

            Int16 threadCount = Convert.ToInt16(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_THREAD_COUNT"));
            //_smsThreads = new Thread[threadCount];
            _batchSmsThreads = new Thread[threadCount];

            for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
            {
                //Thread t = new Thread(new ThreadStart(processSMSCycle));
                Thread t = new Thread(new ThreadStart(processBatchSMSCycle));
                //t.Name = "SMSProcessThread" + threadNum.ToString();
                t.Name = "BatchSMSProcessThread" + threadNum.ToString();
                t.Start();
                //_smsThreads[threadNum] = t;			
                _batchSmsThreads[threadNum] = t;
            }

#if DEBUG
            System.Diagnostics.Trace.WriteLine("Started all Batch SMS threads");
#endif
        }

        private void stopGreyThreads()
        {
            Int16 threadCount = (Int16)_greyThreads.Length;

            for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
            {
                _greyThreads[threadNum].Join(10000);
            }

#if DEBUG
            System.Diagnostics.Trace.WriteLine("Stopped all grey threads");
#endif
        }


        private void stopSoapThreads()
        {
            Int16 threadCount = (Int16)_soapThreads.Length;

            for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
            {
                _soapThreads[threadNum].Join(10000);
            }

#if DEBUG
            System.Diagnostics.Trace.WriteLine("Stopped all soap threads");
#endif
        }

        private void stopSMSThreads()
        {
            Int16 threadCount = (Int16)_smsThreads.Length;

            for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
            {
                _smsThreads[threadNum].Join(10000);
            }

#if DEBUG
            System.Diagnostics.Trace.WriteLine("Stopped all SMS threads");
#endif
        }

        private void stopMessmoThreads()
        {
            Int16 threadCount = (Int16)_messmoThreads.Length;

            for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
            {
                _messmoThreads[threadNum].Join(10000);
            }

#if DEBUG
            System.Diagnostics.Trace.WriteLine("Stopped all Messmo threads");
#endif
        }

        private void stopMessmoDelayedThreads()
        {
            Int16 threadCount = (Int16)_messmoDelayedThreads.Length;

            for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
            {
                _messmoDelayedThreads[threadNum].Join(10000);
            }

#if DEBUG
            System.Diagnostics.Trace.WriteLine("Stopped all Messmo DELAYED threads");
#endif
        }

        private void stopBatchSMSThreads()
        {
            Int16 threadCount = (Int16)_batchSmsThreads.Length;

            for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
            {
                _batchSmsThreads[threadNum].Join(10000);
            }

#if DEBUG
            System.Diagnostics.Trace.WriteLine("Stopped all batch SMS threads");
#endif
        }


        private static string getSoapQueuePath()
        {
            if (_soapQueuePath == null || string.Empty.Equals(_soapQueuePath))
            {
                _soapQueuePath = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_QUEUE_PATH") + "_Soap";

                if (_soapQueuePath == null || string.Empty.Equals(_soapQueuePath))
                {
                    _soapQueuePath = QueueProcessor.DEFAULT_SOAP_QUEUE_PATH;
                }
            }

            return _soapQueuePath;
        }


        private static string getGreyQueuePath()
        {
            if (_greyQueuePath == null || string.Empty.Equals(_greyQueuePath))
            {
                _greyQueuePath = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_QUEUE_PATH");

                if (_greyQueuePath == null || string.Empty.Equals(_greyQueuePath))
                {
                    _greyQueuePath = QueueProcessor.DEFAULT_SOAP_QUEUE_PATH;
                }
            }

            return _greyQueuePath;
        }

        private static string getSMSQueuePath()
        {
            if (_smsQueuePath == null || string.Empty.Equals(_smsQueuePath))
            {
                _smsQueuePath = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_QUEUE_PATH") + "_SMS";

                if (_smsQueuePath == null || string.Empty.Equals(_smsQueuePath))
                {
                    _smsQueuePath = QueueProcessor.DEFAULT_SMS_QUEUE_PATH;
                }
            }

            return _smsQueuePath;
        }

        private static string getMessmoQueuePath()
        {
            if (_messmoQueuePath == null || string.Empty.Equals(_messmoQueuePath))
            {
                _messmoQueuePath = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_QUEUE_PATH") + "_MESSMO";

                if (_messmoQueuePath == null || string.Empty.Equals(_messmoQueuePath))
                {
                    _messmoQueuePath = QueueProcessor.DEFAULT_MESSMO_QUEUE_PATH;
                }
            }

            return _messmoQueuePath;
        }

        private static string getMessmoDelayedQueuePath()
        {
            if (_messmoDelayedQueuePath == null || string.Empty.Equals(_messmoDelayedQueuePath))
            {
                _messmoDelayedQueuePath = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_QUEUE_PATH") + "_MESSMO_DELAYED";

                if (_messmoDelayedQueuePath == null || string.Empty.Equals(_messmoDelayedQueuePath))
                {
                    _messmoDelayedQueuePath = QueueProcessor.DEFAULT_MESSMO_DELAYED_QUEUE_PATH;
                }
            }

            return _messmoDelayedQueuePath;
        }

        private static string getBatchSMSQueuePath()
        {
            if (_batchSmsQueuePath == null || string.Empty.Equals(_batchSmsQueuePath))
            {
                _batchSmsQueuePath = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_QUEUE_PATH") + "_BATCH_SMS";

                if (_batchSmsQueuePath == null || string.Empty.Equals(_batchSmsQueuePath))
                {
                    _batchSmsQueuePath = QueueProcessor.DEFAULT_BATCH_SMS_QUEUE_PATH;
                }
            }

            return _batchSmsQueuePath;
        }

        private void processGreyCycle()
        {
            try
            {
                TimeSpan ts = new TimeSpan(0, 0, 10);

                int noMessageCount = 0;
                bool successful = false;

                SMTPMail smtpMail = new SMTPMail(100);
                smtpMail.SmtpServer = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_RELAY_HOST");

                while (_runnable)
                {
                    MessageQueueTransaction tran = new MessageQueueTransaction();

                    try
                    {
                        ImpulseBase impulse = null;
                        tran.Begin();

                        try
                        {
                            impulse = _greyQueue.Receive(ts, tran).Body as ImpulseBase;
                            //	INSTRUMENTATION - Impulse received from queue (IN)
                            ExternalMailBL.ReceivedFromQueue.Increment();
                        }
                        catch (MessageQueueException mEx)
                        {
                            if (mEx.Message != "Timeout for the requested operation has expired.")
                            {
                                noMessageCount++;
                                if (noMessageCount >= NO_MESSAGE_DISCONNECT_LIMIT)
                                {
                                    smtpMail.Disconnect();
                                }
                                throw new Exception("Message queue error [queuePath: " + _greyQueuePath + "].", mEx);
                            }
                        }

                        if (impulse != null)
                        {
                            Brand brand = BrandConfigSA.Instance.GetBrandByID(impulse.BrandID);

                            if ((impulse is ViralImpulse || impulse is MatchMailImpulse || impulse is NewMemberMailImpulse))
                            {
                                //								YesMailHandler.Instance.SendEmail(impulse);
                                MailRouter.Instance.SendEmail(impulse);

                                successful = true;
                            }
                            else if (impulse is ContactUsImpulse || impulse is ReportAbuseImpulse || impulse is InternalCorpMail || impulse is MemberQuestionImpulse
                                || impulse is BetaFeedbackImpulse)
                            {
                                // These are the mail types that still go through our SMTP.	

                                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, string.Format("About to send a StormPostEmail with type:{0} brandid: {1} queryString: {2}", impulse.GetType(), impulse.BrandID, impulse.GetQueryString()), null);

                                successful = this.SendStormPostEmail(impulse, smtpMail);
                            }
                            else
                            {
                                var errorMessage = string.Format("Unknown impulse type passed into externalmail(greymail) queue. Only Match,Viral,ContactUs, ReportAbuse, and InternalCorpEmail are allowed. [ImpulseType: {0}, BrandID: {1}]",
                                    impulse.GetType().ToString(), impulse.BrandID);

                                throw new BLException(errorMessage);
                            }

                            if (successful)
                            {
                                tran.Commit();

                                ExternalMailBL.SuccessfulSend.Increment();
                            }
                        }
                        else
                        {
                            Thread.Sleep(SLEEP_TIME);
                        }

                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.IndexOf("does not contain a valid BinaryHeader") > -1)
                        {
                            tran.Commit();
                        }
                        else if (ex.Message.IndexOf("Can not find Yes mail mapping") > -1)
                        {
                            tran.Commit();
                        }
                        else
                        {
                            attemptRollback(tran);
                        }

                        ExternalMailBL.ErrorProcessingQueue.Increment();

                        EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Error processing grey transaction. [QueuePath:" + _greyQueuePath + "]" + ex.ToString(), EventLogEntryType.Warning);

                        Thread.Sleep(SLEEP_TIME_ERROR);
                    }
                }

                smtpMail.Dispose();
            }
            catch (Exception ex)
            {
                ExternalMailBL.UncaughtException.Increment();

                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Uncaught Exception in processGreyCycle(): " + ex.ToString(), EventLogEntryType.Error);

                Thread.Sleep(SLEEP_TIME_ERROR);
            }
        }

        private bool SendStormPostEmail(ImpulseBase impulse, SMTPMail smtpMail)
        {
            MessageInfo messageInfo = Composer.Instance.Compose(impulse);

            if (messageInfo != null)
            {
                if (messageInfo.DoSend)
                {
                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, "messageInfo.DoSend was set to true", null);
                    if (_allowedDomains.IsAllowedDomain(messageInfo.ToAddress))
                    {
                        RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, "Domain was allowed", null);
                        sendEmail(messageInfo, impulse, smtpMail);
                    }
                    else
                    {
                        RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, "Domain was NOT allowed, message was not sent", null);
                    }
                }
                else
                {
                    ExternalMailBL.ComposeFailed.Increment();

                    var message = string.Format("MessageInfo DoSend flag was set to false.  Reason for cancelling send: {0}", messageInfo.CancelMessage);

                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, message, null);
                }

                return true;
            }
            else
            {
                ExternalMailBL.NullMessageInfo.Increment();

                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, "Got a null messageInfo from Composer.", null);
                return false;
            }
        }

        private void processSoapCycle()
        {
            try
            {
                TimeSpan ts = new TimeSpan(0, 0, 10);

                while (_runnable)
                {
                    MessageQueueTransaction tran = new MessageQueueTransaction();

                    try
                    {
                        ImpulseBase impulse = null;

                        tran.Begin();

                        #region Receive impulse
                        try
                        {
                            impulse = _soapQueue.Receive(ts, tran).Body as ImpulseBase;

                            ExternalMailBL.ReceivedFromQueue.Increment();
                        }
                        catch (MessageQueueException mex)
                        {
                            if (mex.Message != "Timeout for the requested operation has expired.")
                            {
                                attemptRollback(tran);

                                throw new Exception("Message queue error. [Error:" + mex.Message + "]", mex);
                            }
                        }
                        #endregion

                        #region Process impulse
                        if (impulse != null)
                        {
                            try
                            {
                                Site site = null;

                                /// HACK: In the cases where an unmatching brandID/siteID combination is passed in. 
                                if (impulse is RenewalFailureNotificationImpulse || impulse is EcardToFriendImpulse || impulse is EcardToMemberImpulse
                                    || impulse is MessageBoardDailyUpdatesImpulse || impulse is MessageBoardInstantNotificationImpulse
                                    || impulse is PmtProfileConfirmationImpulse)
                                {
                                    int siteId = Constants.NULL_INT;

                                    if (impulse is RenewalFailureNotificationImpulse)
                                        siteId = ((RenewalFailureNotificationImpulse)impulse).SiteID;
                                    else if (impulse is EcardToFriendImpulse)
                                        siteId = ((EcardToFriendImpulse)impulse).SiteID;
                                    else if (impulse is EcardToMemberImpulse)
                                        siteId = ((EcardToMemberImpulse)impulse).SiteID;
                                    else if (impulse is MessageBoardDailyUpdatesImpulse)
                                        siteId = ((MessageBoardDailyUpdatesImpulse)impulse).SiteID;
                                    else if (impulse is PmtProfileConfirmationImpulse)
                                        siteId = ((PmtProfileConfirmationImpulse)impulse).SiteID;
                                    else
                                        siteId = ((MessageBoardInstantNotificationImpulse)impulse).SiteID;

                                    Sites coll = BrandConfigSA.Instance.GetSites();

                                    foreach (Site s in coll)
                                    {
                                        if (s.SiteID == siteId)
                                        {
                                            site = s;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    Brand brand = BrandConfigSA.Instance.GetBrandByID(impulse.BrandID);
                                    site = brand.Site;
                                }

                                if (site == null)
                                {
                                    throw new BLException("Site is not found. [BrandID:" + impulse.BrandID.ToString() + ", ImpulseType:" +
                                        impulse.ToString());
                                }

                                //								YesMailHandler.Instance.SendEmail(impulse);
                                MailRouter.Instance.SendEmail(impulse);

                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                attemptRollback(tran);

                                throw new BLException("Error importing/sending soap message. [queuePath: " + _soapQueuePath + "/" + impulse.ToString() + "].\r\nInner Ex: " + ex.StackTrace, ex);
                            }
                        }
                        else
                        {
                            tran.Commit();

                            ExternalMailBL.SuccessfulSend.Increment();

                            Thread.Sleep(SLEEP_TIME);
                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        ExternalMailBL.ErrorProcessingQueue.Increment();

                        EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Error processing soap transaction. [QueuePath: " + _soapQueuePath + "]" + ex.ToString(), EventLogEntryType.Error);

                        Thread.Sleep(SLEEP_TIME_ERROR);
                    }
                    finally
                    {
                        if (tran != null)
                        {
                            tran.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalMailBL.UncaughtException.Increment();

                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Uncaught Exception in processSoapCycle(): " + ex.ToString(), EventLogEntryType.Error);

                Thread.Sleep(SLEEP_TIME_ERROR);
            }
        }


        private void processSMSCycle()
        {
            try
            {
                TimeSpan ts = new TimeSpan(0, 0, 10);

                while (_runnable)
                {
                    MessageQueueTransaction tran = new MessageQueueTransaction();

                    try
                    {
                        ImpulseBase impulse = null;

                        tran.Begin();

                        #region Receive impulse
                        try
                        {
                            impulse = _smsQueue.Receive(ts, tran).Body as ImpulseBase;
                            ExternalMailBL.ReceivedFromQueue.Increment();
                        }
                        catch (MessageQueueException mex)
                        {
                            if (mex.Message != "Timeout for the requested operation has expired.")
                            {
                                attemptRollback(tran);
                                throw new Exception("Message queue error. [Error:" + mex.Message + "]", mex);
                            }
                        }
                        #endregion

                        #region Process impulse
                        if (impulse != null)
                        {
                            try
                            {
                                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, "inside processSMSCycle() - about to send SMS alert", null);

                                //int responseCode = sendSMSAlert(impulse);
                                int responseCode = SMSProviderFactory.GetSMSProvider(((SMSAlertImpulseBase)impulse).SiteID).sendSMSAlert(impulse);

                                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME,
                                    string.Format("inside processSMSCycle() - after send SMS alert ResponseCode: {0}", responseCode), null);

                                if (responseCode != 1)
                                {
                                    // This means during HTTP post, we got a code that indicates some sort of error
                                    // Log and move on with the next queue item
                                    ExternalMailBL.ErrorProcessingQueue.Increment();
                                    EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Error processing SMS transaction. [ResponseCode: " + responseCode.ToString() + "] [QueuePath: " + _smsQueuePath + "]", EventLogEntryType.Error);
                                }

                                // Commit the transaction regardless of the result so we don't get stuck on 1 item
                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                attemptRollback(tran);
                                throw new BLException("Error importing/sending SMS message. [queuePath: " + _smsQueuePath + "/" + impulse.ToString() + "].\r\nInner Ex: " + ex.StackTrace, ex);
                            }
                        }
                        else
                        {
                            // If impulse happens to be null, just commit so we don't get stuck on this impulse
                            tran.Commit();
                            ExternalMailBL.SuccessfulSend.Increment();
                            Thread.Sleep(SLEEP_TIME);
                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        ExternalMailBL.ErrorProcessingQueue.Increment();
                        EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Error processing SMS transaction. [QueuePath: " + _smsQueuePath + "]" + ex.ToString(), EventLogEntryType.Error);
                        Thread.Sleep(SLEEP_TIME_ERROR);
                    }
                    finally
                    {
                        if (tran != null)
                        {
                            tran.Dispose();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ExternalMailBL.UncaughtException.Increment();
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Uncaught Exception in processSMSCycle(): " + ex.ToString(), EventLogEntryType.Error);
                Thread.Sleep(SLEEP_TIME_ERROR);
            }
        }

        private void processMessmoCycle()
        {
            try
            {
                TimeSpan ts = new TimeSpan(0, 0, 10);

                while (_runnable)
                {
                    MessageQueueTransaction tran = new MessageQueueTransaction();

                    try
                    {
                        ImpulseBase impulse = null;

                        tran.Begin();

                        #region Receive impulse
                        try
                        {
                            impulse = _messmoQueue.Receive(ts, tran).Body as ImpulseBase;
                            ExternalMailBL.ReceivedFromQueue.Increment();
                        }
                        catch (MessageQueueException mex)
                        {
                            if (mex.Message != "Timeout for the requested operation has expired.")
                            {
                                attemptRollback(tran);
                                throw new Exception("Message queue error. [Error:" + mex.Message + "]", mex);
                            }
                        }
                        #endregion

                        #region Process impulse
                        if (impulse != null)
                        {
                            try
                            {
                                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME,
                                    "inside processMessmoCycle() - about to execute Messmo command.", null);

                                string errorMsg = Messmo.MessmoCommandMapper.GetMessmoCommand((MessmoImpulseBase)impulse).DoCommand();

                                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME,
                                    string.Format("inside processMessmoCycle() - after Messmo command ErrorMessage: {0}", errorMsg), null);

                                if (errorMsg.ToLower() != "ok")
                                {
                                    // Sometimes when Messmo reports back the errors, they send back the problematic argument with it too.
                                    // When the problematic argument is the actual message, the length of the message can cause this EventLog
                                    // write to cause an exeption.  For this reason, we are going to limit the length of the error message.
                                    string shortErrorMsg = string.Empty;

                                    if (errorMsg.Length > 1000)
                                    {
                                        shortErrorMsg = errorMsg.Substring(0, 1000);
                                    }
                                    else
                                    {
                                        shortErrorMsg = errorMsg;
                                    }

                                    // This means during HTTP post, we got a code that indicates some sort of error
                                    // Log and move on with the next queue item
                                    ExternalMailBL.ErrorProcessingQueue.Increment();
                                    EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Error occured. [" + shortErrorMsg + "] [QueuePath: " + _messmoQueuePath +
                                        "] [ImpulseType: " + impulse.ToString() + "]", EventLogEntryType.Error);
                                }

                                // Commit the transaction regardless of the result so we don't get stuck on 1 item
                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                attemptRollback(tran);
                                throw new BLException("Error executing Messmo command. [QueuePath: " + _messmoQueuePath +
                                    "] [ImpulseType: " + impulse.ToString() + "].\r\nInner Ex: " + ex.StackTrace, ex);
                            }
                        }
                        else
                        {
                            // If impulse happens to be null, just commit so we don't get stuck on this impulse
                            tran.Commit();
                            ExternalMailBL.SuccessfulSend.Increment();
                            Thread.Sleep(SLEEP_TIME);
                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        ExternalMailBL.ErrorProcessingQueue.Increment();
                        EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Error processing Messmo transaction. [QueuePath: " + _messmoQueuePath + "]" + ex.ToString(), EventLogEntryType.Error);
                        Thread.Sleep(SLEEP_TIME_ERROR);
                    }
                    finally
                    {
                        if (tran != null)
                        {
                            tran.Dispose();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ExternalMailBL.UncaughtException.Increment();
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Uncaught Exception in processMessmoCycle(): " + ex.ToString(), EventLogEntryType.Error);
                Thread.Sleep(SLEEP_TIME_ERROR);
            }
        }

        private void processMessmoDelayedCycle()
        {
            try
            {
                TimeSpan ts = new TimeSpan(0, 0, 10);

                while (_runnable)
                {
                    MessageQueueTransaction tran = new MessageQueueTransaction();

                    try
                    {
                        ImpulseBase impulse = null;

                        tran.Begin();

                        #region Receive impulse
                        try
                        {
                            impulse = _messmoDelayedQueue.Receive(ts, tran).Body as ImpulseBase;
                            ExternalMailBL.ReceivedFromQueue.Increment();
                        }
                        catch (MessageQueueException mex)
                        {
                            if (mex.Message != "Timeout for the requested operation has expired.")
                            {
                                attemptRollback(tran);
                                throw new Exception("Message queue error. [Error:" + mex.Message + "]", mex);
                            }
                        }
                        #endregion

                        #region Process impulse
                        if (impulse != null)
                        {
                            try
                            {
                                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME,
                                    "inside processMessmoDelayedCycle() - about to execute Messmo command.", null);

                                string errorMsg = Messmo.MessmoCommandMapper.GetMessmoCommand((MessmoImpulseBase)impulse).DoCommand();

                                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME,
                                    string.Format("inside processMessmoDelayedCycle() - after Messmo command ErrorMessage: {0}.", errorMsg), null);

                                if (errorMsg.ToLower() != "ok")
                                {
                                    // This means during HTTP post, we got a code that indicates some sort of error
                                    // Log and move on with the next queue item
                                    ExternalMailBL.ErrorProcessingQueue.Increment();
                                    EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Error occured. [" + errorMsg + "] [QueuePath: " + _messmoDelayedQueuePath +
                                        "] [ImpulseType: " + impulse.ToString() + "]", EventLogEntryType.Error);
                                }

                                // Commit the transaction regardless of the result so we don't get stuck on 1 item
                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                attemptRollback(tran);
                                throw new BLException("Error executing Messmo command. [QueuePath: " + _messmoDelayedQueuePath +
                                    "] [ImpulseType: " + impulse.ToString() + "].\r\nInner Ex: " + ex.StackTrace, ex);
                            }
                        }
                        else
                        {
                            // If impulse happens to be null, just commit so we don't get stuck on this impulse
                            tran.Commit();
                            ExternalMailBL.SuccessfulSend.Increment();
                            Thread.Sleep(SLEEP_TIME);
                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        ExternalMailBL.ErrorProcessingQueue.Increment();
                        EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Error processing Messmo transaction. [QueuePath: " + _messmoDelayedQueuePath + "]" + ex.ToString(), EventLogEntryType.Error);
                        Thread.Sleep(SLEEP_TIME_ERROR);
                    }
                    finally
                    {
                        if (tran != null)
                        {
                            tran.Dispose();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ExternalMailBL.UncaughtException.Increment();
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Uncaught Exception in processMessmoDelayedCycle(): " + ex.ToString(), EventLogEntryType.Error);
                Thread.Sleep(SLEEP_TIME_ERROR);
            }
        }

        private void processBatchSMSCycle()
        {
            try
            {
                TimeSpan ts = new TimeSpan(0, 0, 10);

                while (_runnable)
                {
                    MessageQueueTransaction tran = new MessageQueueTransaction();

                    try
                    {
                        ImpulseBase impulse = null;

                        tran.Begin();

                        #region Receive impulse
                        try
                        {
                            impulse = _batchSmsQueue.Receive(ts, tran).Body as ImpulseBase;
                            ExternalMailBL.ReceivedFromQueue.Increment();
                        }
                        catch (MessageQueueException mex)
                        {
                            if (mex.Message != "Timeout for the requested operation has expired.")
                            {
                                attemptRollback(tran);
                                throw new Exception("Message queue error. [Error:" + mex.Message + "]", mex);
                            }
                        }
                        #endregion

                        #region Process impulse
                        if (impulse != null)
                        {
                            try
                            {
                                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, "inside processBatchSMSCycle() - about to send SMS alert", null);

                                //int responseCode = sendSMSAlert(impulse);
                                int responseCode = SMSProviderFactory.GetSMSProvider(((SMSAlertImpulseBase)impulse).SiteID).sendSMSAlert(impulse);

                                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME,
                                    string.Format("inside processBatchSMSCycle() - after send SMS alert ResponseCode: {0}", responseCode), null);

                                if (responseCode != 1)
                                {
                                    // This means during HTTP post, we got a code that indicates some sort of error
                                    // Log and move on with the next queue item
                                    ExternalMailBL.ErrorProcessingQueue.Increment();
                                    EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Error processing Batch SMS transaction. [ResponseCode: " + responseCode.ToString() + "] [QueuePath: " + _batchSmsQueuePath + "]", EventLogEntryType.Error);
                                }

                                // Commit the transaction regardless of the result so we don't get stuck on 1 item
                                tran.Commit();
                            }
                            catch (Exception ex)
                            {
                                attemptRollback(tran);
                                throw new BLException("Error importing/sending batch SMS message. [queuePath: " + _batchSmsQueuePath + "/" + impulse.ToString() + "].\r\nInner Ex: " + ex.StackTrace, ex);
                            }
                        }
                        else
                        {
                            // If impulse happens to be null, just commit so we don't get stuck on this impulse
                            tran.Commit();
                            ExternalMailBL.SuccessfulSend.Increment();
                            Thread.Sleep(SLEEP_TIME);
                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        ExternalMailBL.ErrorProcessingQueue.Increment();
                        EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Error processing batch SMS transaction. [QueuePath: " + _batchSmsQueuePath + "]" + ex.ToString(), EventLogEntryType.Error);
                        Thread.Sleep(SLEEP_TIME_ERROR);
                    }
                    finally
                    {
                        if (tran != null)
                        {
                            tran.Dispose();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                ExternalMailBL.UncaughtException.Increment();
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Uncaught Exception in processBatchSMSCycle(): " + ex.ToString(), EventLogEntryType.Error);
                Thread.Sleep(SLEEP_TIME_ERROR);
            }
        }

        private void attemptRollback(MessageQueueTransaction tran)
        {
            try
            {
                if (tran.Status == MessageQueueTransactionStatus.Pending)
                {
                    tran.Abort();
                }
            }
            catch (Exception ex)
            {
                string message = "Exception while trying to abort transaction: " + ex.Message + ", StackTrace: " + ex.StackTrace;
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, message, EventLogEntryType.Warning);
            }
        }

        private void sendEmail(MessageInfo messageInfo, ImpulseBase impulse, SMTPMail smtpMail)
        {
            var brandID = impulse.BrandID;
            var language = EmailMemberHelper.DetermineLanguageFromBrandID(brandID);
            SMTPMessage smtpMessage = null;

            try
            {
                smtpMessage = new SMTPMessage(messageInfo.FromAddress,
                    messageInfo.FromName,
                    messageInfo.ToAddress,
                    messageInfo.ToName,
                    messageInfo.Subject,
                    messageInfo.Body,
                    Constants.NULL_STRING,
                    language);

                foreach (string key in messageInfo.Headers.Keys)
                    smtpMessage.AddHeader(key, (string)messageInfo.Headers[key]);

            }
            catch (Exception ex)
            {
                var loggingInfo = string.Format("Exception while creating SMTPMessage object: {0} with stack trace: {1} Impulse query string that generated this message was: {2}",
                    ex.Message, ex.StackTrace, impulse.GetQueryString());
                RollingFileLogger.Instance.LogError(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, new Exception(loggingInfo), null);
            }

            if (smtpMail.Send(smtpMessage))
            {
                ExternalMailBL.SuccessfulSend.Increment();

                var loggingInfo = string.Format("Successfully sent impulse email: [{0}] with query string: [{1}] and brandId [{2}]",
                        messageInfo.ToString(), impulse.GetQueryString(), brandID);

                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, loggingInfo, null);
            }
            else
            {
                ExternalMailBL.UnsuccessfulSend.Increment();

                var loggingInfo = string.Format("Failed to send Impulse email: [{0}] with query string: [{1}] and brandId [{2}]",
                        messageInfo.ToString(), impulse.GetQueryString(), brandID);

                RollingFileLogger.Instance.LogError(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, new Exception(loggingInfo), null);

                var filename = @"C:\ExternalMailDeadLetter\ExternalMailDeadLetter" + Guid.NewGuid() + ".impulse";

                try
                {
                    //Then intention of this file is unclear, leaving here for now.
                    Stream outStream = File.OpenWrite(filename);
                    var formatter = new BinaryFormatter();
                    formatter.Serialize(outStream, impulse);
                    outStream.Close();

                    var info = string.Format("Unable to send email: [{0}] for impulse with query string: [{1}].  Stored out to file: [{2}]",
                        messageInfo.ToString(), impulse.GetQueryString(), filename);
                    RollingFileLogger.Instance.LogError(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, new Exception(loggingInfo), null);
                }
                catch (Exception ex)
                {
                    var info = string.Format("Unable to send email: [{0}] for impulse with query string: [{1}].  Also unable to store out to file: {2} because of Exception with message:  {3} and stack trace: {4}", messageInfo.ToString(), impulse.GetQueryString(), filename, ex.Message, ex.StackTrace);
                    RollingFileLogger.Instance.LogError(ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, new Exception(info), null);
                }
            }
        }
        #endregion
    }
}
