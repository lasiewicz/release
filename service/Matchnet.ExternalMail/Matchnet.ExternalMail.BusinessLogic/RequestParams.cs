using System;
using System.Collections;
using System.Web;

namespace Matchnet.ExternalMail.BusinessLogic
{
	/// <summary>
	/// Summary description for RequestParams.
	/// </summary>
	public class RequestParams
	{
		private Hashtable _params;
	
		public RequestParams(string queryString)
		{
			_params = new Hashtable();
			ParseParams(queryString);
		}

		public string Get(string key)
		{
			object result = _params[key];
			return((string)result);
		}

		private void ParseParams(string queryString)
		{
			string[] fieldValues = queryString.Split(new char[] { '&' });
			foreach(string keyAndValue in fieldValues)
			{
				string[] splitKeyAndValue = keyAndValue.Split(new char[] { '=' });
				string myKey = splitKeyAndValue[0];
				string myValue = HttpUtility.UrlDecode(splitKeyAndValue[1]);

				_params.Add(myKey, myValue);
			}
		}
	}
}
