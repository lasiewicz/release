using System;
using System.Diagnostics;
using System.Text;
using Matchnet.Exceptions;
using Matchnet.ExternalMail.BusinessLogic.StormPostWebService;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.StormpostSoap;
using Matchnet.EmailLibrary;

namespace Matchnet.ExternalMail.BusinessLogic
{
	/// <summary>
	/// Determins templateIDs and calls import and send SOAP calls.
	/// </summary>
	public class StormPostSoapBL
	{
		#region Singleton

		public static readonly StormPostSoapBL Instance = new StormPostSoapBL();
		private AllowedDomains _allowedDomains;

		private StormPostSoapBL()
		{
			_allowedDomains = new AllowedDomains(ServiceConstants.SERVICE_NAME);
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Calls StormPost server to import updated member information.
		/// </summary>
		/// <param name="template">The import template type to use</param>
		/// <param name="communityID">Community ID</param>
		/// <param name="parameterValues">import token values defined in StormPost</param>
		public void Import(ImportTemplateType template, int communityID, params string[] parameterValues)
		{
			#region Validate Parameters

			if (communityID == Constants.NULL_INT)
			{
				EventLog.WriteEntry("Matchnet.ExternalMail.Service", "CommunityID cannot be " + Constants.NULL_INT + ".", EventLogEntryType.Error);

				return;
			}

			// This should never be hit, but being defensive.
			if (parameterValues == null)
			{
				EventLog.WriteEntry("Matchnet.ExternalMail.Service", "Parameter values cannot be null. [CommunityID:" + communityID.ToString() + "]", EventLogEntryType.Error);

				return;
			}

			#endregion

			int templateID = Constants.NULL_INT;

			templateID = getImportTemplateID(template, communityID);

			try
			{
				long startTime = DateTime.Now.Ticks;

				// Their result message is not consistent. So not evaluating the return value.
				this.getService().doImportFromTemplate(templateID, string.Join("\t", parameterValues));

				#region DEBUG - mesasure process time.

#if DEBUG
				Trace.WriteLine("SOAP Import duration in ms: " + (new DateTime((DateTime.Now.Ticks - startTime)).Millisecond));
#endif

				#endregion

				ExternalMailBL.AvgSOAPImportPerSec.IncrementBy(DateTime.Now.Ticks - startTime);
				ExternalMailBL.AvgSOAPImportPerSecBase.Increment();
			}
			catch (Exception ex)
			{
				#region Exception Handling

				// Possible logical exceptions from Stormpost
				//		
				// "Invalid Import Template ID" -- This should blow up.
				// "Realtime import can process at most 500 lines per request." -- This should blow up.
				//
				// Take no action on above exceptions.

				throw new BLException("SOAP Error sending message to import template. [ImportTemplateID:" + templateID.ToString() + "]", ex);

				#endregion
			}
		}


		/// <summary>
		/// Send an email using an existing send template on StormPost.
		/// </summary>
		/// <param name="template">The send template type to use</param>
		/// <param name="siteID">Site ID</param>
		/// <param name="emailAddress">recipient email address</param>
		/// <param name="parameterValues">token values to be sent. refer to template type for required values.</param>
		public void Send(SendTemplateType template, int siteID, string emailAddress, params string[] parameterValues)
		{
			#region Validate Parameters

			if (siteID == Constants.NULL_INT)
			{
				EventLog.WriteEntry("Matchnet.ExternalMail.Service", "SiteID cannot be " + Constants.NULL_INT + ".", EventLogEntryType.Error);

				return;
			}

			// This should never be hit, but being defensive.
			if (parameterValues == null)
			{
				EventLog.WriteEntry("Matchnet.ExternalMail.Service", "Parameter values cannot be null. [EmailAddress:" + emailAddress + "]", EventLogEntryType.Error);

				return;
			}

			// Email address should never be empty here, since in most cases it's coming from member svc. But it is happening although rarely.
			if ((emailAddress == String.Empty) || (emailAddress == null))
			{
				System.Text.StringBuilder sb = new StringBuilder();

				for (int i = 0; i < parameterValues.Length; i++)
				{
					sb.Append(parameterValues[i] + System.Environment.NewLine);
				}

				EventLog.WriteEntry("Matchnet.ExternalMail.Service", "Email address cannot be empty. [ParameterValues:" + sb.ToString() + "]", EventLogEntryType.Error);

				return;
			}

			#endregion

			int templateID = Constants.NULL_INT;
			string[] substitutionTokens;
			string result = Constants.NULL_STRING;

			templateID = getSendTemplateID(template, siteID);

			#region Construct name value string arrays based on template type

			switch (template)
			{
				case SendTemplateType.SendTemplate_PasswordReminder:
					/// requires 
					/// paramValues[0] = username, parameterValues[1] = password
					//substitutionTokens = new string [] {"NAME=" + parameterValues[0], "SPARKPASSWORD=" + parameterValues[1]};
					substitutionTokens = new string[] {"SPARKPASSWORD=" + parameterValues[0]};
					break;

				case SendTemplateType.SendTemplate_Activation:
					/// requires
					/// paramValues[0] = username, parameterValues[1] = email verification code, parameterValues[2] = password
					//substitutionTokens = new string [] {"NAME=" + parameterValues[0], "VERIFYCODE=" + parameterValues[1] ,"sparkpassword=" + parameterValues[2]};
					substitutionTokens = new string[] {"VERIFYCODE=" + parameterValues[0]};
					break;

				case SendTemplateType.SendTemplate_EmailChangeConfiration:
					substitutionTokens = new string[] {"VERIFYCODE=" + parameterValues[0]};
					break;

				case SendTemplateType.SendTemplate_MessageBoardInstantNotification:
					substitutionTokens = new string[]
						{
							"UserName=" + parameterValues[0],
							"BoardName=" + parameterValues[1],
							"TopicSubject=" + parameterValues[2],
							"ReplyURL=" + parameterValues[3],
							"NewReplies=" + parameterValues[4],
							"UnsubscribeURL=" + parameterValues[5]
						};
					break;

				case SendTemplateType.SendTemplate_MessageBoardDailyUpdates:
					substitutionTokens = new string[]
						{
							"UserName=" + parameterValues[0],
							"BoardName=" + parameterValues[1],
							"TopicSubject=" + parameterValues[2],
							"NewReplies=" + parameterValues[3],
							"BoardID=" + parameterValues[4],
							"ThreadID=" + parameterValues[5],
							"LastMessageID=" + parameterValues[6],
							"UnsubscribeURL=" + parameterValues[7]
						};
					break;

				case SendTemplateType.SendTemplate_EcardSendToMember:
					substitutionTokens = new string[]
						{
							"SenderUserName=" + parameterValues[0],
							"SenderAge=" + parameterValues[1],
							"SenderLocation=" + parameterValues[2],
							"SenderThumbnail=" + parameterValues[3],
							"CardUrl=" + parameterValues[4],
							"CardThumbnail=" + parameterValues[5],
							"RecipientEmailAddress=" + parameterValues[6],
							"RecipientUsername=" + parameterValues[7],
							"SentDate=" + parameterValues[8]
						};
					break;

				case SendTemplateType.SendTemplate_EcardSendToFriend:
					substitutionTokens = new string[]
						{
							"SenderUserName=" + parameterValues[0],
							"SenderAge=" + parameterValues[1],
							"SenderLocation=" + parameterValues[2],
							"SenderThumbnail=" + parameterValues[3],
							"CardUrl=" + parameterValues[4],
							"CardThumbnail=" + parameterValues[5],
							"RecipientEmailAddress=" + parameterValues[6],
							"SentDate=" + parameterValues[7]
						};
					break;

				case SendTemplateType.SendTemplate_RenewalFailureNotification:
					substitutionTokens = new string[]
						{
							"UserName=" + parameterValues[0]
						};
					break;
					
				default:
					substitutionTokens = new string[0] {};
					break;
			}

			#endregion

			try
			{
				long startTime = DateTime.Now.Ticks;

				if (_allowedDomains.IsAllowedDomain(emailAddress))
				{
					// Their result message is not consistent. So not evaluating the return value.
					this.getService().sendMessageFromTemplate(templateID, emailAddress, substitutionTokens);
				}

				#region DEBUG - Measure process time
#if DEBUG
				Trace.WriteLine("SOAP Send duration in ms: " + (new DateTime((DateTime.Now.Ticks - startTime)).Millisecond));
				Trace.WriteLine(result);
#endif

				#endregion

				ExternalMailBL.AvgSOAPSendPerSec.IncrementBy(DateTime.Now.Ticks - startTime);
				ExternalMailBL.AvgSOAPSendPerSecBase.Increment();
			}
			catch (Exception ex)
			{
				#region Exception Handling

				string exceptionMessage = ex.ToString().ToLower();
				System.Text.StringBuilder sb = new StringBuilder();

				// Validation ensures that parameterValues is not null.
				for (int i = 0; i < parameterValues.Length; i++)
				{
					sb.Append(parameterValues[i] + System.Environment.NewLine);
				}

				// Possible logical exceptions from Stormpost
				//
				//	"Couldn't load Send Template with ID X" -- This should blow up.
				//	"User does not have permission for this send template" -- This should blow up.
				//	"Invalid e-mail address" - Handle it.
				//	"Sending to this email address is not permitted because it has a status of (HELD,OPTOUT,etc...)" - Handle it.
				if ((exceptionMessage.IndexOf("invalid") > -1) || (exceptionMessage.IndexOf("not permitted") > -1))
				{
					EventLog.WriteEntry("Matchnet.ExternalMail.Service",
						"Unable to send email. [EmailAddress:" + emailAddress + ", SiteID:" + siteID.ToString() + ", ParameterValues:" + 
						sb.ToString() + ", Descripton:" + exceptionMessage + "]", EventLogEntryType.Warning);
				}
				else
				{
					throw new Exception("SOAP Error sending message to send template. [SendTemplateID:" + templateID.ToString() + 
						", EmailAddress:" + emailAddress + ", SiteID:" + siteID.ToString() + ", ParameterValues:" + sb.ToString() + "]" , ex);	
				}

				#endregion
			}
		}

		#endregion

		#region Private Methods

		private SoapRequestProcessorService getService()
		{
			SoapRequestProcessorService service = new SoapRequestProcessorService();
			service.authenticationValue = new authentication();
			service.authenticationValue.username = "StormpostSOAP@spark.net";
			service.authenticationValue.password = "h4x0r";

			return service;
		}


		/// <summary>
		/// This is based on the information stored on the Stormpost database server.
		/// 
		/// ImportID, SiteID, Title, SiteName, CommunityID
		/// 267	2	API Import Member	Americansingles	1
		/// 268	7	API Import Member	CollegeLuv		12
		/// 269	6	API Import Member	Cupid			10
		/// 270	4	API Import Member	JDate			3
		/// 266	5	API Import Member	FaceLink		9  --> This site is dead, but keeping around for testing
		/// </summary>
		/// <param name="templateType"></param>
		/// <param name="communityID"></param>
		/// <returns></returns>
		private int getImportTemplateID(ImportTemplateType templateType, int communityID)
		{
			int result = Constants.NULL_INT;

			switch (templateType)
			{
				case ImportTemplateType.Minimal:
					{
						switch (communityID)
						{
							case 1:
								result = 267;
								break;
							case 12:
								result = 268;
								break;
							case 10:
								result = 269;
								break;
							case 3:
								result = 270;
								break;
							case 9:
								result = 266;
								break;
							default:
								result = Constants.NULL_INT;
								break;
						}
					}
					break;
				default:
					result = Constants.NULL_INT;
					break;
			}

			if (result == Constants.NULL_INT)
			{
				throw new BLException("Unable to determine import templateID. [ImportTemplateType:" + ((int) templateType).ToString() + ", CommunityID:" + communityID.ToString() + "]");
			}

			return result;
		}


		/// <summary>
		/// This is based on the information stored on the Stormpost database server.
		/// </summary>
		/// <param name="templateType"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		private int getSendTemplateID(SendTemplateType templateType, int siteID)
		{
			int result = Constants.NULL_INT;

			switch (templateType)
			{
					#region PasswordReminder

				case SendTemplateType.SendTemplate_PasswordReminder:
					{
						switch (siteID)
						{
								/* select SendTemplateID, c.title from sendtemplates st join campaigns c on st.campaignid = c.campaignid where c.title like '%forgot%'
							35	Forgot - AU
							48	forgot - dateca
							63	forgot - datecouk
							80	forgot - glimpse
							99	forgot - matchnetde
							117	forgot - americansingles
							130	forgot - jdate
							143	forgot - datingresults
							162	forgot - silversingles
							171	forgot - jdatecoil << this is the right one to use for now.
							195	forgot - jdatecoil << this is aparently a test template Chrish Chenault made for testing
							197	forgot - collegeluv
							199	forgot - cupidcoil
							*/
							case 101:
								result = 117;
								break; //AmericanSingles.com
							case 102:
								result = 80;
								break; //Glimpse.com
							case 103:
								result = 130;
								break; //JDate.com
							case 112:
								result = 197;
								break; //CollegeLuv.com
							case 13:
								result = 48;
								break; //Date.ca
							case 15:
								result = 199;
								break; //Cupid.co.il
							case 1840:
								result = 171;
								break; //JDate.msn.co.il
							case 1934:
								result = 143;
								break; //DatingResults.com
							case 4:
								result = 171;
								break; //JDate.co.il
							case 5:
								result = 99;
								break; //MatchNet.de
							case 6:
								result = 63;
								break; //date.co.uk
							case 7:
								result = 35;
								break; //MatchNet.com.au
							default:
								result = Constants.NULL_INT;
								break;
						}
					}
					break;

					#endregion

					#region Activation

				case SendTemplateType.SendTemplate_Activation:
					switch (siteID)
					{
							/* select SendTemplateID, c.title from sendtemplates st join campaigns c on st.campaignid = c.campaignid where c.title like '%activ%'
						34	Activation - AU
						46	activation - dateca
						61	activation - datecouk
						78	activation - glimpse
						97	activation - matchnetde
						115	activation - americansingles
						128	activation - jdate
						141	activation - datingresults
						160	activation - silversingles
						169	activation - jdatecoil
						200	activation - collegeluv
						207	activation - cupidcoil
						*/
						case 101:
							result = 115;
							break; //AmericanSingles.com
						case 102:
							result = 78;
							break; //Glimpse.com
						case 103:
							result = 128;
							break; //JDate.com
						case 112:
							result = 200;
							break; //CollegeLuv.com
						case 13:
							result = 46;
							break; //Date.ca
						case 15:
							result = 207;
							break; //Cupid.co.il
						case 1840:
							result = 169;
							break; //JDate.msn.co.il
						case 1934:
							result = 141;
							break; //DatingResults.com
						case 4:
							result = 169;
							break; //JDate.co.il
						case 5:
							result = 97;
							break; //MatchNet.de
						case 6:
							result = 61;
							break; //date.co.uk
						case 7:
							result = 34;
							break; //MatchNet.com.au
						default:
							result = Constants.NULL_INT;
							break;
					}
					break;

					#endregion

					#region EmailChangeNotification

				case SendTemplateType.SendTemplate_EmailChangeConfiration:
					switch (siteID)
					{
							/*
					select sendtemplateid, c.title from sendtemplates st  join campaigns c on st.campaignid = c.campaignid where c.title like '%emailchange%'
					
					47	emailchange - dateca
					62	emailchange - datecouk
					79	emailchange - glimpse
					116	emailchange - americansingles
					129	emailchange - jdate
					142	emailchange - datingresults
					170	emailchange - jdatecoil
					193	emailchange - cupidcoil
					196	emailchange - collegeluv
					*/

						case 13:
							result = 47;
							break;
						case 6:
							result = 62;
							break;
						case 102:
							result = 79;
							break;
						case 101:
							result = 116;
							break;
						case 103:
							result = 129;
							break;
						case 1934:
							result = 142;
							break;
						case 4:
							result = 170;
							break;
						case 15:
							result = 193;
							break;
						case 112:
							result = 196;
							break;
					}
					break;

					#endregion

					#region MessageBoardInstantNotification

				case SendTemplateType.SendTemplate_MessageBoardInstantNotification:
					switch (siteID)
					{
							/*
					select sendtemplateid, c.title from sendtemplates st  join campaigns c on st.campaignid = c.campaignid where c.title like '%message board instant%'
					295	message board instant - jdate
					297	message board instant - americansingles
					299	message board instant - dateca
					301	message board instant - datecouk
					303	message board instant - collegeluv
					*/

						case 103:
							result = 295;
							break;
						case 101:
							result = 297;
							break;
						case 13:
							result = 299;
							break;
						case 6:
							result = 301;
							break;
						case 112:
							result = 303;
							break;
					}
					break;

					#endregion

					#region MessageBoardDailyUpdates

				case SendTemplateType.SendTemplate_MessageBoardDailyUpdates:
					switch (siteID)
					{
							/*
					select sendtemplateid, c.title from sendtemplates st  join campaigns c on st.campaignid = c.campaignid where c.title like '%message board da%'
					296	message board daily - jdate
					298	message board daily - americansingles
					300	message board daily - dateca
					302	message board daily - datecouk
					304	message board daily - collegeluv
					*/

						case 103:
							result = 296;
							break;
						case 101:
							result = 298;
							break;
						case 13:
							result = 300;
							break;
						case 6:
							result = 302;
							break;
						case 112:
							result = 304;
							break;
					}
					break;

					#endregion

					#region EcardSendToMember

					case SendTemplateType.SendTemplate_EcardSendToMember:
					switch (siteID)
					{
						/*
						312	member - jdate
						314	member - as
						316	member - uk
						318	member - ca
						*/

						case 103:
							result = 312;
							break;
						case 101:
							result = 314;
							break;
						case 6:
							result = 316;
							break;
						case 13:
							result = 318;
							break;
					}
					break;

					#endregion

					#region EcardSendToFriend

					case SendTemplateType.SendTemplate_EcardSendToFriend:
					switch (siteID)
					{
						/*
						313	member - jdate
						315	member - as
						317	member - uk
						319	member - ca
						*/

						case 103:
							result = 313;
							break;
						case 101:
							result = 315;
							break;
						case 6:
							result = 317;
							break;
						case 13:
							result = 319;
							break;
					}
					break;

					#endregion
					
					#region RenewalFailureNotification
					
				case SendTemplateType.SendTemplate_RenewalFailureNotification:
					switch (siteID)
					{
						case 101:
							result = 305;
							break;
						case 13:
							result = 308;
							break;
						case 6:
							result = 309;
							break;
						case 103:
							result = 311;
							break;
					}
					break;
					
					#endregion

				default:
					result = Constants.NULL_INT;
					break;
			}

			if (result == Constants.NULL_INT)
			{
				throw new BLException("Unable to determine send templateID. [SendTemplateType:" + ((int) templateType).ToString() + ", SiteID:" + siteID.ToString() + "]");
			}

			return result;
		}

		#endregion
	}
}