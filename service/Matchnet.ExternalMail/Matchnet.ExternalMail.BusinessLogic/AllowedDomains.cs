using System;
using System.Diagnostics;

using Matchnet.ExternalMail.ValueObjects;
using Matchnet.Exceptions;

namespace Matchnet.ExternalMail.BusinessLogic
{
	public class AllowedDomains
	{
		private static string[] _allowedDomains = null;

		static AllowedDomains()
		{
			_allowedDomains = getAllowedDomains();

			EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "AllowedDomains:" +  String.Join(",", _allowedDomains));
		}

		private static string[] getAllowedDomains()
		{
			string[] allowedDomainsSplit = null;
			string allowedDomains = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTMAILSVC_ALLOWED_DOMAINS").Trim().ToLower();

			if (allowedDomains.Length > 0)
			{
				allowedDomainsSplit = allowedDomains.Split(',');
			}
			else
			{
				allowedDomainsSplit = new string[0];
			}

			return allowedDomainsSplit;
		}

		public static bool IsAllowedDomain(string emailAddress)
		{
			
			try
			{
				bool domainAllowed = true;
				if (emailAddress == null || emailAddress == String.Empty)
				{
					throw new BLException("Email address cannot be null or empty");
				}


				if (_allowedDomains.Length == 0)
				{
					domainAllowed = true;
				}
				else
				{
					foreach (string domain in _allowedDomains)
					{
						if (emailAddress.ToLower().EndsWith(domain))
						{
							domainAllowed = true;

							break;
						}
						else
						{
							domainAllowed = false;
						}
					}
				}

				if (!domainAllowed)
				{ 										
					EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "Sending to this email address's domain is not allowed. [Email Address:" + emailAddress + "]", EventLogEntryType.Warning);
					ExternalMailBL.ProhibitedDomain.Increment();
				}

				return domainAllowed;
			}
			catch(Exception ex)
			{
				EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, ex.ToString(), EventLogEntryType.Warning);
				return false;
			}
		}
	}
}
