using System;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ValueObjects.Impulse;

namespace Matchnet.ExternalMail.BusinessLogic
{
	public interface ITemplateMapper
	{
		string GetProviderTemplateID(Site site, EmailType emailType);
	}
}
