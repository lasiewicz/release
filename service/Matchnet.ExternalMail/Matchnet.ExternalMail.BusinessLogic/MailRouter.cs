using System;
using Matchnet.ActivityRecording.ServiceAdapters;
using Matchnet.ActivityRecording.ValueObjects.Types;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using System.Diagnostics;
using System.Linq;
using Matchnet.ExternalMail.BusinessLogic.Monitoring;
using Matchnet.Member.ValueObjects.Interfaces;
using Spark.Logging;
using Matchnet.ExternalMail.ValueObjects;
using Spark.Logger;

namespace Matchnet.ExternalMail.BusinessLogic
{
	/// <summary>
	/// Summary description for MailRouter.
	/// </summary>
	public class MailRouter
	{
        private const string CLASS_NAME = "MailRouter";
        private CircuitBreaker _breaker;
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(MailRouter));
		#region DB setting constants
        private const string YESMAIL_100_PERCENT = "YESMAIL_100_PERCENT";
		private const string SUB_CONFIRMATION = "YESMAIL_PERCENT_SUBSCRIPTION_CONFIRMATION";
		private const string FORGOT_PASSWORD = "YESMAIL_PERCENT_FORGOT_PASSWORD";
		private const string MSGBOARD_INSTANCE_NOTIF = "YESMAIL_PERCENT_MESSAGEBOARD_INSTANT_NOTIF";
		private const string MSGBOARD_DAILY_UPDATES = "YESMAIL_PERCENT_MESSAGEBOARD_DAILY_UPDATES";
		private const string ECARD_TO_FRIEND = "YESMAIL_PERCENT_ECARD_TO_FRIEND";
		private const string RENEWAL_FAIL_NOTIF = "YESMAIL_PERCENT_RENEWAL_FAIL_NOTIF";
		private const string MATCH_MAIL = "YESMAIL_PERCENT_MATCH_MAIL";
		private const string CLICK_MAIL = "YESMAIL_PERCENT_CLICK_MAIL";
		private const string ECARD_TO_MEMBER = "YESMAIL_PERCENT_ECARD_TO_MEMBER";
		private const string PAYMENT_PROFILE_NOTIF = "YESMAIL_PERCENT_PAYMENT_PROFILE_NOTIF";
		private const string NEW_MEMBER_MAIL = "YESMAIL_PERCENT_NEW_MEMBER_MAIL";
		private const string COLOR_CODE_QUIZ_INVITE = "YESMAIL_PERCENT_COLOR_CODE_QUIZ_INVITE";
		private const string COLOR_CODE_QUIZ_CONFIRM = "YESMAIL_PERCENT_COLOR_CODE_QUIZ_CONFIRM";
		private const string ONE_OFF_PURCHASE = "YESMAIL_PERCENT_ONE_OFF_PURCHASE";
		private const string JDATE_MOBILE_REGISTRATION = "YESMAIL_PERCENT_JDATE_MOBILE_REG";
        private const string REGISTRATION_CAPTURE_YESMAIL_PERCENT = "YESMAIL_PERCENT_REGISTRATION_CAPTURE";
	    private const string BOGO_NOTIFICATION = "YESMAIL_PERCENT_BOGO_NOTIFICATION";


		#endregion

		#region Singleton implementation (thread-safe)
		// Singleton instance and the lock to be used
		private static volatile MailRouter _instance;
		private static object syncRoot = new object();
		
		// Private ctor for Singleton
		private MailRouter()
		{
            _breaker = new CircuitBreaker();
		}

		// Singleton property accessor
		public static MailRouter Instance
		{
			get
			{
				if(_instance == null)
				{
					lock(syncRoot)
					{
						if(_instance == null)
							_instance = new MailRouter();
					}
				}
				return _instance;
			}
		}
		#endregion

		public void SendEmail(ImpulseBase impulse)
		{
            var tags = new CustomDataProvider(impulse.BrandID).Get();
            Log.LogInfoMessage(string.Format("ClassName:{0}, Entering Method {1}", CLASS_NAME, "SendEmail"), tags);

		    bool sendMailViaMingle = false;
			string settingConstant = Matchnet.Constants.NULL_STRING;
            string emailAddress = Matchnet.Constants.NULL_STRING;
			int targetMemberID = Matchnet.Constants.NULL_INT;
			ActionType actionType = ActionType.None;

			#region Main IF statements to set the variables required
            if(impulse is ImpulseBaseEmail)
            {
                var impulseBaseEmail = (ImpulseBaseEmail) impulse;
                settingConstant = impulseBaseEmail.SettingConstant;
                actionType = impulseBaseEmail.ActionType;
                targetMemberID = impulseBaseEmail.TargetMemberId;
                emailAddress = impulseBaseEmail.EmailAddress;
            }
            else if (impulse is PasswordMailImpulse) 
            {
                settingConstant = FORGOT_PASSWORD;
                actionType = ActionType.SendForgotPassword;
                emailAddress = ((PasswordMailImpulse)impulse).EmailAddress;
                targetMemberID = MemberSA.Instance.GetMemberIDByEmail(emailAddress);
            }
            else if (impulse is EcardToFriendImpulse)
            {
                settingConstant = ECARD_TO_FRIEND;
                actionType = ActionType.SendEcardToFriend;
                emailAddress = ((EcardToFriendImpulse)impulse).RecipientEmailAddress;
            }
            else if (impulse is RenewalFailureNotificationImpulse)
            {
                settingConstant = RENEWAL_FAIL_NOTIF;
                actionType = ActionType.SendRenewalFailureNotification;
                emailAddress = ((RenewalFailureNotificationImpulse)impulse).EmailAddress;
                targetMemberID = MemberSA.Instance.GetMemberIDByEmail(emailAddress);
            }
            else if (impulse is SendMemberColorCodeQuizInviteImpulse)
            {
                settingConstant = COLOR_CODE_QUIZ_INVITE;
                actionType = ActionType.SendMemberColorCodeQuizInvite;
                emailAddress = ((SendMemberColorCodeQuizInviteImpulse)impulse).RecipientEmail;
                targetMemberID = MemberSA.Instance.GetMemberIDByEmail(emailAddress);
            }
            else if(impulse is ScheduledNotificationImpulse)
            {
                if (((ScheduledNotificationImpulse)impulse).ScheduleName.ToLower() == "registration")
                {
                    actionType = ActionType.RegistrationCapture;
                    emailAddress = ((ScheduledNotificationImpulse)impulse).EmailAddress;
                    settingConstant = REGISTRATION_CAPTURE_YESMAIL_PERCENT;
                }
                else if (((ScheduledNotificationImpulse)impulse).ScheduleName.ToLower() == "bogo")
                {
                    actionType = ActionType.SendBOGONotification;
                    emailAddress = ((ScheduledNotificationImpulse)impulse).EmailAddress;
                    settingConstant = BOGO_NOTIFICATION;
                    
                    //transform the impulse into a bogonotificationimpulse, allows both scheduled and real time bogo mailings to be handled 
                    //the same way by downstream components
                    impulse = new BOGONotificationImpulse((ScheduledNotificationImpulse)impulse);
                }
            }
            // why is this if statement here?  if base class is ImpulseBaseEmail, if statement based on the derived type won't hit
            else if (impulse is BOGONotificationImpulse)
            {
                actionType = ActionType.SendBOGONotification;
                emailAddress = ((BOGONotificationImpulse)impulse).EmailAddress;
                settingConstant = BOGO_NOTIFICATION;
            }
            #region commented out if statements because base class was changed to ImpulseBaseEmail
            //else if (impulse is OneOffPurchaseConfirmationImpulse)
            //{
            //    settingConstant = ONE_OFF_PURCHASE;
            //    actionType = ActionType.SendOneOffPurchaseConfirmation;
            //    targetMemberID = ((OneOffPurchaseConfirmationImpulse)impulse).MemberID;
            //}
            //else if (impulse is JDateMobileRegistrationConfirmationImpulse)
            //{
            //    settingConstant = JDATE_MOBILE_REGISTRATION;
            //    actionType = ActionType.SendJDateMobileRegistrationConfirmation;
            //    targetMemberID = ((JDateMobileRegistrationConfirmationImpulse)impulse).MemberID;
            //}
            //else if (impulse is SubscriptionConfirmationImpulse) 
            //{
            //    settingConstant = SUB_CONFIRMATION;
            //    actionType = ActionType.SendSubscriptionConfirmation;
            //    targetMemberID = ((SubscriptionConfirmationImpulse)impulse).MemberID;
            //}
            //else if (impulse is ColorCodeQuizConfirmationImpulse)
            //{
            //    settingConstant = COLOR_CODE_QUIZ_CONFIRM;
            //    actionType = ActionType.SendColorCodeQuizConfirmation;
            //    targetMemberID = ((ColorCodeQuizConfirmationImpulse)impulse).MemberID;
            //}
            //else if (impulse is MatchMailImpulse)
            //{
            //    settingConstant = MATCH_MAIL;
            //    actionType = ActionType.SendMatchMail;
            //    targetMemberID = ((MatchMailImpulse)impulse).RecipientMiniprofileInfo.MemberID;
            //}
            //else if (impulse is ViralImpulse)
            //{
            //    settingConstant = CLICK_MAIL;
            //    actionType = ActionType.SendClickMail;
            //    targetMemberID = ((ViralImpulse)impulse).RecipientMiniprofileInfo.MemberID;
            //}
            //else if (impulse is EcardToMemberImpulse)
            //{
            //    settingConstant = ECARD_TO_MEMBER;
            //    actionType = ActionType.SendEcardToMember;
            //    targetMemberID = ((EcardToMemberImpulse)impulse).RecipientMemberID;
            //}
            //else if (impulse is PmtProfileConfirmationImpulse)
            //{
            //    settingConstant = PAYMENT_PROFILE_NOTIF;
            //    actionType = ActionType.SendPaymentProfileNotification;
            //    targetMemberID = ((PmtProfileConfirmationImpulse)impulse).MemberID;
            //}
            //else if (impulse is NewMemberMailImpulse)
            //{
            //    settingConstant = NEW_MEMBER_MAIL;
            //    actionType = ActionType.SendNewMemberMail;
            //    targetMemberID = ((NewMemberMailImpulse)impulse).RecipientMiniprofileInfo.MemberID;
            //}
            //else if (impulse is MessageBoardInstantNotificationImpulse)
            //{
            //    settingConstant = MSGBOARD_INSTANCE_NOTIF;
            //    actionType = ActionType.SendMessageBoardInstantNotification;
            //    targetMemberID = ((MessageBoardInstantNotificationImpulse)impulse).MemberID;
            //}
            //else if (impulse is MessageBoardDailyUpdatesImpulse)
            //{
            //    settingConstant = MSGBOARD_DAILY_UPDATES;
            //    actionType = ActionType.SendMessageBoardDailyUpdates;
            //    targetMemberID = ((MessageBoardDailyUpdatesImpulse)impulse).MemberID;
            //}
            #endregion
            #endregion

            if ( (emailAddress == Matchnet.Constants.NULL_STRING || emailAddress == string.Empty) && targetMemberID != Matchnet.Constants.NULL_INT)
			{
                IMemberDTO member = MailHandlerUtils.GetIMemberDTO(targetMemberID, MemberLoadFlags.None);
				
				if(member != null)
					emailAddress = member.EmailAddress;
			}

		    tags["Email"] = emailAddress;

			// what's the YesMailHandler percentage for this type of email
			int communityID = Constants.NULL_INT;
			int siteID = Constants.NULL_INT;
			
			Brand brand = BrandConfigSA.Instance.GetBrandByID(impulse.BrandID);

			if(brand == null)
			{
				RenewalFailureNotificationImpulse renewalImpulse = (RenewalFailureNotificationImpulse)impulse;

				Sites oSites = BrandConfigSA.Instance.GetSites();
				foreach(Site oSite in oSites)
				{
					if(renewalImpulse.SiteID == oSite.SiteID)
					{
						communityID = oSite.Community.CommunityID;
						siteID = oSite.SiteID;
						break;
					}
				}
			}
			else
			{
				communityID = brand.Site.Community.CommunityID;
				siteID = brand.Site.SiteID;
			}

		    var percentageSetting = RuntimeSettings.GetSetting(settingConstant, communityID, siteID);

		    bool legacySettingValue = false;

		    int yesMailPercent;
		    if (int.TryParse(percentageSetting, out yesMailPercent))
		        legacySettingValue = true;
            
			IMailHandler mailHandler;

            var mailerType = MailerType.YesMail;

		    if (legacySettingValue)
		    {
		        // let's try to route based on the last digit of memberID first
		        if (impulse.MemberIDForRandomSelection != Constants.NULL_INT)
		        {
		            int lastDigitMemberID = impulse.MemberIDForRandomSelection%10;
		            sendMailViaMingle = lastDigitMemberID >= (yesMailPercent/10);
		        }
		        else
		        {
		            // ah man... only way now is to use this crappo pseudo random stuff

		            // get the random number 1-100
		            Random rand = new Random();
		            int randInt = rand.Next(1, 100);

		            // get the right IMailHandler
		            sendMailViaMingle = randInt > yesMailPercent;
		        }
                if(sendMailViaMingle)
                    mailerType = MailerType.MingleMailer;
            }
		    else
		    {
		        var percentages = percentageSetting.Split(',').Select(x => double.Parse(x.Trim())).ToList();
		        if (percentages.Count == 2)
		        {
		            var rand = new Random();
                    var val = 0.00001 + rand.NextDouble()*100.0;
		            var yesMailPercentage = percentages[0];
                    var maropostPercentage = percentages[1];
                    if(val >= yesMailPercentage && val < maropostPercentage + yesMailPercentage)
                        mailerType = MailerType.Maropost;
                    if(val >= maropostPercentage + yesMailPercentage)
                        mailerType = MailerType.MingleMailer;
                }
		    }

		    var mailHandlerType = mailerType.ToString();
            
            Log.LogInfoMessage(
                string.Format("About to call SendMail on the IMailHandler SettingConstant: {0}, EmailAddress: {1}, TargetMemberID: {2}, YesMailPercent: {3}, IMailHandlerType: {4}",
                settingConstant,
                emailAddress,
                targetMemberID,
                yesMailPercent,
                mailHandlerType),
                tags);

		    switch(mailerType)
            {
                case MailerType.YesMail:
                {
                    CircuitBreaker.SendMailDelegate smd =
                        (impulseBase => YesMailHandler.Instance.SendEmail(impulseBase));
                    _breaker.Intercept(smd, impulse);
                        break;
                }
                case MailerType.MingleMailer:
                    MingleMailerHandler.Instance.SendEmail(impulse);
                    break;
                case MailerType.Maropost:
                {
                    CircuitBreaker.SendMailDelegate smd =
                        (impulseBase => MaropostHandler.Instance.SendEmail(impulseBase));
                    _breaker.Intercept(smd, impulse);
                    break;
                }
            }
            
            try
            {
                //don't log if sendmember, we have more detailed logging for that impulse downstream
                if (!(impulse is SendMemberImpulse))
                {
                    // Log the activity
                    // we need to put this in its own try catch so that the queue item doesn't rollback the transaction causing it to
                    // process the same queue item over and over again
                    ActivityRecordingSA.Instance.RecordActivity(Matchnet.Constants.NULL_INT, targetMemberID, siteID,
                                                                actionType, CallingSystem.ExternalMail, emailAddress);
                }
            }
            catch (Exception ex)
            {
                Log.LogError("SendMail threw an exception.", ex, tags);
            }

            Log.LogInfoMessage(string.Format("ClassName:{0}, Leaving Method {1}", CLASS_NAME, "SendEmail"), tags);
		
		}

		
	}
}
