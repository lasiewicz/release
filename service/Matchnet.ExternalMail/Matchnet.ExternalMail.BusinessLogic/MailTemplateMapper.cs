using System;
using System.IO;
using System.Xml.XPath;
using System.Reflection;
using System.Resources;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ValueObjects.Impulse;

namespace Matchnet.ExternalMail.BusinessLogic
{	
	public class MailTemplateMapper: ITemplateMapper
	{
		private XPathDocument _doc;
		private Object _syncBlock = new object();
		
		public MailTemplateMapper(string configFile)
		{
		    _configFile = configFile;
			this.LoadDocument(configFile);
		}

		#region ITemplateMapper Members

	    private string _configFile;
		public string GetProviderTemplateID(Site site, EmailType emailType)
		{	
			XPathNavigator nav = _doc.CreateNavigator();
			XPathNodeIterator iterator = nav.Select(nav.Compile(string.Format("mapping/item[siteID='{0}' and emailType='{1}']/masterID", site.SiteID, emailType.ToString())));

			if(iterator.MoveNext())
				return iterator.Current.Value;
			else
				throw new Exception("Can not find mail mapping. Site name: " + site.Name + 
															" | Site id: " + site.SiteID.ToString() + 
																" | Email type name: " + emailType.ToString() + 
																	" | Email type id: " + ((int)emailType).ToString());
		}

        public int GetAccountID(int siteId, EmailType emailType)
        {
            XPathNavigator nav = _doc.CreateNavigator();
            XPathNodeIterator iterator = nav.Select(nav.Compile(string.Format("mapping/item[siteID='{0}' and emailType='{1}']/accountID", siteId, emailType.ToString())));

            if (iterator.MoveNext())
                return int.Parse(iterator.Current.Value);
            else
                throw new Exception("Can not find mail mapping.  " + 
                                                            " Site id: " + siteId.ToString() +
                                                                " | Email type name: " + emailType.ToString() +
                                                                    " | Email type id: " + ((int)emailType).ToString());
        }	
		

		public string GetEmailType(Site site, string masterId)
		{
			XPathNavigator nav = _doc.CreateNavigator();
			XPathNodeIterator iterator = nav.Select(nav.Compile(string.Format("mapping/item[siteID='{0}' and masterID='{1}']/emailType", site.SiteID, masterId)));
			if(iterator.MoveNext())
				return iterator.Current.Value;
			else
				throw new Exception("Email type not found for SiteID: " + site.SiteID.ToString() + " MasterID: " + masterId);
		}

		#endregion

		private void LoadDocument(string configFile)
		{
			if(_doc == null)
			{
				lock(_syncBlock)
				{
					if(_doc == null)
					{
						Assembly asm = Assembly.GetExecutingAssembly();
						string[] resNames = Assembly.GetExecutingAssembly().GetManifestResourceNames();
						for(int i = 0; i < resNames.Length; i++)
						{
							if(resNames[i].IndexOf(configFile) > -1)
							{
								Stream stream = asm.GetManifestResourceStream(resNames[i]);
								_doc = new XPathDocument(stream);
								stream.Close();
							}
						}
					}
				}
			}
		}
	}
}
