﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.EmailLibrary;
using Matchnet.Exceptions;
using Matchnet.ExternalMail.BusinessLogic.Monitoring;
using Matchnet.ExternalMail.BusinessLogic.QueueProcessing;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Serializers;
using Spark.Logger;
using Spark.Logging;

namespace Matchnet.ExternalMail.BusinessLogic
{
    public class MaropostBL: IMailBL
    {
        private enum ReturnCode
        {
            UnhandledError = 0,
            Success = 1,
            DatabaseDown = 10,
            SiteIdNotAvailable = 20,
            MemberIdNotAvailable = 30,
            MasterIdNotAvailable = 40,
            RecipientEmailNotAvailable = 50,
            TokenNotAvailable = 60,
            SaxParseException = 70
        }

        public MaropostBL()
        {
            _allowedDomains = new AllowedDomains(ServiceConstants.SERVICE_NAME);
            _mailMapper = new MailTemplateMapper("MaropostMailMapping.xml");
            _siteMapper = new SiteMapper("MaropostAccountMapping.xml");
        }

        private MailTemplateMapper _mailMapper;
        private SiteMapper _siteMapper;
        private const string CLASS_NAME = "MaropostBL";
        private AllowedDomains _allowedDomains;
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(MaropostBL));
        public static MaropostBL Instance  = new MaropostBL();
        public void SendTriggerMail(int memberId, int siteId, int brandId, string masterId, string recipientEmail, string[] token, string[] customFields,
            EmailType emailType)
        {
            var tags = new CustomDataProvider(brandId, memberId,recipientEmail).Get();
            Log.LogInfoMessage(string.Format("ClassName:{0}, Entering Method {1}", CLASS_NAME, "SendTriggerMail"), tags);
            int returnCode = 0;
            bool allowedDomain = false;
            try
            {
                allowedDomain = _allowedDomains.IsAllowedDomain(recipientEmail);
            }
            catch (Exception ex)
            {
                returnCode = (int)ReturnCode.RecipientEmailNotAvailable;
            }
            if (allowedDomain)
            {
                try
                {
                    returnCode = SendTriggerMailImpl(memberId, siteId, brandId, masterId, recipientEmail, token, customFields, emailType);
                }
                catch (Exception ex)
                {
                    if (ex.ToString().IndexOf("SAXParseException") > -1)
                    {
                        Log.LogError("SAXParseException",ex,tags);
                        returnCode = (int) ReturnCode.SaxParseException;
                    }
                    else
                    {
                        throw;
                    }
                }
                this.ProcessReturnCode(returnCode, siteId, brandId, memberId, masterId, recipientEmail, emailType);
            }
            else
            {
                this.ProcessReturnCode(returnCode, siteId, brandId, memberId, masterId, recipientEmail, emailType);

            }

            Log.LogInfoMessage(string.Format("ClassName:{0}, Leaving Method {1}", CLASS_NAME, "SendTriggerMail"), tags);

        }

        int SendTriggerMailImpl(int memberId, int siteId, int brandId, string masterId, string recipientEmail, string[] token, string[] customFields,
            EmailType emailType)
        {
            long startTime = DateTime.Now.Ticks;
            var accountId = _mailMapper.GetAccountID(siteId, emailType);
            var tags = new CustomDataProvider(brandId,memberId,recipientEmail).Get();
            Log.LogInfoMessage(string.Format("About to call Maropost MemberID:{0}, SiteID: {1}, MasterID: {2}, RecipientEmail: {3}, Tags: {4}, Fields: {5}, EmailType: {6}", memberId, siteId, masterId, recipientEmail, token != null ? string.Join(",", token) : "", customFields != null ? string.Join(",", customFields) : "", emailType),
                tags);
            var returnCode = SendTriggerEmail(memberId, siteId, brandId, masterId, recipientEmail, token, customFields, accountId);

            Log.LogInfoMessage(string.Format("Return code from Maropost: {0}", returnCode.ToString()), tags);
#if DEBUG
            if (false)
            {
#endif
                ExternalMailBL.AvgSOAPSendPerSec.IncrementBy(DateTime.Now.Ticks - startTime);
                ExternalMailBL.AvgSOAPSendPerSecBase.Increment();
#if DEBUG
            }
#endif
            return returnCode;
        }

        class Contact
        {
            public string email { get; set; }
            public Dictionary<string, string> custom_field { get; set; }
        }
        class TransactionalMailRequest
        {
            public int campaign_id { get; set; }
            public Contact contact { get; set; }
            public Dictionary<string, string> tags { get; set; }
        }


        private int SendTriggerEmail(int memberId, int siteId, int brandId, string masterId, string recepientEmail, string[] token, string[] customFields, int? accountId)
        {
            try
            {
                var rootUrl = RuntimeSettings.GetSetting("MAROPOST_API_ROOT");
                var accessToken = _siteMapper.GetAccessToken(siteId);
                RestClient client = new RestClient(rootUrl);
                var request =
                    new RestRequest(string.Format("/accounts/{0}/emails/deliver.json?auth_token={1}", accountId.Value,
                        accessToken));
                request.RequestFormat = DataFormat.Json;
                var tags = new Dictionary<string, string>();
                var customFieldsMap = new Dictionary<string, string>();

                foreach (var tag in token)
                {
                    var re = new Regex( @"([^=]+)=(.+)");
	                Match match = re.Match(tag);
	                if (match.Success)
	                {
	                    if (!tags.ContainsKey(match.Groups[1].Value))
                            tags[match.Groups[1].Value] = match.Groups[2].Value;
	                }
                }
                if (customFields != null)
                    foreach (var field in customFields)
                    {
                        var re = new Regex(@"([^=]+)=(.+)");
                        Match match = re.Match(field);
                        if (match.Success)
                        {
                            if (!customFieldsMap.ContainsKey(match.Groups[1].Value))
                                customFieldsMap[match.Groups[1].Value] = match.Groups[2].Value;
                        }
                    }

                var body = new TransactionalMailRequest()
                {
                    campaign_id = int.Parse(masterId),
                    contact = new Contact() {email = recepientEmail, custom_field = customFieldsMap},
                    tags = tags
                };

                request.AddBody(body);
                var response = client.Post(request);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    var bodyStr = JsonConvert.SerializeObject(body);
                    var requestString = string.Format("{0} {1}{2}\n\t{3}", request.Method, rootUrl,request.Resource, bodyStr);
                    throw new ServiceTransportException((int)response.StatusCode, string.Format("Maropost returned HTTP code {0}, content {1}\n\t{2}", response.StatusCode, response.Content, requestString),null);
                }
                return 1;
            }
            catch (Exception e)
            {
                // No logging. Otherwise 2 entries will be in the log. !!!
                //Log.LogError("Exception in SendTriggerEmail",e,new CustomDataProvider(brandId,memberId,recepientEmail).Get());
                throw;
            }
        }


        private void ProcessReturnCode(int code, int siteID, int brandId, int memberId, string masterID, string email, EmailType emailType)
        {
            string logMessage = "";
            ReturnCode retCode;
            int emailTypeID = Constants.NULL_INT;

            #region Param Validation

            if (email == null)
            {
                // We're only logging at this point. Assign emtpy value so this method doesn't blow up.
                email = String.Empty;
            }

            try
            {
                emailTypeID = (int)emailType;
            }
            catch (Exception ex)
            {
                throw new BLException("Unknown email type.", ex);
            }

            try
            {
                retCode = (ReturnCode)code;
            }
            catch (Exception ex)
            {
                throw new BLException("Unknown Maropost return code.", ex);
            }

            #endregion

            if (retCode != ReturnCode.Success)
            {
                if (retCode == ReturnCode.DatabaseDown)
                {
                    logMessage = "Maropost database down.";
                }
                else if (retCode == ReturnCode.SiteIdNotAvailable)
                {
                    logMessage = "SiteID not available when calling Maropost web service.";
                }
                else if (retCode == ReturnCode.MemberIdNotAvailable)
                {
                    logMessage = "MemberID not available when calling Maropost web service.";
                }
                else if (retCode == ReturnCode.MasterIdNotAvailable)
                {
                    logMessage = "MasterId not available when calling Maropost web service.";
                }
                else if (retCode == ReturnCode.RecipientEmailNotAvailable)
                {
                    logMessage = "Recipient Email not available when calling Maropost web service.";
                }
                else if (retCode == ReturnCode.UnhandledError)
                {
                    logMessage = "Unhandled error occurred from Maropost web servcie.";
                }
                else if (retCode == ReturnCode.TokenNotAvailable)
                {
                    logMessage = "Token not available when calling Maropost web service.";
                }
                else if (retCode == ReturnCode.SaxParseException)
                {
                    logMessage = "SaxParseException.";
                }

                logMessage += "\r\n";
                logMessage += "Site ID: " + siteID.ToString() + "\r\n";
                logMessage += "Member ID: " + memberId.ToString() + "\r\n";
                logMessage += "Master ID: " + masterID.ToString() + "\r\n";
                logMessage += "EmailType ID: " + emailTypeID.ToString() + "\r\n";
                logMessage += "Recipient Email: " + email.ToString();
                Log.LogWarningMessage(logMessage, new CustomDataProvider(brandId, memberId, email).Get());
            }
        }
    }
}
