﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.ExternalMail.BusinessLogic
{
    public class MaropostHandler
    {
        static MailHandler _instance = new MailHandler("MaropostMailMapping.xml",MaropostBL.Instance);

        public static MailHandler Instance
        {
            get { return _instance; }
        }
    }
}
