using System;
using System.Collections;
using System.Text;

namespace Matchnet.ExternalMail.BusinessLogic
{
	public class SMTPMessage
	{
		private string _fromAddress;
		private string _toAddress;
		private string _subject;
		private string _bodyText;
		private string _bodyHTML;
		private Hashtable _headers;
		private string _boundary = null;
		private MailFormatType _mailFormat = MailFormatType.Text;
		private Language _language;

		public enum MailFormatType
		{
			Text,
			HTML
		}

		public SMTPMessage(string fromAddress,
			string fromName,
			string toAddress,
			string toName,
			string subject,
			string bodyText,
			string bodyHTML,
			Language language)
		{
			if(toAddress == null)
			{
				throw new Exception("toAddress cannot be null");
			} 
			else if(string.Empty.Equals(toAddress))
			{
				throw new Exception("toAddress cannot be blank");
			}

			_headers = new Hashtable();
			_fromAddress = fromAddress;
			_toAddress = toAddress;
			_subject = subject;
			_bodyText = bodyText;
			_bodyHTML = bodyHTML;
			_language = language;

			if (_bodyHTML != null && _bodyHTML.Length > 0)
			{
				_mailFormat = MailFormatType.HTML;
			}

			AddHeader("From", _fromAddress);
			AddHeader("To", _toAddress);
			AddHeader("Date", DateTime.Now.ToString("ddd, d MMM yyyy HH:mm:ss zz") + "00");
			AddHeader("Subject", _subject);

			setContentType();
		}

		private void setContentType()
		{
			string contentType = string.Empty;

			switch (_mailFormat) 
			{
				case MailFormatType.Text:
					switch(Language)
					{
						case Language.English:
							contentType = "text/plain; charset=windows-1252";
							AddHeader("MIME-Version", "1.0; charset=windows-1252");
							break;
						case Language.German:
                        case Language.French:
							contentType = "text/plain; charset=iso-8859-1";
							AddHeader("MIME-Version", "1.0; charset=iso-8859-1");
							break;
						case Language.Hebrew:
							contentType = "text/plain; charset=windows-1255";
							// contentType = "text/plain; charset=UTF-8";
							AddHeader("MIME-Version", "1.0; charset=windows-1255");
							// AddHeader("MIME-Version", "1.0; charset=UTF-8");
							break;
					}
					break;
				case MailFormatType.HTML:
					contentType = "multipart/alternative;\r\n\tboundary=\"" + getTopBoundary() + "\"";
					AddHeader("MIME-Version", "1.0");
					break;
			}

			AddHeader("Content-Type", contentType);

			switch(Language)
			{
				case Language.English:
					AddHeader("Content-Transfer-Encoding", "7bit");
					break;
				case Language.German:
                case Language.French:
					AddHeader("Content-Transfer-Encoding", "8bit");
					break;
				case Language.Hebrew:
					AddHeader("Content-Transfer-Encoding", "8bit");
					break;
				default:
					AddHeader("Content-Transfer-Encoding", "7bit");
					break;
			}
		}

		public void AddHeader(string name, string val)
		{
			if (!_headers.ContainsKey(name))
			{
				_headers.Add(name, val);
			}
			else
			{
				_headers[name] = val;
			}
		}

		public string GetData()
		{
			StringBuilder sb = new StringBuilder();

			foreach (string key in _headers.Keys)
			{
				sb.Append(key + ": " + _headers[key] + "\r\n");
			}

			if (_mailFormat == MailFormatType.HTML)
			{
				sb.Append("This is a multi-part message in MIME format.\r\n");
			}

			sb.Append("\r\n");

			if (_mailFormat != MailFormatType.HTML)
			{
				sb.Append(_bodyText);
			}
			else
			{
				sb.Append(getBoundary());
				sb.Append("\r\n");
				sb.Append("Content-Type: text/plain; charset=ISO-8859-1\r\n\r\n");
				sb.Append(_bodyText);
				sb.Append("\r\n");

				sb.Append(getBoundary());
				sb.Append("\r\n");
				sb.Append("Content-Type: text/html; charset=ISO-8859-1\r\n\r\n");
				sb.Append(_bodyHTML);
				sb.Append("\r\n");
			}

			sb.Append("\r\n.\r\n");
			return sb.ToString();
		}

		private string getTopBoundary() 
		{
			if (_boundary == null) 
			{
				_boundary = "----=_NextPart_000_0001_01C2DBD0.33885B50";
			}
			return _boundary;
		}

		private string getBoundary() 
		{
			if (_boundary == null) 
			{
				_boundary = "----=_NextPart_000_0001_01C2DBD0.33885B50";
			}
			return "--" + _boundary;
		}

		private string getBottomBoundary() 
		{
			if (_boundary == null) 
			{
				_boundary = "----=_NextPart_000_0001_01C2DBD0.33885B50";
			}
			return "--" + _boundary + "--";
		}

		#region public properties

		public Language Language
		{
			get
			{
				return(_language);
			}
		}

		public string FromAddress
		{
			get
			{
				return _fromAddress;
			}
		}

		public string ToAddress
		{
			get
			{
				return _toAddress;
			}
		}

		public string Subject
		{
			get
			{
				return _subject;
			}
		}

		public string BodyText
		{
			get
			{
				return _bodyText;
			}
		}

		public string BodyHTML
		{
			get
			{
				return _bodyHTML;
			}
		}

		#endregion
	}
}
