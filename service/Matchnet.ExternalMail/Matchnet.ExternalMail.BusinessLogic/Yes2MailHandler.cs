﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.ExternalMail.BusinessLogic
{
    public class YesMailHandler
    {
        static MailHandler _instance = new MailHandler("YesMailMapping.xml",YesMailBL.Instance);

        public static MailHandler Instance
        {
            get { return _instance; }
        }
    }
}
