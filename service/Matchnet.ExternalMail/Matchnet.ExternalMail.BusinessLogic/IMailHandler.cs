using System;
using Matchnet.ExternalMail.ValueObjects.Impulse;

namespace Matchnet.ExternalMail.BusinessLogic
{
	/// <summary>
	/// Summary description for IMailHandler.
	/// </summary>
	public interface IMailHandler
	{

		void SendEmail(ImpulseBase impulse);

//		void SendPhotoApproval(ImpulseBase impulse);
//		void SendToFriend(ImpulseBase impulse);
//		void SendNewMailAlert(ImpulseBase impulse);
//		void SendMutualMail(ImpulseBase impulse);
//		void SendPhotoRejection(ImpulseBase impulse);
//		void SendOptOutNotification(ImpulseBase impulse);
//		void SendHotListAlert(ImpulseBase impulse);
//		void SendSubscriptionConfirmation(ImpulseBase impulse);
//		void SendActivationLetter(ImpulseBase impulse);
//		void SendForgotPassword(ImpulseBase impulse);	
//		void SendEmailChangeConfirmation(ImpulseBase impulse);
//		void SendMessageBoardInstantNotification(ImpulseBase impulse);
//		void SendMessageBoardDailyUpdates(ImpulseBase impulse);
//		void SendEcardToFriend(ImpulseBase impulse);
//		void SendRenewalFailureNotification(ImpulseBase impulse);
//		void SendMatchMail(ImpulseBase impulse);
//		void SendClickMail(ImpulseBase impulse);
//		void SendEcardToMember(ImpulseBase impulse);
//		void sendPaymentProfileNotification(ImpulseBase impulse);
//		void sendMatchMeterInvitationMailImpulse(ImpulseBase impulse);
//		void sendMatchMeterCongratsMailImpulse(ImpulseBase impulse);
//		void SendNewMemberMail(ImpulseBase impulse);
//		void SendPhotoCaptionRejectedMail(ImpulseBase impulse);
//		void SendMemberColorCode(ImpulseBase impulse);
//		void SendMemberColorCodeQuizInvite(ImpulseBase impulse);
//		void SendColorCodeQuizConfirmation(ImpulseBase impulse);
//		void SendMemberQuestion(ImpulseBase impulse);
//		void SendOneOffPurchaseConfirmation(ImpulseBase impulse);
//		void SendJDateMobileRegistrationConfirmation(ImpulseBase impulse);
		
	}
}
