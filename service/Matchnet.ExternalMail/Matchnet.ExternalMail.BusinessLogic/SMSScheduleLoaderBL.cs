using System;
using System.Data;
using System.Messaging;
using System.Threading;
using System.Diagnostics;
using System.Text;

using Matchnet.Data;
using Matchnet.Exceptions;

using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.Impulse;


namespace Matchnet.ExternalMail.BusinessLogic
{
	/// <summary>
	/// Summary description for SMSScheduleLoaderBL.
	/// </summary>
	public class SMSScheduleLoaderBL
	{
		public SMSScheduleLoaderBL()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		
		/// <summary>
		/// maximum number of items in an MSMQ queue, to avoid over loading
		/// </summary>
		public const int MAX_ITEMS_IN_QUEUE = 1024 * 16; 
		public const int ITEMS_PER_QUEUE_CYCLE = 32;

		public static readonly SMSScheduleLoaderBL Instance = new SMSScheduleLoaderBL();
		private static DataTable _ScheduleTable;
		//private static DataTable _SearchPreferenceTable;
		//private static string [] _SearchPreferenceColumnNameList = new string [] {"searchorderby","searchtypeid","gendermask","minage","maxage","regionid","schoolid","areacode1","areacode2","areacode3","areacode4","areacode5","areacode6","distance","hasphotoflag","educationlevel","religion","languagemask","ethnicity","smokinghabits","drinkinghabits","minheight","maxheight","maritalstatus","jdatereligion","jdateethnicity","synagogueattendance","keepkosher","sexualidentitytype","relationshipmask","relationshipstatus","bodytype","zodiac","majortype","countryregionid"};
		private static bool _Runnable = false;

		string _cfgQueuePaths = string.Empty;
		private static string [] _QueuePaths;
		private static int _QueuePathSpinIndex = 0;
		private static DateTime _LastSetQueuePathTime = DateTime.Now;
		private static PerformanceCounter [] _perfItemsInQueue;

		public bool Runnable
		{
			get { return _Runnable; }
			set { _Runnable = value; }
		}


		/// <summary>
		/// Start loading queue with items
		/// </summary>
		public void Run()
		{
			_Runnable = true;

			while (_Runnable) 
			{
				SetQueuePaths(); // in case config changed, this call is cheap.

				bool QueueIsTooFull = false;
				for (int i = 0; i < _perfItemsInQueue.Length && !QueueIsTooFull; i++)
				{
					QueueIsTooFull = _perfItemsInQueue[i].NextValue() >= MAX_ITEMS_IN_QUEUE;
					System.Diagnostics.Trace.WriteLine("__" + ServiceConstants.SERVICE_NAME + ": " + _perfItemsInQueue[i].InstanceName + " msg count " + _perfItemsInQueue[i].NextValue().ToString() );

				}

				if (QueueIsTooFull)
				{
					System.Diagnostics.Trace.WriteLine("__" + ServiceConstants.SERVICE_NAME + ": Snore. Queue too full.");
					Thread.Sleep(20000); // sleep because queue has enough items for now.
				}
				else 
				{
					LoadBatchFromDB();
					if ((_ScheduleTable != null && _ScheduleTable.Rows.Count == 0) || _ScheduleTable == null)
					{
						System.Diagnostics.Trace.WriteLine("__" + ServiceConstants.SERVICE_NAME + ": Snore. DB call returned no results");
						Thread.Sleep(10000); // sleep because last scoop brought back nothing.
					}
					else 
					{
						ImpulseBaseQueueItems items = ExtractQueueItems();
						DistributeQueueItems(items);
						_ScheduleTable = null;
						Thread.Sleep(10000);
					}
				}			
			}
		}


		/// <summary>
		/// Stop this instance
		/// </summary>
		public void Stop()
		{
			_Runnable = false;
		}

		/// <summary>
		/// Loads runtime config to determine which queues configured to get matchmail work.
		/// </summary>
		private void SetQueuePaths()
		{
			try 
			{
				string strQueuePaths = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EXTERNALMAILSVC_QUEUE_PATH");
	
				_LastSetQueuePathTime = DateTime.Now;
				if (strQueuePaths != _cfgQueuePaths) 
				{
					_cfgQueuePaths = strQueuePaths;
					_QueuePaths = strQueuePaths.Split(',');

					_perfItemsInQueue = new PerformanceCounter[_QueuePaths.Length];
					for (int i = 0; i < _QueuePaths.Length; i++) 
					{
						string instance = _QueuePaths[i];
						instance = instance.Replace("FormatName:DIRECT=OS:",string.Empty);
						string host = instance.Substring(0,instance.IndexOf("\\"));
						if (host == ".") 
						{ 
							host = System.Environment.MachineName.ToLower(); 
							instance = instance.Replace(".",host);
						}
						_perfItemsInQueue[i] = new PerformanceCounter("MSMQ Queue","Messages in Queue",instance,host);
						System.Diagnostics.Trace.WriteLine("__" + ServiceConstants.SERVICE_NAME + " starting perf counter " + instance + " has : " + _perfItemsInQueue[i].NextValue().ToString()); 
					}
				}
			}
			catch (Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "SetQueuePaths() failed. Can't figure out where to send queue items, or can't connect counter!", ex,false);
			}
			if (_QueuePaths.Length == 0) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "SetQueuePaths() failed. Got 0 queue paths.", null,false);
			}
		}

		/// <summary>
		/// Gets next queue path in round robin fashion.
		/// </summary>
		/// <returns>The next queue path in the queue path list</returns>
		private string GetNextQueuePath()
		{
			if ( DateTime.Now.Subtract(_LastSetQueuePathTime).TotalSeconds > 20)
			{
				SetQueuePaths();
			}
			_QueuePathSpinIndex = ++_QueuePathSpinIndex % _QueuePaths.Length;
			return _QueuePaths[_QueuePathSpinIndex]; 
		}


		private ImpulseBaseQueueItems ExtractQueueItems()
		{
			ImpulseBaseQueueItems items = new ImpulseBaseQueueItems();
			ImpulseBase queuItem; 
			if (_ScheduleTable != null) 
			{
				for (int i = 0; i < _ScheduleTable.Rows.Count; i++)
				{
					queuItem = GetQueueItem(_ScheduleTable.Rows[i]);
					items.Add(queuItem);
					// Update the items in the tables
					Command updtCommand = new Command("mnXmail", "up_SMSAlerts_UpdateItems", 0);
					updtCommand.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, Convert.ToInt32(_ScheduleTable.Rows[i]["MemberID"]));
					updtCommand.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, Convert.ToInt32(_ScheduleTable.Rows[i]["SiteID"]));
					updtCommand.AddParameter("@EmailCount", SqlDbType.Int, ParameterDirection.Input, 0);
					updtCommand.AddParameter("@EcardCount", SqlDbType.Int, ParameterDirection.Input, 0);
					updtCommand.AddParameter("@FlirtCount", SqlDbType.Int, ParameterDirection.Input, 0);
					updtCommand.AddParameter("@SentCount", SqlDbType.Int, ParameterDirection.Input, 0);
					updtCommand.AddParameter("@LastAttemptDate", SqlDbType.SmallDateTime, ParameterDirection.Input, Convert.ToDateTime(_ScheduleTable.Rows[i]["LastAttemptDate"]));
						
					Client.Instance.ExecuteAsyncWrite(updtCommand);

				}
			}
			return items;
		}


		public void DistributeQueueItems(ImpulseBaseQueueItems items)
		{	
			bool sentSuccess = false;
			for (int i = 0;  i < items.Count; i+= ITEMS_PER_QUEUE_CYCLE)
			{
				//				queuItem = items[i];
				for (int attempt = 0; attempt < 3; attempt++)
				{
					sentSuccess = EnqueueExternalMailItem(items, i, ITEMS_PER_QUEUE_CYCLE );
					if (sentSuccess) { break; }
					Thread.Sleep(10000);
				}
				if (!sentSuccess) 
				{
					new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,"Can't enqueue item after repeated attempts. Giving up!",null,false);
					new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,"Last Batch: " + items.Count.ToString() + " total, " + i.ToString() + " sent,"  + (items.Count - i).ToString() + " abandoned." ,null,false);
					_Runnable = false;
				}
			}
			
		}

	/*
		public bool EnqueueExternalMailItem(MatchMailQueueItem item)
		{
			MessageQueue mq;
			
			try 
			{
				mq = new MessageQueue(GetNextQueuePath());
				mq.Formatter = new BinaryMessageFormatter();
				using (MessageQueueTransaction tran = new MessageQueueTransaction())
				{
					tran.Begin();
					mq.Send(item,item.MemberID.ToString() + ":" + item.CommunityID.ToString(),tran);
					tran.Commit();
				}
				return true;
			}
			catch (Exception ex) 
			{
				new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, 
					"EnqueueMatchmailItem failed " +  item.MemberID.ToString() + ":" + item.CommunityID.ToString(),
					ex,false);
				return false;
			}			
		}
*/
	
		public bool EnqueueExternalMailItem(ImpulseBaseQueueItems items, int startAt, int count)
		{
			MessageQueue mq;
			
			try 
			{
				mq = new MessageQueue(GetNextQueuePath());
				mq.Formatter = new BinaryMessageFormatter();
				using (MessageQueueTransaction tran = new MessageQueueTransaction())
				{
					tran.Begin();
					for (int i = startAt; i < Math.Min(items.Count, (startAt + count)); i++)
					{
						mq.Send(items[i], tran);
					}
					tran.Commit();
				}
				return true;
			}
			catch (Exception ex) 
			{
			/*	StringBuilder sb = new StringBuilder(items.Count * 10);
				for (int i = 0; i < items.Count; i++) 
				{ 
					sb.Append(items[0]...);
					sb.Append(',');
				}
				*/

				new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, 
					"EnqueueExternalMailItem failed.\n",
					ex,false);
				return false;
			}			
		}

   
		
		public ImpulseBase GetQueueItem(DataRow row)
		{
			/******************
					Column_name			Type			Length	Nullable
					MemberID			int				4		no
					SiteID				int				4		no
					InsertDate			smalldatetime   1		no
				*	LastAttemptDate		smalldatetime	4		yes
				*	PhoneNumber			nvarchar		50		no
					EmailCount			int				4		yes
					EcardCount			int				4		yes
				*	FlirtCount			int				4		yes
					BrandID				int				4		no

			******************/

		
			Int32 memberID; 
			Int32 siteID;
			string phoneNumber;
			Int32 emailCount = 0;
			Int32 ecardCount = 0;
			Int32 flirtCount = 0;
			Int32 brandID;
			
			memberID = Convert.ToInt32(row["MemberID"]); 
			siteID = Convert.ToInt32(row["SiteID"]);
			phoneNumber = Convert.ToString(row["PhoneNumber"]);
			if (row["EmailCount"] != DBNull.Value) emailCount = Convert.ToInt32(row["EmailCount"]);
			if (row["EcardCount"] != DBNull.Value) ecardCount = Convert.ToInt32(row["EcardCount"]);
			if (row["FlirtCount"] != DBNull.Value) flirtCount = Convert.ToInt32(row["FlirtCount"]);
			brandID = Convert.ToInt32(row["BrandID"]); 
			
			SummarySMSAlertImpulse item = new SummarySMSAlertImpulse(
				brandID,
				siteID,
				memberID,
				phoneNumber,
				emailCount,
				ecardCount, 
				flirtCount
				);

			return item;
		}

		private void LoadBatchFromDB()
		{
			try 
			{
				Command command = new Command("mnXmail", "up_SMSSchedule_List_Send", 0);
				DataSet ds = Matchnet.Data.Client.Instance.ExecuteDataSet(command);

				_ScheduleTable = ds.Tables[0];
			}
			catch (Exception ex) 
			{
				new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "LoadBatchFromDB() failed.", ex,false);
			}
		}
	}
}
