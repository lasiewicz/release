using System;
using System.Collections.Specialized;

namespace Matchnet.ExternalMail.BusinessLogic
{	
	public class TokenConstructor
	{
		private StringCollection _collection;

		public TokenConstructor()
		{
			_collection = new StringCollection();
		}

		public void Add(string tokenName, string tokenValue)
		{
			_collection.Add(tokenName + "=" + tokenValue);
		}

		public string[] BuildList()
		{
			string[] retVal = new string[_collection.Count];
			_collection.CopyTo(retVal, 0);
			return retVal;
		}
	}
}
