
using System;

namespace Matchnet.ExternalMail.BusinessLogic
{
    public class TokenNames
    {
        public const string COMMENT = "Comment";
        public const string CARD_URL = "CardURL";
        public const string BOARD_ID = "BoardID";
        public const string LAST_4CC = "Last4CC";
        public const string REPLY_URL = "ReplyURL";
        public const string PLAN_TYPE = "PlanType";
        public const string LAST_NAME = "LastName";
        public const string SENT_DATE = "SentDate";
        public const string THREAD_ID = "ThreadID";
        public const string USER_NAME = "UserName";
        public const string BOARD_NAME = "BoardName";
        public const string EMAIL_ADDR = "EmailAddr";
        public const string FIRST_NAME = "FirstName";
        public const string RENEW_COST = "RenewCost";
        public const string SENDER_AGE = "SenderAge";
        public const string VERIFY_CODE = "VerifyCode";
        public const string NEW_REPLIES = "NewReplies";
        public const string INITIAL_COST = "InitialCost";
        public const string NEW_MAIL_TIME = "NewMailTime";
        public const string S2F_MEMBER_ID = "S2FMemberID";
        public const string TOPIC_SUBJECT = "TopicSubject";
        public const string USER_NAME_TEXT = "UserNameText";
        public const string MEMBER_MAIL_ID = "MemberMailID";
        public const string RENEW_DURATION = "RenewDuration";
        public const string CARD_THUMBNAIL = "CardThumbnail";
        public const string SPARK_PASSWORD = "SparkPassword";
        public const string LAST_MESSAGE_ID = "LastMessageID";
        public const string FROM_MEMBER_AGE = "FromMemberAge";
        public const string SENDER_LOCATION = "SenderLocation";
        public const string UNSUBSCRIBE_URL = "UnsubscribeURL";
        public const string S2F_FRIENDS_NAME = "S2FFriendsName";
        public const string SENDER_USER_NAME = "SenderUsername";
        public const string MUTUAL_MEMBER_ID = "MutualMemberID";
        public const string DATE_OF_PURCHASE = "DateOfPurchase";
        public const string FROM_MEMBER_NAME = "FromMemberName";
        public const string INITIAL_DURATION = "InitialDuration";
        public const string SENDER_THUMBNAIL = "SenderThumbnail";
        public const string MUTUAL_MEMBER_AGE = "MutualMemberAge";
        public const string SEND_TO_FRIEND_BODY = "SendToFriendBody";
        public const string FROM_MEMBER_SEEKING = "FromMemberSeeking";
        public const string NUM_UNREAD_MESSAGES = "NumUnreadMessages";
        public const string RENEW_DURATION_TYPE = "RenewDurationType";
        public const string RECIPIENT_USER_NAME = "RecipientUsername";
        public const string CONFIRMATION_NUMBER = "ConfirmationNumber";
        public const string CREDIT_CARD_TYPE_ID = "CreditCardTypeID";
        public const string MOBILE_NUMBER = "ConfirmationNumber";
        public const string FROM_MEMBER_LOCATION = "FromMemberLocation";
        public const string FROM_MEMBER_THUMBNAIL = "FromMemberThumbnail";
        public const string INITIAL_DURATION_TYPE = "InitialDurationType";
        public const string MUTUAL_MEMBER_ABOUT_ME = "MutualMemberAboutMe";
        public const string MUTUAL_MEMBER_LOCATION = "MutualMemberLocation";
        public const string MUTUAL_MEMBER_NAME_TEXT = "MutualMemberNameText";
        public const string MUTUAL_MEMBER_USER_NAME = "MutualMemberUserName";
        public const string MUTUAL_MEMBER_THUMBNAIL = "MutualMemberThumbnail";
        public const string HOT_LISTED_BY_MEMBER_AGE = "HotListedByMemberAge";
        public const string RECIPIENT_EMAIL_ADDRESS = "RecipientEmailAddress";
        public const string HOT_LISTED_BY_MEMBER_SEEKING = "HotListedByMemberSeeking";
        public const string HOT_LISTED_BY_MEMBER_LOCATION = "HotListedByMemberLocation";
        public const string HOT_LISTED_BY_MEMBER_THUMBNAIL = "HotListedByMemberThumbnail";
        public const string HOT_LISTED_BY_MEMBER_USER_NAME = "HotListedByMemberUserName";
        public const string SITEID = "SiteID";
        public const string REASON = "Reason";
        public const string SODB_MEMBERID = "memberID";
        public const string SODB_EMAIL = "eMail";
        public const string SODB_USERID = "userid";
        public const string MINGLE_SODB_SITEID = "SiteId";
        public const string SODB_SITEID = "siteID";
        public const string ResetUrl = "reset_url";
        public const string Fields = "Fields";
    }

}
