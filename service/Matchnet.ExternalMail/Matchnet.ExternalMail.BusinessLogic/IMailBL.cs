﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.ExternalMail.ValueObjects.Impulse;

namespace Matchnet.ExternalMail.BusinessLogic
{
    public interface IMailBL
    {
        void SendTriggerMail(int memberId, int siteId, int brandId, string masterId, string recipientEmail, string[] token, string[] customFields, EmailType emailType);
    }
}
