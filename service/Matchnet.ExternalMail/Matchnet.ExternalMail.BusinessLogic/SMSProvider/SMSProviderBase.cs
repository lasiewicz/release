using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

using Matchnet.HTTPMessaging;
using Matchnet.HTTPMessaging.RequestTypes;
using Matchnet.HTTPMessaging.ResponseTypes;

using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.Data;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using MSA = Matchnet.Member.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.Caching;


namespace Matchnet.ExternalMail.BusinessLogic.SMSProvider
{
	/// <summary>
	/// Summary description for SMSProviderBase.
	/// </summary>
	public class SMSProviderBase
	{
		private const string XMAIL_DATABASE_NAME = "mnXmail";
		private Cache _cache;

		private enum SMSAlertTypeID
		{
			Unknown = 0,
			NewEmail = 1,
			Hotlist = 2,
			NewFlirt = 3,
			Clicked = 4,
			Ecard = 5,
			Validation = 6,
			Summary = 7,
			Authentication = 8,
            RegistraionSuccess = 9 
		}


		public SMSProviderBase()
		{
			_cache = Cache.Instance;
		}


		protected void LogSMSSend(int siteID, int memberID, int targetMemberID, string smsNumber, int smsMessageID, int responseCode)
		{
			try
			{
				Command command = new Command(XMAIL_DATABASE_NAME, "up_SMSLog_Save", 0);
				command.AddParameter("@SiteID", SqlDbType.Int, System.Data.ParameterDirection.Input, siteID);
				command.AddParameter("@MemberID", SqlDbType.Int, System.Data.ParameterDirection.Input, memberID);
				command.AddParameter("@TargetMemberID", SqlDbType.Int, System.Data.ParameterDirection.Input, targetMemberID);
				command.AddParameter("@SMSNumber", SqlDbType.NVarChar, System.Data.ParameterDirection.Input, smsNumber);
				command.AddParameter("@SMSMessageID", SqlDbType.Int, System.Data.ParameterDirection.Input, smsMessageID);
				command.AddParameter("@ResponseCodeID", SqlDbType.Int, System.Data.ParameterDirection.Input, responseCode);
				Client.Instance.ExecuteAsyncWrite(command);
			}
			catch(Exception ex)
			{
				string myMessage = "Unable to log a SMS send with the following values: " + "[SiteID=" + siteID.ToString() + ",MemberID=" + memberID.ToString() +
					",TargetMemberID=" + targetMemberID.ToString() + ",SMSNumber=" + smsNumber + ",SMSMessageID=" + smsMessageID.ToString() +
					",ResponseCode=" + responseCode.ToString() + "]" + "Exception message: " + ex.Message;
				
				EventLog.WriteEntry(Matchnet.ExternalMail.ValueObjects.ServiceConstants.SERVICE_NAME, myMessage, EventLogEntryType.Warning);
			}
		}

		protected int GetSMSMessageID(SMSAlertImpulseBase smsImpulse)
		{
			int smsMessageID = Matchnet.Constants.NULL_INT;
			// Determine the SMS alert type ID
			SMSAlertTypeID typeID = SMSAlertTypeID.Unknown;

			if(smsImpulse is NewEmailSMSAlertImpulse)
				typeID = SMSAlertTypeID.NewEmail;
			else if(smsImpulse is HotListedSMSAlertImpulse)
				typeID = SMSAlertTypeID.Hotlist;
			else if(smsImpulse is NewFlirtSMSAlertImpulse)
				typeID = SMSAlertTypeID.NewFlirt;
			else if(smsImpulse is ClickedSMSAlertImpulse)
				typeID = SMSAlertTypeID.Clicked;
			else if(smsImpulse is NewECardSMSAlertImpulse)
				typeID = SMSAlertTypeID.Ecard;
			else if(smsImpulse is ValidationSMSAlertImpulse)
				typeID = SMSAlertTypeID.Validation;
			else if(smsImpulse is SummarySMSAlertImpulse)
				typeID = SMSAlertTypeID.Summary;
			else if(smsImpulse is AuthCodeSMSAlertImpulse)
				typeID = SMSAlertTypeID.Authentication;
            else if (smsImpulse is RegistrationSuccessSMSAlertImpulse)
                typeID = SMSAlertTypeID.RegistraionSuccess;

			if(typeID != SMSAlertTypeID.Unknown)
			{
				Command command = new Command(XMAIL_DATABASE_NAME, "up_SMSMessage_List", 0);
				command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, smsImpulse.SiteID);
				command.AddParameter("@SMSAlertTypeID", SqlDbType.Int, ParameterDirection.Input, (int)typeID);

				SqlDataReader reader = null;
				try
				{
					reader = Client.Instance.ExecuteReader(command);

					if(reader.Read())
					{
						smsMessageID = Convert.ToInt32(reader["SMSMessageID"]);
					}
				}
				finally
				{
					if(reader != null && !reader.IsClosed)
						reader.Close();
				}
				
			}

			return smsMessageID;
		}

		
		public virtual int sendSMSAlert(ImpulseBase impulse)
		{
			int responseCode = -1;
			
			if(impulse is ValidationSMSAlertImpulse)
			{
				ValidationSMSAlertImpulse valImpulse = (ValidationSMSAlertImpulse)impulse;
				
				// For this, sending and receiving are the same
				LogSMSSend(valImpulse.SiteID, valImpulse.SendingMemberID, valImpulse.ReceivingMemberID, valImpulse.TargetPhoneNumber,
					GetSMSMessageID(valImpulse), -1);

				responseCode = 1;
			}
			else
			{
				// Extract out information from the impulse object
				SMSAlertImpulseBase smsImpulse = (SMSAlertImpulseBase)impulse;
				
				int siteID = smsImpulse.SiteID;
				int memberID = smsImpulse.SendingMemberID;
				int targetMemberID = smsImpulse.ReceivingMemberID;
				string targetPhoneNumber = smsImpulse.TargetPhoneNumber;

				// Get the alert message to send
				int smsMessageID = -1;
				Brand brand = BrandConfigSA.Instance.GetBrandByID(smsImpulse.BrandID);
				string messsage = GetAlertBodyText(smsImpulse, ref smsMessageID, brand);

				// Get all the runtime settings for sending a SMS message
				string clientId = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("4INFO_CLIENT_ID",
					brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
				string clientKey = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("4INFO_CLIENT_KEY",
					brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
				string postUrl = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("4INFO_POST_URL",
					brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);

				try
				{
					// Send the message
					SMSMessageRequest req = new SMSMessageRequest(messsage, clientId, clientKey);
					req.PhoneNumber = "+1" + targetPhoneNumber;

					string result = SMSProxy.SendPost(req, postUrl);
					SMSMessageResponse resp = SMSParser.ParseMessage(result);

					responseCode = (int)resp.Status;

					// Log the send of this SMS alert
					LogSMSSend(siteID, memberID, targetMemberID, smsImpulse.TargetPhoneNumber, smsMessageID, (int)resp.Status);
				}
				catch(Exception ex)
				{
					string myMessage = "Unable to send SMS for impulse with query string: [" + smsImpulse.GetQueryString() + "] because of Exception with message: " + ex.Message + " and stack track: " + ex.StackTrace;
					EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, myMessage, EventLogEntryType.Warning);
					responseCode = -1;
				}
			}

			return responseCode;
		}


		protected string GetAlertBodyText(SMSAlertImpulseBase smsImpulse, ref int smsMessageID, Brand brand)
		{
			string retVal = string.Empty;
									
			// Determine the SMS alert type ID
			SMSAlertTypeID typeID = SMSAlertTypeID.Unknown;

			if(smsImpulse is NewEmailSMSAlertImpulse)
				typeID = SMSAlertTypeID.NewEmail;
			else if(smsImpulse is HotListedSMSAlertImpulse)
				typeID = SMSAlertTypeID.Hotlist;
			else if(smsImpulse is NewFlirtSMSAlertImpulse)
				typeID = SMSAlertTypeID.NewFlirt;
			else if(smsImpulse is ClickedSMSAlertImpulse)
				typeID = SMSAlertTypeID.Clicked;
			else if(smsImpulse is NewECardSMSAlertImpulse)
				typeID = SMSAlertTypeID.Ecard;
			else if(smsImpulse is SummarySMSAlertImpulse)
				typeID = SMSAlertTypeID.Summary;
			else if(smsImpulse is AuthCodeSMSAlertImpulse)
				typeID = SMSAlertTypeID.Authentication;
            else if (smsImpulse is RegistrationSuccessSMSAlertImpulse)
                typeID = SMSAlertTypeID.RegistraionSuccess;
			

			if(typeID != SMSAlertTypeID.Unknown)
			{
				CacheableSMSMessageDesc cachedSMSDesc = _cache.Get(CacheableSMSMessageDesc.GetCacheKey(smsImpulse.SiteID, (int)typeID)) as CacheableSMSMessageDesc;
				
				if(cachedSMSDesc==null)
				{
					Command command = new Command(XMAIL_DATABASE_NAME, "up_SMSMessage_List", 0);
					command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, smsImpulse.SiteID);
					command.AddParameter("@SMSAlertTypeID", SqlDbType.Int, ParameterDirection.Input, (int)typeID);

					SqlDataReader reader = null;
					try
					{
						reader = Client.Instance.ExecuteReader(command);

						if(reader.Read())
						{
							cachedSMSDesc = new CacheableSMSMessageDesc(smsImpulse.SiteID, (int)typeID, reader["SMSMessageDesc"].ToString(), Convert.ToInt32(reader["SMSMessageID"]));
							_cache.Add(cachedSMSDesc);
						}
					}
					finally
					{
						if(reader != null && !reader.IsClosed)
							reader.Close();
					}

				}
				retVal = cachedSMSDesc.SMSMessageDesc;
				smsMessageID = cachedSMSDesc.SMSMessageID;
			}

			// Do token replacement before passing it back
			if(retVal != string.Empty)
				retVal = ReplaceTokens(smsImpulse.SendingMemberID, retVal, brand);

			return retVal;
		}

		string ReplaceTokens(int memberID, string messageStr, Brand brand)
		{
			// TODO: Modify this later to at least support all keywords that match
			// the member attributes' names
			try
			{
                IMemberDTO member = MailHandlerUtils.GetIMemberDTO(memberID, MSA.MemberLoadFlags.None);
			
				messageStr =  messageStr.Replace("<UserName>", member.GetUserName(brand));
			}
			catch{}
			return messageStr;
		}

		[Serializable]
		private class CacheableSMSMessageDesc : ICacheable
		{
			private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
			private int _cacheTTLSeconds = 60 * 60 * 3; // Three hours
			public const string CACHE_KEY = "SMSMSGDESC_";

			private int _SiteID;
			private int _SMSAlertTypeID;
			private string _SMSMessageDesc;
			private int _SMSMessageID;

			public int SiteID
			{
				get { return _SiteID; }
				set { _SiteID = value; }
			}
			public int SMSAlertTypeID
			{
				get { return _SMSAlertTypeID; }
				set { _SMSAlertTypeID = value; }
			}
			public string SMSMessageDesc
			{
				get { return _SMSMessageDesc; }
				set { _SMSMessageDesc = value; }
			}
			public int SMSMessageID
			{
				get { return _SMSMessageID; }
				set { _SMSMessageID = value; }
			}

			public CacheItemMode CacheMode
			{
				get
				{
					return Matchnet.CacheItemMode.Absolute;
				}
			}

			public CacheItemPriorityLevel CachePriority
			{
				get
				{
					return _cachePriority;
				}
				set
				{
					_cachePriority = value;
				}
			}

			public int CacheTTLSeconds
			{
				get
				{
					return _cacheTTLSeconds;
				}
				set
				{
					_cacheTTLSeconds = value;
				}
			}

			public CacheableSMSMessageDesc(int siteID, int sMSAlertTypeID, string sMSMessageDesc, int sMSMessageID)
			{
				this._SiteID = siteID;
				this.SMSAlertTypeID = sMSAlertTypeID;
				this.SMSMessageDesc = sMSMessageDesc;
				this.SMSMessageID = sMSMessageID;
			}

			public static string GetCacheKey(int siteID, int sMSAlertTypeID)
			{
				return CACHE_KEY  + siteID.ToString() + "_" + sMSAlertTypeID.ToString()  ;
			}
			public string GetCacheKey()
			{
				return GetCacheKey(_SiteID, _SMSAlertTypeID);
			}
		}
	}
}
