using System;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;
namespace Matchnet.ExternalMail.BusinessLogic.SMSProvider
{
	/// <summary>
	/// Summary description for SMSProviderFactory.
	/// </summary>
	public class SMSProviderFactory
	{
		public enum SITE_ID : int
		{
			JDateCoIL = 4,
			MatchnetUK = 6,
			DateCA = 13,
			Cupid = 15,
			Spark = 100,
			AmericanSingles = 101,
			Glimpse = 102,
			JDate = 103,
			JDateFR = 105,
			JDateUK = 107,
			Matchnet = 108,
			CollegeLuv = 112,
			JewishMingle = 9171
		}
		public SMSProviderFactory()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public static SMSProviderBase GetSMSProvider(int siteID)
		{
			SMSProviderBase smsProvider = new SMSProviderBase();
			Sites sites=BrandConfigSA.Instance.GetSites();
			int communityid=-1;
			foreach(Site s in sites)
			{
				if(s.SiteID==siteID)
				{
					communityid=s.Community.CommunityID;
					break;

				}
			}
			if(communityid <0)
			{
				throw new BLException("Cannot find SMS provider for the Site ID :"+ siteID.ToString());
			}

			bool fourInfoFlag=Conversion.CBool(RuntimeSettings.GetSetting("4INFO_ENABLED",communityid,siteID));
			bool teleclalFlag=Conversion.CBool(RuntimeSettings.GetSetting("TELECLAL_ENABLED",communityid,siteID));

			if(fourInfoFlag)
				smsProvider = new FourInfoSMSProvider();
			else if(teleclalFlag)
			{
				smsProvider = new TeleClalSMSProvider();
			}
			else
			{
			throw new BLException("No SMS provider for the Site ID :"+ siteID.ToString());
			}
			return smsProvider;
		}
	}
}
