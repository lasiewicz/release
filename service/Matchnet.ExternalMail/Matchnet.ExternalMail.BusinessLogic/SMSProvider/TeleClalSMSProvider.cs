using System;
using System.Diagnostics;
using System.Text;
using Matchnet.HTTPMessaging;
using Matchnet.HTTPMessaging.RequestTypes;
using Matchnet.HTTPMessaging.ResponseTypes;

using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.ExternalMail.ValueObjects;

using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;

namespace Matchnet.ExternalMail.BusinessLogic.SMSProvider
{
    /// <summary>
    /// Summary description for TeleClalSMSProvider.
    /// </summary>
    public class TeleClalSMSProvider : SMSProviderBase
    {
        public TeleClalSMSProvider()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public override int sendSMSAlert(ImpulseBase impulse)
        {
            int responseCode = -1;
            bool sendMessage = true;

            // Extract out information from the impulse object
            SMSAlertImpulseBase smsImpulse = (SMSAlertImpulseBase)impulse;

            int siteID = smsImpulse.SiteID;
            int brandID = smsImpulse.BrandID;
            int targetMemberID = smsImpulse.SendingMemberID;
            int memberID = smsImpulse.ReceivingMemberID;
            string targetPhoneNumber = smsImpulse.TargetPhoneNumber;
            Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);

            // Check if a message is allowed to be sent on Saturday. AuthCode and RegSuccess messages will be sent anyway
            if (SMSAlertsBL.Instance.CheckSaturday(memberID, brand) || (smsImpulse is AuthCodeSMSAlertImpulse) || (smsImpulse is RegistrationSuccessSMSAlertImpulse))
            {
                // Get the alert message to send
                int smsMessageID = -1;
                string messsage = base.GetAlertBodyText(smsImpulse, ref smsMessageID, brand);

                string postURL = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SMS_ALERTS_POST_URL",
                    brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
                string port = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SMS_ALERTS_PORT",
                    brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
                string url = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SMS_ALERTS_WAP_URL",
                    brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
                string toPrefix = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SMS_ALERTS_TO_PREFIX",
                    brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
                string requestTimeout = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SMS_ALERTS_REQUEST_TIMEOUT",
                    brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
                string account = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SMS_ALERTS_ACCOUNT",
                    brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
                string user = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SMS_ALERTS_USER",
                    brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
                string password = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SMS_ALERTS_PASSWORD",
                    brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
                string from = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SMS_ALERTS_FROM",
                    brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
                string messageType = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SMS_ALERTS_MESSAGE_TYPE",
                    brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);

                if (smsImpulse is SummarySMSAlertImpulse)
                {
                    int emailCount = ((SummarySMSAlertImpulse)smsImpulse).EmailCount;
                    int flirtCount = ((SummarySMSAlertImpulse)smsImpulse).FlirtCount;
                    int ecardCount = ((SummarySMSAlertImpulse)smsImpulse).EcardCount;
                    messsage = messsage.Replace("<M>", emailCount.ToString());
                    messsage = messsage.Replace("<F>", flirtCount.ToString());
                    messsage = messsage.Replace("<E>", ecardCount.ToString());
                    bool emptySummarySMS = ((emailCount + flirtCount + ecardCount) == 0);
                    bool allowSummary = SMSAlertsBL.Instance.IsSummaryAllowed(memberID, brand);
                    if (allowSummary)
                    {
                        if (emptySummarySMS)
                        {
                            sendMessage = false;
                        }
                    }
                    else
                    {
                        sendMessage = false;
                    }
                }

                if (sendMessage)
                {
                    url = url.Replace("[MEMID]", memberID.ToString());
                    if (smsImpulse is SummarySMSAlertImpulse)
                    {
                        url = url.Split(new char[] { '?' })[0];
                    }
                    url = System.Web.HttpUtility.UrlEncode(url);

                    if (smsImpulse is AuthCodeSMSAlertImpulse || smsImpulse is RegistrationSuccessSMSAlertImpulse)
                    {
                        port = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SMS_ALERTS_AUTH_CODE_PORT",
                            brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);


                        url = String.Empty;

                        if (smsImpulse is AuthCodeSMSAlertImpulse)
                        {
                            messsage += " " + ((AuthCodeSMSAlertImpulse)smsImpulse).AuthCode;
                        }
                    }

                    messsage = System.Web.HttpUtility.UrlEncode(messsage);

                    try
                    {
                        // Send the message
                        TeleclalSMSMessageRequest req = new TeleclalSMSMessageRequest(account, user, password, from,
                            toPrefix + targetPhoneNumber, messsage, port, messageType, url);

                        string result = string.Empty;
                        for (int attempt = 0; attempt < 3; attempt++)
                        {
                            result = SendTeleclalPost(req.GetPostData(), postURL, int.Parse(requestTimeout), "application/x-www-form-urlencoded");
                            if (result != null) { break; }
                            System.Threading.Thread.Sleep(500);
                        }

                        if (result != null)
                        {
                            TeleclalSMSMessageResponse resp = SMSParser.ParseTeleclalPost(result);

                            responseCode = int.Parse(resp.Status);

                            // Log the send of this SMS alert
                            base.LogSMSSend(siteID, memberID, targetMemberID, smsImpulse.TargetPhoneNumber, smsMessageID, responseCode);

                            if (responseCode == 0) // If sending was successful
                            {
                                responseCode = 1; // return a success code to processSMSCycle
                            }
                        }
                        else
                        {
                            throw new Exception("Send message using Teleclal exceeded 3 attempts.");
                        }
                    }
                    catch (Exception ex)
                    {
                        string myMessage = "Unable to send SMS for impulse with query string: [" + smsImpulse.GetQueryString() + "] because of Exception with message: " + ex.Message + " and stack track: " + ex.StackTrace;
                        EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, myMessage, EventLogEntryType.Warning);
                        responseCode = -1;
                    }
                }
            }
            return responseCode;
        }

        private string SendTeleclalPost(string postData, string postURL, int requestTimeout, string contentType)
		{
			string result = null;
			try
			{
				result = SMSProxy.SendPost(postData, postURL,  requestTimeout, contentType);
			}
			catch(Exception ex)
			{
                StringBuilder sb = new StringBuilder();

			    sb.AppendLine("SendTeleclalPost failed with the following data:");
			    sb.Append("postData:");
			    sb.AppendLine(postData);
			    sb.Append("postURL:");
			    sb.AppendLine(postURL);
                sb.Append("requestTimeout:");
			    sb.AppendLine(requestTimeout.ToString());
                sb.Append("contentType:");
			    sb.AppendLine(contentType);
			    sb.Append("Exception message:");
			    sb.AppendLine(ex.Message);
                sb.Append("Stack Trace:");
			    sb.AppendLine(ex.StackTrace);

                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, sb.ToString(), EventLogEntryType.Warning);

				return null;
			}

			return result;
		}
    }
}
