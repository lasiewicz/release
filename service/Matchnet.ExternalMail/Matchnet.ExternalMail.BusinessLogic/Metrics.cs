using System;
using System.Diagnostics;
using System.Collections;

using Matchnet.ExternalMail.ValueObjects;

namespace Matchnet.ExternalMail.BusinessLogic
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class Metrics
	{
		public static readonly Metrics Instance = new Metrics();
		private static readonly Hashtable _PerfCounterInstances = new Hashtable();

		public Metrics()
		{
		}

		public PerformanceCounter ImpulseConsumed(string instance) { return (_PerfCounterInstances[instance] as CounterSet).ImpulseConsumed ; } 
		public PerformanceCounter ImpulsesConsumedPerSecond(string instance){ return (_PerfCounterInstances[instance] as CounterSet).ImpulsesConsumedPerSecond; }
		public PerformanceCounter ImpulsesFailed(string instance){ return (_PerfCounterInstances[instance] as CounterSet).ImpulsesFailed; }

		public void ResetCounters()
		{
			foreach(object obj in _PerfCounterInstances.Values)
			{
				(obj as CounterSet).ResetCoutners();
			}
		}


		public void InitializeCounters(string [] instances)
		{	
			foreach (string instance in instances)
			{
				_PerfCounterInstances[instance] = new CounterSet(instance);
			}	
		}


		public static void InstallPerformanceCounters()
		{
			UninstallPerformanceCounters();
			PerformanceCounterCategory.Create(ServiceConstants.SERVICE_NAME,"External Mail Service related coutners",
				new CounterCreationDataCollection( 
					new CounterCreationData [] {
						new CounterCreationData("Impulses Total","# of items consumed from queue(s) in total.",PerformanceCounterType.NumberOfItems32),
						new CounterCreationData("Impulses Per Second","rate of items / second processed",PerformanceCounterType.RateOfCountsPerSecond32),
						new CounterCreationData("Impulses Failed","# of items which for whatever reason did not make it as an email out the door.",PerformanceCounterType.NumberOfItems32)
				   }));
		}


		public static void UninstallPerformanceCounters()
		{
			_PerfCounterInstances.Clear();
			if (PerformanceCounterCategory.Exists(ServiceConstants.SERVICE_NAME)) 
			{
				PerformanceCounterCategory.Delete(ServiceConstants.SERVICE_NAME);
			}

		}


		private class CounterSet {
			public PerformanceCounter ImpulseConsumed;
			public PerformanceCounter ImpulsesConsumedPerSecond;
			public PerformanceCounter ImpulsesFailed;

			public CounterSet(string instance)
			{
				ImpulseConsumed				= new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Impulses Total", instance, false);
				ImpulsesConsumedPerSecond	= new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Impulses Per Second", instance, false);
				ImpulsesFailed				= new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Impulses Failed", instance, false);

				this.ResetCoutners();
			}

			public void ResetCoutners(){
				ImpulseConsumed.RawValue			= 0;
				ImpulsesConsumedPerSecond.RawValue	= 0;
				ImpulsesFailed.RawValue				= 0;
			}

		}
	}
}
