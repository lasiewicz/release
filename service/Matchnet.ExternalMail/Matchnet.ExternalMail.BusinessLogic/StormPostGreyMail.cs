#region

using System;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.EmailLibrary;
using Matchnet.ExternalMail.BusinessLogic.Monitoring;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Search.ServiceAdapters;
using Spark.Logger;
using Spark.Logging;

#endregion

namespace Matchnet.ExternalMail.BusinessLogic
{
    /// <summary>
    ///     Handles requests for grey mail impulse.
    ///     Sends to an ironport server. Stormpost server then picks up the items and processes them  asynchronously
    /// </summary>
    public class StormPostGreyMail
    {
        #region Private Constsants

        private const Int32 EMAIL_VERIFIED = 4;
        private const string TAB = "\t";
        private const int XOR_SALT_ADD_VALUE = 42;

        #endregion

        #region Private Varibles

        private readonly string[] ENGLISH_REJECT_REASONS =
        {
            "we were unable to read the file format you sent us so you will have to submit a new photo.", //	0
            "we do not accept copyrighted material, so you will have to submit a new photo.", //	1
            "it looks blurry when it's posted in your profile, so you'll have to submit a new one. Please make sure that your new photo is approximately 350 x 350 pixels, 72 dpi and is less than 100 KB.", //	2
            "the photo that you sent in was either too big or too small to post. Please submit a new photo that is approximately 350 x 350 pixels, 72 dpi and is less than 100 KB.", //	3
            "it does not meet our basic photo requirements, so you will have to submit a new photo.", //	4
            "we can't tell which one you are in the photo, so you will have to either resubmit it with clear instructions or send another.", //	5
            "we do not accept photos that are too suggestive so you will have to submit a new photo."
        }; //	6

        private readonly string[] FRENCH_REJECT_REASONS =
        {
            "we were unable to read the file format you sent us so you will have to submit a new photo.", //	0
            "nous n'acceptons pas les photos protégées par un copyright ou autres droits de la propriété. Veuillez en envoyer une nouvelle respectant cette règle.", //	1
            "votre photo est trop floue. Veuillez donc en envoyer une autre plus nette, correspondant a nos critères de taille suivant: 350x350 72 dpi de 5Mo maximum.", //	2
            "votre photos ne correspond pas aux normes de taille en vigueur sur le site. Veuillez en envoyer une de taille 350x350 pixels 72dpi, maximum, ne dépassant pas 5 Mo.", //	3
            "celle-ci ne répond pas a nos critères de sélection. Veuillez donc envoyer une nouvelle photo de vous.", //	4
            "nous ne pouvons pas savoir qui vous êtes. Veuillez en envoyer une nouvelle qui soit  commentées, ou bien une qui soit de vous-même seul.", //	5
            "nous n'acceptons pas les photos a caractère tendancieux. Veuillez nous envoyer une nouvelle photo répondant a nos critères."
        }; //	6

        private readonly string[] HEBREW_REJECT_REASONS =
        {
            "הצליח, היות וסוג הקובץ אינו תקין. יש להעלות תמונה מסוג GIF,JPEG  או  JPG בלבד.", //	0
            "הצליח, היות והתמונה בעלת זכויות יוצרים. להזכירך, איננו מקבלים תמונות שאינן תמונות  מקוריות של בעל הכרטיס ומוגנות בזכויות יוצרים.", //	1
            "הצליח, היות ותמונתך הופיעה במטושטש במערכת האתר.", //	2
            "הצליח. ככל הנראה, תמונתך חורגת מהגודל המקסימלי שניתן להעלות לאתר.יש לטעון תמונות במשקל שלא עולה על 5 מגה בייט וברזולוציה של 72 DPI.", //	3
            "הצליח. התמונה שהעלית אינה עונה על הדרישות באתר.  הדרישות מופיעות באתר בעמוד התמונות שלי.", //	4
            "הצליח. לצערנו, אין באפשרותנו לזהותך בוודאות בתמונה.  יש להקפיד להיות המופיע היחיד בתמונה הראשית", //	5
            "הצליח. להזכירך,  איננו מאשרים תמונות חשופות או פרובוקטיביות."
        }; //	6

        private readonly string[] REJECT_REASONS =
        {
            "Rejected_BadFileFormat", //	0
            "Rejected_Copyright", //	1
            "Rejected_Blurry", //	2
            "Rejected_FileSize", //	3
            "Rejected_General", //	4
            "Rejected_UnknownMember", //	5
            "Rejected_Suggestive"
        }; //	6

        private readonly string[] SEARCH_FIELDS_FOR_URL =
        {
            "SearchTypeID", "Distance", "MinAge", "MaxAge",
            "MinHeight", "MaxHeight", "EducationLevel", "Religion",
            "Ethnicity", "SmokingHabits", "DrinkingHabits", "HasPhotoFlag",
            "MajorType", "RelationshipMask", "RelationShipStatus", "RegionID",
            "SearchOrderBy"
        };

        private readonly ImpulseBase _impulse;

        private readonly MessageInfo _messageInfo;

        private readonly string _queryString;
        private readonly RequestParams _requestParams;

        private Brand _brand;
        private int _brandID;
        private int _emailType;
        private ListCountCollection _listCountCollection;
        private int _memberID;
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(StormPostGreyMail));
        #endregion

        #region Public Properties

        public MessageInfo MessageInfo
        {
            get { return (_messageInfo); }
        }

        public string QueryString
        {
            get { return (_queryString); }
        }

        public RequestParams Params
        {
            get { return (_requestParams); }
        }

        /// <summary>
        ///     This can be set and will then be used in making the x-headers.
        ///     Defaults to 0
        /// </summary>
        public int MemberID
        {
            get { return (_memberID); }
            set { _memberID = value; }
        }

        #endregion

        #region Public Constructors

        public StormPostGreyMail(string queryString, ImpulseBase impulse)
        {
            _queryString = queryString;
            _requestParams = new RequestParams(queryString);
            _messageInfo = new MessageInfo();
            _impulse = impulse;
        }

        #endregion

        #region Public Methods

        public MessageInfo Process()
        {
            _emailType = Convert.ToInt32(Params.Get("EmailType"));

            _brandID = Convert.ToInt32(Params.Get("BrandID"));
            _brand = BrandConfigSA.Instance.GetBrands().GetBrand(_brandID);

            var privateLabelID = DeterminePrivateLabelIDFromSiteID(_brand.Site.SiteID);

            var contentBuilder = new StringBuilder();
            BuildContent(contentBuilder);

            //	Set up x-header info
            //	First the old-school x-headers

            if (_emailType != (int) EmailType.ContactUs && _emailType != (int) EmailType.ReportAbuse
                && _emailType != (int) EmailType.InternalCorpMail && _emailType != (int) EmailType.MemberQuestionMail
                && _emailType != (int) EmailType.BetaFeedback)
            {
                var classification = new Classification();
                classification.Populate(DetermineActualEmailType(_emailType), MemberID, privateLabelID);
                var enumerator = classification.Headers.GetHeaderEnumerator();

                while (enumerator.MoveNext())
                {
                    var header = (Header) enumerator.Current;
                    MessageInfo.Headers.Add(header.Name, header.Value);
                }

                //	Now the StormPost Grey Mail specific x-header
                var numProfiles = DetermineProfileCount(_emailType);
                MessageInfo.Headers.Add("X-MsgType", privateLabelID + "-" + DetermineActualEmailType(_emailType) + "-" + numProfiles);
            }

            //	Done with the headers, now write out actual content

            var myContent = contentBuilder.ToString();
            MessageInfo.Body = myContent;

            var emailInfo = string.Format("GreyMail EmailType: {0} From: {1} To: {2} Body: {3}", _emailType,
                MessageInfo.FromAddress, MessageInfo.ToAddress, MessageInfo.Body);

            Log.LogInfoMessage(emailInfo, new CustomDataProvider(_brandID,_memberID,MessageInfo.ToAddress).Get());

            return (MessageInfo);
        }

        public static Language DetermineLanguageFromBrandID(int brandID)
        {
            var retVal = Language.English;

            switch (brandID)
            {
                case 1105:
                    retVal = Language.French;
                    break;
                case 1015: //	Cupid.co.il
                case 1004: //	Jdate.co.il
                    retVal = Language.Hebrew;
                    break;
                case 1005: //	DE
                    retVal = Language.German;
                    break;
                default:
                    retVal = Language.English;
                    break;
            }

            return (retVal);
        }


        public static int GetGenderMaskSelf(int genderMask)
        {
            var maskSelf = (Int32) GenderMask.Male + (Int32) GenderMask.Female + (Int32) GenderMask.MTF + (Int32) GenderMask.FTM;
            return genderMask & maskSelf;
        }


        public static int GetGenderMaskSeeking(int genderMask)
        {
            var maskSeeking = (Int32) GenderMask.SeekingMale + (Int32) GenderMask.SeekingFemale + (Int32) GenderMask.SeekingMTF + (Int32) GenderMask.SeekingFTM;
            return genderMask & maskSeeking;
        }

        #endregion

        #region Private Methods

        private int DetermineActualEmailType(int emailType)
        {
            var retVal = emailType;

            if (emailType == (int) EmailType.PhotoRejection)
            {
                //	This is a hack, but we will send both as photoapproval and set the profile
                //	count to 1 for the rejection (which is hacked in elsewhere)
                retVal = (int) EmailType.PhotoApproval;
            }

            return (retVal);
        }

        private int DeterminePrivateLabelIDFromSiteID(int siteID)
        {
            var retVal = 1;

            switch (siteID)
            {
                case 15: //	Cupid.co.il
                    retVal = 15;
                    break;
                case 112: //	CollegeLuv.com
                    retVal = 12;
                    break;
                case 102: //	Glimpse.com
                    retVal = 2;
                    break;
                case 103: //	JDate.com
                    retVal = 3;
                    break;
                case 101: //	AmericanSingles.com
                case 7: //	MatchNet.com.au
                case 1934: //	DatingResults.com
                case 113: //	SilverSingles.com
                    // AU DR SS are no long supported as their own sites, now represented as AS
                    retVal = 1;
                    break;
                case 13: //	Date.ca
                    retVal = 13;
                    break;
                case 6: //		Date.co.uk
                    retVal = 6;
                    break;
                case 4: //	JDate.co.il
                    retVal = 4;
                    break;
                case 108: //	MatchNet.com
                    retVal = 8;
                    break;
                case 100: //	Spark.com
                    retVal = 1;
                    break;
            }

            return (retVal);
        }

        private int DetermineProfileCount(int emailType)
        {
            var retVal = 0;

            if (emailType == (int) EmailType.ViralNewsLetter)
            {
                var viralImpulse = (ViralImpulse) _impulse;
                if (viralImpulse.TargetMiniprofileInfoCollection != null)
                {
                    retVal = viralImpulse.TargetMiniprofileInfoCollection.Count;
                }
                else
                {
                    throw new Exception("Trying to create a ViralImpulse with a null Miniprofile Collection");
                }
            }
            else if (emailType == (int) EmailType.PhotoRejection)
            {
                //	This is a vicious hack, but there are two templates used by type 16, and
                //	for the rejection we set the count to 1 rather than 0. 
                retVal = 1;
            }

            if (emailType == (int) EmailType.MatchNewsLetter)
            {
                var matchImpulse = (MatchMailImpulse) _impulse;
                if (matchImpulse.TargetMiniprofileInfoCollection != null)
                {
                    retVal = matchImpulse.TargetMiniprofileInfoCollection.Count;
                }
                else
                {
                    throw new Exception("Trying to create a MatchMail with a null Miniprofile Collection");
                }

                //	For these the user's own profile counts as one apparently.
                retVal++;
            }

            return (retVal);
        }

        private void BuildContent(StringBuilder content)
        {
            try
            {
                switch ((EmailType) _emailType)
                {
                    case EmailType.ActivationLetter:
                        BuildActivationLetter(content);
                        break;
                    case EmailType.EmailChangeConfirmation:
                        BuildEmailVerificationLetter(content);
                        break;
                    case EmailType.ForgotPassword:
                        BuildForgotPassword(content);
                        break;
                    case EmailType.PhotoApproval:
                        BuildPhotoApproval(content);
                        break;
                    case EmailType.SendToFriend:
                        BuildSendToFriend(content);
                        break;
                    case EmailType.NewMailAlert:
                        BuildNewMailAlert(content);
                        break;
                    case EmailType.MatchNewsLetter:
                        BuildMatchNewsLetter(content);
                        break;
                    case EmailType.MutualMail:
                        BuildMutualMail(content);
                        break;
                    case EmailType.ViralNewsLetter:
                        BuildViralNewsLetter(content);
                        break;
                    case EmailType.PhotoRejection:
                        BuildPhotoRejection(content);
                        break;
                    case EmailType.ContactUs:
                        BuildContactUs(content);
                        break;
                    case EmailType.OptOutNotify:
                        BuildOptOutNotify(content);
                        break;
                    case EmailType.HotListedAlert:
                        BuildHotListedAlert(content);
                        break;
                    case EmailType.ReportAbuse:
                        BuildReportAbuseEmail(content);
                        break;
                    case EmailType.SubscriptionConfirmation:
                        BuildSubscriptionConfirmation(content);
                        break;
                    case EmailType.InternalCorpMail:
                        BuildInternalCorpMail(content);
                        break;
                    case EmailType.MemberQuestionMail:
                        BuildMemberQuestionMail(content);
                        break;
                    case EmailType.BetaFeedback:
                        BuildBetaFeedback(content);
                        break;
                    default:
                        throw new Exception("Invalid EmailType of: " + _emailType
                                            + " could not be matched with a GreyMail Build method.  QueryString was: "
                                            + _queryString);
                }
            }
            catch (Exception ex)
            {
                MessageInfo.CancelSend("Exception while processing impulse with query string '" + _queryString
                                       + "'.  Exception message is: " + ex.Message + " and stack trace is: " + ex.StackTrace);
            }
        }

        private void BuildActivationLetter(StringBuilder content)
        {
            _memberID = Convert.ToInt32(Params.Get("MemberID"));

            var member = MailHandlerUtils.GetIMemberDTO(MemberID, MemberLoadFlags.None);
            var emailAddress = member.EmailAddress;
            if (emailAddress == null)
            {
                MessageInfo.CancelSend("Member: " + MemberID + " has a null email address so we cannot send emails to them");
                return;
            }
            if (string.Empty.Equals(emailAddress))
            {
                MessageInfo.CancelSend("Member: " + MemberID + " has an empty email address so we cannot send emails to them");
                return;
            }
            var verifyCode = EmailVerify.HashMemberID(_memberID);

            content.Append(Filter(emailAddress));
            content.Append(TAB);
            content.Append(Filter(verifyCode));
            if (_brandID == 1002 || _brandID == 1013 || _brandID == 1006) //	Glimpse, Date.ca, Date.co.uk
            {
                var password = MemberSA.Instance.GetPassword(emailAddress);
                if (password == null || string.Empty.Equals(password))
                {
                    password = "********";
                }
                content.Append(TAB);
                content.Append(Filter(password));
            }

            MessageInfo.FromAddress = "Activation@" + _brand.Site.Name;
            MessageInfo.FromName = "Activation@" + _brand.Site.Name;
            MessageInfo.ToAddress = emailAddress;
            MessageInfo.ToName = emailAddress;
            MessageInfo.Subject = _brand.Site.Name + " Welcome";
        }

        private void BuildEmailVerificationLetter(StringBuilder content)
        {
            _memberID = Convert.ToInt32(Params.Get("MemberID"));

            var member = MailHandlerUtils.GetIMemberDTO(MemberID, MemberLoadFlags.None);

            var emailAddress = member.EmailAddress;
            var verifyCode = EmailVerify.HashMemberID(_memberID);

            content.Append(Filter(emailAddress));
            content.Append(TAB);
            content.Append(Filter(verifyCode));

            MessageInfo.FromAddress = "verify@" + _brand.Site.Name;
            MessageInfo.FromName = "verify@" + _brand.Site.Name;
            MessageInfo.ToAddress = emailAddress;
            MessageInfo.ToName = emailAddress;
            MessageInfo.Subject = "Email Verification";
        }

        private void BuildForgotPassword(StringBuilder content)
        {
            var emailAddress = Params.Get("EmailAddress");
            var password = MemberSA.Instance.GetPassword(emailAddress);
            if (password == null || string.Empty.Equals(password))
            {
                //	This should never happen, but apparently from time to time it does
                password = "********";
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME,
                    "Got a null or blank password for a user with email: " + emailAddress + ".  Will send email with placeholder.",
                    EventLogEntryType.Warning);
            }
            var totalRows = 0;
            var members = MemberSA.Instance.GetMembersByEmail(emailAddress, 0, 100, ref totalRows);

            content.Append(Filter(emailAddress));
            content.Append(TAB);
            content.Append(Filter(password));

            MessageInfo.FromAddress = "memberservices@" + _brand.Site.Name;
            MessageInfo.FromName = "memberservices@" + _brand.Site.Name;
            MessageInfo.ToAddress = emailAddress;
            MessageInfo.ToName = emailAddress;
            MessageInfo.Subject = "Forgot Password";
        }

        private void BuildPhotoApproval(StringBuilder content)
        {
            _memberID = Convert.ToInt32(Params.Get("MemberID"));
            var member = MailHandlerUtils.GetIMemberDTO(MemberID, MemberLoadFlags.None);
            var emailAddress = member.EmailAddress;

            content.Append(Filter(emailAddress));

            MessageInfo.FromAddress = "photos@" + _brand.Site.Name;
            MessageInfo.FromName = "photos@" + _brand.Site.Name;
            MessageInfo.ToAddress = emailAddress;
            MessageInfo.ToName = emailAddress;
            MessageInfo.Subject = _brand.Site.Name + " photo approved";
        }

        private void BuildSendToFriend(StringBuilder content)
        {
            var friendEmail = Params.Get("FriendEmail");
            var friendName = Params.Get("FriendName");
            var sentMemberID = Params.Get("SentMemberID");
            var subject = Params.Get("Subject");
            var userEmail = Params.Get("UserEmail");
            var memberComment = Params.Get("Message");

            content.Append(Filter(friendEmail));
            content.Append(TAB);
            content.Append(Filter(friendName));
            content.Append(TAB);
            content.Append(Filter(sentMemberID));
            content.Append(TAB);
            content.Append(Filter(memberComment));

            MessageInfo.FromAddress = userEmail;
            MessageInfo.FromName = userEmail;
            MessageInfo.ToAddress = friendEmail;
            MessageInfo.ToName = friendName;
            MessageInfo.Subject = subject;
        }

        private void BuildNewMailAlert(StringBuilder content)
        {
            _memberID = Convert.ToInt32(Params.Get("MemberID"));
            var member = MailHandlerUtils.GetIMemberDTO(MemberID, MemberLoadFlags.None);

            var send = true;
            if (Convert.ToBoolean(RuntimeSettings.GetSetting(EmailAlertConstant.YES_MAIL_FLAG,
                _brand.Site.Community.CommunityID, _brand.Site.SiteID)))
            {
                var group = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(_brand.Site.Community.CommunityID, EmailAlertConstant.ATTRIBUTE_ID);

                var mask = (int) EmailAlertMask.NewEmailAlert;
                var userSetting = member.GetAttributeInt(_brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, int.Parse(group.DefaultValue));
                if ((mask & userSetting) != mask)
                {
                    send = false;
                    MessageInfo.CancelSend("Setting turned off for (New Mail Alert)");
                }
            }

            if (send)
            {
                var fromMemberID = Convert.ToInt32(Params.Get("SentMemberID"));
                var fromMember = MailHandlerUtils.GetIMemberDTO(fromMemberID, MemberLoadFlags.None);

                var emailID = Convert.ToInt32(Params.Get("EmailID"));
                var emailSent = Convert.ToDateTime(Params.Get("InsertDate"));
                var messageCount = Convert.ToInt32(Params.Get("UnreadEmailCount"));

                var emailSentString = EmailMemberHelper.GetDateDisplay(emailSent, _brand);

                content.Append(Filter(member.EmailAddress));
                content.Append(TAB);

                content.Append(Filter(member.GetUserName(_brand)));
                content.Append(TAB);

                content.Append(Filter(emailSentString));
                content.Append(TAB);

                content.Append(messageCount);
                content.Append(TAB);

                // show miniprofile regardless if setting allows for site
                if (Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_VIEW_MINIPROFILE_NEW_MAIL_ALERT", _brand.Site.Community.CommunityID, _brand.Site.SiteID)))
                {
                    content.Append(Filter(EmailMemberHelper.DetermineMemberThumbnail(fromMember, _brand)));
                }
                else
                {
                    // if non subscribe, do not show the from member's miniprofile
                    if (member.IsPayingMember(_brand.Site.SiteID))
                    {
                        content.Append(Filter(EmailMemberHelper.DetermineMemberThumbnail(fromMember, _brand)));
                    }
                    else
                    {
                        content.Append("HTML"); // this is how storm post template determines when to render from profile.
                    }
                }
                content.Append(TAB);

                content.Append(Filter(fromMember.GetUserName(_brand)));
                content.Append(TAB);

                content.Append(EmailMemberHelper.DetermineMemberAge(fromMember, _brand));
                content.Append(TAB);

                content.Append(Filter(DetermineMemberSeeking(fromMember)));
                content.Append(TAB);

                if (_brandID != 1002) //	Glimpse is different.  Here is the normal way:
                {
                    content.Append(Filter(EmailMemberHelper.DetermineMemberRegionDisplay(fromMember, _brand)));
                    content.Append(TAB);

                    content.Append(emailID);
                }
                else //	And here is the Glimpse way:
                {
                    content.Append(emailID);
                    content.Append(TAB);

                    content.Append(Filter(EmailMemberHelper.DetermineMemberRegionDisplay(fromMember, _brand)));
                }

                MessageInfo.FromAddress = "Communications@" + _brand.Site.Name;
                MessageInfo.FromName = "Communications@" + _brand.Site.Name;
                MessageInfo.ToAddress = member.EmailAddress;
                MessageInfo.ToName = member.EmailAddress;
                MessageInfo.Subject = "You have mail";
            }
        }


        /// <summary>
        /// </summary>
        /// <param name="content"></param>
        private void BuildMatchNewsLetter(StringBuilder content)
        {
            var impulse = (MatchMailImpulse) _impulse;
            var emailAddress = impulse.RecipientEmailAddress;
            var recipientMiniprofileInfo = impulse.RecipientMiniprofileInfo;
            var members = impulse.TargetMiniprofileInfoCollection;
            _memberID = recipientMiniprofileInfo.MemberID;

            //	Start making the actual GreyMail content

            content.Append(Filter(emailAddress));
            content.Append(TAB);

            foreach (MiniprofileInfo currentMember in members)
            {
                content.Append(currentMember.MemberID);
                content.Append(TAB);
            }

            content.Append(MemberID);
            content.Append(TAB);

            foreach (MiniprofileInfo currentMember in members)
            {
                content.Append(Filter(currentMember.UserName));
                content.Append(TAB);
            }

            content.Append(Filter(recipientMiniprofileInfo.UserName));
            content.Append(TAB);

            foreach (MiniprofileInfo currentMember in members)
            {
                var age = Conversion.Age(currentMember.BirthDate);

                content.Append(age);
                content.Append(TAB);
            }

            content.Append(EmailMemberHelper.DetermineMemberAge(recipientMiniprofileInfo.BirthDate));
            content.Append(TAB);

            foreach (MiniprofileInfo currentMember in members)
            {
                var regionDisplay = EmailMemberHelper.DetermineMemberRegionDisplay(currentMember.RegionID, _brand);

                content.Append(Filter(regionDisplay));
                content.Append(TAB);
            }

            content.Append(Filter(EmailMemberHelper.DetermineMemberRegionDisplay(recipientMiniprofileInfo.RegionID, _brand)));
            content.Append(TAB);

            foreach (MiniprofileInfo currentMember in members)
            {
                var thumbnail = EmailMemberHelper.DetermineBaseNoPhoto(_brand);

                if (currentMember.ThumbNailWebPath != Constants.NULL_STRING)
                {
                    thumbnail = currentMember.ThumbNailWebPath;
                }

                content.Append(Filter(thumbnail));
                content.Append(TAB);
            }

            var recipientThumbnail = EmailMemberHelper.DetermineBaseNoPhoto(_brand);

            if (recipientMiniprofileInfo.ThumbNailWebPath != Constants.NULL_STRING)
            {
                recipientThumbnail = recipientMiniprofileInfo.ThumbNailWebPath;
            }

            content.Append(Filter(recipientThumbnail));
            content.Append(TAB);

            content.Append(recipientMiniprofileInfo.AboutMe);
            content.Append(TAB);

            #region Append HotList Tokens (You XXed others)

            content.Append(DetermineListCount(recipientMiniprofileInfo.MemberID, HotListCategory.Default));
            content.Append(TAB);

            content.Append(DetermineListCount(recipientMiniprofileInfo.MemberID, HotListCategory.MembersYouEmailed));
            content.Append(TAB);

            content.Append(DetermineListCount(recipientMiniprofileInfo.MemberID, HotListCategory.MembersYouTeased));
            content.Append(TAB);

            content.Append(DetermineListCount(recipientMiniprofileInfo.MemberID, HotListCategory.MembersYouIMed));
            content.Append(TAB);

            content.Append(DetermineListCount(recipientMiniprofileInfo.MemberID, HotListCategory.MembersYouViewed));
            content.Append(TAB);

            #endregion

            content.Append(Filter(DetermineSearchUrl(recipientMiniprofileInfo.MemberID,
                BrandConfigSA.Instance.GetBrandByID(impulse.BrandID).Site.Community.CommunityID)));
            content.Append(TAB);

            content.Append(Filter(recipientMiniprofileInfo.UserName));
            content.Append(TAB);

            // Append View Profile MMVID (Enables hot listing w/o logging in)
            foreach (MiniprofileInfo currentMember in members)
            {
                content.Append(GetMMVID(recipientMiniprofileInfo.MemberID, currentMember.MemberID));
                content.Append(TAB);
            }

            #region Append Reverse HotList Tokens (Who XXed you)

            content.Append(DetermineListCount(recipientMiniprofileInfo.MemberID, HotListCategory.WhoAddedYouToTheirFavorites));
            content.Append(TAB);

            content.Append(DetermineListCount(recipientMiniprofileInfo.MemberID, HotListCategory.WhoEmailedYou));
            content.Append(TAB);

            content.Append(DetermineListCount(recipientMiniprofileInfo.MemberID, HotListCategory.WhoTeasedYou));
            content.Append(TAB);

            content.Append(DetermineListCount(recipientMiniprofileInfo.MemberID, HotListCategory.WhoIMedYou));
            content.Append(TAB);

            content.Append(DetermineListCount(recipientMiniprofileInfo.MemberID, HotListCategory.WhoViewedYourProfile));
            content.Append(TAB);

            #endregion

            #region V1.3 #17638 Additional tokens for banners			

            //Religion
            content.Append(recipientMiniprofileInfo.ReligionID.ToString());
            content.Append(TAB);

            //JDateReligion
            content.Append(recipientMiniprofileInfo.JdateReligionID.ToString());
            content.Append(TAB);

            //Ethnicity
            content.Append(recipientMiniprofileInfo.EthnicityID.ToString());
            content.Append(TAB);

            //JdateEthnicity
            content.Append(recipientMiniprofileInfo.JdateEthnicityID.ToString());
            content.Append(TAB);

            //MaritalStatus
            content.Append(recipientMiniprofileInfo.MaritalStatusID.ToString());
            content.Append(TAB);

            //ChildrenCount
            content.Append(recipientMiniprofileInfo.ChildrenCount.ToString());
            content.Append(TAB);

            //Custody
            content.Append(recipientMiniprofileInfo.CustodyID.ToString());
            content.Append(TAB);

            //IsPayingMember
            content.Append(recipientMiniprofileInfo.IsPayingMember.ToString());
            content.Append(TAB);

            //IsVerifiedEamil
            var isVerifiedEmail = false;
            var globalStatusMask = recipientMiniprofileInfo.GlobalStatusMask;
            if ((globalStatusMask != Constants.NULL_INT) && ((globalStatusMask & EMAIL_VERIFIED) == EMAIL_VERIFIED))
            {
                isVerifiedEmail = true;
            }
            content.Append(isVerifiedEmail.ToString());
            content.Append(TAB);

            //HasApprovedPhoto
            content.Append(recipientMiniprofileInfo.HasApprovedPhotos.ToString());
            content.Append(TAB);

            //Gender
            content.Append(GetGenderMaskSelf(recipientMiniprofileInfo.GenderMask).ToString());
            content.Append(TAB);

            //SeekingGender
            content.Append(GetGenderMaskSeeking(recipientMiniprofileInfo.GenderMask).ToString());
            content.Append(TAB);

            //EmailVerificationCode
            content.Append(EmailVerify.HashMemberID(recipientMiniprofileInfo.MemberID));
            content.Append(TAB);

            #endregion

            MessageInfo.FromAddress = "YourMatches@" + _brand.Site.Name;
            MessageInfo.FromName = "YourMatches@" + _brand.Site.Name;
            MessageInfo.ToAddress = emailAddress;
            MessageInfo.ToName = emailAddress;
            MessageInfo.Subject = "Your matches from " + _brand.Site.Name;
        }


        private void BuildMutualMail(StringBuilder content)
        {
            _memberID = Convert.ToInt32(Params.Get("MemberID"));
            var member = MailHandlerUtils.GetIMemberDTO(MemberID, MemberLoadFlags.None);

            var send = true;
            if (Convert.ToBoolean(RuntimeSettings.GetSetting(EmailAlertConstant.YES_MAIL_FLAG,
                _brand.Site.Community.CommunityID, _brand.Site.SiteID)))
            {
                var group = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(_brand.Site.Community.CommunityID, EmailAlertConstant.ATTRIBUTE_ID);

                var mask = (int) EmailAlertMask.GotClickAlert;
                var userSetting = member.GetAttributeInt(_brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, int.Parse(group.DefaultValue));
                if ((mask & userSetting) != mask)
                {
                    send = false;
                    MessageInfo.CancelSend("Setting turned off for (Got Cliqued Alert)");
                }
            }

            if (send)
            {
                var mutualMemberID = Convert.ToInt32(Params.Get("TargetMemberID"));
                var mutualMember = MailHandlerUtils.GetIMemberDTO(mutualMemberID, MemberLoadFlags.None);

                content.Append(Filter(member.EmailAddress));
                content.Append(TAB);

                content.Append(Filter(member.GetUserName(_brand)));
                content.Append(TAB);

                content.Append(mutualMember.MemberID);
                content.Append(TAB);

                content.Append(Filter(EmailMemberHelper.DetermineMemberThumbnail(mutualMember, _brand)));
                content.Append(TAB);

                content.Append(Filter(mutualMember.GetUserName(_brand)));
                content.Append(TAB);

                content.Append(EmailMemberHelper.DetermineMemberAge(mutualMember, _brand));
                content.Append(TAB);

                content.Append(Filter(EmailMemberHelper.DetermineMemberRegionDisplay(mutualMember, _brand)));
                content.Append(TAB);

                content.Append(Filter(DetermineMemberAboutMe(mutualMember, member)));
                content.Append(TAB);

                content.Append(DetermineMemberUsernameText(member, _brand));
                content.Append(TAB);

                content.Append(DetermineMemberUsernameText(mutualMember, _brand));
                content.Append(TAB);

                // MutualMemberGender
                content.Append(GetGenderMaskSelf(mutualMember.GetAttributeInt(_brand, "GenderMask", Constants.NULL_INT)).ToString());
                content.Append(TAB);

                MessageInfo.FromAddress = "Communications@" + _brand.Uri;
                MessageInfo.FromName = "Communications@" + _brand.Uri;
                MessageInfo.ToAddress = member.EmailAddress;
                MessageInfo.ToName = member.EmailAddress;
                MessageInfo.Subject = "You Click";
            }
        }


        private void BuildViralNewsLetter(StringBuilder content)
        {
            var impulse = (ViralImpulse) _impulse;
            var emailAddress = impulse.RecipientEmailAddress;
            var recipientMiniprofileInfo = impulse.RecipientMiniprofileInfo;
            var clickMembers = impulse.TargetMiniprofileInfoCollection;
            _memberID = recipientMiniprofileInfo.MemberID;

            content.Append(Filter(emailAddress));
            content.Append(TAB);

            foreach (MiniprofileInfo currentMember in clickMembers)
            {
                content.Append(currentMember.MemberID);
                content.Append(TAB);
            }

            foreach (MiniprofileInfo currentMember in clickMembers)
            {
                var thumbnail = EmailMemberHelper.DetermineBaseNoPhoto(_brand);

                if (currentMember.ThumbNailWebPath != Constants.NULL_STRING)
                {
                    thumbnail = currentMember.ThumbNailWebPath;
                }

                content.Append(Filter(thumbnail));
                content.Append(TAB);
            }

            foreach (MiniprofileInfo currentMember in clickMembers)
            {
                content.Append(Filter(currentMember.UserName));
                content.Append(TAB);
            }

            foreach (MiniprofileInfo currentMember in clickMembers)
            {
                content.Append(EmailMemberHelper.DetermineMemberAge(currentMember.BirthDate));
                content.Append(TAB);
            }

            foreach (MiniprofileInfo currentMember in clickMembers)
            {
                content.Append(Filter(EmailMemberHelper.DetermineMemberRegionDisplay(currentMember.RegionID, _brand)));
                content.Append(TAB);
            }

            foreach (MiniprofileInfo currentMember in clickMembers)
            {
                content.Append(Filter(currentMember.AboutMe));
                content.Append(TAB);
            }

            content.Append(recipientMiniprofileInfo.MemberID);
            content.Append(TAB);

            content.Append(Filter(recipientMiniprofileInfo.UserName));
            content.Append(TAB);

            // it adds username again? need to look up spec.
            content.Append(Filter(recipientMiniprofileInfo.UserName));
            content.Append(TAB);

            foreach (MiniprofileInfo currentMember in clickMembers)
            {
                content.Append(Filter(currentMember.UserName));
                content.Append(TAB);
            }

            //	chop off the trailing tab
            content.Remove(content.Length - 1, 1);

            MessageInfo.FromAddress = "click@" + _brand.Site.Name;
            MessageInfo.FromName = "click@" + _brand.Site.Name;
            MessageInfo.ToAddress = emailAddress;
            MessageInfo.ToName = emailAddress;
            MessageInfo.Subject = "Do You Click?";
        }


        private void BuildPhotoRejection(StringBuilder content)
        {
            _memberID = Convert.ToInt32(Params.Get("MemberID"));
            var member = MailHandlerUtils.GetIMemberDTO(MemberID, MemberLoadFlags.None);
            var emailAddress = member.EmailAddress;
            var comment = Params.Get("Comment");


            //Language theLanguage = DetermineLanguageFromBrandID(_brandID);
            var reason = GetPhotoDisapproveText(comment, _brand.Site.LanguageID);

            content.Append(Filter(emailAddress));
            content.Append(TAB);
            content.Append(Filter(reason));

            MessageInfo.FromAddress = "photos@" + _brand.Site.Name;
            MessageInfo.FromName = "photos@" + _brand.Site.Name;
            MessageInfo.ToAddress = emailAddress;
            MessageInfo.ToName = emailAddress;
            MessageInfo.Subject = _brand.Site.Name + " photo rejected";
        }


        private void BuildReportAbuseEmail(StringBuilder content)
        {
            content.Append(Params.Get("ReportContent"));
            var fromAddress = getMemberEmail(content.ToString());
            if (!string.IsNullOrEmpty(fromAddress))
                MessageInfo.FromAddress = fromAddress;
            else
                MessageInfo.FromAddress = "ReportAbuse@" + _brand.Site.Name;

            MessageInfo.FromName = "Report Abuse";
            MessageInfo.ToAddress = "concern@" + _brand.Site.Name;
            MessageInfo.ToName = "Concern";
            MessageInfo.Subject = "Report Abuse";
        }


        private void BuildInternalCorpMail(StringBuilder content)
        {
            content.Append(Params.Get("Body"));
            MessageInfo.FromAddress = Params.Get("FromAddress");
            MessageInfo.ToAddress = Params.Get("ToAddress");
            MessageInfo.ToName = Params.Get("ToName");
            MessageInfo.Subject = Params.Get("Subject");
        }

        private void BuildContactUs(StringBuilder content)
        {
            content.Append(string.Format("Brand: {0}\n\n", BrandConfigSA.Instance.GetBrandByID(Conversion.CInt(Params.Get("BrandID"))).Uri));
            content.Append(string.Format("UserName: {0}\n\n", Params.Get("UserName")));
            content.Append(string.Format("Email: {0}\n\n", Params.Get("FromEmailAddress")));
            content.Append(string.Format("MemberID: {0}\n\n", Params.Get("SMemberID")));
            content.Append(string.Format("Browser: {0}\n\n", Params.Get("Browser")));
            content.Append(string.Format("RemoteIP: {0}\n\n", Params.Get("RemoteIP")));
            content.Append(string.Format("Comment: {0}\n\n", Params.Get("UserComment")));
            content.Append(string.Format("App/DeviceInfo: {0}", Params.Get("AppDeviceInfo")));

            MessageInfo.FromAddress = Params.Get("FromEmailAddress");
            MessageInfo.FromName = Params.Get("UserName");
            MessageInfo.ToAddress = String.IsNullOrEmpty(Params.Get("ToEmailAddress")) ? "comments@" + _brand.Site.Name : Params.Get("ToEmailAddress");
            MessageInfo.ToName = "Comments";
            MessageInfo.Subject = Params.Get("Reason");
        }


        private void BuildOptOutNotify(StringBuilder content)
        {
            content.Append(HttpUtility.UrlDecode(QueryString.Replace("&", "\n")));
            MessageInfo.FromAddress = Params.Get("FromEmailAddress");
            MessageInfo.FromName = Params.Get("UserName");
            MessageInfo.ToAddress = "comments@" + _brand.Site.Name;
            MessageInfo.ToName = "Comments";
            MessageInfo.Subject = "Contact us";
        }

        private void BuildBetaFeedback(StringBuilder content)
        {
            content.Append(string.Format("Brand: {0}\n\n", BrandConfigSA.Instance.GetBrandByID(Conversion.CInt(Params.Get("BrandID"))).Uri));
            content.Append(string.Format("Name: {0}\n\n", Params.Get("SenderName")));
            content.Append(string.Format("Email: {0}\n\n", Params.Get("SenderEmailAddress")));
            content.Append(string.Format("MemberID: {0}\n\n", Params.Get("SenderMemberID")));
            content.Append(string.Format("Browser: {0}\n\n", Params.Get("Browser")));
            content.Append(string.Format("RemoteIP: {0}\n\n", Params.Get("RemoteIP")));
            content.Append(string.Format("Feedback: {0}", Params.Get("UserComment")));

            MessageInfo.FromAddress = Params.Get("SenderEmailAddress");
            MessageInfo.FromName = Params.Get("SenderName");
            MessageInfo.ToAddress = "beta@" + _brand.Site.Name;
            MessageInfo.ToName = "beta";
            MessageInfo.Subject = Params.Get("Subject");
        }


        /// <summary>
        ///     Stormpost Import Template Tokens
        ///     EmailAddress
        ///     UserName
        ///     HotListedByMemberThumbnail
        ///     HotListedByMemberUserName
        ///     HotListedByMemberAge
        ///     HotListedByMemberSeeking
        ///     HotListedByMemberLocation
        /// </summary>
        /// <param name="content"></param>
        private void BuildHotListedAlert(StringBuilder content)
        {
            var impulse = (HotListedNotifyImpulse) _impulse;
            _memberID = impulse.MemberID;

            var member = MailHandlerUtils.GetIMemberDTO(_memberID, MemberLoadFlags.None);
            var hotListedByMember = MailHandlerUtils.GetIMemberDTO(impulse.HotListedByMemberID, MemberLoadFlags.None);

            var send = true;
            if (Convert.ToBoolean(RuntimeSettings.GetSetting(EmailAlertConstant.YES_MAIL_FLAG,
                _brand.Site.Community.CommunityID, _brand.Site.SiteID)))
            {
                // Yesmail options turned on, check user setting.
                var group = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(_brand.Site.Community.CommunityID, EmailAlertConstant.ATTRIBUTE_ID);

                var mask = (int) EmailAlertMask.HotListedAlert;
                var userSetting = member.GetAttributeInt(_brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, int.Parse(group.DefaultValue));
                if ((mask & userSetting) != mask)
                {
                    send = false;
                    MessageInfo.CancelSend("Setting turned off for (Hot List Alert)");
                }
            }

            if (send)
            {
                content.Append(Filter(member.EmailAddress));
                content.Append(TAB);

                content.Append(Filter(member.GetUserName(_brand)));
                content.Append(TAB);

                content.Append(Filter(EmailMemberHelper.DetermineMemberThumbnail(hotListedByMember, _brand)));
                content.Append(TAB);

                content.Append(Filter(hotListedByMember.GetUserName(_brand)));
                content.Append(TAB);

                content.Append(EmailMemberHelper.DetermineMemberAge(hotListedByMember, _brand));
                content.Append(TAB);

                content.Append(Filter(DetermineMemberSeeking(hotListedByMember)));
                content.Append(TAB);

                content.Append(Filter(EmailMemberHelper.DetermineMemberRegionDisplay(hotListedByMember, _brand)));
                content.Append(TAB);

                MessageInfo.FromAddress = "Communications@" + _brand.Site.Name;
                MessageInfo.FromName = "Communications@" + _brand.Site.Name;
                MessageInfo.ToAddress = member.EmailAddress;
                MessageInfo.ToName = member.EmailAddress;
                MessageInfo.Subject = "You've been hot listed";
            }
        }


        /// <summary>
        ///     Stormpost Import Template Tokens
        ///     EmailAddress
        ///     UserName
        ///     FirstName
        ///     LastName
        ///     Address1
        ///     City
        ///     State
        ///     PostalCode
        ///     DateOfPurchase
        ///     PlanType
        ///     CurrencyType
        ///     InitialCost
        ///     InitialDuration
        ///     InitialDurationType
        ///     RenewCost
        ///     RenewDuration
        ///     RenewDurationType
        /// </summary>
        /// <param name="content"></param>
        private void BuildSubscriptionConfirmation(StringBuilder content)
        {
            var impulse = (SubscriptionConfirmationImpulse) _impulse;
            _memberID = impulse.MemberID;

            content.Append(Filter(impulse.EmailAddress));
            content.Append(TAB);

            content.Append(Filter(impulse.UserName));
            content.Append(TAB);

            content.Append(impulse.FirstName);
            content.Append(TAB);

            content.Append(impulse.LastName);
            content.Append(TAB);

            content.Append(impulse.Address1);
            content.Append(TAB);

            content.Append(impulse.City);
            content.Append(TAB);

            content.Append(impulse.State);
            content.Append(TAB);

            content.Append(impulse.PostalCode);
            content.Append(TAB);

            if (impulse.BrandID == 1004)
            {
                content.Append(impulse.DateOfPurchase.AddHours(10).ToString("dd/MM/yyyy"));
            }
            else if (impulse.BrandID == 1013)
            {
                content.Append(impulse.DateOfPurchase.ToString("dd/MM/yyyy"));
            }
            else
            {
                content.Append(impulse.DateOfPurchase.ToString("d"));
            }
            content.Append(TAB);

            content.Append(impulse.PlanType.ToString());
            content.Append(TAB);

            content.Append(impulse.CurrencyType.ToString());
            content.Append(TAB);

            content.Append(impulse.InitialCost.ToString());
            content.Append(TAB);

            content.Append(impulse.InitialDuration.ToString());
            content.Append(TAB);

            content.Append(((int) impulse.InitialDurationType).ToString());
            content.Append(TAB);

            content.Append(impulse.RenewCost.ToString());
            content.Append(TAB);

            content.Append(impulse.RenewDuration.ToString());
            content.Append(TAB);

            content.Append(((int) impulse.RenewDurationType).ToString());
            content.Append(TAB);

            content.Append(impulse.Last4CC);
            content.Append(TAB);

            content.Append(impulse.CreditCardTypeID.ToString());
            content.Append(TAB);

            content.Append(impulse.ConfirmationNumber);
            content.Append(TAB);

            MessageInfo.FromAddress = "Communications@" + _brand.Site.Name;
            MessageInfo.FromName = "Communications@" + _brand.Site.Name;
            MessageInfo.ToAddress = impulse.EmailAddress;
            MessageInfo.ToName = impulse.EmailAddress;
            MessageInfo.Subject = "Subscription Confirmation";
        }

        private void BuildMemberQuestionMail(StringBuilder content)
        {
            _memberID = Convert.ToInt32(Params.Get("MemberID"));

            var memberQuestionImpulse = (MemberQuestionImpulse) _impulse;
            var brand = BrandConfigSA.Instance.GetBrandByID(Conversion.CInt(Params.Get("BrandID")));
            var member = MailHandlerUtils.GetIMemberDTO(MemberID, MemberLoadFlags.None);
            var emailAddress = member.EmailAddress;
            var memberName = Params.Get("UserName");
            content.Append(string.Format("Brand: {0}\n\n", brand.Uri));
            content.Append(string.Format("Membername: {0}\n\n", memberName));
            content.Append(string.Format("MemberID: {0}\n\n", Params.Get("MemberID")));
            var displayName = (memberQuestionImpulse.IsAnonymous) ? "Anonymous" : "Give Member Credit";
            content.Append(string.Format("Display: {0}\n\n", displayName));
            content.Append(string.Format("Question: {0}\n\n", Params.Get("QuestionText")));
            content.Append(string.Format("Link to member profile: http://{0}/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&Ordinal=1&MemberID={1}", "www." + _brand.Site.Name, Params.Get("MemberID")));

            MessageInfo.FromName = memberName;
            MessageInfo.FromAddress = emailAddress;
            MessageInfo.ToAddress = "suggestions@" + _brand.Site.Name;
            MessageInfo.ToName = "Suggestions";
            MessageInfo.Subject = "Q&A Suggestion from " + memberName;
        }

        private IMemberDTO[] LoadMemberGroup()
        {
            //	Get an array of memberID ints
            var sMemberIDs = Params.Get("MemberIDList");
            var aMemberIDs = sMemberIDs.Split(new[] {','});
            var memberIDArray = new int[aMemberIDs.Length];
            for (var i = 0; i < memberIDArray.Length; i++)
            {
                memberIDArray[i] = Convert.ToInt32(aMemberIDs[i]);
            }

            //	Load up those members into a Member array
            IMemberDTO[] members;
            members = new IMemberDTO[memberIDArray.Length];
            for (var i = 0; i < memberIDArray.Length; i++)
            {
                members[i] = MailHandlerUtils.GetIMemberDTO(memberIDArray[i], MemberLoadFlags.None);
            }

            return (members);
        }

        #region Methods to pull selected data from members

        private string DetermineMemberUsernameText(IMemberDTO member, Brand brand)
        {
            var retVal = member.GetUserName(brand);

            return (retVal);
        }

        private string DetermineMemberAboutMe(IMemberDTO member, IMemberDTO viewingMember)
        {
            var Reason = string.Empty;

            if (DetermineLanguageFromBrandID(_brandID) == Language.Hebrew)
            {
                Reason = "הטקסט החופשי ממתין לאישור על-ידי צוות שירות הלקוחות.";
            }
            else
            {
                Reason = "My new essay is being approved by Customer Care. Check back soon to find out more about me.";
            }

            var retVal = member.GetAttributeTextApproved(_brand, "AboutMe", viewingMember.MemberID, Reason);
            if (retVal != null && retVal.Length > 30)
            {
                retVal = retVal.Substring(0, 27) + "...";
            }

            return (retVal);
        }

        private int DetermineListCount(Int32 memberID, HotListCategory listID)
        {
            if (_listCountCollection == null)
            {
                _listCountCollection = ListSA.Instance.GetListCounts(memberID, _brand.Site.Community.CommunityID, _brand.Site.SiteID);
            }

            return _listCountCollection[listID];
        }

        private string DetermineSearchUrl(Member.ServiceAdapters.Member member)
        {
            return DetermineSearchUrl(member.MemberID, _brand.Site.Community.CommunityID);
        }

        private string DetermineSearchUrl(Int32 memberID, Int32 communityID)
        {
            var sb = new StringBuilder();
            var prefs = SearchPreferencesSA.Instance.GetSearchPreferences(memberID, communityID);

            sb.Append("/Applications/Search/SearchResults.aspx?");

            for (var i = 0; i < SEARCH_FIELDS_FOR_URL.Length; i++)
            {
                if (i > 0)
                {
                    sb.Append("&");
                }
                sb.Append(HttpUtility.UrlEncode(SEARCH_FIELDS_FOR_URL[i]));
                sb.Append("=");
                sb.Append(HttpUtility.UrlEncode(prefs[SEARCH_FIELDS_FOR_URL[i]]));
            }

            //	Make the correct gender mask fields
            var genderMask = Matchnet.GenderUtils.FlipMaskIfHeterosexual(Convert.ToInt32(prefs["GenderMask"]));

            sb.Append("&GenderID=");
            sb.Append(GetGenderMaskSelf(genderMask));
            sb.Append("&SeekingGenderID=");
            sb.Append(GetGenderMaskSeeking(genderMask));

            return (sb.ToString());
        }


        /// <summary>
        ///     Compute the XOR values with salt given viewing memberID and being viewed memberID
        ///     Result is used for MMVID querystring parameter key in mail templates with links to view profile
        ///     MMVID - MatchMail Viewing Member ID
        /// </summary>
        /// <param name="recipientMiniprofileInfo">Viewing Member</param>
        /// <param name="memberID">MemberID being viewed</param>
        /// <returns>XOR value with salt</returns>
        private int GetMMVID(int recipientMemberID, int memberID)
        {
            return (recipientMemberID + XOR_SALT_ADD_VALUE) ^ (memberID);
        }


        /// <summary>
        ///     Remove any unneeded characters from the value (right now just tabs)
        /// </summary>
        /// <param name="theObject">object to convert to a sanitized string</param>
        /// <returns>sanitized version of the object's string representation</returns>
        private string Filter(object theObject)
        {
            var retVal = string.Empty;
            if (theObject != null)
            {
                retVal = theObject.ToString();
                retVal = retVal.Replace(TAB, string.Empty);
                retVal = retVal.Replace("\n", string.Empty);
                retVal = retVal.Replace("\r", string.Empty);
                retVal = retVal.Replace("'", "\\'");
                retVal = retVal.Replace("\"", "\\\"");
            }

            return (retVal);
        }


        private string DetermineMemberSeeking(IMemberDTO member)
        {
            var genderMask = member.GetAttributeInt(_brand, "GenderMask");

            var gender = GetGenderString(genderMask);
            var seeking = GetSeekingGenderString(genderMask);

            return gender + " " + SeekingText(_brandID) + " " + seeking;
        }


        private static string MaleText(int brandID)
        {
            var retVal = "Man";

            switch (DetermineLanguageFromBrandID(brandID))
            {
                case (Language.English):
                    retVal = "Man";
                    break;
                case (Language.French):
                    retVal = "Homme";
                    break;
                case (Language.German):
                    retVal = "[german male]";
                    break;
                case (Language.Hebrew):
                    retVal = "גבר";
                    break;
            }

            return (retVal);
        }


        private static string FemaleText(int brandID)
        {
            var retVal = "Woman";

            switch (DetermineLanguageFromBrandID(brandID))
            {
                case (Language.English):
                    retVal = "Woman";
                    break;
                case (Language.French):
                    retVal = "Femme";
                    break;
                case (Language.German):
                    retVal = "[german female]";
                    break;
                case (Language.Hebrew):
                    retVal = "אשה";
                    break;
            }

            return (retVal);
        }


        private static string MaleToFemaleText(int brandID)
        {
            var retVal = "[male turning female]";

            switch (DetermineLanguageFromBrandID(brandID))
            {
                case (Language.English):
                    retVal = "Male Turning Female";
                    break;
                case (Language.French):
                    retVal = "[french male turning female]";
                    break;
                case (Language.German):
                    retVal = "[german male turning female]";
                    break;
                case (Language.Hebrew):
                    retVal = "[hebrew male turning female]";
                    break;
            }

            return (retVal);
        }


        private static string FemaleToMaleText(int brandID)
        {
            var retVal = "[female turning male]";

            switch (DetermineLanguageFromBrandID(brandID))
            {
                case (Language.English):
                    retVal = "Female Turning Male";
                    break;
                case (Language.French):
                    retVal = "[french female turning male]";
                    break;
                case (Language.German):
                    retVal = "[german female turning male]";
                    break;
                case (Language.Hebrew):
                    retVal = "[hebrew female turning male]";
                    break;
            }

            return (retVal);
        }


        private static string SeekingText(int brandID)
        {
            var retVal = " seeking ";

            switch (DetermineLanguageFromBrandID(brandID))
            {
                case (Language.English):
                    retVal = " seeking ";
                    break;
                case (Language.French):
                    retVal = " Recherche un/une ";
                    break;
                case (Language.German):
                    retVal = " [german seeking] ";
                    break;
                case (Language.Hebrew):
                    retVal = " המחפש/ת ";
                    break;
            }

            return (retVal);
        }


        private string GetGenderString(int genderMask)
        {
            if ((genderMask & (Int32) GenderMask.Male) == (Int32) GenderMask.Male)
            {
                return MaleText(_brandID);
            }

            if ((genderMask & (Int32) GenderMask.Female) == (Int32) GenderMask.Female)
            {
                return FemaleText(_brandID);
            }

            if ((genderMask & (Int32) GenderMask.MTF) == (Int32) GenderMask.MTF)
            {
                return MaleToFemaleText(_brandID);
            }

            if ((genderMask & (Int32) GenderMask.FTM) == (Int32) GenderMask.FTM)
            {
                return FemaleToMaleText(_brandID);
            }

            // Else
            return "";
        }


        private string GetSeekingGenderString(int genderMask)
        {
            var seeking = "";

            if ((genderMask & (Int32) GenderMask.SeekingFemale) == (Int32) GenderMask.SeekingFemale)
            {
                seeking = FemaleText(_brandID);
            }

            if ((genderMask & (Int32) GenderMask.SeekingMale) == (Int32) GenderMask.SeekingMale)
            {
                if (seeking.Length > 0)
                {
                    seeking = seeking + ", ";
                }

                seeking = seeking + MaleText(_brandID);
            }

            if ((genderMask & (Int32) GenderMask.SeekingMTF) == (Int32) GenderMask.SeekingMTF)
            {
                if (seeking.Length > 0)
                    seeking = seeking + ", ";

                seeking = seeking + MaleToFemaleText(_brandID);
            }

            if ((genderMask & (Int32) GenderMask.SeekingFTM) == (Int32) GenderMask.SeekingFTM)
            {
                if (seeking.Length > 0)
                    seeking = seeking + ", ";

                seeking = seeking + FemaleToMaleText(_brandID);
            }

            return seeking;
        }


        public string GetPhotoDisapproveText(string reason, int language)
        {
            var retVal = reason;

            var reasonIndex = -1;
            for (var i = 0; i < REJECT_REASONS.Length; i++)
            {
                if (REJECT_REASONS[i].Equals(reason))
                {
                    reasonIndex = i;
                    break;
                }
            }

            if (reasonIndex != -1)
            {
                switch (language)
                {
                    case (int) Language.English:
                        retVal = ENGLISH_REJECT_REASONS[reasonIndex];
                        break;
                    case (int) Language.French:
                        retVal = FRENCH_REJECT_REASONS[reasonIndex];
                        break;
                    case (int) Language.German:
                        break;
                    case (int) Language.Hebrew:
                        retVal = HEBREW_REJECT_REASONS[reasonIndex];
                        break;
                    default:
                        retVal = ENGLISH_REJECT_REASONS[reasonIndex];
                        break;
                }
            }

            return (retVal);
        }

        /* parse ReportAbuse content string to get reporting Member EMail Address) 
		 *  Content string has email address "MemberID: 100046782 (email@spark.net)\"
		 * parse it out.
		*/

        private string getMemberEmail(string content)
        {
            var email = "";
            try
            {
                if (content == null || content == string.Empty)
                    return email;

                var indexFirst = content.IndexOf("(", 0);
                var indexLast = content.IndexOf(")", 0);

                if (indexFirst != 0 && indexLast != 0 && indexLast - indexFirst > 1)
                {
                    email = content.Substring(indexFirst + 1, indexLast - indexFirst - 1);
                }

                if (!isValidEmail(email))
                    email = "";
            }
            catch (Exception ex)
            {
                Log.LogError("StormPostGreyMail:getMemberEmail " + string.Format("Failed to parse email, original content is: {0}", content), ex, new CustomDataProvider(_brandID, _memberID, MessageInfo.ToAddress).Get());
            }

            return email;
        }

        private bool isValidEmail(string anEmailAddress)
        {
            var isValidEmail = true;
            var emailPattern = new Regex("^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$");

            if (!emailPattern.IsMatch(anEmailAddress))
            {
                isValidEmail = false;
            }

            return isValidEmail;
        }

        #endregion

        #endregion
    }
}