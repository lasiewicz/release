using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Threading;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.Impulse;

using Matchnet.Configuration.ServiceAdapters;


namespace Matchnet.ExternalMail.BusinessLogic
{
	public class Composer
	{
		public static Composer Instance = new Composer();

		private Composer()
		{

		}
		
		public MessageInfo Compose(ImpulseBase impulseBase)
		{
			MessageInfo retVal = null;

			try
			{
				string queryString = impulseBase.GetQueryString();
				// update this to only pass in impulse base obj
				StormPostGreyMail greyMail = new StormPostGreyMail(queryString, impulseBase); 
				retVal = greyMail.Process();
			} 
			catch (Exception ex)
			{
				string message = "Exception while generating MailInfo: " + ex.Message + " StackTrace: " + ex.StackTrace;
				EventLog.WriteEntry(Matchnet.ExternalMail.ValueObjects.ServiceConstants.SERVICE_NAME, message, EventLogEntryType.Warning);
				throw(ex);
			}

			return(retVal);
		}


	}
}
