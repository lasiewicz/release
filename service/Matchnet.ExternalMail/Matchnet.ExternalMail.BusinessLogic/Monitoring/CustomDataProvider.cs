﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;

namespace Matchnet.ExternalMail.BusinessLogic.Monitoring
{
    public class CustomDataProvider
    {
        private string _environment;
        private string _serviceName;
        private string _brandId;
        private string _memberId;
        private string _email;
        private Dictionary<string, string> _value; 
        public CustomDataProvider(int brandId = 0, int memberId = 0, string email = null)
        {
            
            _environment = GetEnvironment();
            _serviceName = Matchnet.ExternalMail.ValueObjects.ServiceConstants.SERVICE_NAME;
            _brandId = brandId > 0? brandId.ToString():null;
            _memberId = memberId > 0 ? memberId.ToString() : null;
            _email = email;
        }

        string GetEnvironment()
        {
            try
            {
                var environment = RuntimeSettings.GetSetting(SettingConstants.ENVIRONMENT_TYPE);
                switch (environment)
                {
                    case "dev":
                        return "dev";
                    case "stgv":
                    case "newstage3":
                        return "newstage3";
                    default:
                        return environment;
                }
            }
            catch (Exception e)
            {
                return "prod";
            }
        }
        public Dictionary<string, string> Get()
        {
            if (_value == null)
            {
                _value = new Dictionary<string, string>()
                {
                    {"Environment", _environment},
                    {"ServiceName", _serviceName}
                };
                if (_brandId != null)
                {
                    _value["BrandID"] = _brandId;
                }
                if (_memberId != null)
                {
                    _value["MemberID"] = _memberId;
                }
                if (_email != null)
                {
                    _value["Email"] = _email;
                }
            }
            return _value;
            
        } 
    }
}
