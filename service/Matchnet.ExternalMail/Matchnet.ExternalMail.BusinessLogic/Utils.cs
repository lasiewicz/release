using System;
using System.Text;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;
using Matchnet.Content.ValueObjects.AttributeOption;

namespace Matchnet.ExternalMail.BusinessLogic
{
	/// <summary>
	/// Utility type methods used by mail handlers
	/// </summary>
	public class MailHandlerUtils
	{
		public const int XOR_SALT_ADD_VALUE = 42;
		public static string[] SEARCH_FIELDS_FOR_URL = new string[]
			{
				"SearchTypeID", "Distance", "MinAge", "MaxAge",
				"MinHeight", "MaxHeight", "EducationLevel", "Religion",
				"Ethnicity", "SmokingHabits", "DrinkingHabits", "HasPhotoFlag",
				"MajorType", "RelationshipMask", "RelationShipStatus", "RegionID",
				"SearchOrderBy"
			};

        private static ISettingsSA _settingsService;

        public static ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService) _settingsService = RuntimeSettings.Instance;
                return _settingsService;
            }
            set { _settingsService = value; }
        }

		public static Brand GetBrand(int brandID)
		{
			return Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
		}

		
		public static string Filter(object theObject)
		{
			string retVal = string.Empty;
			if (theObject != null)
			{
				retVal = theObject.ToString();
				retVal = retVal.Replace("\n", string.Empty);
				retVal = retVal.Replace("\r", string.Empty);
				retVal = retVal.Replace("'", "\\'");
				retVal = retVal.Replace("\"", "\\\"");
				retVal = retVal.Replace("|", string.Empty);
			}

			return (retVal);
		}

		
		public static int GetMMVID(int recipientMemberID, int memberID)
		{
			return (recipientMemberID + XOR_SALT_ADD_VALUE) ^ (memberID);
		}

		
		public static string DetermineSearchUrl(Int32 memberID, Int32 communityID)
		{
			StringBuilder sb = new StringBuilder();
			SearchPreferenceCollection prefs = SearchPreferencesSA.Instance.GetSearchPreferences(memberID, communityID);

			sb.Append("/Applications/Search/SearchResults.aspx?");

			for (int i = 0; i < SEARCH_FIELDS_FOR_URL.Length; i++)
			{
				if (i > 0)
				{
					sb.Append("&");
				}
				sb.Append(HttpUtility.UrlEncode(SEARCH_FIELDS_FOR_URL[i]));
				sb.Append("=");
				sb.Append(HttpUtility.UrlEncode(prefs[SEARCH_FIELDS_FOR_URL[i]]));
			}

			//	Make the correct gender mask fields
			int genderMask = Matchnet.GenderUtils.FlipMaskIfHeterosexual(Convert.ToInt32(prefs["GenderMask"]));

			sb.Append("&GenderID=");
			sb.Append(GetGenderMaskSelf(genderMask));
			sb.Append("&SeekingGenderID=");
			sb.Append(GetGenderMaskSeeking(genderMask));

			return (sb.ToString());
		}

		
		public static int GetAge(DateTime birthDate)
		{
			return (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
		}

		
		public static string GetMatchText(MiniprofileInfo recipientProfile, MiniprofileInfo matchProfile, Brand brand)
		{
			int match = 0;
			StringBuilder text = new StringBuilder();
			string matchString = string.Empty;
			string returnValue = string.Empty;

			AppendMatchString(text, recipientProfile.EntertainmentLocation, matchProfile.EntertainmentLocation, "EntertainmentLocation", brand);
			AppendMatchString(text, recipientProfile.LeisureActivity, matchProfile.LeisureActivity, "LeisureActivity", brand);
			AppendMatchString(text, recipientProfile.Cuisine, matchProfile.Cuisine, "Cuisine", brand);
			AppendMatchString(text, recipientProfile.Music, matchProfile.Music, "Music", brand);
			AppendMatchString(text, recipientProfile.Reading, matchProfile.Reading, "Reading", brand);
			AppendMatchString(text, recipientProfile.PhysicalActivity, matchProfile.PhysicalActivity, "PhysicalActivity", brand);
			AppendMatchString(text, recipientProfile.Pets, matchProfile.Pets, "Pets", brand);
			
			if(text.Length == 0)
			{
				if(matchProfile.AboutMe.Length > 197)
				{
					text.Append(matchProfile.AboutMe.Substring(0, 197));
					text.Append("...");
					returnValue = text.ToString();
				}
				else
				{
					text.Append(matchProfile.AboutMe);
					returnValue = text.ToString();
				}
			}
			else
			{
				returnValue = "You both like: " + text.ToString();
				if (returnValue.Length > 255)
				{
					returnValue = returnValue.Substring(0, 252) + "...";
				}
			}
			
			return returnValue;
		}


		public static void AppendMatchString(StringBuilder sb, int attrValue, int attrValue2, string attrName, Brand brand)
		{
			if (attrValue > 0 && attrValue2 > 0)
			{
				string matchString = GetMaskContent(attrName, attrValue & attrValue2, brand);
				if(matchString.Length > 0)
				{
					if(sb.Length > 0)
					{
						sb.Append(", " + matchString);
					}
					else
					{
						sb.Append(matchString);
					}
				}
			}
		}

		
		public static string GetMaskContent(string attributeName, int maskValue, Brand brand)
		{
			AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);

			if (attributeOptionCollection == null)
				return string.Empty;

			StringBuilder sb = new StringBuilder();

			foreach (AttributeOption attributeOption in attributeOptionCollection)
			{
				if (attributeOption.MaskContains(maskValue))
				{
					if (sb.Length > 0)
					{
						sb.Append(", ");
					}
					sb.Append(attributeOption.Description);
				}
			}

			return sb.ToString();
		}

		
		public static int GetGenderMaskSelf(int genderMask)
		{
			int maskSelf = (Int32) GenderMask.Male + (Int32) GenderMask.Female + (Int32) GenderMask.MTF + (Int32) GenderMask.FTM;
			return genderMask & maskSelf;
		}


		public static int GetGenderMaskSeeking(int genderMask)
		{
			int maskSeeking = (Int32) GenderMask.SeekingMale + (Int32) GenderMask.SeekingFemale + (Int32) GenderMask.SeekingMTF + (Int32) GenderMask.SeekingFTM;
			return genderMask & maskSeeking;
		}

        public static IMemberDTO GetIMemberDTO(int memberid, MemberLoadFlags memberLoadFlags)
        {
            return GetIMemberDTO(MemberSA.Instance, memberid, memberLoadFlags);
        }

        public static IMemberDTO GetIMemberDTO(IGetMember memberService, int memberid, MemberLoadFlags memberLoadFlags)
        {
            string settingFromSingleton = SettingsService.GetSettingFromSingleton("ENABLE_MEMBER_DTO");
            bool isEnabled = (null != settingFromSingleton && Boolean.TrueString.ToLower().Equals(settingFromSingleton.ToLower()));
            if (isEnabled)
            {
                return memberService.GetMemberDTO(memberid, MemberType.MatchMail);
            }
            else
            {
                return memberService.GetMember(memberid, memberLoadFlags);
            }
        }
	}
}
