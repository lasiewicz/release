using System;
using Matchnet.Caching;

namespace Matchnet.ExternalMail.BusinessLogic
{
	/// <summary>
	/// Summary description for CacheableString.
	/// </summary>
	public class CacheableString : ICacheable
	{
		private string _myValue;
		private int _cacheTTLSeconds = 600;
		private string _cacheKey;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="myValue"></param>
		public CacheableString(string myValue, string cacheKey)
		{
			_myValue = myValue;
			_cacheKey = cacheKey;
		}

		/// <summary>
		/// 
		/// </summary>
		public string MyValue
		{
			get { return(_myValue); }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return(_myValue);
		}


		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return(_cacheTTLSeconds);
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				// TODO:  Add CacheableString.CacheMode getter implementation
				return new Matchnet.CacheItemMode ();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				// TODO:  Add CacheableString.CachePriority getter implementation
				return new Matchnet.CacheItemPriorityLevel ();
			}
			set
			{
				// TODO:  Add CacheableString.CachePriority setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return(_cacheKey);
		}

		#endregion
	}
}
