using System;
using System.Data;

using Matchnet.Data;
using Matchnet.Exceptions;

using Matchnet.ExternalMail.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects.ServiceDefinitions;
using Matchnet.Member.ValueObjects.Interfaces;

namespace Matchnet.ExternalMail.BusinessLogic
{
	/// <summary>
	/// Summary description for SMSAlertsBL.
	/// </summary>
	public class SMSAlertsBL
	{
		private const int MAX_EMAIL_ALERT_COUNT = 5;

		public static readonly SMSAlertsBL Instance = new SMSAlertsBL();

		private SMSAlertsBL()
		{
		}

		public bool NewInboxItemReceived(int memberID, int siteID, SMSAlertsDefinitions.MessageType messageType, bool isFriend)
		{
			bool sendSMS = false;
			
			try
			{
				Command command = new Command("mnXmail", "up_SMSAlerts_List", 0);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);

				DataTable dt = Matchnet.Data.Client.Instance.ExecuteDataTable(command);

				if(dt.Rows.Count>0)
				{
					int brandID = Convert.ToInt32(dt.Rows[0]["BrandID"]);
					int emailCount = Convert.ToInt32(dt.Rows[0]["EmailCount"]);
					int ecardCount = Convert.ToInt32(dt.Rows[0]["EcardCount"]);
					int flirtCount = Convert.ToInt32(dt.Rows[0]["FlirtCount"]);
					int sentCount = Convert.ToInt32(dt.Rows[0]["SentCount"]);

					Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);

					//if(messageType == SMSAlertsDefinitions.MessageType.Email)
					//{
						sendSMS = CheckSMSSend(memberID, brand, sentCount, isFriend, messageType);
					//}
					if(CheckSMSCount(brand))
					{
						switch(messageType)
						{
							case SMSAlertsDefinitions.MessageType.Email :
								emailCount++;
								if(sendSMS)
									sentCount++;
								break;
							case SMSAlertsDefinitions.MessageType.Ecard :
								ecardCount++;
								if(sendSMS)
									sentCount++;
								break;
							case SMSAlertsDefinitions.MessageType.Flirt :
								flirtCount++;
								if(sendSMS)
									sentCount++;
								break;
						}

						Command updtCommand = new Command("mnXmail", "up_SMSAlerts_UpdateCounters", 0);
						updtCommand.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
						updtCommand.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
						updtCommand.AddParameter("@EmailCount", SqlDbType.Int, ParameterDirection.Input, emailCount);
						updtCommand.AddParameter("@EcardCount", SqlDbType.Int, ParameterDirection.Input, ecardCount);
						updtCommand.AddParameter("@FlirtCount", SqlDbType.Int, ParameterDirection.Input, flirtCount);
						updtCommand.AddParameter("@SentCount", SqlDbType.Int, ParameterDirection.Input, sentCount);

						//Matchnet.Data.Client.Instance.ExecuteNonQuery(updtCommand);
						Client.Instance.ExecuteAsyncWrite(updtCommand);
					}
				}
				else
				{
					throw new BLException(string.Format("No records was found in SMSAlerts table. MemberID = {0}; SiteID = {1}", memberID.ToString(), siteID.ToString()));
				}

			}
			catch (Exception ex) 
			{
				throw new BLException("NewInboxItemReceived() failed.", ex);
			}
			return sendSMS;
		}

		public bool CheckSaturday(int memberID, Brand brand)
		{
            IMemberDTO aMember = MailHandlerUtils.GetIMemberDTO(memberID, MemberLoadFlags.None);
			int prefs = aMember.GetAttributeInt(brand, "SMSAlertPreference");
			bool enableOnSaturday = ((prefs & (int)SMSAlertsDefinitions.SMSAlertsPrefs.AllowSaturday) == (int)SMSAlertsDefinitions.SMSAlertsPrefs.AllowSaturday);
			return (enableOnSaturday || !IsSaturday(brand));
		}
		private bool CheckSMSSend(int memberID, Brand brand, int sentCount, bool isFriend, SMSAlertsDefinitions.MessageType messageType)
		{
			bool result = false;
            IMemberDTO aMember = MailHandlerUtils.GetIMemberDTO(memberID, MemberLoadFlags.None);

			bool isConfirmed = aMember.GetAttributeBool(brand, "SMSPhoneIsValidated");
			int prefs = aMember.GetAttributeInt(brand, "SMSAlertPreference");
			bool enableOnSaturday = ((prefs & (int)SMSAlertsDefinitions.SMSAlertsPrefs.AllowSaturday) == (int)SMSAlertsDefinitions.SMSAlertsPrefs.AllowSaturday);
			bool isPhase2User = ((prefs & (int)SMSAlertsDefinitions.SMSAlertsPrefs.PhaseTwoUser) == (int)SMSAlertsDefinitions.SMSAlertsPrefs.PhaseTwoUser);

			if(sentCount < MAX_EMAIL_ALERT_COUNT) // If number of sent messages not exceeded the limit
			{
				if(isConfirmed) // If the member has SMS Alerts features enabled
				{
					if(IsLegitHour(brand)) // If we're in between legit hours to send alerts
					{
						if(CheckSaturday(memberID, brand)) // If enabled on Sat OR if not and it's not Sat
						{
							if(isPhase2User)
							{
								result = true;
							}
							else
							{
								if(messageType == SMSAlertsDefinitions.MessageType.Email)
								{
									// If the member wants to be alerted on every incoming email
									if((prefs & (int)SMSAlertsDefinitions.SMSAlertsPrefs.NewEmailReceived) == (int)SMSAlertsDefinitions.SMSAlertsPrefs.NewEmailReceived)
									{
										result = true;
									}
										// If the member wants to be alerted only on email from friends
									else
									{
										if(isFriend && ((prefs & (int)SMSAlertsDefinitions.SMSAlertsPrefs.NewEmailFromFriends) == (int)SMSAlertsDefinitions.SMSAlertsPrefs.NewEmailFromFriends))
										{
											result = true;
										}
									}
								}
							}
						}
					}
				}
			}

			return result;
		}
		private bool CheckSMSCount(Brand brand)
		{
			return IsCountHour(brand);
		}

		// Checks if it's Saturday or any other Holyday
		private bool IsSaturday(Brand brand)
		{
			bool result = false;
			string hoursRange = RuntimeSettings.GetSetting("SMS_ALERTS_SABBATH_HOURS_RANGE", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
			
			DateTime localDateTime = GetLocalDateTime(brand);
			

			int fromHH = int.Parse(hoursRange.Substring(0,2));
			int fromMM = int.Parse(hoursRange.Substring(2,2));
			int toHH = int.Parse(hoursRange.Substring(5,2));
			int toMM = int.Parse(hoursRange.Substring(7,2));
			
			int nowHH = localDateTime.Hour;
			int nowMM = localDateTime.Minute;

			if(localDateTime.DayOfWeek == System.DayOfWeek.Friday)
			{
				if(nowHH >= fromHH)
				{
					result = true;
					if(nowHH==fromHH && nowMM<fromMM)
					{
						result = false;
					}
				}
			}
			if(localDateTime.DayOfWeek == System.DayOfWeek.Saturday)
			{
				if(nowHH <= toHH)
				{
					result = true;
					if(nowHH == toHH && nowMM>toMM)
					{
						result = false;
					}
				}
			}

			// If it's not Saturday
			if(result == false) 
			{
				// We'll check if it's any other religious day
				string religiousDates = RuntimeSettings.GetSetting("SMS_ALERTS_RELIGIOUS_DAYS", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
				DataRow religiousDate = CheckReligiousDate(localDateTime, religiousDates);
				if(religiousDate!=null)
				{
					if(localDateTime.ToShortDateString() == ((DateTime)religiousDate[0]).ToShortDateString())
					{
						if(nowHH >= fromHH)
						{
							result = true;
							if(nowHH==fromHH && nowMM<fromMM)
							{
								result = false;
							}
						}
					}
					else
					{
						if(localDateTime.ToShortDateString() == ((DateTime)religiousDate[1]).ToShortDateString())
						{
							if(nowHH <= toHH)
							{
								result = true;
								if(nowHH == toHH && nowMM>toMM)
								{
									result = false;
								}
							}
						}
						else
						{
							// It's somewhere between the two
							result = true;
						}
					}
				}
			}

			return result;
		}

		public DataRow CheckReligiousDate(DateTime localDateTime, string religiousDates)
		{
			DataRow result = null;
			if(religiousDates!=null && religiousDates!=string.Empty)
			{
				DataTable datesDT = new DataTable();
				datesDT.Columns.Add("DateFrom", typeof(DateTime));
				datesDT.Columns.Add("DateTo", typeof(DateTime));
				string[] dates = religiousDates.Split(new char[] {','});
				foreach (string date in dates)
				{
					DateTime dateFrom = DateTime.Parse(date.Split(new char[] {'-'})[0]);
					DateTime dateTo = dateFrom.AddDays(int.Parse(date.Split(new char[] {'-'})[1]));
					datesDT.Rows.Add(new object[] {dateFrom, dateTo});
				}

				DataRow[] rows = datesDT.Select("'" + localDateTime.ToShortDateString() + "' >= DateFrom AND DateTo >= '" + localDateTime.ToShortDateString() + "'");
				if(rows.Length >0)
				{
					result = rows[0];
				}
			}
			return result;
		}
		private bool IsLegitHour(Brand brand)
		{
			return IsBetweenHours("SMS_ALERTS_HOURS_RANGE", brand);
		}
		private bool IsCountHour(Brand brand)
		{
			return IsBetweenHours("SMS_ALERTS_COUNT_HOURS_RANGE", brand);
		}
		private bool IsBetweenHours(string settingConst, Brand brand)
		{
			bool result = false;
			string hoursRange = RuntimeSettings.GetSetting(settingConst, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
			int fromHH = int.Parse(hoursRange.Substring(0,2));
			int fromMM = int.Parse(hoursRange.Substring(2,2));
			int toHH = int.Parse(hoursRange.Substring(5,2));
			int toMM = int.Parse(hoursRange.Substring(7,2));
			DateTime localDateTime = GetLocalDateTime(brand);
			int nowHH = localDateTime.Hour;
			int nowMM = localDateTime.Minute;
			
			if(nowHH >= fromHH)
			{
				if(nowHH==fromHH && nowMM<fromMM)
				{
					result = false;
				}
				else
				{
					if(nowHH <= toHH)
					{
						if(nowHH== toHH && nowMM >toMM)
						{
							result = false;
						}
						else
						{
							result = true;
						}
					}
				}
			}
			return result;
		}
		
		private DateTime GetLocalDateTime(Brand brand)
		{
			DateTime result = DateTime.Now;

			int hoursToSubstruct = int.Parse(RuntimeSettings.GetSetting("SMS_ALERTS_COUNT_HOURS_TO_SUBSTRUCT", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
			result = result.AddHours(hoursToSubstruct);

			return result;
		}
		public void UpdatePhonesList(int memberID, int brandID, string phoneNumber, SMSAlertsDefinitions.Action action)
		{
			try 
			{
				Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);

				string commandName = "up_SMSAlerts_";
				Command command = new Command("mnXmail", commandName, 0);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, brand.Site.SiteID);
				
				switch(action)
				{
					case SMSAlertsDefinitions.Action.Insert:

						string summaryHour = RuntimeSettings.GetSetting("SMS_ALERTS_SUMMERY_HOUR", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
						int summaryHH = int.Parse(summaryHour.Substring(0,2));
						int summaryMM = int.Parse(summaryHour.Substring(2,2));

						DateTime lastAttempDate = DateTime.Now;
						lastAttempDate = lastAttempDate.Add(new TimeSpan(-1, -lastAttempDate.Hour, -lastAttempDate.Minute, -lastAttempDate.Second));
						lastAttempDate = lastAttempDate.AddHours(summaryHH);
						lastAttempDate = lastAttempDate.AddMinutes(summaryMM);
						command.StoredProcedureName+="Add";
						command.AddParameter("@PhoneNumber", SqlDbType.NVarChar, ParameterDirection.Input, phoneNumber);
						command.AddParameter("@BrandID", SqlDbType.Int, ParameterDirection.Input, brand.BrandID);
						command.AddParameter("@LastAttemptDate", SqlDbType.SmallDateTime, ParameterDirection.Input, lastAttempDate);
						break;
					case SMSAlertsDefinitions.Action.Remove:
						command.StoredProcedureName+="Delete";
						break;
					case SMSAlertsDefinitions.Action.Update:
						command.StoredProcedureName+="Update";
						command.AddParameter("@PhoneNumber", SqlDbType.VarChar, ParameterDirection.Input, phoneNumber);
						break;
				}
				
				// Matchnet.Data.Client.Instance.ExecuteNonQuery(command);
				Client.Instance.ExecuteAsyncWrite(command);
			}
			catch (Exception ex) 
			{
				throw new BLException("UpdatePhonesList() failed.", ex);
			}
		}

		public bool IsPhoneAvailable(int siteID, string phoneNumber)
		{
			bool phonaAvailable = false;
			try
			{
				
				Command command = new Command("mnXmail", "up_SMSAlerts_IsPhoneInUse", 0);
				command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
				command.AddParameter("@PhoneNumber", SqlDbType.NVarChar, ParameterDirection.Input, phoneNumber);

				DataTable dt =Matchnet.Data.Client.Instance.ExecuteDataTable(command);
				if(dt.Rows.Count==0)
				{
					phonaAvailable = true;
				}
			}
			catch (Exception ex) 
			{
				throw new BLException("IsPhoneAvailable() failed.", ex);
			}
			return phonaAvailable;
		}

		public bool IsSummaryAllowed(int memberID, Brand brand)
		{
            IMemberDTO aMember = MailHandlerUtils.GetIMemberDTO(memberID, MemberLoadFlags.None);
			int prefs = aMember.GetAttributeInt(brand, "SMSAlertPreference");
			bool allowSummary = ((prefs & (int)SMSAlertsDefinitions.SMSAlertsPrefs.AllowSummary) == (int)SMSAlertsDefinitions.SMSAlertsPrefs.AllowSummary);
			bool phaseTwoUser = ((prefs & (int)SMSAlertsDefinitions.SMSAlertsPrefs.PhaseTwoUser) == (int)SMSAlertsDefinitions.SMSAlertsPrefs.PhaseTwoUser);
			return (allowSummary || phaseTwoUser);
		}
	}
}
