using System;
using System.Diagnostics;
using Matchnet.ActivityRecording.ServiceAdapters;
using Matchnet.Exceptions;
using System.Web;
using System.Text;
using System.Globalization; 


using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.BulkMail;
using Matchnet.Member.ValueObjects.Interfaces;
using MSA = Matchnet.Member.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;

using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.AttributeOption;

using Matchnet.ExternalMail.BusinessLogic.YesMailWebService;
using System.Collections.Specialized;
using System.Collections;
using Matchnet.ExternalMail.ValueObjects.StormpostSoap;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;

using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;

using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.EmailLibrary;
using Matchnet.Email.ServiceAdapters;
using Matchnet.ExternalMail.BusinessLogic.Monitoring;
using Spark.Logger;
using Spark.Logging;



namespace Matchnet.ExternalMail.BusinessLogic
{
	/// <summary>
	/// Handles all mail types going to YesMail. SOAP & Bulk mail. Partially refactored to handle Trigger Mails for multiple providers. Bulk Mails go through YesMail.
	/// </summary>
	public class MailHandler : IMailHandler
	{
		#region CTOR

		public MailHandler(string configFile, IMailBL mailBl)
		{
		    _configFile = configFile;
			_mapper = new MailTemplateMapper(configFile);
		    _mailBl = mailBl;
		}

		#endregion

		#region Constants
        private const string CLASS_NAME = "MailHandler";
		private const Int32 EMAIL_VERIFIED = 4;
		private const int XOR_SALT_ADD_VALUE = 42;
	    private const string FIRST_NAME_ATTR = "SiteFirstName";
	    private IMailBL _mailBl;
		private string[] SEARCH_FIELDS_FOR_URL = new string[]
			{
				"SearchTypeID", "Distance", "MinAge", "MaxAge",
				"MinHeight", "MaxHeight", "EducationLevel", "Religion",
				"Ethnicity", "SmokingHabits", "DrinkingHabits", "HasPhotoFlag",
				"MajorType", "RelationshipMask", "RelationShipStatus", "RegionID",
				"SearchOrderBy"
			};

		#endregion

		#region Member

		private ITemplateMapper _mapper;
		
		private static Object _syncBlock = new object();
	    private string _configFile;
        private static readonly ISparkLogger Log = SparkLoggerManager.Singleton.GetSparkLogger(typeof(MailHandler));
		#endregion

		#region PUBLIC METHOD

		public void SendEmail(ImpulseBase impulse)
		{
            var tags = new CustomDataProvider(impulse.BrandID).Get();
            Log.LogInfoMessage(string.Format("ClassName:{0}, Entering Method {1}",CLASS_NAME, "SendEmail"),tags);
		    Brand brand = BrandConfigSA.Instance.GetBrandByID(impulse.BrandID);
		    if (brand != null)
		    {
		        int communityid = brand.Site.Community.CommunityID;
		        int siteid = brand.Site.SiteID;


		        if (
		            Conversion.CBool(
		                Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MIGRATION_MINGLE_EMAIL_BLOCK",
		                    communityid, siteid, brand.BrandID)))
		        {
		            if ((impulse is PhotoApprovedImpulse)
		                || (impulse is PhotoRejectedImpulse)
		                || (impulse is PhotoCaptionRejectedImpulse))
		            {
		                return;
		            }
		        }
		    }

		    if (impulse is PhotoApprovedImpulse)
		        this.SendPhotoApproval(impulse);
		    else if (impulse is SendMemberImpulse)
		        this.SendToFriend(impulse);
		    else if (impulse is InternalMailNotifyImpulse)
		        this.SendNewMailAlert(impulse);
		    else if (impulse is MutualYesNotifyImpulse)
		        this.SendMutualMail(impulse);
		    else if (impulse is PhotoRejectedImpulse)
		        this.SendPhotoRejection(impulse);
		    else if (impulse is OptOutNotifyImpulse)
		        this.SendOptOutNotification(impulse);
		    else if (impulse is HotListedNotifyImpulse)
		        this.SendHotListAlert(impulse);
		    else if (impulse is SubscriptionConfirmationImpulse)
		        this.SendSubscriptionConfirmation(impulse);
		    else if (impulse is RegVerificationImpulse)
		        this.SendActivationLetter(impulse);
		    else if (impulse is PasswordMailImpulse)
		        this.SendForgotPassword(impulse);
		    else if (impulse is ResetPasswordImpulse)
		        this.SendResetPassword(impulse);
		    else if (impulse is EmailVerificationImpulse)
		        this.SendEmailChangeConfirmation(impulse);
		    else if (impulse is MessageBoardInstantNotificationImpulse)
		        this.SendMessageBoardInstantNotification(impulse);
		    else if (impulse is MessageBoardDailyUpdatesImpulse)
		        this.SendMessageBoardDailyUpdates(impulse);
		    else if (impulse is EcardToFriendImpulse)
		        this.SendEcardToFriend(impulse);
		    else if (impulse is RenewalFailureNotificationImpulse)
		        this.SendRenewalFailureNotification(impulse);
		    else if (impulse is MatchMailImpulse) // this should never trigger since Match mail is handled by BulkMail svc only
		        this.SendMatchMail(impulse);
		    else if (impulse is ViralImpulse)
		        this.SendClickMail(impulse);
		    else if (impulse is EcardToMemberImpulse)
		        this.SendEcardToMember(impulse);
		    else if (impulse is PmtProfileConfirmationImpulse)
		        this.sendPaymentProfileNotification(impulse);
		    else if (impulse is MatchMeterInvitationMailImpulse)
		        this.sendMatchMeterInvitationMailImpulse(impulse);
		    else if (impulse is MatchMeterCongratsMailImpulse)
		        this.sendMatchMeterCongratsMailImpulse(impulse);
		    else if (impulse is NewMemberMailImpulse) // this should never trigger since New Member mail is handled by BulkMail svc only
		        this.SendNewMemberMail(impulse);
		    else if (impulse is PhotoCaptionRejectedImpulse)
		        this.SendPhotoCaptionRejectedMail(impulse);
		    else if (impulse is SendMemberColorCodeImpulse)
		        this.SendMemberColorCode(impulse);
		    else if (impulse is SendMemberColorCodeQuizInviteImpulse)
		        this.SendMemberColorCodeQuizInvite(impulse);
		    else if (impulse is ColorCodeQuizConfirmationImpulse)
		        this.SendColorCodeQuizConfirmation(impulse);
		    else if (impulse is SendMemberQuestionAndAnswerImpulse)
		        this.SendMemberQuestion(impulse);
		    else if (impulse is OneOffPurchaseConfirmationImpulse)
		        this.SendOneOffPurchaseConfirmation(impulse);
		    else if (impulse is JDateMobileRegistrationConfirmationImpulse)
		        this.SendJDateMobileRegistrationConfirmation(impulse);
		    else if (impulse is AllAccessEmailNudgeImpulse)
		        this.SendAllAccessNudgeEmail(impulse);
		    else if (impulse is AllAccessInitialEmailImpulse)
		        this.SendAllAccessInitialEmail(impulse);
		    else if (impulse is AllAccessReplyEmailImpulse)
		        this.SendAllAccessReplyEmail(impulse);
		    else if (impulse is AllAccessNoThanksEmailImpulse)
		        this.SendAllAccessNoThanksEmail(impulse);
		    else if (impulse is AllAccessReturnEmailImpulse)
		        this.SendAllAccessReturnEmail(impulse);
		    else if (impulse is BOGONotificationImpulse)
		    {   //right now there is only 1 scheduled email that YM handles (Mingle does regrecapture),
		        //for the future will create method or case
		        this.SendBOGONotification(impulse);
		    }
		    // We do not need reg capture emails for now
		    // else if (impulse is ScheduledNotificationImpulse)
		    //    this.SendScheduledNotification(impulse);
		    else if (impulse is ViewedYouEmailImpulse)
		        this.SendViewedYouEmail(impulse);
		    else if (impulse is BBEPasscodeImpulse)
		        this.SendBBEPasscodeEmail(impulse);
		    else if (impulse is SodbPhotoUploadImpulse)
		        this.SendSodbPhotoUploadEmail(impulse);
		    else if (impulse is NoEssayRequestEmailImpulse)
		        this.SendNoEssayRequestEmail(impulse);
		    else if (impulse is ProfileChangedConfirmationImpulse)
		        this.SendProfileChangedConfirmation(impulse);
		    else
                Log.LogWarningMessage("Unknown Impulse Type " + impulse.GetType().Name, tags);

            Log.LogInfoMessage(string.Format("ClassName:{0}, Leaving Method {1}", CLASS_NAME, "SendEmail"), tags);

		}
        
	    #region Ported over from GrayMail	
		
		/// <summary>
		/// Send photo approval email.
		/// </summary>
		/// <param name="impulse"></param>
		public void SendPhotoApproval(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			TokenConstructor tc = new TokenConstructor();
			PhotoApprovedImpulse imp = (PhotoApprovedImpulse)impulse;
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.None);
            var firstName = member.GetAttributeText(brand, FIRST_NAME_ATTR);
            tc.Add(TokenNames.FIRST_NAME, firstName);

			tc.Add(TokenNames.EMAIL_ADDR, member.EmailAddress);
			_mailBl.SendTriggerMail(imp.MemberID, brand.Site.SiteID, imp.BrandID, 
				_mapper.GetProviderTemplateID(brand.Site, EmailType.PhotoApproval), 
				member.EmailAddress, tc.BuildList(), null, EmailType.PhotoApproval);
																	
		}
		/// <summary>
		/// Send  SendPhotoCaptionRejectedMail email.
		/// </summary>
		/// <param name="impulse"></param>
		public void SendPhotoCaptionRejectedMail(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			TokenConstructor tc = new TokenConstructor();
			PhotoCaptionRejectedImpulse imp = (PhotoCaptionRejectedImpulse)impulse;
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.None);
            var firstName = member.GetAttributeText(brand, FIRST_NAME_ATTR);
            tc.Add(TokenNames.FIRST_NAME, firstName);

			tc.Add(TokenNames.EMAIL_ADDR, member.EmailAddress);
            _mailBl.SendTriggerMail(imp.MemberID, brand.Site.SiteID, imp.BrandID, 
				_mapper.GetProviderTemplateID(brand.Site, EmailType.PhotoCaptionRejection),
                member.EmailAddress, tc.BuildList(), null, EmailType.PhotoApproval);
																	
		}

		/// <summary>
		/// Send member to friend email.  Friend can be off site.
		/// </summary>
		/// <param name="impulse"></param>
		public void SendToFriend(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			TokenConstructor tc = new TokenConstructor();
			SendMemberImpulse imp = (SendMemberImpulse)impulse;
			
			tc.Add(TokenNames.SEND_TO_FRIEND_BODY, imp.Message);
			tc.Add(TokenNames.S2F_FRIENDS_NAME, imp.FriendName);	
			tc.Add(TokenNames.S2F_MEMBER_ID, imp.SentMemberID.ToString());

            _mailBl.SendTriggerMail(imp.SentMemberID, brand.Site.SiteID, imp.BrandID, 
				_mapper.GetProviderTemplateID(brand.Site, EmailType.SendToFriend),
                imp.FriendEmail, tc.BuildList(), null, EmailType.SendToFriend);

            try
            {
                var captionString = string.Format("<Mail><Recipient>{0}</Recipient><SendToFriend><MemberID>{1}</MemberID></SendToFriend></Mail>",
                        imp.FriendEmail, imp.SentMemberID.ToString());
                ActivityRecordingSA.Instance.RecordActivity(imp.SendingMemberID, Constants.NULL_INT, brand.Site.SiteID, ActivityRecording.ValueObjects.Types.ActionType.SendToFriend,
                   ActivityRecording.ValueObjects.Types.CallingSystem.ExternalMail, captionString);
            }
            catch (Exception ex)
            {
                Log.LogError("SendToFriend threw an exception",ex, new CustomDataProvider(impulse.BrandID,imp.SentMemberID,imp.FriendEmail).Get());
            }
		}

		/// <summary>
		/// Send member's color code profile to friend
		/// </summary>
		/// <param name="impulse"></param>
		public void SendMemberColorCode(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			TokenConstructor tc = new TokenConstructor();
			SendMemberColorCodeImpulse imp = (SendMemberColorCodeImpulse)impulse;
			
			//message
			tc.Add(TokenNames.SEND_TO_FRIEND_BODY, imp.Message);
			//friend's name
			tc.Add(TokenNames.FIRST_NAME, imp.FriendName);	
			//sender's name
			tc.Add(TokenNames.S2F_FRIENDS_NAME, imp.SenderName.ToString());
			//sender's memberid
			tc.Add(TokenNames.S2F_MEMBER_ID, imp.SenderMemberID.ToString());

            _mailBl.SendTriggerMail(imp.SenderMemberID, brand.Site.SiteID, imp.BrandID, 
				_mapper.GetProviderTemplateID(brand.Site, EmailType.SendMemberColorCode),
                imp.FriendEmail, tc.BuildList(), null, EmailType.SendMemberColorCode);
		}

		/// <summary>
		/// Send member confirmation on color code quiz
		/// </summary>
		/// <param name="impulse"></param>
		public void SendColorCodeQuizConfirmation(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			TokenConstructor tc = new TokenConstructor();
			ColorCodeQuizConfirmationImpulse imp = (ColorCodeQuizConfirmationImpulse)impulse;
			
			//firstname (member username)
			tc.Add(TokenNames.FIRST_NAME, imp.MemberUserName);
			//boardID (color)
			tc.Add(TokenNames.BOARD_ID, imp.Color.ToString());


            _mailBl.SendTriggerMail(imp.MemberID, brand.Site.SiteID, imp.BrandID,
				_mapper.GetProviderTemplateID(brand.Site, EmailType.ColorCodeQuizConfirmation),
                imp.EmailAddress, tc.BuildList(), null, EmailType.ColorCodeQuizConfirmation);
		}

		/// <summary>
		/// Send color code quiz invite
		/// </summary>
		/// <param name="impulse"></param>
		public void SendMemberColorCodeQuizInvite(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			TokenConstructor tc = new TokenConstructor();
			SendMemberColorCodeQuizInviteImpulse imp = (SendMemberColorCodeQuizInviteImpulse)impulse;
			
			//recipient name (username)
			tc.Add(TokenNames.FIRST_NAME, imp.RecipientUserName.ToString());	
			//sender's user name
			tc.Add(TokenNames.S2F_FRIENDS_NAME, imp.SenderUserName.ToString());
			//sender's memberid
			tc.Add(TokenNames.S2F_MEMBER_ID, imp.SenderMemberID.ToString());

            _mailBl.SendTriggerMail(imp.SenderMemberID, brand.Site.SiteID, imp.BrandID,
				_mapper.GetProviderTemplateID(brand.Site, EmailType.SendMemberColorCodeQuizInvite),
                imp.RecipientEmail, tc.BuildList(), null, EmailType.SendMemberColorCodeQuizInvite);
		}

		/// <summary>
		/// Send new mail alert email.
		/// </summary>
		/// <param name="impulse"></param>
		public void SendNewMailAlert(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			InternalMailNotifyImpulse imp = (InternalMailNotifyImpulse)impulse;
            IMemberDTO toMember = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.None);
            IMemberDTO fromMember = MailHandlerUtils.GetIMemberDTO(imp.SentMemberID, MemberLoadFlags.None);

			bool send = true;

			AttributeGroup group = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(brand.Site.Community.CommunityID, EmailAlertConstant.ATTRIBUTE_ID);
			
			int mask = (int)EmailAlertMask.NewEmailAlert;
			int userSetting = toMember.GetAttributeInt(brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, int.Parse(group.DefaultValue));
			if((mask & userSetting) != mask)
			{
				send = false;
			}

			if(send)
			{
				TokenConstructor tc = new TokenConstructor();
                var firstName = toMember.GetAttributeText(brand, FIRST_NAME_ATTR);
                tc.Add(TokenNames.FIRST_NAME, firstName);
				tc.Add(TokenNames.USER_NAME, toMember.GetUserName(brand));
				tc.Add(TokenNames.NUM_UNREAD_MESSAGES, imp.UnreadEmailCount.ToString());
				tc.Add(TokenNames.NEW_MAIL_TIME, EmailMemberHelper.GetDateDisplay(imp.InsertDate, brand));
				tc.Add(TokenNames.FROM_MEMBER_AGE, EmailMemberHelper.DetermineMemberAge(fromMember, brand).ToString());
				tc.Add(TokenNames.FROM_MEMBER_LOCATION, EmailMemberHelper.DetermineMemberRegionDisplay(fromMember, brand));
				tc.Add(TokenNames.FROM_MEMBER_NAME, fromMember.GetUserName(brand));
				tc.Add(TokenNames.FROM_MEMBER_SEEKING, EmailMemberHelper.DetermineMemberSeeking(brand, fromMember));
				tc.Add(TokenNames.FROM_MEMBER_THUMBNAIL, EmailMemberHelper.DetermineMemberThumbnail(fromMember, brand));
				tc.Add(TokenNames.MEMBER_MAIL_ID, imp.EmailID.ToString());

                _mailBl.SendTriggerMail(toMember.MemberID, brand.Site.SiteID, imp.BrandID, 
					_mapper.GetProviderTemplateID(brand.Site, EmailType.NewMailAlert),
                    toMember.EmailAddress, tc.BuildList(), null, EmailType.NewMailAlert);
			}
		}

		/// <summary>
		/// Send mutual (both cliqued) email.
		/// </summary>
		/// <param name="impulse"></param>
		public void SendMutualMail(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			TokenConstructor tc = new TokenConstructor();
			MutualYesNotifyImpulse imp = (MutualYesNotifyImpulse)impulse;
			
			// This is the member who initiated the click.
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.None);
			
			// This is the member who clicked back.
            IMemberDTO mutualMember = MailHandlerUtils.GetIMemberDTO(imp.TargetMemberID, MemberLoadFlags.None);

			bool send = true;

			AttributeGroup group = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(brand.Site.Community.CommunityID, EmailAlertConstant.ATTRIBUTE_ID);

			int mask = (int)EmailAlertMask.GotClickAlert;
			int userSetting = member.GetAttributeInt(brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, int.Parse(group.DefaultValue));
			if((mask & userSetting) != mask)
			{
				send = false;
			}

			if(send)
			{
                var firstName = member.GetAttributeText(brand, FIRST_NAME_ATTR);
                tc.Add(TokenNames.FIRST_NAME, firstName);
				tc.Add(TokenNames.USER_NAME, member.GetUserName(brand));
				tc.Add(TokenNames.FROM_MEMBER_NAME, member.GetUserName(brand)); 
				tc.Add(TokenNames.MUTUAL_MEMBER_NAME_TEXT, mutualMember.GetUserName(brand));
				tc.Add(TokenNames.MUTUAL_MEMBER_USER_NAME, mutualMember.GetUserName(brand));
				tc.Add(TokenNames.MUTUAL_MEMBER_ID, mutualMember.MemberID.ToString());
				tc.Add(TokenNames.MUTUAL_MEMBER_ABOUT_ME, EmailMemberHelper.DetermineMemberAboutMe(brand, mutualMember, member));
				tc.Add(TokenNames.MUTUAL_MEMBER_AGE, EmailMemberHelper.DetermineMemberAge(mutualMember, brand).ToString());
				tc.Add(TokenNames.MUTUAL_MEMBER_THUMBNAIL, EmailMemberHelper.DetermineMemberThumbnail(mutualMember, brand));
				tc.Add(TokenNames.MUTUAL_MEMBER_LOCATION, EmailMemberHelper.DetermineMemberRegionDisplay(mutualMember, brand));

                _mailBl.SendTriggerMail(member.MemberID, brand.Site.SiteID, imp.BrandID,
					_mapper.GetProviderTemplateID(brand.Site, EmailType.MutualMail),
                    member.EmailAddress, tc.BuildList(), null, EmailType.MutualMail);
                try
                {
                    ActivityRecordingSA.Instance.RecordActivity(member.MemberID, mutualMember.MemberID, brand.Site.SiteID, ActivityRecording.ValueObjects.Types.ActionType.SendSecretAdmirerBothYesMail,
                       ActivityRecording.ValueObjects.Types.CallingSystem.ExternalMail, "");
                }
                catch (Exception ex)
                {
                    Log.LogError("SendMutualMail threw an exception", ex, new CustomDataProvider(impulse.BrandID,member.MemberID,member.EmailAddress).Get());
                }

			}
		}

		/// <summary>
		/// Send user uploaded photo rejection email.
		/// </summary>
		/// <param name="impulse"></param>
		public void SendPhotoRejection(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			TokenConstructor tc = new TokenConstructor();
			PhotoRejectedImpulse imp = (PhotoRejectedImpulse)impulse;
			
			StormPostGreyMail sp = new StormPostGreyMail("dummy=1", null);
			tc.Add(TokenNames.COMMENT, sp.GetPhotoDisapproveText(imp.Comment, brand.Site.LanguageID));
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MSA.MemberLoadFlags.None);
            var firstName = member.GetAttributeText(brand, FIRST_NAME_ATTR);
            tc.Add(TokenNames.FIRST_NAME, firstName);

            _mailBl.SendTriggerMail(imp.MemberID, brand.Site.SiteID, imp.BrandID,
				_mapper.GetProviderTemplateID(brand.Site, EmailType.PhotoRejection),
                MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.None).EmailAddress, tc.BuildList(), null, EmailType.PhotoRejection);
		}

		/// <summary>
		/// Send news letter opt out notification email.
		/// </summary>
		/// <param name="impulse"></param>
		public void SendOptOutNotification(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			TokenConstructor tc = new TokenConstructor();
			OptOutNotifyImpulse imp = (OptOutNotifyImpulse)impulse;

            _mailBl.SendTriggerMail(MemberSA.Instance.GetMemberIDByEmail(imp.Email, brand.Site.Community.CommunityID), brand.Site.SiteID, imp.BrandID,
				_mapper.GetProviderTemplateID(brand.Site, EmailType.OptOutNotify),
                imp.Email, tc.BuildList(), null, EmailType.OptOutNotify);
			
		}

		/// <summary>
		/// Send hot list email.
		/// </summary>
		/// <param name="impulse"></param>
		public void SendHotListAlert(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			TokenConstructor tc = new TokenConstructor();
			HotListedNotifyImpulse imp = (HotListedNotifyImpulse)impulse;
			
			if (!Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_HOTLISTED_EMAIL_NOTIFICATION", brand.Site.Community.CommunityID, brand.Site.SiteID)))
			{
				return;
			}

            IMemberDTO hotListedByMember = MailHandlerUtils.GetIMemberDTO(imp.HotListedByMemberID, MemberLoadFlags.None);
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.None);



			bool send = true;

			AttributeGroup group = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(brand.Site.Community.CommunityID, EmailAlertConstant.ATTRIBUTE_ID);

			int mask = (int)EmailAlertMask.HotListedAlert;
			int userSetting = member.GetAttributeInt(brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, int.Parse(group.DefaultValue));
			if((mask & userSetting) != mask)
			{
				send = false;
			}

			if(send)
			{
                var firstName = member.GetAttributeText(brand, FIRST_NAME_ATTR);
                tc.Add(TokenNames.FIRST_NAME, firstName);
				tc.Add(TokenNames.USER_NAME, member.GetUserName(brand));
				tc.Add(TokenNames.HOT_LISTED_BY_MEMBER_USER_NAME, hotListedByMember.GetUserName(brand));
				tc.Add(TokenNames.HOT_LISTED_BY_MEMBER_AGE, EmailMemberHelper.DetermineMemberAge(hotListedByMember, brand).ToString());
				tc.Add(TokenNames.HOT_LISTED_BY_MEMBER_SEEKING, EmailMemberHelper.DetermineMemberSeeking(brand, hotListedByMember));
				tc.Add(TokenNames.HOT_LISTED_BY_MEMBER_THUMBNAIL, EmailMemberHelper.DetermineMemberThumbnail(hotListedByMember, brand));
				tc.Add(TokenNames.HOT_LISTED_BY_MEMBER_LOCATION, EmailMemberHelper.DetermineMemberRegionDisplay(hotListedByMember, brand));

                _mailBl.SendTriggerMail(member.MemberID, brand.Site.SiteID, imp.BrandID,
					_mapper.GetProviderTemplateID(brand.Site, EmailType.HotListedAlert),
                    member.EmailAddress, tc.BuildList(), null, EmailType.HotListedAlert);
			}
		}

		/// <summary>
		/// Send subscription confirmation email.
		/// </summary>
		/// <param name="impulse"></param>
		public void SendSubscriptionConfirmation(ImpulseBase impulse)
		{ 
            
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			TokenConstructor tc = new TokenConstructor();
			SubscriptionConfirmationImpulse imp = (SubscriptionConfirmationImpulse)impulse;
			
			string dateOfPurchase = "";
			if (impulse.BrandID == 1004)
				dateOfPurchase = imp.DateOfPurchase.AddHours(10).ToString("dd/MM/yyyy");
			else if (impulse.BrandID == 1013)
				dateOfPurchase = imp.DateOfPurchase.ToString("dd/MM/yyyy");
			else if (impulse.BrandID == 1107)
				dateOfPurchase = imp.DateOfPurchase.AddHours(8).ToString("dd/MM/yyyy");
			else if (impulse.BrandID == 1105)
				dateOfPurchase = imp.DateOfPurchase.AddHours(9).ToString("dd/MM/yyyy");			
			else
				dateOfPurchase = imp.DateOfPurchase.ToString("d");

           

			tc.Add(TokenNames.LAST_NAME, imp.LastName);
			tc.Add(TokenNames.FIRST_NAME, imp.FirstName);
			tc.Add(TokenNames.DATE_OF_PURCHASE, dateOfPurchase);
			tc.Add(TokenNames.RENEW_COST, imp.RenewCost.ToString());
			tc.Add(TokenNames.RENEW_DURATION, imp.RenewDuration.ToString());
			tc.Add(TokenNames.RENEW_DURATION_TYPE, imp.RenewDurationType.ToString());

			if (brand.Site.LanguageID == (int)Language.Hebrew)
			{
				tc.Add(TokenNames.INITIAL_COST, imp.InitialCost.ToString("F0"));
			}
			else
			{
				tc.Add(TokenNames.INITIAL_COST, imp.InitialCost.ToString("F2"));
			}
			tc.Add(TokenNames.INITIAL_DURATION, imp.InitialDuration.ToString());
			tc.Add(TokenNames.INITIAL_DURATION_TYPE, imp.InitialDurationType.ToString());
				
			tc.Add(TokenNames.LAST_4CC, imp.Last4CC);
			tc.Add(TokenNames.PLAN_TYPE, imp.PlanType.ToString());
			tc.Add(TokenNames.CREDIT_CARD_TYPE_ID, imp.CreditCardTypeID.ToString());
			tc.Add(TokenNames.CONFIRMATION_NUMBER, imp.ConfirmationNumber.ToString());

            _mailBl.SendTriggerMail(imp.MemberID, brand.Site.SiteID, imp.BrandID, 
				_mapper.GetProviderTemplateID(brand.Site, EmailType.SubscriptionConfirmation),
                imp.EmailAddress, tc.BuildList(), null, EmailType.SubscriptionConfirmation);
		}

		/// <summary>
		/// Send one off purchase confirmation email.
		/// </summary>
		/// <param name="impulse"></param>
		public void SendOneOffPurchaseConfirmation(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			TokenConstructor tc = new TokenConstructor();
			OneOffPurchaseConfirmationImpulse imp = (OneOffPurchaseConfirmationImpulse)impulse;
			
			string dateOfPurchase = "";
			if (impulse.BrandID == 1004)
				dateOfPurchase = imp.DateOfPurchase.AddHours(10).ToString("dd/MM/yyyy");
			else if (impulse.BrandID == 1013)
				dateOfPurchase = imp.DateOfPurchase.ToString("dd/MM/yyyy");
			else if (impulse.BrandID == 1107)
				dateOfPurchase = imp.DateOfPurchase.AddHours(8).ToString("dd/MM/yyyy");
			else if (impulse.BrandID == 1105)
				dateOfPurchase = imp.DateOfPurchase.AddHours(9).ToString("dd/MM/yyyy");			
			else
				dateOfPurchase = imp.DateOfPurchase.ToString("d");			
		
			tc.Add(TokenNames.LAST_NAME, imp.LastName);
			tc.Add(TokenNames.FIRST_NAME, imp.FirstName);
			tc.Add(TokenNames.DATE_OF_PURCHASE, dateOfPurchase);

			if (brand.Site.LanguageID == (int)Language.Hebrew)
			{
				tc.Add(TokenNames.INITIAL_COST, imp.InitialCost.ToString("F0"));
			}
			else
			{
				tc.Add(TokenNames.INITIAL_COST, imp.InitialCost.ToString("F2"));
			}
			tc.Add(TokenNames.INITIAL_DURATION, imp.InitialDuration.ToString());
			tc.Add(TokenNames.INITIAL_DURATION_TYPE, imp.InitialDurationType.ToString());
				
			tc.Add(TokenNames.LAST_4CC, imp.Last4CC);
			//tc.Add(TokenNames.PLAN_TYPE, imp.PlanType.ToString());
			tc.Add(TokenNames.CREDIT_CARD_TYPE_ID, imp.CreditCardTypeID.ToString());
			tc.Add(TokenNames.CONFIRMATION_NUMBER, imp.ConfirmationNumber.ToString());

            _mailBl.SendTriggerMail(imp.MemberID, brand.Site.SiteID, imp.BrandID, 
				_mapper.GetProviderTemplateID(brand.Site, EmailType.OneOffPurchaseConfirmation),
                imp.EmailAddress, tc.BuildList(), null, EmailType.OneOffPurchaseConfirmation);
		}

		public void sendPaymentProfileNotification(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			TokenConstructor tc = new TokenConstructor();
			PmtProfileConfirmationImpulse imp = (PmtProfileConfirmationImpulse)impulse;
			
			string dateOfPurchase = "";
			if (impulse.BrandID == 1004)
				dateOfPurchase = imp.DateOfPurchase.AddHours(10).ToString("dd/MM/yyyy");
			else if (impulse.BrandID == 1013)
				dateOfPurchase = imp.DateOfPurchase.ToString("dd/MM/yyyy");
			else if (impulse.BrandID == 1107)
				dateOfPurchase = imp.DateOfPurchase.AddHours(8).ToString("dd/MM/yyyy");
			else
				dateOfPurchase = imp.DateOfPurchase.ToString("d");			
		
			tc.Add(TokenNames.LAST_NAME, imp.LastName);
			tc.Add(TokenNames.FIRST_NAME, imp.FirstName);
			tc.Add(TokenNames.DATE_OF_PURCHASE, dateOfPurchase);
			
							
			tc.Add(TokenNames.LAST_4CC, imp.Last4CC);
			
			tc.Add(TokenNames.CONFIRMATION_NUMBER, imp.ConfirmationNumber.ToString());

			Site site = null;
			Sites coll = BrandConfigSA.Instance.GetSites();

			foreach(Site s in coll)
			{
				if(s.SiteID == imp.SiteID)
				{
					site = s;
					break;
				}
			}

            _mailBl.SendTriggerMail(imp.MemberID, imp.SiteID, brand.BrandID, 
				_mapper.GetProviderTemplateID(site, EmailType.PmtProfileConfirmation),
                imp.EmailAddress, tc.BuildList(), null, EmailType.PmtProfileConfirmation);
		}

		public void sendMatchMeterInvitationMailImpulse(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			TokenConstructor tc = new TokenConstructor();
			MatchMeterInvitationMailImpulse imp = (MatchMeterInvitationMailImpulse)impulse;

			tc.Add(TokenNames.SENDER_USER_NAME, imp.SenderUsername);
			tc.Add(TokenNames.EMAIL_ADDR, imp.EmailAddr);
			tc.Add(TokenNames.RECIPIENT_EMAIL_ADDRESS, imp.RecipientEmailAddress);
			tc.Add(TokenNames.S2F_MEMBER_ID, imp.MemberID.ToString());
			

			Site site = null;
			Sites coll = BrandConfigSA.Instance.GetSites();

			foreach(Site s in coll)
			{
				if(s.SiteID == imp.SiteID)
				{
					site = s;
					break;
				}
			}
            _mailBl.SendTriggerMail(imp.MemberID, imp.SiteID, brand.BrandID, 
				_mapper.GetProviderTemplateID(site, EmailType.MatchMeterInvitationMail)
                , imp.RecipientEmailAddress, tc.BuildList(), null, EmailType.MatchMeterInvitationMail);
		
		}

		public void sendMatchMeterCongratsMailImpulse(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			TokenConstructor tc = new TokenConstructor();
			MatchMeterCongratsMailImpulse imp = (MatchMeterCongratsMailImpulse)impulse;
		
			tc.Add(TokenNames.USER_NAME, imp.UserName);
			tc.Add(TokenNames.EMAIL_ADDR, imp.EmailAddr);
			tc.Add(TokenNames.RECIPIENT_EMAIL_ADDRESS, imp.EmailAddr);
			tc.Add(TokenNames.S2F_MEMBER_ID, imp.MemberID.ToString());

			Site site = null;
			Sites coll = BrandConfigSA.Instance.GetSites();

			foreach(Site s in coll)
			{
				if(s.SiteID == imp.SiteID)
				{
					site = s;
					break;
				}
			}
            _mailBl.SendTriggerMail(imp.MemberID, imp.SiteID, brand.BrandID, 
				_mapper.GetProviderTemplateID(site, EmailType.MatchMeterCongratsMail)
                , imp.EmailAddr, tc.BuildList(), null, EmailType.MatchMeterCongratsMail);
		
		}

		/// <summary>
		/// Formerly known as ViralMail.
		/// </summary>
		/// <param name="impulseBase"></param>
		public void SendClickMail(ImpulseBase impulseBase)
		{
		    int profileCountExpected = 5;
			int counter;

			ViralImpulse impulse = (ViralImpulse) impulseBase;

			MiniprofileInfo recipientMiniprofileInfo = impulse.RecipientMiniprofileInfo;
			MiniprofileInfoCollection clickMembers = impulse.TargetMiniprofileInfoCollection;

			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);

			// 40 is the number of total tokens passed into YesMail.
			SortedList tokenValues = new SortedList(40);

			tokenValues.Add("03_EmailAddress", MailHandlerUtils.Filter(impulse.RecipientEmailAddress));
			tokenValues.Add("01_SiteID", brand.Site.SiteID.ToString());
			tokenValues.Add("02_MemberID", recipientMiniprofileInfo.MemberID);	
			tokenValues.Add("34_UserName", MailHandlerUtils.Filter(recipientMiniprofileInfo.UserName));

			counter = 1;
            for (int i = 0; i < profileCountExpected; i++)
            {
                int clickMemberIDOrder = 4;
                string order = String.Format("{0:0#}", clickMemberIDOrder + counter - 1);

                if (clickMembers.Count > i)
                {
                    MiniprofileInfo currentMember = clickMembers.Item(i);
                    tokenValues.Add(order + "_Click" + counter++ + "MemberID", currentMember.MemberID.ToString());
                }
                else
                {
                    tokenValues.Add(order + "_Click" + counter++ + "MemberID", "");
                }
            }

			counter = 1;
            for (int i = 0; i < profileCountExpected; i++)
            {
                int clickThumbnailOrder = 9;
                string order = String.Format("{0:0#}", clickThumbnailOrder + counter - 1);

                if (clickMembers.Count > i)
                {
                    MiniprofileInfo currentMember = clickMembers.Item(i);
                    string thumbnail = EmailMemberHelper.DetermineBaseNoPhoto(brand);

                    if (currentMember.ThumbNailWebPath != Constants.NULL_STRING)
                    {
                        thumbnail = currentMember.ThumbNailWebPath;
                    }
                    tokenValues.Add(order + "_Click" + counter++ + "ThumbNail", MailHandlerUtils.Filter(thumbnail));
                }
                else
                {
                    tokenValues.Add(order + "_Click" + counter++ + "ThumbNail", "");
                }
            }

			counter = 1;
            for (int i = 0; i < profileCountExpected; i++)
            {
                int clickNameOrder = 14;
                string order = String.Format("{0:0#}", clickNameOrder + counter - 1);

                if (clickMembers.Count > i)
                {
                    MiniprofileInfo currentMember = clickMembers.Item(i);
                    tokenValues.Add(order + "_Click" + counter++ + "Name", MailHandlerUtils.Filter(currentMember.UserName));
                }
                else
                {
                    tokenValues.Add(order + "_Click" + counter++ + "Name", "");
                }
            }

			counter = 1;
            for (int i = 0; i < profileCountExpected; i++)
            {
                int clickAgeOrder = 19;
                string order = String.Format("{0:0#}", clickAgeOrder + counter - 1);

                if (clickMembers.Count > i)
                {
                    MiniprofileInfo currentMember = clickMembers.Item(i);
                    tokenValues.Add(order + "_Click" + counter++ + "Age", EmailMemberHelper.DetermineMemberAge(currentMember.BirthDate));
                }
                else
                {
                    tokenValues.Add(order + "_Click" + counter++ + "Age", "");
                }
            }

			counter = 1;
            for (int i = 0; i < profileCountExpected; i++)
            {
                int clickLocationOrder = 24;
                string order = String.Format("{0:0#}", clickLocationOrder + counter - 1);

                if (clickMembers.Count > i)
                {
                    MiniprofileInfo currentMember = clickMembers.Item(i);
                    tokenValues.Add(order + "_Click" + counter++ + "Location", MailHandlerUtils.Filter(EmailMemberHelper.DetermineMemberRegionDisplay(currentMember.RegionID, brand)));	
                }
                else
                {
                    tokenValues.Add(order + "_Click" + counter++ + "Location", "");	
                }
            }

			counter = 1;
            for (int i = 0; i < profileCountExpected; i++)
            {
                int clickNameText = 29;
                string order = String.Format("{0:0#}", clickNameText + counter - 1);

                if (clickMembers.Count > i)
                {
                    MiniprofileInfo currentMember = clickMembers.Item(i);
                    string miniAboutMe = currentMember.AboutMe;

                    if (miniAboutMe.Length > 18)
                    {
                        miniAboutMe = miniAboutMe.Substring(0, 18);
                    }
                    tokenValues.Add(order + "_Click" + counter++ + "NameText", MailHandlerUtils.Filter(miniAboutMe));
                }
                else
                {
                    tokenValues.Add(order + "_Click" + counter++ + "NameText", "");
                }
            }

			YesMailBL.Instance.SendBulkMail(BulkMailType.ClickMail,
				recipientMiniprofileInfo.MemberID,
				brand.Site.SiteID,
				impulse.RecipientEmailAddress,
				tokenValues);
		}

		/// <summary>
		/// Send MatchMail using BulkMail
		/// 
		/// Refer to MatchMail Tokens.xls. Final column of MatchMail Tokens worksheet.
		/// Tokens are sent in the same order as they're in the column.
		/// 
		/// File is stored in
		/// \\matchnet.com\departments\Project Management\Project Folders\Redesign\RD06-0006_ESP\Technical Documentations\Bulk Email
		/// </summary>
		/// <param name="impulseBase"></param>
		public void SendMatchMail(ImpulseBase impulseBase)
		{
			int counter;

			MatchMailImpulse impulse = (MatchMailImpulse) impulseBase;

			MiniprofileInfo recipientMiniprofileInfo = impulse.RecipientMiniprofileInfo;
			MiniprofileInfoCollection members = impulse.TargetMiniprofileInfoCollection;

			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);

			ListCountCollection listCountCollection = ListSA.Instance.GetListCounts(recipientMiniprofileInfo.MemberID, brand.Site.Community.CommunityID, brand.Site.SiteID);

			// 79 is the number of total tokens passed into YesMail.
			SortedList tokens = new SortedList(79);

			tokens.Add("03_EmailAddress", MailHandlerUtils.Filter(impulse.RecipientEmailAddress));
			tokens.Add("01_SiteID", brand.Site.SiteID.ToString());
			tokens.Add("02_MemberID", impulse.RecipientMiniprofileInfo.MemberID.ToString());
			tokens.Add("16_UserName", MailHandlerUtils.Filter(recipientMiniprofileInfo.UserName));
			tokens.Add("23_UserAge", EmailMemberHelper.DetermineMemberAge(recipientMiniprofileInfo.BirthDate));
			tokens.Add("30_MemberLocation", EmailMemberHelper.DetermineMemberRegionDisplay(recipientMiniprofileInfo.RegionID, brand));

			counter = 1;
			foreach (MiniprofileInfo currentMember in members)
			{
				int matchMemberIDOrder=4;
				string order=String.Format("{0:0#}", matchMemberIDOrder + counter - 1);
				tokens.Add(order+ "_Match" + counter++ + "MemberID", currentMember.MemberID.ToString());
			}

			counter = 1;
			foreach (MiniprofileInfo currentMember in members)
			{
				int matchNameOrder=10;
				string order=String.Format("{0:0#}", matchNameOrder + counter - 1);
				tokens.Add(order+ "_Match" + counter++ + "Name", MailHandlerUtils.Filter(currentMember.UserName));
			}

			counter = 1;
			foreach (MiniprofileInfo currentMember in members)
			{
				int age = EmailMemberHelper.DetermineMemberAge(currentMember.BirthDate);

				int matchAgeOrder=17;
				string order=String.Format("{0:0#}", matchAgeOrder + counter - 1);
				tokens.Add(order+ "_Match" + counter++ + "Age", age);
			}

			counter = 1;
			foreach (MiniprofileInfo currentMember in members)
			{
				string regionDisplay = EmailMemberHelper.DetermineMemberRegionDisplay(currentMember.RegionID, brand);

				int matchLocationOrder=24;
				string order=String.Format("{0:0#}", matchLocationOrder + counter - 1);
				tokens.Add(order+ "_Match" + counter++ + "Location", MailHandlerUtils.Filter(regionDisplay));
			}

			counter = 1;
			foreach (MiniprofileInfo currentMember in members)
			{
				string thumbnail = EmailMemberHelper.DetermineBaseNoPhoto(brand);

				if (currentMember.ThumbNailWebPath != Constants.NULL_STRING)
				{
					thumbnail = currentMember.ThumbNailWebPath;
				}

				int matchThumbNailOrder=31;
				string order=String.Format("{0:0#}", matchThumbNailOrder + counter - 1);
				tokens.Add(order+ "_Match" + counter++ + "ThumbNail", (MailHandlerUtils.Filter(thumbnail)));

			}

			string recipientThumbnail = EmailMemberHelper.DetermineBaseNoPhoto(brand);

			if (recipientMiniprofileInfo.ThumbNailWebPath != Constants.NULL_STRING)
			{
				recipientThumbnail = recipientMiniprofileInfo.ThumbNailWebPath;
			}

			tokens.Add("37_UserThumbNail", MailHandlerUtils.Filter(recipientThumbnail));

			#region Append HotList Tokens (You XXed others)

			tokens.Add("38_NumberYouHotlisted", listCountCollection[HotListCategory.Default]);
			tokens.Add("42_NumberYouContacted", listCountCollection[HotListCategory.MembersYouEmailed]);
			tokens.Add("39_NumberYouTeased", listCountCollection[HotListCategory.MembersYouTeased]);
			tokens.Add("40_NumberYouIM", listCountCollection[HotListCategory.MembersYouIMed]);
			tokens.Add("41_NumberYouViewed", listCountCollection[HotListCategory.MembersYouViewed]);
			
			#endregion

			tokens.Add("43_MemberSearchURL", MailHandlerUtils.Filter(MailHandlerUtils.DetermineSearchUrl(recipientMiniprofileInfo.MemberID,
				BrandConfigSA.Instance.GetBrandByID(impulse.BrandID).Site.Community.CommunityID)));

			// Append View Profile MMVID (Enables hot listing w/o logging in)
			counter = 1;
			foreach (MiniprofileInfo currentMember in members)
			{
				int matchMMVIDOrder=44;
				string order=String.Format("{0:0#}", matchMMVIDOrder + counter - 1);
				tokens.Add(order+ "_MMVID" + counter++, MailHandlerUtils.GetMMVID(recipientMiniprofileInfo.MemberID, currentMember.MemberID));
			}

			#region Append Reverse HotList Tokens (Who XXed you)

			tokens.Add("50_NumberHotlistedYou", listCountCollection[HotListCategory.WhoAddedYouToTheirFavorites]);
			tokens.Add("54_NumberContactedYou", listCountCollection[HotListCategory.WhoEmailedYou]);
			tokens.Add("51_NumberTeasedYou", listCountCollection[HotListCategory.WhoTeasedYou]);
			tokens.Add("52_NumberIMYou", listCountCollection[HotListCategory.WhoIMedYou]);
			tokens.Add("53_NumberViewedYou", listCountCollection[HotListCategory.WhoViewedYourProfile]);

			#endregion

			#region V1.3 #17638 Additional tokens for banners			

			//Religion
			tokens.Add("55_Religion", recipientMiniprofileInfo.ReligionID.ToString());

			//JDateReligion
			tokens.Add("56_JdateReligion", recipientMiniprofileInfo.JdateReligionID.ToString());
			
			//Ethnicity
			tokens.Add("57_Ethnicity", recipientMiniprofileInfo.EthnicityID.ToString());
			
			//JdateEthnicity
			tokens.Add("58_JdateEthnicity", recipientMiniprofileInfo.JdateEthnicityID.ToString());
			
			//MaritalStatus
			tokens.Add("59_MaritalStatus", recipientMiniprofileInfo.MaritalStatusID.ToString());
			
			//ChildrenCount
			tokens.Add("60_ChildrenCount", recipientMiniprofileInfo.ChildrenCount.ToString());
			
			//Custody
			tokens.Add("61_Custody", recipientMiniprofileInfo.CustodyID.ToString());
			
			//IsPayingMember
			tokens.Add("62_IsPayingMember", recipientMiniprofileInfo.IsPayingMember.ToString());
			
			//IsVerifiedEamil
			bool isVerifiedEmail = false;
			int globalStatusMask = recipientMiniprofileInfo.GlobalStatusMask;
			if ((globalStatusMask != Constants.NULL_INT) && ((globalStatusMask & EMAIL_VERIFIED) == EMAIL_VERIFIED))
			{
				isVerifiedEmail = true;
			}
			tokens.Add("63_IsVerifiedEmail", isVerifiedEmail.ToString());
			

			//HasApprovedPhoto
			tokens.Add("64_HasApprovedPhoto", recipientMiniprofileInfo.HasApprovedPhotos.ToString());

			//Gender
			tokens.Add("65_Gender", MailHandlerUtils.GetGenderMaskSelf(recipientMiniprofileInfo.GenderMask).ToString());
			
			//SeekingGender
			tokens.Add("66_SeekingGender", MailHandlerUtils.GetGenderMaskSeeking(recipientMiniprofileInfo.GenderMask).ToString());

			//EmailVerificationCode
			tokens.Add("67_EmailVerificationCode", EmailVerify.HashMemberID(recipientMiniprofileInfo.MemberID).ToString());


			if(Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MATCHMAIL_INCLUDE_MORE_DATA")))
			{
				if(members.Item(0) != null)
				{
					tokens.Add("68_MM1Generic", MailHandlerUtils.GetMatchText(recipientMiniprofileInfo, members.Item(0), brand));
				}
				else
				{
					tokens.Add("68_MM1Generic", string.Empty);
				}

				if(members.Item(1) != null)
				{
					tokens.Add("69_MM2Generic", MailHandlerUtils.GetMatchText(recipientMiniprofileInfo, members.Item(1), brand));
				}
				else
				{
					tokens.Add("69_MM2Generic", string.Empty);
				}

				tokens.Add("70_MM3Generic", string.Empty);
				tokens.Add("71_MM4Generic", string.Empty);
				tokens.Add("72_MM5Generic", string.Empty);
				tokens.Add("73_MM6Generic", string.Empty);
				tokens.Add("74_MM7Generic", string.Empty);
				tokens.Add("75_MM8Generic", string.Empty);
				tokens.Add("76_MM9Generic", string.Empty);
				tokens.Add("77_MM10Generic", string.Empty);
				tokens.Add("78_MM11Generic", string.Empty);
				tokens.Add("79_MM12Generic", string.Empty);
			}

			#endregion

			YesMailBL.Instance.SendBulkMail(BulkMailType.MatchMail, 
				recipientMiniprofileInfo.MemberID,
				brand.Site.SiteID,
				impulse.RecipientEmailAddress,
				tokens);
		}

		public void SendNewMemberMail(ImpulseBase impulseBase)
		{
			int counter;
			int tokenLineCounter;
			string tokenConstant = "UT_Generic";
			string tokenName; 

			NewMemberMailImpulse impulse = (NewMemberMailImpulse) impulseBase;

			MiniprofileInfo recipientMiniprofileInfo = impulse.RecipientMiniprofileInfo;
			MiniprofileInfoCollection members = impulse.TargetMiniprofileInfoCollection;

			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			
			ArrayList tokens = new ArrayList();
			tokens.Insert(0, new Token("EmailAddress", MailHandlerUtils.Filter(impulse.RecipientEmailAddress)));
			tokens.Insert(1, new Token("NMIA_TimeStamp", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss", DateTimeFormatInfo.InvariantInfo)));
			tokens.Insert(2, new Token("generic_num3", impulse.TargetMiniprofileInfoCollection.Count.ToString()));
			//this field is reserved for future use, right now hard code it to zero
			tokens.Insert(3, new Token("generic_num4", "0"));

			counter = 0;
			tokenLineCounter = 3;
			foreach (MiniprofileInfo currentMember in impulse.TargetMiniprofileInfoCollection)
			{
				string regionDisplay = EmailMemberHelper.DetermineMemberRegionDisplay(currentMember.RegionID, brand);
				string userName = currentMember.UserName;
				string age = MailHandlerUtils.GetAge(currentMember.BirthDate).ToString();
				string imageURL = currentMember.ThumbNailWebPath;
				string memberID = currentMember.MemberID.ToString();
				
				counter++;
				tokenLineCounter++;
				tokenName = tokenConstant + counter.ToString();
				tokens.Insert(tokenLineCounter, new Token(tokenName, userName));

				counter++;
				tokenLineCounter++;
				tokenName = tokenConstant + counter.ToString();
				tokens.Insert(tokenLineCounter, new Token(tokenName, age));

				counter++;
				tokenLineCounter++;
				tokenName = tokenConstant + counter.ToString();
				tokens.Insert(tokenLineCounter, new Token(tokenName, regionDisplay));
				
				counter++;
				tokenLineCounter++;
				tokenName = tokenConstant + counter.ToString();
				tokens.Insert(tokenLineCounter, new Token(tokenName, imageURL));
				
				counter++;
				tokenLineCounter++;
				tokenName = tokenConstant + counter.ToString();
				tokens.Insert(tokenLineCounter, new Token(tokenName, memberID));
			}

			while(counter < 20)
			{
				counter++;
				tokenLineCounter++;
				tokenName = tokenConstant + counter.ToString();
				tokens.Insert(tokenLineCounter, new Token(tokenName, string.Empty));
			}

			YesMailBL.Instance.SendUserExportData(BulkMailType.NewMembers, 
				recipientMiniprofileInfo.MemberID,
				brand.Site.SiteID,
				impulse.RecipientEmailAddress,
				tokens);
		}


		#endregion

		#region Ported over from SoapHandler

		/// <summary>
		/// Send acount activation email.
		/// </summary>
		/// <param name="impulse"></param>
		public void SendActivationLetter(ImpulseBase impulse)
		{
			TokenConstructor tc = new TokenConstructor();
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			RegVerificationImpulse imp = (RegVerificationImpulse)impulse;

            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.IngoreSACache);
		    var firstName = member.GetAttributeText(brand, FIRST_NAME_ATTR);
            tc.Add(TokenNames.FIRST_NAME,firstName);
			// This is to prevent occasional errors we get from AllowedDomains() method call. Checking against Constants.NULL_STRING since that's the default for this property.
			// This could possibly happen when a new member's registration info has not been replicated.
			if (member != null && member.EmailAddress == Constants.NULL_STRING)
			{
			    try
			    {   // Saving Stack into the exception, required for reygun 
                    throw new Exception("Member SendActivationLetter Exception. EmailAddress is null. MemberID:" + member.MemberID);    
			    }
			    catch (Exception ex)
			    {
                    Log.LogError("SendActivationLetter threw an exception", ex, new CustomDataProvider(impulse.BrandID,member.MemberID).Get());
			    }
			    return;
			}

			string emailAddress=member.EmailAddress;

			tc.Add(TokenNames.VERIFY_CODE,EmailVerify.GetHashCode(emailAddress, imp.MemberID,brand));

            _mailBl.SendTriggerMail(member.MemberID, brand.Site.SiteID, imp.BrandID, 
				_mapper.GetProviderTemplateID(brand.Site, EmailType.ActivationLetter),
                member.EmailAddress, tc.BuildList(), null, EmailType.ActivationLetter);
		}

		/// <summary>
		/// Send password recovery email.
		/// </summary>
		/// <param name="impulse">PasswordMailImpulse</param>
		public void SendForgotPassword(ImpulseBase impulse)
		{
			TokenConstructor tc = new TokenConstructor();
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			PasswordMailImpulse imp = (PasswordMailImpulse)impulse;
			int memberID = Constants.NULL_INT;

            memberID = MemberSA.Instance.GetMemberIDByEmail(imp.EmailAddress, brand.Site.Community.CommunityID);
			// To prevent invalid email addresses from calling up Member Service which ends up in exceptions.
			if (memberID == Constants.NULL_INT)
			{
                try
                {   // Saving Stack into the exception, required for reygun 
                    throw new Exception("MemberID not found. [Email Address:" + imp.EmailAddress + "]");
                }
                catch (Exception ex)
                {
                    Log.LogError("MemberID not found. [Email Address:" + imp.EmailAddress + "]", ex, new CustomDataProvider(impulse.BrandID,memberID,imp.EmailAddress).Get());
                }

				return;
			}

            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(memberID, MSA.MemberLoadFlags.None);
            var firstName = member.GetAttributeText(brand, FIRST_NAME_ATTR);
            tc.Add(TokenNames.FIRST_NAME, firstName);
            string password = String.Empty;

			tc.Add(TokenNames.EMAIL_ADDR, imp.EmailAddress);
			
			// Member service is throwing out errors on password retrival of some members. Just log and send a blank password for now.
			try
			{
				password = MemberSA.Instance.GetPassword(imp.EmailAddress);
			}
			catch (Exception ex)
			{
                Log.LogError("Member SendForgotPassword Exception.", ex, new CustomDataProvider(impulse.BrandID,memberID,imp.EmailAddress).Get());
			}
			
			tc.Add(TokenNames.SPARK_PASSWORD , password);

            _mailBl.SendTriggerMail(MemberSA.Instance.GetMemberIDByEmail(imp.EmailAddress, brand.Site.Community.CommunityID), brand.Site.SiteID, imp.BrandID,
				_mapper.GetProviderTemplateID(brand.Site, EmailType.ForgotPassword),
                imp.EmailAddress, tc.BuildList(), null, EmailType.ForgotPassword);		
	
		}

        /// <summary>
        /// When member wants to reset his password, this email gets sent with the link to reset the password.
        /// </summary>
        /// <param name="impulse"></param>
        public void SendResetPassword(ImpulseBase impulse)
        {
            TokenConstructor tc = new TokenConstructor();
            Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
            ResetPasswordImpulse imp = (ResetPasswordImpulse)impulse;

            var member = MailHandlerUtils.GetIMemberDTO(imp.MemberId, MemberLoadFlags.None);
            var firstName = member.GetAttributeText(brand, FIRST_NAME_ATTR);
            tc.Add(TokenNames.FIRST_NAME, firstName);

            tc.Add(TokenNames.EMAIL_ADDR, member.EmailAddress);
            // since we are using the old email template, we are putting the link to the password reset page in the password token slot
            tc.Add(TokenNames.SPARK_PASSWORD, imp.ResetPasswordUrl);
            var customFields = new TokenConstructor();
            customFields.Add(TokenNames.ResetUrl,imp.ResetPasswordUrl);

            _mailBl.SendTriggerMail(imp.MemberId, brand.Site.SiteID, imp.BrandID,
            _mapper.GetProviderTemplateID(brand.Site, EmailType.ForgotPassword),
            member.EmailAddress, tc.BuildList(), customFields.BuildList(), EmailType.ForgotPassword);		

        }

		/// <summary>
		/// Send Email update email.
		/// </summary>
		/// <param name="impulse">EmailVerificationImpulse</param>
		public void SendEmailChangeConfirmation(ImpulseBase impulse)
		{			
			TokenConstructor tc = new TokenConstructor();
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			EmailVerificationImpulse imp = (EmailVerificationImpulse)impulse;
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MSA.MemberLoadFlags.IngoreSACache);

            var firstName = member.GetAttributeText(brand, FIRST_NAME_ATTR);
            tc.Add(TokenNames.FIRST_NAME, firstName);

			string emailAddress=member.EmailAddress;
			tc.Add(TokenNames.VERIFY_CODE,EmailVerify.GetHashCode(emailAddress, imp.MemberID,brand));
			//tc.Add(TokenNames.VERIFY_CODE, EmailVerify.HashMemberID(imp.MemberID));

            _mailBl.SendTriggerMail(imp.MemberID, brand.Site.SiteID, imp.BrandID,
                _mapper.GetProviderTemplateID(brand.Site, EmailType.EmailChangeConfirmation), emailAddress, tc.BuildList(), null, EmailType.EmailChangeConfirmation);
				//MemberSA.Instance.GetMember(imp.MemberID, MSA.MemberLoadFlags.None).EmailAddress, tc.BuildList(), EmailType.EmailChangeConfirmation);
		}

		/// <summary>
		/// Send message board notification email.
		/// </summary>
		/// <param name="impulse"></param>
		public void SendMessageBoardInstantNotification(ImpulseBase impulse)
		{
			TokenConstructor tc = new TokenConstructor();
			//Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			MessageBoardInstantNotificationImpulse imp = (MessageBoardInstantNotificationImpulse)impulse;
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.None);
			Brand brand = BrandConfigSA.Instance.GetBrandByID(impulse.BrandID);
			
			tc.Add(TokenNames.REPLY_URL, imp.ReplyURL);			
			tc.Add(TokenNames.BOARD_NAME, imp.BoardName);
			tc.Add(TokenNames.USER_NAME, member.GetUserName(brand));
			tc.Add(TokenNames.TOPIC_SUBJECT, imp.TopicSubject);
			tc.Add(TokenNames.UNSUBSCRIBE_URL, imp.UnsubscribeURL);
			tc.Add(TokenNames.NEW_REPLIES, imp.NewReplies.ToString());

			Site site = null;
			Sites coll = BrandConfigSA.Instance.GetSites();

			foreach(Site s in coll)
			{
				if(s.SiteID == imp.SiteID)
				{
					site = s;
					break;
				}
			}

            _mailBl.SendTriggerMail(member.MemberID, site.SiteID, imp.BrandID,
				_mapper.GetProviderTemplateID(site, EmailType.MessageBoardInstantNotification),
                member.EmailAddress, tc.BuildList(), null, EmailType.MessageBoardInstantNotification);
		}

		/// <summary>
		/// Send mesasge board daily updates email.
		/// </summary>
		/// <param name="impulse"></param>
		public void SendMessageBoardDailyUpdates(ImpulseBase impulse)
		{
			TokenConstructor tc = new TokenConstructor();
			//Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			MessageBoardDailyUpdatesImpulse imp = (MessageBoardDailyUpdatesImpulse) impulse;
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.None);
			Brand brand = BrandConfigSA.Instance.GetBrandByID(impulse.BrandID);

			tc.Add(TokenNames.BOARD_NAME, imp.BoardName);
			tc.Add(TokenNames.USER_NAME, member.GetUserName(brand));		
			tc.Add(TokenNames.TOPIC_SUBJECT, imp.TopicSubject);			
			tc.Add(TokenNames.BOARD_ID, imp.BrandID.ToString());
			tc.Add(TokenNames.THREAD_ID, imp.ThreadID.ToString());
			tc.Add(TokenNames.UNSUBSCRIBE_URL, imp.UnsubscribeURL);
			tc.Add(TokenNames.NEW_REPLIES, imp.NewReplies.ToString());			
			tc.Add(TokenNames.LAST_MESSAGE_ID, imp.LastMessageID.ToString());
			
			Site site = null;
			Sites coll = BrandConfigSA.Instance.GetSites();

			foreach(Site s in coll)
			{
				if(s.SiteID == imp.SiteID)
				{
					site = s;
					break;
				}
			}

            _mailBl.SendTriggerMail(member.MemberID, site.SiteID, imp.BrandID,
				_mapper.GetProviderTemplateID(site, EmailType.MessageBoardDailyUpdates),
                member.EmailAddress, tc.BuildList(), null, EmailType.MessageBoardDailyUpdates);
		}

		/// <summary>
		/// Send ecard to member email.
		/// </summary>
		/// <param name="impulse"></param>
		public void SendEcardToMember(ImpulseBase impulse)
		{				
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			TokenConstructor tc = new TokenConstructor();
			EcardToMemberImpulse imp = (EcardToMemberImpulse)impulse;

            IMemberDTO sender = MailHandlerUtils.GetIMemberDTO(imp.SenderMemberID, MemberLoadFlags.None);
            IMemberDTO recipient = MailHandlerUtils.GetIMemberDTO(imp.RecipientMemberID, MemberLoadFlags.None);	
		
			bool send = true;
			
			AttributeGroup group = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(brand.Site.Community.CommunityID, EmailAlertConstant.ATTRIBUTE_ID);
			
			int mask = (int)EmailAlertMask.ECardAlert;
			int userSetting = recipient.GetAttributeInt(brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, int.Parse(group.DefaultValue));

			// Make sure user specified to recieve ecard notification email.
			if((mask & userSetting) != mask)
			{
				send = false;
			}

			if(send)
			{
				string senderThumbnail = "";
				if (Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_VIEW_MINIPROFILE_ECARD_TO_MEMBER", brand.Site.Community.CommunityID, imp.SiteID)) || recipient.IsPayingMember(imp.SiteID))
				{
					senderThumbnail = EmailMemberHelper.DetermineMemberThumbnail(sender, brand);
				}

				string ecardUrl = EmailMemberHelper.CreateEcardUrl(recipient, brand, imp.CardUrl);
				if (ecardUrl != Constants.NULL_STRING)
				{
					tc.Add(TokenNames.CARD_URL, ecardUrl);
					tc.Add(TokenNames.SENDER_USER_NAME, sender.GetUserName(brand));
					tc.Add(TokenNames.SENDER_THUMBNAIL, senderThumbnail);
					tc.Add(TokenNames.CARD_THUMBNAIL, imp.CardThumbnail);
					tc.Add(TokenNames.RECIPIENT_USER_NAME, recipient.GetUserName(brand));
					tc.Add(TokenNames.RECIPIENT_EMAIL_ADDRESS, recipient.EmailAddress);
					tc.Add(TokenNames.SENT_DATE, EmailMemberHelper.GetDateDisplay(imp.SentDate, brand));
					tc.Add(TokenNames.SENDER_AGE, EmailMemberHelper.DetermineMemberAge(sender, brand).ToString());
					tc.Add(TokenNames.SENDER_LOCATION, EmailMemberHelper.DetermineMemberRegionDisplay(sender, brand));
				
					/*
					Site site = null;
					Sites coll = BrandConfigSA.Instance.GetSites();

					foreach(Site s in coll)
					{
						if(s.SiteID == imp.SiteID)
						{
							site = s;
							break;
						}
					}
					*/
                    _mailBl.SendTriggerMail(sender.MemberID, brand.Site.SiteID, imp.BrandID, 
						_mapper.GetProviderTemplateID(brand.Site, EmailType.EcardToMember),
                        recipient.EmailAddress, tc.BuildList(), null, EmailType.EcardToMember);
				}
			}
		}

		/// <summary>
		/// Send ecard to off site friend email.
		/// </summary>
		/// <param name="impulse"></param>
		public void SendEcardToFriend(ImpulseBase impulse)
		{
			TokenConstructor tc = new TokenConstructor();
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			EcardToFriendImpulse imp = (EcardToFriendImpulse) impulse;
            IMemberDTO sender = MailHandlerUtils.GetIMemberDTO(imp.SenderMemberID, MemberLoadFlags.None);
			
			string ecardUrl = EmailMemberHelper.CreateEcardUrl(null, brand, imp.CardUrl);
			if (ecardUrl != Constants.NULL_STRING)
			{
				tc.Add(TokenNames.CARD_URL, ecardUrl);
				tc.Add(TokenNames.CARD_THUMBNAIL, imp.CardThumbnail);
				tc.Add(TokenNames.SENDER_USER_NAME, sender.GetUserName(brand));
				tc.Add(TokenNames.RECIPIENT_EMAIL_ADDRESS, imp.RecipientEmailAddress);
				tc.Add(TokenNames.SENT_DATE, EmailMemberHelper.GetDateDisplay(imp.SentDate, brand));
				tc.Add(TokenNames.SENDER_AGE, EmailMemberHelper.DetermineMemberAge(sender, brand).ToString());
				tc.Add(TokenNames.SENDER_THUMBNAIL, EmailMemberHelper.DetermineMemberThumbnail(sender, brand));
				tc.Add(TokenNames.SENDER_LOCATION, EmailMemberHelper.DetermineMemberRegionDisplay(sender, brand));
				
				Site site = null;
				Sites coll = BrandConfigSA.Instance.GetSites();

				foreach(Site s in coll)
				{
					if(s.SiteID == imp.SiteID)
					{
						site = s;
						break;
					}
				}

                _mailBl.SendTriggerMail(imp.SenderMemberID, site.SiteID, imp.BrandID, 
					_mapper.GetProviderTemplateID(site, EmailType.EcardToFriend),
                    imp.RecipientEmailAddress, tc.BuildList(), null, EmailType.EcardToFriend);
			}
		}

		/// <summary>
		/// Send renewal failure notification email.
		/// </summary>
		/// <param name="impulse"></param>
		public void SendRenewalFailureNotification(ImpulseBase impulse)
		{
			TokenConstructor tc = new TokenConstructor();
			Sites sites = BrandConfigSA.Instance.GetSites();
			RenewalFailureNotificationImpulse imp = (RenewalFailureNotificationImpulse) impulse;
			
			foreach(Site site in sites)
			{
				if(imp.SiteID == site.SiteID)
				{
					tc.Add(TokenNames.USER_NAME, imp.UserName);
                    _mailBl.SendTriggerMail(MemberSA.Instance.GetMemberIDByEmail(imp.EmailAddress, site.Community.CommunityID), site.SiteID, imp.BrandID,
						_mapper.GetProviderTemplateID(site, EmailType.RenewalFailureNotification),
                        imp.EmailAddress, tc.BuildList(), null, EmailType.RenewalFailureNotification);
				}
			}
		}

		/// <summary>
		/// Send to friend Question of the Week
		/// </summary>
		/// <param name="impulse"></param>
		public void SendMemberQuestion(ImpulseBase impulse)
		{
			Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
			TokenConstructor tc = new TokenConstructor();
			SendMemberQuestionAndAnswerImpulse imp = (SendMemberQuestionAndAnswerImpulse)impulse;
			
			tc.Add(TokenNames.SITEID, brand.Site.SiteID.ToString());
			tc.Add(TokenNames.USER_NAME, imp.FriendName);
			//sender's name
			tc.Add(TokenNames.S2F_FRIENDS_NAME, imp.SenderName.ToString());
			//message
			tc.Add(TokenNames.SEND_TO_FRIEND_BODY, imp.Message);
			//sender's memberid
			tc.Add(TokenNames.S2F_MEMBER_ID, imp.SenderMemberID.ToString());

			tc.Add(TokenNames.BOARD_ID, imp.QuestionId.ToString());
			tc.Add(TokenNames.BOARD_NAME, imp.QuestionText);

            _mailBl.SendTriggerMail(imp.SenderMemberID, brand.Site.SiteID, imp.BrandID,
				_mapper.GetProviderTemplateID(brand.Site, EmailType.SendMemberQuestionAndAnswer),
                imp.FriendEmail, tc.BuildList(), null, EmailType.SendMemberQuestionAndAnswer);
		}


		#endregion

		public void SendJDateMobileRegistrationConfirmation(ImpulseBase impulse)
		{
			JDateMobileRegistrationConfirmationImpulse imp = (JDateMobileRegistrationConfirmationImpulse)impulse;
			Brand brand = MailHandlerUtils.GetBrand(imp.BrandID);

			if(brand != null)
			{
				TokenConstructor tc = new TokenConstructor();
				tc.Add(TokenNames.USER_NAME, imp.UserName);
				tc.Add(TokenNames.MOBILE_NUMBER, imp.MobileNumber);

                _mailBl.SendTriggerMail(imp.MemberID, brand.Site.SiteID, imp.BrandID, 
					_mapper.GetProviderTemplateID(brand.Site, EmailType.JDateMobileRegistrationConfirmation),
                    imp.EmailAddress, tc.BuildList(), null, EmailType.JDateMobileRegistrationConfirmation);
			}

		}

        public void SendAllAccessInitialEmail(ImpulseBase pImpulse)
        {
            var imp = (AllAccessInitialEmailImpulse)pImpulse;

            var recipient = MailHandlerUtils.GetIMemberDTO(imp.TargetMemberId, MemberLoadFlags.None);
            var recipientBrand = BrandConfigSA.Instance.GetBrandByID(imp.BrandID);
            bool send = isUserAlertSettingOn(recipient, recipientBrand);

            if (send)
            {
                var sender = MailHandlerUtils.GetIMemberDTO(imp.SenderMemberId, MemberLoadFlags.None);
                var senderBrand = BrandConfigSA.Instance.GetBrandByID(imp.SenderBrandId);

                var senderUsername = sender.GetUserName(senderBrand);
                var senderThumbnail = EmailMemberHelper.DetermineMemberThumbnail(sender, senderBrand);
                var senderAge = EmailMemberHelper.DetermineMemberAge(sender, senderBrand).ToString();
                //var senderSeeking = EmailMemberHelper.DetermineMemberSeeking(senderBrand, sender);
                var senderGender = EmailMemberHelper.DetermineMemberGender(senderBrand, sender);
                var senderLocation = EmailMemberHelper.DetermineMemberRegionDisplay(sender, senderBrand);
                var uid = EmailMemberHelper.DetermineUIDForPixel(imp.MessageListId, imp.TargetMemberId, recipientBrand.Site.Community.CommunityID);

                TokenConstructor tc = new TokenConstructor();
                tc.Add(TokenNames.FROM_MEMBER_NAME, senderUsername);
                tc.Add(TokenNames.FROM_MEMBER_THUMBNAIL, senderThumbnail);
                tc.Add(TokenNames.FROM_MEMBER_AGE, senderAge);
                tc.Add(TokenNames.FROM_MEMBER_SEEKING, senderGender);
                tc.Add(TokenNames.FROM_MEMBER_LOCATION, senderLocation);
                tc.Add(TokenNames.MEMBER_MAIL_ID, imp.MessageListId.ToString());
                tc.Add(TokenNames.BOARD_ID, uid);
                tc.Add(TokenNames.BOARD_NAME, imp.EmailSubject);
                tc.Add(TokenNames.SEND_TO_FRIEND_BODY, imp.EmailContent.Substring(0,  Math.Min(imp.EmailContent.Length, 2000)));

                _mailBl.SendTriggerMail(recipient.MemberID, recipientBrand.Site.SiteID, imp.BrandID,
                        _mapper.GetProviderTemplateID(recipientBrand.Site, EmailType.AllAccessInitialEmail),
                        recipient.EmailAddress, tc.BuildList(), null, EmailType.AllAccessInitialEmail);
            }
        }

        public void SendAllAccessReplyEmail(ImpulseBase pImpulse)
        {
            var imp = (AllAccessReplyEmailImpulse)pImpulse;

            var recipient = MailHandlerUtils.GetIMemberDTO(imp.TargetMemberId, MemberLoadFlags.None);
            var recipientBrand = BrandConfigSA.Instance.GetBrandByID(imp.BrandID);

            bool send = isUserAlertSettingOn(recipient, recipientBrand);

            if (send)
            {
                var sender = MailHandlerUtils.GetIMemberDTO(imp.SenderMemberId, MemberLoadFlags.None);
                var senderBrand = BrandConfigSA.Instance.GetBrandByID(imp.SenderBrandId);

                var senderUsername = sender.GetUserName(senderBrand);
                var senderThumbnail = EmailMemberHelper.DetermineMemberThumbnail(sender, senderBrand);
                var senderAge = EmailMemberHelper.DetermineMemberAge(sender, senderBrand).ToString();
                //var senderSeeking = EmailMemberHelper.DetermineMemberSeeking(senderBrand, sender);
                var senderGender = EmailMemberHelper.DetermineMemberGender(senderBrand, sender);
                var senderLocation = EmailMemberHelper.DetermineMemberRegionDisplay(sender, senderBrand);

                TokenConstructor tc = new TokenConstructor();
                tc.Add(TokenNames.FROM_MEMBER_NAME, senderUsername);
                tc.Add(TokenNames.FROM_MEMBER_THUMBNAIL, senderThumbnail);
                tc.Add(TokenNames.FROM_MEMBER_AGE, senderAge);
                tc.Add(TokenNames.FROM_MEMBER_SEEKING, senderGender);
                tc.Add(TokenNames.FROM_MEMBER_LOCATION, senderLocation);
                tc.Add(TokenNames.MEMBER_MAIL_ID, imp.MessageListId.ToString());
                tc.Add(TokenNames.BOARD_NAME, imp.EmailSubject);
                tc.Add(TokenNames.SEND_TO_FRIEND_BODY, imp.EmailContent.Substring(0, Math.Min(imp.EmailContent.Length, 2000)));

                _mailBl.SendTriggerMail(recipient.MemberID, recipientBrand.Site.SiteID, imp.BrandID,
                        _mapper.GetProviderTemplateID(recipientBrand.Site, EmailType.AllAccessReplyEmail),
                        recipient.EmailAddress, tc.BuildList(), null, EmailType.AllAccessReplyEmail);
            }
            
        }

        public void SendAllAccessNudgeEmail(ImpulseBase pImpulse)
        {
            var imp = (AllAccessEmailNudgeImpulse)pImpulse;

            var recipient = MailHandlerUtils.GetIMemberDTO(imp.TargetMemberId, MemberLoadFlags.None);
            var recipientBrand = BrandConfigSA.Instance.GetBrandByID(imp.BrandID);

            bool send = isUserAlertSettingOn(recipient, recipientBrand);
            if (send)
            {
                var sender = MailHandlerUtils.GetIMemberDTO(imp.SenderMemberId, MemberLoadFlags.None);
                var senderBrand = BrandConfigSA.Instance.GetBrandByID(imp.SenderBrandId);

                var senderUsername = sender.GetUserName(senderBrand);
                var senderThumbnail = EmailMemberHelper.DetermineMemberThumbnail(sender, senderBrand);
                var senderAge = EmailMemberHelper.DetermineMemberAge(sender, senderBrand).ToString();
                //var senderSeeking = EmailMemberHelper.DetermineMemberSeeking(senderBrand, sender);
                var senderGender = EmailMemberHelper.DetermineMemberGender(senderBrand, sender);
                var senderLocation = EmailMemberHelper.DetermineMemberRegionDisplay(sender, senderBrand);
                var uid = EmailMemberHelper.DetermineUIDForPixel(imp.MessageListId, imp.TargetMemberId, recipientBrand.Site.Community.CommunityID);

                // get the date of the original vip message
                var emailMessage = EmailMessageSA.Instance.RetrieveMessage(recipient.MemberID, recipientBrand.Site.Community.CommunityID, imp.MessageListId);
                var mailDate = emailMessage.Message.InsertDate;

                TokenConstructor tc = new TokenConstructor();
                tc.Add(TokenNames.FROM_MEMBER_NAME, senderUsername);
                tc.Add(TokenNames.FROM_MEMBER_THUMBNAIL, senderThumbnail);
                tc.Add(TokenNames.FROM_MEMBER_AGE, senderAge);
                tc.Add(TokenNames.FROM_MEMBER_SEEKING, senderGender);
                tc.Add(TokenNames.FROM_MEMBER_LOCATION, senderLocation);
                tc.Add(TokenNames.MEMBER_MAIL_ID, imp.MessageListId.ToString());
                tc.Add(TokenNames.BOARD_ID, uid);
                tc.Add(TokenNames.BOARD_NAME, imp.EmailSubject);
                tc.Add(TokenNames.SEND_TO_FRIEND_BODY, imp.EmailContent.Substring(0, Math.Min(imp.EmailContent.Length, 2000)));
                tc.Add(TokenNames.NEW_MAIL_TIME, mailDate.ToString("dd/MM/yyyy"));
                tc.Add(TokenNames.S2F_MEMBER_ID, sender.MemberID.ToString());

                _mailBl.SendTriggerMail(recipient.MemberID, recipientBrand.Site.SiteID, imp.BrandID,
                        _mapper.GetProviderTemplateID(recipientBrand.Site, EmailType.AllAccessNudgeEmail),
                        recipient.EmailAddress, tc.BuildList(), null, EmailType.AllAccessNudgeEmail);
            }
        }

        public void SendAllAccessNoThanksEmail(ImpulseBase pImpulse)
        {
            var imp = (AllAccessNoThanksEmailImpulse)pImpulse;

            var sender = MailHandlerUtils.GetIMemberDTO(imp.SenderMemberId, MemberLoadFlags.None);
            var recipient = MailHandlerUtils.GetIMemberDTO(imp.TargetMemberId, MemberLoadFlags.None);
            var senderBrand = BrandConfigSA.Instance.GetBrandByID(imp.SenderBrandId);
            var recipientBrand = BrandConfigSA.Instance.GetBrandByID(imp.BrandID);

            var senderUsername = sender.GetUserName(senderBrand);
            var senderThumbnail = EmailMemberHelper.DetermineMemberThumbnail(sender, senderBrand);
            var senderAge = EmailMemberHelper.DetermineMemberAge(sender, senderBrand).ToString();
            //var senderSeeking = EmailMemberHelper.DetermineMemberSeeking(senderBrand, sender);
            var senderGender = EmailMemberHelper.DetermineMemberGender(senderBrand, sender);
            var senderLocation = EmailMemberHelper.DetermineMemberRegionDisplay(sender, senderBrand);

            TokenConstructor tc = new TokenConstructor();
            tc.Add(TokenNames.FROM_MEMBER_NAME, senderUsername);
            tc.Add(TokenNames.FROM_MEMBER_THUMBNAIL, senderThumbnail);
            tc.Add(TokenNames.FROM_MEMBER_AGE, senderAge);
            tc.Add(TokenNames.FROM_MEMBER_SEEKING, senderGender);
            tc.Add(TokenNames.FROM_MEMBER_LOCATION, senderLocation);

            _mailBl.SendTriggerMail(recipient.MemberID, recipientBrand.Site.SiteID, imp.BrandID,
                    _mapper.GetProviderTemplateID(recipientBrand.Site, EmailType.AllAccessNoThanksEmail),
                    recipient.EmailAddress, tc.BuildList(), null, EmailType.AllAccessNoThanksEmail);

        }

        public void SendBOGONotification(ImpulseBase pImpulse)
        {
            var imp = (BOGONotificationImpulse)pImpulse;

            var sender = MailHandlerUtils.GetIMemberDTO(imp.SenderMemberId, MemberLoadFlags.None);
            var senderBrand = BrandConfigSA.Instance.GetBrandByID(imp.BrandID);
            var senderFirstName = sender.GetAttributeText(senderBrand, "SiteFirstName");
            var senderAge = EmailMemberHelper.DetermineMemberAge(sender, senderBrand).ToString();
            var senderLocation = EmailMemberHelper.DetermineMemberRegionDisplay(sender, senderBrand);
            var senderUsername = sender.GetUserName(senderBrand);
            var senderThumbnail = EmailMemberHelper.DetermineMemberThumbnail(sender, senderBrand);
            var senderReason = imp.GiftPromo;
            var senderBoardID = imp.GiftCode;
            var senderBoardName = sender.MemberID.ToString();

            TokenConstructor tc = new TokenConstructor();
            tc.Add(TokenNames.FIRST_NAME, senderFirstName);
            tc.Add(TokenNames.FROM_MEMBER_AGE, senderAge);
            tc.Add(TokenNames.FROM_MEMBER_LOCATION, senderLocation);
            tc.Add(TokenNames.FROM_MEMBER_NAME, senderUsername);
            tc.Add(TokenNames.FROM_MEMBER_THUMBNAIL, senderThumbnail);
            tc.Add(TokenNames.COMMENT, senderReason);
            tc.Add(TokenNames.BOARD_ID, senderBoardID);
            tc.Add(TokenNames.BOARD_NAME, senderBoardName);

            _mailBl.SendTriggerMail(sender.MemberID, senderBrand.Site.SiteID, imp.BrandID,
                    _mapper.GetProviderTemplateID(senderBrand.Site, EmailType.BOGONotification),
                    imp.EmailAddress, tc.BuildList(), null, EmailType.BOGONotification);
            
        }

        public void SendAllAccessReturnEmail(ImpulseBase pImpulse)
        {
            // sender = sender of the original AA email
            // recipient = recipient of the origonal AA email
            // in this case, we are refunding the sender, so the email will be sent to the "sender" with the
            // recipient's profile info in the email           

            var imp = (AllAccessReturnEmailImpulse)pImpulse;

            var sender = MailHandlerUtils.GetIMemberDTO(imp.SenderMemberId, MemberLoadFlags.None);
            var recipient = MailHandlerUtils.GetIMemberDTO(imp.TargetMemberId, MemberLoadFlags.None);
            var senderBrand = BrandConfigSA.Instance.GetBrandByID(imp.SenderBrandId);
            var recipientBrand = BrandConfigSA.Instance.GetBrandByID(imp.BrandID);

            var recUsername = recipient.GetUserName(recipientBrand);
            var recThumbnail = EmailMemberHelper.DetermineMemberThumbnail(recipient, recipientBrand);
            var recAge = EmailMemberHelper.DetermineMemberAge(recipient, recipientBrand).ToString();
            var recGender = EmailMemberHelper.DetermineMemberGender(recipientBrand, recipient);
            var recLocation = EmailMemberHelper.DetermineMemberRegionDisplay(recipient, recipientBrand);

            var firstName = sender.GetAttributeText(senderBrand, FIRST_NAME_ATTR);

            TokenConstructor tc = new TokenConstructor();
            
            tc.Add(TokenNames.FIRST_NAME, firstName);
            tc.Add(TokenNames.FROM_MEMBER_NAME, recUsername);
            tc.Add(TokenNames.FROM_MEMBER_THUMBNAIL, recThumbnail);
            tc.Add(TokenNames.FROM_MEMBER_AGE, recAge);
            tc.Add(TokenNames.FROM_MEMBER_SEEKING, recGender);
            tc.Add(TokenNames.FROM_MEMBER_LOCATION, recLocation);

            _mailBl.SendTriggerMail(sender.MemberID, senderBrand.Site.SiteID, imp.BrandID,
                    _mapper.GetProviderTemplateID(senderBrand.Site, EmailType.AllAccessReturnEmail),
                    sender.EmailAddress, tc.BuildList(), null, EmailType.AllAccessReturnEmail);
        }

        public void SendViewedYouEmail(ImpulseBase pImpulse)
        {
            var imp = (ViewedYouEmailImpulse)pImpulse;

            var member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.None);
            var sender = MailHandlerUtils.GetIMemberDTO(imp.ViewedByMemberID, MemberLoadFlags.None);
            var senderBrand = BrandConfigSA.Instance.GetBrandByID(imp.BrandID);
            var senderAge = EmailMemberHelper.DetermineMemberAge(sender, senderBrand).ToString();
            var senderLocation = EmailMemberHelper.DetermineMemberRegionDisplay(sender, senderBrand);
            var senderUsername = sender.GetUserName(senderBrand);
            var senderThumbnail = EmailMemberHelper.DetermineMemberThumbnail(sender, senderBrand);
            var senderSeeking = EmailMemberHelper.DetermineMemberSeeking(senderBrand, sender);
            var memberUserName = member.GetUserName(senderBrand);

            TokenConstructor tc = new TokenConstructor();
            Brand brand = MailHandlerUtils.GetBrand(imp.BrandID);
            var firstName = member.GetAttributeText(brand, FIRST_NAME_ATTR);
            tc.Add(TokenNames.FIRST_NAME, firstName);
            tc.Add(TokenNames.HOT_LISTED_BY_MEMBER_THUMBNAIL, senderThumbnail);
            tc.Add(TokenNames.HOT_LISTED_BY_MEMBER_USER_NAME, senderUsername);
            tc.Add(TokenNames.HOT_LISTED_BY_MEMBER_AGE, senderAge);
            tc.Add(TokenNames.HOT_LISTED_BY_MEMBER_SEEKING, senderSeeking);
            tc.Add(TokenNames.HOT_LISTED_BY_MEMBER_LOCATION, senderLocation);
            tc.Add(TokenNames.FROM_MEMBER_NAME, senderUsername);
            tc.Add(TokenNames.USER_NAME, memberUserName);
            tc.Add(TokenNames.COMMENT, imp.NumberOfMembersWhoViewedSinceLastEmail+"");

            _mailBl.SendTriggerMail(imp.MemberID, senderBrand.Site.SiteID, imp.BrandID,
                    _mapper.GetProviderTemplateID(senderBrand.Site, EmailType.ViewedYouEmail),
                    member.EmailAddress, tc.BuildList(), null, EmailType.ViewedYouEmail);
        }

        public void SendNoEssayRequestEmail(ImpulseBase pImpulse)
        {
            var imp = (NoEssayRequestEmailImpulse)pImpulse;

			var toMember = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.None);
            Brand brand = MailHandlerUtils.GetBrand(pImpulse.BrandID);
            var member = MailHandlerUtils.GetIMemberDTO(imp.MemberID, MemberLoadFlags.None);
            var sender = MailHandlerUtils.GetIMemberDTO(imp.RequestedByMemberID, MemberLoadFlags.None);
            var senderBrand = BrandConfigSA.Instance.GetBrandByID(imp.BrandID);
            var senderAge = EmailMemberHelper.DetermineMemberAge(sender, senderBrand).ToString();
            var senderLocation = EmailMemberHelper.DetermineMemberRegionDisplay(sender, senderBrand);
            var senderUsername = sender.GetUserName(senderBrand);
            var senderThumbnail = EmailMemberHelper.DetermineMemberThumbnail(sender, senderBrand);
            var senderSeeking = EmailMemberHelper.DetermineMemberSeeking(senderBrand, sender);
            var memberUserName = member.GetUserName(senderBrand);

            bool send = true;

			AttributeGroup group = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(brand.Site.Community.CommunityID, EmailAlertConstant.ATTRIBUTE_ID);
			
			int mask = (int)EmailAlertMask.NoEssayRequestEmail;
			int userSetting = toMember.GetAttributeInt(brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, int.Parse(group.DefaultValue));
			if((mask & userSetting) != mask)
			{
				send = false;
			}

            if (send)
            {

                TokenConstructor tc = new TokenConstructor();
                tc.Add(TokenNames.HOT_LISTED_BY_MEMBER_THUMBNAIL, senderThumbnail);
                tc.Add(TokenNames.HOT_LISTED_BY_MEMBER_USER_NAME, senderUsername);
                tc.Add(TokenNames.HOT_LISTED_BY_MEMBER_AGE, senderAge);
                tc.Add(TokenNames.HOT_LISTED_BY_MEMBER_SEEKING, senderSeeking);
                tc.Add(TokenNames.HOT_LISTED_BY_MEMBER_LOCATION, senderLocation);
                tc.Add(TokenNames.USER_NAME, memberUserName);

                _mailBl.SendTriggerMail(imp.MemberID, senderBrand.Site.SiteID, imp.BrandID,
                                                   _mapper.GetProviderTemplateID(senderBrand.Site,
                                                                                 EmailType.NoEssayRequestEmail),
                                                   member.EmailAddress, tc.BuildList(), null, EmailType.NoEssayRequestEmail);
            }
        }

        public void SendBBEPasscodeEmail(ImpulseBase pImpulse)
        {
            var imp = (BBEPasscodeImpulse)pImpulse;
            var senderBrand = BrandConfigSA.Instance.GetBrandByID(imp.BrandID);

            TokenConstructor tc = new TokenConstructor();
            tc.Add(TokenNames.CONFIRMATION_NUMBER, imp.Passcode);

            _mailBl.SendTriggerMail(imp.MemberID, senderBrand.Site.SiteID, imp.BrandID,
                    _mapper.GetProviderTemplateID(senderBrand.Site, EmailType.BBEPasscodeEmail),
                    imp.EmailAddress, tc.BuildList(), null, EmailType.BBEPasscodeEmail);
        }

        public void SendSodbPhotoUploadEmail(ImpulseBase pImpulse)
        {
            var imp = (SodbPhotoUploadImpulse)pImpulse;
            var brand = BrandConfigSA.Instance.GetBrandByID(imp.BrandID);

            var tc = new TokenConstructor();

            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(imp.MemberId, MemberLoadFlags.None);
            var firstName = member.GetAttributeText(brand, FIRST_NAME_ATTR);
            tc.Add(TokenNames.FIRST_NAME, firstName);

            tc.Add(TokenNames.SODB_EMAIL, imp.EmailAddress);
            if(imp.IsBHMember)
            {
                tc.Add(TokenNames.SODB_SITEID, brand.Site.SiteID.ToString());
                tc.Add(TokenNames.SODB_MEMBERID, imp.MemberId.ToString());
            }
            else
            {
                tc.Add(TokenNames.MINGLE_SODB_SITEID, brand.Site.SiteID.ToString());
                tc.Add(TokenNames.SODB_USERID, imp.MemberId.ToString());
            }

            _mailBl.SendTriggerMail(imp.MemberId, brand.Site.SiteID, imp.BrandID,
                    _mapper.GetProviderTemplateID(brand.Site, EmailType.SodbPhotoUpload),
                    imp.EmailAddress, tc.BuildList(), null, EmailType.SodbPhotoUpload);
        }

        /// <summary>
        /// Send profile changed confirmation email
        /// </summary>
        /// <param name="impulse"></param>
        /// <param name="brandInsertDate"></param>
        public void SendProfileChangedConfirmation(ImpulseBase impulse)
        {
            Brand brand = MailHandlerUtils.GetBrand(impulse.BrandID);
            ProfileChangedConfirmationImpulse imp = (ProfileChangedConfirmationImpulse)impulse;

            if (imp.Fields.Count > 0)
            {
                var tc = new TokenConstructor();

                tc.Add(TokenNames.FIRST_NAME,imp.FirstName);
                tc.Add(TokenNames.Fields, string.Join(",",imp.Fields));

                _mailBl.SendTriggerMail(imp.MemberID, brand.Site.SiteID, imp.BrandID,
                    _mapper.GetProviderTemplateID(brand.Site, EmailType.ProfileChangedConfirmation),
                    imp.EmailAddress, tc.BuildList(), null, EmailType.ProfileChangedConfirmation);
            }
        }


	    #endregion

        #region Private Methods
        private bool isUserAlertSettingOn(IMemberDTO pMember, Brand pBrand)
        {
            AttributeGroup group = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(pBrand.Site.Community.CommunityID, EmailAlertConstant.ATTRIBUTE_ID);
            int mask = (int)EmailAlertMask.NewEmailAlert;
            int userEmailSetting = pMember.GetAttributeInt(pBrand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, int.Parse(group.DefaultValue));

            return (userEmailSetting & mask) == mask;
        }
        #endregion

	}

}
