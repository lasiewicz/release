using System;
using System.Diagnostics;
using System.Threading;
using System.Collections;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.ExternalMail.BusinessLogic;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.ExternalMail.ValueObjects.ServiceDefinitions;
using Matchnet.ExternalMail.ValueObjects.DoNotEmail;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.ExternalMail.BusinessLogic.QueueProcessing;


namespace Matchnet.ExternalMail.ServiceManagers
{
	public class ExternalMailSM : MarshalByRefObject, IExternalMailService, IServiceManager, IDisposable, IDoNotEmailService
	{
        private IQueueProcessor _queueProcessor;
		private string			_cfgPollingProcessorHost;
		private Thread			_threadGetNext;

		public ExternalMailSM()
		{
			DoNotEmailBL.Instance.LoadCache();
			_cfgPollingProcessorHost = RuntimeSettings.GetSetting("EXTERNALMAILSVC_POLL_PROCESSOR").ToLower();

            _queueProcessor =
                int.Parse(RuntimeSettings.GetSetting("EXTMAILSVC_QUEUE_PROCESSOR_VERSION")) == 1 ?
                    (IQueueProcessor)new BusinessLogic.QueueProcessor() :
                    (IQueueProcessor)new BusinessLogic.QueueProcessing.QueueProcessor();
			_queueProcessor.Start();

			//poll to get next summary sms items from mnXmail database
			if (_cfgPollingProcessorHost == System.Environment.MachineName.ToLower())
			{
				_threadGetNext = new Thread(new ThreadStart(SMSScheduleLoaderBL.Instance.Run));
				_threadGetNext.Name = "Next Batch SMS Cycle Thread";
				_threadGetNext.Start();
				System.Diagnostics.EventLog.WriteEntry(Matchnet.ExternalMail.ValueObjects.ServiceConstants.SERVICE_NAME,
					"Started next batch SMS cycle thread: " + _threadGetNext.Name,
					EventLogEntryType.Information);
			}

			
		}


		public override object InitializeLifetimeService()
		{
			return null;
		}

		
		public void PrePopulateCache()
		{
		}


		public void Enqueue(ImpulseBase impulse)
		{
			try
			{
			    impulse = impulse.CopyWithSettingFirstAttemptUtc();
				ExternalMailBL.Instance.Enqueue(impulse);
			}
			catch (Exception ex)
			{
				string message = string.Empty;
				if(impulse != null)
				{
					message = "Exception while queueing impulse object with query string: " + impulse.GetQueryString() + ".  Message was: " + ex.Message + " and StackTrace was: " + ex.StackTrace;
				}
				else
				{
					message = "Exception while trying to queue null impulse object.  Message was: " + ex.Message + " and StackTrace was: " + ex.StackTrace;
				}
				EventLog.WriteEntry(Matchnet.ExternalMail.ValueObjects.ServiceConstants.SERVICE_NAME, message, EventLogEntryType.Warning);

				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error queuing impulse object.", ex);
			}
		}
		public bool NewInboxItemReceived(int memberID, int brandID, SMSAlertsDefinitions.MessageType messageType, bool isFriend)
		{
			return SMSAlertsBL.Instance.NewInboxItemReceived(memberID, brandID, messageType, isFriend);
		}

		public void UpdatePhonesList(int memberID, int siteID, string phoneNumber, SMSAlertsDefinitions.Action action)
		{
			SMSAlertsBL.Instance.UpdatePhonesList(memberID, siteID, phoneNumber, action);
		}

		public bool IsPhoneAvailable(int siteID, string phoneNumber)
		{
			return SMSAlertsBL.Instance.IsPhoneAvailable(siteID, phoneNumber);
		}

		public bool AddEmailAddress(int brandID, string emailAddress, int memberID)
		{
			return DoNotEmailBL.Instance.AddEmailAddress(brandID, emailAddress, memberID);
		}

		public bool AddEmailAddressBySiteID(int siteID, string emailAddress)
		{
			return DoNotEmailBL.Instance.AddEmailAddressBySiteID(siteID, emailAddress);
		}

		public bool RemoveEmailAddress(int siteID, string emailAddress)
		{
			return DoNotEmailBL.Instance.RemoveEmailAddress(siteID, emailAddress);
		}

		public DoNotEmailEntry GetEntryBySiteAndEmailAddress(int siteID, string emailAddress)
		{
			return DoNotEmailBL.Instance.GetEntryBySiteAndEmailAddress(siteID, emailAddress);
		}

		public DoNotEmailEntry GetEntryByEmailAddress(string emailAddress)
		{
			return DoNotEmailBL.Instance.GetEntryByEmailAddress(emailAddress);
		}

		public ArrayList GetEntriesBySite(int siteID)
		{
			return DoNotEmailBL.Instance.GetEntriesBySite(siteID);
		}

		public ArrayList GetEntries()
		{
			return DoNotEmailBL.Instance.GetEntries();
		}

		public ArrayList Search(int siteID, string partialEmailAddress, int startRow, int pageSize, ref int totalRows)
		{
			return DoNotEmailBL.Instance.Search(siteID, partialEmailAddress, startRow, pageSize, ref totalRows);
		}

		public ArrayList GetSiteSummary()
		{
			return DoNotEmailBL.Instance.GetSiteSummary();
		}

		#region IDisposable Members
		public void Dispose()
		{
			if (_queueProcessor != null)
			{
				_queueProcessor.Stop();
			}

			if (_threadGetNext != null)
			{
				_threadGetNext.Join(20000);
			}
		}
		#endregion
	}
}
