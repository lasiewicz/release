﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Matchnet.ExternalMail.BusinessLogic;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using NUnit.Framework;

namespace Matchnet.ExternalMail.Tests
{
    [TestFixture]    
    public class ExternalMailBLTests
    {
        [TestFixtureSetUp]
        public void SetUp()
        {
            YesMailBL.Instance.IsDebug = true;

        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            YesMailBL.Instance.IsDebug = false;
        }

        //[Test]
        //public void TestViewedYouEmail()
        //{
        //    ViewedYouEmailImpulse viewedYouEmailImpulse = new ViewedYouEmailImpulse(100004579, 100067248, 6, 1003);
        //    YesMailHandler.Instance.SendEmail(viewedYouEmailImpulse);
        //}

        //[Test]
        //public void TestSodbPhotoUploadEmail()
        //{
        //    SodbPhotoUploadImpulse imp = new SodbPhotoUploadImpulse(111644313, 90810, "kgunasekaran@spark.net", false);
        //    YesMailHandler.Instance.SendEmail(imp);
        //    //imp = new SodbPhotoUploadImpulse(111644313, 1003, "kgunasekaran@spark.net", true);
        //   // YesMailHandler.Instance.SendEmail(imp);
        //    //imp = new SodbPhotoUploadImpulse(111644313, 92310, "kgunasekaran@spark.net");
        //    //YesMailHandler.Instance.SendEmail(imp);
        //}
        /*
        [Test]
        public void TestMaropostRegistrationEmail()
        {
            var imp = new RegVerificationImpulse(101805229, 1003);
            MaropostHandler.Instance.SendEmail(imp);
        }

        [Test]
        public void TestMaropostSubscriptionConfirmationEmail()
        {
            var imp = new SubscriptionConfirmationImpulse(101805229, 103,  1003,
                "nrubell","nrubell@spark.net","Nick","Rubell","123 Address","City1","ST","9000",DateTime.Today,
							64,
							1,
							19.99m,
							3,
							Matchnet.DurationType.Month,
							15.99m,
							3,
							Matchnet.DurationType.Month,
							"3456",
							1,
							"767676767633");
            MaropostHandler.Instance.SendEmail(imp);
        }
         */
        [Test]
        public void TestMaropostResetPasswordEmail()
        {
            var imp = new ResetPasswordImpulse(1003, 101805229, "http://www.JDate.com?a=b");
            MaropostHandler.Instance.SendEmail(imp);
        }
        
    }
}
