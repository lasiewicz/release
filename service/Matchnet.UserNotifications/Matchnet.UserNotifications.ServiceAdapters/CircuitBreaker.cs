﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Matchnet.UserNotifications.ValueObjects;

namespace Matchnet.UserNotifications.ServiceAdapters
{
    /// <summary>  
    /// Castle Interceptor Circuit Breaker.  
    ///   
    /// Used in an AOP-style to wrap calls to protected methods.  
    /// If the number of unhandled exceptions that occur within a specified period  
    ///  exceeds a specified threshold, then invocations will cease to be passed to the   
    ///  protected code until a specified timeout has elapsed.  
    /// Instead, OpenCircuitExceptions will immediately be thrown when invocations are  
    ///  attempted.  
    ///   
    /// Based on code by Davy Brion (http://davybrion.com) and Ayende Rahien (http://ayende.com).  
    ///   
    /// For background on the Circuit Breaker pattern, see "Release It! - Design and Deploy  
    ///  Production-Ready Software" by Michael Nygard (Pragmatic Bookshelf, 2007).  
    /// </summary>  
    public class CircuitBreaker
    {
        private readonly object monitor = new object();
        private int _version = 2;

        private CircuitBreakerState _state;
        private List<DateTime> failures;
        
        public int MaxExceptionCount {
            get {
                int maxExceptionCount = 50;
                try
                {
                    string maxExceptionString = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USERNOTIFICATIONSSVC_MAX_EXCEPTION_COUNT");
                    maxExceptionCount = Int32.Parse(maxExceptionString);
                }
                catch (Exception e)
                {
                    maxExceptionCount = 50;
                }
                return maxExceptionCount;
            }
        }

        public TimeSpan ExceptionDuration {
            get {
                int maxExceptionDurationInSeconds = 20;
                try
                {
                    string maxExceptionDurationString = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USERNOTIFICATIONSSVC_EXCEPTION_DURATION_SECONDS");
                    maxExceptionDurationInSeconds = Int32.Parse(maxExceptionDurationString);                    
                }
                catch (Exception e)
                {
                    maxExceptionDurationInSeconds = 20;
                }
                return new TimeSpan(0,0,maxExceptionDurationInSeconds);
            }
        }

        public TimeSpan OpenStateDuration
        {
            get
            {
                int openStateDurationInSeconds = 30;
                try
                {
                    string openStateDurationString = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USERNOTIFICATIONSSVC_OPEN_STATE_DURATION_SECONDS");
                    openStateDurationInSeconds = Int32.Parse(openStateDurationString);
                }
                catch (Exception e)
                {
                    openStateDurationInSeconds = 30;
                }
                return new TimeSpan(0, 0, openStateDurationInSeconds);
            }
        }

        public string State
        {
            get { return _state.GetType().Name; }
        }

        /// <summary>  
        /// Initializes a new instance of the CircuitBreaker class.  
        /// </summary>  
        public CircuitBreaker()
        {
            this.failures = new List<DateTime>();
            this.MoveToClosedState();

        }

        /// <summary>  
        /// Intercepts the planned method invocation.  
        /// </summary>  
        /// <param name="invocation">Original method call.</param>  
        public List<UserNotificationViewObject> Intercept(UserNotificationsServiceSA.Apply apply, Hashtable applyParams)
        {
            List<UserNotificationViewObject> notifications = null;
            
            _state.ProtectedCodeIsAboutToBeCalled();

            try
            {
                notifications=apply(applyParams);
            }
            catch (Exception e)
            {
                using (TimedLock.Lock(monitor))
                {
                    // Add current datetime to list of failed invocations.  
                    failures.Add(DateTime.UtcNow);
                    _state.ActUponException(e);
                }
                throw;
            }

            _state.ProtectedCodeHasBeenCalled();

            return notifications;
        }

        private void MoveToClosedState()
        {
            if (!(_state is ClosedState))
                _state = new ClosedState(this);
        }

        private void MoveToOpenState()
        {
            if (!(_state is OpenState))
                _state = new OpenState(this);
        }

        private void MoveToHalfOpenState()
        {
            if (!(_state is HalfOpenState))
                _state = new HalfOpenState(this);
        }

        private void ResetFailureList()
        {
            this.failures.Clear();
        }

        private bool ThresholdReached()
        {
            // Remove log of any failed invocations that occurred earlier than period in which we are interested.  
            this.failures.RemoveAll(f => f < DateTime.UtcNow - ExceptionDuration);

            // Have number of failures breached the allowed threshold?  
            return failures.Count > MaxExceptionCount;
        }

        private abstract class CircuitBreakerState
        {
            protected readonly CircuitBreaker circuitBreaker;

            protected CircuitBreakerState(CircuitBreaker circuitBreaker)
            {
                this.circuitBreaker = circuitBreaker;
            }

            public virtual void ProtectedCodeIsAboutToBeCalled() { }
            public virtual void ProtectedCodeHasBeenCalled() { }
            public virtual void ActUponException(Exception e) { }
        }

        /// <summary>  
        /// Represents a closed CircuitBreaker - this is the "normal" behaviour,  
        ///  with invocations passed through to the protected code.  
        /// </summary>  
        private class ClosedState : CircuitBreakerState
        {
            public ClosedState(CircuitBreaker circuitBreaker)
                : base(circuitBreaker)
            {
                circuitBreaker.ResetFailureList();
            }

            /// <summary>  
            /// Called when an exception occurs in the protected code.  
            /// </summary>  
            /// <param name="e"></param>  
            public override void ActUponException(Exception e)
            {
                // If the threshold has been breached, open the circuit.  
                if (circuitBreaker.ThresholdReached())
                {
                    using (TimedLock.Lock(circuitBreaker.monitor))
                    {
                        circuitBreaker.MoveToOpenState();
                    }
                }
            }
        }

        /// <summary>  
        /// Represents an open CircuitBreaker - in this state we do not pass the   
        ///  invocation to the protected code, but instead throw OpenCircuitExceptions  
        ///  until the timeout expires.  
        /// </summary>  
        private class OpenState : CircuitBreakerState
        {
            private readonly DateTime expiry;

            public OpenState(CircuitBreaker circuitBreaker)
                : base(circuitBreaker)
            {
                // Keep a note of the time at which the circuit should be moved  
                // to half-open state.  
                expiry = DateTime.UtcNow + circuitBreaker.OpenStateDuration;
                //UserNotificationsServiceBL.Instance.ResetConnection();
            }

            public override void ProtectedCodeIsAboutToBeCalled()
            {
                // Has the timeout expired?  
                if (DateTime.UtcNow < expiry)
                {
                    // Not yet, so throw an eppy.  
                    throw new OpenCircuitException();
                }

                using (TimedLock.Lock(circuitBreaker.monitor))
                {
                    // yes, timeout has expired, so move to half-open state.  
                    circuitBreaker.MoveToHalfOpenState();
                }
            }
        }

        /// <summary>  
        /// Represents a CircuitBreaker in the "Half-Open" state.  
        /// In this state a timeout has just expired and we are tentatively calling  
        ///  the protected code again. If all goes well, we will move to the closed state.  
        /// However, if this first dipping of our electronic toe into the water is met by  
        ///  the shock of an unhandled exception, we shall quickly open the circuit again  
        ///  for a further timeout period.  
        /// </summary>  
        private class HalfOpenState : CircuitBreakerState
        {
            public HalfOpenState(CircuitBreaker circuitBreaker) : base(circuitBreaker) { }

            public override void ActUponException(Exception e)
            {
                using (TimedLock.Lock(circuitBreaker.monitor))
                {
                    // Eek, problems remain. Open the circuit again quickly.  
                    circuitBreaker.MoveToOpenState();
                }
            }

            public override void ProtectedCodeHasBeenCalled()
            {
                using (TimedLock.Lock(circuitBreaker.monitor))
                {
                    // Things seem to be OK now. Close the circuit.  
                    circuitBreaker.MoveToClosedState();
                }
            }
        }
    }

    public struct TimedLock : IDisposable
    {
        private readonly object target;

        private TimedLock(object o)
        {
            target = o;
        }

        public void Dispose()
        {
            Monitor.Exit(target);
        }

        public static TimedLock Lock(object o)
        {
            return Lock(o, TimeSpan.FromSeconds(5));
        }

        public static TimedLock Lock(object o, TimeSpan timeout)
        {
            return Lock(o, timeout.Milliseconds);
        }

        public static TimedLock Lock(object o, int milliSeconds)
        {
            var timedLock = new TimedLock(o);
            if (!Monitor.TryEnter(o, milliSeconds))
            {
                throw new LockTimeoutException();
            }
            return timedLock;
        }
    }

    public class LockTimeoutException : ApplicationException
    {
        public LockTimeoutException() : base("Timeout waiting for lock") { }
    }

    public class OpenCircuitException : ApplicationException
    {
        public OpenCircuitException() : base("Circuit is still open") { }
    }
}




