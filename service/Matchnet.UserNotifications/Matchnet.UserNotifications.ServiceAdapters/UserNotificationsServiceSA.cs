﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Caching;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.UserNotifications.ValueObjects.ServiceDefinitions;

namespace Matchnet.UserNotifications.ServiceAdapters
{
    public class UserNotificationsServiceSA : SABase
    {
        private Cache _cache;
        
        // ye ole' singleton instance
        public static readonly UserNotificationsServiceSA Instance = new UserNotificationsServiceSA();
        public delegate List<UserNotificationViewObject> Apply(Hashtable theParams);
        private CircuitBreaker breaker;

        private UserNotificationsServiceSA()
        {
            breaker = new CircuitBreaker();
            _cache = Cache.Instance;
        }

        private void WriteEventLog(string message)
        {
            EventLog.WriteEntry("WWW", message, EventLogEntryType.Error);
        }

        public void AddUserNotification(UserNotificationParams unParams, UserNotificationViewObject notification)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    Apply add = delegate(Hashtable theParams)
                    {
                        UserNotificationParams unp = (null != theParams && theParams.ContainsKey("unp")) ? (UserNotificationParams)theParams["unp"] : null;
                        UserNotificationViewObject unvo = (null != theParams && theParams.ContainsKey("unvo")) ? (UserNotificationViewObject)theParams["unvo"] : null;
                        getService(uri).AddUserNotification(unp, unvo);
                        return null;
                    };

                    Hashtable parms = new Hashtable();
                    parms["unp"] = unParams;
                    parms["unvo"] = notification;
                    breaker.Intercept(add, parms);
                }
                catch (OpenCircuitException oce)
                {
                    System.Diagnostics.EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, oce.Message, EventLogEntryType.Warning);
                    throw oce;  //throw exception so this attempt is requeued
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("AddUserNotification() error (uri: {0}, exception:{1}, params:{2}).", uri, ex, notification.ToString(), unParams.ToString())));
            }
        }

        public void RemoveUserNotifications(UserNotificationParams unParams, UserNotificationViewObject notification)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    Apply remove = delegate(Hashtable theParams)
                    {
                        UserNotificationParams unp = (null != theParams && theParams.ContainsKey("unp")) ? (UserNotificationParams)theParams["unp"] : null;
                        UserNotificationViewObject unvo = (null != theParams && theParams.ContainsKey("unvo")) ? (UserNotificationViewObject)theParams["unvo"] : null;
                        getService(uri).RemoveUserNotification(unp, unvo);
                        return null;
                    };
                    Hashtable parms = new Hashtable();
                    parms["unp"] = unParams;
                    parms["unvo"] = notification;
                    breaker.Intercept(remove, parms);
                }
                catch (OpenCircuitException oce)
                {
                    System.Diagnostics.EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, oce.Message, EventLogEntryType.Warning);
                    throw oce;  //throw exception so this attempt is requeued
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("RemoveUserNotification() error (uri: {0}, exception:{1}, params:{2}).", uri, ex, unParams.ToString())));
            }
        }

        [Obsolete("Use GetUserNotificationsWithDataPoints() instead.")]
        public List<UserNotificationViewObject> GetUserNotifications(UserNotificationParams unParams, int startNum, int pageSize)
        {
            List<UserNotificationViewObject> notifications = null;
            string uri = string.Empty;
            try
            {
                //check cache
                UserNotificationList userNotifications = _cache.Get(UserNotificationList.GetCacheKeyString(unParams.SiteId,unParams.CommunityId,unParams.MemberId)) as UserNotificationList;

                //update cache if: notifications is null or if we need to set notifications as viewed
                bool hasNewNotifications = false;
                if (null != userNotifications)
                {
                    foreach (UserNotificationViewObject unvo in userNotifications.UserNotifications)
                    {
                        if (!unvo.Viewed)
                        {
                            hasNewNotifications = true;
                            break;
                        }
                    }
                }
                if (null == userNotifications || (unParams.SetAsViewed && hasNewNotifications))
                {
                    uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                    base.Checkout(uri);
                    try
                    {
                            Apply get = delegate(Hashtable theParams)
                            {
                                UserNotificationParams unp = (null != theParams && theParams.ContainsKey("unp")) ? (UserNotificationParams)theParams["unp"] : null;
                                int start = (null != theParams && theParams.ContainsKey("sn")) ? (int)theParams["sn"] : -1;
                                int pSize = (null != theParams && theParams.ContainsKey("ps")) ? (int)theParams["ps"] : -1;
                                return getService(uri).GetUserNotifications(unParams, start, pSize);
                            };
                            Hashtable parms = new Hashtable();
                            parms["unp"] = unParams;
                            parms["sn"] = -1;
                            parms["ps"] = -1;
                            notifications = breaker.Intercept(get, parms);

                            if (null != notifications)
                            {
                                if (null != userNotifications)
                                {
                                    _cache.Remove(UserNotificationList.GetCacheKeyString(unParams.SiteId, unParams.CommunityId, unParams.MemberId));
                                }
                                userNotifications = new UserNotificationList();
                                userNotifications.MemberID = unParams.MemberId;
                                userNotifications.SiteID = unParams.SiteId;
                                userNotifications.CommunityID = unParams.CommunityId;
                                userNotifications.UserNotifications = notifications;
                                userNotifications.CacheTTLSeconds = GetCacheTTLSeconds();
                                _cache.Add(userNotifications);
                            }
                    }
                    catch (OpenCircuitException oce)
                    {
                        System.Diagnostics.EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, oce.Message, EventLogEntryType.Warning);
                        throw oce;  //throw exception so this attempt is requeued
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }
                else
                {
                    notifications = userNotifications.UserNotifications;
                }

                //return number of notifications requested
                if (startNum >= 0 && pageSize > 0 && notifications != null && notifications.Count > 0 && startNum + pageSize < notifications.Count)
                {
                    notifications = notifications.GetRange(startNum, pageSize);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("GetUserNotifications() error (uri: {0}, exception:{1}, params:{2})", uri, ex, unParams.ToString())));
            }
            return notifications;
        }

        public List<UserNotificationViewObject> GetUserNotificationsWithDataPoints(UserNotificationParams unParams, int startNum, int pageSize)
        {
            return GetUserNotificationsWithDataPoints(unParams, startNum, pageSize, false);
        }

        public List<UserNotificationViewObject> GetUserNotificationsWithDataPoints(UserNotificationParams unParams, int startNum, int pageSize, bool refreshDataPointCache)
        {
            List<UserNotificationViewObject> notifications = null;
            string uri = string.Empty;
            try
            {
                //check cache
                UserNotificationList userNotificationsWDP = _cache.Get(UserNotificationList.GetCacheKeyWDPString(unParams.SiteId, unParams.CommunityId, unParams.MemberId)) as UserNotificationList;

                if (refreshDataPointCache)
                {
                    //compare data point cache to regular notification cache to determine if we need to refresh
                    UserNotificationList userNotifications = _cache.Get(UserNotificationList.GetCacheKeyString(unParams.SiteId, unParams.CommunityId, unParams.MemberId)) as UserNotificationList;
                    if (userNotifications != null && userNotificationsWDP != null)
                    {
                        if ((userNotifications.UserNotifications.Count == userNotificationsWDP.UserNotifications.Count)
                            && (userNotifications.UserNotifications[0].Timestamp == userNotificationsWDP.UserNotifications[0].Timestamp))
                        {
                            //this indicates no new notifications were added, so WDP does not need to be refreshed
                            refreshDataPointCache = false;
                        }
                    }
                }

                //update cache if: notifications is null or if we need to set notifications as viewed
                bool hasNewNotifications = false;
                if (userNotificationsWDP != null && userNotificationsWDP.UserNotifications.Count > 0)
                {
                    foreach (UserNotificationViewObject unvo in userNotificationsWDP.UserNotifications)
                    {
                        if (!unvo.Viewed)
                        {
                            hasNewNotifications = true;
                            break;
                        }
                    }
                }
                if (userNotificationsWDP == null || userNotificationsWDP.UserNotifications.Count < 1 || (unParams.SetAsViewed && hasNewNotifications) || refreshDataPointCache)
                {
                    uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                    base.Checkout(uri);
                    try
                    {
                        Apply get = delegate(Hashtable theParams)
                        {
                            UserNotificationParams unp = (null != theParams && theParams.ContainsKey("unp")) ? (UserNotificationParams)theParams["unp"] : null;
                            int start = (null != theParams && theParams.ContainsKey("sn")) ? (int)theParams["sn"] : -1;
                            int pSize = (null != theParams && theParams.ContainsKey("ps")) ? (int)theParams["ps"] : -1;
                            return getService(uri).GetUserNotificationsWithDataPoints(unParams, start, pSize);
                        };
                        Hashtable parms = new Hashtable();
                        parms["unp"] = unParams;
                        parms["sn"] = -1;
                        parms["ps"] = -1;
                        notifications = breaker.Intercept(get, parms);

                        if (null != notifications)
                        {
                            if (null != userNotificationsWDP)
                            {
                                _cache.Remove(UserNotificationList.GetCacheKeyWDPString(unParams.SiteId, unParams.CommunityId, unParams.MemberId));
                            }
                            userNotificationsWDP = new UserNotificationList();
                            userNotificationsWDP.MemberID = unParams.MemberId;
                            userNotificationsWDP.SiteID = unParams.SiteId;
                            userNotificationsWDP.CommunityID = unParams.CommunityId;
                            userNotificationsWDP.UserNotifications = notifications;
                            userNotificationsWDP.CacheTTLSeconds = GetCacheTTLSeconds();
                            userNotificationsWDP.UseWDPCache = true;
                            _cache.Add(userNotificationsWDP);
                        }
                    }
                    catch (OpenCircuitException oce)
                    {
                        System.Diagnostics.EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, oce.Message, EventLogEntryType.Warning);
                        throw oce;  //throw exception so this attempt is requeued
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }
                else
                {
                    notifications = userNotificationsWDP.UserNotifications;
                }

                if (notifications != null && ((startNum + 1) > notifications.Count))
                {
                    notifications = null;
                }

                //return number of notifications requested
                if (startNum >= 0 && pageSize > 0 && notifications != null && notifications.Count > 0)
                {
                    if (startNum + pageSize > notifications.Count)
                    {
                        notifications = notifications.GetRange(startNum, notifications.Count - startNum);
                    }
                    else
                    {
                        notifications = notifications.GetRange(startNum, pageSize);    
                    }
                    
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("GetUserNotifications() error (uri: {0}, exception:{1}, params:{2})", uri, ex, unParams.ToString())));
            }
            return notifications;
        }

        public void RemoveUserNotifications(UserNotificationParams unParams, List<UserNotificationViewObject> notifications)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    Apply remove = delegate(Hashtable theParams)
                    {
                        UserNotificationParams unp = (null != theParams && theParams.ContainsKey("unp")) ? (UserNotificationParams)theParams["unp"] : null;
                        List<UserNotificationViewObject> unvos = (null != theParams && theParams.ContainsKey("unvos")) ? (List<UserNotificationViewObject>)theParams["unvos"] : null;
                        getService(uri).RemoveUserNotifications(unp, unvos);
                        return null;
                    };
                    Hashtable parms = new Hashtable();
                    parms["unp"] = unParams;
                    parms["unvos"] = notifications;
                    breaker.Intercept(remove, parms);
                }
                catch (OpenCircuitException oce)
                {
                    System.Diagnostics.EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, oce.Message, EventLogEntryType.Warning);
                    throw oce;  //throw exception so this attempt is requeued
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("RemoveUserNotifications() error (uri: {0}, exception:{1}, params:{2}).", uri, ex, unParams.ToString())));
            }
        }

        public int GetTotalNotificationsNumber(UserNotificationParams unParams)
        {
            int count = 0;
            unParams.SetAsViewed = false;
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                List<UserNotificationViewObject> notifications = GetUserNotifications(unParams, -1, -1);
                if (null != notifications)
                {
                    count = notifications.Count;
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("GetTotalNotificationsNumber() error (uri: {0}, exception:{1}, params:{2}).", uri, ex, unParams.ToString())));
            }
            return count;
        }

        public int GetCountOfNewUserNotifications(UserNotificationParams unParams)
        {
            int count = 0;
            unParams.SetAsViewed = false;
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                List<UserNotificationViewObject> notifications = GetUserNotifications(unParams, -1, -1);
                if (null != notifications)
                {
                    foreach (UserNotificationViewObject viewObject in notifications)
                {
                        if (!viewObject.Viewed) count++;
                }
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(string.Format("GetCountOfNewUserNotifications() error (uri: {0}, exception:{1}, params:{2}).", uri, ex, unParams.ToString())));
            }
            return count;
        }

        private IUserNotificationService getService(string uri)
        {
            try
            {
                return (IUserNotificationService)Activator.GetObject(typeof(IUserNotificationService), uri);
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot activate remote service manager at " + uri, ex);
            }
        }

        private string GetServiceManagerUri(string serviceManagerName)
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(serviceManagerName);
                string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USERNOTIFICATIONSSVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    uri = "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot get configuration settings for remote service manager.", ex);
            }
        }

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USERNOTIFICATIONSSVC_SA_CONNECTION_LIMIT"));
        }

        private int GetCacheTTLSeconds()
        {
            int ttlSeconds=300; //default to 5 minutes
            try
            {
                string str = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USERNOTIFICATIONSSVC_CACHE_TTL_SECONDS");
                if (!string.IsNullOrEmpty(str))
                {
                    ttlSeconds = Conversion.CInt(str);
                }
            }
            catch (Exception ex)
            {
                //return default seconds
            }
            return ttlSeconds;
        }
    }
}
