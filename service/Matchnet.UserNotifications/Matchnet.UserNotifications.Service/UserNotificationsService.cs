﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using Matchnet.RemotingServices;
using Matchnet.UserNotifications.ServiceManagers;

namespace Matchnet.UserNotifications.Service
{
    public partial class UserNotificationsService : RemotingServiceBase
    {
        private UserNotificationsServiceSM m_userNotificationsServiceSM;

        public UserNotificationsService()
        {
            InitializeComponent();
        }

        public static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new UserNotificationsService() 
			};
            ServiceBase.Run(ServicesToRun);
        }

        protected override void RegisterServiceManagers()
        {
            System.Diagnostics.Trace.WriteLine("UserNotificationsService, Start RegisterServiceManagers");

            m_userNotificationsServiceSM = new UserNotificationsServiceSM();
            base.RegisterServiceManager(m_userNotificationsServiceSM);

            base.RegisterServiceManagers();
        }
    }
}
