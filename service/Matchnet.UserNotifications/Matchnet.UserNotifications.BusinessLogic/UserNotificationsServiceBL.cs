﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Couchbase;
using Couchbase.Configuration;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.UserNotifications.ValueObjects.ServiceDefinitions;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;


namespace Matchnet.UserNotifications.BusinessLogic
{
    public class UserNotificationsServiceBL : IUserNotificationService, IDisposable
    {

#if DEBUG
        private bool _use_Nunit = false;

        public bool USE_NUNIT
        {
            get { return _use_Nunit; }
            set { _use_Nunit = value; }
        }

        public bool USE_MEMBASE { get; set; }

        public void ResetMembaseConfigs()
        {
            _couchbaseClients=new Dictionary<string, CouchbaseClient>();
            InitMembaseClients();
        }

        public Dictionary<string, CouchbaseClient> CacheConnections
        {
            get { return _couchbaseClients; }
        }
#endif

        /// <summary>
        /// Singleton instance
        /// </summary>
        public readonly static UserNotificationsServiceBL Instance = new UserNotificationsServiceBL();
        private const string CACHENAME = "notifications";
        private int _manageConnectionsThreadSleepInMillis = 3600*1000;

        private ReaderWriterLock _externalLock = null;
        public delegate void UserNotificationsGetRequestEventHandler();
        public event UserNotificationsGetRequestEventHandler UserNotificationsGetRequest;

        public delegate void UserNotificationsPutRequestEventHandler();
        public event UserNotificationsPutRequestEventHandler UserNotificationsPutRequest;

        private string MEMBASE_CONFIG_URL_TMPL = "http://{0}:{1}/pools/default";
        private Dictionary<string, CouchbaseClient> _couchbaseClients = new Dictionary<string, CouchbaseClient>();
        private Dictionary<string, int> _objectTTLInSeconds;
        private ReaderWriterLock _membaseLock = null;
        private bool _isRunning = false;
        private Thread manageCacheConnections;
        private static ISettingsSA _settingsService = null;

        public static ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService) _settingsService = RuntimeSettings.Instance;
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        public int ManageConnectionsThreadSleepInMillis
        {
            get { return _manageConnectionsThreadSleepInMillis; }
            set { _manageConnectionsThreadSleepInMillis = value; }
        }

        private UserNotificationsServiceBL() {
            System.Diagnostics.Trace.Write("UserNotificationsServiceBL constructor.");
            _externalLock = new ReaderWriterLock();
            _membaseLock = new ReaderWriterLock();
            _objectTTLInSeconds = new Dictionary<string, int>();
            _isRunning = true;
            InitMembaseClients();

#if DEBUG
            if (!USE_NUNIT)
            {
#endif
                manageCacheConnections = new Thread(ManageCacheConnections);
                manageCacheConnections.Start();
#if DEBUG
            }
#endif
        }

        private void InitMembaseClients()
        {
            try
            {
                List<MembaseConfig> configs = SettingsService.GetMembaseConfigsFromSingleton();
                foreach (MembaseConfig config in configs)
                {
                    CreateCacheConnection(config);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Could not get setup membase cache!", ex);
            }
        }

        //This thread will add new connections if any db changes are made
        public void ManageCacheConnections()
        {
            while (_isRunning)
            {
                Thread.Sleep(ManageConnectionsThreadSleepInMillis);  // only check once per hour
                List<MembaseConfig> membaseConfigs = SettingsService.GetMembaseConfigsFromSingleton();
                foreach (MembaseConfig config in membaseConfigs)
                {
                    try
                    {
                        if (!_couchbaseClients.ContainsKey(config.BucketName))
                        {
                            CreateCacheConnection(config);
                        }
                    }
                    catch (Exception e)
                    {
                        EventLog.WriteEntry(ValueObjects.ServiceConstants.SERVICE_NAME, "Could not create cache connection for bucket:" + config.BucketName +", Exception: " + e.Message, EventLogEntryType.Error);
                    }
                }
#if DEBUG
                if (USE_NUNIT)
                {
                    _isRunning = false;
                }
#endif
            }
        }

        private void CreateCacheConnection(MembaseConfig config)
        {
            var mc = new CouchbaseClientConfiguration()
            {
                Bucket = config.BucketName,
                BucketPassword = (string.IsNullOrEmpty(config.BucketPassword)) ? "" : config.BucketPassword
            };
            if (config.RetryCount > int.MinValue) mc.RetryCount = config.RetryCount;
            if (config.RetryTimeout > int.MinValue)
            {
                mc.RetryTimeout = new TimeSpan(0, 0, config.RetryCount);
            }
            if (config.SocketPoolMaxPoolSize > int.MinValue) mc.SocketPool.MaxPoolSize = config.SocketPoolMaxPoolSize;
            if (config.SocketPoolMinPoolSize > int.MinValue) mc.SocketPool.MinPoolSize = config.SocketPoolMinPoolSize;
            if (config.SocketPoolConnectionTimeout > int.MinValue)
            {
                mc.SocketPool.ConnectionTimeout = new TimeSpan(0, 0, config.SocketPoolConnectionTimeout);
            }
            if (config.SocketPoolDeadTimeout > int.MinValue)
            {
                mc.SocketPool.DeadTimeout = new TimeSpan(0, 0, config.SocketPoolDeadTimeout);
            }
            if (config.SocketPoolQueueTimeout > int.MinValue)
            {
                mc.SocketPool.QueueTimeout = new TimeSpan(0, 0, config.SocketPoolQueueTimeout);
            }
            if (config.SocketPoolReceiveTimeout > int.MinValue)
            {
                mc.SocketPool.ReceiveTimeout = new TimeSpan(0, 0, config.SocketPoolReceiveTimeout);
            }

            foreach (MembaseConfigServer server in config.Servers)
            {
                mc.Urls.Add(new Uri(string.Format(MEMBASE_CONFIG_URL_TMPL, server.ServerIP, server.ServerPort)));
            }

            _membaseLock.AcquireWriterLock(-1);
            try
            {
                if (_couchbaseClients.ContainsKey(mc.Bucket))
                {
                    _couchbaseClients.Remove(mc.Bucket);
                }
                _couchbaseClients.Add(mc.Bucket, new CouchbaseClient(mc));

                if (_objectTTLInSeconds.ContainsKey(mc.Bucket))
                {
                    _objectTTLInSeconds.Remove(mc.Bucket);
                }

                if (config.ObjectTimeToLiveInSeconds > 0)
                {
                    _objectTTLInSeconds.Add(mc.Bucket, config.ObjectTimeToLiveInSeconds);
                }
            }
            finally
            {
                _membaseLock.ReleaseLock();
            }
        }

        private CouchbaseClient GetCouchbaseClient(string cacheName)
        {
            CouchbaseClient couchbaseClient = null;
            _membaseLock.AcquireReaderLock(-1);
            try
            {
                if (_couchbaseClients.ContainsKey(cacheName))
                {
                    couchbaseClient = _couchbaseClients[cacheName];
                }
            }
            finally
            {
                _membaseLock.ReleaseLock();
            }
            return couchbaseClient;
        }

        #region Private Methods
        private int GetIntSetting(string settingConstant, int communityId, int siteId, int defaultVal)
        {
            int value = defaultVal;
            try
            {
                string valueStr = SettingsService.GetSettingFromSingleton(settingConstant, communityId, siteId);
                if (!string.IsNullOrEmpty(valueStr))
                {
                    value = Convert.ToInt32(valueStr);
                }
            }
            catch (Exception e)
            {
                value = defaultVal;
                ServiceTrace.Instance.LogServiceException(UserNotifications.ValueObjects.ServiceConstants.SERVICE_NAME, "GetIntSetting(\"" + settingConstant + "\"", e, false);
            }
            return value;
        }

        private bool GetBoolSetting(string settingConstant, int communityId, int siteId, bool defaultVal)
        {
            bool value = defaultVal;
            try
            {
                string valueStr = SettingsService.GetSettingFromSingleton(settingConstant, communityId, siteId);
                if (!string.IsNullOrEmpty(valueStr))
                {
                    value = Convert.ToBoolean(valueStr);
                }
            }
            catch (Exception e)
            {
                value = defaultVal;
                ServiceTrace.Instance.LogServiceException(UserNotifications.ValueObjects.ServiceConstants.SERVICE_NAME, "GetBoolSetting(\"" + settingConstant + "\"", e, false);
            }
            return value;
        }

        public void ResetConnection()
        {
            Thread t=new Thread(new ThreadStart(internalReset));
            t.Start();
        }

        private void internalReset()
        {
            Dictionary<string, CouchbaseClient> oldClients = _couchbaseClients;
            _membaseLock.AcquireWriterLock(-1);
            try
            {
                _couchbaseClients = new Dictionary<string, CouchbaseClient>();
                _objectTTLInSeconds = new Dictionary<string, int>();
            }
            finally
            {
                _membaseLock.ReleaseLock();
            }

            InitMembaseClients();
            foreach (string cacheName in oldClients.Keys)
            {
                oldClients[cacheName].Dispose();
            }
        }

        private List<UserNotificationViewObject> getNotificationsList(UserNotificationParams unParams)
        {
            List<UserNotificationViewObject> list = null;
            try
            {
                list = GetCouchbaseClient(CACHENAME).Get(GetCacheKey(unParams)) as List<UserNotificationViewObject>;
            }
            catch (Exception ex)
            {
                UserNotifications.ValueObjects.ServiceTrace.Instance.LogServiceException("UserNotificationsBL", "getNotificationsList()", ex, false);
            }
            return list;
        }
        
        private bool HasListInCache(UserNotificationParams unParams)
        {
            bool b = false;
            if (null != unParams)
            {
                List<UserNotificationViewObject> list = getNotificationsList(unParams);
                if (null != list)
                {
                    return true;
                }
                UserNotificationsGetRequest();
            }
            return b;
        }

        private List<UserNotificationViewObject> GetListFromCache(UserNotificationParams unParams)
        {
            List<UserNotificationViewObject> notifications = null;
            if (null != unParams)
            {
                notifications = getNotificationsList(unParams);
                UserNotificationsGetRequest();
            }
            return notifications;
        }

        private void SetListInCache(UserNotificationParams unParams, List<UserNotificationViewObject> list)
        {
            if (null != unParams)
            {
                try
                {
                    CouchbaseClient couchbaseClient = GetCouchbaseClient(CACHENAME);
                    if (_objectTTLInSeconds.ContainsKey(CACHENAME))
                    {
                        bool b = couchbaseClient.Store(Enyim.Caching.Memcached.StoreMode.Set, GetCacheKey(unParams), list, DateTime.Now.AddSeconds(_objectTTLInSeconds[CACHENAME]));
                    }
                    else
                    {
                        bool b = couchbaseClient.Store(Enyim.Caching.Memcached.StoreMode.Set, GetCacheKey(unParams), list);
                    }
                }
                catch (Exception ex)
                {
                    UserNotifications.ValueObjects.ServiceTrace.Instance.LogServiceException("UserNotificationsBL", "SetListInCache()", ex, false);
                }
                UserNotificationsPutRequest();
            }
        }

        private static string GetCacheKey(UserNotificationParams unParams)
        {
            return unParams.MemberId + "-" + unParams.SiteId;
        }
        #endregion

        #region IUserNotificationService Members
        public void AddUserNotification(UserNotificationParams unParams, UserNotificationViewObject notification)
        {
            int MAX_LIMIT = GetIntSetting("USERNOTIFICATIONSSVC_MAX_LIMIT", unParams.CommunityId, unParams.SiteId, 100);
            int MAX_DAYS = GetIntSetting("USERNOTIFICATIONSSVC_MAX_DAYS", unParams.CommunityId, unParams.SiteId, 30);

            try
            {
                List<UserNotificationViewObject> notifications = GetListFromCache(unParams);
                if (null == notifications)
                {
                    notifications = new List<UserNotificationViewObject>();
                }

                if (notifications.Count > 0)
                {
                    //if the notification already exists in the list then dedupe
                    UserNotificationViewObject match = notifications.FindLast(delegate(UserNotificationViewObject un)
                    {
                        bool b = un.Equals(notification);
                        return b;
                    });

                    if (null != match)
                    {
                        notifications.Remove(notification);
                    }

                    //make sure user notifications limit is not reached
                    if (notifications.Count > MAX_LIMIT && MAX_LIMIT > 0)
                    {
                        int difference = notifications.Count - MAX_LIMIT;
                        int idx = notifications.Count - difference;
                        //sort
                        notifications.Sort();
                        notifications.RemoveRange(idx, difference);
                    }

                    //make sure user notifications are not older than max days
                    List<UserNotificationViewObject> matches = notifications.FindAll(delegate(UserNotificationViewObject un)
                    {
                        TimeSpan ts = DateTime.Now - un.Created;
                        bool b = ts.Duration().Days > MAX_DAYS;
                        return b;
                    });

                    if (null != matches && matches.Count > 0)
                    {
                        foreach (UserNotificationViewObject m in matches)
                        {
                            notifications.Remove(m);
                        }
                    }
                }
                //finally add notification
                notifications.Add(notification);
                SetListInCache(unParams, notifications);
            }
            catch (Exception ex)
            {
                string message = string.Format("AddUserNotification() error for unParams={0}", unParams.ToString());
                throw new Matchnet.Exceptions.BLException(message, ex);
            }
        }

        public void RemoveUserNotification(UserNotificationParams unParams, UserNotificationViewObject notification)
        {
            try
            {
                if (HasListInCache(unParams))
                {
                    List<UserNotificationViewObject> userNotifications = GetListFromCache(unParams);
                    userNotifications.Remove(notification);
                    SetListInCache(unParams, userNotifications);
                }
            }
            catch (Exception ex)
            {
                string message = string.Format("RemoveUserNotification() error for unParams={0}", unParams.ToString());
                throw new Matchnet.Exceptions.BLException(message, ex);
            }
        }

        public List<UserNotificationViewObject> GetUserNotificationsWithDataPoints(UserNotificationParams unParams, int startNum, int pageSize)
        {
            List<UserNotificationViewObject> notifications = null;
            try
            {
                notifications = GetListFromCache(unParams);
                if (null != notifications)
                {
                    if (unParams.SetAsViewed)
                    {
                        bool hasNew = false;
                        foreach (UserNotificationViewObject viewObject in notifications)
                        {
                            if (!viewObject.Viewed)
                            {
                                viewObject.Viewed = true;
                                hasNew = true;
                            }
                        }
                        if (hasNew)
                        {
                            //sort newest to oldest
                            notifications.Sort();
                            SetListInCache(unParams, notifications);
                        }
                    }

                    notifications = notifications.Where(n => n.NotificationType != NotificationType.NotTyped).ToList();
                 
                    if (startNum > 0 && pageSize > 0 && notifications.Count > 0 && startNum + pageSize < notifications.Count)
                    {
                        notifications = notifications.GetRange(startNum, pageSize);
                    }
                }
            }
            catch (Exception ex)
            {
                string message = string.Format("GetUserNotifications() error for unParams={0}, startNum={1}, and pageSize={2}", unParams.ToString(), startNum, pageSize);
                throw new Matchnet.Exceptions.BLException(message, ex);
            }

            return notifications;
        }



        public List<UserNotificationViewObject> GetUserNotifications(UserNotificationParams unParams, int startNum, int pageSize)
        {
            List<UserNotificationViewObject> notifications = null;
            try
            {
                notifications = GetListFromCache(unParams);
                if (null != notifications)
                {
                    if (unParams.SetAsViewed)
                    {
                        bool hasNew = false;
                        foreach (UserNotificationViewObject viewObject in notifications)
                        {
                            if (!viewObject.Viewed)
                            {
                                viewObject.Viewed = true;
                                hasNew = true;
                            }
                        }
                        if (hasNew)
                        {
                            //sort newest to oldest
                            notifications.Sort();
                            SetListInCache(unParams, notifications);
                        }
                    }
                    if (startNum > 0 && pageSize > 0 && notifications.Count > 0 && startNum + pageSize < notifications.Count)
                    {
                        notifications = notifications.GetRange(startNum, pageSize);
                    }
                }
            }
            catch (Exception ex)
            {   
                string message = string.Format("GetUserNotifications() error for unParams={0}, startNum={1}, and pageSize={2}", unParams.ToString(), startNum, pageSize);
                throw new Matchnet.Exceptions.BLException(message,ex);
            }

            return notifications;
        }

        public void RemoveUserNotifications(UserNotificationParams unParams, List<UserNotificationViewObject> notifications)
        {
            try
            {
                List<UserNotificationViewObject> userNotifications = GetListFromCache(unParams);
                if (null != notifications)
                {
                    foreach (UserNotificationViewObject notification in notifications)
                    {
                        userNotifications.Remove(notification);
                    }
                }
                SetListInCache(unParams, userNotifications);
            }
            catch (Exception ex)
            {
                string message = string.Format("RemoveUserNotifications() error for unParams={0}", unParams.ToString());
                throw new Matchnet.Exceptions.BLException(message, ex);
            }
        }
        #endregion



        #region IDisposable Members

        public void Dispose()
        {
            _isRunning = false;
            if (null != manageCacheConnections)
            {
                manageCacheConnections.Abort();
                manageCacheConnections = null;
            }

            if (null != _couchbaseClients)
            {
                foreach (CouchbaseClient mc in _couchbaseClients.Values)
                {
                    mc.Dispose();
                }
            }
        }

        #endregion
    }

}
