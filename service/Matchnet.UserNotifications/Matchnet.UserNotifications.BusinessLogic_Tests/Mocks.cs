﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;

namespace Matchnet.UserNotifications.BusinessLogic_Tests.Mocks
{
    public class Mocks
    {}

    public class MockSettingsService : ISettingsSA
    {
        private Dictionary<int, Dictionary<string, string>> communitySettings = new Dictionary<int, Dictionary<string, string>>();
        private List<MembaseConfig> membaseConfigs = new List<Configuration.ValueObjects.MembaseConfig>();

        #region ISettingsSA Members

        public void AddSetting(string key, string value)
        {
            AddSetting(key, value, 0);
        }

        public void AddSetting(string key, string value, int communityId)
        {
            if (!communitySettings.ContainsKey(communityId)) communitySettings.Add(communityId, new Dictionary<string, string>());

            if (communitySettings[communityId].ContainsKey(key))
                communitySettings[communityId][key] = value;
            else
                communitySettings[communityId].Add(key, value);
        }

        public void RemoveSetting(string key)
        {
            RemoveSetting(key, 0);
        }

        public void RemoveSetting(string key, int communityId)
        {
            if (SettingExistsFromSingleton(key, communityId, 0))
            {
                communitySettings[communityId].Remove(key);
            }
            else if (SettingExistsFromSingleton(key, 0, 0))
            {
                communitySettings[0].Remove(key);
            }
        }

        public List<MembaseConfig> AddMembaseConfig(MembaseConfig mc)
        {
            membaseConfigs.Add(mc);
            return membaseConfigs;
        }

        public List<MembaseConfig> GetMembaseConfigsFromSingleton()
        {
            return membaseConfigs;
        }

        public Matchnet.Configuration.ValueObjects.ServiceInstanceConfig GetServiceInstanceConfigFromSingleton(string serviceConstant, string machineName)
        {
            throw new System.NotImplementedException();
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID, int brandID)
        {
            if (SettingExistsFromSingleton(constant, communityID, 0))
            {
                return communitySettings[communityID][constant];
            }
            else
            {
                return GetSettingFromSingleton(constant);
            }
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID)
        {
            if (SettingExistsFromSingleton(constant, communityID, 0))
            {
                return communitySettings[communityID][constant];
            }
            else
            {
                return GetSettingFromSingleton(constant);
            }
        }

        public string GetSettingFromSingleton(string constant, int communityID)
        {
            if (SettingExistsFromSingleton(constant, communityID, 0))
            {
                return communitySettings[communityID][constant];
            }
            else
            {
                return GetSettingFromSingleton(constant);
            }
        }

        public string GetSettingFromSingleton(string constant)
        {
            try
            {
                return communitySettings[0][constant];
            }
            catch (Exception e)
            {
                print(string.Format("Could not find setting key:{0}, community:{1}", constant, 0));
                throw e;
            }
        }

        public bool IsServerEnabledFromSingleton()
        {
            throw new System.NotImplementedException();
        }

        public bool SettingExistsFromSingleton(string constant, int communityId, int siteId)
        {
            bool settingExistsFromSingleton = communitySettings.ContainsKey(communityId) && communitySettings[communityId].ContainsKey(constant);
            //if (!settingExistsFromSingleton)
            //{
            //    print(string.Format("Could not find setting key:{0}, community:{1}", constant, communityId));
            //}
            return settingExistsFromSingleton;
        }

        protected void print(string s)
        {
            print(s, null);
        }

        protected void print(string s, string test)
        {
            if (!string.IsNullOrEmpty(test)) Console.WriteLine(string.Format("---------{0}---------", test));
            Console.WriteLine(s);
        }

        public List<Matchnet.Configuration.ValueObjects.ApiAppSetting> GetAPIAppSettingsFromSingleton()
        {
            throw new NotImplementedException();
        }

        public void AddNewOrUpdateSetting(string settingConstant, string globalDefaultValue, string settingDescription,
            int settingCategoryId = 2, bool isRequiredForCommunity = false, bool isRequiredForSite = false)
        {
            throw new NotImplementedException();
        }

        public void UpdateGlobalDefault(string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void SetValueForSite(int siteId, string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void ResetToGlobalDefault(string settingConstant)
        {
            throw new NotImplementedException();
        }


        public MembaseConfig GetMembaseConfigByBucketFromSingleton(string bucketName)
        {
            MembaseConfig mc = null;
            if (null != membaseConfigs)
            {
                foreach (MembaseConfig membaseConfig in membaseConfigs)
                {
                    if (membaseConfig.BucketName.ToLower() == bucketName.ToLower())
                    {
                        mc = membaseConfig;
                        break;
                    }
                }
            }
            return mc;
        }

        #endregion
    }

}
