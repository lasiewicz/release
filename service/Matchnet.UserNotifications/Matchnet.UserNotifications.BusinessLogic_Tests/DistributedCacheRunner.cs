﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
#if DEBUG
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
#endif
namespace Matchnet.UserNotifications.BusinessLogic_Tests
{
#if DEBUG
    class DistributedCacheRunner : IStressTestable
    {
        private UserNotificationParams _params;
        private int _notificationsAmount;
        private List<UserNotificationViewObject> _notifications = new List<UserNotificationViewObject>();
        private ReaderWriterLock _externalLock = null;

        private bool _runSucceeded = false;

        public bool RunSuceeded
        {
            get { return _runSucceeded; }
        }

        public DistributedCacheRunner(UserNotificationParams unParams, int notificationsAmount)
        {
            _params = unParams;
            _notificationsAmount = notificationsAmount;
            _externalLock = new ReaderWriterLock();
        }

        #region IStressTestable Members

        public void Prepare()
        {
            for (int i = 0; i < _notificationsAmount; i++)
            {
                UserNotificationViewObject viewObject = new UserNotificationViewObject("name" + _params.MemberId,
                    _params.MemberId,
                    _params.MemberId,
                    "thumb" + i + "-" + _params.MemberId,
                    "text" + i + "-" + _params.MemberId,
                    DateTime.Now.ToShortTimeString(),
                    false,
                    DateTime.Now,
                    _params.SiteId,
                    _params.CommunityId,
                    "reskey" + i,
                    "omnitag" + i);
                _notifications.Add(viewObject);
            }
        }

        public void Run()
        {
            Random r = new Random();
            try
            {
                foreach (UserNotificationViewObject viewObject in _notifications)
                {
                    int result = r.Next() % 5;
                    string operation=string.Empty;
                    switch (result)
                    {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                            operation = "AddUserNotification";
                            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.AddUserNotification(_params, viewObject);
                            //UserNotificationsServiceSA.Instance.AddUserNotification(_params, viewObject);
                            break;
                        default:
                            operation="GetUserNotification";
                            //UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.GetUserNotifications(_params, -1, -1);
                            //System.Threading.Thread.Sleep(500);
                            List<UserNotificationViewObject> notifications = UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.GetUserNotifications(_params, -1, -1);
                            //List<UserNotificationViewObject> notifications = UserNotificationsServiceSA.Instance.GetUserNotifications(_params, -1, -1);
                            Console.WriteLine(string.Format("Thread:{0}, Operation:{1}, Objects:{2}", this.GetHashCode(), operation, ((null!=notifications)?notifications.Count.ToString():"null")));
                            break;
                    }
                    
                    Console.WriteLine(string.Format("Thread:{0}, Operation:{1}, Time:{2}",this.GetHashCode(),operation,DateTime.Now.Ticks));
                }
                this._runSucceeded = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                //System.Diagnostics.EventLog.WriteEntry("NUNIT", e.Message, System.Diagnostics.EventLogEntryType.Error);
                throw e;
            }
        }

        #endregion
    }
#endif
}
