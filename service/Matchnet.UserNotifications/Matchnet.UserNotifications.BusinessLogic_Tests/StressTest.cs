﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#if DEBUG
using Matchnet.Configuration.ValueObjects;
using Matchnet.UserNotifications.BusinessLogic_Tests.Mocks;
using Matchnet.UserNotifications.ValueObjects;
using NUnit.Framework;
#endif
namespace Matchnet.UserNotifications.BusinessLogic_Tests
{
#if DEBUG
    [TestFixture]
    public class StressTest
    {
        private void UserNotificationsServiceBL_GetRequest() { }
        private void UserNotificationsServiceBL_PutRequest() { }

        [TestFixtureSetUp]
        public void StartUp()
        {
            MockSettingsService mockSettingsService = new Mocks.MockSettingsService();
            MembaseConfig membaseConfig = new MembaseConfig("notifications", null, int.MinValue, int.MinValue, int.MinValue, int.MinValue, int.MinValue, int.MinValue, int.MinValue, int.MinValue, 1296000);
            membaseConfig.AddServer(new MembaseConfigServer("172.16.200.141", 8091));
            membaseConfig.AddServer(new MembaseConfigServer("192.168.4.121", 8091));
            mockSettingsService.AddMembaseConfig(membaseConfig);
            mockSettingsService.AddSetting("USERNOTIFICATIONSSVC_MAX_DAYS", 15.ToString());
            mockSettingsService.AddSetting("USERNOTIFICATIONSSVC_MAX_LIMIT", 15.ToString());
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.SettingsService = mockSettingsService;

            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.USE_NUNIT = true;
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.UserNotificationsGetRequest += new UserNotifications.BusinessLogic.UserNotificationsServiceBL.UserNotificationsGetRequestEventHandler(UserNotificationsServiceBL_GetRequest);
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.UserNotificationsPutRequest += new UserNotifications.BusinessLogic.UserNotificationsServiceBL.UserNotificationsPutRequestEventHandler(UserNotificationsServiceBL_PutRequest);
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.ResetMembaseConfigs();
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.SettingsService = null;
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.USE_NUNIT = false;
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.ResetMembaseConfigs();
        }

        [Test]
        public void TestStressOnDistributedCache()
        {
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.USE_MEMBASE = true;

            Random r = new Random();
            
            List<IStressTestable> runners = new List<IStressTestable>();

            for (int i = 0; i < 2000; i++)
            {
                runners.Add(new DistributedCacheRunner(UserNotificationParams.GetParamsObject(r.Next(), 103, 1), 100));
            }

            using (StressTester tester = new StressTester(runners))
            {
                tester.RunConcurrently();
            }

            foreach (IStressTestable testable in runners)
            {                
                Assert.IsTrue(((DistributedCacheRunner)testable).RunSuceeded);
            }
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.USE_MEMBASE = false;            
        }
    }
#endif
}
