﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Couchbase;
#if DEBUG
using Matchnet.Configuration.ValueObjects;
using Matchnet.UserNotifications.BusinessLogic_Tests.Mocks;
using NUnit.Framework;
using Matchnet.UserNotifications.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;
#endif

namespace Matchnet.UserNotifications.BusinessLogic_Tests
{
#if DEBUG
    //  <!-- add uri="http://192.168.4.121:8091/pools/default" / -->

    [TestFixture]
    public class UserNotificationsBLTests
    {
        MockSettingsService mockSettingsService;

        private void UserNotificationsServiceBL_GetRequest() { }
        private void UserNotificationsServiceBL_PutRequest(){ }

        [TestFixtureSetUp]
        public void StartUp()
        {
            mockSettingsService = new Mocks.MockSettingsService();
            MembaseConfig membaseConfig = new MembaseConfig("notifications", null, int.MinValue, int.MinValue, int.MinValue, int.MinValue, int.MinValue, int.MinValue, int.MinValue, int.MinValue, 1296000);
            membaseConfig.AddServer(new MembaseConfigServer("172.16.200.141", 8091));
            membaseConfig.AddServer(new MembaseConfigServer("192.168.4.121", 8091));
            mockSettingsService.AddMembaseConfig(membaseConfig);
            mockSettingsService.AddSetting("USERNOTIFICATIONSSVC_MAX_DAYS",15.ToString());
            mockSettingsService.AddSetting("USERNOTIFICATIONSSVC_MAX_LIMIT",15.ToString());
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.SettingsService = mockSettingsService;

            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.USE_NUNIT = true;
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.UserNotificationsGetRequest += new UserNotifications.BusinessLogic.UserNotificationsServiceBL.UserNotificationsGetRequestEventHandler(UserNotificationsServiceBL_GetRequest);
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.UserNotificationsPutRequest += new UserNotifications.BusinessLogic.UserNotificationsServiceBL.UserNotificationsPutRequestEventHandler(UserNotificationsServiceBL_PutRequest);
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.ResetMembaseConfigs();
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.SettingsService = null;
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.USE_NUNIT = false;
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.ResetMembaseConfigs();
        }

        [Test]
        public void TestAddToCache()
        {
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.USE_MEMBASE=true;
            int memberId = new Random().Next();
            UserNotificationParams unParams = UserNotificationParams.GetParamsObject(memberId, 103, 1);
            UserNotificationViewObject viewObject = new UserNotificationViewObject("membername",
                                                                                    memberId,
                                                                                    memberId,
                                                                                    "thumbnail",
                                                                                    "text",
                                                                                    "10/10/2010",
                                                                                    false,
                                                                                    DateTime.Now,
                                                                                    103,
                                                                                    1,
                                                                                    "reskey",
                                                                                    "omnitag");
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.AddUserNotification(unParams, viewObject);
            List<UserNotificationViewObject> notifications = UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.GetUserNotifications(unParams, -1, -1); 
            
            System.Threading.Thread.Sleep(3000);
            Assert.NotNull(notifications);
            Assert.GreaterOrEqual(1, notifications.Count);
            Assert.IsTrue(viewObject.Equals(notifications[0]));
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.USE_MEMBASE = false;
        }

        [Test]
        public void TestAddBucketAtRuntime()
        {
            Dictionary<string, CouchbaseClient> cacheConnections = UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.CacheConnections;
            Assert.NotNull(cacheConnections);
            Assert.AreEqual(1, cacheConnections.Count);
            MembaseConfig membaseConfig = new MembaseConfig("memberdtocache", null, int.MinValue, int.MinValue, int.MinValue, int.MinValue, int.MinValue, int.MinValue, int.MinValue, int.MinValue, (60*5));
            membaseConfig.AddServer(new MembaseConfigServer("172.16.200.141", 8091));
            membaseConfig.AddServer(new MembaseConfigServer("192.168.4.121", 8091));
            mockSettingsService.AddMembaseConfig(membaseConfig);
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.SettingsService = mockSettingsService;
            int origMillis = UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.ManageConnectionsThreadSleepInMillis;
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.ManageConnectionsThreadSleepInMillis = 1;
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.ManageCacheConnections();
            cacheConnections = UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.CacheConnections;
            Assert.NotNull(cacheConnections);
            Assert.AreEqual(2, cacheConnections.Count);
            Assert.IsTrue(cacheConnections.ContainsKey("memberdtocache"));
            UserNotifications.BusinessLogic.UserNotificationsServiceBL.Instance.ManageConnectionsThreadSleepInMillis = origMillis;
        }

    }
#endif
}
