﻿using System;
using System.Messaging;
using System.Threading;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.UserNotifications.ValueObjects;

namespace Matchnet.UserNotifications.ServiceManagers
{
    public class QueueProcessor
    {
        const string MODULE_NAME = "UserNotifications.QueueProcessor";
        public const string QUEUE_PATH = @".\private$\UserNotifications";
        bool _runnable;

        public UserNotificationsServiceSM ServiceManager { get; set; }
        private MessageQueue userNotificationsQueue;
        private Thread[] threads;
        private ISettingsSA _settingsService = null;

        public ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService) _settingsService = RuntimeSettings.Instance;
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        public void Start()
        {
            const string functionName = "Start";
            try
            {
                startThreads();
            }
            catch (Exception ex)
            {
                UserNotifications.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
        }

        public void Stop()
        {
            const string functionName = "Stop";
            try
            {
                stopThreads();
            }
            catch (Exception ex)
            {
                UserNotifications.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
        }

        private void startThreads()
        {
            const string functionName = "startThreads";
            try
            {
                userNotificationsQueue = new MessageQueue(QUEUE_PATH);
                userNotificationsQueue.Formatter = new BinaryMessageFormatter();

                Int16 threadCount = Convert.ToInt16(SettingsService.GetSettingFromSingleton("USERNOTIFICATIONSSVC_THREAD_COUNT"));
                threads = new Thread[threadCount];

                for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
                {
                    Thread t = new Thread(new ThreadStart(processQueue));
                    t.Name = "ProcessMemberThread" + threadNum.ToString();
                    t.Start();
                    threads[threadNum] = t;
                    _runnable = true;
                }
            }
            catch (Exception ex)
            {
                UserNotifications.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }

        }


        private void stopThreads()
        {
            const string functionName = "stopThreads";
            _runnable = false;
            try
            {
               Int16 threadCount = (Int16)threads.Length;

			for (Int16 threadNum = 0; threadNum < threadCount; threadNum++)
			{
				threads[threadNum].Join(10000);
			}

            UserNotifications.ValueObjects.ServiceTrace.Instance.DebugTrace(MODULE_NAME + functionName, "Stopped all SpotlightMember threads");

    		}
            catch (Exception ex)
            {
               UserNotifications.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);                
            }

        }

        public static void Enqueue(AddNotificationQueueObject message)
        {
            const string functionName = "Enqueue";
            try
            {
                if (!MessageQueue.Exists(QUEUE_PATH))
                {
                    MessageQueue.Create(QUEUE_PATH, true);
                }

                MessageQueue queue = new MessageQueue(QUEUE_PATH);
                queue.Formatter = new BinaryMessageFormatter();
                MessageQueueTransaction trans = new MessageQueueTransaction();

                try
                {
                    trans.Begin();
                    queue.Send(message, trans);

                    trans.Commit();
                }
                finally
                {
                    trans.Dispose();
                }

            }
            catch (Exception ex)
            {
                UserNotifications.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);
            }
        }
        
        private void processQueue()
        {
            const string functionName = "processQueue";
            try
            {
                while (_runnable)
                {
                    processQueueTransaction();
                }

            }           
            catch (Exception ex)
            {           
                UserNotifications.ValueObjects.ServiceTrace.Instance.LogServiceException(MODULE_NAME, functionName, ex, true);           
            }
            

        }

        private void processQueueTransaction()
        {
            MessageQueueTransaction tran = null;

            try
            {
                tran = new MessageQueueTransaction();
                tran.Begin();
                AddNotificationQueueObject msg = receive(tran);

                
                processMessage(msg, tran);
                tran.Commit();
                tran.Dispose();
                tran = null;
            }
            catch (Exception ex)
            {
                attemptRollback(tran);
                Thread.Sleep(5000);
            }
        }

        private void processMessage(AddNotificationQueueObject msg, MessageQueueTransaction tran)
        {

            try
            {
                if (msg == null)
                { throw (new Exception("Queue message is null")); }

                ServiceManager.ProcessUserNotification(msg);
            }
            catch (Exception ex)
            {
                attemptResend(tran, msg, ex);
            }
        }

        private AddNotificationQueueObject receive(MessageQueueTransaction tran)
        {
            AddNotificationQueueObject msg = null;
            try
            {
                msg = (AddNotificationQueueObject)userNotificationsQueue.Receive(tran).Body;
                return msg;
            }
            catch (MessageQueueException mex)
            { throw new Exception("Message queue error. [Error:" + mex.Message + "]", mex); }
        }
       
        private void attemptResend(MessageQueueTransaction tran, AddNotificationQueueObject msg, Exception ex)
        {
            try
            {
                if (msg == null)
                {
                    attemptRollback(tran);
                    return;
                }

                if (tran.Status == MessageQueueTransactionStatus.Pending)
                {
                    tran.Commit();

                    msg.Errors.Add(ex.Source + ", " + ex.Message);
                    msg.Attempts += 1;
                    msg.LastAttemptDateTime = DateTime.Now;

                    Enqueue(msg);
                }
            }
            catch (Exception e)
            {
                string message = "Exception while trying to resend msg: " + ex.Message + ", StackTrace: " + ex.StackTrace;
                throw (new Exception(message));
            }
        }

        private void attemptRollback(MessageQueueTransaction tran)
        {
            try
            {
                if (tran != null)
                {
                    if (tran.Status == MessageQueueTransactionStatus.Pending)
                    {
                        tran.Abort();
                    }
                    tran.Dispose();
                    tran = null;
                }
            }
            catch (Exception ex)
            {
                string message = "Exception while trying to abort transaction: " + ex.Message + ", StackTrace: " + ex.StackTrace;
                throw (new Exception(message));
            }
        }

   
    }
}
