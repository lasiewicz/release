﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.UserNotifications.ValueObjects;
using Matchnet.UserNotifications.ValueObjects.ServiceDefinitions;
using Matchnet.UserNotifications.BusinessLogic;

namespace Matchnet.UserNotifications.ServiceManagers
{
    public class UserNotificationsServiceSM : MarshalByRefObject, IServiceManager, IDisposable, IUserNotificationService
    {
        private PerformanceCounter m_perfGetHitCount;
        private PerformanceCounter m_perfGetHitCountRate;
        private PerformanceCounter m_perfPutHitCount;
        private PerformanceCounter m_perfPutHitCountRate;

        private CircuitBreaker breaker;
        private QueueProcessor _queueProcessor;
        public delegate List<UserNotificationViewObject> Apply(Hashtable theParams);
        private ISettingsSA _settingsService = null;

        public ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService) _settingsService = RuntimeSettings.Instance;
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        public UserNotificationsServiceSM()
        {
            breaker = new CircuitBreaker();
            breaker.SettingsService = SettingsService;
            _queueProcessor = new QueueProcessor();
            _queueProcessor.SettingsService = SettingsService;
            _queueProcessor.ServiceManager = this;
            _queueProcessor.Start();

            initPerfCounters();
            UserNotificationsServiceBL.Instance.UserNotificationsGetRequest += new UserNotificationsServiceBL.UserNotificationsGetRequestEventHandler(UserNotificationsServiceBL_GetRequest);
            UserNotificationsServiceBL.Instance.UserNotificationsPutRequest += new UserNotificationsServiceBL.UserNotificationsPutRequestEventHandler(UserNotificationsServiceBL_PutRequest);

            System.Diagnostics.Trace.WriteLine("UserNotificationsServiceSM contstructor completed.");
        }

        /// <summary>
        /// keep the object alive indefinitely
        /// </summary>
        /// <returns></returns>
        public override object InitializeLifetimeService()
        {
            return null;
        }

        #region IUserNotificationService Members

        public void AddUserNotification(UserNotificationParams unParams, UserNotificationViewObject notification)
        {
            //put notification add in msmq
            QueueProcessor.Enqueue(new AddNotificationQueueObject(unParams, notification));
        }

        public void ProcessUserNotification(AddNotificationQueueObject addNotificationQueueObject)
        {
            try
            {
                Apply add = delegate(Hashtable theParams)
                {
                    UserNotificationParams unp = (null != theParams && theParams.ContainsKey("unp")) ? (UserNotificationParams)theParams["unp"] : null;
                    UserNotificationViewObject unvo = (null != theParams && theParams.ContainsKey("unvo")) ? (UserNotificationViewObject)theParams["unvo"] : null;
                    UserNotificationsServiceBL.Instance.AddUserNotification(unp, unvo);
                    return null;
                };
                Hashtable parms=new Hashtable();
                parms["unp"] = addNotificationQueueObject.GetParams();
                parms["unvo"] = addNotificationQueueObject.GetNotification();
                breaker.Intercept(add, parms);
            }
            catch (OpenCircuitException oce)
            {
                System.Diagnostics.EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, oce.Message, EventLogEntryType.Warning);
                throw oce;  //throw exception so this attempt is requeued
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "AddUserNotification() error. ", ex);
            }
        }

        public void RemoveUserNotification(UserNotificationParams unParams, UserNotificationViewObject notification)
        {
            try
            {
                Apply remove = delegate(Hashtable theParams)
                {
                    UserNotificationParams unp = (null != theParams && theParams.ContainsKey("unp")) ? (UserNotificationParams)theParams["unp"] : null;
                    UserNotificationViewObject unvo = (null != theParams && theParams.ContainsKey("unvo")) ? (UserNotificationViewObject)theParams["unvo"] : null;
                    UserNotificationsServiceBL.Instance.RemoveUserNotification(unp, unvo);
                    return null;
                };
                Hashtable parms = new Hashtable();
                parms["unp"] = unParams;
                parms["unvo"] = notification;
                breaker.Intercept(remove, parms);
            }
            catch (OpenCircuitException oce)
            {
                System.Diagnostics.EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, oce.Message, EventLogEntryType.Warning);
                throw oce;  //throw exception so this attempt is requeued
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "RemoveUserNotification() error. ", ex);
            }
        }

        public List<UserNotificationViewObject> GetUserNotificationsWithDataPoints(UserNotificationParams unParams, int startNum, int pageSize)
        {
            List<UserNotificationViewObject> notifications = null;
            try
            {
                Apply get = delegate(Hashtable theParams)
                {
                    //if (new Random().Next(4) % 2 == 0 && !"HalfOpenState".Equals(breaker.State)) throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "test exception");
                    List<UserNotificationViewObject> myNotifications = null;
                    UserNotificationParams unp = (null != theParams && theParams.ContainsKey("unp")) ? (UserNotificationParams)theParams["unp"] : null;
                    int start = (null != theParams && theParams.ContainsKey("startnum")) ? (int)theParams["startnum"] : 1;
                    int size = (null != theParams && theParams.ContainsKey("pagesize")) ? (int)theParams["pagesize"] : 10;
                    myNotifications = UserNotificationsServiceBL.Instance.GetUserNotificationsWithDataPoints(unp, start, size);
                    return myNotifications;
                };
                Hashtable parms = new Hashtable();
                parms["unp"] = unParams;
                parms["startnum"] = startNum;
                parms["pagesize"] = pageSize;
                notifications = breaker.Intercept(get, parms);
            }
            catch (OpenCircuitException oce)
            {
                System.Diagnostics.EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, oce.Message, EventLogEntryType.Warning);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetUserNotificationsWithDataPoints() error. ", ex);
            }

            if (null != notifications) notifications.Sort();
            return notifications;
        }

        public List<UserNotificationViewObject> GetUserNotifications(UserNotificationParams unParams, int startNum, int pageSize)
        {
            List<UserNotificationViewObject> notifications = null;
            try
            {
                Apply get = delegate(Hashtable theParams)
                {
                    //if (new Random().Next(4) % 2 == 0 && !"HalfOpenState".Equals(breaker.State)) throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "test exception");
                    List<UserNotificationViewObject> myNotifications = null;
                    UserNotificationParams unp = (null != theParams && theParams.ContainsKey("unp")) ? (UserNotificationParams)theParams["unp"] : null;
                    int start = (null != theParams && theParams.ContainsKey("startnum")) ? (int)theParams["startnum"] : 1;
                    int size = (null != theParams && theParams.ContainsKey("pagesize")) ? (int)theParams["pagesize"] : 10;
                    myNotifications = UserNotificationsServiceBL.Instance.GetUserNotifications(unp, start, size);
                    return myNotifications;
                };
                Hashtable parms = new Hashtable();
                parms["unp"] = unParams;
                parms["startnum"] = startNum;
                parms["pagesize"] = pageSize;
                notifications = breaker.Intercept(get, parms);
            }
            catch (OpenCircuitException oce)
            {
                System.Diagnostics.EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, oce.Message, EventLogEntryType.Warning);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "GetUserNotifications() error. ", ex);
            }

            if (null != notifications) notifications.Sort();
            return notifications;
        }

        public void RemoveUserNotifications(UserNotificationParams unParams, List<UserNotificationViewObject> notifications)
        {
            try
            {
                Apply remove = delegate(Hashtable theParams)
                {
                    UserNotificationParams unp = (null != theParams && theParams.ContainsKey("unp")) ? (UserNotificationParams)theParams["unp"] : null;
                    List<UserNotificationViewObject> unvos = (null != theParams && theParams.ContainsKey("unvos")) ? (List<UserNotificationViewObject>)theParams["unvos"] : null;
                    UserNotificationsServiceBL.Instance.RemoveUserNotifications(unp, unvos);
                    return null;
                };
                Hashtable parms=new Hashtable();
                parms["unp"] = unParams;
                parms["unvos"] = notifications;
                breaker.Intercept(remove, parms);
            }
            catch (OpenCircuitException oce)
            {
                System.Diagnostics.EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, oce.Message, EventLogEntryType.Warning);
                throw oce;  //throw exception so this attempt is requeued
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "RemoveUserNotifications() error. ", ex);
            }
        }
        #endregion

        #region IServiceManager Members

        public void PrePopulateCache()
        {
            // no implementation
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (_queueProcessor != null)
                _queueProcessor.Stop();
            resetPerfCounters();
        }

        #endregion

        #region Instrumentation

        private void UserNotificationsServiceBL_GetRequest()
        {
            try
            {
                m_perfGetHitCount.Increment();
                m_perfGetHitCountRate.Increment();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("UserNotificationsServiceSM exception in UserNotificationsServiceBL_GetRequest()");
            }
        }

        private void UserNotificationsServiceBL_PutRequest()
        {
            try
            {
                m_perfPutHitCount.Increment();
                m_perfPutHitCountRate.Increment();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("UserNotificationsServiceSM exception in UserNotificationsServiceBL_PutRequest()");
            }
        }

        private static CounterCreationData[] GetCounters()
        {
            return new CounterCreationData[] { 
                new CounterCreationData(ServiceConstants.PERF_GET_REQS_COUNT_NAME, "Counter for all incoming get requests.", PerformanceCounterType.NumberOfItems64),
                new CounterCreationData(ServiceConstants.PERF_GET_REQS_PER_SECOND_NAME, "Get requests per second.", PerformanceCounterType.RateOfCountsPerSecond64), 
                new CounterCreationData(ServiceConstants.PERF_PUT_REQS_COUNT_NAME, "Counter for all incoming put requests.", PerformanceCounterType.NumberOfItems64),
                new CounterCreationData(ServiceConstants.PERF_PUT_REQS_PER_SECOND_NAME, "Put requests per second.", PerformanceCounterType.RateOfCountsPerSecond64) 
            };
        }

        public static void PerfCounterInstall()
        {
            try
            {
                System.Diagnostics.Trace.WriteLine("UserNotificationsServiceSM,Start PerfCounterInstall");

                CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
                ccdc.AddRange(GetCounters());

                PerformanceCounterCategory.Create(ServiceConstants.SERVICE_NAME, ServiceConstants.SERVICE_NAME,
                    PerformanceCounterCategoryType.SingleInstance, ccdc);

                System.Diagnostics.Trace.WriteLine("UserNotificationsServiceSM,Finish PerfCounterInstall");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("UserNotificationsServiceSM Exception in PerfCounterInstall.");
                new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "PerfCounterInstall() error. ", ex);
            }
        }

        public static void PerfCounterUninstall()
        {
            PerformanceCounterCategory.Delete(ServiceConstants.SERVICE_NAME);
        }

        private void initPerfCounters()
        {
            try
            {
                System.Diagnostics.Trace.WriteLine("UserNotificationsServiceSM, Start initPerfCounters");

                m_perfGetHitCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, ServiceConstants.PERF_GET_REQS_COUNT_NAME, false);
                m_perfGetHitCountRate = new PerformanceCounter(ServiceConstants.SERVICE_NAME, ServiceConstants.PERF_GET_REQS_PER_SECOND_NAME, false);
                m_perfPutHitCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, ServiceConstants.PERF_PUT_REQS_COUNT_NAME, false);
                m_perfPutHitCountRate = new PerformanceCounter(ServiceConstants.SERVICE_NAME, ServiceConstants.PERF_PUT_REQS_PER_SECOND_NAME, false);
                resetPerfCounters();

                System.Diagnostics.Trace.WriteLine("UserNotificationsServiceSM, Finish initPerfCounters");
            }
            catch
            {
                System.Diagnostics.Trace.WriteLine("UserNotificationsServiceSM exception in initPerfCounters()");
            }
        }

        private void resetPerfCounters()
        {
            m_perfGetHitCount.RawValue = 0;
            m_perfGetHitCountRate.RawValue = 0;
            m_perfPutHitCount.RawValue = 0;
            m_perfPutHitCountRate.RawValue = 0;
            System.Diagnostics.Trace.WriteLine("Perf counters reseted.");
        }
        #endregion
    }
}
