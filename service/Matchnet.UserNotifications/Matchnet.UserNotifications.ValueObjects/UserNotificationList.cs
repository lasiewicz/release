﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.UserNotifications.ValueObjects
{
    /// <summary>
    /// Cacheable Container class of User Notification objects for a member
    /// </summary>
    [Serializable]
    public class UserNotificationList : IValueObject, ICacheable
    {
        List<UserNotificationViewObject> _UserNotifications = new List<UserNotificationViewObject>();
        int _MemberID = Constants.NULL_INT;
        int _SiteID = Constants.NULL_INT;
        int _CommunityID = Constants.NULL_INT;

        #region Properties
        public List<UserNotificationViewObject> UserNotifications
        {
            get { return _UserNotifications; }
            set { _UserNotifications = value; }
        }

        public int MemberID
        {
            get { return _MemberID; }
            set { _MemberID = value; }
        }

        public int SiteID
        {
            get { return _SiteID; }
            set { _SiteID = value; }
        }

        public int CommunityID
        {
            get { return _CommunityID; }
            set { _CommunityID = value; }
        }

        public bool UseWDPCache { get; set; }

        #endregion


        #region ICacheable Members
        private int _cacheTTLSeconds = 540;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEYFORMAT = "UserNotificationList-{0}-{1}-{2}";
        const string CACHEKEYWDPFORMAT = "UserNotificationListWDP-{0}-{1}-{2}";

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public string GetCacheKey()
        {
            if (UseWDPCache)
            {
                return GetCacheKeyWDPString(_SiteID, _CommunityID, _MemberID);
            }
            else
            {
                return GetCacheKeyString(_SiteID, _CommunityID, _MemberID);
            }
        }

        public static string GetCacheKeyString(int siteID, int community, int memberid)
        {
            return String.Format(CACHEKEYFORMAT, siteID, community, memberid);
        }
        public static string GetCacheKeyWDPString(int siteID, int community, int memberid)
        {
            return String.Format(CACHEKEYWDPFORMAT, siteID, community, memberid);
        }

        #endregion

    }
}
