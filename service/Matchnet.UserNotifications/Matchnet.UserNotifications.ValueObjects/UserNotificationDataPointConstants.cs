﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.UserNotifications.ValueObjects
{
    public static class UserNotificationDataPointConstants
    {
        public const string CREATOR_USER_NAME = "CREATOR_USER_NAME";
        public const string CREATOR_AGE = "CREATOR_AGE";
        public const string CREATOR_MARITAL_STATUS = "CREATOR_MARITAL_STATUS";
        public const string CREATOR_GENDERMASK = "CREATOR_GENDERMASK";
        public const string CREATOR_LOCATION = "CREATOR_LOCATION";
        public const string MESSAGE_PREVIEW = "MESSAGE_PREVIEW";
        public const string NOTIFICATION_FROM_FAVORITE = "NOTIFICATION_FROM_FAVORITE";
        public const string SPECIFIC_ESSAY_LIKED = "SPECIFIC_ESSAY_LIKED";
        public const string QNA_QUESTION_ID = "QNA_QUESTION_ID";
        public const string QNA_ANSWER_ID = "QNA_ANSWER_ID";
        public const string QNA_ANSWER_MEMBER_ID = "QNA_ANSWER_MEMBER_ID";
        public const string MAIL_MESSAGE_ID = "MAIL_MESSAGE_ID";
        public const string MAIL_MESSAGE_FOLDER_ID = "MAIL_MESSAGE_FOLDER_ID";
        public const string NUMBER_NEW_MATCHES = "NUMBER_NEW_MATCHES";
    }
}
