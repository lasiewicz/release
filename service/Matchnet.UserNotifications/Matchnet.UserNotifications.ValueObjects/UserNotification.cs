﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Matchnet.UserNotifications.ValueObjects
{
    [Serializable]
    public class UserNotificationParams
    {
        public int MemberId = int.MinValue;
        public int SiteId = int.MinValue;
        public int CommunityId = int.MinValue;
        public bool SetAsViewed = false;

        public static UserNotificationParams GetParamsObject(int memberId, int siteId, int communityId)
        {
            UserNotificationParams c = new UserNotificationParams();
            c.MemberId = memberId;
            c.SiteId = siteId;
            c.CommunityId = communityId;
            return c;
        }

        public override string ToString()
        {
            string format = "{{MemberId:{0}, SiteId:{1},CommunityId:{2}}}";
            return string.Format(format, this.MemberId, this.SiteId, this.CommunityId);
        }
    }

    [Serializable]
    public class UserNotificationViewObject :  ISerializable, IValueObject, IComparable, IEquatable<UserNotificationViewObject>
    {
        public string Text = string.Empty;
        public string Timestamp = string.Empty;
        public bool Viewed = false;
        public string MemberName = string.Empty;
        public string Thumbnail = string.Empty;
        public int OwnerId = int.MinValue;
        public int CreatorId = int.MinValue;
        public bool Expired = false;
        public DateTime Created = DateTime.MinValue;
        public int SiteId = int.MinValue;
        public int CommunityId = int.MinValue;
        public string ResourceKey = string.Empty;
        public string OmnitureTag = string.Empty;
        public Dictionary<string, string> DataPoints = new Dictionary<string, string>();
        public bool SystemNotification = false;
        public string ThumbnailFile = string.Empty;
        public NotificationType NotificationType = NotificationType.NotTyped;
        
        public UserNotificationViewObject() { }

        public UserNotificationViewObject(string membername,
            int ownerid,
            int creatorid,
            string thumbnail,
            string text,
            string timestamp,
            bool viewed,
            DateTime created,
            int siteid,
            int communityid,
            string reskey,
            string omnitag)
        {
            this.OwnerId = ownerid;
            this.CreatorId = creatorid;
            this.MemberName = membername;
            this.Thumbnail = thumbnail;
            this.Text = text;
            this.Timestamp = timestamp;
            this.Viewed = viewed;
            this.Created = created;
            this.SiteId = siteid;
            this.CommunityId = communityid;
            this.ResourceKey = reskey;
            this.OmnitureTag = omnitag;
        }

        public UserNotificationViewObject(string membername,
            int ownerid,
            int creatorid,
            string thumbnail,
            string text,
            string timestamp,
            bool viewed,
            DateTime created,
            int siteid,
            int communityid,
            string reskey,
            string omnitag, 
            bool systemNotification,
            string thumbnailFile,
            NotificationType notificationType ): this(membername, ownerid, creatorid, thumbnail, text, timestamp, viewed, created, siteid, communityid, reskey, omnitag)
        {
            SystemNotification = systemNotification;
            ThumbnailFile = thumbnailFile;
            NotificationType = notificationType;
        }

        public UserNotificationViewObject(SerializationInfo info, StreamingContext context)
        {
            try
            {
                var memberNames = new List<string>();

                foreach (SerializationEntry entry in info)
                {
                    memberNames.Add(entry.Name);
                }

                OwnerId = info.GetInt32("OwnerId");
                CreatorId = info.GetInt32("CreatorId");
                MemberName = info.GetString("MemberName");
                Thumbnail = info.GetString("Thumbnail");
                Text = info.GetString("Text");
                Timestamp = info.GetString("Timestamp");
                Viewed = info.GetBoolean("Viewed");
                Created = info.GetDateTime("Created");
                SiteId = info.GetInt32("SiteId");
                CommunityId = info.GetInt32("CommunityId");
                ResourceKey = info.GetString("ResourceKey");
                OmnitureTag = info.GetString("OmnitureTag");
                if (memberNames.Contains("DataPoints"))
                {
                    DataPoints = (Dictionary<string, string>) info.GetValue("DataPoints", typeof (Dictionary<string, string>));
                }
                if (memberNames.Contains("SystemNotification"))
                {
                    SystemNotification = info.GetBoolean("SystemNotification");
                }
                if (memberNames.Contains("ThumbnailFile"))
                {
                    ThumbnailFile = info.GetString("ThumbnailFile");
                }
                if (memberNames.Contains("NotificationType"))
                {
                    NotificationType = (NotificationType) info.GetInt32("NotificationType");
                }
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.DebugTrace(ServiceConstants.SERVICE_NAME, ex, this);
            }
        }

        #region IComparable Members

        public Int32 CompareTo(object obj)
        {
            if (obj is UserNotificationViewObject)
            {
                UserNotificationViewObject un = obj as UserNotificationViewObject;
                Int32 result = un.Created.CompareTo(this.Created);
                if (result == 0)
                {
                    return un.OwnerId.CompareTo(this.OwnerId);
                }

                return result;
            }
            else
                throw new Exception("Compare object is not a UserNotificationViewObject");
        }
        #endregion

        #region IEquatable<UserNotificationViewObject> Members
        public bool Equals(UserNotificationViewObject other)
        {
            if (other is UserNotificationViewObject)
            {
                UserNotificationViewObject un = other as UserNotificationViewObject;
                string key1 = string.Format("{0}|{1}|{2}|{3}|{4}", un.OwnerId, un.CreatorId, un.SiteId, un.CommunityId, un.ResourceKey);
                string key2 = string.Format("{0}|{1}|{2}|{3}|{4}", this.OwnerId, this.CreatorId, this.SiteId, this.CommunityId, this.ResourceKey);
                return key2.Equals(key1);
            }
            return false;
        }
        #endregion


        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            try
            {
                info.AddValue("OwnerId", OwnerId);
                info.AddValue("CreatorId", CreatorId);
                info.AddValue("MemberName", MemberName);
                info.AddValue("Thumbnail", Thumbnail);
                info.AddValue("Text", Text);
                info.AddValue("Timestamp", Timestamp);
                info.AddValue("Viewed", Viewed);
                info.AddValue("Created", Created);
                info.AddValue("SiteId", SiteId);
                info.AddValue("CommunityId", CommunityId);
                info.AddValue("ResourceKey", ResourceKey);
                info.AddValue("OmnitureTag", OmnitureTag);
                info.AddValue("DataPoints", DataPoints);
                info.AddValue("SystemNotification", SystemNotification);
                info.AddValue("ThumbnailFile", ThumbnailFile);
                info.AddValue("NotificationType", NotificationType);
            }
            catch (Exception ex)
            {
                ServiceTrace.Instance.DebugTrace(ServiceConstants.SERVICE_NAME, ex, this);
            }
        }

        #endregion
    }

    [Serializable]
    public class AddNotificationQueueObject
    {
        private UserNotificationParams _unParams;
        private UserNotificationViewObject _notification;

        public int Attempts { get; set; }
        public List<string> Errors { get; set; }
        public DateTime LastAttemptDateTime { get; set; }

        public AddNotificationQueueObject(UserNotificationParams unParams, UserNotificationViewObject notification)
        {
            _unParams = unParams;
            _notification = notification;
            Errors = new List<string>();
        }

        public UserNotificationParams GetParams()
        {
            return _unParams;
        }

        public UserNotificationViewObject GetNotification()
        {
            return _notification;
        }
    }
}
