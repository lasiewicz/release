﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.UserNotifications.ValueObjects
{
    public class ServiceConstants
    {
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_CONSTANT = "USERNOTIFICATION_SVC";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_NAME = "Matchnet.UserNotifications.Service";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_MANAGER_NAME = "UserNotificationsServiceSM";
        /// <summary>
        /// Performance counter name for total hits.
        /// </summary>
        public const string PERF_GET_REQS_COUNT_NAME = "Get Requests count";
        public const string PERF_GET_REQS_PER_SECOND_NAME = "Get Requests Per Second count";
        public const string PERF_PUT_REQS_COUNT_NAME = "Put Requests count";
        public const string PERF_PUT_REQS_PER_SECOND_NAME = "Put Requests Per Second count";
    }
}
