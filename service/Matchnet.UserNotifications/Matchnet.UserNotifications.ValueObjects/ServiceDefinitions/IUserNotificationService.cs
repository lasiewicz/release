﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.UserNotifications.ValueObjects.ServiceDefinitions
{
    public interface IUserNotificationService
    {
        void AddUserNotification(UserNotificationParams unParams, UserNotificationViewObject notification);
        void RemoveUserNotification(UserNotificationParams unParams, UserNotificationViewObject notification);
        List<UserNotificationViewObject> GetUserNotifications(UserNotificationParams unParams, int startNum, int pageSize);
        List<UserNotificationViewObject> GetUserNotificationsWithDataPoints(UserNotificationParams unParams, int startNum, int pageSize);
        void RemoveUserNotifications(UserNotificationParams unParams, List<UserNotificationViewObject> notifications);
    }
}
