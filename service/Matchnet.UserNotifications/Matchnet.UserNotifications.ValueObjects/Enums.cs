﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.UserNotifications.ValueObjects
{
    public enum NotificationType
    {
        NotTyped=0,
        ViewedYourProfile=1,
        FlirtedYou=2,
        EmailedYou=3,
        AddedYouAsFavorite=4,
        ECardedYou=5,
        IMedYou=6,
        MutualYes=7,
        FavoriteUpdatedProfile=8,
        FavoriteUploadedPhoto=9,
        FavoriteAnsweredQandA=10,
        NewMatchesAdded=11,
        NewMembersInLocation=12,
        UpdateProfileNudge=13,
        UploadPhotoNudge=14,
        AnswerTodaySparksNudge=15,
        SubscriptionRenewal=16,
        SubscriptionExpired=17,
        ResourceDrivenNudge=18,
        AllAccessEmailedYou=19,
        LikedAnswer=20,
        LikedFreeText=21,
        NewMemberPromo=22,
        LikedPhoto=23
    }

    public enum LikedFreeTextEssayType
    {
        AboutMe=1,
        PerfectMatch=2,
        PerfectFirstDate=3,
        IdealRelationship=4,
        LearnFromThePast=5,
        PersonalityTrait=6,
        LeisureActivity=7,
        EntertainmentLocation=8,
        PhysicalActivity=9,
        Cuisine=10,
        Music=11,
        Reading=12
    }
}
