﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.PhotoSearchEngine.ValueObjects
{
    [Serializable]
    public enum SearchParameterType
    {
        Text=0,
        Int=1,
        Long=2,
        Double=3,
        Mask=4,
        Keyword=5,
        LocationRegion=6,
        LocationLatLng=7,
        IntRangeFilter = 8,
        DateRangeFilter = 9,
        AgeRangeFilter=10,
        MaxResults = 11
    }

    [Serializable]
    public enum SearchSorting
    {
        JoinDate = 1,
        InsertDate = 2,
        Proximity = 3,
        Popularity = 4,
        ColorCode = 5,
        KeywordRelevance = 6,
        MutualMatch = 7
    }


    [Serializable]
    public class SearchParameterCollection
    {
        public readonly List<SearchParameter> SearchParameters=new List<SearchParameter>();

        public SearchSorting Sort { get; set; }
        public SearchResultType ResultType { get; set; }
        public SearchParameterCollection Add(SearchParameter searchParameter)
        {
            if (!SearchParameters.Contains(searchParameter))
            {
                SearchParameters.Add(searchParameter);
            }
            return this;
        }

        public SearchParameterCollection Remove(SearchParameter searchParameter)
        {
            if (SearchParameters.Contains(searchParameter))
            {
                SearchParameters.Remove(searchParameter);
            }
            return this;            
        }

        public SearchParameterCollection Update(SearchParameter searchParameter)
        {
            Remove(searchParameter);
            Add(searchParameter);
            return this;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("{ ");
            foreach (SearchParameter sp in SearchParameters)
            {
                sb.Append(sp.Name + ":" + sp.Value+", ");
            }
            sb.Remove(sb.Length - 2, 1);
            return sb.Append("}").ToString();
        } 
    }

    [Serializable]
    public class SearchParameter : IEquatable<SearchParameter>
    {
        public SearchParameter(string name, object value, SearchParameterType type)
        {
            Name = name;
            Value = value;
            Type = type;
        }

        public string Name { get; set; }
        public object Value { get; set; }
        public SearchParameterType Type { get; set; }

        #region IEquatable<SearchParameter> Members

        public bool Equals(SearchParameter other)
        {
            return (Name.ToLower().Equals(other.Name.ToLower()) && Type.Equals(other.Type));
        }
        #endregion
    }
}
