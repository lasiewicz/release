﻿using System;
using System.Data;

namespace Spark.PhotoSearchEngine.ValueObjects
{
    [Serializable]
    public enum UpdateTypeEnum:int
    {
        none=0,
        all=1,
        emailcount=2,
        lastactivedate=3,
        hasphoto=4,
        delete=5,
        add=6,
        partial=7
    }

    [Serializable]
    public enum UpdateReasonEnum : int
    {
        none=0,
        adminSuspend=1,
        adminUnsuspend = 2,
        selfSuspend=3,
        selfUnsuspend = 4,
        hide = 5,
        unHide = 6,
        searchDataChanged=7,
        logon=8,
        updateChangedToRemove =9
    }

    [Serializable]
    public enum UpdateModeEnum : int
    {
        none=0,
        add=1,
        update=2,
        remove=3
    }

    #region class PhotoSearch
    [Serializable]
    public class PhotoSearch
    {

        #region private
        private int _memberID;
        private int _memberPhotoID;
        private int _fileID;
        private int _communityID;
        private DateTime _birthDate;
        private int _gender;
        private DateTime _insertDate;
        private int _privateFlag;
        private int _regionID;
        private int _displayToGuestFlag;
        private double _latitude;
        private double _longitude;
        private int _depth1RegionID;
        private int _depth2RegionID;
        private int _depth3RegionID;
        private int _depth4RegionID;
        private DateTime _photoStoreInsertDate;
        private DateTime _photoStoreUpdateDate;
 
        private UpdateReasonEnum _updateReason = UpdateReasonEnum.none;
        private UpdateModeEnum _updateMode = UpdateModeEnum.none;
        #endregion
        #region properties

        //for compatibility with e1loader updates
        public UpdateTypeEnum UpdateType { get; set; }
        public UpdateReasonEnum UpdateReason
        {
            get { return _updateReason; }
            set { _updateReason = value; }
        }
        public UpdateModeEnum UpdateMode
        {
            get { return _updateMode; }
            set { _updateMode = value; }
        }

        public int MemberPhotoId
        {
            get { return _memberPhotoID; }
            set { _memberPhotoID = value; }
        }

        public int MemberId
        {
            get { return _memberID; }
            set { _memberID = value; }
        }

        public int FileId
        {
            get { return _fileID; }
            set { _fileID = value; }
        }

        public int CommunityId
        {
            get { return _communityID; }
            set { _communityID = value; }
        }

        public DateTime BirthDate
        {
            get { return _birthDate; }
            set { _birthDate = value; }
        }

        public int Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        public DateTime InsertDate
        {
            get { return _insertDate; }
            set { _insertDate = value; }
        }

        public int PrivateFlag
        {
            get { return _privateFlag; }
            set { _privateFlag = value; }
        }

        public int RegionId
        {
            get { return _regionID; }
            set { _regionID = value; }
        }

        public int DisplayToGuestFlag
        {
            get { return _displayToGuestFlag; }
            set { _displayToGuestFlag = value; }
        }

        public double Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }

        public double Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }

        public int Depth1RegionId
        {
            get { return _depth1RegionID; }
            set { _depth1RegionID = value; }
        }

        public int Depth2RegionId
        {
            get { return _depth2RegionID; }
            set { _depth2RegionID = value; }
        }

        public int Depth3RegionId
        {
            get { return _depth3RegionID; }
            set { _depth3RegionID = value; }
        }

        public int Depth4RegionId
        {
            get { return _depth4RegionID; }
            set { _depth4RegionID = value; }
        }

        public DateTime PhotoStoreInsertDate
        {
            get { return _photoStoreInsertDate; }
            set { _photoStoreInsertDate = value; }
        }

        public DateTime PhotoStoreUpdateDate
        {
            get { return _photoStoreUpdateDate; }
            set { _photoStoreUpdateDate = value; }
        }

        #endregion

        public static PhotoSearch CreatePhotoSearch(int memberId, int memberPhotoId, int fileId, int communityId, DateTime birthDate, int gender, DateTime insertDate, int privateFlag, int regionId, int displayToGuestFlag, double latitude, double longitude, int depth1RegionId, int depth2RegionId, int depth3RegionId, int depth4RegionId, DateTime photoStoreInsertDate, DateTime photoStoreUpdateDate)
        {
            PhotoSearch photoSearch = new PhotoSearch();
            photoSearch.MemberId = memberId;
            photoSearch.MemberPhotoId = memberPhotoId;
            photoSearch.FileId = fileId;
            photoSearch.CommunityId = communityId;
            photoSearch.BirthDate = birthDate;
            photoSearch.Gender = gender;
            photoSearch.InsertDate = insertDate;
            photoSearch.PrivateFlag = privateFlag;
            photoSearch.RegionId = regionId;
            photoSearch.DisplayToGuestFlag = displayToGuestFlag;
            photoSearch.Latitude = latitude;
            photoSearch.Longitude = longitude;
            photoSearch.Depth1RegionId = depth1RegionId;
            photoSearch.Depth2RegionId = depth2RegionId;
            photoSearch.Depth3RegionId = depth3RegionId;
            photoSearch.Depth4RegionId = depth4RegionId;
            photoSearch.PhotoStoreInsertDate = photoStoreInsertDate;
            photoSearch.PhotoStoreUpdateDate = photoStoreUpdateDate;
            return photoSearch;
        }


        public PhotoSearch Populate(IDataReader dr)
        {
            //columns for search
            if (!dr.IsDBNull(dr.GetOrdinal("MemberID"))) MemberId = dr.GetInt32(dr.GetOrdinal("MemberID"));
            if (!dr.IsDBNull(dr.GetOrdinal("MemberPhotoID"))) MemberPhotoId = dr.GetInt32(dr.GetOrdinal("MemberPhotoID"));
            if (!dr.IsDBNull(dr.GetOrdinal("Gender"))) Gender = dr.GetInt32(dr.GetOrdinal("Gender"));
            if (!dr.IsDBNull(dr.GetOrdinal("Birthdate"))) BirthDate = dr.GetDateTime(dr.GetOrdinal("Birthdate"));
            if (!dr.IsDBNull(dr.GetOrdinal("InsertDate"))) InsertDate = dr.GetDateTime(dr.GetOrdinal("InsertDate"));
            if (!dr.IsDBNull(dr.GetOrdinal("PhotoStoreInsertDate"))) PhotoStoreInsertDate = dr.GetDateTime(dr.GetOrdinal("PhotoStoreInsertDate"));
            if (!dr.IsDBNull(dr.GetOrdinal("PhotoStoreUpdateDate"))) PhotoStoreUpdateDate = dr.GetDateTime(dr.GetOrdinal("PhotoStoreUpdateDate"));
            if (!dr.IsDBNull(dr.GetOrdinal("GroupID"))) CommunityId = dr.GetInt32(dr.GetOrdinal("GroupID"));
            if (!dr.IsDBNull(dr.GetOrdinal("FileID"))) FileId = dr.GetInt32(dr.GetOrdinal("FileID"));
            if (!dr.IsDBNull(dr.GetOrdinal("RegionID"))) RegionId = dr.GetInt32(dr.GetOrdinal("RegionID"));
            if (!dr.IsDBNull(dr.GetOrdinal("Depth1RegionID"))) Depth1RegionId = dr.GetInt32(dr.GetOrdinal("Depth1RegionID"));
            if (!dr.IsDBNull(dr.GetOrdinal("Depth2RegionID"))) Depth2RegionId = dr.GetInt32(dr.GetOrdinal("Depth2RegionID"));
            if (!dr.IsDBNull(dr.GetOrdinal("Depth3RegionID"))) Depth3RegionId = dr.GetInt32(dr.GetOrdinal("Depth3RegionID"));
            if (!dr.IsDBNull(dr.GetOrdinal("Depth4RegionID"))) Depth4RegionId = dr.GetInt32(dr.GetOrdinal("Depth4RegionID"));
            if (!dr.IsDBNull(dr.GetOrdinal("Longitude"))) Longitude = double.Parse(dr.GetDecimal(dr.GetOrdinal("Longitude")).ToString());
            if (!dr.IsDBNull(dr.GetOrdinal("Latitude"))) Latitude = double.Parse(dr.GetDecimal(dr.GetOrdinal("Latitude")).ToString());
            if (!dr.IsDBNull(dr.GetOrdinal("PrivateFlag"))) PrivateFlag = (dr.GetBoolean(dr.GetOrdinal("PrivateFlag")))?1:0;
            if (!dr.IsDBNull(dr.GetOrdinal("DisplayToGuestFlag"))) DisplayToGuestFlag = (dr.GetBoolean(dr.GetOrdinal("DisplayToGuestFlag")))?1:0;
           
            return this;
        }
    }

    #endregion


}
