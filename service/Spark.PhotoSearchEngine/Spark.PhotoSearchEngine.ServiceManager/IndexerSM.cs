﻿using System;
using Matchnet;
using Matchnet.Data.Hydra;
using Spark.Logging;
using Spark.PhotoSearchEngine.BusinessLogic;
using Spark.PhotoSearchEngine.BusinessLogic.Indexer;
using Spark.PhotoSearchEngine.ServiceInterface;
using Spark.PhotoSearchEngine.ValueObjects;

namespace Spark.PhotoSearchEngine.ServiceManager
{
    public class IndexerSM : MarshalByRefObject, IIndexer, IServiceManager, IDisposable
    {
        private HydraWriter _hydraWriter = null;

        public IndexerSM()
        {
            try
            {
                RollingFileLogger.Instance.EnterMethod(ServiceConstants.SERVICE_INDEXER_CONST, "IndexerSM", "constructor");
                IndexerBL.Instance.Start();
                _hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[] { "mnsystem", "mnPhotoStore" });
                _hydraWriter.Start();
                RollingFileLogger.Instance.LeaveMethod(ServiceConstants.SERVICE_INDEXER_CONST, "IndexerSM", "constructor");
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LeaveMethod(ServiceConstants.SERVICE_INDEXER_NAME, "IndexerSM", "constructor");
            }
        }


        #region IServiceManager Members

        public void PrePopulateCache()
        {
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }
            IndexerBL.Instance.Stop();
        }

        #endregion
    }
}
