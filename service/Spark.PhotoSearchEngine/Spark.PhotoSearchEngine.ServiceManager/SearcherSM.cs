﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using Matchnet;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Spark.Logging;
using Spark.PhotoSearchEngine.BusinessLogic.Searcher;
using Spark.PhotoSearchEngine.ServiceInterface;
using Spark.PhotoSearchEngine.ServiceInterfaceNRT;
using Spark.PhotoSearchEngine.ValueObjects;

namespace Spark.PhotoSearchEngine.ServiceManager
{
    public class SearcherSM : MarshalByRefObject, ISearcher, ISearcherNRT, IServiceManager, IDisposable
    {
        private PerformanceCounter _avgSearchTime;
        private PerformanceCounter _avgSearchTimeBase;
        private Stopwatch _avgSearchStopwatch = new Stopwatch();
        private PerformanceCounter _searchReqsPerSec;
        private PerformanceCounter _numConcurrentSearches;
        private PerformanceCounter _timeOfSlowestSearch;
        private PerformanceCounter _totalSearches;
        private Stopwatch _timeOfSlowestSearchStopwatch = new Stopwatch();
        private string _communityName = string.Empty;
        private string _serviceConstant = string.Empty;
        private string _serviceName = string.Empty;
        private HydraWriter _hydraWriter = null;

        public SearcherSM()
        {
            _serviceConstant = ServiceConstants.SERVICE_SEARCHER_CONST;
            _serviceName = ServiceConstants.SERVICE_SEARCHER_NAME;
            try
            {
                // performance counters / instrumentation
                initPerfCounters();

                // bind SearcherBL events
                SearcherBL.Instance.SearcherAddSearch += new SearcherBL.SearcherAddSearchEventHandler(SearcherBL_IncrementCounters);
                SearcherBL.Instance.SearcherRemoveSearch += new SearcherBL.SearcherRemoveSearchEventHandler(SearcherBL_DecrementCounter);
                SearcherBL.Instance.SearcherAverageSearch += new SearcherBL.SearcherAverageSearchEventHandler(SearcherBL_AverageSearchTime);
                SearcherBL.Instance.Start();
                _hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[] { "mnsystem", "mnPhotoStore" });
                _hydraWriter.Start();
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceName, "SearcherSM", "Constructor", ex, null);
            }

        }

        #region ISearcher Members

        public ArrayList RunQuery(SearchParameterCollection parameters, int communityId)
        {
            try
            {
                return SearcherBL.Instance.RunQuery(parameters, communityId);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceName, "SearcherSM", "RunQuery", ex, parameters);
                throw (new BLException(ex));
            }
        }

        #endregion

        #region ISearcherNRT Members

        [OneWay]
        public void NRTUpdatePhoto(PhotoSearch photoSearch)
        {
            try
            {
                SearcherBL.Instance.NRTUpdatePhoto(photoSearch);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceName, "SearcherSM", "NRTUpdatePhoto", ex, null);
                throw (new BLException(ex));
            }
        }

        [OneWay]
        public void NRTRemovePhoto(PhotoSearchUpdate photoSearchUpdate)
        {
            try
            {
                SearcherBL.Instance.NRTRemovePhoto(photoSearchUpdate);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceName, "SearcherSM", "NRTRemovePhoto", ex, null);
                throw (new BLException(ex));
            }
        }

        #endregion

        #region IServiceManager
        public void PrePopulateCache()
        {
        }
        public override object InitializeLifetimeService()
        { return null; }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }
            SearcherBL.Instance.Stop();
            resetPerfCounters();
        }
        #endregion

        #region performance counters
        private int GetAverageSearchTimeInterval()
        {
            string interval = Spark.PhotoSearchEngine.BusinessLogic.Utils.GetSetting("SEARCHER_AVG_SEARCH_INTERVAL", "120000");
            return Convert.ToInt32(interval);
        }

        private int GetTimeOfSlowestSearchInterval()
        {
            string interval = Spark.PhotoSearchEngine.BusinessLogic.Utils.GetSetting("SEARCHER_SLOWEST_SEARCH_INTERVAL", "300000");
            return Convert.ToInt32(interval);
        }

        public static void PerfCounterInstall()
        {
            string serviceName = ServiceConstants.SERVICE_SEARCHER_NAME;
            try
            {
                if (!PerformanceCounterCategory.Exists(serviceName, System.Environment.MachineName))
                {
                    CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
                    ccdc.AddRange(GetCounters());
                    PerformanceCounterCategory.Create(serviceName, serviceName, PerformanceCounterCategoryType.SingleInstance, ccdc);
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                throw new Exception(message + " " + serviceName + " could not create perf counters!!");
            }
        }


        public static void PerfCounterUninstall()
        {
            string serviceName = ServiceConstants.SERVICE_SEARCHER_NAME;
            if (PerformanceCounterCategory.Exists(serviceName, System.Environment.MachineName))
            {
                PerformanceCounterCategory.Delete(serviceName);
            }
        }

        public static CounterCreationData[] GetCounters()
        {
            return new CounterCreationData[] { 
				new CounterCreationData("Search Requests Per Second", "Count of search requests per second", PerformanceCounterType.RateOfCountsPerSecond64),
				new CounterCreationData("Total Searches", "Total number of searches", PerformanceCounterType.NumberOfItems64),
				new CounterCreationData("Number Of Concurrent Searches", "Snapshot of number of concurrent searches", PerformanceCounterType.NumberOfItems64),
				new CounterCreationData("Time Of Slowest Search","Time of slowest search in X minutes", PerformanceCounterType.NumberOfItems64),
                new CounterCreationData("Average Search Time For Last Minute", "Average time of searches for last minute", PerformanceCounterType.RawFraction),
                new CounterCreationData("Base for Average Search Time For Last Minute", "Base Average time of searches for last minute", PerformanceCounterType.AverageBase)
			};
        }

        private void initPerfCounters()
        {
            string serviceName = ServiceConstants.SERVICE_SEARCHER_NAME;
            _avgSearchTime = new PerformanceCounter(serviceName, "Average Search Time For Last Minute", false);
            _avgSearchTimeBase = new PerformanceCounter(serviceName, "Base for Average Search Time For Last Minute", false);
            _timeOfSlowestSearch = new PerformanceCounter(serviceName, "Time Of Slowest Search", false);
            _numConcurrentSearches = new PerformanceCounter(serviceName, "Number Of Concurrent Searches", false);
            _totalSearches = new PerformanceCounter(serviceName, "Total Searches", false);
            _searchReqsPerSec = new PerformanceCounter(serviceName, "Search Requests Per Second", false);
            resetPerfCounters();
        }

        private void resetPerfCounters()
        {
            _avgSearchTime.RawValue = 0;
            _avgSearchTimeBase.RawValue = 0;
            _searchReqsPerSec.RawValue = 0;
            _numConcurrentSearches.RawValue = 0;
            _timeOfSlowestSearch.RawValue = 0;
            _totalSearches.RawValue = 0;
            //reset and start stopwatches
            _avgSearchStopwatch.Reset();
            _timeOfSlowestSearchStopwatch.Reset();
            _avgSearchStopwatch.Start();
            _timeOfSlowestSearchStopwatch.Start();
        }

        private void SearcherBL_IncrementCounters()
        {
            _numConcurrentSearches.Increment();
            _searchReqsPerSec.Increment();
            _totalSearches.Increment();
            //reset if it approaches max value
            if (_totalSearches.RawValue > long.MaxValue - 1)
            {
                _totalSearches.RawValue = 0;
            }
        }

        private void SearcherBL_DecrementCounter()
        {
            _numConcurrentSearches.Decrement();
        }

        private void SearcherBL_AverageSearchTime(long searchTime)
        {
            //reset avg base after 1 minute
            if (_avgSearchStopwatch.ElapsedMilliseconds >= GetAverageSearchTimeInterval())
            {
                _avgSearchTime.RawValue = 0;
                _avgSearchTimeBase.RawValue = 0;
                _avgSearchStopwatch.Reset();
                _avgSearchStopwatch.Start();
            }
            _avgSearchTime.IncrementBy(searchTime);
            _avgSearchTimeBase.IncrementBy(100);

            //reset slowest time after X minutes
            if (_timeOfSlowestSearchStopwatch.ElapsedMilliseconds >= GetTimeOfSlowestSearchInterval())
            {
                _timeOfSlowestSearch.RawValue = 0;
                _timeOfSlowestSearchStopwatch.Reset();
                _timeOfSlowestSearchStopwatch.Start();
            }
            if (_timeOfSlowestSearch.RawValue < searchTime)
            {
                _timeOfSlowestSearch.RawValue = searchTime;
            }
        }
        #endregion

    }
}
