﻿using System;
using Lucene.Net.Index;
using Lucene.Net.Store;
using Spark.PhotoSearchEngine.BusinessLogic.Documents;
using Spark.PhotoSearchEngine.ValueObjects;

namespace Spark.PhotoSearchEngine.BusinessLogic.Indexer
{
    public interface IProcessorFactory : IDisposable
    {
        bool AreAllDone(int communityId, IProcessorType iProcessorType);
        void StartProcessors(int communityId, IProcessorType iProcessorType);
        void LogActiveProcessorCount(int communityId, IProcessorType iProcessorType);
        void ResetCommunity(int communityId, IProcessorType iProcessorType);
        void ChangeMaxSize(int communityId, IProcessorType iProcessorType, int maxSize);

        IProcessor GetIProcessor(int communityId, DocumentMetadata documentMetadata, IndexWriter writer, int maxSize, IProcessorType iProcessorType);
        IProcessor GetIProcessor(int communityId, DocumentMetadata documentMetadata, string indexPath, int maxSize, IProcessorType iProcessorType);
        IProcessor GetIProcessor(int communityId, DocumentMetadata documentMetadata, Directory dir, int maxSize, IProcessorType iProcessorType);

    }
}
