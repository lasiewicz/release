﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Threading;
using Lucene.Net.Index;
using Lucene.Net.Spatial;
using Lucene.Net.Store;
using Spark.Logging;
using Spark.PhotoSearchEngine.BusinessLogic.Documents;
using Spark.PhotoSearchEngine.ValueObjects;
using Spatial4n.Core.Context;
using Spatial4n.Core.Shapes;


namespace Spark.PhotoSearchEngine.BusinessLogic.Indexer
{
    public class PhotoIndexProcessor : IProcessor
    {
        public static string CLASS_NAME = "PhotoIndexProcessor";
        private int _maxSize = 100000;
        private List<PhotoSearch> _searchPhotos = null;
        private long count = 0;
        private long elapsedProcessingTime = 0;
        private long elapsedIndexWriterTime = 0;
        private IndexWriter _writer;
        private DocumentMetadata _documentMetadata;
        private SearchDocument memberdoc;
        private IProcessorType _processorType = IProcessorType.PhotoIndexProcessor;
        private SpatialContext _spatialContext = null;
        private SpatialStrategy _spatialStrategy = null;
        private Dictionary<string, Shape> _spatialShapes = null;
        Stopwatch processingTime = new Stopwatch();
        Stopwatch indexWriterTime = new Stopwatch();

        public bool IsProcessing { get; set; }
        public bool IsFull { get; set; }

        public DocumentMetadata DocumentMetadata
        {
            get { return _documentMetadata; }
            set { _documentMetadata = value; }
        }

        public IndexWriter Writer
        {
            get { return _writer; }
            set { _writer = value; }
        }

        public int MaxSize
        {
            get { return _maxSize; }
            set { _maxSize = value; }
        }

        public int CommunityId { get; set; }

        public Directory IndexDir { get; set; }

        public SpatialContext SpatialContext
        {
            get { return _spatialContext; }
            set { _spatialContext = value; }
        }

        public SpatialStrategy SpatialStrategy
        {
            get { return _spatialStrategy; }
            set { _spatialStrategy = value; }
        }

        public Dictionary<string, Shape> SpatialShapes
        {
            get { return _spatialShapes; }
            set { _spatialShapes = value; }
        }

        public decimal AverageProcessingTime
        {
            get { return (count == 0) ? 0 : (decimal)elapsedProcessingTime / (decimal)count; }
        }

        public decimal AverageIndexWriterTime
        {
            get { return (count == 0) ? 0 : (decimal)elapsedIndexWriterTime / (decimal)count; }
        }

        public bool IsInitialized
        {
            get
            {
                return (null != Writer)
                       && (null != DocumentMetadata)
                       && (null != SpatialContext) 
                       && (null != SpatialStrategy)
                       && (null != SpatialShapes);
            }

        }

        public void Initialize(Hashtable context)
        {
            IndexWriter indexWriter = context["indexWriter"] as IndexWriter;
            DocumentMetadata documentMetadata = context["documentMetadata"] as DocumentMetadata;
            int maxSize = (int)context["maxSize"];
            SpatialContext spatialContext= context["spatialContext"] as SpatialContext;
            SpatialStrategy spatialStrategy = context["spatialStrategy"] as SpatialStrategy;
            Dictionary<string, Shape> spatialShapes = context["spatialShapes"] as Dictionary<string, Shape>;
            this.Writer = indexWriter;
            this.DocumentMetadata = documentMetadata;
            this.MaxSize = maxSize;
            this.SpatialContext = spatialContext;
            this.SpatialStrategy = spatialStrategy;
            this.SpatialShapes = spatialShapes;
        }

        public PhotoIndexProcessor(DocumentMetadata documentMetadata, IndexWriter writer, Directory indexDir, int maxSize)
        {
            this.DocumentMetadata = documentMetadata;
            this.Writer = writer;
            this.IndexDir = indexDir;
            this.MaxSize = maxSize;
            memberdoc = new SearchDocument(DocumentMetadata);
        }

        public void AddDataRow(IDataReader iDataReader)
        {
            if (IsFull)
            {
                throw new ConstraintException("Processor is Full!!");
            }
            if (null == _searchPhotos)
            {
                _searchPhotos = new List<PhotoSearch>(MaxSize);
            }
            processingTime.Start();
            PhotoSearch photoSearch = new PhotoSearch().Populate(iDataReader);
            photoSearch.UpdateType = UpdateTypeEnum.all;
            _searchPhotos.Add(photoSearch);
            processingTime.Stop();
            elapsedProcessingTime += processingTime.ElapsedMilliseconds;
            processingTime.Reset();
            if (_searchPhotos.Count >= MaxSize)
            {
                IsFull = true;
                //start processing
                this.IsProcessing = true;
                Thread t = new Thread(new ThreadStart(this.Run));
                t.IsBackground = true;
                t.Start();
            }
        }

        public void Run()
        {

            IsProcessing = true;
            IndexProcessorFactory.Instance.LogActiveProcessorCount(CommunityId, _processorType);
            count = _searchPhotos.Count;
            RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME,
                                                      string.Format("Total Processing Time: {0}s, Avg Processing Time: {1}ms, Total Photos Processed: {2} for communityId={3} and processor type={4}\n",
                                                          (elapsedProcessingTime / 1000),
                                                          AverageProcessingTime,
                                                          count,
                                                          CommunityId, _processorType), null);
            try
            {
//                int idx = 0;
                int length = _searchPhotos.Count;
                for (int i = 0; i < length;i++)
                {
                    if (IsProcessing)
                    {
                        PhotoSearch photoSearch = _searchPhotos[i];
                        indexWriterTime.Start();
                        IndexPhoto(photoSearch, DocumentMetadata, Writer);
                        indexWriterTime.Stop();
                        elapsedIndexWriterTime += indexWriterTime.ElapsedMilliseconds;
                        indexWriterTime.Reset();
                        _searchPhotos[i] = null;
                        photoSearch = null;
                        //                        idx++;
                        //                        RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, 
                        //                            string.Format("Indexed Photo:{0}, Number {1} out of {2}\n", member.UserID, idx, count), null);
                    }
                    if (!IsProcessing)
                    {
                        break;
                    }
                }

                RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME,
                                                          string.Format(
                                                              "Total Index Write Time: {0}min, Avg Index Write Time: {1}ms, Total Photos Indexed: {2} for communityId={3} and processor type={4}\n",
                                                              ((elapsedIndexWriterTime / 1000) / 60),
                                                              AverageIndexWriterTime, count, CommunityId, _processorType), null);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, ex);
            }
            finally 
            {
                IsProcessing = false;    
            }
            if (null == IndexDir)
            {
                CleanUp();
            }
        }

        private void CleanUp()
        {
            _searchPhotos.Clear();
            Writer = null;
            DocumentMetadata = null;
            count = 0;
            elapsedProcessingTime = 0;
            elapsedIndexWriterTime = 0;
            IsProcessing = false;
            IsFull = false;
            SpatialStrategy = null;
            SpatialContext = null;
            SpatialShapes = null;
        }

        public void IndexPhoto(PhotoSearch photo, DocumentMetadata docMetadata, IndexWriter writer)
        {
            try
            {
                memberdoc.CommunityName = ServiceConstants.GetCommunityName(this.CommunityId);
                memberdoc.Context = SpatialContext;
                memberdoc.Strategy = SpatialStrategy;
                memberdoc.SpatialShapes = SpatialShapes;

                if (photo.MemberId <= 0 || photo.MemberPhotoId <= 0 || photo.CommunityId <= 0)
                    return;
                if (photo.UpdateType == UpdateTypeEnum.all)
                {
                    memberdoc.Populate(photo, docMetadata);
                }
                else if (photo.UpdateType == UpdateTypeEnum.lastactivedate)
                {
                    memberdoc.Add(docMetadata.GetField("PhotoStoreUpdateDate"), photo.PhotoStoreUpdateDate.ToString());
                }
                if (photo.UpdateType != UpdateTypeEnum.delete)
                    writer.UpdateDocument(new Term("id", photo.MemberPhotoId.ToString()), memberdoc.LuceneDocument);
                else
                    writer.DeleteDocuments(new Term("id", photo.MemberPhotoId.ToString()));
                //clear field values
                memberdoc.ClearAllFieldValues();

                //RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, string.Format("Member {0} written to index.", member.MemberID),null);             
            }
            catch (Lucene.Net.Store.AlreadyClosedException ace)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, string.Format("Already closed writer: {0}, {1}, {2}, {3}", this.Writer.ToString(), this.IndexDir, this.CommunityId, this.count), ace, null);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, CLASS_NAME, "IndexPhoto", ex, null);
            }

        }


        public void Dispose()
        {
            this.IsProcessing = false;
            this.CleanUp();
        }
    }
}
