﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Spatial.Queries;
using Lucene.Net.Util;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Spark.Logging;
using Spark.PhotoSearchEngine.BusinessLogic.Searcher.Sorting;
using Spark.PhotoSearchEngine.ValueObjects;
using Spatial4n.Core.Distance;
using Spatial4n.Core.Shapes;

namespace Spark.PhotoSearchEngine.BusinessLogic.Searcher
{
    public class QueryBuilderFactory
    {
        public readonly static QueryBuilderFactory Instance = new QueryBuilderFactory();
        private QueryBuilderFactory() { }

        private int GetMaxActiveDays(int communityId)
        {
            string maxActiveDaysStr = Utils.GetSetting("PHOTO_SEARCHER_MAX_ACTIVE_DAYS", communityId, "180");
            return Convert.ToInt32(maxActiveDaysStr);
        }

        public QueryBuilder GetQueryBuilder(SearchParameterCollection parameters, int communityId)
        {
            string serviceConstant = ServiceConstants.SERVICE_SEARCHER_CONST;
            QueryBuilder queryBuilder = new QueryBuilder();
            queryBuilder.CommunityId = communityId;
            //always get the latest active members
            int maxActiveDays = GetMaxActiveDays(communityId);
//            NumericRangeFilter<long> activeDateFilter = NumericRangeFilter.NewLongRange("PhotoStoreUpdateDate", DateTime.Now.AddDays(-maxActiveDays).Ticks, DateTime.Now.AddDays(1).Ticks, true, true);
//            queryBuilder.AddFilter(activeDateFilter);


            foreach (SearchParameter parameter in parameters.SearchParameters)
            {
                SearchParameterTypeHandlerFactory.Instance.GetSearchParameterTypeHandler(parameter.Type).Handle(parameter, queryBuilder);
            }

            SearchSorting resultSort = parameters.Sort;
            switch (resultSort)
            {
                case SearchSorting.Proximity:
                    SortField sparkSpatialDistanceSortField = new SparkSpatialDistanceSortField("geoField", false, queryBuilder.SpatialShape);
                    queryBuilder.AddSortField(sparkSpatialDistanceSortField);
                    break;                    break;
                case SearchSorting.InsertDate:
                default:
                    break;
            }

            //always sort by last active date
            queryBuilder.AddSortField(new SortField("insertdate", SortField.LONG, true));
            return queryBuilder;
        }
    }

    public class SearchParameterTypeHandlerFactory
    {
        public readonly static SearchParameterTypeHandlerFactory Instance = new SearchParameterTypeHandlerFactory();

        private SearchParameterTypeHandlerFactory() { }

        public ISearchParameterTypeHandler GetSearchParameterTypeHandler(SearchParameterType type)
        {
            ISearchParameterTypeHandler iTypeHandler = null;
            switch (type)
            {
                case SearchParameterType.Int:
                    iTypeHandler = new IntParameterTypeHandler();
                    break;
                case SearchParameterType.Long:
                    iTypeHandler = new LongParameterTypeHandler();
                    break;
                case SearchParameterType.Double:
                    iTypeHandler = new DoubleParameterTypeHandler();
                    break;
                case SearchParameterType.Mask:
                    iTypeHandler = new MaskParameterTypeHandler();
                    break;
                case SearchParameterType.LocationRegion:
                    iTypeHandler= new LocationRegionParameterTypeHandler();
                    break;
                case SearchParameterType.LocationLatLng:
                    iTypeHandler = new LocationLatLngParameterTypeHandler();
                    break;
                case SearchParameterType.IntRangeFilter:
                    iTypeHandler = new IntRangeFilterParameterTypeHandler();
                    break;
                case SearchParameterType.DateRangeFilter:
                    iTypeHandler = new DateRangeFilterParameterTypeHandler();
                    break;
                case SearchParameterType.AgeRangeFilter:
                    iTypeHandler = new AgeRangeFilterParameterTypeHandler();
                    break;
                case SearchParameterType.MaxResults:
                    iTypeHandler = new MaxResultsParameterTypeHandler();
                    break;
                case SearchParameterType.Keyword:
                    iTypeHandler = new KeywordParameterTypeHandler();
                    break;
                default:
                    break;
            }
            return iTypeHandler;
        }
    }

    public interface ISearchParameterTypeHandler
    {
        void Handle(SearchParameter parameter, QueryBuilder queryBuilder);
    }

    public class IntParameterTypeHandler : ISearchParameterTypeHandler
    {
        #region ISearchParameterTypeHandler Members

        public void Handle(SearchParameter parameter, QueryBuilder queryBuilder)
        {
            TermQuery cq = new TermQuery(new Term(parameter.Name, Lucene.Net.Util.NumericUtils.IntToPrefixCoded((int)parameter.Value)));
            queryBuilder.AddQuery(cq);
        }

        #endregion
    }

    public class LongParameterTypeHandler : ISearchParameterTypeHandler
    {
        #region ISearchParameterTypeHandler Members

        public void Handle(SearchParameter parameter, QueryBuilder queryBuilder)
        {
            TermQuery cq = new TermQuery(new Term(parameter.Name, Lucene.Net.Util.NumericUtils.LongToPrefixCoded((long)parameter.Value)));
            queryBuilder.AddQuery(cq);
        }

        #endregion
    }

    public class DoubleParameterTypeHandler : ISearchParameterTypeHandler
    {
        #region ISearchParameterTypeHandler Members

        public void Handle(SearchParameter parameter, QueryBuilder queryBuilder)
        {
            TermQuery cq = new TermQuery(new Term(parameter.Name, Lucene.Net.Util.NumericUtils.DoubleToPrefixCoded((double)parameter.Value)));
            queryBuilder.AddQuery(cq);
        }

        #endregion
    }

    public class MaskParameterTypeHandler : ISearchParameterTypeHandler
    {
        #region ISearchParameterTypeHandler Members

        public void Handle(SearchParameter parameter, QueryBuilder queryBuilder)
        {
            List<int> intValuesFromMask = Utils.GetIntValuesFromMask((int) parameter.Value);
            BooleanQuery boolQuery = new BooleanQuery();
            foreach (int i in intValuesFromMask)
            {
                TermQuery cq = new TermQuery(new Term(parameter.Name, Lucene.Net.Util.NumericUtils.IntToPrefixCoded(i)));
                boolQuery.Add(cq, Occur.SHOULD);
            }
            queryBuilder.AddQuery(boolQuery);
        }

        #endregion
    }

    public class LocationRegionParameterTypeHandler : ISearchParameterTypeHandler
    {
        #region ISearchParameterTypeHandler Members

        public void Handle(SearchParameter parameter, QueryBuilder queryBuilder)
        {
            var locationRegionValues = parameter.Value.ToString().Split(new string[]{"|"}, StringSplitOptions.RemoveEmptyEntries);
            Region region = RegionSA.Instance.RetrieveRegionByID(Convert.ToInt32(locationRegionValues[0]), (int)Matchnet.Language.English);
            string lat = region.Latitude.ToString();
            string lon = region.Longitude.ToString();
            double latitude = DistanceUtils.ToDegrees(Convert.ToDouble(lat));
            double longitude = DistanceUtils.ToDegrees(Convert.ToDouble(lon));
            Point shapeCenter = Utils.SpatialContext.MakePoint(longitude, latitude);            
            int radius = Convert.ToInt32(locationRegionValues[1]);
            Circle circle = Utils.SpatialContext.MakeCircle(shapeCenter, DistanceUtils.Dist2Degrees(radius, DistanceUtils.EARTH_MEAN_RADIUS_MI));
            SpatialArgs args = new SpatialArgs(SpatialOperation.Intersects, circle);
            Filter spatialFilter = Utils.SpatialStrategy.MakeFilter(args);

            queryBuilder.SpatialShape = shapeCenter; 
            queryBuilder.AddFilter(spatialFilter);
        }

        #endregion
    }

    public class LocationLatLngParameterTypeHandler : ISearchParameterTypeHandler
    {
        #region ISearchParameterTypeHandler Members

        public void Handle(SearchParameter parameter, QueryBuilder queryBuilder)
        {
            var locationRegionValues = parameter.Value.ToString().Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            string lat = locationRegionValues[0];
            string lon = locationRegionValues[1];
            double latitude = DistanceUtils.ToDegrees(Convert.ToDouble(lat));
            double longitude = DistanceUtils.ToDegrees(Convert.ToDouble(lon));
            Point shapeCenter = Utils.SpatialContext.MakePoint(longitude, latitude);
            int radius = Convert.ToInt32(locationRegionValues[2]);
            Circle circle = Utils.SpatialContext.MakeCircle(shapeCenter, DistanceUtils.Dist2Degrees(radius, DistanceUtils.EARTH_MEAN_RADIUS_MI));
            SpatialArgs args = new SpatialArgs(SpatialOperation.Intersects, circle);
            Filter spatialFilter = Utils.SpatialStrategy.MakeFilter(args);

            queryBuilder.SpatialShape = shapeCenter;
            queryBuilder.AddFilter(spatialFilter);
        }

        #endregion
    }

    public class IntRangeFilterParameterTypeHandler : ISearchParameterTypeHandler
    {
        #region ISearchParameterTypeHandler Members

        public void Handle(SearchParameter parameter, QueryBuilder queryBuilder)
        {
            var intRangeValues = parameter.Value.ToString().Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            int minRange = Convert.ToInt32(intRangeValues[0]);
            int maxRange = Convert.ToInt32(intRangeValues[1]);
            NumericRangeFilter<int> rangeFilter = NumericRangeFilter.NewIntRange(parameter.Name, minRange, maxRange, true, true);
            queryBuilder.AddFilter(rangeFilter);
        }

        #endregion
    }

    public class DateRangeFilterParameterTypeHandler : ISearchParameterTypeHandler
    {
        #region ISearchParameterTypeHandler Members

        public void Handle(SearchParameter parameter, QueryBuilder queryBuilder)
        {
            var dateRangeValues = parameter.Value.ToString().Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            DateTime minRange = Convert.ToDateTime(dateRangeValues[0]);
            DateTime maxRange = Convert.ToDateTime(dateRangeValues[1]);
            NumericRangeFilter<long> rangeFilter = NumericRangeFilter.NewLongRange(parameter.Name, minRange.Ticks, maxRange.Ticks, true, true);
            queryBuilder.AddFilter(rangeFilter);
        }

        #endregion
    }

    public class AgeRangeFilterParameterTypeHandler : ISearchParameterTypeHandler
    {
        #region ISearchParameterTypeHandler Members

        public void Handle(SearchParameter parameter, QueryBuilder queryBuilder)
        {
            var ageRangeValues = parameter.Value.ToString().Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            int minAgeValue = Convert.ToInt32(ageRangeValues[0]);
            if (minAgeValue <= 0) minAgeValue = 18;
            DateTime minAgeDateTime = DateTime.Now.AddYears(-minAgeValue);

            int maxAgeValue = Convert.ToInt32(ageRangeValues[1]);
            if (maxAgeValue <= 0) maxAgeValue = 99;
            DateTime maxAgeDateTime = DateTime.Now.AddYears(-maxAgeValue);

            NumericRangeFilter<long> rangeFilter = NumericRangeFilter.NewLongRange(parameter.Name, maxAgeDateTime.Ticks, minAgeDateTime.Ticks, true, true);
            queryBuilder.AddFilter(rangeFilter);
        }

        #endregion
    }

    public class MaxResultsParameterTypeHandler : ISearchParameterTypeHandler
    {
        #region ISearchParameterTypeHandler Members

        public void Handle(SearchParameter parameter, QueryBuilder queryBuilder)
        {
            int maxNumOfMatches=0;
            try
            {
                maxNumOfMatches = Convert.ToInt32(parameter.Value);
            }
            catch (Exception ignore)
            {
                Console.WriteLine(ignore);
            }
            if(maxNumOfMatches>0) queryBuilder.MaxNumberOfMatches = maxNumOfMatches;
        }

        #endregion
    }


    public class KeywordParameterTypeHandler : ISearchParameterTypeHandler
    {
        #region ISearchParameterTypeHandler Members
        private int GetSlopFactor(int communityId)
        {
            string slopFactorStr = Utils.GetSetting("SEARCHER_SLOP_FACTOR", communityId, "2");
            return Convert.ToInt32(slopFactorStr);
        }

        public string Sanitize(string s)
        {
            if (string.IsNullOrEmpty(s)) return s;
            //remove special characters that lucene uses for advanced searching
            string sanitizedString = Regex.Replace(s, @"[^\'\""\,\;\w\.@-]", " ");
            return sanitizedString.Trim();
        }

        public string PreProcessKeywords(string value, int minWordLength)
        {
            StringBuilder result = new StringBuilder();
            //remove phrases
            string[] phrases = value.Split(new string[] { "\"", "\'" }, StringSplitOptions.RemoveEmptyEntries);
            StringBuilder multiTerms = new StringBuilder();
            if (phrases.Length > 1)
            {
                for (int i = 0; i < phrases.Length; i++)
                {
                    if (i == 0 || i % 2 == 0)
                    {
                        string s = phrases[i].Trim();
                        if (!string.IsNullOrEmpty(s))
                        {
                            multiTerms.Append(s + " ");
                        }
                    }
                }
            }
            else
            {
                if (!value.StartsWith("\""))
                {
                    multiTerms.Append(value);
                }
            }

            if (multiTerms.Length > 0)
            {
                //split out terms by punctuation
                string[] multiTerm = multiTerms.ToString().Split(new string[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries);
                //create phrase terms for multi terms, this will boost relevance if text has entire phrase
                foreach (string terms in multiTerm)
                {
                    //first split multi term by word
                    string[] term = terms.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    StringBuilder termBuilder = new StringBuilder();
                    foreach (string s in term)
                    {
                        //only use words of a certain length (greater than 2:default)
                        if (s.Trim().Length > minWordLength)
                        {
                            termBuilder.Append(s + " ");
                        }
                    }
                    //if final phrase is more than 1 word, add it as a phrase query
                    if (termBuilder.ToString().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).Length > 1)
                    {
                        result.Append("\"" + termBuilder.ToString().ToLower().Trim() + "\",");
                    }
                }
            }
            //append original terms after sanitizing them to prevent Lucene parse exceptions on advanced search chars: !, ?, ~, etc
            result.Append(Sanitize(value));
            return result.ToString();
        }

        public void Handle(SearchParameter parameter, QueryBuilder queryBuilder)
        {            
            try
            {
                //these text attributes are used across all communities
                List<string> fields = new List<string>(new string[]
                                                        {
                                                            "aboutme",
                                                            "cuisine",
                                                            "music",
                                                            "leisureactivity",
                                                            "physicalactivity"
                                                        });

                //Using MultiFieldQueryParser instead of DisjuncationMaxQuery.  MultiFieldQueryParser produces more relevant matches
                int minWordLength = Utils.GetSetting("SEARCHER_MIN_KEYWORD_LENGTH", queryBuilder.CommunityId, 2);
                SparkMultiFieldQueryParser multiFieldQueryParser = new SparkMultiFieldQueryParser(Lucene.Net.Util.Version.LUCENE_CURRENT,
                                                   fields.ToArray(),
                                                   new StandardAnalyzer(
                                                       Lucene.Net.Util.Version.LUCENE_CURRENT));
                multiFieldQueryParser.UseFuzzyQueryForSingleTerms =
                    Utils.GetSetting("SEARCHER_USE_FUZZY_QUERY_FOR_SINGLE_TERMS", queryBuilder.CommunityId, false);
                multiFieldQueryParser.MinimumWordLength = minWordLength;
                multiFieldQueryParser.PhraseSlop = GetSlopFactor(queryBuilder.CommunityId);
                multiFieldQueryParser.DefaultOperator = QueryParser.Operator.OR;
                string s = parameter.Value.ToString();
                if (Utils.GetSetting("SEARCHER_PREPROCESS_KEYWORD_TERMS", false))
                {
                    s = PreProcessKeywords(s, minWordLength);
                }

                Query query1 = multiFieldQueryParser.Parse(s);
                query1.Boost = 5.0f;
                queryBuilder.AddQuery(query1);
            }
            catch (Exception e)
            {
                string serviceConstant = string.Format(ServiceConstants.SERVICE_SEARCHER_CONST, ServiceConstants.GetCommunityName(queryBuilder.CommunityId).ToUpper());
                RollingFileLogger.Instance.LogException(serviceConstant, "QueryBuilderFactory", e, null);
            }
        }

        #endregion
    }

}
