﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Util;
using Spatial4n.Core.Context;
using Spatial4n.Core.Distance;
using Spatial4n.Core.Shapes;

namespace Spark.PhotoSearchEngine.BusinessLogic.Searcher.Sorting
{
    public class SparkSpatialDistanceSortField : SortField
  	{
		private readonly Point center;

		public SparkSpatialDistanceSortField(string field, bool reverse, Shape shape)
			: base(field, CUSTOM, reverse)
		{
			center = shape.GetCenter();
		}

		public override FieldComparator GetComparator(int numHits, int sortPos)
		{
			return new SpatialDistanceFieldComparatorSource.SpatialDistanceFieldComparator(center, numHits);
		}

		public override FieldComparatorSource ComparatorSource
		{
			get
			{
				return new SpatialDistanceFieldComparatorSource(center);
			}
		} 
	}

	public class SpatialDistanceFieldComparatorSource : FieldComparatorSource
	{
		private readonly Point center;

		public SpatialDistanceFieldComparatorSource(Point center)
		{
			this.center = center;
		}

		public override FieldComparator NewComparator(string fieldname, int numHits, int sortPos, bool reversed)
		{
			return new SpatialDistanceFieldComparator(center, numHits);
		}

		public class SpatialDistanceFieldComparator : FieldComparator
		{
			private readonly double[] values;
			private double bottom;
			private readonly Point originPt;

			private IndexReader currentIndexReader;

			public SpatialDistanceFieldComparator(Point origin, int numHits)
			{
				values = new double[numHits];
				originPt = origin;
			}

			public override int Compare(int slot1, int slot2)
			{
				double a = values[slot1];
				double b = values[slot2];
				if (a > b)
					return 1;
				if (a < b)
					return -1;

				return 0;
			}

			public override void SetBottom(int slot)
			{
				bottom = values[slot];
			}

			public override int CompareBottom(int doc)
			{
				var v2 = CalculateDistance(doc);
				if (bottom > v2)
				{
					return 1;
				}

				if (bottom < v2)
				{
					return -1;
				}

				return 0;
			}

			public override void Copy(int slot, int doc)
			{
				values[slot] = CalculateDistance(doc);
			}

			private double CalculateDistance(int doc)
			{
				var document = currentIndexReader.Document(doc);
				if (document == null)
					return double.NaN;
				double lng = Double.Parse(document.GetField("longitude").StringValue);
                double lat = Double.Parse(document.GetField("latitude").StringValue);
                double lngDEG = DistanceUtils.ToDegrees(lng);
                double latDEG = DistanceUtils.ToDegrees(lat);
                Shape shape;
				try
				{
					shape = Utils.SpatialContext.MakePoint(lngDEG, latDEG);
				}
				catch (InvalidOperationException)
				{
					return double.NaN;
				}
				var pt = shape as Point;
			    double docDistDEG = Utils.SpatialContext.GetDistCalc().Distance(pt, originPt);
			    return DistanceUtils.Degrees2Dist(docDistDEG, DistanceUtils.EARTH_MEAN_RADIUS_MI);

			}

			public override void SetNextReader(IndexReader reader, int docBase)
			{
				currentIndexReader = reader;
			}

			public override IComparable this[int slot]
			{
				get { return values[slot]; }
			}
		}
	}
}
    