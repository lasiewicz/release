﻿using System.Collections.Generic;
using Lucene.Net.Search;
using Spatial4n.Core.Shapes;

namespace Spark.PhotoSearchEngine.BusinessLogic.Searcher
{
    public class QueryBuilder
    {
        public List<Query> queries = new List<Query>();
        public List<Filter> filters = new List<Filter>();
        public List<SortField> sortFields = new List<SortField>();
        private bool _useSearchRedesign30 = false;
        private int maxNumberOfMatches = int.MinValue;

        public int CommunityId { get; set; }
        public Shape SpatialShape { get; set; }
        public int MaxNumberOfMatches
        {
            get { return maxNumberOfMatches; }
            set { maxNumberOfMatches = value; }
        }

        public void AddQuery(Query q)
        {
            queries.Add(q);
        }

        public void ClearQueries()
        {
            queries.Clear();
        }

        public void AddFilter(Filter f)
        {
            filters.Add(f);
        }

        public void AddFilterFirst(Filter f)
        {
            List<Filter> newfilters = new List<Filter>();
            newfilters.Add(f);
            foreach (Filter _f in filters)
            {
                newfilters.Add(_f);
            }
            filters = null;
            filters=newfilters;
        }

        public void AddSortField(SortField s)
        {
            sortFields.Add(s);
        }
    }
}
