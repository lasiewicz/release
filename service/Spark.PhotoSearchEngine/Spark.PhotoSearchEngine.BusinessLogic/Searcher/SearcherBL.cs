﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Matchnet;
using Matchnet.Exceptions;
using Spark.Logging;
using Spark.PhotoSearchEngine.BusinessLogic.Documents;
using Spark.PhotoSearchEngine.ValueObjects;
using Spatial4n.Core.Distance;
using Spatial4n.Core.Shapes;

namespace Spark.PhotoSearchEngine.BusinessLogic.Searcher
{
    public class SearcherBL
    {
        #region debug code
#if DEBUG
        private bool _Show_Distances = false;
        public bool NUNIT_Show_Distances
        {
            get { return _Show_Distances; }
            set { _Show_Distances = value; }
        }

        private bool _Use_NUnit = false;
        public bool USE_NUNIT
        {
            get { return _Use_NUnit; }
            set { _Use_NUnit = value; }
        }

        private bool _NUnit_UseNRT = false;
        public bool NUNIT_UseNRT
        {
            get { return _NUnit_UseNRT; }
            set { _NUnit_UseNRT = value; }
        }

        private bool _Nunit_Print = false;
        public bool NUNIT_Print
        {
            get { return _Nunit_Print; }
            set { _Nunit_Print = value; }
        }

        private StringBuilder _Nunit_StringBuilder = null;
        public StringBuilder NUNIT_StringBuilder
        {
            get
            {
                if (null == _Nunit_StringBuilder) _Nunit_StringBuilder = new StringBuilder();
                return _Nunit_StringBuilder;
            }
            set { _Nunit_StringBuilder = value; }
        }

        public Dictionary<int, string> NUNIT_IndexPaths { get; set; }
        public Dictionary<int, string> NUNIT_KeywordIndexPaths { get; set; }
        public string[] NUNIT_Community_List { get; set; }
        public TopDocs TopDocs_forNUNIT { get; set; }
        public QueryBuilder QueryBuilder_forNUNIT { get; set; }


        public static SearcherBL CreateSearcherBL()
        {
            return new SearcherBL();
        }

#endif
        #endregion

        public readonly static SearcherBL Instance = new SearcherBL();
        private string _communityName = string.Empty;
        private string _serviceConstant = null;
        private Dictionary<string, Searcher> searchers = new Dictionary<string, Searcher>();
        private Thread _updateThread = null;
        private static DocumentMetadata _documentMetadata = null;

        public delegate void SearcherAddSearchEventHandler();
        public event SearcherAddSearchEventHandler SearcherAddSearch;

        public delegate void SearcherRemoveSearchEventHandler();
        public event SearcherRemoveSearchEventHandler SearcherRemoveSearch;

        public delegate void SearcherAverageSearchEventHandler(long searchTime);
        public event SearcherAverageSearchEventHandler SearcherAverageSearch;

        private SearcherBL() { }

        private string GetSearcherKey(int communityId, IProcessorType iProcessorType)
        {
            return string.Format("{0}-{1}", communityId, iProcessorType);
        }

        public void Start()
        {
            try
            {
                this._serviceConstant = ServiceConstants.SERVICE_SEARCHER_CONST;
                string[] communityList = Utils.GetSetting("PHOTO_SEARCHER_COMMUNITY_LIST", "").Split(new char[] { ';' });
#if DEBUG
                if (USE_NUNIT)
                {
                    if (null != NUNIT_Community_List)
                    {
                        communityList = NUNIT_Community_List;
                    }
                }
#endif
//                int thisSearcherCommunityId = ServiceConstants.GetCommunityId(_communityName);
                foreach (string cid in communityList)
                {
                    int communityid = Convert.ToInt32(cid);

//                    bool isSameCommunityAsSearcher = (communityid == thisSearcherCommunityId);
                    string photoSearcherKey = GetSearcherKey(communityid, IProcessorType.PhotoIndexProcessor);
//                    if (!searchers.ContainsKey(photoSearcherKey) && isSameCommunityAsSearcher)
                    if (!searchers.ContainsKey(photoSearcherKey))
                    {
                        Searcher searcher = new Searcher(communityid, IProcessorType.PhotoIndexProcessor);
#if DEBUG
                        searcher.USE_NUNIT = USE_NUNIT;
                        searcher.NUNIT_UseNRT = NUNIT_UseNRT;
                        if (null != NUNIT_IndexPaths && NUNIT_IndexPaths.ContainsKey(communityid))
                        {
                            searcher.NUNIT_IndexPath = NUNIT_IndexPaths[communityid];
                        }
#endif
                        searcher.Init();
                        searchers.Add(photoSearcherKey, searcher);
                    }                    
                }
            }
            finally
            {
#if DEBUG
                if (!USE_NUNIT)
                {
#endif
                    if (null != searchers && searchers.Count > 0)
                    {
                        _updateThread = new Thread(new ThreadStart(Update));
                        _updateThread.Start();
                    }
#if DEBUG
                }
#endif
            }
        }

        public void RemoveSearcher(int communityid, IProcessorType iProcessorType)
        {
            Searcher searcher = null;
            try
            {
                string searcherKey = GetSearcherKey(communityid, iProcessorType);
                if (searchers.ContainsKey(searcherKey))
                {
                    searcher = searchers[searcherKey];
                }

                if (null != searcher)
                {
                    searcher.Dispose();
                    searchers.Remove(searcherKey);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL", "RemoveSearcher()", ex, null);
            }
        }

        public Searcher GetSearcher(int communityid, IProcessorType iProcessorType)
        {
            Searcher searcher = null;
            try
            {
                string searcherKey = GetSearcherKey(communityid, iProcessorType);
                if (searchers.ContainsKey(searcherKey))
                {
                    searcher = searchers[searcherKey];
                }

                if (null == searcher)
                {
                    searcher = new Searcher(communityid, iProcessorType);
                    searcher.Init();
                    searchers.Add(searcherKey, searcher);
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL", "GetSearcher()", ex, null);
            }
            return searcher;
        }

        public ArrayList RunQuery(SearchParameterCollection parameters, int communityId)
        {
            ArrayList results = null;
            string methodName = "RunQuery";
            System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
            Searcher searcher = null;
            QueryBuilder queryBuilder = null;
            TopDocs topDocs = null;
            try
            {
                  searcher = GetSearcher(communityId, IProcessorType.PhotoIndexProcessor);

//                bool enableKeywordFilter = Utils.GetSetting("ENABLE_KEYWORD_FILTER", communityid, false);
//                //if keywords filtering is on and user has keyword terms in their search prefs, filter out users without keyword matches
//                bool hasKeywordTerms = HasKeywordTerms(query);
//                IMemberIdsAccessor keywordMemberIdsAccessor = null;
//                if (enableKeywordFilter && hasKeywordTerms)
//                {
//                    QueryBuilder keywordQueryBuilder = QueryBuilderFactory.Instance.ConvertKeywordQuery(query, communityid);
//                    //if max results for keyword is not set by MatchnetQuery, use the setting
//                    if (keywordQueryBuilder.MaxNumberOfMatches <= 0)
//                    {
//                        int maxResults = Utils.GetSetting("KEYWORD_SEARCHER_MAX_RESULTS", communityid, 2000);
//                        keywordQueryBuilder.MaxNumberOfMatches = maxResults;
//                    }
//                    Searcher keywordSearcher = GetKeywordSearcher(communityid);
//                    if (!stopWatch.IsRunning) stopWatch.Start();
//                    TopDocs keywordResults = keywordSearcher.Search(keywordQueryBuilder);
//                    int length = keywordResults.ScoreDocs.Length;
//                    string[] memberIds = new string[length];
//                    for (int i = 0; i < length; i++)
//                    {
//                        Document doc = keywordSearcher.IndexSearcher.Doc(keywordResults.scoreDocs[i].doc);
//                        memberIds[i] = doc.GetField("id").StringValue();
//                    }
//                    keywordMemberIdsAccessor = new MemberIdsAccessor(memberIds);
//                }

                //convert search params into query
                queryBuilder = QueryBuilderFactory.Instance.GetQueryBuilder(parameters, communityId);//, keywordMemberIdsAccessor);

//                //add blocked member ids
//                for (int i = 0; i < query.Count; i++)
//                {
//                    if (query[i].Parameter == QuerySearchParam.BlockedMemberIDs)
//                    {
//                        object value = query[i].Value;
//                        if (!string.IsNullOrEmpty(value.ToString()))
//                        {
//                            string[] memberIds = Regex.Split(value.ToString(), @"\W+");
//                            BlockedMemberFilter blockedMemberFilter = new BlockedMemberFilter(new MemberIdsAccessor(memberIds));
//                            queryBuilder.AddFilter(blockedMemberFilter);
//                            break;
//                        }
//                    }
//                }


                //increment search counter
                SearcherAddSearch();
                if (!stopWatch.IsRunning) stopWatch.Start();
                topDocs = searcher.Search(queryBuilder);
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL", "RunQuery", ex, null);
                throw new BLException("Unable to run query.  Parameters: " + parameters.ToString().Replace("\r\n", "|"), ex);
            }
            finally
            {
                //decrement search counter
                SearcherRemoveSearch();
                if (null != stopWatch)
                {
                    stopWatch.Stop();
                    //compute search average
                    SearcherAverageSearch(stopWatch.ElapsedMilliseconds);
                }
            }

            try
            {

                if (null == topDocs || topDocs.ScoreDocs.Length == 0)
                {
                    results = new ArrayList(0);
                    RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", methodName, string.Format("Time: {0} ms, Results: {1}, Criteria: {2}", stopWatch.ElapsedMilliseconds, 0, parameters.ToString().Replace("\r\n", "|")));
                }
                else
                {
                    results = new ArrayList(topDocs.ScoreDocs.Length);
                    var documentMetadata = GetDocumentMetadata(communityId);
                    StringBuilder sbIDList = new StringBuilder();
                    for (int i = 0; i < topDocs.ScoreDocs.Length; i++)
                    {
                        Document doc = searcher.IndexSearcher.Doc(topDocs.ScoreDocs[i].Doc);
                        int id = Convert.ToInt32(doc.GetField("id").StringValue);                                
                        sbIDList.Append(id.ToString() + ", ");
                        Hashtable result = new Hashtable();
                        result.Add("memberphotoid", id);
                        result.Add("insertdate", new DateTime(Convert.ToInt64(doc.GetField("insertdate").StringValue)));
                        result.Add("memberid", Convert.ToInt32(doc.GetField("memberid").StringValue));

                        switch (parameters.ResultType)
                        {
                            case SearchResultType.DetailedResult:

                                foreach (Field f in doc.GetFields())
                                {
                                    string fieldname = f.Name;
                                    if ("id".Equals(fieldname.ToLower()) || "insertdate".Equals(fieldname.ToLower()) || "memberid".Equals(fieldname.ToLower()))
                                    {
                                        continue;
                                    }
                                    SearchField searchField = documentMetadata.GetField(fieldname);
                                    switch (searchField.FieldType)
                                    {
                                        case DataType.MASK:
                                            if (!result.ContainsKey(fieldname))
                                            {
                                                result.Add(fieldname, f.StringValue);
                                            }
                                            else
                                            {
                                                result[fieldname] += ","+f.StringValue;
                                            }
                                            break;
                                        case DataType.LONG:
                                        case DataType.INT:
                                            result.Add(fieldname, Conversion.CInt(f.StringValue));
                                            break;
                                        case DataType.DOUBLE:
                                            result.Add(fieldname, Conversion.CDouble(f.StringValue));
                                            break;
                                        case DataType.DATE:
                                            result.Add(fieldname, new DateTime(Convert.ToInt64(f.StringValue)));
                                            break;
                                        case DataType.TXT:
                                        default:
                                            result.Add(fieldname, f.StringValue);
                                            break;
                                    }
                                    if (fieldname.Equals("birthdate"))
                                    {                                        
                                        result.Add("age", Utils.CalculateAge((DateTime)result[fieldname]));
                                    }
                                }

                                if (result.ContainsKey("latitude") && result.ContainsKey("longitude") && null != queryBuilder.SpatialShape)
                                {
                                    double lngDEG = DistanceUtils.ToDegrees((double)result["longitude"]);
                                    double latDEG = DistanceUtils.ToDegrees((double)result["latitude"]);
                                    Point docPoint = Utils.SpatialContext.MakePoint(lngDEG, latDEG);
                                    double docDistDEG = Utils.SpatialContext.GetDistCalc().Distance(queryBuilder.SpatialShape.GetCenter(), docPoint);
                                    double distanceMi = DistanceUtils.Degrees2Dist(docDistDEG, DistanceUtils.EARTH_MEAN_RADIUS_MI);
                                    result.Add("distance",distanceMi);
                                }

                                if (result.ContainsKey("insertdate"))
                                {
                                    DateTime joinDate = (DateTime)result["insertdate"];
                                    int isNew = (DateTime.Now.Subtract(joinDate).TotalDays <= 14) ? 1 : 0;
                                    result.Add("isnew", isNew);
                                }

                                break;
                            default:
                                break;
                        }
                        results.Add(result);

#if DEBUG
//                        if (USE_NUNIT)
//                        {
//                            WriteOutputToBuffer(queryBuilder, sortType, doc, item);
//                        }
                        QueryBuilder_forNUNIT = queryBuilder;
#endif
                    }
                    RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", methodName, string.Format("Time: {0} ms, Results: {1}, Criteria: {2}", stopWatch.ElapsedMilliseconds, topDocs.ScoreDocs.Length, parameters.ToString().Replace("\r\n", "|")));
                    

                    //Toma testing only, will be removed
                    //RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", methodName, sbIDList.ToString());

                }
//#if DEBUG
//                if (USE_NUNIT)
//                {
//                    if (NUNIT_Show_Distances)
//                    {
//                        WriteDistanceOutputToBuffer(searcher.IndexSearcher, queryBuilder, topDocs);
//                    }
//                }
//#endif
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL", "RunQuery", ex, null);
                throw new BLException("Unable to run query.  Parameters: " + parameters.ToString().Replace("\r\n", "|"), ex);
            }
            return results;
        }

        private static DocumentMetadata GetDocumentMetadata(int communityId)
        {
            if (null == _documentMetadata)
            {
                string metadataFile = AppDomain.CurrentDomain.BaseDirectory + "/" +
                                      Utils.GetSetting("PHOTO_SEARCHER_COMMUNITY_METADATA_FILE", communityId, "PhotoSearchDocument.xml");
                _documentMetadata = Serialization.FromXmlFile<DocumentMetadata>(metadataFile);
            }
            return _documentMetadata;
        }

        public void Stop()
        {
            if (null != searchers && searchers.Count > 0)
            {
                foreach (Searcher searcher in searchers.Values)
                {
                    searcher.Dispose();
                }
                searchers.Clear();
            }
#if DEBUG
            if (!USE_NUNIT)
            {
#endif
                if(null != _updateThread) _updateThread.Abort();
#if DEBUG
            }
#endif
        }
    
        private int GetUpdateInterval()
        {
            return Utils.GetSetting("PHOTO_SEARCHER_INDEX_UPDATE_INTERVAL", 60000);
        }

        private void Update()
        {
            while (true)
            {
                Thread.Sleep(GetUpdateInterval());
                string[] communityList = Utils.GetSetting("PHOTO_SEARCHER_COMMUNITY_LIST", "").Split(new char[] { ';' });
//                int thisSearcherCommunityId = ServiceConstants.GetCommunityId(_communityName);
        
                foreach (string communityStr in communityList)
                {
                    int communityId = Convert.ToInt32(communityStr);
//                    bool isSameCommunityAsSearcher = (communityId == thisSearcherCommunityId);
//                    if (isSameCommunityAsSearcher)
//                    {
                        try
                        {
                            Searcher searcher = GetSearcher(communityId, IProcessorType.PhotoIndexProcessor);
                            if (null != searcher)
                            {
                                searcher.Update();
                            }
                        }
                        catch (Exception e)
                        {
                            RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL", "Update", e,null);
                        }
//                    }
//                    else
//                    {
//                        RemoveSearcher(communityId, IProcessorType.PhotoIndexProcessor);
//                    }
                }
            }
        }

        public void NRTUpdatePhoto(PhotoSearch photoSearch)
        {
            try
            {
                if (photoSearch != null)
                {
                    string paramInfo = "CommunityID: " + photoSearch.CommunityId.ToString() + 
                                       ", MemberPhotoID: " + photoSearch.MemberPhotoId.ToString() +
                                       ", MemberID: " + photoSearch.MemberId.ToString() + 
                                       ", UpdateMode: " + photoSearch.UpdateMode.ToString() + 
                                       ", UpdateReason: " + photoSearch.UpdateReason.ToString();
                    if (photoSearch.CommunityId > 0 && photoSearch.MemberId > 0 && photoSearch.MemberPhotoId > 0 &&
                        (photoSearch.UpdateMode == UpdateModeEnum.add || photoSearch.UpdateMode == UpdateModeEnum.update))
                    {
                        //update index
                        Searcher searcher = GetSearcher(photoSearch.CommunityId, IProcessorType.PhotoIndexProcessor);
                        if (searcher != null && searcher.IndexWriter != null)
                        {
                            bool isValid = true;

                            //term based on memberID
                            Term memberIDTerm = new Term("id", photoSearch.MemberPhotoId.ToString());
//                            TermQuery cq = new TermQuery(memberIDTerm);

                            #region Uncomment if we decide to only update if member exists in index

                            //if (searchMember.UpdateMode == UpdateModeEnum.update)
                            //{
                            //    //only update is member exists in index
                            //    QueryBuilder queryBuilder = new QueryBuilder();
                            //    queryBuilder.AddQuery(cq);
                            //    TopDocs topDocs = searcher.Search(queryBuilder);
                            //    if (null == topDocs || topDocs.scoreDocs.Length == 0)
                            //    {
                            //        isValid = false;
                            //        RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTUpdateMemberField", "Member document not found. CommunityID:" + searchMember.CommunityID.ToString() + ", MemberID:" + searchMember.MemberID.ToString());
                            //    }
                            //}

                            #endregion

                            if (isValid)
                            {
                                //create new document
                                DocumentMetadata documentMetadata = Utils.GetDocMetadata(photoSearch.CommunityId);
                                SearchDocument memberDoc = new SearchDocument(documentMetadata);
                                memberDoc.CommunityName = ServiceConstants.GetCommunityName(photoSearch.CommunityId);
                                memberDoc.Context = Utils.SpatialContext;
                                memberDoc.Strategy = Utils.SpatialStrategy;
                                memberDoc.SpatialShapes = Utils.SpatialShapes;
                                memberDoc.Populate(photoSearch, documentMetadata);

                                //update index document
                                searcher.IndexWriter.UpdateDocument(memberIDTerm, memberDoc.LuceneDocument);
                                RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTUpdatePhoto", "Updated MemberPhoto in index. " + paramInfo);
                            }
                        }
                        else
                        {
                            RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTUpdatePhoto", "IndexWriter is unavailable. USE_E2_NRT: " + paramInfo);
                        }
                    }
                    else
                    {
                        RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTUpdatePhoto", "Invalid data in PhotoSearch obj. USE_E2_NRT: " + paramInfo);
                    }
                }
                else
                {
                    RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTUpdatePhoto", "PhotoSearch obj is null");
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL", "NRTUpdatePhoto", ex, photoSearch);
                throw new BLException("Unable to update MemberPhoto for NRT", ex);
            }
        }

        public void NRTRemovePhoto(PhotoSearchUpdate photoSearchUpdate)
        {
            string paramInfo = string.Empty;
            try
            {
                if (photoSearchUpdate != null)
                {
                    paramInfo = "CommunityID: " + photoSearchUpdate.CommunityID.ToString() +
                                ", MemberPhotoID: " + photoSearchUpdate.MemberPhotoID.ToString() +
                                ", MemberID: " + photoSearchUpdate.MemberID.ToString() +
                                ", Reason: " + photoSearchUpdate.UpdateReason.ToString();

                    if (photoSearchUpdate.MemberID > 0 && photoSearchUpdate.CommunityID > 0 && photoSearchUpdate.MemberPhotoID > 0)
                    {
                        //remove member from index, if exists
                        Searcher searcher = GetSearcher(photoSearchUpdate.CommunityID, IProcessorType.PhotoIndexProcessor);
                        if (searcher != null && searcher.IndexWriter != null)
                        {
                            Term memberIDTerm = new Term("id", photoSearchUpdate.MemberPhotoID.ToString());
                            searcher.IndexWriter.DeleteDocuments(memberIDTerm);
                            RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTRemovePhoto", "Removed memberphoto from index. " + paramInfo);
                        }
                        else
                        {
                            RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTRemovePhoto", "IndexWriter is unavailable. USE_E2_NRT: " + paramInfo);
                        }
                    }
                    else
                    {
                        RollingFileLogger.Instance.LogInfoMessage(_serviceConstant, "SearcherBL", "NRTRemovePhoto", "Invalid data. " + paramInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(_serviceConstant, "SearcherBL", "NRTRemovePhoto", ex, null);
                throw new BLException("Unable to remove MemberPhoto for NRT", ex);
            }
        }


    }
}
