﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Lucene.Net.Spatial;

namespace Spark.PhotoSearchEngine.BusinessLogic.Documents
{
    [Serializable]
    public class DocumentMetadata
    {
        [XmlArray("Fields")]
        [XmlArrayItem("Field")]
        public List<SearchField> Fields { get; set; }

        public SearchField GetField(string name)
        {

            if (Fields == null || String.IsNullOrEmpty(name)) return null;

           SearchField field= Fields.Find(delegate(SearchField f) { return f.Name.ToLower() == name.ToLower(); });
           return field;
        }
    }
}
