﻿using System;
using System.Collections.Generic;
using System.Data;
using Lucene.Net.Documents;
using Lucene.Net.Spatial;
using Matchnet;
using Spark.Logging;
using Spark.PhotoSearchEngine.ValueObjects;
using Spatial4n.Core.Context;
using Spatial4n.Core.Distance;
using Spatial4n.Core.Shapes;

namespace Spark.PhotoSearchEngine.BusinessLogic.Documents
{
    [Serializable]
    public class SearchDocument
    {
        private Document _document;
        private DocumentMetadata _documentMetadata;
        private Dictionary<string, Field> _documentFields = new Dictionary<string, Field>();
        private Dictionary<string, NumericField> _documentNumericFields = new Dictionary<string, NumericField>();
        private Dictionary<string, Dictionary<object, NumericField>> _documentMaskNumericFields = new Dictionary<string, Dictionary<object, NumericField>>();

        public int ID { get; set; }
        private static object _lockObject = new object();

        private SpatialContext _context;
        private SpatialStrategy _strategy;
        private Dictionary<string, Shape> _spatialShapes = null;

        public string CommunityName { get; set; }

        public SpatialContext Context
        {
            get { return _context; }
            set { _context = value; }
        }

        public SpatialStrategy Strategy
        {
            get { return _strategy; }
            set { _strategy = value; }
        }

        public Dictionary<string, Shape> SpatialShapes
        {
            get { return _spatialShapes; }
            set { _spatialShapes = value; }
        }

        private void AddSpatialShapes(string latLongKey, Shape shape)
        {
            lock (_lockObject)
            {
                if (null != SpatialShapes && !SpatialShapes.ContainsKey(latLongKey))
                {
                    SpatialShapes.Add(latLongKey, shape);
                }
            }
        }

        private bool HasSpatialShapes(string latLongKey)
        {
            bool b = false;
            lock (_lockObject)
            {
                if (null != SpatialShapes)
                {
                    b = SpatialShapes.ContainsKey(latLongKey);
                }
            }
            return b;
        }


        public SearchDocument(DocumentMetadata metadata)
        {
            _document = new Document();
            _documentMetadata = metadata;
        }

        public Document LuceneDocument
        {
            get { return _document; }
            set { _document = value; }
        }

        #region Lucence Indexing Optimization
        private Field GetField(string fieldName, string value, Field.Index fieldIndex, Field.Store fieldStore, float boost, bool useTermVectors)
        {
            Field f=null;
            if (_documentFields.ContainsKey(fieldName))
            {
                f = _documentFields[fieldName];
                if (null != f && string.IsNullOrEmpty(f.StringValue))
                {
                    f.SetValue(value);
                    if (boost > 0)
                    {
                        f.Boost=boost;
                    }
                }
            }

            if (null == f)
            {
                if (useTermVectors)
                {
                    f = new Field(fieldName, value, fieldStore, fieldIndex, Field.TermVector.YES);
                    if (boost > 0)
                    {
                        f.Boost = boost;
                    }
                }
                else
                {
                    f = new Field(fieldName, value, fieldStore, fieldIndex);
                    if (boost > 0)
                    {
                        f.Boost = boost;
                    }
                }
                if(!_documentFields.ContainsKey(fieldName)) _documentFields.Add(fieldName, f);
            }
            return f;
        }

        private NumericField GetNumericField(string fieldName, int value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f=null;
            if (_documentNumericFields.ContainsKey(fieldName))
            {
                f = _documentNumericFields[fieldName];
                if (null != f && int.MinValue.Equals((int) f.NumericValue))
                {
                    f.SetIntValue(value);
                    if (boost > 0)
                    {
                        f.Boost=boost;
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, fieldStore, fieldIndex);
                f.SetIntValue(value);
                if (boost > 0)
                {
                        f.Boost=boost;
                }
                if (!_documentNumericFields.ContainsKey(fieldName)) _documentNumericFields.Add(fieldName, f);
            }
            return f;
        }

        private NumericField GetNumericField(string fieldName, double value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f = null;
            if (_documentNumericFields.ContainsKey(fieldName))
            {
                f = _documentNumericFields[fieldName];
                if (null != f && double.MinValue.Equals((double) f.NumericValue))
                {
                    f.SetDoubleValue(value);
                    if (boost > 0)
                    {
                        f.Boost = boost;
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, fieldStore, fieldIndex);
                f.SetDoubleValue(value);
                if (boost > 0)
                {
                    f.Boost = boost;
                }
                if (!_documentNumericFields.ContainsKey(fieldName)) _documentNumericFields.Add(fieldName, f);
            }
            return f;
        }

        private NumericField GetNumericField(string fieldName, long value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f = null;
            if (_documentNumericFields.ContainsKey(fieldName))
            {
                f = _documentNumericFields[fieldName];
                if (null != f && long.MinValue.Equals((long) f.NumericValue))
                {
                    f.SetLongValue(value);
                    if (boost > 0)
                    {
                        f.Boost = boost;
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, fieldStore, fieldIndex);
                f.SetLongValue(value);
                if (boost > 0)
                {
                    f.Boost = boost;
                }
                if (!_documentNumericFields.ContainsKey(fieldName)) _documentNumericFields.Add(fieldName, f);
            }
            return f;
        }

        private NumericField GetMaskNumericField(string fieldName, int value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f = null;
            Dictionary<object, NumericField> fields = null;
            if (_documentMaskNumericFields.ContainsKey(fieldName))
            {
                fields = _documentMaskNumericFields[fieldName];
                if (fields.ContainsKey(value))
                {
                    f = fields[value];
                    if (null != f && int.MinValue.Equals((int)f.NumericValue))
                    {
                        f.SetIntValue(value);
                        if (boost > 0)
                        {
                            f.Boost = boost;
                        }
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, fieldStore, fieldIndex);
                f.SetIntValue(value);
                if (boost > 0)
                {
                    f.Boost = boost;
                }
                if (null == fields) fields = new Dictionary<object, NumericField>();
                if (!fields.ContainsKey(value)) fields.Add(value, f);
                if (!_documentMaskNumericFields.ContainsKey(fieldName)) _documentMaskNumericFields.Add(fieldName, fields);
            }
            return f;
        }

        private NumericField GetMaskNumericField(string fieldName, double value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f = null;
            Dictionary<object, NumericField> fields = null;
            if (_documentMaskNumericFields.ContainsKey(fieldName))
            {
                fields = _documentMaskNumericFields[fieldName];
                if (fields.ContainsKey(value))
                {
                    f = fields[value];
                    if (null != f && double.MinValue.Equals((double)f.NumericValue))
                    {
                        f.SetDoubleValue(value);
                        if (boost > 0)
                        {
                            f.Boost = boost;
                        }
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, fieldStore, fieldIndex);
                f.SetDoubleValue(value);
                if (boost > 0)
                {
                    f.Boost = boost;
                }
                if (null == fields) fields = new Dictionary<object, NumericField>();
                if (!fields.ContainsKey(value)) fields.Add(value, f);
                if (!_documentMaskNumericFields.ContainsKey(fieldName)) _documentMaskNumericFields.Add(fieldName, fields);
            }
            return f;
        }

        private NumericField GetMaskNumericField(string fieldName, long value, bool fieldIndex, Field.Store fieldStore, float boost)
        {
            NumericField f = null;
            Dictionary<object, NumericField> fields = null;
            if (_documentMaskNumericFields.ContainsKey(fieldName))
            {
                fields = _documentMaskNumericFields[fieldName];
                if (fields.ContainsKey(value))
                {
                    f = fields[value];
                    if (null != f && long.MinValue.Equals((long)f.NumericValue))
                    {
                        f.SetLongValue(value);
                        if (boost > 0)
                        {
                            f.Boost = boost;
                        }
                    }
                }
            }

            if (null == f)
            {
                f = new NumericField(fieldName, fieldStore, fieldIndex);
                f.SetLongValue(value);
                if (boost > 0)
                {
                    f.Boost = boost;
                }
                if (null == fields) fields = new Dictionary<object, NumericField>();
                if (!fields.ContainsKey(value)) fields.Add(value, f);
                if (!_documentMaskNumericFields.ContainsKey(fieldName)) _documentMaskNumericFields.Add(fieldName, fields);
            }
            return f;
        }

        public void ClearAllFieldValues()
        {
            foreach (Field f in _documentFields.Values)
            {
                f.SetValue(string.Empty);
                if (f.Boost > 0)
                {
                    f.Boost=0f;
                }
                LuceneDocument.RemoveFields(f.Name);
            }

            foreach (NumericField nf in _documentNumericFields.Values)
            {
                if (nf.NumericValue is int) nf.SetIntValue(int.MinValue);
                if (nf.NumericValue is double) nf.SetDoubleValue(double.MinValue);
                if (nf.NumericValue is long) nf.SetLongValue(long.MinValue);
                if (nf.Boost > 0)
                {
                    nf.Boost=0f;
                }
                LuceneDocument.RemoveFields(nf.Name);
            }

            foreach (Dictionary<object, NumericField> dnf in _documentMaskNumericFields.Values)
            {
                foreach (NumericField nf in dnf.Values)
                {
                    if (nf.NumericValue is int) nf.SetIntValue(int.MinValue);
                    if (nf.NumericValue is double) nf.SetDoubleValue(double.MinValue);
                    if (nf.NumericValue is long) nf.SetLongValue(long.MinValue);
                    if (nf.Boost > 0)
                    {
                        nf.Boost=0f;
                    }
                    LuceneDocument.RemoveFields(nf.Name);
                }
            }
        }
        #endregion

        public void Populate(DataRow rs, DocumentMetadata docMetadata)
        {
            try
            {
                for (int i = 0; i < docMetadata.Fields.Count; i++)
                {
                    SearchField fld = docMetadata.Fields[i];
                    if (fld == null)
                        continue;

                    if (fld.FieldType != DataType.LOCATION && fld.FieldType != DataType.AREACODE && fld.FieldType != DataType.TXT)
                    {
                        Add(fld, rs[fld.Name].ToString());
                    }
                    else if (fld.FieldType == DataType.TXT)
                    {
                        Add(fld, rs[fld.Name].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(string.Format(ServiceConstants.SERVICE_INDEXER_CONST, CommunityName.ToUpper()), "Document", "Populate()", ex, null);
            }
        }

        public void Populate(PhotoSearch photo)
        {
            Populate(photo, _documentMetadata);
        }

        public void Populate(PhotoSearch photo, DocumentMetadata docMetadata)
        {
            try
            {
                Add(docMetadata.GetField("memberid"), photo.MemberId.ToString());
                Add(docMetadata.GetField("communityid"), photo.CommunityId.ToString());
                Add(docMetadata.GetField("gender"), photo.Gender.ToString());
                Add(docMetadata.GetField("memberphotoid"), photo.MemberPhotoId.ToString());
                Add(docMetadata.GetField("birthdate"), photo.BirthDate.ToString());
                Add(docMetadata.GetField("insertdate"), photo.InsertDate.ToString());
                Add(docMetadata.GetField("photostoreinsertdate"), photo.PhotoStoreInsertDate.ToString());
                Add(docMetadata.GetField("photostoreupdatedate"), photo.PhotoStoreUpdateDate.ToString());
                Add(docMetadata.GetField("fileid"), photo.FileId.ToString());
                Add(docMetadata.GetField("regionid"), photo.RegionId.ToString());
                Add(docMetadata.GetField("depth1regionid"), photo.Depth1RegionId.ToString());
                Add(docMetadata.GetField("depth2regionid"), photo.Depth2RegionId.ToString());
                Add(docMetadata.GetField("depth3regionid"), photo.Depth3RegionId.ToString());
                Add(docMetadata.GetField("depth4regionid"), photo.Depth4RegionId.ToString());
                Add(docMetadata.GetField("latitude"), photo.Latitude.ToString());
                Add(docMetadata.GetField("longitude"), photo.Longitude.ToString());                
                // Converting radian lat long to degrees because it is what is expected by Spatial.net
                AddLocation(DistanceUtils.ToDegrees(photo.Longitude), DistanceUtils.ToDegrees(photo.Latitude));
                Add(docMetadata.GetField("privateflag"), photo.PrivateFlag.ToString());
                Add(docMetadata.GetField("displaytoguestflag"), photo.DisplayToGuestFlag.ToString());
            }
            catch (Exception ex)
            {
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_INDEXER_CONST, "Document", "Populate(PhotoSearch), MemberPhotoId: "+photo.MemberPhotoId.ToString(), ex, CommunityName);
                throw ex;
            }
        }
        
        public void Add(SearchField fld, String value)
        {
            if (fld.IsDocumentID)
            {

                int id = Conversion.CInt(value);
                if (id > 0)
                {
                    _document.Add(GetField("id",value, Field.Index.NOT_ANALYZED, Field.Store.YES, fld.Boost, false)); //new Field("id", value, Field.Store.YES, Field.Index.NOT_ANALYZED));
                    ID = id;
                    return;
                }
            }
            switch (fld.FieldType)
            {
                case DataType.LONG:
                case DataType.INT:
                case DataType.DOUBLE:
                    addNumeric(fld, value);
                    break;
                case DataType.MASK:
                    addMask(fld, value);
                    break;
                case DataType.DATE:
                    addDate(fld, value);
                    break;
                case DataType.TXT:
                    addText(fld, value);
                    break;
            }
        }

        public void AddLocation(double lng, double lat)
        {
            string latLongKey = lat.ToString() +","+ lng.ToString();
            Shape shape=null;
            if (HasSpatialShapes(latLongKey))
            {
                shape = SpatialShapes[latLongKey];
            }
            else
            {
                shape = Context.MakePoint(lng, lat);
                AddSpatialShapes(latLongKey, shape);
            }

            AbstractField[] shapes = Strategy.CreateIndexableFields(shape);
            //Potentially more than one shape in this field is supported by some
            // strategies; see the javadocs of the SpatialStrategy impl to see.
            int i = 0;
            foreach (AbstractField f in shapes)
            {
                _document.Add(f);
                i++;                
            }
//            RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_INDEXER_CONST, "Document",string.Format("Added {0} spatial shapes", i),null);
//            AbstractField storedField = Strategy.CreateStoredField(shape);
//            _document.Add(storedField);
        }

        private void addNumeric(SearchField fld, String value)
        {
            try
            {
                switch (fld.FieldType)
                {
                    case DataType.INT:
                        {
                            int val = Conversion.CInt(value);
                            _document.Add(GetNumericField(fld.Name, val, true, fld.getLuceneStore(), fld.Boost));
                            break;
                        }
                    case DataType.DOUBLE:
                        {
                            double val = Conversion.CDouble(value);
                            if (val > 0 || fld.Name == "longitude")
                                _document.Add(GetNumericField(fld.Name, val, true, fld.getLuceneStore(), fld.Boost));
                            break;
                        }
                    case DataType.LONG:
                        {
                            long val = Conversion.CInt(value);
                            _document.Add(GetNumericField(fld.Name, val, true, fld.getLuceneStore(), fld.Boost));
                            break;
                        }
                    case DataType.MASK:
                        {
                            int val = Conversion.CInt(value);
                            _document.Add(GetMaskNumericField(fld.Name, val, true, fld.getLuceneStore(), fld.Boost));
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void addText(SearchField fld, string value)
        {
            if (null != value)
            {
                Field field = GetField(fld.Name, value, fld.getLuceneIndex(), fld.getLuceneStore(), fld.Boost, true);
                _document.Add(field);
            }
        }

        private void addDate(SearchField fld, String value)
        {
            addDate(fld, Conversion.CDateTime(value));
        }

        private void addDate(SearchField fld, DateTime value)
        {
            _document.Add(GetNumericField(fld.Name, value.Ticks, true, fld.getLuceneStore(), fld.Boost));
        }

        private void addMask(SearchField fld, String value)
        {
            Int32 mask = Conversion.CInt(value);
            List<int> intValuesFromMask = Utils.GetIntValuesFromMask(mask);
            foreach (int i in intValuesFromMask)
            {
                addNumeric(fld,i.ToString());
            }
        }
    }
}
