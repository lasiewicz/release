﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using Spark.PhotoSearchEngine.ValueObjects;

namespace Spark.PhotoSearchEngine.ServiceInterface
{
    [ServiceContract(Namespace = "spark.net")]
    public interface ISearcher
    {
        [OperationContract]
        ArrayList RunQuery(SearchParameterCollection parameters, int communityId);
    }
}
