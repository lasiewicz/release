﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using Spark.PhotoSearchEngine.ValueObjects;

namespace Spark.PhotoSearchEngine.ServiceInterfaceNRT
{
    public interface ISearcherNRT
    {
        [OperationContract]
        void NRTUpdatePhoto(PhotoSearch photoSearch);
        void NRTRemovePhoto(PhotoSearchUpdate photoSearchUpdate);
    }
}
