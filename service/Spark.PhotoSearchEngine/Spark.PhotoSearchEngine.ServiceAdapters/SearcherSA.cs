﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Spark.PhotoSearchEngine.ServiceInterface;
using Spark.PhotoSearchEngine.ValueObjects;

namespace Spark.PhotoSearchEngine.ServiceAdapters
{
    public class SearcherSA : SABase
    {
        public static readonly SearcherSA Instance = new SearcherSA();
        private ISettingsSA _settingsService = null;

        public string TestURI { get; set; }

        public ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService)
                {
                    _settingsService = RuntimeSettings.Instance;
                }
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        private SearcherSA() { }


        public ArrayList RunQuery(SearchParameterCollection searchParams, int communityId)
        {
            string uri = "";
            ArrayList results = null;
            try
            {
                uri = getServiceManagerUri(communityId);
                base.Checkout(uri);
                try
                {
                    results = getService(uri).RunQuery(searchParams, communityId);
                }
                finally
                {
                    base.Checkin(uri);
                }

            }
            catch (Exception ex)
            {
                throw new SAException(ex);
            }
            return results;

        }

        private ISearcher getService(string uri)
        {
            try
            {
                return (ISearcher)Activator.GetObject(typeof(ISearcher), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }


        private string getServiceManagerUri(int communityId)
        {
            try
            {
                if (!string.IsNullOrEmpty(TestURI))
                {
                    return TestURI;
                }

                string uri = AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_SEARCHER_CONST, PartitionMode.Random).ToUri(ServiceConstants.SERVICE_SEARCHER_MANAGER_CONST);
                string overrideHostName = GetSetting(ServiceConstants.SEARCHER_SA_HOST_OVERRIDE, string.Empty);

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

        protected override void GetConnectionLimit()
        {
            try
            {
                base.MaxConnections = Convert.ToByte(SettingsService.GetSettingFromSingleton("PHOTOSEARCHER_SA_CONNECTION_LIMIT"));
            }
            catch (Exception ex)
            {
                base.MaxConnections = 50;
            }
        }

        public string GetSetting(string name, string defaultvalue)
        {
            try
            {
                return SettingsService.GetSettingFromSingleton(name);

            }
            catch (Exception ex)
            { return defaultvalue; }

        }

        public string GetSetting(string name, int communityID, string defaultvalue)
        {
            try
            {
                return SettingsService.GetSettingFromSingleton(name, communityID);

            }
            catch (Exception ex)
            { return defaultvalue; }

        }

    }
}
