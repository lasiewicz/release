﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Spark.PhotoSearchEngine.ServiceInterfaceNRT;
using Spark.PhotoSearchEngine.ValueObjects;

namespace Spark.PhotoSearchEngine.ServiceAdaptersNRT
{
    public class SearcherNRTSA : SABase
    {
        public static readonly SearcherNRTSA Instance = new SearcherNRTSA();
        private ISettingsSA _settingsService = null;

        public string TestURI { get; set; }

        public ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService)
                {
                    _settingsService = RuntimeSettings.Instance;
                }
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        private SearcherNRTSA() { }

        public void NRTUpdateMemberPhoto(PhotoSearchUpdate memberPhotoUpdate, PhotoSearch photo)
        {
            try
            {
                if (memberPhotoUpdate != null && memberPhotoUpdate.CommunityID > 0 && memberPhotoUpdate.MemberPhotoID > 0)
                {
                    if (memberPhotoUpdate.UpdateMode == UpdateModeEnum.remove)
                    {
                        NRTRemoveMemberPhoto(memberPhotoUpdate);
                    }
                    else
                    {
                        string uri = getServiceManagerUri();
                        base.Checkout(uri);
                        try
                        {
                            if (photo != null)
                            {
                                getService(uri).NRTUpdatePhoto(photo);
                            }
                        }
                        finally
                        {
                            base.Checkin(uri);
                        }
                    }                        
                }
                else
                {
                    //throw new Exception("NRTUpdateMember(): Missing community ID or MemberID");
                }
            }
            catch (Exception ex)
            {
                throw new SAException(ex);
            }
        }


        public void NRTRemoveMemberPhoto(PhotoSearchUpdate memberPhotoUpdate)
        {
            try
            {
                if (memberPhotoUpdate != null && memberPhotoUpdate.CommunityID > 0 && memberPhotoUpdate.MemberPhotoID > 0)
                {
                    string uri = getServiceManagerUri();
                    {
                        base.Checkout(uri);
                        try
                        {
                            memberPhotoUpdate.UpdateMode = UpdateModeEnum.remove;
                            getService(uri).NRTRemovePhoto(memberPhotoUpdate);
                        }
                        catch (Exception e)
                        {
                            //ignore, these API calls are OneWay, if service instance is down, we'll skip it
                        }
                        finally
                        {
                            base.Checkin(uri);
                        }
                    }
                }
                else
                {
                    throw new Exception("NRTRemoveMemberPhoto(): Invalid data");
                }
            }
            catch (Exception ex)
            {
                throw new SAException(ex);
            }
        }

        private ISearcherNRT getService(string uri)
        {
            try
            {
                return (ISearcherNRT)Activator.GetObject(typeof(ISearcherNRT), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }

        private string getServiceManagerUri()
        {
            try
            {
                if (!string.IsNullOrEmpty(TestURI))
                {
                    return TestURI;
                }
                string uri = AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_SEARCHER_CONST, PartitionMode.Random).ToUri(ServiceConstants.SERVICE_SEARCHER_MANAGER_CONST);
                string overrideHostName = GetSetting(ServiceConstants.SEARCHER_SA_HOST_OVERRIDE, string.Empty);

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }


        protected override void GetConnectionLimit()
        {
            try
            {
                base.MaxConnections = Convert.ToByte(SettingsService.GetSettingFromSingleton("PHOTOSEARCHER_SA_CONNECTION_LIMIT"));
            }
            catch (Exception ex)
            {
                base.MaxConnections = 50;
            }
        }

        public string GetSetting(string name, string defaultvalue)
        {
            try
            {
                return SettingsService.GetSettingFromSingleton(name);

            }
            catch (Exception ex)
            { return defaultvalue; }

        }

        public string GetSetting(string name, int communityID, string defaultvalue)
        {
            try
            {
                return SettingsService.GetSettingFromSingleton(name, communityID);

            }
            catch (Exception ex)
            { return defaultvalue; }

        }


    }
}
