﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;
using NUnit.Framework;
using Spark.PhotoSearchEngine.BusinessLogic;
using Spark.PhotoSearchEngine.BusinessLogic.Searcher;
using Spark.PhotoSearchEngine.ValueObjects;

namespace Spark.PhotoSearchEngine.BusinessLogic.Searcher
{
    [TestFixture]
    public class PhotoSearchEngineSearcherTests : AbstractSparkUnitTest
    {

        [TestFixtureSetUp]
        public void StartUp()
        {
            SetupMockSettingsService();
            Utils.SettingsService = _mockSettingsService;
            Spark.Logging.Utils.SettingsService = _mockSettingsService;
            SearchersSetup();
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            _mockSettingsService = null;
            Utils.SettingsService = null;
            Spark.Logging.Utils.SettingsService = null;
            SearchersTeardown();
        }

        [Test]
        public void TestJDatePhotoSearch()
        {
            int communityId = 3;
            int genderVal = (int)GenderMask.Female;
            int minAgeVal = 18;
            int maxAgeVal = 55;
            double radiansLat = 0.594557;
            double radiansLng = -2.066127;
            int radiusMI = 40;

            SearchParameterCollection searchParameterCollection = new SearchParameterCollection()
                .Add(new SearchParameter("communityid", communityId, SearchParameterType.Int))
                .Add(new SearchParameter("gender", genderVal, SearchParameterType.Int))
                .Add(new SearchParameter("birthdate", minAgeVal + "|" + maxAgeVal, SearchParameterType.AgeRangeFilter))
                .Add(new SearchParameter("depth1regionid", 223, SearchParameterType.Int));
//                .Add(new SearchParameter("location", radiansLat + "|" + radiansLng + "|" + radiusMI, SearchParameterType.LocationLatLng));
            DateTime insertMin = DateTime.Now.AddDays(-900);
            DateTime insertMax = DateTime.Now.AddDays(1);
            searchParameterCollection.Add(new SearchParameter("insertdate", insertMin + "|" + insertMax, SearchParameterType.DateRangeFilter));
            searchParameterCollection.Sort = SearchSorting.InsertDate;
            searchParameterCollection.ResultType = SearchResultType.DetailedResult;

            SearcherBL searcherBl = GetSearcherBL(communityId);
            ArrayList results = searcherBl.RunQuery(searchParameterCollection, communityId);
            Assert.NotNull(results);
            Assert.Greater(results.Count, 0);

            foreach (Hashtable result in results)
            {
                int userId = (int)result["memberid"];
                int age = (int)result["age"];
                int gender = (int)result["gender"];
                double distanceMi = (result.ContainsKey("distance")) ? (double)result["distance"] : double.MinValue;
                DateTime insertDate = (DateTime)result["insertdate"];

                Assert.GreaterOrEqual(age, minAgeVal);
                Assert.LessOrEqual(age, maxAgeVal);
                Assert.AreEqual(gender, genderVal);
                Assert.LessOrEqual(distanceMi, radiusMI);

                print(string.Format("UserId:{0}, Gender:{1}, Age:{2}, InsertDate:{3}, Distance:{4}", userId, gender, age, insertDate.ToString(), distanceMi));
            }
        }

        [Test]
        public void TestSparkPhotoSearch()
        {
            int communityId = 1;
            int genderVal = (int)GenderMask.Male;
            int minAgeVal = 18;
            int maxAgeVal = 45;
            double radiansLat = 0.594557;
            double radiansLng = -2.066127;
            int radiusMI = 200;

            SearchParameterCollection searchParameterCollection = new SearchParameterCollection()
                .Add(new SearchParameter("communityid", communityId, SearchParameterType.Int))
                .Add(new SearchParameter("gender", genderVal, SearchParameterType.Int))
                .Add(new SearchParameter("birthdate", minAgeVal + "|" + maxAgeVal, SearchParameterType.AgeRangeFilter))
                .Add(new SearchParameter("location", radiansLat + "|" + radiansLng + "|" + radiusMI, SearchParameterType.LocationLatLng));
            DateTime insertMin = DateTime.Now.AddDays(-900);
            DateTime insertMax = DateTime.Now.AddDays(1);
            searchParameterCollection.Add(new SearchParameter("insertdate", insertMin + "|" + insertMax, SearchParameterType.DateRangeFilter));
            searchParameterCollection.Sort = SearchSorting.Proximity;
            searchParameterCollection.ResultType = SearchResultType.DetailedResult;

            SearcherBL searcherBl = GetSearcherBL(communityId);
            ArrayList results = searcherBl.RunQuery(searchParameterCollection, communityId);
            Assert.NotNull(results);
            Assert.Greater(results.Count, 0);

            foreach (Hashtable result in results)
            {
                int userId = (int)result["memberid"];
                int age = (int)result["age"];
                int gender = (int)result["gender"];
                double distanceMi = (result.ContainsKey("distance")) ? (double)result["distance"] : double.MinValue;
                DateTime insertDate = (DateTime)result["insertdate"];

                Assert.GreaterOrEqual(age, minAgeVal);
                Assert.LessOrEqual(age, maxAgeVal);
                Assert.AreEqual(gender, genderVal);
                Assert.LessOrEqual(distanceMi, radiusMI);

                print(string.Format("UserId:{0}, Gender:{1}, Age:{2}, InsertDate:{3}, Distance:{4}", userId, gender, age, insertDate.ToString(), distanceMi));
            }
        }

        [Test]
        public void TestSparkNRTRemoveMemberPhoto_AdminSuspend()
        {
            int communityId = 1;
            int genderVal = (int)GenderMask.Female;
            int minAgeVal = 18;
            int maxAgeVal = 55;
            double radiansLat = 0.594637;
            double radiansLng = -2.066426;
            int radiusMI = 100;

            SearchParameterCollection searchParameterCollection = new SearchParameterCollection()
                .Add(new SearchParameter("maxResults", 500, SearchParameterType.MaxResults))
                .Add(new SearchParameter("birthdate", minAgeVal + "|" + maxAgeVal, SearchParameterType.AgeRangeFilter))
                .Add(new SearchParameter("communityid", communityId, SearchParameterType.Int))
                .Add(new SearchParameter("gender", genderVal, SearchParameterType.Int))
                .Add(new SearchParameter("location", radiansLat + "|" + radiansLng + "|" + radiusMI, SearchParameterType.LocationLatLng));
            searchParameterCollection.Sort = SearchSorting.InsertDate;
            searchParameterCollection.ResultType = SearchResultType.DetailedResult;

            SearcherBL searcherBl = GetSearcherBL(communityId);

            //set test photos
            var photoSearches = GetSearchPhotos(genderVal, communityId);

            //create ram directory
            SetupTestDataInWriter(communityId, "../../../Spark.PhotoSearchEngine.ValueObjects/PhotoSearchDocument.xml", searcherBl, photoSearches);

            //get initial results count
            ArrayList results = searcherBl.RunQuery(searchParameterCollection, communityId);
            int initialResultsCount = results.Count;
            Assert.AreEqual(photoSearches.Count, initialResultsCount);

            //remove photo NRT
            int memberID = 103820222;
            int memberPhotoID = 3182467;
            PhotoSearchUpdate photoSearchUpdate = new PhotoSearchUpdate();
            photoSearchUpdate.CommunityID = communityId;
            photoSearchUpdate.MemberID = memberID;
            photoSearchUpdate.MemberPhotoID = memberPhotoID;
            photoSearchUpdate.UpdateMode = UpdateModeEnum.remove;
            photoSearchUpdate.UpdateReason = UpdateReasonEnum.adminSuspend;

            searcherBl.NRTRemovePhoto(photoSearchUpdate);

            //refresh NRT Reader
            searcherBl.GetSearcher(communityId, IProcessorType.PhotoIndexProcessor).UpdateNRT();

            //check results count
            ArrayList finalResults = searcherBl.RunQuery(searchParameterCollection, communityId);
            int finalResultsCount = finalResults.Count;
            print("SparkNRTRemovePhoto Test - MemberID:" + memberID.ToString() + ", InitialResults:" + initialResultsCount.ToString() + ", FinalResults:" + finalResultsCount.ToString());
            Assert.AreEqual(initialResultsCount - 1, finalResultsCount);

            //Regenerate SearchBL to use default FS index
            SearcherBL searcherBL = CreateSearcherBL(communityId, photoIndexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
            CreateAndInitializeSearcherBL(communityId, searcherBL, searchParameterCollection);
        }

        [Test]
        public void TestSparkNRTUpdateMemberPhoto_LoginDate()
        {
            int communityId = 1;
            int genderVal = (int)GenderMask.Female;
            int minAgeVal = 18;
            int maxAgeVal = 55;
            double radiansLat = 0.594637;
            double radiansLng = -2.066426;
            int radiusMI = 100;
            int memberID = 103939807;
            int memberPhotoID = 3002407;

            SearchParameterCollection searchParameterCollection = new SearchParameterCollection()
                .Add(new SearchParameter("maxResults", 500, SearchParameterType.MaxResults))
                .Add(new SearchParameter("birthdate", minAgeVal + "|" + maxAgeVal, SearchParameterType.AgeRangeFilter))
                .Add(new SearchParameter("communityid", communityId, SearchParameterType.Int))
                .Add(new SearchParameter("gender", genderVal, SearchParameterType.Int))
                .Add(new SearchParameter("location", radiansLat + "|" + radiansLng + "|" + radiusMI, SearchParameterType.LocationLatLng));
            searchParameterCollection.Sort = SearchSorting.InsertDate;
            searchParameterCollection.ResultType = SearchResultType.DetailedResult;

            SearcherBL searcherBl = GetSearcherBL(communityId);

            //set test photos
            var photoSearches = GetSearchPhotos(genderVal, communityId);

            //create ram directory
            SetupTestDataInWriter(communityId, "../../../Spark.PhotoSearchEngine.ValueObjects/PhotoSearchDocument.xml", searcherBl, photoSearches);

            //get initial results count
            ArrayList results = searcherBl.RunQuery(searchParameterCollection, communityId);
            int initialResultsCount = results.Count;
            int initialMostRecentActiveMemberPhotoId = (int)((Hashtable)results[0])["memberphotoid"];
            Assert.AreEqual(photoSearches.Count, initialResultsCount);

            System.Threading.Thread.Sleep(1500);
            //update member photo NRT
            PhotoSearch photoSearch = PhotoSearch.CreatePhotoSearch(memberID, memberPhotoID, 120016098, communityId, DateTime.Parse("1986-02-03"), genderVal, DateTime.Parse("2009-08-25"), 0, 3443821, 1, 0.594637, -2.066426, 223, 1538, 3403764, 3443821, DateTime.Now, DateTime.Now);
            photoSearch.UpdateReason = UpdateReasonEnum.logon;
            photoSearch.UpdateMode = UpdateModeEnum.update;
            searcherBl.NRTUpdatePhoto(photoSearch);

            //refresh NRT Reader
            searcherBl.GetSearcher(communityId, IProcessorType.PhotoIndexProcessor).UpdateNRT();

            //check results count and most recent active member
            ArrayList finalResults = searcherBl.RunQuery(searchParameterCollection, communityId);
            int finalResultsCount = finalResults.Count;
            int finalMostRecentActiveMemberPhotoId = (int)((Hashtable)finalResults[0])["memberphotoid"];

            print("SparkNRTUpdateMemberPhoto Test - InitialMostRecentMemberPhotoID:" + initialMostRecentActiveMemberPhotoId.ToString() + ", InitialResults:" + initialResultsCount.ToString() + ", MemberIDToUpdate: " + memberID.ToString() + ", FinalMostRecentMemberPhotoID: " + finalMostRecentActiveMemberPhotoId.ToString() + ",  FinalResults:" + finalResultsCount.ToString());
            Assert.AreEqual(initialResultsCount, finalResultsCount);
            Assert.AreEqual(memberPhotoID, finalMostRecentActiveMemberPhotoId);

            //Regenerate SearchBL to use default FS index
            SearcherBL searcherBL = CreateSearcherBL(communityId, photoIndexPaths, nunitCommunityList, SearcherBL_IncrementCounters, SearcherBL_DecrementCounter, SearcherBL_AverageSearchTime);
            CreateAndInitializeSearcherBL(communityId, searcherBL, searchParameterCollection);
        }


        private List<PhotoSearch> GetSearchPhotos(int genderVal, int communityId)
        {
            List<PhotoSearch> photoSearches = new List<PhotoSearch>();
            //beverly hills (zip=90211)
            photoSearches.Add(PhotoSearch.CreatePhotoSearch(103939806, 3001035, 120001464, communityId,
                                                            DateTime.Parse("1987-01-22"), genderVal,
                                                            DateTime.Parse("2009-01-14"), 0, 3443821, 1, 0.594557,
                                                            -2.066127, 223, 1538, 3403764, 3443821,
                                                            DateTime.Parse("2008-12-03"), DateTime.Parse("2010-11-09")));

            photoSearches.Add(PhotoSearch.CreatePhotoSearch(103939807, 3002407, 120016098, communityId,
                                                            DateTime.Parse("1986-02-03"), genderVal,
                                                            DateTime.Parse("2009-08-25"), 0, 3443821, 1, 0.594637,
                                                            -2.066426, 223, 1538, 3403764, 3443821,
                                                            DateTime.Parse("2009-08-25"), DateTime.Parse("2009-08-25")));
            
            //hollywood 5 miles away (zip=90028)
            photoSearches.Add(PhotoSearch.CreatePhotoSearch(103820222, 3182467, 120198418, communityId,
                                                            DateTime.Parse("1986-02-01"), genderVal,
                                                            DateTime.Parse("2013-01-29"), 0, 9805694, 1, 0.595197,
                                                            -2.065054, 223, 1538, 3404282, 9805694,
                                                            DateTime.Parse("2013-01-04"), DateTime.Parse("2013-01-04")));
            
            //pasadena 20 miles away (zip=91101)
            photoSearches.Add(PhotoSearch.CreatePhotoSearch(23247846, 3004677, 120008697, communityId,
                                                            DateTime.Parse("1974-01-02"), genderVal,
                                                            DateTime.Parse("2009-01-14"), 0, 3445194, 1, 0.595984,
                                                            -2.061917, 223, 1538, 3404721, 3445194,
                                                            DateTime.Parse("2008-12-03"), DateTime.Parse("2007-11-09")));                
            return photoSearches;
        }

    }
}
