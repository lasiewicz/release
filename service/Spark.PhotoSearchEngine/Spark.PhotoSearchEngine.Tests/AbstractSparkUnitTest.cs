﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Lucene.Net.Index;
using Lucene.Net.Store;
using Matchnet.Configuration.ServiceAdapters;
using NUnit.Framework;
using Spark.PhotoSearchEngine.BusinessLogic;
using Spark.PhotoSearchEngine.BusinessLogic.Documents;
using Spark.PhotoSearchEngine.BusinessLogic.Indexer;
using Spark.PhotoSearchEngine.BusinessLogic.Searcher;
using Spark.PhotoSearchEngine.ValueObjects;

namespace Spark.PhotoSearchEngine
{
    public abstract class AbstractSparkUnitTest
    {
        protected bool ENABLE_PRINT = false;
        protected Dictionary<int, SearcherBL> searcherBLs = null;
        protected Dictionary<int, List<PhotoSearch>> searchPhotos = null;
        protected string[] nunitCommunityList = new string[] { "1","3" };
        protected Dictionary<int, string> photoIndexPaths = new Dictionary<int, string>();
        protected decimal avgSearchTime = 0;
        protected decimal searchTimes = 0;
        protected int totalSearches = 0;
        protected MockSettingsService _mockSettingsService = null;

        protected void SearcherBL_IncrementCounters() { }
        protected void SearcherBL_DecrementCounter() { }

        protected void SearcherBL_AverageSearchTime(long searchTime)
        {
            totalSearches++;
            searchTimes += searchTime;
            avgSearchTime = searchTimes / totalSearches;
        }

        protected void print(string s)
        {
            print(s, null);
        }

        protected void print(string s, string test)
        {
            if(!string.IsNullOrEmpty(test)) Console.WriteLine(string.Format("---------{0}---------", test));
            Console.WriteLine(s);
        }

//        protected void printResults(Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs, IMatchnetQueryResults results, long millis, SearcherBL searcherBL)
//        {
//            Console.WriteLine(string.Format("time: {0} ms\nsize: {1} results\nprefs: {2}", millis, results.Items.Count, searchPrefs.ToString()));
//            Console.WriteLine(searcherBL.NUNIT_StringBuilder.ToString());
//            searcherBL.NUNIT_StringBuilder = null;
//        }
//

        protected void printMaskValues(List<int> values)
        {
            if (ENABLE_PRINT)
            {
                Console.WriteLine(string.Format("---------VALUES SIZE {0}---------", values.Count));
                foreach (int value in values)
                {
                    Console.WriteLine(string.Format("Value: {0}", value));
                }
            }
        }

        protected SearcherBL GetSearcherBL(int communityId)
        {
            if (searcherBLs.ContainsKey(communityId))
                return searcherBLs[communityId];
            else
                return null;
        }

        protected void RemoveSearcherBL(int communityID)
        {
            SearcherBL searcherBL = GetSearcherBL(communityID);
            if (searcherBL != null)
            {
                searcherBL.Stop();
                searcherBLs.Remove(communityID);
            }
        }

        protected SearcherBL CreateSearcherBL(int communityID, Dictionary<int, string> indexPaths, string[] nunitCommunityList, SearcherBL.SearcherAddSearchEventHandler addSearchEventHandler, SearcherBL.SearcherRemoveSearchEventHandler removeSearchEventHandler, SearcherBL.SearcherAverageSearchEventHandler averageSearchEventHandler)
        {
            SearcherBL searcherBL = SearcherBL.CreateSearcherBL();
            searcherBL.USE_NUNIT = true;
            searcherBL.NUNIT_UseNRT = true;
            if (null != indexPaths)
            {
                searcherBL.NUNIT_IndexPaths = indexPaths;
            }
            searcherBL.NUNIT_Community_List = nunitCommunityList;
            searcherBL.SearcherAddSearch += new SearcherBL.SearcherAddSearchEventHandler(addSearchEventHandler);
            searcherBL.SearcherRemoveSearch += new SearcherBL.SearcherRemoveSearchEventHandler(removeSearchEventHandler);
            searcherBL.SearcherAverageSearch += new SearcherBL.SearcherAverageSearchEventHandler(averageSearchEventHandler);
            switch (communityID)
            {
                case 1:
                    searcherBL.Start();
                    break;
                case 3:
                    searcherBL.Start();
                    break;
                default:
                    break;
            }
            return searcherBL;
        }
//
//        protected void TestAccuracy(Matchnet.Search.ValueObjects.SearchPreferenceCollection searchPrefs, int communityId, int siteId, int[] expectedMemberIds, SearcherBL searcherBL)
//        {
//            IMatchnetQuery matchnetQuery = TestQueryBuilder.build(searchPrefs, communityId, siteId);
//
//            IMatchnetQueryResults results = searcherBL.RunDetailedQuery(matchnetQuery, communityId);
//
//            for (int i = 0; i < results.Items.Count; i++)
//            {
//                DetailedQueryResult result = results.Items[i] as DetailedQueryResult;
//                print(string.Format("Memberid {0}:", result.MemberID));
//                foreach (string key in result.Tags.Keys)
//                {
//                    print(string.Format("\tField \"{0}\", Value \"{1}\"", key, result.Tags[key]));
//                }
//            }
//
//            Assert.NotNull(results);
//            Assert.GreaterOrEqual(results.Items.Count, 1);
//
//            foreach (int memberId in expectedMemberIds)
//            {
//                bool success = false;
//                foreach (IMatchnetResultItem resultItem in results.Items.List)
//                {
//                    if (resultItem.MemberID == memberId)
//                    {
//                        success = true;
//                    }
//                }
//                Assert.IsTrue(success, string.Format("{0} is not in Results!!", memberId));
//            }
//            Assert.AreEqual(expectedMemberIds.Length, results.Items.Count);
//        }



//        protected void SetupTestDataInWriter(int communityId, string xmlFilePath, SearcherBL searcherBL, IProcessorType iProcessorType)
//        {
//            IList members = null;
//            switch(iProcessorType)
//            {
//                case IProcessorType.KeywordIndexProcessor:
//                    members = searchKeywords[communityId];
//                    break;
//                case IProcessorType.MemberIndexProcessor:
//                default:
//                    members = searchMembers[communityId];
//                    break;
//            }
//            SetupTestDataInWriter(communityId, xmlFilePath, searcherBL, members, iProcessorType);
//        }
//
//        protected void SetupTestDataInWriter(int communityId, string xmlFilePath, SearcherBL searcherBL)
//        {
//            List<SearchMember> commSearchMembers = searchMembers[communityId];
//            SetupTestDataInWriter(communityId, xmlFilePath, searcherBL, commSearchMembers);
//        }
//
        protected void SetupTestDataInWriter(int communityId, string xmlFilePath, SearcherBL searcherBL, IList commSearchPhotos)
        {
            SetupTestDataInWriter(communityId,xmlFilePath,searcherBL,commSearchPhotos,IProcessorType.PhotoIndexProcessor);
        }

        protected void SetupTestDataInWriter(int communityId, string xmlFilePath, SearcherBL searcherBL, IList commSearchPhotos, IProcessorType iProcessorType)
        {
            Directory dir = new RAMDirectory();
            IndexWriter writer = new IndexWriter(dir, new Lucene.Net.Analysis.Standard.StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_CURRENT), true, IndexWriter.MaxFieldLength.UNLIMITED);
            writer.UseCompoundFile=false;
            //add docs
            DocumentMetadata documentMetadata = Serialization.FromXmlFile<DocumentMetadata>(xmlFilePath);

            Searcher searcher = null;
            switch (iProcessorType)
            {
                case IProcessorType.PhotoIndexProcessor:
                default:
                    PhotoIndexProcessor photoIndexProcessor = new PhotoIndexProcessor(documentMetadata, writer, dir, commSearchPhotos.Count * 2);
                    photoIndexProcessor.CommunityId = communityId;
                    photoIndexProcessor.SpatialContext = Utils.SpatialContext;
                    photoIndexProcessor.SpatialShapes = Utils.SpatialShapes;
                    photoIndexProcessor.SpatialStrategy = Utils.SpatialStrategy;
                    foreach (PhotoSearch searchPhoto in commSearchPhotos)
                    {
                        searchPhoto.UpdateType = UpdateTypeEnum.all;
                        photoIndexProcessor.IndexPhoto(searchPhoto, documentMetadata, writer);
                    }
                    writer.Close();
                    searcher = searcherBL.GetSearcher(communityId, IProcessorType.PhotoIndexProcessor);
                    break;
            }
            searcher.Update(dir, string.Empty);
        }

        protected void CreateAndInitializeSearcherBL(int communityID, SearcherBL searcherBL, SearchParameterCollection testSearchParams)
        {
            RemoveSearcherBL(communityID);
            searcherBLs.Add(communityID, searcherBL);
            testSearchParams.Add(new SearchParameter("communityid", communityID, SearchParameterType.Int));
            searcherBL.RunQuery(testSearchParams, communityID);
        }

        protected void SearchersSetup()
        {
            int communityId = 3;
            int genderVal = 1;
            int minAgeVal = 18;
            int maxAgeVal = 55;
            double radiansLat = 0.594557;
            double radiansLng = -2.066127;
            int radiusMI = 200;

            SearchParameterCollection searchParameterCollection = new SearchParameterCollection();
            searchParameterCollection.Add(new SearchParameter("communityid", communityId, SearchParameterType.Int))
                .Add(new SearchParameter("gender", genderVal, SearchParameterType.Int))
                .Add(new SearchParameter("birthdate", minAgeVal + "|" + maxAgeVal, SearchParameterType.AgeRangeFilter))
                .Add(new SearchParameter("location", radiansLat + "|" + radiansLng + "|" + radiusMI,SearchParameterType.LocationLatLng));
            searchParameterCollection.Sort=SearchSorting.Proximity;
            searchParameterCollection.ResultType=SearchResultType.DetailedResult;

            if (!photoIndexPaths.ContainsKey(1)) photoIndexPaths.Add(1, "C:/matchnet/PhotoIndexStage/Spark2");
            if (!photoIndexPaths.ContainsKey(3)) photoIndexPaths.Add(3, "C:/matchnet/PhotoIndexStage/JDate2");


            searcherBLs = new Dictionary<int, SearcherBL>();
            foreach (string s in nunitCommunityList)
            {
                int communityID = Convert.ToInt32(s);
                SearcherBL searcherBL = CreateSearcherBL(communityID, photoIndexPaths, nunitCommunityList,
                                                         SearcherBL_IncrementCounters, SearcherBL_DecrementCounter,
                                                         SearcherBL_AverageSearchTime);
                CreateAndInitializeSearcherBL(communityID, searcherBL, searchParameterCollection);
            }
        }

        protected void SearchersTeardown()
        {
            foreach (SearcherBL searcherBL in searcherBLs.Values)
            {
                searcherBL.Stop();
                searcherBL.USE_NUNIT = false;
                searcherBL.NUNIT_StringBuilder = null;
            }
            photoIndexPaths.Clear();
            searcherBLs = null;
        }

        protected void SetupMockSettingsService()
        {
            _mockSettingsService = new MockSettingsService();

            _mockSettingsService.AddSetting("SEARCHER_CLOSE_WAIT_TIME", "300");
            _mockSettingsService.AddSetting("INDEXER_DB_TIMEOUT", "12000");
            _mockSettingsService.AddSetting("INDEXER_MAX_MB_BUFFER_SIZE", "48");
            _mockSettingsService.AddSetting("INDEXER_MAX_MERGE_DOCS", "1500");
            _mockSettingsService.AddSetting("INDEXER_MERGE_FACTOR", "48");
            _mockSettingsService.AddSetting("INDEXER_COMMUNITY_THREADS", "4");
            _mockSettingsService.AddSetting("INDEXER_USE_COMPOUND_FILE", "true");

            _mockSettingsService.AddSetting("PHOTO_INDEXER_COPY_INDEX", "false");
            _mockSettingsService.AddSetting("PHOTO_SEARCHER_INDEX_UPDATE_INTERVAL", "false");
            _mockSettingsService.AddSetting("PHOTO_SEARCHER_MAX_RESULTS", "1000");
            _mockSettingsService.AddSetting("PHOTO_SEARCHER_MAX_ACTIVE_DAYS", "28000");
            _mockSettingsService.AddSetting("PHOTO_SEARCHER_COMMUNITY_LIST", "1;3");
            _mockSettingsService.AddSetting("PHOTOSEARCHINDEXER_SVC_LOGGER_MASK", "63");
            _mockSettingsService.AddSetting("PHOTOSEARCHER_SVC_LOGGER_MASK", "63");
            _mockSettingsService.AddSetting("PHOTO_INDEXER_ACTIVE_DAYS", "5000");
            _mockSettingsService.AddSetting("PHOTO_INDEXER_STAGE_COMMUNITY_DIR", "c:/Matchnet/PhotoIndexStage/");
            _mockSettingsService.AddSetting("PHOTO_SEARCHER_COMMUNITY_DIR", "c:/Matchnet/PhotoIndex/");
            _mockSettingsService.AddSetting("PHOTO_INDEXER_SWAPPING_INTERVAL", "60");
            _mockSettingsService.AddSetting("ENABLE_PHOTO_INDEX_DIR_SWAP_UPDATES", "true");
            _mockSettingsService.AddSetting("PHOTO_SEARCHER_COMMUNITY_METADATA_FILE", "PhotoSearchDocument.xml");
            _mockSettingsService.AddSetting("PHOTO_INDEXER_STAGE_COMMUNITY_DIR", "c:/Matchnet/PhotoIndexStage/JDate", 3);
            _mockSettingsService.AddSetting("PHOTO_SEARCHER_COMMUNITY_DIR", "c:/Matchnet/PhotoIndex/JDate", 3);
            _mockSettingsService.AddSetting("ENABLE_PHOTO_MATCHES_SEARCHPREF", "true");
            _mockSettingsService.AddSetting("PHOTO_INDEXER_CRON_EXPRESSION", "0 0/60 * * * ?");
            _mockSettingsService.AddSetting("PHOTO_INDEXER_STAGE_COMMUNITY_DIR", "c:/Matchnet/PhotoIndexStage/Spark", 1);
            _mockSettingsService.AddSetting("PHOTO_SEARCHER_COMMUNITY_DIR", "c:/Matchnet/PhotoIndex/Spark", 1);

            _mockSettingsService.AddSetting("SEARCHER_SLOP_FACTOR", "2");
            _mockSettingsService.AddSetting("ENABLE_KEYWORD_FILTER", "true");
            _mockSettingsService.AddSetting("SEARCHER_USE_FUZZY_QUERY_FOR_SINGLE_TERMS", "false");
            _mockSettingsService.AddSetting("INDEXER_USE_SYSTEM_DB_FOR_INDEX_UPDATES", "true");
            _mockSettingsService.AddSetting("SEARCHER_USE_SYSTEM_DB_FOR_INDEX_UPDATES", "true");
            _mockSettingsService.AddSetting("SEARCHER_MIN_KEYWORD_LENGTH", "2");
            _mockSettingsService.AddSetting("SEARCHER_PREPROCESS_KEYWORD_TERMS", "true");

            //SA settings
            _mockSettingsService.AddSetting("PHOTOSEARCHER_SA_HOST_OVERRIDE", "bhd-arodriguez");
        }
    }

    public class MockSettingsService : ISettingsSA
    {
        private Dictionary<int, Dictionary<string, string>> communitySettings = new Dictionary<int, Dictionary<string, string>>();
        #region ISettingsSA Members

        public void AddSetting(string key, string value)
        {
            AddSetting(key, value, 0);
        }

        public void AddSetting(string key, string value, int communityId)
        {
            if (!communitySettings.ContainsKey(communityId)) communitySettings.Add(communityId, new Dictionary<string, string>());
            communitySettings[communityId].Add(key, value);
        }

        public void RemoveSetting(string key)
        {
            RemoveSetting(key, 0);
        }

        public void RemoveSetting(string key, int communityId)
        {
            if (SettingExistsFromSingleton(key, communityId, 0))
            {
                communitySettings[communityId].Remove(key);
            }
            else if (SettingExistsFromSingleton(key, 0, 0))
            {
                communitySettings[0].Remove(key);
            }
        }

        public System.Collections.Generic.List<Matchnet.Configuration.ValueObjects.MembaseConfig> GetMembaseConfigsFromSingleton()
        {
            throw new System.NotImplementedException();
        }

        public Matchnet.Configuration.ValueObjects.ServiceInstanceConfig GetServiceInstanceConfigFromSingleton(string serviceConstant, string machineName)
        {
            throw new System.NotImplementedException();
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID, int brandID)
        {
            if (SettingExistsFromSingleton(constant, communityID, 0))
            {
                return communitySettings[communityID][constant];
            }
            else
            {
                return GetSettingFromSingleton(constant);
            }
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID)
        {
            if (SettingExistsFromSingleton(constant, communityID, 0))
            {
                return communitySettings[communityID][constant];
            }
            else
            {
                return GetSettingFromSingleton(constant);
            }
        }

        public string GetSettingFromSingleton(string constant, int communityID)
        {
            if (SettingExistsFromSingleton(constant, communityID, 0))
            {
                return communitySettings[communityID][constant];
            }
            else
            {
                return GetSettingFromSingleton(constant);
            }
        }

        public string GetSettingFromSingleton(string constant)
        {
            try
            {
                return communitySettings[0][constant];
            }
            catch (Exception e)
            {
                print(string.Format("Could not find setting key:{0}, community:{1}", constant, 0));
                throw e;
            }
        }

        public bool IsServerEnabledFromSingleton()
        {
            throw new System.NotImplementedException();
        }

        public bool SettingExistsFromSingleton(string constant, int communityId, int siteId)
        {
            bool settingExistsFromSingleton = communitySettings.ContainsKey(communityId) && communitySettings[communityId].ContainsKey(constant);
            //if (!settingExistsFromSingleton)
            //{
            //    print(string.Format("Could not find setting key:{0}, community:{1}", constant, communityId));
            //}
            return settingExistsFromSingleton;
        }

        protected void print(string s)
        {
            print(s, null);
        }

        protected void print(string s, string test)
        {
            if (!string.IsNullOrEmpty(test)) Console.WriteLine(string.Format("---------{0}---------", test));
            Console.WriteLine(s);
        }

        public List<Matchnet.Configuration.ValueObjects.ApiAppSetting> GetAPIAppSettingsFromSingleton()
        {
            throw new NotImplementedException();
        }

        public void AddNewOrUpdateSetting(string settingConstant, string globalDefaultValue, string settingDescription,
            int settingCategoryId = 2, bool isRequiredForCommunity = false, bool isRequiredForSite = false)
        {
            throw new NotImplementedException();
        }

        public void UpdateGlobalDefault(string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void SetValueForSite(int siteId, string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void ResetToGlobalDefault(string settingConstant)
        {
            throw new NotImplementedException();
        }

        public Matchnet.Configuration.ValueObjects.MembaseConfig GetMembaseConfigByBucketFromSingleton(string bucketName)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}
