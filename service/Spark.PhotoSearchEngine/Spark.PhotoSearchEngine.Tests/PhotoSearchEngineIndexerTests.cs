﻿using System.Collections;
using NUnit.Framework;
using Spark.PhotoSearchEngine.BusinessLogic;
using Spark.PhotoSearchEngine.BusinessLogic.Indexer;
using Spark.PhotoSearchEngine.ValueObjects;

namespace Spark.PhotoSearchEngine.BusinessLogic.Indexer
{
    [TestFixture]
    public class PhotoSearchEngineIndexerTests : AbstractSparkUnitTest
    {
        [TestFixtureSetUp]
        public void StartUp()
        {
            IndexerBL.Instance.USE_NUNIT = true;
            SetupMockSettingsService();
            Utils.SettingsService = _mockSettingsService;
            Spark.Logging.Utils.SettingsService = _mockSettingsService;
            IndexerBL.Instance.ClearProcessorTypesToIndex();
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            IndexerBL.Instance.USE_NUNIT = false;
            _mockSettingsService = null;
            Utils.SettingsService = null;
            Spark.Logging.Utils.SettingsService = null;
            IndexerBL.Instance.Stop();
        }

        private Hashtable GetHashConfig(int communityId, IProcessorType iProcessorType)
        {
            Hashtable h = new Hashtable();
            CommunityIndexConfig cic = IndexerBL.Instance.GetCommunityConfig(communityId, iProcessorType);
            h["config"] = cic;
            h["path"] = (!cic.EnableRollingUpdates) ? cic.IndexPath : cic.IndexPath + cic.RollingIndexDirectoryID;
            return h;
        }

        [Test]
        public void TestJdatePhotoIndex()
        {
            IndexProcessorFactory.Instance.ResetCommunity(3, IProcessorType.PhotoIndexProcessor);
            IndexerBL.Instance.AddProcessorTypeToIndex(IProcessorType.PhotoIndexProcessor);
            IndexerBL.Instance.Start();
            System.Threading.Thread.Sleep((1 * 60) * 1000);
            while (!IndexerBL.Instance.IProcessorFactory.AreAllDone(3, IProcessorType.PhotoIndexProcessor))
            {
                System.Threading.Thread.Sleep((10 * 60) * 1000);
            }
            IndexerBL.Instance.ClearProcessorTypesToIndex();
        }

        [Test]
        public void TestSparkPhotoIndex()
        {
            _mockSettingsService.RemoveSetting("PHOTO_SEARCHER_COMMUNITY_LIST");
            _mockSettingsService.AddSetting("PHOTO_SEARCHER_COMMUNITY_LIST", "1");
            IndexProcessorFactory.Instance.ResetCommunity(1, IProcessorType.PhotoIndexProcessor);
            IndexerBL.Instance.AddProcessorTypeToIndex(IProcessorType.PhotoIndexProcessor);
            IndexerBL.Instance.Start();
            System.Threading.Thread.Sleep((1 * 60) * 1000);
            while (!IndexerBL.Instance.IProcessorFactory.AreAllDone(1, IProcessorType.PhotoIndexProcessor))
            {
                System.Threading.Thread.Sleep((10 * 60) * 1000);
            }
            IndexerBL.Instance.ClearProcessorTypesToIndex();
            _mockSettingsService.RemoveSetting("PHOTO_SEARCHER_COMMUNITY_LIST");
        }

    }
}
