﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Matchnet.Exceptions;
using Spark.PhotoSearchEngine.ServiceManager;
using Spark.PhotoSearchEngine.ValueObjects;

namespace Spark.PhotoSearcher.Service
{
    public partial class PhotoSearcherService : Matchnet.RemotingServices.RemotingServiceBase
    {
        private SearcherSM _searcherSM;

        public PhotoSearcherService()
        {
            InitializeComponent();
        }

        protected override void RegisterServiceManagers()
        {
            try
            {

                _searcherSM = new SearcherSM();
                base.RegisterServiceManager(_searcherSM);

            }
            catch (Exception ex)
            {
                Trace.Write(ServiceConstants.SERVICE_SEARCHER_NAME + " - RegisterServiceManagers Exception:" + ex.ToString());
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_SEARCHER_NAME, "Error occurred when starting the Windows Service - register service managers", ex);
            }
        }


        protected override void OnStart(string[] args)
        {
            try
            {
                // System.Threading.Thread.Sleep(20000);
                base.OnStart(args);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_SEARCHER_NAME, "Error occurred when starting the Windows Service", ex);
            }
        }

        protected override void OnStop()
        {
            base.OnStop();
        }
    }
}
