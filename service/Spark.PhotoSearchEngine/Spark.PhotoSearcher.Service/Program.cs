﻿using System;
using System.ServiceProcess;
using Matchnet.Exceptions;
using Spark.PhotoSearchEngine.ValueObjects;

namespace Spark.PhotoSearcher.Service
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new PhotoSearcherService() 
			};
            ServiceBase.Run(ServicesToRun);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            new ServiceBoundaryException(ServiceConstants.SERVICE_SEARCHER_NAME, "An unhandled exception occured.", e.ExceptionObject as Exception);
        }

    }
}
