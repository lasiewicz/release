﻿using System;
using System.Diagnostics;
using Matchnet.Exceptions;
using Spark.PhotoSearchEngine.ServiceManager;
using Spark.PhotoSearchEngine.ValueObjects;
namespace Spark.PhotoSearchIndexer.Service
{
    public partial class PhotoSearchIndexerService : Matchnet.RemotingServices.RemotingServiceBase
    {
        private IndexerSM _indexerSM;

        public PhotoSearchIndexerService()
        {
            InitializeComponent();
        }

        protected override void RegisterServiceManagers()
        {
            try
            {

                _indexerSM = new IndexerSM();
                base.RegisterServiceManager(_indexerSM);

            }
            catch (Exception ex)
            {
                Trace.Write(ServiceConstants.SERVICE_INDEXER_NAME + " - RegisterServiceManagers Exception:" + ex.ToString());
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_INDEXER_NAME, "Error occurred when starting the Windows Service - register service managers", ex);
            }
        }


        protected override void OnStart(string[] args)
        {
            try
            {
                // System.Threading.Thread.Sleep(20000);
                base.OnStart(args);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_INDEXER_NAME, "Error occurred when starting the Windows Service", ex);
            }
        }

        protected override void OnStop()
        {
            base.OnStop();
        }
    }
}
