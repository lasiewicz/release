﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Matchnet.Exceptions;
using Spark.PhotoSearchEngine.ValueObjects;

namespace Spark.PhotoSearchIndexer.Service
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new PhotoSearchIndexerService() 
			};
            ServiceBase.Run(ServicesToRun);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            new ServiceBoundaryException(ServiceConstants.SERVICE_INDEXER_NAME, "An unhandled exception occured.", e.ExceptionObject as Exception);
        }
    }
}
