@ECHO ********************************
@echo *** Deploying to svcmatchmail02

psexec \\svcmatchmail02 net stop Matchnet.Matchmail.Service
pskill \\svcmatchmail02 Matchnet.Matchmail.Service.exe
robocopy C:\Matchnet\Bedrock\bin\Services\Matchnet.Matchmail.Service\ \\svcmatchmail02\c$\Matchnet\bin\Service\Matchnet.Matchmail.Service * /s /xo /r:999 /w:0
robocopy C:\Matchnet\Bedrock\Service\Matchnet.MatchMail\Matchnet.MatchMail.Harness\bin\Release\ \\svcmatchmail02\c$\Matchnet\bin\Service\Matchnet.Matchmail.Service Matchnet.MatchMail.Harness.exe /xo /r:999 /w:0

@ECHO ********************************
@echo *** Done copying. Any key to restart svc, CTRL+C to leave it off
pause

psexec \\svcmatchmail02 net start Matchnet.Matchmail.Service
pause



@ECHO ********************************
@echo *** Deploying to svcmatchmail01

psexec \\svcMatchMail01 net stop Matchnet.Matchmail.Service
pskill \\svcMatchMail01 Matchnet.Matchmail.Service.exe
robocopy C:\Matchnet\Bedrock\bin\Services\Matchnet.Matchmail.Service\ \\svcMatchMail01\c$\Matchnet\bin\Service\Matchnet.Matchmail.Service * /s /xo /r:999 /w:0
robocopy C:\Matchnet\Bedrock\Service\Matchnet.MatchMail\Matchnet.MatchMail.Harness\bin\Release\ \\svcMatchMail01\c$\Matchnet\bin\Service\Matchnet.Matchmail.Service Matchnet.MatchMail.Harness.exe /xo /r:999 /w:0

@ECHO ********************************
@echo *** Done copying. Any key to restart svc, CTRL+C to leave it off
pause

psexec \\svcMatchMail01 net start Matchnet.Matchmail.Service
pause

