﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.BulkMail.ValueObjects;
using Matchnet.BulkMail.ValueObjects.ServiceDefinitions;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Search.ValueObjects;
using Matchnet.Member.ServiceAdapters;

namespace Matchnet.BulkMail.ServiceAdapters
{
    public class BulkMailReportingSA : SABase
    {
        public static readonly BulkMailReportingSA Instance = new BulkMailReportingSA();
        
        public BulkMailBatch GetBulkMailBatchForDate(DateTime date)
		{
			string uri = "";
            BulkMailBatch batch = null;

			try
			{
                uri = getServiceManagerUri();
				base.Checkout(uri);
				try
				{
                    batch = getService(uri).GetBulkMailBatchForDate(date);
				}
				finally
				{
					base.Checkin(uri);
				}
                return batch;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot get bulk mail batch for date (uri: " + uri + ")", ex));
			}
		}

        public MatchResult RunMatchSimulation(int memberID, BulkMailQueueItem queueItem, Brand brand, ScrubList scrublist)
        {
            string uri = "";
            MatchResult result = null;

            try
            {
                uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    result = getService(uri).RunMatchSimulation(memberID, queueItem, brand, scrublist);
                }
                finally
                {
                    base.Checkin(uri);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot run match simulation for member " + memberID.ToString() + " (uri: " + uri + ")", ex));
            }
        }

        public BulkMailScheduleRecord GetBulkMailScheduleRecord(int memberID, int communityID, BulkMailType bulkMailType)
        {
            string uri = "";
            BulkMailScheduleRecord record = null;

            try
            {
                uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    record = getService(uri).GetBulkMailScheduleRecord(memberID, communityID, bulkMailType);
                }
                finally
                {
                    base.Checkin(uri);
                }
                return record;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get bulk mail record for member " + memberID.ToString() + " (uri: " + uri + ")", ex));
            }
        }
        public SearchPreferenceCollection GetLaxSearchPreference(int memberID, Brand brand, SearchPreferenceCollection strictPreferences)
        {
            string uri = "";
            SearchPreferenceCollection laxPreferences = null;

            try
            {
                uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    laxPreferences = getService(uri).GetLaxSearchPreference(memberID, brand, strictPreferences);
                }
                finally
                {
                    base.Checkin(uri);
                }
                return laxPreferences;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get bulk mail batch for date (uri: " + uri + ")", ex));
            }
        }
        
        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("BULKMAILSVC_SA_CONNECTION_LIMIT"));
        }

        private string getServiceManagerUri()
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(Matchnet.BulkMail.ValueObjects.ServiceConstants.REPORTING_SERVICE_MANAGER_NAME);
                string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("BULKMAILSVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

        private IBulkMailReporting getService(string uri)
        {
            try
            {
                return (IBulkMailReporting)Activator.GetObject(typeof(IBulkMailReporting), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }
    }
}
