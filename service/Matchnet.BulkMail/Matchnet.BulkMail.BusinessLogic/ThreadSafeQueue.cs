﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.BulkMail.ValueObjects;
using System.Threading;

namespace Matchnet.BulkMail.BusinessLogic
{
    public class ThreadSafeQueue
    {
        readonly object listLock = new object();

        Queue<BulkMailQueueItem> queue = new Queue<BulkMailQueueItem>();

        public void Enqueue(BulkMailQueueItem queueItem)
        {
            lock (listLock)
            {
                queue.Enqueue(queueItem);
            }
        }

        public void EnqueueMultiple(List<BulkMailQueueItem> queueItems)
        {
            lock (listLock)
            {
                foreach (BulkMailQueueItem queueItem in queueItems)
                {
                    queue.Enqueue(queueItem);
                }
            }
        }

        public BulkMailQueueItem Dequeue()
        {
            lock (listLock)
            {
                if (queue.Count > 0)
                {
                    return queue.Dequeue();
                }
                else
                {
                    return null;
                }
            }
        }

        public int GetCount()
        {
            lock (listLock)
            {
                return queue.Count;
            }
        }
    }
}


