﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.Configuration.ServiceAdapters;
using System.Net;
using Matchnet.HTTPMessaging.RequestTypes.MingleMailer;
using Matchnet.HTTPMessaging;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;
using System.Globalization;
using Matchnet.EmailLibrary;
using Matchnet.List.ValueObjects;
using Matchnet.List.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ServiceAdapters;
using System.Web;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeOption;

namespace Matchnet.BulkMail.BusinessLogic
{
    /// <summary>
    /// Handles 2 bulk mails; Match and New Members
    /// </summary>
    public class MingleMailerHandler : IMailHandler
    {
        #region Members

        private ITemplateMapper _mapper;
        private static MingleMailerHandler _instance;
        private static Object _syncBlock = new object();
        private AllowedDomains _allowedDomains;

        #endregion

        #region Constants

        private const Int32 EMAIL_VERIFIED = 4;
        private const int XOR_SALT_ADD_VALUE = 42;
        private string[] SEARCH_FIELDS_FOR_URL = new string[]
			{
				"SearchTypeID", "Distance", "MinAge", "MaxAge",
				"MinHeight", "MaxHeight", "EducationLevel", "Religion",
				"Ethnicity", "SmokingHabits", "DrinkingHabits", "HasPhotoFlag",
				"MajorType", "RelationshipMask", "RelationShipStatus", "RegionID",
				"SearchOrderBy"
			};

        #endregion

        #region Singleton Implementation
        /// <summary>
        /// Singleton instance
        /// </summary>
        public static MingleMailerHandler Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncBlock)
                    {
                        if (_instance == null)
                        {
                            _instance = new MingleMailerHandler();
                        }
                    }
                }
                return _instance;
            }
        }

        /// <summary>
        /// Private C'tor for Singleton
        /// </summary>
        private MingleMailerHandler()
        {
            _mapper = new MingleMailerTemplateMapper();
        }
        #endregion

        #region IMailHandler Members

        public AllowedDomains AllowedDomainsProperty
        {
            set
            {
                _allowedDomains = value;
            }
        }
        
        /// <summary>
        /// Sends a type of bulk mail
        /// </summary>
        /// <param name="impulse"></param>
        public void SendEmail(ImpulseBase impulse)
        {
            if (impulse is MatchMailImpulse)
            {
                sendMatchMail(impulse);
            }
            else if(impulse is NewMemberMailImpulse)
            {
                sendNewMemberMail(impulse);
            }
        }
                
        #endregion

        #region Mail handling for 2 bulk mail types
        
        private void sendMatchMail(ImpulseBase impulseBase)
        {
            MatchMailImpulse impulse = (MatchMailImpulse)impulseBase;

            MiniprofileInfo recipientMiniprofileInfo = impulse.RecipientMiniprofileInfo;
            MiniprofileInfoCollection members = impulse.TargetMiniprofileInfoCollection;

            Brand brand = this.GetBrand(impulse.BrandID);

            ListCountCollection listCountCollection = ListSA.Instance.GetListCounts(recipientMiniprofileInfo.MemberID, brand.Site.Community.CommunityID, brand.Site.SiteID);

            string emailAddress, siteID, memberID, username, userAge, memberLocation, finalRecipientThumbnail;
            emailAddress = MailHandlerUtils.Filter(impulse.RecipientEmailAddress);
            siteID = brand.Site.SiteID.ToString();
            memberID = impulse.RecipientMiniprofileInfo.MemberID.ToString();
            username = MailHandlerUtils.Filter(recipientMiniprofileInfo.UserName);
            userAge = EmailMemberHelper.DetermineMemberAge(recipientMiniprofileInfo.BirthDate).ToString();
            memberLocation = EmailMemberHelper.DetermineMemberRegionDisplay(recipientMiniprofileInfo.RegionID, brand);

            var member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(impulse.RecipientMiniprofileInfo.MemberID, MemberLoadFlags.None);
            var brandJoinDate = member.GetAttributeDate(brand, "BrandInsertDate");

            string recipientThumbnail = EmailMemberHelper.DetermineBaseNoPhoto(brand);

            if (recipientMiniprofileInfo.ThumbNailWebPath != Constants.NULL_STRING)
            {
                recipientThumbnail = recipientMiniprofileInfo.ThumbNailWebPath;
            }

            finalRecipientThumbnail = MailHandlerUtils.Filter(recipientThumbnail);

            #region Append HotList Tokens (You XXed others)

            string numberYouHotListed, numberYouContacted, numberYouTeased, numberYouIM, numberYouViewed;

            numberYouHotListed = listCountCollection[HotListCategory.Default].ToString();
            numberYouContacted = listCountCollection[HotListCategory.MembersYouEmailed].ToString();
            numberYouTeased = listCountCollection[HotListCategory.MembersYouTeased].ToString();
            numberYouIM = listCountCollection[HotListCategory.MembersYouIMed].ToString();
            numberYouViewed = listCountCollection[HotListCategory.MembersYouViewed].ToString();
            #endregion

            string memberSearchURL;
            memberSearchURL = MailHandlerUtils.Filter(MailHandlerUtils.DetermineSearchUrl(recipientMiniprofileInfo.MemberID,
                BrandConfigSA.Instance.GetBrandByID(impulse.BrandID).Site.Community.CommunityID));



            #region Append Reverse HotList Tokens (Who XXed you)
            string numberHotlistedYou, numberContactedYou, numberTeasedYou, numberIMYou, numberViewedYou;

            numberHotlistedYou = listCountCollection[HotListCategory.WhoAddedYouToTheirFavorites].ToString();
            numberContactedYou = listCountCollection[HotListCategory.WhoEmailedYou].ToString();
            numberTeasedYou = listCountCollection[HotListCategory.WhoTeasedYou].ToString();
            numberIMYou = listCountCollection[HotListCategory.WhoIMedYou].ToString();
            numberViewedYou = listCountCollection[HotListCategory.WhoViewedYourProfile].ToString();

            #endregion

            #region V1.3 #17638 Additional tokens for banners
            string religion, jdateReligion, ethnicity, jdateEthnicity, maritalStatus, childrenCount, custody, isPayingMember,
                finalIsVerifiedEmail, hasApprovedPhoto, gender, seekingGender, emailVerificationCode;

            religion = recipientMiniprofileInfo.ReligionID.ToString();
            jdateReligion = recipientMiniprofileInfo.JdateReligionID.ToString();
            ethnicity = recipientMiniprofileInfo.EthnicityID.ToString();
            jdateEthnicity = recipientMiniprofileInfo.JdateEthnicityID.ToString();
            maritalStatus = recipientMiniprofileInfo.MaritalStatusID.ToString();
            childrenCount = recipientMiniprofileInfo.ChildrenCount.ToString();
            custody = recipientMiniprofileInfo.CustodyID.ToString();
            isPayingMember = recipientMiniprofileInfo.IsPayingMember.ToString();

            //IsVerifiedEamil
            bool isVerifiedEmail = false;
            int globalStatusMask = recipientMiniprofileInfo.GlobalStatusMask;
            if ((globalStatusMask != Constants.NULL_INT) && ((globalStatusMask & EMAIL_VERIFIED) == EMAIL_VERIFIED))
            {
                isVerifiedEmail = true;
            }
            finalIsVerifiedEmail = isVerifiedEmail.ToString();

            hasApprovedPhoto = recipientMiniprofileInfo.HasApprovedPhotos.ToString();
            gender = MailHandlerUtils.GetGenderMaskSelf(recipientMiniprofileInfo.GenderMask).ToString();
            seekingGender = MailHandlerUtils.GetGenderMaskSeeking(recipientMiniprofileInfo.GenderMask).ToString();
            emailVerificationCode = EmailVerify.HashMemberID(recipientMiniprofileInfo.MemberID).ToString();

            #endregion

            // First create the EnvParameter object here
            EnvParameterMatchMail envParm = new EnvParameterMatchMail(getMingleMailerSiteShortName(brand),
                emailAddress, siteID, memberID, username, userAge, memberLocation, finalRecipientThumbnail, numberYouHotListed,
                numberYouContacted, numberYouTeased, numberYouIM, numberYouViewed, memberSearchURL, numberHotlistedYou, numberContactedYou,
                numberTeasedYou, numberIMYou, numberViewedYou, religion, jdateReligion, ethnicity, jdateEthnicity, maritalStatus, childrenCount,
                custody, isPayingMember, finalIsVerifiedEmail, hasApprovedPhoto, gender, seekingGender, emailVerificationCode, brandJoinDate);

            #region Add the mini profiles

            string mpMemberID, mpUsername, mpAge, mpRegionDisplay, mpThumbnail, mmvid;
            foreach (MiniprofileInfo currentMember in members)
            {
                mpMemberID = currentMember.MemberID.ToString();
                mpUsername = MailHandlerUtils.Filter(currentMember.UserName);

                // Age
                int age = EmailMemberHelper.DetermineMemberAge(currentMember.BirthDate);
                mpAge = age.ToString();

                mpRegionDisplay = MailHandlerUtils.Filter(EmailMemberHelper.DetermineMemberRegionDisplay(currentMember.RegionID, brand));

                // Thumbnail
                string tempThumbnail = EmailMemberHelper.DetermineBaseNoPhoto(brand);
                if (currentMember.ThumbNailWebPath != Constants.NULL_STRING)
                {
                    tempThumbnail = currentMember.ThumbNailWebPath;
                }
                mpThumbnail = MailHandlerUtils.Filter(tempThumbnail);

                // Append View Profile MMVID (Enables hot listing w/o logging in)
                mmvid = MailHandlerUtils.GetMMVID(recipientMiniprofileInfo.MemberID, currentMember.MemberID).ToString();

                MatchMiniProfile miniProfile = new MatchMiniProfile(mpMemberID, mpThumbnail, mpUsername, mpAge, mpRegionDisplay, mmvid);

                if (Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MATCHMAIL_INCLUDE_MORE_DATA")))
                {
                    miniProfile.matchtext = MailHandlerUtils.GetMatchText(recipientMiniprofileInfo, currentMember, brand);
                }

                envParm.AddMiniProfile(miniProfile);
            }

            #endregion

            sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.MatchNewsLetter),
                impulse.RecipientEmailAddress, envParm, brand);
        }

        private void sendNewMemberMail(ImpulseBase impulseBase)
        {
            NewMemberMailImpulse impulse = (NewMemberMailImpulse)impulseBase;

            MiniprofileInfo recipientMiniprofileInfo = impulse.RecipientMiniprofileInfo;
            MiniprofileInfoCollection members = impulse.TargetMiniprofileInfoCollection;

            Brand brand = this.GetBrand(impulse.BrandID);

            string emailAddress, nmiaTimestamp;
            emailAddress = MailHandlerUtils.Filter(impulse.RecipientEmailAddress);
            nmiaTimestamp = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss", DateTimeFormatInfo.InvariantInfo);
            var member = MemberSA.Instance.GetMember(impulse.RecipientMiniprofileInfo.MemberID, MemberLoadFlags.None);
            var brandJoinDate = member.GetAttributeDate(brand, "BrandInsertDate");

            EnvParameterNewMember envParm = new EnvParameterNewMember(getMingleMailerSiteShortName(brand),
                emailAddress, nmiaTimestamp, recipientMiniprofileInfo.MemberID.ToString(), impulse.RecipientEmailAddress, brandJoinDate);

            foreach (MiniprofileInfo currentMember in impulse.TargetMiniprofileInfoCollection)
            {
                string regionDisplay = EmailMemberHelper.DetermineMemberRegionDisplay(currentMember.RegionID, brand);
                string userName = currentMember.UserName;
                string age = MailHandlerUtils.GetAge(currentMember.BirthDate).ToString();
                string imageURL = currentMember.ThumbNailWebPath;
                string memberID = currentMember.MemberID.ToString();

                NewMemberMiniProfile miniProfile = new NewMemberMiniProfile(memberID, imageURL, userName, age, regionDisplay);

                envParm.AddMiniProfile(miniProfile);
            }

            sendMingleMail(_mapper.GetProviderTemplateID(brand.Site, EmailType.NewMembersNewsLetter),
                impulse.RecipientEmailAddress, envParm, brand);
        }

        private void sendMingleMail(string templateName, string emailAddress, EnvParameterBase envParameter, Brand brand)
        {
            string respResult = "";
            string mingleMailerPostUrl = RuntimeSettings.GetSetting("MINGLE_MAILER_POST_URL");
            string siteKey = RuntimeSettings.GetSetting("MINGLE_MAILER_SITE_KEY", brand.Site.Community.CommunityID, brand.Site.SiteID);

            SendMailRequest mailReq = new SendMailRequest(templateName, envParameter, emailAddress, siteKey);
            HttpStatusCode statusCode;
            try
            {
                respResult = SMSProxy.SendPost(mailReq.GetPostData(), mingleMailerPostUrl, 60000, "application/x-www-form-urlencoded", out statusCode);

                if ((respResult.ToLower().IndexOf("\"error\"") > -1) || (respResult.ToLower().IndexOf("\"failure\"") > -1))
                {
                    throw new Exception("Error returned from Mingle spammer. Response:" + respResult);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error sending a mail request to Mingle Mailer. [EmailAddress: " + emailAddress + ", TemplateName: " +
                    templateName + ", BrandID: " + brand.BrandID.ToString() + ", Response:" + respResult +
                    ", EnvParameter JSON string (First 1000 characters): " +
                    envParameter.GetJsonString().Substring(0, 1000) + "]", ex);
            }
        }

        #endregion

        #region Helper methods ( this should be moved to a utility class later)

        private string getMingleMailerSiteShortName(Brand brand)
        {
            return RuntimeSettings.GetSetting("MINGLE_MAILER_SITE_SHORTNAME", brand.Site.Community.CommunityID, brand.Site.SiteID);
        }

        private Brand GetBrand(int brandID)
        {
            return Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);
        }
        
        #endregion
    }
}
