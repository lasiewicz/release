﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ValueObjects.Impulse;

namespace Matchnet.BulkMail.BusinessLogic
{
    /// <summary>
    /// Interface for all template mappers
    /// </summary>
    public interface ITemplateMapper
    {
        string GetProviderTemplateID(Site site, EmailType emailType);
    }
}
