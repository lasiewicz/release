using System;
using Matchnet.BulkMail.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeOption;
using System.Text;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Member.ValueObjects.MemberDTO;
using Matchnet.Search.ValueObjects;
using System.Web;
using Matchnet.Search.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.BulkMail.BusinessLogic
{
    public class BulkMailUtils
    {
        private static ISettingsSA _settingsService;

        public static ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService) _settingsService = RuntimeSettings.Instance;
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        private static readonly BulkMailUtils _instance = new BulkMailUtils();

        public static BulkMailUtils Instance
        {
            get { return _instance; }
        }

        private BulkMailUtils(){}

        public int GetNumberOfItemsToSendForFrequency(byte frequency)
        {
            // once weekly or daily frequency, send 2 members
            if (frequency == ServiceConstants.DAILY_FREQUENCY || frequency == ServiceConstants.ONCE_WEEKLY_FREQUENCY)
            {
                return 2;
            }
            else //all other frequencies (i.e. twice weekly) return 6 members
            {
                return 6;
            }
        }

        public bool ShouldRunSearchPrefType(Matchnet.BulkMail.ValueObjects.Enumerations.SearchPrefType searchPrefType, Brand brand)
        {
            bool allowed = true;
            string disabledSearchPrefTypes = SettingsService.GetSettingFromSingleton("MATCHMAIL_DISABLED_SEARCHPREFTYPES", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            if (!string.IsNullOrEmpty(disabledSearchPrefTypes))
            {
                string[] searchPrefTypeList = disabledSearchPrefTypes.Split(',');
                foreach (string prefType in searchPrefTypeList)
                {
                    if (!string.IsNullOrEmpty(prefType))
                    {
                        if (int.Parse(prefType) == (int)searchPrefType)
                        {
                            allowed = false;
                            break;
                        }
                    }
                }
            }

            return allowed;
        }
    }

	public class AgeUtils 
	{
		public static int [] GetAgeRange(DateTime birthDate, int ageMin, int ageMax)
		{
			if(ageMin == Constants.NULL_INT || ageMax == Constants.NULL_INT) 
			{
				int age = (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
				if (age >= Constants.MIN_AGE_RANGE && age <= 24) 
				{
					ageMin = Constants.MIN_AGE_RANGE;
					ageMax = 24;
				} 
				else if (age >= 25 && age <= 34) 
				{
					ageMin = 25;
					ageMax = 34;
				}
				else if (age >= 35 && age <= 44) 
				{
					ageMin = 35;
					ageMax = 44;
				}
				else if (age >= 45 && age <= 54) 
				{
					ageMin = 45;
					ageMax = 54;
				}
                else if (age >= 55 && age <= 64)
                {
                    ageMin = 55;
                    ageMax = 64;
                }
                else if (age >= 65 && age <= 74)
                {
                    ageMin = 65;
                    ageMax = 74;
                }
				else if (age >= 75 && age <= Constants.MAX_AGE_RANGE) 
				{
					ageMin = 75;
					ageMax = Constants.MAX_AGE_RANGE;
				}
				else 
				{
					ageMin = 25;
					ageMax = 34;
				}
			}
			return new int [] {ageMin, ageMax};
		}

		public static int GetAge(DateTime birthDate)
		{
			return (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
		}
	}

    /// <summary>
    /// Utility type methods for mail handlers to use
    /// </summary>
    public class MailHandlerUtils
    {
        public const int XOR_SALT_ADD_VALUE = 42;

        private static ISettingsSA _settingsService;

        public static ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService) _settingsService = RuntimeSettings.Instance;
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        public static string[] SEARCH_FIELDS_FOR_URL = new string[]
			{
				"SearchTypeID", "Distance", "MinAge", "MaxAge",
				"MinHeight", "MaxHeight", "EducationLevel", "Religion",
				"Ethnicity", "SmokingHabits", "DrinkingHabits", "HasPhotoFlag",
				"MajorType", "RelationshipMask", "RelationShipStatus", "RegionID",
				"SearchOrderBy"
			};


        public static int GetAge(DateTime birthDate)
        {
            return (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
        }

        public static string GetMaskContent(string attributeName, int maskValue, Brand brand)
        {
            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);

            if (attributeOptionCollection == null)
                return string.Empty;

            StringBuilder sb = new StringBuilder();

            foreach (AttributeOption attributeOption in attributeOptionCollection)
            {
                if (attributeOption.MaskContains(maskValue))
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(", ");
                    }
                    sb.Append(attributeOption.Description);
                }
            }

            return sb.ToString();
        }

        public static string GetMatchText(MiniprofileInfo recipientProfile, MiniprofileInfo matchProfile, Brand brand)
        {
            int match = 0;
            StringBuilder text = new StringBuilder();
            string matchString = string.Empty;
            string returnValue = string.Empty;

            AppendMatchString(text, recipientProfile.EntertainmentLocation, matchProfile.EntertainmentLocation, "EntertainmentLocation", brand);
            AppendMatchString(text, recipientProfile.LeisureActivity, matchProfile.LeisureActivity, "LeisureActivity", brand);
            AppendMatchString(text, recipientProfile.Cuisine, matchProfile.Cuisine, "Cuisine", brand);
            AppendMatchString(text, recipientProfile.Music, matchProfile.Music, "Music", brand);
            AppendMatchString(text, recipientProfile.Reading, matchProfile.Reading, "Reading", brand);
            AppendMatchString(text, recipientProfile.PhysicalActivity, matchProfile.PhysicalActivity, "PhysicalActivity", brand);
            AppendMatchString(text, recipientProfile.Pets, matchProfile.Pets, "Pets", brand);

            if (text.Length == 0)
            {
                if (matchProfile.AboutMe.Length > 197)
                {
                    text.Append(matchProfile.AboutMe.Substring(0, 197));
                    text.Append("...");
                    returnValue = text.ToString();
                }
                else
                {
                    text.Append(matchProfile.AboutMe);
                    returnValue = text.ToString();
                }
            }
            else
            {
                returnValue = "You both like: " + text.ToString();
                if (returnValue.Length > 255)
                {
                    returnValue = returnValue.Substring(0, 252) + "...";
                }
            }

            return returnValue;
        }

        public static void AppendMatchString(StringBuilder sb, int attrValue, int attrValue2, string attrName, Brand brand)
        {
            if (attrValue > 0 && attrValue2 > 0)
            {
                string matchString = MailHandlerUtils.GetMaskContent(attrName, attrValue & attrValue2, brand);
                if (matchString.Length > 0)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(", " + matchString);
                    }
                    else
                    {
                        sb.Append(matchString);
                    }
                }
            }
        }

        public static int GetMMVID(int recipientMemberID, int memberID)
        {
            return (recipientMemberID + XOR_SALT_ADD_VALUE) ^ (memberID);
        }

        public static string DetermineSearchUrl(Int32 memberID, Int32 communityID)
        {
            StringBuilder sb = new StringBuilder();
            SearchPreferenceCollection prefs = SearchPreferencesSA.Instance.GetSearchPreferences(memberID, communityID);

            sb.Append("/Applications/Search/SearchResults.aspx?");

            for (int i = 0; i < SEARCH_FIELDS_FOR_URL.Length; i++)
            {
                if (i > 0)
                {
                    sb.Append("&");
                }
                sb.Append(HttpUtility.UrlEncode(SEARCH_FIELDS_FOR_URL[i]));
                sb.Append("=");
                sb.Append(HttpUtility.UrlEncode(prefs[SEARCH_FIELDS_FOR_URL[i]]));
            }

            //	Make the correct gender mask fields
            int genderMask = Matchnet.GenderUtils.FlipMaskIfHeterosexual(Convert.ToInt32(prefs["GenderMask"]));

            sb.Append("&GenderID=");
            sb.Append(GetGenderMaskSelf(genderMask));
            sb.Append("&SeekingGenderID=");
            sb.Append(GetGenderMaskSeeking(genderMask));

            return (sb.ToString());
        }

        public static int GetGenderMaskSelf(int genderMask)
        {
            int maskSelf = (Int32)GenderMask.Male + (Int32)GenderMask.Female + (Int32)GenderMask.MTF + (Int32)GenderMask.FTM;
            return genderMask & maskSelf;
        }

        public static int GetGenderMaskSeeking(int genderMask)
        {
            int maskSeeking = (Int32)GenderMask.SeekingMale + (Int32)GenderMask.SeekingFemale + (Int32)GenderMask.SeekingMTF + (Int32)GenderMask.SeekingFTM;
            return genderMask & maskSeeking;
        }

        public static string Filter(object theObject)
        {
            string retVal = string.Empty;
            if (theObject != null)
            {
                retVal = theObject.ToString();
                retVal = retVal.Replace("\n", string.Empty);
                retVal = retVal.Replace("\r", string.Empty);
                retVal = retVal.Replace("'", "\\'");
                retVal = retVal.Replace("\"", "\\\"");
                retVal = retVal.Replace("|", string.Empty);
            }

            return (retVal);
        }

        public static SearchPreferenceCollection GetLaxSearchPreference(IMemberDTO member, Brand brand, SearchPreferenceCollection strictPreferences)
        {
            SearchPreferenceCollection laxPrefs = new SearchPreferenceCollection();
            DateTime birthDate = member.GetAttributeDate(brand.Site.Community.CommunityID, 0, 0, "Birthdate", DateTime.MinValue);

            if (birthDate != DateTime.MinValue)
            {
                int[] agerange = AgeUtils.GetAgeRange(birthDate, Constants.NULL_INT, Constants.NULL_INT);
                laxPrefs.Add("MinAge", agerange[0].ToString());
                laxPrefs.Add("MaxAge", agerange[1].ToString());
                laxPrefs.Add("HasPhotoFlag", "1");
                laxPrefs.Add("GenderMask", strictPreferences.Value("GenderMask"));
                laxPrefs.Add("domainid", strictPreferences.Value("domainid"));


                bool retainReligion = Convert.ToBoolean(SettingsService.GetSettingFromSingleton("MATCHMAIL_LAX_PREFERENCES_RETAIN_JDATERELIGION", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));

                if(retainReligion && strictPreferences.ContainsKey("jdatereligion"))
                {
                    laxPrefs.Add("jdatereligion", strictPreferences["jdatereligion"]);
                }

                SearchTypeID searchType = (SearchTypeID)Conversion.CInt(strictPreferences["SearchTypeID"]);

                if (searchType == SearchTypeID.AreaCode)
                {
                    laxPrefs.Add("SearchTypeID", SearchTypeID.AreaCode.ToString("d"));
                    laxPrefs.Add("SearchOrderBy", strictPreferences.Value("SearchOrderBy"));

                    if (strictPreferences.ContainsKey("areacode1"))
                    {
                        laxPrefs.Add("areacode1", strictPreferences["areacode1"]);
                    }
                    if (strictPreferences.ContainsKey("areacode2"))
                    {
                        laxPrefs.Add("areacode2", strictPreferences["areacode2"]);
                    }
                    if (strictPreferences.ContainsKey("areacode3"))
                    {
                        laxPrefs.Add("areacode3", strictPreferences["areacode3"]);
                    }
                    if (strictPreferences.ContainsKey("areacode4"))
                    {
                        laxPrefs.Add("areacode4", strictPreferences["areacode4"]);
                    }
                    if (strictPreferences.ContainsKey("areacode5"))
                    {
                        laxPrefs.Add("areacode5", strictPreferences["areacode5"]);
                    }
                    if (strictPreferences.ContainsKey("areacode6"))
                    {
                        laxPrefs.Add("areacode6", strictPreferences["areacode6"]);
                    }
                }
                else
                {
                    laxPrefs.Add("RegionID", strictPreferences.Value("RegionID"));
                    laxPrefs.Add("SearchTypeID", SearchTypeID.Region.ToString("d")); 
                    laxPrefs.Add("SearchOrderBy", Matchnet.Search.Interfaces.QuerySorting.Proximity.ToString("d")); // Proximity - will expand to 160 miles or whole country(!)

                    int settingDistance = Convert.ToInt32(SettingsService.GetSettingFromSingleton("MATCHMAIL_LAX_SEARCH_RADIUS", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
                    int memberDistance = 0;
                    int existingDistance = 0;

                    if (strictPreferences.ContainsKey("Distance"))
                    {
                        existingDistance = Convert.ToInt32(strictPreferences.Value("Distance"));
                        memberDistance = existingDistance > settingDistance ? existingDistance : settingDistance;
                    }
                    else if (strictPreferences.ContainsKey("Radius"))
                    {
                        existingDistance = Convert.ToInt32(strictPreferences.Value("Radius"));
                        memberDistance = existingDistance > settingDistance ? existingDistance : settingDistance;
                    }
                    else
                    {
                        memberDistance = settingDistance;
                    }

                    laxPrefs.Add("Radius", memberDistance.ToString());
                }

            }
            return laxPrefs;
        }

        public static IMemberDTO GetIMemberDTO(int memberid, MemberLoadFlags memberLoadFlags)
        {
            return GetIMemberDTO(MemberSA.Instance, memberid, memberLoadFlags);
        }

        public static IMemberDTO GetIMemberDTO(IGetMember memberService, int memberid, MemberLoadFlags memberLoadFlags)
        {
            string settingFromSingleton = SettingsService.GetSettingFromSingleton("ENABLE_MEMBER_DTO");
            bool isEnabled = (null != settingFromSingleton && Boolean.TrueString.ToLower().Equals(settingFromSingleton.ToLower()));
            if (isEnabled)
            {
                return memberService.GetMemberDTO(memberid, MemberType.MatchMail);
            }
            else
            {
                return memberService.GetMember(memberid, memberLoadFlags);
            }
        }
    }
    
}

