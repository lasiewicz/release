using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Threading;
using System.Text;
using System.Web;
using System.Linq;

using Matchnet.Data;
using Matchnet.Exceptions;
using Matchnet.BulkMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Search.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.Security;
using Matchnet.List.ServiceAdapters;
using Matchnet.EmailLibrary;
using Matchnet.ActivityRecording.ServiceAdapters;
using Spark.CloudStorage;
using Spark.Logging;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Search.ValueObjects.ServiceDefinitions;
using Client = Matchnet.Data.Client;

namespace Matchnet.BulkMail.BusinessLogic
{
    public class BulkMailBL
    {
        private bool _isDebug = false;

        public bool IsDebug
        {
            get { return _isDebug; }
            set { _isDebug = true; }
        }

        #region Private variables
        private const int MAX_SCRUBLIST_ITEMS = 100;
        private const int PAGE_SIZE = 106; // This is how many single query hit resturns from Search MT.
        private const string HYDRA_SAVE_NAME = "mnAlertSaveMM";
        public const string CLASS_NAME = "BulkMailBL";
        private const string SETTING_USE_LARGE_IMAGES = "MM_USE_LARGE_IMAGES";
        private const string SETTING_LARGE_PHOTO_URL = "MM_LARGE_IMAGES_URL";
        private const string SETTING_NEW_MEMBER_EMAIL_AGE_FACTOR = "NEW_MEMBER_EMAIL_AGE_FACTOR";
        private const string SETTING_NEW_MEMBER_EMAIL_DISTANCE = "NEW_MEMBER_EMAIL_DISTANCE";
        private const string SETTING_NEW_MEMBER_EMAIL_NUMBER_OF_MATCHES = "NEW_MEMBER_EMAIL_NUMBER_OF_MATCHES";
        private const string SETTING_NEW_MEMBER_EMAIL_SIGNUP_FACTOR = "NEW_MEMBER_EMAIL_SIGNUP_FACTOR";
        private const string SETTING_REGISTERED_DAYS_CRITERIA = "BULKMAILSVC_REGISTERED_DAYS_CRITERIA";
        private const Int32 EMAIL_VERIFIED = 4;
        private const int XOR_SALT_ADD_VALUE = 42;
        private const string DELIMITER = @"|";
        private const string LINEDELIMITER = "^";

        private int _cfgPollingInterval;
        private long _cfgQPSLimit; //matches per sec threshold
        private string _cfgBulkMailQueuePath;
        private int _cfgNewMemberEmailSignUpFactor;
        private bool _cfgUseExternalMail;
        private int _cfgThreadCount;
        private int _cfgRegisteredDaysCriteria;
        private int _cfgMaxDaysSinceLastLogin;
        private int _cfgQueueFloor;
        private float _fQPSLimit;
        private float _fThreadCount;
        private bool _runnable = false;
        private string _processingServer;
        private Thread _threadGetNext;
        private Thread[] _threadQueue;
        private AllowedDomains _allowedDomains;
        private ThreadSafeQueue _queue;

        private static readonly string[] _AreaCodeNames = { "areacode1", "areacode2", "areacode3", "areacode4", "areacode5", "areacode6" };
        private static readonly string[] _SearchPreferenceColumnNameList = new string[] { "searchorderby", "searchtypeid", "gendermask", "minage", "maxage", "regionid", "schoolid", "areacode1", "areacode2", "areacode3", "areacode4", "areacode5", "areacode6", "distance", "hasphotoflag", "educationlevel", "religion", "languagemask", "ethnicity", "smokinghabits", "drinkinghabits", "minheight", "maxheight", "maritalstatus", "jdatereligion", "jdateethnicity", "synagogueattendance", "keepkosher", "sexualidentitytype", "relationshipmask", "relationshipstatus", "bodytype", "zodiac", "majortype", "countryregionid" };
        private IGetMember _memberService = null;
        private IMemberSearchSA _memberSearchService = null;
        private ISettingsSA _settingsService = null;

        #region PerfCounters
        // perf counters
        private PerformanceCounter _perfMatchRate;
        private PerformanceCounter _perfHaveStoredSearchPrefTotal;
        private PerformanceCounter _perfNoSearchPrefTotal;
        private PerformanceCounter _perfItemsProcessedTotal;
        private PerformanceCounter _perfQuery1Total;
        private PerformanceCounter _perfQuery2Total;
        private PerformanceCounter _perfQuery3Total;
        private PerformanceCounter _perfQuery1NoResults;
        private PerformanceCounter _perfQuery2NoResults;
        private PerformanceCounter _perfQuery3NoResults;
        private PerformanceCounter _perfTotalSent;
        private PerformanceCounter _perfTotalSentRate;
        private PerformanceCounter _perfTotalRegulated;
        private PerformanceCounter _perfTotalRegulatedRate;
        private PerformanceCounter _perfAvgSearchTime;
        private PerformanceCounter _perfAvgSearchTimeBase;
        private PerformanceCounter _perfAvgScrubTime;
        private PerformanceCounter _perfAvgScrubTimeBase;
        private PerformanceCounter _perfAvgGetQueueItemTime;
        private PerformanceCounter _perfAvgGetQueueItemTimeBase;
        private PerformanceCounter _perfAvgPopulateResultsTime;
        private PerformanceCounter _perfAvgPopulateResultsTimeBase;
        private PerformanceCounter _perfAvgSendToXmailTime;
        private PerformanceCounter _perfAvgSendToXmailTimeBase;
        private PerformanceCounter _perfAvgSavemnAlertTime;
        private PerformanceCounter _perfAvgSavemnAlertTimeBase;
        private PerformanceCounter _perfAvgGetMemberTime;
        private PerformanceCounter _perfAvgGetMemberTimeBase;
        private PerformanceCounter _perfAvgBuildDefaultPrefTime;
        private PerformanceCounter _perfAvgBuildDefaultPrefTimeBase;
        private PerformanceCounter _perfAvgAdjustScrublistTime;
        private PerformanceCounter _perfAvgAdjustScrublistTimeBase;
        private PerformanceCounter _perfAvgSlowdownTime;
        private PerformanceCounter _perfAvgSlowdownTimeBase;
        private PerformanceCounter _perfAvgCycleTime;
        private PerformanceCounter _perfAvgCycleTimeBase;
        #endregion

        public IGetMember MemberService
        {
            get
            {
                if (null == _memberService) return Matchnet.Member.ServiceAdapters.MemberSA.Instance;
                return _memberService;
            }
            set { _memberService = value; }
        }

        public IMemberSearchSA MemberSearchService
        {
            get
            {
                if(null == _memberSearchService) return Matchnet.Search.ServiceAdapters.MemberSearchSA.Instance;
                return _memberSearchService;
            }
            set { _memberSearchService = value; }
        }

        public ISettingsSA SettingsService
        {
            get
            {
                if (null == _settingsService) return Matchnet.Configuration.ServiceAdapters.RuntimeSettings.Instance;
                return _settingsService;
            }
            set { _settingsService = value; }
        }

        private bool IsE2ProfileBlockEnabled(int communityId)
        {
            bool isE2ProfileBlockingEnabled=false;
            try
            {
                isE2ProfileBlockingEnabled = Convert.ToBoolean(RuntimeSettings.GetSetting("E2_PROFILE_BLOCK_ENABLED", communityId));
            }
            catch (Exception ignore)
            {
                isE2ProfileBlockingEnabled = false;
            }
            return isE2ProfileBlockingEnabled;
        }

        #endregion

        #region service control
        public void Start()
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledExceptionHandler);

            traceOut(AppDomain.CurrentDomain.FriendlyName);

            Initialize();

            _queue = new ThreadSafeQueue();
            _processingServer = System.Environment.MachineName.ToLower();

            // cache floats rather than convert on the fly for slowdown calculations
            _fQPSLimit = Convert.ToSingle(_cfgQPSLimit);
            _fThreadCount = Convert.ToSingle(_cfgThreadCount);

            Inform(string.Format("Config:\nMM Q:{0}\nMM Threads:{1}\nQPS Limit:{2}\nPolling Interval:{3} \nQ PATH raw: {4}",
                _cfgBulkMailQueuePath,
                _cfgThreadCount,
                _cfgQPSLimit,
                _cfgPollingInterval,
                SettingsService.GetSettingFromSingleton("BULKMAILSVC_QUEUE_PATH"))
                );

            _runnable = true;

            _threadGetNext = new Thread(new ThreadStart(RunQueueLoad));
            _threadGetNext.Name = "Next Matches Cycle Thread";
            _threadGetNext.Start();
            Inform("Started next matches cycle thread: " + _threadGetNext.Name);

            _threadQueue = new Thread[_cfgThreadCount];
            for (int threadNum = 0; threadNum < _cfgThreadCount; threadNum++)
            {
                Thread t = new Thread(new ThreadStart(processCycle));
                t.Name = "Process Cycle Thread " + threadNum;
                t.Start();
                _threadQueue[threadNum] = t;
            }
            Inform("Started " + _cfgThreadCount + " processing cycle threads.");

        }

        public void Initialize()
        {
            _cfgThreadCount = Convert.ToInt32(SettingsService.GetSettingFromSingleton("BULKMAILSVC_THREAD_COUNT"));
            _cfgPollingInterval = Convert.ToInt32(SettingsService.GetSettingFromSingleton("BULKMAILSVC_POLL_INTERVAL"));
            _cfgMaxDaysSinceLastLogin = Convert.ToInt32(SettingsService.GetSettingFromSingleton("BULKMAILSVC_LAST_LOGON_THRESHOLD"));
            _cfgNewMemberEmailSignUpFactor = Convert.ToInt32(SettingsService.GetSettingFromSingleton(SETTING_NEW_MEMBER_EMAIL_SIGNUP_FACTOR));
            _cfgUseExternalMail = Convert.ToBoolean(SettingsService.GetSettingFromSingleton("BULKMAILSVC_USE_EXTERNAL_MAIL"));
            _cfgRegisteredDaysCriteria = Convert.ToInt32(SettingsService.GetSettingFromSingleton(SETTING_REGISTERED_DAYS_CRITERIA));
            _cfgQueueFloor = Convert.ToInt32(SettingsService.GetSettingFromSingleton("BULKMAILSVC_QUEUE_FLOOR"));
            _cfgQPSLimit = Convert.ToInt64(SettingsService.GetSettingFromSingleton("BULKMAILSVC_QPS_THRESHOLD"));
            _allowedDomains = new AllowedDomains(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME);
            initPerfCounters();
        }

        public void Stop()
        {
            _runnable = false;

            if (_threadGetNext != null)
            {
                _threadGetNext.Join(20000);
            }

            for (int threadNum = 0; threadNum < _threadQueue.Length; threadNum++)
            {
                _threadQueue[threadNum].Join(10000);
            }
        }

        public void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs args)
        {
            new ServiceBoundaryException(BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Unhandled Exception.", (Exception)args.ExceptionObject, false);
        }

        #endregion service control

        #region process queue items

        private void processCycle()
        {
            int matchesFound = 0;
            int numToPad = 0;
            bool successfulProcessing;
            ScrubList scrubList;
            PerformanceCounterHelper perfHelper = new PerformanceCounterHelper();
            PerformanceCounterHelper perfHelperCycle = new PerformanceCounterHelper();
            ExternalMail.ValueObjects.DoNotEmail.DoNotEmailEntry dneEntry;
            
            List<int> communities = (from int i in BrandConfigSA.Instance.GetCommunities().Keys select i).ToList();
            int dequeued = 0;

            while (_runnable)
            {
                matchesFound = 0;
                numToPad = 0;
                successfulProcessing = true;
                
                perfHelperCycle.StartTiming();

                BulkMailQueueItem queueItem = null;
                IMemberDTO member = null;
                int brandID = Constants.NULL_INT;
                Brand brand = null;
                SearchPreferenceCollection searchPrefs = null;
                bool HasStoredSearchPrefs = true;

                try
                {
                    #region Get Queue Item

                    try
                    {
                        perfHelper.StartTiming();
                        queueItem = _queue.Dequeue();
                        dequeued++;

                        if (queueItem == null)
                        {
                            Thread.Sleep(10000);
                            continue;
                        }

                        traceOut("Dequeued", queueItem.MemberID);


                        if (!communities.Contains(queueItem.CommunityID))
                        {
                            continue;
                        }
                        perfHelper.EndTiming(_perfAvgGetQueueItemTime, _perfAvgGetQueueItemTimeBase);
                        _perfItemsProcessedTotal.Increment();
                    }
                    catch (Exception ex)
                    {
                        new ServiceBoundaryException(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME,
                            "getBulkMailQueueItem failed getting item from queue", ex);
                        Thread.Sleep(10000);
                        continue;
                    }

                    #endregion

                    // get member
                    try
                    {
                        perfHelper.StartTiming();
                        member = MailHandlerUtils.GetIMemberDTO(MemberService, queueItem.MemberID, MemberLoadFlags.None);
                        member.GetLastLogonDate(queueItem.CommunityID, out brandID);
                        brand = BrandConfigSA.Instance.GetBrandByID(brandID);
                        perfHelper.EndTiming(_perfAvgGetMemberTime, _perfAvgGetMemberTimeBase);
                    }
                    catch (Exception ex)
                    {
                        new ServiceBoundaryException(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "GetMember() failed", ex);
                        traceOut("Error getting member", queueItem.MemberID);
                        saveBulkMailStatusToDBNoUpdate(queueItem, ProcessingStatusID.ProcessedErrorGettingMember);
                        Thread.Sleep(10000);
                        continue;
                    }

                    if (member.EmailAddress == null || member.EmailAddress == string.Empty)
                    {
                        new ServiceBoundaryException(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "MemberID " + member.MemberID.ToString() + " has no email address?!");
                        traceOut("No email address", queueItem.MemberID);
                        saveBulkMailStatusToDBNoUpdate(queueItem, ProcessingStatusID.ProcessedErrorNoEmailAddress);
                        continue;
                    }

                    if (brand == null)
                    {
                        new ServiceBoundaryException(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "MemberID " + member.MemberID.ToString() + " no brand last logon?!");
                        traceOut("No brand", queueItem.MemberID);
                        saveBulkMailStatusToDBNoUpdate(queueItem, ProcessingStatusID.ProcessedErrorNoBrand);
                        continue;
                    }

                    if (!_cfgUseExternalMail)
                    {
                        //check the DNE if we're not using External Mail which does that for us
                        dneEntry = ExternalMail.ServiceAdapters.DoNotEmailSA.Instance.GetEntryByEmailAddress(member.EmailAddress);
                        if (dneEntry != null)
                        {
                            traceOut("On DNE", queueItem.MemberID);
                            saveBulkMailStatusToDBNoUpdate(queueItem, ProcessingStatusID.ProcessedOnDNE);
                            continue;
                        }
                    }

                    try
                    {
                        scrubList = new ScrubList(queueItem.SentMemberIDList);
                        ProcessingStatusID processingStatusID = ProcessingStatusID.Processed;

                        switch (queueItem.BulkMailTypeID)
                        {
                            case BulkMailType.MatchMail:

                                MatchResult matchResult = ProcessMatchMailQueueItem(perfHelper, queueItem, member, brand, scrubList, MemberService, MemberSearchService);
                                successfulProcessing = matchResult.MatchSuccessful;
                                matchesFound = matchResult.TotalMatchesFound;
                                processingStatusID = matchResult.ProcessingStatus;
                                break;
                            case BulkMailType.NewMembers:
                                successfulProcessing = processNewMemberQueueItem(perfHelper, queueItem, member, brand, scrubList, out matchesFound, MemberSearchService, MemberService);
                                if(successfulProcessing) 
                                {
                                    processingStatusID = ProcessingStatusID.Processed;
                                }
                                else 
                                {
                                    processingStatusID = ProcessingStatusID.ProcessedNoUpdate;
                                }
                                break;
                        }

                        perfHelper.StartTiming();
                        saveBulkMailStatusToDB(member,
                            brand,
                            queueItem,
                            scrubList.ToString(),
                            successfulProcessing,
                            matchesFound, processingStatusID);
                        perfHelper.EndTiming(_perfAvgSavemnAlertTime, _perfAvgSavemnAlertTimeBase);
                        _perfMatchRate.Increment();

                        if (!successfulProcessing)
                        {
                            traceOut("No Update", queueItem.MemberID);
                            saveBulkMailStatusToDBNoUpdate(queueItem, ProcessingStatusID.ProcessedNoUpdate);
                            continue;
                        }

                    }
                    catch (Exception ex)
                    {
                        traceOut("GeneralError", queueItem.MemberID);
                        logException("General Error", ex);
                        saveBulkMailStatusToDBNoUpdate(queueItem, ProcessingStatusID.ProcessedErrorGeneral);
                        continue;
                    }

                }
                catch (Exception ex)
                {
                    traceOut("GeneralError 2", queueItem.MemberID);
                    logException("General Error 2", ex);
                    saveBulkMailStatusToDBNoUpdate(queueItem, ProcessingStatusID.ProcessedErrorGeneral);
                    new ServiceBoundaryException(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME,
                        "Error processing match mail item. [CommunityID:" + queueItem.CommunityID + ", MemberID: " + queueItem.MemberID + "]", ex);
                    Thread.Sleep(10000);
                }

                slowDown();
                perfHelperCycle.EndTiming(_perfAvgCycleTime, _perfAvgCycleTimeBase);
            }
        }

        public MatchResult ProcessMatchMailQueueItem(PerformanceCounterHelper perfHelper, BulkMailQueueItem queueItem, IMemberDTO member, Brand brand, ScrubList scrubList, IGetMember memberService, IMemberSearchSA searchService)
        {
            MatchResult matchResult = new MatchResult();

            matchResult.MemberID = member.MemberID;
            matchResult.Brand = brand;
            matchResult.Scrublist = scrubList;
            Matchnet.BulkMail.ValueObjects.Enumerations.SearchPrefType searchPrefType = Enumerations.SearchPrefType.StrictNoAge;

            try
            {

                int numMatchesToSendToYesMail = Conversion.CInt(SettingsService.GetSettingFromSingleton("MATCHMAIL_MATCHES_SENT", queueItem.CommunityID, brand.Site.SiteID));
                int numMatchesToSendForFrequency = numMatchesToSendToYesMail;
                if(Conversion.CBool(SettingsService.GetSettingFromSingleton("MATCHMAIL_USE_FREQUENCY_FOR_NUM_TO_SEND", queueItem.CommunityID, brand.Site.SiteID), false))
                {
                    numMatchesToSendForFrequency = BulkMailUtils.Instance.GetNumberOfItemsToSendForFrequency(queueItem.Frequency);
                }
                int numToPad = 0;
                SearchPreferenceCollection searchPrefs = null;
                bool HasStoredSearchPrefs = true;
                MatchnetQueryResults searchResults;

                matchResult.NumberMatchesNeeded = numMatchesToSendForFrequency;

                perfHelper.StartTiming();

                searchPrefs = queueItem.SearchPreferences;
                matchResult.StoredSearchPreferences = queueItem.SearchPreferences;

                if (searchPrefs == null || searchPrefs.Count == 0)
                {
                    HasStoredSearchPrefs = false;
                    _perfNoSearchPrefTotal.Increment();
                    matchResult.StoredSearchPreferencesValid = false;
                }
                else
                {
                    matchResult.HasStoredSearchPreferences = true;
                    searchPrefs = ReplaceDistanceWithRadiusIfPresent(searchPrefs);
                    matchResult.StoredSearchPreferences = searchPrefs;
                    ValidationResult validationResult = searchPrefs.Validate();
                    matchResult.InitialSearchValidationResult = validationResult;

                    if (validationResult.Status == PreferencesValidationStatus.Failed)
                    {
                        searchPrefs = null;
                        HasStoredSearchPrefs = false; // what they have is not valid. Might as well be they have none.
                        _perfNoSearchPrefTotal.Increment();
                        traceOut("Stored Search Pref invalid. Reason:" + validationResult.ValidationError.ToString(), queueItem.MemberID);
                        matchResult.StoredSearchPreferencesValid = false;
                    }
                    else
                    {
                        HasStoredSearchPrefs = true;
                        fixIsraelAreaCodes(ref searchPrefs);
                        _perfHaveStoredSearchPrefTotal.Increment();
                        matchResult.StoredSearchPreferencesValid = true;
                    }
                }

                if (!HasStoredSearchPrefs) // either had none or had some but were not valid
                {
                    searchPrefs = getDefaultPrefs(member, brand);
                    matchResult.CreatedDefaultPreferences = true;
                    matchResult.DefaultSearchPreferences = searchPrefs;
                    traceOut("Perfs invented", queueItem.MemberID);
                    if (searchPrefs == null)
                    {
                        matchResult.MatchSuccessful = false;
                        matchResult.ProcessingStatus = ProcessingStatusID.ProcessedNoUpdate;
                        return matchResult;
                    }
                }

                if (perfHelper != null)
                    perfHelper.EndTiming(_perfAvgBuildDefaultPrefTime, _perfAvgBuildDefaultPrefTimeBase);

                #region Searches to populate miniprofile collection

                ProcessingStatusID searchPrefsProcessingStatus = ProcessingStatusID.SentMatchesWithStoredPrefs;
                MiniprofileInfoCollection miniprofileInfoCollection = new MiniprofileInfoCollection();
                int tmpCount = 0;
                // First search w/ photos and age filter
                searchPrefs.Add("HasPhotoFlag", "1");

                bool useAgeFilter = UseAgeFilter();
                if (useAgeFilter)
                {
                    if (BulkMailUtils.Instance.ShouldRunSearchPrefType(Enumerations.SearchPrefType.StrictWithAge, brand))
                    {
                        AddAgeFilter(member, brand, searchPrefs);
                        searchPrefType = Enumerations.SearchPrefType.StrictWithAge;
                        if (HasStoredSearchPrefs)
                        {
                            matchResult.StoredSearchPreferencesWithAgeFilter = GetClonedSearchPrefs(searchPrefs);
                            searchPrefsProcessingStatus = ProcessingStatusID.SentMatchesWithStoredPrefsWithAgeFilter;
                        }
                        else
                        {
                            matchResult.DefaultSearchPreferencesWithAgeFilter = GetClonedSearchPrefs(searchPrefs);
                            searchPrefsProcessingStatus = ProcessingStatusID.SentMatchesWithDefaultPrefsWithAgeFilter;
                        }

                        perfHelper.StartTiming();

                        traceOut("Initial SearchPrefs w/ Age Filter: " + getPrefDump(searchPrefs), queueItem.MemberID);
                        searchResults = populateResultsCollection(searchPrefs, brand, queueItem.MemberID, searchService);
                        matchResult.InitialSearchResultsWithAgeFilter = searchResults;

                        traceOut("First matches populated. Results: " + searchResults.MatchesReturned.ToString(), queueItem.MemberID);
                        if (searchResults != null)
                        {
                            matchResult.TotalMatchesFound = populateMiniProfileCollection(searchResults, brand,
                                                                                          miniprofileInfoCollection,
                                                                                          scrubList, numMatchesToSendForFrequency,
                                                                                          queueItem.MemberID,
                                                                                          BulkMailType.MatchMail,
                                                                                          matchResult.InitialMatchResultsWithAgeFilter,
                                                                                          memberService);
                        }
                        traceOut("Matches found from initial results w/ Age Filter: " + matchResult.TotalMatchesFound.ToString(), queueItem.MemberID);

                        perfHelper.EndTiming(_perfAvgPopulateResultsTime, _perfAvgPopulateResultsTimeBase);
                        _perfQuery1Total.Increment();

                        if (miniprofileInfoCollection.Count == 0)
                        {
                            _perfQuery1NoResults.Increment();
                        }


                        if (matchResult.TotalMatchesFound >= numMatchesToSendForFrequency)
                        {
                            matchResult.InitialMatchResultsWithAgeFilter.SearchSufficient = true;
                        }
                    }
                    else
                    {
                        traceOut("Search: StrictWithAge is disabled");
                    }
                }

                // second search (first search if age filter is turned off), only if need more: remove age filter
                if (miniprofileInfoCollection.Count < numMatchesToSendForFrequency)
                {
                    if (BulkMailUtils.Instance.ShouldRunSearchPrefType(Enumerations.SearchPrefType.StrictNoAge, brand))
                    {
                        searchPrefType = Enumerations.SearchPrefType.StrictNoAge;
                        if (useAgeFilter)
                        {
                            RemoveAgeFilter(searchPrefs);
                        }

                        if (HasStoredSearchPrefs)
                        {
                            searchPrefsProcessingStatus = ProcessingStatusID.SentMatchesWithStoredPrefs;
                        }
                        else
                        {
                            searchPrefsProcessingStatus = ProcessingStatusID.SentMatchesWithDefaultPrefs;
                        }

                        perfHelper.StartTiming();

                        traceOut("Initial SearchPrefs w/o Age Filter: " + getPrefDump(searchPrefs), queueItem.MemberID);
                        searchResults = populateResultsCollection(searchPrefs, brand, queueItem.MemberID, searchService);
                        matchResult.InitialSearchResults = searchResults;

                        traceOut("Second matches populated. Results: " + searchResults.MatchesReturned.ToString(), queueItem.MemberID);
                        if (searchResults != null)
                        {
                            matchResult.TotalMatchesFound = populateMiniProfileCollection(searchResults, brand,
                                                                                          miniprofileInfoCollection,
                                                                                          scrubList, numMatchesToSendForFrequency,
                                                                                          queueItem.MemberID,
                                                                                          BulkMailType.MatchMail,
                                                                                          matchResult.InitialMatchResults,
                                                                                          memberService);
                        }
                        traceOut("Matches found from initial w/o age filter results: " + matchResult.TotalMatchesFound.ToString(), queueItem.MemberID);

                        perfHelper.EndTiming(_perfAvgPopulateResultsTime, _perfAvgPopulateResultsTimeBase);
                        _perfQuery1Total.Increment();

                        if (miniprofileInfoCollection.Count == 0)
                        {
                            _perfQuery1NoResults.Increment();
                        }

                        tmpCount = 0;

                        if (matchResult.TotalMatchesFound >= numMatchesToSendForFrequency)
                        {
                            matchResult.InitialMatchResults.SearchSufficient = true;
                        }
                    }
                    else
                    {
                        traceOut("Search: StrictNoAge is disabled");
                    }
                }

                // third search, only if need more and uses the default wide search prefs with age filter
                SearchPreferenceCollection laxSearchPrefs = null;
                if (HasStoredSearchPrefs)
                {
                    if (useAgeFilter)
                    {
                        if (BulkMailUtils.Instance.ShouldRunSearchPrefType(Enumerations.SearchPrefType.RelaxWithAge, brand))
                        {
                            if (miniprofileInfoCollection.Count < numMatchesToSendForFrequency)
                            {
                                perfHelper.StartTiming();

                                searchPrefType = Enumerations.SearchPrefType.RelaxWithAge;
                                tmpCount = miniprofileInfoCollection.Count;
                                // if had stored prefs, just copy over basics, and expand age range to bracket around age.
                                laxSearchPrefs = getLaxSearchPreference(member, brand, searchPrefs);
                                //Add Age Filter
                                AddAgeFilter(member, brand, laxSearchPrefs);
                                matchResult.LaxSearchPreferencesWithAgeFilter = GetClonedSearchPrefs(laxSearchPrefs);
                                searchPrefsProcessingStatus = ProcessingStatusID.SentMatchesWithLaxPrefsWithAgeFilter;

                                searchResults = populateResultsCollection(laxSearchPrefs, brand, queueItem.MemberID, searchService);
                                matchResult.LaxSearchResultsWithAgeFilter = searchResults;

                                traceOut("Lax Prefs w/ Age Filter invented" + getPrefDump(laxSearchPrefs), queueItem.MemberID);
                                traceOut("Lax matches populated. Results: " + searchResults.MatchesReturned.ToString(), queueItem.MemberID);

                                if (searchResults != null)
                                {
                                    matchResult.TotalMatchesFound = matchResult.TotalMatchesFound +
                                                                    populateMiniProfileCollection(searchResults, brand,
                                                                                                  miniprofileInfoCollection,
                                                                                                  scrubList,
                                                                                                  numMatchesToSendForFrequency,
                                                                                                  queueItem.MemberID,
                                                                                                  BulkMailType.MatchMail,
                                                                                                  matchResult.LaxMatchResultsWithAgeFilter,
                                                                                                  memberService);
                                }

                                if (matchResult.TotalMatchesFound >= numMatchesToSendForFrequency)
                                {
                                    matchResult.LaxMatchResultsWithAgeFilter.SearchSufficient = true;
                                }

                                traceOut("Matches found after lax search w/ Age Filter: " + matchResult.TotalMatchesFound.ToString(), queueItem.MemberID);

                                perfHelper.EndTiming(_perfAvgPopulateResultsTime, _perfAvgPopulateResultsTimeBase);
                                _perfQuery3Total.Increment();
                                if (miniprofileInfoCollection.Count == tmpCount)
                                {
                                    _perfQuery3NoResults.Increment();
                                }
                            }
                        }
                        else
                        {
                            traceOut("Search: RelaxWithAge is disabled");
                        }
                    }

                    // fourth search (second search if age filter turned off), only if need more and uses the default wide search prefs: remove age filter
                    if (miniprofileInfoCollection.Count < numMatchesToSendForFrequency)
                    {
                        if (BulkMailUtils.Instance.ShouldRunSearchPrefType(Enumerations.SearchPrefType.RelaxNoAge, brand))
                        {
                            perfHelper.StartTiming();

                            tmpCount = miniprofileInfoCollection.Count;
                            searchPrefType = Enumerations.SearchPrefType.RelaxNoAge;
                            if (useAgeFilter)
                            {
                                RemoveAgeFilter(laxSearchPrefs);
                            }
                            if (null == laxSearchPrefs)
                            {
                                // if had stored prefs, just copy over basics, and expand age range to bracket around age.
                                laxSearchPrefs = getLaxSearchPreference(member, brand, searchPrefs);
                            }
                            matchResult.LaxSearchPreferences = laxSearchPrefs;
                            searchPrefsProcessingStatus = ProcessingStatusID.SentMatchesWithLaxPrefs;

                            searchResults = populateResultsCollection(laxSearchPrefs, brand, queueItem.MemberID, searchService);
                            matchResult.LaxSearchResults = searchResults;

                            traceOut("Lax Perfs w/o Age Filter invented" + getPrefDump(laxSearchPrefs), queueItem.MemberID);
                            traceOut("Lax matches populated. Results: " + searchResults.MatchesReturned.ToString(), queueItem.MemberID);

                            if (searchResults != null)
                            {
                                matchResult.TotalMatchesFound = matchResult.TotalMatchesFound +
                                                                populateMiniProfileCollection(searchResults, brand,
                                                                                              miniprofileInfoCollection,
                                                                                              scrubList, numMatchesToSendForFrequency,
                                                                                              queueItem.MemberID,
                                                                                              BulkMailType.MatchMail,
                                                                                              matchResult.LaxMatchResults,
                                                                                              memberService);
                            }

                            if (matchResult.TotalMatchesFound >= numMatchesToSendForFrequency)
                            {
                                matchResult.LaxMatchResults.SearchSufficient = true;
                            }

                            traceOut("Matches found after lax search w/o Age Filter: " + matchResult.TotalMatchesFound.ToString(), queueItem.MemberID);

                            perfHelper.EndTiming(_perfAvgPopulateResultsTime, _perfAvgPopulateResultsTimeBase);
                            _perfQuery3Total.Increment();
                            if (miniprofileInfoCollection.Count == tmpCount)
                            {
                                _perfQuery3NoResults.Increment();
                            }
                        }
                        else
                        {
                            traceOut("Search: RelaxNoAge is disabled");
                        }
                    }
                }

                if (miniprofileInfoCollection.Count >= numMatchesToSendForFrequency)
                {
                    matchResult.MatchSuccessful = true;
                    matchResult.ProcessingStatus = searchPrefsProcessingStatus;

                    numToPad = numMatchesToSendToYesMail - miniprofileInfoCollection.Count;

                    //YesMail excpects there to be a certain number of matches, represented by MATCH_COUNT, but we're only
                    //going to send the number represented by numMatchesToSend, so we need to pad out the collection for the 
                    //difference with empty miniprofiles.
                    for (int count = 0; count < numToPad; count++)
                    {
                        miniprofileInfoCollection.Add(new MiniprofileInfo(0, string.Empty, DateTime.MinValue, 0,
                                                                          string.Empty, string.Empty));
                    }
                }

                #endregion

                #region Process query result(s)
                matchResult.SearchPrefType = searchPrefType;
                if (miniprofileInfoCollection.Count == numMatchesToSendToYesMail)
                {
                    perfHelper.StartTiming();
                    perfHelper.EndTiming(_perfAvgAdjustScrublistTime, _perfAvgAdjustScrublistTimeBase);

                    try
                    {
                        perfHelper.StartTiming();

                        if (!_isDebug)
                        {
                            //we don't want to actually send an email if we're in debug mode
                            bool isRelaxedPrefs = (matchResult.LaxSearchPreferences != null) ? true : false;
                            sendEmail(brand, member.EmailAddress, getMiniprofileFromMember(brand, member),
                                      miniprofileInfoCollection, BulkMailType.MatchMail, searchPrefType);
                            traceOut("Sent Matches! SearchPrefType: " + searchPrefType.ToString(), queueItem.MemberID);
                        }

                        perfHelper.EndTiming(_perfAvgSendToXmailTime, _perfAvgSendToXmailTimeBase);
                        _perfTotalSent.Increment();
                        _perfTotalSentRate.Increment();
                    }
                    catch (Exception ex)
                    {
                        new ServiceBoundaryException(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME,
                                                     "BulkMail got exception from External Mail while attempting to send",
                                                     ex);
                        matchResult.MatchSuccessful = false;
                        matchResult.ProcessingException = ex;
                        matchResult.ProcessingStatus = ProcessingStatusID.ProcessedErrorGeneral;
                        return matchResult;
                    }
                }
                else
                {
                    traceOut("No Matches!", queueItem.MemberID);
                    matchResult.ProcessingStatus = ProcessingStatusID.ProcessedNoUpdate;
                    matchResult.MatchSuccessful = false;
                }
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME,
                                             "BulkMail processing exception", ex);
                matchResult.MatchSuccessful = false;
                matchResult.ProcessingException = ex;
                matchResult.ProcessingStatus = ProcessingStatusID.ProcessedErrorGeneral;
            }

            #endregion

            return matchResult;
        }

        private SearchPreferenceCollection GetClonedSearchPrefs(SearchPreferenceCollection searchPrefs)
        {
            SearchPreferenceCollection clonedSearchPrefs = new SearchPreferenceCollection();
            foreach (string key in searchPrefs.Keys())
            {
                clonedSearchPrefs.Add(key, searchPrefs.Value(key));
            }
            return clonedSearchPrefs;
        }

        private bool UseAgeFilter()
        {
            bool b = false;
            try
            {
                b = bool.TrueString.ToLower().Equals(SettingsService.GetSettingFromSingleton("BULKMAIL_USE_AGE_FILTER").ToLower());
            }
            catch (Exception e)
            {
                traceOut("Could not find setting: BULKMAIL_USE_AGE_FILTER");
            }
            return b;
        }

        private void AddAgeFilter(IMemberDTO member, Brand brand, SearchPreferenceCollection searchPrefs)
        {
            DateTime birthdate = member.GetAttributeDate(brand, "BirthDate");
            int age = Matchnet.Lib.Util.Util.Age(birthdate);
            searchPrefs.Add("preferredage", age.ToString());
        }

        private void RemoveAgeFilter(SearchPreferenceCollection searchPrefs)
        {
            if (searchPrefs != null)
            {
                if (searchPrefs.ContainsKey("preferredage"))
                {
                    searchPrefs.Add("preferredage", "");
                }
            }
        }

        private SearchPreferenceCollection ReplaceDistanceWithRadiusIfPresent(SearchPreferenceCollection searchPrefs)
        {
            SearchPreferenceCollection newPrefs = new SearchPreferenceCollection();

            foreach (string key in searchPrefs.Keys())
            {
                if (key.ToLower() == "distance")
                {
                    int distance = Int32.Parse(searchPrefs["distance"]);
                    newPrefs.Add("Radius", distance.ToString());
                }
                else
                {
                    newPrefs.Add(key, searchPrefs[key]);
                }
            }

            return newPrefs;
        }

        private bool processNewMemberQueueItem(PerformanceCounterHelper perfHelper, BulkMailQueueItem queueItem, IMemberDTO member, Brand brand, ScrubList scrubList, out int matchesFound, IMemberSearchSA searchService, IGetMember memberService)
        {
            matchesFound = 0;
            bool success = true;
            SearchPreferenceCollection searchPrefs = null;
            bool HasStoredSearchPrefs = true;
            int numMatchesToSend = Conversion.CInt(SettingsService.GetSettingFromSingleton(SETTING_NEW_MEMBER_EMAIL_NUMBER_OF_MATCHES, queueItem.CommunityID, brand.Site.SiteID));
            MatchnetQueryResults searchResults;

            perfHelper.StartTiming();
            searchPrefs = queueItem.SearchPreferences;
            if (searchPrefs == null || searchPrefs.Count == 0)
            {
                HasStoredSearchPrefs = false;
                _perfNoSearchPrefTotal.Increment();
            }
            else
            {
                ValidationResult validationResult = searchPrefs.Validate();
                if (validationResult.Status == PreferencesValidationStatus.Failed)
                {
                    searchPrefs = null;
                    HasStoredSearchPrefs = false; // what they have is not valid. Might as well be they have none.
                    _perfNoSearchPrefTotal.Increment();
                }
                else
                {
                    HasStoredSearchPrefs = true;
                    fixIsraelAreaCodes(ref searchPrefs);
                    _perfHaveStoredSearchPrefTotal.Increment();
                }
            }

            if (!HasStoredSearchPrefs) // either had none or had some but were not valid
            {
                searchPrefs = getNewMemberPrefs(member, brand);
                if (searchPrefs == null)
                {
                    success = false;
                    return success;
                }
            }

            perfHelper.EndTiming(_perfAvgBuildDefaultPrefTime, _perfAvgBuildDefaultPrefTimeBase);

            MiniprofileInfoCollection miniprofileInfoCollection = new MiniprofileInfoCollection();

            perfHelper.StartTiming();

            searchResults = populateResultsCollection(searchPrefs, brand, queueItem.MemberID, searchService);
            if (searchResults != null)
            {
                matchesFound = populateMiniProfileCollection(searchResults, brand, miniprofileInfoCollection, scrubList, numMatchesToSend, queueItem.MemberID, BulkMailType.NewMembers, null, memberService);
            }

            _perfQuery1Total.Increment();

            if (miniprofileInfoCollection.Count == numMatchesToSend)
            {
                perfHelper.StartTiming();

                sendEmail(brand, member.EmailAddress, getMiniprofileFromMember(brand, member), miniprofileInfoCollection, BulkMailType.NewMembers, Matchnet.BulkMail.ValueObjects.Enumerations.SearchPrefType.None);

                perfHelper.EndTiming(_perfAvgSendToXmailTime, _perfAvgSendToXmailTimeBase);
                _perfTotalSent.Increment();
                _perfTotalSentRate.Increment();
            }
            else
            {
                success = false;
            }

            return success;
        }

        private void sendEmail(Brand brand, string emailAddress, MiniprofileInfo recipientMiniProfileInfo, MiniprofileInfoCollection targetMiniProfileCollection, BulkMailType bulkMailType, Matchnet.BulkMail.ValueObjects.Enumerations.SearchPrefType searchPrefType)
        {
            string settingConstant = string.Empty;

            if (bulkMailType == BulkMailType.MatchMail)
            {
                settingConstant = "YESMAIL_PERCENT_MATCH_MAIL";
            }
            else if (bulkMailType == BulkMailType.NewMembers)
            {
                settingConstant = "YESMAIL_PERCENT_NEW_MEMBER_MAIL";
            }

            if (settingConstant != string.Empty)
            {
                int yesMailPercent = Convert.ToInt32(SettingsService.GetSettingFromSingleton(settingConstant, brand.Site.Community.CommunityID,
                    brand.Site.SiteID));

                // Decide on the mail handler based on the last digit of the memberID
                IMailHandler mailHandler;
                int lastDigitMemberID = recipientMiniProfileInfo.MemberID % 10;

                if (lastDigitMemberID < (yesMailPercent / 10))
                {
                    mailHandler = YesMailHandler.Instance;
                }
                else
                {
                    mailHandler = MingleMailerHandler.Instance;
                }

                // don't forget to set the allowed domains. this is really only required for YesMail but still
                mailHandler.AllowedDomainsProperty = _allowedDomains;

                // Wrap the arguments into an impulse object so we can call SendEmail on the mail handlers
                ActivityRecording.ValueObjects.Types.ActionType actionType = Matchnet.ActivityRecording.ValueObjects.Types.ActionType.None;
                ImpulseBase impulseBase = null;
                if (bulkMailType == BulkMailType.MatchMail)
                {
                    impulseBase = new MatchMailImpulse(brand.BrandID, emailAddress, recipientMiniProfileInfo, targetMiniProfileCollection);

                    switch (searchPrefType)
                    {
                        case Enumerations.SearchPrefType.StrictWithAge:
                            ((MatchMailImpulse)impulseBase).SearchPrefType = "strict-w-age";
                            break;
                        case Enumerations.SearchPrefType.StrictNoAge:
                            ((MatchMailImpulse)impulseBase).SearchPrefType = "strict-no-age";
                            break;
                        case Enumerations.SearchPrefType.RelaxWithAge:
                            ((MatchMailImpulse)impulseBase).SearchPrefType = "lax-w-age";
                            break;
                        case Enumerations.SearchPrefType.RelaxNoAge:
                            ((MatchMailImpulse)impulseBase).SearchPrefType = "lax-no-age";
                            break;
                    }
                    actionType = Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendMatchMail;

                }
                else if (bulkMailType == BulkMailType.NewMembers)
                {
                    impulseBase = new NewMemberMailImpulse(brand.BrandID, emailAddress, recipientMiniProfileInfo, targetMiniProfileCollection);
                    actionType = Matchnet.ActivityRecording.ValueObjects.Types.ActionType.SendNewMemberMail;
                }
                mailHandler.SendEmail(impulseBase);

                string activityCaption = "";
                StringBuilder activityCaptionSb = new StringBuilder();
                try
                {
                    activityCaptionSb.Append("<Mail><MatchMail>");
                    foreach (MiniprofileInfo miniprofileInfo in targetMiniProfileCollection)
                    {
                        if (miniprofileInfo.MemberID != 0)
                        {
                            activityCaptionSb.Append("<MemberID>").Append(miniprofileInfo.MemberID).Append("</MemberID>");
                        }
                     }
                    activityCaptionSb.Append("</MatchMail></Mail>");

                    activityCaption = activityCaptionSb.ToString();
                }
                catch (Exception)
                {
                    activityCaption = "";
                }

                // Log this to ActivityRecording svc
                ActivityRecordingSA.Instance.RecordActivity(recipientMiniProfileInfo.MemberID, Constants.NULL_INT, brand.Site.SiteID, actionType,
                    ActivityRecording.ValueObjects.Types.CallingSystem.BulkMail, activityCaption);
            }

        }

        #endregion

        #region Helpers for processing queue items

        public bool resultItemEligibleForNewMemberMail(MatchnetResultItem resultItem, Brand brand)
        {
            bool match = true;

            IMemberDTO possibleMatch = MailHandlerUtils.GetIMemberDTO(MemberService, resultItem.MemberID, MemberLoadFlags.None);
            DateTime matchInsertDate = possibleMatch.GetAttributeDate(brand, "brandinsertdate");
            int matchTimeFromSignup = (DateTime.Now - matchInsertDate).Days;
            if (matchTimeFromSignup > _cfgNewMemberEmailSignUpFactor)
            {
                match = false;
            }

            return match;
        }

        public static void fixIsraelAreaCodes(ref SearchPreferenceCollection searchPrefs)
        {
            ///HACK:
            ///Since area codes are saved as int, sometimes they are null number.
            ///Prepend a "0" to israel area codes, otherwise they don't resolve down the line.
            ///Only do so if the area code is a digit, and not already 0 prepended.
            if (searchPrefs["SearchTypeID"] == "2" && searchPrefs["CountryRegionID"] == "105")
            {
                foreach (string ac in _AreaCodeNames)
                {
                    if (searchPrefs[ac] != String.Empty && searchPrefs[ac] != Constants.NULL_INT.ToString() && !searchPrefs[ac].StartsWith("0"))
                    {
                        searchPrefs[ac] = "0" + searchPrefs[ac];
                    }
                }
            }
        }

        private int populateMiniProfileCollection(MatchnetQueryResults searchResults, Brand brand, MiniprofileInfoCollection miniprofileInfoCollection, ScrubList scrubList, int numMatchesToProcess, int memberID, BulkMailType mailType, MatchPopulateResult matchPopulateResult, IGetMember memberService)
        {
            BulkMailSearchResultItem BulkMailResult;
            int matches = 0;
            IMemberDTO member;

            if (searchResults.Items.Count > 0)
            {
                for (int resultIdx = 0; resultIdx < searchResults.Items.Count; resultIdx++)
                {
                    member = null;
                    MatchnetResultItem resultItem = (searchResults.Items[resultIdx] as MatchnetResultItem);
                    BulkMailResult = new BulkMailSearchResultItem(resultItem);

                    if (!scrubList.Contains(resultItem.MemberID) && resultItem.MemberID != memberID)
                    {
                        if ((mailType == BulkMailType.NewMembers && resultItemEligibleForNewMemberMail(resultItem, brand)) || mailType == BulkMailType.MatchMail)
                        {
                            if (!isInMiniprofileInfoCollection(miniprofileInfoCollection, resultItem))
                            {
                                member = getValidatedMember(resultItem.MemberID, memberID, brand, memberService);
                                if (member != null)
                                {
                                    miniprofileInfoCollection.Add(getMiniprofileFromResultItem(BulkMailResult, brand, member, mailType));
                                    matches++;
                                    scrubList.Add(resultItem.MemberID);
                                    if(matchPopulateResult != null) matchPopulateResult.MatchedMembers.Add(resultItem.MemberID);
                                }
                                else
                                {
                                    if (matchPopulateResult != null) matchPopulateResult.InvalidMembers.Add(resultItem.MemberID);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (matchPopulateResult != null) matchPopulateResult.MembersOnScrubList.Add(resultItem.MemberID);
                    }

                    if (miniprofileInfoCollection.Count == numMatchesToProcess)
                    {
                        break;
                    }
                }
            }

            return matches;
        }

        private IMemberDTO getValidatedMember(int resultMemberID, int memberID, Brand brand, IGetMember memberService)
        {
            IMemberDTO resultMember = MailHandlerUtils.GetIMemberDTO(memberService, resultMemberID, MemberLoadFlags.None);

            DateTime registerDate = resultMember.GetAttributeDate(brand, "BrandInsertDate", DateTime.MinValue);
            if (DateTime.Now.Subtract(registerDate).Days < _cfgRegisteredDaysCriteria)
            {
                resultMember = null;
            }

            if (!_isDebug)
            {
                bool isProfileBlockingEnabled = Convert.ToBoolean(SettingsService.GetSettingFromSingleton("PROFILE_BLOCK_ENABLED", brand.Site.Community.CommunityID));
                bool isE2ProfileBlockEnabled = IsE2ProfileBlockEnabled(brand.Site.Community.CommunityID);
                //don't filter out blocked members if E2 has already done it
                if (isProfileBlockingEnabled && !isE2ProfileBlockEnabled)
                {
                    Matchnet.List.ServiceAdapters.List list = ListSA.Instance.GetList(memberID);

                    if (list.IsHotListed(Matchnet.List.ValueObjects.HotListCategory.ExcludeListInternal, brand.Site.Community.CommunityID, resultMemberID))
                    {
                        resultMember = null;
                    }
                }
            }

            return resultMember;
        }

        private void saveBulkMailStatusToDB(IMemberDTO member, Brand brand, BulkMailQueueItem queueItem, string scrubList, bool sentMatchesFlag, int matchesFoundCount, ProcessingStatusID processingStatusID)
        {

#if DEBUG
            Trace.WriteLine(queueItem.ToString());
            Trace.WriteLine("...	" + scrubList.ToString());
#endif

            try
            {
                traceOut("Saving BulkMailStatus", queueItem.MemberID);
                Command command = new Command(Matchnet.BulkMail.ValueObjects.ServiceConstants.HYDRA_SAVE_NAME, "up_BulkMailSchedule_Save_Service", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, queueItem.MemberID);
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, queueItem.CommunityID);
                command.AddParameter("@BulkMailTypeID", SqlDbType.Int, ParameterDirection.Input, (int)queueItem.BulkMailTypeID);
                command.AddParameter("@Frequency", SqlDbType.TinyInt, ParameterDirection.Input, queueItem.Frequency);
                command.AddParameter("@LastSentDate", SqlDbType.SmallDateTime, ParameterDirection.Input, (sentMatchesFlag) ? DateTime.Now : queueItem.LastSentDate);
                command.AddParameter("@LastAttemptDate", SqlDbType.SmallDateTime, ParameterDirection.Input, queueItem.LastAttemptDate);
                command.AddParameter("@NextAttemptDate", SqlDbType.SmallDateTime, ParameterDirection.Input, queueItem.NextAttemptDate);
                command.AddParameter("@SentCount", SqlDbType.Int, ParameterDirection.Input, (sentMatchesFlag) ? queueItem.SentCount + matchesFoundCount : queueItem.SentCount);
                command.AddParameter("@FrequencyUpdateDate", SqlDbType.SmallDateTime, ParameterDirection.Input, queueItem.FrequencyUpdateDate);
                command.AddParameter("@InsertDate", SqlDbType.SmallDateTime, ParameterDirection.Input, queueItem.InsertDate);
                command.AddParameter("@SentMemberIDList", SqlDbType.VarChar, ParameterDirection.Input, (sentMatchesFlag) ? scrubList.ToString() : null);
                command.AddParameter("@ProcessingStatusID", SqlDbType.Int, ParameterDirection.Input, (int)processingStatusID);

                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw new Exception("Error updating mnAlert. " + queueItem.ToString()
                    + "\nnew scrublist" + scrubList, ex);
            }
        }

        public void saveBulkMailStatusToDBNoUpdate(BulkMailQueueItem queueItem, ProcessingStatusID processingStatus)
        {
            try
            {
                traceOut("Saving BulkMailStatus NoUpdate with status: " + processingStatus.ToString(), queueItem.MemberID);
                Command command = new Command(Matchnet.BulkMail.ValueObjects.ServiceConstants.HYDRA_SAVE_NAME, "up_BulkMailSchedule_Save_Service_No_Update", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, queueItem.MemberID);
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, queueItem.CommunityID);
                command.AddParameter("@BulkMailTypeID", SqlDbType.Int, ParameterDirection.Input, (int)queueItem.BulkMailTypeID);
                command.AddParameter("@ProcessingStatusID", SqlDbType.TinyInt, ParameterDirection.Input, (int)processingStatus);
                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw new Exception("Error updating mnAlert (processing update only). " + queueItem.ToString());
            }
        }

        private MatchnetQueryResults populateResultsCollection(SearchPreferenceCollection searchPrefs, Brand brand, int memberID, IMemberSearchSA searchService)
        {
            if (searchPrefs == null)
                traceOut("null searchprefs", memberID);

            if (brand == null)
                traceOut("null brand", memberID);

            MatchnetQueryResults searchResults = null;
            ValidationResult validationResult = searchPrefs.Validate();

            if (validationResult.Status == PreferencesValidationStatus.Failed)
            {
                traceOut("populateResultsCollection got invalid prefs (" + validationResult.ValidationError.ToString() + ")" + getPrefDump(searchPrefs), memberID);
                return searchResults;
            }

            int communityId = brand.Site.Community.CommunityID;
            int siteId = brand.Site.SiteID;

            try
            {
                PerformanceCounterHelper pch = new PerformanceCounterHelper();
                searchResults = searchService.Search(searchPrefs,
                    communityId,
                    siteId,
                    0,
                    PAGE_SIZE,
                    memberID,
                    Matchnet.Search.Interfaces.SearchEngineType.FAST,
                    Matchnet.Search.Interfaces.SearchType.MatchMail,
                    false);
                pch.EndTiming(_perfAvgSearchTime, _perfAvgSearchTimeBase);
                return searchResults;

            }
            catch (Exception ex)
            {
                traceOut("populateResultCollection search failed. " + getPrefDump(searchPrefs) +"\n"+ex.Message+"\n"+ex.StackTrace);
                return searchResults;
            }

        }

        private bool isInMiniprofileInfoCollection(MiniprofileInfoCollection miniprofileInfoCollection, MatchnetResultItem resultItem)
        {
            bool found = false;
            foreach (MiniprofileInfo miniprofileInfo in miniprofileInfoCollection)
            {
                if (miniprofileInfo.MemberID == resultItem.MemberID)
                {
                    found = true;
                    break;
                }
            }
            return found;
        }

        private MiniprofileInfo getMiniprofileFromResultItem(BulkMailSearchResultItem resultItem, Brand brand, IMemberDTO member, BulkMailType bulkMailType)
        {
            try
            {
                Int32 memberID = resultItem.MemberID;
                MiniprofileInfo resultProfile;

                populateResultItemFromMemberService(resultItem, brand, member, bulkMailType);

                String userName = resultItem["username"];
                DateTime birthDate = DateTime.Parse(resultItem["birthdate"]);
                Int32 regionID = Matchnet.Conversion.CInt(resultItem["regionid"]);
                string thumbNailWebPath = resultItem["thumbpath"];
                string aboutMe = resultItem["aboutme"];

                resultProfile = new MiniprofileInfo(memberID, userName, birthDate, regionID, thumbNailWebPath, aboutMe);

                resultProfile.EntertainmentLocation = Matchnet.Conversion.CInt(resultItem["entertainmentLocation"]);
                resultProfile.LeisureActivity = Matchnet.Conversion.CInt(resultItem["leisureActivity"]);
                resultProfile.Cuisine = Matchnet.Conversion.CInt(resultItem["cuisine"]);
                resultProfile.Music = Matchnet.Conversion.CInt(resultItem["music"]);
                resultProfile.Reading = Matchnet.Conversion.CInt(resultItem["reading"]);
                resultProfile.PhysicalActivity = Matchnet.Conversion.CInt(resultItem["physicalActivity"]);
                resultProfile.Pets = Matchnet.Conversion.CInt(resultItem["pets"]);

                return resultProfile;
            }
            catch (Exception ex)
            {
                throw new BLException("Error converting ResultItem to MiniprofileInfo. Missing member attributes?", ex);
            }
        }




        private void populateResultItemFromMemberService(BulkMailSearchResultItem resultItem, Brand brand, IMemberDTO member, BulkMailType bulkMailType)
        {
            resultItem["username"] = member.GetUserName(brand);
            resultItem["birthdate"] = member.GetAttributeDate(brand, "birthdate", DateTime.MinValue).ToString();
            resultItem["regionid"] = member.GetAttributeInt(brand, "regionid").ToString();
            resultItem["gendermask"] = member.GetAttributeInt(brand, "gendermask").ToString();
            resultItem["thumbpath"] = GetThumbnailImage(member, brand, bulkMailType);

            resultItem["entertainmentLocation"] = member.GetAttributeInt(brand, "entertainmentLocation").ToString();
            resultItem["leisureActivity"] = member.GetAttributeInt(brand, "leisureActivity").ToString();
            resultItem["cuisine"] = member.GetAttributeInt(brand, "cuisine").ToString();
            resultItem["music"] = member.GetAttributeInt(brand, "music").ToString();
            resultItem["reading"] = member.GetAttributeInt(brand, "reading").ToString();
            resultItem["physicalActivity"] = member.GetAttributeInt(brand, "physicalActivity").ToString();
            resultItem["pets"] = member.GetAttributeInt(brand, "pets").ToString();
            resultItem["aboutme"] = member.GetAttributeTextApproved(brand, "aboutme", string.Empty);
        }

        private SearchPreferenceCollection getNewMemberPrefs(IMemberDTO member, Brand brand)
        {
            SearchPreferenceCollection newPrefs = new SearchPreferenceCollection();
            try
            {
                int ageFactor = Convert.ToInt32(SettingsService.GetSettingFromSingleton(SETTING_NEW_MEMBER_EMAIL_AGE_FACTOR));
                int distance = Convert.ToInt32(SettingsService.GetSettingFromSingleton(SETTING_NEW_MEMBER_EMAIL_DISTANCE));

                int genderMask = member.GetAttributeInt(brand.Site.Community.CommunityID, 0, 0, "GenderMask", Constants.NULL_INT);
                int regionID = member.GetAttributeInt(brand.Site.Community.CommunityID, 0, 0, "RegionID", Constants.NULL_INT);
                DateTime birthDate = member.GetAttributeDate(brand.Site.Community.CommunityID, 0, 0, "Birthdate", DateTime.MinValue);

                if (genderMask != Constants.NULL_INT && regionID != Constants.NULL_INT && birthDate != DateTime.MinValue && GenderUtils.IsFlippable(genderMask))
                {
                    genderMask = GenderUtils.FlipMaskIfHeterosexual(genderMask); // Get seeking gender
                    int[] agerange = AgeUtils.GetAgeRange(birthDate, Constants.NULL_INT, Constants.NULL_INT);
                    newPrefs.Add("GenderMask", genderMask.ToString());
                    newPrefs.Add("RegionID", regionID.ToString());
                    newPrefs.Add("SearchTypeID", "4"); //Region
                    newPrefs.Add("SearchOrderBy", "1"); // JoinDate
                    newPrefs.Add("Distance", distance.ToString());
                    newPrefs.Add("MinAge", (agerange[0] - ageFactor).ToString());
                    newPrefs.Add("MaxAge", (agerange[1] + ageFactor).ToString());
                    return newPrefs;
                }
                else
                {
                    traceOut("getDefaultPrefs failed for. Bunk member? CommunityID " + brand.Site.Community.CommunityID.ToString() + " |" + genderMask.ToString() + "|" + regionID.ToString() + "|" + birthDate.ToString(), member.MemberID);
                    return null;
                }
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME,
                    "getDefaultPrefs could not extract gender, region, birthdate for " + member.MemberID.ToString() + " CommunityID " + brand.Site.Community.CommunityID.ToString(),
                    ex);
                return null;
            }
        }

        private SearchPreferenceCollection getDefaultPrefs(IMemberDTO member, Brand brand)
        {
            SearchPreferenceCollection newPrefs = new SearchPreferenceCollection();
            try
            {
                int genderMask = member.GetAttributeInt(brand.Site.Community.CommunityID, 0, 0, "GenderMask", Constants.NULL_INT);
                int regionID = member.GetAttributeInt(brand.Site.Community.CommunityID, 0, 0, "RegionID", Constants.NULL_INT);
                int distance = Convert.ToInt32(SettingsService.GetSettingFromSingleton("MATCHMAIL_DEFAULT_SEARCH_RADIUS", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
                
                DateTime birthDate = member.GetAttributeDate(brand.Site.Community.CommunityID, 0, 0, "Birthdate", DateTime.MinValue);

                if (genderMask != Constants.NULL_INT && regionID != Constants.NULL_INT && birthDate != DateTime.MinValue && GenderUtils.IsFlippable(genderMask))
                {
                    genderMask = GenderUtils.FlipMaskIfHeterosexual(genderMask); // Get seeking gender
                    int[] agerange = AgeUtils.GetAgeRange(birthDate, Constants.NULL_INT, Constants.NULL_INT);
                    newPrefs.Add("GenderMask", genderMask.ToString());
                    newPrefs.Add("RegionID", regionID.ToString());
                    newPrefs.Add("SearchTypeID", "4"); //Region
                    newPrefs.Add("SearchOrderBy", "3"); // Proximity - will expand to 160 miles or whole country(!)
                    newPrefs.Add("Radius", distance.ToString());
                    newPrefs.Add("MinAge", agerange[0].ToString());
                    newPrefs.Add("MaxAge", agerange[1].ToString());
                    newPrefs.Add("domain", brand.Site.Community.CommunityID.ToString());
                    return newPrefs;
                }
                else
                {
                    traceOut("getDefaultPrefs failed for. Bunk member? CommunityID " + brand.Site.Community.CommunityID.ToString() + " |" + genderMask.ToString() + "|" + regionID.ToString() + "|" + birthDate.ToString(), member.MemberID);
                    return null;
                }
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME,
                    "getDefaultPrefs could not extract gender, region, birthdate for " + member.MemberID.ToString() + " CommunityID " + brand.Site.Community.CommunityID.ToString(),
                    ex);
                return null;
            }
        }

        private SearchPreferenceCollection getLaxSearchPreference(IMemberDTO member, Brand brand, SearchPreferenceCollection strictPreferences)
        {
            SearchPreferenceCollection laxPrefs = new SearchPreferenceCollection();
            try
            {
                return MailHandlerUtils.GetLaxSearchPreference(member, brand, strictPreferences);
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME,
                    "getLaxSearchPrefs could not extract birthdate for " + member.MemberID.ToString() + " CommunityID " + brand.Site.Community.CommunityID.ToString(),
                    ex);
                return null;
            }

        }

        private string GetThumbnailImage(IMemberDTO member, Brand brand, BulkMailType bulkMailType)
        {
            string thumbPath = string.Empty;

            bool useLargePhotos = Conversion.CBool(SettingsService.GetSettingFromSingleton(SETTING_USE_LARGE_IMAGES, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));

            if (useLargePhotos && bulkMailType == BulkMailType.MatchMail)
            {
                thumbPath = getLargeImageURL(member.MemberID, brand);
            }
            else
            {
                Matchnet.Member.ValueObjects.Photos.PhotoCommunity photos = member.GetPhotos(brand.Site.Community.CommunityID);

                if (photos.Count > 0 && photos[0].IsApproved)
                {
                    if (photos[0].IsPrivate)
                    {
                        thumbPath = getThumbPathPrivatePhoto(brand);
                    }
                    else
                    {
                        bool showPhotosFromCloud = Convert.ToBoolean(SettingsService.GetSettingFromSingleton("ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD"));
                        if (showPhotosFromCloud)
                        {
                            if (String.IsNullOrEmpty(photos[0].ThumbFileCloudPath))
                            {
                                thumbPath = getThumbPathNoPhoto(brand);
                            }
                            else
                            {
                                Spark.CloudStorage.Client cloudClient = new Spark.CloudStorage.Client(brand.Site.Community.CommunityID, brand.Site.SiteID, RuntimeSettings.Instance);
                                thumbPath = cloudClient.GetFullCloudImagePath(photos[0].ThumbFileCloudPath,FileType.MemberPhoto, false);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(photos[0].ThumbFileWebPath))
                            {
                                thumbPath = getThumbPathNoPhoto(brand);
                            }
                            else
                            {
                                thumbPath = "http://" + brand.Site.DefaultHost + "." + brand.Uri + photos[0].ThumbFileWebPath;
                            }
                        }
                    }
                }
                else
                {
                    thumbPath = getThumbPathNoPhoto(brand);
                }
            }

            return thumbPath;
        }

        private string getThumbPathNoPhoto(Brand brand)
        {
            // if the site is JDIL (site id of 4) use Site folder for the image (Because JDIL is within JD community)
            if (brand.Site.SiteID == 4)
            {
                return "http://" + brand.Site.DefaultHost + "." + brand.Uri + @"/img/Site/" + brand.Uri.Replace(".", "-") + "/nophoto.jpg";
            }
            else
            {
                return "http://" + brand.Site.DefaultHost + "." + brand.Uri + @"/img/Community/" + brand.Site.Community.Name + "/nophoto.jpg";
            }
        }

        private string getThumbPathPrivatePhoto(Brand brand)
        {
            string retVal = "http://" + brand.Site.DefaultHost + "." + brand.Uri + @"/img/Community/" + brand.Site.Community.Name + "/privatephoto80x104_ynm4.jpg";
            return (retVal);
        }

        private string getLargeImageURL(int memberID, Brand brand)
        {
            string imageURL = SettingsService.GetSettingFromSingleton(SETTING_LARGE_PHOTO_URL, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            imageURL = string.Format(imageURL, memberID.ToString(), brand.BrandID.ToString());
            string querystring = imageURL.Substring(imageURL.IndexOf("?") + 1);
            string encryptedQuerystring = Crypto.Encrypt(SettingsService.GetSettingFromSingleton("KEY"), querystring);
            string encodedQuerystring = HttpServerUtility.UrlTokenEncode(System.Text.ASCIIEncoding.ASCII.GetBytes(encryptedQuerystring));
            imageURL = imageURL.Substring(0, imageURL.IndexOf("?") + 1) + encodedQuerystring;

            return "http://" + brand.Site.DefaultHost + "." + brand.Uri + imageURL;
        }

        
        private MiniprofileInfo getMiniprofileFromMember(Brand brand, IMemberDTO member)
        {
            Int32 memberID = member.MemberID;
            String userName = member.GetUserName(brand);
            DateTime birthDate = member.GetAttributeDate(brand, "Birthdate", DateTime.MinValue);
            Int32 regionID = member.GetAttributeInt(brand, "RegionID", Constants.NULL_INT);
            String thumbNailWebPath = GetThumbnailImage(member, brand, BulkMailType.MatchMail);
            Int32 genderMask = member.GetAttributeInt(brand, "gendermask");
            Int32 entertainmentLocation = member.GetAttributeInt(brand, "entertainmentLocation");
            Int32 leisureActivity = member.GetAttributeInt(brand, "leisureActivity");
            Int32 cuisine = member.GetAttributeInt(brand, "cuisine");
            Int32 music = member.GetAttributeInt(brand, "music");
            Int32 reading = member.GetAttributeInt(brand, "reading");
            Int32 physicalActivity = member.GetAttributeInt(brand, "physicalActivity");
            Int32 pets = member.GetAttributeInt(brand, "pets");
            string aboutme = member.GetAttributeTextApproved(brand, "aboutme", string.Empty);

            MiniprofileInfo profile = new MiniprofileInfo(memberID, userName, birthDate, regionID, thumbNailWebPath, aboutme);

            profile.EntertainmentLocation = entertainmentLocation;
            profile.LeisureActivity = leisureActivity;
            profile.Cuisine = cuisine;
            profile.Music = music;
            profile.Reading = reading;
            profile.PhysicalActivity = physicalActivity;
            profile.Pets = pets;

            return profile;

        }

        #endregion

        #region DBLoad

        private void RunQueueLoad()
        {
            List<BulkMailQueueItem> items = null;
            while (_runnable)
            {
                if (_queue.GetCount() < _cfgQueueFloor)
                {
                    items = LoadBatchFromDB();
                    if (items.Count > 0)
                    {
                        _queue.EnqueueMultiple(items);
                        traceOut("Retrieved " + items.Count.ToString() + " items from DB");
                    }
                    else
                    {
                        traceOut("Sleeping because no items from DB");
                        Thread.Sleep(10000); // sleep because last scoop brought back nothing.
                    }
                }
                else
                {
                    traceOut("Sleeping because enough items in queue");
                    Thread.Sleep(10000);
                }
            }
        }

        private List<BulkMailQueueItem> LoadBatchFromDB()
        {
            List<BulkMailQueueItem> items = new List<BulkMailQueueItem>();

            try
            {
                Command command = new Command("mnAlertWrite", "up_BulkMailSchedule_Get_Items_To_Process", 0);
                command.AddParameter("@MaxDaysSinceLastLogin", SqlDbType.Int, ParameterDirection.Input, _cfgMaxDaysSinceLastLogin);
                command.AddParameter("@ProcessingServer", SqlDbType.VarChar, ParameterDirection.Input, _processingServer);

                DataSet ds = Matchnet.Data.Client.Instance.ExecuteDataSet(command);
                DataTable scheduleTable = ds.Tables[0];
                DataTable searchPreferenceTable = ds.Tables[1];

                scheduleTable.ChildRelations.Add(
                    "fk_BulkmailSchedule_SearchPreference",
                    new DataColumn[] { scheduleTable.Columns["MemberID"], scheduleTable.Columns["CommunityID"], scheduleTable.Columns["BulkMailTypeID"] },
                    new DataColumn[] { searchPreferenceTable.Columns["MemberID"], searchPreferenceTable.Columns["GroupID"], searchPreferenceTable.Columns["BulkMailTypeID"] }
                    );

                BulkMailQueueItem queueItem;
                if (scheduleTable != null)
                {
                    for (int i = 0; i < scheduleTable.Rows.Count; i++)
                    {
                        queueItem = GetQueueItem(scheduleTable.Rows[i]);
                        items.Add(queueItem);
                    }
                }
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "LoadBatchFromDB() failed.", ex, false);
            }

            return items;
        }

        public BulkMailQueueItem GetQueueItem(DataRow row)
        {
            Int32 memberID;
            Int32 communityID;
            BulkMailType bulkMailTypeID;
            byte frequency;
            DateTime lastSentDate = new DateTime(1900, 1, 1); // nullable!
            DateTime lastAttemptDate = new DateTime(1900, 1, 1); // nullable!
            DateTime nextAttemptDate;
            Int32 sentCount;
            DateTime frequencyUpdateDate = new DateTime(1900, 1, 1); // nullable!
            DateTime insertDate;
            string[] sentMemberIDList = new string[0]; // nullable!

            SearchPreferenceCollection searchPreferences;

            memberID = Convert.ToInt32(row["MemberID"]);
            communityID = Convert.ToInt32(row["CommunityID"]);
            bulkMailTypeID = (BulkMailType)Convert.ToInt32(row["BulkMailTypeID"]);
            frequency = Convert.ToByte(row["Frequency"]);
            if (row["LastSentDate"] != DBNull.Value) lastSentDate = Convert.ToDateTime(row["LastSentDate"]);
            if (row["LastAttemptDate"] != DBNull.Value) lastAttemptDate = Convert.ToDateTime(row["LastAttemptDate"]);
            nextAttemptDate = Convert.ToDateTime(row["NextAttemptDate"]);
            sentCount = Convert.ToInt32(row["SentCount"]);
            if (row["FrequencyUpdateDate"] != DBNull.Value) frequencyUpdateDate = Convert.ToDateTime(row["FrequencyUpdateDate"]);
            insertDate = Convert.ToDateTime(row["InsertDate"]);
            if (row["SentMemberIDList"] != DBNull.Value && row["SentMemberIDList"].ToString() != string.Empty) sentMemberIDList = row["SentMemberIDList"].ToString().Split(new char[] { ',' });

            searchPreferences = GetSearchPreferences(row);

            BulkMailQueueItem queueItem = new BulkMailQueueItem(
                                                        memberID,
                                                        communityID,
                                                        bulkMailTypeID,
                                                        frequency,
                                                        lastSentDate,
                                                        lastAttemptDate,
                                                        nextAttemptDate,
                                                        sentCount,
                                                        frequencyUpdateDate,
                                                        insertDate,
                                                        sentMemberIDList,
                                                        searchPreferences
                                                        );


            return queueItem;
        }


        private SearchPreferenceCollection GetSearchPreferences(DataRow row)
        {
            SearchPreferenceCollection searchPrefs = new SearchPreferenceCollection();
            DataRow[] searhPrefRows = row.GetChildRows("fk_BulkmailSchedule_SearchPreference");
            if (searhPrefRows.Length == 1) // there should be only one perf per group per member
            {
                foreach (string columnName in _SearchPreferenceColumnNameList)
                {
                    if (searhPrefRows[0][columnName] != DBNull.Value && searhPrefRows[0][columnName].ToString() != Constants.NULL_INT.ToString())
                    {
                        try
                        {
                            searchPrefs.Add(new SearchPreference(columnName, searhPrefRows[0][columnName].ToString()));
                        }
                        catch
                        {
                            searchPrefs.Add(new SearchPreference(columnName, String.Empty));
                        }
                    }
                }
            }
            return searchPrefs;
        }

        #endregion

        #region helpers

        private void slowDown()
        {
            
            CounterSample sample1 = _perfMatchRate.NextSample();
            float matchrate = CounterSample.Calculate(sample1, _perfMatchRate.NextSample());
            if (matchrate > _fQPSLimit)
            {	// this would make the thread sleep when mps gets too high
                traceOut(System.Environment.MachineName + ": slowingdown", 1);
                System.Diagnostics.Trace.WriteLine("matchrate " + matchrate.ToString() + " << " + _perfMatchRate.NextValue().ToString() + " | qps limit:" + _fQPSLimit.ToString());
                PerformanceCounterHelper perfHelper = new PerformanceCounterHelper();
                Thread.Sleep(Convert.ToInt32(10.0 * _fThreadCount / _fQPSLimit));
                perfHelper.EndTiming(_perfAvgSlowdownTime, _perfAvgSlowdownTimeBase);
            }
        }


        private void traceOut(string msg)
        {
            traceOut(msg, -1);
        }

        private void traceOut(string msg, int memberID)
        {
            if (memberID > 0)
            {
                RollingFileLogger.Instance.LogInfoMessage(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, msg + " MemberID: " + memberID.ToString(), null);
            }
            else
            {
                RollingFileLogger.Instance.LogInfoMessage(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, msg, null);
            }
        }

        private void logException(string msg, Exception ex)
        {
            RollingFileLogger.Instance.LogException(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, CLASS_NAME, msg, ex, null);
        }


        private void Warn(string warning)
        {
            System.Diagnostics.EventLog.WriteEntry(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME,
                warning,
                EventLogEntryType.Warning);
        }


        private void Inform(string statement)
        {
            System.Diagnostics.EventLog.WriteEntry(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME,
                statement,
                EventLogEntryType.Information);
        }


        public static string getPrefDump(SearchPreferenceCollection searchPrefs)
        {
            if (searchPrefs == null || searchPrefs.Count == 0)
            {
                return "searchPref empty or null";
            }
            StringBuilder sb = new StringBuilder(100);
            foreach (DictionaryEntry sp in searchPrefs)
            {
                if (sb == null)
                {
                    sb.Append("[<NULL> sp]");
                    continue;
                }
                sb.Append("[");
                sb.Append((sp.Key == null) ? "NULL" : sp.Key.ToString());
                sb.Append(" = ");
                sb.Append((sp.Value == null) ? "NULL" : sp.Value.ToString());
                sb.Append("]");
            }
            return sb.ToString();
        }


        #endregion helpers

        #region instrumentation
        public static void PerfCounterInstall()
        {
            CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
            ccdc.AddRange(new CounterCreationData[] {	
														 new CounterCreationData("Matches processed/second", "Matches processed/second", PerformanceCounterType.RateOfCountsPerSecond32),
														 new CounterCreationData("Have Stored SearchPref Total", "Count of people with stored search pref", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("No SearchPref Total", "Count of people with no stored search pref", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("Items Processed Total", "Cont Total Items Processed", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("Query1 Total", "Query Step 1 Total", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("Query2 Total", "Query Step 2 Total", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("Query3 Total", "Query Step 3 Total", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("Query1 No Results", "Query Step 1 Returned No Results", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("Query2 No Results", "Query Step 1 Returned No Results", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("Query3 No Results", "Query Step 1 Returned No Results", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("Total Sent", "Total BulkMail sent to external mail svc", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("Total Sent/second", "Rate of BulkMail sent/second to external mail svc", PerformanceCounterType.RateOfCountsPerSecond32),
														 new CounterCreationData("Avg Search Time","Average time spent from call to search to receipt of results", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg Search Base","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg Scrub Time","Avg time scrubbing results", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg Scrub Base","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg GetQueueItem Time","Avg time to get queue item", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg GetQueueItem Base","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg Populate Results Time","Avg time ", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg Populate Results Base","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg SendToXmail Time","Avg time sending to external mail", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg SendToXmail Base","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg Save mnAlert Time","Avg time saving an async call to hydra", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg Save mnAlert Base","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg GetMember Time","Avg time getting member from Member svc", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg GetMemberBase","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg Build Default Pref Time","Avg time Building default search prefs", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg Build Default Pref Base","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg Adjust Scrublist Time","Avg time adding / building  scrublist", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg Adjust Scrublist Base","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg Slowdown Time","Avg time waiting, slowdown", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg Slowdown Base","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg Cycle Time","Avg time for a full cycle. This is a full loop of sending BulkMail to one person.", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg Cycle Base","Base", PerformanceCounterType.AverageBase)

													 });

            PerformanceCounterCategory.Create(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, ccdc);
        }


        public static void PerfCounterUninstall()
        {
            if (PerformanceCounterCategory.Exists(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME))
            {
                PerformanceCounterCategory.Delete(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME);
            }
        }


        private void initPerfCounters()
        {
            _perfMatchRate = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Matches processed/second", false);
            _perfHaveStoredSearchPrefTotal = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Have Stored SearchPref Total", false);
            _perfNoSearchPrefTotal = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "No SearchPref Total", false);
            _perfItemsProcessedTotal = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Items Processed Total", false);
            _perfQuery1Total = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Query1 Total", false);
            _perfQuery2Total = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Query2 Total", false);
            _perfQuery3Total = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Query3 Total", false);
            _perfQuery1NoResults = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Query1 No Results", false);
            _perfQuery2NoResults = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Query2 No Results", false);
            _perfQuery3NoResults = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Query3 No Results", false);
            _perfTotalSent = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Total Sent", false);
            _perfTotalSentRate = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Total Sent/second", false);
            _perfTotalRegulated = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Total Regulated", false);
            _perfTotalRegulatedRate = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Total Regulated/second", false);
            _perfAvgSearchTime = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Search Time", false);
            _perfAvgSearchTimeBase = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Search Base", false);
            _perfAvgScrubTime = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Scrub Time", false);
            _perfAvgScrubTimeBase = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Scrub Base", false);
            _perfAvgGetQueueItemTime = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg GetQueueItem Time", false);
            _perfAvgGetQueueItemTimeBase = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg GetQueueItem Base", false);
            _perfAvgPopulateResultsTime = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Populate Results Time", false);
            _perfAvgPopulateResultsTimeBase = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Populate Results Base", false);
            _perfAvgSendToXmailTime = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg SendToXmail Time", false);
            _perfAvgSendToXmailTimeBase = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg SendToXmail Base", false);
            _perfAvgSavemnAlertTime = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Save mnAlert Time", false);
            _perfAvgSavemnAlertTimeBase = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Save mnAlert Base", false);
            _perfAvgGetMemberTime = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg GetMember Time", false);
            _perfAvgGetMemberTimeBase = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg GetMemberBase", false);
            _perfAvgBuildDefaultPrefTime = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Build Default Pref Time", false);
            _perfAvgBuildDefaultPrefTimeBase = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Build Default Pref Base", false);
            _perfAvgAdjustScrublistTime = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Adjust Scrublist Time", false);
            _perfAvgAdjustScrublistTimeBase = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Adjust Scrublist Base", false);
            _perfAvgSlowdownTime = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Slowdown Time", false);
            _perfAvgSlowdownTimeBase = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Slowdown Base", false);
            _perfAvgCycleTime = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Cycle Time", false);
            _perfAvgCycleTimeBase = new PerformanceCounter(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Cycle Base", false);

            resetPerfCounters();
        }


        private void resetPerfCounters()
        {
            _perfMatchRate.RawValue = 0;
            _perfHaveStoredSearchPrefTotal.RawValue = 0;
            _perfNoSearchPrefTotal.RawValue = 0;
            _perfItemsProcessedTotal.RawValue = 0;
            _perfQuery1Total.RawValue = 0;
            _perfQuery2Total.RawValue = 0;
            _perfQuery3Total.RawValue = 0;
            _perfQuery1NoResults.RawValue = 0;
            _perfQuery2NoResults.RawValue = 0;
            _perfQuery3NoResults.RawValue = 0;
            _perfTotalSent.RawValue = 0;
            _perfTotalSentRate.RawValue = 0;
            _perfTotalRegulated.RawValue = 0;
            _perfTotalRegulatedRate.RawValue = 0;
            _perfAvgSearchTime.RawValue = 0;
            _perfAvgSearchTimeBase.RawValue = 0;
            _perfAvgScrubTime.RawValue = 0;
            _perfAvgScrubTimeBase.RawValue = 0;
            _perfAvgGetQueueItemTime.RawValue = 0;
            _perfAvgGetQueueItemTimeBase.RawValue = 0;
            _perfAvgPopulateResultsTime.RawValue = 0;
            _perfAvgPopulateResultsTimeBase.RawValue = 0;
            _perfAvgSendToXmailTime.RawValue = 0;
            _perfAvgSendToXmailTimeBase.RawValue = 0;
            _perfAvgSavemnAlertTime.RawValue = 0;
            _perfAvgSavemnAlertTimeBase.RawValue = 0;
            _perfAvgGetMemberTime.RawValue = 0;
            _perfAvgGetMemberTimeBase.RawValue = 0;
            _perfAvgBuildDefaultPrefTime.RawValue = 0;
            _perfAvgBuildDefaultPrefTimeBase.RawValue = 0;
            _perfAvgAdjustScrublistTime.RawValue = 0;
            _perfAvgAdjustScrublistTimeBase.RawValue = 0;
            _perfAvgSlowdownTime.RawValue = 0;
            _perfAvgSlowdownTimeBase.RawValue = 0;
            _perfAvgCycleTime.RawValue = 0;
            _perfAvgCycleTimeBase.RawValue = 0;

        }
        #endregion
    }
}
