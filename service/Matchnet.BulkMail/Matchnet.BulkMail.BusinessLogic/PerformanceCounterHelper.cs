using System;
using System.Diagnostics;

namespace Matchnet.BulkMail.BusinessLogic
{
	/// <summary>
	/// Summary description for PerformanceCounterHelper.
	/// </summary>
	public class PerformanceCounterHelper
	{
		private DateTime _Start;
		public PerformanceCounterHelper()
		{
			StartTiming();
		}

		public void StartTiming()
		{
			_Start = DateTime.Now;		
		}

		public void EndTiming(PerformanceCounter averageCounter, PerformanceCounter averageCounterBase )
		{
			averageCounter.IncrementBy( Convert.ToInt64(DateTime.Now.Subtract(_Start).TotalMilliseconds) );
			averageCounterBase.Increment();
		}
	}
}
