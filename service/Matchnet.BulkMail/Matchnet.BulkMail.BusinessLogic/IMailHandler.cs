﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.EmailLibrary;

namespace Matchnet.BulkMail.BusinessLogic
{
    /// <summary>
    /// Interface for all mail handlers
    /// </summary>
    public interface IMailHandler
    {
        AllowedDomains AllowedDomainsProperty
        {
            set;
        }

        /// <summary>
        /// Execute the appropriate action based on the impulse type
        /// </summary>
        /// <param name="impulse"></param>
        void SendEmail(ImpulseBase impulse);
    }
}
