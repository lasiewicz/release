﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.List.ValueObjects;
using Matchnet.List.ServiceAdapters;
using System.Collections;
using Matchnet.EmailLibrary;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using System.Web;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Data;
using System.Data;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.ExternalMail.ValueObjects.BulkMail;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ValueObjects.Impulse;

namespace Matchnet.BulkMail.BusinessLogic
{
    public class YesMailHandler : IMailHandler
    {
        #region Members

        private static YesMailHandler _instance;
        private static Object _syncBlock = new object();
        private AllowedDomains _allowedDomains;

        #endregion

        #region Constants

        private const Int32 EMAIL_VERIFIED = 4;
        private const int XOR_SALT_ADD_VALUE = 42;
        private const string DELIMITER = @"|";
        private string[] SEARCH_FIELDS_FOR_URL = new string[]
			{
				"SearchTypeID", "Distance", "MinAge", "MaxAge",
				"MinHeight", "MaxHeight", "EducationLevel", "Religion",
				"Ethnicity", "SmokingHabits", "DrinkingHabits", "HasPhotoFlag",
				"MajorType", "RelationshipMask", "RelationShipStatus", "RegionID",
				"SearchOrderBy"
			};

        #endregion

        #region Singleton stuff

        /// <summary>
        /// Singleton instance
        /// </summary>
        public static YesMailHandler Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncBlock)
                    {
                        if (_instance == null)
                        {
                            _instance = new YesMailHandler();
                        }
                    }
                }
                return _instance;
            }
        }

        /// <summary>
        /// Private C'tor for Singleton
        /// </summary>
        private YesMailHandler()
        {

        }

        #endregion


        #region IMailHandler implementation
        public AllowedDomains AllowedDomainsProperty
        {
            set
            {
                _allowedDomains = value;
            }
        }
        
        /// <summary>
        /// Sends a type of bulk mail
        /// </summary>
        /// <param name="impulse"></param>
        public void SendEmail(ImpulseBase impulse)
        {
            if (impulse is MatchMailImpulse)
            {
                sendMatchMail(impulse);
            }
            else if (impulse is NewMemberMailImpulse)
            {
                sendNewMemberMail(impulse);
            }
        }

        #endregion

        #region Mail handling for 2 bulk mail types
        
        private void sendMatchMail(ImpulseBase impulseBase)
        {
            MatchMailImpulse impulse = (MatchMailImpulse)impulseBase;

            sendMatchToDestination(BrandConfigSA.Instance.GetBrandByID(impulse.BrandID), impulse.RecipientEmailAddress,
                impulse.RecipientMiniprofileInfo, impulse.TargetMiniprofileInfoCollection, impulse.SearchPrefType);
        }

        private void sendNewMemberMail(ImpulseBase impulseBase)
        {
            NewMemberMailImpulse impulse = (NewMemberMailImpulse)impulseBase;

            sendNewMemberMailToDestination(BrandConfigSA.Instance.GetBrandByID(impulse.BrandID), impulse.RecipientEmailAddress,
                impulse.RecipientMiniprofileInfo, impulse.TargetMiniprofileInfoCollection);
        }

        private void sendMatchToDestination(Brand brand, string emailAddress, MiniprofileInfo recipientMiniprofileInfo, MiniprofileInfoCollection targetMiniprofileInfoCollection, string searchPrefType)
        {

            int counter;

            ListCountCollection listCountCollection = ListSA.Instance.GetListCounts(recipientMiniprofileInfo.MemberID, brand.Site.Community.CommunityID, brand.Site.SiteID);

            // 79 is the number of total tokens passed into YesMail.
            SortedList tokens = new SortedList(79);

            tokens.Add("03_EmailAddress", MailHandlerUtils.Filter(emailAddress));
            tokens.Add("01_SiteID", brand.Site.SiteID.ToString());
            tokens.Add("02_MemberID", recipientMiniprofileInfo.MemberID.ToString());
            tokens.Add("16_UserName", MailHandlerUtils.Filter(recipientMiniprofileInfo.UserName));
            tokens.Add("23_UserAge", EmailMemberHelper.DetermineMemberAge(recipientMiniprofileInfo.BirthDate));
            tokens.Add("30_MemberLocation", EmailMemberHelper.DetermineMemberRegionDisplay(recipientMiniprofileInfo.RegionID, brand));

            counter = 1;
            foreach (MiniprofileInfo currentMember in targetMiniprofileInfoCollection)
            {
                int matchMemberIDOrder = 4;
                string order = String.Format("{0:0#}", matchMemberIDOrder + counter - 1);
                tokens.Add(order + "_Match" + counter++ + "MemberID", currentMember.MemberID.ToString());
            }

            counter = 1;
            foreach (MiniprofileInfo currentMember in targetMiniprofileInfoCollection)
            {
                int matchNameOrder = 10;
                string order = String.Format("{0:0#}", matchNameOrder + counter - 1);
                tokens.Add(order + "_Match" + counter++ + "Name", MailHandlerUtils.Filter(currentMember.UserName));
            }

            counter = 1;
            foreach (MiniprofileInfo currentMember in targetMiniprofileInfoCollection)
            {
                int age = EmailMemberHelper.DetermineMemberAge(currentMember.BirthDate);

                int matchAgeOrder = 17;
                string order = String.Format("{0:0#}", matchAgeOrder + counter - 1);
                tokens.Add(order + "_Match" + counter++ + "Age", age);
            }

            counter = 1;
            foreach (MiniprofileInfo currentMember in targetMiniprofileInfoCollection)
            {
                string regionDisplay = EmailMemberHelper.DetermineMemberRegionDisplay(currentMember.RegionID, brand);

                int matchLocationOrder = 24;
                string order = String.Format("{0:0#}", matchLocationOrder + counter - 1);
                tokens.Add(order + "_Match" + counter++ + "Location", MailHandlerUtils.Filter(regionDisplay));
            }

            counter = 1;
            foreach (MiniprofileInfo currentMember in targetMiniprofileInfoCollection)
            {
                string thumbnail = EmailMemberHelper.DetermineBaseNoPhoto(brand);

                if (currentMember.ThumbNailWebPath != Constants.NULL_STRING)
                {
                    thumbnail = currentMember.ThumbNailWebPath;
                }

                int matchThumbNailOrder = 31;
                string order = String.Format("{0:0#}", matchThumbNailOrder + counter - 1);
                tokens.Add(order + "_Match" + counter++ + "ThumbNail", (MailHandlerUtils.Filter(thumbnail)));

            }

            string recipientThumbnail = EmailMemberHelper.DetermineBaseNoPhoto(brand);

            if (recipientMiniprofileInfo.ThumbNailWebPath != Constants.NULL_STRING)
            {
                recipientThumbnail = recipientMiniprofileInfo.ThumbNailWebPath;
            }

            tokens.Add("37_UserThumbNail", MailHandlerUtils.Filter(recipientThumbnail));

            #region Append HotList Tokens (You XXed others)

            tokens.Add("38_NumberYouHotlisted", listCountCollection[HotListCategory.Default]);
            tokens.Add("42_NumberYouContacted", listCountCollection[HotListCategory.MembersYouEmailed]);
            tokens.Add("39_NumberYouTeased", listCountCollection[HotListCategory.MembersYouTeased]);
            tokens.Add("40_NumberYouIM", listCountCollection[HotListCategory.MembersYouIMed]);
            tokens.Add("41_NumberYouViewed", listCountCollection[HotListCategory.MembersYouViewed]);

            #endregion

            tokens.Add("43_MemberSearchURL", MailHandlerUtils.Filter(MailHandlerUtils.DetermineSearchUrl(recipientMiniprofileInfo.MemberID,
                BrandConfigSA.Instance.GetBrandByID(brand.BrandID).Site.Community.CommunityID)));

            // Append View Profile MMVID (Enables hot listing w/o logging in)
            counter = 1;
            foreach (MiniprofileInfo currentMember in targetMiniprofileInfoCollection)
            {
                int matchMMVIDOrder = 44;
                string order = String.Format("{0:0#}", matchMMVIDOrder + counter - 1);
                tokens.Add(order + "_MMVID" + counter++, MailHandlerUtils.GetMMVID(recipientMiniprofileInfo.MemberID, currentMember.MemberID));
            }

            #region Append Reverse HotList Tokens (Who XXed you)

            tokens.Add("50_NumberHotlistedYou", listCountCollection[HotListCategory.WhoAddedYouToTheirFavorites]);
            tokens.Add("54_NumberContactedYou", listCountCollection[HotListCategory.WhoEmailedYou]);
            tokens.Add("51_NumberTeasedYou", listCountCollection[HotListCategory.WhoTeasedYou]);
            tokens.Add("52_NumberIMYou", listCountCollection[HotListCategory.WhoIMedYou]);
            tokens.Add("53_NumberViewedYou", listCountCollection[HotListCategory.WhoViewedYourProfile]);

            #endregion

            #region V1.3 #17638 Additional tokens for banners

            //Religion
            tokens.Add("55_Religion", recipientMiniprofileInfo.ReligionID.ToString());

            //JDateReligion
            tokens.Add("56_JdateReligion", recipientMiniprofileInfo.JdateReligionID.ToString());

            //Ethnicity
            tokens.Add("57_Ethnicity", recipientMiniprofileInfo.EthnicityID.ToString());

            //JdateEthnicity
            tokens.Add("58_JdateEthnicity", recipientMiniprofileInfo.JdateEthnicityID.ToString());

            //MaritalStatus
            tokens.Add("59_MaritalStatus", recipientMiniprofileInfo.MaritalStatusID.ToString());

            //ChildrenCount
            tokens.Add("60_ChildrenCount", recipientMiniprofileInfo.ChildrenCount.ToString());

            //Custody
            tokens.Add("61_Custody", recipientMiniprofileInfo.CustodyID.ToString());

            //IsPayingMember
            tokens.Add("62_IsPayingMember", recipientMiniprofileInfo.IsPayingMember.ToString());

            //IsVerifiedEamil
            bool isVerifiedEmail = false;
            int globalStatusMask = recipientMiniprofileInfo.GlobalStatusMask;
            if ((globalStatusMask != Constants.NULL_INT) && ((globalStatusMask & EMAIL_VERIFIED) == EMAIL_VERIFIED))
            {
                isVerifiedEmail = true;
            }
            tokens.Add("63_IsVerifiedEmail", isVerifiedEmail.ToString());


            //HasApprovedPhoto
            tokens.Add("64_HasApprovedPhoto", recipientMiniprofileInfo.HasApprovedPhotos.ToString());

            //Gender
            tokens.Add("65_Gender", MailHandlerUtils.GetGenderMaskSelf(recipientMiniprofileInfo.GenderMask).ToString());

            //SeekingGender
            tokens.Add("66_SeekingGender", MailHandlerUtils.GetGenderMaskSeeking(recipientMiniprofileInfo.GenderMask).ToString());

            //EmailVerificationCode
            tokens.Add("67_EmailVerificationCode", EmailVerify.HashMemberID(recipientMiniprofileInfo.MemberID).ToString());

            bool moreMatchData = Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MATCHMAIL_INCLUDE_MORE_DATA"));

            if (moreMatchData && targetMiniprofileInfoCollection.Item(0) != null)
            {
                tokens.Add("68_MM1Generic", MailHandlerUtils.GetMatchText(recipientMiniprofileInfo, targetMiniprofileInfoCollection.Item(0), brand));
            }
            else
            {
                tokens.Add("68_MM1Generic", string.Empty);
            }

            if (moreMatchData && targetMiniprofileInfoCollection.Item(1) != null)
            {
                tokens.Add("69_MM2Generic", MailHandlerUtils.GetMatchText(recipientMiniprofileInfo, targetMiniprofileInfoCollection.Item(1), brand));
            }
            else
            {
                tokens.Add("69_MM2Generic", string.Empty);
            }

            tokens.Add("70_MM3Generic", searchPrefType); //strict or relax related info
            tokens.Add("71_MM4Generic", string.Empty);
            tokens.Add("72_MM5Generic", string.Empty);
            tokens.Add("73_MM6Generic", string.Empty);
            tokens.Add("74_MM7Generic", string.Empty);
            tokens.Add("75_MM8Generic", string.Empty);
            tokens.Add("76_MM9Generic", string.Empty);
            tokens.Add("77_MM10Generic", string.Empty);
            tokens.Add("78_MM11Generic", string.Empty);
            tokens.Add("79_MM12Generic", string.Empty);

            #endregion

            Command command = new Command("mnFileExchange", "up_BulkMail_Save", 0);

            command.AddParameter("@BulkMailTypeID", SqlDbType.Int, ParameterDirection.Input, (int)BulkMailType.MatchMail);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, recipientMiniprofileInfo.MemberID);
            command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, brand.Site.SiteID);
            command.AddParameter("@EmailAddress", SqlDbType.VarChar, ParameterDirection.Input, emailAddress);

            string delimitedTokenValues = string.Empty;
            IList tokenValueList = tokens.GetValueList();
            for (int i = 0; i < tokens.Count; i++)
            {
                // Per YesMail's request, we need to replace out null int value with an empty value.
                delimitedTokenValues += Convert.ToString(tokenValueList[i]).Replace(Matchnet.Constants.NULL_INT.ToString(), String.Empty) + DELIMITER;
            }
            delimitedTokenValues = delimitedTokenValues.Remove(delimitedTokenValues.Length - 1, 1);

#if DEBUG
            string delimitedTokenKeys = string.Empty;
            IList tokenKeyList = tokens.GetKeyList();
            for (int i = 0; i < tokens.Count; i++)
            {
                delimitedTokenKeys += Convert.ToString(tokenKeyList[i]).Substring(3, Convert.ToString(tokenKeyList[i]).Length - 3) + DELIMITER;
            }
            delimitedTokenKeys = delimitedTokenKeys.Remove(delimitedTokenKeys.Length - 1, 1);

            System.Diagnostics.Trace.WriteLine("-- begin tokens --");
            System.Diagnostics.Trace.WriteLine(delimitedTokenKeys);
            System.Diagnostics.Trace.WriteLine(delimitedTokenValues);
            System.Diagnostics.Trace.WriteLine("--end token values--");
#endif

            command.AddParameter("@TokenValues", SqlDbType.NVarChar, ParameterDirection.Input, delimitedTokenValues);

            try
            {
                if (_allowedDomains.IsAllowedDomain(emailAddress))
                {
                    Client.Instance.ExecuteAsyncWrite(command);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error writing Bulkmail entry. [MemberID:" + recipientMiniprofileInfo.MemberID.ToString() +
                    ", SiteID:" + brand.Site.SiteID.ToString() + ", EmailAddress:" + emailAddress + ", DelimitedTokens:" +
                    delimitedTokenValues, ex);
            }
        }

        private void sendNewMemberMailToDestination(Brand brand, string emailAddress, MiniprofileInfo recipientMiniprofileInfo, MiniprofileInfoCollection members)
        {
            int counter;
            int tokenLineCounter;
            string tokenConstant = "UT_Generic";
            string tokenName;

            ArrayList tokens = new ArrayList();
            tokens.Insert(0, new Token("EmailAddress", MailHandlerUtils.Filter(emailAddress)));
            tokens.Insert(1, new Token("NMIA_TimeStamp", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)));
            tokens.Insert(2, new Token("generic_num3", members.Count.ToString()));
            //this field is reserved for future use, right now hard code it to zero
            tokens.Insert(3, new Token("generic_num4", "0"));

            counter = 0;
            tokenLineCounter = 3;
            foreach (MiniprofileInfo currentMember in members)
            {
                string regionDisplay = EmailMemberHelper.DetermineMemberRegionDisplay(currentMember.RegionID, brand);
                string userName = currentMember.UserName;
                string age = MailHandlerUtils.GetAge(currentMember.BirthDate).ToString();
                string imageURL = currentMember.ThumbNailWebPath;
                string memberID = currentMember.MemberID.ToString();

                counter++;
                tokenLineCounter++;
                tokenName = tokenConstant + counter.ToString();
                tokens.Insert(tokenLineCounter, new Token(tokenName, userName));

                counter++;
                tokenLineCounter++;
                tokenName = tokenConstant + counter.ToString();
                tokens.Insert(tokenLineCounter, new Token(tokenName, age));

                counter++;
                tokenLineCounter++;
                tokenName = tokenConstant + counter.ToString();
                tokens.Insert(tokenLineCounter, new Token(tokenName, regionDisplay));

                counter++;
                tokenLineCounter++;
                tokenName = tokenConstant + counter.ToString();
                tokens.Insert(tokenLineCounter, new Token(tokenName, imageURL));

                counter++;
                tokenLineCounter++;
                tokenName = tokenConstant + counter.ToString();
                tokens.Insert(tokenLineCounter, new Token(tokenName, memberID));
            }

            while (counter < 20)
            {
                counter++;
                tokenLineCounter++;
                tokenName = tokenConstant + counter.ToString();
                tokens.Insert(tokenLineCounter, new Token(tokenName, string.Empty));
            }

            string tokenKey = string.Empty;
            string tokenValue = string.Empty;
            string tokenDBValue = string.Empty;
            int lineNumber = 0;

            //for ( int i = 0; i < tokenValues.Count; i++ )  
            foreach (Token token in tokens)
            {
                lineNumber++;
                tokenKey = token.Name;
                tokenValue = token.Value.Replace(Matchnet.Constants.NULL_INT.ToString(), String.Empty);

                Command command = new Command("mnFileExchange", "up_UserExportData_Insert", 0);

                command.AddParameter("@BulkMailTypeID", SqlDbType.Int, ParameterDirection.Input, BulkMailType.NewMembers);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, recipientMiniprofileInfo.MemberID);
                command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, brand.Site.SiteID);
                command.AddParameter("@LineOrderID", SqlDbType.Int, ParameterDirection.Input, lineNumber);
                command.AddParameter("@AttributeName", SqlDbType.VarChar, ParameterDirection.Input, tokenKey);
                command.AddParameter("@AttributeValue", SqlDbType.NVarChar, ParameterDirection.Input, tokenValue);

                try
                {
                    if (_allowedDomains.IsAllowedDomain(emailAddress))
                    {
                        Client.Instance.ExecuteAsyncWrite(command);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error writing Bulkmail entry. [MemberID:" + recipientMiniprofileInfo.MemberID.ToString() +
                        ", SiteID:" + brand.Site.SiteID.ToString() + ", EmailAddress:" + emailAddress, ex);
                }
            }

#if DEBUG
            string delimitedTokenValues = string.Empty;
            string delimitedTokenKeys = string.Empty;

            foreach (Token token in tokens)
            {
                delimitedTokenValues += Convert.ToString(token.Value).Replace(Matchnet.Constants.NULL_INT.ToString(), String.Empty) + DELIMITER;
                delimitedTokenKeys += Convert.ToString(token.Name).Substring(3, Convert.ToString(token.Name).Length - 3) + DELIMITER;
            }

            System.Diagnostics.Trace.WriteLine("-- begin tokens --");
            System.Diagnostics.Trace.WriteLine(delimitedTokenKeys);
            System.Diagnostics.Trace.WriteLine(delimitedTokenValues);
            System.Diagnostics.Trace.WriteLine("--end token values--");
#endif
        }

        #endregion

    }
}
