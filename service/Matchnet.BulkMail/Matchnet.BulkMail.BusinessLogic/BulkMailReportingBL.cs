﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Messaging;
using System.Threading;
using System.Text;
using System.IO;
using System.Web;
using System.Linq;


using Matchnet;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.BulkMail.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Search.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

namespace Matchnet.BulkMail.BusinessLogic
{
    public class BulkMailReportingBL
    {
        public static readonly BulkMailReportingBL Instance = new BulkMailReportingBL();

        public SearchPreferenceCollection GetLaxSearchPreference(int memberID, Brand brand, SearchPreferenceCollection strictPreferences)
        {
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(memberID, MemberLoadFlags.None);
            return GetLaxSearchPreference(member, brand, strictPreferences);
        }

        public SearchPreferenceCollection GetLaxSearchPreference(IMemberDTO member, Brand brand, SearchPreferenceCollection strictPreferences)
        {
            SearchPreferenceCollection laxPreferences = null;
            try
            {
                laxPreferences =  MailHandlerUtils.GetLaxSearchPreference(member, brand, strictPreferences);
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(Matchnet.BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME,
                    "getLaxSearchPrefs could not extract birthdate for " + member.MemberID.ToString() + " CommunityID " + brand.Site.Community.CommunityID.ToString(),
                    ex);
            }

            return laxPreferences;
        }

        public BulkMailBatch GetBulkMailBatchForDate(DateTime date)
        {
            BulkMailBatch batch = null;

            try
            {
                Command command = new Command(Matchnet.BulkMail.ValueObjects.ServiceConstants.HYDRA_SAVE_NAME, "up_BulkMail_Batch_List_For_Date", 0);
                command.AddParameter("@BatchDate", SqlDbType.Date, ParameterDirection.Input, date);
                DataSet dataSet = Client.Instance.ExecuteDataSet(command);
                if (dataSet != null && dataSet.Tables[0] != null && dataSet.Tables[0].Rows.Count > 0 
                    && dataSet.Tables[1] != null && dataSet.Tables[1].Rows.Count > 0)
                {
                    batch = dataSetToBulkMailBatch(dataSet);
                }
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "GetBulkMailBatchForDate failed for " + date.ToString(), ex, false);
            }

            return batch;
        }

        public BulkMailScheduleRecord GetBulkMailScheduleRecord(int memberID, int communityID, BulkMailType bulkMailType)
        {
            BulkMailScheduleRecord record = null;

            try
            {
                Command command = new Command(Matchnet.BulkMail.ValueObjects.ServiceConstants.HYDRA_SAVE_NAME, "up_BulkMailSchedule_List_Schedule_For_Member", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                command.AddParameter("@BulkMailTypeID", SqlDbType.Int, ParameterDirection.Input, (int)bulkMailType);
                DataTable dataTable = Client.Instance.ExecuteDataTable(command);
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    record = dataRowToScheduleRecord(dataTable.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(BulkMail.ValueObjects.ServiceConstants.SERVICE_NAME, "GetBulkMailScheduleRecord failed for " + memberID.ToString(), ex, false);
            }
            return record;
        }

        private BulkMailScheduleRecord dataRowToScheduleRecord(DataRow dr)
        {
            int memberID = Convert.ToInt32(dr["MemberID"]);
            int communityID = Convert.ToInt32(dr["CommunityID"]);
            BulkMailType bulkMailTypeID = (BulkMailType)Convert.ToInt32(dr["BulkMailTypeID"]);
            int frequency = Convert.ToInt32(dr["Frequency"]);
            DateTime lastSentDate = dr["LastSentDate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dr["LastSentDate"]);
            DateTime lastAttemptDate = dr["LastAttemptDate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dr["LastAttemptDate"]);
            DateTime nextAttemptDate = Convert.ToDateTime(dr["NextAttemptDate"]);
            int sentCount = Convert.ToInt32(dr["SentCount"]);
            DateTime frequencyUpdateDate = dr["FrequencyUpdateDate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dr["FrequencyUpdateDate"]);
            DateTime insertDate = Convert.ToDateTime(dr["InsertDate"]);
            string sentMemberIDList = dr["SentMemberIDList"] == DBNull.Value ? string.Empty : dr["SentMemberIDList"].ToString();
            return new BulkMailScheduleRecord(memberID, communityID, bulkMailTypeID, frequency, lastSentDate, lastAttemptDate, nextAttemptDate, sentCount, frequencyUpdateDate, insertDate, sentMemberIDList);
        }


        private BulkMailBatch dataSetToBulkMailBatch(DataSet dataSet)
        {
            List<BulkMailBatchSummaryRecord> summaryRecords = new List<BulkMailBatchSummaryRecord>();
            foreach (DataRow dataRow in dataSet.Tables[1].Rows)
            {
                summaryRecords.Add(drToSummaryRecord(dataRow));
            }

            int batchID = Convert.ToInt32(dataSet.Tables[0].Rows[0]["BulkMailBatchID"]);
            int recordsProcessed = Convert.ToInt32(dataSet.Tables[0].Rows[0]["RecordsProcessed"]);
            DateTime runDate = Convert.ToDateTime(dataSet.Tables[0].Rows[0]["RunDate"]);
            int runTime = Convert.ToInt32(dataSet.Tables[0].Rows[0]["RunTimeMinutes"]);

            return new BulkMailBatch(batchID, recordsProcessed, runTime, runDate, summaryRecords);

        }

        private BulkMailBatchSummaryRecord drToSummaryRecord(DataRow row)
        {
            int batchID = Convert.ToInt32(row["BulkMailBatchID"]);
            int recordCount = Convert.ToInt32(row["RecordCount"]);
            ProcessingStatusID processingStatus = (ProcessingStatusID) Convert.ToInt32(row["ProcessingStatusID"]);
            string processingServer = row["ProcessingServer"].ToString();
            return new BulkMailBatchSummaryRecord(batchID, recordCount, processingStatus, processingServer);
        }
    }
}
