﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Matchnet.BulkMail.ValueObjects;
using Matchnet.BulkMail.ValueObjects.ServiceDefinitions;
using Matchnet.BulkMail.BusinessLogic;
using Matchnet.Exceptions;
using Matchnet.Data.Hydra;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.Search.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Search.ValueObjects.ServiceDefinitions;

namespace Matchnet.BulkMail.ServiceManagers
{
    public class BulkMailReportingSM : MarshalByRefObject, IServiceManager, IBulkMailReporting, IDisposable
    {
        #region IServiceManager Members
        
        public BulkMailReportingSM()
        {
        }

        public void PrePopulateCache()
        {
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
        }

        #endregion

        public override object InitializeLifetimeService()
        {
            return null;
        }

        public BulkMailBatch GetBulkMailBatchForDate(DateTime date)
        {
            return BulkMailReportingBL.Instance.GetBulkMailBatchForDate(date);
        }

        public BulkMailScheduleRecord GetBulkMailScheduleRecord(int memberID, int communityID, BulkMailType bulkMailType)
        {
            return BulkMailReportingBL.Instance.GetBulkMailScheduleRecord(memberID, communityID, bulkMailType);
        }

        public SearchPreferenceCollection GetLaxSearchPreference(int memberID, Brand brand, SearchPreferenceCollection strictPreferences)
        {
            return BulkMailReportingBL.Instance.GetLaxSearchPreference(memberID, brand, strictPreferences);
        }

        public MatchResult RunMatchSimulation(int memberID, BulkMailQueueItem queueItem, Brand brand, ScrubList scrublist)
        {
            IMemberSearchSA searchService = (IMemberSearchSA)Matchnet.Search.ServiceAdapters.MemberSearchSA.Instance;
            IGetMember memberService = (IGetMember)Matchnet.Member.ServiceAdapters.MemberSA.Instance;
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(memberService, memberID, MemberLoadFlags.IngoreSACache);

            BulkMailBL bulkMailBL = new BulkMailBL();
            bulkMailBL.Initialize();
            bulkMailBL.IsDebug = true;
            
            return bulkMailBL.ProcessMatchMailQueueItem(new PerformanceCounterHelper(),
                queueItem,
                member,
                brand,
                scrublist,
                memberService,
                searchService);
        }
    }
}
