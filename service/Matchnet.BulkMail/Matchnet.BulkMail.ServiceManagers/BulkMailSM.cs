﻿using System;
using System.Text;
using System.Collections.Generic;
using Matchnet.BulkMail.ValueObjects;
using Matchnet.BulkMail.BusinessLogic;
using Matchnet.Exceptions;
using Matchnet.Data.Hydra;

namespace Matchnet.BulkMail.ServiceManagers
{
    public class BulkMailSM : MarshalByRefObject, IServiceManager
    {
        #region IServiceManager Members
        private BulkMailBL _bulkMailBL;
        private HydraWriter _hydraWriter;

        public BulkMailSM()
        {
            _hydraWriter = new HydraWriter(ServiceConstants.HYDRA_SAVE_NAME, "mnFileExchange");
            _bulkMailBL = new BulkMailBL();
        }

        public void PrePopulateCache()
        {
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
        }

        #endregion

        public void Start()
        {
            _hydraWriter.Start();
            _bulkMailBL.Start();
        }

        public void Stop()
        {
            _hydraWriter.Stop();
            _bulkMailBL.Stop();
        }

        public override object InitializeLifetimeService()
        {
            return null;
        }
    }
}
