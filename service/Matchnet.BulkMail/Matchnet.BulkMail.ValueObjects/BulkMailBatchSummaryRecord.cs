﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.BulkMail.ValueObjects
{
    [Serializable]
    public class BulkMailBatchSummaryRecord
    {
        public int BatchID { get; set; }
        public int RecordCount { get; set; }
        public ProcessingStatusID ProcessingStatus { get; set; }
        public string ProcessingServer { get; set; }

        public BulkMailBatchSummaryRecord() { }

        public BulkMailBatchSummaryRecord(int batchID, int recordCount, ProcessingStatusID processingStatus, string processingServer)
        {
            BatchID = batchID;
            RecordCount = recordCount;
            ProcessingStatus = processingStatus;
            ProcessingServer = processingServer;
        }
    }
}
