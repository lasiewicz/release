﻿using System;
using System.Collections;

using Matchnet.Search.ValueObjects;
using Matchnet.ExternalMail.ValueObjects;

namespace Matchnet.BulkMail.ValueObjects
{
    public enum ProcessingStatusID
    {
        Waiting=1,
        Processing=2,
        Processed=3,
        ProcessedNoUpdate=4,
        ProcessedErrorGettingMember=5, 
        ProcessedErrorNoEmailAddress=6,
        ProcessedErrorNoBrand=7,
        ProcessedErrorGeneral=8,
        ProcessedOnDNE=9,
        SentMatchesWithStoredPrefs=10,
        SentMatchesWithDefaultPrefs=11,
        SentMatchesWithLaxPrefs=12,
        SentMatchesWithLaxPrefsWithAgeFilter=13,
        SentMatchesWithStoredPrefsWithAgeFilter = 14,
        SentMatchesWithDefaultPrefsWithAgeFilter = 15
    }

    [Serializable]
    public class BulkMailQueueItem : IValueObject
    {
        /*

        SentMemberIDList
        */
        private Int32 _MemberID;
        private Int32 _CommunityID;
        private BulkMailType _BulkMailTypeID;
        private byte _Frequency;
        private DateTime _LastSentDate;
        private DateTime _LastAttemptDate;
        private DateTime _NextAttemptDate;
        private Int32 _SentCount;
        private DateTime _FrequencyUpdateDate;
        private DateTime _InsertDate;
        private String[] _SentMemberIDList;

        private SearchPreferenceCollection _SearchPreferences;

        public BulkMailQueueItem(
            Int32 memberID,
            Int32 communityID,
            BulkMailType bulkMailTypeID,
            byte frequency,
            DateTime lastSentDate,
            DateTime lastAttemptDate,
            DateTime nextAttemptDate,
            Int32 sentCount,
            DateTime frequencyUpdateDate,
            DateTime insertDate,

            String[] sentMemberIDList,
            SearchPreferenceCollection searchPreferences)
        {
            _MemberID = memberID;
            _CommunityID = communityID;
            _BulkMailTypeID = bulkMailTypeID;
            _Frequency = frequency;
            _LastSentDate = lastSentDate;
            _LastAttemptDate = lastAttemptDate;
            _NextAttemptDate = nextAttemptDate;
            _SentCount = sentCount;
            _FrequencyUpdateDate = frequencyUpdateDate;
            _InsertDate = insertDate;
            _SentMemberIDList = sentMemberIDList;
            _SearchPreferences = searchPreferences;
        }


        public Int32 MemberID { get { return _MemberID; } }
        public Int32 CommunityID { get { return _CommunityID; } }
        public BulkMailType BulkMailTypeID { get { return _BulkMailTypeID; } }
        public byte Frequency { get { return _Frequency; } }
        public DateTime LastSentDate { get { return _LastSentDate; } }
        public DateTime LastAttemptDate { get { return _LastAttemptDate; } }
        public DateTime NextAttemptDate { get { return _NextAttemptDate; } }
        public Int32 SentCount { get { return _SentCount; } }
        public DateTime FrequencyUpdateDate { get { return _FrequencyUpdateDate; } }
        public DateTime InsertDate { get { return _InsertDate; } }
        public String[] SentMemberIDList { get { return _SentMemberIDList; } set { _SentMemberIDList = value; } }

        public SearchPreferenceCollection SearchPreferences { get { return _SearchPreferences; } }

        public override string ToString()
        {
            return "MemberID=" + _MemberID.ToString() +
                    "|CommunityID=" + _CommunityID.ToString() +
                    "|BulkMailTypeID=" + _BulkMailTypeID.ToString("d") +
                    "|Frequency=" + _Frequency.ToString() +
                    "|LastSentDate=" + _LastSentDate.ToString() +
                    "|LastAttemptDate=" + _LastAttemptDate.ToString() +
                    "|NextAttemptDate=" + _NextAttemptDate.ToString() +
                    "|SentCount=" + _SentCount.ToString() +
                    "|FrequencyUpdateDate=" + _FrequencyUpdateDate.ToString() +
                    "|InsertDate=" + _InsertDate.ToString() +
                    "|SentMemberIDList has " + _SentMemberIDList.Length.ToString();
        }

    }
}
