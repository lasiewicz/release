using System;

namespace Matchnet.BulkMail.ValueObjects
{
	public class ServiceConstants
	{
		public const string SERVICE_NAME = "Matchnet.BulkMail.Service";
		public const string SERVICE_CONSTANT = "BULKMAIL_SVC";
        public const string HYDRA_SAVE_NAME = "mnAlertSaveMM";
        public const string REPORTING_SERVICE_MANAGER_NAME = "BulkMailReportingSM";
        public const byte NEVER_FREQUENCY = 0;
        public const byte DAILY_FREQUENCY = 1;
        public const byte TWICE_WEEKLY_FREQUENCY = 3;
        public const byte ONCE_WEEKLY_FREQUENCY = 7;
		
		private ServiceConstants()
		{
		}

	}
}
