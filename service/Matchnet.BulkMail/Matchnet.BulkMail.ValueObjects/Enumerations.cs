﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.BulkMail.ValueObjects
{
    [Serializable]
    public class Enumerations
    {
        public enum SearchPrefType
        {
            None = 0,
            StrictWithAge = 1,
            StrictNoAge = 2,
            RelaxWithAge = 3,
            RelaxNoAge = 4
        }
    }
}
