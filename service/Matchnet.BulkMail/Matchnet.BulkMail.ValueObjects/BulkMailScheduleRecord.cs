﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.ExternalMail.ValueObjects;

namespace Matchnet.BulkMail.ValueObjects
{
    [Serializable]
    public class BulkMailScheduleRecord
    {
        public int MemberID {get;set;}
        public int CommunityID { get; set; }
        public BulkMailType BulkMailTypeID { get; set; }
        public int Frequency { get; set; }
        public DateTime LastSentDate { get; set; }
        public DateTime LastAttemptDate { get; set; }
        public DateTime NextAttemptDate { get; set; }
        public int SentCount { get; set; }
        public DateTime FrequencyUpdateDate { get; set; }
        public DateTime InsertDate { get; set; }
        public string SentMemberIDList { get; set; }

        public BulkMailScheduleRecord() { }

        public BulkMailScheduleRecord(int memberID, int communityID, BulkMailType bulkMailTypeID, int frequency, DateTime lastSentDate, 
            DateTime lastAttemptDate, DateTime nextAttemptDate, int sentCount, DateTime frequencyUpdateDate, DateTime insertDate, string sentMemberIDList)
        {
            MemberID = memberID;
            CommunityID = communityID;
            BulkMailTypeID = bulkMailTypeID;
            Frequency = frequency;
            LastSentDate = lastSentDate;
            LastAttemptDate = lastAttemptDate;
            NextAttemptDate = nextAttemptDate;
            SentCount = sentCount;
            FrequencyUpdateDate = frequencyUpdateDate;
            InsertDate = insertDate;
            SentMemberIDList = sentMemberIDList;
        }
    }
}
