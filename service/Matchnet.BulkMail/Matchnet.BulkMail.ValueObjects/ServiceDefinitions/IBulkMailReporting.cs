﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Search.ValueObjects;
using Matchnet.ExternalMail.ValueObjects;

namespace Matchnet.BulkMail.ValueObjects.ServiceDefinitions
{
    public interface IBulkMailReporting
    {
        BulkMailBatch GetBulkMailBatchForDate(DateTime date);
        BulkMailScheduleRecord GetBulkMailScheduleRecord(int memberID, int communityID, BulkMailType bulkMailType);
        SearchPreferenceCollection GetLaxSearchPreference(int memberID, Brand brand, SearchPreferenceCollection strictPreferences);
        MatchResult RunMatchSimulation(int memberID, BulkMailQueueItem queueItem, Brand brand, ScrubList scrublist);
    }
}
