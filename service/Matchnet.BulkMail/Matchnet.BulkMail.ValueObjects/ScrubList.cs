﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;

namespace Matchnet.BulkMail.ValueObjects
{
    [Serializable]
    public class ScrubList
    {
        List<int> _scrub = null;
        private const int MAX_SCRUBLIST_ITEMS = 100;

        public ScrubList()
        {
            _scrub = new List<int>();
        }

        public int Count
        {
            get
            {
                return _scrub.Count;
            }
        }

        public ScrubList(string commaDelimitedList)
        {
            _scrub = new List<int>();
            string listNoWhiteSpace = commaDelimitedList.Replace(" ", "");
            string[] list = listNoWhiteSpace.Split(new char[] { ',' });
            foreach (string memID in list)
            {
                _scrub.Add(Conversion.CInt(memID));
            }
        }

        public ScrubList(string[] list)
        {
            _scrub = new List<int>();
            foreach (string memID in list)
            {
                _scrub.Add(Conversion.CInt(memID));
            }
        }

        public void Add(int memberID)
        {
            if (_scrub.Count == MAX_SCRUBLIST_ITEMS)
            {
                _scrub.Clear();
            }
            _scrub.Add(memberID);
        }

        public void Remove(int memberID)
        {
            if (_scrub.Contains(memberID))
            {
                _scrub.Remove(memberID);
            }
        }

        public bool Contains(int memberID)
        {
            return _scrub.Contains(memberID);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (int memberID in _scrub)
            {
                if (sb.Length != 0)
                {
                    sb.Append("," + memberID.ToString());
                }
                else
                {
                    sb.Append(memberID.ToString());
                }
            }

            return sb.ToString();
        }
    }
}
