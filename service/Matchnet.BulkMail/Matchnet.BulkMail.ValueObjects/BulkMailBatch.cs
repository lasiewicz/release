﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.BulkMail.ValueObjects
{
    
    [Serializable]
    public class BulkMailBatch
    {
        public int BatchID { get; set; }
        public int RecordsProcessed { get; set; }
        public int RunTime { get; set; }
        public DateTime RunDate { get; set; }
        public List<BulkMailBatchSummaryRecord> SummaryRecords { get; set; }

        public BulkMailBatch() { }

        public BulkMailBatch(int batchID, int recordsProcesed, int runTime, DateTime runDate, List<BulkMailBatchSummaryRecord> summaryRecords)
        {
            BatchID = batchID;
            RecordsProcessed = recordsProcesed;
            RunTime = runTime;
            RunDate = runDate;
            SummaryRecords = summaryRecords;
        }

    }
}
