using System;
using System.Collections;
using Matchnet.Search.ValueObjects;

namespace Matchnet.BulkMail.ValueObjects
{
	[Serializable]
	public class BulkMailSearchResultItem: IValueObject
	{
		private Hashtable _Attributes = new Hashtable();
		private int _MemberID;

		public BulkMailSearchResultItem(int memberID)
		{
			_MemberID = memberID;
		}

		public BulkMailSearchResultItem(MatchnetResultItem resultItem)
		{
			_MemberID = resultItem.MemberID;

			_Attributes.Add("username", resultItem["username"]);
			_Attributes.Add("birthdate", resultItem["birthdate"]);
			_Attributes.Add("regionid", resultItem["regionid"]);
			_Attributes.Add("gendermask", resultItem["gendermask"]);
			_Attributes.Add("aboutme", resultItem["aboutme"]);
			_Attributes.Add("headline", resultItem["headline"]);
			_Attributes.Add("thumbpath", resultItem["thumbpath"]);
		}

		public string this[string AttributeName]
		{
			get 
			{
				if (_Attributes.ContainsKey(AttributeName))
				{
					return _Attributes[AttributeName].ToString();
				}
				else
				{
					return String.Empty;
				}
			}
			set 
			{
				string valueNotNull = value == null ? string.Empty: value;
				
				if (_Attributes.ContainsKey(AttributeName))
				{
					_Attributes[AttributeName] = valueNotNull;
				}
				else 
				{
					_Attributes.Add(AttributeName, valueNotNull);
				}
			}
		}

		public void AddAttribute(string name, string value)
		{
			_Attributes.Add(name, value);
		}

		public int MemberID
		{
			get
			{
				return _MemberID;
			}
			set
			{
				_MemberID = value;
			}
		}
	}
}
