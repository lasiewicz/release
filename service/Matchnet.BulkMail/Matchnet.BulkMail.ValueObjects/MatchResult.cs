﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Search.ValueObjects;


namespace Matchnet.BulkMail.ValueObjects
{
    [Serializable]
    public class MatchResult
    {
        public int NumberMatchesNeeded { get; set; }
        public int MemberID { get; set; }
        public Brand Brand { get; set; }
        public ScrubList Scrublist { get; set; }

        public bool HasStoredSearchPreferences { get; set; }
        public bool StoredSearchPreferencesValid { get; set; }
        public bool CreatedDefaultPreferences { get; set; }
        public bool MatchSuccessful { get; set; }
        public int TotalMatchesFound { get; set; }

        public SearchPreferenceCollection StoredSearchPreferences { get; set; }
        public SearchPreferenceCollection DefaultSearchPreferences { get; set; }
        public SearchPreferenceCollection LaxSearchPreferences { get; set; }

        public SearchPreferenceCollection StoredSearchPreferencesWithAgeFilter { get; set; }
        public SearchPreferenceCollection DefaultSearchPreferencesWithAgeFilter { get; set; }
        public SearchPreferenceCollection LaxSearchPreferencesWithAgeFilter { get; set; }

        public ValidationResult InitialSearchValidationResult { get; set; }
        public MatchnetQueryResults InitialSearchResults { get; set; }
        public MatchPopulateResult InitialMatchResults { get; set; }

        public ValidationResult LaxSearchValidationResult { get; set; }
        public MatchnetQueryResults LaxSearchResults { get; set; }
        public MatchPopulateResult LaxMatchResults { get; set; }

        public ValidationResult InitialSearchValidationResultWithAgeFilter { get; set; }
        public MatchnetQueryResults InitialSearchResultsWithAgeFilter { get; set; }
        public MatchPopulateResult InitialMatchResultsWithAgeFilter { get; set; }

        public ValidationResult LaxSearchValidationResultWithAgeFilter { get; set; }
        public MatchnetQueryResults LaxSearchResultsWithAgeFilter { get; set; }
        public MatchPopulateResult LaxMatchResultsWithAgeFilter { get; set; }

        public ProcessingStatusID ProcessingStatus { get; set; }
        public Exception ProcessingException { get; set; }
        public Matchnet.BulkMail.ValueObjects.Enumerations.SearchPrefType SearchPrefType { get; set; }

        public MatchResult()
        {
            InitialMatchResults = new MatchPopulateResult();
            LaxMatchResults = new MatchPopulateResult();
            InitialMatchResultsWithAgeFilter = new MatchPopulateResult();
            LaxMatchResultsWithAgeFilter = new MatchPopulateResult();
            SearchPrefType = Enumerations.SearchPrefType.None;
        }
    }

    [Serializable]
    public class MatchPopulateResult
    {
        public List<int> MembersOnScrubList { get; set; }
        public List<int> InvalidMembers { get; set; }
        public List<int> MatchedMembers { get; set; }
        public bool SearchSufficient { get; set; }

        public MatchPopulateResult()
        {
            MembersOnScrubList = new List<int>();
            InvalidMembers = new List<int>();
            MatchedMembers = new List<int>();
        }
    }
}
