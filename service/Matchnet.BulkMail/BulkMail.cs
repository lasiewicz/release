using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using Matchnet.BulkMail.BusinessLogic;
using Matchnet.BulkMail.ValueObjects;
using Matchnet.BulkMail.ServiceManagers;

using Matchnet.RemotingServices;
using Matchnet.Exceptions;


namespace Matchnet.BulkMail
{
	public class BulkMail :  Matchnet.RemotingServices.RemotingServiceBase
	{
		private System.ComponentModel.Container components = null;
        private BulkMailSM _bulkMailSM;
        private BulkMailReportingSM _bulkMailReportingSM;

        public BulkMail()
		{
			InitializeComponent();
            _bulkMailSM = new BulkMailSM();
            _bulkMailReportingSM = new BulkMailReportingSM();
		}


		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
            ServicesToRun = new System.ServiceProcess.ServiceBase[] { new BulkMail() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}


		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = ServiceConstants.SERVICE_NAME;
			base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
		}

        protected override void RegisterServiceManagers()
        {
            try
            {
                base.RegisterServiceManager(_bulkMailSM);
                base.RegisterServiceManager(_bulkMailReportingSM);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occurred when starting the Windows Service", ex);
            }
        }

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		protected override void OnStart(string[] args)
		{
            base.OnStart(args);
            _bulkMailSM.Start();
	}	
 

		protected override void OnStop()
		{
            _bulkMailSM.Stop();
			base.OnStop();
		}



	}
}
