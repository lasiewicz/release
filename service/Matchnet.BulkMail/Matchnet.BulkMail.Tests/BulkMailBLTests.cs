﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Reflection;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ValueObjects.Interfaces;
using NUnit.Framework;
using NUnit.Mocks;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.BulkMail.BusinessLogic;
using Matchnet.BulkMail.ValueObjects;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects.ServiceDefinitions;
using Matchnet.Member.ValueObjects;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;

namespace Matchnet.BulkMail.Tests
{
    public class BulkMailBLTests
    {
        Attributes _attributes;
        int _JDateBrandInsertAttributeGroupID = 0;
        int _JDateBirthdateAttributeGroupID = 0;
        int _JDateGenderMaskAttributeGroupID = 0;
        int _JDateRegionIDAttributeGroupID = 0;
        GetMemberMock _memberMock = null;
        MockSettingsService _mockSettingsService = null;
        DynamicMock _dynamicMockSettings = null;

        [TestFixtureSetUp]
        public void StartUp()
        {
            _attributes = AttributeMetadataSA.Instance.GetAttributes();
            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute brandInsertDate = _attributes.GetAttribute("BrandInsertDate");
            AttributeGroup brandInsertDateGroup = _attributes.GetAttributeGroup(1003, brandInsertDate.ID);
            _JDateBrandInsertAttributeGroupID = brandInsertDateGroup.ID;

            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute birthdate = _attributes.GetAttribute("Birthdate");
            AttributeGroup brandBirthdateGroup = _attributes.GetAttributeGroup(8383, birthdate.ID);
            _JDateBirthdateAttributeGroupID = brandBirthdateGroup.ID;

            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute genderMask = _attributes.GetAttribute("GenderMask");
            AttributeGroup genderMaskGroup = _attributes.GetAttributeGroup(3, genderMask.ID);
            _JDateGenderMaskAttributeGroupID = genderMaskGroup.ID;

            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute regionID = _attributes.GetAttribute("RegionID");
            AttributeGroup regionIDGroup = _attributes.GetAttributeGroup(8383, regionID.ID);
            _JDateRegionIDAttributeGroupID = regionIDGroup.ID;

            _memberMock = new GetMemberMock(_JDateBrandInsertAttributeGroupID, _JDateBirthdateAttributeGroupID, _JDateGenderMaskAttributeGroupID, _JDateRegionIDAttributeGroupID);
            _memberMock.AddMember(1111, DateTime.Parse("1-1-2007"), DateTime.Parse("05-09-1975"), 9, 3443821);
            _memberMock.AddMember(2222, DateTime.Parse("1-1-2007"), DateTime.Parse("05-09-1975"), 9, 3443821);
            _memberMock.AddMember(3333, DateTime.Parse("1-1-2007"), DateTime.Parse("05-09-1975"), 9, 3443821);
            _memberMock.AddMember(4444, DateTime.Parse("1-1-2007"), DateTime.Parse("05-09-1975"), 9, 3443821);
            _memberMock.AddMember(5555, DateTime.Parse("1-1-2007"), DateTime.Parse("05-09-1975"), 9, 3443821);
            _memberMock.AddMember(6666, DateTime.Parse("1-1-2007"), DateTime.Parse("05-09-1975"), 9, 3443821);
            _memberMock.AddMember(7777, DateTime.Parse("1-1-2007"), DateTime.Parse("05-09-1975"), 9, 3443821);
            _memberMock.AddMember(8888, DateTime.Parse("1-1-2007"), DateTime.Parse("05-09-1975"), 9, 3443821);
            _memberMock.AddMember(1234, DateTime.Parse("1-1-2007"), DateTime.Parse("05-09-1975"), 9, 3443821);

            _mockSettingsService = new MockSettingsService();
            _mockSettingsService.AddSetting("KEY", "cb4859be");
            _mockSettingsService.AddSetting("MATCHMAIL_MATCHES_SENT", "6");
            _mockSettingsService.AddSetting("MM_USE_LARGE_IMAGES", "true");
            _mockSettingsService.AddSetting("MM_LARGE_IMAGES_URL", "/Framework/MMImageDisplay.aspx?MID={0}&BID={1}");
            _mockSettingsService.AddSetting("NEW_MEMBER_EMAIL_AGE_FACTOR", "7");
            _mockSettingsService.AddSetting("NEW_MEMBER_EMAIL_DISTANCE", "7");
            _mockSettingsService.AddSetting("NEW_MEMBER_EMAIL_NUMBER_OF_MATCHES", "1");
            _mockSettingsService.AddSetting("NEW_MEMBER_EMAIL_SIGNUP_FACTOR", "4000");
            _mockSettingsService.AddSetting("BULKMAILSVC_POLL_INTERVAL", "5");
            _mockSettingsService.AddSetting("BULKMAILSVC_QPS_THRESHOLD", "30");
            _mockSettingsService.AddSetting("BULKMAILSVC_QUEUE_PATH", "FormatName:DIRECT=OS:bhd-mmishkoff\\private$\\bulkmail");
            _mockSettingsService.AddSetting("BULKMAILSVC_THREAD_COUNT", "10");
            _mockSettingsService.AddSetting("BULKMAILSVC_USE_EXTERNAL_MAIL", "false");
            _mockSettingsService.AddSetting("YESMAIL_PERCENT_MATCH_MAIL", "100");
            _mockSettingsService.AddSetting("YESMAIL_PERCENT_NEW_MEMBER_MAIL", "100");
            _mockSettingsService.AddSetting("BULKMAILSVC_REGISTERED_DAYS_CRITERIA", "759");
            _mockSettingsService.AddSetting("PROFILE_BLOCK_ENABLED", "false");
            _mockSettingsService.AddSetting("BULKMAILSVC_LAST_LOGON_THRESHOLD", "800");
            _mockSettingsService.AddSetting("BULKMAILSVC_QUEUE_FLOOR", "500");
            _mockSettingsService.AddSetting("MATCHMAIL_DEFAULT_SEARCH_RADIUS", "50");
            _mockSettingsService.AddSetting("SCRIPTING_ENVIRONMENT", "DEV");
            _mockSettingsService.AddSetting("MATCHMAIL_USE_FREQUENCY_FOR_NUM_TO_SEND", "true");
            _mockSettingsService.AddSetting("ENABLE_MEMBER_DTO", "true");

            _dynamicMockSettings = new DynamicMock(typeof(ISettingsSA));
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","cb4859be", "KEY");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","2", "MATCHMAIL_MATCHES_SENT");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","true", "MM_USE_LARGE_IMAGES");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","/Framework/MMImageDisplay.aspx?MID={0}&BID={1}", "MM_LARGE_IMAGES_URL");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","7", "NEW_MEMBER_EMAIL_AGE_FACTOR");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","7", "NEW_MEMBER_EMAIL_DISTANCE");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","1", "NEW_MEMBER_EMAIL_NUMBER_OF_MATCHES");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","4000", "NEW_MEMBER_EMAIL_SIGNUP_FACTOR");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","5", "BULKMAILSVC_POLL_INTERVAL");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","30", "BULKMAILSVC_QPS_THRESHOLD");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","FormatName:DIRECT=OS:bhd-mmishkoff\private$\bulkmail", "BULKMAILSVC_QUEUE_PATH");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","10", "BULKMAILSVC_THREAD_COUNT");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","false", "BULKMAILSVC_USE_EXTERNAL_MAIL");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","100", "YESMAIL_PERCENT_MATCH_MAIL");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","100", "YESMAIL_PERCENT_NEW_MEMBER_MAIL");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","759", "BULKMAILSVC_REGISTERED_DAYS_CRITERIA");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","false", "PROFILE_BLOCK_ENABLED");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","800", "BULKMAILSVC_LAST_LOGON_THRESHOLD");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","500", "BULKMAILSVC_QUEUE_FLOOR");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton","50", "MATCHMAIL_DEFAULT_SEARCH_RADIUS");        
//            MailHandlerUtils.SettingsService = _mockSettingsService;
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
//            MailHandlerUtils.SettingsService = null;
        }

        [Test]
        public void SearchPreferencesIntactTest()
        {
            SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("countryregionid", "223");
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("searchtypeid", "4");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("radius", "50");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("gendermask", "9");
            searchPrefs.Add("minAge", "33");
            searchPrefs.Add("maxAge", "35");

            List<int> memberIDs = new List<int>(new int[] { 1111, 2222, 3333, 1234 });
            MatchResult result = RunMatchScenario(1234, 3, searchPrefs, 1003, memberIDs, new ScrubList(), ValueObjects.ServiceConstants.TWICE_WEEKLY_FREQUENCY);

            Assert.IsTrue(result.StoredSearchPreferences["countryregionid"] == searchPrefs["countryregionid"]);
            Assert.IsTrue(result.StoredSearchPreferences["domainid"] == searchPrefs["domainid"]);
            Assert.IsTrue(result.StoredSearchPreferences["searchtypeid"] == searchPrefs["searchtypeid"]);
            Assert.IsTrue(result.StoredSearchPreferences["searchorderby"] == searchPrefs["searchorderby"]);
            Assert.IsTrue(result.StoredSearchPreferences["radius"] == searchPrefs["radius"]);
            Assert.IsTrue(result.StoredSearchPreferences["regionid"] == searchPrefs["regionid"]);
            Assert.IsTrue(result.StoredSearchPreferences["gendermask"] == searchPrefs["gendermask"]);
            Assert.IsTrue(result.StoredSearchPreferences["minAge"] == searchPrefs["minAge"]);
            Assert.IsTrue(result.StoredSearchPreferences["maxAge"] == searchPrefs["maxAge"]);
        }

        [Test]
        public void ReligionSettingRespectedForLaxSearches()
        {
            Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            bool retainReligion = Convert.ToBoolean(RuntimeSettings.GetSetting("MATCHMAIL_LAX_PREFERENCES_RETAIN_JDATERELIGION", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
            
            SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("searchtypeid", "4");
            searchPrefs.Add("searchorderby", "3");
            searchPrefs.Add("radius", "50");
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("gendermask", "9");
            searchPrefs.Add("minAge", "33");
            searchPrefs.Add("maxAge", "35");
            searchPrefs.Add("jdatereligion", "8");

            List<int> memberIDs = new List<int>(new int[] { 1111 });
            MatchResult result = RunMatchScenario(1234, 3, searchPrefs, brand.BrandID, memberIDs, new ScrubList(), ValueObjects.ServiceConstants.TWICE_WEEKLY_FREQUENCY);

            if (retainReligion)
            {
                Assert.IsTrue(result.LaxSearchPreferences.ContainsKey("jdatereligion"));
            }
            else
            {
                Assert.IsTrue(!result.LaxSearchPreferences.ContainsKey("jdatereligion"));
            }
        }

        [Test]
        public void AreaCodeSearchesRespected()
        {
            SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("searchtypeid", SearchTypeID.AreaCode.ToString("d"));
            searchPrefs.Add("searchorderby", Matchnet.Search.Interfaces.QuerySorting.JoinDate.ToString("d"));
            searchPrefs.Add("areacode1", "01");
            searchPrefs.Add("areacode2", "02");
            searchPrefs.Add("areacode3", "03");
            searchPrefs.Add("areacode4", "04");
            searchPrefs.Add("areacode5", "05");
            searchPrefs.Add("areacode6", "06");
            searchPrefs.Add("gendermask", "9");
            searchPrefs.Add("minAge", "33");
            searchPrefs.Add("maxAge", "35");
            
            List<int> memberIDs = new List<int>(new int[] { 1111 });

            MatchResult result = RunMatchScenario(1234, 3, searchPrefs, 1003, memberIDs, new ScrubList(), ValueObjects.ServiceConstants.TWICE_WEEKLY_FREQUENCY);

            Assert.IsTrue(result.LaxSearchPreferences.ContainsKey("areacode1"));
            Assert.IsTrue(result.LaxSearchPreferences.ContainsKey("areacode2"));
            Assert.IsTrue(result.LaxSearchPreferences.ContainsKey("areacode3"));
            Assert.IsTrue(result.LaxSearchPreferences.ContainsKey("areacode4"));
            Assert.IsTrue(result.LaxSearchPreferences.ContainsKey("areacode5"));
            Assert.IsTrue(result.LaxSearchPreferences.ContainsKey("areacode6"));
            Assert.IsTrue(Int32.Parse(result.LaxSearchPreferences["searchtypeid"]) == (int)SearchTypeID.AreaCode);
            Assert.IsTrue(Int32.Parse(result.LaxSearchPreferences["searchorderby"]) == (int)Matchnet.Search.Interfaces.QuerySorting.JoinDate);
            Assert.IsTrue(!result.LaxSearchPreferences.ContainsKey("radius"));
        }

        [Test]
        public void AreaCodeSearchesRespectedWithAgeFilter()
        {
            SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("searchtypeid", SearchTypeID.AreaCode.ToString("d"));
            searchPrefs.Add("searchorderby", Matchnet.Search.Interfaces.QuerySorting.JoinDate.ToString("d"));
            searchPrefs.Add("areacode1", "01");
            searchPrefs.Add("areacode2", "02");
            searchPrefs.Add("areacode3", "03");
            searchPrefs.Add("areacode4", "04");
            searchPrefs.Add("areacode5", "05");
            searchPrefs.Add("areacode6", "06");
            searchPrefs.Add("gendermask", "9");
            searchPrefs.Add("minAge", "33");
            searchPrefs.Add("maxAge", "35");
            
            List<int> memberIDs = new List<int>(new int[] { 1111 });

            //setup expext and return in order each method will be called
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton", "6", "MATCHMAIL_MATCHES_SENT", 3, 103);
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton", "true", "BULKMAIL_USE_AGE_FILTER");
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton", "true", "MM_USE_LARGE_IMAGES", 3, 103, 1003);
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton", "/Framework/MMImageDisplay.aspx?MID={0}&BID={1}", "MM_LARGE_IMAGES_URL", 3, 103, 1003);
//            _dynamicMockSettings.ExpectAndReturn("GetSettingFromSingleton", "cb4859be", "KEY");


            _mockSettingsService.AddSetting("BULKMAIL_USE_AGE_FILTER", "true");

            MatchResult result = RunMatchScenario(1234, 3, searchPrefs, 1003, memberIDs, new ScrubList(), (ISettingsSA)_mockSettingsService, ValueObjects.ServiceConstants.TWICE_WEEKLY_FREQUENCY);

            Assert.IsTrue(result.LaxSearchPreferencesWithAgeFilter.ContainsKey("areacode1"));
            Assert.IsTrue(result.LaxSearchPreferencesWithAgeFilter.ContainsKey("areacode2"));
            Assert.IsTrue(result.LaxSearchPreferencesWithAgeFilter.ContainsKey("areacode3"));
            Assert.IsTrue(result.LaxSearchPreferencesWithAgeFilter.ContainsKey("areacode4"));
            Assert.IsTrue(result.LaxSearchPreferencesWithAgeFilter.ContainsKey("areacode5"));
            Assert.IsTrue(result.LaxSearchPreferencesWithAgeFilter.ContainsKey("areacode6"));
            Assert.IsTrue(Int32.Parse(result.LaxSearchPreferencesWithAgeFilter["searchtypeid"]) == (int)SearchTypeID.AreaCode);
            Assert.IsTrue(Int32.Parse(result.LaxSearchPreferencesWithAgeFilter["searchorderby"]) == (int)Matchnet.Search.Interfaces.QuerySorting.JoinDate);
            Assert.IsTrue(!result.LaxSearchPreferencesWithAgeFilter.ContainsKey("radius"));

            _mockSettingsService.RemoveSetting("BULKMAIL_USE_AGE_FILTER");

        }

        [Test]
        public void DefaultPrefsCreatedAndStatusSetCorrectly()
        {
            List<int> memberIDs = new List<int>(new int[] { 1111, 2222, 3333, 4444,5555,6666 });

            MatchResult result = RunMatchScenario(1234, 3, null, 1003, memberIDs, new ScrubList(), ValueObjects.ServiceConstants.TWICE_WEEKLY_FREQUENCY);

            Assert.IsTrue(result.CreatedDefaultPreferences);
            Assert.IsTrue(result.DefaultSearchPreferences != null);
            Assert.IsTrue(result.ProcessingStatus == ProcessingStatusID.SentMatchesWithDefaultPrefs);
        }

        [Test]
        public void DefaultPrefsCreatedAndStatusSetCorrectlyWithAgeFilter()
        {
            List<int> memberIDs = new List<int>(new int[] { 1111, 2222, 3333, 4444, 5555, 6666 });

            _mockSettingsService.AddSetting("BULKMAIL_USE_AGE_FILTER", "true");
            MatchResult result = RunMatchScenario(1234, 3, null, 1003, memberIDs, new ScrubList(), _mockSettingsService, ValueObjects.ServiceConstants.TWICE_WEEKLY_FREQUENCY);

            Assert.IsTrue(result.CreatedDefaultPreferences);
            Assert.IsTrue(result.DefaultSearchPreferencesWithAgeFilter != null);
            Assert.IsTrue(result.ProcessingStatus == ProcessingStatusID.SentMatchesWithDefaultPrefsWithAgeFilter);
            _mockSettingsService.RemoveSetting("BULKMAIL_USE_AGE_FILTER");
        }

        [Test]
        public void LaxPreferencesCreatedCorrectly()
        {
            int memberDistance = 200;
            Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            _memberMock.AddMember(4321, DateTime.Parse("1-1-2007"), DateTime.Parse("05-09-1975"), 9, 3443821);
            SearchPreferenceCollection searchPrefs = new Matchnet.Search.ValueObjects.SearchPreferenceCollection();
            searchPrefs.Add("domainid", "3");
            searchPrefs.Add("searchtypeid", SearchTypeID.Region.ToString("d"));
            searchPrefs.Add("searchorderby", Matchnet.Search.Interfaces.QuerySorting.Proximity.ToString("d"));
            searchPrefs.Add("distance", memberDistance.ToString());
            searchPrefs.Add("regionid", "3443821");
            searchPrefs.Add("gendermask", "9");
            searchPrefs.Add("minAge", "33");
            searchPrefs.Add("maxAge", "99");

            List<int> memberIDs = new List<int>(new int[] { 1111});

            MatchResult result = RunMatchScenario(4321, 3, searchPrefs, 1003, memberIDs, new ScrubList("1111"), ValueObjects.ServiceConstants.TWICE_WEEKLY_FREQUENCY);

            Assert.IsTrue(result.LaxSearchPreferences != null);
            Assert.IsTrue(result.LaxSearchPreferences["domainid"] == searchPrefs["domainid"]);
            Assert.IsTrue(result.LaxSearchPreferences["searchtypeid"] == searchPrefs["searchtypeid"]);
            Assert.IsTrue(result.LaxSearchPreferences["searchorderby"] == searchPrefs["searchorderby"]);
            Assert.IsTrue(result.LaxSearchPreferences["regionid"] == searchPrefs["regionid"]);
            Assert.IsTrue(result.LaxSearchPreferences["gendermask"] == searchPrefs["gendermask"]);
            Assert.IsTrue(result.LaxSearchPreferences["HasPhotoFlag"] == "1");

            int settingDistance = Convert.ToInt32(RuntimeSettings.GetSetting("MATCHMAIL_LAX_SEARCH_RADIUS", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID));
            if (settingDistance < memberDistance)
            {
                Assert.IsTrue(result.LaxSearchPreferences["radius"] == memberDistance.ToString());
            }
            else
            {
                Assert.IsTrue(result.LaxSearchPreferences["radius"] == settingDistance.ToString());
            }

            //member should be 36ish
            Assert.IsTrue(result.LaxSearchPreferences["minage"] == "35");
            Assert.IsTrue(result.LaxSearchPreferences["maxage"] == "44");
        }

        [Test]
        public void MatchesPopulatedIfSufficientNumberOfResults()
        {
            //this test assumes that 8 matches is sufficient for a successful match run
            List<int> memberIDs = new List<int>(new int[] { 1111, 2222, 3333, 4444,5555,6666,7777,8888 });
            MatchResult result = RunMatchScenario(1234, 3, null, 1003, memberIDs, new ScrubList(), ValueObjects.ServiceConstants.TWICE_WEEKLY_FREQUENCY);

            if (result.NumberMatchesNeeded <= 8)
            {
                Assert.IsTrue(result.ProcessingStatus == ProcessingStatusID.SentMatchesWithDefaultPrefs);
                Assert.IsTrue(result.InitialMatchResults.MatchedMembers.Count == result.NumberMatchesNeeded);
                for (int i = 0; i < result.InitialMatchResults.MatchedMembers.Count - 1; i++)
                {
                    Assert.IsTrue(result.InitialMatchResults.MatchedMembers.Contains(memberIDs[i]));
                }
            }
        }

        [Test]
        public void MatchesPopulatedIfSufficientNumberOfResultsWithAgeFilter()
        {
            //this test assumes that 8 matches is sufficient for a successful match run
            List<int> memberIDs = new List<int>(new int[] { 1111, 2222, 3333, 4444, 5555, 6666, 7777, 8888 });
            _mockSettingsService.AddSetting("BULKMAIL_USE_AGE_FILTER", "true");

            MatchResult result = RunMatchScenario(1234, 3, null, 1003, memberIDs, new ScrubList(), _mockSettingsService, ValueObjects.ServiceConstants.TWICE_WEEKLY_FREQUENCY);

            if (result.NumberMatchesNeeded <= 8)
            {
                Assert.IsTrue(result.ProcessingStatus == ProcessingStatusID.SentMatchesWithDefaultPrefsWithAgeFilter);
                Assert.IsTrue(result.InitialMatchResultsWithAgeFilter.MatchedMembers.Count == result.NumberMatchesNeeded);
                for (int i = 0; i < result.InitialMatchResultsWithAgeFilter.MatchedMembers.Count - 1; i++)
                {
                    Assert.IsTrue(result.InitialMatchResultsWithAgeFilter.MatchedMembers.Contains(memberIDs[i]));
                }
            }
            _mockSettingsService.RemoveSetting("BULKMAIL_USE_AGE_FILTER");

        }

        [Test]
        public void ScrubListRespected()
        {
            List<int> memberIDs = new List<int>(new int[] { 1111, 2222, 3333, 4444, 5555, 6666, 7777, 8888 });
            MatchResult result = RunMatchScenario(1234, 3, null, 1003, memberIDs, new ScrubList("1111,2222"), ValueObjects.ServiceConstants.TWICE_WEEKLY_FREQUENCY);

            Assert.IsTrue(result.InitialMatchResults.MembersOnScrubList.Contains(1111));
            Assert.IsTrue(result.InitialMatchResults.MembersOnScrubList.Contains(2222));
        }

        [Test]
        public void ScrubListRespectedWithAgeFilter()
        {
            List<int> memberIDs = new List<int>(new int[] { 1111, 2222, 3333, 4444, 5555, 6666, 7777, 8888 });
            _mockSettingsService.AddSetting("BULKMAIL_USE_AGE_FILTER", "true");
            MatchResult result = RunMatchScenario(1234, 3, null, 1003, memberIDs, new ScrubList("1111,2222"), _mockSettingsService, ValueObjects.ServiceConstants.TWICE_WEEKLY_FREQUENCY);

            Assert.IsTrue(result.InitialMatchResultsWithAgeFilter.MembersOnScrubList.Contains(1111));
            Assert.IsTrue(result.InitialMatchResultsWithAgeFilter.MembersOnScrubList.Contains(2222));
            _mockSettingsService.RemoveSetting("BULKMAIL_USE_AGE_FILTER");
        }


        [Test]
        public void NumMatchesSentBasedOnTwiceWeeklyFrequency()
        {
            List<int> memberIDs = new List<int>(new int[] {1111, 2222, 3333, 4444, 5555, 6666, 7777, 8888 });
            _mockSettingsService.AddSetting("BULKMAIL_USE_AGE_FILTER", "true");
            MatchResult result = RunMatchScenario(1234, 3, null, 1003, memberIDs, new ScrubList("1111,2222"), _mockSettingsService, ValueObjects.ServiceConstants.TWICE_WEEKLY_FREQUENCY);

            Assert.IsTrue(result.InitialMatchResultsWithAgeFilter.SearchSufficient);
            Assert.AreEqual(6,result.InitialMatchResultsWithAgeFilter.MatchedMembers.Count);
            Assert.AreEqual(3333,result.InitialMatchResultsWithAgeFilter.MatchedMembers.First());
            Assert.AreEqual(8888, result.InitialMatchResultsWithAgeFilter.MatchedMembers.Last());
            _mockSettingsService.RemoveSetting("BULKMAIL_USE_AGE_FILTER");
        }

        [Test]
        public void NumMatchesSentBasedOnceWeeklyFrequency()
        {
            List<int> memberIDs = new List<int>(new int[] { 1111, 2222, 3333, 4444, 5555, 6666, 7777, 8888 });
            _mockSettingsService.AddSetting("BULKMAIL_USE_AGE_FILTER", "true");
            MatchResult result = RunMatchScenario(1234, 3, null, 1003, memberIDs, new ScrubList("1111,2222"), _mockSettingsService, ValueObjects.ServiceConstants.ONCE_WEEKLY_FREQUENCY);

            Assert.IsTrue(result.InitialMatchResultsWithAgeFilter.SearchSufficient);
            Assert.AreEqual(2, result.InitialMatchResultsWithAgeFilter.MatchedMembers.Count);
            Assert.AreEqual(3333, result.InitialMatchResultsWithAgeFilter.MatchedMembers.First());
            Assert.AreEqual(4444, result.InitialMatchResultsWithAgeFilter.MatchedMembers.Last());
            _mockSettingsService.RemoveSetting("BULKMAIL_USE_AGE_FILTER");
        }

        [Test]
        public void RealSearchRunWithNoException()
        {
            int realMemberID=0;

            string enivornment = RuntimeSettings.GetSetting("SCRIPTING_ENVIRONMENT");

            switch (enivornment)
            {
                case "DEV":
                    realMemberID = 100068787;
                    break;
                case "STGV1":
                    realMemberID = 113377690;
                    break;
                case "STGV2":
                    realMemberID = 113377690;
                    break;
                case "STGV3":
                    realMemberID = 113377690;
                    break;
                case "PROD":
                    realMemberID = 113377690;
                    break;
            }

            IMemberSearchSA searchService = (IMemberSearchSA)Matchnet.Search.ServiceAdapters.MemberSearchSA.Instance;
            IGetMember memberService = (IGetMember)Matchnet.Member.ServiceAdapters.MemberSA.Instance;
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(memberService, realMemberID, Member.ServiceAdapters.MemberLoadFlags.IngoreSACache);
            SearchPreferenceCollection searchPrefs = Search.ServiceAdapters.SearchPreferencesSA.Instance.GetSearchPreferences(realMemberID, 3);
            Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            BulkMailQueueItem queueItem = createQueueItem(realMemberID, 3, searchPrefs, null, ValueObjects.ServiceConstants.TWICE_WEEKLY_FREQUENCY);

            BulkMailBL bulkMailBL = new BulkMailBL();
            bulkMailBL.Initialize();
            bulkMailBL.IsDebug = true;
            bulkMailBL.MemberService = memberService;
            bulkMailBL.MemberSearchService = searchService;

            MatchResult result= bulkMailBL.ProcessMatchMailQueueItem(new PerformanceCounterHelper(),
                queueItem,
                member,
                brand,
                new ScrubList(),
                memberService,
                searchService);

            Assert.IsTrue(result.ProcessingException == null);
        }

        [Test]
        public void RealSearchRunWithNoExceptionWithAgeFilter()
        {
            int realMemberID = 0;

            string enivornment = RuntimeSettings.GetSetting("SCRIPTING_ENVIRONMENT");

            switch (enivornment)
            {
                case "DEV":
                    realMemberID = 100068787;
                    break;
                case "STGV1":
                    realMemberID = 113377690;
                    break;
                case "STGV2":
                    realMemberID = 113377690;
                    break;
                case "STGV3":
                    realMemberID = 113377690;
                    break;
                case "PROD":
                    realMemberID = 113377690;
                    break;
            }

            IMemberSearchSA searchService = (IMemberSearchSA)Matchnet.Search.ServiceAdapters.MemberSearchSA.Instance;
            IGetMember memberService = (IGetMember)Matchnet.Member.ServiceAdapters.MemberSA.Instance;
            Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
            IMemberDTO member = MailHandlerUtils.GetIMemberDTO(memberService, realMemberID, Member.ServiceAdapters.MemberLoadFlags.IngoreSACache);
            DateTime birthdate = member.GetAttributeDate(brand, "BirthDate");
            int age = Matchnet.Lib.Util.Util.Age(birthdate);
            SearchPreferenceCollection searchPrefs = Search.ServiceAdapters.SearchPreferencesSA.Instance.GetSearchPreferences(realMemberID, 3);
            BulkMailQueueItem queueItem = createQueueItem(realMemberID, 3, searchPrefs, null, ValueObjects.ServiceConstants.TWICE_WEEKLY_FREQUENCY);
            _mockSettingsService.AddSetting("BULKMAIL_USE_AGE_FILTER", "true");

            BulkMailBL bulkMailBL = new BulkMailBL();
            bulkMailBL.Initialize();
            bulkMailBL.IsDebug = true;
            bulkMailBL.MemberService = memberService;
            bulkMailBL.MemberSearchService = searchService;
            bulkMailBL.SettingsService = _mockSettingsService;

            MatchResult result = bulkMailBL.ProcessMatchMailQueueItem(new PerformanceCounterHelper(),
                queueItem,
                member,
                brand,
                new ScrubList(),
                memberService,
                searchService);

            Assert.IsTrue(result.ProcessingException == null);
            result.StoredSearchPreferencesWithAgeFilter.Add("DomainID", "3");
            result.StoredSearchPreferencesWithAgeFilter.Add("SearchType", Matchnet.Search.Interfaces.SearchType.MatchMail.ToString());
            FASTQueryTransformer.TransformSearchPreferences(result.StoredSearchPreferencesWithAgeFilter,3);
            ArrayList searchDetailedMembers = Matchnet.Search.ServiceAdapters.MemberSearchSA.Instance.SearchDetailed(result.StoredSearchPreferencesWithAgeFilter, 3, 103);
            foreach (int matchedMemberId in result.InitialMatchResultsWithAgeFilter.MatchedMembers)
            {
                DetailedQueryResult detailedQueryResult = null;
                foreach (DetailedQueryResult dqr in searchDetailedMembers)
                {
                    if (dqr.MemberID == matchedMemberId)
                    {
                        detailedQueryResult = dqr;
                        break;
                    }
                }
                Assert.NotNull(detailedQueryResult, string.Format("Can't find memberid:{0}", matchedMemberId));
                Assert.GreaterOrEqual(age, (int)detailedQueryResult.Tags["preferredminage"], string.Format("Member:{0} preferred min age > {1}!", detailedQueryResult.Tags["preferredminage"], age));
                Assert.LessOrEqual(age, (int)detailedQueryResult.Tags["preferredmaxage"], string.Format("Member:{0} preferred max age < {1}!", detailedQueryResult.Tags["preferredmaxage"], age));
            }
            _mockSettingsService.RemoveSetting("BULKMAIL_USE_AGE_FILTER");
        }

//        [Test]
//        public void RealSearchRunWithBlockedMembersAndScrubList()
//        {
//            
//            int realMemberID = 0;
//
//            string enivornment = RuntimeSettings.GetSetting("SCRIPTING_ENVIRONMENT");
//
//            switch (enivornment)
//            {
//                case "DEV":
//                    realMemberID = 100068787;
//                    break;
//                case "STGV1":
//                    realMemberID = 113377690;
//                    break;
//                case "STGV2":
//                    realMemberID = 113377690;
//                    break;
//                case "STGV3":
//                    realMemberID = 113377690;
//                    break;
//                case "PROD":
//                    realMemberID = 113377690;
//                    break;
//            }
//
//            Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);
//            int communityId = brand.Site.Community.CommunityID;
//            int siteId = brand.Site.SiteID;
//            Matchnet.Member.ServiceAdapters.Member member = Member.ServiceAdapters.MemberSA.Instance.GetMember(realMemberID, Member.ServiceAdapters.MemberLoadFlags.IngoreSACache);
//            DateTime birthdate = member.GetAttributeDate(brand, "BirthDate");
//            int age = Matchnet.Lib.Util.Util.Age(birthdate);
//            SearchPreferenceCollection searchPrefs = Search.ServiceAdapters.SearchPreferencesSA.Instance.GetSearchPreferences(realMemberID, communityId);
//            BulkMailQueueItem queueItem = createQueueItem(realMemberID, communityId, searchPrefs, null, ValueObjects.ServiceConstants.TWICE_WEEKLY_FREQUENCY);
//            IMemberSearchSA searchService = (IMemberSearchSA)Matchnet.Search.ServiceAdapters.MemberSearchSA.Instance;
//            IGetMember memberService = (IGetMember)Matchnet.Member.ServiceAdapters.MemberSA.Instance;
//
//            BulkMailBL bulkMailBL = new BulkMailBL();
//            bulkMailBL.Initialize();
//            bulkMailBL.IsDebug = true;
//            bulkMailBL.TestBlockedAndScrubMemberIds = true;
//            bulkMailBL.MemberService = memberService;
//            bulkMailBL.MemberSearchService = searchService;
//
//            //block member
//            int blockedMemberId = 100068946;
//            int scrubListMemberId = 100065205;
//            Matchnet.List.ServiceAdapters.ListSA.Instance.AddListMember(HotListCategory.ExcludeList,
//                                                                        communityId,
//                                                                        siteId, member.MemberID, blockedMemberId,
//                                                                        string.Empty, Constants.NULL_INT, false, false);
//
//            MatchResult result = bulkMailBL.ProcessMatchMailQueueItem(new PerformanceCounterHelper(),
//                queueItem,
//                member,
//                brand,
//                new ScrubList(scrubListMemberId.ToString()),
//                memberService,
//                searchService);
//
//            Assert.IsTrue(result.ProcessingException == null);
//            foreach (int matchedMemberId in result.InitialMatchResults.MatchedMembers)
//            {
//                Assert.AreNotEqual(blockedMemberId,matchedMemberId, string.Format("Memberid:{0} was not blocked!", matchedMemberId));
//                Assert.AreNotEqual(scrubListMemberId, matchedMemberId, string.Format("Memberid:{0} was not scrubbed!", matchedMemberId));
//            }
//            Matchnet.List.ServiceAdapters.ListSA.Instance.RemoveListMember(HotListCategory.ExcludeList, communityId, member.MemberID, blockedMemberId);
//        }

        private MatchResult RunMatchScenario(int memberID, int communityID, SearchPreferenceCollection searchPrefs,
            int brandID, List<int> membersForResults, ScrubList scrubList, byte frequency)
        {
            return RunMatchScenario(memberID, communityID, searchPrefs, brandID, membersForResults, scrubList, null, frequency);
        }

        private MatchResult RunMatchScenario(int memberID, int communityID, SearchPreferenceCollection searchPrefs,
            int brandID, List<int> membersForResults, ScrubList scrubList, ISettingsSA settingsService, byte frequency)
        {
            BulkMailQueueItem queueItem = createQueueItem(memberID, communityID, searchPrefs, null, frequency);
            MemberSearchMock searchMock = new MemberSearchMock(membersForResults, membersForResults.Count);

            Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);
            
            BulkMailBL bulkMailBL = new BulkMailBL();
            bulkMailBL.Initialize();
            bulkMailBL.IsDebug = true;
            bulkMailBL.MemberService = _memberMock;
            bulkMailBL.MemberSearchService = searchMock;
            if (null != settingsService) bulkMailBL.SettingsService = settingsService;

            return bulkMailBL.ProcessMatchMailQueueItem(new PerformanceCounterHelper(),
                queueItem,
                MailHandlerUtils.GetIMemberDTO(_memberMock, memberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None),
                brand,
                scrubList,
                _memberMock,
                searchMock);
        }

        private BulkMailQueueItem createQueueItem(int memberID, int communityID, SearchPreferenceCollection preferences, string[] sentMembers, byte frequency)
        {
            BulkMailQueueItem queueItem = new BulkMailQueueItem(memberID, communityID,
                ExternalMail.ValueObjects.BulkMailType.MatchMail,
                frequency,
                DateTime.Parse("01-01-2011"),
                DateTime.Parse("01-01-2011"),
                DateTime.Parse("01-01-2011"),
                0,
                DateTime.Parse("01-01-2011"),
                DateTime.Parse("01-01-2011"),
                sentMembers,
                preferences);

            return queueItem;
        }
    }

    

    public class GetMemberMock: IGetMember
    {
        Dictionary<int,  Matchnet.Member.ServiceAdapters.Member> members = new Dictionary<int,Member.ServiceAdapters.Member>();

        int _brandInsertAttID = 0;
        int _birthdateAttID = 0;
        int _genderMaskAttID = 0;
        int _regionIDAttID = 0;

        public GetMemberMock(int brandInsertAttID, int birthdateAttID, int genderMaskAttID, int regionIDAttID)
        {
            _brandInsertAttID = brandInsertAttID;
            _birthdateAttID = birthdateAttID;
            _genderMaskAttID = genderMaskAttID;
            _regionIDAttID = regionIDAttID;
        }

        public void AddMember(int memberID, DateTime brandInsertDate, DateTime birthDate, int genderMask, int regionID)
        {
            CachedMember cachedMember = new CachedMember(memberID);
            cachedMember.SetAttributeDate(_brandInsertAttID, brandInsertDate);
            cachedMember.SetAttributeDate(_birthdateAttID, birthDate);
            cachedMember.SetAttributeInt(_genderMaskAttID, genderMask);
            cachedMember.SetAttributeInt(_regionIDAttID, regionID);

            Matchnet.Member.ServiceAdapters.Member member = new Member.ServiceAdapters.Member(cachedMember);
            members.Add(memberID, member);
        }

        public Matchnet.Member.ServiceAdapters.Member GetMember(int memberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags flags)
        {
            if (members.ContainsKey(memberID)) 
                return members[memberID];
            else
                return null;
        }

        public int GetMemberIDByEmail(string emailAddress)
        {
            throw new NotImplementedException();
        }

        public int ValidateResetPasswordToken(string tokenGuidString)
        {
            throw new NotImplementedException();
        }

        public Member.ValueObjects.Admin.ImpersonateToken ValidateImpersonateToken(string impersonateTokenGUID)
        {
            throw new NotImplementedException();
        }

        public ArrayList GetMembers(ArrayList memberIDs, Member.ServiceAdapters.MemberLoadFlags memberLoadFlags)
        {
            throw new NotImplementedException();
        }

        public Member.ValueObjects.Interfaces.IMemberDTO GetMemberDTO(int memberID, Member.ValueObjects.MemberDTO.MemberType type)
        {
            if (members.ContainsKey(memberID))
                return members[memberID];
            else
                return null;
        }

        public ArrayList GetMemberDTOs(ArrayList memberIDs, Member.ValueObjects.MemberDTO.MemberType type)
        {
            throw new NotImplementedException();
        }
    }


    public class MemberSearchMock: IMemberSearchSA 
    {
        MatchnetQueryResults _results = null;
        
        public MemberSearchMock(List<int> memberIDs, int matchesReturned)
        {
            _results = new MatchnetQueryResults(memberIDs.Count);
            foreach(int memberID in memberIDs)
            {
                IMatchnetResultItem item = new MatchnetResultItem(memberID);
                _results.AddResult(item);            
            }
        }

        public MatchnetQueryResults  Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize, int pMemberID, SearchEngineType pSearchEngineType, SearchType pSearchType, bool ignoreCache)
        {
 	        return _results;
        }

        public MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize, int pMemberID, bool ignoreSACache, bool ignoreSAFilteredCacheOnly, bool ignoreAllCache, SearchType pSearchType)
        {
            throw new NotImplementedException();
        }

        public MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize, int pMemberID, bool ignoreSAFilteredCacheOnly, SearchType pSearchType)
        {
            throw new NotImplementedException();
        }

        public MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize, int pMemberID)
        {
            throw new NotImplementedException();
        }

        public MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize)
        {
            throw new NotImplementedException();
        }

        public MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize, int pMemberID, SearchEngineType pSearchEngineType, SearchType pSearchType, bool ignoreSACache, bool ignoreSAFilteredCacheOnly, bool ignoreAllCache)
        {
            throw new NotImplementedException();
        }

        public MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize, int pMemberID, SearchEngineType pSearchEngineType, SearchType pSearchType, bool ignoreSACache, bool ignoreSAFilteredCacheOnly)
        {
            throw new NotImplementedException();
        }

        public MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize, SearchEngineType pSearchEngineType, SearchType pSearchType)
        {
            throw new NotImplementedException();
        }

        public ArrayList SearchDetailed(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID)
        {
            throw new NotImplementedException();
        }

        public ArrayList SearchExplainDetailed(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID)
        {
            throw new NotImplementedException();
        }

        public MatchnetQueryResults SearchForPushFlirtMatches(ISearchPreferences pPreferences, int memberID, int communityID, int siteID, int brandID, int numMatchesToReturn)
        {
            throw new NotImplementedException();
        }

        public MatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, int pStartRow, int pPageSize, int pMemberID, SearchEngineType pSearchEngineType, SearchType pSearchType, SearchEntryPoint pSearchEntryPoint, bool ignoreSACache, bool ignoreSAFilteredCacheOnly, bool ignoreAllCache)
        {
            throw new NotImplementedException();
        }
    }

    public class MockSettingsService : ISettingsSA
    {

        private Dictionary<string, string> dictionary = new Dictionary<string, string>();
 
        public void AddSetting(string name, string value)
        {
            dictionary.Add(name, value);
        }

        public void RemoveSetting(string name)
        {
            dictionary.Remove(name);
        }

        #region ISettingsSA Members

        public List<Matchnet.Configuration.ValueObjects.MembaseConfig> GetMembaseConfigsFromSingleton()
        {
            throw new NotImplementedException();
        }

        public Matchnet.Configuration.ValueObjects.ServiceInstanceConfig GetServiceInstanceConfigFromSingleton(string serviceConstant, string machineName)
        {
            throw new NotImplementedException();
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID, int brandID)
        {
            return dictionary[constant];
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID)
        {
            return dictionary[constant];
        }

        public string GetSettingFromSingleton(string constant, int communityID)
        {
            return dictionary[constant];
        }

        public string GetSettingFromSingleton(string constant)
        {
            return dictionary[constant];
        }

        public bool IsServerEnabledFromSingleton()
        {
            throw new NotImplementedException();
        }

        public bool SettingExistsFromSingleton(string constant, int communityID, int siteID)
        {
            return dictionary.ContainsKey(constant);
        }

        public List<Configuration.ValueObjects.ApiAppSetting> GetAPIAppSettingsFromSingleton()
        {
            throw new NotImplementedException();
        }

        public void AddNewOrUpdateSetting(string settingConstant, string globalDefaultValue, string settingDescription,
            int settingCategoryId = 2, bool isRequiredForCommunity = false, bool isRequiredForSite = false)
        {
            throw new NotImplementedException();
        }

        public void UpdateGlobalDefault(string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void SetValueForSite(int siteId, string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void ResetToGlobalDefault(string settingConstant)
        {
            throw new NotImplementedException();
        }

        public Configuration.ValueObjects.MembaseConfig GetMembaseConfigByBucketFromSingleton(string bucketName)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
