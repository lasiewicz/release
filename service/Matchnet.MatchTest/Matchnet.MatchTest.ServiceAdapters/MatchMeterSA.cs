﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects.Interfaces;
using Matchnet.RemotingClient;
using Matchnet.MatchTest.ValueObjects.MatchMeter;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Exceptions;

using System.Net;
using System.IO;

using Matchnet.Caching;
using System.Threading;
using Matchnet.DistributedCaching.ServiceAdapters;


namespace Matchnet.MatchTest.ServiceAdapters
{
    public class MatchMeterSA : SABase
    {

        #region Fields (18)

        public static readonly MatchMeterSA Instance = new MatchMeterSA();
        private Cache _cache;

        // Zoozamen web methods
        //private const string BEST_MATCHES = "Zoozamen_BestMatches?Z_PartnerID={0}&Z_MemberID={1}&Z_GenderPreference={2}&Z_MemberStatus={3}&Z_Threshold={4}&Z_BachSize={5}&Z_BatchNumber={6}&Z_Langauge={7}";
        //private const string BEST_MATCHES = "Zoozamen_BestMatches?Z_PartnerID={0}&Z_MemberID={1}&Z_GenderPreference={2}&Z_MemberStatus={3}&Z_Threshold={4}&Z_BatchSize={5}&Z_BatchNumber={6}&Z_LanguageID={7}";
        private const string BEST_MATCHES = "Zoozamen_BestMatches?Z_PartnerID={0}&Z_MemberID={1}&Z_GenderPreference={2}&Z_MemberStatus={3}&Z_Threshold={4}&Z_BatchSize={5}&Z_BatchNumber={6}&Z_LanguageID={7}&Z_BachSize={8}&Z_Langauge={9}";
        private const string ONE_ON_ONE = "Zoozamen_OneOnOne?Z_PartnerID={0}&Z_MemberID={1}&Z_MemberStatus={2}&Z_MatchToMemberID={3}&Z_MatchToMemberStatus={4}&Z_Langauge={5}";
        private const string PERSONAL_FEEDBACK = "Zoozamen_PersonalFeedbak?Z_PartnerID={0}&Z_MemberID={1}&Z_LanguageID={2}&Z_UserStat={3}";
        private const string RE_TEST = "Zoozamen_ReTest?Z_PartnerID={0}&Z_MemberID={1}";
        private const string RESET_PERSONALITY_TEST = "Zoozamen_ResetPersonalityTest?Z_PartnerID={0}&Z_MemberID={1}&Z_MemberStatus={2}";
        private const string MEMBER_STATUS = "Zoozamen_MemberStatus?Z_PartnerID={0}&Z_MemberID={1}";
        private const string USER_INFO_UPDATE = "Zoozamen_UserInfoUpdate?Z_PartnerID={0}&Z_MemberID={1}&Z_ParameterToUpdate={2}&Z_NewValue={3}";

        // Zoozamen web methods V3
        private const string ONE_ON_ONE_V3 = "Zoozamen_OneOnOne_Relative?Z_PartnerID={0}&Z_MemberID={1}&Z_MemberStatus={2}&Z_MatchToMemberID={3}&Z_MatchToMemberStatus={4}&Z_Language={5}";
        private const string PERSONAL_FEEDBACK_V3 = "Zoozamen_PersonalFeedbak?Z_PartnerID={0}&Z_MemberID={1}&Z_LanguageID={2}&Z_UserStat={3}";
        private const string BEST_MATCHES_V3 = "Zoozamen_BestMatches?Z_PartnerID={0}&Z_MemberID={1}&Z_GenderPreference={2}&Z_MemberStatus={3}&Z_Threshold={4}&Z_BatchSize={5}&Z_BatchNumber={6}&Z_LanguageID={7}";
        private const string ZOOZAMEN_PARTENER_ID_V3_JDIL = "SPARK.JDIL.V3";
        private const string ZOOZAMEN_PARTENER_ID_V3_JDUK = "SPARK.JDUK.V3";

        private const string Z_BATCHNUMBER = "1";
        private const string Z_BOTH = "Both";
        private const string Z_FEMALE = "Female";
        private const string Z_MALE = "Male";
        private const string GENDER_PARAMETER_UPDATE_NAME = "gender";
        private const string GENDER_MALE_PARAMETER_VALUE = "m";
        private const string GENDER_FEMALE_PARAMETER_VALUE = "f";

        private const string SEARCH_RESULTS_NAMED_CACHE = "searchresults";

        private static bool ZoozamenServerAvailable = true;
        private static DateTime ZoozamenServerTimeoutTimeStamp;
        private static object ZoozamenServerAvailableLockObject = new object();
        public const string SA_NAME = "Matchnet.MatchTest.ServiceAdapter";

        #endregion Fields

        #region Constructors (1)

        private MatchMeterSA()
        {
            _cache = Cache.Instance;
        }

        #endregion Constructors

        #region Methods (12)


        // Public Methods (5) 

        private string GetZoozamenLanguageConst(Brand brand)
        {
            return Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MATCH_METER_LANGUAGE_CONST", brand.Site.Community.CommunityID, brand.Site.SiteID);
        }
        private string GetZoozamenPartnerIDConst(Brand brand)
        {
            // return "SPARK.JDIL.V3";
            return Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MATCH_METER_PARTNER_ID_CONST", brand.Site.Community.CommunityID, brand.Site.SiteID);
        }

        private string GetExcludedListCacheKey(int memberID, int genderMask, Brand brand, int regionID, int minage, int maxage, bool isSubscriber, bool photoRequired)
        {
            string orginalCacheKey = BestMatchesResultsFromZooz.GetCacheKey(memberID, genderMask, GetZoozamenLanguageConst(brand));
            return string.Join("_", new string[] {
                "EXCL_ZOOZ",
                orginalCacheKey,
                brand.BrandID.ToString(),
                regionID.ToString(),
                minage.ToString(),
                maxage.ToString(),
                isSubscriber.ToString(),
                photoRequired.ToString()
            });
        }

        private string GetExcludedListMatchScoreCacheKey(string cacheKey)
        {
            return (cacheKey + "_MS");
        }

        public BestMatches GetBestMatches(int memberID, Brand brand, int regionID, int minAge, int maxAge, Int32 genderMask, bool isSubscriber, int pStartRow, int pageSize, bool photoRequired = false)
        {
            // Try getting the best matches results from cache
            /*BestMatchesResultsFromZooz bmresults = _cache.Get(BestMatchesResultsFromZooz.GetCacheKey(memberID, genderMask, brand.Site.LanguageID)) as BestMatchesResultsFromZooz;
            if (bmresults == null)
            {
                // No cache, getting the results from Zoozamen and cache them
                bmresults = GetBestMatchesFromZoozamen(memberID, genderMask, isSubscriber, int.Parse(Z_BATCHSIZE), brand.Site.LanguageID);
                _cache.Add(bmresults);
            }*/

            // Is profile blocking enabled?  If it is, we need an extra where clause in the query
            bool isProfileBlockingEnabled = false;
            isProfileBlockingEnabled = Convert.ToBoolean(RuntimeSettings.GetSetting("PROFILE_BLOCK_ENABLED", brand.Site.Community.CommunityID));
            string distributedCacheKey = GetExcludedListCacheKey(memberID, genderMask, brand, regionID, minAge, maxAge, isSubscriber, photoRequired);

            // This will be the return value
            BestMatches bm = new BestMatches()
            {
                //MatchesCount = bmresults.MatchesCount,
                MatchResultsDic = new Dictionary<IMemberDTO, decimal>()
            };

            if (isProfileBlockingEnabled)
            {
                // check the distributed cache and it cache hit happens just process everything here and return
                int[] arrMemberIds = DistributedCachingSA.Instance.GetArray(distributedCacheKey, SEARCH_RESULTS_NAMED_CACHE) as int[];
                decimal[] arrMatchScores = DistributedCachingSA.Instance.GetArray(GetExcludedListMatchScoreCacheKey(distributedCacheKey), SEARCH_RESULTS_NAMED_CACHE) as decimal[];

                if (arrMemberIds != null && arrMatchScores != null && arrMemberIds.Count() == arrMatchScores.Count())
                {
                    bm.MatchesCount = arrMemberIds.Count();
                    for (int i = pStartRow; i < (pStartRow + pageSize) && i < arrMemberIds.Count(); i++)
                    {
                        bm.MatchResultsDic.Add(MemberSA.Instance.GetMember(arrMemberIds[i], MemberLoadFlags.None), arrMatchScores[i]);
                    }

                    return bm;
                }
            }

            // call out to zoozamen to get the data. GetBestMatchesFromZoozamen actually has caching inside it too.
            int batchSize = Conversion.CInt(Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MATCH_METER_BEST_MATCHES_BATCH_SIZE", brand.Site.Community.CommunityID, brand.Site.SiteID), 400);
            BestMatchesResultsFromZooz bmresults = GetBestMatchesFromZoozamen(memberID, genderMask, isSubscriber, batchSize, brand);

            if (!bmresults.IsInLongTimeoutRequest)
            {
                // Loading the member objects and setting the Depth1RegionID for each of the member objects
                foreach (MatchResultItem mri in bmresults.MatchResults)
                {
                    // do the loading of member data here for the query later on
                    Matchnet.Member.ServiceAdapters.Member m = MemberSA.Instance.GetMember(mri.MemberID, MemberLoadFlags.None);
                    if (m != null)
                    {
                        mri.Age = GetAge(m, brand);
                        mri.Depth1RegionID = RegionSA.Instance.RetrievePopulatedHierarchy(m.GetAttributeInt(brand, "RegionID"), brand.Site.LanguageID).Depth1RegionID;
                        mri.RegionID = m.GetAttributeInt(brand, "RegionID");
                        mri.GlobalStatusMask = m.GetAttributeInt(brand, "GlobalStatusMask");
                        mri.SelfSuspended = m.GetAttributeBool(brand.Site.Community.CommunityID, brand.Site.SiteID, Constants.NULL_INT, "SelfSuspendedFlag");
                        mri.ProperlyLoaded = true;
                        mri.HasPhoto = m.HasAnyApprovedPhoto(brand.Site.Community.CommunityID);
                    }

                }

                // Filtering the results
                List<MatchResultItem> filteredResults = null;
                if (isProfileBlockingEnabled)
                {
                    // consider the excludelistinternal in the query
                    Matchnet.List.ServiceAdapters.List list = Matchnet.List.ServiceAdapters.ListSA.Instance.GetList(memberID);
                    if (list != null)
                    {

                        filteredResults = bmresults.MatchResults.Where(mr => mr.ProperlyLoaded &&
                                                                !list.IsHotListed(Matchnet.List.ValueObjects.HotListCategory.ExcludeListInternal, brand.Site.Community.CommunityID, mr.MemberID) &&
                                                                mr.Age >= (minAge) &&
                                                                mr.Age <= (maxAge) &&
                                                                (photoRequired ? mr.HasPhoto : true) && // photo is required
                                                                (
                                                                    (regionID > 0 ? mr.Depth1RegionID == regionID : true)
                                                                    ||
                                                                    (regionID > 0 ? mr.RegionID == regionID : true)
                                                                ) &&
                                                                !((mr.GlobalStatusMask & 1) == 1) && // Not admin suspended
                                                                !mr.SelfSuspended   // Not self suspended from the site 
                                                            ).ToList();

                        if (filteredResults != null)
                        {
                            // cache this result to our distributed cache
                            var memberIds = from m in filteredResults
                                            select m.MemberID;

                            var matchScores = from m in filteredResults
                                              select m.MatchScore;

                            DistributedCachingSA.Instance.Put(distributedCacheKey,
                                memberIds.ToArray(),
                                SEARCH_RESULTS_NAMED_CACHE);

                            DistributedCachingSA.Instance.Put(GetExcludedListMatchScoreCacheKey(distributedCacheKey),
                                matchScores.ToArray(),
                                SEARCH_RESULTS_NAMED_CACHE);
                        }
                    }
                }
                else
                {
                    filteredResults = bmresults.MatchResults.Where(mr => mr.ProperlyLoaded &&
                                                                mr.Age >= (minAge) &&
                                                                mr.Age <= (maxAge) &&
                                                                (
                                                                    (regionID > 0 ? mr.Depth1RegionID == regionID : true)
                                                                    ||
                                                                    (regionID > 0 ? mr.RegionID == regionID : true)
                                                                ) &&
                                                                !((mr.GlobalStatusMask & 1) == 1) && // Not admin suspeneded
                                                                !mr.SelfSuspended // Not self suspended from the site
                                                            ).ToList();
                }

                // prepare what is to be returned here
                if (filteredResults != null)
                {
                    bm.MatchesCount = filteredResults.Count;
                    // Adding the filtered results to the result dictonary + paging
                    for (int i = pStartRow; i < (pStartRow + pageSize) && i < filteredResults.Count; i++)
                    {
                        bm.MatchResultsDic.Add(MemberSA.Instance.GetMember(filteredResults[i].MemberID, MemberLoadFlags.None), filteredResults[i].MatchScore);
                    }
                }

            }
            else
            {
                bm = null; // When best matches results exceeded the timeout.
            }

            return bm;
        }

        public OneOnOne GetOneOnOne(Int32 memberID, bool isMemberSubscriber, Int32 matchMemberID, bool isMatchMemberSubscriber, Brand brand)
        {
            if (IsJMeterV3(brand))
            {
                return GetOneOnOneV3(memberID, isMemberSubscriber, matchMemberID, isMatchMemberSubscriber, brand);
            }

            //ONE_ON_ONE = "Zoozamen_OneOnOne?Z_PartnerID={0}&Z_MemberID={1}&Z_MemberStatus={2}&Z_MatchToMemberID={3}&Z_MatchToMemberStatus={4}&Z_Langauge={5}";
            string url = string.Format(ONE_ON_ONE, GetZoozamenPartnerIDConst(brand), memberID.ToString(), GetZoozamenMemberStatus(isMemberSubscriber), matchMemberID.ToString(), GetZoozamenMemberStatus(isMatchMemberSubscriber), GetZoozamenLanguageConst(brand));

            XElement l = ((XElement)GetXDocument(url, false, brand).Elements().ElementAt(0).FirstNode);
            OneOnOneStatus oooStat;
            Int32 oneOnOneCategoriesCount = 0;
            string description = string.Empty;
            decimal matchScore = 0;
            List<OneOnOneCategoryItem> ooofcil = null;
            DateTime matchMemberTestDate = DateTime.MinValue;
            DateTime memberTestDate = DateTime.MinValue;

            switch (l.Attribute("ST").Value)
            {
                case "E1-0": oooStat = OneOnOneStatus.RequestUserNotTaken; break;
                case "E1-1": oooStat = OneOnOneStatus.RequestUserNotCompleted; break;
                case "E2-0": oooStat = OneOnOneStatus.MatchMemberNotTaken; break;
                case "E2-1": oooStat = OneOnOneStatus.MatchMemberNotCompleted; break;
                case "1": oooStat = OneOnOneStatus.Success; break;
                default: oooStat = OneOnOneStatus.MatchMemberNotTaken; break;
            }
            if (oooStat == OneOnOneStatus.Success)
            {
                if (l.Attribute("TD") != null)
                {
                    memberTestDate = DateTime.Parse(l.Attribute("TD").Value);
                }

                if (l.Attribute("TD") != null)
                {
                    matchMemberTestDate = DateTime.Parse(l.Attribute("MT").Value);
                }
                matchScore = decimal.Parse(l.Attribute("MS").Value);
                description = l.Attribute("MD").Value;
                oneOnOneCategoriesCount = Int32.Parse(l.Attribute("FC").Value);
                if (oneOnOneCategoriesCount > 0)
                {
                    var q = from c in l.Elements()
                            select new OneOnOneCategoryItem()
                            {
                                CategoryName = (string)c.Attribute("CN"),
                                Comment = (string)c.Attribute("CC"),
                                LeftHeader = (string)c.Attribute("LH"),
                                RightHeader = (string)c.Attribute("RH"),
                                Score = Int32.Parse(c.Attribute("CS").Value)
                                //Score = 5
                            };
                    ooofcil = q.ToList();
                }
            }
            return new OneOnOne()
            {
                Description = description,
                MatchMemberTestDate = matchMemberTestDate,
                MatchScore = matchScore,
                MatchStatus = oooStat,
                MemberTestDate = memberTestDate,
                OneOnOneCategories = ooofcil,
                OneOnOneCategoriesCount = oneOnOneCategoriesCount
            };

        }

        public OneOnOne GetOneOnOneV3(Int32 memberID, bool isMemberSubscriber, Int32 matchMemberID, bool isMatchMemberSubscriber, Brand brand)
        {
            //ONE_ON_ONE_V3 = "Zoozamen_OneOnOne?Z_PartnerID={0}&Z_MemberID={1}&Z_MemberStatus={2}&Z_MatchToMemberID={3}&Z_MatchToMemberStatus={4}&Z_Langauge={5}";
            string url = string.Format(ONE_ON_ONE_V3, GetZoozamenPartnerIDConst(brand), memberID.ToString(), GetZoozamenMemberStatus(isMemberSubscriber), matchMemberID.ToString(), GetZoozamenMemberStatus(isMatchMemberSubscriber), GetZoozamenLanguageConst(brand));

            XElement l = ((XElement)GetXDocument(url, false, brand).Elements().ElementAt(0).FirstNode);
            OneOnOneStatus oooStat;
            Int32 oneOnOneCategoriesCount = 0;
            string description = string.Empty;
            decimal matchScore = 0;
            List<OneOnOneCategoryItem> ooofcil = null;
            DateTime matchMemberTestDate = DateTime.MinValue;
            DateTime memberTestDate = DateTime.MinValue;
            // v3 parameters
            Int32 relativeMatchScore = 0;
            decimal oneMatchScore = 0;

            switch (l.Attribute("ST").Value)
            {
                case "E1-0": oooStat = OneOnOneStatus.RequestUserNotTaken; break;
                case "E1-1": oooStat = OneOnOneStatus.RequestUserNotCompleted; break;
                case "E2-0": oooStat = OneOnOneStatus.MatchMemberNotTaken; break;
                case "E2-1": oooStat = OneOnOneStatus.MatchMemberNotCompleted; break;
                case "1": oooStat = OneOnOneStatus.Success; break;
                default: oooStat = OneOnOneStatus.MatchMemberNotTaken; break;
            }
            if (oooStat == OneOnOneStatus.Success)
            {
                if (l.Attribute("TD") != null)
                {
                    // memberTestDate = DateTime.Parse(l.Attribute("TD").Value);
                    memberTestDate = DateTime.ParseExact(l.Attribute("TD").Value, "M/d/yyyy h:m:s tt",
                                                         CultureInfo.InvariantCulture);
                }

                if (l.Attribute("TD") != null)
                {
                    // matchMemberTestDate = DateTime.Parse(l.Attribute("MT").Value);
                    matchMemberTestDate = DateTime.ParseExact(l.Attribute("MT").Value, "M/d/yyyy h:m:s tt",
                                                         CultureInfo.InvariantCulture);
                }
                matchScore = 0;// decimal.Parse(l.Attribute("MS").Value); Z_MatchScore (MS)  was deprecated in v3
                description = l.Attribute("MD").Value;
                relativeMatchScore = Int32.Parse(l.Attribute("RMS").Value);
                oneMatchScore = decimal.Parse(l.Attribute("OMS").Value);
                oneOnOneCategoriesCount = Int32.Parse(l.Attribute("FC").Value);

                if (oneOnOneCategoriesCount > 0)
                {
                    var q = from c in l.Elements()
                            select new OneOnOneCategoryItem()
                            {
                                CategoryName = (string)c.Attribute("CN"),
                                Comment = (string)c.Attribute("CT"),
                                LeftHeader = c.Attribute("LH") == null ? string.Empty : (string)c.Attribute("LH"),
                                RightHeader = c.Attribute("RH") == null ? string.Empty : (string)c.Attribute("RH"),
                                Score = Int32.Parse(c.Attribute("CS").Value),
                                ShowGraph = c.Attribute("SG") == null ? true : (c.Attribute("SG").Value == "0" ? false : true),
                                GraphYTitle = (string)c.Attribute("GYT"),
                                GraphYHigh = (string)c.Attribute("GYH"),
                                GraphYLow = (string)c.Attribute("GYL"),
                                GraphXTitle = (string)c.Attribute("GXT"),
                                GraphXRight = (string)c.Attribute("GXR"),
                                GraphXLow = (string)c.Attribute("GXL"),
                                GraphUserScore = decimal.Parse(c.Attribute("GUS").Value),
                                //GraphMatchToScore = decimal.Parse(c.Attribute("GMTS").Value),
                                GraphDescription = c.Attribute("GD").Value
                            };
                    ooofcil = q.ToList();
                }
            }
            return new OneOnOne()
            {
                Description = description,
                MatchMemberTestDate = matchMemberTestDate,
                MatchScore = matchScore,
                MatchStatus = oooStat,
                MemberTestDate = memberTestDate,
                RelativeMatchScore = relativeMatchScore,
                OneMatchScore = oneMatchScore,
                OneOnOneCategories = ooofcil,
                OneOnOneCategoriesCount = oneOnOneCategoriesCount
            };

        }

        public PersonalFeedback GetPersonalFeedback(Int32 MemberID, Brand brand, bool isSubscriber)
        {
            if (IsJMeterV3(brand))
            {
                return GetPersonalFeedbackV3(MemberID, brand, isSubscriber);
            }
            string url = string.Format(PERSONAL_FEEDBACK, GetZoozamenPartnerIDConst(brand), MemberID.ToString(), GetZoozamenLanguageConst(brand), GetZoozamenMemberStatus(isSubscriber));
            XElement l = ((XElement)GetXDocument(url, false, brand).Elements().ElementAt(0).FirstNode);

            MatchTestStatus mtStat;

            switch (l.Attribute("ST").Value)
            {
                case "E-0": mtStat = MatchTestStatus.NotTaken; break;
                case "E-1": mtStat = MatchTestStatus.NotCompleted; break;
                case "G-0": mtStat = MatchTestStatus.MinimumCompleted; break;
                case "G-1": mtStat = MatchTestStatus.Completed; break;
                default: mtStat = MatchTestStatus.NotTaken; break;
            }
            DateTime testDate = DateTime.MinValue;
            if (l.Attribute("TD") != null)
            {
                testDate = DateTime.Parse(l.Attribute("TD").Value);
            }

            Int32 fc = Int32.Parse(l.Attribute("FC").Value);
            List<PersoanlFeedbackCatergoryItem> pfcis = null;
            if (fc > 0)
            {
                var q = from c in l.Elements()
                        select new PersoanlFeedbackCatergoryItem()
                        {
                            CategoryName = (string)c.Attribute("CN"),
                            Comment = (string)c.Attribute("CC"),
                            LeftHeader = (string)c.Attribute("LH"),
                            RightHeader = (string)c.Attribute("RH"),
                            Score = (Int32)c.Attribute("CS")
                        };
                pfcis = q.ToList();
            }

            return new PersonalFeedback()
            {
                MemberStatus = mtStat,
                TestDate = testDate,
                FeedBackCategoriesCount = fc,
                FeedbackCategories = pfcis
            };
        }

        public PersonalFeedback GetPersonalFeedbackV3(Int32 MemberID, Brand brand, bool isSubscriber)
        {
            // PERSONAL_FEEDBACK_V3 = "Zoozamen_PersonalFeedbak?Z_PartnerID={0}&Z_MemberID={1}&Z_LanguageID={2}&Z_UserStat={3}";
            string url = string.Format(PERSONAL_FEEDBACK_V3, GetZoozamenPartnerIDConst(brand), MemberID.ToString(), GetZoozamenLanguageConst(brand), GetZoozamenMemberStatus(isSubscriber));
            XElement l = ((XElement)GetXDocument(url, false, brand).Elements().ElementAt(0).FirstNode);

            MatchTestStatus mtStat;

            switch (l.Attribute("ST").Value)
            {
                case "E-0": mtStat = MatchTestStatus.NotTaken; break;
                case "E-1": mtStat = MatchTestStatus.NotCompleted; break;
                case "G-0": mtStat = MatchTestStatus.MinimumCompleted; break;
                case "G-1": mtStat = MatchTestStatus.Completed; break;
                default: mtStat = MatchTestStatus.NotTaken; break;
            }
            DateTime testDate = DateTime.MinValue;
            if (l.Attribute("TD") != null)
            {
                testDate = DateTime.Parse(l.Attribute("TD").Value);
            }

            Int32 fc = Int32.Parse(l.Attribute("FC").Value);
            Int32 levelOfMaturity = Int32.Parse(l.Attribute("LM").Value);
            string levelOfMaturityTEXT = l.Attribute("LMT").Value;
            List<PersoanlFeedbackCatergoryItem> pfcis = null;
            if (fc > 0)
            {
                var q = from c in l.Elements()
                        select new PersoanlFeedbackCatergoryItem()
                        {
                            CategoryName = (string)c.Attribute("CN"),
                            Comment = c.Attribute("CC") == null ? (string)c.Attribute("CT") : (string)c.Attribute("CC"),
                            LeftHeader = c.Attribute("LH") == null ? (string)c.Attribute("RLT") : (string)c.Attribute("LH"),
                            RightHeader = c.Attribute("RH") == null ? (string)c.Attribute("RRT") : (string)c.Attribute("RH"),
                            Score = (Int32)c.Attribute("CS"),
                            ShowRuler = c.Attribute("SR").Value == "0" ? false : true,
                            RulerLeftText = c.Attribute("RLT").Value,
                            RulerRightText = c.Attribute("RRT").Value,
                            ShowGraph = c.Attribute("SG").Value == "0" ? false : true,
                            GraphYTitle = c.Attribute("GYT").Value,
                            GraphYHigh = c.Attribute("GYH").Value,
                            GraphYLow = c.Attribute("GYL").Value,
                            GraphXTitle = c.Attribute("GXT").Value,
                            GraphXRight = c.Attribute("GXR").Value,
                            GraphXLow = c.Attribute("GXL").Value,
                            GraphUserScore = decimal.Parse(c.Attribute("GUS").Value),
                            GraphDescription = c.Attribute("GD").Value,
                        };
                pfcis = q.ToList();
            }

            return new PersonalFeedback()
            {
                MemberStatus = mtStat,
                TestDate = testDate,
                LevelOfMaturity = levelOfMaturity,
                LevelOfMaturityTEXT = levelOfMaturityTEXT,
                FeedBackCategoriesCount = fc,
                FeedbackCategories = pfcis
            };
        }

        // Deletes the user from Zoozamens DB
        public bool ResetPersonalityTest(Int32 memberID, bool isSubscriber, Brand brand)
        {
            // private const string RESET_PERSONALITY_TEST = "Zoozamen_ResetPersonalityTest?Z_PartnerID={0}&Z_MemberID={1}&Z_MemberStatus={2}";
            string url = string.Format(RESET_PERSONALITY_TEST, GetZoozamenPartnerIDConst(brand), memberID.ToString(), GetZoozamenMemberStatus(isSubscriber));
            XDocument doc = GetXDocument(url, false, brand);
            // _cache.Remove(CacheableMatchTestStatus.GetCacheKey(memberID));
            SetMatchTestStatusToDefault(MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None), brand);
            return (((XElement)doc.FirstNode).Value == "1") ? true : false;
        }

        // Allowing the user to modify his answers (currenrly not in use)
        public bool Retest(Int32 memberID, Brand brand)
        {
            string url = string.Format(RE_TEST, GetZoozamenPartnerIDConst(brand), memberID.ToString(), GetZoozamenLanguageConst(brand));
            XDocument doc = GetXDocument(url, false, brand);
            SetMatchTestStatusToDefault(MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None), brand);
            return (((XElement)doc.FirstNode).Value == "1") ? true : false;
        }

        private bool UserInfoUpdate(Int32 memberID, string parameterToUpdate, string newValue, Brand brand)
        {
            string url = string.Format(USER_INFO_UPDATE, GetZoozamenPartnerIDConst(brand), memberID.ToString(), parameterToUpdate, newValue);
            XDocument doc = GetXDocument(url, false, brand);
            return (((XElement)doc.FirstNode).Value == "1") ? true : false;
        }

        public bool UpdateUserGender(Int32 memberID, Int32 genderMask, Brand brand)
        {
            bool result = false;
            try
            {
                result = UserInfoUpdate(memberID, GENDER_PARAMETER_UPDATE_NAME, GetZoozamenUserGender(genderMask), brand);
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }



        // Protected Methods (1) 

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MATCHTESTSVC_SA_CONNECTION_LIMIT"));
        }

        // Private Methods (6) 

        private int GetAge(DateTime birthDate)
        {
            if (birthDate == DateTime.MinValue) return 0;
            return (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
        }

        private int GetAge(Matchnet.Member.ServiceAdapters.Member member, Brand brand)
        {
            return GetAge(member.GetAttributeDate(brand, "BirthDate", DateTime.MinValue));
        }

        private BestMatchesResultsFromZooz GetBestMatchesFromZoozamen(Int32 memberID, Int32 genderMask, bool isMemberSubscriber, Int32 batchSize, Brand brand)
        {

            BestMatchesResultsFromZooz bmresults = _cache.Get(BestMatchesResultsFromZooz.GetCacheKey(memberID, genderMask, GetZoozamenLanguageConst(brand))) as BestMatchesResultsFromZooz;
            if (bmresults == null)
            {
                // No cache, getting the results from Zoozamen and cache them

                //"Zoozamen_BestMatches?Z_PartnerID={0}&Z_MemberID={1}&Z_GenderPreference={2}&Z_MemberStatus={3}&Z_Threshold={4}&Z_BachSize={5}&Z_BatchNumber={6}&Z_Langauge={7}"

                string url = string.Format(BEST_MATCHES, GetZoozamenPartnerIDConst(brand), memberID.ToString(), GetZoozamenGender(genderMask), GetZoozamenMemberStatus(isMemberSubscriber), batchSize.ToString(), batchSize.ToString(), Z_BATCHNUMBER, GetZoozamenLanguageConst(brand), batchSize.ToString(), GetZoozamenLanguageConst(brand));
                try
                {
                    XElement l = ((XElement)GetXDocument(url, false, brand).Elements().ElementAt(0).FirstNode);

                    int matchesCount = int.Parse(l.Attribute("TM").Value);

                    List<MatchResultItem> matchResultsList = null;

                    if (matchesCount > 0)
                    {
                        int memID;

                        if (IsJMeterV3(brand))
                        {
                            var q = from c in l.Elements()
                                    where int.TryParse(c.Attribute("ID").Value, out memID)
                                    select new MatchResultItem()
                                    {
                                        MemberID = int.Parse(c.Attribute("ID").Value),
                                        // TestDate = DateTime.Parse(c.Attribute("TD").Value),
                                        TestDate = DateTime.ParseExact(c.Attribute("TD").Value, "M/d/yyyy h:m:s tt", CultureInfo.InvariantCulture),
                                        MatchScore = Decimal.Parse(c.Attribute("MS").Value),
                                    };
                            matchResultsList = q.ToList();
                        }
                        else
                        {
                            var q = from c in l.Elements()
                                    where int.TryParse(c.Attribute("ID").Value, out memID)
                                    select new MatchResultItem()
                                    {
                                        MemberID = int.Parse(c.Attribute("ID").Value),
                                        TestDate = DateTime.Parse(c.Attribute("TD").Value),
                                        MatchScore = Decimal.Parse(c.Attribute("MS").Value),
                                    };
                            matchResultsList = q.ToList();
                        }
                    }
                    bmresults = new BestMatchesResultsFromZooz()
                    {
                        MatchesCount = matchesCount,
                        MatchResults = matchResultsList,
                        LanguageConst = GetZoozamenLanguageConst(brand),
                        GenderMask = genderMask,
                        MemberID = memberID
                    };
                }
                catch (SAException SAex) // Timeout has exceeded
                {
                    bmresults = new BestMatchesResultsFromZooz() { IsInLongTimeoutRequest = true };
                    // Start a thread to get the matches with long timeout
                    //Thread t = new Thread(new ThreadStart(processSoapCycle));
                    Thread t = new Thread(GetBestMatchesWithLongTimeout);
                    t.Start(new BestMatchesThreadParameters()
                    {
                        BatchSize = batchSize,
                        GenderMask = genderMask,
                        IsMemberSubscriber = isMemberSubscriber,
                        LanguageConst = GetZoozamenLanguageConst(brand),
                        MemberID = memberID,
                        PartnerID = GetZoozamenPartnerIDConst(brand),
                        Brand = brand
                    });
                }

                //XElement l = ((XElement)XDocument.Load(@"C:\matchnet\bedrock\Service\Matchnet.MatchTest\Matchnet.MatchTest.Harness\JDILbestmatches.xml").Elements().ElementAt(0));

                _cache.Add(bmresults);
            }
            return bmresults;
        }

        private BestMatchesResultsFromZooz GetBestMatchesFromZoozamenV3(Int32 memberID, Int32 genderMask, bool isMemberSubscriber, Int32 batchSize, Brand brand)
        {
            //BEST_MATCHES_V3 = "Zoozamen_BestMatches?Z_PartnerID={0}&Z_MemberID={1}&Z_GenderPreference={2}&Z_MemberStatus={3}&Z_Threshold={4}&Z_BachSize={5}&Z_BatchNumber={6}&Z_Langauge={7}";
            BestMatchesResultsFromZooz bmresults = _cache.Get(BestMatchesResultsFromZooz.GetCacheKey(memberID, genderMask, GetZoozamenLanguageConst(brand))) as BestMatchesResultsFromZooz;
            if (bmresults == null)
            {
                // No cache, getting the results from Zoozamen and cache them

                //"Zoozamen_BestMatches?Z_PartnerID={0}&Z_MemberID={1}&Z_GenderPreference={2}&Z_MemberStatus={3}&Z_Threshold={4}&Z_BachSize={5}&Z_BatchNumber={6}&Z_Langauge={7}"

                string url = string.Format(BEST_MATCHES, GetZoozamenPartnerIDConst(brand), memberID.ToString(), GetZoozamenGender(genderMask), GetZoozamenMemberStatus(isMemberSubscriber), batchSize.ToString(), batchSize.ToString(), Z_BATCHNUMBER, GetZoozamenLanguageConst(brand), batchSize.ToString(), GetZoozamenLanguageConst(brand));
                try
                {
                    XElement l = ((XElement)GetXDocument(url, false, brand).Elements().ElementAt(0).FirstNode);

                    int matchesCount = int.Parse(l.Attribute("TM").Value);

                    List<MatchResultItem> matchResultsList = null;

                    if (matchesCount > 0)
                    {
                        int memID;
                        var q = from c in l.Elements()
                                where int.TryParse(c.Attribute("ID").Value, out memID)
                                select new MatchResultItem()
                                {
                                    MemberID = int.Parse(c.Attribute("ID").Value),
                                    TestDate = DateTime.Parse(c.Attribute("TD").Value),
                                    MatchScore = Decimal.Parse(c.Attribute("MS").Value),
                                };
                        matchResultsList = q.ToList();

                    }
                    bmresults = new BestMatchesResultsFromZooz()
                    {
                        MatchesCount = matchesCount,
                        MatchResults = matchResultsList,
                        LanguageConst = GetZoozamenLanguageConst(brand),
                        GenderMask = genderMask,
                        MemberID = memberID
                    };
                }
                catch (SAException SAex) // Timeout has exceeded
                {
                    bmresults = new BestMatchesResultsFromZooz() { IsInLongTimeoutRequest = true };
                    // Start a thread to get the matches with long timeout
                    //Thread t = new Thread(new ThreadStart(processSoapCycle));
                    Thread t = new Thread(GetBestMatchesWithLongTimeout);
                    t.Start(new BestMatchesThreadParameters()
                    {
                        BatchSize = batchSize,
                        GenderMask = genderMask,
                        IsMemberSubscriber = isMemberSubscriber,
                        LanguageConst = GetZoozamenLanguageConst(brand),
                        MemberID = memberID,
                        PartnerID = GetZoozamenPartnerIDConst(brand),
                        Brand = brand
                    });
                }

                //XElement l = ((XElement)XDocument.Load(@"C:\matchnet\bedrock\Service\Matchnet.MatchTest\Matchnet.MatchTest.Harness\JDILbestmatches.xml").Elements().ElementAt(0));

                _cache.Add(bmresults);
            }
            return bmresults;
        }
        //BestMatchesThreadParameters bmtp
        private void GetBestMatchesWithLongTimeout(object objBestMatchesThreadParameters)
        {
            if (objBestMatchesThreadParameters is BestMatchesThreadParameters)
            {
                BestMatchesThreadParameters bmtp = (BestMatchesThreadParameters)objBestMatchesThreadParameters;
                BestMatchesResultsFromZooz bmresults = new BestMatchesResultsFromZooz()
                {
                    IsInLongTimeoutRequest = false,
                    LanguageConst = bmtp.LanguageConst,
                    GenderMask = bmtp.GenderMask,
                    MemberID = bmtp.MemberID
                };
                try
                {
                    string url = string.Format(BEST_MATCHES, bmtp.PartnerID, bmtp.MemberID.ToString(), GetZoozamenGender(bmtp.GenderMask), GetZoozamenMemberStatus(bmtp.IsMemberSubscriber), bmtp.BatchSize.ToString(), bmtp.BatchSize.ToString(), Z_BATCHNUMBER, bmtp.LanguageConst, bmtp.BatchSize.ToString(), GetZoozamenLanguageConst(bmtp.Brand));

                    bool allowLongTimout = true;
                    XElement l = ((XElement)GetXDocument(url, allowLongTimout, bmtp.Brand).Elements().ElementAt(0).FirstNode);

                    int matchesCount = int.Parse(l.Attribute("TM").Value);

                    List<MatchResultItem> matchResultsList = null;

                    if (matchesCount > 0)
                    {
                        int memID;
                        var q = from c in l.Elements()
                                where int.TryParse(c.Attribute("ID").Value, out memID)
                                select new MatchResultItem()
                                {
                                    MemberID = int.Parse(c.Attribute("ID").Value),
                                    TestDate = DateTime.Parse(c.Attribute("TD").Value),
                                    MatchScore = Decimal.Parse(c.Attribute("MS").Value),
                                };
                        matchResultsList = q.ToList();

                    }
                    bmresults.MatchesCount = matchesCount;
                    bmresults.MatchResults = matchResultsList;


                    // Replacing the empty object with a new one.
                    _cache.Remove(bmresults.GetCacheKey());
                    _cache.Add(bmresults);
                }
                catch (Exception ex)
                {
                    _cache.Remove(bmresults.GetCacheKey());
                }
            }
        }

        private static XDocument GetXDocument(string url, bool allowLongTimout, Brand brand)
        {
            return RuntimeSettings.GetSetting("MATCHTEST_WEB_SERVICE_CALL_LOCK_ENABLED").ToLower() == "true" ?
                GetXDocumentWithLock(url, allowLongTimout, brand) :
                GetXDocumentWithoutLock(url, allowLongTimout, brand);
        }

        private static XDocument GetXDocumentWithoutLock(string url, bool allowLongTimout, Brand brand)
        {
            //StringBuilder sb = new StringBuilder();
            string responseString = string.Empty;

            url = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MATCHTEST_WEB_SERVICE_URL", brand.Site.Community.CommunityID, brand.Site.SiteID) + url;
            // url = "http://jdilws-pp.zoozamen.net/Zoozamen.asmx/" + url;
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Timeout = 3000; // Set timeout to 3 seconds.
            if (allowLongTimout)
            {
                request.Timeout = 10000; // In cases of long best maches request time.
            }

            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    using (StreamReader oResponseStream = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        responseString = oResponseStream.ReadToEnd();
                        oResponseStream.Close();
                    }
                    response.Close();
                }
            }
            catch (WebException ex)
            {
                throw new SAException("Response from Zoozamen server has timed out.", ex);
            }
            // return XDocument.Parse(sb.ToString().Replace("&lt;", "<").Replace("&gt;", ">"));
            return XDocument.Parse(responseString.Replace("&lt;", "<").Replace("&gt;", ">"));
        }

        private static XDocument GetXDocumentWithLock(string url, bool allowLongTimout, Brand brand)
        {
            XDocument result = new XDocument();

            // lock (ZoozamenServerAvailableLockObject)
            //    {
            if (!ZoozamenServerAvailable)
            {
                if (Monitor.TryEnter(ZoozamenServerAvailableLockObject))
                {
                    try
                    {
                        // Check the time limit
                        double timeLimit = double.Parse(RuntimeSettings.GetSetting("MATCHTEST_WEB_SERVICE_TIMEOUT_LIMIT"));

                        if ((DateTime.Now - ZoozamenServerTimeoutTimeStamp).TotalSeconds > timeLimit)
                        {
                            ZoozamenServerAvailable = true;
                        }

                    }
                    finally
                    {
                        Monitor.Exit(ZoozamenServerAvailableLockObject);
                    }
                }
            }

            if (ZoozamenServerAvailable)
            {
                string responseString = string.Empty;

                url = RuntimeSettings.GetSetting("MATCHTEST_WEB_SERVICE_URL", brand.Site.Community.CommunityID, brand.Site.SiteID) + url;
                //   url = "http://jdilv3-ws-prod.zoozamen.net/Zoozamen.asmx/" + url;
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                request.Timeout = 3000; // Set timeout to 3 seconds.
                if (allowLongTimout)
                {
                    request.Timeout = 10000; // In cases of long best maches request time.
                }

                try
                {
                    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                    {
                        using (StreamReader oResponseStream = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                        {
                            responseString = oResponseStream.ReadToEnd();
                            oResponseStream.Close();
                        }
                        response.Close();
                    }
                }
                catch (WebException ex)
                {
                    if (ZoozamenServerAvailable)
                    {
                        lock (ZoozamenServerAvailableLockObject)
                        {
                            ZoozamenServerAvailable = false;
                            ZoozamenServerTimeoutTimeStamp = DateTime.Now;
                            string message = "Response from Zoozamen server has timed out. Request:" + url + " Exception:" + ex.Message;
                            EventLog.WriteEntry(SA_NAME, message, EventLogEntryType.Warning);

                            throw new SAException("Response from Zoozamen server has timed out.", ex);
                        }
                    }
                }
                result = XDocument.Parse(responseString.Replace("&lt;", "<").Replace("&gt;", ">"));
            }
            else
            {
                throw new SAException("Zoozamen is currently in a timeout time limit.");
            }
            // }

            return result;
        }

        private string GetZoozamenGender(int genderMask)
        {
            string result = Z_BOTH;
            if (genderMask == 6)
            {
                result = Z_FEMALE;
            }
            else
            {
                if (genderMask == 9)
                {
                    result = Z_MALE;
                }
            }
            return result;
        }
        private string GetZoozamenUserGender(int genderMask)
        {
            string result = GENDER_FEMALE_PARAMETER_VALUE;
            if ((genderMask & 1) == 1)
            {
                result = GENDER_MALE_PARAMETER_VALUE;
            }
            return result;
        }

        private string GetZoozamenMemberStatus(bool isSubscriber)
        {
            return (isSubscriber ? "S" : "M");
        }

        public bool IsJMeterV3(Brand brand)
        {
            return (GetZoozamenPartnerIDConst(brand) == ZOOZAMEN_PARTENER_ID_V3_JDIL || GetZoozamenPartnerIDConst(brand) == ZOOZAMEN_PARTENER_ID_V3_JDUK) ? true : false;
        }

        /// <summary>
        /// Gets the cacheable match test status.
        /// </summary>
        /// <param name="MemberID">The member ID.</param>
        /// <param name="languageID">The language ID.</param>
        /// <param name="isSubscriber">if set to <c>true</c> [is subscriber].</param>
        /// <returns></returns>
        public MatchTestStatus GetMatchTestStatus(Int32 MemberID, bool isSubscriber, Brand brand)
        {
            //CacheableMatchTestStatus result = _cache.Get(CacheableMatchTestStatus.GetCacheKey(MemberID)) as CacheableMatchTestStatus;

            //if (result == null || result.MatchTestStatusValue == MatchTestStatus.NotTaken || result.MatchTestStatusValue == MatchTestStatus.NotCompleted)
            //{
            //    /*
            //    PersonalFeedback pf = GetPersonalFeedback(MemberID, languageID, isSubscriber);
            //    result = new CacheableMatchTestStatus() { MemberID = MemberID, MatchTestStatusValue = pf.MemberStatus };
            //     * */
            //    MatchTestStatus mts = GetMatchTestStatusFromZoozamen(MemberID, brand);
            //    result = new CacheableMatchTestStatus() { MemberID = MemberID, MatchTestStatusValue = mts };
            //    _cache.Add(result);
            //}
            //return result.MatchTestStatusValue;

            MatchTestStatus mts;

            Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(MemberID, MemberLoadFlags.None);
            int mtsInt = member.GetAttributeInt(brand, "ZoozamenMemberStatus", 0);
            if (mtsInt == 0)
            {
                mts = GetMatchTestStatusFromZoozamen(MemberID, brand);
                member.SetAttributeInt(brand, "ZoozamenMemberStatus", (int)mts);
                MemberSA.Instance.SaveMember(member);
            }
            else
            {
                mts = (MatchTestStatus)mtsInt;
            }

            return mts;
        }

        /// <summary>
        /// Sets the match test status to default so that next time the member status will be checked, the update value will be retrieved from Zoozamen
        /// </summary>
        /// <param name="member">The member.</param>
        /// <param name="brand">The brand.</param>
        public void SetMatchTestStatusToDefault(Member.ServiceAdapters.Member member, Brand brand)
        {
            member.SetAttributeInt(brand, "ZoozamenMemberStatus", 0);
            MemberSA.Instance.SaveMember(member);
        }

        private MatchTestStatus GetMatchTestStatusFromZoozamen(Int32 MemberID, Brand brand)
        {
            //private const string MEMBER_STATUS = "Zoozamen_MemberStatus?Z_PartnerID={0}&Z_MemberID={1}";
            string url = string.Format(MEMBER_STATUS, GetZoozamenPartnerIDConst(brand), MemberID.ToString());
            XElement l = ((XElement)GetXDocument(url, false, brand).Elements().ElementAt(0).FirstNode);

            MatchTestStatus mtStat;

            switch (l.Attribute("ST").Value)
            {
                case "E-0": mtStat = MatchTestStatus.NotTaken; break;
                case "E-1": mtStat = MatchTestStatus.NotCompleted; break;
                case "G-0": mtStat = MatchTestStatus.MinimumCompleted; break;
                case "G-1": mtStat = MatchTestStatus.Completed; break;
                default: mtStat = MatchTestStatus.NotTaken; break;
            }

            return mtStat;
        }


        #endregion Methods

        #region Nested Classes (2)

        [Serializable]
        private class BestMatchesResultsFromZooz : ICacheable
        {
            public const string CACHE_KEY = "BEST_MATCHES";
            private int _cacheTTLSeconds = 60 * 60 * 6;  // Default to 6 hours
            private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;

            #region Properties (7)

            public Int32 Depth1RegionID { get; set; }

            public Int32 MatchesCount { get; set; }

            public List<MatchResultItem> MatchResults { get; set; }

            public Decimal MatchScore { get; set; }

            public Int32 MemberID { get; set; }

            public Matchnet.Member.ServiceAdapters.Member MemberObj { get; set; }

            public DateTime TestDate { get; set; }

            public int GenderMask { get; set; }

            public string LanguageConst { get; set; }

            public bool IsInLongTimeoutRequest { get; set; }

            #endregion Properties


            #region ICacheable Members

            public CacheItemMode CacheMode
            {
                get
                {
                    return Matchnet.CacheItemMode.Absolute;
                }
            }

            public CacheItemPriorityLevel CachePriority
            {
                get
                {
                    return _cachePriority;
                }
                set
                {
                    _cachePriority = value;
                }
            }

            public int CacheTTLSeconds
            {
                get
                {
                    return _cacheTTLSeconds;
                }
                set
                {
                    _cacheTTLSeconds = value;
                }
            }
            public static string GetCacheKey(int _memberID, int _genderMask, string _languageConst)
            {
                return CACHE_KEY + "_" + _memberID.ToString() + "_" + _genderMask.ToString() + "_" + _languageConst;
            }
            public string GetCacheKey()
            {
                return GetCacheKey(MemberID, GenderMask, LanguageConst);
            }

            #endregion
        }
        [Serializable]
        private class MatchResultItem
        {

            #region Properties (5)

            public Int32 Depth1RegionID { get; set; }

            public Decimal MatchScore { get; set; }

            public Int32 MemberID { get; set; }

            //public Matchnet.Member.ServiceAdapters.Member MemberObj { get; set; }
            public bool ProperlyLoaded { get; set; }
            public int Age { get; set; }
            public int GlobalStatusMask { get; set; }
            public bool SelfSuspended { get; set; }
            public int RegionID { get; set; }

            public DateTime TestDate { get; set; }

            public bool HasPhoto { get; set; }

            #endregion Properties
        }


        private class BestMatchesThreadParameters
        {
            public Int32 MemberID { get; set; }
            public Int32 GenderMask { get; set; }
            public bool IsMemberSubscriber { get; set; }
            public Int32 BatchSize { get; set; }
            public string LanguageConst { get; set; }
            public string PartnerID { get; set; }
            public Brand Brand { get; set; }
        }

        [Serializable]
        private class CacheableMatchTestStatus : ICacheable
        {
            private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
            private int _cacheTTLSeconds = 60 * 60 * 3; // Three hours
            public const string CACHE_KEY = "MatchTestStatus";

            public MatchTestStatus MatchTestStatusValue { get; set; }
            public Int32 MemberID { get; set; }

            public CacheItemMode CacheMode
            {
                get
                {
                    return Matchnet.CacheItemMode.Absolute;
                }
            }

            public CacheItemPriorityLevel CachePriority
            {
                get
                {
                    return _cachePriority;
                }
                set
                {
                    _cachePriority = value;
                }
            }

            public int CacheTTLSeconds
            {
                get
                {
                    return _cacheTTLSeconds;
                }
                set
                {
                    _cacheTTLSeconds = value;
                }
            }

            /*public CacheableMatchTestStatus(int _memberID)
            {
                MemberID = _memberID;
            }*/

            public static string GetCacheKey(int _memberID)
            {
                return CACHE_KEY + "_" + _memberID.ToString();
            }
            public string GetCacheKey()
            {
                return GetCacheKey(MemberID);
            }
        }
        #endregion Nested Classes

    }
}
