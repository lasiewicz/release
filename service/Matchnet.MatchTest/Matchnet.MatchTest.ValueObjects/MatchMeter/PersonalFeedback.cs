﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Matchnet.MatchTest.ValueObjects.MatchMeter
{
    [Flags]
    public enum MatchTestStatus
    {
        NotTaken = 1,
        NotCompleted = 2,
        MinimumCompleted = 3,
        Completed = 4
    }
    [Serializable]
    public class PersonalFeedback : IValueObject
    {
        public MatchTestStatus MemberStatus { get; set; }
        public DateTime TestDate { get; set; }
        public Int32 FeedBackCategoriesCount { get; set; }
        public List<PersoanlFeedbackCatergoryItem> FeedbackCategories { get; set; }
        // V3 parameters
        public Int32 LevelOfMaturity { get; set; }
        public string LevelOfMaturityTEXT { get; set; }
    }
    [Serializable]
    public class PersoanlFeedbackCatergoryItem : IValueObject
    {
        public string CategoryName { get; set; }
        public string LeftHeader { get; set; }
        public string RightHeader { get; set; }
        public Int32 Score { get; set; }
        public string Comment { get; set; }
        // V3 parameters
        public bool ShowRuler { get; set; }
        public string RulerLeftText { get; set; }
        public string RulerRightText { get; set; }
        public bool ShowGraph { get; set; }
        public string GraphYTitle { get; set; }
        public string GraphYHigh { get; set; }
        public string GraphYLow { get; set; }
        public string GraphXTitle { get; set; }
        public string GraphXRight { get; set; }
        public string GraphXLow { get; set; }
        public decimal GraphUserScore { get; set; }
        public string GraphDescription { get; set; }
        
    }
}

