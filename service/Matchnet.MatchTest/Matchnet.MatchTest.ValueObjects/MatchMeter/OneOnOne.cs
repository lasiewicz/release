﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.MatchTest.ValueObjects.MatchMeter
{
    [Flags]
    public enum OneOnOneStatus
    {
        RequestUserNotTaken,
        RequestUserNotCompleted,
        MatchMemberNotTaken,
        MatchMemberNotCompleted,
        Success
    }
    [Serializable]
    public class OneOnOne : IValueObject
    {
        public OneOnOneStatus MatchStatus { get; set; }
        public decimal MatchScore { get; set; }
        public string Description { get; set; }
        public DateTime MemberTestDate { get; set; }
        public DateTime MatchMemberTestDate { get; set; }
        public Int32 OneOnOneCategoriesCount { get; set; }
        public List<OneOnOneCategoryItem> OneOnOneCategories { get; set; }
        // V3 parameters
        public Int32 RelativeMatchScore { get; set; }
        public decimal OneMatchScore { get; set; }


    }
    [Serializable]
    public class OneOnOneCategoryItem : IValueObject
    {
        public string CategoryName { get; set; }
        public string LeftHeader { get; set; }
        public string RightHeader { get; set; }
        public Int32 Score { get; set; }
        public string Comment { get; set; }
        // V3 parameters
        public bool ShowGraph { get; set; }
        public string GraphYTitle { get; set; }
        public string GraphYHigh { get; set; }
        public string GraphYLow { get; set; }
        public string GraphXTitle { get; set; }
        public string GraphXRight { get; set; }
        public string GraphXLow { get; set; }
        public decimal GraphUserScore { get; set; }
        //public decimal GraphMatchToScore { get; set; }
        public string GraphDescription { get; set; }
    }
}
