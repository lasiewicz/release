﻿using System;
using System.Collections.Generic;
using Matchnet.Member.ValueObjects.Interfaces;

namespace Matchnet.MatchTest.ValueObjects.MatchMeter
{
    public class BestMatches
    {
        public Int32 MatchesCount { get; set; }
        //  public List<MatchResultItem> MatchResults { get; set; }
        public Dictionary<IMemberDTO, decimal> MatchResultsDic { get; set; }
    }
    //public class MatchResultItem
    //{
    //    public Int32 MemberID { get; set; }
    //    public DateTime TestDate { get; set; }
    //    public Decimal MatchScore { get; set; }
    //    public Matchnet.Member.ServiceAdapters.Member MemberObj { get; set; }
    //    public Int32 Depth1RegionID { get; set; }
    //}

}
