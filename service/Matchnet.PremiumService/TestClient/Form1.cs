﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Matchnet.PremiumServices.ServiceAdapter;
using Matchnet.PremiumServices.ValueObjects;

namespace TestClient
{
    public partial class Form1 : Form
    {
        InstanceMemberCollection services = null;
        public Form1()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int serviceinstanceid = (int) dataGridView1.Rows[e.RowIndex].Cells[1].Value;
            InstanceMember m = services[serviceinstanceid];

            DataGridViewTextBoxColumn newCol = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn newCol1 = new DataGridViewTextBoxColumn();
            DataGridViewTextBoxColumn newCol2 = new DataGridViewTextBoxColumn();
            dataGridView2.Columns.Add(newCol);
            dataGridView2.Columns.Add(newCol1);
            dataGridView2.Columns.Add(newCol2);
            foreach (Matchnet.PremiumServices.ValueObjects.IAttribute a in m.Attributes)
            {
                DataGridViewRow row = new DataGridViewRow();
                DataGridViewTextBoxCell newCell = new DataGridViewTextBoxCell();
                newCell.Value = a.ID.ToString();

                DataGridViewTextBoxCell attrCell = new DataGridViewTextBoxCell();
                attrCell.Value = (Matchnet.PremiumServices.ValueObjects.ServiceConstants.Attributes)a.ID;

                DataGridViewTextBoxCell newCell1 = new DataGridViewTextBoxCell();
                newCell1.Value = m.Attributes[a.ID].ToString();

                row.Cells.Add(newCell);
                row.Cells.Add(attrCell);
                row.Cells.Add(newCell1);
                dataGridView2.Rows.Add(row);

            }

        }

        private void btnGetServices_Click(object sender, EventArgs e)
        {
            try
            {
                int brandid = Int32.Parse(txtBrandID.Text);
                int memberid = Int32.Parse(txtMemberID.Text);
                services = Matchnet.PremiumServices.ServiceAdapter.ServiceMemberSA.Instance.GetMemberServices(brandid,memberid);

                dataGridView1.DataSource = services;
                //  members = null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
