﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Proxies;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Configuration;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Member.ValueObjects;
using Matchnet.PremiumServices.ValueObjects;
using Matchnet.PremiumServiceSearch.ServiceAdapters;

namespace Matchnet.PremiumServices.ServiceAdapter
{
    public class ServiceMemberSA : SABase
    {
        private Cache _cache;
        private AttributeMetadataCollection _attributes;

        public static readonly ServiceMemberSA Instance = new ServiceMemberSA();

        private ServiceMemberSA()
        {
            _cache = Cache.Instance;
        }


        public InstanceMemberCollection GetMemberServices(int brandid, int memberid)
        {
            try
            {
                Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandid);
                InstanceMemberCollection coll = GetMemberServices(brand, memberid);

                return coll;
            }
            catch (Exception ex)
            { throw; }
        }

        public InstanceMemberCollection GetMemberServices(int brandid, Member.ServiceAdapters.Member member)
        {
            try
            {
                Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandid);
                InstanceMemberCollection coll = GetMemberServices(brand, member);

                return coll;
            }
            catch (Exception ex)
            { throw; }
        }


        public InstanceMember GetMemberService(int brandid, int memberid, int serviceid)
        {
            try
            {
                Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandid);
                InstanceMemberCollection coll = GetMemberServices(brand, memberid);
                PremiumServiceCollections services = ServiceSA.Instance.GetPremiumServiceCollection();
                if (coll == null)
                    return null;


                return coll.GetMemberServiceInstance(serviceid);
            }
            catch (Exception ex)
            { return null; }
        }

        public InstanceMember GetMemberService(int brandid, Member.ServiceAdapters.Member member, int serviceid)
        {
            try
            {
                Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandid);
                InstanceMemberCollection coll = GetMemberServices(brand, member);
                PremiumServiceCollections services = ServiceSA.Instance.GetPremiumServiceCollection();
                if (coll == null)
                    return null;


                return coll.GetMemberServiceInstance(serviceid);
            }
            catch (Exception ex)
            { return null; }
        }


        public InstanceMemberCollection GetMemberServices(Content.ValueObjects.BrandConfig.Brand brand, int memberid)
        {
            try
            {
                InstanceMemberCollection coll = (InstanceMemberCollection)_cache.Get(InstanceMemberCollection.GetCacheKey(brand.Site.SiteID, memberid));
                if (coll == null)
                {
                    coll = new InstanceMemberCollection(brand.Site.SiteID, memberid);
                    InstanceMember highlightmember = getHighlightedProfile(brand, memberid);
                    if (highlightmember != null)
                    {
                        coll.Add(highlightmember);

                    }

                    InstanceMember spotlight = getSpotlightedProfile(brand, memberid);
                    if (spotlight != null)
                    {
                        coll.Add(spotlight);

                    }

                    InstanceMember jmeter = getJMeter(brand, memberid);
                    if (jmeter != null)
                    {
                        coll.Add(jmeter);

                    }

                    if (coll.Count > 0)
                        _cache.Insert(coll);
                }

                return coll;
            }
            catch (Exception ex)
            { throw; }
        }

        public InstanceMemberCollection GetMemberServices(Content.ValueObjects.BrandConfig.Brand brand, Member.ServiceAdapters.Member member)
        {
            try
            {
                InstanceMemberCollection coll = (InstanceMemberCollection)_cache.Get(InstanceMemberCollection.GetCacheKey(brand.Site.SiteID, member.MemberID));
                if (coll == null)
                {
                    coll = new InstanceMemberCollection(brand.Site.SiteID, member.MemberID);
                    InstanceMember highlightmember = getHighlightedProfile(brand, member);
                    if (highlightmember != null)
                    {
                        coll.Add(highlightmember);

                    }

                    InstanceMember spotlight = getSpotlightedProfile(brand, member.MemberID);
                    if (spotlight != null)
                    {
                        coll.Add(spotlight);

                    }

                    InstanceMember jmeter = getJMeter(brand, member);
                    if (jmeter != null)
                    {
                        coll.Add(jmeter);

                    }

                    if (coll.Count > 0)
                        _cache.Insert(coll);
                }

                return coll;
            }
            catch (Exception ex)
            { throw; }
        }

        public void SaveMemberServices(Content.ValueObjects.BrandConfig.Brand brand, InstanceMemberCollection memberServices)
        {
            try
            {
                foreach (InstanceMember m in memberServices)
                {
                    PremiumService s = ServiceSA.Instance.GetPremiumService(m.ServiceInstanceID);
                    if (s != null)
                    {
                        if (s.ServiceID == (int)ValueObjects.ServiceConstants.ServiceIDs.HighligtedProfile)
                        {
                            saveHighlightedProfile(brand, memberServices.MemberID, m);
                        }
                        else if (s.ServiceID == (int)ValueObjects.ServiceConstants.ServiceIDs.SpotlightMember)
                        {
                            SpotlightMemberSA.Instance.SaveMember(brand.BrandID, memberServices.MemberID, m, false);
                        }
                        else if (s.ServiceID == (int)ValueObjects.ServiceConstants.ServiceIDs.JMeter)
                        {
                            saveJMeter(brand, memberServices.MemberID, m);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }


        }


        public void SaveMemberService(Content.ValueObjects.BrandConfig.Brand brand, int memberid, InstanceMember m)
        {
            try
            {
                if (m == null)
                    return;

                PremiumService s = ServiceSA.Instance.GetPremiumService(m.ServiceInstanceID);
                if (s != null)
                {
                    if (s.ServiceID == (int)ValueObjects.ServiceConstants.ServiceIDs.HighligtedProfile)
                    {
                        saveHighlightedProfile(brand, memberid, m);
                    }
                    else if (s.ServiceID == (int)ValueObjects.ServiceConstants.ServiceIDs.SpotlightMember)
                    {
                        SpotlightMemberSA.Instance.SaveMember(brand.BrandID, memberid, m, false);
                    }
                    else if (s.ServiceID == (int)ValueObjects.ServiceConstants.ServiceIDs.JMeter)
                    {
                        saveJMeter(brand, memberid, m);
                    }
                }


            }
            catch (Exception ex)
            {
                throw;
            }


        }

        private InstanceMemberCollection getMemberServices(int siteid, int memberid)
        {
            try
            {
                InstanceMemberCollection coll = new InstanceMemberCollection(siteid, memberid);

                return coll;

            }
            catch (Exception ex)
            {
                throw;
            }


        }

        #region SABase implementation
        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PREMIUMSERVICESVC_SA_CONNECTION_LIMIT"));
        }
        #endregion SABase implementation

        //hack to populate without framework
        private InstanceMember getHighlightedProfile(Content.ValueObjects.BrandConfig.Brand brand, int memberid)
        {
            InstanceMember member = null;
            try
            {
                Matchnet.Member.ServiceAdapters.Member m = Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, Member.ServiceAdapters.MemberLoadFlags.None);
                Spark.Common.AccessService.AccessPrivilege privilege = m.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.HighlightedProfile, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

                DateTime expDate = DateTime.MinValue;// m.GetAttributeDate(brand, "HighlightedExpirationDate");
                if (privilege != null)
                {
                    expDate = privilege.EndDatePST;
                }

                Boolean enableFlag = m.GetAttributeBool(brand, "HighlightedFlag");
                if (expDate < DateTime.Now)
                {
                    return member;
                }
                PremiumServiceCollections coll = ServiceSA.Instance.GetPremiumServiceCollection();
                List<ServiceInstance> instances = coll.GetServiceSiteInstances(brand.Site.SiteID, (int)ValueObjects.ServiceConstants.ServiceIDs.HighligtedProfile);
                if (instances != null && instances.Count > 0)
                {//severe hack;
                    member = new InstanceMember();
                    member.ServiceInstanceID = instances[0].ServiceInstanceID;
                    member.ServiceID = (int)ValueObjects.ServiceConstants.ServiceIDs.HighligtedProfile;
                    member.Attributes.Add((int)ValueObjects.ServiceConstants.Attributes.ExpirationDate, expDate);
                    member.Attributes.Add((int)ValueObjects.ServiceConstants.Attributes.EnableFlag, enableFlag);
                    member.ServiceName = coll.GetServiceName(member.ServiceInstanceID);
                }


                return member;

            }
            catch (Exception ex)
            { throw; }

        }


        private InstanceMember getSpotlightedProfile(Content.ValueObjects.BrandConfig.Brand brand, int memberid)
        {
            InstanceMember member = null;
            try
            {
                int instid = 0;
                PremiumServiceCollections coll = ServiceSA.Instance.GetPremiumServiceCollection();
                List<ServiceInstance> instances = coll.GetServiceSiteInstances(brand.Site.SiteID, (int)ValueObjects.ServiceConstants.ServiceIDs.SpotlightMember);
                if (instances != null && instances.Count > 0)
                {//severe hack;
                    instid = instances[0].ServiceInstanceID;
                }

                member = SpotlightMemberSA.Instance.GetMember(brand.BrandID, memberid, instid);

                if (member != null)
                {
                    DateTime expDate = member.Attributes.Get<DateTime>((int)ValueObjects.ServiceConstants.Attributes.ExpirationDate);

                    member.ServiceID = (int)ValueObjects.ServiceConstants.ServiceIDs.SpotlightMember;

                    if (expDate < DateTime.Now)
                    {
                        return null;
                    }
                }

                return member;

            }
            catch (Exception ex)
            { throw; }

        }

        private InstanceMember getHighlightedProfile(Content.ValueObjects.BrandConfig.Brand brand, Member.ServiceAdapters.Member m)
        {
            InstanceMember member = null;
            try
            {
                Spark.Common.AccessService.AccessPrivilege privilege = m.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.HighlightedProfile, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

                DateTime expDate = DateTime.MinValue;// m.GetAttributeDate(brand, "HighlightedExpirationDate");
                if (privilege != null)
                {
                    expDate = privilege.EndDatePST;
                }

                Boolean enableFlag = m.GetAttributeBool(brand, "HighlightedFlag");
                if (expDate < DateTime.Now)
                {
                    return member;
                }
                PremiumServiceCollections coll = ServiceSA.Instance.GetPremiumServiceCollection();
                List<ServiceInstance> instances = coll.GetServiceSiteInstances(brand.Site.SiteID, (int)ValueObjects.ServiceConstants.ServiceIDs.HighligtedProfile);
                if (instances != null && instances.Count > 0)
                {//severe hack;
                    member = new InstanceMember();
                    member.ServiceInstanceID = instances[0].ServiceInstanceID;
                    member.ServiceID = (int)ValueObjects.ServiceConstants.ServiceIDs.HighligtedProfile;
                    member.Attributes.Add((int)ValueObjects.ServiceConstants.Attributes.ExpirationDate, expDate);
                    member.Attributes.Add((int)ValueObjects.ServiceConstants.Attributes.EnableFlag, enableFlag);
                    member.ServiceName = coll.GetServiceName(member.ServiceInstanceID);
                }


                return member;

            }
            catch (Exception ex)
            { throw; }

        }


        private InstanceMember getJMeter(Content.ValueObjects.BrandConfig.Brand brand, Member.ServiceAdapters.Member m)
        {
            InstanceMember member = null;
            try
            {
                Spark.Common.AccessService.AccessPrivilege privilege = m.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.JMeter, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

                DateTime expDate = DateTime.MinValue;// m.GetAttributeDate(brand, "JMeterExpirationDate");
                if (privilege != null)
                {
                    expDate = privilege.EndDatePST;
                }
                Boolean enableFlag = m.GetAttributeBool(brand, "JMeterFlag");
                if (expDate < DateTime.Now)
                {
                    return member;
                }
                PremiumServiceCollections coll = ServiceSA.Instance.GetPremiumServiceCollection();
                List<ServiceInstance> instances = coll.GetServiceSiteInstances(brand.Site.SiteID, (int)ValueObjects.ServiceConstants.ServiceIDs.JMeter);
                if (instances != null && instances.Count > 0)
                {//severe hack;
                    member = new InstanceMember();
                    member.ServiceInstanceID = instances[0].ServiceInstanceID;
                    member.ServiceID = (int)ValueObjects.ServiceConstants.ServiceIDs.JMeter;
                    member.Attributes.Add((int)ValueObjects.ServiceConstants.Attributes.ExpirationDate, expDate);
                    member.Attributes.Add((int)ValueObjects.ServiceConstants.Attributes.EnableFlag, enableFlag);
                    member.ServiceName = coll.GetServiceName(member.ServiceInstanceID);
                }


                return member;

            }
            catch (Exception ex)
            { throw; }

        }

        private InstanceMember getJMeter(Content.ValueObjects.BrandConfig.Brand brand, int memberid)
        {
            InstanceMember member = null;
            try
            {
                Matchnet.Member.ServiceAdapters.Member m = Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, Member.ServiceAdapters.MemberLoadFlags.None);
                Spark.Common.AccessService.AccessPrivilege privilege = m.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.JMeter, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);

                DateTime expDate = DateTime.MinValue;// m.GetAttributeDate(brand, "JMeterExpirationDate");
                if (privilege != null)
                {
                    expDate = privilege.EndDatePST;
                }
                Boolean enableFlag = m.GetAttributeBool(brand, "JMeterFlag");
                if (expDate < DateTime.Now)
                {
                    return member;
                }
                PremiumServiceCollections coll = ServiceSA.Instance.GetPremiumServiceCollection();
                List<ServiceInstance> instances = coll.GetServiceSiteInstances(brand.Site.SiteID, (int)ValueObjects.ServiceConstants.ServiceIDs.JMeter);
                if (instances != null && instances.Count > 0)
                {//severe hack;
                    member = new InstanceMember();
                    member.ServiceInstanceID = instances[0].ServiceInstanceID;
                    member.ServiceID = (int)ValueObjects.ServiceConstants.ServiceIDs.JMeter;
                    member.Attributes.Add((int)ValueObjects.ServiceConstants.Attributes.ExpirationDate, expDate);
                    member.Attributes.Add((int)ValueObjects.ServiceConstants.Attributes.EnableFlag, enableFlag);
                    member.ServiceName = coll.GetServiceName(member.ServiceInstanceID);
                }


                return member;

            }
            catch (Exception ex)
            { throw; }

        }

        private void saveHighlightedProfile(Content.ValueObjects.BrandConfig.Brand brand, int memberid, InstanceMember instanceMember)
        {

            try
            {
                Matchnet.Member.ServiceAdapters.Member m = Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, Member.ServiceAdapters.MemberLoadFlags.None);

                PremiumServiceCollections coll = ServiceSA.Instance.GetPremiumServiceCollection();
                List<ServiceInstance> instances = coll.GetServiceSiteInstances(brand.Site.SiteID, (int)ValueObjects.ServiceConstants.ServiceIDs.HighligtedProfile);
                if (instances != null && instances.Count > 0)
                {
                    //no need to save expiratedate since it now comes from UPS Access
                    //m.SetAttributeDate(brand, "HighlightedExpirationDate", instanceMember.Attributes.Get<DateTime>((int)ValueObjects.ServiceConstants.Attributes.ExpirationDate));
                    
                    bool enabledFlag = instanceMember.Attributes.Get<bool>((int)ValueObjects.ServiceConstants.Attributes.EnableFlag);
                    m.SetAttributeInt(brand, "HighlightedFlag", enabledFlag ? 1 : 0);
                    Member.ServiceAdapters.MemberSA.Instance.SaveMember(m);
                }



            }
            catch (Exception ex)
            { throw; }

        }

        private void saveJMeter(Content.ValueObjects.BrandConfig.Brand brand, int memberid, InstanceMember instanceMember)
        {

            try
            {
                Matchnet.Member.ServiceAdapters.Member m = Member.ServiceAdapters.MemberSA.Instance.GetMember(memberid, Member.ServiceAdapters.MemberLoadFlags.None);

                PremiumServiceCollections coll = ServiceSA.Instance.GetPremiumServiceCollection();
                List<ServiceInstance> instances = coll.GetServiceSiteInstances(brand.Site.SiteID, (int)ValueObjects.ServiceConstants.ServiceIDs.JMeter);
                if (instances != null && instances.Count > 0)
                {
                    //no need to save expirate date since this comes from Access
                    //m.SetAttributeDate(brand, "JMeterExpirationDate", instanceMember.Attributes.Get<DateTime>((int)ValueObjects.ServiceConstants.Attributes.ExpirationDate));
                    
                    bool enabledFlag = instanceMember.Attributes.Get<bool>((int)ValueObjects.ServiceConstants.Attributes.EnableFlag);
                    m.SetAttributeInt(brand, "JMeterFlag", enabledFlag ? 1 : 0);
                    Member.ServiceAdapters.MemberSA.Instance.SaveMember(m);
                }

            }
            catch (Exception ex)
            { throw; }

        }
    }
}
