﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Matchnet;
using Matchnet.Caching;
using Matchnet.Configuration;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Member.ValueObjects;
using Matchnet.PremiumServices.ValueObjects;


namespace Matchnet.PremiumServices.ServiceAdapter
{
    public class PremiumServiceMemberBase
    {

        InstanceMember member;
        Matchnet.Content.ValueObjects.BrandConfig.Brand brand;

       

        public Matchnet.Content.ValueObjects.BrandConfig.Brand Brand
        {
            get { return brand; }
            set { brand = value; }
        }

     
        public InstanceMember Member
        {
            get { return member; }
            set { member = value; }
        }

        public T GetAttribute<T>(int id)
        {
            return member.Attributes.Get<T>(id);
         }

        public T GetAttribute<T>(int id, bool firstInRange)
        {
            return member.Attributes.Get<T>(id, firstInRange);
        }

        public void SetAttribute<T>(int id, T value)
        {
            member.Attributes.Add<T>(id, value);
        }

        

        public void SetAttribute<T>(int id, T value1, T value2)
        {
            member.Attributes.Add<T>(id, value1,value2);
        }
      

        public bool EnableFlag
        {
            get
            {
                return member.Attributes.Get<bool>((int)ValueObjects.ServiceConstants.Attributes.EnableFlag);
            }
            set
            {
                member.Attributes.Add((int)ValueObjects.ServiceConstants.Attributes.EnableFlag, value);
            }

        }

        public DateTime ExpirationDate
        {
            get
            {
                return member.Attributes.Get<DateTime>((int)ValueObjects.ServiceConstants.Attributes.ExpirationDate);
            }
            set
            {
                member.Attributes.Add((int)ValueObjects.ServiceConstants.Attributes.ExpirationDate, value);
            }

        }


    }
   
}
