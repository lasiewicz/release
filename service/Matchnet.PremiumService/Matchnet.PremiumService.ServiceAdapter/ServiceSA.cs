﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Proxies;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Configuration;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Member.ValueObjects;
using Matchnet.PremiumServices.ValueObjects;

namespace Matchnet.PremiumServices.ServiceAdapter
{
    public class ServiceSA:SABase
    {
    
        private Cache _cache;
        

        public static readonly ServiceSA Instance = new ServiceSA();


        private ServiceSA()
        {
            _cache = Cache.Instance;
            //hack!!!
            PremiumServiceCollections services = populate();
            _cache.Insert(services);
        }

        public PremiumService GetPremiumService(int instanceid)
        {
            PremiumServiceCollections services=(PremiumServiceCollections)_cache.Get(PremiumServiceCollections.GetCacheKeyString());
            if(services == null)
            {
                services=populate();
                _cache.Insert(services);
            }


            return services.GetService(instanceid);
        }

        public PremiumServiceCollections GetPremiumServiceCollection()
        {
            PremiumServiceCollections services = (PremiumServiceCollections)_cache.Get(PremiumServiceCollections.GetCacheKeyString());
            if (services == null)
            {
                services = populate();
                _cache.Insert(services);
            }


            return services;
        }

        #region hack
        //hack since we don't have framework

       
        private PremiumServiceCollections populate()
        {
            PremiumServiceCollections services = null;
            try
            {
                Matchnet.Content.ValueObjects.BrandConfig.Brands brands = BrandConfigSA.Instance.GetBrands();

                Matchnet.Content.ValueObjects.BrandConfig.Site[] sites = brands.GetSites();

                services = new PremiumServiceCollections();


                services.ServiceCollection.Add(new PremiumService((int)ValueObjects.ServiceConstants.ServiceIDs.HighligtedProfile, ValueObjects.ServiceConstants.PS_HIGHLIGHTED_PROFILE, ""));
                services.ServiceCollection.Add(new PremiumService((int)ValueObjects.ServiceConstants.ServiceIDs.SpotlightMember, ValueObjects.ServiceConstants.PS_SPOTLIGHT_PROFILE, ""));
                services.ServiceCollection.Add(new PremiumService((int)ValueObjects.ServiceConstants.ServiceIDs.JMeter, ValueObjects.ServiceConstants.PS_JMETER_PROFILE, ""));

                foreach(PremiumService s in  services.ServiceCollection)
                {

                   // int svcid = 100 + s.ServiceID;

                    for (int i = 0; i < sites.Length; i++)
                    {
                       int servicesiteid = ServiceSite.GetServiceSiteID(sites[i].SiteID, s.ServiceID);

                       ServiceSite serviceSite = new ValueObjects.ServiceSite(servicesiteid, s.ServiceID, sites[i].SiteID, true);
                       services.ServiceSiteCollection.Add(serviceSite);
                        
                       int instanceid = ServiceInstance.GetServiceSiteInstanceID(servicesiteid, 1);

                       ServiceInstance instance = new ValueObjects.ServiceInstance(servicesiteid, instanceid, "Only instance per site", true);
                       if (s.ServiceID == (int)ValueObjects.ServiceConstants.ServiceIDs.SpotlightMember)
                       { populateSpotlightServiceAttributes(sites[i].Community.CommunityID, serviceSite); }
                      
                        services.InstanceCollection.Add(instance);
                    }

                }
                return services;
            }
            catch (Exception ex)
            {
                throw;

            }


        }

        #endregion

        #region hack

        private void populateSpotlightServiceAttributes(int communityid, ServiceSite servicesite)
        {
            try
            {
                string velocitycap = Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SPOTLIGHT_VELOCITY_LIST_CAP", communityid, servicesite.SiteID);
                string velocitytime = Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SPOTLIGHT_VELOCITY_TIME", communityid, servicesite.SiteID);
                string velocityquota = Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SPOTLIGHT_VELOCITY_QUOTA", communityid, servicesite.SiteID);
                string promotionprofileratio = Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SPOTLIGHT_PROMOTION_RATIO", communityid, servicesite.SiteID);

                servicesite.Attributes.Add<int>((int)PremiumServices.ValueObjects.ServiceConstants.ServiceAttributes.VelocityListCap, Conversion.CInt(velocitycap));
                servicesite.Attributes.Add<int>((int)PremiumServices.ValueObjects.ServiceConstants.ServiceAttributes.VelocityTime, Conversion.CInt(velocitytime));
                servicesite.Attributes.Add<int>((int)PremiumServices.ValueObjects.ServiceConstants.ServiceAttributes.VelocityQuota, Conversion.CInt(velocityquota));
                servicesite.Attributes.Add<int>((int)PremiumServices.ValueObjects.ServiceConstants.ServiceAttributes.PromotionMemberDisplayRatio, Conversion.CInt(promotionprofileratio));


            }
            catch (Exception ex)
            { 
            
            
            
            }


        }


        #endregion
        #region SABase implementation
        protected override void GetConnectionLimit()
        {
            //base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PREMIUMSERVICESVC_SA_CONNECTION_LIMIT"));
            base.MaxConnections = 10;
        }
        #endregion SABase implementation

    }
}
