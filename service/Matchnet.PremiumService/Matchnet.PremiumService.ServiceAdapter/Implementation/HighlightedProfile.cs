﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.PremiumServices.ValueObjects;


using Matchnet;
using Matchnet.Caching;
using Matchnet.Configuration;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Member.ValueObjects;


namespace Matchnet.PremiumServices.ServiceAdapter
{
    public class HighlightedProfile:PremiumServiceMemberBase, IServiceMemberInterface
    {


        public HighlightedProfile(InstanceMember member)
        {
            PremiumService service = ServiceSA.Instance.GetPremiumService(member.ServiceInstanceID);
            string serviceName = service.Name;
            if (serviceName != ValueObjects.ServiceConstants.PS_HIGHLIGHTED_PROFILE)
            { throw (new Exception()); }
            base.Member = member;

        }

        #region IServiceMemberInterface implementation
        public void Save()
        { }
        public void Get()
        {

            Member = new InstanceMember();
            Member.Attributes = new Matchnet.PremiumServices.ValueObjects.AttributeCollection();


        
        
        
        
        }
        #endregion

    }
}
