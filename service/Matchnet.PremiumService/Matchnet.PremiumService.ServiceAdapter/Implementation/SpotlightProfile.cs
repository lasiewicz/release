﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.PremiumServices.ValueObjects;


using Matchnet;
using Matchnet.Caching;
using Matchnet.Configuration;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Member.ValueObjects;

namespace Matchnet.PremiumServices.ServiceAdapter.Implementation
{
    public class SpotlightProfile:PremiumServiceMemberBase, IServiceMemberInterface
    {


        public SpotlightProfile(InstanceMember member)
        {
            PremiumService service = ServiceSA.Instance.GetPremiumService(member.ServiceInstanceID);
            string serviceName = service.Name;
            if (serviceName != ValueObjects.ServiceConstants.PS_SPOTLIGHT_PROFILE)
            { throw (new Exception()); }
            base.Member = member;

        }

        public Int32 MemberID
        {
            get
            { return GetAttribute<Int32>((int)ValueObjects.ServiceConstants.Attributes.MemberID); }
            set
            { SetAttribute<Int32>((int)ValueObjects.ServiceConstants.Attributes.MemberID, value); }

        }

        public Int32 GenderMask
        {
            get
            { return GetAttribute<Int32>((int)ValueObjects.ServiceConstants.Attributes.GenderMask); }
            set
            { SetAttribute<Int32>((int)ValueObjects.ServiceConstants.Attributes.GenderMask, value); }

        }

        public Int32 Distance
        {
            get
            { return GetAttribute<Int32>((int)ValueObjects.ServiceConstants.Attributes.Distance); }
            set
            { SetAttribute<Int32>((int)ValueObjects.ServiceConstants.Attributes.Distance, value); }

        }

        public Int32 RegionID
        {
            get
            { return GetAttribute<Int32>((int)ValueObjects.ServiceConstants.Attributes.RegionID); }
            set
            { SetAttribute<Int32>((int)ValueObjects.ServiceConstants.Attributes.RegionID, value); }

        }

        public Int32 AgeMin
        {
            get
            { return GetAttribute<Int32>((int)ValueObjects.ServiceConstants.Attributes.AgeRange, true); }
         }


        public Int32 AgeMax
        {
            get
            { return GetAttribute<Int32>((int)ValueObjects.ServiceConstants.Attributes.AgeRange, true); }
         }

        public void SetAge(int ageMin, int ageMax)
        {
            SetAttribute<Int32>((int)ValueObjects.ServiceConstants.Attributes.AgeRange, ageMin, ageMax);
        }


        #region IServiceMemberInterface implementation
        public void Save()
        {}
        public void Get()
        {

            try
            {






            }
            catch (Exception ex)
            { }
        
        
        
        
        
        }
        #endregion

        
       

    }
    
}
