﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Matchnet.PremiumServices.ValueObjects;

namespace Matchnet.PremiumServices.ValueObjects
{
 [Serializable]
   public class AttributeMetadataCollection : KeyedCollection<Int32, IAttributeDefinition>
    {


        protected override int GetKeyForItem(IAttributeDefinition item)
        {
            return item.ID;
        }

        #region get attrs by
        //public IAttributeDefinition GetAttributeByName(string itemName)
        //{

        //    List<IAttributeDefinition> list = Items.Where<IAttributeDefinition>(delegate(IAttributeDefinition a) { return a.Name == itemName; }).ToList<IAttributeDefinition>();
        //    return list[0];
        //}


        public List<IAttributeDefinition> GetAttributesByDataType(AttributeDataType d)
        {

            List<IAttributeDefinition> list = Items.Where<IAttributeDefinition>(delegate(IAttributeDefinition a) { return a.DataType == d; }).ToList<IAttributeDefinition>();
            return list;
        }

        public List<IAttributeDefinition> GetAttributesByScope(AttrScope scope)
        {

            List<IAttributeDefinition> list = Items.Where<IAttributeDefinition>(delegate(IAttributeDefinition a) { return a.Scope == scope; }).ToList<IAttributeDefinition>();
            return list;
        }

        #endregion



    }

    [Serializable]
    public class AttributeCollection : KeyedCollection<Int32, IAttribute>
    {


        protected override int GetKeyForItem(IAttribute item)
        {
            return item.ID;
        }

       
         public T Get<T>(int id)
        {
            T val=default(T);
            if (!this.Contains(id))
                return val;
            Attribute<AttrValue<T>> attr = (Attribute<AttrValue<T>>)this[id];
            if (attr != null && attr.Value != null)
            {
                val = attr.Value.Value;
            }

            return val;
        }

         public void Get<T>(int id, out T val1, out T val2)
         {
             Attribute<AttrValueRange<T>> attr = (Attribute<AttrValueRange<T>>)this[id];
             val1 = attr.Value.Value1;
             val2 = attr.Value.Value2;
         }

        public T Get<T>(int id, bool first)
         {
             T val = default(T);
          
             if (!this.Contains(id))
                 return val;
             Attribute<AttrValueRange<T>> attr = (Attribute<AttrValueRange<T>>)this[id];
             if (attr != null && attr.Value != null)
             {
                 if (first)
                     val = attr.Value.Value1;
                 else
                     val = attr.Value.Value2;
             }
             return val;

         }

        public void Add(Int32 id,  Int32 value)
        {
            Add<int>(id,value);

        }

        public void Add(Int32 id,  Int32 value1, Int32 value2)
        {
            Add<int>(id, value1, value2);
        }

       

        public void Add(Int32 id,  DateTime value)
        {
            Add<DateTime>(id, value);
        }

        public void Add(Int32 id,  DateTime value1, DateTime value2)
        {
            Add<DateTime>(id, value1, value2);
        }

        public void Add(Int32 id,  bool value)
        {
            Add<bool>(id, value);
        }

        public void Add(Int32 id,  string value)
        {
            Add<string>(id, value);
        }

        public void Add<T>(Int32 id,  T value)
        {
            if (!this.Contains(id))
            {
                AttrValue<T> val = new AttrValue<T>(value);
                Attribute<AttrValue<T>> attr = new Attribute<AttrValue<T>>(id, val);
                this.Items.Add(attr);
            }
            else
            {
                ((Attribute<AttrValue<T>>)this[id]).Value.Value = value;

            }
        }
        public void Add<T>(Int32 id,  T value1, T value2)
        {
            if (!this.Contains(id))
            {
                AttrValueRange<T> val = new AttrValueRange<T>(value1, value2);
                Attribute<AttrValueRange<T>> attr = new Attribute<AttrValueRange<T>>(id, val);
                this.Items.Add(attr);
            }
            else
            {
               
                if (!value1.Equals(default(T)))
                    ((Attribute<AttrValueRange<T>>)this[id]).Value.Value1 = value1;
                if (!value2.Equals(default(T)))
                 ((Attribute<AttrValueRange<T>>)this[id]).Value.Value2 = value2;
            }
        }

        public override string ToString()
        {
            string attrs = "";
            try
            {
                foreach (IAttribute a in this.Items)
                {
                    attrs += String.Format("{0}\r\n", a.ToString());
                }
                return attrs;
            }
            catch (Exception ex)
            { return ""; }

        }

    }



//    public class AttributeByNameCollection : KeyedCollection<string, IAttribute>
//    {


//        protected override string GetKeyForItem(IAttribute item)
//        {
//            return item.Name;
//        }

//        public T Get<T>(string name)
//        {
//            Attribute<AttrValue<T>> attr = (Attribute<AttrValue<T>>)this[name];
//            return attr.Value.Value;
//        }

//        #region Add attrs
//        public void Add(Int32 id, string name, Int32 value)
//        {
//            add<int>(id, name, value);

//        }

//        public void Add(Int32 id, string name, Int32 value1, Int32 value2)
//        {
//            add<int>(id, name, value1, value2);
//        }

//        public void Add(Int32 id, string name, DateTime value)
//        {
//            add<DateTime>(id, name, value);
//        }

//        public void Add(Int32 id, string name, DateTime value1, DateTime value2)
//        {
//            add<DateTime>(id, name, value1, value2);
//        }

//        public void Add(Int32 id, string name, bool value)
//        {
//            add<bool>(id, name, value);
//        }

//        public void Add(Int32 id, string name, string value)
//        {
//            add<string>(id, name, value);
//        }

//        private void add<T>(Int32 id, string name, T value)
//        {
//            if (this[name] == null)
//            {
//                AttrValue<T> val = new AttrValue<T>(value);
//                Attribute<AttrValue<T>> attr = new Attribute<AttrValue<T>>(id, name, val);
//                this.Items.Add(attr);
//            }
//            else
//            {
//               ((Attribute<AttrValue<T>>)this[name]).Value.Value=value;

//            }
//        }
//        private void add<T>(Int32 id, string name, T value1, T value2)
//        {
//            if (this[name] == null)
//            {
//                AttrValueRange<T> val = new AttrValueRange<T>(value1, value2);
//                Attribute<AttrValueRange<T>> attr = new Attribute<AttrValueRange<T>>(id, name, val);
//                this.Items.Add(attr);
//            }
//            else
//            {
//                ((Attribute<AttrValueRange<T>>)this[name]).Value.Value1 = value1;
//                ((Attribute<AttrValueRange<T>>)this[name]).Value.Value2 = value2;
//            }
//        }

        
       
//# endregion

//    }




   
}
