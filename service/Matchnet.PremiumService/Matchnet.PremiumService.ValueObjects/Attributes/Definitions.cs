﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.PremiumServices.ValueObjects;
namespace Matchnet.PremiumServices.ValueObjects
{
    public enum AttributeDataType
    {
        typeInt = 0,
        typeDouble,
        typeBool,
        typeDate,
        typeText,
        typeIntRange,
        typeDoubleRange,
        typeDateRange
    }
    public enum AttrScope
    {
        typeService = 0,
        typeMember = 1,
        typeBoth = 2

    }
    [Serializable]
    public class AttrValue<T>
    {
        T val;

        public AttrValue(T v)
        {
            val = v;

        }
       public  T Value { get { return val; } set { val = value; } }

       public override String ToString() { return val.ToString(); }

    }
    [Serializable]
    public class AttrValueRange<T>
    {
        T value1;
        T value2;
        public AttrValueRange(T v1, T v2)
        {
            value1 = v1;
            value2 = v2;
        }
        public T Value1 { get { return value1; } set { value1 = value; } }
        public T Value2 { get { return value2; } set { value2 = value; } }
        public override String ToString() {return value1.ToString() + "-" + value2.ToString(); } 

    }


}
[Serializable]
public class AttributeDefinition<T> : IAttributeDefinition
{
    Int32 id;
    string name;
    AttributeDataType dataType;
    AttrScope attributeScope;


    public AttributeDefinition() { }
    public AttributeDefinition(Int32 attrid, string attrname, AttributeDataType datatype, AttrScope scope)
    {
        id = attrid;
        name = attrname;
        dataType = datatype;
        attributeScope = scope;

    }
    public Int32 ID { get { return id; } set { id = value; } }

    public String Name { get { return name; } set { name = value; } }

    public AttributeDataType DataType { get { return dataType; } set { dataType = value; } }

    public AttrScope Scope { get { return attributeScope; } set { attributeScope = value; } }

   
}
[Serializable]
public class Attribute<T>:IAttribute
{
    Int32 id;
   // string name;
    T attrValue=default(T);
    //System.Type type;
    public Attribute() { }
    public Attribute(Int32 attrid, T attrvalue)
    {
        id = attrid;
       // name = attrname;
        attrValue = attrvalue;
        

    }

    public override string ToString()
    {
        return String.Format("ID={0};Value={1}", id, attrValue.ToString());
    }

    public Int32 ID { get { return id; } set { id = value; } }
  //  public string Name { get { return name; } set { name = value; } }

    public T Value { get { return attrValue; } set { attrValue = value; } }

    public string ValueString
    {
        get {

            return attrValue.ToString();
        
        }
    }
}
