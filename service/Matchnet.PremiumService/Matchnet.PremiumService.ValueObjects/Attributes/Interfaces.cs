﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.PremiumServices.ValueObjects
{
    public interface IAttribute
    {
        Int32 ID { get; set; }
        String ToString();
     
    }

    public interface IAttributeDefinition
    {
        Int32 ID { get; set; }
      
       // String Name { get; set; }
        AttributeDataType DataType { get; set; }
        AttrScope Scope { get; set; }
        //Int32 Category { get; set; }

    }
}
