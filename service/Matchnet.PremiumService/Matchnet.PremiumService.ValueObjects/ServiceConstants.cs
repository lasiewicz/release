﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.PremiumServices.ValueObjects
{
    public class ServiceConstants
    {

        public const string PS_HIGHLIGHTED_PROFILE = "HIGHLIGHTED_PROFILE";
        public const string PS_SPOTLIGHT_PROFILE = "SPOTLIGHT_PROFILE";
        public const string PS_JMETER_PROFILE = "JMETER_PROFILE";

        public enum ServiceIDs
        {
            HighligtedProfile=101,
            SpotlightMember,
            JMeter
        }
        public enum Attributes
        {
            EnableFlag=0,
            ExpirationDate,
            MemberID,
            PromotionalFlag,
            AgeRange,
            GenderMask,
            Distance,
            RegionID,
            Longitude,
            Latitude,
            BoxXMin,
            BoxXMax,
            BoxYMin,
            BoxYMax,
            Depth1RegionID,
            Depth2RegionID,
            Depth3RegionID,
            Depth4RegionID,
            StartDate,
            Description

        }

        public enum ServiceAttributes
        {
            VelocityTime=0,
            VelocityQuota,
            PromotionMemberDisplayRatio,
            VelocityListCap 



        }
    }
}
