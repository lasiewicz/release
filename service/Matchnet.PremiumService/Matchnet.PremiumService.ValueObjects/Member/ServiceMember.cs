﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Matchnet;
using Matchnet.Caching;
using Matchnet.PremiumServices.ValueObjects;

namespace Matchnet.PremiumServices.ValueObjects
{
    [Serializable]
    public class InstanceMember :IValueObject
    {
        int serviceInstanceMemberID=default(int);
        int serviceInstanceID = default(int);
        int serviceID = default(int);
        string serviceName="";
        AttributeCollection attributes=new AttributeCollection();

        //public Int32 MemberID
        //{ get { return memberID; } set { memberID = value; } }

        public Int32 ServiceInstanceMemberID
        { get { return serviceInstanceMemberID; } set { serviceInstanceMemberID = value; } }

        public Int32 ServiceInstanceID
        { get { return serviceInstanceID; } set { serviceInstanceID = value; } }

        public Int32 ServiceID
        { get { return serviceID; } set { serviceID = value; } }

        public String ServiceName
        { get { return serviceName; } set { serviceName = value; } }

        public AttributeCollection Attributes
        { get { return attributes; } set { attributes = value; } }


        public T Get<T>(int id)
        {
            if (!attributes.Contains(id))
                return default(T);
            Attribute<AttrValue<T>> attr = (Attribute<AttrValue<T>>)attributes[id];
            return attr.Value.Value;
        }

        public T Get<T>(ServiceConstants.Attributes eid)
        {
            int id =(int)eid;
            if (!attributes.Contains(id))
                return default(T);
            Attribute<AttrValue<T>> attr = (Attribute<AttrValue<T>>)attributes[id];
            return attr.Value.Value;
        }



        public void Get<T>(int id, out T val1, out T val2)
        {
           

            Attribute<AttrValueRange<T>> attr = (Attribute<AttrValueRange<T>>)attributes[id];
            val1 = attr.Value.Value1;
            val2 = attr.Value.Value2;
        }

        public T Get<T>(ServiceConstants.Attributes eid, bool first)
        {
            int id = (int)eid;
            T val = default(T);
            if (!attributes.Contains(id))
                return val;
            Attribute<AttrValueRange<T>> attr = (Attribute<AttrValueRange<T>>)attributes[id];
            if (first)
                val = attr.Value.Value1;
            else
                val = attr.Value.Value2;

            return val;

        }

        public T Get<T>(int id, bool first)
        {
            T val = default(T);
            if (!attributes.Contains(id))
                return val;
            Attribute<AttrValueRange<T>> attr = (Attribute<AttrValueRange<T>>)attributes[id];
            if (first)
                val = attr.Value.Value1;
            else
                val = attr.Value.Value2;

            return val;

        }
        public override string ToString()
        {
            string attrs = "";
            StringBuilder s = new StringBuilder();
            try
            {
                s.Append("ServiceInstanceID = " + serviceInstanceID.ToString());
                s.AppendLine();
                s.Append( "ServiceInstanceMemberID = " + serviceInstanceMemberID.ToString());
                s.AppendLine();
                if (!String.IsNullOrEmpty(serviceName))
                {
                    s.Append("ServiceName = " + serviceName.ToString());
                    s.AppendLine();
                }
                s.Append("Attributes");
                s.AppendLine();
                attrs = attributes.ToString();
                s.Append(attrs);

                return s.ToString();
               
            }
            catch (Exception ex)
            { return s.ToString(); }

        }

    }

    //public class ServiceMember : IValueObject, ICacheable
    //{
    //    int memberID;
    //    int siteID;
     
    //    InstanceMemberCollection memberservices;

    //    public Int32 MemberID
    //    { get { return memberID; } set { memberID = value; } }

    //    public Int32 SiteID
    //    { get { return siteID; } set { siteID = value; } }


    //    public InstanceMemberCollection MemberServices
    //    { get { return memberservices; } set { memberservices = value; } }

    //    #region ICacheable Members

    //    private int _cacheTTLSeconds = 300;
    //    private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
    //    private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
    //    const string CACHEKEY = "PremiumServiceMember{0}_{1}";

    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public int CacheTTLSeconds
    //    {
    //        get { return _cacheTTLSeconds; }
    //        set { _cacheTTLSeconds = value; }

    //    }
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public Matchnet.CacheItemMode CacheMode
    //    {
    //        get { return _cacheItemMode; }
    //        set { _cacheItemMode = value; }
    //    }

    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public Matchnet.CacheItemPriorityLevel CachePriority
    //    {
    //        get { return _cachePriority; }
    //        set { _cachePriority = value; }
    //    }


    //    public string GetCacheKey()
    //    {
    //        return String.Format(CACHEKEY, siteID, memberID); ;
    //    }

    //    public static string GetCacheKeyString()
    //    {
    //        return CACHEKEY;
    //    }


    //    #endregion

    //}

    public class InstanceMemberCollection : KeyedCollection<int, InstanceMember>, IValueObject, ICacheable
    { 
        private int memberID;
        private int siteID;

        //caching
        private int _cacheTTLSeconds = 60;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEYFORMAT = "InstanceMemberCollection_{0}_{1}";
        
        
        public InstanceMemberCollection(int siteid, int memberid)
        {
            memberID = memberid;
            siteID = siteid;

        }


        public bool HasService(int serviceid)
        {
            try
            {
                List<InstanceMember> list = null;
                IEnumerable<InstanceMember> coll = Items.Where<InstanceMember>(delegate(InstanceMember i) { return i.ServiceID == serviceid; });

                if (coll != null)
                    list = coll.ToList<InstanceMember>();

                return (list[0] !=null);

            }
            catch (Exception ex)
            { return false; }

        }


        public InstanceMember GetMemberServiceInstance(int serviceid)
        {
            try
            {
                List<InstanceMember> list = null;
                IEnumerable<InstanceMember> coll = Items.Where<InstanceMember>(delegate(InstanceMember i) { return i.ServiceID == serviceid; });

                if (coll != null)
                    list = coll.ToList<InstanceMember>();

                return (list[0] );

            }
            catch (Exception ex)
            { return null; }

        }

        public int  MemberID
        {
            get { return memberID; }
            set { memberID = value; }

        }

        public int SiteID
        {
            get { return siteID; }
            set { siteID = value; }

        }
        protected override int GetKeyForItem(InstanceMember item)
        {
            return item.ServiceInstanceID;
        }

        #region ICacheable Members
        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }


        public string GetCacheKey()
        {
            return getCacheKey(siteID,memberID);
        }


        public static string GetCacheKey(int siteid,int memberid)
        {
            return getCacheKey(siteid,memberid);
        }
        private static string getCacheKey(int siteid, int memberid)
        {
            return String.Format(CACHEKEYFORMAT, siteid, memberid);
        }
        
        #endregion
    }


}
