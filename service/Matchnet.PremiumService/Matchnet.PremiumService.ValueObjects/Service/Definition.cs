﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.PremiumServices.ValueObjects;

namespace Matchnet.PremiumServices.ValueObjects
{
    

    public class PremiumService
    {
        int serviceID;
        string name;
        string desc;
       
        public PremiumService() { }
        public PremiumService(Int32 serviceid, string name1, string desc1)
        {
            serviceID=serviceid;
            name = name1;
            desc = desc1;
            
        }


        public Int32 ServiceID { get { return serviceID; } set { serviceID = value; } }
        public String Name { get { return name; } set { name = value; } }
        public String Description { get { return desc; } set { desc = value; } }
        //public bool ActiveFlag { get { return activeFlag; } set { activeFlag = value; } }

        //public PremiumServiceInstanceCollection Instances { get { return instances; } set { instances = value; } }

       

    }
    public class ServiceSite
    {
        int serviceSiteID;
        int serviceID;
        int siteID;
        string desc;
        bool activeFlag;
        AttributeCollection attributes;

        public ServiceSite() { }
        public ServiceSite(Int32 servicesiteid,Int32 serviceid,Int32 siteid,  bool active)
        {
            serviceID = serviceid;
            serviceSiteID = servicesiteid;
            siteID = siteid;
            attributes = new AttributeCollection();
          
           
        }

        public AttributeCollection Attributes { get { return attributes; } set { attributes = value; } }

        public ServiceSite( Int32 serviceid, Int32 siteid, bool active)
        {
            serviceID = serviceid;
            serviceSiteID = GetServiceSiteID(siteid,serviceid);
            siteID = siteid;


        }

        public Int32 ServiceSiteID { get { return serviceSiteID; } set { serviceSiteID = value; } }
        public Int32 ServiceID { get { return serviceID; } set { serviceID = value; } }
        public Int32 SiteID { get { return siteID; } set { siteID = value; } }
         public String Description { get { return desc; } set { desc = value; } }
        public bool ActiveFlag { get { return activeFlag; } set { activeFlag = value; } }


        #region hack
        //hack since we don't have framework

        public static int GetServiceSiteID(int siteid, int id)
        {
            int servicesiteid = siteid < 1000 ? siteid + 1000 : siteid;
            string srvsiteid = servicesiteid.ToString() + id.ToString();
            //hack because JMingle siteid=9721 on dev and it caused overflow
            if (servicesiteid >= 2000)
            {
                srvsiteid = servicesiteid.ToString().Substring(0,3) + id.ToString();
            }
            return Int32.Parse(srvsiteid);
        }
        #endregion

        public T GetAttribute<T>(int id)
        {
            T value = default(T);
            try
            {
                if(attributes != null)
                    value = attributes.Get<T>(id);


                return value;
            }
            catch (Exception ex)
            {
                return value;
            }

        }

    }


    public class ServiceInstance
    {
        Int32 serviceInstanceID;
        Int32 serviceSiteID;
        bool activeFlag;
        string desc;
        AttributeCollection attributes;

        public ServiceInstance() { }
        public ServiceInstance( Int32 serviceSiteid,Int32 instanceid, string desc1, bool active)
        {
            serviceSiteID = serviceSiteid;
            serviceInstanceID = instanceid;
            desc = desc1;
            activeFlag = active;
        }
        public ServiceInstance(Int32 siteid, Int32 serviceid, Int32 instanceindex, string desc1, bool active)
        {
            serviceSiteID = ServiceSite.GetServiceSiteID(siteid, serviceid);
            serviceInstanceID = GetServiceSiteInstanceID(serviceSiteID, instanceindex);
            desc = desc1;
            activeFlag = active;
        }

        public Int32 ServiceSiteID { get { return serviceSiteID; } set { serviceSiteID = value; } }
        public Int32 ServiceInstanceID { get { return serviceInstanceID; } set { serviceInstanceID = value; } }
        public bool ActiveFlag { get { return activeFlag; } set { activeFlag = value; } }
        public String Description { get { return desc; } set { desc = value; } }
        public AttributeCollection Attributes { get { return attributes; } set { attributes = value; } }

        public static int GetServiceSiteInstanceID(int servicesiteid,  int instanceid)
        {
         
            int id = instanceid + 100;
            string instid = servicesiteid.ToString() + id.ToString();
            return (Int32.Parse(instid));

        }
    }
}
