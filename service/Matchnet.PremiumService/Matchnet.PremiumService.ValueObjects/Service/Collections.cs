﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Matchnet.PremiumServices.ValueObjects
{[Serializable]
    public class ServiceCollection : KeyedCollection<Int32, PremiumService>
    {
        protected override int GetKeyForItem(PremiumService item)
        {
            return item.ServiceID;
        }

        public PremiumService GetServiceByName(string itemName)
        {
            PremiumService service = null;
            IEnumerable<PremiumService> coll = Items.Where<PremiumService>(delegate(PremiumService s) { return s.Name == itemName; });

            if (coll != null)
                service = coll.ToList<PremiumService>()[0];

            return service;
            
        }



    }

    [Serializable]
    public class ServiceInstanceCollection : KeyedCollection<Int32, ServiceInstance>
    {
        protected override int GetKeyForItem(ServiceInstance item)
        {
            return item.ServiceInstanceID;
        }
     
        
      
    }
[Serializable]
    public class ServiceSiteCollection : KeyedCollection<Int32, ServiceSite>
    {
        protected override int GetKeyForItem(ServiceSite item)
        {
            return item.ServiceSiteID;
        }

        public List<ServiceSite> GetSiteServices(int siteID)
        {
            List<ServiceSite> list = null;
            IEnumerable<ServiceSite> coll = Items.Where<ServiceSite>(delegate(ServiceSite s) { return s.SiteID == siteID; });

            if (coll != null)
                list = coll.ToList<ServiceSite>();

            return list;

        }

        public ServiceSite GetSiteService(int siteID, int serviceid)
        {
            List<ServiceSite> list = null;
            IEnumerable<ServiceSite> coll = Items.Where<ServiceSite>(delegate(ServiceSite s) { return s.SiteID == siteID && s.ServiceID==serviceid; });

            if (coll != null)
                list = coll.ToList<ServiceSite>();

            return list[0];

        }
    }

    [Serializable]
    public class PremiumServiceCollections : IValueObject, ICacheable
    {
        ServiceCollection serviceColl = null;
        ServiceInstanceCollection instances = null;
        ServiceSiteCollection sites = null;

        //caching
        private int _cacheTTLSeconds = 300;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEY = "PremiumServices";
        public PremiumServiceCollections()
        {
            serviceColl = new ServiceCollection();
            instances = new ServiceInstanceCollection();
            sites = new ServiceSiteCollection();
            //populatePremiumServiceColl();
            //populatePremiumServiceSite();
            //populatePremiumServiceInstances();

        }
        public ServiceCollection ServiceCollection
        {
            get { return serviceColl; }
            set { serviceColl = value; }

        }


        public ServiceInstanceCollection InstanceCollection
        {
            get { return instances; }
            set { instances = value; }

        }

        public ServiceSiteCollection ServiceSiteCollection
        {
            get { return sites; }
            set { sites = value; }

        }


        public PremiumService GetService(int instanceid)
        {

            IEnumerable<PremiumService> query = from s in serviceColl
                                        join ss in sites
                                        on s.ServiceID equals ss.ServiceID
                                        join i in instances
                                        on ss.ServiceSiteID equals i.ServiceSiteID
                                        where i.ServiceInstanceID == instanceid
                                        select s;


            return (PremiumService)query.ToArray<PremiumService>()[0];


        }

        public String GetServiceName(int instanceid)
        {

            IEnumerable<string> query = from s in serviceColl
                                        join ss in sites
                                        on s.ServiceID equals ss.ServiceID
                                        join i in instances
                                        on ss.ServiceSiteID equals i.ServiceSiteID
                                        where i.ServiceInstanceID == instanceid
                                        select s.Name;


            return query.ToArray<String>()[0].ToString();


        }
        public List<ServiceInstance> GetServiceSiteInstances(int siteid, int serviceid)
        {

            IEnumerable<ServiceInstance> query = from s in serviceColl
                                                 join ss in sites
                                                 on s.ServiceID equals ss.ServiceID
                                                 join i in instances
                                                 on ss.ServiceSiteID equals i.ServiceSiteID
                                                 where ss.SiteID == siteid && s.ServiceID == serviceid
                                                 select i;


            List<ServiceInstance> coll = query.ToList<ServiceInstance>();
            return coll;

        }

        public List<PremiumService> GetSitePremiumServices(int siteid)
        {

            var query = from s in serviceColl
                        join ss in sites
                        on s.ServiceID equals ss.ServiceID

                        select s;
            List<PremiumService> coll = query.ToList<PremiumService>();
            return coll;

        }


        public T GetAttribute<T>(int serviceinstanceid,int id)
        {
            T value=default(T);
            try
            {
                ServiceInstance inst = instances[serviceinstanceid];
                if (inst == null)
                    return value;


                value=inst.Attributes.Get<T>(id);


                return value;
            }
            catch (Exception ex)
            {
                return value;
            }

        }

        #region ICacheable Members
        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }

        }
        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }


        public string GetCacheKey()
        {
            return CACHEKEY;
        }

        public static string GetCacheKeyString()
        {
            return CACHEKEY;
        }
       

        #endregion

    }
}
