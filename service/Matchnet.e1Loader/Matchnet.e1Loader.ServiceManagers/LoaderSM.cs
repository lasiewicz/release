using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.e1Loader.BusinessLogic;
using Matchnet.e1Loader.ValueObjects;
using Matchnet.Member.ValueObjects;
using Matchnet.Replication;

namespace Matchnet.e1Loader.ServiceManagers
{
    public class LoaderSM : MarshalByRefObject, IServiceManager, IEngineLoaderService, IDisposable, IReplicationActionRecipient
    {
        private const string SERVICE_MANAGER_NAME = "LoaderSM";
        private const string CATEGORY_NAME = Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME;

        private bool _isEnabled;

        private Dictionary<SearchUpdateType, PerformanceCounter> _outboundQueueCounts;
        private Dictionary<SearchUpdateType, PerformanceCounter> _memberUpdateRates;

        public delegate void HealthCheckChangedEventHandler();
        public event HealthCheckChangedEventHandler HealthCheckChanged;

        private Replicator _replicator;
        private string _replicationURI;
        private HydraWriter _hydraWriter;

        public LoaderSM()
        {
            initPerfCounters();
            initializeReplication();
            initializeEngineStatus();
            initializeHydra();
        }

        private void initializeReplication()
        {
            string machineName = System.Environment.MachineName;

            string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
            if (overrideMachineName != null)
            {
                machineName = overrideMachineName;
            }

            _replicationURI = string.Empty;

            try
            {
                _replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("E1LOADERSVC_REPLICATION_OVERRIDE");
            }
            catch { }

            if (_replicationURI.Length == 0)
            {
                _replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_CONSTANT, SERVICE_MANAGER_NAME, machineName);
            }

            _replicator = new Replicator(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME);
            _replicator.SetDestinationUri(_replicationURI);
            _replicator.Start();

            LoaderBL.Instance.ReplicationRequested += new LoaderBL.ReplicationEventHandler(Instance_ReplicationRequested);
        }

        private void initializeEngineStatus()
        {
            string overrideHostName = new Uri(_replicationURI).Host;
            LoaderBL.Instance.InitializeEngineStatus(overrideHostName);
        }

        private void initializeHydra()
        {
            _hydraWriter = new HydraWriter(new string[] { "mnSearchStore" });
            _hydraWriter.Start();
        }

        void Instance_ReplicationRequested(IReplicationAction replicationAction)
        {
            _replicator.Enqueue(replicationAction);
        }

        public void PrePopulateCache()
        {
            // no implementation at this time
        }

        #region IDisposable Members
        public void Dispose()
        {
            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }
        }
        #endregion

        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                _isEnabled = value;
                HealthCheckChanged();
            }
        }

        public override object InitializeLifetimeService()
        {
            return null;
        }



        #region IEngineLoaderService Members

        public void AddServer(string serverName)
        {
            try
            {
                LoaderBL.Instance.AddServer(serverName);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME, "Error adding server.", ex);
            }
        }

        public void RemoveServer(string serverName)
        {
            try
            {
                LoaderBL.Instance.RemoveServer(serverName);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME, "Error removing server.", ex);
            }
        }

        public void StartOutboundQueue(string serverName)
        {
            try
            {
                LoaderBL.Instance.StartOutboundQueue(serverName);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME, "Error starting outbound queue.", ex);
            }
        }

        public void StopOutboundQueue(string serverName)
        {
            try
            {
                LoaderBL.Instance.StopOutboundQueue(serverName);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME, "Error stopping outbound queue.", ex);
            }
        }

        public void ProcessUpdate(SearchUpdate searchUpdate)
        {
            try
            {
                LoaderBL.Instance.ProcessUpdate(searchUpdate);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME, "Error processing search update.", ex);
            }
        }

        public EngineCollection GetEngineStatus()
        {
            try
            {
                return LoaderBL.Instance.GetEngineStatus();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME, "Error retrieving engine status.", ex);
            }
        }

        public Hashtable GetDBMember(Int32 memberID, Int32 communityID)
        {
            try
            {
                return LoaderBL.Instance.GetDBMember(memberID, communityID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME, "Error retrieving member from SearchStore DB.", ex);
            }
        }

        #endregion

        #region Instrumentation

        private void initPerfCounters()
        {
            //Multi-instance counters by update type

            Array updateTypes = Enum.GetValues(typeof(SearchUpdateType));

            _outboundQueueCounts = new Dictionary<SearchUpdateType, PerformanceCounter>(updateTypes.Length);
            _memberUpdateRates = new Dictionary<SearchUpdateType, PerformanceCounter>(updateTypes.Length);

            foreach (SearchUpdateType updateType in updateTypes)
            {
                string instanceName = updateType.ToString();

                _outboundQueueCounts.Add(updateType, new PerformanceCounter(CATEGORY_NAME, "Loader Queue Count", instanceName, false));
                _memberUpdateRates.Add(updateType, new PerformanceCounter(CATEGORY_NAME, "Member Updates/sec", instanceName, false));
            }

            foreach (PerformanceCounter pc in _outboundQueueCounts.Values)
            {
                pc.RawValue = 0;
            }

            foreach (PerformanceCounter pc in _memberUpdateRates.Values)
            {
                pc.RawValue = 0;
            }

            LoaderBL.Instance.OutboundQueueChanged += new LoaderBL.OutboundQueueChangedEventHandler(LoaderBL_OutboundQueueChanged);
            LoaderBL.Instance.MemberUpdated += new LoaderBL.MemberUpdatedEventHandler(LoaderBL_MemberUpdated);
        }

        void LoaderBL_MemberUpdated(SearchUpdateType updateType)
        {
            _memberUpdateRates[updateType].Increment();
        }

        void LoaderBL_OutboundQueueChanged(Int32 delta, SearchUpdateType updateType)
        {
            _outboundQueueCounts[updateType].IncrementBy(delta);
        }

        public static void PerfCounterInstall()
        {
            if (PerformanceCounterCategory.Exists(CATEGORY_NAME))
            {
                PerformanceCounterCategory.Delete(CATEGORY_NAME);
            }

            CounterCreationDataCollection CCDC = new CounterCreationDataCollection();

            CCDC.Add(new CounterCreationData("Outbound Queue Count", "Number of members queued to be sent to e1.", PerformanceCounterType.NumberOfItems32));
            CCDC.Add(new CounterCreationData("Member Updates/sec", "Number of members loaded from DB per second.", PerformanceCounterType.RateOfCountsPerSecond32));

            // Create the category
            PerformanceCounterCategory.Create(CATEGORY_NAME , string.Empty, PerformanceCounterCategoryType.MultiInstance, CCDC);
        }

        public static void PerfCounterUninstall()
        {
            if (PerformanceCounterCategory.Exists(CATEGORY_NAME))
            {
                PerformanceCounterCategory.Delete(CATEGORY_NAME);
            }
        }

        #endregion

        #region IReplicationActionRecipient Members

        public void Receive(IReplicationAction replicationAction)
        {
            LoaderBL.Instance.PlayReplicationAction(replicationAction);
        }

        #endregion
    }
}
