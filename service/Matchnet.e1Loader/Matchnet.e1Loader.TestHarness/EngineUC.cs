using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using Matchnet.e1Loader.ServiceAdapters;
using Matchnet.e1Loader.ValueObjects;


namespace Matchnet.e1Loader.TestHarness
{
   public partial class EngineUC : UserControl
   {
	  public EngineUC(string serverName, bool isFeedOn)
	  {
		 this.cbSelected.Text = serverName;
		 if (isFeedOn)
			cbSelected.ForeColor = Color.ForestGreen;
		 else
			cbSelected.ForeColor = Color.Red;

		 InitializeComponent();
	  }
   }
}
