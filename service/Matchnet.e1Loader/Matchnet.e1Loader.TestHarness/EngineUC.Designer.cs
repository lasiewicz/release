namespace Matchnet.e1Loader.TestHarness
{
   partial class EngineUC
   {
	  /// <summary> 
	  /// Required designer variable.
	  /// </summary>
	  private System.ComponentModel.IContainer components = null;

	  /// <summary> 
	  /// Clean up any resources being used.
	  /// </summary>
	  /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	  protected override void Dispose(bool disposing)
	  {
		 if (disposing && (components != null))
		 {
			components.Dispose();
		 }
		 base.Dispose(disposing);
	  }

	  #region Component Designer generated code

	  /// <summary> 
	  /// Required method for Designer support - do not modify 
	  /// the contents of this method with the code editor.
	  /// </summary>
	  private void InitializeComponent()
	  {
		 this.cbSelected = new System.Windows.Forms.CheckBox();
		 this.SuspendLayout();
		 // 
		 // cbSelected
		 // 
		 this.cbSelected.AutoSize = true;
		 this.cbSelected.Checked = true;
		 this.cbSelected.CheckState = System.Windows.Forms.CheckState.Checked;
		 this.cbSelected.Dock = System.Windows.Forms.DockStyle.Fill;
		 this.cbSelected.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
		 this.cbSelected.Location = new System.Drawing.Point(0, 0);
		 this.cbSelected.Name = "cbSelected";
		 this.cbSelected.Size = new System.Drawing.Size(147, 23);
		 this.cbSelected.TabIndex = 1;
		 this.cbSelected.Text = "{engine server name}";
		 this.cbSelected.UseVisualStyleBackColor = true;
		 // 
		 // EngineUC
		 // 
		 this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
		 this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
		 this.AutoSize = true;
		 this.Controls.Add(this.cbSelected);
		 this.Name = "EngineUC";
		 this.Size = new System.Drawing.Size(147, 23);
		 this.ResumeLayout(false);
		 this.PerformLayout();

	  }

	  #endregion

	  private System.Windows.Forms.CheckBox cbSelected;

   }
}
