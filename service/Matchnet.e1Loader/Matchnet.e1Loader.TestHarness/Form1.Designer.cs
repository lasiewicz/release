namespace Matchnet.e1Loader.TestHarness
{
   partial class Form1
   {
	  /// <summary>
	  /// Required designer variable.
	  /// </summary>
	  private System.ComponentModel.IContainer components = null;

	  /// <summary>
	  /// Clean up any resources being used.
	  /// </summary>
	  /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	  protected override void Dispose(bool disposing)
	  {
		 if (disposing && (components != null))
		 {
			components.Dispose();
		 }
		 base.Dispose(disposing);
	  }

	  #region Windows Form Designer generated code

	  /// <summary>
	  /// Required method for Designer support - do not modify
	  /// the contents of this method with the code editor.
	  /// </summary>
	  private void InitializeComponent()
	  {
          this.btnBulkLoad = new System.Windows.Forms.Button();
          this.btnBulkLoadCSV = new System.Windows.Forms.Button();
          this.btnAddServer = new System.Windows.Forms.Button();
          this.lbServers = new System.Windows.Forms.ListBox();
          this.tbServerName = new System.Windows.Forms.TextBox();
          this.btnStartOutbound = new System.Windows.Forms.Button();
          this.btnStopOutbound = new System.Windows.Forms.Button();
          this.btnDevLoad = new System.Windows.Forms.Button();
          this.btnUpdateTest = new System.Windows.Forms.Button();
          this.btmRemove = new System.Windows.Forms.Button();
          this.btnStopFeedForSelectedEngine = new System.Windows.Forms.Button();
          this.lbLoaderNames = new System.Windows.Forms.ListBox();
          this.lbEngines = new System.Windows.Forms.ListBox();
          this.groupBox1 = new System.Windows.Forms.GroupBox();
          this.txtFeedStatus = new System.Windows.Forms.TextBox();
          this.btnStartFeedForSelectedEngine = new System.Windows.Forms.Button();
          this.txtUR = new System.Windows.Forms.TextBox();
          this.label1 = new System.Windows.Forms.Label();
          this.label2 = new System.Windows.Forms.Label();
          this.txte1Serv = new System.Windows.Forms.TextBox();
          this.btnRemove = new System.Windows.Forms.Button();
          this.button1 = new System.Windows.Forms.Button();
          this.groupBox1.SuspendLayout();
          this.SuspendLayout();
          // 
          // btnBulkLoad
          // 
          this.btnBulkLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
          this.btnBulkLoad.Location = new System.Drawing.Point(402, 36);
          this.btnBulkLoad.Name = "btnBulkLoad";
          this.btnBulkLoad.Size = new System.Drawing.Size(124, 23);
          this.btnBulkLoad.TabIndex = 0;
          this.btnBulkLoad.Text = "Begin Bulk Load";
          this.btnBulkLoad.UseVisualStyleBackColor = true;
          // 
          // btnBulkLoadCSV
          // 
          this.btnBulkLoadCSV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
          this.btnBulkLoadCSV.Location = new System.Drawing.Point(402, 65);
          this.btnBulkLoadCSV.Name = "btnBulkLoadCSV";
          this.btnBulkLoadCSV.Size = new System.Drawing.Size(124, 23);
          this.btnBulkLoadCSV.TabIndex = 1;
          this.btnBulkLoadCSV.Text = "Begin Bulk Load CSV";
          this.btnBulkLoadCSV.UseVisualStyleBackColor = true;
          // 
          // btnAddServer
          // 
          this.btnAddServer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
          this.btnAddServer.Location = new System.Drawing.Point(402, 7);
          this.btnAddServer.Name = "btnAddServer";
          this.btnAddServer.Size = new System.Drawing.Size(124, 23);
          this.btnAddServer.TabIndex = 2;
          this.btnAddServer.Text = "Add Server";
          this.btnAddServer.UseVisualStyleBackColor = true;
          this.btnAddServer.Click += new System.EventHandler(this.btnAddServer_Click);
          // 
          // lbServers
          // 
          this.lbServers.FormattingEnabled = true;
          this.lbServers.Location = new System.Drawing.Point(14, 36);
          this.lbServers.Name = "lbServers";
          this.lbServers.Size = new System.Drawing.Size(276, 69);
          this.lbServers.TabIndex = 3;
          this.lbServers.SelectedIndexChanged += new System.EventHandler(this.lbServers_SelectedIndexChanged);
          // 
          // tbServerName
          // 
          this.tbServerName.Location = new System.Drawing.Point(14, 10);
          this.tbServerName.Name = "tbServerName";
          this.tbServerName.Size = new System.Drawing.Size(276, 20);
          this.tbServerName.TabIndex = 4;
          // 
          // btnStartOutbound
          // 
          this.btnStartOutbound.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
          this.btnStartOutbound.Location = new System.Drawing.Point(402, 94);
          this.btnStartOutbound.Name = "btnStartOutbound";
          this.btnStartOutbound.Size = new System.Drawing.Size(124, 23);
          this.btnStartOutbound.TabIndex = 5;
          this.btnStartOutbound.Text = "Start Outbound Queue";
          this.btnStartOutbound.UseVisualStyleBackColor = true;
          this.btnStartOutbound.Click += new System.EventHandler(this.btnStartOutbound_Click);
          // 
          // btnStopOutbound
          // 
          this.btnStopOutbound.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
          this.btnStopOutbound.Location = new System.Drawing.Point(402, 123);
          this.btnStopOutbound.Name = "btnStopOutbound";
          this.btnStopOutbound.Size = new System.Drawing.Size(124, 23);
          this.btnStopOutbound.TabIndex = 6;
          this.btnStopOutbound.Text = "Stop Outbound Queue";
          this.btnStopOutbound.UseVisualStyleBackColor = true;
          this.btnStopOutbound.Click += new System.EventHandler(this.btnStopOutbound_Click);
          // 
          // btnDevLoad
          // 
          this.btnDevLoad.Location = new System.Drawing.Point(14, 118);
          this.btnDevLoad.Name = "btnDevLoad";
          this.btnDevLoad.Size = new System.Drawing.Size(124, 32);
          this.btnDevLoad.TabIndex = 7;
          this.btnDevLoad.Text = "Dev Load";
          this.btnDevLoad.UseVisualStyleBackColor = true;
          this.btnDevLoad.Click += new System.EventHandler(this.btnDevLoad_Click);
          // 
          // btnUpdateTest
          // 
          this.btnUpdateTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
          this.btnUpdateTest.Location = new System.Drawing.Point(402, 181);
          this.btnUpdateTest.Name = "btnUpdateTest";
          this.btnUpdateTest.Size = new System.Drawing.Size(124, 23);
          this.btnUpdateTest.TabIndex = 8;
          this.btnUpdateTest.Text = "Update Test";
          this.btnUpdateTest.UseVisualStyleBackColor = true;
          this.btnUpdateTest.Click += new System.EventHandler(this.btnUpdateTest_Click);
          // 
          // btmRemove
          // 
          this.btmRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
          this.btmRemove.Location = new System.Drawing.Point(402, 152);
          this.btmRemove.Name = "btmRemove";
          this.btmRemove.Size = new System.Drawing.Size(124, 23);
          this.btmRemove.TabIndex = 9;
          this.btmRemove.Text = "Remove";
          this.btmRemove.UseVisualStyleBackColor = true;
          this.btmRemove.Click += new System.EventHandler(this.btmRemove_Click);
          // 
          // btnStopFeedForSelectedEngine
          // 
          this.btnStopFeedForSelectedEngine.Location = new System.Drawing.Point(6, 105);
          this.btnStopFeedForSelectedEngine.Name = "btnStopFeedForSelectedEngine";
          this.btnStopFeedForSelectedEngine.Size = new System.Drawing.Size(96, 21);
          this.btnStopFeedForSelectedEngine.TabIndex = 11;
          this.btnStopFeedForSelectedEngine.Text = "Stop Feed";
          this.btnStopFeedForSelectedEngine.UseVisualStyleBackColor = true;
          this.btnStopFeedForSelectedEngine.Click += new System.EventHandler(this.btnStopFeedForSelectedEngine_Click);
          // 
          // lbLoaderNames
          // 
          this.lbLoaderNames.FormattingEnabled = true;
          this.lbLoaderNames.Location = new System.Drawing.Point(6, 19);
          this.lbLoaderNames.Name = "lbLoaderNames";
          this.lbLoaderNames.Size = new System.Drawing.Size(97, 82);
          this.lbLoaderNames.TabIndex = 12;
          this.lbLoaderNames.SelectedIndexChanged += new System.EventHandler(this.lbLoaderNames_SelectedIndexChanged);
          this.lbLoaderNames.SelectedValueChanged += new System.EventHandler(this.lbLoaderNames_SelectedValueChanged);
          // 
          // lbEngines
          // 
          this.lbEngines.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)));
          this.lbEngines.FormattingEnabled = true;
          this.lbEngines.Location = new System.Drawing.Point(109, 19);
          this.lbEngines.Name = "lbEngines";
          this.lbEngines.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
          this.lbEngines.Size = new System.Drawing.Size(143, 316);
          this.lbEngines.TabIndex = 13;
          // 
          // groupBox1
          // 
          this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.groupBox1.Controls.Add(this.txtFeedStatus);
          this.groupBox1.Controls.Add(this.btnStartFeedForSelectedEngine);
          this.groupBox1.Controls.Add(this.lbEngines);
          this.groupBox1.Controls.Add(this.btnStopFeedForSelectedEngine);
          this.groupBox1.Controls.Add(this.lbLoaderNames);
          this.groupBox1.Location = new System.Drawing.Point(14, 248);
          this.groupBox1.Name = "groupBox1";
          this.groupBox1.Size = new System.Drawing.Size(512, 341);
          this.groupBox1.TabIndex = 14;
          this.groupBox1.TabStop = false;
          this.groupBox1.Text = "Loader Control";
          // 
          // txtFeedStatus
          // 
          this.txtFeedStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.txtFeedStatus.Location = new System.Drawing.Point(258, 20);
          this.txtFeedStatus.Multiline = true;
          this.txtFeedStatus.Name = "txtFeedStatus";
          this.txtFeedStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
          this.txtFeedStatus.Size = new System.Drawing.Size(248, 315);
          this.txtFeedStatus.TabIndex = 15;
          // 
          // btnStartFeedForSelectedEngine
          // 
          this.btnStartFeedForSelectedEngine.Location = new System.Drawing.Point(6, 132);
          this.btnStartFeedForSelectedEngine.Name = "btnStartFeedForSelectedEngine";
          this.btnStartFeedForSelectedEngine.Size = new System.Drawing.Size(96, 21);
          this.btnStartFeedForSelectedEngine.TabIndex = 14;
          this.btnStartFeedForSelectedEngine.Text = "Start Feed";
          this.btnStartFeedForSelectedEngine.UseVisualStyleBackColor = true;
          this.btnStartFeedForSelectedEngine.Click += new System.EventHandler(this.btnSartFeedForSelectedEngine_Click);
          // 
          // txtUR
          // 
          this.txtUR.Location = new System.Drawing.Point(50, 158);
          this.txtUR.Name = "txtUR";
          this.txtUR.Size = new System.Drawing.Size(334, 20);
          this.txtUR.TabIndex = 15;
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(11, 158);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(26, 13);
          this.label1.TabIndex = 16;
          this.label1.Text = "URI";
          // 
          // label2
          // 
          this.label2.AutoSize = true;
          this.label2.Location = new System.Drawing.Point(11, 180);
          this.label2.Name = "label2";
          this.label2.Size = new System.Drawing.Size(42, 13);
          this.label2.TabIndex = 18;
          this.label2.Text = "e1 serv";
          // 
          // txte1Serv
          // 
          this.txte1Serv.Location = new System.Drawing.Point(50, 180);
          this.txte1Serv.Name = "txte1Serv";
          this.txte1Serv.Size = new System.Drawing.Size(204, 20);
          this.txte1Serv.TabIndex = 17;
          // 
          // btnRemove
          // 
          this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
          this.btnRemove.Location = new System.Drawing.Point(260, 181);
          this.btnRemove.Name = "btnRemove";
          this.btnRemove.Size = new System.Drawing.Size(83, 23);
          this.btnRemove.TabIndex = 19;
          this.btnRemove.Text = "Remove";
          this.btnRemove.UseVisualStyleBackColor = true;
          this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
          // 
          // button1
          // 
          this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
          this.button1.Location = new System.Drawing.Point(334, 181);
          this.button1.Name = "button1";
          this.button1.Size = new System.Drawing.Size(62, 23);
          this.button1.TabIndex = 20;
          this.button1.Text = "Add";
          this.button1.UseVisualStyleBackColor = true;
          this.button1.Click += new System.EventHandler(this.button1_Click);
          // 
          // Form1
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(538, 601);
          this.Controls.Add(this.button1);
          this.Controls.Add(this.btnRemove);
          this.Controls.Add(this.label2);
          this.Controls.Add(this.txte1Serv);
          this.Controls.Add(this.label1);
          this.Controls.Add(this.txtUR);
          this.Controls.Add(this.groupBox1);
          this.Controls.Add(this.btmRemove);
          this.Controls.Add(this.btnUpdateTest);
          this.Controls.Add(this.btnDevLoad);
          this.Controls.Add(this.btnStopOutbound);
          this.Controls.Add(this.btnStartOutbound);
          this.Controls.Add(this.tbServerName);
          this.Controls.Add(this.lbServers);
          this.Controls.Add(this.btnAddServer);
          this.Controls.Add(this.btnBulkLoadCSV);
          this.Controls.Add(this.btnBulkLoad);
          this.Name = "Form1";
          this.Text = "Form1";
          this.Load += new System.EventHandler(this.Form1_Load);
          this.groupBox1.ResumeLayout(false);
          this.groupBox1.PerformLayout();
          this.ResumeLayout(false);
          this.PerformLayout();

	  }

	  #endregion

	  private System.Windows.Forms.Button btnBulkLoad;
	  private System.Windows.Forms.Button btnBulkLoadCSV;
	  private System.Windows.Forms.Button btnAddServer;
	  private System.Windows.Forms.ListBox lbServers;
	  private System.Windows.Forms.TextBox tbServerName;
	  private System.Windows.Forms.Button btnStartOutbound;
	  private System.Windows.Forms.Button btnStopOutbound;
	  private System.Windows.Forms.Button btnDevLoad;
	  private System.Windows.Forms.Button btnUpdateTest;
	  private System.Windows.Forms.Button btmRemove;
	  private System.Windows.Forms.Button btnStopFeedForSelectedEngine;
	  private System.Windows.Forms.ListBox lbLoaderNames;
	  private System.Windows.Forms.ListBox lbEngines;
	  private System.Windows.Forms.GroupBox groupBox1;
	  private System.Windows.Forms.Button btnStartFeedForSelectedEngine;
	  private System.Windows.Forms.TextBox txtFeedStatus;
      private System.Windows.Forms.TextBox txtUR;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.TextBox txte1Serv;
      private System.Windows.Forms.Button btnRemove;
      private System.Windows.Forms.Button button1;
   }
}

