using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Matchnet.e1Loader.ServiceAdapters;
using Matchnet.Member.ValueObjects;

namespace Matchnet.e1Loader.TestHarness
{
    public partial class SearchUpdateHarness : Form
    {
        public SearchUpdateHarness()
        {
            InitializeComponent();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            SearchUpdate searchUpdate = new SearchUpdate(Conversion.CInt(tbMemberID.Text),
                Conversion.CInt(tbCommunityID.Text),
                (SearchUpdateType) cbUpdateType.SelectedItem,
                //(SearchUpdateType) Enum.Parse(typeof(SearchUpdateType), lbUpdateType.SelectedItem),
                Conversion.CInt(tbEmailCount.Text),
                dtLastActiveDate.Value,
                cbHasPhoto.Checked);

            if (cbAsync.Checked)
            {
                LoaderSA.Instance.ProcessUpdateAsync(searchUpdate);
            }
            else
            {
                LoaderSA.Instance.ProcessUpdate(searchUpdate);
            }
        }

        private void SearchUpdateHarness_Load(object sender, EventArgs e)
        {
            cbUpdateType.DataSource = Enum.GetValues(typeof(SearchUpdateType));
        }
    }
}