namespace Matchnet.e1Loader.TestHarness
{
    partial class SearchUpdateHarness
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUpdate = new System.Windows.Forms.Button();
            this.cbAsync = new System.Windows.Forms.CheckBox();
            this.tbMemberID = new System.Windows.Forms.TextBox();
            this.tbCommunityID = new System.Windows.Forms.TextBox();
            this.lblMemberID = new System.Windows.Forms.Label();
            this.lblCommunityID = new System.Windows.Forms.Label();
            this.lblUpdateType = new System.Windows.Forms.Label();
            this.lblEmailCount = new System.Windows.Forms.Label();
            this.lblLastActiveDate = new System.Windows.Forms.Label();
            this.tbEmailCount = new System.Windows.Forms.TextBox();
            this.cbHasPhoto = new System.Windows.Forms.CheckBox();
            this.dtLastActiveDate = new System.Windows.Forms.DateTimePicker();
            this.cbUpdateType = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(144, 227);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(101, 36);
            this.btnUpdate.TabIndex = 0;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // cbAsync
            // 
            this.cbAsync.AutoSize = true;
            this.cbAsync.Location = new System.Drawing.Point(39, 238);
            this.cbAsync.Name = "cbAsync";
            this.cbAsync.Size = new System.Drawing.Size(55, 17);
            this.cbAsync.TabIndex = 1;
            this.cbAsync.Text = "Async";
            this.cbAsync.UseVisualStyleBackColor = true;
            // 
            // tbMemberID
            // 
            this.tbMemberID.Location = new System.Drawing.Point(102, 25);
            this.tbMemberID.Name = "tbMemberID";
            this.tbMemberID.Size = new System.Drawing.Size(142, 20);
            this.tbMemberID.TabIndex = 2;
            // 
            // tbCommunityID
            // 
            this.tbCommunityID.Location = new System.Drawing.Point(102, 54);
            this.tbCommunityID.Name = "tbCommunityID";
            this.tbCommunityID.Size = new System.Drawing.Size(142, 20);
            this.tbCommunityID.TabIndex = 3;
            // 
            // lblMemberID
            // 
            this.lblMemberID.AutoSize = true;
            this.lblMemberID.Location = new System.Drawing.Point(12, 27);
            this.lblMemberID.Name = "lblMemberID";
            this.lblMemberID.Size = new System.Drawing.Size(56, 13);
            this.lblMemberID.TabIndex = 4;
            this.lblMemberID.Text = "MemberID";
            // 
            // lblCommunityID
            // 
            this.lblCommunityID.AutoSize = true;
            this.lblCommunityID.Location = new System.Drawing.Point(12, 54);
            this.lblCommunityID.Name = "lblCommunityID";
            this.lblCommunityID.Size = new System.Drawing.Size(69, 13);
            this.lblCommunityID.TabIndex = 5;
            this.lblCommunityID.Text = "CommunityID";
            // 
            // lblUpdateType
            // 
            this.lblUpdateType.AutoSize = true;
            this.lblUpdateType.Location = new System.Drawing.Point(12, 84);
            this.lblUpdateType.Name = "lblUpdateType";
            this.lblUpdateType.Size = new System.Drawing.Size(69, 13);
            this.lblUpdateType.TabIndex = 6;
            this.lblUpdateType.Text = "Update Type";
            // 
            // lblEmailCount
            // 
            this.lblEmailCount.AutoSize = true;
            this.lblEmailCount.Location = new System.Drawing.Point(12, 114);
            this.lblEmailCount.Name = "lblEmailCount";
            this.lblEmailCount.Size = new System.Drawing.Size(63, 13);
            this.lblEmailCount.TabIndex = 8;
            this.lblEmailCount.Text = "Email Count";
            // 
            // lblLastActiveDate
            // 
            this.lblLastActiveDate.AutoSize = true;
            this.lblLastActiveDate.Location = new System.Drawing.Point(12, 140);
            this.lblLastActiveDate.Name = "lblLastActiveDate";
            this.lblLastActiveDate.Size = new System.Drawing.Size(80, 13);
            this.lblLastActiveDate.TabIndex = 9;
            this.lblLastActiveDate.Text = "LastActiveDate";
            // 
            // tbEmailCount
            // 
            this.tbEmailCount.Location = new System.Drawing.Point(102, 111);
            this.tbEmailCount.Name = "tbEmailCount";
            this.tbEmailCount.Size = new System.Drawing.Size(142, 20);
            this.tbEmailCount.TabIndex = 12;
            // 
            // cbHasPhoto
            // 
            this.cbHasPhoto.AutoSize = true;
            this.cbHasPhoto.Location = new System.Drawing.Point(102, 179);
            this.cbHasPhoto.Name = "cbHasPhoto";
            this.cbHasPhoto.Size = new System.Drawing.Size(82, 17);
            this.cbHasPhoto.TabIndex = 13;
            this.cbHasPhoto.Text = "Has Photo?";
            this.cbHasPhoto.UseVisualStyleBackColor = true;
            // 
            // dtLastActiveDate
            // 
            this.dtLastActiveDate.Location = new System.Drawing.Point(102, 140);
            this.dtLastActiveDate.Name = "dtLastActiveDate";
            this.dtLastActiveDate.Size = new System.Drawing.Size(143, 20);
            this.dtLastActiveDate.TabIndex = 14;
            // 
            // cbUpdateType
            // 
            this.cbUpdateType.FormattingEnabled = true;
            this.cbUpdateType.Location = new System.Drawing.Point(102, 81);
            this.cbUpdateType.Name = "cbUpdateType";
            this.cbUpdateType.Size = new System.Drawing.Size(141, 21);
            this.cbUpdateType.TabIndex = 15;
            // 
            // SearchUpdateHarness
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Controls.Add(this.cbUpdateType);
            this.Controls.Add(this.dtLastActiveDate);
            this.Controls.Add(this.cbHasPhoto);
            this.Controls.Add(this.tbEmailCount);
            this.Controls.Add(this.lblLastActiveDate);
            this.Controls.Add(this.lblEmailCount);
            this.Controls.Add(this.lblUpdateType);
            this.Controls.Add(this.lblCommunityID);
            this.Controls.Add(this.lblMemberID);
            this.Controls.Add(this.tbCommunityID);
            this.Controls.Add(this.tbMemberID);
            this.Controls.Add(this.cbAsync);
            this.Controls.Add(this.btnUpdate);
            this.Name = "SearchUpdateHarness";
            this.Text = "SearchUpdate";
            this.Load += new System.EventHandler(this.SearchUpdateHarness_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.CheckBox cbAsync;
        private System.Windows.Forms.TextBox tbMemberID;
        private System.Windows.Forms.TextBox tbCommunityID;
        private System.Windows.Forms.Label lblMemberID;
        private System.Windows.Forms.Label lblCommunityID;
        private System.Windows.Forms.Label lblUpdateType;
        private System.Windows.Forms.Label lblEmailCount;
        private System.Windows.Forms.Label lblLastActiveDate;
        private System.Windows.Forms.TextBox tbEmailCount;
        private System.Windows.Forms.CheckBox cbHasPhoto;
        private System.Windows.Forms.DateTimePicker dtLastActiveDate;
        private System.Windows.Forms.ComboBox cbUpdateType;
    }
}