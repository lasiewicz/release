using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.e1Loader.ServiceAdapters;
using Matchnet.e1Loader.ValueObjects;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using System.Collections;
using Matchnet.e1.ServiceAdapters;
using Matchnet.e1Loader.BusinessLogic;


namespace Matchnet.e1Loader.TestHarness
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                populateLoaderInstances();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error populating Loader list");
            }

            //try
            //{
            //   foreach (DictionaryEntry entry in LoaderSA.Instance.GetEngineStatus())
            //   {
            //      Engine engine = entry.Value as Engine;
            //      lbServers.Items.Add(engine);
            //   }
            //}
            //catch (Exception ex)
            //{
            //   MessageBox.Show(ex.ToString(), "Error populating engines list");
            //}
        }

        private string[] serverNames
        {
            get
            {
                ListBox.ObjectCollection servers = lbServers.Items;
                string[] retVal = new string[servers.Count];

                for (Int32 i = 0; i < servers.Count; i++)
                {
                    retVal[i] = servers[i].ToString();
                }

                return retVal;
            }
        }

        private void btnAddServer_Click(object sender, EventArgs e)
        {
            addServer(tbServerName.Text);
        }

        private void addServer(string serverName)
        {
            lbServers.Items.Add(new Engine(serverName));
            LoaderSA.Instance.AddServer(serverName);
        }

        private void btnStartOutbound_Click(object sender, EventArgs e)
        {
            //LoaderSA.Instance.StartOutboundQueue(lbServers.SelectedItem.ToString());
            IEngineLoaderService service = getService(txtUR.Text);
            service.StartOutboundQueue(txte1Serv.Text);
        }

        private void btnStopOutbound_Click(object sender, EventArgs e)
        {
          //  LoaderSA.Instance.StopOutboundQueue(lbServers.SelectedItem.ToString());
            IEngineLoaderService service = getService(txtUR.Text);
            service.StopOutboundQueue(txte1Serv.Text);
        }

        //Queues up member ids from a text file -- useful for populating dev DB
        private void btnDevLoad_Click(object sender, EventArgs e)
        {
            ArrayList results = new ArrayList();
            StreamReader sr = new StreamReader(@"C:\ASmemIDs.txt");
            Hashtable communities = BrandConfigSA.Instance.GetCommunities();
            bool excluded;
            bool alreadyLoaded;

            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();
                Int32 memberID = Conversion.CInt(line);

                Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

                foreach (Int32 communityID in member.GetCommunityIDList())
                {
                    string commandResult = EngineSA.Instance.EvaluateCommand("show?type=mem&memid=" + memberID + "&domid=" + communityID).Xml;
                    alreadyLoaded = !commandResult.Contains("NotFound");

                    string exclusionReason;
                    excluded = MemberHelper.GetE1Member(memberID, communityID, out exclusionReason) == null;

                    if (!excluded && !alreadyLoaded)
                    {
                        SearchUpdate searchUpdate = new SearchUpdate(memberID, communityID, SearchUpdateType.Update, Constants.NULL_INT, DateTime.MinValue, false);
                        LoaderSA.Instance.ProcessUpdateAsync(searchUpdate);
                    }

                    results.Add(new MemberLoadResult(memberID, communityID, alreadyLoaded, excluded, exclusionReason, member.GetInsertDate(communityID)));
                }
            }

            /*StringBuilder result = new StringBuilder();
            result.Append("Total members: " + totalMembers + Environment.NewLine);
            result.Append("Total member/community pairs: " + totalMemberCommunities + Environment.NewLine);
            result.Append("Excluded: " + excluded + Environment.NewLine);
            result.Append("Already loaded: " + alreadyLoaded + Environment.NewLine);
            MessageBox.Show(result.ToString());*/

            StreamWriter sw = new StreamWriter(@"C:\ASReload.txt");

            foreach (MemberLoadResult result in results)
            {
                sw.WriteLine(result.RegisterDate + "," + result.MemberID + "," + result.CommunityID + "," + result.AlreadyLoaded + "," + result.IsExcluded + "," + result.ExclusionReason);
            }

            sw.Close();
        }

        private void btnUpdateTest_Click(object sender, EventArgs e)
        {
            new SearchUpdateHarness().Show();
        }


        private void btmRemove_Click(object sender, EventArgs e)
        {
            LoaderSA.Instance.RemoveServer(lbServers.SelectedItem.ToString());
        }

        private void lbServers_SelectedIndexChanged(object sender, EventArgs e)
        {
            Engine engine = lbServers.SelectedItem as Engine;

            if (engine.UpdatesEnabled)
            {
                btnStartOutbound.Enabled = false;
                btnStopOutbound.Enabled = true;
            }
            else
            {
                btnStartOutbound.Enabled = false;
                btnStopOutbound.Enabled = true;
            }
        }


        private void populateLoaderInstances()
        {
            string[] loaderInstances = Configuration.ServiceAdapters.AdapterConfigurationSA.GetServiceHosts(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_CONSTANT) ?? new string[] { };
            for (int i = 0; i < loaderInstances.Length; i++)
                this.lbLoaderNames.Items.Add(loaderInstances[i]);

            //foreach (string loaderName in e1Loader.TestHarness.Properties.Settings.Default.LoaderList)
            //    this.lbLoaderNames.Items.Add(loaderName);

        }


        private void lbLoaderNames_SelectedValueChanged(object sender, EventArgs e)
        {
            lbEngines.Items.Clear();
            string selectedLoader = (sender as ListBox).Text;
            EngineCollection engineList = LoaderSA.Instance.GetEngineStatus(selectedLoader);
            foreach (DictionaryEntry de in engineList)
            {
                Engine engine = de.Value as Engine;

                int pos = lbEngines.Items.Add(new EngineListItem(engine.ServerName, engine, engine.UpdatesEnabled));

            }

        }

        private void btnStopFeedForSelectedEngine_Click(object sender, EventArgs e)
        {
            txtFeedStatus.Text += "Loader@" + lbLoaderNames.SelectedItem.ToString() + " feed STOP" + Environment.NewLine;
            for (int i = 0; i < lbEngines.Items.Count; i++)
            {
                if (lbEngines.GetSelected(i))
                {
                    Engine engine = (lbEngines.Items[i] as EngineListItem).Engine;
                    string engineName = engine.ServerName;
                    //LoaderSA.Instance.StopOutboundQueue(engineName);
                    LoaderSA.Instance.RemoveServer(engineName);
                    txtFeedStatus.Text += " -> " + engineName + Environment.NewLine;
                }
            }
            txtFeedStatus.ScrollToCaret();
        }

        private void btnSartFeedForSelectedEngine_Click(object sender, EventArgs e)
        {
            txtFeedStatus.Text += "Loader@" + lbLoaderNames.SelectedItem.ToString() + " feed START" + Environment.NewLine;
            for (int i = 0; i < lbEngines.Items.Count; i++)
            {
                if (lbEngines.GetSelected(i))
                {
                    Engine engine = (lbEngines.Items[i] as EngineListItem).Engine;
                    string engineName = engine.ServerName;
                    EngineSA.Instance.ChangeMaintenance(engineName, false);
                    LoaderSA.Instance.StartOutboundQueue(engineName);
                    txtFeedStatus.Text += " -> " + engineName + Environment.NewLine;
                }
            }
            txtFeedStatus.ScrollToCaret();
        }

        private IEngineLoaderService getService(string uri)
        {
            try
            {
                return (IEngineLoaderService)Activator.GetObject(typeof(IEngineLoaderService), uri);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString()); return null;
            }
        }

        private class EngineListItem
        {
            public string Name;
            public Engine Engine;
            public bool FeedEnabled;

            public EngineListItem(string name, Engine engine, bool isFeedEnabled)
            {
                this.Engine = engine;
                this.Name = name;
                this.FeedEnabled = isFeedEnabled;
            }

            public override string ToString()
            {
                return "e1@" + this.Name + "-" + FeedEnabled.ToString();
            }
        }

        public class MemberLoadResult
        {
            public Int32 MemberID;
            public Int32 CommunityID;
            public bool AlreadyLoaded;
            public bool IsExcluded;
            public string ExclusionReason;
            public DateTime RegisterDate;

            public MemberLoadResult(Int32 memberID, Int32 communityID, bool alreadyLoaded, bool isExcluded, string exclusionReason, DateTime registerDate)
            {
                MemberID = memberID;
                CommunityID = communityID;
                AlreadyLoaded = alreadyLoaded;
                IsExcluded = isExcluded;
                ExclusionReason = exclusionReason;
                RegisterDate = registerDate;
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {


                IEngineLoaderService service = getService(txtUR.Text);
                service.RemoveServer(txte1Serv.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void lbLoaderNames_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {


                IEngineLoaderService service = getService(txtUR.Text);
                service.AddServer(txte1Serv.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}