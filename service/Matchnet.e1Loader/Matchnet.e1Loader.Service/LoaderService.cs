using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.ServiceProcess;
using System.Text;

using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Matchnet.e1Loader.ServiceManagers;
using Matchnet.e1Loader.ValueObjects;

namespace Matchnet.e1Loader.Service
{
    public partial class LoaderService : Matchnet.RemotingServices.RemotingServiceBase
    {
        private LoaderSM _loaderSM;
        private System.ComponentModel.Container components;

        public LoaderService()
        {
            InitializeComponent();
        }

        protected override void RegisterServiceManagers()
        {
            try
            {
                _loaderSM = new LoaderSM();
               base.RegisterServiceManager(_loaderSM);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occurred when starting the Windows Service, see details:" + ex.Message);
            }
        }

        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = ServiceConstants.SERVICE_NAME;
            base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
        }

        protected override void Dispose(bool disposing)
        {
            if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}

			if (_loaderSM != null) 
			{
				_loaderSM.Dispose();
			}

            base.Dispose(disposing);
        }
    }
}