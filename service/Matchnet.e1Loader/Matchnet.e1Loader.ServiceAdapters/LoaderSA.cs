using System;
using System.Collections;
using System.Text;

using Matchnet.Exceptions;
using Matchnet.Member.ValueObjects;
using Matchnet.RemotingClient;
using Matchnet.e1Loader.ValueObjects;

namespace Matchnet.e1Loader.ServiceAdapters
{
    public class LoaderSA : SABase
    {
        private const string SERVICE_MANAGER_NAME = "LoaderSM";
        public static LoaderSA Instance = new LoaderSA();
        private SearchUpdater _searchUpdater;

        private LoaderSA()
        {
        }

        public void AddServer(String serverName)
        {
            String uri = String.Empty;

            try
            {
                uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    getService(uri).AddServer(serverName);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot add server (uri: " + uri + ")", ex));
            }
        }

        public void RemoveServer(String serverName)
        {
            String uri = String.Empty;

            try
            {
                uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    getService(uri).RemoveServer(serverName);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot remove server (uri: " + uri + ")", ex));
            }
        }


        public void StartOutboundQueue(String serverName)
        {
            String uri = String.Empty;

            try
            {
                uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    getService(uri).StartOutboundQueue(serverName);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot start outbound queue (uri: " + uri + ")", ex));
            }
        }


        public void StopOutboundQueue(String serverName)
        {
            String uri = String.Empty;

            try
            {
                uri = getServiceManagerUri();
                base.Checkout(uri);
                try
                {
                    getService(uri).StopOutboundQueue(serverName);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot stop outbound queue (uri: " + uri + ")", ex));
            }
        }

        public void ProcessUpdate(SearchUpdate searchUpdate)
        {
            ProcessUpdate(searchUpdate, String.Empty);
        }

        public void ProcessUpdate(SearchUpdate searchUpdate, string overrideHostName)
        {
            String uri = String.Empty;

            try
            {
                if (overrideHostName == string.Empty)
                {
                    uri = getServiceManagerUri();
                }
                else
                {
                    uri = getServiceManagerUri(overrideHostName);
                }

                base.Checkout(uri);
                try
                {
                    getService(uri).ProcessUpdate(searchUpdate);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot process search update (uri: " + uri + ")", ex));
            }
        }

        public void ProcessUpdateAsync(SearchUpdate searchUpdate)
        {
            try
            {
                if (_searchUpdater == null)
                {
                    lock (this)
                    {
                        if (_searchUpdater == null)
                        {
                            _searchUpdater = new SearchUpdater();
                        }
                    }
                }

                _searchUpdater.Enqueue(searchUpdate);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot process search update." + ex.ToString()));
            }
        }

        public EngineCollection GetEngineStatus()
        {
            return GetEngineStatus(string.Empty);
        }

        public EngineCollection GetEngineStatus(string overrideHostName)
        {
            String uri = String.Empty;

            try
            {
                uri = getServiceManagerUri(overrideHostName);
                base.Checkout(uri);
                try
                {
                    return getService(uri).GetEngineStatus();
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve engine status (uri: " + uri + ")", ex));
            }
        }

        public Hashtable GetDBMember(Int32 memberID, Int32 communityID)
        {
            return GetDBMember(memberID, communityID, String.Empty);
        }

        public Hashtable GetDBMember(Int32 memberID, Int32 communityID, string overrideHostName)
        {
            String uri = String.Empty;

            try
            {
                if (overrideHostName == string.Empty)
                {
                    uri = getServiceManagerUri();
                }
                else
                {
                    uri = getServiceManagerUri(overrideHostName);
                }

                base.Checkout(uri);
                try
                {
                    return getService(uri).GetDBMember(memberID, communityID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve member from SearchStore DB (uri: " + uri + ")", ex));
            }
        }

        private string getServiceManagerUri()
        {
            try
            {
                return getServiceManagerUri(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("E1LOADERSVC_SA_HOST_OVERRIDE"));
            }
            catch { }

            return getServiceManagerUri(string.Empty);
        }

        private string getServiceManagerUri(string overrideHostName)
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(SERVICE_MANAGER_NAME);

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }

        private IEngineLoaderService getService(string uri)
        {
            try
            {
                return (IEngineLoaderService)Activator.GetObject(typeof(IEngineLoaderService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = 10;
        }
    }
}
