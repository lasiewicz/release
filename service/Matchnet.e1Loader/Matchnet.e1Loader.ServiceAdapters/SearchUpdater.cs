using System;
using System.Messaging;
using System.Text;
using System.Threading;

using Matchnet.Exceptions;
using Matchnet.Member.ValueObjects;

namespace Matchnet.e1Loader.ServiceAdapters
{
    public class SearchUpdater
    {
        private const Int32 QUEUE_RECEIVE_TIMEOUT = 1000;

        private string _queuePath = @".\private$\SearchUpdate";
        private MessageQueue _queue;
        private Thread _queueReaderThread;
        private bool _isRunning = false;

        public SearchUpdater()
        {
            initializeQueue();
            Start();
        }

        /// <summary>
        /// 
        /// </summary>
        private void initializeQueue()
        {
            if (!MessageQueue.Exists(_queuePath))
            {
                _queue = MessageQueue.Create(_queuePath);
                _queue.Formatter = new BinaryMessageFormatter();
            }
            else
            {
                _queue = new MessageQueue(_queuePath);
            }
        }


        public void Enqueue(SearchUpdate searchUpdate)
        {
            Message message = new Message(searchUpdate, new BinaryMessageFormatter());
            message.Recoverable = true;
            _queue.Send(message);
            message.Dispose();
        }


        public void Start()
        {
            _isRunning = true;

            if (_queueReaderThread == null)
            {
                _queueReaderThread = new Thread(new ThreadStart(workCycle));
                _queueReaderThread.Start();
            }
        }

        public void Purge()
        {
            _queue.Purge();
        }

        private void workCycle()
        {
            Object body = null;
            DateTime lastExceptionTime = DateTime.Now;
            string lastExceptionMessage = string.Empty;

            while (_isRunning)
            {
                try
                {
                    body = null;

                    //System.Diagnostics.Trace.WriteLine("begin dequeue");

                    try
                    {
                        System.Messaging.Message message = _queue.Receive(new TimeSpan(0, 0, 0, 0, QUEUE_RECEIVE_TIMEOUT));
                        message.Formatter = new BinaryMessageFormatter();
                        body = message.Body;
                        message.Dispose();
                    }
                    catch (MessageQueueException mEx)
                    {
                        if (mEx.Message != "Timeout for the requested operation has expired.")
                        {
                            new ServiceBoundaryException(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME, "Error receiving queue item: " + mEx.ToString());
                        }
                    }

                    //System.Diagnostics.Trace.WriteLine("end dequeue");

                    SearchUpdate searchUpdate = body as SearchUpdate;
                    if (searchUpdate != null)
                    {
                        LoaderSA.Instance.ProcessUpdate(searchUpdate);
                    }
                }
                catch (Exception ex)
                {
                    try
                    {
                        if (body != null)
                        {
                            _queue.Formatter = new BinaryMessageFormatter();
                            _queue.Send(body);
                        }
                    }
                    catch (Exception enqueueEx)
                    {
                        new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Enqueue error: " + enqueueEx.ToString());
                    }

                    if (DateTime.Now > lastExceptionTime || ex.Message != lastExceptionMessage)
                    {
                        ServiceBoundaryException sbEx = new ServiceBoundaryException(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure dequeueing member from SearchUpdate queue.", ex);
                        lastExceptionTime = DateTime.Now.AddMinutes(1);
                        lastExceptionMessage = ex.Message;
                    }
                }
            }
        }

    }
}
