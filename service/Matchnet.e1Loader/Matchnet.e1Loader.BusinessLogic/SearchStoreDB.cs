using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

using Matchnet.Data;
using Matchnet.e1.ValueObjects;
using Matchnet.Exceptions;

namespace Matchnet.e1Loader.BusinessLogic
{
    public class SearchStoreDB
    {
        public static DataTable GetMember(Int32 memberID, Int32 communityID)
        {
            try
            {
                Command command = new Command("mnSearchStore", "dbo.up_SearchStore_Get_Member", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                return Client.Instance.ExecuteDataTable(command);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME, "Error reading mnSearchStore.", ex);
            }
        }

        public static void SaveMemberEmailCount(Int32 memberID, Int32 communityID, Int32 emailCount)
        {
            try
            {
                Command command = new Command("mnSearchStore", "dbo.up_SearchStore_Save_EmailCount", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                command.AddParameter("@EmailCount", SqlDbType.Int, ParameterDirection.Input, emailCount);
                command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME, "Error writing mnSearchStore.", ex, memberID);
            }
        }

        public static void SaveMemberLastActiveDate(Int32 memberID, Int32 communityID, DateTime lastActiveDate)
        {
            try
            {
                Command command = new Command("mnSearchStore", "dbo.up_SearchStore_Save_LastActiveDate", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                command.AddParameter("@LastActiveDate", SqlDbType.DateTime, ParameterDirection.Input, lastActiveDate);
                command.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME, "Error writing mnSearchStore.", ex, memberID);
            }
        }

        public static void SaveMember(E1Member e1Member)
        {
            try
            {
                if (e1Member.BirthDate == DateTime.MinValue || e1Member.RegisterDate == DateTime.MinValue || e1Member.ActiveDate == DateTime.MinValue
                    || e1Member.BirthDate == DateTime.MaxValue || e1Member.RegisterDate == DateTime.MaxValue || e1Member.ActiveDate == DateTime.MaxValue)
                {
                    return;
                }

                Command commandSearchStore = new Command("mnSearchStore", "dbo.up_SearchStore_Save", 0);
                commandSearchStore.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, e1Member.MemberID);
                commandSearchStore.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, e1Member.CommunityID);
                commandSearchStore.AddParameter("@GenderMask", SqlDbType.Int, ParameterDirection.Input, e1Member.GenderMask);
                commandSearchStore.AddParameter("@BirthDate", SqlDbType.DateTime, ParameterDirection.Input, e1Member.BirthDate);
                commandSearchStore.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, e1Member.RegisterDate);
                commandSearchStore.AddParameter("@LastActiveDate", SqlDbType.DateTime, ParameterDirection.Input, e1Member.ActiveDate);
                commandSearchStore.AddParameter("@HasPhotoFlag", SqlDbType.Bit, ParameterDirection.Input, e1Member.HasPhoto == Matchnet.Search.Interfaces.UpdateFlag.SetYes);
                commandSearchStore.AddParameter("@Height", SqlDbType.Int, ParameterDirection.Input, e1Member.Height);
                commandSearchStore.AddParameter("@Longitude", SqlDbType.Decimal, ParameterDirection.Input, e1Member.Longitude);
                commandSearchStore.AddParameter("@Latitude", SqlDbType.Decimal, ParameterDirection.Input, e1Member.Latitude);
                commandSearchStore.AddParameter("@Depth1RegionID", SqlDbType.Int, ParameterDirection.Input, e1Member.Country);
                commandSearchStore.AddParameter("@Depth2RegionID", SqlDbType.Int, ParameterDirection.Input, e1Member.State);
                commandSearchStore.AddParameter("@Depth3RegionID", SqlDbType.Int, ParameterDirection.Input, e1Member.City);
                commandSearchStore.AddParameter("@Depth4RegionID", SqlDbType.Int, ParameterDirection.Input, e1Member.Zip);
                commandSearchStore.AddParameter("@AreaCode", SqlDbType.Char, ParameterDirection.Input, e1Member.AreaCode);
                commandSearchStore.AddParameter("@EmailCount", SqlDbType.Int, ParameterDirection.Input, e1Member.EmailCount);
                commandSearchStore.AddParameter("@Weight", SqlDbType.Int, ParameterDirection.Input, e1Member.Weight);
                commandSearchStore.AddParameter("@ChildrenCount", SqlDbType.Int, ParameterDirection.Input, e1Member.ChildrenCount);

                foreach (SearchMemberColumn column in E1Member.TagNames)
                {
                    string attributeName = column.ToString();
                    try
                    {
                        if (e1Member.Tags[attributeName] != null)
                        {
                            commandSearchStore.AddParameter("@" + attributeName, E1Member.TagTypes[column], ParameterDirection.Input, e1Member.Tags[attributeName]);
                        }
                    }
                    catch (Exception ex) { ex = null; }
                }

                commandSearchStore.AddParameter("@UpdateDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
                Client.Instance.ExecuteAsyncWrite(commandSearchStore);
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME, "Error writing mnSearchStore.", ex, e1Member.MemberID);
            }
        }

        public static void DeleteMember(Int32 memberID, Int32 communityID)
        {
            try
            {
                Command command = new Command("mnSearchStore", "dbo.up_SearchStore_Delete", 0);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, communityID);
                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME, "Error deleting from mnSearchStore.", ex, memberID);
            }
        }
    }
}
