using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.e1.ValueObjects;
using Matchnet.FAST.Push.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Search.Interfaces;
using Matchnet.Exceptions;

namespace Matchnet.e1Loader.BusinessLogic
{
    public class MemberHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="communityID"></param>
        /// <returns>An E1Member containing the member's data, if the member satisfies the searchability criteria.
        /// Otherwise, null. </returns>
        public static E1Member GetE1Member(Int32 memberID, Int32 communityID, out string exclusionReason)
        {
         
                bool ignoreCache = false;
                try
                {
                    string s = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("E1LOADERSVC_IGNORE_MEMBER_CACHE", communityID);
                    ignoreCache=Conversion.CBool(s);
                }
                catch (Exception ex){
                    ignoreCache = false;
                }

                MemberLoadFlags memberLoadFlag = (ignoreCache) ? MemberLoadFlags.IngoreSACache : MemberLoadFlags.None;
                Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, memberLoadFlag);

                if (!isExcluded(member, communityID, out exclusionReason))
                {
                    return getE1Member(member, communityID);
                }
                else
                {
                    new BLException("GetE1Member() Returning null. MemberID:" + memberID +
                        "CommunityID:" + communityID + ".Exclusion reson " + exclusionReason);

                    return null;
                }
           
        }

        /// <summary>
        /// Determines whether a member should be searchable.
        /// </summary>
        /// <param name="member"></param>
        /// <param name="communityID"></param>
        /// <returns></returns>
        private static bool isExcluded(Matchnet.Member.ServiceAdapters.Member member, Int32 communityID, out string exclusionReason)
        {
            try
            {
                exclusionReason = string.Empty;

                if (member.EmailAddress == null || member.EmailAddress.Length == 0)
                {
                    exclusionReason = "Member e-mail address was null or zero-length.";
                    return true;
                }

                ExclusionRule rule = new ExclusionRule(communityID);

                List<string> evidenceAttributes = new List<string>();
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_GLOBALSTATUSMASK);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_HIDEMASK);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_SELFSUSPENDEDFLAG);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_REGIONID);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_GENDERMASK);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_BIRTHDATE);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_HEADLINE);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_IDEALRELATIONSHIPESSAY);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_IDEALVACATION);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_LEARNFROMTHEPASTESSAY);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_OCCUPATIONDESCRIPTION);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_PERFECTFIRSTDATEESSAY);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_PERFECTMATCHESSAY);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_STUDIESEMPHASIS);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_GREWUPIN);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_ESSAYDESCRIBEHUMOR);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_ESSAYFIRSTTHINGNOTICEABOUTYOU);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_ESSAYFRIENDSDESCRIPTION);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_ESSAYORGANIZATIONS);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_FAVORITEALBUMSONGARTIST);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_FAVORITEBOOKAUTHOR);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_FAVORITEMOVIEDIRECTORACTOR);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_ABOUTME);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_ABOUTMYAPPEARANCEESSAY);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_WHENIHAVETIME);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_FACEPAGEINTRODUCTION);
                evidenceAttributes.Add(VOConstants.ATTRIBUTENAME_CANNOTDOWITHOUT);

                //This is a special case because we need to call HasApprovedPhoto() instead of looking at HasPhotoFlag attribute
                rule.AddEvidence(new ExclusionRule.ExclusionEvidence(VOConstants.ATTRIBUTENAME_HASPHOTOFLAG, member.HasApprovedPhoto(communityID) ? 1 : 0));

                Attributes attributes = AttributeMetadataSA.Instance.GetAttributes();

                foreach (string attributeName in evidenceAttributes)
                {
                    Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = attributes.GetAttribute(attributeName);
                    Object attributeValue = null;

                    switch (attribute.DataType)
                    {
                        case DataType.Bit:
                        case DataType.Mask:
                        case DataType.Number:
                            attributeValue = member.GetAttributeInt(communityID, Constants.NULL_INT, Constants.NULL_INT, attribute.ID, Constants.NULL_INT);
                            break;
                        case DataType.Date:
                            attributeValue = member.GetAttributeDate(communityID, Constants.NULL_INT, Constants.NULL_INT, attribute.ID, DateTime.MinValue);
                            break;
                        case DataType.Text:
                            TextStatusType textStatus = TextStatusType.None;
                            attributeValue = member.GetAttributeText(communityID, Constants.NULL_INT, Constants.NULL_INT, (Int32)Language.English, attribute.ID, null, out textStatus); //TOFIX: hard-coded language

                            if (textStatus != TextStatusType.Auto && textStatus != TextStatusType.Human)
                            {
                                attributeValue = null;
                            }

                            break;
                    }

                    if (rule.AddEvidence(new ExclusionRule.ExclusionEvidence(attributeName, attributeValue)))
                    {
                        exclusionReason = rule.ExclusionReason;
                        return true;
                    }
                }

                if (rule.IsExcluded)
                {
                    exclusionReason = rule.ExclusionReason;
                }

                return rule.IsExcluded;
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().IndexOf("attribute group not found") >= 0)
                {
                    exclusionReason = ex.Message;
                    return true;
                }
                throw ex;
            }
        }

        private static E1Member getE1Member(Matchnet.Member.ServiceAdapters.Member member, Int32 communityID)
        {
            E1Member e1Member = new E1Member();

            e1Member.MemberID = member.MemberID;
            e1Member.CommunityID = (Byte)communityID;
            e1Member.GenderMask = (Byte)member.GetAttributeInt(communityID, 0, 0, "GenderMask", Constants.NULL_INT);
            e1Member.BirthDate = member.GetAttributeDate(communityID, 0, 0, "BirthDate", DateTime.MinValue);
            e1Member.RegisterDate = getRegisterDate(member, communityID);
            e1Member.ActiveDate = member.GetLastLogonDate(communityID);
            e1Member.HasPhoto = member.HasApprovedPhoto(communityID) ? UpdateFlag.SetYes : UpdateFlag.SetNo;
            e1Member.Height = member.GetAttributeInt(communityID, 0, 0, "Height", 0);
            e1Member.AreaCode = getAreaCode(member.GetAttributeInt(communityID, 0, 0, "RegionID", Constants.NULL_INT));
            e1Member.EmailCount = member.GetAttributeInt(communityID, 0, 0, "CommunityEmailCount", 0);
            e1Member.Weight = member.GetAttributeInt(communityID, 0, 0, "Weight", 0);
            e1Member.ChildrenCount = member.GetAttributeInt(communityID, 0, 0, "ChildrenCount", 0);
           
            setMemberLocation(e1Member, member);

            foreach (SearchMemberColumn column in E1Member.TagNames)
            {
                String attributeName = column.ToString();
                if (attributeName.ToLower() == "colorcode")
                {
                    try
                    {
                        String value = getAttributeAsString(member, communityID, "colorcodeprimarycolor");
                        setMemberColorCode(e1Member, member, value);
                    }
                    catch (Exception ex) { ex = null; }
                }
                else
                {
                    String value = getAttributeAsString(member, communityID, attributeName);
                    e1Member.Tags.Add(attributeName, value);
                }
            }

            return e1Member;
        }

        private static string getAreaCode(Int32 regionID)
        {
            string areaCode = string.Empty;

            RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, (Int32)Language.English);
            Int32 cityRegionID = regionLanguage.CityRegionID;

            RegionAreaCodeDictionary dictionary = RegionSA.Instance.RetrieveAreaCodesByRegion();

            if (dictionary.Contains(cityRegionID))
            {
                ArrayList areaCodes = dictionary[cityRegionID];

                if (areaCodes.Count == 1)
                {
                    areaCode = areaCodes[0].ToString();
                }
                else
                {
                    //If more than one, use lowest area code in region since user can only have one
                    Int32 maxAreaCode = 0;

                    foreach (Int32 areaCodeNum in areaCodes)
                    {
                        if (areaCodeNum > maxAreaCode)
                        {
                            maxAreaCode = areaCodeNum;
                        }
                    }

                    areaCode = maxAreaCode.ToString();
                }
            }

            return areaCode;
        }

        private static DateTime getRegisterDate(Matchnet.Member.ServiceAdapters.Member member, Int32 communityID)
        {
            DateTime registerDate = DateTime.MaxValue;
            foreach (Brand brand in BrandConfigSA.Instance.GetBrandsByCommunity(communityID))
            {
                DateTime brandInsertDate = member.GetAttributeDate(brand, "BrandInsertDate");
                if (brandInsertDate < registerDate && brandInsertDate != DateTime.MinValue)
                {
                    registerDate = brandInsertDate;
                }
            }

            return registerDate;
        }

        private static String getAttributeAsString(Matchnet.Member.ServiceAdapters.Member member, Int32 communityID, String attributeName)
        {
            String value = null;
            DataType dataType = AttributeMetadataSA.Instance.GetAttributes().GetAttribute(attributeName).DataType;

            switch (dataType)
            {
                case DataType.Bit:
                case DataType.Mask:
                case DataType.Number:
                    Int32 intValue = member.GetAttributeInt(communityID, 0, 0, attributeName, Constants.NULL_INT);
                    if (intValue != Constants.NULL_INT)
                    {
                        value = intValue.ToString();
                    }
                    break;
                case DataType.Date:
                    DateTime dtValue;

                    if (attributeName == "SubscriptionExpirationDate")
                    {
                        dtValue = getSubscriptionExpirationDate(member, communityID);
                    }
                    else
                    {
                        dtValue = member.GetAttributeDate(communityID, Constants.NULL_INT, Constants.NULL_INT, attributeName, DateTime.MinValue);
                    }

                    if (dtValue != DateTime.MinValue)
                    {
                        value = dtValue.ToString(); ;
                    }                    
                    break;
                case DataType.Text:
                    TextStatusType textStatus;
                    value = member.GetAttributeText(communityID, Constants.NULL_INT, Constants.NULL_INT, (Int32)Language.English, attributeName, string.Empty, out textStatus);//TODO: hard-coded Language
                    if (textStatus == TextStatusType.Pending || textStatus == TextStatusType.None) //don't index unapproved text attributes
                    {
                        value = null;
                    }
                    break;
            }

            return value;
        }

        /// <summary>
        /// The SubscriptionExpirationDate attribute is defined at the site scope.
        /// This function looks at all sites that a member belongs to in a particular community
        /// and returns the greatest SubscriptionExpirationDate for that community.
        /// </summary>
        /// <param name="member"></param>
        /// <param name="communityID"></param>
        /// <returns></returns>
        private static DateTime getSubscriptionExpirationDate(Matchnet.Member.ServiceAdapters.Member member, Int32 communityID)
        {
            DateTime communitySubscriptionExpirationDate = DateTime.MinValue;

            foreach (Int32 siteID in member.GetSiteIDList())
            {
                foreach (Site site in BrandConfigSA.Instance.GetSites())
                {
                    if (siteID == site.SiteID && site.Community.CommunityID == communityID)
                    {
                        DateTime siteSubscriptionExpirationDate = member.GetAttributeDate(communityID, siteID, Constants.NULL_INT, "SubscriptionExpirationDate", DateTime.MinValue);
                        if (siteSubscriptionExpirationDate > communitySubscriptionExpirationDate)
                        {
                            communitySubscriptionExpirationDate = siteSubscriptionExpirationDate;
                        }
                    }
                }
            }

            return communitySubscriptionExpirationDate;
        }

        private static void setMemberLocation(E1Member e1Member, Matchnet.Member.ServiceAdapters.Member member)
        {
            //note: hard-coded Language does not matter here since we're not using any text fields of the RegionLanguage object
            RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(member.GetAttributeInt(Constants.NULL_INT, Constants.NULL_INT, Constants.NULL_INT, "RegionID", Constants.NULL_INT), (Int32)Language.English);

            e1Member.Longitude = (float)regionLanguage.Longitude;
            e1Member.Latitude = (float)regionLanguage.Latitude;
            e1Member.Country = regionLanguage.CountryRegionID.ToString();
            e1Member.State = regionLanguage.StateRegionID.ToString();
            e1Member.City = regionLanguage.CityRegionID.ToString();
            e1Member.Zip = regionLanguage.PostalCodeRegionID.ToString();
        }

        private static void setMemberColorCode(E1Member e1Member, Matchnet.Member.ServiceAdapters.Member member, string attributeText)
        {
           
            int e1ColorCode=0;
            if (!String.IsNullOrEmpty(attributeText))
            {
                switch (attributeText)
                {
                    case "none":
                        e1ColorCode = 0;
                        break;
                    case "white":
                        e1ColorCode = 1;
                        break;
                    case "blue":
                        e1ColorCode = 2;
                        break;
                    case "yellow":
                        e1ColorCode = 4;
                        break;
                    case "red":
                        e1ColorCode = 8;
                        break;
                }
                if(e1ColorCode > 0)
                    e1Member.Tags.Add("ColorCode", e1ColorCode.ToString());
            }
        }
    }
}
