using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Threading;

using Matchnet.Caching;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Data;
using e1SA= Matchnet.e1.ServiceAdapters;
using Matchnet.e1.ValueObjects;
using Matchnet.e1Loader.ServiceAdapters;
using Matchnet.e1Loader.ValueObjects;
using Matchnet.Exceptions;
using Matchnet.Member.ValueObjects;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;
namespace Matchnet.e1Loader.BusinessLogic
{
    public class LoaderBL
    {
        #region Events
        public delegate void OutboundQueueChangedEventHandler(Int32 delta, SearchUpdateType updateType);
        public event OutboundQueueChangedEventHandler OutboundQueueChanged;

        public delegate void MemberUpdatedEventHandler(SearchUpdateType updateType);
        public event MemberUpdatedEventHandler MemberUpdated;

        public delegate void ReplicationEventHandler(IReplicationAction replicationAction);
        public event ReplicationEventHandler ReplicationRequested;
        #endregion

        private const Int32 ENGINE_QUEUE_SIZE = 10000;
        private const Int32 MAX_RETRY_COUNT = 10;
        private const Int32 RETRY_INTERVAL_SECONDS = 60;

        private EngineCollection _engines = new EngineCollection();
        private Dictionary<string, E1UpdateQueue> _engineQueues = new Dictionary<string, E1UpdateQueue>();
        private Dictionary<string, E1UpdateCollection> _engineBatches = new Dictionary<string, E1UpdateCollection>();
        private Int32 _batchSize = 1; //Not batching updates since we're not bulk loading from this service
        private Queue<FailedSearchUpdate> _failedUpdates = new Queue<FailedSearchUpdate>();
        private Thread _failedUpdateWorkerThread;

        #region Constructor

        public static LoaderBL Instance = new LoaderBL();

        private LoaderBL()
        {
        }

        #endregion

        private void QueueReader_UpdateDequeued(SearchUpdate searchUpdate)
        {
            ProcessUpdate(searchUpdate);
        }

        public void AddServer(string serverName)
        {
            AddServer(serverName, true);
        }

        public void AddServer(string serverName, bool replicate)
        {
            serverName = serverName.ToLower();

            if (!_engines.Contains(serverName))
            {
                _engineQueues.Add(serverName, new E1UpdateQueue(serverName, ENGINE_QUEUE_SIZE, Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME));
                _engineQueues[serverName].MemberEnqueued += new E1UpdateQueue.MemberEnQueuedEventHandler(memberEnqueued);
                _engineQueues[serverName].MemberDequeued += new E1UpdateQueue.MemberDeQueuedEventHandler(memberDequeued);

                _engineBatches.Add(serverName, new E1UpdateCollection(_batchSize));

                _engines.Add(new Engine(serverName));
                
                if (replicate)
                {
                    ReplicationRequested(new EngineAction(serverName, EngineActionType.Add));
                }
            }
        }

        public void RemoveServer(string serverName)
        {
            RemoveServer(serverName, true);
        }

        public void RemoveServer(string serverName, bool replicate)
        {
            serverName = serverName.ToLower();

            if (_engines.Contains(serverName))
            {
                _engineQueues[serverName].Stop();
                _engineQueues.Remove(serverName);
                _engineBatches.Remove(serverName);
                _engines.Remove(serverName);

                if (replicate)
                {
                    ReplicationRequested(new EngineAction(serverName, EngineActionType.Remove));
                }
            }
        }

        public void StartOutboundQueue(string serverName)
        {
            StartOutboundQueue(serverName, true);
        }

        public void StartOutboundQueue(string serverName, bool replicate)
        {
            serverName = serverName.ToLower();

            if (_engines.Contains(serverName) && !_engines[serverName].UpdatesEnabled)
            {
                _engineQueues[serverName].Start();

                _engines[serverName].UpdatesEnabled = true;

                if (replicate)
                {
                    ReplicationRequested(new EngineAction(serverName, EngineActionType.Start));
                }
            }
        }

        public void StopOutboundQueue(string serverName)
        {
            StopOutboundQueue(serverName, true);
        }

        public void StopOutboundQueue(string serverName, bool replicate)
        {
            serverName = serverName.ToLower();

            if (_engines.Contains(serverName) && _engines[serverName].UpdatesEnabled)
            {
                _engineQueues[serverName].Stop();

                _engines[serverName].UpdatesEnabled = false;

                if (replicate)
                {
                    ReplicationRequested(new EngineAction(serverName, EngineActionType.Stop));
                }
            }
        }

        public bool ProcessUpdate(SearchUpdate searchUpdate)
        {
            return ProcessUpdate(searchUpdate, true);
        }

        public bool ProcessUpdate(SearchUpdate searchUpdate, bool withRetry)
        {
            E1Member e1Member = null;


            //hack - this community throws tonns of errors on stage so we cannot test
            if (searchUpdate.CommunityID == 20)
                return true;
            switch (searchUpdate.UpdateType)
            {
                case SearchUpdateType.Update:
                    string exclusionReason;
                    e1Member = MemberHelper.GetE1Member(searchUpdate.MemberID, searchUpdate.CommunityID, out exclusionReason);

                    if (e1Member == null)  //This means the member did not meet the searchable criteria
                    {
                        if (withRetry)
                        {
                            processUpdateWithRetry(searchUpdate, exclusionReason);
                        }
                        return false;
                    }
                    else
                    {
                        SearchStoreDB.SaveMember(e1Member);
                    }
                    break;
                case SearchUpdateType.UpdateEmailCount:
                    SearchStoreDB.SaveMemberEmailCount(searchUpdate.MemberID, searchUpdate.CommunityID, searchUpdate.EmailCount);
                    break;
                case SearchUpdateType.UpdateLastActiveDate:
                    SearchStoreDB.SaveMemberLastActiveDate(searchUpdate.MemberID, searchUpdate.CommunityID, searchUpdate.LastActiveDate);
                    break;
                case SearchUpdateType.UpdateHasPhoto:
                    searchUpdate.UpdateType = SearchUpdateType.Update;
                   //e2 will be updated on the second iteration
                    return ProcessUpdate(searchUpdate);
                case SearchUpdateType.Remove:
                    SearchStoreDB.DeleteMember(searchUpdate.MemberID, searchUpdate.CommunityID);
                    break;
                //No DB update needed for SearchUpdateType.UpdateIsOnline
            }
            
            foreach (E1UpdateQueue updateQueue in _engineQueues.Values)
            {
                if (!updateQueue.IsFull)
                {
                    updateQueue.Enqueue(new E1Update(e1Member, searchUpdate));
                }
            }

            return true;
        }

        /// <summary>
        /// This method will repeatedly try an update that previously failed due one of the exclusion rules.
        /// Normally updates of type SearchUpdateType.Update should not result in a removal, so we want to retry these
        /// a few times to make sure we have the latest member data before excluding the member.
        /// </summary>
        /// <param name="searchUpdate"></param>
        private void processUpdateWithRetry(SearchUpdate searchUpdate, string exclusionReason)
        {
            System.Diagnostics.Trace.WriteLine("MemberID " + searchUpdate.MemberID + " not searchable.  " + exclusionReason);
            _failedUpdates.Enqueue(new FailedSearchUpdate(searchUpdate, 0, exclusionReason));

            if (_failedUpdateWorkerThread == null)
            {
                _failedUpdateWorkerThread = new Thread(new ThreadStart(failedUpdateWorkCycle));
                _failedUpdateWorkerThread.Start();
            }
        }

        private void failedUpdateWorkCycle()
        {
            while (_failedUpdateWorkerThread.IsAlive)
            {
                try
                {
                    while (_failedUpdates.Count == 0)
                    {
                        Thread.Sleep(1000);
                    }

                    FailedSearchUpdate failedSearchUpdate = _failedUpdates.Dequeue();

                    if (DateTime.Now.Subtract(failedSearchUpdate.LastAttemptTime).TotalSeconds < RETRY_INTERVAL_SECONDS)
                    {
                        _failedUpdates.Enqueue(failedSearchUpdate);
                    }
                    else
                    {
                        SearchUpdate searchUpdate = failedSearchUpdate.SearchUpdate;

                        if (!ProcessUpdate(searchUpdate, false))
                        {
                            //System.Diagnostics.Trace.WriteLine("MemberID " + searchUpdate.MemberID + " not searchable.  " + failedSearchUpdate.ExclusionReason);
                            failedSearchUpdate.LastAttemptTime = DateTime.Now;
                            failedSearchUpdate.RetryCount++;

                            if (failedSearchUpdate.RetryCount == MAX_RETRY_COUNT)
                            {
                                System.Diagnostics.Trace.WriteLine("Retries failed.  Removing MemberID " + searchUpdate.MemberID + ".");
                                searchUpdate.UpdateType = SearchUpdateType.Remove;
                                ProcessUpdate(searchUpdate);
                            }
                            else
                            {
                                _failedUpdates.Enqueue(failedSearchUpdate);
                            }
                        }
                        else
                        {
                            System.Diagnostics.Trace.WriteLine("Retry succeeded.  MemberID " + searchUpdate.MemberID + ".");
                        }
                    }

                    Thread.Sleep(10); //don't let this thread steal all cycles
                }
                catch (Exception ex)
                {
                    new ServiceBoundaryException(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME, "Error in failedUpdateWorkCycle().", ex);
                }
            }
        }

        public void PlayReplicationAction(IReplicationAction replicationAction)
        {
            EngineAction engineAction = replicationAction as EngineAction;

            switch (engineAction.EngineActionType)
            {
                case EngineActionType.Add:
                    AddServer(engineAction.ServerName, false);
                    break;
                case EngineActionType.Remove:
                    RemoveServer(engineAction.ServerName, false);
                    break;
                case EngineActionType.Start:
                    StartOutboundQueue(engineAction.ServerName, false);
                    break;
                case EngineActionType.Stop:
                    StopOutboundQueue(engineAction.ServerName, false);
                    break;
            }
        }

        /// <summary>
        /// Return list of engines known by this loader and whether real-time updates are currently enabled for those engines.
        /// </summary>
        /// <returns></returns>
        public EngineCollection GetEngineStatus()
        {
            return _engines;
        }

        /// <summary>
        /// Retrieve a list of running engines from replication partner.
        /// </summary>
        public void InitializeEngineStatus(string overrideHostName)
        {
            try
            {
                EngineCollection engineCollection = LoaderSA.Instance.GetEngineStatus(overrideHostName);

                foreach (DictionaryEntry de in engineCollection)
                {
                    Engine engine = de.Value as Engine;
                    AddServer(engine.ServerName);
                    if (engine.UpdatesEnabled)
                    {
                        StartOutboundQueue(engine.ServerName);
                    }
                }
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME, "Cannot retrieve engine status from replication partner.", ex);
            }
        }

        /// <summary>
        /// For debugging -- returns a member's info directly from the SearchStore DB
        /// </summary>
        /// <returns></returns>
        public Hashtable GetDBMember(Int32 memberID, Int32 communityID)
        {
            Hashtable retval = new Hashtable();

            DataTable dt = SearchStoreDB.GetMember(memberID, communityID);

            if (dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];

                DataColumn[] columns = new DataColumn[dt.Columns.Count];
                dt.Columns.CopyTo(columns, 0);
                Array.Sort<DataColumn>(columns, compareDataColumns);

                foreach (DataColumn column in columns)
                {
                    retval.Add(column.ColumnName, row[column]);
                }
            }
            else
            {
                retval.Add("MemberID", memberID);
                retval.Add("CommunityID", communityID);
                retval.Add("Error", "Not found in mnSearchStore.");
            }

            return retval;
        }

        private static int compareDataColumns(DataColumn column1, DataColumn column2)
        {
            return column1.ColumnName.CompareTo(column2.ColumnName);
        }

        private void memberEnqueued(E1UpdateQueueEventArgs args)
        {
            SearchUpdateType updateType = SearchUpdateType.Update;

            if (args.E1Update.SearchUpdate != null)
            {
                updateType = args.E1Update.SearchUpdate.UpdateType;
            }

            OutboundQueueChanged(1, updateType);
        }

        private void memberDequeued(E1UpdateQueueEventArgs args)
        {
            SearchUpdateType updateType = SearchUpdateType.Update;

            if (args.E1Update.SearchUpdate != null)
            {
                updateType = args.E1Update.SearchUpdate.UpdateType;
            }

            OutboundQueueChanged(-1, updateType);
            MemberUpdated(updateType);

            string serverName = args.QueueName;

            E1UpdateCollection e1UpdateCollection = _engineBatches[serverName];

            lock (e1UpdateCollection)
            {
                e1UpdateCollection.Add(args.E1Update);
            }

            if (e1UpdateCollection.Count >= _batchSize)
            {
                lock (e1UpdateCollection)
                {
               
                    e1SA.EngineSA.Instance.LoadMembers(e1UpdateCollection, serverName);
                    e1UpdateCollection.Clear();
                }
            }
        }

    }
}
