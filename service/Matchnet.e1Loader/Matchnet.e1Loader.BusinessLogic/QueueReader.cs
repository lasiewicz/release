using System;
using System.Collections.Generic;
using System.Messaging;
using System.Text;
using System.Threading;

using Matchnet.e1.ValueObjects;
using Matchnet.Exceptions;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.MembersOnline.ServiceAdapters;

namespace Matchnet.e1Loader.BusinessLogic
{
    /// <summary>
    /// Reads memberIDs from SearchUpdate queues populated by Member service and feeds
    /// corresponding member data into the Loader
    /// </summary>
    class QueueReader
    {
        public delegate void UpdateDequeuedEventHandler(SearchUpdate searchUpdate);
        public event UpdateDequeuedEventHandler UpdateDequeued;

        private string _queuePath;
        private MessageQueue _queue;
        private Thread _queueReaderThread;
        private bool _isRunning = false;

        public QueueReader(string queuePath)
        {
            _queuePath = queuePath;
            initializeQueue();
        }

        /// <summary>
        /// Populate _queues with a MessageQueue for each community
        /// </summary>
        private void initializeQueue()
        {
            if (!MessageQueue.Exists(_queuePath))
            {
                _queue = MessageQueue.Create(_queuePath);
                _queue.MaximumQueueSize = 10240; //TODO: This value was stolen from Matchnet.Replication.  What should it be here?
            }
            else
            {
                _queue = new MessageQueue(_queuePath);
            }
        }

        public void Start()
        {
            _isRunning = true;

            if (_queueReaderThread == null)
            {
                _queueReaderThread = new Thread(new ThreadStart(workCycle));
                _queueReaderThread.Start();
            }
        }

        public void Purge()
        {
            _queue.Purge();
        }

        private void workCycle()
        {
            Object body = null;
            DateTime lastExceptionTime = DateTime.Now;
            string lastExceptionMessage = string.Empty;

            while (_isRunning)
            {
                try
                {
                    body = null;

                    System.Diagnostics.Trace.WriteLine("begin dequeue");

                    try
                    {
                        System.Messaging.Message message = _queue.Receive(new TimeSpan(0, 0, 0, 0, 1000));
                        message.Formatter = new BinaryMessageFormatter();
                        body = message.Body;
                        message.Dispose();
                    }
                    catch (MessageQueueException mEx)
                    {
                        if (mEx.Message != "Timeout for the requested operation has expired.")
                        {
                            System.Diagnostics.EventLog.WriteEntry(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME, "Error receiving queue item: " + mEx.ToString());
                        }
                    }

                    System.Diagnostics.Trace.WriteLine("end dequeue");

                    SearchUpdate searchUpdate = body as SearchUpdate;
                    if (searchUpdate != null)
                    {
                        UpdateDequeued(searchUpdate);
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.EventLog.WriteEntry(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure dequeueing member from SearchUpdate queue: " + ex.ToString());

                    //TODO: Review with Garry -- should we requeue here if we fail to load the member?
                    /*try
                    {
                        if (body != null)
                        {
                            queue.Formatter = new BinaryMessageFormatter();
                            queue.Send(body);
                        }
                    }
                    catch (Exception enqueueEx)
                    {
                        new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Enqueue error: " + enqueueEx.ToString());
                    }*/

                    if (DateTime.Now > lastExceptionTime || ex.Message != lastExceptionMessage)
                    {
                        ServiceBoundaryException sbEx = new ServiceBoundaryException(Matchnet.e1Loader.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure dequeueing member from SearchUpdate queue.", ex);
                        lastExceptionTime = DateTime.Now.AddMinutes(1);
                        lastExceptionMessage = ex.Message;
                    }
                }
            }
        }
    }
}
