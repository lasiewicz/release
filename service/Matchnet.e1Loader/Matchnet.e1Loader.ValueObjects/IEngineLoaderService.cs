using System;
using System.Collections;
using System.Text;

using Matchnet.Member.ValueObjects;
using Matchnet.Search.Interfaces;

namespace Matchnet.e1Loader.ValueObjects
{
    public interface IEngineLoaderService
    {
        void AddServer(string serverName);
        void RemoveServer(string serverName);
        void StartOutboundQueue(string serverName);
        void StopOutboundQueue(string serverName);
        void ProcessUpdate(SearchUpdate searchUpdate);
        EngineCollection GetEngineStatus();
        Hashtable GetDBMember(Int32 memberID, Int32 communityID);
    }
}
