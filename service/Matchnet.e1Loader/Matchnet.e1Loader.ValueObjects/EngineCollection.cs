using System;
using System.Collections;
using System.Text;

namespace Matchnet.e1Loader.ValueObjects
{
    [Serializable]
    public class EngineCollection : IValueObject, ICacheable, IReplicable, IEnumerable
    {
        private const string CACHE_KEY = "EngineCollection";

        private Hashtable _engines;

        private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Normal;
        private int _cacheTTLSeconds = 3600;

        public EngineCollection()
        {
            _engines = new Hashtable();
        }

        public void Add(Engine engine)
        {
            _engines.Add(engine.ServerName, engine);
        }

        public void Remove(string serverName)
        {
            _engines.Remove(serverName);
        }

        public bool Contains(string serverName)
        {
            return _engines.ContainsKey(serverName);
        }

        public Engine this[string serverName]
        {
            get
            {
                return _engines[serverName] as Engine;
            }
        }

        
        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return _engines.GetEnumerator();
        }

        #endregion

        #region ICacheable Members

        public CacheItemMode CacheMode
        {
            get
            {
                return CacheItemMode.Absolute;
            }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _cachePriority;
            }
            set
            {
                _cachePriority = value;
            }
        }

        public int CacheTTLSeconds
        {
            get
            {
                return _cacheTTLSeconds;
            }
            set
            {
                _cacheTTLSeconds = value;
            }
        }

        public string GetCacheKey()
        {
            return CACHE_KEY;
        }

        #endregion

        #region IReplicable Members

        public ReplicationPlaceholder GetReplicationPlaceholder()
        {
            return new ReplicationPlaceholder(GetCacheKey());
        }

        #endregion
    }
}
