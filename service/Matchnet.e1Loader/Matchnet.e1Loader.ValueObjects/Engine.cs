using System;
using System.Text;

namespace Matchnet.e1Loader.ValueObjects
{
    [Serializable]
    public class Engine : IValueObject
    {
        private string _serverName;
        private bool _updatesEnabled = false;

        public Engine(string serverName)
        {
            _serverName = serverName;
        }

        public string ServerName
        {
            get
            {
                return _serverName;
            }
        }

        public bool UpdatesEnabled
        {
            get
            {
                return _updatesEnabled;
            }
            set
            {
                _updatesEnabled = value;
            }
        }

        public override string ToString()
        {
            return _serverName;
        }
    }
}
