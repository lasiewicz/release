using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.e1Loader.ValueObjects
{
    [Serializable]
    public class EngineAction : IReplicationAction
    {
        private string _serverName;
        private EngineActionType _actionType;

        public EngineAction(string serverName, EngineActionType actionType)
        {
            _serverName = serverName;
            _actionType = actionType;
        }

        public string ServerName
        {
            get
            {
                return _serverName;
            }
        }

        public EngineActionType EngineActionType
        {
            get
            {
                return _actionType;
            }
        }
    }

    public enum EngineActionType
    {
        Add,
        Remove,
        Start,
        Stop
    }
}
