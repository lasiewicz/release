using System;
using System.Collections.Generic;
using System.Text;

using Matchnet.Member.ValueObjects;

namespace Matchnet.e1Loader.ValueObjects
{
    public class FailedSearchUpdate
    {
        public SearchUpdate SearchUpdate;
        public Int32 RetryCount = 0;
        public DateTime LastAttemptTime = DateTime.Now;
        public string ExclusionReason;

        public FailedSearchUpdate(SearchUpdate searchUpdate, Int32 retryCount, string exclusionReason)
        {
            SearchUpdate = searchUpdate;
            RetryCount = retryCount;
            ExclusionReason = exclusionReason;
        }
    }
}
