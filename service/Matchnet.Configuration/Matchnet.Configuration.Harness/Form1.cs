using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using Matchnet.Configuration.BusinessLogic;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ServiceManagers;

using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.File;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;
using Matchnet.Configuration.ValueObjects.SparkWS;


namespace Matchnet.Configuration.Harness
{
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.RichTextBox txtOutput;
		private System.Windows.Forms.Button button7;
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.txtOutput = new System.Windows.Forms.RichTextBox();
			this.button7 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(8, 8);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			this.button1.Text = "button1";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(8, 32);
			this.button2.Name = "button2";
			this.button2.TabIndex = 1;
			this.button2.Text = "button2";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(8, 56);
			this.button3.Name = "button3";
			this.button3.TabIndex = 2;
			this.button3.Text = "button3";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(8, 80);
			this.button4.Name = "button4";
			this.button4.TabIndex = 3;
			this.button4.Text = "button4";
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(208, 16);
			this.button5.Name = "button5";
			this.button5.TabIndex = 4;
			this.button5.Text = "button5";
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// button6
			// 
			this.button6.Location = new System.Drawing.Point(208, 40);
			this.button6.Name = "button6";
			this.button6.TabIndex = 5;
			this.button6.Text = "button6";
			this.button6.Click += new System.EventHandler(this.button6_Click);
			// 
			// txtOutput
			// 
			this.txtOutput.Location = new System.Drawing.Point(8, 152);
			this.txtOutput.Name = "txtOutput";
			this.txtOutput.Size = new System.Drawing.Size(272, 112);
			this.txtOutput.TabIndex = 6;
			this.txtOutput.Text = "";
			// 
			// button7
			// 
			this.button7.Location = new System.Drawing.Point(88, 8);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(112, 23);
			this.button7.TabIndex = 7;
			this.button7.Text = "Ticket Serialization";
			this.button7.Click += new System.EventHandler(this.button7_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.button7);
			this.Controls.Add(this.txtOutput);
			this.Controls.Add(this.button6);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void Say(object obj)
		{
			txtOutput.AppendText(obj.ToString());
			txtOutput.AppendText("\n");
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
		    Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetMembaseConfigByBucket("webcache");
			/*Matchnet.Configuration.BusinessLogic.LpdBL.Instance.Start();
			ServiceInstanceConfig test = Matchnet.Configuration.BusinessLogic.SettingsBL.Instance.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, "devapp01");
			MessageBox.Show(test.ToString());*/

			/*Matchnet.Configuration.ValueObjects.Lpd.LogicalDatabases ldbs = new Matchnet.Configuration.ValueObjects.Lpd.LogicalDatabases(System.Guid.NewGuid());
			ldbs.AddLogicalDatabase(new Matchnet.Configuration.ValueObjects.Lpd.LogicalDatabase("test", Matchnet.Configuration.ValueObjects.Lpd.HydraModeType.Spray, 1));
			byte[] bytes = ldbs.ToByteArray();
			Matchnet.Configuration.ValueObjects.Lpd.LogicalDatabases ldbs2 = new Matchnet.Configuration.ValueObjects.Lpd.LogicalDatabases(System.Guid.NewGuid());
			ldbs2.FromByteArray(bytes);*/

			Matchnet.Configuration.ValueObjects.Lpd.LogicalDatabases ldbs = Matchnet.Configuration.ServiceAdapters.LpdSA.Instance.GetLogicalDatabases();

			//ServicePartitions sps = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions();
            
			string machineName = Matchnet.InitialConfiguration.InitializationSettings.MachineName;
			Matchnet.Configuration.ValueObjects.ServiceInstanceConfig sic = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig("EMAIL_SVC", machineName);
			MessageBox.Show(sic.ToString());



			/*
			RootGroups rootGroups = FileRootSA.Instance.GetRootGroups();
			Console.WriteLine(((Root)((ArrayList)rootGroups.Groups[1000])[0]).Path);
			Console.WriteLine(((Root)((ArrayList)rootGroups.Groups[1000])[0]).IsParent);
			Console.WriteLine(((Root)((ArrayList)rootGroups.Groups[1000])[1]).Path);
			Console.WriteLine(((Root)((ArrayList)rootGroups.Groups[1000])[1]).IsParent);
			Console.WriteLine(((Root)((ArrayList)rootGroups.Groups[1000])[2]).Path);
			Console.WriteLine(((Root)((ArrayList)rootGroups.Groups[1000])[2]).IsParent);
			Console.WriteLine(((Root)((ArrayList)rootGroups.Groups[1000])[3]).Path);
			Console.WriteLine(((Root)((ArrayList)rootGroups.Groups[1000])[3]).IsParent);
			\\file00.matchnet.com\FileRoot\1000\
			False
			\\file01.matchnet.com\FileRootC\
			True
			\\clbackup1.matchnet.com\Backups\FileRoot\1000\
			False
			\\lafile01\FileRoot\1000\
			False
			*/

			ArrayList a = new ArrayList();
			a.Add(new Root(1001, 1000, false, false, @"\\file00.matchnet.com\FileRoot\1000\", ""));
			a.Add(new Root(1000, 1000, true, false, @"\\file01.matchnet.com\FileRootC\", ""));
			a.Add(new Root(1002, 1000, false, false, @"\\clbackup1.matchnet.com\Backups\FileRoot\1000\", ""));

			a.Sort();

			Console.WriteLine("");

			Console.WriteLine(((Root)a[0]).Path);
			Console.WriteLine(((Root)a[1]).Path);
			Console.WriteLine(((Root)a[2]).Path);

			Console.WriteLine("");
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			ProfileSectionDefinition psd;

			psd = ProfileSectionSA.Instance.GetProfileSectionDefinition(2);
			Say(psd.AttributeDefinitions.Length);
		}

		private void button3_Click(object sender, System.EventArgs e)
		{

			AccessTicket at;
			at = AccessTicketSA.Instance.GetAccessTicket("D4E36B58E6EB4F25852DC76F90894ECA");
			Say(at.GetCacheKey());
			at = AccessTicketSA.Instance.GetAccessTicket("02F3D63058C14469BA587400A0FDC776");
			Say(at.GetCacheKey());
			at = AccessTicketSA.Instance.GetAccessTicket("238711C489714B22A0AA2635AEAAA9A0");
			Say(at.GetCacheKey());
			at = AccessTicketSA.Instance.GetAccessTicket("8231322C93C14289B4FADB0443FC0F25");
			Say(at.GetCacheKey());
			at = AccessTicketSA.Instance.GetAccessTicket("some made up key!");
			Say(at.ToString());

		}

		private void button4_Click(object sender, System.EventArgs e)
		{
			DestinationUrls urls;
//			DestinationUrlSM  dusm = new DestinationUrlSM();
//			urls = dusm.GetDestinationUrls();
			urls = DestinationUrlSA.Instance.GetDestinationUrls();
			Say(urls.Count);

			string url;
			url = "/BeeGees.aspx";
			Say(url + " allowed = " + DestinationUrlSA.Instance.IsAllowedRedirect(url,true).ToString());

			url = "http://192.168.3.95:5766/Env.aspx";
			Say(url + " allowed = " + DestinationUrlSA.Instance.IsAllowedRedirect(url,true).ToString());

			url = "http://www.yahoo.com/BeeGees.html";
			Say(url + " allowed = " + DestinationUrlSA.Instance.IsAllowedRedirect(url,true).ToString());

			url = "%2fBeeGees.aspx";
			Say(url + " allowed = " + DestinationUrlSA.Instance.IsAllowedRedirect(url,true).ToString());

		}

		private void button5_Click(object sender, System.EventArgs e)
		{
			Say(RuntimeSettings.GetSetting("IS_DEVELOPMENT_MODE"));

			DateTime beginTime = DateTime.Now;

			for (Int32 i = 0; i < 1000000; i++)
			{
				RuntimeSettings.GetSetting("IS_DEVELOPMENT_MODE");
			}

			Say(DateTime.Now.Subtract(beginTime).TotalSeconds);
		}

		private void button6_Click(object sender, System.EventArgs e)
		{
			Say(
				Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri("MEMBER_SVC", 
			"MemberSM", 
			"devapp01"));


			/*
			Say(AdapterConfigurationSA.GetServicePartition("CONTENT_SVC", 0).ToUri("foobar"));

			DateTime beginTime = DateTime.Now;

			for (Int32 i = 0; i < 100000; i++)
			{
				AdapterConfigurationSA.GetServicePartition("CONTENT_SVC", PartitionMode.Random);
				//AdapterConfigurationSA.GetServicePartition("CONTENT_SVC", PartitionMode.Calculated, 0);
			}

			Say(DateTime.Now.Subtract(beginTime).TotalSeconds);
			*/
		}

		private void button7_Click(object sender, System.EventArgs e)
		{
			AccessTicket at;
			FileStream fs = new FileStream("accessTicket.dat",FileMode.Create);
			BinaryFormatter bf = new BinaryFormatter();

			at = AccessTicketBL.Instance.GetAccessTicket("D4E36B58E6EB4F25852DC76F90894ECA");
			bf.Serialize(fs,at);
			fs.Close();
			at = null;
			fs = new FileStream("accessTicket.dat",FileMode.Open);
			at = bf.Deserialize(fs) as AccessTicket;
			Console.WriteLine("Got " + at.AccessTicketID + " ) " + at.Key );
			

		}
	}
}
