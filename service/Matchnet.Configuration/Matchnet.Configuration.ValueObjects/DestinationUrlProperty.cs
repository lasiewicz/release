using System;

namespace Matchnet.Configuration.ValueObjects
{
   [Serializable()]
   public class DestinationUrlProperty
   {
	  public bool IsAllowedRedirect;
	  public bool IsWebServiceContext;

	  public DestinationUrlProperty(bool isAllowedRedirect, bool isWebServiceContext)
	  {
		 IsAllowedRedirect = isAllowedRedirect;
		 IsWebServiceContext = isWebServiceContext;
	  }
   }
}
