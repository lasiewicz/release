﻿#region

using System;

#endregion

namespace Matchnet.Configuration.ValueObjects
{
    ///<summary>
    ///</summary>
    [Serializable]
    public class ApiAppSetting : IValueObject, ICacheable
    {
        private const string Cachekeyformat = "^APIAppSetting";
        private CacheItemMode _cacheItemMode = CacheItemMode.Absolute;
        private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Normal;
        private int _cacheTtlSeconds = 120;
        ///<summary>
        /// Defined in mnAPI..App
        ///</summary>
        public int AppId { get; set; }
        ///<summary>
        ///</summary>
        public string SettingConstant { get; set; }
        ///<summary>
        ///</summary>
        public string ApiAppSettingValue { get; set; }

        #region ICacheable Members

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTtlSeconds; }
            set { _cacheTtlSeconds = value; }
        }

        public string GetCacheKey()
        {
            return GetCacheKeyString();
        }

        #endregion

        ///<summary>
        ///</summary>
        ///<returns></returns>
        public static string GetCacheKeyString()
        {
            return Cachekeyformat;
        }
    }
}