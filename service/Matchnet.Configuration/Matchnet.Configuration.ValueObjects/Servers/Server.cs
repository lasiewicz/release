using System;

namespace Matchnet.Configuration.ValueObjects.Servers
{
	[Serializable, Flags]
	public enum ServerType : int
	{
		Web = 1,
		MiddleTier = 2,
		Database = 4,
		Dev = 8,
		Staging = 16,
		ContentStaging = 32,
		Production = 64
	}
	
	
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class Server
	{
		private bool _isEnabled;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="isEnabled"></param>
		public Server(bool isEnabled)
		{
			_isEnabled = isEnabled;
		}


		/// <summary>
		/// 
		/// </summary>
		public bool IsEnabled
		{
			get
			{
				return _isEnabled;
			}
		}
	}
}
