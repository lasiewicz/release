using System;
using System.Collections;

using Matchnet;


namespace Matchnet.Configuration.ValueObjects.Servers
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class ServerCollection : DictionaryBase, IValueObject, ICacheable
	{
		/// <summary>
		/// 
		/// </summary>
		public const string CACHE_KEY = "Servers";

		private int _cacheTTLSeconds = 60 * 60;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;

		/// <summary>
		/// 
		/// </summary>
		public ServerCollection()
		{
		}


		/// <summary>
		/// 
		/// </summary>
		public Server this[string serverName]
		{
			get
			{
				return base.Dictionary[serverName.ToLower()] as Server;
			}
			set
			{
				base.Dictionary[serverName.ToLower()] = value;
			}
		}


		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return Matchnet.CacheItemMode.Absolute;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return CACHE_KEY;
		}

		#endregion
	}
}
