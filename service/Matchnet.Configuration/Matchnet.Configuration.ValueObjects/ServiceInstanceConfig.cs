using System;

namespace Matchnet.Configuration.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class ServiceInstanceConfig
	{
		private byte _partitionOffset;
		private string _partitionUri;
		private Int32 _port;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="partitionOffset"></param>
		/// <param name="partitionUri"></param>
		/// <param name="port"></param>
		public ServiceInstanceConfig(byte partitionOffset,
			string partitionUri,
			Int32 port)
		{
			_partitionOffset = partitionOffset;
			_partitionUri = partitionUri;
			_port = port;
		}


		/// <summary>
		/// 
		/// </summary>
		public byte PartitionOffset
		{
			get
			{
				return _partitionOffset;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string PartitionUri
		{
			get
			{
				return _partitionUri;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 Port
		{
			get
			{
				return _port;
			}
		}
	}
}
