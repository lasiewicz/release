﻿#region

using System;
using System.Collections.Generic;

#endregion

namespace Matchnet.Configuration.ValueObjects
{
    ///<summary>
    ///</summary>
    [Serializable]
    public class ApiAppSettingList : IValueObject, ICacheable
    {
        private const string Cachekeyformat = "^APIAppSettingList";
        private List<ApiAppSetting> _apiAppSettings = new List<ApiAppSetting>();

        private CacheItemMode _cacheItemMode = CacheItemMode.Absolute;
        private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Normal;
        private int _cacheTtlSeconds = 120;

        ///<summary>
        ///</summary>
        public List<ApiAppSetting> ApiAppSettings
        {
            get { return _apiAppSettings; }
            set { _apiAppSettings = value; }
        }

        #region ICacheable Members

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTtlSeconds; }
            set { _cacheTtlSeconds = value; }
        }

        public string GetCacheKey()
        {
            return GetCacheKeyString();
        }

        #endregion

        ///<summary>
        ///</summary>
        ///<returns></returns>
        public static string GetCacheKeyString()
        {
            return Cachekeyformat;
        }
    }
}