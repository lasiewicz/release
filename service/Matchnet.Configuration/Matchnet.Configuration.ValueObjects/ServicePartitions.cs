using System;
using System.Collections;

using Matchnet;

namespace Matchnet.Configuration.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class ServicePartitions : CollectionBase, IValueObject
	{
		/// <summary>
		/// 
		/// </summary>
		public ServicePartitions()
		{

		}


		/// <summary>
		/// 
		/// </summary>
		public ServicePartition this[int index]
		{
			get{return (ServicePartition)base.InnerList[index];}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="servicePartition"></param>
		/// <returns></returns>
		public int Add(ServicePartition servicePartition)
		{
			return base.InnerList.Add(servicePartition);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="serviceConstant"></param>
		/// <param name="partitionOffset"></param>
		/// <returns></returns>
		public ServicePartition GetPartition(string serviceConstant, int partitionOffset)
		{
			for (Int32 partitionNum = 0; partitionNum < this.Count; partitionNum++)
			{
				ServicePartition servicePartition = this[partitionNum];
				if(servicePartition.Constant.ToUpper() == serviceConstant.ToUpper() && servicePartition.PartitionOffset == partitionOffset)
				{
					return servicePartition;
				}
			}

			throw(new Exception("The specified paritition (partition #" + partitionOffset + " for service " + serviceConstant.ToUpper() + ") could not be found."));
		}

		
		public ServicePartition[] GetAll(string serviceConstant)
		{
			ArrayList partitions = new ArrayList();

			for (Int32 partitionNum = 0; partitionNum < this.Count; partitionNum++)
			{
				ServicePartition servicePartition = this[partitionNum];
				if(servicePartition.Constant.ToUpper() == serviceConstant.ToUpper())
				{
					partitions.Add(servicePartition);
				}
			}

			return (ServicePartition[])partitions.ToArray(typeof(ServicePartition));
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="serviceConstant"></param>
		/// <returns></returns>
		public int GetCount(string serviceConstant)
		{
			int partitionCount = 0;

			for (Int32 partitionNum = 0; partitionNum < this.Count; partitionNum++)
			{
				ServicePartition servicePartition = this[partitionNum];
				if(servicePartition.Constant.ToUpper() == serviceConstant.ToUpper())
				{
					partitionCount++;
				}
			}
			
			if(partitionCount == 0)
			{
				throw(new Exception("The specified service " + serviceConstant + " has no partitions."));
			}

			return partitionCount;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="serviceConstant"></param>
		/// <param name="serviceManagerName"></param>
		/// <param name="machineName"></param>
		/// <returns></returns>
		public string GetReplicationPartnerUri(string serviceConstant, string serviceManagerName, string machineName)
		{
			string hostName = null;
			string uri = null;
			int offset = Constants.NULL_INT;

			machineName = machineName.ToLower();
			
			for (Int32 partitionNum = 0; partitionNum < this.Count; partitionNum++)
			{
				ServicePartition servicePartition = this[partitionNum];
				if (servicePartition.Constant == serviceConstant)
				{
					for (Int32 siNum = 0; siNum < servicePartition.Count; siNum++)
					{
						ServiceInstance serviceInstance = servicePartition[siNum];
						if (serviceInstance.HostName.ToLower() == machineName)
						{
							offset = servicePartition.PartitionOffset;
							break;
						}
					}

					if (uri != null)
					{
						break;
					}
				}
			}

			for (Int32 partitionNum = 0; partitionNum < this.Count; partitionNum++)
			{
				ServicePartition servicePartition = this[partitionNum];
				if (servicePartition.Constant == serviceConstant && servicePartition.PartitionOffset == offset)
				{
					for (Int32 siNum = 0; siNum < servicePartition.Count; siNum++)
					{
						ServiceInstance serviceInstance = servicePartition[siNum];
						if (serviceInstance.HostName.ToLower() != machineName)
						{
							uri = "tcp://" + serviceInstance.HostName + ":" + servicePartition.Port.ToString() + "/" + serviceManagerName + ".rem";
							break;
						}
					}

					if (uri != null)
					{
						break;
					}
				}
			}

			return uri;
		}
	}
}
