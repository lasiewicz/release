using System;
using System.IO;
using System.Collections.Specialized;
using System.Runtime.Serialization;

namespace Matchnet.Configuration.ValueObjects
{
	[Serializable]
	public class DestinationUrls : NameObjectCollectionBase, ISerializable, ICacheable, IByteSerializable
	{
	  

		//	  private Dictionary<string, Dictionary<string, Dictionary<string, DestinationUrlProperty >>> _UrlTree;
	  
		private const byte VERSION_001 = 1;

		public const string CACHE_KEY = "^DestinationUrls";
		public DestinationUrls()
		{	
		}

		protected DestinationUrls(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", this.ToByteArray());
		}

		public void Add(string url, DestinationUrlProperty property)
		{ 
		
			Uri uri = new Uri(url.ToLower());
			string key = uri.Scheme + ":" + uri.Host;
			base.BaseAdd(key, property);

		 
			//
			//		 if (!_UrlTree.ContainsKey(uri.Scheme))
			//			_UrlTree.Add(uri.Scheme, new Dictionary<string,  Dictionary<string, DestinationUrlProperty>>());
			//
			//		 if (!_UrlTree[uri.Scheme].ContainsKey(uri.Host))
			//			_UrlTree[uri.Scheme].Add(uri.Host, new Dictionary<string, DestinationUrlProperty>());
			//
			//		 if (!_UrlTree[uri.Scheme][uri.Host].ContainsKey(uri.AbsolutePath))
			//			_UrlTree[uri.Scheme][uri.Host].Add(uri.AbsolutePath, null);
			//
			//		 _UrlTree[uri.Scheme][uri.Host][uri.AbsolutePath] = property;
		}

		public DestinationUrlProperty this[string url] 
		{
			get
			{
				DestinationUrlProperty result;
				Uri uri = new Uri(url.ToLower());
				string key = uri.Scheme + ":" + uri.Host;
				result = base.BaseGet(key) as DestinationUrlProperty;
				return result;
				//			if (_UrlTree.ContainsKey(uri.Scheme) &&
				//			   _UrlTree[uri.Scheme].ContainsKey(uri.Host) &&
				//			   _UrlTree[uri.Scheme][uri.Host].ContainsKey(uri.AbsolutePath)
				//			   )
				//			   return _UrlTree[uri.Scheme][uri.Host][uri.AbsolutePath];
				//			else
				//			   return null;
			}
			set 
			{
				this.Add(url, value);
			}
		}


		#region ICacheable Members
		public CacheItemMode CacheMode
		{
			get { return CacheItemMode.Absolute; }
		}

		public CacheItemPriorityLevel CachePriority
		{
			get { return CacheItemPriorityLevel.Normal; }
			set { }
		}

		public int CacheTTLSeconds
		{
			get { return 30; }
			set { }
		}

		public string GetCacheKey()
		{
			return CACHE_KEY;
		}

		#endregion  

		#region IByteSerializable Members

		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new BinaryReader(ms);

			byte version = br.ReadByte();
			if (version != VERSION_001 ) 
				throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			
			int length = br.ReadInt32();
			base.BaseClear();
			for (int i = 0; i < length; i++)
			{
				string key = br.ReadString();
				bool isAllowedRedirect = br.ReadBoolean();
				bool isWebServiceContext = br.ReadBoolean();
				this.Add(key, new DestinationUrlProperty( isAllowedRedirect, isWebServiceContext));
			}	
		}

		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			bw.Write(VERSION_001);
			
			bw.Write(base.Count);
			for (int i = 0; i < base.Count; i++)
			{
				string key = base.BaseGetKey(i);
				bw.Write(key);
				DestinationUrlProperty urp = base.BaseGet(key) as DestinationUrlProperty;
				bw.Write(urp.IsAllowedRedirect);
				bw.Write(urp.IsWebServiceContext);
			}
			
			return ms.ToArray();
		}

		#endregion
	}
}
