using System;
using System.Collections;
using System.IO;

using Matchnet;


namespace Matchnet.Configuration.ValueObjects.Lpd
{
	public class Partition
	{
		private PhysicalDatabases _physicalDatabases;
		private Int16 _offset;

		public Partition(Int16 offset)
		{
			_offset = offset;
			_physicalDatabases = new PhysicalDatabases();
		}


		public PhysicalDatabases PhysicalDatabases
		{
			get
			{
				return _physicalDatabases;
			}
		}


		public Int16 Offset
		{
			get
			{
				return _offset;
			}
		}


		internal static Partition FromStream(BinaryReader br)
		{
			Partition partition = new Partition(br.ReadInt16());
			Int32 count = br.ReadInt32();

			for (Int32 i = 0; i < count; i++)
			{
				partition.PhysicalDatabases.Add(PhysicalDatabase.FromStream(br));
			}

			return partition;
		}

			
		internal void appendToStream(BinaryWriter bw)
		{
			bw.Write(_offset);

			bw.Write(PhysicalDatabases.Count);
			for (Int32 i = 0; i < PhysicalDatabases.Count; i++)
			{
				PhysicalDatabases[i].appendToStream(bw);
			}
		}
	}
}
