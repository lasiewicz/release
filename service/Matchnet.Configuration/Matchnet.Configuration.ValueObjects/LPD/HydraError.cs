using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet;


namespace Matchnet.Configuration.ValueObjects.Lpd
{
	[Serializable]
	public class HydraError : ISerializable, IByteSerializable
	{
		private const byte VERSION_001 = 1;

		private DateTime _dateTime;
		private string _queuePath;
		private string _messageID;
		private string _description;

		public HydraError(string queuePath,
			string messageID,
			string description)
		{
			_dateTime = DateTime.Now;
			_queuePath = queuePath;
			_messageID = messageID;
			_description = description;
		}


		protected HydraError(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray",ToByteArray());
		}


		public DateTime DateTime
		{
			get
			{
				return _dateTime;
			}
		}


		public string QueuePath
		{
			get
			{
				return _queuePath;
			}
		}


		public string MessageID
		{
			get
			{
				return _messageID;
			}
		}


		public string Description
		{
			get
			{
				return _description;
			}
		}
        public override string ToString()
        {
            try
            {

                System.Text.StringBuilder bld = new System.Text.StringBuilder();


                bld.Append("HydraError:\r\n");
                bld.Append(String.Format("DateTime:{0}\r\n",_dateTime));
                bld.Append(String.Format("QueuePath:{0}\r\n",_queuePath));
                bld.Append(String.Format("MessageID:{0}\r\n", _messageID));
                bld.Append(String.Format("Description:{0}\r\n", _description));
                return bld.ToString();


            }
            catch (Exception ex)
            { ex = null; return ""; }
        }

		#region IByteSerializable Members
		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new System.IO.BinaryReader(ms);
			byte version = br.ReadByte();

			switch (version)
			{
				case VERSION_001:
					_dateTime = DateTime.FromFileTime(br.ReadInt64());
					_queuePath = br.ReadString();
					_messageID = br.ReadString();
					_description = br.ReadString();
				break;

				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}


		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream(1024);
			BinaryWriter bw = new BinaryWriter(ms);
			
			bw.Write(VERSION_001);

			bw.Write(_dateTime.ToFileTime());
			bw.Write(_queuePath);
			bw.Write(_messageID);
			bw.Write(_description);

			return ms.ToArray();
		}
		#endregion
	}
}
