using System;
using System.Collections;
using System.IO;
using System.Threading;
using System.Runtime.Serialization;

using Matchnet;


namespace Matchnet.Configuration.ValueObjects.Lpd
{
	[Serializable]
	public enum HydraModeType : byte
	{
		None = 0,
		Spray = 1,
		Transactional = 2
	}


	public class LogicalDatabase
	{
		private string _logicalDatabaseName;
		private HydraModeType _hydraMode;
		private Int16 _hydraThreads;
		private Hashtable _partitions;
		private ArrayList _recoveryList;
		private ReaderWriterLock _recoveryLock;

		public LogicalDatabase(string logicalDatabaseName,
			HydraModeType hydraMode,
			Int16 hydraThreads)
		{
			_partitions = new Hashtable();
			_logicalDatabaseName = logicalDatabaseName;
			_hydraMode = hydraMode;
			_hydraThreads = hydraThreads;
			_recoveryList = new ArrayList();
			_recoveryLock = new ReaderWriterLock();
		}


		public void AddPartition(Partition partition)
		{
			_partitions.Add(partition.Offset, partition);
		}


		public Int16 GetOffset(Int32 key)
		{
			Int16 offset = 0;

			if (key > 0)
			{
				offset = (Int16)(key % this.PartitionCount);
			}

			return offset;
		}


		public Partition GetPartition(Int32 key)
		{
			return this[GetOffset(key)];
		}


		public Partition this[Int16 offset]
		{
			get
			{
				return _partitions[offset] as Partition;
			}
		}


		public Int16 PartitionCount
		{
			get
			{
				return (Int16)_partitions.Count;
			}
		}


		public string LogicalDatabaseName
		{
			get
			{
				return _logicalDatabaseName;
			}
		}


		public HydraModeType HydraMode
		{
			get
			{
				return _hydraMode;
			}
		}


		public Int16 HydraThreads
		{
			get
			{
				return _hydraThreads;
			}
		}


		public void AddToRecoveryList(PhysicalDatabase physicalDatabase)
		{
			_recoveryLock.AcquireWriterLock(-1);
			try
			{
				if (!_recoveryList.Contains(physicalDatabase))
				{
					_recoveryList.Add(physicalDatabase);
					physicalDatabase.IsRecovering = true;
				}
			}
			finally
			{
				_recoveryLock.ReleaseLock();
			}
		}


		public bool IsInRecoveryMode(short offset)
		{
			Partition partition = this[offset];
			for (Int32 pdbNum = 0; pdbNum < partition.PhysicalDatabases.Count; pdbNum++)
			{
				PhysicalDatabase pdb = partition.PhysicalDatabases[pdbNum];

				try
				{
					_recoveryLock.AcquireReaderLock(-1);
					if (_recoveryList.Contains(pdb))
					{
						return true;
					}
				}
				catch (Exception ex)
				{
					throw new Exception("Exception in IsInRecoveryMode().  Logical DB: " + _logicalDatabaseName + ", Offset: " + offset + ", pdbNum: " + pdbNum + "partition.PhysicalDatabases.Count: " + partition.PhysicalDatabases.Count, ex);
				}
				finally
				{
					_recoveryLock.ReleaseLock();
				}
			}

			return false;
		}


		public void RemoveFromRecoveryList(PhysicalDatabase physicalDatabase)
		{
			PhysicalDatabase pdbFound = null;
			_recoveryLock.AcquireWriterLock(-1);
			try
			{
				for (Int32 pdbNum = 0; pdbNum < _recoveryList.Count; pdbNum++)
				{
					PhysicalDatabase pdb = _recoveryList[pdbNum] as PhysicalDatabase;
					if (pdb.ServerName == physicalDatabase.ServerName && pdb.PhysicalDatabaseName == physicalDatabase.PhysicalDatabaseName)
					{
						pdbFound = pdb;
					}
				}

				if (pdbFound != null)
				{
					_recoveryList.Remove(pdbFound);
					physicalDatabase.IsRecovering = false;
				}
			}
			finally
			{
				_recoveryLock.ReleaseLock();
			}
		}


		public bool RecoveryMode
		{
			get
			{
				_recoveryLock.AcquireReaderLock(-1);
				try
				{
					return _recoveryList.Count > 0;
				}
				finally
				{
					_recoveryLock.ReleaseLock();
				}
			}
		}


		public ArrayList GetRecoveryList()
		{
			_recoveryLock.AcquireReaderLock(-1);
			try
			{
				return (ArrayList)_recoveryList.Clone();
			}
			finally
			{
				_recoveryLock.ReleaseLock();
			}
		}


		internal static LogicalDatabase FromStream(BinaryReader br)
		{
			LogicalDatabase logicalDatabase = new LogicalDatabase(br.ReadString(),
				(HydraModeType)br.ReadByte(),
				br.ReadInt16());

			Int32 count = br.ReadInt32();
			for (Int32 i = 0; i < count; i++)
			{
				Partition partition = Partition.FromStream(br);
				logicalDatabase.AddPartition(partition);

				for (Int32 dbNum = 0; dbNum < partition.PhysicalDatabases.Count; dbNum++)
				{
					if (partition.PhysicalDatabases[dbNum].IsActive)
					{
						logicalDatabase.AddToRecoveryList(partition.PhysicalDatabases[dbNum]);
					}
				}
			}

			return logicalDatabase;
		}


		internal void appendToStream(BinaryWriter bw)
		{
			bw.Write(_logicalDatabaseName);
			bw.Write((byte)_hydraMode);
			bw.Write(_hydraThreads);

			bw.Write(_partitions.Count);
			IDictionaryEnumerator de = _partitions.GetEnumerator();
			while (de.MoveNext())
			{
				((Partition)de.Value).appendToStream(bw);
			}
		}
	}
}
