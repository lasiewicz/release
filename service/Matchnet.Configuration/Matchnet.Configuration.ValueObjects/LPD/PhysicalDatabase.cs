using System;
using System.Collections;
using System.IO;
using System.Threading;

using Matchnet;


namespace Matchnet.Configuration.ValueObjects.Lpd
{
	/// <summary>
	/// 
	/// </summary>
	public class PhysicalDatabase
	{
		private string _serverName;
		private string _physicalDatabaseName;
		private bool _isActive;
		private bool _isRecovering;
		private Int16 _failureCount = 0;
		private Int16 _failureThreshold;
		private ReaderWriterLock _lock;
		private bool _isUsed = false;
		private Queue _errors;

		public PhysicalDatabase(string serverName,
			string physicalDatabaseName,
			bool isActive,
			Int16 failureThreshold)
	{
			_lock = new ReaderWriterLock();

			_serverName = serverName;
			_physicalDatabaseName = physicalDatabaseName;
			_isActive = isActive;
			_failureThreshold = failureThreshold;
			_errors = new Queue();
		}


		public void AddError(HydraError error)
		{
#if DEBUG
			System.Diagnostics.Trace.WriteLine("__addError\t" + error.Description.Replace("\r", "").Replace("\n", ""));
#endif
			lock (_errors.SyncRoot)
			{
				_errors.Enqueue(error);

				while (_errors.Count > 10)
				{
					_errors.Dequeue();
				}
			}
		}


		public HydraError[] Errors
		{
			get
			{
				lock (_errors.SyncRoot)
				{
					HydraError[] errors = new HydraError[_errors.Count];
					Int32 errorNum = 0;

					while (_errors.Count > 0)
					{
						errors[errorNum] = _errors.Dequeue() as HydraError;
					}

					return errors;
				}
			}
		}


		public bool IsFailed
		{
			get
			{
				return (this.FailureCount >= _failureThreshold);
			}
		}

		
		public void IncrementFailureCount(string processName)
		{
			bool logError = false;

			_lock.AcquireWriterLock(-1);
			try
			{
				if (_failureCount < _failureThreshold)
				{
					_failureCount++;
					if (_failureCount == _failureThreshold && _failureThreshold > 0)
					{
						logError = true;
					}
				}
			}
			finally
			{
				_lock.ReleaseLock();
			}

			if (logError)
			{
				System.Diagnostics.EventLog.WriteEntry(processName,
					"Physical database failure count has met threshold.\r\n\r\n" +
					"threshold: " + _failureThreshold.ToString() + "\r\n" +
					"server: " + _serverName + "\r\n" +
					"database: " + _physicalDatabaseName +
                     "\r\nerrors: " + GetErrors(3),
					System.Diagnostics.EventLogEntryType.Error);
			}
		}

		
		public void ResetFailureCount()
		{
			if (this.FailureCount > 0)
			{
				_lock.AcquireWriterLock(-1);
				try
				{
					_failureCount = 0;
				}
				finally
				{
					_lock.ReleaseLock();
				}
			}
		}

		
		public Int16 FailureThreshold
		{
			get
			{
				return _failureThreshold;
			}
		}

		
		public Int16 FailureCount
		{
			get
			{
				_lock.AcquireReaderLock(-1);
				try
				{
					return _failureCount;
				}
				finally
				{
					_lock.ReleaseLock();
				}
			}
		}

		
		public bool IsUsed
		{
			get
			{
				return _isUsed;
			}
			set
			{
				_isUsed = value;
			}
		}

		
		public string ServerName
		{
			get
			{
				return _serverName;
			}
		}

		
		public string PhysicalDatabaseName
		{
			get
			{
				return _physicalDatabaseName;
			}
		}

		
		public bool IsActive
		{
			get
			{
				return _isActive;
			}
			set
			{
				_isActive = value;
			}
		}

		
		public bool IsRecovering
		{
			get
			{
				return _isRecovering;
			}
			set
			{
				_isRecovering = value;
			}
		}

		
		public string ConnectionString
		{
			get
			{
				return "Server=" + ServerName + ";Database=" + PhysicalDatabaseName + ";Integrated Security=SSPI";
			}
		}

        private string GetErrors(int limit)
        {
            try
            {
                System.Text.StringBuilder bld = new System.Text.StringBuilder();
                
                if (_errors != null)
                {
                    int errlimit = _errors.Count > limit?_errors.Count:limit;
                    for(int i=0;i< errlimit;i++)
                    {
                        bld.Append(Errors[i].ToString());
                    }

                }
                return bld.ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }

		internal static PhysicalDatabase FromStream(BinaryReader br)
		{
			string serverName = br.ReadString();
			string physicalDatabaseName = br.ReadString();
			bool isActive = br.ReadBoolean();
			bool isRecovering = br.ReadBoolean();
			Int16 failureCount = br.ReadInt16();
			Int16 failureThreshold = br.ReadInt16();
			bool isUsed = br.ReadBoolean();

			PhysicalDatabase physicalDatabase = new PhysicalDatabase(serverName,
				physicalDatabaseName,
				isActive,
				failureThreshold);

			return physicalDatabase;
		}

		
		internal void appendToStream(BinaryWriter bw)
		{
			bw.Write(_serverName);
			bw.Write(_physicalDatabaseName);
			bw.Write(_isActive);
			bw.Write(_isRecovering);
			bw.Write(_failureCount);
			bw.Write(_failureThreshold);
			bw.Write(_isUsed);
		}
	}
}
