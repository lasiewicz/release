using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;

using Matchnet;


namespace Matchnet.Configuration.ValueObjects.Lpd
{
	[Serializable]
	public class LogicalDatabases : System.Collections.Specialized.NameObjectCollectionBase, ISerializable, IByteSerializable
	{
		private const byte VERSION_001 = 1;
		private Guid _version;

		public LogicalDatabases(Guid version)
		{
			_version = version;
		}


		protected LogicalDatabases(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}


		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray",ToByteArray());
		}


		public void AddLogicalDatabase(LogicalDatabase logicalDatabase)
		{
			base.BaseAdd(logicalDatabase.LogicalDatabaseName.ToLower(), logicalDatabase);
		}


		public LogicalDatabase this[string logicalDatabaseName]
		{
			get
			{
				return base.BaseGet(logicalDatabaseName.ToLower()) as LogicalDatabase;
			}
		}


		public LogicalDatabase this[Int32 index]
		{
			get
			{
				return base.BaseGet(index) as LogicalDatabase;
			}
		}


		public Guid Version
		{
			get
			{
				return _version;
			}
		}


		#region IByteSerializable Members
		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new System.IO.BinaryReader(ms);
			byte version = br.ReadByte();

			switch (version)
			{
				case VERSION_001:
					_version = new Guid(br.ReadBytes(16));
					Int32 count = br.ReadInt32();
					for (Int32 i = 0; i < count; i++)
					{
						this.AddLogicalDatabase(LogicalDatabase.FromStream(br));
					}
					break;

				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}


		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);
			
			bw.Write(VERSION_001);

			bw.Write(_version.ToByteArray());
			bw.Write(this.Count);
			for (Int32 i = 0; i < this.Count; i++)
			{
				this[i].appendToStream(bw);
			}

			return ms.ToArray();
		}
		#endregion
	}
}
