﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Configuration.ValueObjects.Management
{
    #region class Setting
    [Serializable]
    public class Setting
    {
        int _settingid;
        int _settingcategoryid;
        string _settingconstant = null;
        string _globaldefaultvalue = null;
        string _settingdescription = null;
        bool _isrequiredforcommunity;
        bool _isrequiredforsite;
        object _createdate = null;
        public int SettingID
        {
            get { return _settingid; }
            set { _settingid = value; }
        }


        public int SettingCategoryID
        {
            get { return _settingcategoryid; }
            set { _settingcategoryid = value; }
        }


        public string SettingConstant
        {
            get { return _settingconstant; }
            set { _settingconstant = value; }
        }


        public string GlobalDefaultValue
        {
            get { return _globaldefaultvalue; }
            set { _globaldefaultvalue = value; }
        }


        public string SettingDescription
        {
            get { return _settingdescription; }
            set { _settingdescription = value; }
        }


        public bool IsRequiredForCommunity
        {
            get { return _isrequiredforcommunity; }
            set { _isrequiredforcommunity = value; }
        }


        public bool IsRequiredForSite
        {
            get { return _isrequiredforsite; }
            set { _isrequiredforsite = value; }
        }


        public object CreateDate
        {
            get { return _createdate; }
            set { _createdate = value; }
        }


    }
    #endregion

}
