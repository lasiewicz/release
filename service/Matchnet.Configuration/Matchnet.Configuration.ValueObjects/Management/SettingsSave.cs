﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Configuration.ValueObjects.Management
{
    public enum SettingsScope : int
    {
        global = 1,
        community = 2,
        site = 3,
        brand = 4,
        server = 5
    }
    [Serializable]
    public class SettingsSave
    {
        public SettingsScope Scope { get; set; }
        public bool DeleteFlag { get; set; }
        public string SettingConstant { get; set; }
        public string SettingValue { get; set; }
        public string ServerName { get; set; }
        //list of site/community/brand/server ids
        public List<int> ScopeIDList { get; set; }

        //for server setting, no way around it
        public int CommunityID { get; set; }
        public int SiteID { get; set; }
        public int BrandID { get; set; }
    }
}
