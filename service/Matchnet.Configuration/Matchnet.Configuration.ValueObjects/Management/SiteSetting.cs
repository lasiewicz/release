﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Configuration.ValueObjects.Management
{
    #region class SiteSetting
    [Serializable]
    public class SiteSetting
    {
        int _sitesettingid;
        int _siteid;
        int _settingid;
        string _sitesettingvalue = null;
        public int SiteSettingID
        {
            get { return _sitesettingid; }
            set { _sitesettingid = value; }
        }


        public int SiteID
        {
            get { return _siteid; }
            set { _siteid = value; }
        }


        public int SettingID
        {
            get { return _settingid; }
            set { _settingid = value; }
        }


        public string SiteSettingValue
        {
            get { return _sitesettingvalue; }
            set { _sitesettingvalue = value; }
        }


    }
    #endregion
}
