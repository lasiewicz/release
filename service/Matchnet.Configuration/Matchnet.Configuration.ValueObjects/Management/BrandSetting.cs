﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Configuration.ValueObjects.Management
{

    #region class BrandSetting
    [Serializable]
    public class BrandSetting
    {
        int _brandsettingid;
        int _brandid;
        int _settingid;
        string _brandsettingvalue = null;
        public int BrandSettingID
        {
            get { return _brandsettingid; }
            set { _brandsettingid = value; }
        }


        public int BrandID
        {
            get { return _brandid; }
            set { _brandid = value; }
        }


        public int SettingID
        {
            get { return _settingid; }
            set { _settingid = value; }
        }


        public string BrandSettingValue
        {
            get { return _brandsettingvalue; }
            set { _brandsettingvalue = value; }
        }


    }
    #endregion

}
