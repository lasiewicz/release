﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Configuration.ValueObjects.Management
{
    #region class CommunitySetting
    [Serializable]
    public class CommunitySetting
    {
        int _communitysettingid;
        int _communityid;
        int _settingid;
        string _communitysettingvalue = null;
        public int CommunitySettingID
        {
            get { return _communitysettingid; }
            set { _communitysettingid = value; }
        }


        public int CommunityID
        {
            get { return _communityid; }
            set { _communityid = value; }
        }


        public int SettingID
        {
            get { return _settingid; }
            set { _settingid = value; }
        }


        public string CommunitySettingValue
        {
            get { return _communitysettingvalue; }
            set { _communitysettingvalue = value; }
        }


    }
    #endregion
}
