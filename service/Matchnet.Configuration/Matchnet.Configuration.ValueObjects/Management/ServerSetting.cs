﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Configuration.ValueObjects.Management
{
    #region class ServerSetting
    [Serializable]
    public class ServerSetting
    {
        int _serversettingid;
        string _servername;
        int _serverid;
        int _settingid;
        int _communityid;
        int _siteid;
        int _brandid;
        string _serversettingvalue = null;
        public int ServerSettingID
        {
            get { return _serversettingid; }
            set { _serversettingid = value; }
        }


        public int ServerID
        {
            get { return _serverid; }
            set { _serverid = value; }
        }
        public string ServerName
        {
            get { return _servername; }
            set { _servername = value; }
        }

        public int SettingID
        {
            get { return _settingid; }
            set { _settingid = value; }
        }


        public int CommunityID
        {
            get { return _communityid; }
            set { _communityid = value; }
        }


        public int SiteID
        {
            get { return _siteid; }
            set { _siteid = value; }
        }


        public int BrandID
        {
            get { return _brandid; }
            set { _brandid = value; }
        }


        public string ServerSettingValue
        {
            get { return _serversettingvalue; }
            set { _serversettingvalue = value; }
        }


    }
    #endregion

}
