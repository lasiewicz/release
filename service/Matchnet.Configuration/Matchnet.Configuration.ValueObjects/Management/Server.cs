﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Configuration.ValueObjects.Management
{
    [Serializable]
    public class Server
    {

        int _serverid;
        string _name = null;
        bool _isenabled;
        int _servertype;
        int _locationid;
        public int ServerID
        {
            get { return _serverid; }
            set { _serverid = value; }
        }


        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }


        public bool IsEnabled
        {
            get { return _isenabled; }
            set { _isenabled = value; }
        }


        public int ServerType
        {
            get { return _servertype; }
            set { _servertype = value; }
        }
        public string ServerTypeName
        {
            get;
            set;
        }

        public int LocationID
        {
            get { return _locationid; }
            set { _locationid = value; }
        }
    }
}
