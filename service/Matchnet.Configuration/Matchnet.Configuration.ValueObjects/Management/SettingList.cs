﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Configuration.ValueObjects.Management
{
    [Serializable]
    public class Settings
    {
        public List<Setting> SettingList { get; set; }
        public List<CommunitySetting> CommunitySettingList { get; set; }
        public List<SiteSetting> SiteSettingList { get; set; }
        public List<BrandSetting> BrandSettingList { get; set; }
        public List<ServerSetting> ServerSettingList { get; set; }


        

    }
}
