﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Matchnet.Configuration.ValueObjects
{
    [Serializable]
    public class MembaseConfigList : IValueObject, ICacheable
    {
        List<MembaseConfig> _membaseConfigs = new List<MembaseConfig>();

        public List<MembaseConfig> MembaseConfigs
        {
            get { return _membaseConfigs; }
            set { _membaseConfigs = value; }
        }

        #region ICacheable Members
        private int _cacheTTLSeconds = (60*60); //1 hour absolute cache
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEYFORMAT = "^MembaseConfigs";

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public string GetCacheKey()
        {
            return GetCacheKeyString();
        }

        public static string GetCacheKeyString()
        {
            return CACHEKEYFORMAT;
        }

        #endregion
    }
}
