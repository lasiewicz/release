﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Configuration.ValueObjects.Urls
{
    [Serializable]
    public class DestinationURL
    {
        public string URL { get; set; }
        public bool IsAllowedRedirect { get; set; }
        public bool IsWebServiceContext { get; set; }
        public static string COLLECTION_CACHE_KEY = "^DestinationUrlList";

        public DestinationURL() { }

        public DestinationURL(string url, bool isAllowedRedirect, bool isWebServiceContext)
        {
            URL = url;
            IsAllowedRedirect = isAllowedRedirect;
            IsWebServiceContext = isWebServiceContext;
        }
    }
}
