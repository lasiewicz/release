﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Configuration.ValueObjects.Urls
{
    [Serializable]
    public class CachedDestinationURLs: IValueObject, ICacheable
    {
        private List<DestinationURL> _urls;

        public List<DestinationURL> URLs
        {
            get
            {
                return _urls;
            }
        }

        public CachedDestinationURLs(List<DestinationURL> urls, int cacheTTLSeconds)
        {
            _urls = urls;
            _CacheKey = DestinationURL.COLLECTION_CACHE_KEY;
            _CacheTTLSeconds = cacheTTLSeconds;
        }


        #region ICacheable Members

        private int _CacheTTLSeconds;
        private CacheItemPriorityLevel _CachePriority;
        private string _CacheKey;

        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get
            {
                return _CacheTTLSeconds;
            }
            set
            {
                _CacheTTLSeconds = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get
            {
                return CacheItemMode.Sliding;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _CachePriority;
            }
            set
            {
                _CachePriority = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetCacheKey()
        {
            return _CacheKey;
        }

        #endregion


        #region ICacheable Members

        CacheItemMode ICacheable.CacheMode
        {
            get { return CacheItemMode.Absolute; }
        }

        CacheItemPriorityLevel ICacheable.CachePriority
        {
            get
            {
                return _CachePriority;
            }
            set
            {
                _CachePriority = value;
            }
        }

        int ICacheable.CacheTTLSeconds
        {
            get
            {
                return _CacheTTLSeconds;
            }
            set
            {
                _CacheTTLSeconds = value;
            }
        }

        string ICacheable.GetCacheKey()
        {
            return DestinationURL.COLLECTION_CACHE_KEY;
        }

        #endregion
    }
}
