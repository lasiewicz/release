﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Exceptions;

namespace Matchnet.Configuration.ValueObjects
    {
    [Serializable]
    public class ConfigServiceTrace : IValueObject
    {
        private const string FORMAT_STR = "App Source:{0}\r\n============\r\n Ex.Source:{1} - Ex.Msg:{2}\r\n============";
        private const string FORMAT_STR_TRACE = "App Source:{0}\r\n============\r\nTrace:{1}\r\n============";

        private const string DETAILED_FORMAT_STR = "App Source:{0}.{1}\r\n============\r\n Ex.Source:{2} - Ex.Msg:{3}\r\n============";
        //private const string  FORMAT_STR="App Source:{0}\r\nEx.Source:{1}\r\nEx.Msg:{2}";
        public static readonly ConfigServiceTrace Instance = new ConfigServiceTrace();
        private ConfigServiceTrace()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        public void Trace(string appSource, Exception ex, IValueObject obj)
        {
            try
            {
                System.Diagnostics.Trace.Write(getTrace(appSource, ex, obj));
            }
            catch (Exception e)
            { e = null; }
        }

        public void Trace(string appSource, string trace)
        {
            try
            {
                System.Diagnostics.Trace.Write(String.Format(FORMAT_STR_TRACE, appSource, trace));
            }
            catch (Exception e)
            { e = null; }
        }

        public void DebugTrace(string appSource, string trace)
        {
            try
            {
                Trace(appSource, trace);
            }
            catch (Exception e)
            { e = null; }
        }
        public void DebugTrace(string appSource, Exception ex, IValueObject obj)
        {
            try
            {

#if DEBUG
                Trace(appSource, ex, obj);

#endif

            }
            catch (Exception e)
            { e = null; }
        }
        public string getTrace(string appSource, Exception ex, IValueObject obj)
        {
            string exStr = "";
            string innerexStr = "";


            try
            {
                exStr = String.Format(FORMAT_STR, appSource, ex.Source, ex.Message);
                if (ex.InnerException != null)
                    innerexStr = String.Format(FORMAT_STR, appSource, ex.InnerException.Source, ex.InnerException.Message);
                exStr += innerexStr;
                if (obj != null)
                    exStr += "\r\nValue Obj:" + obj.ToString();

                return exStr;


            }
            catch (Exception e)
            {
                e = null;
                return exStr;

            }
        }

        public void LogServiceException(string modulename, string functionname, Exception ex, bool throwEx)
        {
            string excString = "";
            try
            {
                excString = string.Format(DETAILED_FORMAT_STR, modulename, functionname, ex.Source, ex.ToString());
                ServiceBoundaryException boundaryEx = new ServiceBoundaryException(ValueObjects.ServiceConstants.SERVICE_NAME, excString);
                Trace(String.Format("{0}.{1}", modulename, functionname), excString);
                if (throwEx)
                { throw (boundaryEx); }

            }
            catch (Exception e)
            {
                Trace(String.Format("{0}.{1}", modulename, functionname), excString);
                if (throwEx)
                    throw e;
            }

        }

        public void LogAdapterException(string modulename, string functionname, Exception ex, IValueObject obj, string trace, bool throwEx)
        {
            string excString = "";
            string excData = "";
            try
            {
                excString = string.Format(DETAILED_FORMAT_STR, modulename, functionname, ex.Source, ex.ToString());
                if (obj != null)
                    excData = obj.ToString();

                excString += "\r\nData:\r\n" + excData;


                if (!String.IsNullOrEmpty(trace))
                    excString += "\r\nTrace:\r\n" + trace;
                System.Diagnostics.EventLog.WriteEntry("WWW", excString);
                SAException boundaryEx = new SAException(excString);
                Trace(String.Format("{0}.{1}", modulename, functionname), excString);
                if (throwEx)
                { throw (boundaryEx); }

            }
            catch (Exception e)
            {
                Trace(String.Format("{0}.{1}", modulename, functionname), excString);
                if (throwEx)
                    throw e;
            }

        }

        public ServiceBoundaryException GetServiceException(string modulename, string functionname, Exception ex)
        {
            string excString = "";
            try
            {
                excString = string.Format(DETAILED_FORMAT_STR, modulename, functionname, ex.Source, ex.ToString());
                ServiceBoundaryException boundaryEx = new ServiceBoundaryException(ex.Source, excString);
                Trace(String.Format("{0}.{1}", modulename, functionname), excString);
                return boundaryEx;

            }
            catch (Exception e)
            {
                Trace(String.Format("{0}.{1}", modulename, functionname), excString);
                return null;
            }

        }

    }
}
