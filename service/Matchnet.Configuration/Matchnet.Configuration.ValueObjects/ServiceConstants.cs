using System;

namespace Matchnet.Configuration.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	public class ServiceConstants
	{
		/// <summary>
		/// 
		/// </summary>
		public const string SERVICE_NAME = "Matchnet.Configuration.Service";
		/// <summary>
		/// 
		/// </summary>
		public const string SERVICE_CONSTANT = "CONFIGURATION_SVC";

		private ServiceConstants()
		{
		}
	}
}
