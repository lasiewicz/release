using System;
using System.Text;

namespace Matchnet.Configuration.ValueObjects.SparkWS
{
	public class MemberCommunityInfo
	{

		public int CommunityID;
		public bool IsSelfSuspended;

		public MemberCommunityInfo() { }
		public MemberCommunityInfo(int communityID, bool isSelfSuspended) 
		{
			CommunityID = communityID;
			IsSelfSuspended = isSelfSuspended;
		}

	}
}