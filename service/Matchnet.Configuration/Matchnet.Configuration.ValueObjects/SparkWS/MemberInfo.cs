using System;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace Matchnet.Configuration.ValueObjects.SparkWS
{

	[Serializable()]
	[XmlInclude(typeof(MemberCommunityInfo[])),
	XmlInclude(typeof(MemberSiteInfo[]))]
	public class MemberInfo : VersionedVO, IByteSerializable, ISerializable
	{
		private const byte VERSION_001 = 1;

		public MemberSiteInfo[] Sites;
		public MemberCommunityInfo[] Communities;

		public MemberInfo()
			: base("1.0.0")
		{
		}

		#region IByteSerializable Members

		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new System.IO.BinaryReader(ms);
			byte version = br.ReadByte();

			switch (version)
			{
				case VERSION_001:
					int length = br.ReadInt32();
					this.Sites = new MemberSiteInfo[length];
					for (int i = 0; i < length; i++) 
					{
						int siteID = br.ReadInt32();
						DateTime subscriptionExpirationDate = new DateTime(br.ReadInt64());
						Sites[i] = new MemberSiteInfo(siteID, subscriptionExpirationDate);
					}

					length = br.ReadInt32();
					this.Communities = new MemberCommunityInfo[length];
					for (int i = 0; i < length; i++) 
					{
						int communityID = br.ReadInt32();
						bool isSelfSuspended = br.ReadBoolean();
						Communities[i] = new MemberCommunityInfo(communityID, isSelfSuspended);
					}
					break;

				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
		}

		public byte[] ToByteArray()
		{
			
			MemoryStream ms = new MemoryStream(1024);
			BinaryWriter bw = new BinaryWriter(ms);
			
			bw.Write(VERSION_001);

			bw.Write(Sites.Length);
			for ( int i = 0; i < Sites.Length; i++) 	
			{
				bw.Write(Sites[i].SiteID);
				bw.Write(Sites[i].SubscriptionExpirationDate.Ticks);
			}
			
			bw.Write(Communities.Length);
			for ( int i = 0; i < Communities.Length; i++) 	
			{
				bw.Write(Communities[i].CommunityID);
				bw.Write(Communities[i].IsSelfSuspended);
			}
	
			return ms.ToArray();
		}

		#endregion

		#region ISerializable Members
		protected MemberInfo(SerializationInfo info, StreamingContext context) : base("1.0.0")
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", this.ToByteArray());
		}
		#endregion
	}

}