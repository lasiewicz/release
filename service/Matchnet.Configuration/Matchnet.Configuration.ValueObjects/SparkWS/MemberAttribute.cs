using System;

/********************************************************
* 2006-02-07
* This file contains types that can be replaced with a 2.0 generic type <T>
* because we are not ready with 2.0 in web tier etc, the multiple types have been created to be 1.1 compatible.
*	Nuri
********************************************************/

namespace Matchnet.Configuration.ValueObjects.SparkWS
{

	/// <summary>
	/// Represents a value of a specific member's attribute
	/// </summary>
	[Serializable()]
	public class MemberAttributeOfInt32
	{
		public int AttributeID;
		public int CommunityID = 0;
		public int SiteID = 0;
		public int BrandID = 0;
		public int Value;

		public MemberAttributeOfInt32(){ }

		public MemberAttributeOfInt32(MemberAttributeParameter parameters, int value)
		{
			AttributeID = parameters.AttributeID;
			CommunityID = parameters.CommunityID;
			SiteID = parameters.SiteID;
			BrandID = parameters.BrandID;
			Value = value;
		}
	}


	[Serializable()]
	public class MemberAttributeOfString
	{
		public int AttributeID;
		public int CommunityID = 0;
		public int SiteID = 0;
		public int BrandID = 0;
		public string Value;

		public MemberAttributeOfString(){ }

		public MemberAttributeOfString(MemberAttributeParameter parameters, string value)
		{
			AttributeID = parameters.AttributeID;
			CommunityID = parameters.CommunityID;
			SiteID = parameters.SiteID;
			BrandID = parameters.BrandID;
			Value = value;
		}
	}


	[Serializable()]
	public class MemberAttributeOfDateTime
	{
		public int AttributeID;
		public int CommunityID = 0;
		public int SiteID = 0;
		public int BrandID = 0;
		public DateTime Value;

		public MemberAttributeOfDateTime(){ }

		public MemberAttributeOfDateTime(MemberAttributeParameter parameters, DateTime value)
		{
			AttributeID = parameters.AttributeID;
			CommunityID = parameters.CommunityID;
			SiteID = parameters.SiteID;
			BrandID = parameters.BrandID;
			Value = value;
		}
	}


	[Serializable()]
	public class MemberAttributeOfBoolean
	{
		public int AttributeID;
		public int CommunityID = 0;
		public int SiteID = 0;
		public int BrandID = 0;
		public bool Value;

		public MemberAttributeOfBoolean(){ }

		public MemberAttributeOfBoolean(MemberAttributeParameter parameters, bool value)
		{
			AttributeID = parameters.AttributeID;
			CommunityID = parameters.CommunityID;
			SiteID = parameters.SiteID;
			BrandID = parameters.BrandID;
			Value = value;
		}
	}


	[Serializable()]
	public class MemberAttributeOfArrayOfString
	{
		public int AttributeID;
		public int CommunityID = 0;
		public int SiteID = 0;
		public int BrandID = 0;
		public string [] Value;

		public MemberAttributeOfArrayOfString(){ }

		public MemberAttributeOfArrayOfString(MemberAttributeParameter parameters, string [] value)
		{
			AttributeID = parameters.AttributeID;
			CommunityID = parameters.CommunityID;
			SiteID = parameters.SiteID;
			BrandID = parameters.BrandID;
			Value = value;
		}
	}

}