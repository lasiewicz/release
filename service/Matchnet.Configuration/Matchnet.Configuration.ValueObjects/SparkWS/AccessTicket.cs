using System;
using System.IO;
using Matchnet;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using System.Runtime.Serialization;

namespace Matchnet.Configuration.ValueObjects.SparkWS
{
	/// <summary>
	/// Represents the settings and state of the access mode of the service client
	/// </summary>
	[Serializable()]
	public class AccessTicket : Matchnet.ICacheable, IByteSerializable, ISerializable 
	{
		private const byte VERSION_001 = 1;
		private const byte VERSION_002 = 2;

		public const string DEFAULT_TICKET = "*** DEFAULT NO IDENTITY! ***";
		public int AccessTicketID;
		/// <summary>
		/// Signales whether this ticket should be allowed at all or not
		/// </summary>
		public bool IsBlocked;

		/// <summary>
		/// The permission roll this client is a memebr of.
		/// </summary>
		/// <remarks>Roll membership is used in declerative security, to only allow certain rolls access to some functions</remarks>
		public ApiAccessRoll AccessRoll;

		/// <summary>
		/// A collection of profile sections that the client can see and use
		/// </summary>
		public ProfileSectionDefinitions ProfileSectionDefinitions;

		/// <summary>
		/// List of remote hosts that are allowed access. If the IP of the remote host is not in this list, the 
		/// ticket would not be allowed.
		/// </summary>
		public string [] RemoteHosts;


		/// <summary>
		/// The GUID key gicen to the client for authentication
		/// </summary>
		private string _Key;

		#region ICachable
		private CacheItemPriorityLevel _CacheItemPriorityLevel = CacheItemPriorityLevel.Normal;
		private int _CachTTLSeconds = 60 * 60;
		private CacheItemMode _CacheItemMode = CacheItemMode.Sliding;
		#endregion

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <remarks>Creates an access ticket with bunk ticket string, blocked state 
		/// with most general access roll.
		/// Methods that permit this ticket are considered permit anonymous user action.
		/// </remarks>
		public AccessTicket()
		{

			_Key = DEFAULT_TICKET;
			IsBlocked = true;
			AccessRoll = ApiAccessRoll.Anyone;
			ProfileSectionDefinitions = new ProfileSectionDefinitions();
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="accessTicket">The key GUID for this ticket</param>
		/// <param name="isBlocked">Flag whether this ticket is blocked or not</param>
		/// <param name="accessRoll">Permission roll for this ticket</param>
		public AccessTicket(string accessTicket, bool isBlocked, ApiAccessRoll accessRoll)
		{
			_Key = accessTicket;
			IsBlocked = isBlocked;
			AccessRoll = accessRoll;
			ProfileSectionDefinitions = new ProfileSectionDefinitions();
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="accessTicketID">The int ID of the access ticket</param>
		/// <param name="accessTicket">The key GUID for this ticket</param>
		/// <param name="isBlocked">Flag whether this ticket is blocked or not</param>
		/// <param name="accessRoll">Permission roll for this ticket</param>
		public AccessTicket(int accessTicketID, string accessTicket, bool isBlocked, ApiAccessRoll accessRoll)
		{
			AccessTicketID = accessTicketID;
			_Key = accessTicket;
			IsBlocked = isBlocked;
			AccessRoll = accessRoll;
			ProfileSectionDefinitions = new ProfileSectionDefinitions();
		}

		/// <summary>
		/// GUID key that represents this ticket.
		/// </summary>
		public string Key {
			get { return _Key; }
			set { _Key = value; }
		}

		#region ICacheable Members

		public Matchnet.CacheItemMode CacheMode
		{
			get { return _CacheItemMode; }
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CacheItemPriorityLevel;
			}
			set
			{
				_CacheItemPriorityLevel = value;
			}
		}

		public int CacheTTLSeconds
		{
			get
			{
				return _CachTTLSeconds;
			}
			set
			{
				_CachTTLSeconds = value;
			}
		}

		public string GetCacheKey()
		{
			return _Key;
		}

		#endregion

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder(256);
			sb.Append(_Key);
			sb.Append("\t");
			sb.Append(AccessRoll);
			sb.Append("\t");
			sb.Append(IsBlocked);
			sb.Append(":\n");
			foreach (DictionaryEntry psd in ProfileSectionDefinitions)
			{
				sb.Append(psd.Value.ToString());
				sb.Append("\n");
			}
			return sb.ToString();
		}


		#region IByteSerializable Members

		public void FromByteArray(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new BinaryReader(ms);
			
			byte version = br.ReadByte();

			switch(version)
			{
				case VERSION_001: 
					this.IsBlocked = br.ReadBoolean();
			
					this.AccessRoll = (ApiAccessRoll) br.ReadInt32();

					int length = br.ReadInt32();
					this.ProfileSectionDefinitions = new ProfileSectionDefinitions();
					for (int i = 0; i < length; i++)
					{
						ProfileSectionDefinition psd = new ProfileSectionDefinition();
						psd.Read(ref br);
						this.ProfileSectionDefinitions[psd.ProfileSectionID] = psd;
					}
			
					length = br.ReadInt32();
					this.RemoteHosts = new string[length];
					for (int i = 0; i < length; i++) 
						this.RemoteHosts[i] = br.ReadString();


					this._Key = br.ReadString();

					break;
				case VERSION_002 : // read the accessticketid and fall through to the rest
					this.AccessTicketID = br.ReadInt32();
					goto case VERSION_001;
				default:
					throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
			}
			
		}


		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream(256);
			BinaryWriter bw = new BinaryWriter(ms);
			
			bw.Write(VERSION_002);

			bw.Write(this.AccessTicketID); // new in version 2
			
			bw.Write(IsBlocked);
			
			bw.Write((int)AccessRoll);
			// TODO:
			// ProfileSectionDefinitions

			bw.Write(ProfileSectionDefinitions.Count);
			foreach( DictionaryEntry de in ProfileSectionDefinitions)
			{
				ProfileSectionDefinition psd = de.Value as ProfileSectionDefinition;
				psd.Write(ref bw);
			}
			
			bw.Write(RemoteHosts.Length);
			for (int i = 0; i < RemoteHosts.Length; i++) 
				bw.Write(RemoteHosts[i]);


			bw.Write(_Key);

			return ms.ToArray();
		}


		#endregion

		#region ISerializable Members
		protected AccessTicket(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray", this.ToByteArray());
		}
		#endregion
	}
}