using System;
using System.Collections;
using System.Collections.Specialized;

namespace Matchnet.Configuration.ValueObjects.SparkWS
{
	/// <summary>
	/// Summary description for ProfileSectionDefinitions.
	/// </summary>
	[Serializable()]
	public class ProfileSectionDefinitions: DictionaryBase, IValueObject
	{
		public ProfileSectionDefinitions()
		{
		}

		public ProfileSectionDefinition this[int key]
		{
			get { return Dictionary[key] as ProfileSectionDefinition; }
			set { Dictionary[key] = value; }
		}
		
		public int Count{
			get { return Dictionary.Count; }
		}
	}
}
