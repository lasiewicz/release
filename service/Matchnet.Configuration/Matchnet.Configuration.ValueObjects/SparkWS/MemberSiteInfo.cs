using System;
using System.Text;

namespace Matchnet.Configuration.ValueObjects.SparkWS
{

	public class MemberSiteInfo
	{
		public int SiteID;
		public DateTime SubscriptionExpirationDate;
	    public bool PassedFraudCheck;
		public MemberSiteInfo() { }
		public MemberSiteInfo(int siteID, DateTime susbcriptionExpirationDate) 
		{
			SiteID = siteID;
			SubscriptionExpirationDate = susbcriptionExpirationDate;
		}

	}
}