using System;
using System.Collections;
using System.Xml.Serialization;

namespace Matchnet.Configuration.ValueObjects.SparkWS
{

	/// <summary>
	/// Represents a collection of member attribute values
	/// </summary>
	[Serializable()]
	[  XmlInclude(typeof(MemberAttributeOfInt32)), 
	XmlInclude(typeof(MemberAttributeOfDateTime)), 
	XmlInclude(typeof(MemberAttributeOfString)), 
	XmlInclude(typeof(MemberAttributeOfBoolean)),
	XmlInclude(typeof(MemberAttributeOfArrayOfString))
	]
	public class MemberAttributes
	{
		private ArrayList _Values;
		public MemberAttributes() { }
		public MemberAttributes(int numberOfItems)
		{
			_Values = new ArrayList(numberOfItems);
		}

		public int Add(object memberAttribute)
		{
			return _Values.Add(memberAttribute);
		}

		public ArrayList Values
		{
			get { return _Values; }
			set { _Values = value; }
		}
	}
}