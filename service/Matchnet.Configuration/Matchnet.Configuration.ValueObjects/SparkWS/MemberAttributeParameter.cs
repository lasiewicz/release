using System;


namespace Matchnet.Configuration.ValueObjects.SparkWS
{

	/// <summary>
	/// Represents the full specific dimensions of a member's attribute
	/// </summary>
	[Serializable()]
	public struct MemberAttributeParameter
	{
		public readonly int AttributeID;
		public readonly AttributeDataType DataType;
		public readonly int CommunityID;
		public readonly int SiteID;
		public readonly int BrandID;
		public readonly int LanguageID;


		public MemberAttributeParameter(int attributeID, AttributeDataType dataType, int communityID, int siteID, int brandID)
			:
			this(attributeID, dataType, communityID, siteID, brandID, 0)
		{
		}

		public MemberAttributeParameter(int attributeID, AttributeDataType dataType, int communityID, int siteID, int brandID, int languageID)
		{
			if (dataType == AttributeDataType.Undefined) throw new Exception("Bunk Attribute Definition in construction of MemberAttributeParameters: " + attributeID.ToString());
			AttributeID = attributeID;
			DataType = dataType;
			CommunityID = communityID;
			SiteID = siteID;
			BrandID = brandID;
			LanguageID = languageID;
		}

	}
}