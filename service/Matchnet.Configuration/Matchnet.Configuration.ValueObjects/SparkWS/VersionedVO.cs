using System;

namespace Matchnet.Configuration.ValueObjects.SparkWS
{

	// <summary>
	/// Base class for versioned value objects.
	/// </summary>
	/// <remarks>All vo classes that cross the wire should inherit this class.</remarks>
	/// <example>
	///     /*** note that inheriting class needs to implement at least the parameterless constructors! ***/
	///     [Serializable]
	///     public class Vo1 : VersionedVOBase
	///     {
	///         /* parameterless constructor that initializes the base with a current version */
	///         public Vo1() : base("0.0.1") { }
	///         /* constructor that forwards the version to the base class.*/
	///         public Vo1(string version) : base(version) { }
	///     }
	/// </example>


	[Serializable()]
	public class VersionedVO
	{
		public string Version;
		public VersionedVO(string version)
		{
			Version = version;
		}
	}
}