using System;
using System.IO;
using Matchnet;
using System.Text;


namespace Matchnet.Configuration.ValueObjects.SparkWS
{

	[Serializable()]
	public class ProfileSectionDefinition : Matchnet.ICacheable
	{
		private int _ProfileSectionID;
		public MemberAttributeParameter[] AttributeDefinitions;
		public int [] SiteIDList;
		public int [] CommunityIDList;
		public int [] PhotoCommunityIDList;

		private string _Key;
		private CacheItemPriorityLevel _CacheItemPriorityLevel = CacheItemPriorityLevel.Normal;
		private int _CachTTLSeconds = 60 * 60;
		private CacheItemMode _CacheItemMode = CacheItemMode.Sliding;


		public ProfileSectionDefinition() { }

		public ProfileSectionDefinition(int profileSectionID, MemberAttributeParameter[] attributeDefinitions)
		{
			_ProfileSectionID = profileSectionID;
			AttributeDefinitions = attributeDefinitions;
			_Key = GetCacheKey(profileSectionID);
		}

		public int ProfileSectionID 
		{
			get { return _ProfileSectionID; }
			set
			{
				_ProfileSectionID = value;
				_Key = GetCacheKey(_ProfileSectionID);
			}
		}
		#region ICacheable Members
		public Matchnet.CacheItemMode CacheMode
		{
			get { return _CacheItemMode; }
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CacheItemPriorityLevel;
			}
			set
			{
				_CacheItemPriorityLevel = value;
			}
		}

		public int CacheTTLSeconds
		{
			get
			{
				return _CachTTLSeconds;
			}
			set
			{
				_CachTTLSeconds = value;
			}
		}

		public string GetCacheKey()
		{
			return _Key;
		}
		#endregion

		public static string GetCacheKey(int profileSectionID)
		{
			return "SWS_PSD_" + profileSectionID.ToString();
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder(256);
			sb.Append(_Key); sb.Append(":\n");
			for (int i = 0; i < AttributeDefinitions.Length; i++)
			{
				sb.Append(AttributeDefinitions[i].AttributeID);
				sb.Append("|");
				sb.Append(AttributeDefinitions[i].DataType);
				sb.Append("|");
				sb.Append(AttributeDefinitions[i].CommunityID);
				sb.Append("|");
				sb.Append(AttributeDefinitions[i].SiteID);
				sb.Append("|");
				sb.Append(AttributeDefinitions[i].BrandID);
				sb.Append("|");
				sb.Append(AttributeDefinitions[i].LanguageID);
				sb.Append("\n");
			}
			return sb.ToString();
		}


		internal void Read(ref BinaryReader br)
		{
			this._ProfileSectionID = br.ReadInt32();

			int length = br.ReadInt32();
			AttributeDefinitions = new MemberAttributeParameter[length]; 
			for (int i = 0; i < length; i++)
			{
				int attributeID = br.ReadInt32();
				AttributeDataType dataType = (AttributeDataType)br.ReadInt32();
				int communityID = br.ReadInt32();
				int siteID = br.ReadInt32();
				int brandID = br.ReadInt32();
				int languageID = br.ReadInt32();
				AttributeDefinitions[i] = new MemberAttributeParameter(attributeID, dataType, communityID, siteID, brandID, languageID);
			}
				
		 
			length = br.ReadInt32();
			SiteIDList = new int[length];
			for (int i = 0; i < length; i++)
				SiteIDList[i] = br.ReadInt32();


			length = br.ReadInt32();
			CommunityIDList = new int[length];
			for (int i = 0; i < length; i++)
				CommunityIDList[i] = br.ReadInt32();


			length = br.ReadInt32();
			PhotoCommunityIDList = new int[length];
			for (int i = 0; i < length; i++)
				PhotoCommunityIDList[i] = br.ReadInt32();

			_Key = br.ReadString();
		}


		internal void Write(ref BinaryWriter bw)
		{

			bw.Write(_ProfileSectionID);

			bw.Write(AttributeDefinitions.Length);
			for (int i = 0; i < AttributeDefinitions.Length; i++)
			{
				bw.Write( AttributeDefinitions[i].AttributeID);
				bw.Write( (int)AttributeDefinitions[i].DataType);
				bw.Write( AttributeDefinitions[i].CommunityID);
				bw.Write( AttributeDefinitions[i].SiteID);
				bw.Write( AttributeDefinitions[i].BrandID);
				bw.Write( AttributeDefinitions[i].LanguageID);	
			}

			bw.Write(SiteIDList.Length);
			for (int i = 0; i < SiteIDList.Length; i ++)
				bw.Write(SiteIDList[i]);

			bw.Write(CommunityIDList.Length);
			for (int i = 0; i < CommunityIDList.Length; i ++)
				bw.Write(CommunityIDList[i]);

			bw.Write(PhotoCommunityIDList.Length);
			for (int i = 0; i < PhotoCommunityIDList.Length; i ++)
				bw.Write(PhotoCommunityIDList[i]);

			bw.Write(_Key);		
		}

	}
}