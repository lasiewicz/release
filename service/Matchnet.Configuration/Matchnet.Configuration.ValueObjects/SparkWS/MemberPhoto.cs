using System;
using System.Data;
using System.Configuration;


namespace Matchnet.Configuration.ValueObjects.SparkWS
{

	/// <summary>
	/// Summary description for MemberPhoto
	/// </summary>
	[Serializable()]
	public class MemberPhoto : VersionedVO
	{
		public int CommunityID;

		/// <summary>
		/// path to photo
		/// </summary>
		public string PhotoURL;
		/// <summary>
		///  path to thumbnail
		/// </summary>
		public string ThumbnailURL;
        /// <summary>
	    /// Full path to photo
	    /// </summary>
	    public string FullPhotoURL;
        /// <summary>
        /// Full path to thumbnail
        /// </summary>
        public string FullThumbnailURL;
        /// <summary>
        /// Photo marked as private. view by invitation only.
        /// </summary>
        /// 
		public bool IsPrivate;
		/// <summary>
		/// Member allows photo to be viewed by non members (guests)
		/// </summary>
		public bool IsMembersOnly;

		/// <summary>
		/// The order of the photo, relative to other photos for this community
		/// </summary>
		public int ListOrder;

		public MemberPhoto()
			: base("1.0.0")
		{

		}

		public MemberPhoto(int communityID, string photoURL, string thumbnailURL, bool isPrivate, bool isMembersOnly, int listOrder)
			: this()
		{
			CommunityID = communityID;
			PhotoURL = photoURL;
			ThumbnailURL = thumbnailURL;
			IsPrivate = isPrivate;
			IsMembersOnly = isMembersOnly;
			ListOrder = listOrder;
		}

        public MemberPhoto(int communityID, string photoURL, string thumbnailURL, bool isPrivate, bool isMembersOnly, 
            int listOrder, string fullPhotoURL, string fullThumbnailURL): 
                this(communityID, photoURL, thumbnailURL, isPrivate, isMembersOnly, listOrder)
        {
            FullPhotoURL = fullPhotoURL;
            FullThumbnailURL = fullThumbnailURL;
        }
	}
}