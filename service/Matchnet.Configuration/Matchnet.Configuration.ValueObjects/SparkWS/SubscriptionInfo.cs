using System;
using Matchnet;


namespace Matchnet.Configuration.ValueObjects.SparkWS
{
	/// <summary>
	/// Summary description for SubscriptionInfo.
	/// </summary>

	[Serializable()]
	public class SubscriptionInfo :  Matchnet.ICacheable
	{
		private const string  CACHE_KEY_PREFIX = "ACCESSTICKET_SUBSCRIPTIONINFO_";
		private int _AccessTicketID;
		private int _SubscriptionInfoID;
		private int _AdminMemberID;
		private int _Duration;
		private int _DurationTypeID;
		private int _TranTypeID;
		private int _TransactionReasonID;
		private  bool _ReOpenFlag;
		private int _PlanID;

		public int AdminMemberID
		{
			get { return _AdminMemberID; }
			set { _AdminMemberID = value; } 
		}
		public int Duration
		{
			get { return _Duration; }
			set { _Duration = value; }
		}
		public int DurationTypeID
		{
			get { return _DurationTypeID; }
			set { _DurationTypeID = value; }
		}
		public int TranTypeID
		{
			get { return _TranTypeID; }
			set { _TranTypeID = value; }
		}
		public int TransactionReasonID
		{
			get { return _TransactionReasonID; }
			set { _TransactionReasonID =value; }
		}
		public bool ReOpenFlag
		{
			get { return _ReOpenFlag; }
			set { _ReOpenFlag = value; }
		}
		public int PlanID
		{
			get { return _PlanID; }
			set { _PlanID = value; }
		}
	
		public SubscriptionInfo()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public SubscriptionInfo(int accessTicketID, int subscriptionInfoID)
		{
			_AccessTicketID = accessTicketID;
			_SubscriptionInfoID = subscriptionInfoID;
		}

		#region ICachable
		private CacheItemPriorityLevel _CacheItemPriorityLevel = CacheItemPriorityLevel.Normal;
		private int _CachTTLSeconds = 60 * 60;
		private CacheItemMode _CacheItemMode = CacheItemMode.Sliding;
		#endregion

		#region ICacheable Members

		public Matchnet.CacheItemMode CacheMode
		{
			get { return _CacheItemMode; }
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CacheItemPriorityLevel;
			}
			set
			{
				_CacheItemPriorityLevel = value;
			}
		}

		public int CacheTTLSeconds
		{
			get
			{
				return _CachTTLSeconds;
			}
			set
			{
				_CachTTLSeconds = value;
			}
		}

		public string GetCacheKey()
		{
			return GetCacheKey(_AccessTicketID, _SubscriptionInfoID);
		}
		public static string GetCacheKey(int accessTicketID, int subscriptionInfoID)
		{
			return CACHE_KEY_PREFIX+accessTicketID.ToString()+"_" + subscriptionInfoID.ToString();
		}

		#endregion
	}
}