﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ValueObjects.Management;
namespace Matchnet.Configuration.ValueObjects.ServiceDefinitions
{
    public interface IManagementService
    {

        Settings GetSettings();
        void SaveSettings(List<SettingsSave> settingslist, bool async);
        List<Server> GetServers();

    }
}
