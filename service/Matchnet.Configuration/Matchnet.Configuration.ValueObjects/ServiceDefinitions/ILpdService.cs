using System;

using Matchnet.Configuration.ValueObjects.Lpd;


namespace Matchnet.Configuration.ValueObjects.ServiceDefinitions
{
	public interface ILpdService
	{
		Guid GetVersion();

		LogicalDatabases GetLogicalDatabases();
	}
}
