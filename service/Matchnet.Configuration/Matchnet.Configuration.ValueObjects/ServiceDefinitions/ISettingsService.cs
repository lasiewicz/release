using System;
using System.Collections.Generic;
using System.Collections.Specialized;

using Matchnet.Configuration.ValueObjects;


namespace Matchnet.Configuration.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// 
	/// </summary>
	public interface ISettingsService
	{

		/// <summary>
		/// 
		/// </summary>
		/// <param name="machineName"></param>
		/// <returns></returns>
		NameValueCollection GetSettings(string machineName);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="serverName"></param>
		/// <returns></returns>
		bool IsServerEnabled(string serverName);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="serviceConstant"></param>
		/// <param name="machineName"></param>
		/// <returns></returns>
		ServiceInstanceConfig GetServiceInstanceConfig(string serviceConstant, string machineName);

        /// <summary>
        /// Returns membase configs
        /// </summary>
        /// <returns>list of membase config objects</returns>
        List<MembaseConfig> GetMembaseConfigs();

	    /// <summary>
	    /// Method to return membase config given a bucket name
	    /// </summary>
	    /// <param name="bucketName"></param>
	    /// <returns></returns>
	    MembaseConfig GetMembaseConfigByBucket(string bucketName);

        /// <summary>
        /// Return API App specific settings
        /// </summary>
        /// <returns></returns>
	    List<ApiAppSetting> GetApiAppSettings();

        /// <summary>
        /// Creates or updates a new setting.
        /// </summary>
        /// <param name="settingConstant"></param>
        /// <param name="globalDefaultValue"></param>
        /// <param name="settingDescription"></param>
        /// <param name="settingCategoryId"></param>
        /// <param name="isRequiredForCommunity"></param>
        /// <param name="isRequiredForSite"></param>
	    void AddNewOrUpdateSetting(string settingConstant, string globalDefaultValue, string settingDescription, int settingCategoryId = 2, bool isRequiredForCommunity = false, bool isRequiredForSite = false);
        
        /// <summary>
        /// Update global default for an existing setting.
        /// </summary>
        /// <param name="settingConstant"></param>
        /// <param name="globalDefaultValue"></param>
	    void UpdateGlobalDefault(string settingConstant, string globalDefaultValue);

        /// <summary>
        /// Set value for site setting.
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="settingConstant"></param>
        /// <param name="globalDefaultValue"></param>
	    void SetValueForSite(int siteId, string settingConstant, string globalDefaultValue);

        /// <summary>
        /// Remove all site specific settings.
        /// </summary>
        /// <param name="settingConstant"></param>
	    void ResetToGlobalDefault(string settingConstant);
	}
}
