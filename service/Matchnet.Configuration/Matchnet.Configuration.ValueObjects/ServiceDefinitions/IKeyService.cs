using System;

namespace Matchnet.Configuration.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// 
	/// </summary>
	public interface IKeyService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="keyName"></param>
		/// <returns></returns>
		Int32 GetKey(string keyName);
	}
}
