using System;

using Matchnet.Configuration.ValueObjects;

namespace Matchnet.Configuration.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// 
	/// </summary>
	public interface IAdapterConfigurationService
	{

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		ServicePartitions GetAll();
	
	}
}
