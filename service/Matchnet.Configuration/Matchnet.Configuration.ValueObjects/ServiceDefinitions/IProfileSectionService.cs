using System;
using System.Text;
using Matchnet.Configuration.ValueObjects.SparkWS;

namespace Matchnet.Configuration.ValueObjects.ServiceDefinitions
{
   public interface IProfileSectionService
   {
	  ProfileSectionDefinition GetProfileSectionDefinition(int profileSectionID);
	  //Dictionary<int, ProfileSectionDefinition> GetProfileSectionDefinitions(int[] profileSectionIDs);
	  ProfileSectionDefinitions GetProfileSectionDefinitions(int[] profileSectionIDs);
   }


}
