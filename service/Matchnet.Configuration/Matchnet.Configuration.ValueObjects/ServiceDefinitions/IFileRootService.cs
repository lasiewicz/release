using System;
using System.Collections.Specialized;

using Matchnet.Configuration.ValueObjects.File;


namespace Matchnet.Configuration.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// 
	/// </summary>
	public interface IFileRootService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		RootGroups GetRootGroups();
	}
}
