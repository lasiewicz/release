using System;
using System.Text;

using Matchnet.Configuration.ValueObjects.SparkWS;

namespace Matchnet.Configuration.ValueObjects.ServiceDefinitions
{
   public interface IAccessTicketService
   {
	  AccessTicket GetAccessTicket(string ticketKey);
		
	  SubscriptionInfo GetSubscriptionInfo(int AccessTicketID, int SubscriptionInfoID);
   }
}
