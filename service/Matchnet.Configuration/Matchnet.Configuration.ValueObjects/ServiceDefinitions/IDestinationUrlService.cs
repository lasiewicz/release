using System;
using System.Collections.Generic;
using Matchnet.Configuration.ValueObjects.Urls;


namespace Matchnet.Configuration.ValueObjects.ServiceDefinitions
{
   public interface IDestinationUrlService
   {
	  DestinationUrls GetDestinationUrls();
      List<DestinationURL> GetListOfDestinationURLs();
   }

}
