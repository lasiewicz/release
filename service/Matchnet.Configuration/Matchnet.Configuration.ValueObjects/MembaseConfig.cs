﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Matchnet.Configuration.ValueObjects
{
    [Serializable]
    public class MembaseConfig : ISerializable, IValueObject, ICacheable
    {
        private string _bucketName;
        private string _bucketPassword;
        private int _retryCount;
        private int _retryTimeout;
        private int _socketPoolConnectionTimeout;
        private int _socketPoolDeadTimeout;
        private int _socketPoolQueueTimeout;
        private int _socketPoolReceiveTimeout;
        private int _socketPoolMaxPoolSize;
        private int _socketPoolMinPoolSize;
        private int _objectTimeToLiveInSeconds;
        private List<MembaseConfigServer> _servers = new List<MembaseConfigServer>();

        public string BucketName
        {
            get { return _bucketName; }
        }

        public string BucketPassword
        {
            get { return _bucketPassword; }
        }


        public int RetryCount
        {
            get { return _retryCount; }
        }

        public int RetryTimeout
        {
            get { return _retryTimeout; }
        }

        public int SocketPoolConnectionTimeout
        {
            get { return _socketPoolConnectionTimeout; }
        }

        public int SocketPoolDeadTimeout
        {
            get { return _socketPoolDeadTimeout; }
        }

        public int SocketPoolQueueTimeout
        {
            get { return _socketPoolQueueTimeout; }
        }

        public int SocketPoolReceiveTimeout
        {
            get { return _socketPoolReceiveTimeout; }
        }

        public int SocketPoolMaxPoolSize
        {
            get { return _socketPoolMaxPoolSize; }
        }

        public int SocketPoolMinPoolSize
        {
            get { return _socketPoolMinPoolSize; }
        }

        public void AddServer(MembaseConfigServer server)
        {
            this._servers.Add(server);
        }

        public List<MembaseConfigServer> Servers
        {
            get { return _servers; }
        }

        public int ObjectTimeToLiveInSeconds
        {
            get { return _objectTimeToLiveInSeconds; }
        }

        public MembaseConfig(string bucketName,
            string bucketPassword,
            int retryCount,
            int retryTimeout,
            int socketPoolConnectionTimeout,
            int socketPoolDeadTimeout,
            int socketPoolQueueTimeout,
            int socketPoolReceiveTimeout,
            int socketPoolMaxPoolSize,
            int socketPoolMinPoolSize,
            int objectTimeToLiveInSeconds)
        {
            this._bucketName = bucketName;
            this._bucketPassword = bucketPassword;
            this._retryCount = retryCount;
            this._retryTimeout = retryTimeout;
            this._socketPoolConnectionTimeout = socketPoolConnectionTimeout;
            this._socketPoolDeadTimeout = socketPoolDeadTimeout;
            this._socketPoolQueueTimeout = socketPoolQueueTimeout;
            this._socketPoolReceiveTimeout = socketPoolReceiveTimeout;
            this._socketPoolMaxPoolSize = socketPoolMaxPoolSize;
            this._socketPoolMinPoolSize = socketPoolMinPoolSize;
            this._objectTimeToLiveInSeconds = objectTimeToLiveInSeconds;
        }

        public MembaseConfig(SerializationInfo info, StreamingContext context)
        {
            try
            {
                this._bucketName = info.GetString("_bucketName");
                this._bucketPassword = info.GetString("_bucketPassword");
                this._retryCount = info.GetInt32("_retryCount");
                this._retryTimeout = info.GetInt32("_retryTimeout");
                this._socketPoolConnectionTimeout = info.GetInt32("_socketPoolConnectionTimeout");
                this._socketPoolDeadTimeout = info.GetInt32("_socketPoolDeadTimeout");
                this._socketPoolQueueTimeout = info.GetInt32("_socketPoolQueueTimeout");
                this._socketPoolReceiveTimeout = info.GetInt32("_socketPoolReceiveTimeout");
                this._socketPoolMaxPoolSize = info.GetInt32("_socketPoolMaxPoolSize");
                this._socketPoolMinPoolSize = info.GetInt32("_socketPoolMinPoolSize");
                this._objectTimeToLiveInSeconds = info.GetInt32("_objectTimeToLiveInSeconds");
                this._servers = info.GetValue("_servers",typeof(List<MembaseConfigServer>)) as List<MembaseConfigServer>;
            }
            catch (Exception ex)
            {
                ConfigServiceTrace.Instance.DebugTrace(ServiceConstants.SERVICE_NAME, ex, this);
            }
        }

        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            try
            {
                info.AddValue("_bucketName", this._bucketName);
                info.AddValue("_bucketPassword", this._bucketPassword);
                info.AddValue("_retryCount", this._retryCount);
                info.AddValue("_retryTimeout", this._retryTimeout);
                info.AddValue("_socketPoolConnectionTimeout", this._socketPoolConnectionTimeout);
                info.AddValue("_socketPoolDeadTimeout", this._socketPoolDeadTimeout);
                info.AddValue("_socketPoolQueueTimeout", this._socketPoolQueueTimeout);
                info.AddValue("_socketPoolReceiveTimeout", this._socketPoolReceiveTimeout);
                info.AddValue("_socketPoolMaxPoolSize", this._socketPoolMaxPoolSize);
                info.AddValue("_socketPoolMinPoolSize", this._socketPoolMinPoolSize);
                info.AddValue("_objectTimeToLiveInSeconds", this._objectTimeToLiveInSeconds);
                info.AddValue("_servers", this._servers);
            }
            catch (Exception ex)
            {
                ConfigServiceTrace.Instance.DebugTrace(ServiceConstants.SERVICE_NAME, ex, this);
            }
        }

        #endregion

        #region ICacheable Members

        private int _cacheTTLSeconds = 540;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEYFORMAT = "MembaseConfigbyBucket";

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public string GetCacheKey()
        {
            return GetCacheKeyString();
        }

        public static string GetCacheKeyString()
        {
            return CACHEKEYFORMAT;
        }


        #endregion
    }

    [Serializable]
    public class MembaseConfigServer : ISerializable, IValueObject
    {
        private string _serverIP;
        private int _serverPort;

        public string ServerIP
        {
            get { return _serverIP; }
        }

        public int ServerPort
        {
            get { return _serverPort; }
        }

        public MembaseConfigServer(string serverIP, int serverPort)
        {
            this._serverIP = serverIP;
            this._serverPort = serverPort;
        }

        public MembaseConfigServer(SerializationInfo info, StreamingContext context)
        {
            try
            {
                this._serverIP = info.GetString("_serverIP");
                this._serverPort = info.GetInt32("_serverPort");
            }
            catch (Exception ex)
            {
                ConfigServiceTrace.Instance.DebugTrace(ServiceConstants.SERVICE_NAME, ex, this);
            }
        }

        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            try
            {
                info.AddValue("_serverIP", this._serverIP);
                info.AddValue("_serverPort", this._serverPort);
            }
            catch (Exception ex)
            {
                ConfigServiceTrace.Instance.DebugTrace(ServiceConstants.SERVICE_NAME, ex, this);
            }
        }

        #endregion
    }
}
