using System;

namespace Matchnet.Configuration.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class ServiceInstance
	{
		private string _hostName;
		private bool _isEnabled;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="hostName"></param>
		/// <param name="isEnabled"></param>
		public ServiceInstance(string hostName, bool isEnabled)
		{
			_hostName = hostName;
			_isEnabled = isEnabled;
		}


		/// <summary>
		/// 
		/// </summary>
		public string HostName
		{
			get
			{
				return _hostName;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public bool IsEnabled
		{
			get
			{
				return _isEnabled;
			}
		}
	}
}
