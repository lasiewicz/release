using System;
using System.Collections;

using Matchnet;

namespace Matchnet.Configuration.ValueObjects
{

	/// <summary>
	/// Represents a single Partition which belongs to a given Service.
	/// </summary>
	[Serializable]
	public class ServicePartition : CollectionBase, IValueObject, ICloneable
	{
		private short _partitionOffset = 0;
		private string _constant = Constants.NULL_STRING;
		private string _hostname = Constants.NULL_STRING;
		private Int32 _port = Constants.NULL_INT;
		
		/// <summary>
		/// Constructs a new instance of the ServicePartition object for the specified serviceConstant and partition
		/// </summary>
		/// <param name="serviceConstant"></param>
		/// <param name="partitionOffset"></param>
		/// <param name="hostname"></param>
		/// <param name="port"></param>
		public ServicePartition(string serviceConstant, short partitionOffset, string hostname, Int32 port)
		{
			_constant = serviceConstant;
			_partitionOffset = partitionOffset;
			_hostname = hostname;
			_port = port;
		}
		

		/// <summary>
		/// 
		/// </summary>
		public ServiceInstance this[int index]
		{
			get
			{
				return (ServiceInstance)base.InnerList[index];
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="serviceInstance"></param>
		/// <returns></returns>
		public int Add(ServiceInstance serviceInstance)
		{
			return base.InnerList.Add(serviceInstance);
		}

		
		/// <summary>
		/// The constant that represents the service that this ServicePartition belongs to.
		/// </summary>
		public string Constant
		{
			get{return _constant;}
		}

		/// <summary>
		/// The offset of this ServicePartition.
		/// </summary>
		public int PartitionOffset
		{
			get{return _partitionOffset;}
		}


		/// <summary>
		/// The port number that the service is listening on.
		/// </summary>
		public int Port
		{
			get
			{
				return _port;
			}
		}

		/// <summary>
		/// hostname
		/// </summary>
		public string HostName
		{
			get
			{
				return _hostname;
			}
			set
			{
				_hostname = value;
			}
		}
		

		/// <summary>
		/// Builds a fully qualified service URI.  For example: "tcp://serviceUri:port/serviceManagerName.rem"
		/// </summary>
		/// <param name="serviceManagerName"></param>
		/// <returns></returns>
		public string ToUri(string serviceManagerName)
		{
			string hostname = _hostname;
			string serviceUri = "tcp://" + hostname + ":" + _port + "/" + serviceManagerName + ".rem";
			return serviceUri;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public object Clone()
		{
			return (ServicePartition)this.MemberwiseClone();
		}
	}
}

