using System;
using System.Collections;

using Matchnet;

namespace Matchnet.Configuration.ValueObjects.File
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class RootGroups : IValueObject, ICacheable
	{
		/// <summary>
		/// 
		/// </summary>
		public const string CACHE_KEY = "^RootGroups";

		private Hashtable _idTable;
		private Hashtable _rootIDTable;
		private Hashtable _pathTable;
		private ArrayList _writableList;

		/// <summary>
		/// 
		/// </summary>
		public RootGroups()
		{
			_idTable = new Hashtable();
			_rootIDTable = new Hashtable();
			_pathTable = new Hashtable();
			_writableList = new ArrayList();
		}


		public Hashtable Groups
		{
			get
			{
				return _idTable;
			}
		}


		public void Sort()
		{
			IDictionaryEnumerator de = _idTable.GetEnumerator();

			while (de.MoveNext())
			{
				ArrayList rootList = de.Value as ArrayList;
				rootList.Sort();				
			}
		}
		
		public string[] GetInstancePaths(Int32 fileRootID)
		{						
			ArrayList instances = _idTable[fileRootID] as ArrayList;						
			string[] paths = (string[])Array.CreateInstance(typeof(string), instances.Count);

			for (Int32 instanceNum = 0; instanceNum < instances.Count; instanceNum++)
			{
				paths[instanceNum] = (instances[instanceNum] as Root).Path;
			}

			return paths;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <param name="rootGroupID"></param>
		/// <param name="isParent"></param>
		/// <param name="isWritable"></param>
		/// <param name="path"></param>
		/// <param name="webPath"></param>
		public void AddRoot(Int32 id,
			Int32 rootGroupID,
			bool isParent,
			bool isWritable,
			string path,
			string webPath)
		{
			ArrayList list = _idTable[rootGroupID] as ArrayList;

			if (list == null)
			{
				list = new ArrayList();
			}

			Root root = new Root(id,
				rootGroupID,
				isParent,
				isWritable,
				path,
				webPath);
			list.Add(root);
			_rootIDTable[id] = root;
			_idTable[rootGroupID] = list;

			if (isParent)
			{
				_pathTable[path.ToLower()] = list;
				if (isWritable)
				{
					_writableList.Add(root);
				}
			}
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="rootID"></param>
		/// <returns></returns>
		public Root GetRoot(Int32 rootID)
		{
			return _rootIDTable[rootID] as Root;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public Root GetWritableRoot()
		{
			Int32 count = _writableList.Count;

			if (count == 0)
			{
				throw new Exception("No writable roots found.");
			}


			return _writableList[(Int32)(Math.Floor(count * (new Random((int)DateTime.Now.Ticks).NextDouble())))] as Root;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public string[] GetChildPaths(string filePath)
		{
			Int32[] rootIDs;
			return GetChildPaths(filePath, out rootIDs);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="rootIDs"></param>
		/// <returns></returns>
		public string[] GetChildPaths(string filePath,
			out Int32[] rootIDs)
		{
			string workingPath = filePath;

			if (workingPath.IndexOf(@"\\") == 0)
			{
				workingPath = workingPath.Substring(2);
			}
			string[] pathParts = workingPath.Split('\\');
			string currentPath = @"\\";
			ArrayList list = null;

			for (Int32 pathPartNum = 0; pathPartNum < pathParts.Length; pathPartNum++)
			{
				currentPath = currentPath + pathParts[pathPartNum];
				if (pathPartNum != pathParts.Length - 1)
				{
					currentPath = currentPath + @"\";
				}

				if (list == null)
				{
					list = _pathTable[currentPath.ToLower()] as ArrayList;
					if (list != null)
					{
						currentPath = "";
					}
				}
			}

			if (list == null)
			{
				throw new Exception("Path " + filePath + " not recognized as having valid root.");
			}

			ArrayList resultList = new ArrayList();
			ArrayList resultListIDs = new ArrayList();

			for (Int32 rootNum = 0; rootNum < list.Count; rootNum++)
			{
				Root root = list[rootNum] as Root;

				if (!root.IsParent)
				{
					resultList.Add(root.Path + currentPath);
					resultListIDs.Add(root.ID);
				}
			}

			if (resultList.Count == 0)
			{
				throw new Exception("No replication roots found for path " + filePath + ".");
			}

			rootIDs = (Int32[])resultListIDs.ToArray(typeof(Int32));
			return (string[])resultList.ToArray(typeof(string));
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return 30;
			}
			set
			{
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return Matchnet.CacheItemMode.Absolute;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return Matchnet.CacheItemPriorityLevel.Normal;
			}
			set
			{
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return CACHE_KEY;
		}
		#endregion
	}
}
