using System;

namespace Matchnet.Configuration.ValueObjects.File
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class Root : IComparable
	{
		private Int32 _id;
		private Int32 _rootGroupID;
		private bool _isParent;
		private bool _isWritable;
		private string _path;
		private string _webPath;

		public Root(Int32 id,
			Int32 rootGroupID,
			bool isParent,
			bool isWritable,
			string path,
			string webPath)
		{
			_id = id;
			_rootGroupID = rootGroupID;
			_isParent = isParent;
			_isWritable = isWritable;
			_path = path;
			_webPath = webPath;
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 ID
		{
			get
			{
				return _id;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 RootGroupID
		{
			get
			{
				return _rootGroupID;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public bool IsParent
		{
			get
			{
				return _isParent;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public bool IsWritable
		{
			get
			{
				return _isWritable;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string Path
		{
			get
			{
				return _path;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string WebPath
		{
			get
			{
				return _webPath;
			}
		}

		#region IComparable Members
		public int CompareTo(object obj)
		{
			Root root = obj as Root;

			if (root != null)
			{
				if (root.IsParent && !this.IsParent)
				{
					return 1;
				}
				else if (!root.IsParent && this.IsParent)
				{
					return -1;
				}
				else
				{
					if (root.ID > this.ID)
					{
						return -1;
					}
					else
					{
						return 1;
					}
				}
			}

			return 1;
		}
		#endregion
	}
}
