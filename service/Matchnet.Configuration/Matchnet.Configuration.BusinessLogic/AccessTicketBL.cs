using System;
using System.Text;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;
using System.Data;

using Matchnet.Configuration.ValueObjects.SparkWS;

using Matchnet.Exceptions;
using Matchnet.Data;
using Matchnet.Data.Configuration;
using Matchnet.Data.Exceptions;
using Matchnet.Data.Hydra;
using System.Collections.Specialized;

namespace Matchnet.Configuration.BusinessLogic
{
	public class AccessTicketBL : IAccessTicketService
	{
		public static readonly AccessTicketBL Instance = new AccessTicketBL();

		private AccessTicketBL() { }

		#region IAccessTicketService Members

		public AccessTicket GetAccessTicket(string ticketKey)
		{
			AccessTicket result;
			result = getAccessTicketFromDB(ticketKey);

			return result;

		}

		private AccessTicket getAccessTicketFromDB(string ticketKey)
		{
			AccessTicket result = null;
			if (ticketKey == AccessTicket.DEFAULT_TICKET)
				result = new AccessTicket();
			else
			{
				Command command;
				DataSet ds = null;
				DataTable tbl = null;
				try
				{
					command = new Command("mnSystem", "up_AccessTicket_Get", 0);
					command.Parameters.Add(new Parameter("@TicketKey", SqlDbType.VarChar, ParameterDirection.Input, ticketKey));

					ds = Matchnet.Data.Client.Instance.ExecuteDataSet(command);
					tbl = ds.Tables[0];

					if (tbl != null && tbl.Rows.Count == 1)
					{
						ApiAccessRoll roll = (ApiAccessRoll)tbl.Rows[0]["AccessRoll"];
						bool isBlocked = (bool)tbl.Rows[0]["IsBlockedFlag"];
						int accessTicketID = (int)tbl.Rows[0]["AccessTicketID"];

						result = new AccessTicket(accessTicketID, ticketKey, isBlocked, roll);

						tbl = ds.Tables[1]; // profile section IDs
						if (tbl != null && tbl.Rows.Count > 0)
						{
							int[] profileSectionIDs = new int[tbl.Rows.Count];

							for (int i = 0; i < tbl.Rows.Count; i++)
							{
								profileSectionIDs[i] = (int)tbl.Rows[i]["ProfileSectionID"];
							}

							result.ProfileSectionDefinitions = ProfileSectionBL.Instance.GetProfileSectionDefinitions(profileSectionIDs);
						}


						tbl = ds.Tables[2]; // allowed remote hosts
						if (tbl != null && tbl.Rows.Count > 0)
						{
							string [] remoteHosts = new string[tbl.Rows.Count];

							for (int i = 0; i < tbl.Rows.Count; i++)
							{
								remoteHosts[i] = tbl.Rows[i]["RemoteHost"].ToString().Replace("*","");
							}

							result.RemoteHosts = remoteHosts;
						}
						else 
						{
							result.RemoteHosts = new string[] {};
						}

					}
				}
				catch (Exception ex)
				{
					throw new Matchnet.Exceptions.BLException("AccessTicketBL.gatAccessTicketFromDB failed", ex);
				}
				finally
				{
					tbl = null;
					if (ds != null)
						ds.Dispose();
				}

			}
			return result;
		}

		public SubscriptionInfo GetSubscriptionInfo(int AccessTicketID, int SubscriptionInfoID)
		{
			SubscriptionInfo result=null;
			
			object obj = Caching.Cache.Instance.Get(SubscriptionInfo.GetCacheKey(AccessTicketID, SubscriptionInfoID));
			if (obj != null) result = obj as SubscriptionInfo;
			if (result == null)
			{
				result = getSubscriptionInfoFromDB(AccessTicketID, SubscriptionInfoID);
				if (result != null)
				{
					Caching.Cache.Instance.Add(result);
				}
				else
				{
					///TODO: Protect against multi calls. If failed to get object, complain and  cache an empty one?
					new Matchnet.Exceptions.ServiceBoundaryException("AccessTicketSM", "Can't get SubscriptionInfo " +AccessTicketID.ToString()+"_"+ SubscriptionInfoID.ToString());
				}
			}
			
			return result;
		}
		private SubscriptionInfo getSubscriptionInfoFromDB(int AccessTicketID, int SubscriptionInfoID)
		{
			SubscriptionInfo result = null;
			Command command;
			DataTable dt = null;
			
			try
			{
				command = new Command("mnSystem", "up_AccessTicketSubscriptionInfo_Get", 0);
				command.Parameters.Add(new Parameter("@AccessTicketID", SqlDbType.Int, ParameterDirection.Input, (object)AccessTicketID));
				command.Parameters.Add(new Parameter("@SubscriptionInfoID", SqlDbType.Int, ParameterDirection.Input, (object)SubscriptionInfoID));
				dt = Matchnet.Data.Client.Instance.ExecuteDataTable(command);
				if(dt.Rows.Count>0)
				{
					result = new SubscriptionInfo(AccessTicketID, SubscriptionInfoID);
					result.AdminMemberID = (int)dt.Rows[0]["AdminMemberID"];
					result.Duration = (int)dt.Rows[0]["Duration"];
					result.DurationTypeID = (int)dt.Rows[0]["DurationTypeID"];
					result.TranTypeID = (int)dt.Rows[0]["TranTypeID"];
					result.TransactionReasonID = (int)dt.Rows[0]["TransactionReasonID"];
					result.ReOpenFlag  = (bool)dt.Rows[0]["ReOpenFlag"];
					result.PlanID = (int)dt.Rows[0]["PlanID"];
				}
			}
			catch (Exception ex)
			{
				throw new Matchnet.Exceptions.BLException("AccessTicketBL.getSubscriptionInfoFromDB failed", ex);
			}
			return result;
		}

		#endregion
	}
}
