using System;
using System.Data;

using Matchnet.Data;
using Matchnet.Caching;
using Matchnet.Configuration.ValueObjects.File;


namespace Matchnet.Configuration.BusinessLogic
{
	public class FileRootBL
	{
		public static readonly FileRootBL Instance = new FileRootBL();

		private Matchnet.Caching.Cache _cache;

		private FileRootBL()
		{
			_cache = Matchnet.Caching.Cache.Instance;
		}

		public RootGroups GetRootGroups()
		{
			RootGroups rootGroups = _cache.Get(RootGroups.CACHE_KEY) as RootGroups;

			if (rootGroups == null)
			{
				rootGroups = new RootGroups();
				Command command = new Command("mnFile", "dbo.up_Root_List", 0);
				DataTable dt = Client.Instance.ExecuteDataTable(command, LpdBL.Instance.GetLogicalDatabases());

				for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
				{
					DataRow row = dt.Rows[rowNum];
					rootGroups.AddRoot(Convert.ToInt32(row["RootID"]),
						Convert.ToInt32(row["RootGroupID"]),
						Convert.ToBoolean(row["ParentFlag"]),
						Convert.ToBoolean(row["AppendFlag"]),
						row["Path"].ToString(),
						row["WebPath"].ToString());
				}

				rootGroups.Sort();

				_cache.Add(rootGroups);
			}

			return rootGroups;
		}
	}
}
