using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Diagnostics;

using Matchnet;
using Matchnet.Data;
using Matchnet.Exceptions;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.Lpd;


namespace Matchnet.Configuration.BusinessLogic
{
	public class LpdBL : Matchnet.IBackgroundProcessor
	{
		private const Int32 SERVICE_START_TIME = 30000;
		private const string REFRESH_INTERNAL= "Refresh_Interval";
		private const string SETTING_CONNECTIONSTRINGPRIMARY = "ConnectionStringPrimary";
		private const string SETTING_CONNECTIONSTRINGSECONDARY = "ConnectionStringSecondary";
		private const string SETTING_CONNECTIONSTRINGTERTIARY = "ConnectionStringTertiary";
		private const string LOGICALDB_MNSYSTEM = "mnSystem";

		private Guid _version = Guid.Empty;
		private ReaderWriterLock _lock;
		private Thread _worker = null;
		private bool _runnable = true;
		private LogicalDatabases _logicalDatabases = null;
		private bool _newDBRead;

		public static readonly LpdBL Instance = new LpdBL();

		private LpdBL()
		{
			_lock = new ReaderWriterLock();

			Trace.Listeners.Add(new EventLogTraceListener(Matchnet.Configuration.ValueObjects.ServiceConstants.SERVICE_NAME));
		}


		public void Start()
		{
			lock (this)
			{
				refreshLpd();

				if (_worker == null)
				{
					_worker = new Thread(new ThreadStart(workCycle));
					_runnable = true;
					_worker.Start();
				}
			}
		}


		public void Stop()
		{
			lock (this)
			{
				if (_worker != null)
				{
					_runnable = false;
					_worker.Join();
					_worker = null;
				}
				else
				{
					throw new Exception("Work cycle is not running.");
				}
			}
		}


		public Guid GetVersion()
		{
			_lock.AcquireReaderLock(-1);
			try
			{
				return _version;
			}
			finally
			{
				_lock.ReleaseLock();
			}
		}


		public LogicalDatabases GetLogicalDatabases()
		{
			_lock.AcquireReaderLock(-1);
			try
			{
				if (_logicalDatabases == null)
				{
					new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "_logicalDatabases was null.");
				}

				return _logicalDatabases;
			}
			finally
			{
				_lock.ReleaseLock();
			}
		}


		private void workCycle()
		{
			Thread.Sleep(SERVICE_START_TIME); //Give the service time to start completely before beginning the work cycle

			while (_runnable)
			{
				Thread.Sleep(Convert.ToInt16(System.Configuration.ConfigurationSettings.AppSettings[REFRESH_INTERNAL]));

				try
				{
					refreshLpd();
				}
				catch (Exception ex)
				{
					new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
						"Error refreshing LPD.",
						ex);
				}
			}
		}


		private void refreshLpd()
		{
			string connectionString = null;
			Guid version;

			try
			{
				if (_logicalDatabases == null)
				{
					#region Get a connection string

					connectionString = System.Configuration.ConfigurationSettings.AppSettings[SETTING_CONNECTIONSTRINGPRIMARY];
					try
					{
						version = getVersion(connectionString);
					}
					catch (Exception ex)
					{
						new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
							"Error loading LPD version from ConnectionStringPrimary (" + connectionString + ").",
							ex);

						connectionString = System.Configuration.ConfigurationSettings.AppSettings[SETTING_CONNECTIONSTRINGSECONDARY];
						try
						{
							version = getVersion(connectionString);
						}
						catch (Exception ex2)
						{
							new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
								"Error loading LPD version from ConnectionStringSecondary (" + connectionString + ").",
								ex2);

							connectionString = System.Configuration.ConfigurationSettings.AppSettings[SETTING_CONNECTIONSTRINGTERTIARY];
							version = getVersion(connectionString);
						}
					}

					#endregion
				}
				else
				{
					connectionString = _logicalDatabases[LOGICALDB_MNSYSTEM][0].PhysicalDatabases.GetRandomInstance().ConnectionString;
					version = getVersion(connectionString);
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Error loading LPD version (connectionString: " + connectionString + ").", ex);
			}

			_lock.AcquireReaderLock(-1);
			try
			{
				#region Load from the database

				_newDBRead = false;

				if (_logicalDatabases == null || (version != _logicalDatabases.Version))
				{
					LogicalDatabases logicalDatabases = new LogicalDatabases(version);

					SqlCommand sqlCommand = new SqlCommand();
					sqlCommand.CommandText = "dbo.up_lpd_List";
					sqlCommand.CommandType = CommandType.StoredProcedure;
					SqlConnection conn = new SqlConnection(connectionString);
					sqlCommand.Connection = conn;
					SqlDataReader dataReader = null;

					try
					{
						conn.Open();
						dataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);

						bool colLookupDone = false;
						
						Int32 ordinalLogicalDatabaseName = Constants.NULL_INT;
						Int32 ordinalHydraModeID = Constants.NULL_INT;
						Int32 ordinalHydraThreads = Constants.NULL_INT;
						Int32 ordinalHydraFailureThreshold = Constants.NULL_INT;
						Int32 ordinalServerName = Constants.NULL_INT;
						Int32 ordinalPhysicalDatabaseName = Constants.NULL_INT;
						Int32 ordinalActiveFlag = Constants.NULL_INT;
						Int32 ordinalOffset = Constants.NULL_INT;

						while (dataReader.Read())
						{
							if (!colLookupDone)
							{
								ordinalLogicalDatabaseName = dataReader.GetOrdinal("LogicalDatabaseName");
								ordinalHydraModeID = dataReader.GetOrdinal("HydraModeID");
								ordinalHydraThreads = dataReader.GetOrdinal("HydraThreads");
								ordinalHydraFailureThreshold = dataReader.GetOrdinal("HydraFailureThreshold");
								ordinalServerName = dataReader.GetOrdinal("ServerName");
								ordinalPhysicalDatabaseName = dataReader.GetOrdinal("PhysicalDatabaseName");
								ordinalActiveFlag = dataReader.GetOrdinal("ActiveFlag");
								ordinalOffset = dataReader.GetOrdinal("Offset");

								colLookupDone = true;
							}

							string logicalDatabaseName = dataReader.GetString(ordinalLogicalDatabaseName);
							Int16 offset = (Int16)dataReader.GetInt32(ordinalOffset);

							LogicalDatabase logicalDatabase = logicalDatabases[logicalDatabaseName];

							if (logicalDatabase == null)
							{
								logicalDatabase = new LogicalDatabase(logicalDatabaseName,
									(HydraModeType)Enum.Parse(typeof(HydraModeType), dataReader.GetInt32(ordinalHydraModeID).ToString()),
									(Int16)dataReader.GetInt32(ordinalHydraThreads));
								logicalDatabases.AddLogicalDatabase(logicalDatabase);
							}

							Partition partition = logicalDatabase[offset];

							if (partition == null)
							{
								partition = new Partition(offset);
								logicalDatabase.AddPartition(partition);
							}

							PhysicalDatabase physicalDatabase = new PhysicalDatabase(dataReader.GetString(ordinalServerName),
								dataReader.GetString(ordinalPhysicalDatabaseName),
								dataReader.GetBoolean(ordinalActiveFlag),
								(Int16)dataReader.GetInt32(ordinalHydraFailureThreshold));

							partition.PhysicalDatabases.Add(physicalDatabase);
							if (physicalDatabase.IsActive)
							{
								logicalDatabase.AddToRecoveryList(physicalDatabase);
							}
						}

						if (logicalDatabases[LOGICALDB_MNSYSTEM].PartitionCount > 0 && logicalDatabases[LOGICALDB_MNSYSTEM][0].PhysicalDatabases.Count > 0)
						{
							_lock.UpgradeToWriterLock(-1);
							_version = logicalDatabases.Version;
							_logicalDatabases = logicalDatabases;

							_newDBRead = true;
						}
						else
						{
							throw new Exception("LPD list contains no mnSystem instances (connectionString: " + connectionString + ").");
						}
					}
					finally
					{
						conn.Close();
					}
				}

				#endregion
			}
			finally
			{
				_lock.ReleaseLock();
			}

			#region Trace & EventLog

			// Notify event log & trace of every new read from DB. Otherwise ignore refresh from memory.
			string output = String.Empty;

			if (_logicalDatabases != null)
			{
				if (_newDBRead)
				{
					output = "Refreshed logical databases. " + Environment.NewLine + Environment.NewLine;
					output += "From connection string: " + connectionString + Environment.NewLine + Environment.NewLine;
					output += "Version: " + _version + Environment.NewLine + Environment.NewLine;

					for (int i=0; i < _logicalDatabases.Count; i++)
					{
						LogicalDatabase logicalDatabase = _logicalDatabases[i];

						output += "Database: " + logicalDatabase.LogicalDatabaseName;
						output += " Partition count: " + logicalDatabase.PartitionCount.ToString();
						output += " Hydra mode: " + logicalDatabase.HydraMode.ToString() + Environment.NewLine;
					}

					Trace.Write(output);
				}												
			}

			#endregion
		}


		private Guid getVersion(string connectionString)
		{
			SqlCommand sqlCommand = new SqlCommand();
			sqlCommand.CommandText = "dbo.up_LogicalDatabaseVersion_List";
			sqlCommand.CommandType = CommandType.StoredProcedure;
			SqlConnection conn = new SqlConnection(connectionString);
			sqlCommand.Connection = conn;
			SqlDataReader dataReader = null;

			try
			{
				conn.Open();
				dataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
				if (dataReader.Read())
				{
					return new Guid(dataReader.GetString(0));
				}
				else
				{
					throw new Exception("up_LogicalDatabaseVersion_List returned zero rows (connectionString: " + connectionString + ").");
				}
			}
			finally
			{
				conn.Close();
			}
		}
	}
}
