using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using Matchnet;
using Matchnet.Caching;
using Matchnet.Data;
using Matchnet.Data.Exceptions;
using Matchnet.Exceptions;

using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.Servers;
using Matchnet.Data.Hydra;


namespace Matchnet.Configuration.BusinessLogic
{
	public class SettingsBL
	{
		public static readonly SettingsBL Instance = new SettingsBL();

		private SettingsBL()
		{
		}


		public NameValueCollection GetSettings(string serverName)
		{
			try
			{
				Command command = new Command("mnSystem", "dbo.up_Setting_GetAll", 0);
				command.AddParameter("@ServerName", SqlDbType.VarChar, ParameterDirection.Input, serverName);
				DataTable dt = Client.Instance.ExecuteDataTable(command, LpdBL.Instance.GetLogicalDatabases());

				NameValueCollection settings = new NameValueCollection();
				
				for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
				{
					DataRow row = dt.Rows[rowNum];
					settings.Add(row["SettingKey"].ToString(), row["SettingValue"].ToString());
				}

				return settings;
			}
			catch(Exception ex)
			{
				throw(new BLException("Failure populating NameValueCollection.", ex));
			}
		}


		public bool IsServerEnabled(string serverName)
		{
			try
			{
				Command command = new Command("mnSystem", "up_Server_List", 0);
				DataTable dt = Client.Instance.ExecuteDataTable(command, LpdBL.Instance.GetLogicalDatabases());

				for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
				{
					DataRow row = dt.Rows[rowNum];
					if (row["Name"].ToString().ToLower() == serverName.ToLower())
					{
						if (Convert.ToInt32(row["IsEnabled"]) == 1)
						{
							return true;
						}
						break;
					}
				}

				return false;
			}
			catch (Exception ex)
			{
				throw new BLException("Error retrieving server enabled status.", ex);
			}
		}


		public ServiceInstanceConfig GetServiceInstanceConfig(string serviceConstant, string machineName)
		{
			//do not cache this, it is hit only when a svc is started and needs to be accurate
			try
			{
				Command command = new Command("mnSystem", "dbo.up_ServiceInstance_GetSingle", 0);
				command.AddParameter("@ServiceConstant", SqlDbType.VarChar, ParameterDirection.Input,  serviceConstant);
				command.AddParameter("@ServerName", SqlDbType.VarChar, ParameterDirection.Input,  machineName);
				DataTable dataTable = Client.Instance.ExecuteDataTable(command, LpdBL.Instance.GetLogicalDatabases());

				if(dataTable == null || dataTable.Rows.Count == 0)
				{
					throw(new BLException("Could not load Service Configuration meta data from database for ServiceConstant = '" + serviceConstant + "', ServerName = '" + machineName + "'."));
				}

				DataRow dataRow = dataTable.Rows[0];
				return new ServiceInstanceConfig(Convert.ToByte(dataRow["PartitionOffset"]),
					dataRow["PartitionUri"].ToString(),
					Convert.ToInt32(dataRow["ServicePort"]));
			}
			catch(Exception ex)
			{
				throw(new BLException("Failure loading ServiceInstance metadata.", ex));
			}
		}

        public List<MembaseConfig> GetMembaseConfigs()
        {
            List<MembaseConfig> membaseConfigs = new List<MembaseConfig>();
            try
            {
                Command command = new Command("mnSystem", "dbo.up_Get_AllMembaseConfigs", 0);
                DataTable dataTable = Client.Instance.ExecuteDataTable(command, LpdBL.Instance.GetLogicalDatabases());

                if (dataTable == null || dataTable.Rows.Count == 0)
                {
                    throw (new BLException("Could not load Membase Configuration meta data from database"));
                }

                MembaseConfig mc = null;
                foreach (DataRow dataRow in dataTable.Rows)
                {
                    string bucketName = string.Empty;
                    try
                    {
                        bucketName=dataRow["BucketName"].ToString();
                        if (mc == null || mc.BucketName != bucketName)
                        {
                            mc = new MembaseConfig(bucketName,
                                dataRow["BucketPassword"].ToString(),
                                ((!dataRow.IsNull("RetryCount")) ? Convert.ToInt32(dataRow["RetryCount"]) : int.MinValue),
                                ((!dataRow.IsNull("RetryTimeout")) ? Convert.ToInt32(dataRow["RetryTimeout"]) : int.MinValue),
                                ((!dataRow.IsNull("SocketPoolConnectionTimeout")) ? Convert.ToInt32(dataRow["SocketPoolConnectionTimeout"]) : int.MinValue),
                                ((!dataRow.IsNull("SocketPoolDeadTimeout")) ? Convert.ToInt32(dataRow["SocketPoolDeadTimeout"]) : int.MinValue),
                                ((!dataRow.IsNull("SocketPoolQueueTimeout")) ? Convert.ToInt32(dataRow["SocketPoolQueueTimeout"]) : int.MinValue),
                                ((!dataRow.IsNull("SocketPoolReceiveTimeout")) ? Convert.ToInt32(dataRow["SocketPoolReceiveTimeout"]) : int.MinValue),
                                ((!dataRow.IsNull("SocketPoolMaxPoolSize")) ? Convert.ToInt32(dataRow["SocketPoolMaxPoolSize"]) : int.MinValue),
                                ((!dataRow.IsNull("SocketPoolMinPoolSize")) ? Convert.ToInt32(dataRow["SocketPoolMinPoolSize"]) : int.MinValue),
                                ((!dataRow.IsNull("ObjectTTLInSeconds")) ? Convert.ToInt32(dataRow["ObjectTTLInSeconds"]) : int.MinValue));
                        }
                        mc.AddServer(new MembaseConfigServer(dataRow["ServerIP"].ToString(), Convert.ToInt32(dataRow["ServerPort"])));

                        if(!membaseConfigs.Contains(mc)) {
                            membaseConfigs.Add(mc);
                        }
                    }
                    catch (Exception ex1)
                    {
                        ConfigServiceTrace.Instance.LogServiceException("SettingsBL", "GetMembaseConfigs() - Bucket="+bucketName, ex1, false);
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("Failure loading ServiceInstance metadata.", ex));
            }
            return membaseConfigs;
        }

        public MembaseConfig GetMembaseConfigByBucket(string bucketName)
        {
            MembaseConfig membaseConfig = null;
            try
            {
                var command = new Command("mnSystem", "dbo.up_Get_MembaseConfigbyBucket", 0);
                command.AddParameter("@bucketName", SqlDbType.NVarChar, ParameterDirection.Input, bucketName);
                DataTable dataTable = Client.Instance.ExecuteDataTable(command, LpdBL.Instance.GetLogicalDatabases());

                if (dataTable == null || dataTable.Rows.Count == 0)
                {
                    throw (new BLException("Could not load Membase Configuration meta data from database"));
                }
                foreach (DataRow dataRow in dataTable.Rows)
                {
                    if (membaseConfig == null)
                    {
                        membaseConfig = new MembaseConfig(bucketName,
                                                          dataRow["BucketPassword"].ToString(),
                                                          ((!dataRow.IsNull("RetryCount"))
                                                               ? Convert.ToInt32(dataRow["RetryCount"])
                                                               : int.MinValue),
                                                          ((!dataRow.IsNull("RetryTimeout"))
                                                               ? Convert.ToInt32(dataRow["RetryTimeout"])
                                                               : int.MinValue),
                                                          ((!dataRow.IsNull("SocketPoolConnectionTimeout"))
                                                               ? Convert.ToInt32(dataRow["SocketPoolConnectionTimeout"])
                                                               : int.MinValue),
                                                          ((!dataRow.IsNull("SocketPoolDeadTimeout"))
                                                               ? Convert.ToInt32(dataRow["SocketPoolDeadTimeout"])
                                                               : int.MinValue),
                                                          ((!dataRow.IsNull("SocketPoolQueueTimeout"))
                                                               ? Convert.ToInt32(dataRow["SocketPoolQueueTimeout"])
                                                               : int.MinValue),
                                                          ((!dataRow.IsNull("SocketPoolReceiveTimeout"))
                                                               ? Convert.ToInt32(dataRow["SocketPoolReceiveTimeout"])
                                                               : int.MinValue),
                                                          ((!dataRow.IsNull("SocketPoolMaxPoolSize"))
                                                               ? Convert.ToInt32(dataRow["SocketPoolMaxPoolSize"])
                                                               : int.MinValue),
                                                          ((!dataRow.IsNull("SocketPoolMinPoolSize"))
                                                               ? Convert.ToInt32(dataRow["SocketPoolMinPoolSize"])
                                                               : int.MinValue),
                                                          ((!dataRow.IsNull("ObjectTTLInSeconds"))
                                                               ? Convert.ToInt32(dataRow["ObjectTTLInSeconds"])
                                                               : int.MinValue));
                    }
                    membaseConfig.AddServer(new MembaseConfigServer(dataRow["ServerIP"].ToString(), Convert.ToInt32(dataRow["ServerPort"])));
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("Failure loading Membase Config by bucketname", ex));
            }
            return membaseConfig;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
	    public List<ApiAppSetting> GetApiAppSettings()
	    {
            try
            {
                var command = new Command("mnSystem", "dbo.up_Get_All_APIAppSettings", 0);
                var dataTable = Client.Instance.ExecuteDataTable(command, LpdBL.Instance.GetLogicalDatabases());
                
                if (dataTable == null)
                {
                    throw (new BLException("Could not load API app settings from mnSystem."));
                }

                var settings = (from DataRow dataRow in dataTable.Rows
                                select new ApiAppSetting
                                           {
                                               ApiAppSettingValue = Convert.ToString(dataRow["APIAppSettingValue"]), 
                                               AppId = Convert.ToInt32(dataRow["AppID"]), 
                                               SettingConstant = Convert.ToString(dataRow["SettingConstant"])
                                           }).ToList();
                
                return settings;
            }
            catch (Exception exception)
            {
                throw (new BLException("Failure loading API app settings.", exception));
            }
	    }



	    string GetWriteDatabaseName()
	    {
	        return "mnSystemWrite";
	    }

        //enum SettingCategory
        //{
        //    Uncategorized = 0,
        //    Appearance = 1,
        //    ConfigurationSettings = 2,
        //    SearchSettings = 3,
        //    PaymentSettings = 4,
        //    InformationalSettings = 5,
        //    Globalization = 6,
        //    ServiceSettings = 7,
        //    FeatureSettings = 8,
        //    MobileSettings = 9,
        //    APISettings = 10
        //}            
        public void AddNewOrUpdateSetting(string settingConstant, string globalDefaultValue, string settingDescription, int settingCategoryId = 2, bool isRequiredForCommunity = false, bool isRequiredForSite = false)
	    {
            try
            {
                var command = new Command(GetWriteDatabaseName(), "dbo.up_setting_set_global_default", 0);
                command.AddParameter("@SettingCategoryID", SqlDbType.Int, ParameterDirection.Input, settingCategoryId);
                command.AddParameter("@SettingConstant", SqlDbType.VarChar, ParameterDirection.Input, settingConstant);
                command.AddParameter("@GlobalDefaultValue", SqlDbType.VarChar, ParameterDirection.Input, globalDefaultValue);
                command.AddParameter("@SettingDescription", SqlDbType.VarChar, ParameterDirection.Input, settingDescription);
                command.AddParameter("@IsRequiredForCommunity", SqlDbType.Bit, ParameterDirection.Input, isRequiredForCommunity);
                command.AddParameter("@IsRequiredForSite", SqlDbType.Bit, ParameterDirection.Input, isRequiredForSite);
                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception exception)
            {
                throw (new BLException(string.Format("Failure adding/updating an app setting properties. SettingConstant = {0}, GlobalSettingValue = {1}", settingConstant,globalDefaultValue), exception));
            }
	    }
        
        public void UpdateGlobalDefault(string settingConstant, string globalDefaultValue)
        {
            try
            {
                var command = new Command(GetWriteDatabaseName(), "dbo.up_setting_update_global_default", 0);
                command.AddParameter("@SettingConstant", SqlDbType.VarChar, ParameterDirection.Input, settingConstant);
                command.AddParameter("@GlobalDefaultValue", SqlDbType.VarChar, ParameterDirection.Input, globalDefaultValue);
                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception exception)
            {
                throw (new BLException(string.Format("Failure updating an app setting global value.SettingConstant = {0}, GlobalSettingValue = {1}", settingConstant,globalDefaultValue), exception));
            }
        }
        public void SetValueForSite(int siteId, string settingConstant, string globalDefaultValue)
        {
            try
            {
                var command = new Command();
                command.LogicalDatabaseName = GetWriteDatabaseName();
                command.StoredProcedureName = "dbo.up_setting_set_value_for_site";
                command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteId);
                command.AddParameter("@SettingConstant", SqlDbType.VarChar, ParameterDirection.Input, settingConstant);
                command.AddParameter("@Value", SqlDbType.VarChar, ParameterDirection.Input, globalDefaultValue);
                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception exception)
            {
                throw (new BLException(string.Format("Failure updating an app setting value for a site. SiteId = {0}, SettingConstant = {1}, GlobalSettingValue = {2}", siteId, settingConstant, globalDefaultValue), exception));
            }
        }
        public void ResetToGlobalDefault(string settingConstant)
        {
            try
            {
                var command = new Command(GetWriteDatabaseName(), "dbo.up_setting_reset_to_default", 0);
                command.AddParameter("@SettingConstant", SqlDbType.VarChar, ParameterDirection.Input, settingConstant);
                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception exception)
            {
                throw (new BLException(string.Format("Failure resettingsetting to the global default.SettingConstant = {0}", settingConstant), exception));
            }
        }
    }
}
