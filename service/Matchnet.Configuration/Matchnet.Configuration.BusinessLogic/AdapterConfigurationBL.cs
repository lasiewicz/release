using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet;
using Matchnet.Data;
using Matchnet.Data.Exceptions;
using Matchnet.Exceptions;

using Matchnet.Configuration.ValueObjects;


namespace Matchnet.Configuration.BusinessLogic
{
	public class AdapterConfigurationBL
	{
		public static readonly AdapterConfigurationBL Instance = new AdapterConfigurationBL();

		private AdapterConfigurationBL()
		{
		}


		public ServicePartitions GetAll()
		{
			string serviceConstantPrev = string.Empty;
			Int16 offsetPrev = 0;
			string serviceConstant = string.Empty;
			Int16 offset = 0;
			ServicePartition servicePartition = null;

			try
			{
				Command command = new Command("mnSystem", "dbo.up_Service_List", 0);
				DataTable dt = Client.Instance.ExecuteDataTable(command, LpdBL.Instance.GetLogicalDatabases());

				ServicePartitions servicePartitions = new ServicePartitions();

				for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
				{
					DataRow row = dt.Rows[rowNum];
					serviceConstant = row["ServiceConstant"].ToString();
					offset = Convert.ToInt16(row["PartitionOffset"]);

					if (serviceConstant != serviceConstantPrev || offset != offsetPrev)
					{
						servicePartition = new ServicePartition(serviceConstant, offset, row["PartitionHostName"].ToString(), Convert.ToInt32(row["PartitionPort"]));
						servicePartitions.Add(servicePartition);
						serviceConstantPrev = serviceConstant;
						offsetPrev = offset;
					}

					servicePartition.Add(new ServiceInstance(row["ServerHostName"].ToString(), Convert.ToBoolean(row["IsEnabled"])));
				}

				return servicePartitions;
			}
			catch(Exception ex)
			{
				throw(new BLException("Failure creating ServicePartitionCollection.", ex));
			}
		}
	}
}
