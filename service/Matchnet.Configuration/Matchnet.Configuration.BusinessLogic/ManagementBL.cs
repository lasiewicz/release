﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Matchnet.Data;
using Matchnet.Data.Hydra;

using Matchnet.Configuration.ValueObjects.Management;
using Matchnet.Exceptions;
namespace Matchnet.Configuration.BusinessLogic
{
    public class ManagementBL
    {
        public static readonly ManagementBL Instance = new ManagementBL();

        private ManagementBL()
        {
        }

        public Settings GetSettings()
        {
            try
            {
                Command command = new Command("mnSystem", "dbo.up_Settings_List", 0);

                DataSet ds = Client.Instance.ExecuteDataSet(command);

                DataTable dt = ds.Tables[0];
                //settings
                Settings settings = new Settings();
                List<Setting> settinglist = new List<Setting>();
                for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
                {
                    DataRow rs = dt.Rows[rowNum];
                    Setting setting = new Setting();
                    setting.SettingID = (int)(rs["settingid"] != Convert.DBNull ? rs["settingid"] : 0);
                    setting.SettingCategoryID = (int)(rs["settingcategoryid"] != Convert.DBNull ? rs["settingcategoryid"] : 0);
                    setting.SettingConstant = (string)(rs["settingconstant"] != Convert.DBNull ? rs["settingconstant"] : "");
                    setting.GlobalDefaultValue = (string)(rs["globaldefaultvalue"] != Convert.DBNull ? rs["globaldefaultvalue"] : "");
                    setting.SettingDescription = (string)(rs["settingdescription"] != Convert.DBNull ? rs["settingdescription"] : "");
                    setting.IsRequiredForCommunity = (bool)(rs["isrequiredforcommunity"] != Convert.DBNull ? rs["isrequiredforcommunity"] : false);
                    setting.IsRequiredForSite = (bool)(rs["isrequiredforsite"] != Convert.DBNull ? rs["isrequiredforsite"] : false);
                    setting.CreateDate = (object)(rs["createdate"] != Convert.DBNull ? rs["createdate"] : "");

                    settinglist.Add(setting);
                }

                DataTable dt1 = ds.Tables[1];
                //community settings

                List<CommunitySetting> communitysettinglist = new List<CommunitySetting>();
                for (Int32 rowNum = 0; rowNum < dt1.Rows.Count; rowNum++)
                {
                    DataRow rs = dt1.Rows[rowNum];
                    CommunitySetting setting = new CommunitySetting();
                    setting.CommunitySettingID = (int)(rs["communitysettingid"] != Convert.DBNull ? rs["communitysettingid"] : 0);
                    setting.CommunityID = (int)(rs["communityid"] != Convert.DBNull ? rs["communityid"] : 0);
                    setting.SettingID = (int)(rs["settingid"] != Convert.DBNull ? rs["settingid"] : 0);
                    setting.CommunitySettingValue = (string)(rs["communitysettingvalue"] != Convert.DBNull ? rs["communitysettingvalue"] : "");
                    communitysettinglist.Add(setting);
                }


                DataTable dt2 = ds.Tables[2];
                //site settings

                List<SiteSetting> sitesettinglist = new List<SiteSetting>();
                for (Int32 rowNum = 0; rowNum < dt2.Rows.Count; rowNum++)
                {
                    DataRow rs = dt2.Rows[rowNum];
                    SiteSetting setting = new SiteSetting();
                    setting.SiteSettingID = (int)(rs["sitesettingid"] != Convert.DBNull ? rs["sitesettingid"] : 0);
                    setting.SiteID = (int)(rs["siteid"] != Convert.DBNull ? rs["siteid"] : 0);
                    setting.SettingID = (int)(rs["settingid"] != Convert.DBNull ? rs["settingid"] : 0);
                    setting.SiteSettingValue = (string)(rs["sitesettingvalue"] != Convert.DBNull ? rs["sitesettingvalue"] : "");

                    sitesettinglist.Add(setting);

                }

                DataTable dt3 = ds.Tables[3];
                //brand settings

                List<BrandSetting> brandsettinglist = new List<BrandSetting>();
                for (Int32 rowNum = 0; rowNum < dt3.Rows.Count; rowNum++)
                {
                    DataRow rs = dt3.Rows[rowNum];
                    BrandSetting setting = new BrandSetting();
                    setting.BrandSettingID = (int)(rs["brandsettingid"] != Convert.DBNull ? rs["brandsettingid"] : 0);
                    setting.BrandID = (int)(rs["brandid"] != Convert.DBNull ? rs["brandid"] : 0);
                    setting.SettingID = (int)(rs["settingid"] != Convert.DBNull ? rs["settingid"] : 0);
                    setting.BrandSettingValue = (string)(rs["brandsettingvalue"] != Convert.DBNull ? rs["brandsettingvalue"] : "");

                    brandsettinglist.Add(setting);
                }

                DataTable dt4 = ds.Tables[4];
                //brand settings

                List<ServerSetting> serversettinglist = new List<ServerSetting>();
                for (Int32 rowNum = 0; rowNum < dt4.Rows.Count; rowNum++)
                {
                    DataRow rs = dt4.Rows[rowNum];
                    ServerSetting setting = new ServerSetting();
                    setting.ServerSettingID = (int)(rs["serversettingid"] != Convert.DBNull ? rs["serversettingid"] : 0);
                    setting.ServerName = (string)(rs["servername"] != Convert.DBNull ? rs["servername"] : "");
                    setting.ServerID = (int)(rs["serverid"] != Convert.DBNull ? rs["serverid"] : 0);
                    setting.SettingID = (int)(rs["settingid"] != Convert.DBNull ? rs["settingid"] : 0);
                    setting.CommunityID = (int)(rs["communityid"] != Convert.DBNull ? rs["communityid"] : 0);
                    setting.SiteID = (int)(rs["siteid"] != Convert.DBNull ? rs["siteid"] : 0);
                    setting.BrandID = (int)(rs["brandid"] != Convert.DBNull ? rs["brandid"] : 0);
                    setting.ServerSettingValue = (string)(rs["serversettingvalue"] != Convert.DBNull ? rs["serversettingvalue"] : "");

                    serversettinglist.Add(setting);
                }

                settings.SettingList = settinglist;
                settings.CommunitySettingList = communitysettinglist;
                settings.SiteSettingList = sitesettinglist;
                settings.BrandSettingList = brandsettinglist;
                settings.ServerSettingList = serversettinglist;

                return settings;
            }
            catch (Exception ex)
            {
                throw (new BLException("Failure populating settings for management tool.", ex));
            }

        }

        public List<Server> GetServers()
        {
            try
            {
                Command command = new Command("mnSystem", "dbo.up_servers_List", 0);

                DataTable dt = Client.Instance.ExecuteDataTable(command);

                List<Server> list = new List<Server>();
                for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
                {
                    DataRow rs = dt.Rows[rowNum];
                    Server server = new Server();
                    server.ServerID = (int)(rs["serverid"] != Convert.DBNull ? rs["serverid"] : 0);
                    server.Name = (string)(rs["name"] != Convert.DBNull ? rs["name"] : "");
                    server.IsEnabled = (bool)(rs["isenabled"] != Convert.DBNull ? rs["isenabled"] : false);
                    server.ServerType = (int)(rs["servertype"] != Convert.DBNull ? rs["servertype"] : 0);
                    server.LocationID = (int)(rs["locationid"] != Convert.DBNull ? rs["locationid"] : 0);
                    server.ServerTypeName = (string)(rs["ServerTypeName"] != Convert.DBNull ? rs["ServerTypeName"] : "");
                    list.Add(server);
                }


                return list;
            }
            catch (Exception ex)
            {
                throw (new BLException("Failure populating serverlist for management tool.", ex));
            }

        }


        public void SaveSettings(List<SettingsSave> settingslist, bool asyncWrite)
        {
            try
            {
                if (settingslist == null || settingslist.Count == 0)
                    return;


                foreach (SettingsSave save in settingslist)
                {
                    if (save.ScopeIDList == null || save.ScopeIDList.Count == 0)
                        continue;

                    switch (save.Scope)
                    {
                        case (SettingsScope.community):
                            foreach (int id in save.ScopeIDList)
                            { SaveCommunitySetting(save.SettingConstant, id, save.SettingValue, save.DeleteFlag, asyncWrite); }
                            break;
                        case (SettingsScope.site):
                            foreach (int id in save.ScopeIDList)
                            { SaveSiteSetting(save.SettingConstant, id, save.SettingValue, save.DeleteFlag, asyncWrite); }
                            break;
                        case (SettingsScope.brand):
                            foreach (int id in save.ScopeIDList)
                            { SaveBrandSetting(save.SettingConstant, id, save.SettingValue, save.DeleteFlag, asyncWrite); }
                            break;
                        case (SettingsScope.server):
                            foreach (int id in save.ScopeIDList)
                            { SaveServerSetting(save.SettingConstant, save.ServerName, save.SettingValue, save.DeleteFlag, save.CommunityID, save.SiteID, save.BrandID, asyncWrite); }
                            break;


                    }
                }

            }
            catch (Exception ex)
            {
                throw (new BLException("Failure saving settings for management tool.", ex));
            }


        }

        public void SaveCommunitySetting(string settingconst, int scopeid, string settingvalue, bool deleteflag, bool asyncWrite)
        {
            try
            {
                Command command = new Command("mnSystem", "dbo.up_CommunitySetting_Save", 0);
                command.AddParameter("@settingconst", SqlDbType.VarChar, ParameterDirection.Input, settingconst);
                command.AddParameter("@communityid", SqlDbType.Int, ParameterDirection.Input, scopeid);
                command.AddParameter("@settingvalue", SqlDbType.VarChar, ParameterDirection.Input, settingvalue);
                command.AddParameter("@deleteflag", SqlDbType.Bit, ParameterDirection.Input, deleteflag);
                if (asyncWrite)
                    Client.Instance.ExecuteAsyncWrite(command);
                else
                {
                    SyncWriter writer = new SyncWriter();
                    Exception ex = null;
                    writer.Execute(command, out ex);
                    if (ex != null) throw ex;
                }

            }
            catch (Exception ex)
            {
                throw (new BLException("Failure in SaveCommunitySetting.", ex));
            }

        }
        public void SaveSiteSetting(string settingconst, int scopeid, string settingvalue, bool deleteflag, bool asyncWrite)
        {
            try
            {
                Command command = new Command("mnSystem", "dbo.[up_SiteSetting_Save]", 0);
                command.AddParameter("@settingconst", SqlDbType.VarChar, ParameterDirection.Input, settingconst);
                command.AddParameter("@siteid", SqlDbType.Int, ParameterDirection.Input, scopeid);
                command.AddParameter("@settingvalue", SqlDbType.VarChar, ParameterDirection.Input, settingvalue);
                command.AddParameter("@deleteflag", SqlDbType.Bit, ParameterDirection.Input, deleteflag);
                if (asyncWrite)
                    Client.Instance.ExecuteAsyncWrite(command);
                else
                {
                    SyncWriter writer = new SyncWriter();
                    Exception ex = null;
                    writer.Execute(command, out ex);
                    if (ex != null) throw ex;
                }


            }
            catch (Exception ex)
            {
                throw (new BLException("Failure in SaveSiteSetting.", ex));
            }

        }

        public void SaveBrandSetting(string settingconst, int scopeid, string settingvalue, bool deleteflag, bool asyncWrite)
        {
            try
            {
                Command command = new Command("mnSystem", "dbo.[up_BrandSetting_Save]", 0);
                command.AddParameter("@settingconst", SqlDbType.VarChar, ParameterDirection.Input, settingconst);
                command.AddParameter("@Brandid", SqlDbType.Int, ParameterDirection.Input, scopeid);
                command.AddParameter("@settingvalue", SqlDbType.VarChar, ParameterDirection.Input, settingvalue);
                command.AddParameter("@deleteflag", SqlDbType.Bit, ParameterDirection.Input, deleteflag);
                if (asyncWrite)
                    Client.Instance.ExecuteAsyncWrite(command);
                else
                {
                    SyncWriter writer = new SyncWriter();
                    Exception ex = null;
                    writer.Execute(command, out ex);
                    if (ex != null) throw ex;
                }

            }
            catch (Exception ex)
            {
                throw (new BLException("Failure in SaveBrandSetting.", ex));
            }

        }

        public void SaveServerSetting(string settingconst, string servername, string settingvalue, bool deleteflag, int communityid, int siteid, int brandid, bool asyncWrite)
        {
            try
            {
                Command command = new Command("mnSystem", "dbo.[up_ServerSetting_Save]", 0);
                command.AddParameter("@settingconst", SqlDbType.VarChar, ParameterDirection.Input, settingconst);
                command.AddParameter("@servername", SqlDbType.VarChar, ParameterDirection.Input, servername);
                command.AddParameter("@settingvalue", SqlDbType.VarChar, ParameterDirection.Input, settingvalue);
                command.AddParameter("@deleteflag", SqlDbType.Bit, ParameterDirection.Input, deleteflag);
                if (communityid > 0)
                    command.AddParameter("@communityid", SqlDbType.Int, ParameterDirection.Input, communityid);
                if (siteid > 0)
                    command.AddParameter("@siteid", SqlDbType.Int, ParameterDirection.Input, siteid);
                if (brandid > 0)
                    command.AddParameter("@Brandid", SqlDbType.Int, ParameterDirection.Input, brandid);


                if (asyncWrite)
                    Client.Instance.ExecuteAsyncWrite(command);
                else
                {
                    SyncWriter writer = new SyncWriter();
                    Exception ex = null;
                    writer.Execute(command, out ex);
                    if (ex != null) throw ex;
                }

            }
            catch (Exception ex)
            {
                throw (new BLException("Failure in SaveBrandSetting.", ex));
            }

        }


    }
}
