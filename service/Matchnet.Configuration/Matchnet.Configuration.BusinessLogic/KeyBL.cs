using System;
using System.Data;
using System.Data.SqlClient;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Data;
using Matchnet.Data.Exceptions;


namespace Matchnet.Configuration.BusinessLogic
{
	public class KeyBL
	{
		public static readonly KeyBL Instance = new KeyBL();

		private KeyBL()
		{
		}


		public Int32 GetKey(string keyName)
		{
			try
			{
				Command command = new Command("mnKey", "up_PrimaryKey_Select", 0);
				command.AddParameter("@KeyName", SqlDbType.VarChar, ParameterDirection.Input, keyName);
				DataTable dt = Client.Instance.ExecuteDataTable(command, LpdBL.Instance.GetLogicalDatabases());
				return Convert.ToInt32(dt.Rows[0][0]);
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occurred retrieving key.", ex));
			}
		}
	}
}
