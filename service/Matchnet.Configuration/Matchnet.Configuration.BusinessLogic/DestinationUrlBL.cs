using System;
using System.Data;
using System.Collections.Generic;

using Matchnet.Data;
using Matchnet.Caching;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet.Configuration.ValueObjects.Urls;



namespace Matchnet.Configuration.BusinessLogic
{
   public class DestinationUrlBL
   {
	  public static readonly DestinationUrlBL Instance = new DestinationUrlBL();
      private const int CACHE_TTL = 7200;

	  private Matchnet.Caching.Cache _cache;

	  private DestinationUrlBL()
	  {
		 _cache = Matchnet.Caching.Cache.Instance;
	  }

	  public DestinationUrls GetDestinationUrls()
	  {
		 DestinationUrls result = _cache.Get(DestinationUrls.CACHE_KEY) as DestinationUrls;
		 if (result == null)
		 {
			Command command;
			DataTable dt = null;
			try
			{
			   result = new DestinationUrls();
			   command = new Command("mnSystem", "dbo.up_UrlUrlOwner_List", 0);
			   dt = Client.Instance.ExecuteDataTable(command, LpdBL.Instance.GetLogicalDatabases());

			   for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
			   {
				  DataRow row = dt.Rows[rowNum];
				  bool isWebServiceContext = Convert.ToBoolean(row["IsWebServiceContext"]);
				  bool isAllowedRedirect = Convert.ToBoolean(row["IsAllowedRedirect"]);
				  string url = row["Url"].ToString();
				  // loading all URLs regardless of allowance. ServiceAdapter and consumer to impose usage of URL and assume
				  // responsibility of allowing redirect or not
				  result[url] = new DestinationUrlProperty(isAllowedRedirect, isWebServiceContext);
			   }
			   _cache.Add(result);
			}
			finally {
			   if (dt != null)
				  dt.Dispose();
			}
		 }
		 return result;
	  }

      public List<DestinationURL> GetListOfDestinationURLs()
      {
          List<DestinationURL> urls = null;
          CachedDestinationURLs cachedURLs = _cache.Get(DestinationURL.COLLECTION_CACHE_KEY) as CachedDestinationURLs;
                    
          if (cachedURLs == null)
          {
              Command command;
              DataTable dt = null;
              try
              {
                  urls = new List<DestinationURL>();
                  command = new Command("mnSystem", "dbo.up_UrlUrlOwner_List", 0);
                  dt = Client.Instance.ExecuteDataTable(command, LpdBL.Instance.GetLogicalDatabases());

                  for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
                  {
                      DataRow row = dt.Rows[rowNum];
                      bool isWebServiceContext = Convert.ToBoolean(row["IsWebServiceContext"]);
                      bool isAllowedRedirect = Convert.ToBoolean(row["IsAllowedRedirect"]);
                      string url = row["Url"].ToString();
                      // loading all URLs regardless of allowance. ServiceAdapter and consumer to impose usage of URL and assume
                      // responsibility of allowing redirect or not
                      urls.Add( new DestinationURL(url, isAllowedRedirect, isWebServiceContext));
                  }
                  
                  cachedURLs = new CachedDestinationURLs(urls, CACHE_TTL);
                  _cache.Add(cachedURLs);
              }
              finally
              {
                  if (dt != null)
                      dt.Dispose();
              }
          }
          else
          {
              urls = cachedURLs.URLs; 
          }

          return urls;
      }
   }
}