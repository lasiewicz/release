﻿using System.Linq;
using Matchnet.Configuration.BusinessLogic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Matchnet.Configuration.ValueObjects;
using System.Collections.Generic;

namespace Matchnet.Configuration.Test
{
    
    
    /// <summary>
    ///This is a test class for SettingsBLTest and is intended
    ///to contain all SettingsBLTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SettingsBLTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetApiAppSettings
        ///</summary>
        [TestMethod()]
        public void GetApiAppSettingsTest()
        {
            var actual = SettingsBL.Instance.GetApiAppSettings();
            Assert.IsTrue(actual.Count > 0);

            var result = from ApiAppSetting apiAppSetting in actual
                         where apiAppSetting.AppId == 1054
                         select apiAppSetting;

            Assert.IsTrue(result.ToList().Count > 0);
        }
    }
}
