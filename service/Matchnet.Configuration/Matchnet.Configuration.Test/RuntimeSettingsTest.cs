﻿using Matchnet.Configuration.ServiceAdapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Matchnet.Configuration.ValueObjects;

namespace Matchnet.Configuration.Test
{
    
    
    /// <summary>
    ///This is a test class for RuntimeSettingsTest and is intended
    ///to contain all RuntimeSettingsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RuntimeSettingsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetApiAppSetting
        ///</summary>
        [TestMethod()]
        public void GetApiAppSettingTest()
        {
            const int appId = 1054;
            const string settingConstant = "MOL_MEMBER_TTL";

            var actual = RuntimeSettings.GetApiAppSetting(appId, settingConstant);

            Assert.AreEqual(Convert.ToInt32(actual.ApiAppSettingValue), 60);
        }
    }
}
