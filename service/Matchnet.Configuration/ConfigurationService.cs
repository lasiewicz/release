using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Matchnet.Configuration.ServiceManagers;
using Matchnet.Configuration.ValueObjects;


namespace Matchnet.Configuration
{
	public class ConfigurationService : Matchnet.RemotingServices.RemotingServiceBase
	{
		private System.ComponentModel.Container components = null;
		private AdapterConfigurationSM _adapterConfigurationSM;
		private SettingsSM _settingsSM;
		private KeySM _keySM;
		private FileRootSM _fileRootSM;
		private LpdSM _lpdSM;
		private AccessTicketSM _accessTicketSM;
		private DestinationUrlSM _destinationUrlSM;
		private ProfileSectionSM _profileSectionSM;
        private ManagementSM _managementSM;

		public ConfigurationService()
		{
			InitializeComponent();
		}


		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new ConfigurationService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}


		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = ServiceConstants.SERVICE_NAME;
		}

		protected override void OnStart(string[] args)
		{
			Matchnet.Configuration.BusinessLogic.LpdBL.Instance.Start();
			base.ServiceInstanceConfig = Matchnet.Configuration.BusinessLogic.SettingsBL.Instance.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);			
			base.OnStart(args);
		}



		protected override void RegisterServiceManagers()
		{
			try
			{
				_adapterConfigurationSM = new AdapterConfigurationSM();
				base.RegisterServiceManager(_adapterConfigurationSM);

				_settingsSM = new SettingsSM();
				base.RegisterServiceManager(_settingsSM);

				_keySM = new KeySM();
				base.RegisterServiceManager(_keySM);

				_fileRootSM = new FileRootSM();
				base.RegisterServiceManager(_fileRootSM);

				_lpdSM = new LpdSM();
				base.RegisterServiceManager(_lpdSM);

				_accessTicketSM = new AccessTicketSM();
				base.RegisterServiceManager(_accessTicketSM);

				_destinationUrlSM = new DestinationUrlSM();
				base.RegisterServiceManager(_destinationUrlSM);
				
				_profileSectionSM = new ProfileSectionSM();
				base.RegisterServiceManager(_profileSectionSM);

                _managementSM = new ManagementSM();
                base.RegisterServiceManager(_managementSM);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occurred when starting the Windows Service", ex);
			}
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}

			if (_adapterConfigurationSM != null)
			{
				_adapterConfigurationSM.Dispose();
			}

			if (_settingsSM != null)
			{
				_settingsSM.Dispose();
			}

			if (_keySM != null)
			{
				_keySM.Dispose();
			}

			if (_fileRootSM != null)
			{
				_fileRootSM.Dispose();
			}

			base.Dispose( disposing );
		}


	}
}
