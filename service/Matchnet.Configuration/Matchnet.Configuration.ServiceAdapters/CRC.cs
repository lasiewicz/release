using System;

namespace Matchnet.Configuration.ServiceAdapters
{
	/// <summary>
	/// Provide Cyclic Redundancy Checks on a source string to produce an output value
	/// </summary>
	internal class CRC
	{
		// Constants
		private const int CRC32BASE = 65521;  // largest prime smaller than 65536

		// Largest n such that 255n(n+1)/2 + (n+1)(BASE-1) <= 2^32-1
		// What this means is that in the worst case senario, each byte
		// could have a value of 255.  As we are adding numbers together
		// we could go beyond the value of a unsigned long or 2^32.
		// Thats a value of 4294967296.  To be sure we are safe, we take
		// things in handfulls of 5552 bytes at a time.
		private const int NMAX = 5552;
		
		private CRC()
		{
			// Hide the default constructor
		}


		/// <summary>
		/// Computes the 32-bit CRC of a given string which is used to determine the proper partition
		/// </summary>
		/// <param name="guid">System.Guid for which to compute the CRC</param>
		/// <returns>Returns the 32-bit CRC of the soruce string</returns>
		public static System.Int32 GetCRCKey(System.Guid guid)
		{
			return GetCRCKey(guid.ToString());
		}

		/// <summary>
		/// Computes the 32-bit CRC of a given string which is used to determine the proper partition
		/// </summary>
		/// <param name="guid">String representation of the GUID for which to compute the CRC</param>
		/// <returns>Returns the 32-bit CRC of the soruce string</returns>
		public static System.Int32 GetCRCKey(string guid)
		{
			if (guid == null || guid == string.Empty) 
			{
				throw(new Exception("Cannot compute CRC on GUID string that is null or empty"));
			}
			else
			{
				// NOTE: It is very important that all guide
				return computeCRC32(guid.ToUpper());
			}
		}


		/// <summary>
		/// Computes the 32-bit CRC of a given string which is used to determine the proper partition
		/// </summary>
		/// <param name="source">String for which to compute the 32-bit CRC</param>
		/// <returns>Returns the 32-bit CRC of the soruce string</returns>
		private static System.Int32 computeCRC32(string source)
		{
			if (source.Length > 0)
			{
				// NOTE: It is very important that the string be converted to UPPERCASE so that the CRC function works
				// properly, otherwise it is possible to return a negative number
				byte[] bdata = System.Text.Encoding.Unicode.GetBytes(source.ToUpper());
				return computeAdler32(1, bdata, bdata.Length);
			}
			else
			{
				return 0;
			}
		}

		private static int computeAdler32(long adler32, byte[] data, double length)
		{
			int pos=0;
			int arrayPos=0;
			int lengthRemaining=0;

			double low=0;
			double high=0;

			if(adler32 != 0)
			{
				low = adler32 & 65535;
				high = (int)((adler32 >> 16) & 65535);
			}

			if(data.Length == 0)
			{
				return 1;
			}

			lengthRemaining = (int)length;

			while(lengthRemaining > 0)
			{
				if(lengthRemaining < NMAX)
				{
					pos = lengthRemaining-1;
					lengthRemaining = 0;
				}
				else
				{
					pos = NMAX;
					lengthRemaining = lengthRemaining - (NMAX + 1);
				}
				
				while(pos >= 0)
				{
					low = low + data[arrayPos];
					high = high + low;
					
					arrayPos++;
					pos--;
				} 

				low = modulus(low, CRC32BASE);
				high = modulus(high, CRC32BASE);
			}
            
			return (int)leftShift4Byte(high, 16) | (int)low;
		}

		private static double modulus(double val, double modValue)
		{
			return val - (modValue * (int)(val / modValue));
		}

		private static double leftShift4Byte(double val, int shift)
		{
			return (int)val << shift;
		}

	}
}
