using System;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.Lpd;


namespace Matchnet.Configuration.ServiceAdapters
{
	public class LpdSA
	{
		public static readonly LpdSA Instance = new LpdSA();

		private const string SERVICE_MANAGER_NAME = "LpdSM";

		private LpdSA()
		{
		}


		public Guid GetVersion()
		{
			string uri = null;

			try
			{
				uri = getServiceManagerUri();
				return getService(uri).GetVersion();
			}
			catch (Exception ex)
			{
				throw new SAException("Unable to get version (uri: " + uri + ").", ex);
			}
		}


		public LogicalDatabases GetLogicalDatabases()
		{
			string uri = null;

			try
			{
				uri = getServiceManagerUri();
				return getService(uri).GetLogicalDatabases();
			}
			catch (Exception ex)
			{
				throw new SAException("Unable to get logical databases (uri: " + uri + ").", ex);
			}
		}


		private ILpdService getService(string uri)
		{
			try
			{
				return (ILpdService)Activator.GetObject(typeof(ILpdService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		
		private string getServiceManagerUri()
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONFIGSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}
	}
}
