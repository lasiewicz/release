﻿using Matchnet.Configuration.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Configuration.ServiceAdapters.Mocks
{
    public class ISettingsSAMock : ISettingsSA
    {
        private readonly Dictionary<string, string> dictionary = new Dictionary<string, string>();

        public List<MembaseConfig> GetMembaseConfigsFromSingleton()
        {
            throw new NotImplementedException();
        }

        public List<ApiAppSetting> GetAPIAppSettingsFromSingleton()
        {
            throw new NotImplementedException();
        }

        public void AddNewOrUpdateSetting(string settingConstant, string globalDefaultValue, string settingDescription,
            int settingCategoryId = 2, bool isRequiredForCommunity = false, bool isRequiredForSite = false)
        {
            throw new NotImplementedException();
        }

        public void UpdateGlobalDefault(string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void SetValueForSite(int siteId, string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void ResetToGlobalDefault(string settingConstant)
        {
            throw new NotImplementedException();
        }

        public ServiceInstanceConfig GetServiceInstanceConfigFromSingleton(string serviceConstant, string machineName)
        {
            throw new NotImplementedException();
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID, int brandID)
        {
            return dictionary[constant];
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID)
        {
            return dictionary[constant];
        }

        public string GetSettingFromSingleton(string constant, int communityID)
        {
            return dictionary[constant];
        }

        public string GetSettingFromSingleton(string constant)
        {
            return dictionary[constant];
        }

        public bool IsServerEnabledFromSingleton()
        {
            throw new NotImplementedException();
        }

        public bool SettingExistsFromSingleton(string constant, int communityID, int siteID)
        {
            return dictionary.ContainsKey(constant);
        }

        public void AddSetting(string name, string value)
        {
            if (!dictionary.Keys.Contains(name))
            {
                dictionary.Add(name, value);
            }
            else
            {
                dictionary[name] = value;
            }
        }

        public void RemoveSetting(string name)
        {
            dictionary.Remove(name);
        }

        public MembaseConfig GetMembaseConfigByBucketFromSingleton(string bucketName)
        {
            throw new NotImplementedException();
        }
    }
}
