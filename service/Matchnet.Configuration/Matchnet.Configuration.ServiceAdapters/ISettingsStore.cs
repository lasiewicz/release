using System;

namespace Matchnet.Configuration.ServiceAdapters
{
	public interface ISettingsStore
	{
		string GetSetting(string constant,
			Int32 key);
	}
}
