using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Linq;
using System.Runtime.InteropServices;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;
using Matchnet.Exceptions;
using Matchnet.Configuration.ValueObjects;


namespace Matchnet.Configuration.ServiceAdapters
{
	public class RuntimeSettings : ISettingsSA
	{
        public static readonly RuntimeSettings Instance = new RuntimeSettings();
		private const string CUSTOMSETTING_CONFIGURATION_SERVICE_KEY = "configuration/services/SettingsUri";
		private static string _machineName = Environment.MachineName;

        private RuntimeSettings(){}

	    public static Hashtable GetSettings()
	    {
	        return SettingSAInternal.Instance.GetSettings();
	    }

		public static string GetSetting(string constant)
		{
			string Value;

			Value = SettingSAInternal.Instance.GetSetting(constant);
			if (Value != null)
			{
				return Value;
			}

			throw new Exception("Could not find setting (" + constant + ").");
		}


		public static string GetSetting(string constant, Int32 communityID)
		{
			string Value;

			Value = SettingSAInternal.Instance.GetSetting(constant, communityID);
			if (Value != null)
			{
				return Value;
			}
			
			throw new Exception("Could not find setting (" + constant + ").");
		}


		public static string GetSetting(string constant, Int32 communityID,	Int32 siteID)
		{
			string Value;

			Value = SettingSAInternal.Instance.GetSetting(constant, communityID, siteID);
			if (Value != null)
			{
				return Value;
			}
			
			throw new Exception("Could not find setting (" + constant + ").");
		}
				

		public static string GetSetting(string constant, Int32 communityID, Int32 siteID, Int32 brandID)
		{
			string Value;

			Value = SettingSAInternal.Instance.GetSetting(constant, communityID, siteID, brandID);
			if (Value != null)
			{
				return Value;
			}
			
			throw new Exception("Could not find setting (" + constant + ").");
		}
		

		/// <summary>
		/// Determines if a server is enabled.
		/// </summary>
		/// <returns>True if server is enabled, false otherwise.</returns>
		public static bool IsServerEnabled()
		{
			return SettingSAInternal.Instance.ServerEnabled;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="serviceConstant"></param>
		/// <param name="machineName"></param>
		/// <returns></returns>
		public static ServiceInstanceConfig GetServiceInstanceConfig(string serviceConstant, string machineName)
		{
			return SettingSAInternal.Instance.GetServiceInstanceConfig(serviceConstant, machineName);
		}

		public static bool SettingExists(string constant, Int32 communityID, Int32 siteID)
		{
			string Value;

			Value = SettingSAInternal.Instance.GetSetting(constant, communityID, siteID);
						
			return Value != null;
		}


        public static List<MembaseConfig> GetMembaseConfigs()
        {
            try
            {
                MembaseConfigList cachedMembaseConfigs = Matchnet.Caching.Cache.Instance.Get(MembaseConfigList.GetCacheKeyString()) as MembaseConfigList;
                if (null == cachedMembaseConfigs)
                {
                    cachedMembaseConfigs = new MembaseConfigList();
                    cachedMembaseConfigs.MembaseConfigs = SettingSAInternal.Instance.getService().GetMembaseConfigs();
                    Matchnet.Caching.Cache.Instance.Add(cachedMembaseConfigs);
                }
                return cachedMembaseConfigs.MembaseConfigs;
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot get Membase Configs!!");
            }
        }

        public static MembaseConfig GetMembaseConfigByBucket(string bucketName)
        {
            try
            {
                //get from cached configs
                List<MembaseConfig> configs = GetMembaseConfigs();
                if (configs != null)
                {
                    foreach (MembaseConfig mc in configs)
                    {
                        if (mc.BucketName.ToLower() == bucketName.ToLower())
                        {
                            return mc;
                        }
                    }
                }

                //use this as backup
                return SettingSAInternal.Instance.getService().GetMembaseConfigByBucket(bucketName);
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot get Membase Config");
            }
        }

        ///<summary>
        /// Gets all API app settings.
        ///</summary>
        ///<returns></returns>
        ///<exception cref="Exception"></exception>
        public static List<ApiAppSetting> GetApiAppSettings()
        {
            try
            {
                var cachedApiAppSettings = Caching.Cache.Instance.Get(MembaseConfigList.GetCacheKeyString()) as ApiAppSettingList;
               
                if (cachedApiAppSettings == null)
                {
                    cachedApiAppSettings = new ApiAppSettingList
                                               {
                                                   ApiAppSettings =
                                                       SettingSAInternal.Instance.getService().GetApiAppSettings()
                                               };
                    Caching.Cache.Instance.Add(cachedApiAppSettings);
                }

                return cachedApiAppSettings.ApiAppSettings;
            }
            catch (Exception exception)
            {
                throw new Exception("Error retrieving API app settings.", exception);
            }
        }

        /// <summary>
        /// Gets a specific API app setting.
        /// </summary>
        /// <param name="appId">As defined in mnAPI..App</param>
        /// <param name="settingConstant">As defined in mnSystem..Setting</param>
        /// <returns></returns>
        public static ApiAppSetting GetApiAppSetting(int appId, string settingConstant)
        {
            try
            {
                var apiAppSetting = (from s in GetApiAppSettings()
                                     where s.AppId == appId && s.SettingConstant == settingConstant
                                     select s).FirstOrDefault();

                if (apiAppSetting != null)
                {
                    return apiAppSetting;
                }

                // Returns the global default value.
                apiAppSetting = new ApiAppSetting
                                    {
                                        ApiAppSettingValue = GetSetting(settingConstant),
                                        AppId = appId,
                                        SettingConstant = settingConstant
                                    };

                return apiAppSetting;
            }
            catch (Exception exception)
            {
                throw new SAException(String.Format("Error retrieving API app setting for app {0} and setting {1}", appId, settingConstant), exception);
            }
        }

		#region analitics A/B settings
		
		public static Analitics.Scenario GetAnaliticsScenario(Analitics.IAnaliticsScenario scenario)
		{
			Analitics.Scenario scen=Analitics.Scenario.A; //default
			try
			{
				scen=scenario.GetAnaliticsScenario();
				return scen;
			}
			catch(Exception ex)
			{
				return scen;
				
			}
		}

		#endregion
		

		#region SettingSAInternal

		private class SettingSAInternal
		{
			public readonly static SettingSAInternal Instance = new SettingSAInternal();

			private static System.Timers.Timer _Timer;
			private static bool _IsServerEnabled = true;
			private static Hashtable _settings;

			private SettingSAInternal()
			{
				_Timer = new System.Timers.Timer(60000);
				_Timer.Enabled = false;
				_Timer.AutoReset = true;
				
				_Timer.Elapsed += new System.Timers.ElapsedEventHandler(Timer_Elapsed);
				Timer_Elapsed(null,null);
				_Timer.Start();
			}


            public Hashtable GetSettings()
            {
                return _settings[0] as Hashtable;
            }
            
            public string GetSetting(string constant)
			{
				return (_settings[0] as Hashtable)[constant] as string;
			}

			public string GetSetting(string constant,
				Int32 communityID)
			{
				string val = null;
				Hashtable currentTable = _settings[communityID] as Hashtable;

				if (currentTable != null)
				{
					val = currentTable[constant] as string;
				}

				if (val == null)
				{
					currentTable = _settings[0] as Hashtable;
					if (currentTable != null)
					{
						val = currentTable[constant] as string;
					}
				}

				return val;
			}

			public string GetSetting(string constant,
				Int32 communityID,
				Int32 siteID)
			{
				string val = null;
				Hashtable currentTable = _settings[siteID] as Hashtable;

				if (currentTable != null)
				{
					val = currentTable[constant] as string;
				}

				if (val == null)
				{
					val = GetSetting(constant, communityID);
				}

				return val;
			}

			public string GetSetting(string constant,
				Int32 communityID,
				Int32 siteID,
				Int32 brandID)
			{
				string val = null;
				Hashtable currentTable = _settings[brandID] as Hashtable;

				if (currentTable != null)
				{
					val = currentTable[constant] as string;
				}

				if (val == null)
				{
					val = GetSetting(constant, communityID, siteID);
				}

				return val;
			}

			public void refreshSettings()
			{
				NameValueCollection settingsCollection;
				Hashtable settings = new Hashtable();

				try
				{
					settingsCollection = getService().GetSettings(_machineName);

					if(settingsCollection != null && settingsCollection.Count > 0)
					{
						for (Int32 i = 0; i < settingsCollection.Count; i++)
						{
							string[] nameParts = settingsCollection.Keys[i].Split('/');
							bool isMachine = false;
							Int32 key = 0;
							string name;

							if (nameParts[0] == "OVERRIDE")
							{
								isMachine = true;
							}

							if (nameParts[3] != "*")
							{
								key = Convert.ToInt32(nameParts[3]);
							}
							else if (nameParts[2] != "*")
							{
								key = Convert.ToInt32(nameParts[2]);
							}
							else if (nameParts[1] != "*")
							{
								key = Convert.ToInt32(nameParts[1]);
							}

							name = nameParts[4];

							Hashtable tableCurrent = settings[key] as Hashtable;
							if (tableCurrent == null)
							{
								tableCurrent = new Hashtable();
								settings[key] = tableCurrent;
							}

							if (isMachine)
							{
								tableCurrent[name] = settingsCollection[i];
							}
							else if (!tableCurrent.ContainsKey(name))
							{
								tableCurrent[name] = settingsCollection[i];
							}
						}

						_settings = settings;
					}
				}
				catch (Exception ex)
				{
					throw new SAException("refreshSettings() failed getting new settings", ex);
				}
			}

			private void refreshServerEnabledStatus()
			{
				///NOTE: the default is that the server maintains it's on/ off state.
				///This way, if the backend is not reachable, servers don't all commit suicide. If they were on, they'l remain on.				
				try
				{
					_IsServerEnabled = getService().IsServerEnabled(System.Environment.MachineName);
				}
				catch(Exception ex)
				{
					throw new SAException("Failure getting server enabled status.", ex);
				}
			}

			public ServiceInstanceConfig GetServiceInstanceConfig(string serviceConstant, string machineName)
			{
				try
				{
					return getService().GetServiceInstanceConfig(serviceConstant, machineName);
				}
				catch(Exception ex)
				{
					throw new SAException("Failure loading ServiceInstance metadata.", ex);
				}
			}

			public ISettingsService getService()
			{
				string uri = "";
				try
				{
					uri = getServiceManagerUri();
					return (ISettingsService)Activator.GetObject(typeof(ISettingsService), uri);
				}
				catch(Exception ex)
				{
					throw(new SAException("Cannot activate remote service manager at [" + uri + "].",ex));
				}
			}


			public string getServiceManagerUri()
			{
				try
				{
					return Matchnet.InitialConfiguration.InitializationSettings.Get(CUSTOMSETTING_CONFIGURATION_SERVICE_KEY);
				}
				catch(Exception ex)
				{
					throw(new SAException("Cannot get AdapterConfiguration URI based off of CustomSettings key of: " + CUSTOMSETTING_CONFIGURATION_SERVICE_KEY + ".",ex));
				}
			}

			public bool ServerEnabled
			{
				get 
				{
					return _IsServerEnabled;
				}
			}


			private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
			{
				try
				{
					refreshSettings();
					refreshServerEnabledStatus();
				}
				catch (Exception ex)
				{
					new SAException("Error refreshing settings.", ex);
				}
			}
		}
		#endregion

        #region ISettingsSA Members
	
        public string GetSettingFromSingleton(string constant)
        {
            return GetSetting(constant);
        }

        public string GetSettingFromSingleton(string constant, int communityID)
        {
            return GetSetting(constant, communityID);
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID)
        {
            return GetSetting(constant, communityID, siteID);
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID, int brandID)
        {
            return GetSetting(constant, communityID, siteID, brandID);
        }

        public bool IsServerEnabledFromSingleton()
        {
            return IsServerEnabledFromSingleton();
        }

        public ServiceInstanceConfig GetServiceInstanceConfigFromSingleton(string serviceConstant, string machineName)
        {
            return GetServiceInstanceConfig(serviceConstant, machineName);
        }

        public bool SettingExistsFromSingleton(string constant, int communityID, int siteID)
        {
            return SettingExists(constant, communityID, siteID);
        }

        public List<MembaseConfig> GetMembaseConfigsFromSingleton()
        {
            return GetMembaseConfigs();
        }

        public MembaseConfig GetMembaseConfigByBucketFromSingleton(string bucketName)
        {
            return GetMembaseConfigByBucket(bucketName);
        }

        public List<ApiAppSetting> GetAPIAppSettingsFromSingleton()
        {
            return GetApiAppSettings();
        }

	    public void AddNewOrUpdateSetting(string settingConstant, string globalDefaultValue, string settingDescription,
	        int settingCategoryId = 2, bool isRequiredForCommunity = false, bool isRequiredForSite = false)
	    {
            SettingSAInternal.Instance.getService().AddNewOrUpdateSetting(settingConstant, globalDefaultValue,settingDescription,settingCategoryId,isRequiredForCommunity,isRequiredForSite);
            SettingSAInternal.Instance.refreshSettings();
	    }

	    public void UpdateGlobalDefault(string settingConstant, string globalDefaultValue)
	    {
	        SettingSAInternal.Instance.getService().UpdateGlobalDefault(settingConstant,globalDefaultValue);
            SettingSAInternal.Instance.refreshSettings();
	    }

	    public void SetValueForSite(int siteId, string settingConstant, string globalDefaultValue)
	    {
            SettingSAInternal.Instance.getService().SetValueForSite(siteId, settingConstant, globalDefaultValue);
            SettingSAInternal.Instance.refreshSettings();
	    }

	    public void ResetToGlobalDefault(string settingConstant)
	    {
            SettingSAInternal.Instance.getService().ResetToGlobalDefault(settingConstant);
            SettingSAInternal.Instance.refreshSettings();
	    }

	    #endregion
	}
}

