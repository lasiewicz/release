using System;
using System.Collections;
using System.Collections.Specialized;

namespace Matchnet.Configuration.ServiceAdapters.Analitics
{
	/// <summary>
	/// Summary description for AnaliticsScenarioBase.
	/// </summary>
	public class AnaliticsScenarioBase:IAnaliticsScenario
	{	
		const string setting1_format="{0}_{1}_OPERAND";
		const string setting2_format="{0}_{1}_REMAINDER";
		string _name;
		string _settingConstant="SCENARIO";
		Int32 _communityID;
		Int32 _siteID;
		string _setting1;
		string _setting2;

		ArrayList _criteria;
		string _settingval1;
		string _settingval2;

		int _memberid;
		public string Name{get{return _name;} set{_name=value;}}
		public string SettingConstant{get{return _settingConstant;} set{_settingConstant=value;}}
		
		public Int32 CommunityID{get{return _communityID;} set{_communityID=value;}}
		public Int32 SiteID{get{return _siteID;} set{_siteID=value;}}

		//not implemented insted we are using just memberid
		public ArrayList CriteriaValues{get{return _criteria;} set{_criteria=value;}}

		#region constructors
		public AnaliticsScenarioBase(string name, string setting, int memberid)
		{
			_name=name;
			_settingConstant=setting;
			_memberid=memberid;
			InitSettings();
			
		}

		public AnaliticsScenarioBase(string name, string setting, int communityid, int siteid, int memberid)
		{
			_name=name;
			_settingConstant=setting;
			_communityID=communityid;
			_siteID=siteid;
			_memberid=memberid;
			InitSettings();
			
		}

		public AnaliticsScenarioBase(string name, int communityid, int siteid, int memberid)
		{
			_name=name;
		
			_communityID=communityid;
			_siteID=siteid;
			_memberid=memberid;
			InitSettings();
			
		}

		#endregion

		//base implementation for MOD NUM
		public Scenario GetAnaliticsScenario()
		{ Scenario scenario=Scenario.A;// default scenario
			try
			{
				if(!getSettings())
				{
					return Scenario.Unknown;
				}
								 
				int operand=Conversion.CInt(_settingval1);
				int remainder=Conversion.CInt(_settingval2);
				
				//our border line values
				if(operand <= 0 || remainder <= 0 || _memberid <= 0)
				{return scenario;}


				int res=_memberid % operand;
				
				if(res < remainder)
					scenario=Scenario.B;
				else
					scenario=Scenario.A;

				return scenario;

			}
			catch(Exception ex)
			{throw ex;}

		}

		private bool getSettings()
		{
			bool foundScen=true;
			try
			{
				_settingval1=RuntimeSettings.GetSetting(_setting1,_communityID,_siteID);
				_settingval2=RuntimeSettings.GetSetting(_setting2,_communityID,_siteID);

				return foundScen;
			}
			catch(Exception ex)
			{return false;}

		}

		private void InitSettings()
		{
			_setting1=String.Format(setting1_format,_name,_settingConstant);
			_setting2=String.Format(setting2_format,_name,_settingConstant);
			

		}
	}
}
