using System;
using System.Collections;
using System.Collections.Specialized;


namespace Matchnet.Configuration.ServiceAdapters.Analitics
{
	/// <summary>
	/// Summary description for IAnaliticsScenario.
	/// </summary>
	
	public enum Scenario:int
	{
		Unknown=0,
		A=1,
		B,
		C,
		D,
		E,
		//who knows the rest of abc is welcome to append

	}
	public interface IAnaliticsScenario
	{
		string Name{get;set;}
		string SettingConstant{get;set;}
		Int32 CommunityID{get;set;}
	    Int32 SiteID{get;set;}
		ArrayList CriteriaValues{get;set;}
		Scenario GetAnaliticsScenario();
		

	}
}
