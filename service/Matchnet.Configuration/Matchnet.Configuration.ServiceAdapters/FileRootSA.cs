using System;
using System.Collections.Specialized;
using System.IO;
using System.Web;
using System.Collections;
using System.Data;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Configuration;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.File;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;


namespace Matchnet.Configuration.ServiceAdapters
{

	/// <summary>
	/// 
	/// </summary>
	public class FileRootSA : SABase
	{
		/// <summary>
		/// 
		/// </summary>
		public static readonly FileRootSA Instance = new FileRootSA();

		private const string SERVICE_MANAGER_NAME = "FileRootSM";
		private Matchnet.Caching.Cache _cache;

		private FileRootSA()
		{
			_cache = Matchnet.Caching.Cache.Instance;
		}


		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONFIGSVC_SA_CONNECTION_LIMIT"));
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public RootGroups GetRootGroups()
		{
			try
			{
				RootGroups rootGroups = _cache.Get(RootGroups.CACHE_KEY) as RootGroups;

				if (rootGroups == null)
				{
					string uri = getServiceManagerUri();
					base.Checkout(uri);
					try
					{
						rootGroups = getService(uri).GetRootGroups();
					}
					finally
					{
						base.Checkin(uri);
					}
					_cache.Add(rootGroups);
				}

				return rootGroups;
			}
			catch(Exception ex)
			{
				throw new SAException("Failure loading ServiceInstance metadata.", ex);
			}
		}

		
		private IFileRootService getService(string uri)
		{
			try
			{
				return (IFileRootService)Activator.GetObject(typeof(IFileRootService), uri);
			}
			catch(Exception ex)
			{
				throw(new Exception("Cannot activate remote service manager at " + uri + ".",ex));
			}
		}


		private string getServiceManagerUri()
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONFIGSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}
	}
}
