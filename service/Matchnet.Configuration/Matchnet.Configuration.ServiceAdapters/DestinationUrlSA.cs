using System;
using System.Text;
using System.Web;
using System.Collections.Generic;
using System.Linq;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet.Configuration.ValueObjects.Urls;
using Matchnet.Caching;

namespace Matchnet.Configuration.ServiceAdapters
{
	public class DestinationUrlSA: SABase
	{
		public static readonly DestinationUrlSA Instance = new DestinationUrlSA();

		private const string SERVICE_MANAGER_NAME = "DestinationUrlSM";
		/// <summary>
		/// The reload interval will be the minimum time between reload of config.
		/// Actual time will be between [interval] and [interval] * 2; A random time is added to the interval to smooth out the DB hits.
		/// </summary>
		private const double RELOAD_INTERVAL_SECONDS = 10.0;
		private System.Timers.Timer _Timer;
		private DestinationUrls _DestinationUrls;
        private List<DestinationURL> _ListOfDestinationURLs;
        private Matchnet.Caching.Cache _cache;
        private int _cacheTTL;

		private DestinationUrlSA() 
		{
			try
			{
				_DestinationUrls = getDestinationUrlsFromMT();
				if (_DestinationUrls == null)
					new SAException("DestinationUrlSA initialized with empty list!");
			}
			catch (Exception ex)
			{
				new SAException("Failure loading DestinationUrls from MT", ex);
				_DestinationUrls = new DestinationUrls();
			}
		 
			Random rnd = new Random((int) DateTime.Now.Ticks);
			_Timer = new System.Timers.Timer();
			_Timer.Interval = (rnd.NextDouble() * RELOAD_INTERVAL_SECONDS * 1000) + (RELOAD_INTERVAL_SECONDS * 1000);
			_Timer.Elapsed += new System.Timers.ElapsedEventHandler(_Timer_Elapsed);
			_Timer.AutoReset = false; // to avoid double firing while still in operation, manualy start timer after elapsed and op completed.
			_Timer.Start();

            _cache = Matchnet.Caching.Cache.Instance;
            _cacheTTL = Convert.ToInt32(RuntimeSettings.GetSetting("URL_DESTINATION_CACHE_TTL"));
		}


		private void _Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			try
			{
				DestinationUrls result = getDestinationUrlsFromMT();
				if (result != null)
					_DestinationUrls = result;

			}
			catch (Exception ex)
			{
				new SAException("Failure re-loading DestinationUrls from MT", ex);
			}
			_Timer.Start();

		}


		private DestinationUrls getDestinationUrlsFromMT()
		{
			DestinationUrls result = null;

			string uri = getServiceManagerUri();
			base.Checkout(uri);
			try
			{
				result = getService(uri).GetDestinationUrls();
			}
			finally
			{
				base.Checkin(uri);
			}
			return result;
		}

        private List<DestinationURL> getListOfDestinationURLsFromMT()
        {
            List<DestinationURL> result = null;

            string uri = getServiceManagerUri();
            base.Checkout(uri);
            try
            {
                result = getService(uri).GetListOfDestinationURLs();
            }
            finally
            {
                base.Checkin(uri);
            }
            return result;
        }
        


		/// <summary>
		/// Retreive a collection of destination URLs
		/// </summary>
		/// <returns></returns>
		public DestinationUrls GetDestinationUrls() 
		{
			return _DestinationUrls;
		}

        public List<DestinationURL>  GetListOfDestinationURLs()
        {
            List<DestinationURL> urls = null;
            CachedDestinationURLs cachedURLs = _cache.Get(DestinationURL.COLLECTION_CACHE_KEY) as CachedDestinationURLs;

            if (cachedURLs == null)
            {
                urls = getListOfDestinationURLsFromMT();
                _cache.Add(new CachedDestinationURLs(urls, _cacheTTL));
            }
            else
            {
                urls = cachedURLs.URLs;
            }

            return urls;
        }

/// <summary>
/// 
/// </summary>
/// <param name="destinationURL">The URL to validate against the list of allowed URLs</param>
/// <param name="isWebServiceContext">Whether this is in web service context or not.</param>
/// <returns>true if URL should be allowed to redirect, false otherwise.</returns>
		public bool IsAllowedRedirect(string destinationURL, bool isWebServiceContext) 
		{ 
			string url = HttpUtility.UrlDecode(destinationURL.ToLower());

			if (url.StartsWith("/") == true)
				return true;
				
			
			DestinationUrls urls = GetDestinationUrls();
			if (urls == null) return false;

			DestinationUrlProperty property = urls[url];

			if (property != null &&
				property.IsAllowedRedirect
				&& property.IsWebServiceContext == isWebServiceContext)
			{
				return true;
			}
  	
			return false;
		}

        public bool IsAllowedRedirectFromList(string destinationURL, bool isWebServiceContext)
        {
            string url = HttpUtility.UrlDecode(destinationURL.ToLower());

            if (url.StartsWith("/") == true)
                return true;


            List<DestinationURL> urls = GetListOfDestinationURLs();
            if (urls == null) return false;

            DestinationURL targetURL = (from u in urls where u.URL == destinationURL select u).FirstOrDefault();

            if (targetURL != null &&
                targetURL.IsAllowedRedirect
                && targetURL.IsWebServiceContext == isWebServiceContext)
            {
                return true;
            }

            return false;
        }


		private IDestinationUrlService getService(string uri)
		{
			try
			{
				return (IDestinationUrlService)Activator.GetObject(typeof(IDestinationUrlService), uri);
			}
			catch (Exception ex)
			{
				throw (new Exception("Cannot activate remote service manager at " + uri + ".", ex));
			}
		}


		protected override void GetConnectionLimit()
		{
			base.MaxConnections = 20;
		}


		private string getServiceManagerUri()
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONFIGSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch (Exception ex)
			{
				throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
			}
		}

	}
}
