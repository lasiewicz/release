using System;
using System.Text;

using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;


namespace Matchnet.Configuration.ServiceAdapters
{
   public class AccessTicketSA: SABase
   {
	  private const string SERVICE_MANAGER_NAME = "AccessTicketSM";

	  #region Singleton
	  public static readonly AccessTicketSA Instance = new AccessTicketSA();

	  private AccessTicketSA() { }
	  #endregion

	  public AccessTicket GetAccessTicket(string ticketKey)
	  {

		 AccessTicket result = null;
		 string uri = string.Empty;

		 try
		 {
			 object obj = Caching.Cache.Instance.Get(ticketKey);
			 if (obj != null) result = obj as AccessTicket;
			//result = Matchnet.Caching.Cache.Instance.Get(ticketKey) as AccessTicket;

			if (result == null)
			{

			   uri = getServiceManagerUri();

			   try
			   {
				  base.Checkout(uri);
				  result = getService(uri).GetAccessTicket(ticketKey);
			   }
			   finally
			   {
				  base.Checkin(uri);
			   }
			   
			   if (result != null)
			   {
				   Cache.Instance.Insert(result);
//				  result.AddToCache();
			   }
			   else {
				  ///TODO: Protect against multi calls. If failed to get object, complain and  cache an empty one?
				  new SAException("Can't get AccessTicket " + ticketKey);
			   }
			}

			return result;
		 }
		 catch (Exception ex)
		 {
			throw (new SAException("Cannot retrieve AccessTicket (uri: " + uri + ")", ex));
		 }
	  }
	   public SubscriptionInfo GetSubscriptionInfo(int AccessTicketID, int SubscriptionInfoID)
	   {

		   SubscriptionInfo result = null;
		   string uri = string.Empty;

		   try
		   {
			   uri = getServiceManagerUri();
			   try
			   {
				   base.Checkout(uri);
				   result = getService(uri).GetSubscriptionInfo(AccessTicketID, SubscriptionInfoID);
			   }
			   finally
			   {
				   base.Checkin(uri);
			   }
			   return result;
		   }
		   catch (Exception ex)
		   {
				throw (new SAException("Cannot retrieve SubscriptionInfo (uri: " + uri + ")", ex));
		   }
	}

	   
	  
	  protected override void GetConnectionLimit()
	  {
		 base.MaxConnections = 32;
	  }

	  private IAccessTicketService getService(string uri)
	  {
		 try
		 {
			return (IAccessTicketService)Activator.GetObject(typeof(IAccessTicketService), uri);
		 }
		 catch (Exception ex)
		 {
			throw (new SAException("Cannot activate remote service manager at " + uri, ex));
		 }
	  }

	  private string getServiceManagerUri()
	  {
		 try
		 {
			string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ValueObjects.ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(SERVICE_MANAGER_NAME);
			string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONFIGSVC_SA_HOST_OVERRIDE");

			if (overrideHostName.Length > 0)
			{
			   UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
			   return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
			}

			return uri;
		 }
		 catch (Exception ex)
		 {
			throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
		 }
	  }

   }
}



