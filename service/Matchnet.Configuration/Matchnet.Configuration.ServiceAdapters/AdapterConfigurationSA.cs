using System;
using System.Collections;
using System.Collections.Generic;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Configuration;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;


namespace Matchnet.Configuration.ServiceAdapters
{
	/// <summary>
	/// 
	/// </summary>
	public enum PartitionMode : short
	{
		/// <summary>
		/// No partition offset calculation.
		/// </summary>
		None,

		/// <summary>
		/// Use a CRC and/or Modulus calculation to determine the partition offset.
		/// </summary>
		Calculated,
		
		/// <summary>
		/// Use a random calculation to determine the partition offset.
		/// </summary>
		Random
	}

	/// <summary>
	/// 
	/// </summary>
	public class AdapterConfigurationSA
	{
		private const string CUSTOMSETTING_CONFIGURATION_SERVICE_KEY = "configuration/services/AdapterConfigurationUri";

		private static System.Timers.Timer _timer;
		private static Hashtable _services = null;
		private static ServicePartitions _servicePartitions = null;
		private static System.Random _rnd = new System.Random();

		private AdapterConfigurationSA()
		{

		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public static ServicePartitions GetPartitions()
		{
			if (_servicePartitions == null)
			{
				refresh();
			}

			return _servicePartitions;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="serviceConstant"></param>
		/// <param name="partitionMode"></param>
		/// <returns></returns>
		public static ServicePartition GetServicePartition(string serviceConstant, PartitionMode partitionMode)
		{
			if(partitionMode != PartitionMode.None && partitionMode != PartitionMode.Random)
			{
				throw(new Exception("PartitionMode.None or PartitionMode.Random must be selected if a partition offset value is NOT passed to the ServiceAdapterConfiguration.GetServiceUri() method."));
			}

			ArrayList offsets = getServicePartitions()[serviceConstant] as ArrayList;
			Int32 partitionIndex = 0;

			if (partitionMode == PartitionMode.Random)
			{

				if (offsets == null)
				{
					throw new Exception("No service parititions found for " + serviceConstant + ".");
				}

				if (offsets.Count > 1)
				{
					partitionIndex = (_rnd.Next(0, offsets.Count));
				}
			}

			return offsets[partitionIndex] as ServicePartition;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="serviceConstant"></param>
		/// <param name="partitionMode"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public static ServicePartition GetServicePartition(string serviceConstant, PartitionMode partitionMode, int value)
		{
			if(partitionMode == PartitionMode.None)
			{
				throw(new Exception("PartitionMode.None cannot be selected if a partition offset value is passed to the ServiceAdapterConfiguration.GetServiceUri() method."));
			}

			ArrayList offsets = getServicePartitions()[serviceConstant] as ArrayList;

			if (offsets == null)
			{
				throw new Exception("No service parititions found for " + serviceConstant + ".");
			}

			return offsets[value % offsets.Count] as ServicePartition;
		}


		public static string[] GetServiceHosts(string serviceConstant)
		{
			ArrayList offsets = getServicePartitions()[serviceConstant] as ArrayList;
			ArrayList hosts = new ArrayList();

			for (Int32 offsetNum = 0; offsetNum < offsets.Count; offsetNum++)
			{
				ServicePartition partition = offsets[offsetNum] as ServicePartition;
				for (Int32 siNum = 0; siNum < partition.Count; siNum++)
				{
					hosts.Add(partition[siNum].HostName);
				}
			}

			return (string[])hosts.ToArray(typeof(string));
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="serviceConstant"></param>
		/// <param name="partitionMode"></param>
		/// <param name="guid"></param>
		/// <returns></returns>
		public static ServicePartition GetServicePartition(string serviceConstant, PartitionMode partitionMode, Guid guid)
		{
			ArrayList offsets = getServicePartitions()[serviceConstant] as ArrayList;

			if (offsets == null)
			{
				throw new Exception("No service parititions found for " + serviceConstant + ".");
			}

			return offsets[CRC.GetCRCKey(guid.ToString()) % offsets.Count] as ServicePartition;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceConstant"></param>
        /// <param name="partitionMode"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static ArrayList GetServicePartitions(string serviceConstant)
        {
           

            ArrayList offsets = getServicePartitions()[serviceConstant] as ArrayList;

            if (offsets == null)
            {
                throw new Exception("No service parititions found for " + serviceConstant + ".");
            }
            return offsets;
           
        }

		private static void refresh()
		{
			string uri = "";

			ServicePartitions servicePartitionsFugly = null;
			try
			{
				uri = getServiceManagerUri();

				servicePartitionsFugly = getService(uri).GetAll();

				if(servicePartitionsFugly.Count > 0)
				{
					_servicePartitions = servicePartitionsFugly;
					Hashtable services = new Hashtable();
					ArrayList offsets = null;
					string constantLast = null;

					//this is fugly, converting OG structure to something that performs *much* better
					for (Int32 spNum = 0; spNum < servicePartitionsFugly.Count; spNum++)
					{
						ServicePartition servicePartition = servicePartitionsFugly[spNum];
						
						if (servicePartition.Constant != constantLast)
						{
							constantLast = servicePartition.Constant;
							offsets = new ArrayList();
							services[constantLast] = offsets;
						}

						offsets.Add(servicePartition);
					}

					_services = services;
				}
			}
			catch(Exception ex)
			{
				new SAException("Unable to load servicePartitions, reverting to last successful load.", ex);
			}
		}


		private static void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			try
			{
				refresh();
			}
			catch (Exception ex)
			{
				new SAException("Error refreshing adapter configuration.", ex);
			}
		}

		
		private static Hashtable getServicePartitions()
		{
			if (_services == null)
			{
				_timer = new System.Timers.Timer(60000);
				_timer.Enabled = false;
				_timer.AutoReset = true;
				_timer.Elapsed += new System.Timers.ElapsedEventHandler(Timer_Elapsed);
				_timer.Start();

				refresh();
			}

			return _services;
		}


		private static IAdapterConfigurationService getService(string uri)
		{
			try
			{
				return (IAdapterConfigurationService)Activator.GetObject(typeof(IAdapterConfigurationService), uri);
			}
			catch(Exception ex)
			{
				throw(new Exception("Cannot activate remote service manager. (uri: " + uri + ")",ex));
			}
		}


		private static string getServiceManagerUri()
		{
			try
			{
				return Matchnet.InitialConfiguration.InitializationSettings.Get(CUSTOMSETTING_CONFIGURATION_SERVICE_KEY);
			}
			catch(Exception ex)
			{
				throw(new Exception("Cannot get AdapterConfiguration URI based off of CustomSettings key of: " + CUSTOMSETTING_CONFIGURATION_SERVICE_KEY + ".",ex));
			}
		}

	}
}
