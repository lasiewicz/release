﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;
using Matchnet.Configuration.ValueObjects.Management;
using Matchnet.Exceptions;
namespace Matchnet.Configuration.ServiceAdapters
{
    public class ManagementSA
    {
        public readonly static ManagementSA Instance = new ManagementSA();
        public string URI { get; set; }


        public Settings GetSettings()
        {
            try
            {
                return getService().GetSettings();

            }
            catch (Exception ex)
            {
                throw (new BLException("Failure populating settings for management tool.", ex));
            }

        }


        public List<Server> GetServers()
        {
            try
            {
                return getService().GetServers();

            }
            catch (Exception ex)
            {
                throw (new BLException("Failure populating settings for management tool.", ex));
            }

        }

        public void SaveSettings(List<SettingsSave> settingslist, bool asyncWrite)
        {
            try
            {
                if (settingslist == null || settingslist.Count == 0)
                    throw (new Exception("Settings List is empty"));


                getService().SaveSettings(settingslist, asyncWrite);

            }
            catch (Exception ex)
            {
                throw (new BLException("Failure saving settings for management tool.", ex));
            }


        }

        private IManagementService getService()
        {

            try
            {
                return (IManagementService)Activator.GetObject(typeof(ISettingsService), URI);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at [" + URI + "].", ex));
            }
        }



    }
}
