using System;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;


namespace Matchnet.Configuration.ServiceAdapters
{
	/// <summary>
	/// 
	/// </summary>
	public class KeySA : SABase
	{
		private const string SERVICE_MANAGER_NAME = "KeySM";

		/// <summary>
		/// 
		/// </summary>
		public static readonly KeySA Instance = new KeySA();

		private KeySA()
		{
		}


		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONFIGSVC_SA_CONNECTION_LIMIT"));
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="keyName"></param>
		/// <returns></returns>
		public Int32 GetKey(string keyName)
		{
			string uri = "";

			try
			{
				uri = getServiceManagerUri();
				base.Checkout(uri);
				try
				{
					return getService(uri).GetKey(keyName);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw(new SAException("Error occurred retrieving key. (uri: " + uri + ")", ex));
			}
		}


		private IKeyService getService(string uri)
		{
			try
			{
				return (IKeyService)Activator.GetObject(typeof(IKeyService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}


		private string getServiceManagerUri()
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONFIGSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}
	}
}
