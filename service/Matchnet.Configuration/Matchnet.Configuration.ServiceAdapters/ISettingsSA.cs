﻿using System;
using System.Collections.Generic;
using Matchnet.Configuration.ValueObjects;

namespace Matchnet.Configuration.ServiceAdapters
{
    public interface ISettingsSA
    {
        string GetSettingFromSingleton(string constant);
        string GetSettingFromSingleton(string constant, Int32 communityID);
        string GetSettingFromSingleton(string constant, Int32 communityID, Int32 siteID);
        string GetSettingFromSingleton(string constant, Int32 communityID, Int32 siteID, Int32 brandID);
        bool IsServerEnabledFromSingleton();
        ServiceInstanceConfig GetServiceInstanceConfigFromSingleton(string serviceConstant, string machineName);
        bool SettingExistsFromSingleton(string constant, Int32 communityID, Int32 siteID);
        List<MembaseConfig> GetMembaseConfigsFromSingleton();
        MembaseConfig GetMembaseConfigByBucketFromSingleton(string bucketName);
        List<ApiAppSetting> GetAPIAppSettingsFromSingleton();
        void AddNewOrUpdateSetting(string settingConstant, string globalDefaultValue, string settingDescription, int settingCategoryId = 2, bool isRequiredForCommunity = false, bool isRequiredForSite = false);
        void UpdateGlobalDefault(string settingConstant, string globalDefaultValue);
        void SetValueForSite(int siteId, string settingConstant, string globalDefaultValue);
        void ResetToGlobalDefault(string settingConstant);
    }
}
