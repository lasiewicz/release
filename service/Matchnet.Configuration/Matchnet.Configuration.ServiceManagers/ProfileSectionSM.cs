using System;
using System.Text;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet.Configuration.BusinessLogic;
using Matchnet.Exceptions;

namespace Matchnet.Configuration.ServiceManagers
{
   public class ProfileSectionSM : MarshalByRefObject, IServiceManager, IProfileSectionService
   {
	  #region IServiceManager Members

	  public void PrePopulateCache()
	  {
	  }

	  #endregion

	  #region IDisposable Members

	  public void Dispose()
	  {
	  }

	  #endregion

	  #region IProfileSectionService Members

	  public ProfileSectionDefinition GetProfileSectionDefinition(int profileSectionID)
	  {
		 try
		 {
			return ProfileSectionBL.Instance.GetProfileSectionDefinition(profileSectionID);
		 }
		 catch (ExceptionBase ex)
		 {
			throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting ProfileSectionDefinition " + profileSectionID.ToString(), ex);
		 }
		 catch (Exception ex)
		 {
			throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting ProfileSectionDefinition " + profileSectionID.ToString(), ex);
		 }


	  }

	  public ProfileSectionDefinitions GetProfileSectionDefinitions(int[] profileSectionIDs)
	  {
		 try
		 {
			return ProfileSectionBL.Instance.GetProfileSectionDefinitions(profileSectionIDs);
		 }
		 catch (ExceptionBase ex)
		 {
			string [] ids = new string [profileSectionIDs.Length];
			for (int i = 0; i < ids.Length; ids[i] = profileSectionIDs[i].ToString(), i++);
			throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting ProfileSectionDefinitions " + string.Join("|",ids), ex);
		 }
		 catch (Exception ex)
		 {
			string [] ids = new string [profileSectionIDs.Length];
			for (int i = 0; i < ids.Length; ids[i] = profileSectionIDs[i].ToString(), i++);

			throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting ProfileSectionDefinition " + string.Join("|",ids), ex);
		 }
	  }

	  #endregion

	  public override object InitializeLifetimeService()
	  {
		 return null;
	  }

   }
}
