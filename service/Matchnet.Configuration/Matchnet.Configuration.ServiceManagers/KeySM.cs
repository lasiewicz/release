using System;

using Matchnet.Exceptions;
using Matchnet.Configuration.BusinessLogic;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;


namespace Matchnet.Configuration.ServiceManagers
{
	public class KeySM : MarshalByRefObject, IKeyService, IServiceManager, IDisposable
	{
		public KeySM()
		{
		}


		public override object InitializeLifetimeService()
		{
			return null;
		}


		public void PrePopulateCache()
		{
		}


		public Int32 GetKey(string keyName)
		{
			try
			{
				return KeyBL.Instance.GetKey(keyName);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occurred retrieving key.", ex);
			}
		}


		public void Dispose()
		{
		}
	}
}
