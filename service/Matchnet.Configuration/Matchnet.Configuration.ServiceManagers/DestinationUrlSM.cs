using System;
using System.Text;
using System.Collections.Generic;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet.Configuration.ValueObjects.Urls;
using Matchnet.Configuration.BusinessLogic;
using Matchnet.Exceptions;


namespace Matchnet.Configuration.ServiceManagers
{
	public class DestinationUrlSM : MarshalByRefObject, IServiceManager, IDestinationUrlService
	{
		#region IServiceManager Members

		public void PrePopulateCache()
		{
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
		}

		#endregion

		#region IDestinationUrlService Members
		public DestinationUrls GetDestinationUrls()
		{
			try
			{
				return DestinationUrlBL.Instance.GetDestinationUrls();
			}
			catch (ExceptionBase ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting DestinationUrls (1)", ex);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting DestinationUrls (2)", ex);
			}
		}

        public List<DestinationURL> GetListOfDestinationURLs()
        {
            try
            {
                return DestinationUrlBL.Instance.GetListOfDestinationURLs();
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting ListOfDestinationUrls (1)", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting ListOfDestinationUrls (2)", ex);
            }
        }
		#endregion


		public override object InitializeLifetimeService()
		{
			return null;
		}

	}
}
