using System;

using Matchnet.Configuration.BusinessLogic;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;
using Matchnet.Exceptions;


namespace Matchnet.Configuration.ServiceManagers
{
	public class AdapterConfigurationSM : MarshalByRefObject, IAdapterConfigurationService, IServiceManager, IDisposable
	{
		private const string SERVICE_MANAGER_NAME = "AdapterConfigurationSM";

		public override object InitializeLifetimeService()
		{
			return null;
		}


		public void PrePopulateCache()
		{
		}


		public ServicePartitions GetAll()
		{
			try
			{
				return AdapterConfigurationBL.Instance.GetAll();
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting adapter configurations.", ex));
			}
		}


		public void Dispose()
		{
		}
	}
}
