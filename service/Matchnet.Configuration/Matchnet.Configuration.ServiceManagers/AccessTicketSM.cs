using System;
using System.Text;

using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet.Configuration.BusinessLogic;
using Matchnet.Exceptions;

namespace Matchnet.Configuration.ServiceManagers
{
   public class AccessTicketSM : MarshalByRefObject, IServiceManager, IAccessTicketService
   {
	  #region IServiceManager Members

	  public void PrePopulateCache()
	  {
	  }

	  #endregion

	  #region IDisposable Members

	  public void Dispose()
	  {
	  }

	  #endregion


	  #region IAccessTicketService Members

	  public AccessTicket GetAccessTicket(string ticketKey)
	  {
		 try
		 {
			AccessTicket result = null;
			object obj = Caching.Cache.Instance.Get(ticketKey);
			if (obj != null) result = obj as AccessTicket;
			//result = Matchnet.Caching.Cache.Instance.Get(ticketKey) as AccessTicket;
			if (result == null)
			{
			   result = AccessTicketBL.Instance.GetAccessTicket(ticketKey);
			   if (result != null)
			   {
				  Caching.Cache.Instance.Add(result);
//				  result.AddToCache();
			   }
			   else
			   {
				  ///TODO: Protect against multi calls. If failed to get object, complain and  cache an empty one?
				  new Matchnet.Exceptions.ServiceBoundaryException("AccessTicketSM", "Can't get AccessTicket " + ticketKey);
			   }
			}
			return result;		
		 }
		 catch (ExceptionBase ex)
		 {
			throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting AccessTicket " + ticketKey, ex);
		 }
		 catch (Exception ex)
		 {
			throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting AccessTicket " + ticketKey, ex);
		 }
	  }

	   public SubscriptionInfo GetSubscriptionInfo(int AccessTicketID, int SubscriptionInfoID)
	   {
		   SubscriptionInfo result = null;
		   result = AccessTicketBL.Instance.GetSubscriptionInfo(AccessTicketID, SubscriptionInfoID);
		   if(result==null)
		   {
			   new Matchnet.Exceptions.ServiceBoundaryException("AccessTicketSM", "Can't get SubscriptionInfo " + AccessTicketID.ToString() +"_"+SubscriptionInfoID.ToString() );
		   }
		   return result;
	   }

	  #endregion

	  public override object InitializeLifetimeService()
	  {
		 return null;
	  }
   }

}
