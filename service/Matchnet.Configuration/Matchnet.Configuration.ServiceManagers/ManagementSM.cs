﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;
using Matchnet.Configuration.ValueObjects.Management;
using Matchnet.Configuration.BusinessLogic;
using Matchnet.Exceptions;
using Matchnet.Data.Hydra;
namespace Matchnet.Configuration.ServiceManagers
{
    public class ManagementSM : MarshalByRefObject, IManagementService, IServiceManager, IDisposable
    {
        private HydraWriter _hydraWriter;
        public ManagementSM()
        {
            try
            {
                _hydraWriter = new HydraWriter(new string[] { "mnSystem","mnSystemWrite" });
                _hydraWriter.Start();

            }
            catch (Exception ex)
            { }

        }


        public Settings GetSettings()
        {
            try
            {
                return ManagementBL.Instance.GetSettings();

            }
            catch (Exception ex)
            {
                throw (new BLException("Failure populating settings for management tool.", ex));
            }

        }

        public void SaveSettings(List<SettingsSave> settingslist, bool asyncWrite)
        {
            try
            {
                ManagementBL.Instance.SaveSettings(settingslist, asyncWrite);

            }
            catch (Exception ex)
            {
                throw (new BLException("Failure saving settings for management tool.", ex));
            }


        }

        public List<Server> GetServers()
        {
            try
            {
                return ManagementBL.Instance.GetServers();

            }
            catch (Exception ex)
            {
                throw (new BLException("Failure retrieving servers list for management tool.", ex));
            }
        }

        public override object InitializeLifetimeService()
        {
            return null;
        }


        public void PrePopulateCache()
        {
        }

        public void Dispose()
        {
            try
            {
                if (_hydraWriter != null)
                    _hydraWriter.Stop();
            }
            catch (Exception ex) { }
        }
    }
}
