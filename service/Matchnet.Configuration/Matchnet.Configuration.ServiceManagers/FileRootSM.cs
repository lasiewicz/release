using System;
using System.Collections.Specialized;

using Matchnet.Exceptions;
using Matchnet.Configuration.BusinessLogic;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.File;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;


namespace Matchnet.Configuration.ServiceManagers
{
	public class FileRootSM : MarshalByRefObject, IFileRootService, IServiceManager, IDisposable
	{
		public FileRootSM() {}


		public override object InitializeLifetimeService()
		{
			return null;
		}


		public void PrePopulateCache()
		{
		}

		public RootGroups GetRootGroups()
		{
			try
			{
				return FileRootBL.Instance.GetRootGroups();;
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure loading RootGroups.", ex));
			}
		}
		
		public void Dispose()
		{
		}
	}
}
