using System;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Configuration.BusinessLogic;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.Lpd;


namespace Matchnet.Configuration.ServiceManagers
{
	public class LpdSM : MarshalByRefObject, Matchnet.IBackgroundProcessor, ILpdService
	{
		public LpdSM()
		{
		}

		public override object InitializeLifetimeService()
		{
			return null;
		}


		public void Start()
		{
			LpdBL.Instance.Start();
		}

		
		public void Stop()
		{
			LpdBL.Instance.Stop();
		}


		public Guid GetVersion()
		{
			try
			{
				return LpdBL.Instance.GetVersion();
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
					"Uanble to get LPD version.",
					ex);
			}
		}


		public LogicalDatabases GetLogicalDatabases()
		{
			try
			{
				return LpdBL.Instance.GetLogicalDatabases();
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
					"Uanble to get LPD version.",
					ex);
			}
		}

	}
}
