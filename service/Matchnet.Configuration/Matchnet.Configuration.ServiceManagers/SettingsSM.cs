using System;
using System.Collections.Generic;
using System.Collections.Specialized;

using Matchnet.Exceptions;
using Matchnet.Configuration.BusinessLogic;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.File;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;


namespace Matchnet.Configuration.ServiceManagers
{
	public class SettingsSM : MarshalByRefObject, ISettingsService, IServiceManager, IDisposable
	{
		public SettingsSM() {}


		public override object InitializeLifetimeService()
		{
			return null;
		}


		public void PrePopulateCache()
		{
		}


		public NameValueCollection GetSettings(string machineName)
		{
			try
			{
				return SettingsBL.Instance.GetSettings(machineName);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting settings.", ex));
			}
		}


		public bool IsServerEnabled(string serverName)
		{
			try
			{
				return SettingsBL.Instance.IsServerEnabled(serverName);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting server enabled status (serverName: " + serverName + ").", ex));
			}
		}


		public ServiceInstanceConfig GetServiceInstanceConfig(string serviceConstant, string machineName)
		{
			try
			{
				return SettingsBL.Instance.GetServiceInstanceConfig(serviceConstant, machineName);
			}
			catch(Exception ex)
			{
				throw(new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure loading ServiceInstance metadata.", ex));
			}
		}

        public List<MembaseConfig> GetMembaseConfigs()
        {
            try
            {
                return SettingsBL.Instance.GetMembaseConfigs();
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure loading Membase config metadata.", ex));
            }
        }

        public MembaseConfig GetMembaseConfigByBucket(string bucketName)
        {
            try
            {
                return SettingsBL.Instance.GetMembaseConfigByBucket(bucketName);
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure loading Membase config metadata.", ex));
            }
        }

	    public List<ApiAppSetting> GetApiAppSettings()
	    {
            try
            {
                return SettingsBL.Instance.GetApiAppSettings();
            }
            catch (Exception ex)
            {
                throw (new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure API app settings data.", ex));
            }
	    }

        public void AddNewOrUpdateSetting(string settingConstant, string globalDefaultValue, string settingDescription, int settingCategoryId = 2, bool isRequiredForCommunity = false, bool isRequiredForSite = false)
        {
            SettingsBL.Instance.AddNewOrUpdateSetting(settingConstant, globalDefaultValue, settingDescription, settingCategoryId, isRequiredForCommunity, isRequiredForSite);
        }

        public void UpdateGlobalDefault(string settingConstant, string globalDefaultValue)
        {
            SettingsBL.Instance.UpdateGlobalDefault(settingConstant, globalDefaultValue);
        }

        public void SetValueForSite(int siteId, string settingConstant, string globalDefaultValue)
        {
            SettingsBL.Instance.SetValueForSite(siteId, settingConstant, globalDefaultValue);
        }

        public void ResetToGlobalDefault(string settingConstant)
        {
            SettingsBL.Instance.ResetToGlobalDefault(settingConstant);
        }

	    public void Dispose()
		{
		}
	}
}
