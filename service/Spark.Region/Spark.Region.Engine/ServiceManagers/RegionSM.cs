﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.UnifiedPurchaseSystem.Lib.Logging;
using Spark.Region.Engine.BusinessLogic;
using Spark.UnifiedPurchaseSystem.Lib.Exceptions;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using Spark.Region.Engine.ValueObjects;
using Spark.UnifiedPurchaseSystem.Lib.InternalResponse;
using Spark.UnifiedPurchaseSystem.Lib.ServiceCallResponse;

namespace Spark.Region.Engine.ServiceManagers
{
    public class RegionSM
    {
        public RegionSM()
        {

        }

        public string GetRegionIDByZipCode(string zipCode)
        {
            RegionResponse regionResponse = new RegionResponse();
            int regionID = Constants.NULL_INT;
            string xmlResponse = Constants.NULL_STRING;

            try
            {
                TraceLog.Enter("RegionSM.GetRegionIDByZipCode");
                TraceLog.WriteLine("Begin RegionSM.GetRegionIDByZipCode({0})", zipCode);

                RegionBL regionBL = new RegionBL();
                regionID = regionBL.GetRegionIDByZipCode(zipCode, ref regionResponse);
                TraceLog.WriteLine("Return Region ID [{0}]", regionID);

                SetSuccessfulCall(ref regionResponse);
            }
            catch (Exception ex)
            {
                SMException smException = new SMException("RegionSM.GetRegionIDByZipCode() error.", ex);
                ExceptionUtility.LogException(smException, typeof(RegionSM), "Failed on PurchaseManagerSM.GetRegionIDByZipCode()");

                SetFailedCall(ref regionResponse);
            }
            finally
            {
                TraceLog.WriteLine("End of PurchaseManagerSM.GetRegionIDByZipCode()");
                TraceLog.Leave("PurchaseManagerSM.GetRegionIDByZipCode");
            }

            XMLSerializationHelper<XMLGetRegionIDResponse> xmlSerializationHelper = new XMLSerializationHelper<XMLGetRegionIDResponse>();
            RegionResponseXMLHelper regionResponseHelper = new RegionResponseXMLHelper();
            xmlResponse = xmlSerializationHelper.SerializeObject(regionResponseHelper.BuildXMLGetRegionIDResponse(regionResponse.InternalResponse, Convert.ToString(regionID)));

            return xmlResponse;
        }

        public string GetRegionIDByCountry(string country)
        {
            RegionResponse regionResponse = new RegionResponse();
            int regionID = Constants.NULL_INT;
            string xmlResponse = Constants.NULL_STRING;

            try
            {
                TraceLog.Enter("RegionSM.GetRegionIDByCountry");
                TraceLog.WriteLine("Begin RegionSM.GetRegionIDByCountry({0})", country);

                RegionBL regionBL = new RegionBL();
                regionID = regionBL.GetRegionIDByCountry(country, ref regionResponse);
                TraceLog.WriteLine("Return Region ID [{0}]", regionID);

                SetSuccessfulCall(ref regionResponse);
            }
            catch (Exception ex)
            {
                SMException smException = new SMException("RegionSM.GetRegionIDByCountry() error.", ex);
                ExceptionUtility.LogException(smException, typeof(RegionSM), "Failed on PurchaseManagerSM.GetRegionIDByCountry()");

                SetFailedCall(ref regionResponse);
            }
            finally
            {
                TraceLog.WriteLine("End of PurchaseManagerSM.GetRegionIDByCountry()");
                TraceLog.Leave("PurchaseManagerSM.GetRegionIDByCountry");
            }

            XMLSerializationHelper<XMLGetRegionIDResponse> xmlSerializationHelper = new XMLSerializationHelper<XMLGetRegionIDResponse>();
            RegionResponseXMLHelper regionResponseHelper = new RegionResponseXMLHelper();
            xmlResponse = xmlSerializationHelper.SerializeObject(regionResponseHelper.BuildXMLGetRegionIDResponse(regionResponse.InternalResponse, Convert.ToString(regionID)));

            return xmlResponse;
        }

        /// <summary>
        /// Sets the internal response of the region response if the internal response does not
        /// exists  
        /// </summary>
        /// <param name="paymentResponse"></param>
        private void SetSuccessfulCall(ref RegionResponse regionResponse)
        {
            // To report generically on any system or database errors without providing
            // details of the error  
            if (regionResponse.InternalResponse == null)
            {
                // Only set an internal response if there is no internal response set already
                // Internal response may be set in validations or communication to the gateway
                RegionBL regionBL = new RegionBL();
                regionResponse.InternalResponse = regionBL.GetInternalResponse(ServiceStatusType.Success);
            }
        }

        /// <summary>
        /// Sets the internal response of the region response whenever an error is thrown and
        /// caught at the service manager  
        /// </summary>
        /// <param name="paymentResponse"></param>
        private void SetFailedCall(ref RegionResponse regionResponse)
        {
            // To report generically on any system or database errors without providing
            // details of the error  
            // The internal response gets set only when an error is thrown and caught at the sevice manager  
            RegionBL regionBL = new RegionBL();
            regionResponse.InternalResponse = regionBL.GetInternalResponse(ServiceStatusType.SystemError);
        }
    }
}
