﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Region.Engine.ValueObjects;
using Spark.UnifiedPurchaseSystem.Lib.InternalResponse;

namespace Spark.Region.Engine.ServiceManagers
{
    internal class RegionResponseXMLHelper
    {
        public XMLGetRegionIDResponse BuildXMLGetRegionIDResponse(InternalResponse internalResponse, string regionID)
        {
            XMLGetRegionIDResponse response = new XMLGetRegionIDResponse();
            response.responseCode = internalResponse.InternalResponseCode;
            response.responseMessage = internalResponse.InternalResponseDescription;
            response.responseTime = Convert.ToString(DateTime.Now.ToUniversalTime());
            response.regionID = regionID;

            return response;
        }

        public XMLGetRegionIDByZipCodeResponse BuildXMLGetRegionIDByZipCodeResponse(InternalResponse internalResponse, string regionID)
        {
            XMLGetRegionIDByZipCodeResponse response = new XMLGetRegionIDByZipCodeResponse();
            response.responseCode = internalResponse.InternalResponseCode;
            response.responseMessage = internalResponse.InternalResponseDescription;
            response.responseTime = Convert.ToString(DateTime.Now.ToUniversalTime());
            response.regionID = regionID;

            return response;
        }
    }
}
