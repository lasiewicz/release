﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Data.Hydra;

namespace Spark.Region.Engine.ValueObjects
{
    public class HydraWriterWrapper
    {
        public static readonly HydraWriterWrapper Instance = new HydraWriterWrapper();
        private static HydraWriter hydraWriter;

        private HydraWriterWrapper()
        {
            hydraWriter = new HydraWriter(new string[] { "epRegion" });
        }

        public void HydraWriterStart()
        {
            hydraWriter.Start();
        }

        public void HydraWriterStop()
        {
            hydraWriter.Stop();
        }
    }
}
