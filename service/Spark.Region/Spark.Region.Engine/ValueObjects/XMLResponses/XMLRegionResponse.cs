﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.Region.Engine.ValueObjects
{
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://spark.net/schema/online")]
    [System.Xml.Serialization.XmlRootAttribute(IsNullable = false, Namespace = "http://spark.net/schema/online")]
    public class XMLRegionResponse
    {
        [System.Xml.Serialization.XmlElement()]
        public string responseCode;

        [System.Xml.Serialization.XmlElement()]
        public string responseMessage;

        [System.Xml.Serialization.XmlElement()]
        public string responseTime;
    }
}
