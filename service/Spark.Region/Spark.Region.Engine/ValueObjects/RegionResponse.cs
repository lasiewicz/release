﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Spark.UnifiedPurchaseSystem.Lib.InternalResponse;

namespace Spark.Region.Engine.ValueObjects
{
    [DataContract(Name = "RegionResponse", Namespace = "http://spark.net/")]
    public class RegionResponse
    {
        #region Private Members

        private InternalResponse _internalResponse = null;
        // Response output
        private string _regionID = null;

        #endregion

        #region Constructors

        public RegionResponse()
        {

        }

        #endregion

        #region Properties

        [DataMember(Name = "internalResponse", EmitDefaultValue = false, Order = 1)]
        public InternalResponse InternalResponse
        {
            get { return _internalResponse; }
            set { _internalResponse = value; }
        }

        [DataMember(Name = "regionID", EmitDefaultValue = false, Order = 2)]
        public string RegionID
        {
            get { return this._regionID; }
            set { this._regionID = value; }
        }

        #endregion
    }
}
