﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.UnifiedPurchaseSystem.Lib.Exceptions;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using Spark.UnifiedPurchaseSystem.Lib.Logging;
using Spark.Region.Engine.DataAccess;
using Spark.UnifiedPurchaseSystem.Lib.InternalResponse;
using Spark.UnifiedPurchaseSystem.Lib.InternalResponse.DataAccess;
using Spark.Region.Engine.ValueObjects;

namespace Spark.Region.Engine.BusinessLogic
{
    public class RegionBL
    {
        public RegionBL()
        {

        }

        public int GetRegionIDByZipCode(string zipCode, ref RegionResponse regionResponse)
        {
            int regionID = Constants.NULL_INT;

            try
            {
                TraceLog.Enter("RegionBL.GetRegionIDByZipCode");
                TraceLog.WriteLine("Begin RegionBL.GetRegionIDByZipCode({0})", zipCode);

                RegionDAL regionDAL = new RegionDAL();
                regionID = regionDAL.GetRegionIDByZipCode(zipCode);
                if (regionID != Constants.NULL_INT)
                {
                    TraceLog.WriteLine("Region ID [{0}] found from the database", regionID);
                }
                else
                {
                    regionID = -1;
                    InternalResponse internalResponse = InternalResponseDAL.GetInternalResponse(ServiceStatusType.ZipCodeNotFound);
                    regionResponse.InternalResponse = internalResponse;
                    TraceLog.WriteLine("No Region ID for the zip code found from the database");
                }

                return regionID;
            }
            catch (Exception ex)
            {
                TraceLog.WriteErrorLine(ex, "Error in RegionBL.GetRegionIDByZipCode, Error: " + ex.Message);
                throw new BLException("RegionBL.GetRegionIDByZipCode error.", ex);
            }
            finally
            {
                TraceLog.WriteLine("End RegionBL.GetRegionIDByZipCode");
                TraceLog.Leave("RegionBL.GetRegionIDByZipCode");
            }
        }

        public int GetRegionIDByCountry(string country, ref RegionResponse regionResponse)
        {
            int regionID = Constants.NULL_INT;

            try
            {
                TraceLog.Enter("RegionBL.GetRegionIDByCountry");
                TraceLog.WriteLine("Begin RegionBL.GetRegionIDByCountry({0})", country);

                RegionDAL regionDAL = new RegionDAL();
                regionID = regionDAL.GetRegionIDByCountry(country);
                if (regionID != Constants.NULL_INT)
                {
                    TraceLog.WriteLine("Region ID [{0}] found from the database", regionID);
                }
                else
                {
                    regionID = -1;
                    InternalResponse internalResponse = InternalResponseDAL.GetInternalResponse(ServiceStatusType.CountryNotFound);
                    regionResponse.InternalResponse = internalResponse;
                    TraceLog.WriteLine("No Region ID for the country found from the database");
                }

                TraceLog.WriteLine("Region ID [{0}] found from the database", regionID);

                return regionID;
            }
            catch (Exception ex)
            {
                TraceLog.WriteErrorLine(ex, "Error in RegionBL.GetRegionIDByCountry, Error: " + ex.Message);
                throw new BLException("RegionBL.GetRegionIDByCountry error.", ex);
            }
            finally
            {
                TraceLog.WriteLine("End RegionBL.GetRegionIDByCountry");
                TraceLog.Leave("RegionBL.GetRegionIDByCountry");
            }
        }

        public InternalResponse GetInternalResponse(ServiceStatusType serviceStatusType)
        {
            try
            {
                return InternalResponseDAL.GetInternalResponse(serviceStatusType);
            }
            catch (Exception ex)
            {
                throw new BLException("RegionBL.GetInternalResponse error.", ex);
            }
        }
    }
}
