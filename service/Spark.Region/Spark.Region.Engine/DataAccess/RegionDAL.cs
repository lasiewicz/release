﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Matchnet.Data;
using Spark.UnifiedPurchaseSystem.Lib.Exceptions;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using Spark.UnifiedPurchaseSystem.Lib.Logging;

namespace Spark.Region.Engine.DataAccess
{
    internal class RegionDAL
    {
        internal int GetRegionIDByZipCode(string zipCode)
        {
            int regionID = Constants.NULL_INT;
            SqlDataReader dataReader = null;

            try
            {
                TraceLog.Enter("RegionDAL.GetRegionIDByZipCode");
                TraceLog.WriteLine("Begin RegionDAL.GetRegionIDByZipCode({0})", zipCode);

                Command command = new Command("epRegion", "dbo.up_GetRegionID_By_ZipCode", 0);
                command.AddParameter("@ZipCode", SqlDbType.NVarChar, ParameterDirection.Input, zipCode);

                dataReader = Client.Instance.ExecuteReader(command);

                if (dataReader.Read())
                {
                    regionID = dataReader.GetInt32(dataReader.GetOrdinal("RegionID"));
                    TraceLog.WriteLine("Region ID [{0}] found for zip code [{1}]", regionID, zipCode);
                }

                return regionID;
            }
            catch (Exception exception)
            {
                TraceLog.WriteErrorLine(exception, "Error in RegionDAL.GetRegionIDByZipCode(), Error: " + exception.Message);

                /*
                The exception thrown by Hydra has the following
                    The exception.Message contains the descriptive message of the error that was detailed
                    out by Hydra  
                    The exception.InnerException contains the original exception in Hydra which may be
                    a SqlException
                */
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new DALException("RegionDAL.GetRegionIDByZipCode() error.", exception, exception.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                }
                else
                {
                    throw new DALException("RegionDAL.GetRegionIDByZipCode() error.", exception, exception.Message);
                }
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }

                TraceLog.WriteLine("End RegionDAL.GetRegionIDByZipCode()");
                TraceLog.Leave("RegionDAL.GetRegionIDByZipCode");
            }
        }

        internal int GetRegionIDByCountry(string country)
        {
            int regionID = Constants.NULL_INT;
            SqlDataReader dataReader = null;

            try
            {
                TraceLog.Enter("RegionDAL.GetRegionIDByCountry");
                TraceLog.WriteLine("Begin RegionDAL.GetRegionIDByCountry({0})", country);

                Command command = new Command("epRegion", "dbo.up_GetRegionID_By_Country", 0);
                command.AddParameter("@Country", SqlDbType.VarChar, ParameterDirection.Input, country);

                dataReader = Client.Instance.ExecuteReader(command);

                if (dataReader.Read())
                {
                    regionID = dataReader.GetInt32(dataReader.GetOrdinal("RegionID"));
                    TraceLog.WriteLine("Region ID [{0}] found for country [{1}]", regionID, country);
                }

                return regionID;
            }
            catch (Exception exception)
            {
                TraceLog.WriteErrorLine(exception, "Error in RegionDAL.GetRegionIDByCountry(), Error: " + exception.Message);

                /*
                The exception thrown by Hydra has the following
                    The exception.Message contains the descriptive message of the error that was detailed
                    out by Hydra  
                    The exception.InnerException contains the original exception in Hydra which may be
                    a SqlException
                */
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new DALException("RegionDAL.GetRegionIDByCountry() error.", exception, exception.Message, ExceptionUtility.DetermineSQLDBExceptionLevel(sqlException));
                }
                else
                {
                    throw new DALException("RegionDAL.GetRegionIDByCountry() error.", exception, exception.Message);
                }
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }

                TraceLog.WriteLine("End RegionDAL.GetRegionIDByCountry()");
                TraceLog.Leave("RegionDAL.GetRegionIDByCountry");
            }
        }

    }
}
