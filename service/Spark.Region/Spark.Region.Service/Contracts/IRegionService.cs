﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Spark.Region.Service
{
    // NOTE: If you change the interface name "IRegionService" here, you must also update the reference to "IRegionService" in Web.config.
    [ServiceContract(Namespace = "http://spark")]
    public interface IRegionService
    {
        [OperationContract]
        string GetRegionIDByZipCode(string zipCode);

        [OperationContract]
        string GetRegionIDByCountry(string country);
    }
}
