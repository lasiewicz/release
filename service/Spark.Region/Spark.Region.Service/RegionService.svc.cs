﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Spark.UnifiedPurchaseSystem.Lib.CustomContext;
using Spark.Region.Engine.ServiceManagers;

namespace Spark.Region.Service
{
    // NOTE: If you change the class name "RegionService" here, you must also update the reference to "RegionService" in Web.config.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [ServiceRequestContextAttribute]
    public class RegionService : IRegionService
    {
        public string GetRegionIDByZipCode(string zipCode)
        {
            RegionSM regionSM = new RegionSM();
            return regionSM.GetRegionIDByZipCode(zipCode);
        }

        public string GetRegionIDByCountry(string country)
        {
            RegionSM regionSM = new RegionSM();
            return regionSM.GetRegionIDByCountry(country);
        }
    }
}
