using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.ServiceProcess;
using System.Diagnostics;
using System.Threading;

using Matchnet.PhotoMigration.BusinessLogic;

namespace Matchnet.PhotoMigration.Service
{
	public class Service : System.ServiceProcess.ServiceBase
	{
		private System.ComponentModel.Container components = null;
		private Thread _runningThread;

		public Service()
		{
			InitializeComponent();
		}

		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new Service() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
            this.ServiceName = "Matchnet.PhotoMigration.Service";
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		protected override void OnStart(string[] args)
		{
            _runningThread = new Thread(new ThreadStart( PhotoMigrationBL.Instance.Run));
			_runningThread.Start();

		}

		protected override void OnStop()
		{
			if (_runningThread.ThreadState == System.Threading.ThreadState.Running)
			{
				_runningThread.Abort();
			}

			_runningThread = null;
		}
	}
}
