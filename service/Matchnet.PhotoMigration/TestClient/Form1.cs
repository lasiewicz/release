﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Matchnet.PhotoMigration.BusinessLogic;
namespace TestClient
{
    public partial class Form1 : Form
    {
        System.Threading.Thread thread = null;
        System.Threading.Thread[] threads = null;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime begin = DateTime.Now;
                int limiter = Int32.Parse( Utils.GetSetting("limiter"));

                MigratorBL.Instance.ProcessePhotoBatch(limiter, 0, chkBustCache.Checked, chkAsync.Checked);
                DateTime finish = DateTime.Now;
                MessageBox.Show("Completed: " + (finish - begin).TotalSeconds.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            try
            {
                DateTime begin = DateTime.Now;
                MigratorBL.Instance.ProcessPhotos(chkBustCache.Checked, chkAsync.Checked);
                DateTime finish = DateTime.Now;
                MessageBox.Show("Completed: " + (finish - begin).TotalSeconds.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnProcessUser_Click(object sender, EventArgs e)
        {
            try{
                int userid = Int32.Parse(txtProvoUserID.Text);
                DateTime begin = DateTime.Now;
                MigratorBL.Instance.ProcessePhotoBatch(20, userid, chkBustCache.Checked,chkAsync.Checked);
                DateTime finish = DateTime.Now;
                MessageBox.Show("Completed: " + (finish - begin).TotalSeconds.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int threadscount = Convert.ToInt32(txtThreadsNum.Text);
            threads = new System.Threading.Thread[threadscount];
            for (int i = 0; i < threadscount; i++)
            {
                 threads[i] = new System.Threading.Thread(PhotoMigrationBL.Instance.Run);
                 threads[i].Start();
                 //System.Threading.Thread.Sleep(1000);
            }
          
        }

        private void StopThread()
        {
            try
            {

              
                if (threads == null || threads.Length == 0)
                    return;
                int threadscount = Convert.ToInt32(txtThreadsNum.Text);
                for (int i = 0; i < threadscount; i++)
                {
                    System.Diagnostics.Trace.Write("Stopping thread " + i.ToString());
                    threads[i].Join(10000);
                    System.Diagnostics.Trace.Write("Joined thread " + i.ToString());
                    

                }
                MessageBox.Show("Threads are aborted");
            }
            catch (Exception ex) { MessageBox.Show("Error stopping threads. " + ex.ToString()); }
            

        }

        private void button4_Click(object sender, EventArgs e)
        {
            StopThread();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DateTime begin = DateTime.Now;
           // MigratorBL.Instance.ProcessePhotoBatch(20, userid, chkBustCache.Checked, chkAsync.Checked);
            int limiter=Convert.ToInt32( Utils.GetSetting("limiter"));
            DeltaMigratorBL.Instance.ProcessPhotoChangeBatch(limiter);
            DateTime finish = DateTime.Now;
            MessageBox.Show("Completed: " + (finish - begin).TotalSeconds.ToString());
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DateTime begin = DateTime.Now;
            // MigratorBL.Instance.ProcessePhotoBatch(20, userid, chkBustCache.Checked, chkAsync.Checked);
          
            DeltaMigratorBL.Instance.ProcessPhotoChange();
            DateTime finish = DateTime.Now;
            MessageBox.Show("Completed: " + (finish - begin).TotalSeconds.ToString());
        }

        private void button7_Click(object sender, EventArgs e)
        {
            DateTime begin = DateTime.Now;
            // MigratorBL.Instance.ProcessePhotoBatch(20, userid, chkBustCache.Checked, chkAsync.Checked);
            int limiter = Convert.ToInt32(Utils.GetSetting("limiter"));
            DeltaMigratorBL.Instance.ProcessPhotoAdminBatch(limiter,chkBustCache.Checked,chkAsync.Checked);
            DateTime finish = DateTime.Now;
            MessageBox.Show("Completed: " + (finish - begin).TotalSeconds.ToString());
        }

        private void button8_Click(object sender, EventArgs e)
        {
            DateTime begin = DateTime.Now;
            // MigratorBL.Instance.ProcessePhotoBatch(20, userid, chkBustCache.Checked, chkAsync.Checked);
            
            DeltaMigratorBL.Instance.ProcessPhotoAdmin(chkBustCache.Checked, chkAsync.Checked);
            DateTime finish = DateTime.Now;
            MessageBox.Show("Completed: " + (finish - begin).TotalSeconds.ToString());
        }

        private void txtThreadsNum_TextChanged(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {

                string conn = Utils.GetConnectionString("minglemapper");
                StringBuilder str = new StringBuilder();

                str.AppendLine(SQLConnector.Instance.OpenConnection(conn));

              
                MYSQLConnector connector=new MYSQLConnector(conn);
                conn = Utils.GetConnectionString("minglemember");
                str.AppendLine(connector.OpenConnection(conn));
                conn = Utils.GetConnectionString("minglesite");
                str.AppendLine(connector.OpenConnection(conn));
                conn = Utils.GetConnectionString("mingleadmin");
                str.AppendLine(connector.OpenConnection(conn));

               
                rtfConnectionStatus.Text = str.ToString();

            }
            catch (Exception ex) { MessageBox.Show("Error testing connections: " + ex.ToString()); }
        }
    }
}
