﻿namespace TestClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            StopThread();
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.txtProvoUserID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnProcessUser = new System.Windows.Forms.Button();
            this.chkBustCache = new System.Windows.Forms.CheckBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.chkAsync = new System.Windows.Forms.CheckBox();
            this.txtThreadsNum = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.rtfConnectionStatus = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(56, 110);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 39);
            this.button1.TabIndex = 0;
            this.button1.Text = "Process Single Batch";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(149, 111);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 38);
            this.button2.TabIndex = 1;
            this.button2.Text = "Process All";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtProvoUserID
            // 
            this.txtProvoUserID.Location = new System.Drawing.Point(87, 18);
            this.txtProvoUserID.Name = "txtProvoUserID";
            this.txtProvoUserID.Size = new System.Drawing.Size(112, 20);
            this.txtProvoUserID.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Provo UserID";
            // 
            // btnProcessUser
            // 
            this.btnProcessUser.Location = new System.Drawing.Point(205, 12);
            this.btnProcessUser.Name = "btnProcessUser";
            this.btnProcessUser.Size = new System.Drawing.Size(75, 38);
            this.btnProcessUser.TabIndex = 4;
            this.btnProcessUser.Text = "Process User";
            this.btnProcessUser.UseVisualStyleBackColor = true;
            this.btnProcessUser.Click += new System.EventHandler(this.btnProcessUser_Click);
            // 
            // chkBustCache
            // 
            this.chkBustCache.AutoSize = true;
            this.chkBustCache.Location = new System.Drawing.Point(12, 55);
            this.chkBustCache.Name = "chkBustCache";
            this.chkBustCache.Size = new System.Drawing.Size(225, 17);
            this.chkBustCache.TabIndex = 5;
            this.chkBustCache.Text = "Bust Member from Cache After Processing";
            this.chkBustCache.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(40, 324);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(78, 45);
            this.button3.TabIndex = 6;
            this.button3.Text = "Start Queue Processor";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(176, 324);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(78, 45);
            this.button4.TabIndex = 7;
            this.button4.Text = "Stop Queue Processor";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // chkAsync
            // 
            this.chkAsync.AutoSize = true;
            this.chkAsync.Location = new System.Drawing.Point(12, 87);
            this.chkAsync.Name = "chkAsync";
            this.chkAsync.Size = new System.Drawing.Size(205, 17);
            this.chkAsync.TabIndex = 8;
            this.chkAsync.Text = "Process Save Photos Asynchronously";
            this.chkAsync.UseVisualStyleBackColor = true;
            // 
            // txtThreadsNum
            // 
            this.txtThreadsNum.Location = new System.Drawing.Point(40, 298);
            this.txtThreadsNum.Name = "txtThreadsNum";
            this.txtThreadsNum.Size = new System.Drawing.Size(162, 20);
            this.txtThreadsNum.TabIndex = 9;
            this.txtThreadsNum.TextChanged += new System.EventHandler(this.txtThreadsNum_TextChanged);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(40, 155);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(101, 57);
            this.button5.TabIndex = 10;
            this.button5.Text = "Process Changes Batch";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(149, 155);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(101, 57);
            this.button6.TabIndex = 11;
            this.button6.Text = "Process All Changes";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(40, 231);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(101, 57);
            this.button7.TabIndex = 12;
            this.button7.Text = "Process Admin Changes Batch";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(149, 231);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(101, 57);
            this.button8.TabIndex = 13;
            this.button8.Text = "Process All  Admin Changes";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(12, 387);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(106, 41);
            this.button9.TabIndex = 15;
            this.button9.Text = "Test Connection";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // rtfConnectionStatus
            // 
            this.rtfConnectionStatus.Location = new System.Drawing.Point(16, 434);
            this.rtfConnectionStatus.Name = "rtfConnectionStatus";
            this.rtfConnectionStatus.Size = new System.Drawing.Size(412, 204);
            this.rtfConnectionStatus.TabIndex = 16;
            this.rtfConnectionStatus.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 651);
            this.Controls.Add(this.rtfConnectionStatus);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.txtThreadsNum);
            this.Controls.Add(this.chkAsync);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.chkBustCache);
            this.Controls.Add(this.btnProcessUser);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtProvoUserID);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtProvoUserID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnProcessUser;
        private System.Windows.Forms.CheckBox chkBustCache;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.CheckBox chkAsync;
        private System.Windows.Forms.TextBox txtThreadsNum;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.RichTextBox rtfConnectionStatus;
    }
}

