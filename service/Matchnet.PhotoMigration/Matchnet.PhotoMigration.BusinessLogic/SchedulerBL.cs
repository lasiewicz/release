﻿using System;
using System.Messaging;
using System.Threading;
using System.Drawing;
using System.IO;
using System.Collections;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;

using Matchnet;
using Matchnet.RemotingServices.ServiceManagers;
using Matchnet.Caching;
using Matchnet.CacheSynchronization.Context;
using Matchnet.ApproveQueue.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.MSMQueue;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.File.ServiceAdapters;
using MySql.Data.MySqlClient;
using Matchnet.PhotoMigration.ValueObjects;

using System.Collections.Generic;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Matchnet.PhotoMigration.BusinessLogic
{
   public class SchedulerBL
    {
       public readonly static SchedulerBL Instance = new SchedulerBL();

       private SchedulerBL()
       {

       }
       private SchedulerBL(int siteid, string sitename)
       {

       }



       public List<MingleMapper> GetMappedProfiles(int limiter)
       { SqlDataReader rs=null;
           try
           {
               List<MingleMapper> list = new List<MingleMapper>();
               string sql = "select top {0} * from {1} where PhotoProcessed is null order by createdate";
               string mappertable = Utils.GetSetting("minglemappertable");
               sql = String.Format(sql, limiter,mappertable);
               string conn = Utils.GetConnectionString("minglemapper");
                rs = SQLConnector.Instance.ExecuteReader(sql, conn);
               while (rs.Read())
               {
                   MingleMapper m = new MingleMapper();
                   m.MingleMapperID = (int)(rs["minglemapperid"] != Convert.DBNull ? rs["minglemapperid"] : 0);
                   m.ProvoID = (int)(rs["provoid"] != Convert.DBNull ? rs["provoid"] : 0);
                   m.SparkID = (int)(rs["sparkid"] != Convert.DBNull ? rs["sparkid"] : 0);
                   m.CollisionOutcome = (string)(rs["collisionoutcome"] != Convert.DBNull ? rs["collisionoutcome"] : "");
                   m.ColissionNotes = (string)(rs["colissionnotes"] != Convert.DBNull ? rs["colissionnotes"] : "");
                   m.Attributes = (DateTime)(rs["attributes"] != Convert.DBNull ? rs["attributes"] : new DateTime());
                   m.Password = (string)(rs["password"] != Convert.DBNull ? rs["password"] : "");
                   m.SubStatus = (DateTime)(rs["substatus"] != Convert.DBNull ? rs["substatus"] : new DateTime());
                   m.PhotoProcessed = (DateTime)(rs["photoprocessed"] != Convert.DBNull ? rs["photoprocessed"] : new DateTime());
                   m.CreateDate = (DateTime)(rs["createdate"] != Convert.DBNull ? rs["createdate"] : new DateTime());
                   list.Add(m);
               }

               return list;
           }
           catch (Exception ex)
           { throw (ex); }
           finally
           {
               if (rs != null)
                   rs.Dispose();
           }



       }






        
    }
}
