﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Matchnet.PhotoMigration.BusinessLogic
{
    public class SQLConnector
    {
        public static SQLConnector Instance = new SQLConnector();


        public DataSet ExecuteDataSet(string connStr, string sql)
        {
            SqlConnection conn = null;
            DataSet ds = new DataSet();
            try
            {
                conn = new SqlConnection(connStr);
                conn.Open();
                SqlDataAdapter ad = new SqlDataAdapter(sql, conn);
                ad.Fill(ds);
                return ds;

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally 
            {
                if (conn != null)
                    conn.Dispose();
            }


        }

        public SqlDataReader ExecuteReader(string connStr, string sql)
        {
            SqlConnection conn = null;
            SqlDataReader reader = null;
            
            try
            {
                conn = new SqlConnection(connStr);
                conn.Open();
                SqlCommand comm = new SqlCommand(sql);
                comm.CommandType = CommandType.Text;
                comm.Connection = conn;
                reader = comm.ExecuteReader(CommandBehavior.CloseConnection);

                return reader;

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
              
            }


        }


        public SqlDataReader ExecuteNoQuery(string connStr, SqlCommand comm)
        {
            SqlConnection conn = null;
            SqlDataReader reader = null;

            try
            {
                conn = new SqlConnection(connStr);
                conn.Open();
                comm.Connection = conn;
                comm.ExecuteNonQuery();

                return reader;

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                if (conn != null)
                    conn.Dispose();
            }


        }


        public string OpenConnection(string connStr)
        {
            SqlConnection conn = null;
          
            try
            {
                conn = new SqlConnection(connStr);
                conn.Open();
                return "Successfully connected to:" + connStr;
              
            }
            catch (Exception ex)
            {
                return "Error connecting to:" + connStr + ". " + ex.ToString();
            }
            finally
            {
                if (conn != null)
                    conn.Dispose();
            }


        }

    }
}
