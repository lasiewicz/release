﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.IO;

namespace Matchnet.PhotoMigration.BusinessLogic
{
  
    
    public class MYSQLConnector
    {
        public MYSQLConnector()
        {
        }

        public MYSQLConnector(string connectionstr)
        {
            ConnectionString = connectionstr;
        }

        private MySqlConnection _connection=null;
       
        public string ConnectionString
        {
            get;
            set;
        }

        
        public Image GetBlobbImage(MySqlDataReader reader, int index)
        {
            Image img = null;
          
            int bufferersize = 100;
            byte[] buffer = new byte[bufferersize];
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            try
            {
                int startIndex = 0;
                long retval = reader.GetBytes(index, startIndex, buffer, 0, bufferersize);
                while (retval == bufferersize)
                {
                    bw.Write(buffer);
                    bw.Flush();
                    startIndex += bufferersize;
                    retval = reader.GetBytes(index, startIndex, buffer, 0, bufferersize);
                }
                bw.Write(buffer, 0, (int)retval);
                bw.Flush();
                 img = Image.FromStream(ms);

                return img;

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                if (bw != null)
                    bw.Close();

                if (ms != null)
                    ms.Close();
            }


        }

        public byte[] GetBlobb(MySqlDataReader reader, int index)
        {
            int buffersize = 10;
            
            byte[] buffer = null;
            
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            int startIndex = 0;
             long retval=0;

            try
            {
               
                long size=reader.GetBytes(index, startIndex, null, 0, 0);
                buffer = new byte[buffersize];
                retval= reader.GetBytes(index, startIndex, buffer, 0, buffersize);
                long bytesleft = size - retval;
                while (bytesleft > 0)
                {
                    retval = 0;
                    bw.Write(buffer);
                    bw.Flush();
                    startIndex += buffersize;
                    if (bytesleft < long.Parse(buffersize.ToString()))
                        buffersize =Conversion.CInt( bytesleft);

                    retval = reader.GetBytes(index, startIndex, buffer, 0, buffersize);
                    bytesleft -= retval;


                }
                bw.Write(buffer, 0, (int)retval);
                bw.Flush();

                byte[] blob = ms.ToArray();
                return blob;
          }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                if (bw != null)
                    bw.Close();

                if (ms != null)
                    ms.Close();
            }


        }


        public MySqlDataReader ExecuteReader(string sql)
        {
            string nolock = "set transaction isolation level READ UNCOMMITTED;";
            MySqlCommand command = null;
            MySqlDataReader reader = null;
            try
            {
                _connection = new MySqlConnection(ConnectionString);
                _connection.Open();
                command = _connection.CreateCommand();
                command.CommandText = nolock + " " + sql;
                
                reader = command.ExecuteReader();

                return reader;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
          

        }

        public string OpenConnection(string connStr)
        {
            MySqlConnection conn = null;
            string safetoshowconn = "";
            try
            {
                int stop = connStr.ToLower().IndexOf("uid");
                safetoshowconn = connStr.Substring(0, stop - 1);
                conn = new MySqlConnection(connStr);
                conn.Open();
                return "Successfully connected to:" + safetoshowconn;

            }
            catch (Exception ex)
            {
                return "Error connecting to:" + safetoshowconn + ". " + ex.ToString();
            }
            finally
            {
                if (conn != null)
                    conn.Dispose();
            }


        }

        public void CloseReader(MySqlDataReader reader)
        {
            if (reader != null)
                reader.Dispose();

        }

        public void CloseAll(MySqlDataReader reader)
        {
            if (reader != null)
                reader.Dispose();
            if (_connection != null)
                _connection.Dispose();

        }

    }



}
