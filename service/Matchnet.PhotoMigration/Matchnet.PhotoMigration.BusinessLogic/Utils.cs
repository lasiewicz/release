﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Messaging;
using Matchnet.PhotoMigration.ValueObjects;
namespace Matchnet.PhotoMigration.BusinessLogic
{
   public class Utils
    {
       public static string GetConnectionString(string name)
       {
           string conn = ConfigurationManager.ConnectionStrings[name].ToString();
           return conn;
           
       }

       public static string GetSetting(string name)
       {
           try
           {
               string conn = ConfigurationManager.AppSettings[name].ToString();
               return conn;
           }
           catch (Exception ex)
           {
               Utils.Log("Error reading setting:" + name + ", " + ex.ToString()); return "";
           }
       }

       public static void SaveSetting(string name, string value)
       {
           try
           {
               System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

               config.AppSettings.Settings[name].Value = value;  
              
               config.Save(ConfigurationSaveMode.Modified);
               ConfigurationManager.RefreshSection("appSettings"); 
           }
           catch (Exception ex)
           {  }
           

       }
       public static string GetPartitionName(string format, int id, int partitioncaount)
       {

           return String.Format(format, id % partitioncaount);
       }

       public static void Log(string message)
       {
           StreamWriter Tex = null;
           try
           {
               Tex = System.IO.File.AppendText(Utils.GetSetting("logfile"));
               Tex.Write(String.Format("{0} : {1}", DateTime.Now, message));
               Trace(String.Format("{0} : {1}", DateTime.Now, message));

           }
           catch (Exception ex)
           { System.Diagnostics.Trace.Write("Error logging:" + ex.ToString()); }
           finally
           {
               if (Tex != null)
                   Tex.Dispose();

           }
       }


       public static void Log(PhotoMigrationLog log)
       {
           try
           {
               SqlCommand comm = new SqlCommand();
               comm.CommandType = CommandType.StoredProcedure;

               comm.CommandText = "up_InsertPhotoMigrationLog";
               comm.Parameters.Add(new SqlParameter("@PhotoMigrationBatchStamp", SqlDbType.DateTime));
               comm.Parameters[0].Value = log.PhotoMigrationBatchStamp;

               comm.Parameters.Add(new SqlParameter("@ProvoUserID", SqlDbType.Int));
               comm.Parameters[1].Value = log.ProvoUserId;


               comm.Parameters.Add(new SqlParameter("@SparkMemberID", SqlDbType.Int));
               comm.Parameters[2].Value = log.SparkMemberId;

               comm.Parameters.Add(new SqlParameter("@PhotoNum", SqlDbType.Int));
               comm.Parameters[3].Value = log.PhotoNum;

               comm.Parameters.Add(new SqlParameter("@AdminPhotoNum", SqlDbType.Int));
               comm.Parameters[4].Value = log.AdminPhotoNum;

               comm.Parameters.Add(new SqlParameter("@FileID", SqlDbType.Int));
               comm.Parameters[5].Value = log.FileID;

               comm.Parameters.Add(new SqlParameter("@LogData", SqlDbType.VarChar));
               comm.Parameters[6].Value = log.LogData;

               comm.Parameters.Add(new SqlParameter("@ProcessMessage", SqlDbType.VarChar));
               comm.Parameters[7].Value = log.ProcessMessage;

               comm.Parameters.Add(new SqlParameter("@ProcessStatus", SqlDbType.Int));
               comm.Parameters[8].Value = log.ProcessStatus;

               comm.Parameters.Add(new SqlParameter("@ProcessDateTime", SqlDbType.DateTime));
               comm.Parameters[9].Value = DateTime.Now;
               SQLConnector.Instance.ExecuteNoQuery(Utils.GetConnectionString("minglemapper"), comm);

           }
           catch (Exception ex)
           { Utils.Log("Error writing to photomigrationlog:" + ex.ToString()); }


       }

       public static void SetUpdatedStamp(int provoid, int sparkid, DateTime stamp)
       {

           //string sql = "select distinct provoid,sparkid from MingleMapper where PhotoProcessed is null order by createdate";
           string mappertable = Utils.GetSetting("minglemappertable");
           string sql = "update {3} set PhotoProcessed='{0}' where provoid={1} and sparkid={2} ";
           
           try
           {
               SqlCommand comm = new SqlCommand(String.Format(sql, stamp, provoid, sparkid,mappertable));
               comm.CommandType = CommandType.Text;
               SQLConnector.Instance.ExecuteNoQuery(Utils.GetConnectionString("minglemapper"), comm);


           }
           catch (Exception ex)
           { throw (ex); }
           finally
           {

           }


       }


       public static void Enqueue(MinglePhotoCollection coll)
       {

           try
           {
               string queuepath = PhotoUtil.MSMQPath;
               //if (!MessageQueue.Exists(queuepath))
               //{
               //    MessageQueue.Create(queuepath, true);
               //}

               MessageQueue queue = new MessageQueue(queuepath);
               queue.Formatter = new BinaryMessageFormatter();


               MessageQueueTransaction trans = new MessageQueueTransaction();

               try
               {
                   trans.Begin();
                   queue.Send(coll.Photos, trans);

                   trans.Commit();
               }
               finally
               {
                   trans.Dispose();
               }
           }
           catch (Exception ex)
           { Utils.Log("Error writing to the queue:" + ex.ToString()); }
       }
       public static void Enqueue(MinglePhoto photo)
       {

           try
           {
               string queuepath = PhotoUtil.MSMQPath;
              

               MessageQueue queue = new MessageQueue(queuepath);
               queue.Formatter = new BinaryMessageFormatter();


               MessageQueueTransaction trans = new MessageQueueTransaction();

               try
               {
                   trans.Begin();
                   queue.Send(photo, trans);

                   trans.Commit();
               }
               finally
               {
                   trans.Dispose();
               }
           }
           catch (Exception ex)
           {
               string trace = "";
               if (photo != null)
                   trace = photo.ToString();

               Utils.Log("Error writing to the queue:" + ex.ToString() + ", " + trace ); }
       }
       public static void EnqueueError(MinglePhotoCollection coll)
       {

           try
           {
               string queuepath = @".\private$\photomigration_error";
               if (!MessageQueue.Exists(queuepath))
               {
                   MessageQueue.Create(queuepath, true);
               }

               MessageQueue queue = new MessageQueue(queuepath);
               queue.Formatter = new BinaryMessageFormatter();


               MessageQueueTransaction trans = new MessageQueueTransaction();

               try
               {
                   trans.Begin();
                   queue.Send(coll.Photos, trans);

                   trans.Commit();
               }
               finally
               {
                   trans.Dispose();
               }
           }
           catch (Exception ex)
           {
               string trace = "";
               if (coll != null)
                   trace = coll.ToString();

               Utils.Log("Error writing to the queue:" + ex.ToString() + ", " + trace);
           }
       }
       public static void EnqueueError(MinglePhoto photo)
       {

           try
           {
               string queuepath = @".\private$\photomigration_error"; 
               if (!MessageQueue.Exists(queuepath))
               {
                   MessageQueue.Create(queuepath, true);
               }

               MessageQueue queue = new MessageQueue(queuepath);
               queue.Formatter = new BinaryMessageFormatter();


               MessageQueueTransaction trans = new MessageQueueTransaction();

               try
               {
                   trans.Begin();
                   queue.Send(photo, trans);

                   trans.Commit();
               }
               finally
               {
                   trans.Dispose();
               }
           }
           catch (Exception ex)
           {
               string trace = "";
               if (photo != null)
                   trace = photo.ToString();

               Utils.Log("Error writing to the queue:" + ex.ToString() + ", " + trace);
           }
       }


       public static void Enqueue(Message message)
       {

           try
           {
               string queuepath = PhotoUtil.MSMQPath;
               //if (!MessageQueue.Exists(queuepath))
               //{
               //    MessageQueue.Create(queuepath, true);
               //}

               MessageQueue queue = new MessageQueue(queuepath);
               queue.Formatter = new BinaryMessageFormatter();



               try
               {
                  
                   if(message.Body.GetType()==typeof(MinglePhoto))
                      Enqueue((MinglePhoto)message.Body);
                   else if (message.Body.GetType() == typeof(MinglePhotoCollection))
                       Enqueue((MinglePhotoCollection)message.Body);

                  
               }
               finally
               {
                   
               }
           }
           catch (Exception ex)
           { Utils.Log("Error writing to the queue:" + ex.ToString()); }
       }


       public static void EnqueueErrorQueue(Message message)
       {

           try
           {
               string queuepath = @".\private$\photomigration_error"; 
               if (!MessageQueue.Exists(queuepath))
               {
                   MessageQueue.Create(queuepath, true);
               }

               MessageQueue queue = new MessageQueue(queuepath);
               queue.Formatter = new BinaryMessageFormatter();



               try
               {

                   if (message.Body.GetType() == typeof(MinglePhoto))
                       EnqueueError((MinglePhoto)message.Body);
                   else if (message.Body.GetType() == typeof(MinglePhotoCollection))
                       EnqueueError((MinglePhotoCollection)message.Body);


               }
               finally
               {

               }
           }
           catch (Exception ex)
           { Utils.Log("Error writing to the queue:" + ex.ToString()); }
       }



       public static void Trace(string message)
       {
           try
           {
               System.Diagnostics.Trace.WriteLine("PhotoMigration:" + message);

           }
           catch (Exception ex) { }

       }
    }
}
