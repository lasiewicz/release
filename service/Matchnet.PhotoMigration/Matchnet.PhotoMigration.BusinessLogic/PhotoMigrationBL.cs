using System;
using System.Messaging;
using System.Threading;
using System.Drawing;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;

using Matchnet;
using Matchnet.RemotingServices.ServiceManagers;
using Matchnet.Caching;
using Matchnet.CacheSynchronization.Context;
using Matchnet.ApproveQueue.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.MSMQueue;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.File.ServiceAdapters;
using Matchnet.PhotoMigration.ValueObjects;

namespace Matchnet.PhotoMigration.BusinessLogic
{
	/// <summary>
	/// Processes photo queue items sent from admin photo approval front end application
	/// </summary>
    /// 
  
	public class PhotoMigrationBL
	{
		#region Public Variables
        public readonly static PhotoMigrationBL Instance = new PhotoMigrationBL();
		#endregion

		#region Private Variables
		private static int _sleepTime; // sleep between batches in seconds
		private static bool _isRunnable;
        private int _communityid;
		#endregion

		#region Constructors
        private PhotoMigrationBL()
		{
			//setup cache synchronization stuff
            //const Int32 CACHE_SYNC_PORT = 48880;
            //Evaluator.Instance.SetRemotingPort(CACHE_SYNC_PORT);
            //ChannelServices.RegisterChannel(new TcpServerChannel("", CACHE_SYNC_PORT));
            //CacheManagerSM cacheManager = new CacheManagerSM("Matchnet.PhotoMigration.Service");
            //System.Runtime.Remoting.RemotingServices.Marshal(cacheManager, cacheManager.GetType().Name + ".rem");

			_isRunnable = true;
			_sleepTime = 1;
            _communityid = Conversion.CInt( Utils.GetSetting("communityid"));
		}

		#endregion

		#region Public Methods
		public void Stop()
		{
			_isRunnable = false;
		}

		public void Run()
		{
			while (_isRunnable)
			{
				try
				{
                    if (processMinglePhotoCollection())
					{	
						_sleepTime = 1;
					}
					else
					{
						// 5 second maximum wait. CS is going to check for results frequently 
						// and I wouldn't want them waiting for minutes.
						if (_sleepTime > 5)
						{
							_sleepTime = 1;
						}
						else
						{
							_sleepTime++;
						}

						Thread.Sleep(_sleepTime * 1000);
					}
				}
				catch(Exception ex)
				{
					Thread.Sleep(1000);
					Metrics.Instance.WriteError(ex.ToString());
				}
			}
		}

		#endregion
		
		#region Private Methods

        private bool processMinglePhotoCollection()
        {
            MinglePhotoCollection photoCollection;
            long start = DateTime.Now.Ticks;
            bool res;
            MessageQueueTransaction tran = null;
            Message message = null;
            try
            {
                tran = new MessageQueueTransaction();
                tran.Begin();
                message = receive(tran);
                if (message == null)
                    return false;
                if(message.Body.GetType()==typeof(List<MinglePhoto>))
                {
                    photoCollection = getNextMinglePhotoCollection(message);
                    ProcessMemberPhotos(photoCollection);
                }
                else if (message.Body.GetType() == typeof(MinglePhoto))
                {
                    MinglePhoto photo = message.Body as MinglePhoto;
                    SendToApproveQueue(photo);
                }

                tran.Commit();
                tran.Dispose();
                tran = null;
            }
            catch (Exception ex)
            {
                attemptResend(tran,message, ex);
                throw new BLException("Error receiving batch", ex);
            }
            finally
            {
              // Metrics.BatchesTotal.Increment();
            }

            
            return true;
        }


        private Message receive(MessageQueueTransaction tran)
        {
            TimeSpan timeout = new TimeSpan(0, 0, 0, 0, 10);

            MessageQueue queue = new MessageQueue(PhotoUtil.QUEUE_LOCAL_PATH);
            queue.Formatter = new BinaryMessageFormatter();
            Message message;

            try
            {
                message = queue.Receive(timeout,tran);
            }
            catch (MessageQueueException mqEx)
            {
                if (!mqEx.Message.Equals("Timeout for the requested operation has expired."))
                {
                    throw mqEx;
                }

                return null;
            }

            if (message == null)
            {
                return null;
            }
            return message;

        }
		private MinglePhotoCollection getNextMinglePhotoCollection(Message message)
		{
			
            
                List<MinglePhoto> photos = message.Body as List<MinglePhoto>;
                MinglePhotoCollection photoCollection = new MinglePhotoCollection();
                photoCollection.Photos = photos;

                photoCollection.UserID = photos[0].UserID;
                photoCollection.SparkID = photos[0].SparkUserID;


                return photoCollection;
            
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="fromFilePath"></param>
		/// <returns>Returns copied to file path</returns>
		private string makeBackupCopy(string fromFilePath, bool forceOverWrite)
		{
			try
			{
				string backupServer = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PHOTOBACKUPSERVER");

				//dev use
				if (backupServer.Length == 0)
				{
					return fromFilePath;
				}

				if (!System.IO.File.Exists(fromFilePath))
				{
					fromFilePath = fromFilePath.Replace(@".jpg", @".bmp");

					if (!System.IO.File.Exists(fromFilePath))
					{
						throw new BLException("Error. File does not exist. [FilePath:" + fromFilePath + "]");
					}
				}

				string backUpServer= @"\\" + backupServer;

				int indexServerPathEnd = fromFilePath.IndexOf(@"\", 3); // \\FILE05\FileRootC\2005\05\24\12\blah.jpg -> get the third one

				string localFilePath = fromFilePath.Substring(indexServerPathEnd); // \FileRootC\2005\05\24\12\blah.jpg -> got this path

				string toFilePath = backUpServer + localFilePath; // \\64.16.66.151\FileRootC\2005\05\24\12\blah.jpg -> build this path

				if (forceOverWrite == false)
				{
					if (System.IO.File.Exists(toFilePath))
					{
						return toFilePath;
					}
				}

				try
				{
					FileInfo fileInfo = new FileInfo(toFilePath);
					Directory.CreateDirectory(fileInfo.Directory.FullName);
					System.IO.File.Copy(fromFilePath, toFilePath);
					System.Diagnostics.Trace.WriteLine("FA:Copied back up From:" + fromFilePath + " To:" + toFilePath);
				}
				catch(Exception ex)
				{
					throw new BLException("Error copying file. [From:" + fromFilePath +
						", To:" + toFilePath + "index:" + indexServerPathEnd + "]", ex);
				}

				return toFilePath;
			}
			catch(Exception ex)
			{
				throw new BLException("Error making backup copy. [FromFilePath:" + fromFilePath + "]", ex);
			}
		}

		private void validateApproveInfo(ApproveInfo approveInfo)
		{
			if (!(isValidIntValue(approveInfo.MemberID) &&
				isValidIntValue(approveInfo.MemberPhotoID) &&
				isValidIntValue(approveInfo.FileID) &&
				isValidIntValue(approveInfo.CommunityID) &&
				isValidIntValue(approveInfo.AdminMemberID)))
			{
				throw new BLException("Invalid approve info member/file data. [MemberID:" + approveInfo.MemberID +
					", MemberPhotoID:" + approveInfo.MemberPhotoID + ", FileID:" + approveInfo.FileID);
			}

			if (approveInfo.CropHeight == 0 ||
				approveInfo.CropWidth == 0 ||
				approveInfo.CropHeightThumb == 0 ||
				approveInfo.CropWidthThumb == 0)
			{
				throw new BLException("Crop height/width value cannot be zero. [MemberID:" + approveInfo.MemberID +
					", AdminMemberID:" + approveInfo.AdminMemberID.ToString() +
					", MemberPhotoID:" + approveInfo.MemberPhotoID.ToString() +
					", FileID:" + approveInfo.FileID.ToString() +
					", CropHeight:" + approveInfo.CropHeight.ToString() + 
					", CropWidth:" + approveInfo.CropWidth.ToString() +
					", CropHeightThumb:" + approveInfo.CropHeightThumb.ToString() + 
					", CropWidthThumb:" + approveInfo.CropWidthThumb.ToString());
			}
		}

		private bool isValidIntValue(int testValue)
		{
			if (testValue == Constants.NULL_INT || testValue == 0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		/// <summary>
		/// Main process method that manipulates each photo and saves to db
		/// </summary>
		/// <param name="approveInfo"></param>
        /// 

        public int ProcessMemberPhotos(MinglePhotoCollection photos)
        {
            int count = 0;
            ApproveInfo approveInfo = null;
            int sparkid = photos.Photos[0].SparkUserID;
            int provoid = photos.Photos[0].UserID;
            if (sparkid <= 0)
               sparkid = photos.Photos[0].Log.SparkMemberId;

            DateTime stamp = photos.Photos[0].Log.PhotoMigrationBatchStamp;
            photos.Photos.Sort(delegate(MinglePhoto p1, MinglePhoto p2) { return p1.ListOrder.CompareTo(p2.ListOrder); });
            
            try
            {
                List<PhotoUpdate> updates = new List<PhotoUpdate>();
                List<MinglePhoto> processedPhotos = new List<MinglePhoto>();
                foreach (MinglePhoto photo in photos.Photos)
                {
                    if (photo.SparkUserID <= 0)
                        photo.SparkUserID = sparkid;

                    if (photo.ThumbRectangle.Height == 0 || photo.ThumbRectangle.Width == 0)
                    {
                        SendToApproveQueue(photo);
                        photo.Log.ProcessStatus = (int)ProcessStatus.sendtoapprovequeue;
                        photo.Log.ProcessMessage = "Sent to approve queueu";
                        Utils.Log(photo.Log);
                        count += 1;
                        continue;
                    }
                    try
                    {
                        approveInfo = processNewFile(photo);

                        PhotoUpdate update = approveMemberPhoto(approveInfo, photo);
                        updates.Add(update);
                       // Matchnet.PhotoMigration.ServiceAdapters.MemberServiceSA.Instance.SavePhotos(approveInfo.CommunityID, approveInfo.MemberID, new  PhotoUpdate[]{ update});
                        photo.Log.LogData = photo.ToString();
                        photo.Log.FileID = update.FileID;
                        photo.Log.ProcessStatus = (int)ProcessStatus.success;
                        photo.Log.ProcessMessage = "Success";
                        photo.Log.AdminPhotoNum=photo.AdminNum;
                        processedPhotos.Add(photo);
                        
                        //Utils.Log(photo.Log);
                        count += 1;

                        
                    }
                    catch (Exception ex)
                    {
                        if(photo.Log.ProcessStatus != (int)ProcessStatus.error)
                        {
                        photo.Log.ProcessMessage = "Exception in ProcessMemberPhotos:" + ex.Message;
                        photo.Log.LogData = photo.ToString();
                        photo.Log.SparkMemberId = photo.SparkUserID;
                        photo.Log.ProvoUserId = photo.UserID;
                        photo.Log.PhotoNum = photo.PhotoNum;
                      
                        
                        }
                       //sent to approve queue anyway
                        SendToApproveQueue(photo);
                        photo.Log.ProcessStatus = (int)ProcessStatus.sendtoapprovequeue;
                        count += 1;
                        Utils.Log(photo.Log);
                        continue;
                    }
                }

                if( updates != null && updates.Count > 0)
                {
                    Matchnet.PhotoMigration.ServiceAdapters.MemberServiceSA.Instance.SavePhotos(approveInfo.CommunityID, approveInfo.MemberID, updates.ToArray());
                    foreach (MinglePhoto p in processedPhotos)
                    {
                        Utils.Log(p.Log);
                    }
                }
                if (count > 0)
                {
                    Utils.SetUpdatedStamp(provoid, sparkid, stamp);

                }
                return count;

                
            }
            catch (Exception ex)
            {
                // approveinfo has been validated so it should be safe to use here
                string extraInfo = "CommunityID:" + approveInfo.CommunityID.ToString() +
                    ", MemberID:" + approveInfo.MemberID.ToString() +
                    ", MemberPhotoID:" + approveInfo.MemberPhotoID.ToString() +
                    ", FileID:" + approveInfo.FileID.ToString() +
                    ", AdminMemberID:" + approveInfo.AdminMemberID.ToString();

                System.Diagnostics.Trace.WriteLine("FA: Error: " + ex.ToString() + ", " + extraInfo);
                throw (ex);
                //throw new BLException("Error processing photo approval." + extraInfo, ex);
            }
            finally
            {


                if (approveInfo != null)
                {
                    approveInfo = null;
                }
            }
        }
		public  void ProcessApproveInfo(MinglePhoto photo)
		{
            ApproveInfo approveInfo = null;
			try{
				
				approveInfo=processNewFile(photo);

                approveMemberPhoto(approveInfo, photo);
			}
			catch(Exception ex)
			{
				// approveinfo has been validated so it should be safe to use here
				string extraInfo = "CommunityID:" + approveInfo.CommunityID.ToString() + 
					", MemberID:" + approveInfo.MemberID.ToString() + 
					", MemberPhotoID:" + approveInfo.MemberPhotoID.ToString() + 
					", FileID:" + approveInfo.FileID.ToString() + 
					", AdminMemberID:" + approveInfo.AdminMemberID.ToString();

				System.Diagnostics.Trace.WriteLine("FA: Error: " + ex.ToString() + ", " +  extraInfo);

				//throw new BLException("Error processing photo approval." + extraInfo, ex);
			}
			finally
			{
				

				if (approveInfo != null)
				{
					approveInfo = null;
				}
			}
		}


		private void saveNewImageFile(Image image, out Int32 fileID, out string fileWebPath)
		{
			MemoryStream stream = new MemoryStream();
			image.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
			byte[] imageData = stream.ToArray();
			ServiceAdapters.FileServiceSA.Instance.SaveNewFile(imageData, "jpg", out fileID, out fileWebPath);
		}


		private void processNewFile(ApproveInfo approveInfo)
		{

			string fullSizeFilePath = String.Empty;
			string fullSizeFileWebPath = String.Empty;

			string thumbFilePath = String.Empty;
			string thumbFileWebPath = String.Empty;

			string backupPath = String.Empty;

			int fullFileID = Constants.NULL_INT;
			int thumbFileID = Constants.NULL_INT;

			Image fullSize = null;
			Image thumbNail = null;

			try
			{
				#region Get Full Size Info from DB
				FileSA.Instance.GetFilePath(approveInfo.FileID, out fullSizeFilePath, out fullSizeFileWebPath);
		
				if ((fullSizeFilePath == null) || (fullSizeFilePath == String.Empty))
				{
					throw new Exception("No file information found for FileID:" + approveInfo.FileID);
				}

				System.Diagnostics.Trace.WriteLine("FA: Found filepath:" + fullSizeFilePath + ", webpath:" + fullSizeFileWebPath +" for FileID:" + approveInfo.FileID);
				
				//fullSizeFilePath = fullSizeFilePath.Replace("lafile02", "lafile01");

				fullSize = getImageFromPath(fullSizeFilePath, approveInfo);
				#endregion

				#region Photo Manipulation
				// Rotate
				fullSize = PhotoUtil.Rotate(fullSize, approveInfo.RotateAngle);

				// Crop Thumbnail
				thumbNail = PhotoUtil.Crop((System.Drawing.Image)fullSize.Clone(), approveInfo.CropXThumb, approveInfo.CropYThumb,
					approveInfo.CropWidthThumb, approveInfo.CropHeightThumb,true);

				// Crop Full Size
				fullSize = PhotoUtil.Crop((System.Drawing.Image)fullSize.Clone(), approveInfo.CropX, approveInfo.CropY,
					approveInfo.CropWidth, approveInfo.CropHeight, false);

				// Resize Full Photo
				fullSize = PhotoUtil.Resize(fullSize, PhotoUtil.MaxPhotoWidth, PhotoUtil.MaxPhotoHeight, false);

				// Resize Thumbnail
				thumbNail = PhotoUtil.Resize(thumbNail, PhotoUtil.ThumbnailWidth, PhotoUtil.ThumbnailHeight, true);

				// Compress
				fullSize = PhotoUtil.CompressJpeg(fullSize);
				thumbNail = PhotoUtil.CompressJpeg(thumbNail);
				#endregion

				// Delete current file since new one will be generated
				if (approveInfo.FileID > 0)
				{
					FileSA.Instance.DeleteFile(approveInfo.FileID);
				}
				if (approveInfo.FileIDThumb > 0)
				{
					FileSA.Instance.DeleteFile(approveInfo.FileIDThumb);
				}
				
				// Save full - creates a new file every time
				saveNewImageFile(fullSize, out fullFileID, out fullSizeFileWebPath);

				// Save Thumb - creates a new file every time
				saveNewImageFile(thumbNail, out thumbFileID, out thumbFileWebPath);

				approveInfo.FileID = fullFileID;
				approveInfo.FilePath = fullSizeFilePath;
				approveInfo.FileWebPath = fullSizeFileWebPath;
				approveInfo.ThumbFilePath = thumbFilePath;
				approveInfo.ThumbWebPath = thumbFileWebPath;
				approveInfo.FileIDThumb = thumbFileID;
			}
			catch(Exception ex)
			{
				throw(ex);
			}
			finally
			{
				if (fullSize != null)
				{
					fullSize.Dispose();
				}

				if (thumbNail != null)
				{
					thumbNail.Dispose();
				}
			}
			
		}

        private ApproveInfo processNewFile(MinglePhoto photo)
        {

            string fullSizeFilePath = String.Empty;
            string fullSizeFileWebPath = String.Empty;

            string thumbFilePath = String.Empty;
            string thumbFileWebPath = String.Empty;

            string backupPath = String.Empty;

            int fullFileID = Constants.NULL_INT;
            int thumbFileID = Constants.NULL_INT;

            Image fullSize = null;
            Image thumbNail = null;

            try
            {

                photo.Full = Image.FromStream(new MemoryStream(photo.FullData));
                fullSize = (Image)photo.Full.Clone();
                #endregion

                #region Photo Manipulation
                // Rotate
               
                // Crop Thumbnail
                thumbNail = PhotoUtil.Crop((System.Drawing.Image)fullSize.Clone(), photo.ThumbRectangle.X - photo.CropRectangle.X ,  photo.ThumbRectangle.Y -photo.CropRectangle.Y,
                    photo.ThumbRectangle.Width, photo.ThumbRectangle.Height, true);

                // Crop Full Size
              //  fullSize = PhotoUtil.Crop((System.Drawing.Image)fullSize.Clone(), approveInfo.CropX, approveInfo.CropY,
                //    approveInfo.CropWidth, approveInfo.CropHeight, false);

                // Resize Full Photo
                fullSize = PhotoUtil.Resize(fullSize, PhotoUtil.MaxPhotoWidth, PhotoUtil.MaxPhotoHeight, false);

                // Resize Thumbnail
                thumbNail = PhotoUtil.Resize(thumbNail, PhotoUtil.ThumbnailWidth, PhotoUtil.ThumbnailHeight, true);

                // Compress
                fullSize = PhotoUtil.CompressJpeg(fullSize);
                thumbNail = PhotoUtil.CompressJpeg(thumbNail);
                #endregion

               

                // Save full - creates a new file every time
                saveNewImageFile(fullSize, out fullFileID, out fullSizeFileWebPath);

                // Save Thumb - creates a new file every time
                saveNewImageFile(thumbNail, out thumbFileID, out thumbFileWebPath);
                int	memberPhotoID = Matchnet.PhotoMigration.ServiceAdapters.ConfigurationServiceSA.Instance.GetKey("MemberPhotoID");
                ApproveInfo approveInfo=new ApproveInfo();
               
                approveInfo.MemberID=photo.SparkUserID;
                approveInfo.FileID = fullFileID;
                approveInfo.FilePath = fullSizeFilePath;
                approveInfo.FileWebPath = fullSizeFileWebPath;
                approveInfo.ThumbFilePath = thumbFilePath;
                approveInfo.ThumbWebPath = thumbFileWebPath;
                approveInfo.FileIDThumb = thumbFileID;
                approveInfo.CommunityID = _communityid;
                return approveInfo;
            }
            catch (Exception ex)
            {
                photo.Log.ProcessStatus = (int)ProcessStatus.error;
                photo.Log.ProcessMessage = "Exception in ProcessNewFile:" + ex.Message;

                throw (ex);
            }
            finally
            {
                if (fullSize != null)
                {
                    fullSize.Dispose();
                }

                if (thumbNail != null)
                {
                    thumbNail.Dispose();
                }
            }

        }
	
		/// <summary>
		/// Works with either UNC path or HTTP web path
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="approveInfo"></param>
		/// <returns></returns>
		private Image getImageFromPath(String filePath, ApproveInfo approveInfo)
		{
			try
			{
				Image image = Image.FromFile(filePath);

				MemoryStream stream = new MemoryStream();
				image.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);

				byte[] fileData = stream.ToArray();
				long fileSize = fileData.Length;

				System.Diagnostics.Trace.WriteLine("FA:Loaded photo. " + 
					"FilePath:" + filePath + 
					",  MemberID:" + approveInfo.MemberID.ToString() +
					", MemberPhotoID:" + approveInfo.MemberPhotoID.ToString() +
					", AdminMemberID:" + approveInfo.AdminMemberID.ToString() +
					", Size:" + fileSize.ToString() + 
					", Width:" + image.Width + 
					", Height:" + image.Height +
					", CropHeight:" + approveInfo.CropHeight.ToString() +
					", CropWidth:" + approveInfo.CropWidth.ToString() +
					", CropX:" + approveInfo.CropX.ToString() +
					", CropY:" + approveInfo.CropY.ToString() +
					", CropHeightThumb:" + approveInfo.CropHeightThumb.ToString() +
					", CropWidthThumb:" + approveInfo.CropWidthThumb.ToString() +
					", CropXThumb:" + approveInfo.CropXThumb.ToString() +
					", CropYThumb:" + approveInfo.CropYThumb.ToString() +
					", RotateAngle:" + approveInfo.RotateAngle.ToString());

				image.Dispose();

				return Image.FromStream(stream);
			}
			catch(Exception ex)
			{
				throw new BLException("Error retrieving photo file. [" +
					"FilePath: " + filePath + ", FileID: " + approveInfo.FileID + 
					", MemberPhotoID: " + approveInfo.MemberPhotoID + ", MemberID: " + approveInfo.MemberID + "]",
					ex);
			}
		}

        private PhotoUpdate approveMemberPhoto(ApproveInfo approveInfo, MinglePhoto photo)
		{
			Boolean isApproved;
			Boolean isCaptionApproved=false;
            try
            {

                isCaptionApproved = !String.IsNullOrEmpty(photo.Caption);
                PhotoUpdate photoUpdate = new PhotoUpdate(null,
                    false,
                    Constants.NULL_INT,
                    approveInfo.FileID,
                    approveInfo.FileWebPath,
                    approveInfo.FileIDThumb,
                    approveInfo.ThumbWebPath,
                    photo.ListOrder,
                    true, false,
                    Constants.NULL_INT, photo.SparkUserID, photo.Caption, isCaptionApproved);


                return photoUpdate;
            }
            catch (Exception ex)
            {
                photo.Log.ProcessStatus = (int)ProcessStatus.error;
                photo.Log.ProcessMessage = "Exception in approveMemberPhoto:" + ex.Message;

                throw (ex);
            }
			
		}

        public bool SendToApproveQueue(MinglePhoto photo)
        {
            try
            {
                PhotoUpdate[] update = new PhotoUpdate[1];
                if (photo.SparkUserID <= 0)
                    photo.SparkUserID = photo.Log.SparkMemberId;
                if(String.IsNullOrEmpty(photo.Caption))

                update[0] = new PhotoUpdate(photo.FullData,
                    false,
                    Constants.NULL_INT,
                    Constants.NULL_INT,
                    Constants.NULL_STRING,
                    Constants.NULL_INT,
                    Constants.NULL_STRING,
                    photo.ListOrder,
                    false,
                    false,
                    Constants.NULL_INT,
                    Constants.NULL_INT);
                else


                    update[0] = new PhotoUpdate(photo.FullData,
                        false,
                        Constants.NULL_INT,
                        Constants.NULL_INT,
                        Constants.NULL_STRING,
                        Constants.NULL_INT,
                        Constants.NULL_STRING,
                        photo.ListOrder,
                        false,
                        false,
                        Constants.NULL_INT,
                        Constants.NULL_INT,photo.Caption,false);
                int communityid = Conversion.CInt(Utils.GetSetting("communityid"));
                ServiceAdapters.MemberServiceSA.Instance.SavePhotos(communityid, photo.SparkUserID, update);
                photo.Log.ProcessStatus = (int)ProcessStatus.sendtoapprovequeue;
                photo.Log.ProcessMessage = "Sent to approve queueu";
                photo.Log.LogData = photo.ToString();
                Utils.Log(photo.Log);
                return true;

            }
            catch (Exception ex)
            {
                photo.Log.ProcessStatus = (int)ProcessStatus.error;
                photo.Log.ProcessMessage = "Error sending to approve queue:" + ex.Message;
                throw (ex);


            }


        }
 
        private void attemptResend(MessageQueueTransaction tran, Message msg, Exception ex)
        {
            try
            {
                if (msg == null)
                {
                    attemptRollback(tran);
                    return;
                }

                if (tran.Status == MessageQueueTransactionStatus.Pending)
                {
                    tran.Commit();
                }
                Utils.EnqueueErrorQueue(msg);
            }
            catch (Exception e)
            {
                string message = "Exception while trying to resend msg: " + ex.Message + ", StackTrace: " + ex.StackTrace;
                throw (new Exception(message));
            }
        }

        private void attemptRollback(MessageQueueTransaction tran)
        {
            try
            {
                if (tran != null)
                {
                    if (tran.Status == MessageQueueTransactionStatus.Pending)
                    {
                        tran.Abort();
                    }
                    tran.Dispose();
                    tran = null;
                }
            }
            catch (Exception ex)
            {
                string message = "Exception while trying to abort transaction: " + ex.Message + ", StackTrace: " + ex.StackTrace;
            }
        }


   

		#region PhotoStore

		private void InsertPhotoStore(PhotoUpdate photoUpdate, int memberid, int communityid)
		{
			try
			{
				Matchnet.PhotoSearch.ServiceAdapters.PhotoStoreSA.Instance.InsertPhoto(photoUpdate,memberid,communityid);
			}
		catch(Exception ex)
		{ex=null;}
		}
		#endregion
	}

		
}
