using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing.Drawing2D;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;

namespace Matchnet.PhotoMigration.BusinessLogic
{
	/// <summary>
	/// Provides photo manipulation methods
	/// </summary>
	public class PhotoUtil
	{
        public const string QUEUE_LOCAL_PATH = @".\private$\photo_migration";
		public static string MSMQPath
		{
			get
			{
                string queues=Utils.GetSetting("queueus");
                int queuenum =0;
                if (!String.IsNullOrEmpty(queues))
                {
                    string[] paths = queues.Split(';');
                    
                    int mil = DateTime.Now.Millisecond;
                    if (paths != null || paths.Length > 0)
                    {
                        Utils.Trace("MSMQPath "  + paths.Length.ToString());
                        queuenum = mil % paths.Length;
                        Utils.Trace("MSMQPath,returning "  + paths[queuenum] + @"\private$\photo_migration");
                        return paths[queuenum] + @"\private$\photo_migration";
                    }
                    else
                        return @".\private$\photo_migration";
                }
                else
                {
                    return @".\private$\photo_migration";
                }
				
			}
		}
		public static int MaxPhotoWidth
		{
			get
			{
				return Convert.ToInt16(RuntimeSettings.GetSetting("MEMBERSVC_PHOTO_XMAX"));
			}
		}
		public static int MaxPhotoHeight
		{
			get
			{
				return Convert.ToInt16(RuntimeSettings.GetSetting("MEMBERSVC_PHOTO_YMAX"));
			}
		}
		public static int MaxPhotoSizeBytes
		{
			get
			{
				return Convert.ToInt32(RuntimeSettings.GetSetting("MEMBERSVC_PHOTO_MAXBYTES"));
			}
		}
		public static int ThumbnailWidth
		{
			get
			{
				return Convert.ToInt16(RuntimeSettings.GetSetting("MEMBERSVC_PHOTOTHUMB_XMAX"));
			}
		}
		public static int ThumbnailHeight
		{
			get
			{
				return Convert.ToInt16(RuntimeSettings.GetSetting("MEMBERSVC_PHOTOTHUMB_YMAX"));
			}
		}
		public static int DefaultPhotoSizeBytes
		{
			get
			{
				return 19456;
			}
		}


		/// <summary>
		/// Compresses a jpg image file
		/// Start off with jpeg 90 quality factor and decrements by 2 in loop
		/// When not used in junction with resize it could come out with very
		/// poor quality.
		/// 0 - Lowest Quality, 100 - Highest Quality
		/// </summary>
		/// <param name="image">Jpg file</param>
		public static System.Drawing.Image CompressJpeg(System.Drawing.Image image)
		{
			MemoryStream stream;
			long length;
			int compression = 90;

			EncoderParameters eps = new EncoderParameters(1);
			eps.Param[0] = new EncoderParameter(Encoder.Quality, compression);
			ImageCodecInfo ici = GetEncoderInfo("image/jpeg");
			
			stream = new MemoryStream();
			image.Save(stream, ici, eps);
			length = stream.Length;

			while ((length > MaxPhotoSizeBytes) & (compression > 0))
			{
				eps = new EncoderParameters(1);
				eps.Param[0] = new EncoderParameter(Encoder.Quality, compression);
				stream = new MemoryStream();
				image.Save(stream, ici, eps);
				length = stream.Length;
				compression -= 2;
				//stream.Close(); //http://support.microsoft.com/?id=814675
			}
			image = System.Drawing.Image.FromStream(stream);
			return image;
		}

		public static System.Drawing.Image Crop(System.Drawing.Image sourceImage, int x, int y, int targetWidth, int targetHeight, bool isThumb)
		{
			if (x < 0 || y < 0 || targetWidth <= 0 || targetHeight <= 0)
			{
				throw new Exception("Crop() Negative param values are passed in. [x:" + x + ", y:" + y + ", targetwidth:" +
					targetWidth + ", targetheight:" + targetHeight + "]");
			}

			if (sourceImage.Width < targetWidth || sourceImage.Height < targetHeight)
			{
				string exceptionMessage = "Crop() Invalid param values are passed in. [isthumb:" + isThumb +
					", sourcewidth:" + sourceImage.Width + 
					", sourceHeight:" + sourceImage.Height + ", targetwidth:" +
					targetWidth + ", targetheight:" + targetHeight + "]";

				System.Diagnostics.Trace.WriteLine(exceptionMessage);

				throw new Exception(exceptionMessage);
			}

			// all valid sizes are passed in
			if (x == 0 && y == 0 && sourceImage.Height == targetHeight && sourceImage.Width == targetWidth) 
			{
				return sourceImage;
			}

			Graphics g = null;
			Bitmap cropped = null;

			try
			{
				cropped = new Bitmap(targetWidth, targetHeight);

				g = Graphics.FromImage(cropped);

				g.CompositingQuality = CompositingQuality.HighQuality;
				g.SmoothingMode = SmoothingMode.HighQuality;
				g.InterpolationMode = InterpolationMode.HighQualityBicubic;

				Rectangle rect = new Rectangle(0, 0, targetWidth, targetHeight);

				g.DrawImage(sourceImage, rect, x, y, targetWidth, targetHeight, GraphicsUnit.Pixel);

				return cropped;
			}
			catch(Exception ex)
			{
				throw new Exception("Error cropping.", ex);
			}
			finally
			{
				if (g != null)
				{
					g.Dispose();
				}

				if (sourceImage != null)
				{
					sourceImage.Dispose();
				}
			}
		}

		public static System.Drawing.Image GetImageFromWeb(string webPath)
		{
			try
			{
				System.Net.WebClient wc = new System.Net.WebClient();
				System.IO.MemoryStream stream = new MemoryStream(wc.DownloadData(webPath));
				System.Drawing.Image image = System.Drawing.Image.FromStream(stream);
				//stream.Close(); //http://support.microsoft.com/?id=814675
				return image;
			}
			catch(Exception ex)
			{
				throw new BLException("Unable to get image from webserver " +
					"[WebPath: " + webPath + "]", ex);
			}
		}

		public static System.Drawing.Image Resize(System.Drawing.Image imgPhoto, int Width, int Height, bool isThumb)
		{
			System.Diagnostics.Trace.WriteLine("Resize IsThumb:" + isThumb);

			int sourceWidth = imgPhoto.Width;
			int sourceHeight = imgPhoto.Height;
			int sourceX = 0;
			int sourceY = 0;
			int destX = 0;
			int destY = 0; 

			float nPercent = 0;
			float nPercentW = 0;
			float nPercentH = 0;

			// If smaller than spec than return it back untouched.
			if ((sourceWidth <= Width) & (sourceHeight <= Height))
				return imgPhoto;

			nPercentW = ((float)Width/(float)sourceWidth);
			nPercentH = ((float)Height/(float)sourceHeight);

			if(nPercentH < nPercentW)
			{
				nPercent = nPercentH;
				destX = System.Convert.ToInt16((Width - 
					(sourceWidth * nPercent))/2);
			}
			else
			{
				nPercent = nPercentW;
				destY = System.Convert.ToInt16((Height - 
					(sourceHeight * nPercent))/2);
			}

			int destWidth  = (int)(sourceWidth * nPercent);
			int destHeight = (int)(sourceHeight * nPercent);
			
			Bitmap bmPhoto = new Bitmap(destWidth, destHeight, 
				PixelFormat.Format32bppRgb);
			bmPhoto.SetResolution(imgPhoto.HorizontalResolution, 
				imgPhoto.VerticalResolution);

			Graphics grPhoto = Graphics.FromImage(bmPhoto);
			grPhoto.Clear(Color.White);
			grPhoto.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
			grPhoto.InterpolationMode = 
				InterpolationMode.HighQualityBicubic;

			grPhoto.DrawImage(imgPhoto,
				new Rectangle(sourceX,sourceY,destWidth,destHeight),
				new Rectangle(sourceX,sourceY,sourceWidth,sourceHeight),
				GraphicsUnit.Pixel);

			grPhoto.Dispose();
			imgPhoto.Dispose();

			return bmPhoto;
		}

		public static System.Drawing.Image Rotate(System.Drawing.Image photo, int angle)
		{
			System.Drawing.Imaging.EncoderValue angleValue;

			switch (angle)
			{
				case 0:
					return photo;
				case 90:
					angleValue = EncoderValue.TransformRotate90;
					break;
				case 180:
					angleValue = EncoderValue.TransformRotate180;
					break;
				case 270:
					angleValue = EncoderValue.TransformRotate270;
					break;
				default:
					throw new Exception("Angle must be 0, 90, 180, or 270. [Angle: " 
						+ angle + "]");
			}

			Encoder Enc = Encoder.Transformation; 
			EncoderParameters EncParms = new EncoderParameters(1); 
			EncoderParameter EncParm; 
			ImageCodecInfo CodecInfo = GetEncoderInfo("image/jpeg"); 

			// for rewriting without recompression we must rotate the image 90 degrees
			EncParm = new EncoderParameter(Enc,(long)angleValue); 
			EncParms.Param[0] = EncParm; 

			MemoryStream stream = new MemoryStream();
			photo.Save(stream, CodecInfo, EncParms);

			return System.Drawing.Image.FromStream(stream);
		}

		private static ImageCodecInfo GetEncoderInfo(String mimeType)
		{
			int j;
			ImageCodecInfo[] encoders;
			encoders = ImageCodecInfo.GetImageEncoders();
			for(j = 0; j < encoders.Length; ++j)
			{
				if(encoders[j].MimeType == mimeType)
					return encoders[j];
			}
			return null;
		}
	}
}
