﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.PhotoMigration.ValueObjects
{
    public class MingleMapper
    {
        int _minglemapperid;
        int _provoid;
        int _sparkid;
        string _collisionoutcome = null;
        string _colissionnotes = null;
        DateTime _attributes;
        string _password = null;
        DateTime _substatus;
        DateTime _photoprocessed;
        DateTime _createdate;


        public int MingleMapperID
        {
            get
            {
                return _minglemapperid;
            }
            set
            {
                _minglemapperid = value;
            }

        }


        public int ProvoID
        {
            get
            {
                return _provoid;
            }
            set
            {
                _provoid = value;
            }

        }


        public int SparkID
        {
            get
            {
                return _sparkid;
            }
            set
            {
                _sparkid = value;
            }

        }


        public string CollisionOutcome
        {
            get
            {
                return _collisionoutcome;
            }
            set
            {
                _collisionoutcome = value;
            }

        }


        public string ColissionNotes
        {
            get
            {
                return _colissionnotes;
            }
            set
            {
                _colissionnotes = value;
            }

        }


        public DateTime Attributes
        {
            get
            {
                return _attributes;
            }
            set
            {
                _attributes = value;
            }

        }


        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
            }

        }


        public DateTime SubStatus
        {
            get
            {
                return _substatus;
            }
            set
            {
                _substatus = value;
            }

        }


        public DateTime PhotoProcessed
        {
            get
            {
                return _photoprocessed;
            }
            set
            {
                _photoprocessed = value;
            }

        }


        public DateTime CreateDate
        {
            get
            {
                return _createdate;
            }
            set
            {
                _createdate = value;
            }

        }

        public override string ToString()
        {
            string format = "MingleMapper user ProvoID:{0} SparkID: {1} Created: {2} PhotoProcesses: {3}";
            try
            {
                return String.Format(format, _provoid, _sparkid, _createdate.ToLongDateString(), _photoprocessed.ToLongDateString());

            }
            catch (Exception ex) { return "Error outputting MingleMapper user"; }
        }
    }
}
