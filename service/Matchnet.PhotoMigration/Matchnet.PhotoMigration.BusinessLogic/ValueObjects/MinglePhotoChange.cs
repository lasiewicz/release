﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.PhotoMigration.ValueObjects
{
    public class MinglePhotoChange
    {
        int _userid;
        int _photonum;
        string _action;
        DateTime stamp;


        public int UserID { get { return _userid; } set { _userid = value; } }
      
        public int PhotoNum { get { return _photonum; } set { _photonum = value; } }

        public string Action { get { return _action; } set { _action = value; } }

        public DateTime ChangeStamp { get { return stamp; } set { stamp = value; } }
    }
}
