﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.PhotoMigration.ValueObjects
{

    public class PhotoDeltaLog
    {
        public const string DELTATYPE_CHANGE = "change";

        public const string DELTATYPE_ADMIN = "admin";

        public int PhotoDeltaLogID;
        public DateTime PhotoDeltaDateTime;
        public string DeltaType;

        public string DeltaStatus;



    }
}
