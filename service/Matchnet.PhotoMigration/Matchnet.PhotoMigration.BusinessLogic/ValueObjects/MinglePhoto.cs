﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Matchnet.PhotoMigration.ValueObjects
{
    [Serializable]
    public class MinglePhoto
    {
        int _userid;
        int _sparkmemberid;
        int _photonum;
        int _adminnum;
        byte[] _fulldata;
        byte[] _previewdata;
        byte[] _thumbdata;
        int _possibledefault;

        byte _listorder;
        Rectangle _thumbrect;
        Rectangle _crop;

        Image _full;
        Image _preview;
        Image _thumb;

        string _caption;
        string _processstatus="";

        PhotoMigrationLog _log;

        public int UserID { get { return _userid; } set { _userid = value; } }
        public int SparkUserID { get { return _sparkmemberid; } set { _sparkmemberid = value; } }
        public int PhotoNum { get { return _photonum; } set { _photonum = value; } }
        public int AdminNum { get { return _adminnum; } set { _adminnum = value; } }
        public int PossibleDefault { get { return _possibledefault; } set { _possibledefault = value; } }
        public byte ListOrder { get { return _listorder; } set { _listorder = value; } }

        public PhotoMigrationLog Log { get { return _log; } set { _log = value; } }

        public byte[] FullData { get { return _fulldata; } set { _fulldata = value; } }
        public byte[] PreviewData { get { return _previewdata; } set { _previewdata = value; } }
        public byte[] ThumbData { get { return _thumbdata; } set { _thumbdata = value; } }

        public Image Full { get { return _full; } set { _full = value; } }
        public Image Preview { get { return _preview; } set { _preview = value; } }
        public Image Thumb { get { return _thumb; } set { _thumb = value; } }

        public Rectangle ThumbRectangle { get { return _thumbrect; } set { _thumbrect = value; } }
        public Rectangle CropRectangle { get { return _crop; } set { _crop = value; } }
        
        public string ProcessStatus { get { return _processstatus; } set { _processstatus = value; } }

        public string Caption { get { return _caption; } set { _caption = value; } }
       public override string ToString()
       {
           string format="{0}={1}\r\n";
           System.Text.StringBuilder bld = new System.Text.StringBuilder();
           try
           {
                bld.Append(String.Format(format, "======", "======="));
               bld.Append(String.Format(format, "userid", _userid));
               bld.Append(String.Format(format, "sparkuserid", _sparkmemberid));
               bld.Append(String.Format(format, "photonum", _photonum));
               bld.Append(String.Format(format, "thumbX", _thumbrect.X));
               bld.Append(String.Format(format, "thumbY", _thumbrect.Y));
               bld.Append(String.Format(format, "thumbW", _thumbrect.Width));
               bld.Append(String.Format(format, "thumbH", _thumbrect.Height));
               bld.Append(String.Format(format, "cropX", _crop.X));
               bld.Append(String.Format(format, "cropY", _crop.Y));
               bld.Append(String.Format(format, "cropW", _crop.Width));
               bld.Append(String.Format(format, "cropH", _crop.Height));
               bld.Append(String.Format(format, "fullimgW", _full.Size.Width));
               bld.Append(String.Format(format, "fullimgH", _full.Size.Height));
               bld.Append(String.Format(format, "possibledefault", PossibleDefault));
               bld.Append(String.Format(format, "caption", _caption));
               bld.Append(_processstatus);
               return bld.ToString();

           }
           catch (Exception ex)
           { return bld.ToString(); }
       }


    }



    public class AdminPhoto
    {
        int _userid;
        int _photonum;
     
        Rectangle _thumbrect;
        Rectangle _crop;
        DateTime _stamp;
     
        public int UserID { get { return _userid; } set { _userid = value; } }
        public int PhotoNum { get { return _photonum; } set { _photonum = value; } }
        public DateTime Stamp { get { return _stamp; } set { _stamp = value; } }
     
        public Rectangle ThumbRectangle { get { return _thumbrect; } set { _thumbrect = value; } }
        public Rectangle CropRectangle { get { return _crop; } set { _crop = value; } }


       

    }
}

