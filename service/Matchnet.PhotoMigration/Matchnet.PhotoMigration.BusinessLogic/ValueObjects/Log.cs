﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.PhotoMigration.ValueObjects
{
    [Serializable]
    public class PhotoMigrationLog
{

DateTime _photomigrationbatchstamp;
int _provouserid;
int _sparkmemberid;
int _photonum;
int _adminphotonum;
int _fileid;
string _logdata = null;
string _processmessage = null;
int _processstatus;
DateTime _processdatetime;


public DateTime PhotoMigrationBatchStamp
{ 
 get { 
         return _photomigrationbatchstamp;
     }
set {
      _photomigrationbatchstamp = value; 
     }

}


public int ProvoUserId
{ 
 get { 
         return _provouserid;
     }
set {
      _provouserid = value; 
     }

}


public int SparkMemberId
{ 
 get { 
         return _sparkmemberid;
     }
set {
      _sparkmemberid = value; 
     }

}


public int PhotoNum
{ 
 get { 
         return _photonum;
     }
set {
      _photonum = value; 
     }

}

public int AdminPhotoNum
{
    get
    {
        return _adminphotonum;
    }
    set
    {
        _adminphotonum = value;
    }

}

public int FileID
{
    get
    {
        return _fileid;
    }
    set
    {
        _fileid = value;
    }

}

public string LogData
{ 
 get { 
         return _logdata;
     }
set {
      _logdata = value; 
     }

}


public string ProcessMessage
{ 
 get { 
         return _processmessage;
     }
set {
      _processmessage = value; 
     }

}


public int ProcessStatus
{ 
 get { 
         return _processstatus;
     }
set {
      _processstatus = value; 
     }

}


public DateTime ProcessDateTime
{ 
 get { 
         return _processdatetime;
     }
set {
      _processdatetime = value; 
     }

}


}


}
