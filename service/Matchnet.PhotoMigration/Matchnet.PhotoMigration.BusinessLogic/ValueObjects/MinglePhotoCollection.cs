﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using Matchnet.PhotoMigration.ValueObjects;
namespace Matchnet.PhotoMigration.ValueObjects
{
    [Serializable]
    public class MinglePhotoCollection
    {
  
        int _userid;
        int _sparkid;
        public int UserID { get { return _userid; } set { _userid = value; } }
        public int SparkID { get { return _sparkid; } set { _sparkid = value; } }

        
        List<MinglePhoto> _photos=null;

      [XmlArray]
        public List<MinglePhoto> Photos { get { return _photos; } set { _photos = value; } }

      public override string ToString()
      {
          string format = "MingleMapper user ProvoID:{0} SparkID: {1} Photos: {2}";
          int count=0;
          try
          {
              if(_photos != null)
                  count=_photos.Count;
              return String.Format(format, _userid, _sparkid,count) ;

          }
          catch (Exception ex) { return "Error outputting MinglePhotoCollection: " + ex.ToString(); }
      }
    }
}
