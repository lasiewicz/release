﻿using System;
using System.Collections.Generic;
//using System.Text;


using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.File;
using Matchnet.File.ServiceAdapters;
using Matchnet.File.ValueObjects.ServiceDefinitions;
using Matchnet.File.ValueObjects;
using Matchnet.PhotoFile.ServiceAdapters;
using Matchnet.PhotoFile.ServiceDefinitions;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;

namespace Matchnet.PhotoMigration.ServiceAdapters
{
    public class FileServiceSA
    {

        private const string DATE_FORMAT = "yyyy/MM/dd/HH/";
        private const Int32 DEFAULT_SAVE_DEPTH = 4;
        public static readonly FileServiceSA Instance = new FileServiceSA();

        private FileServiceSA()
		{
            ChannelServices.RegisterChannel(new HttpClientChannel("", new BinaryClientFormatterSinkProvider()));
		}

        public void SaveNewFile(byte[] fileBytes,
        string extension,
        out Int32 fileID,
        out string fileWebPath)
        {
            Int32 fileRootID;

            getNewImageFile(extension, out fileRootID,out fileID, out fileWebPath);

           Save(fileRootID, fileWebPath, fileBytes);
        }


        private void getNewImageFile(string extension,
            out Int32 fileRootID,
            out Int32 fileID,
            out string fileWebPath)
        {
            fileID = ConfigurationServiceSA.Instance.GetKey("FileID");
            fileWebPath = new Uri(assignFilePath(fileID,
                extension,
                out fileRootID)).LocalPath;
        }


        private string assignFilePath(Int32 fileID,
        string fileExtension,
        out Int32 fileRootID)
        {
            try
            {
                Root root = ConfigurationServiceSA.Instance.GetRootGroups().GetWritableRoot();
                DateTime insertDate = Convert.ToDateTime(DateTime.Now.ToString("g")); // no seconds. sql server smalldatetime rounds seconds
                fileRootID = root.RootGroupID;

                insertFile(fileID,
                    root.ID,
                    fileExtension,
                    insertDate);

                string folder = insertDate.ToString(DATE_FORMAT);
                return root.WebPath + FileInstance.GetFileRelativeWebPath(fileID, fileExtension, insertDate, DEFAULT_SAVE_DEPTH);
            }
            catch (Exception ex)
            {
                throw new Exception("assignFilePath() error.", ex);
            }
        }


        private void insertFile(Int32 fileID,
        Int32 fileRootID,
        string fileExtension,
        DateTime insertDate)
        {
            string uri = "";

            try
            {
                uri = getServiceManagerUri();
                getService(uri).InsertFile(fileID,
                    fileRootID,
                    fileExtension,
                    insertDate);
            }
            catch (Exception ex)
            {
                throw new SAException("InsertFile() error (uri:" + uri + ", fileID: " + fileID.ToString() + ", fileRootID: " + fileRootID.ToString() + ", fileExtension: " + fileExtension + ", insertDate: " + insertDate.ToString() + ").",
                    ex);
            }
        }


        private string getServiceManagerUri()
        {
            try
            {
                string uri =BusinessLogic.Utils.GetConnectionString("fileservice");

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }


        private IFileService getService(string uri)
        {
            return Activator.GetObject(typeof(IFileService), uri) as IFileService;
        }



        public void Save(Int32 rootID,
        string relativeFilePath,
        byte[] fileData)
        {
            string uri = "";

            fileData = ResizeImageBytes(fileData);
            fileData = CompressImageBytes(fileData);

            try
            {
                uri = BusinessLogic.Utils.GetConnectionString("photoservice");
                IPhotoFileService service = (IPhotoFileService)Activator.GetObject(typeof(IPhotoFileService), uri);
                service.Save(rootID,relativeFilePath,fileData,Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PHOTOFILESVC_KEY"));
               // PhotoFileSA.Instance.Save(rootID, relativeFilePath, fileData);
            }
            catch (Exception ex)
            {
                throw new Exception("Save() error (uri: " + uri + ", rootID: " + rootID.ToString() + ", relativeFilePath: " + relativeFilePath + ").",
                    ex);
            }
        }


        public byte[] ResizeImageBytes(byte[] fileData)
        {
            try
            {
                MemoryStream stream = new MemoryStream(fileData);
                System.Drawing.Image imgPhoto = Image.FromStream(stream);

                int Width = Convert.ToInt16(RuntimeSettings.GetSetting("MEMBERSVC_PHOTO_XMAX")) * 2;
                int Height = Convert.ToInt16(RuntimeSettings.GetSetting("MEMBERSVC_PHOTO_YMAX")) * 2;
                int sourceWidth = imgPhoto.Width;
                int sourceHeight = imgPhoto.Height;
                int sourceX = 0;
                int sourceY = 0;
                int destX = 0;
                int destY = 0;

                float nPercent = 0;
                float nPercentW = 0;
                float nPercentH = 0;

                // If smaller than spec than return it back untouched.
                if ((sourceWidth <= Width) & (sourceHeight <= Height))
                    return fileData;

                nPercentW = ((float)Width / (float)sourceWidth);
                nPercentH = ((float)Height / (float)sourceHeight);

                if (nPercentH < nPercentW)
                {
                    nPercent = nPercentH;
                    destX = System.Convert.ToInt16((Width -
                        (sourceWidth * nPercent)) / 2);
                }
                else
                {
                    nPercent = nPercentW;
                    destY = System.Convert.ToInt16((Height -
                        (sourceHeight * nPercent)) / 2);
                }

                int destWidth = (int)(sourceWidth * nPercent);
                int destHeight = (int)(sourceHeight * nPercent);

                Bitmap bmPhoto = new Bitmap(destWidth, destHeight,
                    PixelFormat.Format32bppRgb);
                bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                    imgPhoto.VerticalResolution);

                Graphics grPhoto = Graphics.FromImage(bmPhoto);
                grPhoto.Clear(Color.White);
                grPhoto.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                grPhoto.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

                grPhoto.DrawImage(imgPhoto,
                    new Rectangle(sourceX, sourceY, destWidth, destHeight),
                    new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                    GraphicsUnit.Pixel);

                MemoryStream finalStream = new MemoryStream();
                bmPhoto.Save(finalStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                byte[] newFileData = finalStream.ToArray();

                stream.Close();
                finalStream.Close();
                grPhoto.Dispose();
                imgPhoto.Dispose();

                return newFileData;
            }
            catch (Exception ex)
            {
                return fileData;
            }
        }


        private byte[] CompressImageBytes(byte[] fileData)
        {
            try
            {
                MemoryStream stream;
                System.Drawing.Image image;

                long length;
                int compression = 90;

                EncoderParameters eps = new EncoderParameters(1);
                eps.Param[0] = new EncoderParameter(Encoder.Quality, compression);

                ImageCodecInfo ici = GetEncoderInfo("image/jpeg");

                stream = new MemoryStream(fileData);

                image = Image.FromStream(stream);
                length = stream.Length;

                System.Diagnostics.Debug.WriteLine("Image Initial Length:" + stream.Length);

                int loop = 0;
                // allow up to 3 times the limited size. still only 120k but should give better quality to work with.
                while ((length > Convert.ToInt32(RuntimeSettings.GetSetting("MEMBERSVC_PHOTO_MAXBYTES")) * 3) & (compression > 0))
                {
                    eps = new EncoderParameters(1);
                    eps.Param[0] = new EncoderParameter(Encoder.Quality, compression);
                    stream = new MemoryStream();
                    image.Save(stream, ici, eps);
                    length = stream.Length;
                    compression -= 2;
                    //stream.Close(); //http://support.microsoft.com/?id=814675


                    loop++;
                }

                System.Diagnostics.Debug.WriteLine("Image compressed Length:" + stream.Length + " compressed " + loop + " times.");

                byte[] newFileData = stream.ToArray();

                stream.Close();

                image.Dispose();

                return newFileData;
            }
            catch (Exception ex)
            {
                return fileData;
            }
        }


        private ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }




    }
}
