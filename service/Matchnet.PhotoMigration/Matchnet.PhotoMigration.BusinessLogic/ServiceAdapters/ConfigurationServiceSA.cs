﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Matchnet.Configuration;
//using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;
using Matchnet.Configuration.ValueObjects.File;
namespace Matchnet.PhotoMigration.ServiceAdapters
{
    public class ConfigurationServiceSA
    {
        private Matchnet.Caching.Cache _cache;
        public static ConfigurationServiceSA Instance = new ConfigurationServiceSA();

        private ConfigurationServiceSA()
        {
            _cache = Matchnet.Caching.Cache.Instance;
        }

        public Int32 GetKey(string keyName)
        {
            string uri = "";

            try
            {
                uri = BusinessLogic.Utils.GetConnectionString("keyservice");
                return getKeyService(uri).GetKey(keyName);
               
            }
            catch (Exception ex)
            {
                throw (new Exception("Error occurred retrieving key. (uri: " + uri + ")", ex));
            }
        }


        private IKeyService getKeyService(string uri)
        {
            try
            {
                return (IKeyService)Activator.GetObject(typeof(IKeyService), uri);
            }
            catch (Exception ex)
            {
                throw (new Exception("Cannot activate remote service manager at " + uri, ex));
            }
        }

        public RootGroups GetRootGroups()
        {
            try
            {
                RootGroups rootGroups = _cache.Get(RootGroups.CACHE_KEY) as RootGroups;

                if (rootGroups == null)
                {
                    string uri = BusinessLogic.Utils.GetConnectionString("filerootservice");

                    rootGroups = getFileRootService(uri).GetRootGroups();
                    
                    _cache.Add(rootGroups);
                }

                return rootGroups;
            }
            catch (Exception ex)
            {
                throw new Exception("Failure loading ServiceInstance metadata.", ex);
            }
        }


        private IFileRootService getFileRootService(string uri)
        {
            try
            {
                return (IFileRootService)Activator.GetObject(typeof(IFileRootService), uri);
            }
            catch (Exception ex)
            {
                throw (new Exception("Cannot activate remote service manager at " + uri, ex));
            }
        }
    }
}
