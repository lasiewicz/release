﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Collections;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.Member.ValueObjects;
using Matchnet.PhotoMigration.BusinessLogic;
using Matchnet.RemotingClient;
using Matchnet.Caching;
using Matchnet.Member.ValueObjects.Photos;

using Matchnet.CacheSynchronization.Context;
using Matchnet.CacheSynchronization.ValueObjects;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using System.Net;

using Matchnet.RemotingServices.ValueObjects;
using Matchnet.RemotingServices.ValueObjects.ServiceDefinitions;
namespace Matchnet.PhotoMigration.ServiceAdapters
{
   public class MemberServiceSA:SABase
    {
       public static MemberServiceSA Instance = new MemberServiceSA();


       private Cache _cache;
       public const int CACHED_MEMBER_VERSION = 1;


       public void SavePhotos(Int32 communityID,
           Int32 memberID,
           PhotoUpdate[] photoUpdates)
       {
           string uri = "";

           try
           {
               uri = getServiceManagerUri();
              BustMemberMTCache(memberID);
              CachedMember cachedMember = getCachedMember(memberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags.IngoreSACache);
              
               IMemberService svc = getService(uri);

               for (Int32 num = 0; num < photoUpdates.Length; num++)
               {
                   PhotoUpdate photoUpdate = photoUpdates[num];

                   if (photoUpdate.FileID == Constants.NULL_INT && photoUpdate.ImageData != null)
                   {
                       Int32 fileID;
                       string filePath;
                       string fileWebPath;

                       System.Drawing.Image image = null;
                       MemoryStream streamIn;
                       MemoryStream streamOut;
                       try
                       {
                           streamIn = new MemoryStream(photoUpdate.ImageData);
                           image = System.Drawing.Image.FromStream(streamIn, true);
                           streamOut = new MemoryStream();
                           image.Save(streamOut, System.Drawing.Imaging.ImageFormat.Jpeg);
                       }
                       catch (Exception ex)
                       {
                           throw (new Exception("Invalid photo file type or corrupt photo file, could not convert from MemoryStream to System.Drawing.Image.", ex));
                       }
                       base.Checkout(uri);
                       try
                       {
                           FileServiceSA.Instance.SaveNewFile(streamOut.ToArray(),
                               "jpg",
                               out fileID,
                               out fileWebPath);
                       }
                       finally
                       {
                           base.Checkin(uri);
                       }

                       photoUpdate.FileID = fileID;
                       photoUpdate.FileWebPath = fileWebPath;

                       photoUpdate.ImageData = null;
                   }
                   
               }

               base.Checkout(uri);
               try
               {
                   cachedMember.Photos = svc.SavePhotos(System.Environment.MachineName,
                       communityID,
                       memberID,
                       photoUpdates, CACHED_MEMBER_VERSION);
               }
               finally
               {
                   base.Checkin(uri);
               }
           }
           catch (Exception ex)
           {
               throw (new Exception("Error occurred while attempting save photos (uri: " + uri + ")." + ex.ToString()));
           }
       }

       private MemberServiceSA()
       {
           _cache = Cache.Instance;
       }

       internal CachedMember getCachedMember(int memberID, Matchnet.Member.ServiceAdapters.MemberLoadFlags memberLoadFlags)
       {
           string uri = "";

           try
           {
               CachedMember cachedMember = null;
               bool ignoreSACache = (memberLoadFlags & Matchnet.Member.ServiceAdapters.MemberLoadFlags.IngoreSACache) == Matchnet.Member.ServiceAdapters.MemberLoadFlags.IngoreSACache;

               if (!ignoreSACache)
               {
                   cachedMember = _cache.Get(CachedMember.GetCacheKey(memberID)) as CachedMember;
               }
               else
               { System.Diagnostics.EventLog.WriteEntry("WWW", "geCachedMember: IgnoreSACache"); }

               if (cachedMember == null || cachedMember.Version != CACHED_MEMBER_VERSION)
               {
                   
                   Int32 cacheTTL = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MEMBERSVC_CACHE_TTL_MEMBER_SA"));

                   uri = getServiceManagerUri();
                   DateTime begin = DateTime.Now;
                   base.Checkout(uri);
                   try
                   {
                       cachedMember = new CachedMember(getService(uri).GetCachedMemberBytes(System.Environment.MachineName,
                           Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
                           memberID,
                           false, CACHED_MEMBER_VERSION), CACHED_MEMBER_VERSION);

                       ArrayList communityIDList = cachedMember.GetCommunityIDList(AttributeMetadataSA.Instance.GetAttributes(),
                           BrandConfigSA.Instance.GetBrands());

                       for (Int32 i = 0; i < communityIDList.Count; i++)
                       {
                           Int32 communityID = (Int32)communityIDList[i];
                           if (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MEMBERSVC_PHOTO_RELATIVEPATHS", communityID)))
                           {
                               cachedMember.Photos.GetCommunity(communityID).StripHostFromPaths();
                           }
                       }
                   }
                   finally
                   {
                       base.Checkin(uri);
                   }

                   Double totalSeconds = DateTime.Now.Subtract(begin).TotalSeconds;
                   if (totalSeconds > 1)
                   {
                       System.Diagnostics.Trace.WriteLine("__GetCachedMember(" + memberID.ToString() + ") " + totalSeconds.ToString());
                   }

                   cachedMember.CacheTTLSeconds = cacheTTL;
                   _cache.Insert(cachedMember);
               }

               return cachedMember;
           }
           catch (Exception ex)
           {
               throw new Exception("Error occurred while attempting to get cached member. (uri: " + uri + "). " + ex.ToString());
           }
       }

       public void BustMemberMTCache(int memberid)
       {
           try
           {
               string uri = Utils.GetConnectionString("membercachemanager");
               ICacheManagerService svc=(ICacheManagerService)Activator.GetObject(typeof(ICacheManagerService), uri);
               svc.Remove("Member" + memberid.ToString());
           }
           catch (Exception ex)
           { }

       }
       
       public void BustWebCache(string server, string key)
       {
           string url = @"http://" + server + @"/CacheManager.aspx?DeleteKey=" + key;
           HttpWebRequest request = null;
           HttpWebResponse response = null;
           Stream responseStream = null;
           try
           {

               request = (HttpWebRequest)WebRequest.Create(url);

               response = (HttpWebResponse)request.GetResponse();

               responseStream = response.GetResponseStream();

               StringBuilder sb = new StringBuilder();
               byte[] buf = new byte[8192];
               string tempString = null;
               int count = 0;

               do
               {
                   // fill the buffer with data
                   count = responseStream.Read(buf, 0, buf.Length);

                   // make sure we read some data
                   if (count != 0)
                   {
                       // translate from bytes to ASCII text
                       tempString = Encoding.ASCII.GetString(buf, 0, count);

                       // continue building the string
                       sb.Append(tempString);
                   }
               }
               while (count > 0); // any more data to read?

           }
           catch (Exception ex)
           {

           }
           finally
           {
               if (responseStream != null)
                   responseStream.Dispose();
               if (response != null)
                   response.Close();
                   
           }
       }

       internal static IMemberService getService(string uri)
       {
           try
           {
               return (IMemberService)Activator.GetObject(typeof(IMemberService), uri);
           }
           catch (Exception ex)
           {
               throw (new Exception("Cannot activate remote service manager at " + uri, ex));
           }
       }

       internal static string getServiceManagerUri()
       {
           try
           {
               string uri = Utils.GetConnectionString("memberservice");
              
               return uri;
           }
           catch (Exception ex)
           {
               throw (new Exception("Cannot get configuration settings for remote service manager." + ex.ToString()));
           }
       }
       
       protected override void GetConnectionLimit()
       {
           base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MEMBERSVC_SA_CONNECTION_LIMIT"));
       }
    }
}
