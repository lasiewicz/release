﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Matchnet.PhotoMigration.ValueObjects;

using Matchnet;
using Matchnet.RemotingServices.ServiceManagers;
using Matchnet.Caching;
using Matchnet.CacheSynchronization.Context;
using Matchnet.ApproveQueue.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.MSMQueue;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.File.ServiceAdapters;
using MySql.Data.MySqlClient;


using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace Matchnet.PhotoMigration.BusinessLogic
{
   public class DeltaMigratorBL
    {
       #region Public Variables
       public readonly static DeltaMigratorBL Instance = new DeltaMigratorBL();
       #endregion

       private  DeltaMigratorBL()
       {
       }

       public void ProcessPhotoChange()
       {

           int limiter = 0;
           try
           {
               limiter = Conversion.CInt(Utils.GetSetting("limiter"));

               int batch = ProcessPhotoChangeBatch(limiter);
               while (batch > 0)
               {

                   batch = ProcessPhotoChangeBatch(limiter);
               }

           }
           catch (Exception ex)
           { }

       }


       public void ProcessPhotoAdmin(bool bustcache, bool async)
       {

           int limiter = 0;
           try
           {
               limiter = Conversion.CInt(Utils.GetSetting("limiter"));

               int batch = ProcessPhotoAdminBatch(limiter,bustcache,async);
               while (batch > 0)
               {

                   batch = ProcessPhotoAdminBatch(limiter, bustcache, async);
               }

           }
           catch (Exception ex)
           { }

       }
       public int ProcessPhotoChangeBatch(int limiter)
       {
           try
           {
               PhotoDeltaLog deltalog = GetLastPhotoDeltaLog(PhotoDeltaLog.DELTATYPE_CHANGE);

               DateTime lastchangeprocessedstamp=deltalog.PhotoDeltaDateTime;
               Utils.Trace("ProcessPhotoChangeBatch start: " + lastchangeprocessedstamp.ToString());
               List<MinglePhotoChange> changes = GetPhotoChange(limiter, lastchangeprocessedstamp);
               if(changes != null)
                    Utils.Trace("ProcessPhotoChangeBatch get changes: " + changes.Count.ToString());

               else
                   Utils.Trace("ProcessPhotoChangeBatch got  no changes: " );

               string users = GetPhotoChangeUserList(changes);
               List<MingleMapper> mapper = GetMappedUsers(users);
               List<PhotoMigrationLog> logs = GetPhotoMigrationLog(users);
               int communityid = Conversion.CInt(Utils.GetSetting("communityid"));
               foreach (MingleMapper m in mapper)
               {
                   Utils.Trace("ProcessPhotoChangeBatch foreach: " + m.ToString());
                   List<MinglePhotoChange> userchanges = changes.FindAll(delegate(MinglePhotoChange c) { return c.UserID == m.ProvoID; });
                   if(userchanges== null  || userchanges.Count <=0)
                   continue;
                   List<PhotoUpdate> updates = new List<PhotoUpdate>();
                   List<MinglePhoto> minglephotos = MigratorBL.Instance.GetMinglePhotos(m.ProvoID);
                   foreach (MinglePhotoChange change in userchanges)
                   {
                       Utils.Trace("ProcessPhotoChangeBatch foreach photochange: " + m.ToString());
                       PhotoMigrationLog log = logs.Find(delegate(PhotoMigrationLog l) { return l.ProvoUserId == change.UserID && l.PhotoNum == change.PhotoNum; });
                       if (log == null)
                       {
                           Utils.Trace("ProcessPhotoChangeBatch foreach no photochange: " + m.ToString());
                           continue;
                       }


                       Photo photo = GetMemberPhoto(log);
                       if (photo == null)
                           continue;
                       Utils.Trace("ProcessPhotoChangeBatch Got Member Photo: "  + photo.FileID.ToString() + ", " + m.ToString());
                       MinglePhoto mp = minglephotos.Find(delegate(MinglePhoto p) { return p.PhotoNum == change.PhotoNum; });
                       Utils.Trace("ProcessPhotoChangeBatch Got Mingle Photo for: " + photo.FileID.ToString() + ", " + m.ToString());
                      PhotoUpdate update= ProcessPhotoChange(change, photo, log,mp);
                      if (update != null)
                      {
                          updates.Add(update);
                          Utils.Trace("ProcessPhotoChangeBatch adding update for: " + photo.FileID.ToString() + ", " + m.ToString());
                      }
                   }

                   PhotoUpdate[] updateArr = updates.ToArray();
                   Matchnet.PhotoMigration.ServiceAdapters.MemberServiceSA.Instance.SavePhotos(communityid, m.SparkID, updateArr);
               }

               changes.Sort(delegate(MinglePhotoChange c1, MinglePhotoChange c2) { return c1.ChangeStamp.CompareTo(c2.ChangeStamp); });
               changes.Reverse();
               DateTime laststamp = changes[0].ChangeStamp;

               deltalog.PhotoDeltaDateTime = laststamp;
               deltalog.DeltaStatus = "finished";
               LogDelta(deltalog);
               Utils.Trace("ProcessPhotoChangeBatch logging delta: " + laststamp.ToString());
               return changes.Count;
               //Utils.SaveSetting("lastchangestamp", laststamp.ToLongDateString());
           }
           catch (Exception ex)
           {
               Utils.Trace("ProcessPhotoChangeBatch exc: " + ex.ToString());
               throw (ex); }
           finally
           {
           }

       }

       public int ProcessPhotoAdminBatch(int limiter, bool bustcache, bool async)
       {
      
           try
           {
               List<MinglePhotoCollection> allphotos = new List<MinglePhotoCollection>();
               PhotoDeltaLog deltalog = GetLastPhotoDeltaLog(PhotoDeltaLog.DELTATYPE_ADMIN);
               DateTime lastchangeprocessedstamp = deltalog.PhotoDeltaDateTime;
               Utils.Trace("ProcessPhotoAdminBatch: Admin Delta Batch TimeStamp:" + deltalog.PhotoDeltaDateTime.ToString());
               List<AdminPhoto> changes = MigratorBL.GetAdminData(limiter, lastchangeprocessedstamp);
               Utils.Trace("ProcessPhotoAdminBatch: GetAdminChanges " + changes.Count);
               string users = GetPhotoChangeUserList(changes);
               Utils.Trace("ProcessPhotoAdminBatch: GetPhotoChangeUserList: " + users);
               List<MingleMapper> mapper = GetMappedUsers(users);
               List<PhotoMigrationLog> logs = GetPhotoMigrationLog(users);
               Utils.Trace("ProcessPhotoAdminBatch: GetPhotoMigrationLog: " + users);
               int communityid = Conversion.CInt(Utils.GetSetting("communityid"));
               foreach (MingleMapper m in mapper)
               {
                   Utils.Trace("ProcessPhotoAdminBatch: foreach MingleMapped user: " + m.ToString());
                   List<AdminPhoto> userchanges = changes.FindAll(delegate(AdminPhoto c) { return c.UserID == m.ProvoID; });
                   if (userchanges == null || userchanges.Count <= 0)
                   {
                       Utils.Trace("ProcessPhotoAdminBatch - no user changes: " + m.ToString());
                        continue;
                   }
                  
                   List<PhotoMigrationLog> migrationlog = logs.FindAll(delegate(PhotoMigrationLog l) { return l.ProvoUserId == m.ProvoID; });
                   if (migrationlog != null)
                       Utils.Trace("ProcessPhotoAdminBatch - looking into migration log: " + migrationlog.Count.ToString());
                   else
                   {
                       Utils.Trace("ProcessPhotoAdminBatch - looking into migration log: log is null ");
                   }
                   List<MinglePhoto> minglephotos = MigratorBL.Instance.GetMinglePhotos(m.ProvoID);

                   Utils.Trace("ProcessPhotoAdminBatch - Getting minglephotos for " + m.ProvoID.ToString());
                   MigratorBL.Instance.GetMinglePhotosAdditionalInfo(minglephotos, m.ProvoID);
                   Utils.Trace("ProcessPhotoAdminBatch - Getting GetMinglePhotosAdditionalInfo for " + m.ProvoID.ToString());
                   MigratorBL.Instance.GetUserPhotoNotes(m.ProvoID, minglephotos);
                   Utils.Trace("ProcessPhotoAdminBatch - Getting GetUserPhotoNotes for " + m.ProvoID.ToString());

                   //var w = from u in userchanges
                   //        join l in
                   //            (from l in migrationlog where l.ProcessStatus == 1 select l)
                   //            on u.PhotoNum equals l.AdminPhotoNum
                   //            into g
                   //        from l1 in g.DefaultIfEmpty()
                   //        select new { u };

                   List<AdminPhoto> deltaquserchanges = new List<AdminPhoto>();

                   foreach(AdminPhoto a in userchanges)
                   {
                       Utils.Trace("ProcessPhotoAdminBatch - Finding PhotoMigrationLog for " + m.ProvoID.ToString());
                       PhotoMigrationLog l = migrationlog.Find(delegate(PhotoMigrationLog pl) { return pl.PhotoNum == a.PhotoNum && pl.ProcessStatus==2; });
                       if (l == null)
                       {
                           Utils.Trace("ProcessPhotoAdminBatch - Not found PhotoMigrationLog for " + m.ProvoID.ToString());
                           deltaquserchanges.Add(a);
                       }
                       else
                       {
                           Utils.Trace("ProcessPhotoAdminBatch - Found PhotoMigrationLog for " + m.ProvoID.ToString());

                       }

                   }

                   var q = from u in deltaquserchanges
                           join
                           p in minglephotos
                           on u.PhotoNum equals p.AdminNum
                           select new { u, p };

                   List<MinglePhoto> deltaminglephotos = new List<MinglePhoto>();
                   foreach (var i in q)
                   {

                       MigratorBL.Instance.GetAdminPhotoData((MinglePhoto)i.p, deltaquserchanges);
                       MigratorBL.Instance.ProcessUserPhoto(i.p, m, deltaquserchanges, lastchangeprocessedstamp);
                       Utils.Trace("ProcessPhotoAdminBatch - getting deltaminglephotos" + m.ProvoID.ToString());
                       deltaminglephotos.Add((MinglePhoto)i.p);
                   }

                    // we got mingle photos that weren't processed, and those which are present in admin change list
                   if (deltaminglephotos.Count > 0)
                   {
                       MinglePhotoCollection memberPhotos = new MinglePhotoCollection();
                       memberPhotos.UserID = m.ProvoID;
                       memberPhotos.SparkID = m.SparkID;
                       memberPhotos.Photos = deltaminglephotos;
                       
                       allphotos.Add(memberPhotos);
                       Utils.Trace("ProcessPhotoAdminBatch - adding photos for processing" + m.ProvoID.ToString());

                   }


                 
               }

               foreach (MinglePhotoCollection coll in allphotos)
               {

                   Utils.Trace("ProcessPhotoAdminBatch - processing photos" + coll.UserID.ToString() + ", photos count " + coll.Photos.Count) ;
                   int ret = 0;
                   if (!async)
                   {
                       ProceeApproveCollection(coll);
                      
                   }
                   else
                       Utils.Enqueue(coll);

               }

               changes.Sort(delegate(AdminPhoto c1, AdminPhoto c2) { return c1.Stamp.CompareTo(c2.Stamp); });
               changes.Reverse();
               DateTime laststamp = changes[0].Stamp;

               deltalog.PhotoDeltaDateTime = laststamp;
               deltalog.DeltaStatus = "finished";
               LogDelta(deltalog);
               Utils.Trace("ProcessPhotoAdminBatch - logging: " + laststamp.ToString());

               return changes.Count;

               //Utils.SaveSetting("lastchangestamp", laststamp.ToLongDateString());
           }
           catch (Exception ex)
           {
               Utils.Trace("ProcessPhotoAdminBatch - exception: " + ex.ToString());

               throw (ex); }
           finally
           {
           }

       }
       
       private List<MinglePhotoChange> GetPhotoChange(int limiter,DateTime lastprocessedchange)
       {
           MYSQLConnector mysqlconnector = null;
           MySqlDataReader rs = null;
           List<MinglePhotoChange> changes = new List<MinglePhotoChange>();
           try
           {
               string sql = "select * from photo_change where ymdt > '{0}' limit {1}";
               string mysqldate = String.Format("{0}-{1}-{2} {3}:{4}:{5}", lastprocessedchange.Year, lastprocessedchange.Month, lastprocessedchange.Day, lastprocessedchange.Hour, lastprocessedchange.Minute, lastprocessedchange.Second);
              
               sql = String.Format(sql,  mysqldate, limiter);
               mysqlconnector = new MYSQLConnector(Utils.GetConnectionString("minglesite"));
               rs = mysqlconnector.ExecuteReader(String.Format(sql, lastprocessedchange,limiter));

               while (rs.Read())
               {
                   MinglePhotoChange m = new MinglePhotoChange();
                   m.UserID = rs.GetInt32("userid"); ;
                   m.PhotoNum = rs.GetInt32("photo_id");
                   m.Action = rs.GetString("action");
                   m.ChangeStamp = rs.GetDateTime("ymdt");
                  
                   changes.Add(m);
               }

               return changes;
           }
           catch (Exception ex)
           { throw (ex); }
           finally
           {
               if (mysqlconnector != null)
               {
                   mysqlconnector.CloseAll(rs);
               }
           }

       }

       private List<MingleMapper> GetMappedUsers(string userids)
       {

           //string sql = "select distinct provoid,sparkid from MingleMapper where PhotoProcessed is null order by createdate";
           string sql = "select  provoid,sparkid, photoprocessed from {0} where provoid in ({1})";
           string mappertable = Utils.GetSetting("minglemappertable");
           sql = String.Format(sql,mappertable, userids);
           userids = "";
           
           SqlDataReader rs = null;
           try
           {
              rs= SQLConnector.Instance.ExecuteReader(Utils.GetConnectionString("minglemapper"), sql);

              List<MingleMapper> users = new List<MingleMapper>();

               while (rs.Read())
               {
                   int userid =Conversion.CInt( rs["provoid"]);
                   int sparkuserid =Conversion.CInt( rs["sparkid"]);
                   DateTime processed = Conversion.CDateTime(rs["photoprocessed"]);
                   MingleMapper mapper = new MingleMapper();
                   mapper.ProvoID = userid;
                   mapper.SparkID = sparkuserid;
                   mapper.PhotoProcessed=processed;
                   users.Add(mapper);
                   userids += userid.ToString() + ",";

               }

               if(!String.IsNullOrEmpty(userids))
                userids = userids.Substring(0, userids.Length - 1);
               return users;
           }
           catch (Exception ex)
           { throw (ex); }
           finally
           {
               if (rs != null)
               {
                   rs.Dispose();
               }
           }



       }

       private List<PhotoMigrationLog> GetPhotoMigrationLog(string userids)
       {

           //string sql = "select distinct provoid,sparkid from MingleMapper where PhotoProcessed is null order by createdate";
           string sql = "SELECT  photomigrationid,photomigrationbatchstamp,provouserid,sparkmemberid,photonum, adminphotonum,fileid,logdata,processmessage,processstatus,processdatetime from PhotoMigrationLog";
           sql+=" WHERE  provouserid in ({0})";
           sql = String.Format(sql, userids);
           userids = "";
           
           SqlDataReader rs = null;
           try
           {
              rs= SQLConnector.Instance.ExecuteReader(Utils.GetConnectionString("minglemapper"), sql);

              List<PhotoMigrationLog> logs = new List<PhotoMigrationLog>();

               while (rs.Read())
               {
                   PhotoMigrationLog log=new PhotoMigrationLog();
                     //log.PhotoMigrationID = (int) (rs["photomigrationid"]!=Convert.DBNull ? rs["photomigrationid"] : 0);
                    log.PhotoMigrationBatchStamp = (DateTime) (rs["photomigrationbatchstamp"]!=Convert.DBNull ? rs["photomigrationbatchstamp"] : new DateTime());
                    log.SparkMemberId = (int) (rs["sparkmemberid"]!=Convert.DBNull ? rs["sparkmemberid"] : 0);
                    log.PhotoNum = (int) (rs["photonum"]!=Convert.DBNull ? rs["photonum"] : 0);
                    log.AdminPhotoNum = (int)(rs["adminphotonum"] != Convert.DBNull ? rs["adminphotonum"] : 0);
                    log.FileID = (int) (rs["fileid"]!=Convert.DBNull ? rs["fileid"] : 0);
                    log.LogData = (string) (rs["logdata"]!=Convert.DBNull ? rs["logdata"] : "");
                    log.ProcessMessage = (string) (rs["processmessage"]!=Convert.DBNull ? rs["processmessage"] : "");
                    log.ProcessStatus = (int) (rs["processstatus"]!=Convert.DBNull ? rs["processstatus"] : 0);
                    log.ProcessDateTime = (DateTime) (rs["processdatetime"]!=Convert.DBNull ? rs["processdatetime"] : new DateTime());
                    log.ProvoUserId = (int)(rs["provouserid"] != Convert.DBNull ? rs["provouserid"] : 0);
                   logs.Add(log);
               }

             
             return logs;
           }
           catch (Exception ex)
           { throw (ex); }
           finally
           {
               if (rs != null)
               {
                   rs.Dispose();
               }
           }



       }


       private string GetPhotoChangeUserList(List<MinglePhotoChange> list)
       {
         
           StringBuilder users=new StringBuilder();
           try
           {
               if(list== null || list.Count <= 0)
                   return users.ToString();

               list.Sort(delegate(MinglePhotoChange c1, MinglePhotoChange c2) { return c1.UserID.CompareTo(c2.UserID); });
               int lastuserid=list[0].UserID;
               users.Append( lastuserid);
               foreach (MinglePhotoChange c in list)
               {  
                   if(lastuserid !=c.UserID)
                   {
                       users.Append("," + c.UserID.ToString());
                      lastuserid=c.UserID;
                   }
                    
               }
               return users.ToString();
           }
           catch (Exception ex)
           { throw (ex); }
           finally
           {
               
           }

       }

       private string GetPhotoChangeUserList(List<AdminPhoto> list)
       {

           StringBuilder users = new StringBuilder();
           try
           {
               if (list == null || list.Count <= 0)
                   return users.ToString();

               list.Sort(delegate(AdminPhoto c1, AdminPhoto c2) { return c1.UserID.CompareTo(c2.UserID); });
               int lastuserid = list[0].UserID;
               users.Append(lastuserid);
               foreach (AdminPhoto c in list)
               {
                   if (lastuserid != c.UserID)
                   {
                       users.Append("," + c.UserID.ToString());
                       lastuserid = c.UserID;
                   }

               }
               return users.ToString();
           }
           catch (Exception ex)
           { throw (ex); }
           finally
           {

           }

       }

       private Photo GetMemberPhoto(PhotoMigrationLog log)
       {
           List<Photo> photos = new List<Photo>();
           try
           {
               Member.ServiceAdapters.Member m = Member.ServiceAdapters.MemberSA.Instance.GetMember(log.SparkMemberId, MemberLoadFlags.None);

               if (m == null)
                   return null;
               int communityid= Conversion.CInt(Utils.GetSetting("communityid"));
               PhotoCommunity photoCommunity= m.GetPhotos(communityid);
               foreach(Photo p in photoCommunity)
               {
                   if (p.FileID == log.FileID)
                       return p;

               }
               return null;
           }
           catch (Exception ex)
           {
               throw (ex);
           }
       }

       private PhotoUpdate ProcessPhotoChange(MinglePhotoChange change, Photo photo, PhotoMigrationLog log,MinglePhoto minglephoto)
       {
           try
           {
               
               PhotoUpdate update=null;
               if (photo == null || log == null)
                   return null;

               switch (change.Action.ToLower())
               {
                   case "delete":
                        update = new PhotoUpdate(null, true, photo.MemberPhotoID, photo.FileID, photo.FileWebPath, photo.ThumbFileID, photo.ThumbFileWebPath, photo.ListOrder, photo.IsApproved, photo.IsPrivate,Constants.NULL_INT,Constants.NULL_INT);
                       break;

                   case "change_description":
                       {
                       if(minglephoto != null)
                       {string caption=minglephoto.Caption;
                       update = new PhotoUpdate(null, false, photo.MemberPhotoID, photo.FileID, photo.FileWebPath, photo.ThumbFileID, photo.ThumbFileWebPath, photo.ListOrder, photo.IsApproved, photo.IsPrivate, Constants.NULL_INT, Constants.NULL_INT, caption, true);
                       }
                       }
                       break;


               }

               return update;

           }
           catch (Exception ex)
           { throw (ex); }


       }


       private PhotoDeltaLog GetLastPhotoDeltaLog(string type)
       {
           //string sql = "select distinct provoid,sparkid from MingleMapper where PhotoProcessed is null order by createdate";
           string sql = "select  top 1 * from PhotoDeltaLog where deltatype='{0}' order by PhotoDeltaDateTime desc";
           sql=String.Format(sql,type);
           SqlDataReader rs = null;
           PhotoDeltaLog deltalog=new PhotoDeltaLog();
           deltalog.DeltaType = type;
           try
           {
              rs= SQLConnector.Instance.ExecuteReader(Utils.GetConnectionString("minglemapper"), sql);
              
               while (rs.Read())
               {
                   deltalog.PhotoDeltaLogID =Conversion.CInt( rs["PhotoDeltaLogID"]);
                   
                   deltalog.PhotoDeltaDateTime = Conversion.CDateTime(rs["PhotoDeltaDateTime"]);
                   deltalog.DeltaType=type;
                   deltalog.DeltaStatus=rs["DeltaStatus"].ToString();
               }
               if(String.IsNullOrEmpty(deltalog.DeltaType))
               {
                   deltalog.DeltaType=type;
                   deltalog.PhotoDeltaDateTime = new DateTime(2010, 1, 1);

               }
           }
           catch(Exception ex){}
           return deltalog;
       }

       //public int ProcessPhotoAdminBatch(int limiter)
       //{
       //    try
       //    {
       //        PhotoDeltaLog deltalog = GetLastPhotoDeltaLog(PhotoDeltaLog.DELTATYPE_ADMIN);
       //        DateTime lastchangeprocessedstamp = deltalog.PhotoDeltaDateTime;
       //        List<MinglePhotoChange> changes = GetPhotoChange(limiter, lastchangeprocessedstamp);
       //        string users = GetPhotoChangeUserList(changes);
       //        List<MingleMapper> mapper = GetMappedUsers(users);
       //        List<PhotoMigrationLog> logs = GetPhotoMigrationLog(users);
       //        int communityid = Conversion.CInt(Utils.GetSetting("communityid"));
       //        foreach (MingleMapper m in mapper)
       //        {
       //            List<MinglePhotoChange> userchanges = changes.FindAll(delegate(MinglePhotoChange c) { return c.UserID == m.ProvoID; });
       //            if (userchanges == null || userchanges.Count <= 0)
       //                continue;
       //            List<PhotoUpdate> updates = new List<PhotoUpdate>();
       //            List<MinglePhoto> minglephotos = MigratorBL.Instance.GetMinglePhotos(m.ProvoID);
       //            foreach (MinglePhotoChange change in userchanges)
       //            {
       //                PhotoMigrationLog log = logs.Find(delegate(PhotoMigrationLog l) { return l.ProvoUserId == change.UserID && l.PhotoNum == change.PhotoNum; });
       //                if (log == null)
       //                    continue;

       //                Photo photo = GetMemberPhoto(log);
       //                if (photo == null)
       //                    continue;

       //                MinglePhoto mp = minglephotos.Find(delegate(MinglePhoto p) { return p.PhotoNum == change.PhotoNum; });

       //                PhotoUpdate update = ProcessPhotoChange(change, photo, log, mp);
       //                if (update != null)
       //                    updates.Add(update);
       //            }

       //            PhotoUpdate[] updateArr = updates.ToArray();
       //            Matchnet.PhotoMigration.ServiceAdapters.MemberServiceSA.Instance.SavePhotos(communityid, m.SparkID, updateArr);
       //        }

       //        changes.Sort(delegate(MinglePhotoChange c1, MinglePhotoChange c2) { return c1.ChangeStamp.CompareTo(c2.ChangeStamp); });
       //        DateTime laststamp = changes[changes.Count - 1].ChangeStamp;

       //        deltalog.PhotoDeltaDateTime = laststamp;
       //        deltalog.DeltaStatus = "finished";
       //        return changes.Count;

       //        //Utils.SaveSetting("lastchangestamp", laststamp.ToLongDateString());
       //    }
       //    catch (Exception ex)
       //    { throw (ex); }
       //    finally
       //    {
       //    }

       //}
       public static void LogDelta(PhotoDeltaLog log)
       {
           try
           {
               SqlCommand comm = new SqlCommand();
               comm.CommandType = CommandType.StoredProcedure;

               comm.CommandText = "up_PhotoDeltaLogInsert";
               comm.Parameters.Add(new SqlParameter("@photodeltadatetime", SqlDbType.DateTime));
               comm.Parameters[0].Value = log.PhotoDeltaDateTime;


               comm.Parameters.Add(new SqlParameter("@deltatype", SqlDbType.VarChar));
               comm.Parameters[1].Value = log.DeltaType;

               comm.Parameters.Add(new SqlParameter("@deltastatus", SqlDbType.VarChar));
               comm.Parameters[2].Value = log.DeltaStatus;

            
               SQLConnector.Instance.ExecuteNoQuery(Utils.GetConnectionString("minglemapper"), comm);

           }
           catch (Exception ex)
           { Utils.Trace("Error logging new delta:" + ex.ToString()); }


       }
       private int ProceeApproveCollection(MinglePhotoCollection p)
       {
           int processedcount = 0;
           try
           {

               processedcount = PhotoMigrationBL.Instance.ProcessMemberPhotos(p);
               return processedcount;
           }
           catch (Exception ex)
           {
               return processedcount;
           }
       }

    }
}
