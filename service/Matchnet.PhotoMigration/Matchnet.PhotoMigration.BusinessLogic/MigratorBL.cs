﻿using System;
using System.Messaging;
using System.Threading;
using System.Drawing;
using System.IO;
using System.Collections;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;

using Matchnet;
using Matchnet.RemotingServices.ServiceManagers;
using Matchnet.Caching;
using Matchnet.CacheSynchronization.Context;
using Matchnet.ApproveQueue.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.MSMQueue;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.File.ServiceAdapters;
using MySql.Data.MySqlClient;
using Matchnet.PhotoMigration.ValueObjects;

using System.Collections.Generic;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Matchnet.PhotoMigration.BusinessLogic
{

    public enum ProcessStatus : int
    {
        success=1,
        sendtoapprovequeue=2,
        error = 4,
        admindatanotfound=8
     }

    
    public class MigratorBL
    {


       public static MigratorBL Instance = new MigratorBL();

       public void ProcessPhotos(bool bustCache, bool async)
       {
           int limiter = 0;
           try
           {
               limiter = Conversion.CInt(Utils.GetSetting("limiter"));
               
               int batch = ProcessePhotoBatch(limiter,0,bustCache, async);
               while(batch > 0)
               {

                   batch=ProcessePhotoBatch(limiter,0,bustCache,async);
               }

           }
           catch (Exception ex)
           { }

       }

       public int ProcessePhotoBatch(int limiter, int provouserid, bool bustcache, bool async)
       {

           MYSQLConnector mysqlconnector = null;
           MySqlDataReader rs = null;
           List<MingleMapper> users = new  List<MingleMapper>();
           List<MinglePhotoCollection> allphotos = new List<MinglePhotoCollection>();
           int processedphotos = 0;
           int alldatafound = 0;
           int allphotoscount = 0;
           DateTime stamp = DateTime.Now;
           int usercount = 0;
           int usernoadmin = 0;
           int photonoadmin = 0;
           try
           {
               DateTime begin=DateTime.Now;
               string logformat = "Batch={0};Execution Time={1};Provo Users={2};Users with not found Admin Photos={3};All photos={4}; Admin data not found={5};Processed={6}\r\n";
               string logerrorformat = "Batch={0};Error ProvoUserID={1};Error SparkUserID={2}; Photos={3}; Error:{4}\r\n";
              
               string site = Utils.GetSetting("minglesite");
               string userids = "";
               if (provouserid <= 0)
               {
                   Utils.Trace("Getting mapper batch");
                  users = GetMappedUsers(out userids, limiter);
               }
               else
               {
                   Utils.Trace("Getting mapper user: " + provouserid.ToString());
                   users = GetMappedUser(provouserid, out userids);

               }
               if (users.Count == 0)
               {  Utils.Log(String.Format(logformat,stamp,0,0,0,0,0,0,0));
               Utils.Trace("Getting mapper users - 0 users found");
                   return 0;
               }

              // userids = userids.Substring(0, userids.Length - 1);
               Utils.Trace("Getting GetAdminData, " + users.Count + " users found. " );
               List<AdminPhoto> adminphotos = GetAdminData(userids);
               Utils.Trace("Got GetAdminData, " + adminphotos.Count + " photos found");
               int loop = 0;
               usercount = users.Count;
               foreach (MingleMapper user in users)
               { int userphotocount=0;
                   try
                   {
                      // if (user.ProvoID == 171977193)
                      // { string test = ""; }
                       
                       if (adminphotos.Find(delegate(AdminPhoto p) { return p.UserID == user.ProvoID; }) == null)
                       {
                           Utils.Trace("No admin data for " + user.ToString());
                           usernoadmin += 1;

                           List<MinglePhoto> noAdminList = GetMinglePhotos(user.ProvoID);

                           if (noAdminList == null || noAdminList.Count == 0)
                           {
                               LogNoPhoto(stamp, user);
                           }
                           else
                           {
                               Utils.Trace("No admin data. Found " + noAdminList.Count + " Mingle Photos for " + user.ToString());
                           }
                           byte listorder = 0;
                           foreach(MinglePhoto p in noAdminList)
                           {
                               p.SparkUserID = user.SparkID;
                               p.Log = new PhotoMigrationLog();
                             
                               p.Log.PhotoNum = p.PhotoNum;
                               p.Log.PhotoMigrationBatchStamp = stamp;
                               p.Log.ProvoUserId = user.ProvoID;
                               p.Log.SparkMemberId = user.SparkID;
                               p.Log.LogData = p.ToString();
                             
                               List<MinglePhoto> list=new List<MinglePhoto>();
                             
                              
                               
                           }
                        
                           GetMinglePhotosAdditionalInfo(noAdminList, user.ProvoID);
                           ReorderPhotos(noAdminList);
                           foreach (MinglePhoto p in noAdminList)
                           {
                               Utils.Trace("No admin data. Mingle photo: " + p.Log.LogData + ", User: " + user.ToString());
                               if (!async)
                                   PhotoMigrationBL.Instance.SendToApproveQueue(p);
                               else
                                   Utils.Enqueue(p);
                           }
                           listorder = 0;
                           Utils.SetUpdatedStamp(user.ProvoID, user.SparkID, stamp);
                           if (bustcache)
                           {
                               BustCache(user.SparkID);
                           }
                           continue;
                       }
                       //System.Diagnostics.Trace.Write("\r\nUserLoop:" + loop.ToString());
                       Utils.Trace("Found admin data, User: " + user.ToString());
                       int userid = user.ProvoID;
                       List<MinglePhoto> photos = GetMinglePhotos(userid);

                       if (photos != null && photos.Count > 0)
                           userphotocount = photos.Count;
                       else
                       {
                           LogNoPhoto(stamp, user);
                           Utils.SetUpdatedStamp(user.ProvoID, user.SparkID, stamp);
                           Utils.Trace("Found admin data, No Mingle photos found. User: " + user.ToString());
                           continue;
                       }

                       GetMinglePhotosAdditionalInfo(photos, userid);
                       Utils.Trace("Found admin data, found " + photos.Count + " Mingle photos . User: " + user.ToString());
                       allphotoscount += photos.Count;
                       GetUserPhotoNotes(userid, photos);

                       foreach (MinglePhoto photo in photos)
                       {
                           photo.Log.ProcessDateTime=stamp;
                           photo.Log.AdminPhotoNum = photo.AdminNum;
                           ProcessUserPhoto(photo,user,adminphotos,stamp);

                           Utils.Trace("Extracting AdminPhotoId . User: " + user.ToString());
                           if (photo.AdminNum <= 0)
                           {
                               Utils.Trace("AdminPhotoId not found . User: " + user.ToString());
                               if (!async)
                                   PhotoMigrationBL.Instance.SendToApproveQueue(photo);
                               else
                               {
                                   photo.Log.ProcessStatus = (int)ProcessStatus.sendtoapprovequeue;
                                   photo.Log.ProcessMessage = "Sent to approve queueu";
                                   photo.Log.LogData = photo.ToString();
                                   Utils.Enqueue(photo);
                               }
                               
                               photonoadmin += 1;
                            

                           }
                           else
                           {
                              
                               if (!GetAdminPhotoData(photo, adminphotos))
                               {
                                   photo.Log.ProcessStatus += (int)ProcessStatus.admindatanotfound;
                                   if (!async)
                                       PhotoMigrationBL.Instance.SendToApproveQueue(photo);
                                   else
                                   {
                                       photo.Log.ProcessStatus = (int)ProcessStatus.sendtoapprovequeue;
                                       photo.Log.ProcessMessage = "Sent to approve queueu";
                                       photo.Log.LogData = photo.ToString();
                                       Utils.Enqueue(photo);

                                   }

                                  // Utils.Log(photo.Log);
                                   Utils.Trace("Crop info not found. User: " + user.ToString() + " photo:" + photo.ToString());
                               }
                           }

                       }

                       ReorderPhotos(photos);
                       photos.RemoveAll(delegate(MinglePhoto p) { return p.Log.ProcessStatus == (int)ProcessStatus.sendtoapprovequeue; });

                       if (photos.Count > 0)
                       {
                           MinglePhotoCollection memberPhotos=new  MinglePhotoCollection();
                           memberPhotos.UserID=userid;
                           memberPhotos.SparkID = user.SparkID;
                           memberPhotos.Photos=photos;
                           allphotos.Add(memberPhotos);
                           Utils.Trace("Adding to photocoll for later processing. User: " + user.ToString() + " photos count:" + photos.Count.ToString());
                           
                       }
                       Utils.SetUpdatedStamp(userid, user.SparkID, stamp);
                   }
                   catch (Exception ex) 
                   {
                       Utils.Log(String.Format(logerrorformat, stamp, user.ProvoID,user.SparkID,userphotocount, ex.Message));
                   }

               }
              
              // allphotoscount = allphotos.Count;
               foreach (MinglePhotoCollection coll in allphotos)
               {
                   Utils.Trace("Processing photocollections. " + coll.ToString());
                   int ret = 0;
                   if (!async)
                   {
                       ProceeApproveCollection(coll);
                       Utils.SetUpdatedStamp(coll.UserID, coll.SparkID, stamp);
                       processedphotos += ret;
                       if (bustcache)
                       {
                           BustCache(coll.SparkID);
                       }

                   }
                   else
                       Utils.Enqueue(coll);
                
               }

               DateTime end=DateTime.Now;
               int time=(end - begin).Seconds;
               Utils.Log(String.Format(logformat,stamp,time, usercount,usernoadmin,allphotoscount,photonoadmin,processedphotos));
               
               return users.Count;
               
           }
           catch (Exception ex)
           { throw (ex); }
           finally
           {
               if (mysqlconnector != null)
               {
                   mysqlconnector.CloseAll(rs);
               }
               System.Diagnostics.Trace.Write("\r\n All User Photos:" + allphotoscount.ToString() + ", Processed: " + processedphotos.ToString());
           }

       }


   
       public List<MinglePhoto> GetMinglePhotos(int userid)
        {   
            MYSQLConnector mysqlconnector=null;
            MySqlDataReader rs=null;
            List<MinglePhoto> photos=new List<MinglePhoto>();
            try
            {
                string sql="select * from {0} inner join {1}  on {0}.userid={1}.userid and {0}.num={1}.num  where {0}.userid={2} order by {0}.num";
                string fulltable = Utils.GetPartitionName("full_{0}", userid, 64);
                string thumbtable = Utils.GetPartitionName("thumb_{0}", userid, 64);

                mysqlconnector = new MYSQLConnector(Utils.GetConnectionString("minglemember"));
                rs=mysqlconnector.ExecuteReader(String.Format(sql,fulltable, thumbtable, userid));

                while(rs.Read())
                {
                    MinglePhoto m=new MinglePhoto();
                    m.UserID=userid;
                    m.PhotoNum=rs.GetInt32("num");
                    m.FullData=mysqlconnector.GetBlobb(rs,2);
                    m.Log = new PhotoMigrationLog();
                    
                    //m.Full=mysqlconnector.GetBlobbImage(rs,2);
                    //try
                    //{
                    //    m.ThumbData = mysqlconnector.GetBlobb(rs, 6);
                    //    m.Thumb = mysqlconnector.GetBlobbImage(rs, 6);
                    //}
                   // catch (Exception ex) { //who needs thumb}
                    photos.Add(m);
                }

                return photos;
            }
            catch (Exception ex)
            { throw (ex); }
            finally 
            { 
                 if(mysqlconnector != null)
                 {
                     mysqlconnector.CloseAll(rs);
                 }
            }


        }

       public void GetMinglePhotosAdditionalInfo(List<MinglePhoto> photos, int userid)
        {   
            MYSQLConnector mysqlconnector=null;
            MySqlDataReader rs=null;
           
            try
            {
                string sql="select * from photos where userid={0} and num in ({1})";
                string photonums = "";
                if (photos == null || photos.Count == 0)
                    return;

                foreach (MinglePhoto p in photos)
                {
                    photonums+= p.PhotoNum + ",";
                }

                photonums = photonums.Substring(0, photonums.Length - 1);

                mysqlconnector = new MYSQLConnector(Utils.GetConnectionString("minglesite"));
                rs=mysqlconnector.ExecuteReader(String.Format(sql, userid, photonums));

                while(rs.Read())
                { 
                  int num=rs.GetInt32("num");
                  MinglePhoto p = photos.Find(delegate(MinglePhoto p1) { return p1.PhotoNum == num; });
                  if (p != null)
                  {
                      int shift = 0;
                      p.Caption = rs.GetString("description");
                      p.PossibleDefault = rs.GetInt32("possible_default");
                      int order = rs.GetInt32("display_order");
                         if (order == 999)
                          {
                              order = 255;
                          }
                     
                      p.ListOrder = Byte.Parse( order.ToString());
                  }
                }

                
            }
            catch (Exception ex)
            { throw (ex); }
            finally 
            { 
                 if(mysqlconnector != null)
                 {
                     mysqlconnector.CloseAll(rs);
                 }
            }


        }

       public bool GetAdminPhotoNum(MinglePhoto minglephoto)
        {   
            MYSQLConnector mysqlconnector=null;
            MySqlDataReader rs=null;
            string pattern="=>{0} - Photo OK";
            try
            {
                string sql="select * from user_notes  where userid={0} and type='photo'";
                pattern = String.Format(pattern, minglephoto.PhotoNum);
                mysqlconnector = new MYSQLConnector(Utils.GetConnectionString("minglesite"));
                rs=mysqlconnector.ExecuteReader(String.Format(sql,minglephoto.UserID));

                while(rs.Read())
                {
                   string note=rs.GetString("note");
                   int index = note.IndexOf(pattern);
                   minglephoto.ProcessStatus = "not found adminnum";
                   if (index >= 0)
                   {
                       minglephoto.AdminNum = ParseNum(note, index - 3);
                       minglephoto.ProcessStatus = "found adminnum";
                       return true;
                       break;
                   }
                }

                return false;
            }
            catch (Exception ex)
            { throw (ex); }
            finally 
            { 
                 if(mysqlconnector != null)
                 {
                     mysqlconnector.CloseAll(rs);
                 }
            }


        }


       public bool GetUserPhotoNotes(int provouserid, List<MinglePhoto> photos)
       {
           MYSQLConnector mysqlconnector = null;
           MySqlDataReader rs = null;
           string pattern1 = "=>";
           string pattern2 = "- Photo OK";
           try
           {
               string sql = "select * from user_notes  where userid={0} and type='photo' and note like '%Photo OK%'";
             //  pattern = String.Format(pattern, minglephoto.PhotoNum);
               mysqlconnector = new MYSQLConnector(Utils.GetConnectionString("minglesite"));
               rs = mysqlconnector.ExecuteReader(String.Format(sql, provouserid));

               while (rs.Read())
               {
                   string note = rs.GetString("note");
                   int index1 = note.IndexOf(pattern1);
                   int index2 = note.IndexOf(pattern2);
                  // minglephoto.ProcessStatus = "not found adminnum";
                   if (index1 >= 0)
                   {
                       int adminNum = ParseNum(note, index1 - 3);
                       int photonum = ParseNum(note, index1 + 2);

                       MinglePhoto p = photos.Find(delegate(MinglePhoto photo) { return photo.PhotoNum == photonum; });
                       if (p != null)
                           p.AdminNum = adminNum;
                       
                       
                   }
               }

               return false;
           }
           catch (Exception ex)
           { throw (ex); }
           finally
           {
               if (mysqlconnector != null)
               {
                   mysqlconnector.CloseAll(rs);
               }
           }


       }

       public List<AdminPhoto> GetAdminData(int userid)
       {
           MYSQLConnector mysqlconnector = null;
           MySqlDataReader rs = null;
           List<AdminPhoto> photos = new List<AdminPhoto>();
           try
           {
               string sql = "select * from photoQueue_archive where userid={0} and site='{1}'";
               string site = Utils.GetSetting("minglesite");
               mysqlconnector = new MYSQLConnector(Utils.GetConnectionString("mingleadmin"));
               rs = mysqlconnector.ExecuteReader(String.Format(sql, userid, site));

               while (rs.Read())
               {
                   string status = rs.GetString("action");
                   if (status != "approved")
                       continue;

                   AdminPhoto photo = new AdminPhoto();

                   int num = rs.GetInt32("num");
                   int thumbX = rs.GetInt32("thumbX");
                   int thumbY = rs.GetInt32("thumbY");
                   int thumbH = rs.GetInt32("thumbH");
                   int thumbW = rs.GetInt32("thumbW");

                   int cropX = rs.GetInt32("cropX");
                   int cropY = rs.GetInt32("cropY");
                   int cropH = rs.GetInt32("cropH");
                   int cropW = rs.GetInt32("cropW");

                   Rectangle rect = new Rectangle(thumbX, thumbY, thumbW, thumbH);
                   Rectangle croprect = new Rectangle(cropX, cropY, cropW, cropH);

                   photo.UserID = userid;
                   photo.PhotoNum = num;
                   photo.ThumbRectangle = rect;
                   photo.CropRectangle = croprect;
                   photos.Add(photo);

               }
               return photos;
           }
           catch (Exception ex)
           { throw (ex); }
           finally
           {
               if (mysqlconnector != null)
               {
                   mysqlconnector.CloseAll(rs);
               }
           }


       }

       public List<AdminPhoto> GetAdminData(string userids)
       {
           MYSQLConnector mysqlconnector = null;
           MySqlDataReader rs = null;
           List<AdminPhoto> photos = new List<AdminPhoto>();
           try
           {
               string sql = "select * from photoQueue_archive where userid in({0}) and site='{1}'";
               string site = Utils.GetSetting("minglesite");
               mysqlconnector = new MYSQLConnector(Utils.GetConnectionString("mingleadmin"));
               rs = mysqlconnector.ExecuteReader(String.Format(sql, userids, site));
               int UserID = 0;
               while (rs.Read())
               {
                   Utils.Trace("GetAdminData : rs.read");
                   try
                   {
                       string status = rs.GetString("action");
                       if (status != "approved")
                           continue;
                        UserID = rs.GetInt32("userid");
                       AdminPhoto photo = new AdminPhoto();

                       int num = rs.GetInt32("num");
                       int thumbX = rs.GetInt32("thumbX");
                       int thumbY = rs.GetInt32("thumbY");
                       int thumbH = rs.GetInt32("thumbH");
                       int thumbW = rs.GetInt32("thumbW");

                       int cropX = rs.GetInt32("cropX");
                       int cropY = rs.GetInt32("cropY");
                       int cropH = rs.GetInt32("cropH");
                       int cropW = rs.GetInt32("cropW");

                       Rectangle rect = new Rectangle(thumbX, thumbY, thumbW, thumbH);
                       Rectangle croprect = new Rectangle(cropX, cropY, cropW, cropH);

                       photo.UserID = rs.GetInt32("userid");
                       photo.PhotoNum = num;
                       photo.ThumbRectangle = rect;
                       photo.CropRectangle = croprect;
                       photos.Add(photo);
                       UserID = 0;
                   }
                   catch (Exception ex)
                   { Utils.Trace("GetAdminData function exception while reading rs for user:" + UserID.ToString() + ", " + ex.ToString()); }

               }
               return photos;
           }
           catch (Exception ex)
           {
               Utils.Trace("GetAdminData function exceptiion:" + ex.ToString());
               throw (ex); }
           finally
           {
               if (mysqlconnector != null)
               {
                   mysqlconnector.CloseAll(rs);
               }
           }


       }

       public static List<AdminPhoto> GetAdminData(int limiter, DateTime lastprocessedchange)
       {
           MYSQLConnector mysqlconnector = null;
           MySqlDataReader rs = null;
           List<AdminPhoto> photos = new List<AdminPhoto>();
           try
           {
               string sql = "select * from photoQueue_archive where site='{0}' and queued > '{1}' order by queued limit {2}";
               string mysqldate = String.Format("{0}-{1}-{2} {3}:{4}:{5}", lastprocessedchange.Year, lastprocessedchange.Month, lastprocessedchange.Day,lastprocessedchange.Hour,lastprocessedchange.Minute,lastprocessedchange.Second);
               string site = Utils.GetSetting("minglesite");
               sql = String.Format(sql, site, mysqldate, limiter);
               mysqlconnector = new MYSQLConnector(Utils.GetConnectionString("mingleadmin"));
               rs = mysqlconnector.ExecuteReader(sql);

               while (rs.Read())
               {
                   string status = rs.GetString("action");
                   if (status != "approved")
                       continue;

                   AdminPhoto photo = new AdminPhoto();

                   int num = rs.GetInt32("num");
                   int thumbX = rs.GetInt32("thumbX");
                   int thumbY = rs.GetInt32("thumbY");
                   int thumbH = rs.GetInt32("thumbH");
                   int thumbW = rs.GetInt32("thumbW");

                   int cropX = rs.GetInt32("cropX");
                   int cropY = rs.GetInt32("cropY");
                   int cropH = rs.GetInt32("cropH");
                   int cropW = rs.GetInt32("cropW");

                   Rectangle rect = new Rectangle(thumbX, thumbY, thumbW, thumbH);
                   Rectangle croprect = new Rectangle(cropX, cropY, cropW, cropH);

                   photo.UserID = rs.GetInt32("userid");
                   photo.PhotoNum = num;
                   photo.ThumbRectangle = rect;
                   photo.CropRectangle = croprect;
                   photo.Stamp = rs.GetDateTime("queued");
                   photos.Add(photo);

               }
               return photos;
           }
           catch (Exception ex)
           { throw (ex); }
           finally
           {
               if (mysqlconnector != null)
               {
                   mysqlconnector.CloseAll(rs);
               }
           }


       }

        public bool GetAdminPhotoData(MinglePhoto minglephoto, List<AdminPhoto> adminphotos)
       {
           
           try
           {

               AdminPhoto adminphoto = adminphotos.Find(delegate(AdminPhoto a) { return a.PhotoNum == minglephoto.AdminNum && a.UserID==minglephoto.UserID; });
               if(adminphoto != null)
               {
                   minglephoto.ThumbRectangle = adminphoto.ThumbRectangle;
                   minglephoto.CropRectangle =adminphoto.CropRectangle;
                   minglephoto.ProcessStatus += ",found arhivedata";
                   return true;
               }

               return false;
           }
           catch (Exception ex)
           { throw (ex); }
           finally
           {
             
           }


       }

       private int ParseNum(string source, int start)
          {

              string num = source.Substring(start, 3);
              num=num.Replace("-", "").Trim();
              
              return Int32.Parse(num);
          }

       private bool ProcessImage(MinglePhoto p)
       {
           try
           {
               Image img = (Image)p.Full.Clone();
               Bitmap bnew = new Bitmap(p.Full);
               Graphics g = Graphics.FromImage(bnew);
               g.Dispose();
               System.Diagnostics.Trace.Write(p.ToString());
               bnew.Save("c:\\\\test\\full_" + p.UserID.ToString() + "_" + p.PhotoNum.ToString() + ".jpg");

               if (p.ThumbRectangle.Width != 0)
               {
                   Image cropped = PhotoUtil.Crop(img, p.ThumbRectangle.X - p.CropRectangle.X, p.ThumbRectangle.Y - p.CropRectangle.Y, p.ThumbRectangle.Width, p.ThumbRectangle.Height, true);
                 
                   Image resized = PhotoUtil.Resize(cropped, PhotoUtil.ThumbnailWidth, PhotoUtil.ThumbnailHeight, true);
                   Bitmap bcropped = new Bitmap(resized);
                   Graphics g1 = Graphics.FromImage(bcropped);
                   g1.Dispose();
                   bcropped.Save("c:\\\\test\\full_" + p.UserID.ToString() + "_" + p.PhotoNum.ToString() + "_1.jpg");
               }
               else
                   return false;

               return true;
           }
           catch (Exception ex) { return false; }

       }
   
       private bool ProceeApproveImage(MinglePhoto p)
       {
           try
           {

               PhotoMigrationBL.Instance.ProcessApproveInfo(p);
               return true;
           }
           catch (Exception ex)
           {
               return false;
           }
       }

       private int ProceeApproveCollection(MinglePhotoCollection p)
       {
           int processedcount = 0;
           try
           {

             processedcount=  PhotoMigrationBL.Instance.ProcessMemberPhotos(p);
             return processedcount;
           }
           catch (Exception ex)
           {
               return processedcount;
           }
       }

       public bool ProcessUserPhoto(MinglePhoto photo, MingleMapper user, List<AdminPhoto> adminphotos, DateTime stamp )
       {
           try
           {
               photo.Log = new PhotoMigrationLog();
               photo.Log.ProvoUserId = user.ProvoID;
               photo.Log.PhotoMigrationBatchStamp = stamp;
               photo.Log.PhotoNum = photo.PhotoNum;
               photo.Log.SparkMemberId = user.SparkID;

             
               return true;
           }
           catch (Exception ex)
           {
               photo.Log.ProcessStatus = (int)ProcessStatus.error;
               photo.Log.ProcessMessage = "Error processing user photo:" + ex.Message;
               photo.Log.LogData = photo.ToString();
               return false;
           }
       }
   
       private List<MingleMapper> GetUsers(out string userids)
       {

           string sql = "select * from user where deleted <> 0 and photo <> 0 order by userdata_update_ymdt desc limit 20";
           string site = Utils.GetSetting("minglesite");
           userids = "";
           MYSQLConnector mysqlconnector = new MYSQLConnector(Utils.GetConnectionString("minglesite"));

           MySqlDataReader rs = null;
           try
           {

              rs= mysqlconnector.ExecuteReader(sql);
               List<MingleMapper> users = new List<MingleMapper>();

               while (rs.Read())
               {
                   int userid = rs.GetInt32("userid");
                   MingleMapper mapper = new MingleMapper();
                   mapper.ProvoID = userid;
                   //mapper.SparkID = 100046782;
                   
                   users.Add(mapper);
                   userids += userid.ToString() + ",";

               }

               userids = userids.Substring(0, userids.Length - 1);
               return users;
           }
           catch (Exception ex)
           { throw (ex); }
           finally
           {
               if (mysqlconnector != null)
               {
                   mysqlconnector.CloseAll(rs);
                   mysqlconnector = null;
               }
           }

          

       }

       private List<MingleMapper> GetMappedUsers(out string userids, int limiter)
       {

           //string sql = "select distinct provoid,sparkid from MingleMapper where PhotoProcessed is null order by createdate";
           string sql = "select  top {0}  provoid,sparkid from {1} where PhotoProcessed is null ";
           string mappertable = Utils.GetSetting("minglemappertable");
           string additionalwhereclause = Utils.GetSetting("minglemapperwhere");
           if (!String.IsNullOrEmpty(additionalwhereclause))
               sql += " " + additionalwhereclause;

           string orderby = Utils.GetSetting("minglemapperorderby");
           if (!String.IsNullOrEmpty(orderby))
               sql += " " + orderby;

           sql = String.Format(sql, limiter,mappertable);
           userids = "";
           
           SqlDataReader rs = null;
           try
           {
              rs= SQLConnector.Instance.ExecuteReader(Utils.GetConnectionString("minglemapper"), sql);

              List<MingleMapper> users = new List<MingleMapper>();

               while (rs.Read())
               {
                   int userid =Conversion.CInt( rs["provoid"]);
                   int sparkuserid =Conversion.CInt( rs["sparkid"]);

                   MingleMapper mapper = new MingleMapper();
                   mapper.ProvoID = userid;
                   mapper.SparkID = sparkuserid;
                   users.Add(mapper);
                   userids += userid.ToString() + ",";

               }

               if(!String.IsNullOrEmpty(userids))
                userids = userids.Substring(0, userids.Length - 1);
               return users;
           }
           catch (Exception ex)
           { throw (ex); }
           finally
           {
               if (rs != null)
               {
                   rs.Dispose();
               }
           }



       }


       private List<MingleMapper> GetMappedUser(int userid,out string userids)
       {
           string mappertable = Utils.GetSetting("minglemappertable");
           string sql = String.Format("select * from {0} where provoid={1}",mappertable, userid);
          
           userids = "";

           SqlDataReader rs = null;
           try
           {
               rs = SQLConnector.Instance.ExecuteReader(Utils.GetConnectionString("minglemapper"), sql);

               List<MingleMapper> users = new List<MingleMapper>();

               while (rs.Read())
               {
                   
                   int sparkuserid = Conversion.CInt(rs["sparkid"]);

                   MingleMapper mapper = new MingleMapper();
                   mapper.ProvoID = userid;
                   mapper.SparkID = sparkuserid;
                   users.Add(mapper);
                   userids += userid.ToString() ;

               }

            //   userids = userids.Substring(0, userids.Length - 1);
               return users;
           }
           catch (Exception ex)
           { throw (ex); }
           finally
           {
               if (rs != null)
               {
                   rs.Dispose();
               }
           }



       }

       private void ReorderPhotos(List<MinglePhoto> photos)
       {
           try
           {
              
               List<MinglePhoto> p999list = photos.FindAll(delegate(MinglePhoto p) { return Int32.Parse(p.ListOrder.ToString()) == 255; });
               if (p999list == null || p999list.Count == 0)
                   return;

               int p99count = p999list.Count;
               int count = photos.Count;
               foreach (MinglePhoto p in p999list)
               {
                   p.ListOrder =Byte.Parse((count - p99count + 1).ToString());
                   p99count -= 1;

               }

           }

           catch (Exception ex)
           {

           }


       }

       private void LogNoPhoto(DateTime stamp, MingleMapper user)
       {
           try
           {

               PhotoMigrationLog log = new PhotoMigrationLog();
               log.ProcessStatus = 0;
               log.ProcessMessage = "No photos";
               log.ProcessDateTime = DateTime.Now;
               log.SparkMemberId = user.SparkID;
               log.ProvoUserId = user.ProvoID;
               log.LogData = "no photos";
               log.PhotoMigrationBatchStamp = stamp;
               Utils.Log(log);
           }
           catch (Exception ex) { }
           
       }

      

        private void BustCache(int memberid)
        {   try
            {
                ServiceAdapters.MemberServiceSA.Instance.BustMemberMTCache(memberid);

                string webservers = Utils.GetSetting("webservers");
                string[] servers = webservers.Split(';');
                for (int i = 0; i < servers.Length; i++)
                {
                    ServiceAdapters.MemberServiceSA.Instance.BustWebCache(servers[i], "Member" + memberid.ToString());
                }

            }catch(Exception ex)
            {}

        }


        //private List<MingleMapper> GetMappedUsers(out string userids, int limiter)
        //{

        //    //string sql = "select distinct provoid,sparkid from MingleMapper where PhotoProcessed is null order by createdate";
        //    string sql = "select distinct top {0}  provoid,sparkid from MingleMapper where PhotoProcessed is null ";
        //    sql = String.Format(sql, limiter);
        //    userids = "";

        //    SqlDataReader rs = null;
        //    try
        //    {
        //        rs = SQLConnector.Instance.ExecuteReader(Utils.GetConnectionString("minglemapper"), sql);

        //        List<MingleMapper> users = new List<MingleMapper>();

        //        while (rs.Read())
        //        {
        //            int userid = Conversion.CInt(rs["provoid"]);
        //            int sparkuserid = Conversion.CInt(rs["sparkid"]);

        //            MingleMapper mapper = new MingleMapper();
        //            mapper.ProvoID = userid;
        //            mapper.SparkID = sparkuserid;
        //            users.Add(mapper);
        //            userids += userid.ToString() + ",";

        //        }

        //        if (!String.IsNullOrEmpty(userids))
        //            userids = userids.Substring(0, userids.Length - 1);
        //        return users;
        //    }
        //    catch (Exception ex)
        //    { throw (ex); }
        //    finally
        //    {
        //        if (rs != null)
        //        {
        //            rs.Dispose();
        //        }
        //    }



        //}

    }
}
