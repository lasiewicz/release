using System;
using System.Diagnostics;

namespace Matchnet.PhotoMigration.BusinessLogic
{
	public class Metrics
	{
		public static readonly Metrics Instance = new Metrics();

		public static PerformanceCounter BatchesTotal;
		public static PerformanceCounter ErrorsTotal;
		public static PerformanceCounter SuccessTotal;
		public static PerformanceCounter AvgApprovalSecs;
		public static PerformanceCounter AvgApprovalSecsBase;

		public const string SERVICE_NAME = "Matchnet.PhotoMigration.Service";
		
		private Metrics()
		{
			if (PerformanceCounterCategory.Exists(SERVICE_NAME))
			{
				BatchesTotal = new PerformanceCounter(SERVICE_NAME, "Batches Total", false);
				ErrorsTotal = new PerformanceCounter(SERVICE_NAME, "Errors Total", false);
				SuccessTotal = new PerformanceCounter(SERVICE_NAME, "Success Total", false);
				AvgApprovalSecs = new PerformanceCounter(SERVICE_NAME, "Avg Batch Time", false);
				AvgApprovalSecsBase = new PerformanceCounter(SERVICE_NAME, "Avg Batch Time_base", false);

				ResetCounters();
			}
		}

		public void ResetCounters()
		{
			BatchesTotal.RawValue = 0;
			ErrorsTotal.RawValue = 0;
			SuccessTotal.RawValue = 0;
			AvgApprovalSecs.RawValue = 0;
			AvgApprovalSecsBase.RawValue = 0;
		}

		public void WriteInformation(string message) 
		{
			EventLog.WriteEntry(SERVICE_NAME, message, EventLogEntryType.Information);
		}

		public void WriteError(string message) 
		{
			EventLog.WriteEntry(SERVICE_NAME , message, EventLogEntryType.Error);
		}

		public class MetricsInstaller
		{
			public static void Install()
			{
				if (!PerformanceCounterCategory.Exists(SERVICE_NAME))
				{
					CounterCreationDataCollection counters = new CounterCreationDataCollection();

					counters.Add(new CounterCreationData("Batches Total","# items processed total",
						PerformanceCounterType.NumberOfItems32));
					counters.Add(new CounterCreationData("Errors Total","# items processed total",
						PerformanceCounterType.NumberOfItems32));
					counters.Add(new CounterCreationData("Success Total","# items processed total",
						PerformanceCounterType.NumberOfItems32));
					counters.Add(new CounterCreationData("Avg Batch Time", "# seconds for a batch",
						PerformanceCounterType.AverageCount64));
					counters.Add(new CounterCreationData("Avg Batch Time_base", "# seconds for a batch base",
						PerformanceCounterType.AverageBase));

					PerformanceCounterCategory.Create(SERVICE_NAME, 
						"Counters for the service that runs Matchnet Photo Approval Service", 
						counters);
				}

				if(!EventLog.SourceExists(SERVICE_NAME))
				{
					EventLog.CreateEventSource(SERVICE_NAME, "Application");
				}
			}

			public static void Uninstall()
			{
				if (PerformanceCounterCategory.Exists(SERVICE_NAME))
				{
					PerformanceCounterCategory.Delete(SERVICE_NAME);
				}

				if (EventLog.SourceExists(SERVICE_NAME))
				{
					EventLog.DeleteEventSource(SERVICE_NAME);
				}
			}
		}
	}
}
