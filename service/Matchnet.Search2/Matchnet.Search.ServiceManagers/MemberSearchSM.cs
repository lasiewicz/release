using System;
using System.Collections;
using System.Diagnostics;

using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.Replication;
using Matchnet.Search.BusinessLogic;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;


namespace Matchnet.Search.ServiceManagers 
{
	/// <summary>
	/// Service Manager for MemberSearch
	/// </summary>
	public class MemberSearchSM : MarshalByRefObject, IServiceManager, IMemberSearchService, IDisposable, IReplicationRecipient
	{
		private const string SERVICE_MANAGER_NAME = "MemberSearchSM";

		#region Class members

		//private Replicator _replicator = null;
		private PerformanceCounter _perfCount;
		private PerformanceCounter _perfRequestsSec;
		private PerformanceCounter _perfHitRate;
		private PerformanceCounter _perfHitRate_Base;

		#endregion

		/// <summary>
		/// Default constructor for MemberSearch service.
		/// </summary>
		public MemberSearchSM()
		{
			// performance counters / instrumentation
			initPerformanceCounters();

			// bind MemberSearchBL events
			MemberSearchBL.Instance.ReplicationRequested += new MemberSearchBL.ReplicationEventHandler(MemberSearchBL_ReplicationRequested);
			MemberSearchBL.Instance.ResultsAdded += new MemberSearchBL.ResultsAddEventHandler(MemberSearchBL_SearchResultsItemAdded);
			MemberSearchBL.Instance.ResultsRemoved += new MemberSearchBL.ResultsRemoveEventHandler(MemberSearchBL_SearchResultsItemRemoved);
			MemberSearchBL.Instance.ResultsRequested += new MemberSearchBL.ResultsRequestedEventHandler(MemberSearchBL_ResultsRequested);

			string machineName = System.Environment.MachineName;

			string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
			if(overrideMachineName != null)
			{
				machineName = overrideMachineName;
			}

            /*string replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SEARCHSVC_REPLICATION_OVERRIDE");
            if (replicationURI.Length == 0)
            {
                replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(ServiceConstants.SERVICE_CONSTANT, SERVICE_MANAGER_NAME, machineName);
            }

            Trace.WriteLine("Replicating to: " + replicationURI);

			_replicator = new Replicator(SERVICE_MANAGER_NAME);
			_replicator.SetDestinationUri(replicationURI);
			_replicator.Start();*/
		}


		#region IMemberSearchService Members

		/// <summary>
		/// Execute member search.
		/// </summary>
		/// <param name="pPreferences"></param>
		/// <param name="pCommunityID"></param>
		/// <param name="pSiteID"></param>
		/// <param name="pSearchEngineType"></param>
		/// <returns></returns>
		public IMatchnetQueryResults Search(
			ISearchPreferences pPreferences
			, int pCommunityID
			, int pSiteID
			, SearchEngineType pSearchEngineType)
		{
            return Search(pPreferences, pCommunityID, pSiteID, pSearchEngineType, false);
		}

        public IMatchnetQueryResults Search(
            ISearchPreferences pPreferences
            , int pCommunityID
            , int pSiteID
            , SearchEngineType pSearchEngineType
            , bool ignoreCache)
        {
            try
            {
                return MemberSearchBL.Instance.Search((SearchPreferenceCollection)pPreferences, pCommunityID, pSiteID, ignoreCache) as IMatchnetQueryResults;
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure executing member search.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure executing member search.", ex);
            }
        }

        public IMatchnetQueryResults SearchForPushFlirtMatches(ISearchPreferences pPreferences, int memberID, int communityID, int siteID, int brandID, int numMatchesToReturn)
        {
            try
            {
                return MemberSearchBL.Instance.SearchForPushFlirtMatches((SearchPreferenceCollection)pPreferences, memberID, communityID, siteID, brandID, numMatchesToReturn) as IMatchnetQueryResults;
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure executing search for push flirt matches.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure executing search for push flirt matches.", ex);
            }
        }

        public ArrayList SearchDetailed(ISearchPreferences pPreferences, int pCommunityID, int pSiteID)
        {
            try
            {
                return MemberSearchBL.Instance.SearchDetailed((SearchPreferenceCollection)pPreferences, pCommunityID, pSiteID);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure executing member search.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure executing member search.", ex);
            }
        }

        public ArrayList SearchExplainDetailed(ISearchPreferences pPreferences, int pCommunityID, int pSiteID)
        {
            try
            {
                return MemberSearchBL.Instance.SearchExplainDetailed((SearchPreferenceCollection)pPreferences, pCommunityID, pSiteID);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure executing member search.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure executing member search.", ex);
            }
        }


		#endregion

		#region IDisposable Members
		/// <summary>
		/// Dispose implementation.
		/// </summary>
		public void Dispose()
		{
			//resetPerfCounters();
		}
		#endregion

		#region IReplicationRecipient Members

		/// <summary>
		/// Processes an IReplicable object and caches the item locally.
		/// </summary>
		/// <param name="replicableObject"></param>
		public void Receive(IReplicable replicableObject)
		{
			MemberSearchBL.Instance.CacheReplicatedObject(replicableObject);
		}

		// Replication event handler
		private void MemberSearchBL_ReplicationRequested(IReplicable pReplicableObject)
		{
			// disabled for now 12/22
			// _replicator.Enqueue(pReplicableObject);
		}

		#endregion

		#region Instrumentation / Performance Counters

		/// <summary>
		/// Install the performance counters for this service manager.
		/// </summary>
		public static void PerfCounterInstall()
		{	
			CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
			ccdc.AddRange(new CounterCreationData [] {	
														 new CounterCreationData("Search Results Items", "Search Results Items", PerformanceCounterType.NumberOfItems64),
														 new CounterCreationData("Search Requests/second", "Search Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
														 new CounterCreationData("Search Hit Rate", "Search Hit Rate", PerformanceCounterType.RawFraction),
														 new CounterCreationData("Search Hit Rate_Base","Search Hit Rate_Base", PerformanceCounterType.RawBase),
														 new CounterCreationData("Search Preferences Items", "Search Preferences Items", PerformanceCounterType.NumberOfItems64),
														 new CounterCreationData("Preferences Requests/second", "Preferences Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
														 new CounterCreationData("Preferences Hit Rate", "Preferences Hit Rate", PerformanceCounterType.RawFraction),
														 new CounterCreationData("Preferences Hit Rate_Base","Preferences Hit Rate_Base", PerformanceCounterType.RawBase),
														 new CounterCreationData("Preferences Saved", "Preferences Saved", PerformanceCounterType.NumberOfItems64)
													 });
			PerformanceCounterCategory.Create(ServiceConstants.SERVICE_NAME, ServiceConstants.SERVICE_NAME, ccdc);
		}

		/// <summary>
		/// Uninstall the performance counters for this service manager.
		/// </summary>
		public static void PerfCounterUninstall()
		{	
			PerformanceCounterCategory.Delete(ServiceConstants.SERVICE_NAME);
		}

		private void initPerformanceCounters()
		{
			_perfCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Search Results Items", false);
			_perfRequestsSec = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Search Requests/second", false);
			_perfHitRate = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Search Hit Rate", false);
			_perfHitRate_Base = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Search Hit Rate_Base", false);

			resetPerfCounters();
		}

		private void resetPerfCounters()
		{
			_perfCount.RawValue = 0;
			_perfRequestsSec.RawValue = 0;
			_perfHitRate.RawValue = 0;
			_perfHitRate_Base.RawValue = 0;
		}

		private void MemberSearchBL_ResultsRequested(bool cacheHit)
		{
			_perfRequestsSec.Increment();
			_perfHitRate_Base.Increment();

			if (cacheHit)
			{
				_perfHitRate.Increment();
			}
		}

		private void MemberSearchBL_SearchResultsItemAdded()
		{
			_perfCount.Increment();
		}

		private void MemberSearchBL_SearchResultsItemRemoved()
		{
			_perfCount.Decrement();
		}

		#endregion

		/// <summary>
		/// Prepopulate the Matchnet.Search cache items (not implemented at this time).
		/// </summary>
		public void PrePopulateCache()
		{
			// No implementation at this time.
		}

		/// <summary>
		/// Not implemented, returns null.
		/// </summary>
		/// <returns></returns>
		public override object InitializeLifetimeService()
		{
			return null;
		}

	}
}
