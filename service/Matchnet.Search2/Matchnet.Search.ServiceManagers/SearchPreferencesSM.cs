using System;
using System.Diagnostics;

using Matchnet;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Replication;
using Matchnet.Search.BusinessLogic;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;

namespace Matchnet.Search.ServiceManagers
{
	/// <summary>
	/// Summary description for SearchPreferencesSM.
	/// </summary>
    public class SearchPreferencesSM : MarshalByRefObject, IServiceManager,ISearchPreferencesService, Matchnet.Search.ValueObjects.ServiceDefinitions.ISearchPreferencesService, IDisposable, IReplicationRecipient
	{
		private const string SERVICE_MANAGER_NAME = "SearchPreferencesSM";

		#region Class members

		private Replicator _replicator = null;
		private PerformanceCounter _perfCount = null;
		private PerformanceCounter _perfSaveCount = null;
		private PerformanceCounter _perfRequestsSec = null;
		private PerformanceCounter _perfHitRate = null;
		private PerformanceCounter _perfHitRate_Base = null;
		private HydraWriter _hydraWriter;

		#endregion

		/// <summary>
		/// Default constructor for SearchPreferenceSM (Service Manager).
		/// </summary>
		public SearchPreferencesSM()
		{
			// Bind SearchPreferencesBL events.
			SearchPreferencesBL.Instance.ReplicationRequested += new SearchPreferencesBL.ReplicationEventHandler(SearchPreferencesBL_ReplicationRequested);
			SearchPreferencesBL.Instance.PreferencesAdded += new SearchPreferencesBL.PreferencesAddEventHandler(SearchPreferencesBL_SearchPreferencesAdded);
			SearchPreferencesBL.Instance.PreferencesRemoved += new SearchPreferencesBL.PreferencesRemoveEventHandler(SearchPreferencesBL_SearchPreferencesRemoved);
			SearchPreferencesBL.Instance.PreferencesRequested += new SearchPreferencesBL.PreferencesRequestedEventHandler(SearchPreferencesBL_SearchPreferencesRequested);
			SearchPreferencesBL.Instance.PreferencesSaved += new SearchPreferencesBL.PreferencesSavedEventHandler(SearchPreferencesBL_SearchPreferencesSaved);

			string machineName = System.Environment.MachineName;
			string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
			if(overrideMachineName != null)
			{
				machineName = overrideMachineName;
			}

			initPerformanceCounters();		// Initialize performance coutners

			_hydraWriter = new HydraWriter(new string[]{"mnMember","mnAlert"});
			_hydraWriter.Start();

            string replicationURI = string.Empty;

            try
            {
                replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SEARCHSVC_PREFERENCES_REPLICATION_OVERRIDE");
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving replication partner URI.", ex);
            }

            if (replicationURI.Length == 0)
            {
                replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(ServiceConstants.SERVICE_CONSTANT, SERVICE_MANAGER_NAME, machineName);
            }

            Trace.WriteLine("Replicating to: " + replicationURI);

            _replicator = new Replicator(SERVICE_MANAGER_NAME);
            _replicator.SetDestinationUri(replicationURI);
            _replicator.Start();
		}


		#region ISearchPreferencesService implementation

		/// <summary>
		/// Retrieves a collection of search preferences based on the supplied parameters.
		/// </summary>
		/// <param name="pMemberID"></param>
		/// <param name="pCommunityID"></param>
		/// <returns></returns>
		public ISearchPreferences GetSearchPreferences(int pMemberID, int pCommunityID)
		{
			try 
			{
				return SearchPreferencesBL.Instance.GetSearchPreferences(pMemberID, pCommunityID) as ISearchPreferences;
			}
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving search preferences.", ex);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving search preferences.", ex);
			}

		}


		/// <summary>
		/// Save the search preferences for session and member (as applicable).
		/// </summary>
		/// <param name="pMemberId"></param>
		/// <param name="pCommunityId"></param>
		/// <param name="pSearchPreferences"></param>
		public void Save(int pMemberId, int pCommunityId, ISearchPreferences pSearchPreferences)
		{
			try
			{
				SearchPreferencesBL.Instance.Save(pMemberId, pCommunityId, (SearchPreferenceCollection)pSearchPreferences);
			}
			catch (ExceptionBase ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving search preferences.", ex);
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving search preferences.", ex);
			}
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public PreferenceCollection GetPreferences()
        {
            try
            {
                return SearchPreferencesBL.Instance.GetPreferences();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving preferences.", ex);
            }
        }

        public MemberSearchCollection GetMemberSearches(Int32 memberID, Int32 groupID)
        {
            try
            {
                return SearchPreferencesBL.Instance.GetMemberSearches(memberID, groupID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving preferences.", ex);
            }
        }

        public void ProcessAction(IReplicationAction action)
        {
            try
            {
                SearchPreferencesBL.Instance.ProcessAction(action);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving preferences.", ex);
            }
        }
		#endregion

		#region IDisposable Members
		/// <summary>
		/// Dispose implementation.
		/// </summary>
		public void Dispose()
		{
			_hydraWriter.Stop();
		}
		#endregion

		#region IReplicationRecipient Members
		/// <summary>
		/// 
		/// </summary>
		public void Receive(IReplicable pReplicableObject)
		{
			if (pReplicableObject != null)
			{
				SearchPreferencesBL.Instance.CacheReplicatedObject(pReplicableObject);
			}
		}

		// Replication event handler
		private void SearchPreferencesBL_ReplicationRequested(IReplicable pReplicableObject)
		{
			if (pReplicableObject != null)
			{
				_replicator.Enqueue(pReplicableObject);
			}
		}

		#endregion

		#region Instrumentation / Performance Counters
		// NOTE: The performance counters are installed via the MemberSearchSM Install / Uninstall methods in conjunction 
		// with the service being installed.

		private void initPerformanceCounters()
		{
			_perfCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Search Preferences Items", false);
			_perfSaveCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Preferences Saved", false);
			_perfRequestsSec = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Preferences Requests/second", false);
			_perfHitRate = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Preferences Hit Rate", false);
			_perfHitRate_Base = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Preferences Hit Rate_Base", false);

			resetPerfCounters();
		}

		private void resetPerfCounters()
		{
			_perfCount.RawValue = 0;
			_perfSaveCount.RawValue = 0;
			_perfRequestsSec.RawValue = 0;
			_perfHitRate.RawValue = 0;
			_perfHitRate_Base.RawValue = 0;
		}

		private void SearchPreferencesBL_SearchPreferencesRequested(bool cacheHit)
		{
			_perfRequestsSec.Increment();
			_perfHitRate_Base.Increment();

			if (cacheHit)
			{
				_perfHitRate.Increment();
			}
		}

		private void SearchPreferencesBL_SearchPreferencesAdded()
		{
			_perfCount.Increment();
		}

		private void SearchPreferencesBL_SearchPreferencesRemoved()
		{
			_perfCount.Decrement();
		}

		private void SearchPreferencesBL_SearchPreferencesSaved()
		{
			_perfSaveCount.Increment();
		}

		#endregion

		/// <summary>
		/// Prepopulate the Matchnet.Search cache items (not implemented at this time).
		/// </summary>
		public void PrePopulateCache()
		{
		}

		/// <summary>
		/// Returns null
		/// </summary>
		/// <returns></returns>
		public override object InitializeLifetimeService()
		{
			return null;
		}

	}
}
