﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Replication;
using Matchnet.Search.BusinessLogic;
using Matchnet.Search.ValueObjects.ServiceDefinitions;
using Spark.SearchPreferences;


namespace Matchnet.Search.ServiceManagers
{
    public class MemberSearchPreferencesSM : MarshalByRefObject, IServiceManager, IReplicationRecipient, IMemberSearchPreferencesService
    {
        private const string SERVICE_MANAGER_NAME = "MemberSearchPreferencesSM";

        private Replicator _replicator = null;
        private HydraWriter _hydraWriter;

        private PerformanceCounter _perfCount = null;
        private PerformanceCounter _perfSaveCount = null;
        private PerformanceCounter _perfRequestsSec = null;
        private PerformanceCounter _perfHitRate = null;
        private PerformanceCounter _perfHitRate_Base = null;

        public MemberSearchPreferencesSM()
		{
			// Bind SearchPreferencesBL events.
            MemberSearchPreferencesBL.Instance.ReplicationRequested += new MemberSearchPreferencesBL.ReplicationEventHandler(MemberSearchPreferencesBL_ReplicationRequested);
            MemberSearchPreferencesBL.Instance.PreferencesAdded += new MemberSearchPreferencesBL.PreferencesAddEventHandler(MemberSearchPreferencesBL_SearchPreferencesAdded);
            MemberSearchPreferencesBL.Instance.PreferencesRemoved += new MemberSearchPreferencesBL.PreferencesRemoveEventHandler(MemberSearchPreferencesBL_SearchPreferencesRemoved);
            MemberSearchPreferencesBL.Instance.PreferencesRequested += new MemberSearchPreferencesBL.PreferencesRequestedEventHandler(MemberSearchPreferencesBL_SearchPreferencesRequested);
            MemberSearchPreferencesBL.Instance.PreferencesSaved += new MemberSearchPreferencesBL.PreferencesSavedEventHandler(MemberSearchPreferencesBL_SearchPreferencesSaved);

            initPerformanceCounters();		// Initialize performance coutners

			string machineName = System.Environment.MachineName;
			string overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
			if(overrideMachineName != null)
			{
				machineName = overrideMachineName;
			}
            
			_hydraWriter = new HydraWriter(new string[]{"mnMember","mnAlert"});
			_hydraWriter.Start();

            string replicationURI = string.Empty;

            try
            {
                replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SEARCHSVC_PREFERENCES_REPLICATION_OVERRIDE");
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving replication partner URI.", ex);
            }

            if (replicationURI.Length == 0)
            {
                replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(ServiceConstants.SERVICE_CONSTANT, SERVICE_MANAGER_NAME, machineName);
            }

            Trace.WriteLine("Replicating to: " + replicationURI);

            _replicator = new Replicator(SERVICE_MANAGER_NAME);
            _replicator.SetDestinationUri(replicationURI);
            _replicator.Start();
		}

        public void SaveMemberSearchCollection(MemberSearchCollection memberSearchCollection, int memberID, int communityID)
        {
            try
            {
                MemberSearchPreferencesBL.Instance.SaveMemberSearchCollection(memberSearchCollection, memberID, communityID);
            }

            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving member search collection.", ex);
            }
        }

        public MemberSearchCollection GetMemberSearchCollection(int memberID, int communityID)
        {
            try
            {
                return MemberSearchPreferencesBL.Instance.GetMemberSearchCollection( memberID, communityID);
            }

            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retreiving member search collection.", ex);
            }
        }


        #region IDisposable Members
        /// <summary>
        /// Dispose implementation.
        /// </summary>
        public void Dispose()
        {
            _hydraWriter.Stop();
        }
        #endregion

        #region IReplicationRecipient Members
        /// <summary>
        /// 
        /// </summary>
        public void Receive(IReplicable pReplicableObject)
        {
            if (pReplicableObject != null)
            {
                MemberSearchPreferencesBL.Instance.CacheReplicatedObject(pReplicableObject);
            }
        }

        // Replication event handler
        private void MemberSearchPreferencesBL_ReplicationRequested(IReplicable pReplicableObject)
        {
            if (pReplicableObject != null)
            {
                _replicator.Enqueue(pReplicableObject);
            }
        }

        #endregion

        #region Instrumentation / Performance Counters
        // NOTE: The performance counters are installed via the MemberSearchSM Install / Uninstall methods in conjunction 
        // with the service being installed.

        private void initPerformanceCounters()
        {
            _perfCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Search Preferences Items", false);
            _perfSaveCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Preferences Saved", false);
            _perfRequestsSec = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Preferences Requests/second", false);
            _perfHitRate = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Preferences Hit Rate", false);
            _perfHitRate_Base = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Preferences Hit Rate_Base", false);

            resetPerfCounters();
        }

        private void resetPerfCounters()
        {
            _perfCount.RawValue = 0;
            _perfSaveCount.RawValue = 0;
            _perfRequestsSec.RawValue = 0;
            _perfHitRate.RawValue = 0;
            _perfHitRate_Base.RawValue = 0;
        }

        private void MemberSearchPreferencesBL_SearchPreferencesRequested(bool cacheHit)
        {
            _perfRequestsSec.Increment();
            _perfHitRate_Base.Increment();

            if (cacheHit)
            {
                _perfHitRate.Increment();
            }
        }

        private void MemberSearchPreferencesBL_SearchPreferencesAdded()
        {
            _perfCount.Increment();
        }

        private void MemberSearchPreferencesBL_SearchPreferencesRemoved()
        {
            _perfCount.Decrement();
        }

        private void MemberSearchPreferencesBL_SearchPreferencesSaved()
        {
            _perfSaveCount.Increment();
        }

        #endregion

        public void PrePopulateCache()
        {
        }

        /// <summary>
        /// Returns null
        /// </summary>
        /// <returns></returns>
        public override object InitializeLifetimeService()
        {
            return null;
        }
    }
}
