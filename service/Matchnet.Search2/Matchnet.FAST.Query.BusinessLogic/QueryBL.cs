using System;
using System.Collections;
using System.Diagnostics;
using System.Threading;

using no.fast.ds.search;

using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;
using Matchnet.Exceptions;

using Matchnet.FAST.Query.ValueObjects;

namespace Matchnet.FAST.Query.BusinessLogic 
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class QueryBL 
	{
		public static readonly QueryBL Instance = new QueryBL();
		private string [] _miniProfileFields;
		
		private QueryBL() 
		{
			_miniProfileFields =  new string [] {"username", "birthdate", "regionid", "gendermask",  "thumbpath" /* "aboutme",  "headline", not in use*/};
		}

		public IMatchnetQueryResults RunQuery(IMatchnetQuery query)
		{
			Metrics.pcInputTermCount.RawValue = query.Count;
			Metrics.pcQueriesTotal.Increment();
			Metrics.pcQueriesPerSec.Increment();

			TimeSpan t1 = new TimeSpan(DateTime.Now.Ticks);
			
			no.fast.ds.search.IQuery FASTQuery = QueryTransformer.Instance.Transform(query);
			try 
			{
				IMatchnetQueryResults results = RunQuery(FASTQuery, query.SearchType);
				TimeSpan t2 = new TimeSpan(DateTime.Now.Ticks);
				Metrics.pcQueryTime.IncrementBy((long)t2.Subtract(t1).TotalMilliseconds);
				Metrics.pcQueryTimeBase.Increment();
				return results;
			}
			catch (Exception ex)
			{
				new ServiceBoundaryException(Query.ValueObjects.Constants.EVENT_LOG_SOURCE_QUERY,"RunQuery(IMatchnetQuery) exception",ex,false);
				return new MatchnetQueryResults(0);
			}
		}


		public IMatchnetQueryResults RunQuery(no.fast.ds.search.IQuery query, Matchnet.Search.Interfaces.SearchType searchtype)
		{
			IFastSearchEngine engine = SearchEngineFactory.Instance.GetSearchEngine();			
			return TransformResults(engine.Search(query),searchtype);
		}


		/// <summary>
		/// Runs a raw query, that is, full text of FAST query expected.
		/// </summary>
		/// <param name="PassThroughQueryText">full text of query to pass through to FAST</param>
		public IMatchnetQueryResults RunQuery(string PassThroughQueryText)
		{
			IQuery query =QueryFactory.NewInstance().CreateQuery(PassThroughQueryText);
			return RunQuery(query, Matchnet.Search.Interfaces.SearchType.WebSearch);
		}


		public no.fast.ds.search.IQuery GetFASTQuery(IMatchnetQuery query)
		{
			return QueryTransformer.Instance.Transform(query);
		}
		

		private IMatchnetQueryResults TransformResults(no.fast.ds.search.IQueryResult FASTResult, Matchnet.Search.Interfaces.SearchType searchtype)
		{
			MatchnetQueryResults result;

			Metrics.pcFASTQueryTime.IncrementBy(FASTResult.TimeUsed);
			Metrics.pcFASTQueryTimeBase.Increment();

			if ( FASTResult == null) 
			{

				Metrics.pcQueriesReturnedEmpty.Increment();

				result = new MatchnetQueryResults(0);
				return result;
			}

			Metrics.pcFASTMatches.RawValue = FASTResult.DocCount;
			
			result = new MatchnetQueryResults(FASTResult.Hits); // .Hits is the actual number of items returned
			result.MatchesFound = FASTResult.DocCount; // .DocCount is the number of matches found but not necessarily returned

			for (int i = 1; i <= FASTResult.Hits ;i++)
			{ // GetDocument(i) is 1 based !!! not zero based!!!			
				IDocumentSummary summary = FASTResult.GetDocument(i);
				IMatchnetResultItem item = new MatchnetResultItem(Int32.Parse(summary.GetSummaryField("memberid").Summary));
				if (searchtype == Matchnet.Search.Interfaces.SearchType.MatchMail )
				{
					foreach (string field in _miniProfileFields) 
					{
						IDocumentSummaryField sum = summary.GetSummaryField(field);
						item[field] = (sum == null) ? String.Empty : item[field] = sum.Summary;
					}
				}
				result.AddResult(item);
			}

			Metrics.pcResultCount.RawValue = result.Items.Count;

			return result;
		}
	}
}
