using System;
using System.Collections.Specialized;
using System.Threading;

using no.fast.ds.search;

using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.FAST.Query.BusinessLogic {
	/// <summary>
	/// Summary description for SearchEngine.
	/// </summary>
	public class SearchEngineFactory {

		private const int FAST_SEARCH_SERVER_PORT = 15100;
		private const string SETTING_FASTQUERYSVC_QUERYHOST = "FASTQUERYSVC_QUERYHOST";
		private const string SETTING_FAST_SEARCH_SERVER_PORT = "FAST_SEARCH_SERVER_PORT";

		private static string _host;
		private static IFastSearchEngineFactory _factory;

		public static readonly SearchEngineFactory Instance = new SearchEngineFactory();

		private SearchEngineFactory()
		{
			_host = RuntimeSettings.GetSetting(SETTING_FASTQUERYSVC_QUERYHOST);
			_factory = FastSearchEngineFactory.NewInstance();
		}

		/// <summary>
		/// Get a search engine obj.
		/// Exception thrown if failes.
		/// </summary>
		/// <returns></returns>
		public IFastSearchEngine GetSearchEngine(){
			return _factory.CreateSearchEngine(_host, FAST_SEARCH_SERVER_PORT);
		}
	}
}
