using System;
using System.Text;
using System.Diagnostics;
using System.IO;

using no.fast.ds.search;

using Matchnet.Search.Interfaces;
using Matchnet.FAST.Push.ValueObjects;


namespace Matchnet.FAST.Query.BusinessLogic 
{
	/// <summary>
	/// Transforms Matchnet Queries to FAST Queries
	/// </summary>
	public class QueryTransformer 
	{
		public static readonly QueryTransformer Instance = new QueryTransformer();

		private const string OP_AND = " AND ";
		private const string OP_OR = " OR ";
		private const string OP_ANDNOT = " ANDNOT ";
		private const string OP_RANK = " RANK ";
		private const string OP_PLUS = " +";
		private const int MAX_RESULTS_WEBSITE = 36 * 12;
		private const int MAX_RESULTS_MATCHMAIL = 100 + 6;
		
		private IQueryFactory _QueryFactory = QueryFactory.NewInstance();

		private QueryTransformer()
		{
		}

		public no.fast.ds.search.IQuery Transform(IMatchnetQuery  query)
		{
			no.fast.ds.search.IQuery FASTQuery;

			ISearchParameters parameters = this.GetBaseParameters( query.SearchType );
			IMatchnetQueryParameter [] wkp = GetWellKnownParameters(query);		
			IMatchnetQueryParameter mqp;

			QuerySorting sort = QuerySorting.Proximity;
			mqp = GetWellKnownParameter(QuerySearchParam.Sorting,wkp);
			if (mqp != null)
			{
				sort = (QuerySorting)Int32.Parse(mqp.Value.ToString());
			}

			BuildSortTerms(parameters,sort);

			// We must build the Filters before the QueryTerms so that we know if there is an AreaCode filter or not.
			parameters.Parameter = _QueryFactory.CreateParameter(
				BaseParameter.FILTER,
				BuildFilterTerms(wkp, query.SearchType, sort)
				);
			
			FASTQuery = _QueryFactory.CreateQuery(
				BuildQueryTerms(wkp, query.SearchType ,sort),
				parameters
				);

			return FASTQuery;
		}

		public ISearchParameters GetBaseParameters(Matchnet.Search.Interfaces.SearchType searchtype)
		{
			ISearchParameters parameters = _QueryFactory.CreateParameters();
			parameters.Parameter = _QueryFactory.CreateParameter(no.fast.ds.search.SearchType.SEARCH_ADVANCED);
			parameters.Parameter = _QueryFactory.CreateParameter(BaseParameter.DUPREM_SLOT1,"bsummemberid"); // dynamic duplicate removal

			if (searchtype == Matchnet.Search.Interfaces.SearchType.MatchMail)
			{
				parameters.Parameter = _QueryFactory.CreateParameter(BaseParameter.HITS, MAX_RESULTS_MATCHMAIL );
				parameters.Parameter = _QueryFactory.CreateParameter(BaseParameter.RESULT_VIEW, "miniprofile");
			}
			else if (searchtype == Matchnet.Search.Interfaces.SearchType.WebSearch)
			{
				parameters.Parameter = _QueryFactory.CreateParameter(BaseParameter.HITS, MAX_RESULTS_WEBSITE );
				parameters.Parameter = _QueryFactory.CreateParameter(BaseParameter.RESULT_VIEW, "smallresultset"); // just the MemberID
			}
			else
			{
				parameters.Parameter = _QueryFactory.CreateParameter(BaseParameter.HITS, MAX_RESULTS_WEBSITE);
			}

			return parameters;
		}


		private void BuildSortTerms(ISearchParameters parameters,QuerySorting sort)
		{
			//TODO: Add language to have secondary (tie breaker) sorting field
			switch (sort)
			{
				case QuerySorting.JoinDate: 
					parameters.Parameter = _QueryFactory.CreateParameter(BaseParameter.SORT_BY,"-memberregistrationdate");
					break;
				case QuerySorting.LastLogonDate: 
					parameters.Parameter = _QueryFactory.CreateParameter(BaseParameter.SORT_BY,"-lastlogondate");
					break;
				case QuerySorting.Popularity: 
					parameters.Parameter = _QueryFactory.CreateParameter(BaseParameter.SORT_BY,"-popularityrank -popularityrankdate");
					break;
				case QuerySorting.Proximity: // the name "default" refers to default rank profile, not a filed named default.
					// the intention here is to use the rank, not the sort.
					parameters.Parameter = _QueryFactory.CreateParameter(BaseParameter.SORT_BY,"default");
					break;
			}
			
		}


		private string BuildQueryTerms(IMatchnetQueryParameter [] wkp, Matchnet.Search.Interfaces.SearchType searchtype , QuerySorting sort)
		{
			StringBuilder buf = new StringBuilder(255);

			IMatchnetQueryParameter mqp;
			mqp = GetWellKnownParameter(QuerySearchParam.AreaCode,wkp);

			// Only look for Long and Lat if this is not an AreaCode search.
			if (sort == QuerySorting.Proximity && mqp == null)
			{			
				IMatchnetQueryParameter mqpLongitude = GetWellKnownParameter(QuerySearchParam.Longitude,wkp); 
				IMatchnetQueryParameter mqpLatitude = GetWellKnownParameter(QuerySearchParam.Latitude,wkp);
				if (mqpLongitude == null || mqpLatitude == null)
				{
					Debug.WriteLine("Long or Lat not supplied!");
					throw new Exception("Both Longitude and Latitude must be supplied!");
				}

				double Longitude	= Convert.ToDouble(mqpLongitude.Value);
				double Latitude		= Convert.ToDouble(mqpLatitude.Value);

				buf.Append(  string.Join(OP_OR, GeoBox.BestPhases(Longitude, Latitude )) );
				
				mqp = GetWellKnownParameter(QuerySearchParam.RegionID,wkp);
				if (mqp != null)
				{
					buf.Append(OP_OR); buf.Append("RID"); buf.Append(mqp.Value);
				}
				
				mqp = GetWellKnownParameter(QuerySearchParam.RegionIDCountry,wkp);
				if (mqp != null)
				{
					buf.Append(OP_OR); buf.Append("RID"); buf.Append(mqp.Value);
				}
				
				mqp = GetWellKnownParameter(QuerySearchParam.RegionIDCity,wkp);
				if (mqp != null)
				{
					buf.Append(OP_OR); buf.Append("RID"); buf.Append(mqp.Value); buf.Append("!400");
				}

				mqp = GetWellKnownParameter(QuerySearchParam.ZipCode,wkp);
				if (mqp != null)
				{
					buf.Append(OP_OR); buf.Append("ZIP"); buf.Append(mqp.Value); buf.Append("!!!!");
				}
			}

			string result = buf.ToString();
			buf = null;
			return result;
		
		}

		private string BuildFilterTerms(IMatchnetQueryParameter [] wkp, Matchnet.Search.Interfaces.SearchType searchtype, QuerySorting sort)
		{
			StringBuilder buffer = new StringBuilder();
			IMatchnetQueryParameter mqp;
			bool IsAreaCodeSearch = false;
			bool IsSchoolIDSearch = false;
			bool IsIsraelSearch = false;
			string CountryRegionID = "";
			string DomainID = "0";

			// NOTE:  Even the parameter in the filter list must have an OP_PLUS because we are using FILTERTYPE=ANY (by
			// default.  See page 21 of the Query Language and Query Parameters guide.
			if (searchtype == Matchnet.Search.Interfaces.SearchType.MatchMail)
			{
				buffer.Append(OP_PLUS);
				buffer.Append("lastlogondate:>");
				buffer.Append(DateTime.Today.AddYears(-1).ToString("yyyy-MM-dd"));
			}

			mqp = GetWellKnownParameter(QuerySearchParam.DomainID,wkp); 
			if (mqp != null)
			{
				buffer.Append(OP_PLUS);
				buffer.Append("meta.collection:domainid");
				buffer.Append(Int32.Parse(mqp.Value.ToString()));
				DomainID = mqp.Value.ToString();
			}
			mqp = GetWellKnownParameter(QuerySearchParam.AgeMax,wkp); 
			if (mqp != null)
			{
				buffer.Append(OP_PLUS);
				buffer.Append( "birthdate:>");
				buffer.Append(DateTime.Today.AddYears(- Int32.Parse(mqp.Value.ToString()) -1).ToString("yyyy-MM-dd"));
			}
			mqp = GetWellKnownParameter(QuerySearchParam.AgeMin,wkp); 
			if (mqp != null)
			{
				buffer.Append(OP_PLUS);
				buffer.Append( "birthdate:<");
				buffer.Append(DateTime.Today.AddYears(- Int32.Parse(mqp.Value.ToString())).AddDays(1).ToString("yyyy-MM-dd"));
			}
			mqp = GetWellKnownParameter(QuerySearchParam.AllTextFields,wkp); 
			if (mqp != null)
			{ 
				//TODO: post bedrock additions
			}
			mqp = GetWellKnownParameter(QuerySearchParam.BodyType,wkp); 
			if (mqp != null)
			{
				ExpandMaskToValueList("bodytype",Int32.Parse(mqp.Value.ToString())," ", ref buffer);
			}
			mqp = GetWellKnownParameter(QuerySearchParam.DrinkingHabits,wkp); 
			if (mqp != null)
			{
				ExpandMaskToValueList("drinkinghabits",Int32.Parse(mqp.Value.ToString())," ", ref buffer);
			}
			mqp = GetWellKnownParameter(QuerySearchParam.EducationLevel,wkp); 
			if (mqp != null)
			{
				ExpandMaskToValueList("educationlevel",Int32.Parse(mqp.Value.ToString())," ", ref buffer);
			}
			mqp = GetWellKnownParameter(QuerySearchParam.EssayWords,wkp); 
			if (mqp != null)
			{ 
				//TODO: post bedrock additions
			}
			mqp = GetWellKnownParameter(QuerySearchParam.Ethnicity,wkp); 
			if (mqp != null)
			{ 
				ExpandMaskToValueList("ethnicity",Int32.Parse(mqp.Value.ToString())," ", ref buffer);
			}
			mqp = GetWellKnownParameter(QuerySearchParam.GenderMask,wkp); 
			if (mqp != null)
			{ 
				ExpandMaskToValueList("gendermask","gendermask",Int32.Parse(mqp.Value.ToString()),OP_PLUS, false, ref buffer);
			}
			mqp = GetWellKnownParameter(QuerySearchParam.HasPhotoFlag,wkp); 
			if (mqp != null)
			{
																			
				if (mqp.Value.ToString() == "1")
				{
					buffer.Append(OP_PLUS);
					buffer.Append("hasphotoflag:1");
				}
			}
			IMatchnetQueryParameter mqpHeightMin = GetWellKnownParameter(QuerySearchParam.HeightMin,wkp); 
			IMatchnetQueryParameter mqpHeightMax = GetWellKnownParameter(QuerySearchParam.HeightMax,wkp); 
			if (mqpHeightMin != null || mqpHeightMax != null)
			{ 
				buffer.Append(OP_PLUS);
				buffer.Append( "height:[");
				if (mqpHeightMin != null){ buffer.Append(Int32.Parse(mqpHeightMin.Value.ToString())); }
				buffer.Append(";");
				if (mqpHeightMax != null){ buffer.Append(Int32.Parse(mqpHeightMax.Value.ToString())); }
				buffer.Append("]");
			}
			mqp = GetWellKnownParameter(QuerySearchParam.JDateEthnicity,wkp); 
			if (mqp != null)
			{ 
				ExpandMaskToValueList("jdateethnicity",Int32.Parse(mqp.Value.ToString())," ", ref buffer); 
			}
			mqp = GetWellKnownParameter(QuerySearchParam.JDateReligion,wkp); 
			if (mqp != null)
			{ 
				ExpandMaskToValueList("jdatereligion",Int32.Parse(mqp.Value.ToString())," ", ref buffer); 
			}
			mqp = GetWellKnownParameter(QuerySearchParam.KeepKosher,wkp); 
			if (mqp != null)
			{ 
				ExpandMaskToValueList("keepkosher",Int32.Parse(mqp.Value.ToString())," ", ref buffer); 
			}
			mqp = GetWellKnownParameter(QuerySearchParam.LanguageMask,wkp); 
			if (mqp != null)
			{ 
				ExpandMaskToValueList("languagemask","languagemask",Int32.Parse(mqp.Value.ToString())," ", ref buffer); 
			}
			mqp = GetWellKnownParameter(QuerySearchParam.MajorType,wkp);
			if (mqp != null)
			{
				buffer.Append(OP_PLUS);
				buffer.Append("majortype:");
				buffer.Append(Int32.Parse(mqp.Value.ToString()));
			}
			mqp = GetWellKnownParameter(QuerySearchParam.MaritalStatus,wkp); 
			if (mqp != null)
			{ 
				ExpandMaskToValueList("maritalstatus",Int32.Parse(mqp.Value.ToString())," ", ref buffer); 
			}
			mqp = GetWellKnownParameter(QuerySearchParam.RelationshipMask,wkp); 
			if (mqp != null)
			{ 
				ExpandMaskToValueList("relationshipmask","relationshipmask",Int32.Parse(mqp.Value.ToString())," ", ref buffer); 
			}
			mqp = GetWellKnownParameter(QuerySearchParam.RelationshipStatus,wkp); 
			if (mqp != null)
			{ 
				ExpandMaskToValueList("relationshipstatus",Int32.Parse(mqp.Value.ToString())," ", ref buffer); 
			}
			mqp = GetWellKnownParameter(QuerySearchParam.Religion,wkp); 
			if (mqp != null)
			{ 
				ExpandMaskToValueList("religion",Int32.Parse(mqp.Value.ToString())," ", ref buffer); 
			}
			mqp = GetWellKnownParameter(QuerySearchParam.SexualIdentityType,wkp); 
			if (mqp != null)
			{ 
				ExpandMaskToValueList("sexualidentitytype",Int32.Parse(mqp.Value.ToString())," ", ref buffer); 
			}
			mqp = GetWellKnownParameter(QuerySearchParam.SmokingHabits,wkp); 
			if (mqp != null)
			{ 
				ExpandMaskToValueList("smokinghabits",Int32.Parse(mqp.Value.ToString())," ", ref buffer); 
			}
			mqp = GetWellKnownParameter(QuerySearchParam.SynagogueAttendance,wkp); 
			if (mqp != null)
			{ 
				ExpandMaskToValueList("synagogueattendance",Int32.Parse(mqp.Value.ToString())," ", ref buffer); 
			}
			mqp = GetWellKnownParameter(QuerySearchParam.Username,wkp); 
			if (mqp != null)
			{ 
				buffer.Append(OP_PLUS);
				buffer.Append("username:");
				buffer.Append(mqp.Value); 
			}
			mqp = GetWellKnownParameter(QuerySearchParam.Zodiac,wkp); 
			if (mqp != null)
			{ 
				ExpandMaskToValueList("zodiac",Int32.Parse(mqp.Value.ToString())," ", ref buffer); 
			}

			mqp = GetWellKnownParameter(QuerySearchParam.RegionIDCountry,wkp); 
			if (mqp != null)
			{
				CountryRegionID = mqp.Value.ToString();
				if ( CountryRegionID == "105")
				{
					IsIsraelSearch = true;
				}
			}

			mqp = GetWellKnownParameter(QuerySearchParam.AreaCode,wkp);
			if (mqp != null)
			{
				buffer.Append(OP_PLUS);
				buffer.Append("(");
				string [] AreaCodeList = mqp.Value.ToString().Split(',');
				for (int i = 0; i < AreaCodeList.Length; i++)
				{
					if (IsIsraelSearch)
					{
						if (AreaCodeList[i] != string.Empty)
						{
							buffer.Append(IsraelStateFilter( Int32.Parse(AreaCodeList[i])  ));
						}
					}
					else
					{
						buffer.Append(" geoareacode:AC");
						buffer.Append(DomainID);
						buffer.Append("Q");
						buffer.Append(AreaCodeList[i]);
					}
				}
				buffer.Append(")");

				// Set the flag so that we don't look for Long and Lat when building the query terms.
				IsAreaCodeSearch = true;
			}

			// Only add a RegionIDCountry filter if this is an AreaCode search (an AreaCode was included).
			if (IsAreaCodeSearch)
			{
				if (CountryRegionID != string.Empty)
				{ 
					buffer.Append(OP_PLUS);
					buffer.Append("geoencountry:RID");
					buffer.Append(CountryRegionID); 
				}
			}


			mqp = GetWellKnownParameter(QuerySearchParam.SchoolID,wkp); 
			if (mqp != null) // when a school ID is passed, geo doesn't matter. Only school ID is considered.
			{ 
				buffer.Append(OP_PLUS);
				buffer.Append("schoolid:");
				buffer.Append(mqp.Value); 
				IsSchoolIDSearch = true;
			}


			if (sort != QuerySorting.Proximity && !IsAreaCodeSearch && !IsSchoolIDSearch)
			{
				// get the right box sizes and box name
				IMatchnetQueryParameter mqpLongitude = GetWellKnownParameter(QuerySearchParam.Longitude,wkp); 
				IMatchnetQueryParameter mqpLatitude = GetWellKnownParameter(QuerySearchParam.Latitude,wkp);
				if (mqpLongitude == null || mqpLatitude == null)
				{
					Debug.WriteLine("Long or Lat not supplied!");
					throw new Exception("Both Longitude and Latitude must be supplied!");
				}

				double Longitude	= Convert.ToDouble(mqpLongitude.Value);
				double Latitude		= Convert.ToDouble(mqpLatitude.Value);

				mqp = GetWellKnownParameter(QuerySearchParam.GeoDistance,wkp);
				if (mqp == null)
				{
					throw new Exception("Geo Distance from long lat not supplied");
				}
				
				int Distance = Convert.ToInt32(mqp.Value) ;
				if (Distance < 1 || Distance > GeoBox.GetConfig().GridCount)
				{
					throw new Exception("Geo Distance exceeds allowable index of band sizes array");
				}
				
				buffer.Append(OP_PLUS);
				buffer.Append(GeoBox.BestPhases(Longitude,Latitude)[Distance - 1]);
				
			}

			string result = buffer.ToString();
			buffer = null;
			return result;
		}



		/// <summary>
		/// Transforms an int to it's base 2 components and renders ({field}:{prefix}1 {op} {field}:{prefix}16 {op} {field}:{prefix}1024..) into the buffer.
		/// </summary>
		/// <param name="field">the field name in the index profile</param>
		/// <param name="name">the prefix for the int value in the expanded mask</param>
		/// <param name="val">the integer that is the mask to break apart</param>
		/// <param name="op">the operator to stick between terms</param>
		/// <param name="buffer">a reference to a buffer object to render into</param>
		/// <returns>True for success, false for failure</returns>
		private bool ExpandMaskToValueList(string field, string prefix, int val, string op, bool UseParenthesis, ref StringBuilder buffer)
		{
			if ( val <= 0 || val == Int32.MaxValue)
			{
				return false;
			}
			else
			{
				int mask = val;
				buffer.Append(OP_PLUS);
				
				if (UseParenthesis)
					buffer.Append('(');
			
				for (int i = 1; mask != 0 ; mask = mask >> 1, i = i * 2)
				{
					if ((1 & mask) == 1)
					{
						buffer.Append(field);
						buffer.Append(':');
						buffer.Append(prefix);
						buffer.Append(i.ToString());
						buffer.Append(op);
					}	
				}

				buffer.Remove(buffer.Length - op.Length,op.Length);

				if (UseParenthesis)
					buffer.Append(')');

				return true;
			} 
		}

		private bool ExpandMaskToValueList(string field, string prefix, int val, string op, ref StringBuilder buffer)
		{
			return ExpandMaskToValueList(field, prefix, val, op, true, ref buffer);
		}


		/// <summary>
		/// Transforms an int to it's base 2 components and renders ({field}:1 {op} {field}:16 {op} {field}:1024..) into the buffer.
		/// </summary>
		/// <param name="field">the field name in the index profile</param>
		/// <param name="val">the integer that is the mask to break apart</param>
		/// <param name="op">the operator to stick between terms</param>
		/// <param name="buffer">a reference to a buffer object to render into</param>
		/// <returns>True for success, false for failure</returns>
		private bool ExpandMaskToValueList(string field, int val, string op, bool UseParenthesis, ref StringBuilder buffer)
		{
			if ( val <= 0 || val == Int32.MaxValue)
			{
				return false;
			}
			else 
			{
				buffer.Append(OP_PLUS);

				if (UseParenthesis)
					buffer.Append('(');
				
				int mask = val;
				for (int i = 1; mask != 0 ; mask = mask >> 1, i = i * 2)
				{
					if ((1 & mask) == 1)
					{
						buffer.Append(field);
						buffer.Append(':');
						buffer.Append(i.ToString());
						buffer.Append(op);
					}	
				}
			
				buffer.Remove(buffer.Length - op.Length,op.Length);

				if (UseParenthesis)
					buffer.Append(')');

				return true;
			} 
		}

		private bool ExpandMaskToValueList(string field, int val, string op, ref StringBuilder buffer)
		{
			return ExpandMaskToValueList(field, val, op, true, ref buffer);
		}

		/// <summary>
		/// Gets an array with parameter values at array[(int)parameter.paramEnumVal] 
		/// This allows for no scanning when building sub queries.
		/// </summary>
		/// <param name="qry"></param>
		/// <returns></returns>
		private IMatchnetQueryParameter [] GetWellKnownParameters( Matchnet.Search.Interfaces.IMatchnetQuery qry )	
		{
			IMatchnetQueryParameter [] wkp = new IMatchnetQueryParameter[ Enum.GetValues(typeof(QuerySearchParam)).Length + 1];
			for (int i = 0; i < qry.Count; i++)
			{
				wkp[(int)(qry[i].Parameter)] = qry[i];
			}
			return wkp;
		}


		private IMatchnetQueryParameter GetWellKnownParameter(QuerySearchParam param, IMatchnetQueryParameter[] WellKnownParameters)
		{
			return WellKnownParameters[(int)param];
		}


		/// <summary>
		/// Map the israeli area code index ( attribute option ID) to a "state" level regionid for exact match.
		/// </summary>
		/// <param name="IsraelPhoneAreaCodeAttributeOptionID"></param>
		/// <returns></returns>
		private string IsraelStateFilter(int IsraelPhoneAreaCodeAttributeOptionID)
		{
			switch (IsraelPhoneAreaCodeAttributeOptionID)
			{
				case 1: return " geoenstate:RID3484016 geoenstate:RID3484017"; 
					//1	02
					// 3484016	Yehuda & Shomron
					// 3484017	Jerusalem & surroundings
				case 2: return " geoenstate:RID3484012"; 
					//2	03
					//3484012 ;//	Merkaz
				case 3:return " geoenstate:RID3484024"; 
					//3	04
					//3484024	North
				case 4:return " geoenstate:RID3484025 geoenstate:RID3484027"; 
					//4	08
					//3484025	Darom
					//3484027	Eilat / Arava
				case 5:return " geoenstate:RID3484014"; 
					//5	09
					// 3484014	Hasharon& Shfelat Ha Hof
			}

			throw new Exception("Can't map areacode from attributeoptionid " + IsraelPhoneAreaCodeAttributeOptionID.ToString());
		}
		
	}
}
