using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;

using System.Runtime.Serialization.Formatters.Binary ;

using Matchnet.FAST.Query.BusinessLogic;
using Matchnet.FAST.Query.Interfaces;
using Matchnet.FAST.Query.ValueObjects;

using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;


namespace Matchnet.FAST.Query.TestHarness
{
	/// <summary>
	/// Summary description for LoadTester.
	/// </summary>
	public class LoadTester
	{

		private static IMatchnetQuery [] _Queries = null;
		public bool IsRunnable = false;
		public string ID = "anonymous";
		public double TestWallTime = 0;
		public int Itterations;

		public double SumWallTime = 0;
		public double AvgWallTime = 0 ;
		public double AvgWallTime_SkipMax = 0;
		public double MaxWallTime = double.MinValue;
		public double MinWallTime = double.MaxValue;

		public const int QUERY_COUNT = 6000;
		private const string CONNECTION_STRING = "SERVER=CLSQL74;DATABASE=mnAlert;UID=webuser;PWD=dbaccess";

		public LoadTester()
		{
			if (_Queries == null){
				lock (this)
				{
					if (_Queries == null) 
					{
						_Queries = GetQueries();
					}
				}
			}
		}


		public void RunTest()
		{
//			QuerySM sm = new QuerySM();
//			IsRunnable = true;
//			Itterations = 0;
//			Random rnd = new Random();
//
//			Trace.WriteLine("Got Queries @" + DateTime.Now.ToString());
//
//			DateTime fulltest1 = DateTime.Now;
//			for ( int i = rnd.Next(_Queries.Length) ; IsRunnable ; i = i++ % _Queries.Length, Itterations++)
//			{
//				DateTime query1 = DateTime.Now;
//				IMatchnetQueryResults r = sm.Search(_Queries[i]);
//				DateTime query2 = DateTime.Now;
//				double last = query2.Subtract(query1).TotalMilliseconds;
//
//				SumWallTime += last;
//				MinWallTime = Math.Min(last,MinWallTime);
//				MaxWallTime = Math.Max(last,MaxWallTime);		
//			}
//			DateTime fulltest2 = DateTime.Now;
//			TestWallTime = fulltest2.Subtract(fulltest1).TotalMilliseconds;
//			AvgWallTime = SumWallTime / Itterations;
//			AvgWallTime_SkipMax = (SumWallTime - MaxWallTime)/(Itterations -1);
//			Trace.WriteLine("Done with queries @" + DateTime.Now.ToString());
		}


		public void StopTest()
		{
			IsRunnable = false;
		}
		
		public static bool SaveQueries(string fileName){
			DataTable dt = GetSearchPreference(CONNECTION_STRING);
			if (dt.Rows == null || dt.Rows.Count == 0)
			{
				return false;
			}

			IMatchnetQuery [] queries = GetQueries(dt);

			FileStream fs = new FileStream(fileName,FileMode.Append);
			BinaryFormatter formatter = new BinaryFormatter();

			try 
			{
				formatter.Serialize(fs, queries);
				return true;
			}
			catch 
			{
				return false;
			}
			finally 
			{
				fs.Close();
			}

		}


		public static IMatchnetQuery [] GetQueries()
		{
			DataTable dt = GetSearchPreference(CONNECTION_STRING);
			if (dt.Rows == null || dt.Rows.Count == 0)
			{
				return new IMatchnetQuery [] {};
			}

			IMatchnetQuery [] queries = GetQueries(dt);
			return queries;		
		}


		private static DataTable GetSearchPreference(string connectionString) 
		{
			//			SqlConnection conn = new SqlConnection("SERVER=CLCollector01;DATABASE=AnalysisReporting;UID=webuser;PWD=dbaccess");

			SqlConnection conn  = new SqlConnection(connectionString);
			SqlCommand command = new SqlCommand(@"SELECT TOP " + QUERY_COUNT.ToString() + @"
													[AreaCode1] as [AreaCode1],
													[AreaCode2] as [AreaCode2],
													[AreaCode3] as [AreaCode3],
													[AreaCode4] as [AreaCode4],
													[AreaCode5] as [AreaCode5],
													[AreaCode6] as [AreaCode6],
													[BodyType] as [BodyType],
													[CountryRegionID] as [RegionIDCountry],
													[GroupID] as [DomainID],
													[DrinkingHabits] as [DrinkingHabits],
													[EducationLevel] as [EducationLevel],
													[Ethnicity] as [Ethnicity],
													[GenderMask] as [GenderMask],
													[HasPhotoFlag] as [HasPhotoFlag],
													[JDateEthnicity] as [JDateEthnicity],
													[JDateReligion] as [JDateReligion],
													[KeepKosher] as [KeepKosher],
													[LanguageMask] as [LanguageMask],
													[MajorType] as [MajorType],
													[MaritalStatus] as [MaritalStatus],
													[MaxAge] as [AgeMax],
													[MaxHeight] as [HeightMax],
													[MinAge] as [AgeMin],
													[MinHeight] as [HeightMin],
													sp.[RegionID] as [RegionID],
													[RelationshipMask] as [RelationshipMask],
													[RelationshipStatus] as [RelationshipStatus],
													[Religion] as [Religion],
													[SchoolID] as [SchoolID],
													[SexualIdentityType] as [SexualIdentityType],
													[SmokingHabits] as [SmokingHabits],
													[SynagogueAttendance] as [SynagogueAttendance],
													[Zodiac] as [Zodiac],
													r.[Longitude],
													r.[Latitude]
												FROM [SearchPreference] sp join mnRegion.dbo.Region r on sp.regionid = r.regionid
												WHERE GroupID = 3 and memberid % 13 = 0", conn);

			DataTable table = new DataTable();
			SqlDataAdapter da = new SqlDataAdapter(command);

			try
			{
				conn.Open();
				da.Fill(table);
			}
			finally
			{
				conn.Close();
			}
			
			return table;
		}



		private static IMatchnetQuery [] GetQueries(DataTable storedPrefs)
		{
			//TODO: Add support for [Distance] [SearchOrderBy] [SearchTypeID]
		

			IMatchnetQuery [] queries = new IMatchnetQuery [storedPrefs.Rows.Count];
				
			for (int rownum = 0; rownum < storedPrefs.Rows.Count; rownum ++)
			{
				DataRow row = storedPrefs.Rows[rownum];
				MatchnetQuery mq = new MatchnetQuery();
				for (int colnum = 0; colnum < storedPrefs.Columns.Count; colnum++)
				{
					object val = row[colnum];
					if (val != DBNull.Value)
					{								
						if (val is Int32 && (int)val < 1) 
						{
							continue;
						}
						else 
						{
							try 
							{
								QuerySearchParam param;
								if (storedPrefs.Columns[colnum].ColumnName.StartsWith("AreaCode"))
								{
									param = (QuerySearchParam)Enum.Parse(typeof(QuerySearchParam), "AreaCode", true);
								}
								else 
								{
									param = (QuerySearchParam)Enum.Parse(typeof(QuerySearchParam), storedPrefs.Columns[colnum].ColumnName, true);
								}
								mq.AddParameter(new MatchnetQueryParameter(param,row[colnum]));
							}
							catch (Exception ex)
							{
								Trace.WriteLine(ex.Message);
							}
						}
					}
				}
				mq.SearchType = SearchType.MatchMail;
				queries[rownum] = mq;						
			}

			return queries;
		}


		public override string ToString()
		{
			return string.Format("Thread\tTestWallTime\tItterations\tSumWallTime\tAvgWallTime\tMaxWallTime\tMinWallTime\tAvgWallTime_SkipMax\n{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}",
								ID , TestWallTime,Itterations,SumWallTime,AvgWallTime,MaxWallTime,MinWallTime,AvgWallTime_SkipMax);
		}
			
	}

}

