using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;
using System.Web;
using System.Diagnostics;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Proxies;
using System.Configuration;
using System.Net;
using System.Threading;


using no.fast.ds.search;

using Matchnet.Configuration.ServiceAdapters;

using Matchnet.Data;

using Matchnet.FAST.Push.ValueObjects;

using Matchnet.FAST.Query.BusinessLogic;
using Matchnet.FAST.Query.Interfaces;
using Matchnet.FAST.Query.ValueObjects;

using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;




namespace Matchnet.FAST.Query.TestHarness 
{
	/// <summary>
	/// Summary description for QueryTestHarness.
	/// </summary>
	public class QueryTestHarness : System.Windows.Forms.Form 
	{
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.RichTextBox txtOutput;
		private System.Windows.Forms.TabPage tpTransform;
		private System.Windows.Forms.Button btnTransformDoTransform;
		private System.Windows.Forms.TextBox txtTransformMatchnetQuery;
		private System.Windows.Forms.TabPage tpQuery;
		private System.Windows.Forms.Button btnQueryRun;
		private System.Windows.Forms.TextBox txtQueryMatchnetQuery;
		private System.Windows.Forms.TextBox txtQueryFASTQuery;
		private System.Windows.Forms.Button btnTransformTranslateSearchPref;
		private System.Windows.Forms.Button btnSMRunQuery;
		private System.Windows.Forms.Button btnSMRunQueryRemote;
		private System.Windows.Forms.Button btnSMAddSearchServer;
		private System.Windows.Forms.Button btnVOInstallCounters;
		private System.Windows.Forms.Button btnVOUninstallCounters;
		private System.Windows.Forms.TabPage tpValueObjects;
		private System.Windows.Forms.Button btnQueryGeoBoxMake;
		private System.Windows.Forms.Button btnVOGetConfigs;
		private System.Windows.Forms.TabPage tpFQD;
		private System.Windows.Forms.TextBox txtFQDFastQueryTerms;
		private System.Windows.Forms.TextBox txtFQDFastFilterTerms;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox cbFQDSortTerms;
		private System.Windows.Forms.Button btnFQDRunRawQuery;
		private System.Windows.Forms.Button btnTransformExpandBitmask;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private const string APPSETTING_FASTQUERYSVC_SMURL = "FASTQueryServiceManagerURL";
		private const string SETTING_FASTPUSHSVC_COMMUNITY_LIST = "FASTPUSHSVC_COMMUNITY_LIST";
		private System.Windows.Forms.Button btnQueryMatchMailRun;
		private System.Windows.Forms.Button btnSerializationTest;
		private System.Windows.Forms.Button btnFQDMatchMailSearch;
		private System.Windows.Forms.TabPage tpLoadTest;
		private System.Windows.Forms.Button btnLTDumpQueriesToFile;
		private System.Windows.Forms.Button btnLTQueryLoadTes1;
		private const string SETTING_FASTPUSHSVC_QUEUE_NAME = "FASTPUSHSVC_QUEUE_NAME";

		private const int LOAD_TEST_OBJ = 0;
		private const int LOAD_TEST_THREAD = 1;
		private System.Windows.Forms.TrackBar tkbLT_NumThreads;
		private System.Windows.Forms.Label lblLT_NumThreads;
		private System.Windows.Forms.NumericUpDown numLT_TestDuration;
		private object [,] _LoadTesters;

		private static double _Long = 0.0;
		private static double _Lat = 0.0;

		public QueryTestHarness() 
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing ) 
		{
			if( disposing ) 
			{
				if(components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new QueryTestHarness());
		}
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() 
		{
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tpTransform = new System.Windows.Forms.TabPage();
			this.btnTransformExpandBitmask = new System.Windows.Forms.Button();
			this.btnQueryGeoBoxMake = new System.Windows.Forms.Button();
			this.btnTransformTranslateSearchPref = new System.Windows.Forms.Button();
			this.txtTransformMatchnetQuery = new System.Windows.Forms.TextBox();
			this.btnTransformDoTransform = new System.Windows.Forms.Button();
			this.tpQuery = new System.Windows.Forms.TabPage();
			this.btnQueryMatchMailRun = new System.Windows.Forms.Button();
			this.txtQueryFASTQuery = new System.Windows.Forms.TextBox();
			this.txtQueryMatchnetQuery = new System.Windows.Forms.TextBox();
			this.btnQueryRun = new System.Windows.Forms.Button();
			this.btnSMRunQuery = new System.Windows.Forms.Button();
			this.btnSMRunQueryRemote = new System.Windows.Forms.Button();
			this.btnSMAddSearchServer = new System.Windows.Forms.Button();
			this.tpValueObjects = new System.Windows.Forms.TabPage();
			this.btnSerializationTest = new System.Windows.Forms.Button();
			this.btnVOGetConfigs = new System.Windows.Forms.Button();
			this.btnVOUninstallCounters = new System.Windows.Forms.Button();
			this.btnVOInstallCounters = new System.Windows.Forms.Button();
			this.tpFQD = new System.Windows.Forms.TabPage();
			this.btnFQDMatchMailSearch = new System.Windows.Forms.Button();
			this.cbFQDSortTerms = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.txtFQDFastFilterTerms = new System.Windows.Forms.TextBox();
			this.txtFQDFastQueryTerms = new System.Windows.Forms.TextBox();
			this.btnFQDRunRawQuery = new System.Windows.Forms.Button();
			this.tpLoadTest = new System.Windows.Forms.TabPage();
			this.btnLTQueryLoadTes1 = new System.Windows.Forms.Button();
			this.btnLTDumpQueriesToFile = new System.Windows.Forms.Button();
			this.txtOutput = new System.Windows.Forms.RichTextBox();
			this.tkbLT_NumThreads = new System.Windows.Forms.TrackBar();
			this.lblLT_NumThreads = new System.Windows.Forms.Label();
			this.numLT_TestDuration = new System.Windows.Forms.NumericUpDown();
			this.tabControl1.SuspendLayout();
			this.tpTransform.SuspendLayout();
			this.tpQuery.SuspendLayout();
			this.tpValueObjects.SuspendLayout();
			this.tpFQD.SuspendLayout();
			this.tpLoadTest.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tkbLT_NumThreads)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numLT_TestDuration)).BeginInit();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tpTransform);
			this.tabControl1.Controls.Add(this.tpQuery);
			this.tabControl1.Controls.Add(this.tpValueObjects);
			this.tabControl1.Controls.Add(this.tpFQD);
			this.tabControl1.Controls.Add(this.tpLoadTest);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(768, 168);
			this.tabControl1.TabIndex = 0;
			// 
			// tpTransform
			// 
			this.tpTransform.Controls.Add(this.btnTransformExpandBitmask);
			this.tpTransform.Controls.Add(this.btnQueryGeoBoxMake);
			this.tpTransform.Controls.Add(this.btnTransformTranslateSearchPref);
			this.tpTransform.Controls.Add(this.txtTransformMatchnetQuery);
			this.tpTransform.Controls.Add(this.btnTransformDoTransform);
			this.tpTransform.Location = new System.Drawing.Point(4, 22);
			this.tpTransform.Name = "tpTransform";
			this.tpTransform.Size = new System.Drawing.Size(760, 142);
			this.tpTransform.TabIndex = 0;
			this.tpTransform.Text = "Transform";
			// 
			// btnTransformExpandBitmask
			// 
			this.btnTransformExpandBitmask.Location = new System.Drawing.Point(608, 32);
			this.btnTransformExpandBitmask.Name = "btnTransformExpandBitmask";
			this.btnTransformExpandBitmask.Size = new System.Drawing.Size(104, 23);
			this.btnTransformExpandBitmask.TabIndex = 6;
			this.btnTransformExpandBitmask.Text = "Expand Bitmask";
			this.btnTransformExpandBitmask.Click += new System.EventHandler(this.btnTransformExpandBitmask_Click);
			// 
			// btnQueryGeoBoxMake
			// 
			this.btnQueryGeoBoxMake.Location = new System.Drawing.Point(264, 8);
			this.btnQueryGeoBoxMake.Name = "btnQueryGeoBoxMake";
			this.btnQueryGeoBoxMake.Size = new System.Drawing.Size(144, 23);
			this.btnQueryGeoBoxMake.TabIndex = 5;
			this.btnQueryGeoBoxMake.Text = "Make Geo Boxes";
			this.btnQueryGeoBoxMake.Click += new System.EventHandler(this.btnQueryGeoBoxMake_Click);
			// 
			// btnTransformTranslateSearchPref
			// 
			this.btnTransformTranslateSearchPref.Location = new System.Drawing.Point(520, 120);
			this.btnTransformTranslateSearchPref.Name = "btnTransformTranslateSearchPref";
			this.btnTransformTranslateSearchPref.Size = new System.Drawing.Size(144, 23);
			this.btnTransformTranslateSearchPref.TabIndex = 2;
			this.btnTransformTranslateSearchPref.Text = "Txform Search Prefs";
			this.btnTransformTranslateSearchPref.Click += new System.EventHandler(this.btnTransformTranslateSearchPref_Click);
			// 
			// txtTransformMatchnetQuery
			// 
			this.txtTransformMatchnetQuery.Location = new System.Drawing.Point(8, 40);
			this.txtTransformMatchnetQuery.Multiline = true;
			this.txtTransformMatchnetQuery.Name = "txtTransformMatchnetQuery";
			this.txtTransformMatchnetQuery.Size = new System.Drawing.Size(400, 64);
			this.txtTransformMatchnetQuery.TabIndex = 1;
			this.txtTransformMatchnetQuery.Text = "gendermask=6 agemin=18 agemax=24 domainid=3 longitude=-2.0676779747 latitude=0.59" +
				"3572020531 sorting=1 regionidcity=3404943 zipcode=90405 geodistance=3 hasphotofl" +
				"ag=1";
			// 
			// btnTransformDoTransform
			// 
			this.btnTransformDoTransform.Location = new System.Drawing.Point(8, 8);
			this.btnTransformDoTransform.Name = "btnTransformDoTransform";
			this.btnTransformDoTransform.Size = new System.Drawing.Size(120, 23);
			this.btnTransformDoTransform.TabIndex = 0;
			this.btnTransformDoTransform.Text = "Transform";
			this.btnTransformDoTransform.Click += new System.EventHandler(this.btnTransformDoTransform_Click);
			// 
			// tpQuery
			// 
			this.tpQuery.Controls.Add(this.btnQueryMatchMailRun);
			this.tpQuery.Controls.Add(this.txtQueryFASTQuery);
			this.tpQuery.Controls.Add(this.txtQueryMatchnetQuery);
			this.tpQuery.Controls.Add(this.btnQueryRun);
			this.tpQuery.Controls.Add(this.btnSMRunQuery);
			this.tpQuery.Controls.Add(this.btnSMRunQueryRemote);
			this.tpQuery.Controls.Add(this.btnSMAddSearchServer);
			this.tpQuery.Location = new System.Drawing.Point(4, 22);
			this.tpQuery.Name = "tpQuery";
			this.tpQuery.Size = new System.Drawing.Size(760, 142);
			this.tpQuery.TabIndex = 1;
			this.tpQuery.Text = "Query and SM";
			// 
			// btnQueryMatchMailRun
			// 
			this.btnQueryMatchMailRun.Location = new System.Drawing.Point(280, 32);
			this.btnQueryMatchMailRun.Name = "btnQueryMatchMailRun";
			this.btnQueryMatchMailRun.Size = new System.Drawing.Size(152, 23);
			this.btnQueryMatchMailRun.TabIndex = 4;
			this.btnQueryMatchMailRun.Text = "Run MatchMail Query";
			this.btnQueryMatchMailRun.Click += new System.EventHandler(this.btnQueryMatchMailRun_Click);
			// 
			// txtQueryFASTQuery
			// 
			this.txtQueryFASTQuery.Location = new System.Drawing.Point(0, 80);
			this.txtQueryFASTQuery.Multiline = true;
			this.txtQueryFASTQuery.Name = "txtQueryFASTQuery";
			this.txtQueryFASTQuery.Size = new System.Drawing.Size(760, 64);
			this.txtQueryFASTQuery.TabIndex = 3;
			this.txtQueryFASTQuery.Text = "";
			// 
			// txtQueryMatchnetQuery
			// 
			this.txtQueryMatchnetQuery.Location = new System.Drawing.Point(440, 0);
			this.txtQueryMatchnetQuery.Multiline = true;
			this.txtQueryMatchnetQuery.Name = "txtQueryMatchnetQuery";
			this.txtQueryMatchnetQuery.Size = new System.Drawing.Size(320, 80);
			this.txtQueryMatchnetQuery.TabIndex = 2;
			this.txtQueryMatchnetQuery.Text = "gendermask=6 agemin=18 agemax=24 domainid=3 longitude=-2.0676779747 latitude=0.59" +
				"3572020531 sorting=1 regionidcity=3404943 zipcode=90405 geodistance=3 hasphotofl" +
				"ag=1";
			// 
			// btnQueryRun
			// 
			this.btnQueryRun.Location = new System.Drawing.Point(8, 8);
			this.btnQueryRun.Name = "btnQueryRun";
			this.btnQueryRun.TabIndex = 0;
			this.btnQueryRun.Text = "RunQuery";
			this.btnQueryRun.Click += new System.EventHandler(this.btnQueryRun_Click);
			// 
			// btnSMRunQuery
			// 
			this.btnSMRunQuery.Location = new System.Drawing.Point(96, 8);
			this.btnSMRunQuery.Name = "btnSMRunQuery";
			this.btnSMRunQuery.Size = new System.Drawing.Size(184, 23);
			this.btnSMRunQuery.TabIndex = 0;
			this.btnSMRunQuery.Text = "Run SM Query (direct)";
			this.btnSMRunQuery.Click += new System.EventHandler(this.btnSMRunQuery_Click);
			// 
			// btnSMRunQueryRemote
			// 
			this.btnSMRunQueryRemote.Location = new System.Drawing.Point(96, 32);
			this.btnSMRunQueryRemote.Name = "btnSMRunQueryRemote";
			this.btnSMRunQueryRemote.Size = new System.Drawing.Size(184, 23);
			this.btnSMRunQueryRemote.TabIndex = 1;
			this.btnSMRunQueryRemote.Text = "Run SM Query (Remoting)";
			this.btnSMRunQueryRemote.Click += new System.EventHandler(this.btnSMRunQueryRemote_Click);
			// 
			// btnSMAddSearchServer
			// 
			this.btnSMAddSearchServer.Enabled = false;
			this.btnSMAddSearchServer.Location = new System.Drawing.Point(280, 8);
			this.btnSMAddSearchServer.Name = "btnSMAddSearchServer";
			this.btnSMAddSearchServer.Size = new System.Drawing.Size(152, 23);
			this.btnSMAddSearchServer.TabIndex = 2;
			this.btnSMAddSearchServer.Text = "Add Search Server";
			this.btnSMAddSearchServer.Click += new System.EventHandler(this.btnSMAddSearchServer_Click);
			// 
			// tpValueObjects
			// 
			this.tpValueObjects.Controls.Add(this.btnSerializationTest);
			this.tpValueObjects.Controls.Add(this.btnVOGetConfigs);
			this.tpValueObjects.Controls.Add(this.btnVOUninstallCounters);
			this.tpValueObjects.Controls.Add(this.btnVOInstallCounters);
			this.tpValueObjects.Location = new System.Drawing.Point(4, 22);
			this.tpValueObjects.Name = "tpValueObjects";
			this.tpValueObjects.Size = new System.Drawing.Size(760, 142);
			this.tpValueObjects.TabIndex = 3;
			this.tpValueObjects.Text = "ValueObjects";
			// 
			// btnSerializationTest
			// 
			this.btnSerializationTest.Location = new System.Drawing.Point(584, 16);
			this.btnSerializationTest.Name = "btnSerializationTest";
			this.btnSerializationTest.Size = new System.Drawing.Size(120, 23);
			this.btnSerializationTest.TabIndex = 3;
			this.btnSerializationTest.Text = "Serializeation Test";
			this.btnSerializationTest.Click += new System.EventHandler(this.button1_Click);
			// 
			// btnVOGetConfigs
			// 
			this.btnVOGetConfigs.Location = new System.Drawing.Point(344, 16);
			this.btnVOGetConfigs.Name = "btnVOGetConfigs";
			this.btnVOGetConfigs.Size = new System.Drawing.Size(144, 23);
			this.btnVOGetConfigs.TabIndex = 2;
			this.btnVOGetConfigs.Text = "Get Configs";
			this.btnVOGetConfigs.Click += new System.EventHandler(this.btnVOGetConfigs_Click);
			// 
			// btnVOUninstallCounters
			// 
			this.btnVOUninstallCounters.Location = new System.Drawing.Point(184, 16);
			this.btnVOUninstallCounters.Name = "btnVOUninstallCounters";
			this.btnVOUninstallCounters.Size = new System.Drawing.Size(144, 23);
			this.btnVOUninstallCounters.TabIndex = 1;
			this.btnVOUninstallCounters.Text = "Uninstall Counters";
			this.btnVOUninstallCounters.Click += new System.EventHandler(this.btnVOUninstallCounters_Click);
			// 
			// btnVOInstallCounters
			// 
			this.btnVOInstallCounters.Location = new System.Drawing.Point(24, 16);
			this.btnVOInstallCounters.Name = "btnVOInstallCounters";
			this.btnVOInstallCounters.Size = new System.Drawing.Size(144, 23);
			this.btnVOInstallCounters.TabIndex = 0;
			this.btnVOInstallCounters.Text = "Install Counters";
			this.btnVOInstallCounters.Click += new System.EventHandler(this.btnVOInstallCounters_Click);
			// 
			// tpFQD
			// 
			this.tpFQD.Controls.Add(this.btnFQDMatchMailSearch);
			this.tpFQD.Controls.Add(this.cbFQDSortTerms);
			this.tpFQD.Controls.Add(this.label3);
			this.tpFQD.Controls.Add(this.label2);
			this.tpFQD.Controls.Add(this.label1);
			this.tpFQD.Controls.Add(this.txtFQDFastFilterTerms);
			this.tpFQD.Controls.Add(this.txtFQDFastQueryTerms);
			this.tpFQD.Controls.Add(this.btnFQDRunRawQuery);
			this.tpFQD.Location = new System.Drawing.Point(4, 22);
			this.tpFQD.Name = "tpFQD";
			this.tpFQD.Size = new System.Drawing.Size(760, 142);
			this.tpFQD.TabIndex = 4;
			this.tpFQD.Text = "FAST Query Direct";
			// 
			// btnFQDMatchMailSearch
			// 
			this.btnFQDMatchMailSearch.Location = new System.Drawing.Point(344, 112);
			this.btnFQDMatchMailSearch.Name = "btnFQDMatchMailSearch";
			this.btnFQDMatchMailSearch.Size = new System.Drawing.Size(128, 23);
			this.btnFQDMatchMailSearch.TabIndex = 8;
			this.btnFQDMatchMailSearch.Text = "Matchmail Raw Test";
			this.btnFQDMatchMailSearch.Click += new System.EventHandler(this.btnFQDMatchMailSearch_Click);
			// 
			// cbFQDSortTerms
			// 
			this.cbFQDSortTerms.AllowDrop = true;
			this.cbFQDSortTerms.Items.AddRange(new object[] {
																"default",
																"lastlogondate",
																"memberregistrationdate",
																"popularityrank"});
			this.cbFQDSortTerms.Location = new System.Drawing.Point(112, 80);
			this.cbFQDSortTerms.Name = "cbFQDSortTerms";
			this.cbFQDSortTerms.Size = new System.Drawing.Size(376, 21);
			this.cbFQDSortTerms.Sorted = true;
			this.cbFQDSortTerms.TabIndex = 7;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(16, 80);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(88, 16);
			this.label3.TabIndex = 6;
			this.label3.Text = "Sorting terms";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 40);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(88, 16);
			this.label2.TabIndex = 5;
			this.label2.Text = "Filter terms";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(88, 16);
			this.label1.TabIndex = 4;
			this.label1.Text = "Query terms";
			// 
			// txtFQDFastFilterTerms
			// 
			this.txtFQDFastFilterTerms.Location = new System.Drawing.Point(112, 40);
			this.txtFQDFastFilterTerms.Multiline = true;
			this.txtFQDFastFilterTerms.Name = "txtFQDFastFilterTerms";
			this.txtFQDFastFilterTerms.Size = new System.Drawing.Size(648, 40);
			this.txtFQDFastFilterTerms.TabIndex = 2;
			this.txtFQDFastFilterTerms.Text = "";
			// 
			// txtFQDFastQueryTerms
			// 
			this.txtFQDFastQueryTerms.Location = new System.Drawing.Point(112, 0);
			this.txtFQDFastQueryTerms.Multiline = true;
			this.txtFQDFastQueryTerms.Name = "txtFQDFastQueryTerms";
			this.txtFQDFastQueryTerms.Size = new System.Drawing.Size(648, 40);
			this.txtFQDFastQueryTerms.TabIndex = 1;
			this.txtFQDFastQueryTerms.Text = "";
			// 
			// btnFQDRunRawQuery
			// 
			this.btnFQDRunRawQuery.Location = new System.Drawing.Point(112, 112);
			this.btnFQDRunRawQuery.Name = "btnFQDRunRawQuery";
			this.btnFQDRunRawQuery.Size = new System.Drawing.Size(144, 23);
			this.btnFQDRunRawQuery.TabIndex = 0;
			this.btnFQDRunRawQuery.Text = "Run raw query";
			this.btnFQDRunRawQuery.Click += new System.EventHandler(this.btnFQDRunRawQuery_Click);
			// 
			// tpLoadTest
			// 
			this.tpLoadTest.Controls.Add(this.numLT_TestDuration);
			this.tpLoadTest.Controls.Add(this.lblLT_NumThreads);
			this.tpLoadTest.Controls.Add(this.tkbLT_NumThreads);
			this.tpLoadTest.Controls.Add(this.btnLTQueryLoadTes1);
			this.tpLoadTest.Controls.Add(this.btnLTDumpQueriesToFile);
			this.tpLoadTest.Location = new System.Drawing.Point(4, 22);
			this.tpLoadTest.Name = "tpLoadTest";
			this.tpLoadTest.Size = new System.Drawing.Size(760, 142);
			this.tpLoadTest.TabIndex = 5;
			this.tpLoadTest.Text = "Load Tests";
			// 
			// btnLTQueryLoadTes1
			// 
			this.btnLTQueryLoadTes1.Location = new System.Drawing.Point(152, 8);
			this.btnLTQueryLoadTes1.Name = "btnLTQueryLoadTes1";
			this.btnLTQueryLoadTes1.Size = new System.Drawing.Size(128, 23);
			this.btnLTQueryLoadTes1.TabIndex = 1;
			this.btnLTQueryLoadTes1.Text = "Load Test Query BL";
			this.btnLTQueryLoadTes1.Click += new System.EventHandler(this.btnLTQueryLoadTes1_Click);
			// 
			// btnLTDumpQueriesToFile
			// 
			this.btnLTDumpQueriesToFile.Location = new System.Drawing.Point(8, 8);
			this.btnLTDumpQueriesToFile.Name = "btnLTDumpQueriesToFile";
			this.btnLTDumpQueriesToFile.Size = new System.Drawing.Size(128, 23);
			this.btnLTDumpQueriesToFile.TabIndex = 0;
			this.btnLTDumpQueriesToFile.Text = "Dump Queries To File";
			this.btnLTDumpQueriesToFile.Click += new System.EventHandler(this.btnLTDumpQueriesToFile_Click);
			// 
			// txtOutput
			// 
			this.txtOutput.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtOutput.Location = new System.Drawing.Point(0, 168);
			this.txtOutput.Name = "txtOutput";
			this.txtOutput.Size = new System.Drawing.Size(768, 341);
			this.txtOutput.TabIndex = 1;
			this.txtOutput.Text = "";
			this.txtOutput.LinkClicked += new System.Windows.Forms.LinkClickedEventHandler(this.txtOutput_LinkClicked);
			// 
			// tkbLT_NumThreads
			// 
			this.tkbLT_NumThreads.LargeChange = 4;
			this.tkbLT_NumThreads.Location = new System.Drawing.Point(296, 8);
			this.tkbLT_NumThreads.Maximum = 32;
			this.tkbLT_NumThreads.Minimum = 1;
			this.tkbLT_NumThreads.Name = "tkbLT_NumThreads";
			this.tkbLT_NumThreads.Size = new System.Drawing.Size(216, 42);
			this.tkbLT_NumThreads.TabIndex = 2;
			this.tkbLT_NumThreads.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
			this.tkbLT_NumThreads.Value = 1;
			this.tkbLT_NumThreads.ValueChanged += new System.EventHandler(this.tkbLT_NumThreads_ValueChanged);
			// 
			// lblLT_NumThreads
			// 
			this.lblLT_NumThreads.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblLT_NumThreads.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblLT_NumThreads.Location = new System.Drawing.Point(512, 8);
			this.lblLT_NumThreads.Name = "lblLT_NumThreads";
			this.lblLT_NumThreads.Size = new System.Drawing.Size(48, 42);
			this.lblLT_NumThreads.TabIndex = 3;
			this.lblLT_NumThreads.Text = "1";
			this.lblLT_NumThreads.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.lblLT_NumThreads.UseMnemonic = false;
			// 
			// numLT_TestDuration
			// 
			this.numLT_TestDuration.AccessibleDescription = "Test duration (seconds)";
			this.numLT_TestDuration.AccessibleName = "TestDuration";
			this.numLT_TestDuration.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolTip;
			this.numLT_TestDuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.numLT_TestDuration.Increment = new System.Decimal(new int[] {
																				 10,
																				 0,
																				 0,
																				 0});
			this.numLT_TestDuration.Location = new System.Drawing.Point(568, 16);
			this.numLT_TestDuration.Maximum = new System.Decimal(new int[] {
																			   600,
																			   0,
																			   0,
																			   0});
			this.numLT_TestDuration.Minimum = new System.Decimal(new int[] {
																			   10,
																			   0,
																			   0,
																			   0});
			this.numLT_TestDuration.Name = "numLT_TestDuration";
			this.numLT_TestDuration.Size = new System.Drawing.Size(56, 29);
			this.numLT_TestDuration.TabIndex = 4;
			this.numLT_TestDuration.Value = new System.Decimal(new int[] {
																			 10,
																			 0,
																			 0,
																			 0});
			// 
			// QueryTestHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(768, 509);
			this.Controls.Add(this.txtOutput);
			this.Controls.Add(this.tabControl1);
			this.Name = "QueryTestHarness";
			this.Text = "QueryTestHarness";
			this.tabControl1.ResumeLayout(false);
			this.tpTransform.ResumeLayout(false);
			this.tpQuery.ResumeLayout(false);
			this.tpValueObjects.ResumeLayout(false);
			this.tpFQD.ResumeLayout(false);
			this.tpLoadTest.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tkbLT_NumThreads)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numLT_TestDuration)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion


		private MatchnetQuery ParseNVString(string rawquery)
		{
			string [] nvpairs = rawquery.Split(' ');
			string [] nv;

			MatchnetQuery qry = new MatchnetQuery();

			foreach (string nvpair in nvpairs)
			{
				try
				{
					nv = nvpair.Split('=');
					string name = nv[0];
					string val = nv[1];
					qry.AddParameter(new MatchnetQueryParameter( (QuerySearchParam)Enum.Parse(typeof(QuerySearchParam),name,true),val));

				}
				catch (Exception ex)
				{
					txtOutput.AppendText("\nIgnored term -> " + nvpair + " Exception:"  + ex.Message);
				}			
			}
			return qry;
		}


		private void btnTransformDoTransform_Click(object sender, System.EventArgs e) 
		{
			MatchnetQuery qry = ParseNVString(txtTransformMatchnetQuery.Text);
			txtOutput.AppendText("\n" + QueryTransformer.Instance.Transform(qry).ToString());
		}


		private void btnQueryRun_Click(object sender, System.EventArgs e) 
		{
			MatchnetQuery qry = ParseNVString(txtQueryMatchnetQuery.Text);
			qry.SearchType = Matchnet.Search.Interfaces.SearchType.Default; // Default.

			no.fast.ds.search.IQuery FASTQuery = QueryTransformer.Instance.Transform(qry);
			txtQueryFASTQuery.Text = FASTQuery.ToString();

			RenderResults(FASTQuery);
			//			IMatchnetQueryResults res = QueryBL.Instance.RunQuery(FASTQuery);
			//			RenderResults(res);

		}

		private void btnQueryMatchMailRun_Click(object sender, System.EventArgs e) 
		{
			MatchnetQuery qry = ParseNVString(txtQueryMatchnetQuery.Text);
			qry.SearchType = Matchnet.Search.Interfaces.SearchType.MatchMail;
			no.fast.ds.search.IQuery FASTQuery = QueryTransformer.Instance.Transform(qry);
			txtQueryFASTQuery.Text = FASTQuery.ToString();
			
			IMatchnetQueryResults results = QueryBL.Instance.RunQuery(qry);

			RenderResults(results, Matchnet.Search.Interfaces.SearchType.MatchMail);
		
		}



		private void RenderResults( IMatchnetQueryResults results, Matchnet.Search.Interfaces.SearchType searchtype)
		{
			txtOutput.AppendText(string.Format("Found {0}, returning {1}\n",results.MatchesFound , results.MatchesReturned));
			if (searchtype == Matchnet.Search.Interfaces.SearchType.WebSearch)
			{
				foreach (IMatchnetResultItem item in results.Items.List)
				{
					txtOutput.AppendText(string.Format("\nhttp://www.jdate.com/default.asp?p=7070&PageType=4&MemberID={0}",item.MemberID));			
				}
			}
			if (searchtype == Matchnet.Search.Interfaces.SearchType.MatchMail)
			{
				foreach (IMatchnetResultItem item in results.Items.List)
				{
					txtOutput.AppendText(string.Format("\n{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t",
						item.MemberID,
						item["username"],
						item["birthdate"],
						item["regionid"],
						item["gendermask"],
						item["aboutme"],
						item["headline"],
						item["thumbpath"]));
				}
			
			}
		}


		private void RenderResults(IQuery FASTQuery)
		{
			IFastSearchEngine engine = SearchEngineFactory.Instance.GetSearchEngine();			
			IQueryResult FASTResult = engine.Search(FASTQuery);


			txtOutput.Clear();
			if ( FASTResult != null) 
			{ 	
				txtOutput.AppendText(string.Format("Found {0}, returning {1} in {2}ms\n",FASTResult.DocCount, FASTResult.Hits, FASTResult.TimeUsed));

				txtOutput.AppendText(string.Format("{0}:{1}\tD:{2:##.#####}\txy:{3}/{4}\t{5} {6}\tlgn:{7:yy-MM-dd}\treg:{8:yy-MM-dd}\tpop:{9}\tpopdate:{10}\n",
					"memberid", "rank","distance","long","lat","city","zip","logon","registration","popularity","popularityrankdate"));

				for (int i = 1; i <= FASTResult.Hits ;i++)
				{ // GetDocument(i) is 1 based !!! not zero based!!!			
					IDocumentSummary summary = FASTResult.GetDocument(i);

					if (summary != null)
					{
						double pLt = Convert.ToDouble(summary.GetSummaryField("latitude").Summary);
						double pLg = Convert.ToDouble(summary.GetSummaryField("longitude").Summary);

//						double dLg = Convert.ToDouble(summary.GetSummaryField("longitude").Summary)	- _Long ; // santa monica 90405 longitude delta
//						double dLt = Convert.ToDouble(summary.GetSummaryField("latitude").Summary)	- _Lat; // santa monica 90405 longitude delta
//						double D = Math.Sqrt( Math.Pow(dLg,2D) + Math.Pow(dLt, 2D));

//								3963.0 * arccos[sin(lat1) *  sin(lat2) + cos(lat1) * cos(lat2) * cos(lon2 - lon1)]
						double D = 3963.0 * Math.Acos( (Math.Sin(_Lat) *  Math.Sin(pLt)) + 
											(Math.Cos(_Lat) * Math.Cos(pLt) * Math.Cos(pLg - _Long)) );

						txtOutput.AppendText(string.Format("{0}:{1}\tD:{2:##.#####}\txy:{3}/{4}\t{5} {6}\tlgn:{7:yy-MM-dd}\treg:{8:yy-MM-dd}\tpop:{9}\tpopdate:{10}\n",
							summary.GetSummaryField("memberid").Summary, 
							summary.GetSummaryField("rank").Summary, 
							D,
							summary.GetSummaryField("longitude") == null ? string.Empty : summary.GetSummaryField("longitude").Summary.Substring(1,6),
							summary.GetSummaryField("latitude") == null ? string.Empty : summary.GetSummaryField("latitude").Summary.Substring(1,6), 
							summary.GetSummaryField("geoencity").Summary,
							summary.GetSummaryField("geopostalcode").Summary,
							summary.GetSummaryField("lastlogondate") == null ? string.Empty : summary.GetSummaryField("lastlogondate").Summary,
							summary.GetSummaryField("memberregistrationdate") == null ? string.Empty : summary.GetSummaryField("memberregistrationdate").Summary,
							summary.GetSummaryField("popularityrank") == null ? string.Empty : summary.GetSummaryField("popularityrank").Summary,
							summary.GetSummaryField("popularityrankdate") == null ? string.Empty : summary.GetSummaryField("popularityrankdate").Summary
							));
					}
				}
			}
			else 
			{
				txtOutput.AppendText("no results ");
			}
			
		}


		private void txtOutput_LinkClicked(object sender, System.Windows.Forms.LinkClickedEventArgs e) 
		{
			System.Diagnostics.Process.Start(e.LinkText);
		}


		private void btnSMRunQuery_Click(object sender, System.EventArgs e) 
		{
//			MatchnetQuery qry = ParseNVString(txtQueryMatchnetQuery.Text);
//			ServiceManager.QuerySM sm = new QuerySM();
//			IMatchnetQueryResults results =  sm.Search(qry);
//			RenderResults(results, Matchnet.Search.Interfaces.SearchType.WebSearch);
		}


		private void btnSMRunQueryRemote_Click(object sender, System.EventArgs e) 
		{
			MatchnetQuery qry = ParseNVString(txtQueryMatchnetQuery.Text);
			IFASTQueryService sm = (IFASTQueryService)Activator.GetObject(typeof(IMemberSearchService), ConfigurationSettings.AppSettings[APPSETTING_FASTQUERYSVC_SMURL]);
			IMatchnetQueryResults results =  sm.Search(qry);
			RenderResults(results, Matchnet.Search.Interfaces.SearchType.WebSearch);
		}


		private void btnQueryGeoBoxMake_Click(object sender, System.EventArgs e) 
		{
			IMatchnetQuery qry = ParseNVString(txtTransformMatchnetQuery.Text);
			SaveCenterCoordinates(qry);
			string [] bp = GeoBox.BestPhases(_Long,_Lat);
			txtOutput.AppendText(string.Format("\nBest phases for Long {0} Lat {1} long and lat:\n{2}\n",_Long,_Lat , string.Join("\n", bp ) ) );
		}

		private void SaveCenterCoordinates(IMatchnetQuery qry)
		{
			for (int i = 0; i < qry.Count; i++)
			{
				IMatchnetQueryParameter mqp = qry[i];
				if (mqp.Parameter == QuerySearchParam.Longitude) 
				{
					_Long = Convert.ToDouble(mqp.Value);
				}
				if (mqp.Parameter == QuerySearchParam.Latitude ) 
				{
					_Lat = Convert.ToDouble(mqp.Value);
				}
			}
		}

		/// NOTE:  CURRENTLY THE CODE TO MANAGE MULTIPLE QUERY SERVERS IS NOT USED BECAUSE WE PLAN ON
		/// HARDWARE LOAD BALANCING THE FAST QUERY SERVERS.  EVENTUALLY, THIS CODE CAN BE DELETED.
		private void btnSMAddSearchServer_Click(object sender, System.EventArgs e) 
		{
			//			SearchEngineFactory.Instance.AddSearchServer("foo");
			//			SearchEngineFactory.Instance.AddSearchServer("foo");
			//
			//			// Add Watch on se, then expand to ServerURL to see effects.
			//			for (int i = 0; i < 3; i++){
			//				IFastSearchEngine se = SearchEngineFactory.Instance.GetSearchEngine();
			//				txtOutput.AppendText(string.Format("\nWe have {0}: {1}",i, se));
			//			}
			//
			//			txtOutput.AppendText("\nNow removing foo");
			//			SearchEngineFactory.Instance.RemoveSearchServer("foo");
			//
			//			for (int i = 0; i < 3; i++){
			//				IFastSearchEngine se = SearchEngineFactory.Instance.GetSearchEngine();
			//				txtOutput.AppendText(string.Format("\nNow we have {0}: {1}",i, se));
			//			}
			System.Windows.Forms.MessageBox.Show("CURRENTLY THE CODE TO MANAGE MULTIPLE QUERY SERVERS IS NOT USED BECAUSE WE PLAN ON HARDWARE LOAD BALANCING THE FAST QUERY SERVERS.  EVENTUALLY, THIS CODE CAN BE DELETED.");
		}


		private void btnVOInstallCounters_Click(object sender, System.EventArgs e) 
		{
			MetricsInstaller.Install();
		}


		private void btnVOUninstallCounters_Click(object sender, System.EventArgs e) 
		{
			MetricsInstaller.Uninstall();
		}


		private void btnTransformTranslateSearchPref_Click(object sender, System.EventArgs e) 
		{
			SqlConnection conn = new SqlConnection("SERVER=CLCollector01;DATABASE=AnalysisReporting;UID=webuser;PWD=dbaccess");
			SqlCommand command = new SqlCommand(@"SELECT TOP 3000
													[AreaCode1] as [AreaCode1],
													[AreaCode2] as [AreaCode2],
													[AreaCode3] as [AreaCode3],
													[AreaCode4] as [AreaCode4],
													[AreaCode5] as [AreaCode5],
													[AreaCode6] as [AreaCode6],
													[BodyType] as [BodyType],
													[CountryRegionID] as [RegionIDCountry],
													[DomainID] as [DomainID],
													[DrinkingHabits] as [DrinkingHabits],
													[EducationLevel] as [EducationLevel],
													[Ethnicity] as [Ethnicity],
													[GenderMask] as [GenderMask],
													[HasPhotoFlag] as [HasPhotoFlag],
													[JDateEthnicity] as [JDateEthnicity],
													[JDateReligion] as [JDateReligion],
													[KeepKosher] as [KeepKosher],
													[LanguageMask] as [LanguageMask],
													[MajorType] as [MajorType],
													[MaritalStatus] as [MaritalStatus],
													[MaxAge] as [AgeMax],
													[MaxHeight] as [HeightMax],
													[MinAge] as [AgeMin],
													[MinHeight] as [HeightMin],
													sp.[RegionID] as [RegionID],
													[RelationshipMask] as [RelationshipMask],
													[RelationshipStatus] as [RelationshipStatus],
													[Religion] as [Religion],
													[SchoolID] as [SchoolID],
													[SexualIdentityType] as [SexualIdentityType],
													[SmokingHabits] as [SmokingHabits],
													[SynagogueAttendance] as [SynagogueAttendance],
													[Zodiac] as [Zodiac],
													r.[Longitude],
													r.[Latitude]
												FROM [fctSearchPreference] sp join mnRegion.dbo.Region r on sp.regionid = r.regionid
												WHERE DomainID = 1", conn);

			DataTable table = new DataTable();
			SqlDataAdapter da = new SqlDataAdapter(command);

			try
			{
				conn.Open();
				da.Fill(table);
			}
			finally
			{
				conn.Close();
			}


			//TODO: Add support for [Distance] [SearchOrderBy] [SearchTypeID]
												
			if (table != null && table.Rows.Count > 0) 
			{


		
				
				SaveFileDialog sfd = new SaveFileDialog();

				sfd.InitialDirectory = "c:\\temp" ;
				sfd.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*" ;
				sfd.DefaultExt = "*.txt";
				sfd.CheckFileExists = false;
				sfd.CreatePrompt = false;
				sfd.RestoreDirectory = true ;
				
				if(sfd.ShowDialog() == DialogResult.OK) 
				{

					int RowsPerFile = 0;
					int FileNumber = 0;
					StreamWriter writer = new StreamWriter(string.Format( "{0}.{1:D4}",sfd.FileName,FileNumber) ,false);				
					writer.AutoFlush = true;

					for (int rownum = 0; rownum < table.Rows.Count; rownum ++)
					{
						System.Diagnostics.Trace.WriteLine("row " + rownum.ToString());
						DataRow row = table.Rows[rownum];
						MatchnetQuery mq = new MatchnetQuery();
						for (int colnum = 0; colnum < table.Columns.Count; colnum++)
						{
							object val = row[colnum];
							if (val != DBNull.Value)
							{								
								if (val is Int32 && (int)val < 1) 
								{
									continue;
								}
								else 
								{
									try 
									{
										QuerySearchParam param;
										if (table.Columns[colnum].ColumnName.StartsWith("AreaCode"))
										{
											param = (QuerySearchParam)Enum.Parse(typeof(QuerySearchParam), "AreaCode", true);
										}
										else 
										{
											param = (QuerySearchParam)Enum.Parse(typeof(QuerySearchParam), table.Columns[colnum].ColumnName, true);
										}
										mq.AddParameter(new MatchnetQueryParameter(param,row[colnum]));
									}
									catch (Exception ex)
									{
										Debug.WriteLine(ex.Message);
									}
								}
							}
						}


						IQuery fq = QueryTransformer.Instance.Transform(mq);
						writer.Write("/cgi-bin/smallresultset?sortby=default&hits=600&resultview=smallresultset&type=adv&query=");
						writer.Write(HttpUtility.UrlEncode(fq.QueryString));
						writer.Write("&filter=");
						writer.WriteLine(HttpUtility.UrlEncode(fq.SearchParameters.Filter));

						writer.Flush();
						if ( ++RowsPerFile == 3001)
						{
							writer.Close();
							writer = null;
							writer = new StreamWriter(string.Format( "{0}.{1:D4}",sfd.FileName,++FileNumber) ,false);
							writer.AutoFlush = true;
							txtOutput.AppendText("\nWrote file " + FileNumber.ToString());
							RowsPerFile = 0;
						}
						
					}
				}		
			}
		}


		private void btnVOGetConfigs_Click(object sender, System.EventArgs e)
		{
			string setting;

			txtOutput.AppendText("\r\n");

			// Display the FASTQUERYSVC_QUERYHOST setting.
			setting = RuntimeSettings.GetSetting("FASTQUERYSVC_QUERYHOST");
			txtOutput.AppendText("FASTQUERYSVC_QUERYHOST - Server where the FAST Search Query node (QR) is located:\r\n" + setting + "\r\n\r\n");

			// Display the FASTPUSHSVC_QUEUE_NAME settings.  NOTE:  Each CommunityID should have its own MSMQ for FAST
			// pushing documents into FAST.
			txtOutput.AppendText("FASTPUSHSVC_QUEUE_NAME:\r\n");

			foreach (string val in RuntimeSettings.GetSetting(SETTING_FASTPUSHSVC_COMMUNITY_LIST).Split(','))
			{
				int communityID = Convert.ToInt16(val);

				string cset = RuntimeSettings.GetSetting(SETTING_FASTPUSHSVC_QUEUE_NAME, communityID);

				txtOutput.AppendText(string.Format("Community {0} -> {1}\n", communityID, cset));
			}
			txtOutput.AppendText("\r\n");

			// Display the FASTQueryServiceManagerURL value.
			setting = ConfigurationSettings.AppSettings[APPSETTING_FASTQUERYSVC_SMURL];
			txtOutput.AppendText("FASTQueryServiceManagerURL - This is set in the .config.  It is the location of the FASTQuery Service that is run when you click \"Run SM Query (Remoting)\":\r\n" + setting + "\r\n\r\n");

			// Display the FASTPUSHSVC_QUEUE_PATH settings.
			//			Matchnet.Configuration.ValueObjects.ServicePartitions parts = AdapterConfigurationSA.GetPartitions();
			//			foreach( Matchnet.Configuration.ValueObjects.ServicePartition part in parts){
			//				//Matchnet.Configuration.ValueObjects.ServicePartition part = AdapterConfigurationSA.GetServicePartition("FASTQUERY_SVC",PartitionMode.None);
			//				if (part.Count == 1){
			//					txtOutput.AppendText(string.Format("\n*** {0}\n{1} partition\nhost {2}\noffset {3}\nport {4}\n",
			//						part.Constant, 
			//						part.Count, 
			//						part.HostName,
			//						part.PartitionOffset,
			//						part.Port));
			//				}
			//				else {
			//					
			//					txtOutput.AppendText(string.Format("\n*** {0}\n{1} partitions, [{2}] @ {3}:{4}\n",
			//						part.Constant, 
			//						part.Count,
			//						part.PartitionOffset,
			//						part.HostName,
			//						part.Port
			//						));
			//
			//					for (int i = 0; i < part.Count; i++){
			//						txtOutput.AppendText(string.Format("\toffset[{0}] on {1}, IsEnabled={2}\n",
			//							i,
			//							part[i].HostName,
			//							part[i].IsEnabled
			//							));
			//					}
			//				}
			//			}
		}


		private void btnFQDRunRawQuery_Click(object sender, System.EventArgs e) 
		{
			IQueryFactory qf = QueryFactory.NewInstance();
			ISearchParameters parameters = qf.CreateParameters();
			
			parameters.Parameter = qf.CreateParameter(BaseParameter.HITS,600);

			if (txtFQDFastFilterTerms.Text != "")
			{			
				parameters.Parameter = qf.CreateParameter(no.fast.ds.search.SearchType.SEARCH_ADVANCED);
				parameters.Parameter = qf.CreateParameter(BaseParameter.QUERY,txtFQDFastQueryTerms.Text);
			}
			parameters.Parameter = qf.CreateParameter(BaseParameter.FILTER,txtFQDFastFilterTerms.Text);
			if (cbFQDSortTerms.Text != string.Empty )
			{
				parameters.Parameter = qf.CreateParameter(BaseParameter.SORT_BY, cbFQDSortTerms.Text);
			}
			IQuery query = qf.CreateQuery(parameters);
			Debug.WriteLine(query.ToString());
			RenderResults(query);

			
		}

		private void btnTransformExpandBitmask_Click(object sender, System.EventArgs e) 
		{
			MessageBox.Show(" change the access of Transformer. Expand Bitmask function(s) and uncomment");
			//			for (int i = Int32.MaxValue ; i > Int32.MaxValue - 100 ; i--){
			//				StringBuilder buf = new StringBuilder();			
			//				QueryTransformer.Instance.ExpandMaskToValueList("A","B",i," ",ref buf);
			//				txtOutput.AppendText("\n" + i.ToString() + ": " + buf.ToString());
			//			}
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			IMatchnetResultItem item = new MatchnetResultItem(1234);
			item["username"]	= "foo 1";
			item["birthdate"]	= "foo 2";
			item["regionid"]	= "bawer";
			item["gendermask"]	= "sdljg";
			item["aboutme"]		= "wehgfasdf";
			item["headline"]	= "345345";
			item["thumbpath"]	= "afglks48t";
			
			Stream stream = new FileStream( @"c:\temp\serial.txt",FileMode.Create);
			IFormatter formatter = new BinaryFormatter();
			formatter.Serialize( stream, item );

			stream.Close();
		

		}

		private void btnFQDMatchMailSearch_Click(object sender, System.EventArgs e)
		{
			IQueryFactory qf = QueryFactory.NewInstance();
			ISearchParameters parameters = qf.CreateParameters();
			
			string [] _miniProfileFields =  new string [] {"username", "birthdate", "regionid", "gendermask", "aboutme", "headline", "thumbpath"};			
			Random rnd = new Random( 17 );
			


			parameters.Parameter = qf.CreateParameter(BaseParameter.FILTER,"meta.collection:domainid3");
			parameters.Parameter = qf.CreateParameter(BaseParameter.FILTER,"hasphotoflag:1");
			parameters.Parameter = qf.CreateParameter(BaseParameter.FILTER,"LastLogonDate:<2005-06-" + (rnd.Next(29) + 1).ToString());
			parameters.Parameter = qf.CreateParameter(BaseParameter.HITS,106);
			parameters.Parameter = qf.CreateParameter(BaseParameter.RESULT_VIEW, "miniprofile");
			IQuery query = qf.CreateQuery(parameters);
			Debug.WriteLine(query.ToString());

			TimeSpan s1 = new TimeSpan(DateTime.Now.Ticks);
			IFastSearchEngine engine = SearchEngineFactory.Instance.GetSearchEngine();			
			IQueryResult FASTResult = engine.Search(query);
			TimeSpan s2 = new TimeSpan(DateTime.Now.Ticks);
			txtOutput.AppendText( "\nFAST round trip " + s2.Subtract(s1).TotalMilliseconds + " they say " + FASTResult.TimeUsed  + "\n");

			MatchnetQueryResults result;
			
			result = new MatchnetQueryResults(FASTResult.Hits); // .Hits is the actual number of items returned
			result.MatchesFound = FASTResult.DocCount; // .DocCount is the number of matches found but not necessarily returned
			
			TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
			for (int i = 1; i <= FASTResult.Hits ;i++)
			{ // GetDocument(i) is 1 based !!! not zero based!!!			
				IDocumentSummary summary = FASTResult.GetDocument(i);
				IMatchnetResultItem item = new MatchnetResultItem(Int32.Parse(summary.GetSummaryField("memberid").Summary));
				foreach (string field in _miniProfileFields) 
				{
					IDocumentSummaryField sum = summary.GetSummaryField(field);
					item[field] = (sum == null) ? String.Empty : item[field] = sum.Summary;
				}
				result.AddResult(item);
			}
			TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
			txtOutput.AppendText( result.Items.Count + " took " + ts2.Subtract(ts1).TotalMilliseconds);


			
			TimeSpan d1 = new TimeSpan(DateTime.Now.Ticks);
			WebClient wc = new WebClient();
			byte [] resultdirect = wc.DownloadData("http://fsqry02:15100/cgi-bin/miniprofile?hits=106&filter=+%2bmeta.collection%3adomainid3+%2bhasphotoflag%3a1");

			TimeSpan d2 = new TimeSpan(DateTime.Now.Ticks);
			txtOutput.AppendText( "\ndirect FAST round trip " + d2.Subtract(d1).TotalMilliseconds + " returned " + resultdirect.Length + " bytes.\n");


			
		}

		private void btnLTDumpQueriesToFile_Click(object sender, System.EventArgs e)
		{
			LoadTester.SaveQueries(@"c:\temp\LoadTestQueries.txt");
		}


		private void btnLTQueryLoadTes1_Click(object sender, System.EventArgs e)
		{
			
			int testerCount = tkbLT_NumThreads.Value;
			btnLTQueryLoadTes1.Enabled = false;
			_LoadTesters = new object [testerCount,2];
			for(int i = 0; i < testerCount; i++)
			{
				LoadTester lt = new LoadTester();
				lt.ID = "t" + i.ToString();

				Thread t = new Thread(new ThreadStart(lt.RunTest));
				
				_LoadTesters[i,LOAD_TEST_OBJ] = lt;
				_LoadTesters[i,LOAD_TEST_THREAD] = t;

			}

			txtOutput.AppendText("\nLoaded " + testerCount + " testers, run for " + numLT_TestDuration.Value + " seconds...\n");
			Application.DoEvents();

			for (int i = 0; i < _LoadTesters.GetLength(0); i++)
			{
				(_LoadTesters[i,LOAD_TEST_THREAD] as Thread).Start();
			}

			System.Threading.Thread.Sleep((int)numLT_TestDuration.Value * 1000);

			for (int i = 0; i < _LoadTesters.GetLength(0); i++)
			{
				(_LoadTesters[i,LOAD_TEST_OBJ] as LoadTester).StopTest();
			}

			for (int i = 0; i < _LoadTesters.GetLength(0); i++)
			{
				(_LoadTesters[i,LOAD_TEST_THREAD] as Thread).Join();
			}

			for (int i = 0; i < _LoadTesters.GetLength(0); i++)
			{
				txtOutput.AppendText((_LoadTesters[i,LOAD_TEST_OBJ] as LoadTester).ToString());
				txtOutput.AppendText("\n");

			}

			btnLTQueryLoadTes1.Enabled = true;
		
		}

		private void tkbLT_NumThreads_ValueChanged(object sender, System.EventArgs e)
		{
			lblLT_NumThreads.Text = tkbLT_NumThreads.Value.ToString();
		}

	}
}

