using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Search.BusinessLogic;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ServiceManagers;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.Interfaces;

namespace Matchnet.Search.TestHarness
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class ServiceAdapters : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox txtSessionKey;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtMemberID;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtCommunityID;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.DataGrid dgPreferences;
		private System.Windows.Forms.Button btnSavePreferences;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button btnExecuteSearch;

		private SearchPreferenceCollection _searchPreferences = null;
		private System.Windows.Forms.Button btnLoadPreferences;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox txtSiteID;
		private System.Windows.Forms.TextBox txtStartRow;
		private System.Windows.Forms.TextBox txtPageSize;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.DataGrid dgSearchResults;
		private System.Windows.Forms.Label lblMoreResultsAvailable;
		private System.Windows.Forms.CheckBox cbxUseBLDirectly;
		private System.Windows.Forms.Button btnFASTTransformation;
		private System.Windows.Forms.CheckBox MatchmailSearchType;
        private TextBox txtURI;
        private CheckBox chkUseURI;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ServiceAdapters()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
            txtURI.Text = "tcp://lasvcsearch01:48000/MemberSearchSM.rem";
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.btnLoadPreferences = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFASTTransformation = new System.Windows.Forms.Button();
            this.btnSavePreferences = new System.Windows.Forms.Button();
            this.dgPreferences = new System.Windows.Forms.DataGrid();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCommunityID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMemberID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSessionKey = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.MatchmailSearchType = new System.Windows.Forms.CheckBox();
            this.lblMoreResultsAvailable = new System.Windows.Forms.Label();
            this.dgSearchResults = new System.Windows.Forms.DataGrid();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPageSize = new System.Windows.Forms.TextBox();
            this.txtStartRow = new System.Windows.Forms.TextBox();
            this.txtSiteID = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnExecuteSearch = new System.Windows.Forms.Button();
            this.cbxUseBLDirectly = new System.Windows.Forms.CheckBox();
            this.chkUseURI = new System.Windows.Forms.CheckBox();
            this.txtURI = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPreferences)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSearchResults)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLoadPreferences
            // 
            this.btnLoadPreferences.Location = new System.Drawing.Point(144, 64);
            this.btnLoadPreferences.Name = "btnLoadPreferences";
            this.btnLoadPreferences.Size = new System.Drawing.Size(104, 24);
            this.btnLoadPreferences.TabIndex = 0;
            this.btnLoadPreferences.Text = "Load Preferences";
            this.btnLoadPreferences.Click += new System.EventHandler(this.btnLoadPreferences_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnFASTTransformation);
            this.groupBox1.Controls.Add(this.btnSavePreferences);
            this.groupBox1.Controls.Add(this.dgPreferences);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtCommunityID);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtMemberID);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtSessionKey);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnLoadPreferences);
            this.groupBox1.Location = new System.Drawing.Point(8, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(280, 564);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search Preferences:";
            // 
            // btnFASTTransformation
            // 
            this.btnFASTTransformation.Location = new System.Drawing.Point(8, 416);
            this.btnFASTTransformation.Name = "btnFASTTransformation";
            this.btnFASTTransformation.Size = new System.Drawing.Size(128, 23);
            this.btnFASTTransformation.TabIndex = 10;
            this.btnFASTTransformation.Text = "FAST Transform";
            this.btnFASTTransformation.Click += new System.EventHandler(this.btnFASTTransformation_Click);
            // 
            // btnSavePreferences
            // 
            this.btnSavePreferences.Location = new System.Drawing.Point(160, 416);
            this.btnSavePreferences.Name = "btnSavePreferences";
            this.btnSavePreferences.Size = new System.Drawing.Size(104, 23);
            this.btnSavePreferences.TabIndex = 9;
            this.btnSavePreferences.Text = "Save Preferences";
            this.btnSavePreferences.Click += new System.EventHandler(this.btnSavePreferences_Click);
            // 
            // dgPreferences
            // 
            this.dgPreferences.DataMember = "";
            this.dgPreferences.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgPreferences.Location = new System.Drawing.Point(8, 120);
            this.dgPreferences.Name = "dgPreferences";
            this.dgPreferences.Size = new System.Drawing.Size(264, 288);
            this.dgPreferences.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(8, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 23);
            this.label4.TabIndex = 7;
            this.label4.Text = "Preferences:";
            // 
            // txtCommunityID
            // 
            this.txtCommunityID.Location = new System.Drawing.Point(96, 64);
            this.txtCommunityID.Name = "txtCommunityID";
            this.txtCommunityID.Size = new System.Drawing.Size(40, 20);
            this.txtCommunityID.TabIndex = 6;
            this.txtCommunityID.Text = "1";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(8, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 23);
            this.label3.TabIndex = 5;
            this.label3.Text = "Community ID:";
            // 
            // txtMemberID
            // 
            this.txtMemberID.Location = new System.Drawing.Point(96, 40);
            this.txtMemberID.Name = "txtMemberID";
            this.txtMemberID.Size = new System.Drawing.Size(152, 20);
            this.txtMemberID.TabIndex = 4;
            this.txtMemberID.Text = "27029711";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 23);
            this.label2.TabIndex = 3;
            this.label2.Text = "Member ID:";
            // 
            // txtSessionKey
            // 
            this.txtSessionKey.Location = new System.Drawing.Point(96, 16);
            this.txtSessionKey.Name = "txtSessionKey";
            this.txtSessionKey.Size = new System.Drawing.Size(152, 20);
            this.txtSessionKey.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "Session Key:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtURI);
            this.groupBox2.Controls.Add(this.chkUseURI);
            this.groupBox2.Controls.Add(this.MatchmailSearchType);
            this.groupBox2.Controls.Add(this.lblMoreResultsAvailable);
            this.groupBox2.Controls.Add(this.dgSearchResults);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtPageSize);
            this.groupBox2.Controls.Add(this.txtStartRow);
            this.groupBox2.Controls.Add(this.txtSiteID);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.btnExecuteSearch);
            this.groupBox2.Controls.Add(this.cbxUseBLDirectly);
            this.groupBox2.Location = new System.Drawing.Point(304, 16);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(456, 564);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Execute Member Search";
            // 
            // MatchmailSearchType
            // 
            this.MatchmailSearchType.Location = new System.Drawing.Point(216, 64);
            this.MatchmailSearchType.Name = "MatchmailSearchType";
            this.MatchmailSearchType.Size = new System.Drawing.Size(192, 24);
            this.MatchmailSearchType.TabIndex = 10;
            this.MatchmailSearchType.Text = "Search Type: Matchmail";
            // 
            // lblMoreResultsAvailable
            // 
            this.lblMoreResultsAvailable.Location = new System.Drawing.Point(104, 112);
            this.lblMoreResultsAvailable.Name = "lblMoreResultsAvailable";
            this.lblMoreResultsAvailable.Size = new System.Drawing.Size(100, 23);
            this.lblMoreResultsAvailable.TabIndex = 9;
            // 
            // dgSearchResults
            // 
            this.dgSearchResults.BackgroundColor = System.Drawing.Color.LightGray;
            this.dgSearchResults.DataMember = "";
            this.dgSearchResults.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgSearchResults.Location = new System.Drawing.Point(16, 152);
            this.dgSearchResults.Name = "dgSearchResults";
            this.dgSearchResults.Size = new System.Drawing.Size(416, 264);
            this.dgSearchResults.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(16, 112);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 23);
            this.label8.TabIndex = 7;
            this.label8.Text = "More Results:";
            // 
            // txtPageSize
            // 
            this.txtPageSize.Location = new System.Drawing.Point(128, 72);
            this.txtPageSize.Name = "txtPageSize";
            this.txtPageSize.Size = new System.Drawing.Size(64, 20);
            this.txtPageSize.TabIndex = 6;
            this.txtPageSize.Text = "10";
            // 
            // txtStartRow
            // 
            this.txtStartRow.Location = new System.Drawing.Point(128, 48);
            this.txtStartRow.Name = "txtStartRow";
            this.txtStartRow.Size = new System.Drawing.Size(64, 20);
            this.txtStartRow.TabIndex = 5;
            this.txtStartRow.Text = "1";
            // 
            // txtSiteID
            // 
            this.txtSiteID.Location = new System.Drawing.Point(128, 24);
            this.txtSiteID.Name = "txtSiteID";
            this.txtSiteID.Size = new System.Drawing.Size(64, 20);
            this.txtSiteID.TabIndex = 4;
            this.txtSiteID.Text = "1";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(16, 72);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 23);
            this.label7.TabIndex = 3;
            this.label7.Text = "Page Size:";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(16, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 23);
            this.label6.TabIndex = 2;
            this.label6.Text = "Start Row:";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(16, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 23);
            this.label5.TabIndex = 1;
            this.label5.Text = "Site ID:";
            // 
            // btnExecuteSearch
            // 
            this.btnExecuteSearch.Location = new System.Drawing.Point(216, 104);
            this.btnExecuteSearch.Name = "btnExecuteSearch";
            this.btnExecuteSearch.Size = new System.Drawing.Size(128, 23);
            this.btnExecuteSearch.TabIndex = 0;
            this.btnExecuteSearch.Text = "Execute Search";
            this.btnExecuteSearch.Click += new System.EventHandler(this.btnExecuteSearch_Click);
            // 
            // cbxUseBLDirectly
            // 
            this.cbxUseBLDirectly.Location = new System.Drawing.Point(216, 16);
            this.cbxUseBLDirectly.Name = "cbxUseBLDirectly";
            this.cbxUseBLDirectly.Size = new System.Drawing.Size(232, 48);
            this.cbxUseBLDirectly.TabIndex = 4;
            this.cbxUseBLDirectly.Text = "Use BL Directly (instead of the Service).  You probably want to click the \"FAST T" +
                "ransform\" button first.";
            // 
            // chkUseURI
            // 
            this.chkUseURI.AutoSize = true;
            this.chkUseURI.Location = new System.Drawing.Point(23, 439);
            this.chkUseURI.Name = "chkUseURI";
            this.chkUseURI.Size = new System.Drawing.Size(67, 17);
            this.chkUseURI.TabIndex = 11;
            this.chkUseURI.Text = "Use URI";
            this.chkUseURI.UseVisualStyleBackColor = true;
            // 
            // txtURI
            // 
            this.txtURI.Location = new System.Drawing.Point(24, 466);
            this.txtURI.Name = "txtURI";
            this.txtURI.Size = new System.Drawing.Size(407, 20);
            this.txtURI.TabIndex = 12;
            // 
            // ServiceAdapters
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(768, 601);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "ServiceAdapters";
            this.Text = "Service Adapters";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPreferences)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSearchResults)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private void btnLoadPreferences_Click(object sender, System.EventArgs e)
		{
			// Test Get SearchPreferences
			try
			{
				btnLoadPreferences.Enabled = false;

				// Use Service Adapter
				_searchPreferences = SearchPreferencesSA.Instance.GetSearchPreferences(
					Convert.ToInt32(txtMemberID.Text)
					, Convert.ToInt32(txtCommunityID.Text));

				// Use Service Manager
//				SearchPreferencesSM sm = new SearchPreferencesSM();
//				_searchPreferences = (SearchPreferenceCollection)sm.GetSearchPreferences(
//					Convert.ToInt32(txtMemberID.Text)
//					, Convert.ToInt32(txtCommunityID.Text));

				DataTable dt = new DataTable("preferences");
				dt.Columns.Add("name");
				dt.Columns.Add("preference");
                dt.Columns.Add("weight");

				ICollection keys = _searchPreferences.Keys();

				foreach (string key in keys)
				{
					dt.Rows.Add(new string[] { key, _searchPreferences[key], string.Empty});
				}
				
//				foreach(SearchPreference pref in _searchPreferences)
//				{
//					dt.Rows.Add(new string[] {pref.Name, pref.Value});
//				}
				dgPreferences.DataSource = dt;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
			finally
			{
				btnLoadPreferences.Enabled = true;
			}
		}

		private void btnSavePreferences_Click(object sender, System.EventArgs e)
		{
			try
			{
				btnSavePreferences.Enabled = false;
				if (dgPreferences.DataSource != null)
				{
					DataTable dt = (DataTable)dgPreferences.DataSource;
					SearchPreferenceCollection preferences = new SearchPreferenceCollection();
					
					foreach (DataRow row in dt.Rows)
					{
						preferences.Add(Convert.ToString(row["name"]), Convert.ToString(row["preference"]));
					}

//					// use Service Adapter
//					SearchPreferencesSA.Instance.Save(
//						Convert.ToInt32(txtMemberID.Text)
//						, Convert.ToInt32(txtCommunityID.Text)
//						, preferences);

					// use SM
					SearchPreferencesSM serviceManager = new SearchPreferencesSM();
					serviceManager.Save(
						Convert.ToInt32(txtMemberID.Text)
						, Convert.ToInt32(txtCommunityID.Text)
						, preferences);

					// use BL
//					SearchPreferencesBL.Instance.Save(
//						Convert.ToInt32(txtMemberID.Text)
//						, Convert.ToInt32(txtCommunityID.Text)
//						, preferences);

					// Hold them for search execution.
					_searchPreferences = preferences;
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
			finally
			{
				btnSavePreferences.Enabled = true;
			}
		}

		private string getSessionKey()
		{
			// 267888ce-8030-476a-afbd-b0e6e3188f30	
			string sessionKey = null;
			if (!string.Empty.Equals(txtSessionKey.Text))
			{
				sessionKey = txtSessionKey.Text;
			}
			return sessionKey;
		}

		private void btnExecuteSearch_Click(object sender, System.EventArgs e)
		{

			if (_searchPreferences == null)
			{
				MessageBox.Show("You must 'load search preferences' before executing a member search", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			try
			{
				btnExecuteSearch.Enabled = false;

				MatchnetQueryResults searchResults;

				if (!cbxUseBLDirectly.Checked)
				{
					if (!MatchmailSearchType.Checked)
					{
						// Use this block to test the search execution via the service adapter (will execute in the service process)
                        if (!chkUseURI.Checked)
                        {
                            searchResults = MemberSearchSA.Instance.Search(_searchPreferences
                                , Convert.ToInt32(txtCommunityID.Text)
                                , Convert.ToInt32(txtSiteID.Text)
                                , Convert.ToInt32(txtStartRow.Text)
                                , Convert.ToInt32(txtPageSize.Text));
                        }
                        else
                        {
                            string uri = txtURI.Text;
                            IMemberSearchService service = getService(uri);
                            searchResults = (MatchnetQueryResults)service.Search(_searchPreferences
                                , Convert.ToInt32(txtCommunityID.Text)
                                , Convert.ToInt32(txtSiteID.Text),SearchEngineType.Relational
                               );
                        }
					}
					else
					{
						// Use this block to test the search execution via the service adapter (will execute in the service process)
                        if (!chkUseURI.Checked)
                        {
                            searchResults = MemberSearchSA.Instance.Search(_searchPreferences
                                , Convert.ToInt32(txtCommunityID.Text)
                                , Convert.ToInt32(txtSiteID.Text)
                                , Convert.ToInt32(txtStartRow.Text)
                                , Convert.ToInt32(txtPageSize.Text),
                                SearchEngineType.FAST,
                                SearchType.MatchMail);
                        }
                        else
                        {
                              string uri = txtURI.Text;
                            IMemberSearchService service = getService(uri);
                            FASTQueryTransformer.TransformSearchPreferences(_searchPreferences, Convert.ToInt32(txtCommunityID.Text));

                            searchResults =(MatchnetQueryResults) service.Search(_searchPreferences
                                , Convert.ToInt32(txtCommunityID.Text)
                                , Convert.ToInt32(txtSiteID.Text),
                                 SearchEngineType.FAST);
                        }
					}
				}
				else
				{
					// Use this block to test the seach execution via the BL instance directly
                    searchResults = null;/* (MatchnetQueryResults)MemberSearchBL.Instance.Search(_searchPreferences
                         , Convert.ToInt32(txtCommunityID.Text)
                         , Convert.ToInt32(txtSiteID.Text)
                         , null
                         );*/
				}

				

				// This block works with the ICollection of MatchnetResultItem's
				if (searchResults.Items.Count > 0)
				{	
					if (MatchmailSearchType.Checked)
					{
						DataTable dt = new DataTable("Attributes");
						dt.Columns.Add("MemberID");
						dt.Columns.Add("Birthdate");
						dt.Columns.Add("UserName");
						dt.Columns.Add("AboutMe");
						dt.Columns.Add("ThumbPath");
						dt.Columns.Add("RegionID");

						for (int i = 0; i < searchResults.Items.Count; i++)
						{
							IMatchnetResultItem item = searchResults.Items[i];

							if (item != null)
							{
                                Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(item.MemberID, MemberLoadFlags.None);
                                DateTime birthDate = member.GetAttributeDate(Conversion.CInt(txtCommunityID.Text),
                                    Conversion.CInt(txtSiteID.Text),
                                    Constants.NULL_INT,
                                    "BirthDate",
                                    DateTime.MinValue);


								dt.Rows.Add(new string[] {item.MemberID.ToString(), 
															 birthDate.ToString(), item["username"],
															 item["aboutme"], item["thumbpath"], item["regionid"],
								});
							}
						}

						dgSearchResults.DataSource = dt;
					}

					else
					{
						DataTable dt = new DataTable("MemberIDs");
						dt.Columns.Add("MemberID");

						IMatchnetResultItem item;
						for (int i = 0; i < searchResults.Items.Count; i++)
						{
							item = searchResults.Items[i];

							if (item != null)
							{
								dt.Rows.Add(new string[] {item.MemberID.ToString()});
							}
						}
					
						dgSearchResults.DataSource = dt;
					}
				}
				else
				{
					dgSearchResults.DataSource = new DataTable();
				}

				lblMoreResultsAvailable.Text = searchResults.MoreResultsAvailable.ToString();
              
			}
		catch(Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				btnExecuteSearch.Enabled = true;
			}
		}

		private void btnFASTTransformation_Click(object sender, System.EventArgs e)
		{
			if (_searchPreferences == null)
			{
				btnFASTTransformation.Enabled = false;
				return;
			}

			// get the modified preferences form the data grid.
			DataTable dtPrefs = (DataTable)dgPreferences.DataSource;
			SearchPreferenceCollection preferences = new SearchPreferenceCollection();
					
			foreach (DataRow row in dtPrefs.Rows)
			{
				preferences.Add(Convert.ToString(row["name"]), Convert.ToString(row["preference"]));
			}
			_searchPreferences = preferences;

			// transform on the SA side (latitude, longitude, geodistance, etc.)
			FASTQueryTransformer.TransformSearchPreferences(_searchPreferences, 10);

			string cacheKey = ResultsKey.GetCacheKey(_searchPreferences, 10, 100);
			MessageBox.Show(cacheKey);

			// build the IMatchnetQuery
			IMatchnetQuery fastQuery = MatchnetQueryBuilder.build(_searchPreferences
				, Convert.ToInt32(txtCommunityID.Text)
				, Convert.ToInt32(txtSiteID.Text)
				);

			// bind the results to the data grid
			DataTable dt = new DataTable("preferences");
			dt.Columns.Add("name");
			dt.Columns.Add("preference");

			IMatchnetQueryParameter parameter;
			for (int i = 0; i < fastQuery.Count; i++)
			{
				parameter = fastQuery[i];
				
				dt.Rows.Add(new string[] {parameter.Parameter.ToString(), parameter.Value.ToString()});
			}
			dgPreferences.DataSource = dt;

		
		}

        private IMemberSearchService getService(string uri)
        {
            try
            {
                return (IMemberSearchService)Activator.GetObject(typeof(IMemberSearchService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }
	}
}
