using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Matchnet.Search.TestHarness
{
	/// <summary>
	/// Summary description for TestHarness.
	/// </summary>
	public class TestHarness : System.Windows.Forms.Form
	{
		private System.Windows.Forms.MainMenu mmHarnesses;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem miServiceAdapters;
		private System.Windows.Forms.MenuItem miCaching;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public TestHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new TestHarness());
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mmHarnesses = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.miServiceAdapters = new System.Windows.Forms.MenuItem();
			this.miCaching = new System.Windows.Forms.MenuItem();
			// 
			// mmHarnesses
			// 
			this.mmHarnesses.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						this.menuItem1});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.miServiceAdapters,
																					  this.miCaching});
			this.menuItem1.Text = "Harnesses";
			// 
			// miServiceAdapters
			// 
			this.miServiceAdapters.Index = 0;
			this.miServiceAdapters.Text = "ServiceAdapters";
			this.miServiceAdapters.Click += new System.EventHandler(this.miServiceAdapters_Click);
			// 
			// miCaching
			// 
			this.miCaching.Index = 1;
			this.miCaching.Text = "Caching";
			this.miCaching.Click += new System.EventHandler(this.miCaching_Click);
			// 
			// TestHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(776, 645);
			this.IsMdiContainer = true;
			this.Menu = this.mmHarnesses;
			this.Name = "TestHarness";
			this.Text = "Matchnet.Search - Test Harness";

		}
		#endregion

		private void miServiceAdapters_Click(object sender, System.EventArgs e)
		{
			ServiceAdapters frm = new ServiceAdapters();
			frm.MdiParent = this;
			frm.Show();
		}

		private void miCaching_Click(object sender, System.EventArgs e)
		{
			CachingHarness frm = new CachingHarness();
			frm.MdiParent = this;
			frm.Show();

		}
	}
}
