using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using Matchnet.Replication;
using Matchnet.Search.BusinessLogic;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;

namespace Matchnet.Search.TestHarness
{
	/// <summary>
	/// Summary description for CachingHarness.
	/// </summary>
	public class CachingHarness : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnResultsKey;
		private System.Windows.Forms.Label lblResultsKey;
		private System.Windows.Forms.Button btnTestReplicator;
		private System.Windows.Forms.Button btnIsraelAreaCodeSearch;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public CachingHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblResultsKey = new System.Windows.Forms.Label();
			this.btnResultsKey = new System.Windows.Forms.Button();
			this.btnTestReplicator = new System.Windows.Forms.Button();
			this.btnIsraelAreaCodeSearch = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lblResultsKey
			// 
			this.lblResultsKey.Location = new System.Drawing.Point(192, 16);
			this.lblResultsKey.Name = "lblResultsKey";
			this.lblResultsKey.Size = new System.Drawing.Size(480, 48);
			this.lblResultsKey.TabIndex = 0;
			this.lblResultsKey.Text = "result";
			// 
			// btnResultsKey
			// 
			this.btnResultsKey.Location = new System.Drawing.Point(0, 16);
			this.btnResultsKey.Name = "btnResultsKey";
			this.btnResultsKey.Size = new System.Drawing.Size(184, 23);
			this.btnResultsKey.TabIndex = 1;
			this.btnResultsKey.Text = "Test Results Key";
			this.btnResultsKey.Click += new System.EventHandler(this.btnResultsKey_Click);
			// 
			// btnTestReplicator
			// 
			this.btnTestReplicator.Location = new System.Drawing.Point(8, 96);
			this.btnTestReplicator.Name = "btnTestReplicator";
			this.btnTestReplicator.Size = new System.Drawing.Size(152, 23);
			this.btnTestReplicator.TabIndex = 2;
			this.btnTestReplicator.Text = "Test Replicator";
			this.btnTestReplicator.Click += new System.EventHandler(this.btnTestReplicator_Click);
			// 
			// btnIsraelAreaCodeSearch
			// 
			this.btnIsraelAreaCodeSearch.Location = new System.Drawing.Point(8, 176);
			this.btnIsraelAreaCodeSearch.Name = "btnIsraelAreaCodeSearch";
			this.btnIsraelAreaCodeSearch.Size = new System.Drawing.Size(160, 23);
			this.btnIsraelAreaCodeSearch.TabIndex = 3;
			this.btnIsraelAreaCodeSearch.Text = "Test IsraelAreaCode Search";
			this.btnIsraelAreaCodeSearch.Click += new System.EventHandler(this.btnIsraelAreaCodeSearch_Click);
			// 
			// CachingHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(680, 437);
			this.Controls.Add(this.btnIsraelAreaCodeSearch);
			this.Controls.Add(this.btnTestReplicator);
			this.Controls.Add(this.btnResultsKey);
			this.Controls.Add(this.lblResultsKey);
			this.Name = "CachingHarness";
			this.Text = "CachingHarness";
			this.ResumeLayout(false);

		}
		#endregion

		private void btnResultsKey_Click(object sender, System.EventArgs e)
		{
			try
			{
				btnResultsKey.Enabled = false;
				lblResultsKey.Text = string.Empty;
				SearchPreferenceCollection searchPrefs = SearchPreferencesSA.Instance.GetSearchPreferences(988272336, 1);
				lblResultsKey.Text = ResultsKey.GetCacheKey(searchPrefs, 1, 1);
			}
			finally
			{
				btnResultsKey.Enabled = true;
			}
		}

		private void btnTestReplicator_Click(object sender, System.EventArgs e)
		{
			Replicator replicator = null;

			string replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(
				ServiceConstants.SERVICE_CONSTANT
				, "SearchPreferencesSM"
				, "devapp01");
			replicator = new Replicator(ServiceConstants.SERVICE_NAME);
			replicator.SetDestinationUri(replicationURI);
			replicator.Start();

			SearchPreferenceCollection searchPrefs = SearchPreferencesSA.Instance.GetSearchPreferences(988272336, 1);

			try
			{

				replicator.Enqueue(searchPrefs);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}


		}

		private void btnIsraelAreaCodeSearch_Click(object sender, System.EventArgs e)
		{
			// create prefs
			SearchPreferenceCollection prefs = new SearchPreferenceCollection();
			prefs["SearchTypeID"] = ((int)SearchTypeID.AreaCode).ToString(); 
			prefs["CountryRegionID"] = "105";	//Israel
			prefs["AreaCode1"] = "02";
			prefs["AreaCode3"] = "04";

			// transform
			FASTQueryTransformer.TransformSearchPreferences(prefs, 0);

			//examine prefs

			int count = prefs.Count;

		}
	}
}
