using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using Matchnet.Search.BusinessLogic;
using Matchnet.Search.ServiceManagers;
using Matchnet.FAST.Query.ValueObjects;

namespace Matchnet.Search.Service
{
	/// <summary>
	/// Summary description for ProjectInstaller.
	/// </summary>
	[RunInstaller(true)]
	public class ProjectInstaller : System.Configuration.Install.Installer
	{
		private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller;
		private System.ServiceProcess.ServiceInstaller serviceInstaller;
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Default constructor for ProjectInstaller.
		/// </summary>
		public ProjectInstaller()
		{
			InitializeComponent();
			this.serviceInstaller.ServiceName = ServiceConstants.SERVICE_NAME;
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private void ProjectInstaller_AfterInstall(object sender, InstallEventArgs e)
		{
			MemberSearchSM.PerfCounterInstall();
			MetricsInstaller.Install();
			
		}


		private void ProjectInstaller_BeforeUninstall(object sender, InstallEventArgs e)
		{
			MemberSearchSM.PerfCounterUninstall();
			MetricsInstaller.Uninstall();
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.serviceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
			this.serviceInstaller = new System.ServiceProcess.ServiceInstaller();
			// 
			// serviceProcessInstaller
			// 
			this.serviceProcessInstaller.Password = null;
			this.serviceProcessInstaller.Username = null;
			// 
			// serviceInstaller
			// 
			this.serviceInstaller.ServiceName = "Matchnet.Search.Service";
			this.serviceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
			this.serviceInstaller.ServicesDependedOn = new string[] {"MSMQ"};
			// 
			// ProjectInstaller
			// 
			this.Installers.AddRange(new System.Configuration.Install.Installer[] {
																					  this.serviceProcessInstaller,
																					  this.serviceInstaller});

			this.BeforeUninstall += new System.Configuration.Install.InstallEventHandler(this.ProjectInstaller_BeforeUninstall);
			this.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.ProjectInstaller_AfterInstall);

		}
		#endregion
	}
}
