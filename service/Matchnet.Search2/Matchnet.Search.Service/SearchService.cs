using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Matchnet.Search.BusinessLogic;
using Matchnet.Search.ServiceManagers;


namespace Matchnet.Search.Service
{
	/// <summary>
	/// SearchService
	/// </summary>
	public class SearchService : Matchnet.RemotingServices.RemotingServiceBase
	{
		/// <summary>
		/// Service name
		/// </summary>
		private System.ComponentModel.Container components = null;
		private MemberSearchSM _memberSearchSM = null;
		private SearchPreferencesSM _searchPreferencesSM = null;
        private MemberSearchPreferencesSM _memberSearchPreferencesSM = null;
        
		/// <summary>
		/// Default constructor for the Matchnet.Search service.
		/// </summary>
		public SearchService()
		{
			InitializeComponent();
		}

		// The main entry point for the process
		static void Main()
		{
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new SearchService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            new ServiceBoundaryException("Matchnet.Search.Service", "An unhandled exception occured.", e.ExceptionObject as Exception);
        }

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = ServiceConstants.SERVICE_NAME;
            base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
				if (_memberSearchSM != null)
				{
					_memberSearchSM.Dispose();
				}
				if (_searchPreferencesSM != null)
				{
					_searchPreferencesSM.Dispose();
				}
                if(_memberSearchPreferencesSM != null)
                {
                    _memberSearchPreferencesSM.Dispose();
                }
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// Register the service managers for this service.
		/// - MemberSearch
		/// - SearchPreferences
		/// </summary>
		protected override void RegisterServiceManagers()
		{
			try
			{
				// Initialize service managers.
				_memberSearchSM = new MemberSearchSM();
				_searchPreferencesSM = new SearchPreferencesSM();
                _memberSearchPreferencesSM = new MemberSearchPreferencesSM();

				// Register them.
				base.RegisterServiceManager(_memberSearchSM);
				base.RegisterServiceManager(_searchPreferencesSM);
                base.RegisterServiceManager(_memberSearchPreferencesSM);
                
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(base.ServiceName, "Error occurred when starting the Windows Service, see details: " + ex.Message);
			}
		}

	}
}
