using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using System.Web.Caching;
using System.Collections;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Data;
using Matchnet.Exceptions;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.ValueObjects.ReplicationActions;
using Cache = Matchnet.Caching.Cache;


namespace Matchnet.Search.BusinessLogic
{
	/// <summary>
	/// Summary description for SearchPreferencesBL.
	/// </summary>
	public class SearchPreferencesBL
	{
		private const int GC_THRESHOLD = 10000;

		#region Class members

		private Caching.Cache _cache = null;
		private CacheItemRemovedCallback _expirePreference;
		private string _expiredCountLock = "";
		private int _expiredCount = 0;

		#endregion
		
		#region Delegates and Events

		/// <summary>
		/// Delegate signature for handling a Replication Requested event.
		/// </summary>
		public delegate void ReplicationEventHandler(IReplicable replicableObject);
		
		/// <summary>
		/// Event raised when a replication is being requested by this service.
		/// </summary>
		public event ReplicationEventHandler ReplicationRequested;

		/// <summary>
		/// Delegate signature for handling a PreferencesAdded event.
		/// </summary>
		public delegate void PreferencesAddEventHandler();
		/// <summary>
		/// Event raised when search preferences are added to the service cache.
		/// </summary>
		public event PreferencesAddEventHandler PreferencesAdded;

		/// <summary>
		/// Delegate signature for handling a PreferencesRemoved event.
		/// </summary>
		public delegate void PreferencesRemoveEventHandler();
		/// <summary>
		/// Event raised when search preferences are removed from the service cache.
		/// </summary>
		public event PreferencesRemoveEventHandler PreferencesRemoved;

		/// <summary>
		/// Delegate signature for handling a PreferencesRequested event.
		/// </summary>
		public delegate void PreferencesRequestedEventHandler(bool cacheHit);
		/// <summary>
		/// Event raised when the service is requested to return search preferences
		/// </summary>
		public event PreferencesRequestedEventHandler PreferencesRequested;

		/// <summary>
		/// Delegate signature for handling a PreferencesSaved event.
		/// </summary>
		public delegate void PreferencesSavedEventHandler();
		/// <summary>
		/// Event raised when the service is requested to save a set of search preferences.
		/// </summary>
		public event PreferencesSavedEventHandler PreferencesSaved;

		#endregion

		#region Singleton implementation

		/// <summary>
		/// Singleton instance of the SearchPreferencesBL class.
		/// </summary>
		public static readonly SearchPreferencesBL Instance = new SearchPreferencesBL();

		/// <summary>
		/// Constructor - singleton pattern.
		/// </summary>
		private SearchPreferencesBL()
		{
			_cache = Cache.Instance;
			_expirePreference = new CacheItemRemovedCallback(this.ExpireCallback);
		}

		#endregion

		#region BL implementation
		/// <summary>
		/// Accepts an IReplicable object and updates the local service cache.
		/// </summary>
		/// <param name="pReplicableObject"></param>
		public void CacheReplicatedObject(IReplicable pReplicableObject)
		{
			switch (pReplicableObject.GetType().Name)
			{
				case "SearchPreferenceCollection":
					CachePreferences((SearchPreferenceCollection)pReplicableObject);
					break;
				default:
					throw new Exception("invalid replication object type received (" + pReplicableObject.GetType().ToString() + ")");
			}
		}


		/// <summary>
		/// Retrieve a collection of search preferences for a member (session and/or their saved preferences).
		/// </summary>
		/// <param name="pMemberID">The member id associated with the active session.</param>
		/// <param name="pCommunityID">The domain id (community id) for the active session.</param>
		/// <returns>A collection of search preferences for the associated member.</returns>
		public SearchPreferenceCollection GetSearchPreferences(int pMemberID, int pCommunityID)
		{
			SearchPreferenceCollection searchPreferences;
			try
			{
				bool cacheHit = true;
				searchPreferences = _cache.Get(SearchPreferencesKey.GetCacheKey(pMemberID, pCommunityID)) as SearchPreferenceCollection;
				if (searchPreferences == null)
				{
					cacheHit = false;
					searchPreferences = getPreferences(pMemberID, pCommunityID);

					// Handle transformation of IsraelAreaCodes (zero padding issue)
					transformIsraelAreaCodes(searchPreferences);

					CachePreferences(searchPreferences, pMemberID, pCommunityID);
					ReplicationRequested(searchPreferences);
				}
				PreferencesRequested(cacheHit);		// Raise PreferencesRequested event.
				return searchPreferences;
			}
			catch (Exception ex)
			{
				throw (new BLException("BL Error encountered getting search preferences.", ex));
			}
		}

		/// <summary>
		/// Save the search preferences for session and member (as applicable).
		/// </summary>
		/// <param name="pMemberID"></param>
		/// <param name="pCommunityID"></param>
		/// <param name="pSearchPreferences"></param>
		public void Save(int pMemberID, int pCommunityID, SearchPreferenceCollection pSearchPreferences)
		{
			try
			{	
				// Trying this as a potential fix for search preferences getting out of synch / invalid after
				// a period of time.
				
				// Update Cached Preferences.
				string cacheKey = SearchPreferencesKey.GetCacheKey(pMemberID, pCommunityID);
				SearchPreferenceCollection cachedPreferences = _cache.Get(cacheKey) as SearchPreferenceCollection;
				if (cachedPreferences != null)
				{	
					_cache.Remove(cacheKey);
				}
			    pSearchPreferences.IsDefaultPreferences = false;
				pSearchPreferences.SetCacheKey = cacheKey;
				CachePreferences(pSearchPreferences);

				SaveMemberPreferences(pMemberID, pCommunityID, pSearchPreferences);
				ReplicationRequested(cachedPreferences);
				PreferencesSaved();				// Raise a PreferencesSaved event.
			}
			catch (SqlException sqlEx)
			{
				throw (new BLException("SQL Error encountered saving search preferences.", sqlEx));
			}
			catch (Exception ex)
			{
				throw (new BLException("BL Error encountered saving search preferences.", ex));
			}
		}

        public PreferenceCollection GetPreferences()
        {
            PreferenceCollection preferences;
            SqlDataReader reader = null;

            try
            {
                preferences = _cache.Get(PreferenceCollection.CACHE_KEY) as PreferenceCollection;

                if (preferences == null)
                {
                    preferences = new PreferenceCollection();

                    Command command = new Command("mnSystem", "up_Preference_List", 0);
                    reader = Client.Instance.ExecuteReader(command);

                    Int32 ordinalPreferenceID = reader.GetOrdinal("PreferenceID");
                    Int32 ordinalAttributeID = reader.GetOrdinal("AttributeID");
                    Int32 ordinalPreferenceTypeID = reader.GetOrdinal("PreferenceTypeID");
                    Int32 ordinalAllowNull = reader.GetOrdinal("AllowNull");

                    while (reader.Read())
                    {
                        Int32 preferenceID = reader.GetInt32(ordinalPreferenceID);
                        Int32 attributeID = reader.GetInt32(ordinalAttributeID);
                        Int32 preferenceTypeID = reader.GetInt32(ordinalPreferenceTypeID);
                        bool allowNull = reader.GetBoolean(ordinalAllowNull);

                        Preference preference = new Preference(preferenceID, attributeID, preferenceTypeID, allowNull);
                        preferences.Add(preference);
                    }

                    _cache.Insert(preferences);
                }
            }
            catch (Exception ex)
            {
                throw new BLException("Error retrieving search preference metadata", ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }

            return preferences;
        }

        public MemberSearchCollection GetMemberSearches(Int32 memberID, Int32 groupID)
        {
            return null;
        }

        public void SaveMemberSearches(MemberSearchCollection searches)
        {
            foreach (MemberSearch memberSearch in searches)
            {
                if (memberSearch.IsDirty)
                {
                    Command command = new Command("mnMember", "up_MemberSearchPreference_Save", memberSearch.MemberID);
                    command.AddParameter("@MemberSearchID", SqlDbType.Int, ParameterDirection.Input, memberSearch.MemberSearchID);
                    command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberSearch.MemberID);
                    command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, memberSearch.GroupID);
                    command.AddParameter("@SearchName", SqlDbType.VarChar, ParameterDirection.Input, memberSearch.SearchName);
                    command.AddParameter("@IsPrimarty", SqlDbType.Bit, ParameterDirection.Input, memberSearch.IsPrimary);
                    Client.Instance.ExecuteAsyncWrite(command);

                    foreach (MemberSearchPreference preference in memberSearch)
                    {
                        if (preference.IsDirty)
                        {
                            Command preferenceCommand = new Command("mnMember", "up_MemberSearchPreference_Save", memberSearch.MemberID);
                            preferenceCommand.AddParameter("@MemberSearchID", SqlDbType.Int, ParameterDirection.Input, preference.MemberSearchID);
                            preferenceCommand.AddParameter("@PreferenceID", SqlDbType.Int, ParameterDirection.Input, preference.PreferenceID);
                            preferenceCommand.AddParameter("@Weight", SqlDbType.Int, ParameterDirection.Input, preference.Weight);
                            preferenceCommand.AddParameter("@Value", SqlDbType.VarChar, ParameterDirection.Input, preference.Value);
                            preferenceCommand.AddParameter("@MinValue", SqlDbType.Int, ParameterDirection.Input, preference.MinValue);
                            preferenceCommand.AddParameter("@MaxValue", SqlDbType.Int, ParameterDirection.Input, preference.MaxValue);
                            Client.Instance.ExecuteAsyncWrite(preferenceCommand);
                        }
                    }
                }
            }
        }

		#endregion
 
		#region Private methods

		private void CachePreferences(SearchPreferenceCollection pPreferences)
		{
			pPreferences.CachePriority = CacheItemPriorityLevel.Normal;
			pPreferences.CacheTTLSeconds = Convert.ToInt32(RuntimeSettings.GetSetting("SEARCHSVC_CACHE_TTL_SVC"));
			_cache.Insert(pPreferences, _expirePreference);
			PreferencesAdded();				// Raise preferences added event.
		}

		private void CachePreferences(SearchPreferenceCollection pPreferences, int pMemberID, int pCommunityID)
		{
			pPreferences.SetCacheKey = SearchPreferencesKey.GetCacheKey(pMemberID, pCommunityID);
			CachePreferences(pPreferences);
		}

		// Event handler for handling CacheItemRemovedCallbacj events from the service cache.
		private void ExpireCallback(string key, object value, CacheItemRemovedReason callbackreason) 
		{
			PreferencesRemoved();		// Raise a PreferencesRemoved event.
			IncrementExpirationCounter();
		}

		private void IncrementExpirationCounter()
		{
			lock (_expiredCountLock)
			{
				_expiredCount++;

				if (_expiredCount > GC_THRESHOLD)
				{
					_expiredCount = 0;
					GC.Collect();
				}
			}
		}

		private SearchPreferenceCollection getPreferences(int pMemberID, int pCommunityID)
		{
			SearchPreferenceCollection searchPreferences = new SearchPreferenceCollection();
			GetMemberSearchPrefs(pMemberID, pCommunityID, ref searchPreferences);
			return searchPreferences;
		}

		private void GetMemberSearchPrefs(int memberID, int communityID, ref SearchPreferenceCollection searchPreferences)
		{
			Command command = new Command("mnMember", "up_SearchPreference_ListMember", memberID);
			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, communityID);
			
			DataTable dt = Client.Instance.ExecuteDataTable(command);
			if (dt.Rows.Count > 0) 
			{
				DataRow row = dt.Rows[0];
				foreach (DataColumn column in dt.Columns) 
				{
                    if (column.ColumnName.ToLower() == "isdefault")
                    {
                        //sets whether the preferences returned are using default preferences (which means the user has no saved preferences)
                        searchPreferences.IsDefaultPreferences = Convert.ToString(row[column]) == "1";
                    }
					else if (column.ColumnName.ToLower() != "memberdomainId") 
					{
						if (row[column] != DBNull.Value) 
						{
							try
							{
								searchPreferences.Add(new SearchPreference(column.ColumnName.ToLower(), Convert.ToString(row[column])));
							}
							catch
							{
								searchPreferences.Add(new SearchPreference(column.ColumnName.ToLower(), Constants.NULL_STRING));
							}
						}
					}
				}
			}
		}

		private void SaveMemberPreferences(int pMemberID, int pCommunityID, SearchPreferenceCollection searchPrefCollection)
		{
		    searchPrefCollection.IsDefaultPreferences = false;
			searchPrefCollection["MemberID"] = pMemberID.ToString();
			searchPrefCollection["GroupID"] = pCommunityID.ToString();

            //set default gendermask if missing
            if (string.IsNullOrEmpty(searchPrefCollection["GenderMask"]))
                searchPrefCollection.Add("GenderMask", "9");

            //set default min/max age if missing
            if (string.IsNullOrEmpty(searchPrefCollection["MinAge"]))
                searchPrefCollection.Add("MinAge", "25");

            if (string.IsNullOrEmpty(searchPrefCollection["MaxAge"]))
                searchPrefCollection.Add("MaxAge", "34");

			Command command = new Command("mnMember", "up_SearchPreference_SaveMember", pMemberID);
			ICollection keys = searchPrefCollection.Keys();

			foreach(string key in keys)
			{
				if (! excludeFromSaveOperation(key) && searchPrefCollection[key] != null && searchPrefCollection[key] != string.Empty)
				{
                    if (key.ToLower() == "keywordsearch")
                    {
                        command.AddParameter("@KeywordSearch", SqlDbType.NVarChar, ParameterDirection.Input, searchPrefCollection[key]);
                    }
                    else
                    {
                        command.AddParameter("@" + key, SqlDbType.Int, ParameterDirection.Input, searchPrefCollection[key]);
                    }
				}
			}
			Client.Instance.ExecuteAsyncWrite(command);

			// Update the SearchPreference table in mnAlert
			Command mnAlertCommand = new Command("mnAlert", "up_SearchPreference_SaveMember", pMemberID);
			keys = searchPrefCollection.Keys();

			foreach(string key in keys)
			{
				if (! excludeFromSaveOperation(key))
				{
                    if (key.ToLower() == "keywordsearch")
                    {
                        mnAlertCommand.AddParameter("@KeywordSearch", SqlDbType.NVarChar, ParameterDirection.Input, searchPrefCollection[key]);
                    }
                    else if (searchPrefCollection[key] != null && searchPrefCollection[key] != string.Empty)
                    {
                        mnAlertCommand.AddParameter("@" + key, SqlDbType.Int, ParameterDirection.Input, searchPrefCollection[key]);
                    }
				}
			}
			Client.Instance.ExecuteAsyncWrite(mnAlertCommand);
		}

        public void ProcessAction(IReplicationAction action)
        {
            switch (action.GetType().Name)
            {
                case "ReplicationActionCollection":
                    foreach (IReplicationAction childAction in (ReplicationActionCollection) action)
                    {
                        ProcessAction(childAction);
                    }
                    break;
                case "ChangeSearchAction":
                    ChangeSearchAction changeSearchAction = action as ChangeSearchAction;
                    SearchPreferencesDB.MemberSearchSave(changeSearchAction.MemberSearchID, changeSearchAction.MemberID,
                        changeSearchAction.GroupID, changeSearchAction.SearchName, changeSearchAction.IsPrimary);
                    break;
                case "DeleteSearchAction":
                    DeleteSearchAction deleteSearchAction = action as DeleteSearchAction;
                    SearchPreferencesDB.MemberSearchDelete(deleteSearchAction.MemberID, deleteSearchAction.MemberSearchID);
                    break;
                case "ChangePreferenceAction":
                    ChangePreferenceAction changePreferenceAction = action as ChangePreferenceAction;
                    SearchPreferencesDB.MemberSearchPreferenceSave(changePreferenceAction.MemberID, changePreferenceAction.MemberSearchID,
                        changePreferenceAction.PreferenceID, changePreferenceAction.Weight, changePreferenceAction.Value,
                        changePreferenceAction.MinValue, changePreferenceAction.MaxValue);
                    break;
                case "DeletePreferenceAction":
                    DeletePreferenceAction deletePreferenceAction = action as DeletePreferenceAction;
                    SearchPreferencesDB.MemberSearchPreferenceDelete(deletePreferenceAction.MemberID, deletePreferenceAction.MemberSearchID,
                        deletePreferenceAction.PreferenceID);
                    break;
                default:
                    throw new Exception("Cannot handle IReplicationAction of type " + action.GetType().Name + ".");
            }
        }

		/// <summary>
		/// This method is used to accomodate area code values for Israel (CountryRegionID = 105)
		/// which are in the format 02, 03, 04, etc. (leading zeros).  When we read them from the 
		/// database (where they're stored as data type int, we must repalce the leading zero.
		/// 
		/// </summary>
		/// <param name="pSearchPreferences"></param>
		private void transformIsraelAreaCodes(SearchPreferenceCollection pSearchPreferences)
		{
			if ("105" != pSearchPreferences["CountryRegionID"])
			{
				return;
			}
			
			// CountryRegionID = 105, Israel
			string areaCodeKey = null;
			int iAreaCode;
			for (int i = 1; i < 7; i++)
			{
				areaCodeKey = "AreaCode" + i;
				if (pSearchPreferences[areaCodeKey] != null
					&& pSearchPreferences[areaCodeKey] != string.Empty)
				{
					// The areaCode value may be equal to Constants.NULL_INT, in this case
					// a leading zero should NOT be prepended.
					iAreaCode = Conversion.CInt(pSearchPreferences[areaCodeKey]);
					if (iAreaCode != Constants.NULL_INT)
					{
						pSearchPreferences[areaCodeKey] = "0" + pSearchPreferences[areaCodeKey];
					}
					else
					{
						pSearchPreferences[areaCodeKey] = string.Empty;
					}
				}
			}
		}

		// This method returns a boolean to indicate whether a search preference in the collection should
		// be excluded from the db save operation.
		private bool excludeFromSaveOperation(string pPreferenceName)
		{
			switch (pPreferenceName.ToLower())
			{
				case "sessionkey":
					return true;

				case "domainid":
                    return true;
				
				case "memberdomainid":
					return true;

				case "geodistance":
					return true;

				case "longitude":
					return true;

				case "latitude":
					return true;

				case "areacodes":
					return true;

				case "regionidcity":
					return true;

				case "searchtype":
					return true;

				case "insertdate":
					return true;

				case "updatedate":
					return true;

                case "colorcode":
                    return true;

                case "searchredesign30":
                    return true;

                case "blockedmemberids":
                    return true;

                case "hasbirthdate":
                case "hasmaritalstatus":
                case "hasjdatereligion":
                case "hasreligion":
                case "haskeepkosher":
                case "hassynagogueattendance":
                case "haseducationlevel":
                case "haschildrencount":
                case "hasmorechildrenflag":
                case "hasbodytype":
                case "haslanguagemask":
                case "hasheight":
                case "hassmokinghabits":
                case "hasactivitylevel":
                case "hascustody":
                case "hasdrinkinghabits":
                case "hasrelocateflag":
                case "hasjdateethnicity":
                case "hasethnicity":
                case "hasmemberid":
                case "isdefault":
                case "excludeynmnoflag":
                case "excludeynmmemberids":
                case "online":
                    return true;
                default:
					return false;
			}
		}

		#endregion
    }
}
