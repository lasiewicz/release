using System;

namespace Matchnet.Search.BusinessLogic
{
	/// <summary>
	/// Summary description for Constants.
	/// </summary>
	public class ServiceConstants
	{
		private ServiceConstants()
		{}

		/// <summary>
		/// Service name (Matchnet.Search.Service)
		/// </summary>
		public const string SERVICE_NAME = "Matchnet.Search.Service";

		/// <summary>
		/// Service Constant (SEARCH_SVC)
		/// </summary>
		public const string SERVICE_CONSTANT = "SEARCH_SVC";

	}
}
