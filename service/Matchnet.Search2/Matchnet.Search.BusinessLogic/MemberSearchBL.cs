using System;
using System.Collections;
using System.Diagnostics;
using System.Text;
using System.Web.Caching;
using System.Linq;
using System.Collections.Generic;

using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.FAST.Query.BusinessLogic;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;
using Matchnet.ActivityRecording.ServiceAdapters;
using Matchnet.ActivityRecording.ValueObjects.Types;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;

namespace Matchnet.Search.BusinessLogic
{
    /// <summary>
    /// Summary description for MemberSearchBL.
    /// </summary>
    public class MemberSearchBL : MarshalByRefObject
    {
        #region Events and Delegates

        /// <summary>
        /// Delegate signature for handling a Replication Requested event.
        /// </summary>
        public delegate void ReplicationEventHandler(IReplicable replicableObject);
        /// <summary>
        /// Event raised when a replication is being requested by this service.
        /// </summary>
        public event ReplicationEventHandler ReplicationRequested;

        /// <summary>
        /// Delegate signature for handling a ResultsAdded event.
        /// </summary>
        public delegate void ResultsAddEventHandler();
        /// <summary>
        /// Event raised when search results are added to the service cache.
        /// </summary>
        public event ResultsAddEventHandler ResultsAdded;

        /// <summary>
        /// Delegate signature for handling a ResultsRemoved event.
        /// </summary>
        public delegate void ResultsRemoveEventHandler();
        /// <summary>
        /// Event raised when search results are removed from the service cache.
        /// </summary>
        public event ResultsRemoveEventHandler ResultsRemoved;

        /// <summary>
        /// Delegate signature for handling a ResultsRequested event.
        /// </summary>
        public delegate void ResultsRequestedEventHandler(bool cacheHit);
        /// <summary>
        /// Event raised when the service is requested to perform a search.
        /// </summary>
        public event ResultsRequestedEventHandler ResultsRequested;

        #endregion

        #region Constants
        private const int GC_THRESHOLD = 10000;
        private const string SERVICE_MANAGER_NAME = "MemberSearchSM";
        private const string FAST_SERVICE_CONSTANT = "FASTQUERY_SVC";
        private const string FAST_SERVICE_MANAGER_NAME = "QuerySM";

        private const int DEFAULT_PAGE_SIZE = 10;
        private const string LOGICAL_DB_SEARCH = "mnSearch";
        private const string MEMBER_ID = "MemberID";
        private const string PARAM_XML = "@XML";
        private const string UP_SEARCH_LIST = "up_Search_List";

        //Preference key constants
        private const string PREF_SEARCH_PARTITION_ID = "SearchPartitionID";
        private const string PREF_COMMUNITY_ID = "DomainID";
        private const string PREF_START_ROW = "StartRow";
        private const string PREF_PAGE_SIZE = "PageSize";
        private const string PREF_SEARCH_TYPE_ID = "SearchTypeID";
        private const string PREF_DISTANCE = "Distance";
        #endregion

        #region Class members

        private Matchnet.Caching.Cache _cache = null;
        private CacheItemRemovedCallback _expireResult;
        private string _expiredCountLock = "";
        private int _expiredCount = 0;
#if DEBUG
        private bool _Use_Nunit = false;
        public bool USE_NUNIT
        {
            get { return _Use_Nunit; }
            set { _Use_Nunit = value; }
        }

        private bool _Nunit_Test_Lucene = false;
        public bool NUNIT_TestLucene
        {
            get { return _Nunit_Test_Lucene; }
            set { _Nunit_Test_Lucene = value; }
        }

        private string _Nunit_DistanceAttribute = string.Empty;
        public string NUNIT_DistanceAttribute
        {
            get { return _Nunit_DistanceAttribute; }
            set { _Nunit_DistanceAttribute = value; }
        }
#endif        
        #endregion

        #region Singleton implementation

        /// <summary>
        /// Singleton instance of the MemberSearchBL class.
        /// </summary>
        public readonly static MemberSearchBL Instance = new MemberSearchBL();

        private MemberSearchBL()
        {
            _cache = Matchnet.Caching.Cache.Instance;
            _expireResult = new CacheItemRemovedCallback(this.ExpireCallback);
        }

        #endregion

        #region BL Implementation

        /// <summary>
        /// Inserts the supplied replicable object into the local cache.
        /// </summary>
        /// <param name="pReplicableObject"></param>
        public void CacheReplicatedObject(IReplicable pReplicableObject)
        {
            switch (pReplicableObject.GetType().Name)
            {
                case "MatchnetQueryResults":
                    CacheResults((MatchnetQueryResults)pReplicableObject);
                    break;
                default:
                    throw new Exception("invalid replication object type received (" + pReplicableObject.GetType().ToString() + ")");
            }
        }

        /// <summary>
        /// Executes a member search based on the search preferences, community and site ID's.
        /// </summary>
        /// <param name="pPreferences"></param>
        /// <param name="pCommunityID"></param>
        /// <param name="pSiteID"></param>
        /// <returns></returns>
        public IMatchnetQueryResults Search(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID, bool ignoreCache)
        {
            bool cacheHit = true;
           // System.Diagnostics.Trace.Write("In search service");
           // System.Diagnostics.Trace.Write(pPreferences.ToString());
            MatchnetQueryResults queryResults = null;

            if (!ignoreCache)
            {
                queryResults = _cache.Get(ResultsKey.GetCacheKey(pPreferences, pCommunityID, pSiteID)) as MatchnetQueryResults;
                // System.Diagnostics.Trace.Write("CacheKey:" + ResultsKey.GetCacheKey(pPreferences, pCommunityID, pSiteID));
            }

            if (queryResults == null)
            {
             //   System.Diagnostics.Trace.Write("No results in cache for " + ResultsKey.GetCacheKey(pPreferences, pCommunityID, pSiteID));
                cacheHit = false;

                queryResults = ExecuteSearch(pPreferences, pCommunityID, pSiteID);

                queryResults.CacheKey = ResultsKey.GetCacheKey(pPreferences, pCommunityID, pSiteID);
               // System.Diagnostics.Trace.Write("Caching results: " + queryResults.CacheKey);
                if (cacheResultsEnabled(pPreferences))
                {
                    CacheResults(queryResults);
                    ReplicationRequested(queryResults);
                }
            }
            ResultsRequested(cacheHit);
            return queryResults;
        }

        public IMatchnetQueryResults SearchForPushFlirtMatches(SearchPreferenceCollection preferences, int memberID, int communityID, int siteID, int brandID, int numMatchesToReturn)
        {

            MatchnetQueryResults queryResult = null;
            List<int> returnList = new List<int>();
            int count = 0;

            try
            {
                int daysForSubExpirationFactor =
                    Convert.ToInt32(RuntimeSettings.GetSetting("FILTER_PUSH_FLIRTS_BY_SUBEXPIRATIONDATE_FACTOR",
                                                               communityID, siteID, brandID));
                int daysForRegistrationFactor =
                    Convert.ToInt32(RuntimeSettings.GetSetting("FILTER_PUSH_FLIRTS_BY_REGDATE_FACTOR", communityID,
                                                               siteID, brandID));
                bool prioritizeSubExpiration =
                    Convert.ToBoolean(RuntimeSettings.GetSetting("FILTER_PUSH_FLIRTS_BY_SUBEXPIRATIONDATE", communityID,
                                                                 siteID, brandID));
                bool prioritizeRegistration =
                    Convert.ToBoolean(RuntimeSettings.GetSetting("FILTER_PUSH_FLIRTS_BY_REGDATE", communityID, siteID,
                                                                 brandID));
                List<int> registrationMembers = null;
                List<int> expiringMembers = null;

                List.ServiceAdapters.List list = ListSA.Instance.GetList(memberID, ListLoadFlags.IngoreSACache);

                List<int> flirted =
                    convertToGeneric(list.GetListMembers(HotListCategory.MembersYouTeased, communityID, siteID, 1, 1000,
                                                         out count));
                List<int> emailed =
                    convertToGeneric(list.GetListMembers(HotListCategory.MembersYouEmailed, communityID, siteID, 1, 1000,
                                                         out count));
                List<int> sentEcard =
                    convertToGeneric(list.GetListMembers(HotListCategory.MembersYouSentECards, communityID, siteID, 1,
                                                         1000, out count));
                List<int> emailedAllAccess =
                    convertToGeneric(list.GetListMembers(HotListCategory.MembersYouSentAllAccessEmail, communityID,
                                                         siteID, 1, 1000, out count));
                List<int> IMd =
                    convertToGeneric(list.GetListMembers(HotListCategory.MembersYouIMed, communityID, siteID, 1, 1000,
                                                         out count));
                List<int> alreadySeen =
                    convertToGeneric(list.GetListMembers(HotListCategory.MembersYouViewedInPushFlirts, communityID,
                                                         siteID, 1, 1000, out count));

                List<int> ignored = convertToGeneric(list.GetListMembers(HotListCategory.IgnoreList, communityID,
                                                         siteID, 1, 1000, out count));

                List<int> allIneligibleMembers =
                    flirted.Union(emailed).Union(sentEcard).Union(emailedAllAccess).Union(IMd).Union(alreadySeen).Union(ignored).
                    ToList();

                bool blockingEnabled =
                    Convert.ToBoolean(RuntimeSettings.GetSetting("PROFILE_BLOCK_ENABLED", communityID));

                if (blockingEnabled)
                {
                    List<int> blocked =
                        convertToGeneric(list.GetListMembers(HotListCategory.ExcludeListInternal, communityID, siteID, 1,
                                                             1000, out count));

                    List<int> blocked2 =
                        convertToGeneric(list.GetListMembers(HotListCategory.ExcludeFromList, communityID, siteID, 1,
                                                             1000, out count));
                    allIneligibleMembers = allIneligibleMembers.Union(blocked).Union(blocked2).ToList();
                }

                //all results must have photos
                preferences.Add("HasPhotoFlag", "1");

                ArrayList initialResults = SearchExplainDetailed(preferences, communityID, siteID);
                List<DetailedQueryResult> eligibleResults =
                    (from DetailedQueryResult result in initialResults
                     where !(allIneligibleMembers).Contains(result.MemberID)
                     select result).ToList();

                if (prioritizeSubExpiration)
                {
                    expiringMembers = getExpiringMembers(eligibleResults, daysForSubExpirationFactor);
                    returnList.AddRange(expiringMembers.Take(numMatchesToReturn));
                }
                if (prioritizeRegistration && returnList.Count < numMatchesToReturn)
                {
                    registrationMembers = getRecentlyRegisteredMembers(eligibleResults, daysForRegistrationFactor);
                    if (prioritizeSubExpiration)
                    {
                        registrationMembers = registrationMembers.Except(expiringMembers).ToList();
                    }
                    returnList.AddRange(registrationMembers.Take(numMatchesToReturn - returnList.Count));
                }

                if (returnList.Count < numMatchesToReturn)
                {
                    List<int> remainingMembers =
                        (from DetailedQueryResult result in eligibleResults
                         where !(returnList).Contains(result.MemberID)
                         select result.MemberID).Take(numMatchesToReturn - returnList.Count).ToList();
                    returnList.AddRange(remainingMembers);
                }

                //if we didn't find all the matches we need, return none
                if (returnList.Count < numMatchesToReturn) returnList.Clear();

                //finally, convert to IMatchnetQueryResults object
                queryResult = new MatchnetQueryResults(returnList.Count);
                queryResult.MatchesFound = returnList.Count;
                queryResult.MoreResultsAvailable = false;

                foreach (int ID in returnList)
                {
                    queryResult.AddResult(ID.ToString());
                }
            }
            catch(Exception ex)
            {
                string exceptionMessage = ex.Message;
                throw (new BLException("Error occurred while attempting to execute SearchForPushFlirtMatches." , ex));
            }

            return queryResult;

        }

        private List<int> convertToGeneric(ArrayList list)
        {
            List<int> converted = new List<int>();
            foreach (int i in list)
            {
                converted.Add(i);
            }

            return converted;
        }

        private List<int> getExpiringMembers(List<DetailedQueryResult> mems, int daysForSubExpirationFactor)
        {
            List<DetailedQueryResult> e = (from DetailedQueryResult r in mems where r.Tags.ContainsKey("subscriptionexpirationdate") select r).ToList();
            List<DetailedQueryResult> i = (from r in e where Convert.ToDateTime(r.Tags["subscriptionexpirationdate"]) >= DateTime.Now && Convert.ToDateTime(r.Tags["subscriptionexpirationdate"]) <= DateTime.Now.AddDays(daysForSubExpirationFactor) select r).ToList();

            i.Sort(delegate(DetailedQueryResult mem1, DetailedQueryResult mem2)
            {
                return Convert.ToDateTime(mem1.Tags["subscriptionexpirationdate"]).CompareTo(Convert.ToDateTime(mem2.Tags["subscriptionexpirationdate"]));
            });

            return (from DetailedQueryResult r in i orderby Convert.ToDateTime(r.Tags["subscriptionexpirationdate"]) ascending select r.MemberID).ToList();
        }

        private List<int> getRecentlyRegisteredMembers(List<DetailedQueryResult> mems, int daysForRegistrationFactor)
        {
            List<DetailedQueryResult> regMembers = (from DetailedQueryResult r in mems where r.RegisterDate >= DateTime.Now.AddDays(-daysForRegistrationFactor) select r).ToList();

            regMembers.Sort(delegate(DetailedQueryResult mem1, DetailedQueryResult mem2)
            {
                return Convert.ToDateTime(mem1.RegisterDate).CompareTo(Convert.ToDateTime(mem2.RegisterDate));
            });

            return (from DetailedQueryResult r in regMembers orderby r.RegisterDate ascending select r.MemberID).ToList();
        }

        public ArrayList SearchDetailed(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID)
        {
            return ExecuteSearchDetailed(pPreferences, pCommunityID, pSiteID);
        }


        #endregion

        #region Private methods

        private void CacheResults(MatchnetQueryResults pQueryResults)
        {
            pQueryResults.CachePriority = CacheItemPriorityLevel.Normal;
            pQueryResults.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SEARCHSVC_CACHE_TTL_SVC"));
            _cache.Insert(pQueryResults, _expireResult);
            ResultsAdded();
        }

        private MatchnetQueryResults ExecuteSearch(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID)
        {
            string uri = null;
            MatchnetQueryResults results = null;
            IMatchnetQuery query = null;
            string exceptionMessage = string.Empty;
            CallingSystem callingSystem = CallingSystem.E2Search;
            Stopwatch stopWatch = new Stopwatch();
            try
            {
                // Transform search preferences into an IMatchnetQuery
                query = MatchnetQueryBuilder.build(pPreferences, pCommunityID, pSiteID);

#if DEBUG
				Trace.WriteLine("Matchnet.Search.Service", logSearchParams(query, pCommunityID, pSiteID));
#endif

                stopWatch.Start();
                results = Spark.SearchEngine.ServiceAdapters.SearcherSA.Instance.RunQuery(query, pCommunityID) as MatchnetQueryResults;
                stopWatch.Stop();
            }
            catch (ExceptionBase ex)
            {
                exceptionMessage = ex.Message;
                throw (new SAException("Error occurred while attempting to execute member search against E2. (uri: " + uri + ")", ex));
            }
            catch (Exception ex)
            {
                exceptionMessage = ex.Message;
                throw (new SAException("Error occurred while attempting to execute member search against E2. (uri: " + uri + ")", ex));
            } 
            finally {
                try
                {
                    long elapsedMillis = -1;
                    if (null != stopWatch)
                    {
                        stopWatch.Stop();
                        elapsedMillis = stopWatch.ElapsedMilliseconds;
                    }

                    int memberId = (pPreferences.ContainsKey("MemberId") && !string.IsNullOrEmpty(pPreferences["MemberId"])) ? Convert.ToInt32(pPreferences["MemberId"]) : int.MinValue;
                    LogActivity(memberId, pCommunityID, pSiteID, ((null != results) ? results.Items.List : null), query, exceptionMessage, callingSystem, elapsedMillis);
                }
                catch (Exception activityLogException)
                {
                    EventLog.WriteEntry(ServiceConstants.SERVICE_CONSTANT, "Activity Log failed! " + activityLogException.Message, EventLogEntryType.Warning);
                }
            }

            return results;
        }

        private ArrayList ExecuteSearchDetailed(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID)
        {
            string uri = null;
            ArrayList results = null;
            IMatchnetQuery query = null;
            string exceptionMessage = string.Empty;
            CallingSystem callingSystem = CallingSystem.E2Search;
            Stopwatch stopWatch = new Stopwatch();
            try
            {
                // Transform search preferences into an IMatchnetQuery
                query = MatchnetQueryBuilder.build(pPreferences, pCommunityID, pSiteID);
                stopWatch.Start();
                results = Spark.SearchEngine.ServiceAdapters.SearcherSA.Instance.RunDetailedQuery(query, pCommunityID);
            }
            catch (Exception ex)
            {
                exceptionMessage = ex.Message;
                throw ex;
            }
            finally
            {
                try
                {
                    long elapsedMillis = -1;
                    if (null != stopWatch)
                    {
                        stopWatch.Stop();
                        elapsedMillis = stopWatch.ElapsedMilliseconds;
                    }

                    int memberId = (pPreferences.ContainsKey("MemberId") && !string.IsNullOrEmpty(pPreferences["MemberId"])) ? Convert.ToInt32(pPreferences["MemberId"]) : int.MinValue;
                    LogActivity(memberId, pCommunityID, pSiteID, results, query, exceptionMessage, callingSystem, elapsedMillis);
                }
                catch (Exception activityLogException)
                {
                    EventLog.WriteEntry(ServiceConstants.SERVICE_CONSTANT, "Activity Log failed! " + activityLogException.Message, EventLogEntryType.Warning);
                }
            }
            return results;
        }

        public ArrayList SearchExplainDetailed(SearchPreferenceCollection pPreferences, int pCommunityID, int pSiteID)
        {
            string uri = null;
            try
            {
                // Transform search preferences into an IMatchnetQuery
                IMatchnetQuery query = MatchnetQueryBuilder.build(pPreferences, pCommunityID, pSiteID);
                return Spark.SearchEngine.ServiceAdapters.SearcherSA.Instance.RunDetailedQuery(query, pCommunityID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string logSearchParams(IMatchnetQuery pQuery, int pCommunityID, int pSiteID)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("MemberSearchBL.ExecuteFASTSearch(...)" + System.Environment.NewLine
                + "CommunityID = " + pCommunityID + System.Environment.NewLine
                + "SiteID = " + pSiteID + System.Environment.NewLine
                + "SearchTypeID = " + pQuery.SearchType + System.Environment.NewLine
                + "========================================================" + System.Environment.NewLine);

            for (int i = 0; i < pQuery.Count; i++)
            {
                sb.Append("Param: " + pQuery[i].Parameter + " = " + pQuery[i].Value + System.Environment.NewLine);
            }
            return sb.ToString();
        }

        private bool cacheResultsEnabled(SearchPreferenceCollection pPreferences)
        {
            bool isEnabled = false;

            if (pPreferences["SearchType"] != string.Empty)
            {
                SearchType searchType = (SearchType)Conversion.CInt(pPreferences["SearchType"]);
                if (searchType.Equals(SearchType.MatchMail))
                {	// check configuration setting for MatchMail caching
                    isEnabled = Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SEARCHSVC_MT_CACHING_MATCHMAIL"));
                }
                else
                {	// check configuration setting for WebSearch caching
                    isEnabled = Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SEARCHSVC_MT_CACHING_WEBSEARCH"));
                }
            }
            return isEnabled;
        }

        private void ExpireCallback(string key, object value, CacheItemRemovedReason callbackreason)
        {
            ResultsRemoved();
            IncrementExpirationCounter();
        }

        private void IncrementExpirationCounter()
        {
            lock (_expiredCountLock)
            {
                _expiredCount++;

                if (_expiredCount > GC_THRESHOLD)
                {
                    _expiredCount = 0;
                    GC.Collect();
                }
            }
        }

        private void AddBirthDate(ref SearchPreferenceCollection pSearchPreferences)
        {
            int minAge = DefaultIntValue(pSearchPreferences["MinAge"].ToString(), Matchnet.Constants.NULL_INT);
            int maxAge = DefaultIntValue(pSearchPreferences["MaxAge"].ToString(), Matchnet.Constants.NULL_INT);
            DateTime now = DateTime.Now;
            DateTime min = now.AddYears(-(maxAge + 1));
            min.AddMinutes(1);
            DateTime max = now.AddYears(-minAge);

            pSearchPreferences["BirthDate"] = min.ToShortDateString() + "," + max.ToShortDateString();
        }

        private int DefaultIntValue(string val, int defaultVal)
        {
            if (val == null || val == string.Empty)
            {
                return defaultVal;
            }
            try
            {
                return System.Convert.ToInt32(val);
            }
            catch
            {
                return defaultVal;
            }
        }

        public static string GetSetting(string name, int communityid, string defaultvalue)
        {
            try
            {
                return RuntimeSettings.GetSetting(name, communityid);

            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(ServiceConstants.SERVICE_CONSTANT, string.Format("Missing setting with name={0} for community={1}. Using default value.",name,communityid), EventLogEntryType.Warning);
                return defaultvalue;
            }
        }

        private void LogActivity(int memberId, int pCommunityID, int pSiteID, ICollection results, IMatchnetQuery query, string exceptionMessage, CallingSystem callingSystem, long elapsedMillis)
        {
            StringBuilder xmlForLog = XmlForLog(pCommunityID, results, query, exceptionMessage, elapsedMillis);
            ActivityRecordingSA.Instance.RecordActivity(memberId, int.MinValue, pSiteID, ActionType.E2Search, callingSystem, xmlForLog.ToString());
        }

        public StringBuilder XmlForLog(int pCommunityID, ICollection results, IMatchnetQuery query, string exceptionMessage, long elapsedMillis)
        {
            bool logMemberResultSet = Boolean.TrueString.ToLower().Equals(GetSetting("SEARCH_LOG_MEMBER_RESULT_SET", pCommunityID, Boolean.FalseString.ToLower()).ToLower());
            bool logBlockedMemberIds = Boolean.TrueString.ToLower().Equals(GetSetting("SEARCH_LOG_BLOCKED_MEMBER_IDS", pCommunityID, Boolean.FalseString.ToLower()).ToLower());
            StringBuilder xmlForLog = new StringBuilder("<Search><DebugData>");
            xmlForLog.Append(string.Format("<SearchType>{0}</SearchType>", query.SearchType.ToString()));
            if (null != results)
            {
                xmlForLog.Append(string.Format("<MatchesFound>{0}</MatchesFound>", results.Count));
            }
            xmlForLog.Append(string.Format("<TimeStamp>{0}</TimeStamp>", DateTime.Now));
            xmlForLog.Append(string.Format("<SearchTimeMS>{0}</SearchTimeMS>", elapsedMillis));
            bool hasException = !string.IsNullOrEmpty(exceptionMessage);
            xmlForLog.Append(string.Format("<HasException>{0}</HasException>", hasException));
            xmlForLog.Append(string.Format("<HasMemberResultSet>{0}</HasMemberResultSet>", logMemberResultSet));
            xmlForLog.Append("</DebugData><SearchParameters>");
            for (int i = 0; i < query.Count; i++)
            {
                if (QuerySearchParam.GeoDistance.Equals(query[i].Parameter))
                {
                    string distanceAttribute = (Boolean.TrueString.ToLower().Equals(GetSetting("USE_E2_DISTANCE", pCommunityID, "false"))) ? "E2Distance" : "Distance";
                    if (Boolean.TrueString.ToLower().Equals(GetSetting("USE_MINGLE_DISTANCE", pCommunityID, "false")))
                    {
                        distanceAttribute = "MingleDistance";
                    }
#if DEBUG
                    if (USE_NUNIT)
                    {
                        if (!string.IsNullOrEmpty(NUNIT_DistanceAttribute))
                        {
                            distanceAttribute = NUNIT_DistanceAttribute;
                        }
                    }
#endif
                    xmlForLog.Append(string.Format("<{0}>{1}</{0}>", distanceAttribute, query[i].Value.ToString()));
                }
                else
                {
                    // if flag is turned off, don't log blocked member ids
                    if (QuerySearchParam.BlockedMemberIDs.Equals(query[i].Parameter) && !logBlockedMemberIds)
                    {
                        continue;
                    }
                    xmlForLog.Append(string.Format("<{0}>{1}</{0}>", query[i].Parameter.ToString(), query[i].Value.ToString()));
                }
            }

            xmlForLog.Append("</SearchParameters>");
            if (logMemberResultSet && null != results)
            {
                xmlForLog.Append("<MemberResultSet>");
                foreach (IMatchnetResultItem result in results)
                {
                    xmlForLog.Append(string.Format("<Id>{0}</Id>", result.MemberID));
                }
                xmlForLog.Append("</MemberResultSet>");
            }
            if (hasException)
            {
                xmlForLog.Append(string.Format("<Exception><ExceptionMessage>{0}</ExceptionMessage></Exception>",
                                               exceptionMessage));
            }
            xmlForLog.Append("</Search>");
            return xmlForLog;
        }

        #endregion
    }
}
