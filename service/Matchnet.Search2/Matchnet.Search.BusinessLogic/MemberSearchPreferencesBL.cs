﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Caching;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Data;
using Matchnet.Exceptions;
using Spark.SearchPreferences;
using Cache = Matchnet.Caching.Cache;

namespace Matchnet.Search.BusinessLogic
{
    public class MemberSearchPreferencesBL
    {
        private const int GC_THRESHOLD = 10000;

        #region Class members

        private Caching.Cache _cache = null;
        private CacheItemRemovedCallback _expirePreference;
        private string _expiredCountLock = "";
        private int _expiredCount = 0;

        #endregion

        		#region Singleton implementation

		/// <summary>
		/// Singleton instance of the SearchPreferencesBL class.
		/// </summary>
		public static readonly MemberSearchPreferencesBL Instance = new MemberSearchPreferencesBL();

		/// <summary>
		/// Constructor - singleton pattern.
		/// </summary>
        private MemberSearchPreferencesBL()
		{
			_cache = Cache.Instance;
			_expirePreference = new CacheItemRemovedCallback(this.ExpireCallback);
		}

		#endregion

        #region Delegates and Events

        /// <summary>
        /// Delegate signature for handling a Replication Requested event.
        /// </summary>
        public delegate void ReplicationEventHandler(IReplicable replicableObject);

        /// <summary>
        /// Event raised when a replication is being requested by this service.
        /// </summary>
        public event ReplicationEventHandler ReplicationRequested;

        /// <summary>
        /// Delegate signature for handling a PreferencesAdded event.
        /// </summary>
        public delegate void PreferencesAddEventHandler();
        /// <summary>
        /// Event raised when search preferences are added to the service cache.
        /// </summary>
        public event PreferencesAddEventHandler PreferencesAdded;

        /// <summary>
        /// Delegate signature for handling a PreferencesRemoved event.
        /// </summary>
        public delegate void PreferencesRemoveEventHandler();
        /// <summary>
        /// Event raised when search preferences are removed from the service cache.
        /// </summary>
        public event PreferencesRemoveEventHandler PreferencesRemoved;

        /// <summary>
        /// Delegate signature for handling a PreferencesRequested event.
        /// </summary>
        public delegate void PreferencesRequestedEventHandler(bool cacheHit);
        /// <summary>
        /// Event raised when the service is requested to return search preferences
        /// </summary>
        public event PreferencesRequestedEventHandler PreferencesRequested;

        /// <summary>
        /// Delegate signature for handling a PreferencesSaved event.
        /// </summary>
        public delegate void PreferencesSavedEventHandler();
        /// <summary>
        /// Event raised when the service is requested to save a set of search preferences.
        /// </summary>
        public event PreferencesSavedEventHandler PreferencesSaved;

        #endregion

        public void CacheReplicatedObject(IReplicable replicableObject)
        {
            switch (replicableObject.GetType().Name)
            {
                case "MemberSearchCollection":
                    CachePreferences((MemberSearchCollection)replicableObject);
                    break;
                default:
                    throw new Exception("invalid replication object type received (" + replicableObject.GetType().ToString() + ")");
            }
        }

        private void CachePreferences(MemberSearchCollection memberSearchCollection)
        {
            memberSearchCollection.CachePriority = CacheItemPriorityLevel.Normal;
            memberSearchCollection.CacheTTLSeconds = Convert.ToInt32(RuntimeSettings.GetSetting("SEARCHSVC_CACHE_TTL_SVC"));
            _cache.Insert(memberSearchCollection, _expirePreference);
            PreferencesAdded();				// Raise preferences added event.
        }

        private void CachePreferences(MemberSearchCollection memberSearchCollection, int memberID, int communityID)
        {
            memberSearchCollection.SetCacheKey = MemberSearchCollectionKey.GetCacheKey(memberID, communityID);
            CachePreferences(memberSearchCollection);
        }

        // Event handler for handling CacheItemRemovedCallbacj events from the service cache.
        private void ExpireCallback(string key, object value, CacheItemRemovedReason callbackreason)
        {
            PreferencesRemoved();		// Raise a PreferencesRemoved event.
            IncrementExpirationCounter();
        }

        private void IncrementExpirationCounter()
        {
            lock (_expiredCountLock)
            {
                _expiredCount++;

                if (_expiredCount > GC_THRESHOLD)
                {
                    _expiredCount = 0;
                    GC.Collect();
                }
            }
        }

        public void SaveMemberSearchCollection(MemberSearchCollection memberSearchCollection, int memberId, int communityId)
        {
            // Update Cached Preferences.
            string cacheKey = MemberSearchCollectionKey.GetCacheKey(memberId, communityId);
            var cachedSearches = _cache.Get(cacheKey) as MemberSearchCollection;
            if (cachedSearches != null)
            {
                _cache.Remove(cacheKey);
            }
            
            memberSearchCollection.SetCacheKey = cacheKey;
            CachePreferences(memberSearchCollection, memberId, communityId);
            
            foreach(MemberSearch memberSearch in memberSearchCollection)
            {
                SaveMemberSearch(memberSearch);
            }

            ReplicationRequested(memberSearchCollection);
            PreferencesSaved();				// Raise a PreferencesSaved event.
        }

        public void SaveMemberSearch(MemberSearch memberSearch)
        {
            try
            {

                Command command = new Command("mnMember", "up_MemberSearch_Save", memberSearch.MemberID);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberSearch.MemberID);
                command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, memberSearch.GroupID);
                command.AddParameter("@SearchName", SqlDbType.VarChar, ParameterDirection.Input, memberSearch.SearchName);
                command.AddParameter("@IsPrimary", SqlDbType.Bit, ParameterDirection.Input, memberSearch.IsPrimary);
                command.AddParameter("@Preferences", SqlDbType.NVarChar, ParameterDirection.Input, memberSearch.SearchPreferenceCollection.SerializeToXML());

                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch(Exception ex)
            {
                
            }
        }

        public MemberSearchCollection GetMemberSearchCollection(int memberID, int communityID)
        {
            MemberSearchCollection memberSearchCollection = null;
            try
            {
                bool cacheHit = true;
                memberSearchCollection = _cache.Get(MemberSearchCollectionKey.GetCacheKey(memberID, communityID)) as MemberSearchCollection;

                if (memberSearchCollection == null)
                {
                    cacheHit = false;

                    Command command = new Command("mnMember", "up_MemberSearch_List", memberID);
                    command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                    command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, communityID);

                    DataTable dt = Client.Instance.ExecuteDataTable(command);

                    if (dt.Rows.Count > 0)
                    {
                        memberSearchCollection = new MemberSearchCollection();
                        memberSearchCollection.AddRange((from DataRow dr in dt.Rows select DataRowToMemberSearch(dr)).ToList());

                        CachePreferences(memberSearchCollection, memberID, communityID);
                        ReplicationRequested(memberSearchCollection);
                    }

                    // Handle transformation of IsraelAreaCodes (zero padding issue)
                    //transformIsraelAreaCodes(searchPreferences);
                }

                PreferencesRequested(cacheHit);		// Raise PreferencesRequested event.
                return memberSearchCollection;
            }
            catch (Exception ex)
            {
                throw (new BLException("BL Error encountered getting member search preferences.", ex));
            }
        }

        private MemberSearch DataRowToMemberSearch(DataRow dr)
        {
            int memberID = Conversion.CInt(dr["MemberID"].ToString());
            int groupID = Conversion.CInt(dr["GroupID"].ToString());
            string searchName = dr["SearchName"].ToString();
            bool isPrimary = Conversion.CBool(dr["IsPrimary"].ToString());
            string preferencesXML = dr["Preferences"].ToString();

            var searchPreferenceCollection = SearchPreferenceCollection.DeserializeFromXML(preferencesXML);

            return new MemberSearch(memberID, groupID, searchName, isPrimary, searchPreferenceCollection);
        }


    }
}
