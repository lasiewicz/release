using System;

namespace Matchnet.FAST.Query.ValueObjects
{
	/// <summary>
	/// Summary description for Constants.
	/// </summary>
	public class Constants
	{
		/// <summary> </summary>
		public const string EVENT_LOG_SOURCE_QUERY = "Matchnet.FAST.Query";
		/// <summary> </summary>
		public const string COUNTER_CATEGORY_QUERY = "Matchnet.FAST.Query";

	}
}
