using System;
using System.Diagnostics;


namespace Matchnet.FAST.Query.ValueObjects {
	/// <summary>
	/// 
	/// </summary>
	public class Metrics {


		#region counter instances
		

		/// <summary> </summary>
		public static PerformanceCounter pcQueriesTotal;
		/// <summary> </summary>
		public static PerformanceCounter pcErrorsTotal;
		/// <summary> </summary>
		public static PerformanceCounter pcQueriesPerSec;
		/// <summary> </summary>
		public static PerformanceCounter pcQueryTime;
		/// <summary> </summary>
		public static PerformanceCounter pcQueryTimeBase;
		/// <summary> </summary>
		public static PerformanceCounter pcResultCount;
		/// <summary> </summary>
		public static PerformanceCounter pcQueriesReturnedEmpty;
		/// <summary> </summary>
		public static PerformanceCounter pcTimedOutQueries;
		/// <summary> </summary>
		public static PerformanceCounter pcInputTermCount;
		/// <summary> </summary>
		public static PerformanceCounter pcFASTQueryTime;
		/// <summary> </summary>
		public static PerformanceCounter pcFASTQueryTimeBase;
		/// <summary> </summary>
		public static PerformanceCounter pcFASTMatches;
		#endregion


		/// <summary>
		/// The single instance of this singleteon object
		/// </summary>
		public static readonly Metrics Instance = new Metrics();


		private Metrics() {
			if (PerformanceCounterCategory.Exists(Constants.COUNTER_CATEGORY_QUERY)){
				pcQueriesTotal = new PerformanceCounter(Constants.COUNTER_CATEGORY_QUERY,"Queries Total",false);
				pcErrorsTotal = new PerformanceCounter(Constants.COUNTER_CATEGORY_QUERY,"Errors Total",false);
				pcQueriesPerSec = new PerformanceCounter(Constants.COUNTER_CATEGORY_QUERY,"Queries/Sec",false);

				pcQueryTime = new PerformanceCounter(Constants.COUNTER_CATEGORY_QUERY,"Query Time",false);
				pcQueryTimeBase = new PerformanceCounter(Constants.COUNTER_CATEGORY_QUERY,"Query Time Base",false);

				pcResultCount = new PerformanceCounter(Constants.COUNTER_CATEGORY_QUERY,"Result Count",false);
				pcQueriesReturnedEmpty = new PerformanceCounter(Constants.COUNTER_CATEGORY_QUERY,"Queries Returned Empty",false);
				pcTimedOutQueries = new PerformanceCounter(Constants.COUNTER_CATEGORY_QUERY,"Timed Out Queries",false);		
				pcInputTermCount = new PerformanceCounter(Constants.COUNTER_CATEGORY_QUERY,"Input Term Count",false);

				pcFASTQueryTime = new PerformanceCounter(Constants.COUNTER_CATEGORY_QUERY,"FAST query time",false);
				pcFASTQueryTimeBase = new PerformanceCounter(Constants.COUNTER_CATEGORY_QUERY,"FAST query time base",false);

				pcFASTMatches = new PerformanceCounter(Constants.COUNTER_CATEGORY_QUERY,"FAST Matches",false);
			}
			else {
				throw new Exception("Unable to instantiate counters. PerformanceCounterCategory does not exist");
			}
			
		}

		/// <summary>
		/// Adds an information type event log entry
		/// </summary>
		/// <param name="message">message content</param>
		public void WriteInformation(string message) {
			EventLog.WriteEntry(Constants.EVENT_LOG_SOURCE_QUERY, message, EventLogEntryType.Information);
		}


		/// <summary>
		/// Adds an error type event log entry
		/// </summary>
		/// <param name="message">message content</param>
		public void WriteError(string message) {
			EventLog.WriteEntry(Constants.EVENT_LOG_SOURCE_QUERY , message, EventLogEntryType.Error);
		}

		/// <summary>
		/// Reset all performance counters.
		/// </summary>
		public void ResetCounters(){
			// initialize values
			pcQueriesTotal.RawValue = 0L;
			pcErrorsTotal.RawValue = 0L;
			pcQueriesPerSec.RawValue = 0L;
			pcQueryTime.RawValue = 0L;
			pcQueryTimeBase.RawValue = 0L;
			pcResultCount.RawValue = 0L;
			pcQueriesReturnedEmpty.RawValue = 0L;
			pcTimedOutQueries.RawValue = 0L;
			pcInputTermCount.RawValue = 0L;
			pcFASTQueryTime.RawValue = 0L;
			pcFASTQueryTimeBase.RawValue = 0L;
			pcFASTMatches.RawValue = 0L;
		}

	}
	/// <summary>
	/// 
	/// </summary>
	public class MetricsInstaller{

		/// <summary>
		/// 
		/// </summary>
		public static void Install(){
			Debug.WriteLine(Constants.COUNTER_CATEGORY_QUERY);
			if (!PerformanceCounterCategory.Exists(Constants.COUNTER_CATEGORY_QUERY)){
				CounterCreationDataCollection CountersToCreate = new CounterCreationDataCollection();
				CountersToCreate.Add(new CounterCreationData("Queries Total","# of queries submitted",PerformanceCounterType.NumberOfItems32));
				CountersToCreate.Add(new CounterCreationData("Errors Total","# of query errors encountered",PerformanceCounterType.NumberOfItems32));
				CountersToCreate.Add(new CounterCreationData("Queries/Sec","# of query submitted per second",PerformanceCounterType.RateOfCountsPerSecond32));

				CountersToCreate.Add(new CounterCreationData("Query Time","Time taken for query round trip",PerformanceCounterType.AverageCount64));
				CountersToCreate.Add(new CounterCreationData("Query Time Base","base",PerformanceCounterType.AverageBase));

				CountersToCreate.Add(new CounterCreationData("Result Count","# of results returned in query",PerformanceCounterType.NumberOfItems32));
				CountersToCreate.Add(new CounterCreationData("Queries Returned Empty","# of queries retuned with no results",PerformanceCounterType.NumberOfItems32));
				CountersToCreate.Add(new CounterCreationData("Timed Out Queries","# of queries that timed out en route to FAST",PerformanceCounterType.NumberOfItems32));
				CountersToCreate.Add(new CounterCreationData("Input Term Count","# of queries terms in submitted Matchnet query",PerformanceCounterType.NumberOfItems32));
				CountersToCreate.Add(new CounterCreationData("FAST query time","FAST reported query time",PerformanceCounterType.AverageCount64));
				CountersToCreate.Add(new CounterCreationData("FAST query time base","base",PerformanceCounterType.AverageBase));

				CountersToCreate.Add(new CounterCreationData("FAST Matches","FAST reported matching documents. Only first N are actually returned",PerformanceCounterType.NumberOfItems32));



				// Create the category and pass the collection to it.
				PerformanceCounterCategory.Create(Constants.COUNTER_CATEGORY_QUERY, "Counters for the service that runs a Matchnet Query agains the FAST system", CountersToCreate);
			}

			// Event log prep
			if(!EventLog.SourceExists(Constants.EVENT_LOG_SOURCE_QUERY)){
				EventLog.CreateEventSource( Constants.EVENT_LOG_SOURCE_QUERY ,"Application");
				Debug.WriteLine("Created Event Source -> " + Constants.EVENT_LOG_SOURCE_QUERY  );
			}

		}

		/// <summary>
		/// 
		/// </summary>
		public static void Uninstall(){
			if (PerformanceCounterCategory.Exists(Constants.COUNTER_CATEGORY_QUERY)){
				PerformanceCounterCategory.Delete(Constants.COUNTER_CATEGORY_QUERY);
			}

			if(EventLog.SourceExists(Constants.EVENT_LOG_SOURCE_QUERY)){
				EventLog.DeleteEventSource(Constants.EVENT_LOG_SOURCE_QUERY);
			}
		}
	}
}
