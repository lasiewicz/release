﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Search.Interfaces;
using Matchnet.Search.ValueObjects;
using NUnit.Framework;

namespace Matchnet.Search.BusinessLogic_Tests
{
    public abstract class AbstractSparkUnitTest
    {
        protected bool ENABLE_PRINT = false;
        protected SearchPreferenceCollection testSearchPrefs = null;
        protected decimal avgSearchTime = 0;
        protected decimal searchTimes = 0;
        protected int totalSearches = 0;
        protected MockSettingsService _mockSettingsService = null;
        protected Random _random = new Random();
        protected void SearcherBL_IncrementCounters() { }
        protected void SearcherBL_DecrementCounter() { }

        protected void SearcherBL_AverageSearchTime(long searchTime)
        {
            totalSearches++;
            searchTimes += searchTime;
            avgSearchTime = searchTimes / totalSearches;
        }

        protected void print(string s)
        {
            print(s, null);
        }

        protected void print(string s, string test)
        {
            if(!string.IsNullOrEmpty(test)) Console.WriteLine(string.Format("---------{0}---------", test));
            Console.WriteLine(s);
        }

        protected void printMaskValues(List<int> values)
        {
            if (ENABLE_PRINT)
            {
                Console.WriteLine(string.Format("---------VALUES SIZE {0}---------", values.Count));
                foreach (int value in values)
                {
                    Console.WriteLine(string.Format("Value: {0}", value));
                }
            }
        }

        protected void SetupMockSettingsService()
        {
            _mockSettingsService = new MockSettingsService();
            _mockSettingsService.AddSetting("SEARCHER_COMMUNITY_LIST", "23;24;1;3;10");
//            _mockSettingsService.AddSetting("SEARCHER_COMMUNITY_LIST", "1");
            _mockSettingsService.AddSetting("SEARCHER_SEARCH_THRESHOLD", "3000");
            _mockSettingsService.AddSetting("SEARCH30_SEARCHER_MAX_RESULTS", "1000");
            _mockSettingsService.AddSetting("SEARCHER_CLOSE_WAIT_TIME", "3000");
            _mockSettingsService.AddSetting("ENABLE_MATCHMAIL_RADIUS_LIMIT", "true");
            _mockSettingsService.AddSetting("SEARCHER_MAX_RESULTS", "360");
            _mockSettingsService.AddSetting("USE_E2_DISTANCE", "true");
            _mockSettingsService.AddSetting("USE_MINGLE_DISTANCE", "false");
            _mockSettingsService.AddSetting("SEARCHER_MAX_ACTIVE_DAYS", "1800");
            _mockSettingsService.AddSetting("SEARCHER_USE_ALL_INDEXES", "false");
            _mockSettingsService.AddSetting("INDEXER_ACTIVE_DAYS", "500");
            _mockSettingsService.AddSetting("INDEXER_COMMUNITY_THREADS", "4");
            _mockSettingsService.AddSetting("INDEXER_COPY_INDEX", "false");
            _mockSettingsService.AddSetting("INDEXER_DB_TIMEOUT", "120");
            _mockSettingsService.AddSetting("INDEXER_MAX_MB_BUFFER_SIZE", "48");
            _mockSettingsService.AddSetting("INDEXER_MAX_MERGE_DOCS", "1500");
            _mockSettingsService.AddSetting("INDEXER_MERGE_FACTOR", "48");
            _mockSettingsService.AddSetting("INDEXER_NUM_OF_PARTITIONS", "157");
            _mockSettingsService.AddSetting("INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/IndexStage/");
            _mockSettingsService.AddSetting("INDEXER_SWAPPING_INTERVAL", "15");
            _mockSettingsService.AddSetting("INDEXER_USE_COMPOUND_FILE", "true");
            _mockSettingsService.AddSetting("INDEXER_USE_QUEUE", "false");
            _mockSettingsService.AddSetting("INDEXER_USE_SEARCHSTOREDB", "false");
            _mockSettingsService.AddSetting("SEARCHINDEXER_INDEX_ONSTART", "true");
            _mockSettingsService.AddSetting("SEARCHINDEXER_SVC_LOGGER_MASK", "63");
            _mockSettingsService.AddSetting("ENABLE_INDEX_DIR_SWAP_UPDATES", "true");
            _mockSettingsService.AddSetting("SEARCHER_COMMUNITY_DIR", "d:/Matchnet/Index/");
            _mockSettingsService.AddSetting("SEARCHER_COMMUNITY_METADATA_FILE", "SearchDocument.xml");
            _mockSettingsService.AddSetting("SEARCHER_PER_COMMUNITY", "true");
            _mockSettingsService.AddSetting("INDEXER_SWAPPING_INTERVAL", "75", 3);
            _mockSettingsService.AddSetting("INDEXER_SWAPPING_INTERVAL", "10", 10);
            _mockSettingsService.AddSetting("INDEXER_SWAPPING_INTERVAL", "10", 23);
            _mockSettingsService.AddSetting("INDEXER_SWAPPING_INTERVAL", "10", 24);
            _mockSettingsService.AddSetting("INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/IndexStage/Spark", 1);
            _mockSettingsService.AddSetting("INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/IndexStage/JDate", 3);
            _mockSettingsService.AddSetting("INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/IndexStage/Cupid", 10);
            _mockSettingsService.AddSetting("INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/IndexStage/BBWPersonals", 23);
            _mockSettingsService.AddSetting("INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/IndexStage/BlackSingles", 24);
            _mockSettingsService.AddSetting("SEARCHER_COMMUNITY_DIR", "d:/Matchnet/Index/Spark", 1);
            _mockSettingsService.AddSetting("SEARCHER_COMMUNITY_DIR", "d:/Matchnet/Index/JDate", 3);
            _mockSettingsService.AddSetting("SEARCHER_COMMUNITY_DIR", "d:/Matchnet/Index/Cupid", 10);
            _mockSettingsService.AddSetting("SEARCHER_COMMUNITY_DIR", "d:/Matchnet/Index/BBWPersonalsPlus", 23);
            _mockSettingsService.AddSetting("SEARCHER_COMMUNITY_DIR", "d:/Matchnet/Index/BlackSingles", 24);

            _mockSettingsService.AddSetting("KEYWORD_INDEXER_ACTIVE_DAYS", "500");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_COMMUNITY_THREADS", "4");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_COPY_INDEX", "false");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_DB_TIMEOUT", "120");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_MAX_MB_BUFFER_SIZE", "48");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_MAX_MERGE_DOCS", "1500");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_MERGE_FACTOR", "48");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_NUM_OF_PARTITIONS", "157");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexStage/");
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/KeywordIndex/");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_SWAPPING_INTERVAL", "60");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_USE_COMPOUND_FILE", "true");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_USE_QUEUE", "false");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_USE_SEARCHSTOREDB", "false");
            _mockSettingsService.AddSetting("ENABLE_KEYWORD_INDEX_DIR_SWAP_UPDATES", "true");
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_COMMUNITY_METADATA_FILE", "KeywordSearchDocument.xml");
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexStage/Spark", 1);
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexStage/JDate", 3);
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexStage/Cupid", 10);
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexStage/BBWPersonals", 23);
            _mockSettingsService.AddSetting("KEYWORD_INDEXER_STAGE_COMMUNITY_DIR", "d:/Matchnet/KeywordIndexStage/BlackSingles", 24);
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/KeywordIndex/Spark1", 1);
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/KeywordIndex/JDate1", 3);
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/KeywordIndex/Cupid1", 10);
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/KeywordIndex/BBWPersonalsPlus1", 23);
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/KeywordIndex/BlackSingles1", 24);
            _mockSettingsService.AddSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF", "true", 1);
            _mockSettingsService.AddSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF", "true", 3);
            _mockSettingsService.AddSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF", "true", 10);
            _mockSettingsService.AddSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF", "true", 21);
            _mockSettingsService.AddSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF", "true", 23);
            _mockSettingsService.AddSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF", "true", 24);

            _mockSettingsService.AddSetting("ADMIN_INDEXER_ACTIVE_DAYS", "180");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_COMMUNITY_THREADS", "4");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_COPY_INDEX", "true");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_DB_TIMEOUT", "300");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_MAX_MB_BUFFER_SIZE", "48");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_MAX_MERGE_DOCS", "1500");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_MERGE_FACTOR", "48");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_NUM_OF_PARTITIONS", "157");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_STAGE_COMMUNITY_DIR", "c:/Matchnet/AdminIndexStage/");
            _mockSettingsService.AddSetting("ADMIN_SEARCHER_COMMUNITY_DIR", "D:/Matchnet/AdminIndex/");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_SWAPPING_INTERVAL", "60");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_USE_COMPOUND_FILE", "true");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_USE_QUEUE", "false");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_USE_SEARCHSTOREDB", "false");
            _mockSettingsService.AddSetting("ENABLE_ADMIN_INDEX_DIR_SWAP_UPDATES", "true");
            _mockSettingsService.AddSetting("ADMIN_SEARCHER_COMMUNITY_METADATA_FILE", "AdminSearchDocument.xml");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_STAGE_COMMUNITY_DIR", "c:/Matchnet/AdminIndexStage/Spark", 1);
            _mockSettingsService.AddSetting("ADMIN_INDEXER_STAGE_COMMUNITY_DIR", "c:/Matchnet/AdminIndexStage/JDate", 3);
            _mockSettingsService.AddSetting("ADMIN_INDEXER_STAGE_COMMUNITY_DIR", "c:/Matchnet/AdminIndexStage/Cupid", 10);
            _mockSettingsService.AddSetting("ADMIN_INDEXER_STAGE_COMMUNITY_DIR", "c:/Matchnet/AdminIndexStage/BBWPersonals", 23);
            _mockSettingsService.AddSetting("ADMIN_INDEXER_STAGE_COMMUNITY_DIR", "c:/Matchnet/AdminIndexStage/BlackSingles", 24);
            _mockSettingsService.AddSetting("ADMIN_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/AdminIndex/Spark", 1);
            _mockSettingsService.AddSetting("ADMIN_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/AdminIndex/JDate", 3);
            _mockSettingsService.AddSetting("ADMIN_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/AdminIndex/Cupid", 10);
            _mockSettingsService.AddSetting("ADMIN_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/AdminIndex/BBWPersonalsPlus", 23);
            _mockSettingsService.AddSetting("ADMIN_SEARCHER_COMMUNITY_DIR", "d:/Matchnet/AdminIndex/BlackSingles", 24);
            _mockSettingsService.AddSetting("ADMIN_INDEXER_ONDEMAND_ENABLED", "true");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_ONDEMAND_DATETIME", "06/20/2012 12:02 PM", 1);
            _mockSettingsService.AddSetting("ADMIN_INDEXER_USE_ROWBLOCKS", "true");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_ROWBLOCKS_DATE_RANGE_IN_DAYS", "1");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_PROCESSOR_TIME_LIMIT", "180");
            _mockSettingsService.AddSetting("ADMIN_INDEXER_BRANDLASTLOGONDATE_LOOKBACK", "5");
            
            _mockSettingsService.AddSetting("NRT_USE_SEARCH_PREF_SA", "false");
            _mockSettingsService.AddSetting("SEARCHER_SLOP_FACTOR", "2");
            _mockSettingsService.AddSetting("ENABLE_KEYWORD_FILTER", "true");
            _mockSettingsService.AddSetting("SEARCHER_USE_FUZZY_QUERY_FOR_SINGLE_TERMS", "false");
            _mockSettingsService.AddSetting("INDEXER_USE_SYSTEM_DB_FOR_INDEX_UPDATES", "true");
            _mockSettingsService.AddSetting("SEARCHER_USE_SYSTEM_DB_FOR_INDEX_UPDATES", "true");
            _mockSettingsService.AddSetting("KEYWORD_SEARCHER_MAX_RESULTS", "500");
            _mockSettingsService.AddSetting("SEARCHER_MIN_KEYWORD_LENGTH", "2");
            _mockSettingsService.AddSetting("SEARCHER_PREPROCESS_KEYWORD_TERMS", "true");

            _mockSettingsService.AddSetting("USE_LUCENE_CACHED_DISTANCE_VALUES_SORT", "true");
            _mockSettingsService.AddSetting("USE_LUCENE_FIELDCACHE_MATCH_VALUES_SORT", "true");
            _mockSettingsService.AddSetting("SEARCHER_INDEX_UPDATE_INTERVAL", "30000");
            
        }
    }

    public class MockSettingsService : ISettingsSA
    {
        private Dictionary<int, Dictionary<string, string>> communitySettings = new Dictionary<int, Dictionary<string, string>>();
        #region ISettingsSA Members

        public void AddSetting(string key, string value)
        {
            AddSetting(key, value, 0);
        }

        public void AddSetting(string key, string value, int communityId)
        {
            if (!communitySettings.ContainsKey(communityId)) communitySettings.Add(communityId, new Dictionary<string, string>());
            communitySettings[communityId].Add(key, value);
        }

        public void RemoveSetting(string key)
        {
            RemoveSetting(key, 0);
        }

        public void RemoveSetting(string key, int communityId)
        {
            if (SettingExistsFromSingleton(key, communityId, 0))
            {
                communitySettings[communityId].Remove(key);
            }
            else if (SettingExistsFromSingleton(key, 0, 0))
            {
                communitySettings[0].Remove(key);
            }
        }

        public System.Collections.Generic.List<Matchnet.Configuration.ValueObjects.MembaseConfig> GetMembaseConfigsFromSingleton()
        {
            throw new System.NotImplementedException();
        }

        public Matchnet.Configuration.ValueObjects.ServiceInstanceConfig GetServiceInstanceConfigFromSingleton(string serviceConstant, string machineName)
        {
            throw new System.NotImplementedException();
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID, int brandID)
        {
            if (SettingExistsFromSingleton(constant, communityID, 0))
            {
                return communitySettings[communityID][constant];
            }
            else
            {
                return GetSettingFromSingleton(constant);
            }
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID)
        {
            if (SettingExistsFromSingleton(constant, communityID, 0))
            {
                return communitySettings[communityID][constant];
            }
            else
            {
                return GetSettingFromSingleton(constant);
            }
        }

        public string GetSettingFromSingleton(string constant, int communityID)
        {
            if (SettingExistsFromSingleton(constant, communityID, 0))
            {
                return communitySettings[communityID][constant];
            }
            else
            {
                return GetSettingFromSingleton(constant);
            }
        }

        public string GetSettingFromSingleton(string constant)
        {
            try
            {
                return communitySettings[0][constant];
            }
            catch (Exception e)
            {
                print(string.Format("Could not find setting key:{0}, community:{1}", constant, 0));
                throw e;
            }
        }

        public bool IsServerEnabledFromSingleton()
        {
            throw new System.NotImplementedException();
        }

        public bool SettingExistsFromSingleton(string constant, int communityId, int siteId)
        {
            bool settingExistsFromSingleton = communitySettings.ContainsKey(communityId) && communitySettings[communityId].ContainsKey(constant);
            //if (!settingExistsFromSingleton)
            //{
            //    print(string.Format("Could not find setting key:{0}, community:{1}", constant, communityId));
            //}
            return settingExistsFromSingleton;
        }

        protected void print(string s)
        {
            print(s, null);
        }

        protected void print(string s, string test)
        {
            if (!string.IsNullOrEmpty(test)) Console.WriteLine(string.Format("---------{0}---------", test));
            Console.WriteLine(s);
        }

        public List<Matchnet.Configuration.ValueObjects.ApiAppSetting> GetAPIAppSettingsFromSingleton()
        {
            throw new NotImplementedException();
        }

        public void AddNewOrUpdateSetting(string settingConstant, string globalDefaultValue, string settingDescription,
            int settingCategoryId = 2, bool isRequiredForCommunity = false, bool isRequiredForSite = false)
        {
            throw new NotImplementedException();
        }

        public void UpdateGlobalDefault(string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void SetValueForSite(int siteId, string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void ResetToGlobalDefault(string settingConstant)
        {
            throw new NotImplementedException();
        }


        public Configuration.ValueObjects.MembaseConfig GetMembaseConfigByBucketFromSingleton(string bucketName)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}
