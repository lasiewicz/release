﻿using System;
using System.Collections.Generic;
using Matchnet.Search.BusinessLogic;
using Matchnet.Search.ValueObjects;
using NUnit.Framework;
using Spark.SearchEngine.ServiceAdapters;
using Spark.SearchEngine.ServiceAdaptersNRT;
using Spark.SearchEngine.ValueObjects;

namespace Matchnet.Search.BusinessLogic_Tests
{
    [TestFixture]
    public class StressTest : AbstractSparkUnitTest
    {
        string origIndexPath;
        string origKeywordIndexPath;

        [TestFixtureSetUp]
        public void StartUp()
        {
//            if (!indexPaths.ContainsKey(1)) indexPaths.Add(1, "c:\\Matchnet\\Index\\prefs\\Spark1");
//            if (!indexPaths.ContainsKey(3)) indexPaths.Add(3, "c:\\Matchnet\\Index\\prefs\\Jdate1");
            SetupMockSettingsService();
            MatchnetQueryBuilder.SettingsService = _mockSettingsService;
        }

        [TestFixtureTearDown]
        public void Teardown()
        {
            MatchnetQueryBuilder.SettingsService = null;
        }

        [SetUp]
        public void setUp()
        {}

        [TearDown]
        public void tearDown()
        {}


        [Test]
        public void TestStressOnDistributedCache()
        {
            int communityId=1;
            int siteId=101;
//            SearcherBL searcherBl=GetSearcherBL(communityId);
            List<IStressTestable> runners = new List<IStressTestable>();
            bool useJdate = true;
            for (int i = 0; i < 10000; i++)
            {
                communityId = (useJdate) ? 3 : 1;
                siteId = (useJdate) ? 103 : 101;
//                useJdate = !useJdate;
                int rn = _random.Next()%5;
//                searcherBl = GetSearcherBL(communityId);
                switch (rn)
                {
                    case 0:
                    case 1:
                    case 2:
                        SearchMember searchMember = createSearchMember(communityId, i);
                        SearchMemberUpdate searchMemberUpdate = createSearchMemberUpdate(searchMember, i);
                        runners.Add(new NRTDistributedCacheRunner(searchMemberUpdate, searchMember, SearcherNRTSA.Instance, 1003));
                        break;
                    default:
                        runners.Add(new DistributedCacheRunner(createPrefs(communityId, i), SearcherSA.Instance, communityId, siteId));
                        break;
                }
            }

//            origIndexPath = searcherBl.GetSearcher(communityId).IndexPath;
//            origKeywordIndexPath = searcherBl.GetKeywordSearcher(communityId).IndexPath;
//            Hashtable h = new Hashtable();
//            h.Add("communityId", communityId);
//            h.Add("searcherBl", searcherBl);
//            Thread thread = new Thread(ChangePath);
//            thread.Start(h);

            using (StressTester tester = new StressTester(runners))
            {
                tester.RunConcurrently();
                while (!tester.IsAllDone())
                {
                    System.Threading.Thread.Sleep(500);
                }
            }

            int idx = 0;
            Console.WriteLine("\nAvg Search Time = " + avgSearchTime);
            foreach (IStressTestable testable in runners)
            {                                
                Assert.IsTrue(testable.RunSuceeded, "Failed at runner[" + idx + "], "+testable.ElapsedMillis + " elapsed millis.");
                Assert.Less(testable.ElapsedMillis, 60000, "Failed at runner["+idx+"]");
                idx++;
            }
//            thread.Abort();
        }

        private SearchPreferenceCollection createPrefs(int communityId, int idx)
        {
            SearchPreferenceCollection searchPrefs = new SearchPreferenceCollection();
            searchPrefs.Add("countryregionid","223");
            searchPrefs.Add("hasphotoflag", ((idx % 2 == 0) ? "1" : "0"));
            searchPrefs.Add("distance", ((idx % 4) * 40) + "");
            searchPrefs.Add("searchtype", ((idx % 2 == 0) ? "1" : "0"));
            searchPrefs.Add("geodistance", ((idx % 2 == 0) ? "5" : "7"));
            searchPrefs.Add("searchtypeid","1");
            searchPrefs.Add("searchorderby","3");
            searchPrefs.Add("domainid",communityId+"");
            searchPrefs.Add("minage", (((idx % 3 + 1) * 5) + 10) + "");
            searchPrefs.Add("maxage", (((idx % 3 + 1) * 25) + 20) + "");
            searchPrefs.Add("gendermask", ((idx % 2 == 0) ? "6" : "9"));

            int loc = (idx % 5);
            switch (loc)
            {
                case 0:
                    //beverly hills (zip=90211)
                    searchPrefs.Add("regionid", "3443821");
                    searchPrefs.Add("latitude", "0.594557");
                    searchPrefs.Add("longitude", "-2.066127");
                    break;
                case 1:
                    //provo, ut (zip=84601)
                    searchPrefs.Add("regionid", "3479904");
                    searchPrefs.Add("latitude", "0.701981");
                    searchPrefs.Add("longitude", "-1.949413");
                    break;
                case 2:
                    //paris, ontario
                    searchPrefs.Add("regionid", "2639301");
                    searchPrefs.Add("latitude", "0.753693");
                    searchPrefs.Add("longitude", "-1.402806");
                    break;
                default:
                    //new york 3000 miles away (zip=10017)
                    searchPrefs.Add("regionid", "3466331");
                    searchPrefs.Add("latitude", "0.711260");
                    searchPrefs.Add("longitude", "-1.291094");
                    break;
            }

            int blockedIds = (idx % 5);
            switch (blockedIds)
            {
                case 0:
                    searchPrefs.Add("blockedmemberids", "114994142, 126956941, 118579617, 112781849, 113856125, 115959544, 26608718, 8643310, 119002305,114932342, 7855573, 105540661, 113929219, 126702382, 119402558, 118948520, 119250261, 686976, 7574392, 116893501, 119497490, 116734583, 115039264, 106432847, 49787681, 112515583, 115976109, 119335527, 115581807, 115435227, 106479126, 115408851, 49679916, 105679155, 30572128,106010805, 112611891, 53541306, 578453, 102444640, 114568138, 126707251, 108862841, 126777571, 126191869, 41827164, 30797871, 50941263, 109824927, 115528737, 113528312, 53466392, 45206110, 762944, 119542028, 119166405, 104686801, 114809886, 112128529, 51975775, 126398329, 107975500, 119464510, 8179192, 118790891, 52528769, 115141169, 119188328, 111930519, 52662038, 40824279, 113296328, 112531784, 8632064, 109153047, 114608825, 114513248, 51789835, 110572975, 101331909, 8428226, 7798235, 102998738, 115185314, 119672114, 119578988, 9417917, 40003042, 104864537, 45598897, 49023878, 51022114, 126695698, 30150119, 113113369, 49824581, 119317380, 116574453, 115181335, 114100327, 113414404, 106211583, 114035997, 112726017, 126144013, 113022519, 100845596, 111231791, 119109413, 115909573, 126106915, 118866263, 112900365, 108983814, 105397570, 126805717,118922583, 112205824, 114618357, 114096744, 114562575, 48668566, 109959506, 107685607,49729751, 118838529, 126280465, 105584023, 8585868, 115244572, 46462324, 40064245, 53570117,115200236, 126729394, 53278733, 119371125, 113062820, 8142406, 114349694, 50834930, 119551331, 53493783, 119177081, 40059229, 126629503, 11339049, 13058398, 126114760, 119476316, 110705727, 108958996, 126991414, 18528303, 119214570, 114362199, 6141899, 119243213, 114885357, 53935069, 53410959, 48105170, 114330116, 126627268, 114255162, 126592777, 114054561, 126782683,114411678, 119138270, 115431534, 115643722, 104566472, 46346029, 115728248, 112317176,100765532, 126768046, 119366454, 45999947, 109035171, 115016857, 115896793, 115563498,49063464, 50257714, 113051486, 116804819, 115293799, 110012936, 51641738, 126892198, 115537851, 113518313, 8458902, 111918649, 887910, 118972992, 115634653, 9755728, 116771768, 115924792,126605998, 8400838, 54632565, 53330976, 126482194, 118855569, 102575570, 8577264, 119178206,126749884, 115262401, 113495396, 119348384, 115738354, 46333395, 9719610, 115032528, 44208585, 110332174, 114819212, 113866192, 106642474, 110488625, 110723604, 126471397, 113514321,115200125, 109026288, 114653560, 115545099, 43942834, 52418163, 100222058, 110681706,114682061, 115317923, 110407811, 7009588, 115340404, 115490361, 111082195, 14074785, 112130618, 40683976, 115470846, 115247884, 103083144, 100868431, 51672770, 102044205, 119172704");
                    break;
                case 1:
                    searchPrefs.Add("blockedmemberids", "119484824,119477759,119503394,119455296,126874963,126412708,126829225,119470445,119207058,126636670,119526935,126998623,119551985,48796521,126731107,118931349,119605262,115506774,119454474,119400362,119681468,119367221,119697683,126995557,126966439,119369433,119679182,109494815,119345774,119692067,110831390,119206320,126677941,126990805,119639546,119429048,119435987,126784252,119454545,119240508,126730684,119106327,119496699,119650121,119342391,118903334,119579921,119662796,119647589,119552021,119322572,126703129,119634959,119205849,119354219,126620995,127024804,126675493,126733249,119368826,126673381,126955018,126872929,119595278,119510522,119123010,119327915,126677716,126912547,126768610,126647269,119317014,119174388,119144730,126958210,126863047,126695899,127004041,119452986,119567108,119321184,126711286,119607101,119595590,126709012,119695223,126949201,119391030");
                    break;
                case 2:
                    searchPrefs.Add("blockedmemberids", "114994142, 126956941, 118579617, 119335527, 115581807, 115435227, 106479126, 115408851, 49679916, 105679155, 30572128,106010805, 112611891, 53541306, 578453, 102444640, 114568138, 126707251, 108862841, 126777571, 126191869, 41827164, 30797871, 110572975, 101331909, 8428226, 7798235, 102998738, 115185314, 119672114, 119578988, 9417917, 40003042, 104864537, 45598897, 49023878, 51022114, 126695698, 30150119, 113113369, 49824581, 119317380, 116574453, 115181335, 114100327, 13058398, 126114760, 119476316, 110705727, 108958996, 126991414, 18528303, 119214570, 114362199, 6141899, 119243213, 114885357, 53935069, 53410959, 48105170, 114330116, 126627268, 114255162, 126592777, 114054561, 126782683,114411678, 119172704");
                    break;
                case 3:
                    searchPrefs.Add("blockedmemberids", "119484824,119477759,119503394,119455296,126874963,126412708,126829225,119470445,119207058,126636670,119526935,126998623");
                    break;
                default:
                    break;
            }

            int keywords = (idx % 3);
            switch (keywords)
            {
                case 0:
                    searchPrefs.Add("keywordsearch", "\"Catch a Falling Star\"");
                    break;
                case 1:
                    searchPrefs.Add("keywordsearch", "science");
                    break;
                case 2:
                    searchPrefs.Add("keywordsearch", "single, fun, happy");
                    break;
                default:
                    break;
            }

            return searchPrefs;
        }

        private SearchMember createSearchMember(int communityId, int idx)
        {
            int memberId = _random.Next();
            bool isEven = memberId%2 == 0;
            int gendermask = isEven ? 6 : 9;
            int age = _random.Next(18, 99);
            int hasPhotoFlag = (isEven) ? 0 : 1;
            //beverly hills (zip=90211)
            int regionId = 3443821;
            double lat = 0.594557;
            double lng = -2.066127;
            int loc = (idx % 5);
            switch (loc)
            {
                case 0:
                    //provo, ut (zip=84601)
                    regionId = 3479904;
                    lat = 0.701981;
                    lng = -1.949413;
                    break;
                case 1:
                    //paris, ontario
                    regionId = 2639301;
                    lat = 0.753693;
                    lng = -1.402806;
                    break;
                case 2:
                    //new york 3000 miles away (zip=10017)
                    regionId = 3466331;
                    lat = 0.711260;
                    lng = -1.291094;
                    break;
                default:
                    break;
            }

            SearchMember member = SearchMember.GenerateSearchMember(memberId, communityId, gendermask, DateTime.Now.AddYears(-age), DateTime.Now, DateTime.Now, hasPhotoFlag, 128, 8, 10, 2, 2, 4, 173, 8, -21474, 0, 0, 0, 0, 102, 0, 2, -1, -1, 223, 1538, 3403764, regionId, lng, lat, -1, string.Empty, 82, DateTime.Now, -1, -1, -1, DateTime.Parse("2008-07-26"), -1, -1, -1, -1);
            return member;
        }

        private SearchMemberUpdate createSearchMemberUpdate(SearchMember searchMember, int idx)
        {
            SearchMemberUpdate searchMemberUpdate = new SearchMemberUpdate();
            searchMemberUpdate.CommunityID = searchMember.CommunityID;
            searchMemberUpdate.MemberID = searchMember.MemberID;
            int loc = (idx % 5);
            switch (loc)
            {
                case 0:
                    searchMemberUpdate.UpdateReason=UpdateReasonEnum.none;
                    searchMemberUpdate.UpdateMode=UpdateModeEnum.add;
                    break;
                case 1:
                    searchMemberUpdate.UpdateReason=UpdateReasonEnum.adminSuspend;
                    searchMemberUpdate.UpdateMode=UpdateModeEnum.remove;
                    break;
                case 2:
                    searchMemberUpdate.UpdateReason=UpdateReasonEnum.unHide;
                    searchMemberUpdate.UpdateMode=UpdateModeEnum.update;
                    break;
                default:
                    searchMemberUpdate.UpdateReason=UpdateReasonEnum.none;
                    searchMemberUpdate.UpdateMode=UpdateModeEnum.add;
                    break;
            }
            searchMember.UpdateMode = searchMemberUpdate.UpdateMode;
            searchMember.UpdateReason = searchMemberUpdate.UpdateReason;
            searchMember.UpdateType = UpdateTypeEnum.all;
            return searchMemberUpdate;
        }

//        private void ChangePath(object o)
//        {
//            Hashtable h = (Hashtable) o;
//            int communityId = (int)h["communityId"];
//            SearcherBL searcherBl = (SearcherBL)h["searcherBl"];
//            Searcher searcher = searcherBl.GetSearcher(communityId);
//            Searcher keywordSearcher = searcherBl.GetKeywordSearcher(communityId);
//            
//            int idx2 = 0;
//            while (true)
//            {
//                Thread.Sleep(300000);
//                switch (idx2%3)
//                {
//                    case 0:
//                        searcher.NUNIT_IndexPath = "d:\\Matchnet\\Index\\prod\\Jdate2.member";
//                        keywordSearcher.NUNIT_IndexPath = "d:\\Matchnet\\KeywordIndex\\Prod\\Jdate1.keyword";
//                        break;
//                    case 1:
//                        searcher.NUNIT_IndexPath = "d:\\Matchnet\\Index\\prod\\Jdate3.member";
//                        keywordSearcher.NUNIT_IndexPath = "d:\\Matchnet\\KeywordIndex\\Prod\\Jdate2.keyword";
//                        break;
//                    default:
//                        searcher.NUNIT_IndexPath = origIndexPath;
//                        keywordSearcher.NUNIT_IndexPath = origKeywordIndexPath;
//                        break;
//                }
//                searcher.Update();
//                keywordSearcher.Update();
//                idx2++;
//            }
//        }
    }
}
