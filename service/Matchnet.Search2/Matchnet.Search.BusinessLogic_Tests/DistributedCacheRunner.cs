﻿using System;
using System.Threading;
#if DEBUG
using Matchnet.Search.BusinessLogic;
using Matchnet.Search.ValueObjects;
using Matchnet.Search.Interfaces;
using System.Diagnostics;
using Spark.SearchEngine.ServiceAdapters;

#endif
namespace Matchnet.Search.BusinessLogic_Tests
{
#if DEBUG
    class DistributedCacheRunner : IStressTestable
    {
        private SearchPreferenceCollection _searchPrefs = null;
        private SearcherSA _searcherSa = null;
        private int _communityId;
        private int _siteId;
        private ReaderWriterLock _externalLock = null;
        private long _ellapsedMillis = 0;

        private bool _runSucceeded = false;
        private bool _isDone = false;

        public bool RunSuceeded
        {
            get { return _runSucceeded; }
        }

        public long ElapsedMillis
        {
            get { return _ellapsedMillis; }
        }

        public bool IsDone
        {
            get { return _isDone; }
        }

        public DistributedCacheRunner(SearchPreferenceCollection searchPrefs, SearcherSA searcherSA, int communityId, int siteId)
        {
            _searchPrefs = searchPrefs;
            _searcherSa = searcherSA;
            _communityId = communityId;
            _siteId = siteId;
            _externalLock = new ReaderWriterLock();
        }

        #region IStressTestable Members

        public void Prepare(){}

        public void Run()
        {
            try
            {
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                IMatchnetQuery query = MatchnetQueryBuilder.build(_searchPrefs, _communityId, _siteId);
                IMatchnetQueryResults matchnetResults = _searcherSa.RunQuery(query, _communityId);
                stopWatch.Stop();

                _ellapsedMillis = stopWatch.ElapsedMilliseconds;
                int results = (null == matchnetResults) ? 0 : matchnetResults.Items.Count;
//                foreach (MatchnetResultItem matchnetResultItem in matchnetResults.Items.List)
//                {
//                    Console.WriteLine("Memberid: "+matchnetResultItem.MemberID);
//                }
                //Console.WriteLine(string.Format("Thread:{0}, Results:{1}, Ellapsed Time:{2} ms, SiteId:{3}, CommunityId:{4}",this.GetHashCode(),results,ElapsedMillis,_siteId,_communityId));
                this._runSucceeded = true;
            }
            catch (Exception e)
            {
                Console.WriteLine("\n" + e.Message);
                //System.Diagnostics.EventLog.WriteEntry("NUNIT", e.Message, System.Diagnostics.EventLogEntryType.Error);
                throw e;
            }
            finally
            {
                this._isDone = true;
            }
        }
        #endregion
    }
#endif
}
