﻿using System;
using System.Threading;
#if DEBUG
using System.Diagnostics;
using Spark.SearchEngine.ServiceAdaptersNRT;
using Spark.SearchEngine.ValueObjects;

#endif
namespace Matchnet.Search.BusinessLogic_Tests
{
#if DEBUG
    class NRTDistributedCacheRunner : IStressTestable
    {
        private SearchMemberUpdate _searchMemberUpdate = null;
        private SearchMember _searchMember = null;
        private SearcherNRTSA _searcherNRTSa = null;
        private int _brandId;
        private ReaderWriterLock _externalLock = null;
        private long _ellapsedMillis = 0;

        private bool _runSucceeded = false;
        private bool _isDone = false;

        public bool RunSuceeded
        {
            get { return _runSucceeded; }
        }

        public long ElapsedMillis
        {
            get { return _ellapsedMillis; }
        }

        public bool IsDone
        {
            get { return _isDone; }
        }

        public NRTDistributedCacheRunner(SearchMemberUpdate searchMemberUpdate, SearchMember searchMember, SearcherNRTSA searcherNrtSa, int brandId)
        {
            _searchMemberUpdate = searchMemberUpdate;
            _searchMember = searchMember;
            _searcherNRTSa = searcherNrtSa;
            _brandId = brandId;
            _externalLock = new ReaderWriterLock();
        }

        #region IStressTestable Members

        public void Prepare(){}

        public void Run()
        {
            try
            {
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                if (_searchMember.UpdateMode==UpdateModeEnum.remove)
                {
                    _searcherNRTSa.NRTRemoveMember(_searchMemberUpdate); 
                }
                else
                {
                    _searcherNRTSa.NRTUpdateMember(_searchMemberUpdate, _brandId);
                }
//                _searcherBL.GetSearcher(_communityId).UpdateNRT();              
                stopWatch.Stop();

                _ellapsedMillis = stopWatch.ElapsedMilliseconds;
                //Console.WriteLine(string.Format("Thread:{0}, Results:{1}, Ellapsed Time:{2} ms, SiteId:{3}, CommunityId:{4}",this.GetHashCode(),results,ElapsedMillis,_siteId,_communityId));
                this._runSucceeded = true;
            }
            catch (Exception e)
            {
                Console.WriteLine("\n" + e.Message);
                //System.Diagnostics.EventLog.WriteEntry("NUNIT", e.Message, System.Diagnostics.EventLogEntryType.Error);
                throw e;
            }
            finally
            {
                this._isDone = true;
            }
        }
        #endregion
    }
#endif
}
