﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Matchnet.Search.BusinessLogic_Tests
{
    public class StressTester : IDisposable  {
        private readonly List<IStressTestable> _runners;
        private readonly List<Thread> _threads= new List<Thread>();

        public StressTester(List<IStressTestable> runners)  {
            _runners = runners;
            foreach (IStressTestable runner in _runners)
            {
                runner.Prepare();
            }
        }
        

        private static bool AnotherThreadAlive(List<Thread> threads)
        {            
            foreach (Thread t in threads)
            {
                if (!t.IsAlive) return true;
            }
            return false;
        }

        public void RunConcurrently()  {

            for (int i = 0; i < _runners.Count;i++)
            {
                IStressTestable runner = _runners[i];
                Thread t1 = new Thread(new ThreadStart(delegate
                {
                    if (i==0  || AnotherThreadAlive(_threads))
                    {
                        runner.Run();
                    }
                }));
                _threads.Add(t1);
                t1.Start();
                t1.Join();
            }            
        }

        public void Dispose() {
            foreach (Thread t in _threads)
            {
                t.Abort();
            }
        } 

        public bool IsAllDone()
        {
            bool allDone = true;
            foreach (IStressTestable runner in _runners)
            {
                if (!runner.IsDone)
                {
                    allDone = false;
                    break;
                }
            }
            return allDone;
        }
    } 
}
