﻿#region

using System;
using System.Collections.Generic;
using Matchnet;
using Matchnet.CacheSynchronization.Tracking;

#endregion

namespace Spark.API.ValueObjects
{
    [Serializable]
    public class Apps : List<App>, IValueObject, ICacheable
    {
        private const string CacheKey = "~Api^Apps";

        private CacheItemMode _cacheItemMode = CacheItemMode.Absolute;
        private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Normal;
        private int _cacheTtlSeconds = 3600;
        [NonSerialized] private ReferenceTracker _referenceTracker;

        #region Implementation of ICacheable

        public string GetCacheKey()
        {
            return CacheKey;
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTtlSeconds; }
            set { _cacheTtlSeconds = value; }
        }

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public static string GetCollectionCacheKey()
        {
            return CacheKey;
        }

        #endregion

        public ReferenceTracker ReferenceTracker
        {
            get
            {
                if (_referenceTracker == null)
                {
                    _referenceTracker = new ReferenceTracker();
                }

                return _referenceTracker;
            }
        }
    }
}