﻿#region

using Matchnet.CacheSynchronization.ValueObjects;

#endregion

namespace Spark.API.ValueObjects.ServiceDefinitions
{
    public interface IAPIService
    {
        Apps GetApps();
        int CreateApp(App app);
        void UpdateApp(App app);
        void DeleteApp(int appId);

        AppMember GetAppMember(int appId, int memberId, string clientHostName, CacheReference cacheReference);
        void CreateAppMember(AppMember app);
        void UpdateAppMember(AppMember app);
        void DeleteAppMember(int appId, int memberId);
        void DeleteAppMembers(int memberId);
    }
}