﻿#region

using System;
using Matchnet;

#endregion

namespace Spark.API.ValueObjects
{
    [Serializable]
    public class App : IValueObject
    {
        /// <summary>
        ///     Set this value to 0 for a new App.
        /// </summary>
        public int AppId { get; set; }

        public int BrandId { get; set; }

        public Int16 Status { get; set; }

        public int MemberId { get; set; }

        public string Secret { get; set; }

        public string RedirectUrl { get; set; }

        public bool AutoAuth { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime InsertDateTime { get; set; }

        public DateTime UpdateDateTime { get; set; }

        public string ContactUsEmail { get; set; }
    }
}