﻿#region

using System;
using Matchnet;
using Matchnet.CacheSynchronization.Tracking;

#endregion

namespace Spark.API.ValueObjects
{
    [Serializable]
    public class AppMember : IValueObject, ICacheable
    {
        private const string CacheKey = "~Api^App^{0}^{1}";
        public const int DefaultCacheTtlSeconds = 3600;
        private CacheItemMode _cacheItemMode = CacheItemMode.Absolute;
        private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Normal;
        private int _cacheTtlSeconds = 3600;
        [NonSerialized] private ReferenceTracker _referenceTracker;

        public int AppId { get; set; }

        public int MemberId { get; set; }

        public string AccessToken { get; set; }

        public DateTime AccessTokenExpirationDateTime { get; set; }

        public string RefreshToken { get; set; }

        public DateTime RefreshTokenExpirationDateTime { get; set; }

        public DateTime UpdateDateTime { get; set; }

        public DateTime InsertDateTime { get; set; }

        public ReferenceTracker ReferenceTracker
        {
            get
            {
                if (_referenceTracker == null)
                {
                    _referenceTracker = new ReferenceTracker();
                }

                return _referenceTracker;
            }
        }

        #region Implementation of ICacheable

        public string GetCacheKey()
        {
            return string.Format(CacheKey, AppId, MemberId);
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTtlSeconds; }
            set { _cacheTtlSeconds = value; }
        }

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public static string GetCacheKey(int appId, int memberId)
        {
            return string.Format(CacheKey, appId, memberId);
        }

        public static string GetCollectionCacheKey()
        {
            return CacheKey;
        }

        #endregion
    }
}