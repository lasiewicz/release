﻿namespace Spark.API.ValueObjects
{
    public class ServiceConstants
    {
        public const string ServiceConstant = "API_SVC";
        public const string ServiceName = "Spark.API.Service";
        public const string ServiceManagerName = "APISM";

        private ServiceConstants()
        {
        }
    }
}