﻿#region

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Matchnet;

#endregion

namespace Spark.API.ValueObjects.ReplicationActions
{
    [Serializable]
    public class ExpireAppMember : IReplicationAction, ISerializable
    {
        private string _cacheKey;

        public ExpireAppMember(string cacheKey)
        {
            _cacheKey = cacheKey;
        }

        #region Implementation of ISerializable

        protected ExpireAppMember(SerializationInfo info, StreamingContext context)
        {
            var ms = new MemoryStream((byte[]) info.GetValue("bytearray", typeof (byte[])));
            var br = new BinaryReader(ms);

            _cacheKey = br.ReadString();
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            var ms = new MemoryStream();
            var bw = new BinaryWriter(ms);

            bw.Write(_cacheKey);

            info.AddValue("bytearray", ms.ToArray());
        }

        #endregion

        public string CacheKey
        {
            get { return _cacheKey; }
            set { _cacheKey = value; }
        }
    }
}