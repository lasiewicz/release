﻿#region

using System;
using System.EnterpriseServices;
using System.Reflection;
using System.Runtime.InteropServices;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;

#endregion

namespace Spark.API.BusinessLogic
{
    [Transaction(TransactionOption.Required), ComVisible(true)]
    public class TransactionalWriter : ServicedComponent
    {
        /// <summary>
        ///     For synchronous writes. Hydra TranWriter is Async!.
        /// </summary>
        /// <param name="command"></param>
        public void ExecuteSyncWrite(Command command)
        {
            try
            {
                Exception ex;

                var sw = new SyncWriter();
                sw.Execute(command, null, out ex);
                sw.Dispose();
                if (ex != null)
                {
                    throw ex;
                }
                ContextUtil.SetComplete();
            }
            catch (Exception ex)
            {
                ContextUtil.SetAbort();
                throw new BLException(MethodBase.GetCurrentMethod().Name, ex);
            }
        }
    }
}