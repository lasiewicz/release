﻿#region

using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.Caching;
using Matchnet;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Data;
using Matchnet.Exceptions;
using Spark.API.ValueObjects;
using Spark.API.ValueObjects.ReplicationActions;
using Spark.API.ValueObjects.ServiceDefinitions;
using log4net;
using Cache = Matchnet.Caching.Cache;

#endregion

namespace Spark.API.BusinessLogic
{
    public class APIBL : IAPIService
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(APIBL));

        public static readonly APIBL Instance = new APIBL();

        #region Delegates and Events

        public delegate void ReplicationEventHandler(IReplicationAction replicationAction);

        public delegate void SynchronizationEventHandler(string key, Hashtable cacheReferences);

        public event SynchronizationEventHandler SynchronizationRequested;

        public event ReplicationEventHandler ReplicationRequested;

        #endregion

        private readonly Cache _cache;

        private readonly CacheItemRemovedCallback _expireCallback;

        private APIBL()
        {
            Log.Info("Starting APIBL");
            _cache = Cache.Instance;
            _expireCallback = new CacheItemRemovedCallback(ExpireCallback);
        }

        #region App CRUD

        public Apps GetApps()
        {
            SqlDataReader reader = null;

            try
            {
                var apps = _cache.Get(Apps.GetCollectionCacheKey()) as Apps;

                if (apps != null) return apps;
                apps = new Apps();

                var command = new Command("mnAPI", "dbo.up_App_List", 0);
                reader = Client.Instance.ExecuteReader(command);

                while (reader.Read())
                {
                    //[AppID], [BrandID], [Status], [MemberID], [Secret], [RedirectUrl], [AutoAuth], [Title], [Description], [InsertDateTime], [UpdateDateTime] 
                    var app = new App
                        {
                            AppId = reader.GetInt32(0),
                            BrandId = reader.GetInt32(1),
                            Status = reader.GetInt16(2),
                            MemberId = reader.GetInt32(3),
                            Secret = reader.GetString(4),
                            RedirectUrl = reader.GetString(5),
                            AutoAuth = reader.GetBoolean(6),
                            Title = reader.GetString(7),
                            Description = reader.GetString(8),
                            InsertDateTime = reader.GetDateTime(9),
                            UpdateDateTime = reader.GetDateTime(10),
                            ContactUsEmail = reader.IsDBNull(11)? null: reader.GetString(11)
                        };

                    apps.Add(app);
                }

                _cache.Add(apps, _expireCallback);
                SynchronizationRequested(Apps.GetCollectionCacheKey(),
                                         apps.ReferenceTracker.Purge(Constants.NULL_STRING));
                Log.Info("Retrieved new apps from the database.");
                return apps;
            }
            catch (Exception ex)
            {
                throw new BLException("Error executing GetApps.", ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
        }

        public void UpdateApp(App app)
        {
            try
            {
                if (app.AppId == 0)
                    throw new BLException("AppId value cannot be zero for update.");

                var command = new Command("mnAPI", "dbo.up_App_Update", 0);

                command.AddParameter("@AppID", SqlDbType.Int, ParameterDirection.Input, app.AppId);
                command.AddParameter("@BrandID", SqlDbType.Int, ParameterDirection.Input, app.BrandId);
                command.AddParameter("@Status", SqlDbType.SmallInt, ParameterDirection.Input, app.Status);
                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, app.MemberId);
                command.AddParameter("@Secret", SqlDbType.NVarChar, ParameterDirection.Input, app.Secret);
                command.AddParameter("@RedirectUrl", SqlDbType.NVarChar, ParameterDirection.Input, app.RedirectUrl);
                command.AddParameter("@AutoAuth", SqlDbType.Bit, ParameterDirection.Input, app.AutoAuth);
                command.AddParameter("@Title", SqlDbType.NVarChar, ParameterDirection.Input, app.Title);
                command.AddParameter("@Description", SqlDbType.NVarChar, ParameterDirection.Input, app.Description);

                Client.Instance.ExecuteAsyncWrite(command);

                _cache.Remove(Apps.GetCollectionCacheKey());

                ReplicationRequested(new ExpireApps(Apps.GetCollectionCacheKey()));
            }
            catch (Exception ex)
            {
                throw new BLException("Error executing UpdateApp.", ex);
            }
        }

        public int CreateApp(App app)
        {
            if (app.AppId != 0)
                throw new BLException("AppId value must be zero for create.");

            var appId = KeySA.Instance.GetKey("AppId");

            var command = new Command("mnAPI", "dbo.up_App_Insert", 0);

            command.AddParameter("@AppID", SqlDbType.Int, ParameterDirection.Input, appId);
            command.AddParameter("@BrandID", SqlDbType.Int, ParameterDirection.Input, app.BrandId);
            command.AddParameter("@Status", SqlDbType.SmallInt, ParameterDirection.Input, app.Status);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, app.MemberId);
            command.AddParameter("@Secret", SqlDbType.NVarChar, ParameterDirection.Input, app.Secret);
            command.AddParameter("@RedirectUrl", SqlDbType.NVarChar, ParameterDirection.Input, app.RedirectUrl);
            command.AddParameter("@AutoAuth", SqlDbType.Bit, ParameterDirection.Input, app.AutoAuth);
            command.AddParameter("@Title", SqlDbType.NVarChar, ParameterDirection.Input, app.Title);
            command.AddParameter("@Description", SqlDbType.NVarChar, ParameterDirection.Input, app.Description);

            var syncWriter = new TransactionalWriter();
            syncWriter.ExecuteSyncWrite(command);

            _cache.Remove(Apps.GetCollectionCacheKey());

            ReplicationRequested(new ExpireApps(Apps.GetCollectionCacheKey()));

            return appId;
        }

        public void DeleteApp(int appId)
        {
            var command = new Command("mnAPI", "dbo.up_App_Delete", 0);
            command.AddParameter("@AppID", SqlDbType.Int, ParameterDirection.Input, appId);

            Client.Instance.ExecuteAsyncWrite(command);

            _cache.Remove(Apps.GetCollectionCacheKey());

            ReplicationRequested(new ExpireApps(Apps.GetCollectionCacheKey()));
        }

        #endregion

        #region AppMember CRUD

        public AppMember GetAppMember(int appId, int memberId, string clientHostName, CacheReference cacheReference)
        {
            SqlDataReader reader = null;

            if (appId == 0 || appId == Constants.NULL_INT)
                throw new BLException("GetAppMember appId cannot be 0 or null int.");

            if (memberId == 0 || memberId == Constants.NULL_INT)
                throw new BLException("GetAppMember memberId cannot be 0 or null int.");

            if (string.IsNullOrEmpty(clientHostName))
                throw new BLException("GetAppMember clientHostName cannot be null or empty.");

            if (cacheReference == null)
                throw new BLException("GetAppMember cacheReference cannot be null.");

            try
            {
                var appMember = _cache.Get(AppMember.GetCacheKey(appId, memberId)) as AppMember;

                if (appMember == null)
                {
                    var command = new Command("mnAPI", "dbo.up_AppMember_List", 0);

                    command.AddParameter("@AppID", SqlDbType.Int, ParameterDirection.Input, appId);
                    command.AddParameter("@MemberId", SqlDbType.Int, ParameterDirection.Input, memberId);

                    reader = Client.Instance.ExecuteReader(command);

                    while (reader.Read())
                    {
                        appMember = new AppMember
                            {
                                AppId = reader.GetInt32(0),
                                MemberId = reader.GetInt32(1),
                                AccessToken = reader.GetString(2),
                                AccessTokenExpirationDateTime = reader.GetDateTime(3),
                                RefreshToken = reader.GetString(4),
                                RefreshTokenExpirationDateTime = reader.GetDateTime(5),
                                InsertDateTime = reader.GetDateTime(6),
                                UpdateDateTime = reader.GetDateTime(7)
                            };
                    }

                    if (appMember != null)
                    {
                        _cache.Add(appMember, _expireCallback);
                        SynchronizationRequested(appMember.GetCacheKey(),
                                                 appMember.ReferenceTracker.Purge(Constants.NULL_STRING));
                    }
                }

                if (appMember == null) return null;

                appMember.ReferenceTracker.Add(clientHostName, cacheReference);

                Log.InfoFormat("Added to referencetracker clienthostname:{0} cachKey:{1} address:{2} port:{3}", clientHostName,
                               appMember.GetCacheKey(), cacheReference.Address, cacheReference.Port);

                return appMember;
            }
            catch (Exception ex)
            {
                throw new BLException("Error executing GetApps.", ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
        }

        public void CreateAppMember(AppMember appMember)
        {
            var command = new Command("mnAPI", "dbo.up_AppMember_Insert", 0);

            command.AddParameter("@AppID", SqlDbType.Int, ParameterDirection.Input, appMember.AppId);
            command.AddParameter("@MemberId", SqlDbType.Int, ParameterDirection.Input, appMember.MemberId);
            command.AddParameter("@AccessToken", SqlDbType.NVarChar, ParameterDirection.Input, appMember.AccessToken);
            command.AddParameter("@AccessTokenExpirationDateTime", SqlDbType.SmallDateTime, ParameterDirection.Input,
                                 appMember.AccessTokenExpirationDateTime);
            command.AddParameter("@RefreshToken", SqlDbType.NVarChar, ParameterDirection.Input, appMember.RefreshToken);
            command.AddParameter("@RefreshTokenExpirationDateTime", SqlDbType.SmallDateTime, ParameterDirection.Input,
                                 appMember.RefreshTokenExpirationDateTime);

            var syncWriter = new TransactionalWriter();
            syncWriter.ExecuteSyncWrite(command);

            _cache.Remove(appMember.GetCacheKey());

            ReplicationRequested(new ExpireAppMember(appMember.GetCacheKey()));

            Log.InfoFormat("Created new appmember. memberId:{0} accessToken:{1}", appMember.MemberId, appMember.AccessToken);
        }

        public void UpdateAppMember(AppMember appMember)
        {
            var command = new Command("mnAPI", "dbo.up_AppMember_Update", 0);

            command.AddParameter("@AppID", SqlDbType.Int, ParameterDirection.Input, appMember.AppId);
            command.AddParameter("@MemberId", SqlDbType.Int, ParameterDirection.Input, appMember.MemberId);
            command.AddParameter("@AccessToken", SqlDbType.NVarChar, ParameterDirection.Input, appMember.AccessToken);
            command.AddParameter("@AccessTokenExpirationDateTime", SqlDbType.SmallDateTime, ParameterDirection.Input,
                                 appMember.AccessTokenExpirationDateTime);
            command.AddParameter("@RefreshToken", SqlDbType.NVarChar, ParameterDirection.Input, appMember.RefreshToken);
            command.AddParameter("@RefreshTokenExpirationDateTime", SqlDbType.SmallDateTime, ParameterDirection.Input,
                                 appMember.RefreshTokenExpirationDateTime);

            Client.Instance.ExecuteAsyncWrite(command);

            _cache.Remove(appMember.GetCacheKey());

            ReplicationRequested(new ExpireAppMember(appMember.GetCacheKey()));

            SynchronizationRequested(appMember.GetCacheKey(),
                                     appMember.ReferenceTracker.Purge(Constants.NULL_STRING));
        }

        public void DeleteAppMember(int appId, int memberId)
        {
            var cacheKey = AppMember.GetCacheKey(appId, memberId);

            var appMember = _cache.Get(AppMember.GetCacheKey(appId, memberId)) as AppMember;

            // even if not in cache, tell the partner to remove
            ReplicationRequested(new ExpireAppMember(cacheKey));

            var command = new Command("mnAPI", "dbo.up_AppMember_Delete", 0);
            command.AddParameter("@AppID", SqlDbType.Int, ParameterDirection.Input, appId);
            command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberId);
            Client.Instance.ExecuteAsyncWrite(command);

            if (appMember != null)
            {
                Log.InfoFormat("AppMember found in cache. Deleted from DB. appId:{0}, memberId:{1}", appId, memberId);
                
                var cacheReferences = appMember.ReferenceTracker.Purge(Constants.NULL_STRING);
                SynchronizationRequested(appMember.GetCacheKey(), cacheReferences);
            }

            /* this block is great for debugging synchronization calls. make sure every client gets called.
            var de = cacheReferences.GetEnumerator();
            while (de.MoveNext())
            {
                var address = ((CacheReference) de.Value).Address;
                var port = ((CacheReference) de.Value).Port;
                var protocol = ((CacheReference) de.Value).Protocol;
                Log.DebugFormat("Sync key:{0} address:{1} port:{2} protocol:{3}", cacheKey, address, port, protocol);
            }
             */

            _cache.Remove(cacheKey);
        }

        /// <summary>
        ///     Deletes all appmember records for the member
        /// </summary>
        /// <param name="memberId"></param>
        public void DeleteAppMembers(int memberId)
        {
            try
            {
                Log.InfoFormat("Delete appmembers for memberId:{0}", memberId);

                var apps = GetApps();
                foreach (var app in apps)
                {
                    DeleteAppMember(app.AppId, memberId);
                }
            }
            catch (Exception exception)
            {
                var errorMessage = string.Format("Error deleting appmember records for memberId:{0} exception:{1}",
                                                 memberId, exception);
                Log.ErrorFormat(errorMessage);
                throw new BLException(errorMessage);
            }
        }

        #endregion

        public void PlayReplicationAction(IReplicationAction replicationAction)
        {
            var cacheKey = string.Empty;
            switch (replicationAction.GetType().Name)
            {
                case "ExpireApps":
                    var expireAppsAction = replicationAction as ExpireApps;
                    if (expireAppsAction != null)
                    {
                        cacheKey = expireAppsAction.CacheKey;
                        var apps = _cache.Get(cacheKey) as Apps;
                        if (apps != null)
                            SynchronizationRequested(cacheKey, apps.ReferenceTracker.Purge(Constants.NULL_STRING));        
                        _cache.Remove(cacheKey);
                    }
                    break;
                case "ExpireAppMember":
                    var expireAppMemberAction = replicationAction as ExpireAppMember;
                    if (expireAppMemberAction != null)
                    {
                        cacheKey = expireAppMemberAction.CacheKey;
                        var appMember = _cache.Get(cacheKey) as AppMember;
                        if (appMember != null)
                            SynchronizationRequested(cacheKey, appMember.ReferenceTracker.Purge(Constants.NULL_STRING));        
                        _cache.Remove(cacheKey);
                    }
                    break;
            }
            Log.InfoFormat("PlayReplicationAction for {0} cacheKey:{1}", replicationAction.GetType().Name, cacheKey);
        }

        // Explicitly to handle when a cached vo expires given its TTL.
        private void ExpireCallback(string key, object value, CacheItemRemovedReason callbackreason)
        {
            switch (value.GetType().Name)
            {
                case "Apps":
                    var apps = value as Apps;
                    if (apps != null)
                        SynchronizationRequested(key, apps.ReferenceTracker.Purge(Constants.NULL_STRING));
                    break;
                case "AppMember":
                    var appMember = value as AppMember;
                    if (appMember != null)
                        SynchronizationRequested(key, appMember.ReferenceTracker.Purge(Constants.NULL_STRING));
                    break;
            }
            Log.InfoFormat("SynchronizationRequested for {0}, key:{1}", value.GetType().Name, key);
        }
    }
}