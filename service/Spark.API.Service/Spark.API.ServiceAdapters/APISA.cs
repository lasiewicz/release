﻿#region

using System;
using System.Linq;
using System.Reflection;
using Matchnet.CacheSynchronization.Context;
using Matchnet.Caching;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Spark.API.ValueObjects;
using Spark.API.ValueObjects.ServiceDefinitions;
using log4net;

#endregion

namespace Spark.API.ServiceAdapters
{
    public class APISA : SABase
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (APISA));

        private readonly Cache _cache;

        #region Singleton

        public static readonly APISA Instance = new APISA();

        private APISA()
        {
            _cache = Cache.Instance;
            //this is here purely because otherwise it gets a null reference the first time it's called in GetAppMember, causing an exception to be thrown in the BL
            var preload = Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(AppMember.DefaultCacheTtlSeconds));
        }

        #endregion

        #region Overrides of SABase

        protected override void GetConnectionLimit()
        {
            MaxConnections = Convert.ToByte(RuntimeSettings.GetSetting("APISVC_SA_CONNECTION_LIMIT"));
        }

        #endregion

        #region Implementation of IAPIService

        public Apps GetApps()
        {
            Apps apps;
            var uri = string.Empty;
            try
            {
                apps = _cache.Get(Apps.GetCollectionCacheKey()) as Apps;

                if (apps != null) return apps;

                uri = GetServiceManagerUri(ServiceConstants.ServiceManagerName);
                Checkout(uri);
                try
                {
                    apps = GetService(uri).GetApps();
                }
                finally
                {
                    Checkin(uri);
                }

                _cache.Add(apps);

                Log.Debug("Called the service to get apps");
            }
            catch (Exception ex)
            {
                throw (new SAException(MethodBase.GetCurrentMethod().Name + " (uri: " + uri + ").", ex));
            }
            return apps;
        }

        public int CreateApp(App app)
        {
            var uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.ServiceManagerName);
                Checkout(uri);
                try
                {
                    _cache.Remove(Apps.GetCollectionCacheKey());
                    return GetService(uri).CreateApp(app);
                }
                finally
                {
                    Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(MethodBase.GetCurrentMethod().Name + " (uri: " + uri + ").", ex));
            }
        }

        public void UpdateApp(App app)
        {
            var uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.ServiceManagerName);
                Checkout(uri);
                try
                {
                    GetService(uri).UpdateApp(app);
                }
                finally
                {
                    Checkin(uri);
                }

                _cache.Remove(Apps.GetCollectionCacheKey());
            }
            catch (Exception ex)
            {
                throw (new SAException(MethodBase.GetCurrentMethod().Name + " (uri: " + uri + ").", ex));
            }
        }

        public void DeleteApp(int appId)
        {
            var uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.ServiceManagerName);
                Checkout(uri);
                try
                {
                    GetService(uri).DeleteApp(appId);
                }
                finally
                {
                    Checkin(uri);
                }

                _cache.Remove(Apps.GetCollectionCacheKey());
            }
            catch (Exception ex)
            {
                throw (new SAException(MethodBase.GetCurrentMethod().Name + " (uri: " + uri + ").", ex));
            }
        }

        public AppMember GetAppMember(int appId, int memberId)
        {
            AppMember appMember;
            var uri = string.Empty;
            try
            {
                appMember = _cache.Get(AppMember.GetCacheKey(appId, memberId)) as AppMember;

                if (appMember != null) return appMember;

                uri = GetServiceManagerUri(ServiceConstants.ServiceManagerName);
                Checkout(uri);
                try
                {
                    appMember = GetService(uri).GetAppMember(appId, memberId, Environment.MachineName,
                                                             Evaluator.Instance.GetReference(
                                                                 DateTime.Now.AddSeconds(
                                                                     AppMember.DefaultCacheTtlSeconds)));
                }
                finally
                {
                    Checkin(uri);
                }

                _cache.Add(appMember);

                Log.DebugFormat("Called the service to get appmember {0} for app {1}", memberId, appId);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw (new SAException(MethodBase.GetCurrentMethod().Name + " (uri: " + uri + ").", ex));
            }
            return appMember;
        }

        public void CreateAppMember(AppMember appMember)
        {
            var uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.ServiceManagerName);
                Checkout(uri);
                try
                {
                    GetService(uri).CreateAppMember(appMember);
                }
                finally
                {
                    Checkin(uri);
                }

                _cache.Remove(appMember.GetCacheKey());
            }
            catch (Exception ex)
            {
                throw (new SAException(MethodBase.GetCurrentMethod().Name + " (uri: " + uri + ").", ex));
            }
        }

        public void UpdateAppMember(AppMember appMember)
        {
            var uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.ServiceManagerName);
                Checkout(uri);
                try
                {
                    GetService(uri).UpdateAppMember(appMember);
                }
                finally
                {
                    Checkin(uri);
                }

                _cache.Remove(appMember.GetCacheKey());
            }
            catch (Exception ex)
            {
                throw (new SAException(MethodBase.GetCurrentMethod().Name + " (uri: " + uri + ").", ex));
            }
        }

        public void DeleteAppMember(int appId, int memberId)
        {
            var uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.ServiceManagerName);
                Checkout(uri);
                try
                {
                    GetService(uri).DeleteAppMember(appId, memberId);
                }
                finally
                {
                    Checkin(uri);
                }

                _cache.Remove(AppMember.GetCacheKey(appId, memberId));
            }
            catch (Exception ex)
            {
                throw (new SAException(MethodBase.GetCurrentMethod().Name + " (uri: " + uri + ").", ex));
            }
        }

        public void DeleteAppMembers(int memberId)
        {
            var uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.ServiceManagerName);
                Checkout(uri);
                try
                {
                    GetService(uri).DeleteAppMembers(memberId);
                }
                finally
                {
                    Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException(MethodBase.GetCurrentMethod().Name + " (uri: " + uri + ").", ex));
            }
        }

        #endregion

        public App GetApp(int appId)
        {
            var apps = GetApps();
            var app = (from a in apps
                       where a.AppId == appId
                       select a).First();
            return app;
        }

        private static string GetServiceManagerUri(string serviceManagerName)
        {
            try
            {
                var uri = AdapterConfigurationSA.GetServicePartition(ServiceConstants.ServiceConstant,
                                                                     PartitionMode.Random).ToUri(serviceManagerName);
                var overrideHostName = RuntimeSettings.GetSetting("APISVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Length > 0)
                {
                    var uriBuilder = new UriBuilder(new Uri(uri));
                    uri = "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot get configuration settings for remote service manager.", ex);
            }
        }

        private static IAPIService GetService(string uri)
        {
            try
            {
                return (IAPIService) Activator.GetObject(typeof (IAPIService), uri);
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot activate remote service manager at " + uri, ex);
            }
        }
    }
}