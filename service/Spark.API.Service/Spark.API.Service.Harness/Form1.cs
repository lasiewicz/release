﻿#region

using System;
using System.Windows.Forms;
using Spark.API.ServiceAdapters;

#endregion

namespace Spark.API.Service.Harness
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1Click(object sender, EventArgs e)
        {
            var apps = APISA.Instance.GetApps();
            dataGridView1.DataSource = apps;
        }
    }
}