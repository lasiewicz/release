﻿#region

using System;
using System.Collections;
using System.Reflection;
using Matchnet;
using Matchnet.CacheSynchronization;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.InitialConfiguration;
using Matchnet.Replication;
using Spark.API.BusinessLogic;
using Spark.API.ValueObjects;
using Spark.API.ValueObjects.ServiceDefinitions;
using log4net;

#endregion

namespace Spark.API.ServiceManagers
{
    public class APISM : MarshalByRefObject, IAPIService, IServiceManager, IBackgroundProcessor,
                         IReplicationActionRecipient
    {
        private static Replicator _replicator;
        private static Synchronizer _synchronizer;
        private static readonly ILog Log = LogManager.GetLogger(typeof (APISM));
        private readonly HydraWriter _hydraWriter;

        public APISM()
        {
            Log.InfoFormat("Starting APISM");
            APIBL.Instance.SynchronizationRequested += APIBL_SynchronizationRequested;

            _hydraWriter = new HydraWriter(new string[] {"mnAPI"});
            _hydraWriter.Start();

            var machineName = Environment.MachineName;
            var overrideMachineName = InitializationSettings.Get("configuration/settings/MachineNameOverride");
            if (overrideMachineName != null)
            {
                machineName = overrideMachineName;
            }

            var replicationUri = RuntimeSettings.GetSetting("APISVC_REPLICATION_OVERRIDE");

            if (replicationUri.Length == 0)
            {
                replicationUri =
                    AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(
                        ServiceConstants.ServiceConstant, ServiceConstants.ServiceManagerName, machineName);
            }

            if (!String.IsNullOrEmpty(replicationUri))
            {
                APIBL.Instance.ReplicationRequested += InstanceReplicationRequested;
                _replicator = new Replicator(ServiceConstants.ServiceManagerName);
                _replicator.SetDestinationUri(replicationUri);
                _replicator.Start();
            }

            _synchronizer = new Synchronizer(ServiceConstants.ServiceName);
            _synchronizer.Start();
        }

        private static void InstanceReplicationRequested(IReplicationAction replicationAction)
        {
            _replicator.Enqueue(replicationAction);
        }

        public override object InitializeLifetimeService()
        {
            return null;
        }

        private void APIBL_SynchronizationRequested(string key, Hashtable cacheReferences)
        {
            _synchronizer.Enqueue(key, cacheReferences);
        }

        #region Implementation of IAPIService

        public Apps GetApps()
        {
            try
            {
                return APIBL.Instance.GetApps();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.ServiceName, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public int CreateApp(App app)
        {
            try
            {
                return APIBL.Instance.CreateApp(app);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.ServiceName, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public void UpdateApp(App app)
        {
            try
            {
                APIBL.Instance.UpdateApp(app);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.ServiceName, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public void DeleteApp(int appId)
        {
            try
            {
                APIBL.Instance.DeleteApp(appId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.ServiceName, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public AppMember GetAppMember(int appId, int memberId, string clientHostName, CacheReference cacheReference)
        {
            try
            {
                return APIBL.Instance.GetAppMember(appId, memberId, clientHostName, cacheReference);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.ServiceName, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public void CreateAppMember(AppMember appMember)
        {
            try
            {
                APIBL.Instance.CreateAppMember(appMember);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.ServiceName, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public void UpdateAppMember(AppMember appMember)
        {
            try
            {
                APIBL.Instance.UpdateAppMember(appMember);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.ServiceName, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public void DeleteAppMember(int appId, int memberId)
        {
            try
            {
                APIBL.Instance.DeleteAppMember(appId, memberId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.ServiceName, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        public void DeleteAppMembers(int memberId)
        {
            try
            {
                APIBL.Instance.DeleteAppMembers(memberId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.ServiceName, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        #endregion

        #region Implementation of IBackgroundProcessor

        public void Start()
        {
        }

        public void Stop()
        {
        }

        #endregion

        #region Implementation of IServiceManager

        public void PrePopulateCache()
        {
        }

        #endregion

        #region Implementation of IDisposable

        public void Dispose()
        {
            if (_replicator != null)
            {
                _replicator.Stop();
            }

            if (_synchronizer != null)
            {
                _synchronizer.Stop();
            }

            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }
        }

        #endregion

        #region Implementation of IReplicationActionRecipient

        public void Receive(IReplicationAction replicationAction)
        {
            try
            {
                APIBL.Instance.PlayReplicationAction(replicationAction);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.ServiceName, MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        #endregion
    }
}