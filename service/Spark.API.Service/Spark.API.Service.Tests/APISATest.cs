﻿#region

using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Spark.API.ServiceAdapters;
using Spark.API.ValueObjects;

#endregion

namespace Spark.API.Service.Tests
{
    /// <summary>
    ///     This is a test class for APISATest and is intended
    ///     to contain all APISATest Unit Tests
    /// </summary>
    [TestClass]
    public class APISATest
    {
        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion

        /// <summary>
        ///     A test for GetApps
        /// </summary>
        [TestMethod]
        public void GetAppsTest()
        {
            const int expected = 16000206;
            var actual = APISA.Instance.GetApps();
            Assert.AreEqual(expected, actual[0].MemberId);
        }

        [TestMethod]
        public void GetAppTest()
        {
            var actual = APISA.Instance.GetApp(1);

            Assert.IsTrue(actual.AppId == 1);
        }

        [TestMethod]
        public void CreateAppTest()
        {
            var app = new App
                {
                    AppId = 0,
                    AutoAuth = true,
                    BrandId = 1003,
                    MemberId = 16000206,
                    Secret = "mynewsecret",
                    Status = 1,
                    Description = "mydescription",
                    RedirectUrl = "http://myredirecturl.com",
                    Title = "mytitle"
                };

            var newAppId = APISA.Instance.CreateApp(app);
            System.Threading.Thread.Sleep(2000);
            var apps = APISA.Instance.GetApps();

            var actual = (from a in apps
                          where a.AppId == newAppId
                          select a).First();
            Assert.IsTrue(actual != null);
        }

        [TestMethod]
        public void UpdateAppTest()
        {
            var app = new App
                {
                    AppId = 1,
                    AutoAuth = true,
                    BrandId = 1003,
                    MemberId = 16000206,
                    Secret = "mynewsecretwoot3",
                    Status = 1,
                    RedirectUrl = "redirecturl",
                    Title = "mytitle",
                    Description = "mydescription"
                };
            APISA.Instance.UpdateApp(app);
            System.Threading.Thread.Sleep(2000);
            var apps = APISA.Instance.GetApps();

            var actual = (from a in apps
                          where a.AppId == 1
                          select a).First();
            Assert.IsTrue(actual.Secret == "mynewsecretwoot3");
        }

        [TestMethod]
        public void DeleteAppTest()
        {
            var app = new App
                {
                    AppId = 0,
                    AutoAuth = true,
                    BrandId = 1003,
                    MemberId = 16000206,
                    Secret = "mynewsecret",
                    Status = 1,
                    Description = "mydescription",
                    RedirectUrl = "http://myredirecturl.com",
                    Title = "mytitle"
                };

            var newAppId = APISA.Instance.CreateApp(app);
            System.Threading.Thread.Sleep(2000);
            System.Diagnostics.Debug.WriteLine("NewAppId:" + newAppId);
            APISA.Instance.DeleteApp(newAppId);
            System.Threading.Thread.Sleep(2000);
            var apps = APISA.Instance.GetApps();

            var actual = (from a in apps
                          where a.AppId == newAppId
                          select a).Count();
            Assert.IsTrue(actual == 0);
        }

        [TestMethod]
        public void GetAppMemberTest()
        {
            var appMember = APISA.Instance.GetAppMember(1, 16000206);
            Assert.AreEqual(16000206, appMember.MemberId);
        }

        [TestMethod]
        public void CreateAppMemberTest()
        {
            APISA.Instance.DeleteAppMember(1, 16000206);
            System.Threading.Thread.Sleep(2000);
            var appMember = new AppMember
                {
                    AccessToken = "myaccesstoken",
                    AccessTokenExpirationDateTime = DateTime.Now,
                    AppId = 1,
                    MemberId = 16000206,
                    RefreshToken = "myrefreshtoken",
                    RefreshTokenExpirationDateTime = DateTime.Now
                };

            APISA.Instance.CreateAppMember(appMember);
            System.Threading.Thread.Sleep(2000);
            var actual = APISA.Instance.GetAppMember(1, 16000206);

            Assert.IsTrue(actual != null);
        }

        [TestMethod]
        public void UpdateAppMemberTest()
        {
            var appMember = new AppMember
                {
                    AccessToken = "myaccesstokenupdate",
                    AccessTokenExpirationDateTime = DateTime.Now,
                    AppId = 1,
                    MemberId = 16000206,
                    RefreshToken = "myrefreshtokenupdate",
                    RefreshTokenExpirationDateTime = DateTime.Now
                };
            APISA.Instance.UpdateAppMember(appMember);
            System.Threading.Thread.Sleep(2000);
            var actual = APISA.Instance.GetAppMember(1, 16000206);

            Assert.IsTrue(actual.RefreshToken == "myrefreshtokenupdate");
        }

        [TestMethod]
        public void DeleteAppMembersTest()
        {
            APISA.Instance.DeleteAppMembers(111644580);
        }
    }
}