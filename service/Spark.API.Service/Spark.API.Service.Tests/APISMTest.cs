﻿#region

using System;
using System.Linq;
using Matchnet.CacheSynchronization.Context;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Spark.API.ServiceManagers;
using Spark.API.ValueObjects;

#endregion

namespace Spark.API.Service.Tests
{
    /// <summary>
    ///     This is a test class for APISMTest and is intended
    ///     to contain all APISMTest Unit Tests
    /// </summary>
    [TestClass]
    public class APISMTest
    {
        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion

        /// <summary>
        ///     A test for GetApps
        ///     Dev -Test SPs.sql
        /// </summary>
        [TestMethod]
        public void GetAppsTest()
        {
            var target = new APISM();
            var actual = target.GetApps();
            Assert.IsTrue(actual.Count > 0);
        }

        /// <summary>
        ///     A test for GetApps
        ///     Dev -Test SPs.sql
        /// </summary>
        [TestMethod]
        public void UpdateAppTest()
        {
            var target = new APISM();
            var app = new App
                {
                    AppId = 1,
                    AutoAuth = true,
                    BrandId = 1003,
                    MemberId = 16000206,
                    Secret = "mysecret",
                    Status = 1,
                    RedirectUrl = "redirecturl"
                };
            target.UpdateApp(app);

            var apps = target.GetApps();

            var actual = (from a in apps
                          where a.AppId == 1
                          select a).First();
            Assert.IsTrue(actual.MemberId == 16000206);
        }

        /// <summary>
        ///     A test for GetMember
        ///     Dev -Test SPs.sql
        /// </summary>
        [TestMethod]
        public void GetAppMemberTest()
        {
            var target = new APISM();
            var actual = target.GetAppMember(1, 16000206, Environment.MachineName, Evaluator.Instance.GetReference(
                DateTime.Now.AddSeconds(
                    AppMember.DefaultCacheTtlSeconds)));
            Assert.IsTrue(actual.MemberId == 16000206);
        }
    }
}