﻿#region

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceProcess;
using Matchnet.RemotingServices;
using Spark.API.ServiceManagers;
using Spark.API.ValueObjects;
using log4net;

#endregion

namespace Spark.API.Service
{
    internal partial class Service : RemotingServiceBase
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (Service));
        private APISM _APISM;
        private Container _components;

        public Service()
        {
            InitializeComponent();
            log4net.Config.XmlConfigurator.Configure();
        }

        private static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;

            Trace.WriteLine("Spark.API.Service: Main()");

            var servicesToRun = new ServiceBase[]
                {
                    new Service()
                };
            Run(servicesToRun);
        }

        private static void OnUnhandledException(Object sender, UnhandledExceptionEventArgs e)
        {
            Trace.WriteLine("Spark.API.Service: OnUnhandledException()");
            var ex = e.ExceptionObject as Exception;

            if (ex != null)
            {
                EventLog.WriteEntry(ServiceConstants.ServiceName,
                                    "Unhandled exception:" + ex,
                                    EventLogEntryType.Error);
            }
        }

        protected override void RegisterServiceManagers()
        {
            Log.Info("Registering the service manager.");
            Trace.WriteLine("Spark.API.Service, Start RegisterServiceManagers");

            _APISM = new APISM();
            RegisterServiceManager(_APISM);

            base.RegisterServiceManagers();
        }
    }
}