﻿using System.Diagnostics;
using Spark.API.ValueObjects;

namespace Spark.API.Service
{
    partial class Service
    {
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_components != null)
                {
                    _components.Dispose();
                }
            }

            if (_APISM != null)
            {
                _APISM.Dispose();
            }

            base.Dispose(disposing);
        }


        #region Component Designer generated code

        private void InitializeComponent()
        {
            Trace.WriteLine("Spark.API.Service: InitializeComponent()");
            
            _components = new System.ComponentModel.Container();
            ServiceName = ServiceConstants.ServiceName;
            ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(
                ServiceConstants.ServiceConstant, Matchnet.InitialConfiguration.InitializationSettings.MachineName);

        }

        #endregion
    }
}
