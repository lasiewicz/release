﻿#region

using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using System.ServiceProcess;

#endregion

namespace Spark.API.Service
{
    [RunInstaller(true)]
    public partial class Installer : System.Configuration.Install.Installer
    {
        private ServiceInstaller _serviceInstaller;
        private ServiceProcessInstaller _serviceProcessInstaller;

        public Installer()
        {
            InitializeComponent();
        }

        private static void ProjectInstallerAfterInstall(object sender, InstallEventArgs e)
        {
            Trace.WriteLine("Spark.API.Service: ProjectInstaller_AfterInstall.");
        }

        private static void ProjectInstallerBeforeUninstall(object sender, InstallEventArgs e)
        {
            Trace.WriteLine("Spark.API.Service: ProjectInstaller_BeforeUninstall.");
        }
    }
}