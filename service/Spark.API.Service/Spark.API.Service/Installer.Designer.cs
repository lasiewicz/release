﻿namespace Spark.API.Service
{
    partial class Installer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();

            _serviceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            _serviceInstaller = new System.ServiceProcess.ServiceInstaller();

            _serviceProcessInstaller.Password = null;
            _serviceProcessInstaller.Username = null;

            _serviceInstaller.ServiceName = Spark.API.ValueObjects.ServiceConstants.ServiceName;
            _serviceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            _serviceInstaller.ServicesDependedOn = new string[] { "MSMQ" };

            // 
            // ProjectInstaller
            // 
            Installers.AddRange(new System.Configuration.Install.Installer[] {
																					  _serviceProcessInstaller,
																					  _serviceInstaller});
            // Event handlers
            BeforeUninstall += new System.Configuration.Install.InstallEventHandler(ProjectInstallerBeforeUninstall);
            AfterInstall += new System.Configuration.Install.InstallEventHandler(ProjectInstallerAfterInstall);

        }

        #endregion
    }
}