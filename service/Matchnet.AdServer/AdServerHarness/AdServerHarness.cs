using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Matchnet.AdServer.ServiceManagers;

namespace AdServerHarness
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class AdServerHarness : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private AdServerSM _adserverSM = new AdServerSM();

		public AdServerHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			_adserverSM.InitSlots();
			_adserverSM.InitMediaPlans();
			_adserverSM.InitMPContent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 266);
			this.Name = "Form1";
			this.Text = "Ad Server Test Harness";

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new AdServerHarness());
		}
	}
}
