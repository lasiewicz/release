using System;
using System.Collections;
using Matchnet.Exceptions;
using Matchnet.AdServer.ValueObjects;
using Matchnet.AdServer.ValueObjects.ServiceDefinitions;
using Matchnet.Caching;

namespace Matchnet.AdServer.ServiceAdapters
{
	public class AdServerSA
	{
		#region class variables
		private Cache _cache = null;
		#endregion

		#region singleton
		/// <summary>
		/// 
		/// </summary>
		public static readonly AdServerSA Instance = new AdServerSA();

		/// <summary>
		/// 
		/// </summary>
		private AdServerSA()
		{
			_cache = Cache.Instance;
		}
		#endregion

		private IAdServerService getService(string uri)
		{
			try
			{
				return (IAdServerService)Activator.GetObject(typeof(IAdServerService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		public Hashtable RefreshSlots()
		{
			string uri = string.Empty;

			try
			{
				//uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				uri = "tcp://192.168.3.115:53000/AdServerSM.rem";
				Hashtable oResult = getService(uri).RefreshSlots();

				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve Test Content (uri: " + uri + "):" + ex.Message, ex));
			}
		}

		
		public Hashtable RetrieveMediaPlans()
		{
			string uri = string.Empty;
			Hashtable ret = null;
			try
			{
				ret = (Hashtable)Cache.Instance.Get("ADSERVER_MEDIAPLANS");
				if(ret==null)
				{
					//uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					uri = "tcp://192.168.3.115:53000/AdServerSM.rem";
					ret = getService(uri).RefreshMediaPlans();
				}
				return ret;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve Media Plans (uri: " + uri + "):" + ex.Message, ex));
			}
		}

		public Hashtable RetrieveSlots()
		{
			string uri = string.Empty;
			Hashtable ret = null;
			try
			{
				ret = (Hashtable)Cache.Instance.Get("ADSERVER_SLOTS");
				if(ret==null)
				{
					//uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					uri = "tcp://192.168.3.115:53000/AdServerSM.rem";
					ret = getService(uri).RefreshSlots();
				}
				return ret;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve Slots (uri: " + uri + "):" + ex.Message, ex));
			}
		}
	}
}
