using System;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using Matchnet.Web.Framework;
using Matchnet.Content;
using Matchnet.Member.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.AdServer.ValueObjects;

namespace Matchnet.AdServer.BusinessLogic
{
	/// <summary>
	/// Summary description for AttributeComparer.
	/// </summary>
	public class AttributeComparer
	{
		private static Hashtable _mediaplans = Hashtable.Synchronized(new Hashtable());		//Slot ids with attribute groups, to determine the media plan
		private static ContextGlobal _g;

		public AttributeComparer(Hashtable mp, ContextGlobal g)
		{
			_mediaplans = mp;
			_g = g;
		}

		public AttributeComparer(ContextGlobal g)
		{
			_g = g;
		}

		public Hashtable MediaPlans
		{
			get
			{
				return(_mediaplans);
			}
			set
			{
				_mediaplans = value;
			}
		}

		public bool Compare(string attrib, string attribvalue, NameValueCollection queryString)
		{
			string attributeid = attrib.Substring(0,attrib.IndexOf("***"));
			int a = attrib.IndexOf("***");
			int b = (attrib.Length-attrib.LastIndexOf("*"))-1;
			int numericvalue = 0;
			string attribname = attrib.Substring(a+3, b);
			string attribvalueid = attribvalue.Substring(0,attribvalue.IndexOf("***"));
			string attribvaluedescription = attribvalue.Substring(attribvalue.IndexOf("***")+3, (attribvalue.Length-attribvalue.LastIndexOf("*"))-1);
			bool tempval = false;
			string regionstring;
			//try{numericvalue = Convert.ToInt32(attribvaluedescription);}
			//catch{}
			try
			{
				switch(attribname.ToLower())
				{
					case "city":
						regionstring = Matchnet.Web.Framework.FrameworkGlobals.GetRegionString(_g.Member.GetAttributeInt(_g.Brand, "regionid"),
							_g.Brand.Site.LanguageID,
							false,true,false);
						string city = regionstring.Substring(0, regionstring.IndexOf(","));
						tempval=(city.ToLower()==attribvaluedescription.ToLower());
						break;
					case "state":
						regionstring = Matchnet.Web.Framework.FrameworkGlobals.GetRegionString(_g.Member.GetAttributeInt(_g.Brand, "regionid"),
							_g.Brand.Site.LanguageID,
							false,true,false);
						string state = regionstring.Substring(regionstring.IndexOf(","), regionstring.Length-regionstring.IndexOf(","));
						tempval=(state.ToLower()==attribvaluedescription.ToLower());
						break;
					case "country":
						regionstring = Matchnet.Web.Framework.FrameworkGlobals.GetRegionString(_g.Member.GetAttributeInt(_g.Brand, "regionid"),
							_g.Brand.Site.LanguageID,
							false,true,false);
						string country = regionstring.Substring(regionstring.LastIndexOf(",")+1, regionstring.Length-regionstring.LastIndexOf(",")-1).Trim();
						if(country.Trim().Length==2)
						{
							//That's the state, country is usa
							if(attribvaluedescription.ToLower()=="usa")
								tempval = true;
							else
								tempval = false;
							break;
						}
						tempval=(country.Trim().ToLower()==attribvaluedescription.ToLower());
						break;
					case "status":
						if(attribvaluedescription.ToLower()=="visitor" && _g.Member==null)
							tempval=true;
						else if(attribvaluedescription.ToLower()=="registered member" && _g.Member!=null && !_g.Member.IsPayingMember(_g.Brand.Site.SiteID))
							tempval=true;
						else if(attribvaluedescription.ToLower()=="subscriber" && _g.Member!=null && _g.Member.IsPayingMember(_g.Brand.Site.SiteID))
							tempval=true;
						break;
					case "religion":
						tempval=(_g.Brand.Site.SiteID!=103 && _g.Member.GetAttributeInt(_g.Brand, "religion")==Convert.ToInt32(attribvalueid));
						break;
					case "jdatereligion":
						tempval=(_g.Brand.Site.SiteID==103 && _g.Member.GetAttributeInt(_g.Brand, "religion")==Convert.ToInt32(attribvalueid));
						break;
					case "gendermask":
						tempval=(MapGender(FrameworkGlobals.GetGenderString(_g.Member.GetAttributeInt(_g.Brand, "gendermask")))==Convert.ToInt32(attribvalueid));
						break;
					case "tease":
						tempval=(_g.List.GetCount(HotListCategory.MembersYouTeased,_g.Brand.Site.Community.CommunityID,_g.Brand.Site.SiteID)>=Convert.ToInt32(attribvaluedescription));
						break;
					case "perfectfirstdateessay":
						tempval=GetEssayComplete("perfectfirstdateessay",attribvaluedescription);
						break;
					case "aboutme":
						tempval=GetEssayComplete("aboutme",attribvaluedescription);
						break;
					case "idealrelationshipessay":
						tempval=GetEssayComplete("idealrelationshipessay",attribvaluedescription);
						break;
					case "learnfromthepastessay":
						tempval=GetEssayComplete("learnfromthepastessay",attribvaluedescription);
						break;
					case "perfectmatchessay":
						tempval=GetEssayComplete("perfectmatchessay",attribvaluedescription);
						break;
					case "desiredgender":
						tempval=(MapGenderString(FrameworkGlobals.GetSeekingGenderString(_g.Member.GetAttributeInt(_g.Brand, "gendermask")))==attribvaluedescription);
						break;
					case "minage":
						tempval=(FrameworkGlobals.GetAge(_g.Member, _g.Brand)>=numericvalue);
						break;
					case "maxage":
						tempval=(FrameworkGlobals.GetAge(_g.Member, _g.Brand)<=numericvalue);
						break;
					case "weekday":
						tempval=(DateTime.Now.DayOfWeek.ToString().ToUpper()==attribvaluedescription.ToUpper());
						break;
					case "start hour":
						tempval=(Convert.ToInt32(attribvaluedescription)<=System.DateTime.Now.Hour);
						break;
					case "start minute":
						tempval=(Convert.ToInt32(attribvaluedescription)<=System.DateTime.Now.Minute);
						break;
					case "end hour":
						tempval=(Convert.ToInt32(attribvaluedescription)>System.DateTime.Now.Hour);
						break;
					case "end minute":
						tempval=(Convert.ToInt32(attribvaluedescription)>System.DateTime.Now.Minute);
						break;
					case "hasphotoflag":
						tempval=((_g.Member.GetPhotos(_g.TargetBrand.Site.Community.CommunityID).Count>0 && attribvaluedescription.ToUpper()=="YES") || (_g.Member.GetPhotos(_g.TargetBrand.Site.Community.CommunityID).Count==0 && attribvaluedescription.ToUpper()=="NO"));
						break;
					default:
						if(attributeid=="999999")	//This is a URL parameter
						{
							tempval=(queryString[attribname].ToString().ToLower()==attribvaluedescription);
							if(tempval==true)
								break;
						}
						else if(numericvalue>0)
						{
							//Maybe the attribute is available by name?
							try
							{
								tempval = (_g.Member.GetAttributeInt(_g.Brand, attribname)==numericvalue);}
							catch(Exception ex)
							{
								string msg = ex.Message;
							}
							//Maybe the attribute is available by ID?
							try
							{
								tempval = (_g.Member.GetAttributeInt(_g.Brand, Convert.ToInt32(attributeid))==numericvalue);}
							catch(Exception ex)
							{
								string msg = ex.Message;
							}
						}
						else
						{
							//Maybe the attribute is available by ID?
							try
							{
								tempval = (_g.Member.GetAttributeInt(_g.Brand, Convert.ToInt32(attributeid))==Convert.ToInt32(attribvalueid));
								if(tempval==true)
									break;
							}
							catch(Exception ex)
							{
								string msg = ex.Message;
							}
							//Maybe the attribute is available by name?
							try
							{
								tempval = (_g.Member.GetAttributeText(_g.Brand, attribname)==attribvaluedescription);
								if(tempval==true)
									break;
							}
							catch(Exception ex)
							{
								string msg = ex.Message;
							}
							//Maybe the attribute is available by ID?
							try
							{
								tempval = (_g.Member.GetAttributeInt(_g.Brand, attribname)==Convert.ToInt32(attribvalueid));
								if(tempval==true)
									break;
							}
							catch(Exception ex)
							{
								string msg = ex.Message;
							}
						}
						break;
				}
			}
			catch{}
			return tempval;
		}

		private int MapGender(string genderstring)
		{
			switch(genderstring.ToLower())
			{
				case "man":
					return 1;
				case "woman":
					return 2;
			}
			return 0;
		}

		private string MapGenderString(string genderstring)
		{
			switch(genderstring.ToLower())
			{
				case "man":
					return "Male";
				case "woman":
					return "Female";
			}
			return "";
		}

		private bool GetEssayComplete(string essayName, string attribval)
		{
			string tempstr=_g.Member.GetAttributeTextApproved(_g.Brand, essayName, _g.Member.MemberID, _g.GetResource("FREETEXT_NOT_APPROVED", null));
            if(attribval.ToUpper()=="YES")
				return tempstr.Length>0;
			else
				return tempstr.Length==0;
		}

	}
}
