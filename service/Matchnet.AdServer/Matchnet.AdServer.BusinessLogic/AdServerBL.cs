using System;
using System.Configuration;
using System.ComponentModel;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Messaging;
using System.Threading;
using Matchnet.Content;
using Matchnet.Web.Framework;
using Matchnet.Member.ServiceAdapters;
using Matchnet.AdServer.ValueObjects;


namespace Matchnet.AdServer.BusinessLogic
{
	/// <summary>
	/// Summary description for AdServerBL.
	/// </summary>
	public class AdServerBL
	{
		private static Hashtable slots = Hashtable.Synchronized(new Hashtable());				//List of slot ids along with their default content
		//private static Hashtable slotswithbases = Hashtable.Synchronized(new Hashtable());	//List of slot ids along with their base slot id's by private label
		//private static Hashtable mediaplans = Hashtable.Synchronized(new Hashtable());			//Slot ids with attribute groups, to determine the media plan
		private static Hashtable mnMediaplans = Hashtable.Synchronized(new Hashtable());		//Media plans
		//private static Hashtable mpcontent = Hashtable.Synchronized(new Hashtable());			//Media plans with distributed content
		//private static Hashtable mpcodes = Hashtable.Synchronized(new Hashtable());			//Codes to append to the clickthroughurl
		//private static Hashtable capcount = Hashtable.Synchronized(new Hashtable());			//In-memory cap counter
		private static Hashtable impressions = Hashtable.Synchronized(new Hashtable());			//Aggregated impressions to be sent to middle tier

		public AdServerBL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public void InitSlots()
		{	
			//Get default content for each slot
			//Hashtable ht = Hashtable.Synchronized(new Hashtable());
			slots.Clear();
			SqlDataReader reader = GetAllSlots();
			while(reader.Read())
			{
				Matchnet.AdServer.ValueObjects.Content c = new Matchnet.AdServer.ValueObjects.Content(reader["filename"].ToString(), reader["ClickthroughURL"].ToString(), reader["AltText"].ToString());
				Slot s = new Slot(reader.GetInt32(0), c);
				slots.Add(reader.GetInt32(0), s);
				//ht.Add(reader.GetInt32(0), c);
			}

			//slots = ht;
		}

		public void InitSlotsWithBases()
		{	
			//Used to map base slot id's to distinct slots
			//Hashtable ht = Hashtable.Synchronized(new Hashtable());
			SqlDataReader reader = GetSlotsWithBases();
			while(reader.Read())
			{
				Slot s = (Slot)slots[Convert.ToInt32(reader["slotid"].ToString())];
				//ht.Add(reader["zoneid"].ToString() + "000" + reader["positionordinal"].ToString() + "*" + reader["sparkpageid"].ToString() + "*" + reader["privatelabelid"].ToString(), reader["slotid"].ToString());
				s.ZoneID = Convert.ToInt32(reader["zoneid"].ToString());
				s.PositionOrdinal = Convert.ToInt32(reader["zoneid"].ToString());
				s.PageID = Convert.ToInt32(reader["sparkpageid"].ToString());
				s.PrivateLabelID = Convert.ToInt32(reader["privatelabelid"].ToString());
				slots.Remove(Convert.ToInt32(reader["slotid"].ToString()));
				slots.Add(Convert.ToInt32(reader["slotid"].ToString()),s);
			}
			//slotswithbases = ht;
		}

		public void InitMediaPlans()
		{
			//Get media plans and their attributes
			//mediaplans.Clear();
			ArrayList htmp = new ArrayList();	//The media plans for a slot id
			StringDictionary sd = new StringDictionary();
			int currentSlot = 0;
			int currentMP = 0;
			int currentAGID = 0;
			SqlDataReader reader = GetRuntimeAttributeGroups();
			while(reader.Read())
			{
				if(currentSlot!=Convert.ToInt32(reader["slotid"].ToString()))
				{
					if(sd.Count>0)
						htmp.Add(sd);
					if(htmp.Count>0)
					{
						Slot s = (Slot)slots[currentSlot];
						s.MediaPlans = htmp;
						slots.Remove(currentSlot);
						slots.Add(currentSlot, s);
						//mediaplans.Add(currentSlot, htmp);
					}
					currentSlot = Convert.ToInt32(reader["slotid"].ToString());
					htmp = new ArrayList();
					sd = new StringDictionary();
					currentMP = 0;
				}
				if(currentMP==0||currentAGID==0)
				{
					currentMP = Convert.ToInt32(reader["mediaplanid"].ToString());
					currentAGID = Convert.ToInt32(reader["attributegroupid"].ToString());
				}
				if(currentMP!=Convert.ToInt32(reader["mediaplanid"].ToString()) || currentAGID!=Convert.ToInt32(reader["attributegroupid"].ToString()))
				{
					if(sd.Count>0)
						htmp.Add(sd);
					currentMP = Convert.ToInt32(reader["mediaplanid"].ToString());
					currentAGID = Convert.ToInt32(reader["attributegroupid"].ToString());
					sd = new StringDictionary();
				}
				if(reader["description"].ToString().Length>0 && !sd.ContainsKey(reader["attributeid"].ToString() + "***" + reader["attributename"].ToString()))
					sd.Add(currentMP + "***" + currentAGID + "!!!" + reader["attributeid"].ToString() + "***" + reader["attributename"].ToString(), reader["value"].ToString() + "***" + reader["description"].ToString());
				else
					sd.Add(currentMP + "***" + currentAGID + "!!!" + reader["attributeid"].ToString() + "***" + reader["attributename"].ToString(), reader["value"].ToString() + "***" + reader["value"].ToString());
			}
			//Finish up
			htmp.Add(sd);
			{
				Slot s = (Slot)slots[currentSlot];
				s.MediaPlans = htmp;
				slots.Remove(currentSlot);
				slots.Add(currentSlot, s);
				//mediaplans.Add(currentSlot, htmp);
			}
		}

		public void InitMPCodes()
		{
			//Additional codes to be appended to clickthrough url's per media plan
			string currentMP = "";
			string tempcode = "";
			string temptoken = "";
			//mpcodes.Clear();
			SqlDataReader reader = GetMPCodes();
			if(reader.HasRows)
			{
				while(reader.Read())
				{
					if(currentMP!=reader["mediaplanid"].ToString())
					{
						if(currentMP!="")
						{
							//mpcodes.Add(currentMP, tempcode);
							if(mnMediaplans.Contains(currentMP))
								((MediaPlan)mnMediaplans[currentMP]).Code = tempcode;
							tempcode = "";
						}
						currentMP = reader["mediaplanid"].ToString();
					}
					temptoken = reader["mediaplancode"].ToString();
					temptoken = temptoken.Replace("?","");
					temptoken = temptoken.Replace("&","");
					if(tempcode.Length==0)
						tempcode = "?" + temptoken;
					else if(tempcode.Length>0 && tempcode.Substring(0,1)=="?")
						tempcode += "&" + temptoken;
				}
				//mpcodes.Add(currentMP, tempcode);
				if(mnMediaplans.Contains(currentMP))
					((MediaPlan)mnMediaplans[currentMP]).Code = tempcode;
			}
		}

		public void InitMPContent()
		{
			//The content that is to be returned when a media plan is matched
			//Distributed across 100 positions in a hashtable, retrieved through random number (1-100)
			int currentMP = 0;
			int frequencyType = 0;
			//mpcontent.Clear();
			Hashtable tempcontent = Hashtable.Synchronized(new Hashtable());
			Hashtable content = new Hashtable(100);
			SqlDataReader reader = GetRuntimeMPContent();
			while(reader.Read())
			{
				if(Convert.ToInt32(reader["mediaplanid"].ToString())!=currentMP)
				{
					//Distribute the content
					if(currentMP>0)
					{
						DistributeContent(frequencyType, tempcontent, content);
					}

					if(currentMP>0 && content.Count>0)
					{
						//mpcontent.Add(currentMP, content.Clone());
						if(mnMediaplans.Contains(currentMP))
						{
							MediaPlan mp = (MediaPlan)mnMediaplans[currentMP];
							mp.DistributedContent = (Hashtable)content.Clone();
							mnMediaplans.Remove(currentMP);
							mnMediaplans.Add(currentMP, mp);
						}
					}
					currentMP = Convert.ToInt32(reader["mediaplanid"].ToString());
					frequencyType = Convert.ToInt32(reader["frequencytypeid"].ToString());
					content.Clear();
					Matchnet.AdServer.ValueObjects.Content c1 = new Matchnet.AdServer.ValueObjects.Content(Convert.ToInt32(reader["contentid"].ToString()), reader["filename"].ToString(), reader["clickthroughurl"].ToString(), Convert.ToInt32(reader["frequencyvalue"].ToString()), reader["alttext"].ToString());
					tempcontent.Add(reader["contentid"].ToString(),c1);
				}
				else
				{
					//Just create the content object and add it to the temp list
					Matchnet.AdServer.ValueObjects.Content c = new Matchnet.AdServer.ValueObjects.Content(reader["filename"].ToString(), reader["clickthroughurl"].ToString(), Convert.ToInt32(reader["frequencyvalue"].ToString()), reader["alttext"].ToString());
					tempcontent.Add(reader["contentid"].ToString(),c);
				}
			}
			DistributeContent(frequencyType, tempcontent, content);
			if(currentMP>0 && content.Count>0)
			{
				//mpcontent.Add(currentMP, content.Clone());
				if(mnMediaplans.Contains(currentMP))
				{
					MediaPlan mp = (MediaPlan)mnMediaplans[currentMP];
					mp.DistributedContent = (Hashtable)content.Clone();
					mnMediaplans.Remove(currentMP);
					mnMediaplans.Add(currentMP, mp);
				}
			}
		}

		public void InitCaps()
		{
			//Exposure caps for media plans, for running count
			try
			{
				//capcount.Clear();
				SqlDataReader reader = GetCaps();
				while(reader.Read())
				{
					//capcount.Add(reader[0].ToString(),reader[1].ToString());
					int mediaplanid = Convert.ToInt32(reader[0].ToString());
					MediaPlan mp = (MediaPlan)mnMediaplans[mediaplanid];
					mp.ExposureCap = Convert.ToInt32(reader[1].ToString());
					mnMediaplans.Remove(mediaplanid);
					mnMediaplans.Add(mediaplanid, mp);
				}
			}
			catch(Exception e)
			{
				string msg = e.Message;
			}
		}

		private void DistributeContent(int frequencyType, Hashtable tempcontent, Hashtable content)
		{
			//Distribute content for media plan usage
			int numentries = 0;
			switch(frequencyType)
			{
				case 0:
				case 3:
				{
					//Distribute evenly
					int ctr = 0;
					numentries = 100/tempcontent.Count;
					foreach(DictionaryEntry de in tempcontent)
					{
						for(int i=1;i<=numentries;i++)
						{
							ctr += 1;
							content.Add(ctr, de.Value);
						}
					}
					tempcontent.Clear();
					break;
				}
				case 1:
				{
					//Percentage
					int ctr=0;
					foreach(DictionaryEntry de in tempcontent)
					{
						Matchnet.AdServer.ValueObjects.Content c = (Matchnet.AdServer.ValueObjects.Content)de.Value;
						for(int i=1;i<=c.FrequencyValue;i++)
						{
							ctr += 1;
							content.Add(ctr,de.Value);
						}
					}
					tempcontent.Clear();
					break;
				}
				case 2:
				{
					//Weight
					int totalweight = 0;
					int percentage = 0;
					int ctr = 0;
					foreach(DictionaryEntry de in tempcontent)
					{
						Matchnet.AdServer.ValueObjects.Content c = (Matchnet.AdServer.ValueObjects.Content)de.Value;
						totalweight += c.FrequencyValue;
					}
					foreach(DictionaryEntry de in tempcontent)
					{
						Matchnet.AdServer.ValueObjects.Content c = (Matchnet.AdServer.ValueObjects.Content)de.Value;
						percentage = (c.FrequencyValue*100)/totalweight;
						for(int i=1;i<=percentage;i++)
						{
							ctr += 1;
							content.Add(ctr, de.Value);
						}
					}
					tempcontent.Clear();
					break;
				}
			}
		}

		public Matchnet.AdServer.ValueObjects.Content GetResult(int baseslotid, Matchnet.Web.Framework.ContextGlobal g, int privatelabelid, int sparkpageid, NameValueCollection queryString)
		{
			//int privatelabelid = MapPrivateLabel(PageID);
			//Get the slotid
			//int slotid = Convert.ToInt32(slotswithbases[Convert.ToString(baseslotid) + "*" + Convert.ToString(sparkpageid) + "*" + Convert.ToString(MapPrivateLabel(privatelabelid))]);
			Matchnet.AdServer.ValueObjects.Content ret = null;
			int slotid = 0;
			foreach(Slot s in slots)
			{
				if(Convert.ToString(s.ZoneID)+"000"+Convert.ToString(s.PositionOrdinal)==Convert.ToString(baseslotid) && s.PageID==sparkpageid && s.PrivateLabelID==MapPrivateLabel(privatelabelid))
					slotid=s.SlotID;
			}
			if(slotid>0)
			{
				//Find matching media plan, if any
				int mediaplanid = GetMediaPlan(slotid, g, queryString);
				if(mediaplanid==0)
					//Nothing found, get default content
					ret = (Matchnet.AdServer.ValueObjects.Content)slots[slotid];
				else
				{
					MediaPlan mpresult = (MediaPlan)mnMediaplans[mediaplanid];
					ret = GetMPContent(mediaplanid);
					//Add any custom url codes to the clickthrough url
					//if(mpcodes[Convert.ToString(mediaplanid)]!=null)
					if(mpresult.Code.Length>0)
						//ret.ClickthroughURL += Convert.ToString(mpcodes[Convert.ToString(mediaplanid)]);
						ret.ClickthroughURL += mpresult.Code;
				}

				//Update impression count for this mp
				/*if(mediaplanid>0)
				{
					//if(AddImpression(mediaplanid, ret.ContentID, slotid)==0)
					if(!AddImpression(mediaplanid))
					{
						//We have to remove this mp from the list
						RemoveMediaPlan(mediaplanid);
					}
					else
					{*/
						//Create an Impression object
						Impression oImpression = new Impression();
						oImpression.MediaPlanID = mediaplanid;
						oImpression.SlotID = slotid;
						oImpression.ContentID = ret.ContentID;
						AddImpression(oImpression);
						//Send it on the queue to register it in the db
						//SendImpressionToMessageQueue(oImpression);
					/*}
				}*/
			}
			return ret;
		}

		private SqlDataReader GetRuntimeAttributeGroups()
		{
			string errMsg = "";
			string connString = "server=DV-FRED;database=mnadserver;uid=sa;pwd=redsox05";
			SqlConnection conn = new SqlConnection(connString);
			SqlCommand cmd = new SqlCommand("GetRuntimeAttributeGroups", conn);
			cmd.CommandType = CommandType.StoredProcedure;

			SqlDataReader reader = null;
			try
			{
				conn.Open();
				reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
			}
			catch(Exception ex)
			{
				errMsg = ex.Message;
				if (reader != null)
				{
					reader.Close();
				}
			}
			return reader;
		}

		private SqlDataReader GetRuntimeMPContent()
		{
			string errMsg = "";
			string connString = "server=DV-FRED;database=mnadserver;uid=sa;pwd=redsox05";
			SqlConnection conn = new SqlConnection(connString);
			SqlCommand cmd = new SqlCommand("GetRuntimeMPContent", conn);
			cmd.CommandType = CommandType.StoredProcedure;

			SqlDataReader reader = null;
			try
			{
				conn.Open();
				reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);}
			catch(Exception ex)
			{
				errMsg = ex.Message;
				if (reader != null)
				{
					reader.Close();
				}
			}
			return reader;
		}

		private SqlDataReader GetAllSlots()
		{
			string errMsg = "";
			//string connString = "server=DHXDKM51\\MNAFFILIATE;database=mnadserver;uid=sa;pwd=redsox05";
			string connString = "server=DV-FRED;database=mnAdserver;uid=sa;pwd=redsox05";
			SqlConnection conn = new SqlConnection(connString);
			SqlCommand cmd = new SqlCommand("GetAllSlots", conn);
			cmd.CommandType = CommandType.StoredProcedure;

			SqlDataReader reader = null;
			try
			{
				conn.Open();
				reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);}
			catch(Exception ex)
			{
				errMsg = ex.Message;
			}
			return reader;
		}

		private SqlDataReader GetMPCodes()
		{
			string errMsg = "";
			//string connString = "server=DHXDKM51\\MNAFFILIATE;database=mnadserver;uid=sa;pwd=redsox05";
			string connString = "server=DV-FRED;database=mnAdserver;uid=sa;pwd=redsox05";
			SqlConnection conn = new SqlConnection(connString);
			SqlCommand cmd = new SqlCommand("GetRuntimeMPCodes", conn);
			cmd.CommandType = CommandType.StoredProcedure;

			SqlDataReader reader = null;
			try
			{
				conn.Open();
				reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);}
			catch(Exception ex)
			{
				errMsg = ex.Message;
				if (reader != null)
				{
					reader.Close();
				}
			}
			return reader;
		}

		private SqlDataReader GetCaps()
		{
			string errMsg = "";
			//string connString = "server=DHXDKM51\\MNAFFILIATE;database=mnadserver;uid=sa;pwd=redsox05";
			string connString = "server=DV-FRED;database=mnAdserver;uid=sa;pwd=redsox05";
			SqlConnection conn = new SqlConnection(connString);
			SqlCommand cmd = new SqlCommand("GetRuntimeCap", conn);
			cmd.CommandType = CommandType.StoredProcedure;

			SqlDataReader reader = null;
			try
			{
				conn.Open();
				reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);}
			catch(Exception ex)
			{
				errMsg = ex.Message;
			}
			return reader;
		}

		private SqlDataReader GetSlotsWithBases()
		{
			string errMsg = "";
			string connString = "server=DV-FRED;database=mnadserver;uid=sa;pwd=redsox05";
			SqlConnection conn = new SqlConnection(connString);
			SqlCommand cmd = new SqlCommand("GetSlotsWithBases", conn);
			cmd.CommandType = CommandType.StoredProcedure;

			SqlDataReader reader = null;
			try
			{
				conn.Open();
				reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);}
			catch(Exception ex)
			{
				errMsg = ex.Message;
				if (reader != null)
				{
					reader.Close();
				}
			}
			return reader;
		}

		private int MapPrivateLabel(int sparkprivatelabelid)
		{
			if(sparkprivatelabelid==101)
				return 1;
			else if(sparkprivatelabelid==102)
				return 2;
			else if(sparkprivatelabelid==103)
				return 3;
			else
				return 0;
		}

		public Hashtable RefreshSlots()
		{
			InitSlots();
			InitSlotsWithBases();
			InitMediaPlans();
			return slots;
		}

		public Hashtable RefreshMediaPlans()
		{
			InitMPContent();
			InitMPCodes();
			InitCaps();
			return mnMediaplans;
		}

		private int GetMediaPlan(int slotid, ContextGlobal g, NameValueCollection queryString)
		{
			//Return the first media plan for which the current g has all matching attributes
			int tempmpid = 0;
			int currentmp = 0;
			string attribid = "";
			string sdekey = "";
			AttributeComparer ac = new AttributeComparer(g);
			if(slots[slotid]!=null)
			//if(mediaplans[slotid]!=null)
			{
				//ArrayList ht = (ArrayList)mediaplans[slotid];
				Slot s = (Slot)slots[slotid];
				//foreach(StringDictionary sd in ht)
				foreach(StringDictionary sd in s.MediaPlans)
				{
					bool bfound = true;
					foreach(DictionaryEntry sde in sd)
					{
						sdekey = Convert.ToString(sde.Key);
						currentmp = Convert.ToInt32(sdekey.Substring(0,sdekey.IndexOf("*")));
						attribid = sdekey.Substring(sdekey.IndexOf("!!!")+3, sdekey.Length-(sdekey.IndexOf("!!!")+3));
						//if(!ac.Compare(Convert.ToString(sde.Key), Convert.ToString(sde.Value)))
						if(!ac.Compare(attribid, Convert.ToString(sde.Value), queryString))
						{
							bfound = false;
							break;
						}
					}
					if(bfound)
					{
						//if(Convert.ToString(de.Key).IndexOf("*")>-1)
						tempmpid = currentmp;
						//else
						//	tempmpid = Convert.ToInt32(sde.Key);
						break;
					}
				}
			}
			return tempmpid;
		}
		
		/*private bool AddImpression(int MediaPlanID)
		{
			//If this MP had no entry then it had no maximum cap count
			if(!capcount.Contains(MediaPlanID.ToString()))
				return true;
			//Return false if the remaining cap count for this MP is zero
			int capcounter = Convert.ToInt32(capcount[MediaPlanID.ToString()]);
			if(Interlocked.Decrement(ref capcounter)>0)
			{
				capcount[MediaPlanID.ToString()] = capcounter;
				return true;
			}
			else
				return false;
		}*/

		private void AddImpression(Impression oImpression)
		{
			impressions.Add(new object(), oImpression);
		}

		private int SaveImpression(int MediaPlanID, int ContentID, int SlotID)
		{
			//Add the impression to the db
			string errMsg = "";
			string connString = "server=DV-FRED;database=mnadserver;uid=sa;pwd=redsox05";
			SqlConnection conn = new SqlConnection(connString);
			SqlCommand cmd = new SqlCommand("AddImpression", conn);
			cmd.CommandType = CommandType.StoredProcedure;

			//Add params
			cmd.Parameters.Add(new SqlParameter("@mediaplanid",SqlDbType.Int, 4));
			cmd.Parameters.Add(new SqlParameter("@contentid",SqlDbType.Int, 4));
			cmd.Parameters.Add(new SqlParameter("@slotid",SqlDbType.Int, 4));

			//Provide the values
			cmd.Parameters["@mediaplanid"].Value = MediaPlanID;
			cmd.Parameters["@contentid"].Value = ContentID;
			cmd.Parameters["@slotid"].Value = SlotID;

			//Exec
			conn.Open();
			int ret = 0;
			try
			{
				ret = cmd.ExecuteNonQuery();
			}
			catch(Exception ex)
			{errMsg = ex.Message;}
			finally
			{
				conn.Close();
			}
			if(errMsg.Length>0)
				return -1;
			else
				return ret;
		}

		private Matchnet.AdServer.ValueObjects.Content GetMPContent(int mediaplanid)
		{
			MediaPlan mp = (MediaPlan)mnMediaplans[mediaplanid];
			if(mp!=null)
			{
				int idx = RandomNumber(0,99);
				//Hashtable content = (Hashtable)mpcontent[mediaplanid];
				Hashtable content = (Hashtable)mp.DistributedContent;
				return (Matchnet.AdServer.ValueObjects.Content)content[idx];
			}
			else
				return null;
		}

		private int RandomNumber(int min, int max)
		{
			Random random = new Random();
			return random.Next(min, max); 
		}

		private void SendImpressionToMessageQueue(Impression oImpression)
		{
			try
			{
				//pick up the queuename from the web.config file
				string queueName = @"dv-fred\private$\capcounter";
				MessageQueue destMsgQ = new MessageQueue(queueName);
				Message destMsg = new Message();
				/*In case of unsuccessful attempts to post messages,
				 *  send them to dead letter queue for audit traceability*/
				destMsg.UseDeadLetterQueue = true;
				/*Use binary formatter for posting the request message 
				object into the queue*/
				destMsg.Formatter = new System.Messaging.BinaryMessageFormatter();
				//Assign the request message object to the message body
				destMsg.Body = oImpression;
				//Post the message
				destMsgQ.Send(destMsg);
			}
			catch(System.Exception ex)
			{
				string msg = ex.Message;
			}
		}

		private void RemoveMediaPlan(int MediaPlanID)
		{
			foreach(DictionaryEntry de in slots)
			//foreach(DictionaryEntry de in mediaplans)
			{
				Slot s = (Slot)de.Value;
				foreach(DictionaryEntry de1 in (ArrayList)s.MediaPlans)
				//foreach(DictionaryEntry de1 in (StringDictionary)de.Value)
				{
					string keystring = de1.Key.ToString();
					string mpstring = keystring.Substring(0,keystring.IndexOf("***"));
					if(MediaPlanID.ToString()==mpstring)
					{
						//mediaplans.Remove(de);
						s.MediaPlans.Remove(de1);
						slots.Remove(de.Key);
						slots.Add(s.SlotID, s);
						return;
					}
				}
			}
		}

		#region singleton
		/// <summary>
		/// Singleton instance
		/// </summary>
		public static readonly AdServerBL Instance = new AdServerBL();
		#endregion
	}
}
