using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Matchnet.AdServer.ServiceManagers;
using Matchnet.AdServer.ValueObjects;

namespace Matchnet.AdServer
{
	public class AdServerService : Matchnet.RemotingServices.RemotingServiceBase
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private AdServerSM _adserverSM;

		public AdServerService()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();
		}

		// The main entry point for the process
		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new AdServerService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		protected override void RegisterServiceManagers()
		{
			try
			{
				_adserverSM = new AdServerSM();
				base.RegisterServiceManager(_adserverSM);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occurred when starting the Windows Service", ex);
			}
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = ServiceConstants.SERVICE_NAME;
			base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

//		/// <summary>
//		/// Set things in motion so your service can do its work.
//		/// </summary>
//		protected override void OnStart(string[] args)
//		{
//			//_adserverSM.InitSlots();
//			//_adserverSM.InitMediaPlans();
//			//_adserverSM.InitMPContent();
//		}
// 
//		/// <summary>
//		/// Stop this service.
//		/// </summary>
//		protected override void OnStop()
//		{
//			// TODO: Add code here to perform any tear-down necessary to stop your service.
//		}
	}
}
