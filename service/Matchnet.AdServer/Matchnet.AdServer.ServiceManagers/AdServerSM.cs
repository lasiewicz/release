using System;
using System.Collections;
using System.Collections.Specialized;
using Matchnet;
using Matchnet.Web.Framework;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.AdServer.BusinessLogic;
using Matchnet.AdServer.ValueObjects;

namespace Matchnet.AdServer.ServiceManagers
{
	public class AdServerSM : MarshalByRefObject, IServiceManager, IDisposable
	{

		#region constants
		private const string SERVICE_MANAGER_NAME = "AdServerSM";
		#endregion

		#region constructor
		public AdServerSM()
		{
		}
		#endregion

		#region IDisposable Members
		public void Dispose()
		{

		}
		#endregion

		#region IServiceManager Members
		public void PrePopulateCache()
		{
			// no implementation at this time
		}
		#endregion

		public Matchnet.AdServer.ValueObjects.Content GetResult(int baseslotid, Matchnet.Web.Framework.ContextGlobal g, int privatelabelid, int sparkpageid, NameValueCollection queryString)
		{
			Matchnet.AdServer.ValueObjects.Content oResult = new Matchnet.AdServer.ValueObjects.Content();
			try
			{
				oResult = AdServerBL.Instance.GetResult(baseslotid, g, privatelabelid, sparkpageid, queryString);
			}
			catch(ExceptionBase ex)
			{
				throw new ServiceBoundaryException(ValueObjects.ServiceConstants.SERVICE_NAME, "Failure retrieving Media Plan Content.", ex);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ValueObjects.ServiceConstants.SERVICE_NAME, "Failure retrieving Media Plan Content.", ex);
			}

			return oResult;
		}

		public Hashtable RefreshSlots()
		{
			Hashtable oResult = new Hashtable();
			try
			{
				oResult = AdServerBL.Instance.RefreshSlots();
			}
			catch(ExceptionBase ex)
			{
				throw new ServiceBoundaryException(ValueObjects.ServiceConstants.SERVICE_NAME, "Failure retrieving Slot configuration data.", ex);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ValueObjects.ServiceConstants.SERVICE_NAME, "Failure retrieving Slot configuration data.", ex);
			}

			return oResult;
		}

		public Hashtable RefreshMediaPlans()
		{
			Hashtable oResult = new Hashtable();
			try
			{
				oResult = AdServerBL.Instance.RefreshMediaPlans();
			}
			catch(ExceptionBase ex)
			{
				throw new ServiceBoundaryException(ValueObjects.ServiceConstants.SERVICE_NAME, "Failure retrieving Media Plan configuration data.", ex);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ValueObjects.ServiceConstants.SERVICE_NAME, "Failure retrieving Media Plan configuration data.", ex);
			}

			return oResult;
		}

		public void InitSlots()
		{
			AdServerBL.Instance.InitSlots();
		}

		public void InitSlotsWithBases()
		{
			AdServerBL.Instance.InitSlotsWithBases();
		}

		public void InitMediaPlans()
		{
			AdServerBL.Instance.InitMediaPlans();
		}

		public void InitMPContent()
		{
			AdServerBL.Instance.InitMPContent();
		}

		public void InitMPCodes()
		{
			AdServerBL.Instance.InitMPCodes();
		}

		public void InitCaps()
		{
			AdServerBL.Instance.InitCaps();
		}

	}
}
