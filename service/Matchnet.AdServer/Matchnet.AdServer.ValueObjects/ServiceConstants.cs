using System;

namespace Matchnet.AdServer.ValueObjects
{
	/// <summary>
	/// Summary description for ServiceConstants.
	/// </summary>
	[Serializable]
	public class ServiceConstants
	{
		/// <summary>
		/// 
		/// </summary>
		public const string SERVICE_CONSTANT = "ADSERVER_SVC";
		/// <summary>
		/// 
		/// </summary>
		public const string SERVICE_NAME = "Matchnet.AdServer.Service";

		private ServiceConstants()
		{
		}
	}
}
