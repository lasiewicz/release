using System;

namespace Matchnet.AdServer.ValueObjects
{
	/// <summary>
	/// Summary description for Content.
	/// </summary>
	[Serializable]
	public class Content
	{
		#region Private Properties
		private int _ContentID;
		private string _FileName = string.Empty;
		private string _ClickthroughURL = string.Empty;
		private int _FrequencyValue;
		private string _AltText;
		#endregion

		public Content()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		
		public Content(string pFileName, string pClickthroughURL)
		{
			_FileName = pFileName;
			_ClickthroughURL = pClickthroughURL;
		}

		public Content(string pFileName, string pClickthroughURL, string pAltText)
		{
			_FileName = pFileName;
			_ClickthroughURL = pClickthroughURL;
			_AltText = pAltText;
		}

		public Content(string pFileName, string pClickthroughURL, int pFrequencyValue, string pAltText)
		{
			_FileName = pFileName;
			_ClickthroughURL = pClickthroughURL;
			_FrequencyValue = pFrequencyValue;
			_AltText = pAltText;
		}

		public Content(int pContentID, string pFileName, string pClickthroughURL, int pFrequencyValue, string pAltText)
		{
			_ContentID = pContentID;
			_FileName = pFileName;
			_ClickthroughURL = pClickthroughURL;
			_FrequencyValue = pFrequencyValue;
			_AltText = pAltText;
		}

		public Content(string pFileName, string pClickthroughURL, int pFrequencyValue)
		{
			_FileName = pFileName;
			_ClickthroughURL = pClickthroughURL;
			_FrequencyValue = pFrequencyValue;
		}

		#region Properties

		public int FrequencyValue
		{
			get
			{
				return(_FrequencyValue);
			}
			set
			{
				_FrequencyValue = value;
			}
		}

		public int ContentID
		{
			get
			{
				return(_ContentID);
			}
			set
			{
				_ContentID = value;
			}
		}

		public string FileName
		{
			get
			{
				return(_FileName);
			}
			set
			{
				_FileName = value;
			}
		}

		public string AltText
		{
			get
			{
				return(_AltText);
			}
			set
			{
				_AltText = value;
			}
		}

		public string ClickthroughURL
		{
			get
			{
				return(_ClickthroughURL);
			}
			set
			{
				_ClickthroughURL = value;
			}
		}

		#endregion

	}
}
