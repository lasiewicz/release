using System;
using Matchnet.Member.ServiceAdapters;
using Matchnet.AdServer.ValueObjects;

namespace Matchnet.AdServer.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IAdServerService.
	/// </summary>
	public interface IAdServerService
	{
		Content GetResult(int slotid, Matchnet.Member.ServiceAdapters.Member member);

		Content GetTestResult();
	}
}
