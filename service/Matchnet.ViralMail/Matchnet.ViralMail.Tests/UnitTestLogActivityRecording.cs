﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Matchnet.ViralMail.BusinessLogic;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;
using Matchnet.ActivityRecording.ServiceAdapters;
using Matchnet.ActivityRecording.ValueObjects;


namespace Matchnet.ViralMail.Tests
    {
    [TestFixture]    
    public class ViralMailBLTests
    {
        [TestFixtureSetUp]
        public void SetUp()
        {
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
        }

        [Test]
        public void TestLogGetActivityCaption()
        {
            
            MiniprofileInfoCollection miniprofileInfoCollection = new MiniprofileInfoCollection();
            MiniprofileInfoCollection yesMiniprofileInfoCollection = new MiniprofileInfoCollection();

            MiniprofileInfo miniprofileInfo = new MiniprofileInfo();
            miniprofileInfo.MemberID = 111646266;
            miniprofileInfoCollection.Add(miniprofileInfo);
            miniprofileInfo = new MiniprofileInfo();
            miniprofileInfo.MemberID = 100171610;
            miniprofileInfoCollection.Add(miniprofileInfo);
            miniprofileInfo = new MiniprofileInfo();
            miniprofileInfo.MemberID = 111646047;
            miniprofileInfoCollection.Add(miniprofileInfo);

            MiniprofileInfo YesMiniprofileInfo = new MiniprofileInfo();
            YesMiniprofileInfo.MemberID = 100171922;
            yesMiniprofileInfoCollection.Add(YesMiniprofileInfo);

            string returnActivityCaption = "";
            returnActivityCaption = BusinessLogic.ViralMailBL.GetActivityCaption(miniprofileInfoCollection, yesMiniprofileInfoCollection);

            Assert.AreEqual("<Mail><Secret Admirer><MemberID>111646266</MemberID><MemberID>100171610</MemberID><MemberID>111646047</MemberID></Secret Admirer><Admirer><MemberID>100171922</MemberID></Admirer></Mail>", returnActivityCaption);

            }

        [Test]
        public void TestRecordActivity()
        {
            MiniprofileInfoCollection miniprofileInfoCollection = new MiniprofileInfoCollection();
            MiniprofileInfoCollection yesMiniprofileInfoCollection = new MiniprofileInfoCollection();

            MiniprofileInfo miniprofileInfo = new MiniprofileInfo();
            miniprofileInfo.MemberID = 111646266;
            miniprofileInfoCollection.Add(miniprofileInfo);
            miniprofileInfo = new MiniprofileInfo();
            miniprofileInfo.MemberID = 100171610;
            miniprofileInfoCollection.Add(miniprofileInfo);
            miniprofileInfo = new MiniprofileInfo();
            miniprofileInfo.MemberID = 111646047;
            miniprofileInfoCollection.Add(miniprofileInfo);

            MiniprofileInfo YesMiniprofileInfo = new MiniprofileInfo();
            YesMiniprofileInfo.MemberID = 100171922;
            yesMiniprofileInfoCollection.Add(YesMiniprofileInfo);

            string returnActivityCaption = "";
            returnActivityCaption = BusinessLogic.ViralMailBL.GetActivityCaption(miniprofileInfoCollection, yesMiniprofileInfoCollection);

             // Log this to ActivityRecording svc
            ActivityRecordingSA.Instance.RecordActivity(1111111, Constants.NULL_INT, 111, ActivityRecording.ValueObjects.Types.ActionType.SendSecretAdmirerMail,
            ActivityRecording.ValueObjects.Types.CallingSystem.ViralMail, returnActivityCaption);
        }

    }
}

