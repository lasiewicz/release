using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;

namespace Matchnet.ViralMail.Service
{
	/// <summary>
	/// Summary description for Installer.
	/// </summary>
	[RunInstaller(true)]
	public class Installer : System.Configuration.Install.Installer
	{
		private System.ServiceProcess.ServiceInstaller serviceInstaller;
		private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Installer()
		{
			// This call is required by the Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.serviceInstaller = new System.ServiceProcess.ServiceInstaller();
			this.serviceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
			// 
			// serviceProcessInstaller
			// 
			this.serviceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
			this.serviceProcessInstaller.Password = null;
			this.serviceProcessInstaller.Username = null;
			this.serviceInstaller.ServiceName = Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME;
			// 
			// Installer
			// 
			this.Installers.AddRange(new System.Configuration.Install.Installer[] {
																					  this.serviceInstaller,
																					  this.serviceProcessInstaller});
		
			this.AfterInstall += new InstallEventHandler(Installer_AfterInstall);
			this.AfterUninstall += new InstallEventHandler(Installer_AfterUninstall);
		}
		#endregion

		private void Installer_AfterInstall(object sender, InstallEventArgs e)
		{
			Matchnet.ViralMail.BusinessLogic.Metrics.Install();
		}

		private void Installer_AfterUninstall(object sender, InstallEventArgs e)
		{
			Matchnet.ViralMail.BusinessLogic.Metrics.Uninstall();
		}
	}
}
