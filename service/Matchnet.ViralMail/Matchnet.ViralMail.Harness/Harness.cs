using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Messaging;
using System.Text;
using System.Threading;

using Matchnet.ViralMail.BusinessLogic;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.ViralMail.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.Search.Interfaces;
using Matchnet.Data;
using Matchnet.Content.ServiceAdapters;

namespace Matchnet.ViralMail.Harness
{
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox MemberID;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox SentMemberIDs;
		private System.Windows.Forms.TextBox CommunityID;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.TextBox Output;
		private System.Windows.Forms.CheckBox SendEmptySearchPrefs;
		private System.Windows.Forms.Button btnGenderMaskFlip;
		private System.Windows.Forms.Button btnMemberAttributeGet;
		private System.Windows.Forms.Button btnIsraelAreacodePref;
		private System.Windows.Forms.Button btnScrubTest;
		private System.Windows.Forms.Button btnPerfCountersInstall;
		private System.Windows.Forms.Button button7;
		private System.ComponentModel.Container components = null;

		Thread _threadGetNext  = null;

		public Form1()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.MemberID = new System.Windows.Forms.TextBox();
			this.CommunityID = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.SentMemberIDs = new System.Windows.Forms.TextBox();
			this.button5 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.Output = new System.Windows.Forms.TextBox();
			this.SendEmptySearchPrefs = new System.Windows.Forms.CheckBox();
			this.btnGenderMaskFlip = new System.Windows.Forms.Button();
			this.btnMemberAttributeGet = new System.Windows.Forms.Button();
			this.btnIsraelAreacodePref = new System.Windows.Forms.Button();
			this.btnScrubTest = new System.Windows.Forms.Button();
			this.btnPerfCountersInstall = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(32, 200);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(192, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "Run service";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(32, 160);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(192, 23);
			this.button2.TabIndex = 1;
			this.button2.Text = "Send ViralMail Queue";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// MemberID
			// 
			this.MemberID.Location = new System.Drawing.Point(152, 40);
			this.MemberID.Name = "MemberID";
			this.MemberID.TabIndex = 2;
			this.MemberID.Text = "988264912";
			// 
			// CommunityID
			// 
			this.CommunityID.Location = new System.Drawing.Point(152, 72);
			this.CommunityID.Name = "CommunityID";
			this.CommunityID.Size = new System.Drawing.Size(40, 20);
			this.CommunityID.TabIndex = 3;
			this.CommunityID.Text = "12";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(40, 40);
			this.label1.Name = "label1";
			this.label1.TabIndex = 4;
			this.label1.Text = "Member ID:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(40, 72);
			this.label2.Name = "label2";
			this.label2.TabIndex = 5;
			this.label2.Text = "Community ID:";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(40, 104);
			this.label4.Name = "label4";
			this.label4.TabIndex = 9;
			this.label4.Text = "Sent MemberIDs:";
			// 
			// SentMemberIDs
			// 
			this.SentMemberIDs.Location = new System.Drawing.Point(152, 104);
			this.SentMemberIDs.Name = "SentMemberIDs";
			this.SentMemberIDs.Size = new System.Drawing.Size(104, 20);
			this.SentMemberIDs.TabIndex = 8;
			this.SentMemberIDs.Text = "988264912";
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(32, 240);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(192, 23);
			this.button5.TabIndex = 12;
			this.button5.Text = "Send ViralMail to Ext Mail";
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// button6
			// 
			this.button6.Location = new System.Drawing.Point(248, 240);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(144, 23);
			this.button6.TabIndex = 13;
			this.button6.Text = "Test Search";
			this.button6.Click += new System.EventHandler(this.button6_Click);
			// 
			// Output
			// 
			this.Output.Location = new System.Drawing.Point(0, 304);
			this.Output.Multiline = true;
			this.Output.Name = "Output";
			this.Output.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.Output.Size = new System.Drawing.Size(536, 152);
			this.Output.TabIndex = 14;
			this.Output.Text = "";
			// 
			// SendEmptySearchPrefs
			// 
			this.SendEmptySearchPrefs.Location = new System.Drawing.Point(240, 152);
			this.SendEmptySearchPrefs.Name = "SendEmptySearchPrefs";
			this.SendEmptySearchPrefs.Size = new System.Drawing.Size(152, 32);
			this.SendEmptySearchPrefs.TabIndex = 15;
			this.SendEmptySearchPrefs.Text = "Send Empty Search Prefs";
			// 
			// btnGenderMaskFlip
			// 
			this.btnGenderMaskFlip.Location = new System.Drawing.Point(248, 272);
			this.btnGenderMaskFlip.Name = "btnGenderMaskFlip";
			this.btnGenderMaskFlip.Size = new System.Drawing.Size(144, 23);
			this.btnGenderMaskFlip.TabIndex = 16;
			this.btnGenderMaskFlip.Text = "Gender Flip";
			this.btnGenderMaskFlip.Click += new System.EventHandler(this.btnGenderMaskFlip_Click);
			// 
			// btnMemberAttributeGet
			// 
			this.btnMemberAttributeGet.Location = new System.Drawing.Point(248, 208);
			this.btnMemberAttributeGet.Name = "btnMemberAttributeGet";
			this.btnMemberAttributeGet.Size = new System.Drawing.Size(144, 23);
			this.btnMemberAttributeGet.TabIndex = 17;
			this.btnMemberAttributeGet.Text = "Member Attr";
			this.btnMemberAttributeGet.Click += new System.EventHandler(this.btnMemberAttributeGet_Click);
			// 
			// btnIsraelAreacodePref
			// 
			this.btnIsraelAreacodePref.Location = new System.Drawing.Point(400, 208);
			this.btnIsraelAreacodePref.Name = "btnIsraelAreacodePref";
			this.btnIsraelAreacodePref.Size = new System.Drawing.Size(144, 24);
			this.btnIsraelAreacodePref.TabIndex = 18;
			this.btnIsraelAreacodePref.Text = "Fix Israel Areacode";
			this.btnIsraelAreacodePref.Click += new System.EventHandler(this.btnIsraelAreacodePref_Click);
			// 
			// btnScrubTest
			// 
			this.btnScrubTest.Location = new System.Drawing.Point(400, 240);
			this.btnScrubTest.Name = "btnScrubTest";
			this.btnScrubTest.Size = new System.Drawing.Size(144, 23);
			this.btnScrubTest.TabIndex = 19;
			this.btnScrubTest.Text = "Scrub Test";
			this.btnScrubTest.Click += new System.EventHandler(this.btnScrubTest_Click);
			// 
			// btnPerfCountersInstall
			// 
			this.btnPerfCountersInstall.Location = new System.Drawing.Point(400, 24);
			this.btnPerfCountersInstall.Name = "btnPerfCountersInstall";
			this.btnPerfCountersInstall.Size = new System.Drawing.Size(128, 24);
			this.btnPerfCountersInstall.TabIndex = 20;
			this.btnPerfCountersInstall.Text = "(re) install perfcounters";
			this.btnPerfCountersInstall.Click += new System.EventHandler(this.btnPerfCountersInstall_Click);
			// 
			// button7
			// 
			this.button7.Location = new System.Drawing.Point(400, 64);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(120, 23);
			this.button7.TabIndex = 21;
			this.button7.Text = "Test MM queu item";
			this.button7.Click += new System.EventHandler(this.button7_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(544, 461);
			this.Controls.Add(this.button7);
			this.Controls.Add(this.btnPerfCountersInstall);
			this.Controls.Add(this.btnScrubTest);
			this.Controls.Add(this.btnIsraelAreacodePref);
			this.Controls.Add(this.btnMemberAttributeGet);
			this.Controls.Add(this.btnGenderMaskFlip);
			this.Controls.Add(this.SendEmptySearchPrefs);
			this.Controls.Add(this.Output);
			this.Controls.Add(this.SentMemberIDs);
			this.Controls.Add(this.CommunityID);
			this.Controls.Add(this.MemberID);
			this.Controls.Add(this.button6);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}


		ViralMailBL _bl;
		bool _running;

		private void button1_Click(object sender, System.EventArgs e)
		{

			if (_bl == null)
			{
				_bl = new ViralMailBL();
			}

			if (!_running)
			{
				_running = true;
				_bl.Start();
				button1.Text = "Stop (running)";
			}
			else
			{
				_running = false;
				_bl.Stop();
				button1.Text = "Start (stopped)";
			}
		}

		private MiniprofileInfo getMiniprofileFromMember(Brand brand, Matchnet.Member.ServiceAdapters.Member member)
		{
			Int32 memberID = member.MemberID;
			String userName = member.GetUserName(brand);
			DateTime birthDate = member.GetAttributeDate(brand, "Birthdate", DateTime.MinValue);
			Int32 regionID = member.GetAttributeInt(brand, "RegionID", Constants.NULL_INT);
			String thumbNailWebPath = String.Empty;
			if (member.GetPhotos(brand.Site.Community.CommunityID).Count > 0)
			{
				Photo photo = member.GetPhotos(brand.Site.Community.CommunityID)[0];

				if (photo.IsApproved)
				{
					thumbNailWebPath = photo.ThumbFileWebPath;
				}
			}
			String aboutMe = member.GetAttributeTextApproved(brand, "AboutMe", String.Empty);

			return new MiniprofileInfo(memberID, userName, birthDate, regionID,
				thumbNailWebPath, String.Empty);
		}

	
		// Send viral mail
		private void button2_Click(object sender, System.EventArgs e)
		{
			button2.Enabled = false;
			try
			{
				int memberID = Convert.ToInt32(MemberID.Text);
				int communityID = Convert.ToInt32(CommunityID.Text);

				MessageQueue _queueViralMail = 
					new MessageQueue(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("VIRALMAILSVC_QUEUE_PATH"));
				_queueViralMail.Formatter = new BinaryMessageFormatter();

				string[] sent = SentMemberIDs.Text.Split(new char[]{','});

				SearchPreferenceCollection searchPrefs;
				if (SendEmptySearchPrefs.Checked)
				{
					searchPrefs = new SearchPreferenceCollection();
				}
				else
				{
					searchPrefs = SearchPreferencesSA.Instance.GetSearchPreferences(memberID, communityID);
				}
				
				ViralMailQueueItem queueItem = new ViralMailQueueItem(memberID, communityID,sent, DateTime.Now,
					DateTime.Now, searchPrefs);

				//try not to lose any here if msmq is jacked
				MessageQueueTransaction tran = new MessageQueueTransaction();

				tran.Begin();
				_queueViralMail.Send(queueItem, tran);
				tran.Commit();
				
				System.Diagnostics.Trace.WriteLine(_queueViralMail.Path);
				
			}
			catch (Exception queueItemEx)
			{
				MessageBox.Show(queueItemEx.ToString());

			}
			button2.Enabled = true;
		}

		
		private Boolean isInScrubList(String[] scrubList, Int32 memberID)
		{
			Boolean found = false;	

			for (Int16 index = 0; index < scrubList.Length; index++)
			{
				if ((String)scrubList[index] == memberID.ToString())
				{
					found = true;
					break;
				}	
			}

			return found;
		}



		// sends viralmail to external queue.
		private void button5_Click(object sender, System.EventArgs e)
		{
			try
			{
				MessageQueue _queueExternalMail = new MessageQueue(RuntimeSettings.GetSetting("EXTMAILSVC_QUEUE_PATH"));
				_queueExternalMail.Formatter = new BinaryMessageFormatter();

				int memberID = Convert.ToInt32(MemberID.Text);
				int communityID = Convert.ToInt32(CommunityID.Text);

				Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
				int brandID;
				member.GetLastLogonDate(communityID, out brandID);

				Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandID);

				MiniprofileInfoCollection profiles = new MiniprofileInfoCollection();
				profiles.Add(getMiniprofileFromMember(brand, member));
				profiles.Add(getMiniprofileFromMember(brand, member));
				profiles.Add(getMiniprofileFromMember(brand, member));
				profiles.Add(getMiniprofileFromMember(brand, member));
				profiles.Add(getMiniprofileFromMember(brand, member));
				profiles.Add(getMiniprofileFromMember(brand, member));
				ViralImpulse impulse = new ViralImpulse(brand.BrandID, member.EmailAddress,
					getMiniprofileFromMember(brand, member), profiles);

				MessageQueueTransaction tran = new MessageQueueTransaction();
				tran.Begin();
				_queueExternalMail.Send(impulse, tran);	
				tran.Commit();
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}

		}

		private void button6_Click(object sender, System.EventArgs e)
		{
			try
			{
				Output.Text = "";

				Command command = new Command("mnAlert", "up_SearchPreference_ListMember", 0);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, Convert.ToInt32(MemberID.Text));
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, Convert.ToInt32(CommunityID.Text));
			
				DataTable dt = Client.Instance.ExecuteDataTable(command);


				if (dt.Rows.Count == 0)
				{
					MessageBox.Show("No search prefs found for member");
				}

				DataRow searchPrefRow = dt.Rows[0];
				SearchPreferenceCollection searchPreferences = new SearchPreferenceCollection();

				foreach (DataColumn column in dt.Columns) 
				{
					if (column.ColumnName.ToLower() != "memberdomainId") 
					{
						if (searchPrefRow[column] != DBNull.Value) 
						{
							try
							{
								searchPreferences.Add(new SearchPreference(column.ColumnName.ToLower(), Convert.ToString(searchPrefRow[column])));
							}
							catch
							{
								searchPreferences.Add(new SearchPreference(column.ColumnName.ToLower(), Constants.NULL_STRING));
							}
						}
					}
				}

				Matchnet.Member.ServiceAdapters.Member member = 
					MemberSA.Instance.GetMember(Convert.ToInt32(MemberID.Text), MemberLoadFlags.None);
				Int32 brandID = Constants.NULL_INT;
				member.GetLastLogonDate(Convert.ToInt32(CommunityID.Text), out brandID);
				Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);

				MatchnetQueryResults searchResults = MemberSearchSA.Instance.Search(searchPreferences,
					brand.Site.Community.CommunityID,
					brand.Site.SiteID,
					0,
					106,
					SearchEngineType.FAST,
					SearchType.MatchMail);

				Output.AppendText("Total returned: " + searchResults.Items.Count);

				if (searchResults.Items.Count > 0)
				{
					for (Int32 resultIdx = 0; resultIdx < searchResults.Items.Count; resultIdx++)
					{
						MatchnetResultItem resultItem = (MatchnetResultItem) searchResults.Items[resultIdx];

						Output.AppendText("MemberID:" + resultItem.MemberID + System.Environment.NewLine);
					}
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}

		}

		private void btnGenderMaskFlip_Click(object sender, System.EventArgs e)
		{
			int [] Samplegenders = new int [] {5,6,9,10} ;
			foreach (int i in Samplegenders)
			{
				Output.Text += i.ToString() + " => " + GenderUtils.FlipMaskIfHeterosexual(i).ToString() + System.Environment.NewLine;
			}

			//			for (int i = 16; i < 120; i++){
			//				DateTime bd = DateTime.Today.AddYears(- i);
			//				int [] range = AgeUtils.GetAgeRange(bd,Constants.NULL_INT,Constants.NULL_INT);
			//				int age = AgeUtils.GetAge(bd);
			//				Output.Text += bd.ToShortDateString() + ": " + age + " ==>> " + range[0].ToString() + "-" + range[1].ToString() + Environment.NewLine;
			//			}

			for (int i = 0; i < 255; i++)
			{
				//Output.Text += string.Format("{0}{1}\t{2} ==> {3}",Environment.NewLine, i , GenderUtils.IsReversable(i), GenderUtils.FlipMaskIfHeterosexual(i));
			}
		}

		private void btnMemberAttributeGet_Click(object sender, System.EventArgs e)
		{
			int iMemberID = Int32.Parse(MemberID.Text );
			int iCommunityID = Int32.Parse(CommunityID.Text);

			Member.ServiceAdapters.Member m = Member.ServiceAdapters.MemberSA.Instance.GetMember(iMemberID, MemberLoadFlags.None);
			int genderMask =	m.GetAttributeInt(iCommunityID, 0, 0, "GenderMask", -101010);
			int regionID =		m.GetAttributeInt(iCommunityID, 0, 0, "RegionID", -101010);
			DateTime birthDate= m.GetAttributeDate(iCommunityID, 0, 0, "Birthdate", new DateTime(1919,1,9,11,11,11,11));
			int flippedGender = GenderUtils.FlipMaskIfHeterosexual(genderMask);

			Output.Text = String.Format("memberid:{0} communityid:{1} gendermask:{2} (flipped: {3} regionid:{4} birthdate:{5}",iMemberID, iCommunityID, genderMask, flippedGender, regionID,birthDate);
		}

		internal void btnIsraelAreacodePref_Click(object sender, System.EventArgs e)
		{
			ViralMailBL bl = new ViralMailBL();
			SearchPreferenceCollection sp = new SearchPreferenceCollection();
			sp["SearchTypeID"] = "2";
			sp["CountryRegionID"] = "223";
			sp["AreaCode1"] = "2";
			sp["AreaCode3"] = "4";
			sp["AreaCode5"] = "6";
			sp["AreaCode6"] = "9";

			ViralMailBL.FixIsraelAreaCodes(ref sp);
			Output.Text = ViralMailBL.GetPrefDump(sp);
			Output.Text += Environment.NewLine;

			
			sp["CountryRegionID"] = "105";
			ViralMailBL.FixIsraelAreaCodes(ref sp);

			Output.Text += ViralMailBL.GetPrefDump(sp);


		}

		private void btnScrubTest_Click(object sender, System.EventArgs e)
		{
			string [] current = new string [] {"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100","101","102","103","104","105","106","107","108","109","110","111","112","113","114","115","116","117","118","119","120","121","122","123","124","125","126","127","128","129","130","131","132","133","134","135","136","137","138","139","140","141","142","143","144"};
			StringBuilder sb = new StringBuilder(1500);
			MiniprofileInfoCollection mp = new MiniprofileInfoCollection();
			for (int i = 10001; i < 10007; i++)
			{
				mp.Add(new MiniprofileInfo(i,i.ToString(),DateTime.MinValue,223,"foo.jpg",""));
			}
			ViralMailBL.PopulateScrublist(current,mp, ref sb, 5);

			Output.Text = sb.ToString();

			current = new string [] {"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98"};
			mp = new MiniprofileInfoCollection();
			for (int i = 10001; i < 10007; i++)
			{
				mp.Add(new MiniprofileInfo(i,i.ToString(),DateTime.MinValue,223,"foo.jpg",""));
			}
			ViralMailBL.PopulateScrublist(current,mp, ref sb, 5);
			Output.Text += Environment.NewLine;
			Output.Text += sb.ToString();

		}

		private void btnPerfCountersInstall_Click(object sender, System.EventArgs e)
		{
			ViralMailBL.PerfCounterUninstall();
			Output.Text = "Uninstalled perf counters";
			ViralMailBL.PerfCounterInstall();
			Output.Text += Environment.NewLine + "Installed perf counters";

		}

		private void button7_Click(object sender, System.EventArgs e)
		{
			if (_threadGetNext == null)
			{
				_threadGetNext = new Thread(new ThreadStart(ScheduleLoaderBL.Instance.Run));
				_threadGetNext.Name = "Next Virales Cycle Thread";
				_threadGetNext.Start();
				Output.Text = "Started loading queue items thread";
			}
			else 
			{
				Output.Text = "Stopping loading queue items thread.. ";
				ScheduleLoaderBL.Instance.Stop();
				_threadGetNext.Join();
				_threadGetNext = null;
				Output.Text += "Stopped the loading queue items thread.";
			}

			
		}


	}
}
