using System;

namespace Matchnet.ViralMail.ValueObjects
{
	public class ServiceConstants
	{
		public const string SERVICE_NAME = "Matchnet.ViralMail.Service";
		public const string SERVICE_CONSTANT = "VIRALMAIL_SVC";

		private ServiceConstants()
		{
		}
	}
}
