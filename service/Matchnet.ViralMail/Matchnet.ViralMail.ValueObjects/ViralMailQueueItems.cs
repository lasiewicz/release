using System;
using System.Collections;

namespace Matchnet.ViralMail.ValueObjects
{
	/// <summary>
	/// Summary description for ViralMailQueueItems.
	/// </summary>
	public class ViralMailQueueItems : CollectionBase, ICollection
	{
		public void Add(ViralMailQueueItem item) 
		{
			List.Add(item);
		}

		public void Remove(Int32 index)
		{
			if (index < List.Count && index >= 0)
			{
				List.RemoveAt(index); 
			}
		}

		public ViralMailQueueItem Item(Int32 index)
		{
			return List[index] as ViralMailQueueItem ;
		}
	}
}
