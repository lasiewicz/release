using System;
using System.Collections;

using Matchnet.Search.ValueObjects;

namespace Matchnet.ViralMail.ValueObjects
{
	[Serializable]
	public class ViralMailQueueItem : IValueObject
	{
		#region Variables
		private Int32 memberID;
		private Int32 communityID;
		private String[] sentMemberIDs;
		private SearchPreferenceCollection searchPreferences;
		private DateTime lastAttemptDate;
		private DateTime nextAttemptDate;
		#endregion

		#region Constructors
		public ViralMailQueueItem(Int32 memberIDVal,
			Int32 communityIDVal,
			String[] sentMemberIDsVal,
			DateTime lastAttemptDateVal,
			DateTime nextAttemptDateVal,
			SearchPreferenceCollection searchPreferencesVal)
		{
			memberID = memberIDVal;
			communityID = communityIDVal;
			sentMemberIDs = sentMemberIDsVal;
			lastAttemptDate = lastAttemptDateVal;
			nextAttemptDate = nextAttemptDateVal;
			searchPreferences = searchPreferencesVal;
		}
		#endregion

		#region Properties
		public SearchPreferenceCollection SearchPreferences
		{
			get
			{
				return searchPreferences;
			}
		}

		public Int32 MemberID 
		{
			get
			{
				return memberID;
			}
		}

		public Int32 CommunityID 
		{
			get
			{
				return communityID;
			}
		}

		public DateTime NextAttemptDate 
		{
			get
			{
				return nextAttemptDate;
			}
		}
		public DateTime LastAttemptDate 
		{
			get
			{
				return lastAttemptDate;
			}
		}

		public String[] SentMemberIDs
		{
			get
			{
				return sentMemberIDs;
			}
		}
		#endregion
	}
}
