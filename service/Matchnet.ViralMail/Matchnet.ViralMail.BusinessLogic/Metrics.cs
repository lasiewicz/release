using System;
using System.Diagnostics;

using Matchnet.ViralMail.ValueObjects;

namespace Matchnet.ViralMail.BusinessLogic
{
	public class Metrics
	{
		public static readonly Metrics Instance = new Metrics();

		public static PerformanceCounter AlertsPerSecond;
		public static PerformanceCounter ViralsPerSecond;

		private Metrics()
		{
			if (PerformanceCounterCategory.Exists(ServiceConstants.SERVICE_NAME))
			{
				AlertsPerSecond = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Alerts consumed/second", 
					false);
				ViralsPerSecond = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Virals consumed/second", 
					false);
			}
			else
			{
				Metrics.Install();
			}
		}

		public static void Install()
		{	
			if (!PerformanceCounterCategory.Exists(ServiceConstants.SERVICE_NAME))
			{
				CounterCreationDataCollection counters = new CounterCreationDataCollection();

				counters.Add(new CounterCreationData("Alerts consumed/second", "", 
					PerformanceCounterType.RateOfCountsPerSecond32));
				counters.Add(new CounterCreationData("Virals consumed/second", "", 
					PerformanceCounterType.RateOfCountsPerSecond32));

				PerformanceCounterCategory.Create(ServiceConstants.SERVICE_NAME,
					"Counters for Matchnet.ViralMail.Service", 
					counters);
			}
		}

		public static void Uninstall()
		{	
			if (PerformanceCounterCategory.Exists(ServiceConstants.SERVICE_NAME))
				PerformanceCounterCategory.Delete(ServiceConstants.SERVICE_NAME);
		}

		public void ResetCounters()
		{
			AlertsPerSecond.RawValue = 0;
			ViralsPerSecond.RawValue = 0;
		}

	}
}
