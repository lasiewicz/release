using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Messaging;
using System.Threading;
using System.Text;

using Matchnet;
using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.ViralMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.Impulse;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Spark.CloudStorage;
using Matchnet.ActivityRecording.ServiceAdapters;
using Matchnet.ActivityRecording.ValueObjects;
using Spark.Logging;
using Client = Matchnet.Data.Client;

namespace Matchnet.ViralMail.BusinessLogic
{
	/// <summary>
	/// Gets queue objects sent by getNextVirals.
	/// Pulls out up to 2 Yes votes for recipient memeber.
	/// Saves viral mail sent status for member <-> yes voter.
	/// Populate 3-4 profiles to search results.
	/// Randomizes the order.
	/// Sends to external mail queue.
	/// </summary>
	public class ViralMailBL
	{
		#region Constants
		private const Int32 MAX_SCRUBLIST_ITEMS = 100;
		private const String HYDRA_SAVE_NAME = "mnAlertSaveMM";
		private const Int32 MAIL_STATUS_VIRAL_MAIL_SENT = 2;

        //control
		private const Int32 MAX_YES_PER_EMAIL = 2;
	    private const int MAX_PROFILES_PER_EMAIL = 5;

        //test3 (3 profiles)
	    private const int MAX_YES_PER_EMAIL_TEST3 = 1;
	    private const int MAX_PROFILES_PER_EMAIL_TEST3 = 3;

		private static readonly String[] areaCodeNames = {"areacode1","areacode2","areacode3","areacode4","areacode5","areacode6"};
		private const Int32 PAGE_SIZE = 106;
		#endregion

		#region Variables
		private Boolean isRunning = false;
		private Thread threadGetNext;
		private Thread[] threadQueue;
		private String cfgViralMailQueuePath;
		private Int32 cfgThreadCount;
		private Int32 cfgPollingInterval;
		private Int64 cfgQPSLimit;
		private String cfgPollingProcessorHost;
		private Boolean cfgTracingFlag = false;
		private MessageQueue queueViralMail;
		private HydraWriter hydraWriter;
		private float fQPSLimit;
		private float fThreadCount;

		// Perf counters.
		private PerformanceCounter perfMatchRate;
		private PerformanceCounter perfHaveStoredSearchPrefTotal;
		private PerformanceCounter perfNoSearchPrefTotal;
		private PerformanceCounter perfItemsProcessedTotal;
		private PerformanceCounter perfQuery1Total;
		private PerformanceCounter perfQuery2Total;
		private PerformanceCounter perfQuery3Total;
		private PerformanceCounter perfQuery1NoResults;
		private PerformanceCounter perfQuery2NoResults;
		private PerformanceCounter perfQuery3NoResults;
		private PerformanceCounter perfTotalSent;
		private PerformanceCounter perfTotalSentRate;
		private PerformanceCounter perfTotalRegulated;
		private PerformanceCounter perfTotalRegulatedRate;
		private PerformanceCounter perfAvgSearchTime;
		private PerformanceCounter perfAvgSearchTimeBase;
		private PerformanceCounter perfAvgScrubTime;
		private PerformanceCounter perfAvgScrubTimeBase;
		private PerformanceCounter perfAvgGetQueueItemTime;
		private PerformanceCounter perfAvgGetQueueItemTimeBase;
		private PerformanceCounter perfAvgPopulateResultsTime;
		private PerformanceCounter perfAvgPopulateResultsTimeBase;
		private PerformanceCounter perfAvgSendToXmailTime;
		private PerformanceCounter perfAvgSendToXmailTimeBase;
		private PerformanceCounter perfAvgSavemnAlertTime;
		private PerformanceCounter perfAvgSavemnAlertTimeBase;
		private PerformanceCounter perfAvgGetMemberTime;
		private PerformanceCounter perfAvgGetMemberTimeBase;
		private PerformanceCounter perfAvgBuildDefaultPrefTime;
		private PerformanceCounter perfAvgBuildDefaultPrefTimeBase;
		private PerformanceCounter perfAvgAdjustScrublistTime;
		private PerformanceCounter perfAvgAdjustScrublistTimeBase;
		private PerformanceCounter perfAvgSlowdownTime;
		private PerformanceCounter perfAvgSlowdownTimeBase;
		private PerformanceCounter perfAvgCycleTime;
		private PerformanceCounter perfAvgCycleTimeBase;
		private PerformanceCounter perfTransStarted;
		private PerformanceCounter perfTransCommitted;
		private PerformanceCounter perfTransAborted;
		#endregion
		
		#region Methods
		public void Start()
		{
			cfgViralMailQueuePath = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("VIRALMAILSVC_QUEUE_PATH");
			cfgThreadCount = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("VIRALMAILSVC_THREAD_COUNT"));
			cfgQPSLimit = Convert.ToInt64(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("VIRALMAILSVC_QPS_THRESHOLD"));
			cfgPollingInterval = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("VIRALMAILSVC_POLL_INTERVAL"));
			cfgPollingProcessorHost = RuntimeSettings.GetSetting("VIRALMAILSVC_POLL_PROCESSOR").ToLower();
			cfgTracingFlag = Boolean.Parse(RuntimeSettings.GetSetting("VIRALMAILSVC_TRACING_FLAG"));

			// Cache floats rather than convert on the fly for slowdown calculations.
			fQPSLimit = Convert.ToSingle(cfgQPSLimit);
			fThreadCount = Convert.ToSingle(cfgThreadCount);

			cfgViralMailQueuePath = @".\private$" + cfgViralMailQueuePath.Substring(cfgViralMailQueuePath.LastIndexOf('\\')).ToLower();

			Inform(string.Format("Config:\nViralMail Q:{0}\nVM Threads:{1}\nQPS Limit:{2}\nPolling Interval:{3} (deprecated!)\nPolling Host:{4}\nTracing:{5}\nQ PATH raw: {6}",
				cfgViralMailQueuePath,
				cfgThreadCount,
				cfgQPSLimit,
				cfgPollingInterval,
				cfgPollingProcessorHost,
				cfgTracingFlag,
				Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("VIRALMAILSVC_QUEUE_PATH")
				));

			InitPerfCounters();

			queueViralMail = new MessageQueue(cfgViralMailQueuePath);
			queueViralMail.Formatter = new BinaryMessageFormatter();

			isRunning = true;

			hydraWriter = new HydraWriter(HYDRA_SAVE_NAME, "mnList");
			hydraWriter.Start();

			threadQueue = new Thread[cfgThreadCount];
			for (Int32 threadNum = 0; threadNum < cfgThreadCount; threadNum++)
			{
				Thread t = new Thread(new ThreadStart(ProcessCycle));
				t.Name = "Process Cycle Thread " + threadNum;
				t.Start();
				threadQueue[threadNum] = t;
			}
			Inform("Started " + cfgThreadCount + " Processing cycle threads.");

			// Poll to get next matches from Alert database.
			if (cfgPollingProcessorHost == System.Environment.MachineName.ToLower())
			{
				threadGetNext = new Thread(new ThreadStart(ScheduleLoaderBL.Instance.Run));
				threadGetNext.Name = "Next ViralMail Cycle Thread";
				threadGetNext.Start();
				Inform("Started next ViralMail cycle thread: " + threadGetNext.Name);
                TraceOut("Started next ViralMail cycle thread: " + threadGetNext.Name + " on " + cfgPollingProcessorHost, 0);
			}
			else
			{
			    TraceOut("ScheduleLoader not starting due to pollingprocessorhost being different from the server running viral mail" + cfgPollingProcessorHost, 0);
			}
		}

		public void Stop()
		{
			isRunning = false;
			ScheduleLoaderBL.Instance.Stop();

			if (threadGetNext != null)
			{
				threadGetNext.Join(20000);
			}

			for (int threadNum = 0; threadNum < threadQueue.Length; threadNum++)
			{
				threadQueue[threadNum].Join(10000);
			}

			hydraWriter.Stop();
		}

		private void Inform(String statement)
		{
			System.Diagnostics.EventLog.WriteEntry(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME,
				statement,
				EventLogEntryType.Information);
		}

		private void InitPerfCounters()
		{
			perfMatchRate = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Matches processed/second", false);
			perfHaveStoredSearchPrefTotal = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Have Stored SearchPref Total", false);
			perfNoSearchPrefTotal = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "No SearchPref Total", false);
			perfItemsProcessedTotal = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Items Processed Total", false);
			perfQuery1Total = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Query1 Total", false);
			perfQuery2Total  = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Query2 Total", false);
			perfQuery3Total = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Query3 Total", false);
			perfQuery1NoResults = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Query1 No Results", false);
			perfQuery2NoResults = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Query2 No Results", false);
			perfQuery3NoResults = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Query3 No Results", false);
			perfTotalSent = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Total Sent", false);
			perfTotalSentRate = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Total Sent/second", false);
			perfTotalRegulated = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Total Regulated", false);
			perfTotalRegulatedRate = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Total Regulated/second", false);
			perfAvgSearchTime = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Search Time", false);
			perfAvgSearchTimeBase = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Search Base", false);
			perfAvgScrubTime = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Scrub Time", false);
			perfAvgScrubTimeBase = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Scrub Base", false);
			perfAvgGetQueueItemTime = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg GetQueueItem Time", false);
			perfAvgGetQueueItemTimeBase = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg GetQueueItem Base", false);
			perfAvgPopulateResultsTime = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Populate Results Time", false);
			perfAvgPopulateResultsTimeBase = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Populate Results Base", false);
			perfAvgSendToXmailTime = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg SendToXmail Time", false);
			perfAvgSendToXmailTimeBase = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg SendToXmail Base", false);
			perfAvgSavemnAlertTime = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Save mnAlert Time", false);
			perfAvgSavemnAlertTimeBase = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Save mnAlert Base", false);
			perfAvgGetMemberTime = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg GetMember Time", false);
			perfAvgGetMemberTimeBase = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg GetMemberBase", false);
			perfAvgBuildDefaultPrefTime = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Build Default Pref Time", false);
			perfAvgBuildDefaultPrefTimeBase = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Build Default Pref Base", false);
			perfAvgAdjustScrublistTime = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Adjust Scrublist Time", false);
			perfAvgAdjustScrublistTimeBase = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Adjust Scrublist Base", false);
			perfAvgSlowdownTime  = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Slowdown Time", false);
			perfAvgSlowdownTimeBase = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Slowdown Base",false);
			perfAvgCycleTime = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Cycle Time", false);
			perfAvgCycleTimeBase = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Avg Cycle Base",false);
			perfTransStarted = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Transactions Started", false);
			perfTransCommitted  = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Transactions Committed", false);
			perfTransAborted  = new PerformanceCounter(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "Transactions Aborted", false);

			ResetPerfCounters();
		}

		private void ResetPerfCounters()
		{
			perfMatchRate.RawValue = 0;
			perfHaveStoredSearchPrefTotal.RawValue = 0;
			perfNoSearchPrefTotal.RawValue = 0;
			perfItemsProcessedTotal.RawValue = 0;
			perfQuery1Total.RawValue = 0;
			perfQuery2Total.RawValue = 0;
			perfQuery3Total.RawValue = 0;
			perfQuery1NoResults.RawValue = 0;
			perfQuery2NoResults.RawValue = 0;
			perfQuery3NoResults.RawValue = 0;
			perfTotalSent.RawValue = 0;
			perfTotalSentRate.RawValue = 0;
			perfTotalRegulated.RawValue = 0;
			perfTotalRegulatedRate.RawValue = 0;
			perfAvgSearchTime.RawValue = 0;
			perfAvgSearchTimeBase.RawValue = 0;
			perfAvgScrubTime.RawValue = 0;
			perfAvgScrubTimeBase.RawValue = 0;
			perfAvgGetQueueItemTime.RawValue = 0;
			perfAvgGetQueueItemTimeBase.RawValue = 0;
			perfAvgPopulateResultsTime.RawValue = 0;
			perfAvgPopulateResultsTimeBase.RawValue = 0;
			perfAvgSendToXmailTime.RawValue = 0;
			perfAvgSendToXmailTimeBase.RawValue = 0;
			perfAvgSavemnAlertTime.RawValue = 0;
			perfAvgSavemnAlertTimeBase.RawValue = 0;
			perfAvgGetMemberTime.RawValue = 0;
			perfAvgGetMemberTimeBase.RawValue = 0;
			perfAvgBuildDefaultPrefTime.RawValue = 0;
			perfAvgBuildDefaultPrefTimeBase.RawValue = 0;
			perfAvgAdjustScrublistTime.RawValue = 0;
			perfAvgAdjustScrublistTimeBase.RawValue = 0;
			perfAvgSlowdownTime.RawValue = 0;
			perfAvgSlowdownTimeBase.RawValue = 0;
			perfAvgCycleTime.RawValue = 0;
			perfAvgCycleTimeBase.RawValue = 0;
			perfTransStarted.RawValue = 0;
			perfTransCommitted.RawValue = 0;
			perfTransAborted.RawValue = 0;
		}

		private void ProcessCycle()
		{
			StringBuilder scrubList = new StringBuilder(110 * 10);	// Grab enough for about 110 9 digit memebrids plus comma.
			PerformanceCounterHelper perfHelper = new PerformanceCounterHelper();
			PerformanceCounterHelper perfHelperCycle = new PerformanceCounterHelper();

			while (isRunning)
			{	
				TraceOut("ProcessCycle() beginning.", 0);
				perfHelperCycle.StartTiming();
				using (MessageQueueTransaction tran = new MessageQueueTransaction()) 
				{
					ViralMailQueueItem queueItem = null;
					Matchnet.Member.ServiceAdapters.Member member = null;
					Int32 brandID = Constants.NULL_INT;
					Brand brand = null;
					SearchPreferenceCollection searchPrefs = null;
					bool hasStoredSearchPrefs = true;
					
					try
					{
						tran.Begin();
						perfTransStarted.Increment();
						#region Get QueueItem
						try 
						{
							perfHelper.StartTiming();
							queueItem = GetViralMailQueueItem(tran);
							if (queueItem == null) 
							{
								// This should only happen for MSMQ timeout, or no items in queue.
							    TraceOut("GetViralMailQueueItem() returned nothing.", 0);
								tran.Commit();
								perfTransCommitted.Increment();
								continue;
							}
							perfHelper.EndTiming(perfAvgGetQueueItemTime, perfAvgGetQueueItemTimeBase);
							perfItemsProcessedTotal.Increment();
						}
						catch (Exception ex)
						{
							tran.Abort();
							perfTransAborted.Increment();
                            RollingFileLogger.Instance.LogException(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "ViralMailBL", "GetViralMailQueueItem failed getting item from queue.", ex, null);
							new ServiceBoundaryException(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME,
								"GetViralMailQueueItem failed getting item from queue.", ex);
							Thread.Sleep(10000);
							continue;
						}
						#endregion

                        TraceOut("Starting ViralMail processing for member ", queueItem.MemberID);
						#region Get Member
						try 
						{
							perfHelper.StartTiming();
							member = MemberSA.Instance.GetMember(queueItem.MemberID, MemberLoadFlags.None);
							member.GetLastLogonDate(queueItem.CommunityID, out brandID);
							brand = BrandConfigSA.Instance.GetBrandByID(brandID);
							perfHelper.EndTiming(perfAvgGetMemberTime, perfAvgGetMemberTimeBase);
						}
						catch (Exception ex)
						{
							tran.Abort();
							perfTransAborted.Increment();
                            RollingFileLogger.Instance.LogException(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "ViralMailBL", "GetMember() failed", ex, null);
							new ServiceBoundaryException(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, "GetMember() failed", ex);
							Thread.Sleep(10000);
							continue;
						}

						// Email Volume Regulation
						if (ExternalMail.ServiceAdapters.ExternalMailSA.Instance.IsRegulatedByLastLogonDate(member.MemberID, brand))
						{
							tran.Commit();

							perfTotalRegulated.Increment();
							perfTotalRegulatedRate.Increment();

							continue;
						}
						#endregion

						MiniprofileInfoCollection miniprofileInfoCollection = new MiniprofileInfoCollection();
                        MiniprofileInfoCollection YesMiniprofileInfoCollection = new MiniprofileInfoCollection();

						#region Get Yes Votes
						GetYesVotes(ref miniprofileInfoCollection, member, brand, queueItem.SentMemberIDs);
						#endregion

  						// If there were no yes votes then continue to the next iteration.
						if (miniprofileInfoCollection.Count.Equals(0))
						{
                            TraceOut("Member " + queueItem.MemberID.ToString() + " has no yes votes", 0);
							tran.Commit(); // Consume queue item and move on.
							perfTransCommitted.Increment();
							continue;
						}
                        else
                        {
                            //Save the Yes members for activity logging
                            foreach (MiniprofileInfo miniprofileInfo in miniprofileInfoCollection)
                            {
                                MiniprofileInfo miniprofileInfoYes = new MiniprofileInfo();
                                miniprofileInfoYes = miniprofileInfo;
                                YesMiniprofileInfoCollection.Add(miniprofileInfoYes);
                            }
                        }
						
						#region Gather initial search prefs.
						perfHelper.StartTiming();
						searchPrefs = queueItem.SearchPreferences;
						if (searchPrefs == null || searchPrefs.Count == 0)
						{
							hasStoredSearchPrefs = false;
							perfNoSearchPrefTotal.Increment();
						}
						else 
						{
							ValidationResult validationResult =  searchPrefs.Validate();
							if (validationResult.Status == PreferencesValidationStatus.Failed) 
							{
								searchPrefs = null;
								hasStoredSearchPrefs = false;	// What they have is not valid.  Might as well be they have none.
								perfNoSearchPrefTotal.Increment();
								TraceOut("Stored Search Pref invalid. Reason: " + validationResult.ValidationError.ToString(), member.MemberID);
							}
							else 
							{
								hasStoredSearchPrefs = true;
								FixIsraelAreaCodes(ref searchPrefs);
								perfHaveStoredSearchPrefTotal.Increment();
								//TraceOut("starting with prefs " + getPrefDump(searchPrefs),0);
							}						
						}

						if (!hasStoredSearchPrefs)	// Either had none or had some but were not valid.
						{		
							searchPrefs = GetDefaultPrefs(member, brand);
							//TraceOut("prefs invented" + getPrefDump(searchPrefs),0);
							if (searchPrefs == null)
							{
								tran.Commit();	// No hope for this member. consume queue item and move on.
								perfTransCommitted.Increment();
								continue;
							}
						}

						perfHelper.EndTiming(perfAvgBuildDefaultPrefTime, perfAvgBuildDefaultPrefTimeBase);
						#endregion

						#region Searches to populate miniprofile collection.
						Int32 miniprofileInfoColTempCount = miniprofileInfoCollection.Count;	// For perf counter.
					    int maxProfilePerEmail = IsTest3(queueItem.MemberID, brand)
					                                 ? MAX_PROFILES_PER_EMAIL_TEST3
					                                 : MAX_PROFILES_PER_EMAIL;

						// First search w/ photos.
						perfHelper.StartTiming();
						searchPrefs.Add("HasPhotoFlag", "1");
						PopulateResultsCollection(searchPrefs, brand, ref miniprofileInfoCollection, queueItem.SentMemberIDs, queueItem.MemberID, maxProfilePerEmail);
						perfHelper.EndTiming(perfAvgPopulateResultsTime, perfAvgPopulateResultsTimeBase);
						perfQuery1Total.Increment();

						if (miniprofileInfoCollection.Count == miniprofileInfoColTempCount) { perfQuery1NoResults.Increment(); }
						miniprofileInfoColTempCount = miniprofileInfoCollection.Count;

						// Second search w/ && w/o photos.
                        if (miniprofileInfoCollection.Count < maxProfilePerEmail)
						{
							TraceOut("step (now do search 2)",80);
							perfHelper.StartTiming();
							searchPrefs.Add("HasPhotoFlag", "0");
							PopulateResultsCollection(searchPrefs, brand, ref miniprofileInfoCollection, queueItem.SentMemberIDs, queueItem.MemberID, maxProfilePerEmail);
							perfHelper.EndTiming(perfAvgPopulateResultsTime, perfAvgPopulateResultsTimeBase);
							perfQuery2Total.Increment();

							if (miniprofileInfoCollection.Count == miniprofileInfoColTempCount) { perfQuery2NoResults.Increment(); } 
							miniprofileInfoColTempCount = miniprofileInfoCollection.Count;
						}
						
						// third search, only if need more and uses the default wide search prefs
                        if (hasStoredSearchPrefs && miniprofileInfoCollection.Count < maxProfilePerEmail) 
						{
							perfHelper.StartTiming();
							SearchPreferenceCollection laxSearchPrefs = new SearchPreferenceCollection();
							laxSearchPrefs.Add("GenderMask", searchPrefs.Value("GenderMask"));
							laxSearchPrefs.Add("RegionID", searchPrefs.Value("RegionID"));
							laxSearchPrefs.Add("SearchTypeID", "4");	// Region.
							laxSearchPrefs.Add("SearchOrderBy", "3");	// Proximity - will expand to 160 miles or whole country(!).
							laxSearchPrefs.Add("Distance", "40");
							laxSearchPrefs.Add("HasPhotoFlag", "1");
							// Min age and max age not added so that a default is invented in the search SA / BL.

							PopulateResultsCollection(laxSearchPrefs, brand, ref miniprofileInfoCollection, queueItem.SentMemberIDs, queueItem.MemberID, maxProfilePerEmail);
							perfHelper.EndTiming(perfAvgPopulateResultsTime, perfAvgPopulateResultsTimeBase);
							perfQuery3Total.Increment();

							if (miniprofileInfoCollection.Count == miniprofileInfoColTempCount) { perfQuery3NoResults.Increment(); }
							miniprofileInfoColTempCount = miniprofileInfoCollection.Count;
						}
						#endregion

						#region Process query result(s).
                        if (miniprofileInfoCollection.Count == maxProfilePerEmail)
						{
							// Randomize the order.
							RandomizeMiniprofileInfoCollection(ref miniprofileInfoCollection);

							TraceOut("scrublist current length", + queueItem.SentMemberIDs.Length);
							perfHelper.StartTiming();
                            PopulateScrublist(queueItem.SentMemberIDs, miniprofileInfoCollection, ref scrubList, maxProfilePerEmail);
							perfHelper.EndTiming(perfAvgAdjustScrublistTime, perfAvgAdjustScrublistTimeBase);

							try 
							{
								perfHelper.StartTiming();
                                TraceOut("Calling ExternalMail to send viral mail for member " + member.MemberID.ToString() + ", profile count ", miniprofileInfoCollection.Count);
								ExternalMail.ServiceAdapters.ExternalMailSA.Instance.SendViralEmail(brand.BrandID, member.EmailAddress, GetMiniprofileFromMember(brand, member), miniprofileInfoCollection);
								perfHelper.EndTiming(perfAvgSendToXmailTime, perfAvgSendToXmailTimeBase);

								perfTotalSent.Increment();
								perfTotalSentRate.Increment();

                                //Record Activity:
                                try
                                {
                                    string activityCaption = GetActivityCaption(miniprofileInfoCollection, YesMiniprofileInfoCollection);

                                    // Log this to ActivityRecording svc
                                    ActivityRecordingSA.Instance.RecordActivity(member.MemberID, Constants.NULL_INT, brand.Site.SiteID, ActivityRecording.ValueObjects.Types.ActionType.SendSecretAdmirerMail,
                                        ActivityRecording.ValueObjects.Types.CallingSystem.ViralMail, activityCaption);
                                }
                                catch (Exception ex)
                                {
                                    string message = "ActivityRecording threw an exception: " + ex.Message;
                                    EventLog.WriteEntry(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, message, EventLogEntryType.Error);
                                    RollingFileLogger.Instance.LogException(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "ViralMailBL", "ActivityRecording failed ", ex, null);
                                }

							}
							catch (Exception ex)
							{
                                RollingFileLogger.Instance.LogException(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "ViralMailBL", "ExternalMail call to send mail failed ", ex, null);
								new ServiceBoundaryException(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME,
									"Viralmail got exception from External Mail while attempting to send.",ex);
								continue;
							}
							finally 
							{
								tran.Commit();
								perfTransCommitted.Increment();
							}
						}
						else 
						{
							tran.Commit();
							perfTransCommitted.Increment();
							TraceOut("Tran.Commit - Not enough profiles for Viral mail.", miniprofileInfoCollection.Count);
						}
						#endregion

						#region Save to DB.
						perfHelper.StartTiming();
						SaveViralmailStatusToDB(member, brand, queueItem,scrubList.ToString(), (miniprofileInfoCollection.Count == maxProfilePerEmail));
						perfHelper.EndTiming(perfAvgSavemnAlertTime, perfAvgSavemnAlertTimeBase);
						perfMatchRate.Increment();
						TraceOut("step (saved mnAlert)",180);
						#endregion save to DB
					}
					catch (Exception ex)
					{
						new ServiceBoundaryException(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, 
							"Error processing Viralmail item. [CommunityID: " + queueItem.CommunityID + ", MemberID: " + queueItem.MemberID + "]", ex);
						TraceOut("General exception " + ex.ToString(), 175);
						Thread.Sleep(10000);
					}

					// regardless of the error should commit it so it doesn't stick the queue.
					if (tran.Status != MessageQueueTransactionStatus.Committed) 
					{
						tran.Commit();
						perfTransCommitted.Increment();
						TraceOut("Tran.Commit - Last catch.", Int32.MinValue);
					}
					//SlowDown();
				}
				perfHelperCycle.EndTiming(perfAvgCycleTime, perfAvgCycleTimeBase);
			}
		}

        public static string GetActivityCaption(MiniprofileInfoCollection miniprofileInfoCollection, MiniprofileInfoCollection YesMiniprofileInfoCollection)
        {
            string activityCaption = "";
            StringBuilder activityCaptionSb = new StringBuilder();
            bool yesMember;
            activityCaptionSb.Append("<Mail><Secret Admirer>");
            foreach (MiniprofileInfo miniprofileInfo in miniprofileInfoCollection)
            {
                yesMember = false;
                foreach (MiniprofileInfo YesMiniprofileInfo in YesMiniprofileInfoCollection)
                {
                    if (miniprofileInfo.MemberID == YesMiniprofileInfo.MemberID)
                    {
                        yesMember = true;
                        break;
                    }
                }
                if (miniprofileInfo.MemberID != 0 && !yesMember)
                {
                    activityCaptionSb.Append("<MemberID>").Append(miniprofileInfo.MemberID).Append("</MemberID>");
                }
            }
            activityCaptionSb.Append("</Secret Admirer>");
            activityCaptionSb.Append("<Admirer>");
            foreach (MiniprofileInfo YesMiniprofileInfo in YesMiniprofileInfoCollection)
            {
                activityCaptionSb.Append("<MemberID>").Append(YesMiniprofileInfo.MemberID).Append("</MemberID>");
            }
            activityCaptionSb.Append("</Admirer>");
            activityCaptionSb.Append("</Mail>");

            activityCaption = activityCaptionSb.ToString();
            return activityCaption;
        }
   
		private ViralMailQueueItem GetViralMailQueueItem(MessageQueueTransaction tran)
		{
			TimeSpan ts = new TimeSpan(0, 0, 10);
			ViralMailQueueItem queueItem = null;

			try
			{
				queueItem = queueViralMail.Receive(ts, tran).Body as ViralMailQueueItem;
				return queueItem;
			}
			catch (System.Messaging.MessageQueueException mqEx)
			{
				if (mqEx.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout)
				{
					throw mqEx;
				}
				else 
				{
					return null;
				}
			}
			catch (Exception ex)
			{
                RollingFileLogger.Instance.LogException(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "ViralMailBL", "GetMatchMailQueueItem", ex, null);
				throw new Exception("GetMatchMailQueueItem failed receive. Empty / incompatible?", ex);
			}			
		}

		private void GetYesVotes(ref MiniprofileInfoCollection miniprofileInfoCollection,
			Matchnet.Member.ServiceAdapters.Member member,
			Brand brand,
			String[] scrubList)
		{
			Command command = new Command("mnList", "up_YNMList_ViralMail_List", member.MemberID);

			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, member.MemberID);
			command.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, brand.Site.Community.CommunityID);

			DataTable dt = Client.Instance.ExecuteDataTable(command);

			GenderMask seekingGender = GetSeekingGender(member, brand);

		    int maxYesPerEmail = IsTest3(member.MemberID, brand) ? MAX_YES_PER_EMAIL_TEST3 : MAX_YES_PER_EMAIL;

			foreach (DataRow row in dt.Rows)
			{
				Int32 yesMemberID = System.Convert.ToInt32(row["TargetMemberID"]);
				
				Matchnet.Member.ServiceAdapters.Member yesMember = MemberSA.Instance.GetMember(yesMemberID,
					MemberLoadFlags.None);

				// If the genders are ok, the yesMember is not in the scrub list, and the yesMember is not
				// no the same as member, then send an email.
				if (CheckGender(seekingGender, yesMember, brand) && 
					!IsInScrubList(scrubList, yesMember.MemberID) && 
					yesMember.MemberID != member.MemberID)
				{
					miniprofileInfoCollection.Add(GetMiniprofileFromMember(brand, yesMember));
				}

				// Even if the genders don't match up, mark the ViralStatus as "mail sent" so that 
				// this viral match doesn't keep showing up.
				SaveViralStatus(member.MemberID, yesMember.MemberID, brand);

				// Limit the number of Yes votes per email.
                if (miniprofileInfoCollection.Count.Equals(maxYesPerEmail))
					break;
			}
		}

		private MiniprofileInfo GetMiniprofileFromMember(Brand brand, Matchnet.Member.ServiceAdapters.Member member)
		{
			Int32 memberID = member.MemberID;
			String userName = member.GetUserName(brand);
			DateTime birthDate = member.GetAttributeDate(brand, "Birthdate", DateTime.MinValue);
			Int32 regionID = member.GetAttributeInt(brand, "RegionID", Constants.NULL_INT);
			String thumbNailWebPath = GetThumbPathNoPhoto(brand);
			String aboutMe = member.GetAttributeTextApproved(brand, "AboutMe", String.Empty);
		    thumbNailWebPath = GetThumbnailImage(member.MemberID, brand);
            return new MiniprofileInfo(memberID, userName, birthDate, regionID,	thumbNailWebPath, String.Empty);
		}

		

		/// <summary>
		/// Should only get Yes votes matching seeking gender
		/// and only get two yes votes.
		/// </summary>
		/// <param name="members"></param>
		/// <returns></returns>
		private Boolean CheckGender(GenderMask seekingGender, 
			Matchnet.Member.ServiceAdapters.Member yesMember,
			Brand brand) 
		{
			if (seekingGender == GetGender(yesMember, brand)) 
				return true;
			else
				return false;
		}

		private GenderMask GetSeekingGender(Matchnet.Member.ServiceAdapters.Member member, Brand brand) 
		{
			Int32 genderMask = member.GetAttributeInt(brand, "GenderMask", Constants.NULL_INT);
			
			if ((genderMask & (Int16)GenderMask.SeekingMale) == (Int16)GenderMask.SeekingMale) 
				return GenderMask.Male;
			else if ((genderMask & (Int16)GenderMask.SeekingFemale) == (Int16)GenderMask.SeekingFemale) 
				return GenderMask.Female;
			else if ((genderMask & (Int16)GenderMask.SeekingFTM) == (Int16)GenderMask.SeekingFTM) 
				return GenderMask.FTM;
			else if ((genderMask & (Int16)GenderMask.SeekingMTF) == (Int16)GenderMask.SeekingMTF) 
				return GenderMask.MTF;
			else
				return GenderMask.Female;
		}

		private GenderMask GetGender(Matchnet.Member.ServiceAdapters.Member member, Brand brand) 
		{
			Int32 genderMask = member.GetAttributeInt(brand, "GenderMask", Constants.NULL_INT);

			if ((genderMask & (Int16)GenderMask.Male) == (Int16)GenderMask.Male)
				return GenderMask.Male;
			else if ((genderMask & (Int16)GenderMask.Female) == (Int16)GenderMask.Female)
				return GenderMask.Female;
			else if ((genderMask & (Int16)GenderMask.FTM) == (Int16)GenderMask.FTM)
				return GenderMask.FTM;
			else if ((genderMask & (Int16)GenderMask.MTF) == (Int16)GenderMask.MTF)
				return GenderMask.MTF;
			else
				return GenderMask.Female;
		}

		private void SaveViralStatus(Int32 memberID, Int32 targetMemberID, Brand brand)
		{
			// Update member's list.
			Command command = new Command("mnList", "up_YNMList_Status_Save", memberID);

			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, brand.Site.Community.CommunityID);
			command.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
			command.AddParameter("@MailMask", SqlDbType.TinyInt, ParameterDirection.Input, MAIL_STATUS_VIRAL_MAIL_SENT);
			
			try
			{
				Client.Instance.ExecuteAsyncWrite(command);
			}
			catch(Exception ex)
			{
                RollingFileLogger.Instance.LogException(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "ViralMailBL", "SaveViralStatus RecipientMember " + memberID.ToString(), ex, null);
				throw new BLException("Error saving viral status. [MemberID:" + memberID +
					", TargetmemberID: " + targetMemberID + 
					", CommunityID" + brand.Site.Community.CommunityID +
					", MailMask" + MAIL_STATUS_VIRAL_MAIL_SENT +
					ex);
			}

			// Update target member's list.
			command = new Command("mnList", "up_YNMList_Status_Save", targetMemberID);

			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
			command.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, brand.Site.Community.CommunityID);
			command.AddParameter("@TargetMemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@MailMask", SqlDbType.TinyInt, ParameterDirection.Input, MAIL_STATUS_VIRAL_MAIL_SENT);
			
			try
			{
				Client.Instance.ExecuteAsyncWrite(command);
			}
			catch(Exception ex)
			{
                RollingFileLogger.Instance.LogException(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "ViralMailBL", "SaveViralStatus TargetMember " + targetMemberID.ToString(), ex, null);
				throw new BLException("Error saving viral status. [MemberID:" + memberID +
					", TargetmemberID: " + targetMemberID + 
					", CommunityID" + brand.Site.Community.CommunityID +
					", MailMask" + MAIL_STATUS_VIRAL_MAIL_SENT +
					ex);
			}
		}

		private void TraceOut(String msg, Int32 num)
		{
            RollingFileLogger.Instance.LogInfoMessage(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "ViralMailBL", msg + " : " + num.ToString(), null);
		}

		private void populateResultItemFromMemberService(MatchnetResultItem resultItem, Brand brand)
		{
			//string[] miniProfileFields = new string[] { "username", "birthdate", "regionid", "gendermask", "thumbpath" /* "aboutme",  "headline", not in use*/};
			Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(resultItem.MemberID, MemberLoadFlags.None);
			resultItem["username"] = member.GetUserName(brand);
			resultItem["birthdate"] = member.GetAttributeDate(brand, "birthdate", DateTime.MinValue).ToString();
			resultItem["regionid"] = member.GetAttributeInt(brand, "regionid").ToString();
			resultItem["gendermask"] = member.GetAttributeInt(brand, "gendermask").ToString();
		    resultItem["thumbPath"] = GetThumbnailImage(resultItem.MemberID, brand);
		}

        private string GetThumbnailImage(int memberID, Brand brand)
        {
            string thumbPath = string.Empty;

            Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
            Matchnet.Member.ValueObjects.Photos.PhotoCommunity photos = member.GetPhotos(brand.Site.Community.CommunityID);

            if (photos.Count > 0 && photos[0].IsApproved)
            {
                if (photos[0].IsPrivate)
                {
                    thumbPath = GetThumbPathPrivatePhoto(brand);
                }
                else
                {
                    bool showPhotosFromCloud = Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD"));

                    if(showPhotosFromCloud)
                    {
                        if(String.IsNullOrEmpty(photos[0].ThumbFileCloudPath))
                        {
                            thumbPath = GetThumbPathNoPhoto(brand);
                        }
                        else
                        {
                            Spark.CloudStorage.Client cloudClient = new Spark.CloudStorage.Client(brand.Site.Community.CommunityID, brand.Site.SiteID, RuntimeSettings.Instance);
                            thumbPath = cloudClient.GetFullCloudImagePath(photos[0].ThumbFileCloudPath, FileType.MemberPhoto, false);
                        }
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(photos[0].ThumbFileWebPath))
                        {
                            thumbPath = GetThumbPathNoPhoto(brand);
                        }
                        else
                        {
                            thumbPath = "http://" + brand.Site.DefaultHost + "." + brand.Uri + photos[0].ThumbFileWebPath;
                        }
                    }
                }
            }
            else
            {
                thumbPath = GetThumbPathNoPhoto(brand);
            }

            return thumbPath;
        }

        private String GetThumbPathNoPhoto(Brand brand)
        {
            // if the site is JDIL (site id of 4) use Site folder for the image (Because JDIL is within JD community)
            if (brand.Site.SiteID == 4)
            {
                return "http://" + brand.Site.DefaultHost + "." + brand.Uri + @"/img/Site/" + brand.Uri.Replace(".", "-") + "/nophoto.jpg";
            }
            else
            {
                return "http://" + brand.Site.DefaultHost + "." + brand.Uri + @"/img/Community/" + brand.Site.Community.Name + "/nophoto.jpg";
            }
        }

        private String GetThumbPathPrivatePhoto(Brand brand)
        {
            string retVal = "http://" + brand.Site.DefaultHost + "." + brand.Uri + @"/img/Community/" + brand.Site.Community.Name + "/privatephoto80x104_ynm4.jpg";

            return (retVal);
        }

		/// HACK:
		/// Since area codes are saved as int, sometimes they are null number.
		/// Prepend a "0" to israel area codes, otherwise they don't resolve down the line.
		/// Only do so if the area code is a digit, and not already 0 prepended.
		public static void FixIsraelAreaCodes(ref SearchPreferenceCollection searchPrefs)
		{
			if (searchPrefs["SearchTypeID"] == "2" && searchPrefs["CountryRegionID"] == "105")
			{
				foreach (string ac in areaCodeNames)
				{
					if (searchPrefs[ac] != String.Empty && searchPrefs[ac] != Constants.NULL_INT.ToString() && !searchPrefs[ac].StartsWith("0"))
					{
						searchPrefs[ac] = "0" + searchPrefs[ac];
					}
				}
			}
		}

		private SearchPreferenceCollection GetDefaultPrefs(Matchnet.Member.ServiceAdapters.Member member, Brand brand)
		{
			SearchPreferenceCollection newPrefs = new SearchPreferenceCollection();
			try 
			{
				Int32 genderMask = member.GetAttributeInt(brand.Site.Community.CommunityID, 0, 0, "GenderMask", Constants.NULL_INT);
				Int32 regionID = member.GetAttributeInt(brand.Site.Community.CommunityID, 0, 0, "RegionID", Constants.NULL_INT);
				DateTime birthDate = member.GetAttributeDate(brand.Site.Community.CommunityID, 0, 0, "Birthdate", DateTime.MinValue);

				if (genderMask != Constants.NULL_INT && regionID != Constants.NULL_INT && birthDate != DateTime.MinValue && GenderUtils.IsReversable(genderMask))
				{
					genderMask = GenderUtils.FlipMaskIfHeterosexual(genderMask);	// Get seeking gender.
					Int32[] ageRange = AgeUtils.GetAgeRange(birthDate, Constants.NULL_INT, Constants.NULL_INT);
					newPrefs.Add("GenderMask", genderMask.ToString());
					newPrefs.Add("RegionID", regionID.ToString());
					newPrefs.Add("SearchTypeID", "4"); //Region
					newPrefs.Add("SearchOrderBy", "3"); // Proximity - will expand to 160 miles or whole country(!)
					newPrefs.Add("Distance", "40");
					newPrefs.Add("MinAge", ageRange[0].ToString());
					newPrefs.Add("MaxAge", ageRange[1].ToString());
					return newPrefs;
				}
				else
				{
					TraceOut("GetDefaultPrefs failed for. Bunk member? CommunityID " + brand.Site.Community.CommunityID.ToString() + " |" + genderMask.ToString() + "|"+ regionID.ToString() +"|" + birthDate.ToString(), member.MemberID);
					return null;
				}
			}
			catch (Exception ex)
			{
				new ServiceBoundaryException(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, 
					"GetDefaultPrefs could not extract gender, region, birthdate for " + member.MemberID.ToString() + " CommunityID " + brand.Site.Community.CommunityID.ToString(),
					ex);
				return null;			
			}
		}

		private void PopulateResultsCollection(SearchPreferenceCollection searchPrefs, Brand brand, ref MiniprofileInfoCollection miniprofileInfoCollection, String[] scrubList, Int32 memberID, int maxProfilePerEmail)
		{
			MatchnetQueryResults searchResults;
			ValidationResult validationResult = searchPrefs.Validate();

			if (validationResult.Status == PreferencesValidationStatus.Failed) 
			{ 
				TraceOut("PopulateResultsCollection got invalid prefs (" + validationResult.ValidationError.ToString() + ")" + GetPrefDump(searchPrefs), memberID);
				return;
			}
			
			try
			{
				PerformanceCounterHelper pch = new PerformanceCounterHelper();
				searchResults = MemberSearchSA.Instance.Search(searchPrefs,
					brand.Site.Community.CommunityID,
					brand.Site.SiteID,
					0,
					PAGE_SIZE,
					Matchnet.Search.Interfaces.SearchEngineType.FAST,
					Matchnet.Search.Interfaces.SearchType.MatchMail);
				pch.EndTiming(perfAvgSearchTime, perfAvgSearchTimeBase);
			}
			catch(Exception ex)
			{
				new ServiceBoundaryException(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME ,
					"PopulateResultCollection search failed. " + GetPrefDump(searchPrefs), ex);
				return;
			}

			if (searchResults.Items.Count > 0)
			{
				PerformanceCounterHelper pch = new PerformanceCounterHelper();
				for (int resultIdx = 0; resultIdx < searchResults.Items.Count; resultIdx++)
				{
					MatchnetResultItem resultItem = (searchResults.Items[resultIdx] as MatchnetResultItem);
					
					// Make sure member is not in his own results.
					if (!IsInScrubList(scrubList, resultItem) && resultItem.MemberID != memberID)
					{
						// Make sure there are no duplicates.
						if (!IsInMiniprofileInfoCollection(miniprofileInfoCollection, resultItem))
						{
							miniprofileInfoCollection.Add(GetMiniprofileFromResultItem(resultItem, brand));
						}
					}

                    if (miniprofileInfoCollection.Count == maxProfilePerEmail)
					{
						break;
					}
				}
				pch.EndTiming(perfAvgScrubTime, perfAvgScrubTimeBase);
			}
		}

		public static String GetPrefDump(SearchPreferenceCollection searchPrefs)
		{
			if (searchPrefs == null || searchPrefs.Count == 0) 
			{
				return "searchPref empty or null";
			}

			StringBuilder sb = new StringBuilder(100);
			foreach (DictionaryEntry sp in searchPrefs)
			{
				if (sb ==null) 
				{
					sb.Append("[<NULL> sp]");
					continue;
				}
				sb.Append("["); 
				sb.Append((sp.Key == null) ? "NULL" : sp.Key.ToString() );
				sb.Append(" = ");
				sb.Append( (sp.Value == null) ? "NULL" : sp.Value.ToString()); 
				sb.Append("]");
			}

			return sb.ToString();
		}

		private Boolean IsInMiniprofileInfoCollection(MiniprofileInfoCollection miniprofileInfoCollection, MatchnetResultItem resultItem)
		{
			Boolean found = false;
			foreach (MiniprofileInfo miniprofileInfo in miniprofileInfoCollection)
			{
				if (miniprofileInfo.MemberID == resultItem.MemberID)
				{
					found = true;
					break;
				}
			}

			return found;
		}

		private MiniprofileInfo GetMiniprofileFromResultItem(MatchnetResultItem resultItem, Brand brand)
		{
			try
			{
				Int32 memberID = resultItem.MemberID;

				//Hack to support e1 -- retrieve extra data directly from the Member service
				if (resultItem["username"] == null || resultItem["username"] == string.Empty)
				{
					populateResultItemFromMemberService(resultItem, brand);
				}

				String userName = resultItem["username"];
				DateTime birthDate = DateTime.Parse(resultItem["birthdate"]);
				Int32 regionID = Matchnet.Conversion.CInt(resultItem["regionid"]);
				String aboutMe = String.Empty;	// resultItem["aboutme"];
				String thumbNailWebPath = resultItem["thumbpath"];
				if (String.IsNullOrEmpty(thumbNailWebPath))
				{
				    thumbNailWebPath = GetThumbnailImage(resultItem.MemberID, brand);
				}

				return new MiniprofileInfo(memberID, userName, birthDate, regionID, thumbNailWebPath, aboutMe);
			}
			catch(Exception ex)
			{
				throw new BLException("Error converting ResultItem to MiniprofileInfo. Missing member attributes?", ex);
			}
		}

       


		private void SaveViralmailStatusToDB(Matchnet.Member.ServiceAdapters.Member member, Brand brand, ViralMailQueueItem queueItem, String scrubList, Boolean SentMatchesFlag)
		{
			try
			{
				Command command = new Command(HYDRA_SAVE_NAME, "up_ViralMailSchedule_Save", 0);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, member.MemberID);
				command.AddParameter("@CommunityID", SqlDbType.Int, ParameterDirection.Input, queueItem.CommunityID);
				command.AddParameter("@SentFlag", SqlDbType.Bit, ParameterDirection.Input, (SentMatchesFlag) ? 1 : 0);
				command.AddParameter("@LastAttemptDate", SqlDbType.DateTime, ParameterDirection.Input, queueItem.LastAttemptDate);
				command.AddParameter("@NextAttemptDate", SqlDbType.DateTime, ParameterDirection.Input, queueItem.NextAttemptDate);
				command.AddParameter("@LastSentDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now);
				command.AddParameter("@SentMemberIDList", SqlDbType.VarChar, ParameterDirection.Input, scrubList);

				Client.Instance.ExecuteAsyncWrite(command);
			}
			catch(Exception ex)
			{
				throw new Exception("Error updating mnAlert. " +
					"[MemberID:" + member.MemberID +
					", CommunityID:" + queueItem.CommunityID + 
					", SiteID:" + brand.Site.SiteID +
					", SentFlag:" + ((SentMatchesFlag) ? 1 : 0).ToString() +
					", LastAttemptDate:" + queueItem.LastAttemptDate.ToString("g") +
					", NextAttemptDate:" + queueItem.NextAttemptDate.ToString("g") +
					", LastSentDate:" + DateTime.Now +
					", SentMemberIDList:" + scrubList, ex);
			}
		}

//		private void SlowDown()
//		{
//			CounterSample sample1 = perfMatchRate.NextSample();
//			float matchrate = CounterSample.Calculate(sample1, perfMatchRate.NextSample());
//			if (matchrate > fQPSLimit)
//			{	// This would make the thread sleep when mps gets too high.
//				System.Diagnostics.Trace.WriteLine("matchrate " + matchrate.ToString() + " << " + perfMatchRate.NextValue().ToString() + " | qps limit:" + fQPSLimit.ToString());
//				PerformanceCounterHelper perfHelper = new PerformanceCounterHelper();
//				Thread.Sleep(Convert.ToInt32( 10.0 * fThreadCount / fQPSLimit));
//				perfHelper.EndTiming(perfAvgSlowdownTime, perfAvgSlowdownTimeBase);
//			}
//		}

		public static void PopulateScrublist(String[] currentList, MiniprofileInfoCollection newMatches, ref StringBuilder buffer, int maxProfilePerEmail)
		{
			buffer.Length = 0;
			Int32 startIdx = 0;
			Int32 endIdx = Math.Min(currentList.Length, MAX_SCRUBLIST_ITEMS);

            if (currentList.Length + maxProfilePerEmail > MAX_SCRUBLIST_ITEMS) 
			{
                startIdx = maxProfilePerEmail;
			}

			for (Int32 i = startIdx; i < endIdx; i++) 
			{
				buffer.Append(currentList[i]);
				buffer.Append(",");
			}
		
			for (Int32 i = 0; i < newMatches.Count; i++)
			{
				buffer.Append((newMatches.Item(i) as MiniprofileInfo).MemberID.ToString());
				if (i + 1 < newMatches.Count)
				{
					buffer.Append(",");
				}
			}
		}

		private Boolean IsInScrubList(String[] scrubList, MatchnetResultItem resultItem)
		{
			return IsInScrubList(scrubList, resultItem.MemberID);
		}

		private Boolean IsInScrubList(String[] scrubList, Int32 memberID)
		{
			Boolean found = false;

			for (Int32 index = 0; index < scrubList.Length; index++)
			{
				if ((String)scrubList[index] == memberID.ToString())
				{
					found = true;
					break;
				}	
			}

			return found;
		}

		private void RandomizeMiniprofileInfoCollection(ref MiniprofileInfoCollection miniprofileInfoCollection) 
		{
			Int32 index;
			Random random = new Random();
			
			// Shuffle count * 3 times.
			for (int i = 0; i < miniprofileInfoCollection.Count * 3; i++)
			{
				index = random.Next(0, miniprofileInfoCollection.Count - 1);
				
				miniprofileInfoCollection.Add(miniprofileInfoCollection.Item(index));
				miniprofileInfoCollection.RemoveAt(index);
			}
		}

        private bool IsTest3(int memberID, Brand brand)
        {
            bool testGroup = false;
            string lastDigitList = RuntimeSettings.Instance.GetSettingFromSingleton("USE_SECRET_ADMIRER_EMAIL_TEST3_LASTDIGITS", brand.Site.Community.CommunityID,
                                                                                    brand.Site.SiteID, brand.BrandID);

            if (!string.IsNullOrEmpty(lastDigitList))
            {
                string[] lastDigitsArray = lastDigitList.Split(new char[] { ',' });
                int mod = 10;
                string lastDigit = (memberID % mod).ToString();

                if (lastDigitsArray.Length > 0)
                {
                    foreach (var digit in lastDigitsArray)
                    {
                        if (lastDigit == digit)
                        {
                            testGroup = true;
                            break;
                        }
                    }
                }
            }

            return testGroup;
        }

		#region Perf Counters
		public static void PerfCounterInstall()
		{	
			CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
			ccdc.AddRange(new CounterCreationData [] {	
														 new CounterCreationData("Matches processed/second", "Matches processed/second", PerformanceCounterType.RateOfCountsPerSecond32),
														 new CounterCreationData("Have Stored SearchPref Total", "Count of people with stored search pref", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("No SearchPref Total", "Count of people with no stored search pref", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("Items Processed Total", "Cont Total Items Processed", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("Query1 Total", "Query Step 1 Total", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("Query2 Total", "Query Step 2 Total", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("Query3 Total", "Query Step 3 Total", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("Query1 No Results", "Query Step 1 Returned No Results", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("Query2 No Results", "Query Step 1 Returned No Results", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("Query3 No Results", "Query Step 1 Returned No Results", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("Total Sent", "Total MatchMail sent to external mail svc", PerformanceCounterType.NumberOfItems32),
														 new CounterCreationData("Total Sent/second", "Rate of MatchMail sent/second to external mail svc", PerformanceCounterType.RateOfCountsPerSecond32),
														 new CounterCreationData("Avg Search Time","Average time spent from call to search to receipt of results", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg Search Base","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg Scrub Time","Avg time scrubbing results", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg Scrub Base","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg GetQueueItem Time","Avg time to get queue item", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg GetQueueItem Base","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg Populate Results Time","Avg time ", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg Populate Results Base","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg SendToXmail Time","Avg time sending to external mail", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg SendToXmail Base","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg Save mnAlert Time","Avg time saving an async call to hydra", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg Save mnAlert Base","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg GetMember Time","Avg time getting member from Member svc", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg GetMemberBase","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg Build Default Pref Time","Avg time Building default search prefs", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg Build Default Pref Base","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg Adjust Scrublist Time","Avg time adding / building  scrublist", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg Adjust Scrublist Base","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg Slowdown Time","Avg time waiting, slowdown", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg Slowdown Base","Base", PerformanceCounterType.AverageBase),
														 new CounterCreationData("Avg Cycle Time","Avg time for a full cycle. This is a full loop of sending matchmail to one person.", PerformanceCounterType.AverageCount64),
														 new CounterCreationData("Avg Cycle Base","Base", PerformanceCounterType.AverageBase)

													 });

			PerformanceCounterCategory.Create(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME, ccdc);
		}


		public static void PerfCounterUninstall()
		{	
			if (PerformanceCounterCategory.Exists(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME))
			{
				PerformanceCounterCategory.Delete(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_NAME);
			}
		}
		#endregion
		#endregion
	}
}
