using System;

using Matchnet;


namespace Matchnet.ViralMail.BusinessLogic
{
	/// <summary>
	///	This was copied over from the web solution and should probably be placed in a shared location.
	/// </summary>
	public class GenderUtils
	{

		private static int [] _FlippableMasks = {5,  6,  9,  10,  20,  24,  36,  40,  65,  66,  80,  96,  129,  130,  144,  160 };

		private const int MASK_SLEF = (Int32)GenderMask.Male + (Int32)GenderMask.Female + (Int32)GenderMask.MTF + (Int32)GenderMask.FTM;
		private const int MASK_SEEKING = (Int32)GenderMask.SeekingMale + (Int32)GenderMask.SeekingFemale + (Int32)GenderMask.SeekingMTF + (Int32)GenderMask.SeekingFTM;

		public static int GetGenderMaskSelf(int genderMask)	{ return genderMask & MASK_SLEF; }

		public static int GetGenderMaskSeeking(int genderMask) {	return genderMask & MASK_SEEKING;	}

		public static int GetGenderSeekingFromGender(int genderMask)
		{
			int retVal = 0;

			if (MaskContains(genderMask, (Int32)GenderMask.Male))		retVal += (Int32)GenderMask.SeekingMale;
			if (MaskContains(genderMask, (Int32)GenderMask.Female))	retVal += (Int32)GenderMask.SeekingFemale;
			if (MaskContains(genderMask, (Int32)GenderMask.MTF))		retVal += (Int32)GenderMask.SeekingMTF;
			if (MaskContains(genderMask, (Int32)GenderMask.FTM))		retVal += (Int32)GenderMask.SeekingFTM;

			return retVal;
		}

		public static int GetGenderFromGenderSeeking(int genderMask)
		{
			int retVal = 0;

			if (MaskContains(genderMask, (Int32)GenderMask.SeekingMale))		retVal += (Int32)GenderMask.Male;
			if (MaskContains(genderMask, (Int32)GenderMask.SeekingFemale))	retVal += (Int32)GenderMask.Female;
			if (MaskContains(genderMask, (Int32)GenderMask.SeekingMTF))		retVal += (Int32)GenderMask.MTF;
			if (MaskContains(genderMask, (Int32)GenderMask.SeekingFTM))		retVal += (Int32)GenderMask.FTM;

			return retVal;
		}

		/// <summary>
		/// Determines whether or not a given gender and seeking gender indicate
		/// a homosexual orientation
		/// </summary>
		/// <param name="gender"></param>
		/// <param name="seekingGender"></param>
		/// <returns></returns>
		public static bool IsHomosexual(int gender, int seekingGender)
		{
			bool retVal = false;

			if (
				(gender == (Int32)GenderMask.Male && seekingGender == (Int32)GenderMask.SeekingMale)
				||	(gender == (Int32)GenderMask.Female && seekingGender == (Int32)GenderMask.SeekingFemale) 
				||	(gender == (Int32)GenderMask.MTF && seekingGender == (Int32)GenderMask.SeekingMTF) 
				||	(gender == (Int32)GenderMask.FTM && seekingGender == (Int32)GenderMask.SeekingFTM) )
			{
				retVal = true;
			}

			return(retVal);
		}

		/// <summary>
		///	Determines whether or not a given gender mask indicate a
		///	homosexual orientation
		/// </summary>
		/// <param name="genderMask"></param>
		/// <returns></returns>
		public static bool IsMaskHomosexual(int genderMask)
		{
			int genderSelf = GetGenderMaskSelf(genderMask);
			int genderSeeking = GetGenderMaskSeeking(genderMask);

			return IsHomosexual(genderSelf, genderSeeking);
		}

		/// <summary>
		/// Determines whether or not a given gender and seeking gender indicate
		/// a heterosexual orientation
		/// </summary>
		/// <param name="gender"></param>
		/// <param name="seekingGender"></param>
		/// <returns></returns>
		public static bool IsHeterosexual(int gender, int seekingGender)
		{
			return(!IsHomosexual(gender, seekingGender));
		}

		/// <summary>
		///	Determines whether or not a given gender mask indicate a
		///	heterosexual orientation
		/// </summary>
		/// <param name="genderMask"></param>
		/// <returns></returns>
		public static bool IsMaskHeterosexual(int genderMask)
		{
			return(!IsMaskHomosexual(genderMask));
		}

		/// <summary>
		/// Determine if the bitmask is reversible, that is that the operation of 
		/// getting "who I am looking for" is sane given this.
		/// Currenly only reversible if a single "I am" and a single "seeking" is found.
		/// </summary>
		/// <param name="gendermask"></param>
		/// <returns></returns>
		public static bool IsReversable(int genderMask)
		{
			return (Array.BinarySearch(_FlippableMasks,genderMask) >= 0);
		}

		/// <summary>
		/// Apparently the gender mask is stored in such a way that if
		/// we want it to dispaly properly we have to flip it when we
		/// pull it from the preferences and before it gets put back
		/// into the preferences if and only if the user is a
		/// heterosexual.  This method encapsulates that functionality.
		/// </summary>
		/// <param name="genderMask"></param>
		/// <returns></returns>
		public static int FlipMaskIfHeterosexual(int genderMask)
		{
			int retVal = genderMask;

			if(IsMaskHeterosexual(genderMask))
			{
				int genderSelf = GetGenderMaskSelf(genderMask);
				int genderSeeking = GetGenderMaskSeeking(genderMask);

				genderSelf = GetGenderSeekingFromGender(genderSelf);
				genderSeeking = GetGenderFromGenderSeeking(genderSeeking);

				retVal = genderSelf + genderSeeking;
			}
				
			return (retVal);
		}

		public static bool MaskContains(int genderMask, int genderID)
		{
			return ((genderMask & genderID) == genderID);
		}
	}

	public class AgeUtils 
	{
		public static int [] GetAgeRange(DateTime birthDate, int ageMin, int ageMax)
		{
			if(ageMin == Constants.NULL_INT || ageMax == Constants.NULL_INT) 
			{
				int age = (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
				if (age >= Constants.MIN_AGE_RANGE && age <= 24) 
				{
					ageMin = Constants.MIN_AGE_RANGE;
					ageMax = 24;
				} 
				else if (age >= 25 && age <= 34) 
				{
					ageMin = 25;
					ageMax = 34;
				}
				else if (age >= 35 && age <= 44) 
				{
					ageMin = 35;
					ageMax = 44;
				}
				else if (age >= 45 && age <= 54) 
				{
					ageMin = 45;
					ageMax = 54;
				}
				else if (age >= 55 && age <= Constants.MAX_AGE_RANGE) 
				{
					ageMin = 55;
					ageMax = Constants.MAX_AGE_RANGE;
				}
				else 
				{
					ageMin = 25;
					ageMax = 34;
				}
			}
			return new int [] {ageMin, ageMax};
		}

		public static int GetAge(DateTime birthDate)
		{
			return (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
		}
	}
}
