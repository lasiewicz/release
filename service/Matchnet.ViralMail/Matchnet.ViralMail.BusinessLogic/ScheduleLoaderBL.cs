using System;
using System.Diagnostics;
using System.Threading;
using System.Data;
using System.Messaging;

using Matchnet.Data;
using Matchnet.ViralMail.ValueObjects;
using Matchnet.Search.ValueObjects;
using Matchnet.Exceptions;
using Spark.Logging;

namespace Matchnet.ViralMail.BusinessLogic
{
	/// <summary>
	/// Manages retreival of scheduled eligible recipients, and distribution of them to available running ViralMail instances.
	/// </summary>
	public class ScheduleLoaderBL
	{
		#region Constants
		// Maximum number of items in an MSMQ queue, to avoid over loading.
		public const Int32 MAX_ITEMS_IN_QUEUE = 1024 * 16;
		public static readonly ScheduleLoaderBL Instance = new ScheduleLoaderBL();
		private const String FK_SCHEDULE_SEARCHPREFERENCES = "fk_ViralMailSchedule_SearchPreference";
		#endregion

		#region Variables
		private static String[] searchPreferenceColumnNameList = new string [] {"searchorderby","searchtypeid","gendermask","minage","maxage","regionid","schoolid","areacode1","areacode2","areacode3","areacode4","areacode5","areacode6","distance","hasphotoflag","educationlevel","religion","languagemask","ethnicity","smokinghabits","drinkinghabits","minheight","maxheight","maritalstatus","jdatereligion","jdateethnicity","synagogueattendance","keepkosher","sexualidentitytype","relationshipmask","relationshipstatus","bodytype","zodiac","majortype","countryregionid"};
		private static DataTable scheduleTable;
		private static DataTable searchPreferenceTable;
		private static Boolean isRunning = false;

		private string cfgQueuePaths = String.Empty;
		private static String [] queuePaths;
		private static Int32 queuePathSpinIndex = 0;
		private static DateTime lastSetQueuePathTime = DateTime.Now;
		//private static PerformanceCounter[] perfItemsInQueue;
		#endregion

		#region Methods
		/// <summary>
		/// Start loading queue with items.
		/// </summary>
		public void Run()
		{
			isRunning = true;

            RollingFileLogger.Instance.LogInfoMessage(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "ScheduleLoaderBL", "Run starting", null);
			while (isRunning) 
			{
				Boolean queueIsTooFull = false;

				SetQueuePaths();	// In case config changed, this call is cheap.

//				for (Int32 i = 0; i < perfItemsInQueue.Length && !queueIsTooFull; i++)
//				{
//					queueIsTooFull = perfItemsInQueue[i].NextValue() >= MAX_ITEMS_IN_QUEUE;
//					System.Diagnostics.Trace.WriteLine("__" + ServiceConstants.SERVICE_NAME + ": " + perfItemsInQueue[i].InstanceName + " msg count " + perfItemsInQueue[i].NextValue().ToString() );
//				}

				if (queueIsTooFull)
				{
					System.Diagnostics.Trace.WriteLine("__" + ServiceConstants.SERVICE_NAME + ": Snore. Queue too full.");
					Thread.Sleep(20000);	// Sleep because queue has enough items for now.
				}
				else 
				{
					LoadBatchFromDB();

					if ((scheduleTable != null && scheduleTable.Rows.Count == 0) || scheduleTable == null)
					{
						System.Diagnostics.Trace.WriteLine("__" + ServiceConstants.SERVICE_NAME + ": Snore. DB call returned no results");
						Thread.Sleep(10000);	// Sleep because last scoop brought back nothing.
					}
					else 
					{
						ViralMailQueueItems items = ExtractQueueItems();
						DistributeQueueItems(items);
						scheduleTable = null;
						searchPreferenceTable = null;
					}
				}			
			}
		}

		public void Stop()
		{
			isRunning = false;
		}

		/// <summary>
		/// Loads runtime config to determine which queues configured for ViralMail.
		/// </summary>
		private void SetQueuePaths()
		{
			try 
			{
                RollingFileLogger.Instance.LogInfoMessage(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "ScheduleLoaderBL", "SetQueuePath starting", null);

				lastSetQueuePathTime = DateTime.Now;

				String queuePathsVal = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("VIRALMAILSVC_QUEUE_PATH");
				if (queuePathsVal != cfgQueuePaths) 
				{
					cfgQueuePaths = queuePathsVal;
					queuePaths = queuePathsVal.Split(',');

					//perfItemsInQueue = new PerformanceCounter[queuePaths.Length];
					for (Int32 i = 0; i < queuePaths.Length; i++) 
					{
						String instance = queuePaths[i];
						instance = instance.Replace("FormatName:DIRECT=OS:", String.Empty);

						String host = instance.Substring(0, instance.IndexOf("\\"));
						if (host == ".") 
						{ 
							host = System.Environment.MachineName.ToLower(); 
							instance = instance.Replace(".", host);
						}

                        RollingFileLogger.Instance.LogInfoMessage(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "ScheduleLoaderBL", "queue found: " + instance, null);

						//perfItemsInQueue[i] = new PerformanceCounter("MSMQ Queue", "Messages in Queue", instance, host);
						//System.Diagnostics.Trace.WriteLine("__" + ServiceConstants.SERVICE_NAME + " starting perf counter " + instance + " has : " + perfItemsInQueue[i].NextValue().ToString()); 
					}
				}
			}
			catch (Exception ex) 
			{
                RollingFileLogger.Instance.LogException(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "ScheduleLoaderBL", "SetQueuePaths() failed. Can't figure out where to send queue items, or can't connect counter!", ex, null);
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "SetQueuePaths() failed. Can't figure out where to send queue items, or can't connect counter!", ex, false);
			}
			if (queuePaths.Length == 0) 
			{
                RollingFileLogger.Instance.LogInfoMessage(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "ScheduleLoaderBL", "SetQueuePaths() failed. Got 0 queue paths.", null);
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "SetQueuePaths() failed. Got 0 queue paths.", null, false);
			}
		}

		private void LoadBatchFromDB()
		{
			try 
			{
                RollingFileLogger.Instance.LogInfoMessage(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "ScheduleLoaderBL", "LoadBatchFromDB starting", null);

				Command command = new Command("mnAlertWrite", "up_ViralMailSchedule_List_Send", 0);
				DataSet ds = Client.Instance.ExecuteDataSet(command);

				scheduleTable = ds.Tables[0];
				searchPreferenceTable = ds.Tables[1];
				scheduleTable.ChildRelations.Add(
					FK_SCHEDULE_SEARCHPREFERENCES,
					new DataColumn[] {scheduleTable.Columns["MemberID"], scheduleTable.Columns["CommunityID"]}, 
					new DataColumn[] {searchPreferenceTable.Columns["MemberID"], searchPreferenceTable.Columns["GroupID"]}
					);
			}
			catch (Exception ex) 
			{
                RollingFileLogger.Instance.LogException(Matchnet.ViralMail.ValueObjects.ServiceConstants.SERVICE_CONSTANT, "ScheduleLoaderBL", "LoadBatchFromDB error", ex, null);
				new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "LoadBatchFromDB() failed.", ex, false);

			}
		}

		private ViralMailQueueItems ExtractQueueItems()
		{
			ViralMailQueueItems vmQueueItems = new ViralMailQueueItems();
			ViralMailQueueItem vmQueueItem; 
			if (scheduleTable != null) 
			{
				for (int i = 0; i < scheduleTable.Rows.Count; i++)
				{
					vmQueueItem = GetQueueItem(scheduleTable.Rows[i]);
					vmQueueItems.Add(vmQueueItem);
				}
			}

			return vmQueueItems;
		}

		public ViralMailQueueItem GetQueueItem(DataRow row)
		{
			SearchPreferenceCollection searchPreferences;
			String[] sentMemberIDList = new String[0];
					
			if (row["SentMemberIDList"] != DBNull.Value) 
				sentMemberIDList = row["SentMemberIDList"].ToString().Split(new char[]{','});
			
			searchPreferences = GetSearchPreferences(row);

			ViralMailQueueItem queueItem = new ViralMailQueueItem(
				Convert.ToInt32(row["MemberID"]),
				Convert.ToInt32(row["CommunityID"]),
				sentMemberIDList,
				Convert.ToDateTime(row["LastAttemptDate"]),
				Convert.ToDateTime(row["NextAttemptDate"]), 
				searchPreferences);
	
			return queueItem;
		}

		private SearchPreferenceCollection GetSearchPreferences(DataRow row)
		{
			SearchPreferenceCollection searchPrefs = new SearchPreferenceCollection();
			DataRow[] searhPrefRows = row.GetChildRows(FK_SCHEDULE_SEARCHPREFERENCES);

			if (searhPrefRows.Length == 1) // there should be only one perf per group per member
			{
				foreach (String columnName in searchPreferenceColumnNameList)
				{
					if (searhPrefRows[0][columnName] != DBNull.Value && searhPrefRows[0][columnName].ToString() != Constants.NULL_INT.ToString()) 
					{
						try
						{
							searchPrefs.Add(new SearchPreference(columnName, searhPrefRows[0][columnName].ToString() ));
						}
						catch
						{
							searchPrefs.Add(new SearchPreference(columnName, String.Empty));
						}
					}
				
				}
			}

			return searchPrefs;
		}

		public void DistributeQueueItems(ViralMailQueueItems items)
		{	
			ViralMailQueueItem item; 
			Boolean isSuccess = false;

			for (Int32 i = 0;  i < items.Count; i++)
			{
				item = items.Item(i);

				for (Int32 attempt = 0; attempt < 3; attempt++)
				{
					isSuccess = EnqueueViralMailItem(item);

					if (isSuccess)
						break;

					Thread.Sleep(10000);
				}

				if (!isSuccess) 
				{
					new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Can't enqueue item after repeated attempts. Giving up!", null, false);
					new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Last Batch: " + items.Count.ToString() + " total, " + i.ToString() + " sent," + (items.Count - i).ToString() + " abandoned." , null, false);
					isRunning = false;
				}
			}
		}

		public bool EnqueueViralMailItem(ViralMailQueueItem item)
		{
			MessageQueue mq;
			
			try 
			{
				mq = new MessageQueue(GetNextQueuePath());
				mq.Formatter = new BinaryMessageFormatter();

				using (MessageQueueTransaction tran = new MessageQueueTransaction())
				{
					tran.Begin();
					mq.Send(item,item.MemberID.ToString() + ":" + item.CommunityID.ToString(),tran);
					tran.Commit();
				}

				return true;
			}
			catch (Exception ex) 
			{
				new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, 
					"EnqueueViralMailItem failed " +  item.MemberID.ToString() + ":" + item.CommunityID.ToString(),
					ex, false);

				return false;
			}			
		}

		/// <summary>
		/// Gets next queue path in round robin fashion.
		/// </summary>
		private string GetNextQueuePath()
		{
			if (DateTime.Now.Subtract(lastSetQueuePathTime).TotalSeconds > 20)
			{
				SetQueuePaths();
			}

			queuePathSpinIndex = ++queuePathSpinIndex % queuePaths.Length;
			return queuePaths[queuePathSpinIndex]; 
		}
		#endregion

		#region Properties
		public bool Runnable
		{
			get
			{
				return isRunning;
			}
			set
			{
				isRunning = value;
			}
		}
		#endregion
	}
}
