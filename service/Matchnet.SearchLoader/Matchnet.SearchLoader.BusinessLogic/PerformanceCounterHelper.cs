﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Matchnet.SearchLoader.BusinessLogic
{
    public class PerformanceCounterHelper
    {
        private DateTime _Start;
        public PerformanceCounterHelper()
        {
            StartTiming();
        }

        public void StartTiming()
        {
            _Start = DateTime.Now;
        }

        public void EndTiming(PerformanceCounter averageCounter, PerformanceCounter averageCounterBase)
        {
            averageCounter.IncrementBy(Convert.ToInt64(DateTime.Now.Subtract(_Start).TotalMilliseconds));
            averageCounterBase.Increment();
        }
    }
}
