﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.SearchLoader.BusinessLogic
{
    public class ServiceConstants
    {
        private ServiceConstants()
        { }

        /// <summary>
        /// Service name (Matchnet.Search.Service)
        /// </summary>
        public const string SERVICE_NAME = "Matchnet.SearchLoader.Service";

        /// <summary>
        /// Service Constant (SEARCH_SVC)
        /// </summary>
        public const string SERVICE_CONSTANT = "SEARCHLOADER_SVC";
    }
}
