﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using System.Diagnostics;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.SearchLoader.ReverseSearch;
using Matchnet.SearchLoader.ReverseSearch.Core;
using Matchnet.SearchLoader.ReverseSearch.Enumerations;
using Matchnet.SearchLoader.ReverseSearch.Data;
using Matchnet.SearchLoader.ReverseSearch.Exceptions;

namespace Matchnet.SearchLoader.BusinessLogic
{
    public sealed class ReverseSearchPumpBL : IDisposable
    {


        #region Fields

        private bool runnable;
        private Thread pumpThread;
        private Int32 sleepTime;
        private Int32 partitionsStarted;
        private Int32 partitionsCompleted;
        private Queue<Parameter> parameters;
        private LineRecorder recorder;
        private string cfgLineRecorderFilePath;
        private string cfgDataSourceConnectionString;
        private int cfgPollingInterval;
        private Int32 cfgCommitSleepTime;
        private bool cfgUseThreadedPump;
        private string cfgSolrURLs;
        private SearchDataSource datasource;
        private List<SearchDataAdapter> updaters;


        private PerformanceCounter perfTotalUpdates;
        private PerformanceCounter perfUpdateRate;

        private Thread committer;

        private Boolean shouldCommit;

        #endregion Fields

        private void HandleCommits()
        {
            while (true)
            {
                if (shouldCommit) 
                {
                    foreach (SearchDataAdapter updater in updaters)
                    {
                        try
                        {
                            updater.CommitChanges();
                        }
                        catch (Exception ex)
                        {
                            new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error in commit.", ex);
                        }
                    }
                    shouldCommit = false;
                }

                Thread.Sleep(cfgCommitSleepTime);
            }
        }

        public readonly static ReverseSearchPumpBL Instance = new ReverseSearchPumpBL();

        private ReverseSearchPumpBL()
        {

        }
        
        public void DeleteMember(int communityID, int memberID)
        {
            if (!cfgUseThreadedPump)
            {
                foreach (string url in cfgSolrURLs.Split(';'))
                {
                    SearchDataAdapter.Delete(ReverseSearchDocument.GetId(communityID, memberID), url);
                }
            }
        }

        public void Start()
        {
            runnable = true;
            InitializePerfCounters();

            cfgLineRecorderFilePath = RuntimeSettings.GetSetting("REVERSE_SEARCH_PUMP_LINE_RECORDER_FILEPATH");
            cfgDataSourceConnectionString = RuntimeSettings.GetSetting("REVERSE_SEARCH_PUMP_DATA_CONNECTION_STRING");
            cfgPollingInterval = Conversion.CInt(RuntimeSettings.GetSetting("REVERSE_SEARCH_PUMP_POLLING_INTERVAL"));
            sleepTime = Conversion.CInt(RuntimeSettings.GetSetting("REVERSE_SEARCH_PUMP_THROTTLE_INTERVAL"));
            cfgCommitSleepTime = Conversion.CInt(RuntimeSettings.GetSetting("REVERSE_SEARCH_PUMP_COMMIT_INTERVAL"));
            cfgUseThreadedPump = Conversion.CBool(RuntimeSettings.GetSetting("REVERSE_SEARCH_PUMP_USE_THREADED_PUMP"));
            cfgSolrURLs = RuntimeSettings.GetSetting("REVERSE_SEARCH_PUMP_DATA_SOLR_URL");
            parameters = new Queue<Parameter>();
            recorder = new LineRecorder(cfgLineRecorderFilePath);
            datasource = new SearchDataSource(cfgDataSourceConnectionString);
            updaters = new List<SearchDataAdapter>();

            foreach (string url in cfgSolrURLs.Split(';'))
            {
                updaters.Add(new SearchDataAdapter(url));
            }

            LoadParameters();

            committer = new Thread(HandleCommits);
            committer.Start();

            pumpThread = new Thread(Pump);
            pumpThread.Start();
        }

        public void Stop()
        {
            runnable = false;

            if (pumpThread != null) pumpThread.Join(sleepTime * 2);

            try
            {
                recorder.Write();
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error writing final state to file (while service is stopping)", ex);
            }
        }

        public void Pump()
        {

            while (runnable)
            {
                try
                {
                    if (partitionsCompleted == partitionsStarted)
                    {
                        try
                        {
                            recorder.Write();
                        }
                        catch (Exception ex)
                        {
                            new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error writing progress to file", ex);
                        }

                        Thread.Sleep(cfgPollingInterval);
                        LoadParameters();
                    }

                    while (runnable && parameters.Count > 0)
                    {

                        if (cfgUseThreadedPump)
                        {
                            Parameter p = parameters.Dequeue();
                            if (p != null)
                            {
                                ThreadPool.QueueUserWorkItem(state => PumpParameter(state as Parameter), p);
                            }
                        }
                        else
                        {
                            PumpParameter(parameters.Dequeue());
                        }
                    }

                    Thread.Sleep(sleepTime);
                }
                catch (Exception ex)
                {
                    new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "general error in main loop", ex);
                }
            }
        }

        private void LoadParameters()
        {
            partitionsCompleted = 0;
            partitionsStarted = 0;

            try
            {
                using (var fs = new FileStream(cfgLineRecorderFilePath, FileMode.Open, FileAccess.Read))
                using (var sr = new StreamReader(fs))
                {
                    while (sr.EndOfStream == false)
                    {
                        var pieces = sr.ReadLine().Split('-');
                        if (pieces.Length < 2) continue;
                        int lineID = Conversion.CInt(pieces[2]);
                        if (lineID <= 0) lineID = 1;
                        Parameter param = Parameter.Create((Community)Conversion.CInt(pieces[0]), Conversion.CInt(pieces[1]), lineID, 100, -1);
                        parameters.Enqueue(param);
                        recorder.Record(param.SelectedCommunity, param.Partition, lineID);
                    }

                    partitionsStarted = parameters.Count;
                }
            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error in loading parameters from file :" + cfgLineRecorderFilePath, ex);
            }

        }

        private void PumpParameter(Parameter parameter)
        {
            var rowCount = 0;
            var lineId = 1;

            if (runnable)
            {
                try
                {
                    datasource.Get(
                         parameter,
                         reader =>
                         {
                             int lineOrdinal = 0;
                             int memberIDOrdinal = 0;
                             int communityIDOrdinal = 0;
                             int brandLastLogonOrdinal = 0;

                             try
                             {
                                 lineOrdinal = reader.GetOrdinal("LineID");
                                 memberIDOrdinal = reader.GetOrdinal("MemberID");
                                 communityIDOrdinal = reader.GetOrdinal("CommunityID");
                                 brandLastLogonOrdinal = reader.GetOrdinal("BrandLastLogonDate");
                             }
                             catch
                             {
                                 throw new Exception("No lineID in result set.");
                             }

                             while (reader.Read())
                             {
                                 rowCount++;
                                 lineId = reader.GetInt32(lineOrdinal);

                                 if ((Int32)reader["SearchFlag"] == 1 && !(reader.IsDBNull(brandLastLogonOrdinal)))
                                 {
                                     var lastLogin = reader.GetDateTime(brandLastLogonOrdinal);
                                     if (DateTime.UtcNow.Subtract(lastLogin).TotalDays > 120) continue;

                                     foreach (SearchDataAdapter adapter in updaters)
                                     {
                                         adapter.Put(ReverseSearchDocument.Construct(reader), false);
                                     }

                                     perfTotalUpdates.Increment();
                                     perfUpdateRate.Increment();
                                 }
                                 else
                                 {
                                     if (lineId != parameter.StartingRecord && !cfgUseThreadedPump)
                                     {
                                         foreach (string url in cfgSolrURLs.Split(';'))
                                         {                                             SearchDataAdapter.Delete(ReverseSearchDocument.GetId(reader.GetInt32(communityIDOrdinal), reader.GetInt32(memberIDOrdinal)), url);
                                         }

                                     }
                                 }
                             }

                             if (rowCount > 0) shouldCommit = true;

                             HandleParameterCleanup(rowCount, parameter, lineId);
                         }
                        );

                    recorder.Record(parameter.SelectedCommunity, parameter.Partition, lineId);

                }
                catch (DataSourceTimeoutException dataSourceTimeoutException)
                {
                    HandleDataException(parameter, dataSourceTimeoutException);
                }
                catch (Exception ex)
                {
                    new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, String.Format("Error in pumping: Community-{0} Partition-{1} LineID-{2}", parameter.SelectedCommunity.ToString("d"), parameter.Partition.ToString(), parameter.StartingRecord.ToString()), ex);
                    //don't requeue the parameter
                    HandleParameterCleanup(-1, parameter, 0);
                }
            }
        }

        private void HandleDataException(Parameter parameter, DataSourceTimeoutException dataSourceTimeoutException)
        {
            var error = string.Format(
                 "{0}-{1}-{2} Took too long to process. Time: {3} ms",
                 parameter.SelectedCommunity,
                 parameter.Partition,
                 parameter.StartingRecord,
                 dataSourceTimeoutException.ElapsedTime
                 );

            new ServiceBoundaryException(
                ServiceConstants.SERVICE_NAME,
                String.Format(
                     "{0}-{1}-{2} Took too long to process. Time: {3} ms",
                     parameter.SelectedCommunity,
                     parameter.Partition,
                     parameter.StartingRecord,
                     dataSourceTimeoutException.ElapsedTime
                 ),
                 dataSourceTimeoutException);

            //don't requeue the parameter
            HandleParameterCleanup(-1, parameter, 0);
        }

        private void HandleParameterCleanup(Int32 rowCount, Parameter parameter, Int32 lineId)
        {
            bool endOfRecords = (rowCount == 1 && lineId == parameter.StartingRecord) ? true : false;

            if (!endOfRecords && rowCount >= parameter.RecordsCount)
            {
                parameter.StartingRecord = lineId;
                parameters.Enqueue(parameter);
            }
            else
            {
                Interlocked.Increment(ref partitionsCompleted);
            }
        }

        #region instrumentation

        public static void PerfCounterInstall()
        {
            var ccdc = new CounterCreationDataCollection();
            ccdc.AddRange(new[] {	
				new CounterCreationData("Total Updates", "Running total number of updates", PerformanceCounterType.NumberOfItems32),						 
                new CounterCreationData("Updates/second", "Updates sent to SOLR/second", PerformanceCounterType.RateOfCountsPerSecond32),
													 });

            PerformanceCounterCategory.Create(ServiceConstants.SERVICE_NAME, ServiceConstants.SERVICE_NAME, ccdc);
        }


        public static void PerfCounterUninstall()
        {
            if (PerformanceCounterCategory.Exists(ServiceConstants.SERVICE_NAME))
            {
                PerformanceCounterCategory.Delete(ServiceConstants.SERVICE_NAME);
            }
        }

        private void InitializePerfCounters()
        {
            perfTotalUpdates = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Total Updates", false);
            perfUpdateRate = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Updates/second", false);
            ResetPerfCounters();
        }

        private void ResetPerfCounters()
        {
            perfTotalUpdates.RawValue = 0;
            perfUpdateRate.RawValue = 0;
        }

        #endregion

        #region IDisposable Implementation

        ~ReverseSearchPumpBL()
        {
            Dispose(false);
        }

        private Boolean disposed;

        public void Dispose(Boolean disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (committer == null) return;

                    try
                    {
                        committer.Abort();
                    }
                    catch (ThreadAbortException)
                    {
                    }
                }
            }

            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable Implementation
    }
}
