using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.SearchLoader.ReverseSearch.Test
{
	[TestClass]
	public sealed class SearchDataAdapterTest
	{
		private const String QueryHot = "hot";

		private readonly String[] seeking = new[]
   	{
   		"a bunch of hot chicks", 
   		"a bunch of hot dudes", 
   		"girls who like boys who do boys like they're girls, who do girls like they're boys",
   	};

		private readonly String[] about = new[]
    	{
    		"i'm a guy who's in search of some fun parties", 
    		"i'm a girl, and i want romance and excitement.", 
    		"i'm looking for weird boys or girls or any combination thereof.",
    	};

		[TestMethod]
		public void RetrieveWorks()
		{
			//Array.ForEach( 
			//   Enumerable.Range(0,3).ToArray(),
			//   i => SearchDataAdapter.Put(
			//      new ReverseSearchDocument { About = about[i], Seeking = seeking[i], MemberId = i }
			//   )
			//);

			//var records = SearchDataAdapter.Get(QueryHot);

			//Console.WriteLine("# of records: {0}", records.Rows);

			//Array.ForEach(records.SearchRecords, 
			//   record => Console.WriteLine( "member id: {0}, about: {1}", record.MemberId, record.About));
		}
	}
}