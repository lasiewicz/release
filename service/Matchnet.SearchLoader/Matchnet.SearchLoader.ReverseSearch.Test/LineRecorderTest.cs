using System;
using System.IO;
using Matchnet.SearchLoader.ReverseSearch.Enumerations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.SearchLoader.ReverseSearch.Test
{
	[TestClass]
	public class LineRecorderTest
	{
		private const String RecordPath = "record.xml";

		private const Community Community1 = Community.JDate;

		private const Community Community2 = Community.AmericanSingles;

		private const Int32 Partition = 10;

		private const Int32 Partition2 = 20;

		private const Int32 LineId = 50;

		private const Int32 LineId2 = 100;

		private const Int32 LineId3 = 10000;

		[TestMethod]
		public void WriteWorks()
		{
			try
			{
				var lineRecord = new LineRecorder(RecordPath);

				lineRecord.Record(Community1, Partition, LineId);
				lineRecord.Record(Community1, Partition2, LineId2);
				lineRecord.Record(Community2, Partition2, LineId2);
			}
			catch(Exception exc)
			{
				Assert.Fail(exc.Message + Environment.NewLine + exc.StackTrace);
			}
			finally
			{
				File.Delete(RecordPath);
			}
		}

		[TestMethod]
		public void UpdateWorks()
		{
			try
			{
				var lineRecord = new LineRecorder(RecordPath);

				lineRecord.Record(Community1, Partition, LineId);
				lineRecord.Record(Community1, Partition2, LineId2);
				lineRecord.Record(Community2, Partition2, LineId3);
				lineRecord.Record(Community2, Partition, LineId3);	// The one we'll test against
				lineRecord.Record(Community2, Partition2, LineId);
			}
			catch (Exception exc)
			{
				Assert.Fail(exc.Message + Environment.NewLine + exc.StackTrace);
			}
			finally
			{
				File.Delete(RecordPath);
			}
		}
	}
}