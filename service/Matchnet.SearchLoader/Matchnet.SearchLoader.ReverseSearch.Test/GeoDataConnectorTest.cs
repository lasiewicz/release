using System;
using Matchnet.SearchLoader.ReverseSearch.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.SearchLoader.ReverseSearch.Test
{
	[TestClass]
	public class GeoDataConnectorTest
	{
		private const Int32 VanNuys = 3405347;

		private const Int32 WoodlandHills = 3407105;

		private const Int32 Radius = 40;

		[TestMethod]
		public void GetBoxForRegionWorks()
		{
			var coordinates = GeoDataConnector.GetCoordinates(VanNuys).Convert();

			var cb2 = GeoDataConnector.GetBoxCoordinates(WoodlandHills, Radius).Convert();

			Console.WriteLine();
		}
	}
}