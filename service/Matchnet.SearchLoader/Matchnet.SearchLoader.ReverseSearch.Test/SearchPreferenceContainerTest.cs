﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.SearchLoader.ReverseSearch.Test
{
	[TestClass]
	public class SearchPreferenceContainerTest
	{
		private const String SearchPreferencesPath = @"C:\data\searchprefs";

		private const Int32 MemberId = 114609155;

		[TestMethod]
		public void PreferencesWorks()
		{
			var container = new SearchPreferenceContainer(SearchPreferencesPath, true);

			var preferences = container.Preferences;

			Assert.IsNotNull(preferences);

			var memberPrefs = container.GetPreference(MemberId);

			Assert.IsNotNull(memberPrefs);

			Assert.AreEqual(MemberId, memberPrefs.MemberId);
		}

		[TestMethod]
		public void GetMaxMinAges()
		{
			var minAges = new Dictionary<Int32, Int32>();
			var maxAges = new Dictionary<Int32, Int32>();

			var container = new SearchPreferenceContainer(SearchPreferencesPath, true);

			Array.ForEach(container.Preferences.ToArray(),
			              preferenceContainer => Array.ForEach(
			                                     	preferenceContainer.Keys.ToArray(),
			                                     	key =>
			                                     		{
			                                     			var preference = preferenceContainer[key];

			                                     			if (minAges.ContainsKey(preference.MinimumAge) == false)
			                                     			{
			                                     				minAges.Add(preference.MinimumAge, 1);
			                                     			}
			                                     			else
			                                     			{
			                                     				minAges[preference.MinimumAge]++;
			                                     			}

			                                     			if (maxAges.ContainsKey(preference.MaximumAge) == false)
			                                     			{
			                                     				maxAges.Add(preference.MaximumAge, 1);
			                                     			}
			                                     			else
			                                     			{
			                                     				maxAges[preference.MaximumAge]++;
			                                     			}
			                                     		}
			                                     	)
				);

			Console.WriteLine("Minimum ages:");
			Array.ForEach(minAges.Keys.ToArray(),
			              key => Console.WriteLine("age: {0}, count: {1}", key, minAges[key]));

			Console.WriteLine("Maximum ages:");
			Array.ForEach(maxAges.Keys.ToArray(),
			              key => Console.WriteLine("age: {0}, count: {1}", key, maxAges[key]));
		}
	}
}