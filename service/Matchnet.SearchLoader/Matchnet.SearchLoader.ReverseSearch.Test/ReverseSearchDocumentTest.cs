using System;
using Matchnet.SearchLoader.ReverseSearch.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.SearchLoader.ReverseSearch.Test
{
	/// <summary>
	/// Summary description for DocumentTest
	/// </summary>
	[TestClass]
	public class ReverseSearchDocumentTest
	{
		public const String TestText = "test text";

		public const String AboutPropertyName = "about";

		[TestMethod]
		public void IndexerWorks()
		{
			var document = new ReverseSearchDocument { MemberId = 3,};

			Assert.AreEqual(3, document.MemberId);
		}
	}
}