using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Matchnet.SearchLoader.ReverseSearch.Data;
using Matchnet.SearchLoader.ReverseSearch.Enumerations;
using Matchnet.SearchLoader.ReverseSearch.Pump;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using org.apache.solr.SolrSharp.Configuration;
using org.apache.solr.SolrSharp.Update;

namespace Matchnet.SearchLoader.ReverseSearch.Test.Pump
{
	[TestClass]
	public class ProductionPumpTest
	{
		private const Community TestCommunity = Community.JDate;

		private const String TestRecordPath = @"c:\temp\record.log";

		[TestMethod]
		public void Optimize()
		{
			var solr = SolrSearchers.GetSearcher(Mode.Read);
			var updater = new SolrUpdater(solr);
			updater.Optimize();
			Console.WriteLine("optimized");
		}

		[TestMethod]
		public void PumpOneRoundOfData()
		{
			/*new ProductionPump(
				new[]
					{
						new Parameter
							{
								SelectedCommunity = TestCommunity,
								Partition = 1,
								RecordsCount = 10,
								SearchFlag = 1,
								StartingRecord = 600000,
							},
					},
				false
				) 
				{
					RecordPath = TestRecordPath
				}.PumpData(false);*/
		}

		[TestMethod]
		public void PumpDataFromFileRecords()
		{
			/*var parameters = new List<Parameter>();

			using (var fs = new FileStream(TestRecordPath, FileMode.Open, FileAccess.Read))
			using( var sr = new StreamReader(fs))
			{
				while (sr.EndOfStream == false)
				{
					var pieces = sr.ReadLine().Split('-');
					if (pieces.Length < 1) continue;

					parameters.Add( 
						new Parameter
							{
								Partition = Int32.Parse(pieces[1]), 
								RecordsCount = Constants.Data.DefaultRows,
								StartingRecord = Int32.Parse(pieces[2]),
								SelectedCommunity = TestCommunity,
								SearchFlag = 1,
							}
						);
				}
			}

			new ProductionPump(
				parameters,
				false
				) { 
				  	RecordPath = TestRecordPath
				  }.PumpData(false);*/
		}

		[TestMethod]
		public void PumpAllCommunityData()
		{
			/*new ProductionPump(
				Parameter.CreateMany(TestCommunity),
				true
				).PumpData(true);*/
		}

		[TestMethod]
		public void DataWasWritten()
		{
			var counter = 0;
			using( var sr = new StreamReader( @"c:\data\datalog1.txt", Encoding.Unicode))
			{
				while( sr.EndOfStream == false && counter++ < 10)
				{
					Console.WriteLine(sr.ReadLine());
				}
			}
		}
	}
}