using Matchnet.SearchLoader.ReverseSearch.Pump;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.SearchLoader.ReverseSearch.Test.Pump
{
	[TestClass]
	public class DevPumpTest
	{
		[TestMethod]
		public void PumpWorks()
		{
			var pump = new DevPump();
			pump.PumpData();
		}
	}
}