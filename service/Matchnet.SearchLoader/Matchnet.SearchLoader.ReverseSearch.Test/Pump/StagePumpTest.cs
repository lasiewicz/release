using Matchnet.SearchLoader.ReverseSearch.Pump;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.SearchLoader.ReverseSearch.Test.Pump
{
	[TestClass]
	public class StagePumpTest
	{
		[TestMethod]
		public void PumpWorks()
		{
			var pump = new StagePump();
			pump.PumpData();
		}
	}
}