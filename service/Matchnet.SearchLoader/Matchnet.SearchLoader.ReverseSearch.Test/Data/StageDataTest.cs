using System;
using Matchnet.SearchLoader.ReverseSearch.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.SearchLoader.ReverseSearch.Test.Data
{
	[TestClass]
	public class StageDataTest
	{
		[TestMethod]
		public void GetGenderMasksWorks()
		{
			var data = new StageData();

			var genderMasks = data.GetGenderMasks(1);

			Assert.IsNotNull(genderMasks);

			Assert.IsTrue(genderMasks.Count > 0);

			Console.WriteLine("count: {0}", genderMasks.Count);
		}

		[TestMethod]
		public void GetRegionsWorks()
		{
			var data = new StageData();

			var regions = data.GetRegions(1);

			Assert.IsNotNull(regions);

			Assert.IsTrue(regions.Count > 0);

			Console.WriteLine("count: {0}", regions.Count);
		}

		[TestMethod]
		public void GetLastLoginsWorks()
		{
			var data = new StageData();

			var lastLogins = data.GetLastLogins(1);

			Assert.IsNotNull(lastLogins);

			Assert.IsTrue(lastLogins.Count > 0);

			Console.WriteLine("count: {0}", lastLogins.Count);
		}

		[TestMethod]
		public void GetSignupWorks()
		{
			var data = new StageData();

			var signups = data.GetSignups(1);

			Assert.IsNotNull(signups);

			Assert.IsTrue(signups.Count > 0);

			Console.WriteLine("count: {0}", signups.Count);
		}

		[TestMethod]
		public void GetPreferencesWorks()
		{
			var data = new StageData();

			var preferences = data.GetPreferences(1);

			Assert.IsNotNull(preferences);

			Assert.IsTrue(preferences.Count > 0);

			Console.WriteLine("count: {0}", preferences.Count);
		}

		[TestMethod]
		public void GetMembersWithPhotosWorks()
		{
			var data = new StageData();

			var membersWithPhotos = data.GetMembersWithPhotos(1);

			Assert.IsNotNull(membersWithPhotos);

			Assert.IsTrue(membersWithPhotos.Count > 0);

			Console.WriteLine("count: {0}", membersWithPhotos.Count);
		}
	}
}