using System;
using Matchnet.SearchLoader.ReverseSearch.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.SearchLoader.ReverseSearch.Test.Data
{
	[TestClass]
	public class DevDataTest
	{
		[TestMethod]
		public void GetGenderMasksWorks()
		{
			var devData = new DevData();

			var genderMasks = devData.GetGenderMasks();

			Assert.IsNotNull(genderMasks);

			Assert.IsTrue(genderMasks.Count > 0);

			Console.WriteLine( "count: {0}", genderMasks.Count );
		}

		[TestMethod]
		public void GetRegionsWorks()
		{
			var devData = new DevData();

			var regions = devData.GetRegions();

			Assert.IsNotNull(regions);

			Assert.IsTrue(regions.Count > 0);

			Console.WriteLine("count: {0}", regions.Count);
		}

		[TestMethod]
		public void GetLastLoginsWorks()
		{
			var devData = new DevData();

			var lastLogins = devData.GetLastLogins();

			Assert.IsNotNull(lastLogins);

			Assert.IsTrue(lastLogins.Count > 0);

			Console.WriteLine("count: {0}", lastLogins.Count);
		}

		[TestMethod]
		public void GetSignupWorks()
		{
			var devData = new DevData();

			var signups = devData.GetSignups();

			Assert.IsNotNull(signups);

			Assert.IsTrue(signups.Count > 0);

			Console.WriteLine("count: {0}", signups.Count);
		}

		[TestMethod]
		public void GetPreferencesWorks()
		{
			var devData = new DevData();

			var preferences = devData.GetPreferences();

			Assert.IsNotNull(preferences);

			Assert.IsTrue(preferences.Count > 0);

			Console.WriteLine("count: {0}", preferences.Count);
		}

		[TestMethod]
		public void GetMembersWithPhotosWorks()
		{
			var devData = new DevData();

			var membersWithPhotos = devData.GetMembersWithPhotos();

			Assert.IsNotNull(membersWithPhotos);

			Assert.IsTrue(membersWithPhotos.Count > 0);

			Console.WriteLine("count: {0}", membersWithPhotos.Count);
		}
	}
}