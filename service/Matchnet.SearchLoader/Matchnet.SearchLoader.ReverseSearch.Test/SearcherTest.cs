using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Matchnet.SearchLoader.ReverseSearch;
using Matchnet.SearchLoader.ReverseSearch.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.SearchLoader.ReverseSearch.Test
{
	[TestClass]
	public class SearcherTest
	{
		private const String DataDirectory = @"c:\data\";

		private static String CreateExtendedFileName(String fileName)
		{
			return String.Join(
				String.Empty,
				new[] 
					{
						DataDirectory,
						fileName,
						"-",
						DateTime.Now.ToString("yy-MM-dd-hhmm"),
						".txt"
					}
				);
		}

		private Int32 activityLevelValueIncrementer = 0;

		private readonly Dictionary<String, Int32> activityLevels = new Dictionary<String, Int32>();
		private readonly Dictionary<Int32, String> activityLookup = new Dictionary<Int32, String>();
		private readonly Dictionary<Int32, Int32> memberActivityLevel = new Dictionary<Int32, Int32>();

		private Int32 resultsCounter;

		private Int32 numberOfLines;

		private Dictionary<ResultsParser, TextWriter> testRuns;

		private Stopwatch timer;

		private Thread timerThread;

		private Queue<String> lineQueue;

		private void GetResults(Object line)
		{
			Array.ForEach(
				testRuns.ToArray(),
				testRunPair => 
				testRunPair.Value.WriteLine(
					testRunPair.Key.GetFormattedResults((String)line, memberActivityLevel, activityLookup)
					)
				);

			resultsCounter++;
		}

		private void CheckTime()
		{
			while (true)
			{
				Thread.Sleep(10000);

				var timeInSeconds = timer.ElapsedMilliseconds / (Double)1000;

				var total = 4 * resultsCounter;

				Console.WriteLine("processed {0} in {1} s. Rate: {2}/s.",
				                  total,
				                  timeInSeconds,
				                  total / timeInSeconds
					);
			}
		}

		[TestMethod]
		public void RunThreadedTest()
		{
			const Int32 numberOfLinesToRead = 250000;

			lineQueue = new Queue<String>(numberOfLinesToRead);

			testRuns = new Dictionary<ResultsParser, TextWriter>
			           	{
			           		{new Test5(), TextWriter.Synchronized(new StreamWriter(CreateExtendedFileName("test5")))},
			           		{new Test5WithPhotos(), TextWriter.Synchronized(new StreamWriter(CreateExtendedFileName("test5wphotos")))},

			           		{new Test3(), TextWriter.Synchronized(new StreamWriter(CreateExtendedFileName("test3")))},
			           		{new Test3WithPhotos(), TextWriter.Synchronized(new StreamWriter(CreateExtendedFileName("test3wphotos")))},
			           	};

			using (var fs = new FileStream(@"c:\data\profiles.txt", FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
			using (var sr = new StreamReader(fs))
			{
				while (sr.EndOfStream == false)
				{
					lineQueue.Enqueue(sr.ReadLine());
				}
			}

			try
			{
				resultsCounter = 0;

				timer = new Stopwatch();

				timerThread = new Thread(CheckTime);

				timer.Start();
				timerThread.Start();

				while (lineQueue.Count > 0)
				{
					ThreadPool.QueueUserWorkItem(GetResults, lineQueue.Dequeue());
					numberOfLines++;
				}

				while (resultsCounter < numberOfLines)
				{
					Thread.Sleep(250);
				}
			}
			finally
			{
				timerThread.Abort();
			}
		}

		[TestMethod]
		public void RunNonThreadedTest()
		{
			const Int32 numberOfLinesToRead = 10;

			lineQueue = new Queue<String>(numberOfLinesToRead);

			testRuns = new Dictionary<ResultsParser, TextWriter>
			           	{
			           		//{new Test5(), null},
			           		//{new Test5WithPhotos(), null},

			           		{new Test3(), null},
			           		{new Test3WithPhotos(), null},
			           	};

			var linesRead = 0;

			using (var fs = new FileStream(@"c:\data\profiles.txt", FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
			using (var sr = new StreamReader(fs))
			{
				while (sr.EndOfStream == false && linesRead++ < numberOfLinesToRead)
				{
					lineQueue.Enqueue(sr.ReadLine());
				}
			}

			resultsCounter = 0;

			timer = new Stopwatch();

			timer.Start();

			while (lineQueue.Count > 0)
			{
				GetResults(lineQueue.Dequeue());
				numberOfLines++;
			}

			timer.Stop();

			Console.WriteLine("{0} lines in {1} s.", numberOfLines, timer.ElapsedMilliseconds / 1000.000);

			while (resultsCounter < numberOfLines)
			{
				Thread.Sleep(250);
			}
		}

		[TestMethod]
		public void PrintOutActivityLevelResults()
		{
			PopulateActivity();

			var counter = 0;

			timer = new Stopwatch();

			testRuns = new Dictionary<ResultsParser, TextWriter>
			           	{
			           		{new Test5(), new StreamWriter( CreateExtendedFileName( "test5" ))},
			           	};

			try
			{
				using (var fs = new FileStream(@"c:\data\profiles.txt", FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
				using (var sr = new StreamReader(fs))
				{
					timer.Start();

					while (sr.EndOfStream == false)
					{

						try
						{
							var line = sr.ReadLine();

							Array.ForEach(
								testRuns.ToArray(),
								testRunPair => testRunPair.Value.WriteLine(testRunPair.Key.GetFormattedResults(line,
								                                                                               memberActivityLevel, activityLookup))
								);
						}
						catch (Exception exc)
						{
							Console.WriteLine(exc.Message + Environment.NewLine + exc.StackTrace);
						}
						finally
						{
							if (++counter % 1000 == 0)
							{
								timer.Stop();
								Console.WriteLine("processed {0} in {1} ms. Rate: {2}/s.",
								                  counter,
								                  timer.ElapsedMilliseconds,
								                  1000000 / (Double)timer.ElapsedMilliseconds
									);
								timer.Reset();
								timer.Start();
							}
						}
					}
				}
			}
			finally
			{
				Array.ForEach(
					testRuns.Values.ToArray(),
					sw => { if (sw != null) sw.Dispose(); }
					);
			}
		}

		private void PopulateActivity()
		{
			using (var fs = new FileStream(
				@"C:\projects\Spark.Search.Utility\Spark.Search.Utility.Test\bin\Debug\profiles.txt",
				FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
			using (var sr = new StreamReader(fs))
			{
				while (sr.EndOfStream == false)
				{
					var line = sr.ReadLine();
					if (String.IsNullOrEmpty(line)) continue;

					var parsedLine = line.Split(new[] { ',' });
					if (parsedLine.Length < 18) continue;

					var memberId = Int32.Parse(parsedLine[0]);
					var activityLevel = parsedLine[parsedLine.Length - 1];

					if (activityLevels.ContainsKey(activityLevel) == false)
					{
						activityLevels.Add(activityLevel, ++activityLevelValueIncrementer);
						activityLookup.Add(activityLevelValueIncrementer, activityLevel);
					}

					if (memberActivityLevel.ContainsKey(memberId) == false)
					{
						memberActivityLevel[memberId] = activityLevels[activityLevel];
					}
				}
			}
		}

		[TestMethod]
		public void PrintOutResults()
		{
			var counter = 0;

			var timer = new Stopwatch();

			var testRuns = new Dictionary<ResultsParser, StreamWriter>
			               	{
			               		{new Test1(), new StreamWriter( CreateExtendedFileName( "test1" ))}, 
			               		{new Test2(), new StreamWriter( CreateExtendedFileName( "test2" ))}, 
			               		{new Test3(), new StreamWriter( CreateExtendedFileName( "test3" ))}, 
			               		{new Test4(), new StreamWriter( CreateExtendedFileName( "test4" ))}, 
			               	};

			try
			{
				using (var fs = new FileStream(@"c:\data\profiles.txt", FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
				using (var sr = new StreamReader(fs))
				{
					timer.Start();

					while (sr.EndOfStream == false && counter < 100)
					{

						try
						{
							var line = sr.ReadLine();

							Array.ForEach(
								testRuns.ToArray(),
								testRunPair => testRunPair.Value.WriteLine(testRunPair.Key.GetFormattedResults(line))
								);
						}
						catch (Exception exc)
						{
							Console.WriteLine(exc.Message + Environment.NewLine + exc.StackTrace);
						}
						finally
						{
							if (++counter % 100 == 0)
							{
								timer.Stop();
								Console.WriteLine("processed {0} in {1} ms. Rate: {2}/s.",
								                  counter,
								                  timer.ElapsedMilliseconds,
								                  100000 / (Double)timer.ElapsedMilliseconds
									);
								timer.Reset();
								timer.Start();
							}
						}
					}
				}
			}
			finally
			{
				Array.ForEach(
					testRuns.Values.ToArray(),
					sw => { if (sw != null) sw.Dispose(); }
					);
			}
		}
	}

	class Test1 : ResultsParser
	{
		protected override String CreateUrl(SearchDocument document)
		{
			var queryBuilder = new StringBuilder();

			queryBuilder.Append(Host);

			AddGender(queryBuilder, document);
			AddAge(queryBuilder, document);
			AddLocation(queryBuilder, document);
			AddSmokingStatus(queryBuilder, document);
			AddHaveChildren(queryBuilder, document);
			AddJDateReligion(queryBuilder, document);
			AddMaritalStatus(queryBuilder, document);
			AddEducation(queryBuilder, document);

			return queryBuilder.ToString();
		}
	}

	class Test5WithPhotos : ResultsParser
	{
		protected override String CreateUrl(SearchDocument document)
		{
			var queryBuilder = new StringBuilder();

			queryBuilder.Append(Host);

			AddGender(queryBuilder, document);
			AddAge(queryBuilder, document);
			AddLocation(queryBuilder, document);
			AddSmokingStatus(queryBuilder, document);
			AddHaveChildren(queryBuilder, document);
			AddJDateReligion(queryBuilder, document);
			AddMaritalStatus(queryBuilder, document);
			AddEducation(queryBuilder, document);
			AddActivityLevel(queryBuilder, document);
			AddHasPhotos(queryBuilder, document);

			return queryBuilder.ToString();
		}
	}

	class Test5 : ResultsParser
	{
		protected override String CreateUrl(SearchDocument document)
		{
			var queryBuilder = new StringBuilder();

			queryBuilder.Append(Host);

			AddGender(queryBuilder, document);
			AddAge(queryBuilder, document);
			AddLocation(queryBuilder, document);
			AddSmokingStatus(queryBuilder, document);
			AddHaveChildren(queryBuilder, document);
			AddJDateReligion(queryBuilder, document);
			AddMaritalStatus(queryBuilder, document);
			AddEducation(queryBuilder, document);
			AddActivityLevel(queryBuilder, document);

			return queryBuilder.ToString();
		}
	}

	class Test2 : ResultsParser
	{
		protected override String CreateUrl(SearchDocument document)
		{
			var queryBuilder = new StringBuilder();

			queryBuilder.Append(Host);

			AddGender(queryBuilder, document);
			AddAge(queryBuilder, document);
			AddLocation(queryBuilder, document);
			AddSmokingStatus(queryBuilder, document);
			AddMaritalStatus(queryBuilder, document);
			AddEducation(queryBuilder, document);

			return queryBuilder.ToString();
		}
	}

	class Test3 : ResultsParser
	{
		protected override String CreateUrl(SearchDocument document)
		{
			var queryBuilder = new StringBuilder();

			queryBuilder.Append(Host);

			AddGender(queryBuilder, document);

			AddAgePlusMinus5(queryBuilder, document);

			AddLocation160Miles(queryBuilder, document);

			return queryBuilder.ToString();
		}
	}

	class Test3WithPhotos : ResultsParser
	{
		protected override String CreateUrl(SearchDocument document)
		{
			var queryBuilder = new StringBuilder();

			queryBuilder.Append(Host);

			AddGender(queryBuilder, document);

			AddAgePlusMinus5(queryBuilder, document);

			AddLocation160Miles(queryBuilder, document);

			AddHasPhotos(queryBuilder, document);

			return queryBuilder.ToString();
		}
	}

	class Test4 : ResultsParser
	{
		protected override String CreateUrl(SearchDocument document)
		{
			var queryBuilder = new StringBuilder();

			queryBuilder.Append(Host);

			AddGender(queryBuilder, document);
			AddAge(queryBuilder, document);
			AddLocation(queryBuilder, document);
			AddSmokingStatus(queryBuilder, document);
			AddHaveChildren(queryBuilder, document);
			AddJDateReligion(queryBuilder, document);

			return queryBuilder.ToString();
		}
	}
}