﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.SearchLoader.ReverseSearch.Test
{
	[TestClass]
	public class SearchPreferenceTest
	{
		private const String SearchPreferencesPath = @"C:\data\searchprefs";

		[TestMethod]
		public void LoadWorks()
		{
			var results = SearchPreference.Load(SearchPreferencesPath);

			Assert.IsNotNull(results);
			Assert.AreNotEqual(0, results.Count());
		}
	}
}