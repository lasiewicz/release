using System;
using Matchnet.SearchLoader.ReverseSearch.Enumerations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.SearchLoader.ReverseSearch.Test.Enums
{
	[TestClass]
	public class MaritalStatusTest
	{
		private const Int32 MaxValue = 58;

		[TestMethod]
		public void MaritalStatusWorks()
		{
			var value = ((MaritalStatus) MaxValue).ToString();

			Console.WriteLine(value);
		}
	}
}