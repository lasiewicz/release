using System;
using Matchnet.SearchLoader.ReverseSearch.Enumerations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.SearchLoader.ReverseSearch.Test.Enums
{
	[TestClass]
	public class WantsChildrenTest
	{
		private const Int32 WantsChildrenValue = 6;

		[TestMethod]
		public void WantsChildrenWorks()
		{
			var wantsChildren = ((WantsChildren) WantsChildrenValue).ToString();

			Console.WriteLine(wantsChildren);
		}
	}
}