using System;
using Matchnet.SearchLoader.ReverseSearch.Enumerations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Matchnet.SearchLoader.ReverseSearch.Test.Enums
{
	[TestClass]
	public class LanguageTest
	{
		private const Int32 LanguageId = 245367298;

		[TestMethod]
		public void LanguageFlagsWork()
		{
			var language = (Language) LanguageId;
			Console.WriteLine(language);
		}
	}
}