﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Matchnet.SearchLoader.BusinessLogic;
using Matchnet.SearchLoader.ServiceManagers;

namespace Matchnet.SearchLoader.Service
{
    public partial class SearchLoader : Matchnet.RemotingServices.RemotingServiceBase
    {
        private ReverseSearchPumpSM _reverseSearchPumpSM = null;
                
        public SearchLoader()
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            if (_reverseSearchPumpSM != null)
            {
                _reverseSearchPumpSM.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = ServiceConstants.SERVICE_NAME;
            base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
        }

        #endregion

        static void Main()
        {
            System.ServiceProcess.ServiceBase[] ServicesToRun;
            ServicesToRun = new System.ServiceProcess.ServiceBase[] { new SearchLoader() };
            System.ServiceProcess.ServiceBase.Run(ServicesToRun);
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            ReverseSearchPumpBL.Instance.Start();
        }

        protected override void OnStop()
        {
            ReverseSearchPumpBL.Instance.Stop();
            base.OnStop();
        }

        protected override void RegisterServiceManagers()
        {
            try
            {
                // Initialize service managers.
                _reverseSearchPumpSM = new ReverseSearchPumpSM();

                // Register them.
                base.RegisterServiceManager(_reverseSearchPumpSM);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(base.ServiceName, "Error occurred when starting the Windows Service, see details: " + ex.Message);
            }
        }
    }
}
