﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;

using Matchnet.SearchLoader.BusinessLogic;

namespace Matchnet.SearchLoader.Service
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller1;
        private System.ServiceProcess.ServiceInstaller serviceInstaller1;
        
        public ProjectInstaller()
        {
            InitializeComponent();
            this.serviceInstaller1.ServiceName = ServiceConstants.SERVICE_NAME;
        }

        private void ProjectInstaller_AfterInstall(object sender, InstallEventArgs e)
        {
            ReverseSearchPumpBL.PerfCounterInstall();
        }


        private void ProjectInstaller_BeforeUninstall(object sender, InstallEventArgs e)
        {
            ReverseSearchPumpBL.PerfCounterUninstall();
        }

    }
}
