﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.SearchLoader.ValueObjects
{
    public class ServiceConstants
    {
        public const string SERVICE_CONSTANT = "SEARCHLOADER_SVC";
        public const string SERVICE_NAME = "Matchnet.SearchLoader.Service";
        public const string SERVICE_MANAGER_NAME = "ReverseSearchPumpSM";
    }
}
