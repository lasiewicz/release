﻿using System;
using System.Collections.Generic;
using System.IO;
using Matchnet.SearchLoader.ReverseSearch.Interfaces;

namespace Matchnet.SearchLoader.ReverseSearch
{
	[Serializable]
	public class SearchPreferenceContainer : IPreferenceProvider
	{
		private IList<Dictionary<Int32, SearchPreference>> preferences;

		public SearchPreferenceContainer(String searchPreferencesPath)
			: this(searchPreferencesPath, false)
		{
		}

		public SearchPreferenceContainer(String searchPreferencesPath, Boolean preCacheData)
		{
			if (String.IsNullOrEmpty(searchPreferencesPath)) throw new ArgumentNullException("searchPreferencesPath");

			if (Directory.Exists(searchPreferencesPath) == false && File.Exists(searchPreferencesPath) == false)
			{
				throw new ArgumentOutOfRangeException("searchPreferencesPath");
			}

			SearchPreferencesPath = searchPreferencesPath;

			if (preCacheData) preferences = SearchPreference.Load(SearchPreferencesPath);
		}

		public String SearchPreferencesPath { get; set; }

		public IList<Dictionary<Int32, SearchPreference>> Preferences
		{
			get
			{
				if (preferences == null) preferences = SearchPreference.Load(SearchPreferencesPath);

				return preferences;
			}
		}

		#region IPreferenceProvider Members

		public SearchPreference GetPreference(Int32 memberId)
		{
			return GetPreference(memberId, false);
		}

		#endregion

		public SearchPreference GetPreference(Int32 memberId, Boolean throwNotFoundException)
		{
			foreach (var preferenceContainer in Preferences)
			{
				if (preferenceContainer.ContainsKey(memberId)) return preferenceContainer[memberId];
			}

			if (throwNotFoundException)
				throw new KeyNotFoundException(String.Format("The memberId {0} was not found.", memberId));

			return null;
		}
	}
}