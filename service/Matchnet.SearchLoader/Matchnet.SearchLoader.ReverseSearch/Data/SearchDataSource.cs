﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using Matchnet.SearchLoader.ReverseSearch.Exceptions;

namespace Matchnet.SearchLoader.ReverseSearch.Data
{
    public class SearchDataSource
    {
        public static Int32 TimeoutValue = 240000;
        public static readonly Int32 SleepTime = 250;

        private readonly String connectionString;

        public SearchDataSource(String connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Get(Parameter parameter, Action<SqlDataReader> handler)
        {
            if (parameter == null) throw new ArgumentNullException("parameter");

            var timer = new Stopwatch();

            using (var connection = new SqlConnection { ConnectionString = connectionString })
            using (var command = new SqlCommand
            {
                CommandText = Constants.Data.StoredProcedureName,
                Connection = connection,
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = Constants.Data.DefaultTimeout,
            })
            {
                command.Parameters.AddWithValue(Constants.Data.Parameters.Community, (Int32)parameter.SelectedCommunity);
                command.Parameters.AddWithValue(Constants.Data.Parameters.Partition, parameter.Partition);
                command.Parameters.AddWithValue(Constants.Data.Parameters.StartingRecord, parameter.StartingRecord);
                command.Parameters.AddWithValue(Constants.Data.Parameters.RecordsCount, parameter.RecordsCount);
                if (parameter.SearchFlag >= 0)
                {
                    command.Parameters.AddWithValue(Constants.Data.Parameters.SearchFlag, parameter.SearchFlag);
                }

                if (connection.State != ConnectionState.Open) connection.Open();

                timer.Start();

                var result = command.BeginExecuteReader();

                while (!result.IsCompleted)
                {
                    var elapsedTime = timer.ElapsedMilliseconds;

                    if (elapsedTime > TimeoutValue)
                    {
                        throw new DataSourceTimeoutException(TimeoutValue, elapsedTime);
                    }

                    System.Threading.Thread.Sleep(SleepTime);
                }

                using (var reader = command.EndExecuteReader(result))
                {
                    if (reader == null) return;

                    handler.Invoke(reader);
                    reader.Close();
                }

                if (connection.State != ConnectionState.Closed) connection.Close();
            }

            timer.Stop();
        }
    }
}
