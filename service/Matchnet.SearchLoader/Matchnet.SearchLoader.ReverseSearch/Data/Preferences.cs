﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Data
{
	[Serializable]
	public sealed class Preferences
	{
		public Int32 GroupId { get; set; }

		public Int32 RelocateFlag { get; set; }

		public Int32 MinimumHeight { get; set; }

		public Int32 MaximumHeight { get; set; }

		public Int32 MinimumAge { get; set; }

		public Int32 MaximumAge { get; set; }

		public Int32 MoreChildren { get; set; }

		public Int32 SynagogueAttendance { get; set; }

		public Int32 JDateEthnicity { get; set; }

		public Int32 SmokingHabits { get; set; }

		public Int32 DrinkingHabits { get; set; }

		public Int32 EducationLevel { get; set; }

		public Int32 JDateReligion { get; set; }

		public Int32 ActivityLevel { get; set; }

		public Int32 KeepKosher { get; set; }

		public Int32 MaritalStatus { get; set; }

		public Int32 LanguageMask { get; set; }

		public Int32 Distance { get; set; }

		public Decimal Latitude { get; set; }

		public Decimal Longitude { get; set; }
	}
}