﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Matchnet.SearchLoader.ReverseSearch.Geo;

namespace Matchnet.SearchLoader.ReverseSearch.Data
{
	public sealed class DevData
	{
		private static class Query
		{
			public const String Gender =
				@"select 
					mi.MemberID, mi.Value
				from 
					mnSystem.dbo.Attribute a 
				inner join 
					mnSystem.dbo.AttributeGroup ag on a.AttributeID = ag.AttributeID 
				left outer join 
					MemberAttributeInt mi on ag.AttributeGroupID = mi.AttributeGroupID
				where a.AttributeName = 'GenderMask'";

			public const String Region =
				@"select 
					mi.MemberID, mi.Value as 'RegionId', r.Latitude, r.Longitude
				from 
					mnSystem.dbo.Attribute a 
				inner join 
					mnSystem.dbo.AttributeGroup ag on a.AttributeID = ag.AttributeID 
				left outer join 
					MemberAttributeInt mi on ag.AttributeGroupID = mi.AttributeGroupID
				left outer join 
					mnRegion.dbo.Region r on mi.Value = r.RegionID
				where a.AttributeName = 'RegionID'";

			public const String LastLogin =
				@"select 
					md.MemberID, md.Value
				from 
					mnSystem.dbo.Attribute a 
				inner join 
					mnSystem.dbo.AttributeGroup ag on a.AttributeID = ag.AttributeID 
				left outer join 
					MemberAttributeDate md on ag.AttributeGroupID = md.AttributeGroupID
				where a.AttributeName = 'BrandLastLogonDate'";

			public const String Signup =
				@"select 
					md.MemberID, md.Value
				from 
					mnSystem.dbo.Attribute a 
				inner join 
					mnSystem.dbo.AttributeGroup ag on a.AttributeID = ag.AttributeID 
				left outer join 
					MemberAttributeDate md on ag.AttributeGroupID = md.AttributeGroupID
				where a.AttributeName = 'BrandInsertDate'";

			public const String Preferences =
				@"select
					m.MemberID, 
					mg.GroupID, 
					sp.RelocateFlag,
					sp.MinHeight,
					sp.MaxHeight,
					sp.MinAge,
					sp.MaxAge,
					sp.MoreChildrenFlag,
					sp.SynagogueAttendance,
					sp.JDateEthnicity,
					sp.SmokingHabits,
					sp.DrinkingHabits,
					sp.EducationLevel,
					sp.JDateReligion,
					sp.ActivityLevel,
					sp.KeepKosher,
					sp.MaritalStatus,
					sp.LanguageMask,
					sp.Distance,
					r.Latitude, 
					r.Longitude 
				from 
					Member m
				inner join 
					MemberGroup mg on mg.MemberID = m.MemberID
				left outer join 
					SearchPreference sp on sp.MemberID = m.MemberID
				left outer join 
					mnRegion.dbo.Region r on sp.RegionID = r.RegionID
                        where mg.GroupID = 1 or mg.GroupID =  3";

			//public const String Photos = "select distinct MemberID from MemberPhoto";
            //public const String Photos = "select distinct MemberID from Member";
            public const String Photos = "select memberid, groupid from MemberPhoto where ApprovedFlag = 1 and FileWebPath is not null group by MemberID, GroupID";
		}

		public Dictionary<string, Boolean> GetMembersWithPhotos()
		{
            var membersWithPhotos = new Dictionary<string, Boolean>();

			using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GeoData"].ConnectionString))
			using (var command = new SqlCommand(Query.Photos, connection))
			{
				if (connection.State != ConnectionState.Open) connection.Open();

				connection.ChangeDatabase("mnMember2");

				using (var reader = command.ExecuteReader())
				{
					if (reader == null) throw new ApplicationException("reader is null.");

					while (reader.Read())
					{
						var memberID = GetValue<Int32>(reader[0]);
                        var groupID = getSiteID(GetValue<Int32>(reader[1]));
                        string key = memberID.ToString() + "-" + groupID.ToString();

						if (memberID < 1) continue;

						membersWithPhotos[key] = true;
					}
				}

			}

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GeoData"].ConnectionString))
            using (var command = new SqlCommand(Query.Photos, connection))
            {
                if (connection.State != ConnectionState.Open) connection.Open();

                connection.ChangeDatabase("mnMember1");

                using (var reader = command.ExecuteReader())
                {
                    if (reader == null) throw new ApplicationException("reader is null.");

                    while (reader.Read())
                    {
                        var memberID = GetValue<Int32>(reader[0]);
                        var groupID = getSiteID(GetValue<Int32>(reader[1]));
                        string key = memberID.ToString() + "-" + groupID.ToString();

                        if (memberID < 1) continue;

                        membersWithPhotos[key] = true;
                    }
                }

            }

			return membersWithPhotos;
		}

		public Dictionary<string, Preferences> GetPreferences()
		{
			var preferences = new Dictionary<string, Preferences>();

			using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GeoData"].ConnectionString))
			using (var command = new SqlCommand(Query.Preferences, connection))
			{
				if (connection.State != ConnectionState.Open) connection.Open();

				connection.ChangeDatabase("mnMember2");

				using (var reader = command.ExecuteReader())
				{
					if (reader == null) throw new ApplicationException("reader is null.");

					while (reader.Read())
					{
						var memberId = GetValue<Int32>(reader[0]);
                        var groupID = getSiteID(GetValue<Int32>(reader[1]));
                        string key = memberId.ToString() + "-" + groupID.ToString();

						if (memberId < 1) continue;

                        preferences[key] = new Preferences
						{
							GroupId = GetValue<Int32>(reader[1]),
							RelocateFlag = GetValue<Int32>(reader[2]),
							MinimumHeight = GetValue<Int32>(reader[3]),
							MaximumHeight = GetValue<Int32>(reader[4]),
							MinimumAge = GetValue<Int32>(reader[5]),
							MaximumAge = GetValue<Int32>(reader[6]),
							MoreChildren = GetValue<Int32>(reader[7]),
							SynagogueAttendance = GetValue<Int32>(reader[8]),
							JDateEthnicity = GetValue<Int32>(reader[9]),
							SmokingHabits = GetValue<Int32>(reader[10]),
							DrinkingHabits = GetValue<Int32>(reader[11]),
							EducationLevel = GetValue<Int32>(reader[12]),
							JDateReligion = GetValue<Int32>(reader[13]),
							ActivityLevel = GetValue<Int32>(reader[14]),
							KeepKosher = GetValue<Int32>(reader[15]),
							MaritalStatus = GetValue<Int32>(reader[16]),
							LanguageMask = GetValue<Int32>(reader[17]),
							Distance = GetValue<Int32>(reader[18]),
							Latitude = GetValue<Decimal>(reader[19]),
							Longitude = GetValue<Decimal>(reader[20]),
						};
					}
				}

			}

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GeoData"].ConnectionString))
            using (var command = new SqlCommand(Query.Preferences, connection))
            {
                if (connection.State != ConnectionState.Open) connection.Open();

                connection.ChangeDatabase("mnMember1");

                using (var reader = command.ExecuteReader())
                {
                    if (reader == null) throw new ApplicationException("reader is null.");

                    while (reader.Read())
                    {
                        var memberId = GetValue<Int32>(reader[0]);
                        var groupID = getSiteID(GetValue<Int32>(reader[1]));
                        string key = memberId.ToString() + "-" + groupID.ToString();

                        if (memberId < 1) continue;

                        preferences[key] = new Preferences
                        {
                            GroupId = GetValue<Int32>(reader[1]),
                            RelocateFlag = GetValue<Int32>(reader[2]),
                            MinimumHeight = GetValue<Int32>(reader[3]),
                            MaximumHeight = GetValue<Int32>(reader[4]),
                            MinimumAge = GetValue<Int32>(reader[5]),
                            MaximumAge = GetValue<Int32>(reader[6]),
                            MoreChildren = GetValue<Int32>(reader[7]),
                            SynagogueAttendance = GetValue<Int32>(reader[8]),
                            JDateEthnicity = GetValue<Int32>(reader[9]),
                            SmokingHabits = GetValue<Int32>(reader[10]),
                            DrinkingHabits = GetValue<Int32>(reader[11]),
                            EducationLevel = GetValue<Int32>(reader[12]),
                            JDateReligion = GetValue<Int32>(reader[13]),
                            ActivityLevel = GetValue<Int32>(reader[14]),
                            KeepKosher = GetValue<Int32>(reader[15]),
                            MaritalStatus = GetValue<Int32>(reader[16]),
                            LanguageMask = GetValue<Int32>(reader[17]),
                            Distance = GetValue<Int32>(reader[18]),
                            Latitude = GetValue<Decimal>(reader[19]),
                            Longitude = GetValue<Decimal>(reader[20]),
                        };
                    }
                }

            }


			return preferences;
		}

		public Dictionary<Int32, Coordinates> GetRegions()
		{
			var regions = new Dictionary<Int32, Coordinates>();

			using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GeoData"].ConnectionString))
			using (var command = new SqlCommand(Query.Region, connection))
			{
				if (connection.State != ConnectionState.Open) connection.Open();

				connection.ChangeDatabase("mnMember2");

				using (var reader = command.ExecuteReader())
				{
					if (reader == null) throw new ApplicationException("reader is null.");

					while (reader.Read())
					{
						var memberId = GetValue<Int32>(reader[0]);

						if (memberId < 1) continue;

						regions[memberId] = new Coordinates(
							(Double)GetValue<Decimal>(reader[2]),
							(Double)GetValue<Decimal>(reader[3]),
							false
							);
					}
				}

			}

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GeoData"].ConnectionString))
            using (var command = new SqlCommand(Query.Region, connection))
            {
                if (connection.State != ConnectionState.Open) connection.Open();

                connection.ChangeDatabase("mnMember1");

                using (var reader = command.ExecuteReader())
                {
                    if (reader == null) throw new ApplicationException("reader is null.");

                    while (reader.Read())
                    {
                        var memberId = GetValue<Int32>(reader[0]);

                        if (memberId < 1) continue;

                        regions[memberId] = new Coordinates(
                            (Double)GetValue<Decimal>(reader[2]),
                            (Double)GetValue<Decimal>(reader[3]),
                            false
                            );
                    }
                }

            }

			return regions;
		}

		public Dictionary<Int32, Int32> GetGenderMasks()
		{
			var genderMasks = new Dictionary<Int32, Int32>();

			using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GeoData"].ConnectionString))
			using (var command = new SqlCommand(Query.Gender, connection))
			{
				if (connection.State != ConnectionState.Open) connection.Open();

				connection.ChangeDatabase("mnMember2");

				using (var reader = command.ExecuteReader())
				{
					if (reader == null) throw new ApplicationException("reader is null.");

					while (reader.Read())
					{
						var memberId = GetValue<Int32>(reader[0]);
						var genderMask = GetValue<Int32>(reader[1]);

						if (memberId < 1) continue;

						genderMasks[memberId] = genderMask;
					}
				}

			}

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GeoData"].ConnectionString))
            using (var command = new SqlCommand(Query.Gender, connection))
            {
                if (connection.State != ConnectionState.Open) connection.Open();

                connection.ChangeDatabase("mnMember1");

                using (var reader = command.ExecuteReader())
                {
                    if (reader == null) throw new ApplicationException("reader is null.");

                    while (reader.Read())
                    {
                        var memberId = GetValue<Int32>(reader[0]);
                        var genderMask = GetValue<Int32>(reader[1]);

                        if (memberId < 1) continue;

                        genderMasks[memberId] = genderMask;
                    }
                }

            }

			return genderMasks;
		}

		public Dictionary<Int32, DateTime> GetLastLogins()
		{
			var lastLogins = new Dictionary<Int32, DateTime>();

			using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GeoData"].ConnectionString))
			using (var command = new SqlCommand(Query.LastLogin, connection))
			{
				if (connection.State != ConnectionState.Open) connection.Open();

				connection.ChangeDatabase("mnMember2");

				using (var reader = command.ExecuteReader())
				{
					if (reader == null) throw new ApplicationException("reader is null.");

					while (reader.Read())
					{
						var memberId = GetValue<Int32>(reader[0]);
						var lastLogin = GetValue<DateTime>(reader[1]);

						if (memberId < 1) continue;

						lastLogins[memberId] = lastLogin;
					}
				}
			}

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GeoData"].ConnectionString))
            using (var command = new SqlCommand(Query.LastLogin, connection))
            {
                if (connection.State != ConnectionState.Open) connection.Open();

                connection.ChangeDatabase("mnMember1");

                using (var reader = command.ExecuteReader())
                {
                    if (reader == null) throw new ApplicationException("reader is null.");

                    while (reader.Read())
                    {
                        var memberId = GetValue<Int32>(reader[0]);
                        var lastLogin = GetValue<DateTime>(reader[1]);

                        if (memberId < 1) continue;

                        lastLogins[memberId] = lastLogin;
                    }
                }
            }

			return lastLogins;
		}

		public Dictionary<Int32, DateTime> GetSignups()
		{
			var lastLogins = new Dictionary<Int32, DateTime>();

			using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GeoData"].ConnectionString))
            using (var command = new SqlCommand(Query.Signup, connection))
            {
                if (connection.State != ConnectionState.Open) connection.Open();

                connection.ChangeDatabase("mnMember2");

                using (var reader = command.ExecuteReader())
                {
                    if (reader == null) throw new ApplicationException("reader is null.");

                    while (reader.Read())
                    {
                        var memberId = GetValue<Int32>(reader[0]);
                        var lastLogin = GetValue<DateTime>(reader[1]);

                        if (memberId < 1) continue;

                        lastLogins[memberId] = lastLogin;
                    }
                }

            }

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GeoData"].ConnectionString))
            using (var command = new SqlCommand(Query.Signup, connection))
            {
                if (connection.State != ConnectionState.Open) connection.Open();

                connection.ChangeDatabase("mnMember1");

                using (var reader = command.ExecuteReader())
                {
                    if (reader == null) throw new ApplicationException("reader is null.");

                    while (reader.Read())
                    {
                        var memberId = GetValue<Int32>(reader[0]);
                        var lastLogin = GetValue<DateTime>(reader[1]);

                        if (memberId < 1) continue;

                        lastLogins[memberId] = lastLogin;
                    }
                }

            }
			return lastLogins;
		}

		private static T GetValue<T>(Object field)
		{
			return (field == null || field == DBNull.Value) ? default(T) : (T)field;
		}

        private int getSiteID(int communityID)
        {
            if (communityID == 3)
                return 103;
            else
                return 101;
        }


	}
}