using System;
using System.Collections.Generic;
using System.Linq;
using Matchnet.SearchLoader.ReverseSearch.Enumerations;

namespace Matchnet.SearchLoader.ReverseSearch.Data
{
	[Serializable]
	public sealed class Parameter : IEquatable<Parameter>
	{
		public Community SelectedCommunity { get; set; }

		public Int32 Partition { get; set; }

		public Int32 StartingRecord { get; set; }

		public Int32 RecordsCount { get; set; }

		public Int32 SearchFlag { get; set; }

		public static Parameter Create(
			Community selectedCommunity,
			Int32 partition,
			Int32 startingRecord,
			Int32 recordsCount,
			Int32 searchFlag
			)
		{
			return new Parameter
			{
				SelectedCommunity = selectedCommunity,
				StartingRecord = startingRecord,
				RecordsCount = recordsCount,
				SearchFlag = searchFlag,
				Partition = partition,
			};
		}

		public static IEnumerable<Parameter> CreateMany(
			Community selectedCommunity,
			Int32 startingRecord,
			Int32 recordsCount,
			Int32 searchFlag
			)
		{
			var parameters = new List<Parameter>();

			Array.ForEach(
				Enumerable.Range(Constants.Partition.Minimum, Constants.Partition.Maximum).ToArray(),
				partitionId => parameters.Add(
										new Parameter
										{
											Partition = partitionId,
											RecordsCount = recordsCount,
											SearchFlag = searchFlag,
											SelectedCommunity = selectedCommunity,
											StartingRecord = startingRecord,
										}
										)
				);

			return parameters;
		}

		public static IEnumerable<Parameter> CreateMany(
			Community selectedCommunity,
			IEnumerable<Int32> partitionRange,
			Int32 startingRecord,
			Int32 recordsCount,
			Int32 searchFlag
			)
		{
			var parameters = new List<Parameter>();

			Array.ForEach(
				partitionRange.ToArray(),
				partitionId => parameters.Add(
										new Parameter
										{
											Partition = partitionId,
											RecordsCount = recordsCount,
											SearchFlag = searchFlag,
											SelectedCommunity = selectedCommunity,
											StartingRecord = startingRecord,
										}
										)
				);

			return parameters;
		}

		public static IEnumerable<Parameter> CreateMany(Community community)
		{
			return CreateMany(community, 1, Constants.Data.DefaultRows, 1);
		}

		#region Equality

		public override Int32 GetHashCode()
		{
			return
				Partition.GetHashCode() ^
				RecordsCount.GetHashCode() ^
				SearchFlag.GetHashCode() ^
				SelectedCommunity.GetHashCode() ^
				StartingRecord.GetHashCode();
		}

		public override Boolean Equals(Object obj)
		{
			return Equals(obj as Parameter);
		}

		public static Boolean Equals(Parameter p1, Parameter p2)
		{
			return p1 == p2;
		}

		public static Boolean operator ==(Parameter p1, Parameter p2)
		{
			var objp1 = (Object)p1;
			var objp2 = (Object)p2;

			if (objp1 == null) return objp2 == null;

			return objp2 != null && p1.Equals(p2);
		}

		public static Boolean operator !=(Parameter p1, Parameter p2)
		{
			return !(p1 == p2);
		}


		public Boolean Equals(Parameter other)
		{
			if (other == null) return false;

			if (ReferenceEquals(this, other)) return true;

			return
				other.Partition == Partition &&
				other.RecordsCount == RecordsCount &&
				other.SearchFlag == SearchFlag &&
				other.SelectedCommunity == SelectedCommunity &&
				other.StartingRecord == StartingRecord;
		}

		#endregion Equality
	}
}