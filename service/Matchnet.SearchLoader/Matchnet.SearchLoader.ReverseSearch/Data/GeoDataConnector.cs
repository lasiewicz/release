﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Matchnet.SearchLoader.ReverseSearch.Geo;

namespace Matchnet.SearchLoader.ReverseSearch.Data
{
	public static class GeoDataConnector
	{
		public const Double Angle = 0.1;

		private const String CommandLatLong = "select top 1 Latitude, Longitude from Region where RegionID = '{0}'";
		public const Boolean UseMetric = false;

		public static Coordinates GetCoordinates(Int32 regionId)
		{
			if (regionId < 1) throw new ArgumentOutOfRangeException("regionId");

			using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GeoData"].ConnectionString))
			using (var command = new SqlCommand(String.Format(CommandLatLong, regionId), connection))
			{
				if (connection.State != ConnectionState.Open) connection.Open();

				using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.SingleResult))
				{
					if (reader == null || reader.Read() == false)
					{
						throw new InvalidOperationException();
					}

					return new Coordinates((Double)(Decimal)reader[0], (Double)(Decimal)reader[1], false);
				}
			}
		}

		public static Boolean TryGetCoordinates(Int32 regionId, out Coordinates coordinates)
		{
			if (regionId < 1)
			{
				coordinates = Constants.Geo.Default.Coordinates;
				return false;
			}

			try
			{
				using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["GeoData"].ConnectionString))
				using (var command = new SqlCommand(String.Format(CommandLatLong, regionId), connection))
				{
					if (connection.State != ConnectionState.Open) connection.Open();

					using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.SingleResult))
					{
						if (reader == null || reader.Read() == false)
						{
							coordinates = Constants.Geo.Default.Coordinates;
							return false;
						}

						coordinates = new Coordinates((Double)(Decimal)reader[0], (Double)(Decimal)reader[1], false);
						return true;
					}
				}
			}
			catch
			{
				coordinates = Constants.Geo.Default.Coordinates;
				return false;
			}
		}

		public static CoordinateBox GetBoxCoordinates(Coordinates coordinates, Int32 radius)
		{
			var rad = GeoUtil.DegreeToRadian(Angle);

			var searchBox = new Box
			{
				Key =
				{
					X = (int)(coordinates.Longitude / rad),
					Y = (int)(coordinates.Latitude / rad)
				},
				Size = GeoUtil.CalculateBoxSize(coordinates, rad, UseMetric)
			};

			GeoUtil.CalculateSearchArea(searchBox, radius);

			return new CoordinateBox
			{
				LongitudeMinimum = searchBox.Area.BoxXMin * rad,
				LongitudeMaximum = searchBox.Area.BoxXMax * rad,
				LatitudeMinimum = searchBox.Area.BoxYMin * rad,
				LatitudeMaximum = searchBox.Area.BoxYMax * rad,
				TypeOfCoordinate = CoordinateType.Radians,
			};
		}

		public static CoordinateBox GetBoxCoordinates(Int32 regionId, Int32 radius)
		{
			return GetBoxCoordinates(GetCoordinates(regionId), radius);
		}
	}
}