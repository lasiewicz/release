﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Matchnet.SearchLoader.ReverseSearch
{
	[Serializable]
	public class SearchPreference
	{
		#region Properties

		public Int32 MemberId { get; set; }

		public Int32 GroupId { get; set; }

		public Int32 GenderMask { get; set; }

		public Int32 MinimumAge { get; set; }

		public Int32 MaximumAge { get; set; }

		public Int32 RegionId { get; set; }

		public Int32 SchoolId { get; set; }

		public Int32 AreaCode1 { get; set; }

		public Int32 AreaCode2 { get; set; }

		public Int32 AreaCode3 { get; set; }

		public Int32 AreaCode4 { get; set; }

		public Int32 AreaCode5 { get; set; }

		public Int32 AreaCode6 { get; set; }

		public Int32 Distance { get; set; }

		public Int32 HasPhotoFlag { get; set; }

		public Int32 EducationLevel { get; set; }

		public Int32 Religion { get; set; }

		public Int32 LanguageMask { get; set; }

		public Int32 Ethnicity { get; set; }

		public Int32 SmokingHabits { get; set; }

		public Int32 DrinkingHabits { get; set; }

		public Int32 MinimumHeight { get; set; }

		public Int32 MaximumHeight { get; set; }

		public Int32 MaritalStatus { get; set; }

		public Int32 JDateReligion { get; set; }

		public Int32 JDateEthnicity { get; set; }

		public Int32 SynagogueAttendance { get; set; }

		public Int32 KeepKosher { get; set; }

		public Int32 SexualIdentityType { get; set; }

		public Int32 RelationshipMask { get; set; }

		public Int32 RelationshipStatus { get; set; }

		public Int32 BodyType { get; set; }

		public Int32 Zodiac { get; set; }

		public Int32 MajorType { get; set; }

		public Int32 SearchOrderBy { get; set; }

		public Int32 SearchTypeId { get; set; }

		public Int32 CountryRegionId { get; set; }

		public DateTime InsertDate { get; set; }

		public DateTime UpdateDate { get; set; }

		public Int32 ActivityLevel { get; set; }

		public Int32 ChildrenCount { get; set; }

		public Int32 Custody { get; set; }

		public Int32 MoreChildrenFlag { get; set; }

		public Int32 RelocateFlag { get; set; }

		public Int32 WeightMinimum { get; set; }

		public Int32 WeightMaximum { get; set; }

		#endregion Properties

		public static IList<Dictionary<Int32, SearchPreference>> Load(String path, Boolean useThreading)
		{
			if (String.IsNullOrEmpty(path)) throw new ArgumentNullException("path");

			FileType fileType;

			if (Directory.Exists(path))
			{
				fileType = FileType.Directory;
			}
			else if (File.Exists(path))
			{
				fileType = FileType.File;
			}
			else
			{
				throw new ArgumentOutOfRangeException("path");
			}

			var results = new List<Dictionary<Int32, SearchPreference>>();

			try
			{
				string[] files = fileType == FileType.Directory
										?
											Directory.GetFiles(path)
										: new[] { path };

				int count = files.Length;

				if (useThreading)
				{
					Array.ForEach(
						files,
						file => ThreadPool.QueueUserWorkItem(
									state => Fill(results, (String)state, ref count), file
									)
						);

					do
					{
						Thread.Sleep(250);
					} while (count > 0);
				}
				else
				{
					Array.ForEach(
						files,
						file => Fill(results, file, ref count));
				}
			}
			catch (Exception exc)
			{
				Console.WriteLine("Exception: {0}. Stack trace: {1}", exc.Message, exc.StackTrace);
			}

			return results;
		}

		public static IList<Dictionary<Int32, SearchPreference>> Load(String path)
		{
			return Load(path, false);
		}

		private static void Fill(
			ICollection<Dictionary<Int32, SearchPreference>> preferences,
			String file,
			ref Int32 count)
		{
			var prefs = new Dictionary<Int32, SearchPreference>();
			try
			{
				using (var fs = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.None))
				using (var sr = new StreamReader(fs))
				{
					string line = String.Empty;
					while (sr.EndOfStream == false)
					{
						try
						{
							line = sr.ReadLine();
							ProcessLine(prefs, line);
						}
						catch (Exception exc)
						{
							Console.WriteLine("Exception: {0}, caused by {1}. Stack trace: {2}",
													exc.Message, line, exc.StackTrace);
						}
					}
				}
			}
			catch (Exception exc)
			{
				Console.WriteLine("Exception: {0}, caused by {1}. Stack trace: {2}",
										exc.Message, file, exc.StackTrace);
			}
			finally
			{
				preferences.Add(prefs);

				Interlocked.Decrement(ref count);
			}
		}

		private static void ProcessLine(IDictionary<Int32, SearchPreference> preferences, String line)
		{
			if (IsValidLine(line) == false) return;

			SearchPreference searchPreference = Create(line);

			if (searchPreference.GroupId != Constants.JDate) return;

			try
			{
				preferences.Add(searchPreference.MemberId, searchPreference);
			}
			catch (Exception exc)
			{
				Console.WriteLine(exc.Message);
				throw;
			}
		}

		private static Boolean IsValidLine(String line)
		{
			return line.Length > Constants.Preferences.Ingestion.MinimumLineLength &&
					 !line.StartsWith(Constants.Preferences.Ingestion.InvalidCharacter.Dash) &&
					 !line.StartsWith(Constants.Preferences.Ingestion.InvalidCharacter.M) &&
					 !line.StartsWith(Constants.Preferences.Ingestion.InvalidCharacter.LeftParentheses);
		}

		private static SearchPreference Create(String line)
		{
			string[] fields = line.Split(new[] { Constants.Preferences.Ingestion.Comma }, StringSplitOptions.None);

			return new SearchPreference
			{
				MemberId = GetProperty(fields[0]),
				GroupId = GetProperty(fields[1]),
				GenderMask = GetProperty(fields[2]),
				MinimumAge = GetProperty(fields[3]),
				MaximumAge = GetProperty(fields[4]),
				RegionId = GetProperty(fields[5]),
				SchoolId = GetProperty(fields[6]),
				AreaCode1 = GetProperty(fields[7]),
				AreaCode2 = GetProperty(fields[8]),
				AreaCode3 = GetProperty(fields[9]),
				AreaCode4 = GetProperty(fields[10]),
				AreaCode5 = GetProperty(fields[11]),
				AreaCode6 = GetProperty(fields[12]),
				Distance = GetProperty(fields[13]),
				HasPhotoFlag = GetProperty(fields[14]),
				EducationLevel = GetProperty(fields[15]),
				Religion = GetProperty(fields[16]),
				LanguageMask = GetProperty(fields[17]),
				Ethnicity = GetProperty(fields[18]),
				SmokingHabits = GetProperty(fields[19]),
				DrinkingHabits = GetProperty(fields[20]),
				MinimumHeight = GetProperty(fields[21]),
				MaximumHeight = GetProperty(fields[22]),
				MaritalStatus = GetProperty(fields[23]),
				JDateReligion = GetProperty(fields[24]),
				JDateEthnicity = GetProperty(fields[25]),
				SynagogueAttendance = GetProperty(fields[26]),
				KeepKosher = GetProperty(fields[27]),
				SexualIdentityType = GetProperty(fields[28]),
				RelationshipMask = GetProperty(fields[29]),
				RelationshipStatus = GetProperty(fields[30]),
				BodyType = GetProperty(fields[31]),
				Zodiac = GetProperty(fields[32]),
				MajorType = GetProperty(fields[33]),
				SearchOrderBy = GetProperty(fields[34]),
				SearchTypeId = GetProperty(fields[35]),
				CountryRegionId = GetProperty(fields[36]),
				InsertDate = GetDateProperty(fields[37]),
				UpdateDate = GetDateProperty(fields[38]),
				ActivityLevel = GetProperty(fields[39]),
				ChildrenCount = GetProperty(fields[40]),
				Custody = GetProperty(fields[41]),
				MoreChildrenFlag = GetProperty(fields[42]),
				RelocateFlag = GetProperty(fields[43]),
				WeightMinimum = GetProperty(fields[44]),
				WeightMaximum = GetProperty(fields[45]),
			};
		}

		private static DateTime GetDateProperty(String line)
		{
			DateTime value;

			return DateTime.TryParse(line, out value) ? value : default(DateTime);
		}

		private static Int32 GetProperty(String line)
		{
			Int32 value;

			return Int32.TryParse(line, out value) ? value : default(Int32);
		}

		#region Nested type: FileType

		private enum FileType
		{
			Directory,
			File,
		}

		#endregion
	}
}