﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Enumerations
{
	[Flags]
	public enum Drinking
	{
		never = 2,
		socially = 4,
		on_occasion = 8,
		frequently = 16,
		will_tell_you_later = 32,
	}
}