﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Enumerations
{
	[Flags]
	public enum Education
	{
		Elementary = 2,
		High_School = 4,
		Some_College = 8,
		Bachelor_Degree = 16,
		Master_Degree = 32,
		Doctorate_Degree = 64,
		Associate_Degree = 128,
		Higher_National_Diploma = 256,
		Will_tell_you_later = 512,
	}
}
