﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Enumerations
{
	[Flags]
	public enum MaritalStatus
	{
		Single = 2,
		Divorced = 8,
		Separated = 16,
		Widowed = 32,
		Married = 64,
		Will_tell_you_later = 128,
	}
}