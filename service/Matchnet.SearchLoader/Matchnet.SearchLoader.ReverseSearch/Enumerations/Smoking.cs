﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Enumerations
{
	[Flags]
	public enum Smoking
	{
		Non_Smoker = 2,
		Occasional_Smoker = 4,
		Smokes_Regularly = 8,
		Trying_To_Quit = 16,
		Will_tell_you_later = 32,
	}
}