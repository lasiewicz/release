﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Enumerations
{
	[Serializable]
	public enum Community
	{
		AmericanSingles = 1,
		JDate = 3,
		Cupid = 10,
		College = 12,
		Mingle = 20,
        ItalianSinglesConnection = 21,
        InterRacialSingles = 22,
        BBWPersonalsPlus = 23, 
        BlackSingles = 24,
        ChristianMingle = 25,
		Engage = 30
	}
}