﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Enumerations
{
	[Flags]
	public enum Language
	{
		Other = 1,
		English = 2,
		Hebrew = 262144,
		French = 8,
		Spanish = 16,
		German = 32,
		Russian = 64,
		Italian = 128,
		Yiddish = 256,
		Farsi = 512,
		Dutch = 1024,
		Arabic = 2048,
		Bengali = 4096,
		Bulgarian = 8192,
		Chinese = 16384,
		Czech = 32768,
		Greek = 131072,
		Hindi = 524288,
		Japanese = 1048576,
		Korean = 2097152,
		Malay = 4194304,
		Norwegian = 8388608,
		Polish = 16777216,
		Portuguese = 33554432,
		Romanian = 67108864,
		Swedish = 134217728,
		Tagalog = 268435456,
		Thai = 536870912,
		Urdu = 1073741824,
		Vietnamese = 4,
		Finnish = 65536,
	}
}