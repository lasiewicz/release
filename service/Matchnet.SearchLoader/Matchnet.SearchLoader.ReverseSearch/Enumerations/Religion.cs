﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Enumerations
{
	[Flags]
	public enum Religion
	{
		orthodox_baal_teshuva = 1,
		conservative = 2,
		conservadox = 4,
		hassidic = 8,
		modern_orthodox = 16,
		orthodox_frum_from_birth = 32,
		another_stream = 64,
		reconstructionist = 128,
		reform = 256,
		secular = 512,
		traditional = 1024,
		not_willing_to_convert = 2048,
		not_sure_if_im_willing_to_convert = 4096,
		religious = 8192,
		willing_to_convert = 16384,
		masorti = 32768,
		unafilliated = 65536,
		will_tell_you_later = 131072,
	}
}