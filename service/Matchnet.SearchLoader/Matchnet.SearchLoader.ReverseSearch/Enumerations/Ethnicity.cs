﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Enumerations
{
	[Flags]
	public enum Ethnicity
	{
		ashkenazi = 1,
		mixed_ethnic = 2,
		another_ethnic = 4,
		sephardic = 8,
		unknown = 16,
		will_tell_you_later = 32,
	}
}