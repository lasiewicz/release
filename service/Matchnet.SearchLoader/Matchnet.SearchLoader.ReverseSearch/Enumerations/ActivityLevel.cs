﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Enumerations
{
	[Flags]
	public enum ActivityLevel
	{
		active = 2,
		never_active = 4,
		rarely_active = 8,
		selected_activities = 16,
		very_active = 32,
		will_tell_you_later = 64,
	}
}