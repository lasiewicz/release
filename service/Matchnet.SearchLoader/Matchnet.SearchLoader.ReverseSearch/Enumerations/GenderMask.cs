﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Enumerations
{
	[Flags]
	public enum GenderMask
	{
		/// <summary>Male</summary>
		Male = 1,
		/// <summary>Female</summary>
		Female = 2,
		/// <summary>Seeking a male</summary>
		SeekingMale = 4,
		/// <summary>Seeking a female</summary>
		SeekingFemale = 8,
		/// <summary>Male to female</summary>
		MTF = 16,
		/// <summary>Female to male</summary>
		FTM = 32,
		/// <summary>Seeking a male to female</summary>
		SeekingMTF = 64,
		/// <summary>Seeking a female to male</summary>
		SeekingFTM = 128
	} ;
}