﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Enumerations
{
	[Flags]
	public enum Relocate
	{
		yes = 1,
		no = 2,
		not_sure = 4,
		will_tell_you_later = 8,
	}
}