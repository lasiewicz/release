﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Enumerations
{
	[Flags]
	public enum WantsChildren
	{
		no = 1,
		yes = 2,
		not_sure = 4,
	}
}