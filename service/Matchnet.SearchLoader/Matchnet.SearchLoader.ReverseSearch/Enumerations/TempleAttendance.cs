﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Enumerations
{
	[Flags]
	public enum TempleAttendance
	{
		every_shabbat = 1,
		on_high_holidays = 2,
		never = 4,
		sometimes = 8,
		on_some_shabbat = 16,
		will_tell_you_later = 32,
	}
}