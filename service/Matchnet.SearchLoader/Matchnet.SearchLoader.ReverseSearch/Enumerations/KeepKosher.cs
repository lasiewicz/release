﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Enumerations
{
	[Flags]
	public enum KeepKosher
	{
		at_home_Only = 1,
		at_home_and_outside = 2,
		not_at_all = 4,
		to_some_degree = 8,
		will_tell_you_later = 32,
	}
}