﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Interfaces
{
    public interface IDocument
    {
        String this[String name] { get; }
    }
}