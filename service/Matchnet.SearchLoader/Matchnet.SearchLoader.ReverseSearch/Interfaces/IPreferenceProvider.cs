﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Interfaces
{
    public interface IPreferenceProvider
    {
        SearchPreference GetPreference(Int32 memberId);
    }
}