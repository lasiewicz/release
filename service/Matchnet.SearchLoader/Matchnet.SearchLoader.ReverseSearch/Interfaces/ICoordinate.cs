﻿using System;
using Matchnet.SearchLoader.ReverseSearch.Geo;

namespace Matchnet.SearchLoader.ReverseSearch.Interfaces
{
    public interface ICoordinate
    {
        Double Latitude { get; }

        Double Longitude { get; }

        CoordinateType TypeOfCoordinate { get; }

        ICoordinate Convert();
    }
}