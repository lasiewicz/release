using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Matchnet.SearchLoader.ReverseSearch.Enumerations;

namespace Matchnet.SearchLoader.ReverseSearch
{
	public class LineRecorder : IDisposable
	{
		#region Constants

		public const Int32 NullLineId = -1;

		#endregion Constants

		#region Fields

		private readonly Object innerLock;
		private readonly Dictionary<Community, Dictionary<Int32, Int32>> lineIds;

		private readonly Object lockObject;
		private readonly String path;

		private Boolean disposed;

		#endregion Fields

		public LineRecorder(String path)
		{
			if (String.IsNullOrEmpty(path)) throw new ArgumentNullException("path");

			this.path = path;

			lockObject = new Object();

			innerLock = new Object();

			lineIds = new Dictionary<Community, Dictionary<Int32, Int32>>();
		}

		#region IDisposable Members

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		#endregion

		public void Record(Community community, Int32 partition, Int32 lineId)
		{
			lock (lockObject)
			{
				lock (this)
				{
					RecordLineId(community, partition, lineId);
				}
			}
		}

		public void Write()
		{
			using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None))
			using (var sw = new StreamWriter(fs))
			{
				Array.ForEach(
					lineIds.Keys.ToArray(),
					community =>
					Array.ForEach(
						lineIds[community].Keys.ToArray(),
						partition => sw.WriteLine(
											String.Format(
												"{0}-{1}-{2}",
												community.ToString("d"),
												partition,
												lineIds[community][partition]
												)
											)
						)
					);
			}
		}

		private void RecordLineId(Community community, Int32 partition, Int32 lineId)
		{
			if (lineIds.ContainsKey(community) == false)
			{
				lineIds.Add(community, new Dictionary<Int32, Int32> { { partition, lineId } });
				return;
			}

			lock (innerLock)
			{
				Dictionary<int, int> partitions = lineIds[community];

				if (partitions.ContainsKey(partition) == false)
				{
					partitions.Add(partition, lineId);
				}
				else
				{
					partitions[partition] = lineId;
				}
			}
		}

		private void Dispose(Boolean disposing)
		{
			if (disposed) return;

			if (disposing)
			{
				try
				{
					Write();
				}
				catch
				{
				}
			}

			disposed = true;
		}
	}
}