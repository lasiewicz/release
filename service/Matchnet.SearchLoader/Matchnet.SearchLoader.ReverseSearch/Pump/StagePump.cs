﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Matchnet.SearchLoader.ReverseSearch.Core;
using Matchnet.SearchLoader.ReverseSearch.Data;
using Matchnet.SearchLoader.ReverseSearch.Geo;
using Matchnet.SearchLoader.ReverseSearch.Interfaces;

namespace Matchnet.SearchLoader.ReverseSearch.Pump
{
	public class StagePump : IPump
	{
        public const Int32 LastLoginThreshold = 720;

		public const Int32 SignupThreshold = 720;

		private Dictionary<Int32, Int32> GenderMasks { get; set; }

		private Dictionary<Int32, DateTime> LastLogins { get; set; }

		private Dictionary<Int32, DateTime> Signups { get; set; }

		private Dictionary<Int32, Coordinates> MemberRegions { get; set; }

		private Dictionary<string, Preferences> MemberPreferences { get; set; }

        private Dictionary<string, Boolean> MembersWithPhotos { get; set; }

		private readonly SearchDataAdapter searchDataAdapter =
			new SearchDataAdapter(System.Configuration.ConfigurationManager.AppSettings["solrUrl"]);

		public void PumpData()
		{
			for (var i = 1; i < 25; i++)
			{
				PopulateDictionaries(i);

				Console.WriteLine("Populated dictionaries for partition {0}.", i);

				PopulateIndex();

				Console.WriteLine("Populated index for partition {0}.", i);

				ClearDictionaries();

				Console.WriteLine("Cleared dictionaries for partition {0}.", i);
			}
		}

		private void ClearDictionaries()
		{
			GenderMasks = new Dictionary<Int32, Int32>();
			LastLogins = new Dictionary<Int32, DateTime>();
			Signups = new Dictionary<Int32, DateTime>();
			MemberRegions = new Dictionary<Int32, Coordinates>();
            MemberPreferences = new Dictionary<string, Preferences>();
            MembersWithPhotos = new Dictionary<string, Boolean>();
		}

		private void PopulateIndex()
		{
			var timer = new Stopwatch();

			var documentCounter = 0;

			var badDocuments = 0;

			var goodDocuments = 0;

            int siteID = 0;

			foreach (var key in MemberPreferences.Keys)
			{
				try
				{
                    string[] keyBits = key.Split('-');
                    int memberId = Convert.ToInt32(keyBits[0]);
                    var preferences = MemberPreferences[key];

                    switch (preferences.GroupId)
                    {
                        case 3:
                            siteID = 103;
                            break;
                        case 1:
                            siteID = 101;
                            break;
                    }

                    string Photokey = memberId.ToString() + "-" + siteID.ToString();

    				if (GenderMasks.ContainsKey(memberId) == false) continue;

					var genderMask = GenderMasks[memberId];

					if (LastLogins.ContainsKey(memberId) == false) continue;

					if (Signups.ContainsKey(memberId) == false) continue;

					if (MemberRegions.ContainsKey(memberId) == false) continue;

					var coordinates = MemberRegions[memberId];

					if (DateTime.Now.Subtract(LastLogins[memberId]).TotalDays > LastLoginThreshold) continue;

					if (DateTime.Now.Subtract(Signups[memberId]).TotalDays > SignupThreshold) continue;

					var preferredLanguage = PreferenceAdapter.GetPreferredLanguage(preferences.LanguageMask);
					var wantsChildren = PreferenceAdapter.GetPreferredWantsChildren(preferences.MoreChildren);
					var preferredMaritalStatus = PreferenceAdapter.GetDesiredMaritalStatus(preferences.MaritalStatus);
					var preferredRelocateStatus = PreferenceAdapter.GetPreferredRelocationStatus(preferences.RelocateFlag);
					var preferredEducation = PreferenceAdapter.GetPreferredEducationLevel(preferences.EducationLevel);
					var preferredSmokerStatus = PreferenceAdapter.GetPreferredSmokingStatus(preferences.SmokingHabits);
					var preferredKosherStatus = PreferenceAdapter.GetPreferredKosherStatus(preferences.KeepKosher);
					var preferredTempleAttendance = PreferenceAdapter.GetPreferredTempleAttendance(preferences.SynagogueAttendance);
					var preferredEthnicity = PreferenceAdapter.GetPreferredEthnicity(preferences.JDateEthnicity);
					var preferredReligion = PreferenceAdapter.GetPreferredReligion(preferences.JDateReligion);
					var preferredDrinking = PreferenceAdapter.GetPreferredDrinking(preferences.DrinkingHabits);
					var preferredActivityLevel = PreferenceAdapter.GetPreferredActivityLevel(preferences.ActivityLevel);


					var document = new ReverseSearchDocument
					{
                        Id = ReverseSearchDocument.GetId(siteID, memberId),
						MemberId = memberId,
                        SiteId = siteID,
						Gender = ReverseSearchDocument.GetGender(genderMask),
						SeekingGender = ReverseSearchDocument.GetSeekingGender(genderMask),
						Latitude = coordinates.Latitude * Coordinates.RadiansToDegrees,
						Longitude = coordinates.Longitude * Coordinates.RadiansToDegrees,
						LastLogin = LastLogins[memberId],
						Signup = Signups[memberId],
                        HasPhoto = MembersWithPhotos.ContainsKey(Photokey),
						PreferredHeightMinimum =
							preferences.MinimumHeight > 0
								? preferences.MinimumHeight
								: Constants.Profile.Default.Height.Minimum,
						PreferredHeightMaximum =
							preferences.MaximumHeight > 0
								? preferences.MaximumHeight
								: Constants.Profile.Default.Height.Maximum,
						PreferredAgeMinimum =
							preferences.MinimumAge > 0 ? preferences.MinimumAge : Constants.Profile.Default.Age.Minimum,
						PreferredAgeMaximum =
							preferences.MaximumAge > 0 ? preferences.MaximumAge : Constants.Profile.Default.Age.Maximum,
						PreferredLanguage = preferredLanguage,
						WantsChildren = wantsChildren,
						PreferredMaritalStatus = preferredMaritalStatus,
						PreferredRelocateStatus = preferredRelocateStatus,
						PreferredEducation = preferredEducation,
						PreferredSmokerStatus = preferredSmokerStatus,
						PreferredKosherStatus = preferredKosherStatus,
						PreferredTempleAttendance = preferredTempleAttendance,
						PreferredEthnicity = preferredEthnicity,
						PreferredReligion = preferredReligion,
						PreferredDrinking = preferredDrinking,
						PreferredActivityLevel = preferredActivityLevel,
					};

					ReverseSearchDocument.IncrementSpecificity(document, ReverseSearchDocument.DefaultCoefficient, preferredLanguage.Count());
					ReverseSearchDocument.IncrementSpecificity(document, ReverseSearchDocument.DefaultCoefficient, wantsChildren.Count());
					ReverseSearchDocument.IncrementSpecificity(document, ReverseSearchDocument.DefaultCoefficient, preferredMaritalStatus.Count());
					ReverseSearchDocument.IncrementSpecificity(document, ReverseSearchDocument.DefaultCoefficient, preferredRelocateStatus.Count());
					ReverseSearchDocument.IncrementSpecificity(document, ReverseSearchDocument.DefaultCoefficient, preferredEducation.Count());
					ReverseSearchDocument.IncrementSpecificity(document, ReverseSearchDocument.DefaultCoefficient, preferredSmokerStatus.Count());
					ReverseSearchDocument.IncrementSpecificity(document, ReverseSearchDocument.DefaultCoefficient, preferredKosherStatus.Count());
					ReverseSearchDocument.IncrementSpecificity(document, ReverseSearchDocument.DefaultCoefficient, preferredTempleAttendance.Count());
					ReverseSearchDocument.IncrementSpecificity(document, ReverseSearchDocument.DefaultCoefficient, preferredEthnicity.Count());
					ReverseSearchDocument.IncrementSpecificity(document, ReverseSearchDocument.DefaultCoefficient, preferredReligion.Count());
					ReverseSearchDocument.IncrementSpecificity(document, ReverseSearchDocument.DefaultCoefficient, preferredDrinking.Count());
					ReverseSearchDocument.IncrementSpecificity(document, ReverseSearchDocument.DefaultCoefficient, preferredActivityLevel.Count());

					ReverseSearchDocument.AssignSpecificity(document);

					var preferenceLocationCoordinates = new Coordinates(
						preferences.Latitude == 0 ? coordinates.Latitude : (Double)preferences.Latitude,
						preferences.Longitude == 0 ? coordinates.Longitude : (Double)preferences.Longitude,
						false
						);

					SetLocationPreferences(preferenceLocationCoordinates, preferences, document);

					SetMaxLocationPreferences(preferenceLocationCoordinates, 160, document);

					searchDataAdapter.Put(document, false);

					goodDocuments++;
				}
				catch (Exception exc)
				{
					Console.WriteLine("Exception encountered. " + exc.Message + ", " + exc.StackTrace);
					badDocuments++;
				}
				finally
				{
					documentCounter++;
				}
			}

			searchDataAdapter.CommitChanges();

			timer.Stop();

			Console.WriteLine(
				"{0} documents committed to index in {1} s. Total documents: {2}, Bad: {3}, Good: {4}",
				goodDocuments,
				GetTimeInSeconds(timer.ElapsedMilliseconds),
				documentCounter,
				badDocuments,
				goodDocuments
				);
		}

		private static void SetLocationPreferences(Coordinates coordinates, Preferences preference,
																 ReverseSearchDocument document)
		{
			CoordinateBox preferenceCoordinates = GeoDataConnector.GetBoxCoordinates(
				coordinates,
				preference.Distance > 0 ? preference.Distance : Constants.Profile.Default.Radius
				);

			if (preferenceCoordinates.TypeOfCoordinate == CoordinateType.Radians)
			{
				preferenceCoordinates.Convert();
			}

			document.PreferredLatitudeMinimum = preferenceCoordinates.LatitudeMinimum;

			document.PreferredLatitudeMaximum = preferenceCoordinates.LatitudeMaximum;

			document.PreferredLongitudeMinimum = preferenceCoordinates.LongitudeMinimum;

			document.PreferredLongitudeMaximum = preferenceCoordinates.LongitudeMaximum;
		}

		private static void SetMaxLocationPreferences(
			Coordinates coordinates, Int32 radius, ReverseSearchDocument document)
		{
			CoordinateBox preferenceCoordinates = GeoDataConnector.GetBoxCoordinates(coordinates, radius);

			if (preferenceCoordinates.TypeOfCoordinate == CoordinateType.Radians)
			{
				preferenceCoordinates.Convert();
			}

			document.PreferredLatitudeMinimum160 = preferenceCoordinates.LatitudeMinimum;

			document.PreferredLatitudeMaximum160 = preferenceCoordinates.LatitudeMaximum;

			document.PreferredLongitudeMinimum160 = preferenceCoordinates.LongitudeMinimum;

			document.PreferredLongitudeMaximum160 = preferenceCoordinates.LongitudeMaximum;
		}


		private void PopulateDictionaries(Int32 partition)
		{
			var timer = new Stopwatch();

			var data = new StageData();

			timer.Start();

			GenderMasks = data.GetGenderMasks(partition);
			Console.WriteLine("1. Gender Masks populated for partition {0}. Time: {1} s.",
									partition, GetTimeInSeconds(timer.ElapsedMilliseconds));
			LastLogins = data.GetLastLogins(partition);
			Console.WriteLine("2. Last Logins populated for partition {0}. Time: {1} s.",
									partition, GetTimeInSeconds(timer.ElapsedMilliseconds));
			MemberPreferences = data.GetPreferences(partition);
			Console.WriteLine("3. Member Preferences populated for partition {0}. Time: {1} s.",
									partition, GetTimeInSeconds(timer.ElapsedMilliseconds));
			Signups = data.GetSignups(partition);
			Console.WriteLine("4. Signups populated for partition {0}. Time: {1} s.",
									partition, GetTimeInSeconds(timer.ElapsedMilliseconds));
			MemberRegions = data.GetRegions(partition);
			Console.WriteLine("5. Member Regions populated for partition {0}. Time: {1} s.",
									partition, GetTimeInSeconds(timer.ElapsedMilliseconds));
			MembersWithPhotos = data.GetMembersWithPhotos(partition);
			Console.WriteLine("6. Members With Photos populated for partition {0}. Time: {1} s.",
									partition, GetTimeInSeconds(timer.ElapsedMilliseconds));

			timer.Stop();

			Console.WriteLine("Finished populating dictionaries for partition {0} in {1} s.",
									partition, GetTimeInSeconds(timer.ElapsedMilliseconds));
		}

		private static Double GetTimeInSeconds(Int64 milliseconds)
		{
			return milliseconds / (Double)1000;
		}
	}
}