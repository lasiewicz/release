﻿using System;
using System.IO;
using System.Text;

namespace Matchnet.SearchLoader.ReverseSearch
{
	public class DataRecorder : IDisposable
	{
		private static readonly Object lockObject = new Object();

		private readonly FileStream fs;

		private readonly StreamWriter sw;

		private Boolean disposed;

		private Int64 lineCounter;

		public DataRecorder(String path)
		{
			fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write);
			sw = new StreamWriter(fs, Encoding.Unicode);
		}

		#region IDisposable Members

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		#endregion

		public void Write(String line)
		{
			lock (lockObject)
			{
				lock (this)
				{
					sw.WriteLine(line);

					if (lineCounter++ % 20 == 0) sw.Flush();
				}
			}
		}

		private void Dispose(Boolean disposing)
		{
			if (!disposed)
			{
				if (disposing)
				{
					if (sw != null) sw.Dispose();
					if (fs != null) fs.Dispose();
				}
			}

			disposed = true;
		}


		~DataRecorder()
		{
			Dispose(false);
		}
	}
}