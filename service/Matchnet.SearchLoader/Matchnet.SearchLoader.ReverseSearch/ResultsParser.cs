﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Matchnet.SearchLoader.ReverseSearch.Core;
using Matchnet.SearchLoader.ReverseSearch.Query;

namespace Matchnet.SearchLoader.ReverseSearch
{
	public abstract class ResultsParser
	{
		protected const String Host = @"http://localhost:9999/solr/select?";
		private static readonly Searcher searcher = new Searcher();

		private readonly Dictionary<String, String> educationLevel = new Dictionary<String, String>
		                                                             {
		                                                             	{"Bachelors Degree", "Bachelor Degree"},
		                                                             	{"Masters Degree", "Master Degree"},
		                                                             	{"Ph.D./Postdoctoral", "Doctorate Degree"},
		                                                             	{"Doctorate", "Doctorate Degree"},
		                                                             	{"Associates Degree", "Associate Degree"},
		                                                             };

		protected abstract String CreateUrl(SearchDocument document);

		public Int32 GetTotal(XElement results)
		{
			IEnumerable<string> total = from r in results.Descendants("result")
												 select r.Attribute("numFound").Value;

			return Int32.Parse(total.First());
		}

		public String GetFormattedResults(String line)
		{
			SearchDocument searchDocument = GetSearchDocument(line);

			return FormatResults(searchDocument.MemberId, GetResults(searchDocument));
		}

		public String GetFormattedResults(String line, Dictionary<Int32, Int32> memberActivityLevel,
													 Dictionary<Int32, String> activityLookup)
		{
			SearchDocument searchDocument = GetSearchDocument(line);

			searchDocument.ActivityLevel = memberActivityLevel.ContainsKey(searchDocument.MemberId)
														?
															activityLookup[memberActivityLevel[searchDocument.MemberId]]
														: Constants.WillTellYouLater;

			return FormatResults(searchDocument.MemberId, GetResults(searchDocument));
		}

		public XElement GetResults(String line)
		{
			return GetResults(GetSearchDocument(line));
		}

		public String FormatResults(Int32 memberId, XElement results)
		{
			return String.Format(
				"{0},{1},{2}",
				memberId,
				GetTotal(results),
				String.Join("-", GetMemberIds(results).Select(i => i.ToString()).ToArray())
				);
		}

		public IEnumerable<Int32> GetMemberIds(XElement results)
		{
			return from memberId in results.Descendants("int")
					 where memberId.Attribute("name").Value == "id"
					 select Int32.Parse(memberId.Value.Split(new[] { '-' })[1]);
		}

		protected SearchDocument GetSearchDocument(String line)
		{
			string[] attributes = GetAttributes(line);

			return new SearchDocument
			{
				MemberId = Int32.Parse(attributes[0]),
				Age = Int32.Parse(attributes[1]),
				Latitude = Double.Parse(attributes[2]),
				Longitude = Double.Parse(attributes[3]),
				TempleStatus = attributes[4],
				SmokingHabits = attributes[5],
				DrinkingHabits = attributes[6],
				Education = attributes[7],
				KeepKosher = attributes[8],
				JDateReligion = attributes[9],
				Ethnicity = attributes[10],
				Gender = attributes[11],
				SeekingGender = attributes[12],
				HaveChildren = attributes[13],
				Relationship = attributes[14],
				Relocate = attributes[15],
				Languages = attributes[16],
			};
		}

		private static String[] GetAttributes(String line)
		{
			return line.Split(',');
		}

		public XElement GetResults(SearchDocument document)
		{
			return searcher.Get(CreateUrl(document));
		}

		protected void AddGender(StringBuilder queryBuilder, SearchDocument document)
		{
			queryBuilder.Append(String.Format("q=seekinggender%3A{0}", ReverseSearchDocument.FormatValue(document.Gender)));
		}

		protected void AddAge(StringBuilder queryBuilder, SearchDocument document)
		{
			queryBuilder.Append(String.Format("+AND+preferredagemin%3A[18+TO+{0}]+AND+preferredagemax%3A[{0}+TO+99]",
														 document.Age));
		}

		protected void AddAgePlusMinus5(StringBuilder queryBuilder, SearchDocument document)
		{
			queryBuilder.Append(
				String.Format(
					"+AND+preferredagemin%3A[18+TO+{0}]+AND+preferredagemax%3A[{1}+TO+99]",
					document.Age + 5,
					document.Age - 5
					)
				);
		}

		protected void AddLocation(StringBuilder queryBuilder, SearchDocument document)
		{
			queryBuilder.Append(
				String.Format(
					"+AND+preferredlatitudemin%3A[-90+TO+{0}]+AND+preferredlatitudemax%3A[{0}+TO+90]",
					document.Latitude
					)
				);

			queryBuilder.Append(
				String.Format(
					"+AND+preferredlongitudemin%3A[-180+TO+{0}]+AND+preferredlongitudemax%3A[{0}+TO+180]",
					document.Longitude
					)
				);
		}

		protected void AddLocation160Miles(StringBuilder queryBuilder, SearchDocument document)
		{
			queryBuilder.Append(
				String.Format(
					"+AND+preferredlatitudemin160%3A[-90+TO+{0}]+AND+preferredlatitudemax160%3A[{0}+TO+90]",
					document.Latitude
					)
				);

			queryBuilder.Append(
				String.Format(
					"+AND+preferredlongitudemin160%3A[-180+TO+{0}]+AND+preferredlongitudemax160%3A[{0}+TO+180]",
					document.Longitude
					)
				);
		}

		protected void AddTempleStatus(StringBuilder queryBuilder, SearchDocument document)
		{
			if (document.TempleStatus != Constants.WillTellYouLater)
				queryBuilder.Append(String.Format("+AND+preferredtempleattendance%3A\"{0}\"",
															 ReverseSearchDocument.FormatValue(document.TempleStatus)));
		}

		protected void AddSmokingStatus(StringBuilder queryBuilder, SearchDocument document)
		{
			if (document.SmokingHabits != Constants.WillTellYouLater)
				queryBuilder.Append(
					String.Format(
						"+AND+preferredsmokingstatus%3A\"{0}\"",
						ReverseSearchDocument.FormatValue(document.SmokingHabits).Replace("nonsmoker", "non smoker")
						)
					);
		}

		protected void AddKeepKosherStatus(StringBuilder queryBuilder, SearchDocument document)
		{
			if (document.KeepKosher != Constants.WillTellYouLater)
				queryBuilder.Append(String.Format("+AND+preferredkeepkosher%3A\"{0}\"",
															 ReverseSearchDocument.FormatValue(document.KeepKosher)));
		}

		protected void AddJDateReligion(StringBuilder queryBuilder, SearchDocument document)
		{
			if (document.JDateReligion != Constants.WillTellYouLater)
				queryBuilder.Append(String.Format("+AND+preferredreligion%3A\"{0}\"",
															 ReverseSearchDocument.FormatValue(document.JDateReligion)));
		}

		protected void AddEducation(StringBuilder queryBuilder, SearchDocument document)
		{
			string education = ReverseSearchDocument.FormatValue(document.Education);
			if (education.Contains("ph.d")) education = "doctorate";
			if (document.Education != Constants.WillTellYouLater)
				queryBuilder.Append(
					String.Format("+AND+preferrededucation%3A\"{0}\"",
									  education
						)
					);
		}

		protected void AddHasPhotos(StringBuilder queryBuilder, SearchDocument document)
		{
			queryBuilder.Append("+AND+hasphoto%3A\true");
		}

		protected void AddActivityLevel(StringBuilder queryBuilder, SearchDocument document)
		{
			if (document.ActivityLevel != Constants.WillTellYouLater)
				queryBuilder.Append(
					String.Format("+AND+preferredactivitylevel%3A\"{0}\"",
									  ReverseSearchDocument.FormatValue(document.ActivityLevel)
						)
					);
		}

		protected void AddDrinkingStatus(StringBuilder queryBuilder, SearchDocument document)
		{
			if (document.DrinkingHabits != Constants.WillTellYouLater)
				queryBuilder.Append(String.Format("+AND+preferreddrinking%3A\"{0}\"",
															 ReverseSearchDocument.FormatValue(document.DrinkingHabits)));
		}

		protected void AddMaritalStatus(StringBuilder queryBuilder, SearchDocument document)
		{
			if (document.Relationship != Constants.WillTellYouLater)
				queryBuilder.Append(String.Format("+AND+preferredmaritalstatus%3A\"{0}\"",
															 ReverseSearchDocument.FormatValue(document.Relationship)));
		}

		protected void AddHaveChildren(StringBuilder queryBuilder, SearchDocument document)
		{
			string haveChildren = ReverseSearchDocument.FormatValue(document.HaveChildren);

			if (haveChildren != "any" && haveChildren != Constants.WillTellYouLater)
				queryBuilder.Append(String.Format("+AND+preferredwantschildren%3A\"{0}\"",
															 haveChildren));
		}
	}
}