﻿using System;
using System.Collections.Generic;
using Matchnet.SearchLoader.ReverseSearch.Enumerations;

namespace Matchnet.SearchLoader.ReverseSearch
{
	public class EnumAdapter
	{
		#region Drinking

		private readonly Dictionary<String, Drinking> drinking = new Dictionary<String, Drinking>
		{
			{String.Empty, Drinking.Will_tell_you_later},

			{"N", Drinking.Never},
			{"n", Drinking.Never},
			{"Ne", Drinking.Never},
			{"ne", Drinking.Never},
			{"Nev", Drinking.Never},
			{"nev", Drinking.Never},
			{"Neve", Drinking.Never},
			{"neve", Drinking.Never},
			{"Never", Drinking.Never},
			{"never", Drinking.Never},
			{"Never|", Drinking.Never},
			{"never|", Drinking.Never},

			{"f", Drinking.Frequently},
			{"F", Drinking.Frequently},
			{"fr", Drinking.Frequently},
			{"Fr", Drinking.Frequently},
			{"fre", Drinking.Frequently},
			{"Fre", Drinking.Frequently},
			{"freq", Drinking.Frequently},
			{"Freq", Drinking.Frequently},
			{"frequently", Drinking.Frequently},
			{"Frequently", Drinking.Frequently},
			{"frequently|", Drinking.Frequently},
			{"Frequently|", Drinking.Frequently},

			{"O", Drinking.Occasionally},
			{"o", Drinking.Occasionally},
			{"Oc", Drinking.Occasionally},
			{"oc", Drinking.Occasionally},
			{"Occ", Drinking.Occasionally},
			{"occ", Drinking.Occasionally},
			{"Occa", Drinking.Occasionally},
			{"occa", Drinking.Occasionally},
			{"Occasionally", Drinking.Occasionally},
			{"occasionally", Drinking.Occasionally},
			{"Occasionally|", Drinking.Occasionally},
			{"occasionally|", Drinking.Occasionally},

			{"s", Drinking.Socially},
			{"S", Drinking.Socially},
			{"so", Drinking.Socially},
			{"So", Drinking.Socially},
			{"Soc", Drinking.Socially},
			{"Soci", Drinking.Socially},
			{"soci", Drinking.Socially},
			{"Socially|", Drinking.Socially},
			{"socially|", Drinking.Socially},
			{"Socially", Drinking.Socially},
			{"socially", Drinking.Socially},
		};

		#endregion Drinking

		#region Education

		private readonly Dictionary<String, Education> education = new Dictionary<String, Education>
		{
			{String.Empty, Education.High_School},

			{"ass", Education.Associate_Degree},
			{"Ass", Education.Associate_Degree},
			{"asso", Education.Associate_Degree},
			{"Asso", Education.Associate_Degree},
			{"Associate_Degree|", Education.Associate_Degree},
			{"associate_degree|", Education.Associate_Degree},
			{"Associate Degree|", Education.Associate_Degree},
			{"associate degree|", Education.Associate_Degree},
			{"Associate-Degree|", Education.Associate_Degree},
			{"associate-degree|", Education.Associate_Degree},
			{"Associate_Degree", Education.Associate_Degree},
			{"associate_degree", Education.Associate_Degree},
			{"Associate Degree", Education.Associate_Degree},
			{"associate degree", Education.Associate_Degree},
			{"Associate-Degree", Education.Associate_Degree},
			{"associate-degree", Education.Associate_Degree},

			{"B", Education.Bachelor_Degree},
			{"b", Education.Bachelor_Degree},
			{"Ba", Education.Bachelor_Degree},
			{"ba", Education.Bachelor_Degree},
			{"Bachelor_Degree|", Education.Bachelor_Degree},
			{"bachelor_degree|", Education.Bachelor_Degree},
			{"Bachelor Degree", Education.Bachelor_Degree},
			{"bachelor degree", Education.Bachelor_Degree},
			{"Bachelor Degree|", Education.Bachelor_Degree},
			{"bachelor degree|", Education.Bachelor_Degree},
			{"BachelorDegree", Education.Bachelor_Degree},
			{"bachelordegree", Education.Bachelor_Degree},

			{"Do", Education.Doctorate_Degree},
			{"do", Education.Doctorate_Degree},
			{"Doctorate Degree", Education.Doctorate_Degree},
			{"doctorate degree", Education.Doctorate_Degree},
			{"Doctorate_Degree", Education.Doctorate_Degree},
			{"doctorate_degree", Education.Doctorate_Degree},
			{"DoctorateDegree", Education.Doctorate_Degree},
			{"doctoratedegree", Education.Doctorate_Degree},

			{"E", Education.Elementary},
			{"e", Education.Elementary},
			{"El", Education.Elementary},
			{"el", Education.Elementary},
			{"Ele", Education.Elementary},
			{"ele", Education.Elementary},
			{"Elem", Education.Elementary},
			{"elem", Education.Elementary},
			{"Elementary|", Education.Elementary},
			{"elementary|", Education.Elementary},
			{"Elementary", Education.Elementary},
			{"elementary", Education.Elementary},

			{"h", Education.High_School},
			{"H", Education.High_School},
			{"hi", Education.High_School},
			{"Hi", Education.High_School},
			{"hig", Education.High_School},
			{"Hig", Education.High_School},
			{"high", Education.High_School},
			{"High", Education.High_School},
			{"high school", Education.High_School},
			{"highschool", Education.High_School},
			{"HighSchool", Education.High_School},
			{"High School", Education.High_School},
			{"High_School", Education.High_School},
			{"high_school", Education.High_School},

			{"Higher", Education.Higher_National_Diploma},
			{"higher", Education.Higher_National_Diploma},
			{"Higher_National_Diploma", Education.Higher_National_Diploma},
			{"higher_national_diploma", Education.Higher_National_Diploma},
			{"Higher National Diploma", Education.Higher_National_Diploma},
			{"higher national diploma", Education.Higher_National_Diploma},
			{"HigherNationalDiploma", Education.Higher_National_Diploma},
			{"highernationaldiploma", Education.Higher_National_Diploma},
			{"Higher_National_Diploma|", Education.Higher_National_Diploma},
			{"higher_national_diploma|", Education.Higher_National_Diploma},
			{"Higher National Diploma|", Education.Higher_National_Diploma},
			{"higher national diploma|", Education.Higher_National_Diploma},
			{"HigherNationalDiploma|", Education.Higher_National_Diploma},
			{"highernationaldiploma|", Education.Higher_National_Diploma},

			{"m", Education.Master_Degree},
			{"M", Education.Master_Degree},
			{"ma", Education.Master_Degree},
			{"Ma", Education.Master_Degree},
			{"mas", Education.Master_Degree},
			{"Mas", Education.Master_Degree},
			{"mast", Education.Master_Degree},
			{"Mast", Education.Master_Degree},
			{"Master_Degree", Education.Master_Degree},
			{"master_degree", Education.Master_Degree},
			{"MasterDegree", Education.Master_Degree},
			{"masterdegree", Education.Master_Degree},
			{"Master Degree", Education.Master_Degree},
			{"master degree", Education.Master_Degree},
			{"Master_Degree|", Education.Master_Degree},
			{"master_degree|", Education.Master_Degree},
			{"MasterDegree|", Education.Master_Degree},
			{"masterdegree|", Education.Master_Degree},
			{"Master Degree|", Education.Master_Degree},
			{"master degree|", Education.Master_Degree},

			{"s", Education.Some_College},
			{"S", Education.Some_College},
			{"so", Education.Some_College},
			{"So", Education.Some_College},
			{"som", Education.Some_College},
			{"Som", Education.Some_College},
			{"some", Education.Some_College},
			{"Some", Education.Some_College},
			{"Some_College|", Education.Some_College},
			{"Some College|", Education.Some_College},
			{"Some-College|", Education.Some_College},
			{"some_college|", Education.Some_College},
			{"some college|", Education.Some_College},
			{"some-college|", Education.Some_College},
			{"Some_College", Education.Some_College},
			{"Some College", Education.Some_College},
			{"Some-College", Education.Some_College},
			{"some_college", Education.Some_College},
			{"some college", Education.Some_College},
			{"some-college", Education.Some_College},
			{"somecollege", Education.Some_College},
			{"SomeCollege", Education.Some_College},
		};

		#endregion Education

		#region Ethnicity

		private readonly Dictionary<String, Ethnicity> ethnicity = new Dictionary<String, Ethnicity>
		{
			{String.Empty, Ethnicity.Will_tell_you_later},

			{"an", Ethnicity.Another},
			{"An", Ethnicity.Another},
			{"ano", Ethnicity.Another},
			{"Ano", Ethnicity.Another},
			{"anot", Ethnicity.Another},
			{"Anot", Ethnicity.Another},
			{"another", Ethnicity.Another},
			{"Another", Ethnicity.Another},
			{"another|", Ethnicity.Another},
			{"Another|", Ethnicity.Another},

			{"As", Ethnicity.Ashkenazi},
			{"as", Ethnicity.Ashkenazi},
			{"Ash", Ethnicity.Ashkenazi},
			{"ash", Ethnicity.Ashkenazi},
			{"Ashk", Ethnicity.Ashkenazi},
			{"ashk", Ethnicity.Ashkenazi},
			{"ashkenazi", Ethnicity.Ashkenazi},
			{"Ashkenazi", Ethnicity.Ashkenazi},
			{"ashkenazi|", Ethnicity.Ashkenazi},
			{"Ashkenazi|", Ethnicity.Ashkenazi},

			{"m", Ethnicity.Mixed},
			{"M", Ethnicity.Mixed},
			{"mi", Ethnicity.Mixed},
			{"Mi", Ethnicity.Mixed},
			{"mix", Ethnicity.Mixed},
			{"Mix", Ethnicity.Mixed},
			{"mixe", Ethnicity.Mixed},
			{"Mixe", Ethnicity.Mixed},
			{"mixed", Ethnicity.Mixed},
			{"Mixed", Ethnicity.Mixed},
			{"mixed|", Ethnicity.Mixed},
			{"Mixed|", Ethnicity.Mixed},

			{"s", Ethnicity.Sephardic},
			{"S", Ethnicity.Sephardic},
			{"se", Ethnicity.Sephardic},
			{"Se", Ethnicity.Sephardic},
			{"sep", Ethnicity.Sephardic},
			{"Sep", Ethnicity.Sephardic},
			{"seph", Ethnicity.Sephardic},
			{"Seph", Ethnicity.Sephardic},
			{"sephardic", Ethnicity.Sephardic},
			{"Sephardic", Ethnicity.Sephardic},
			{"sephardic|", Ethnicity.Sephardic},
			{"Sephardic|", Ethnicity.Sephardic},

			{"u", Ethnicity.Unknown},
			{"U", Ethnicity.Unknown},
			{"un", Ethnicity.Unknown},
			{"Un", Ethnicity.Unknown},
			{"unk", Ethnicity.Unknown},
			{"Unk", Ethnicity.Unknown},
			{"unkn", Ethnicity.Unknown},
			{"Unkn", Ethnicity.Unknown},
			{"unknown", Ethnicity.Unknown},
			{"Unknown", Ethnicity.Unknown},
			{"unknown|", Ethnicity.Unknown},
			{"Unknown|", Ethnicity.Unknown},

			{"will", Ethnicity.Will_tell_you_later},
			{"Will", Ethnicity.Will_tell_you_later},
			{"w", Ethnicity.Will_tell_you_later},
			{"W", Ethnicity.Will_tell_you_later},
			{"Will tell you later", Ethnicity.Will_tell_you_later},
			{"Will tell you later|", Ethnicity.Will_tell_you_later},
			{"will tell you later", Ethnicity.Will_tell_you_later},
			{"will tell you later|", Ethnicity.Will_tell_you_later},
		};

		#endregion Ethnicity

		#region KeepKosher

		private readonly Dictionary<String, KeepKosher> keepKosher = new Dictionary<String, KeepKosher>
		{
			{String.Empty, KeepKosher.None},

			{"n", KeepKosher.None},
			{"N", KeepKosher.None},
			{"no", KeepKosher.None},
			{"No", KeepKosher.None},
			{"non", KeepKosher.None},
			{"Non", KeepKosher.None},
			{"none", KeepKosher.None},
			{"None", KeepKosher.None},
			{"none|", KeepKosher.None},
			{"None|", KeepKosher.None},
			{"not", KeepKosher.None},
			{"Not", KeepKosher.None},
			{"not at all", KeepKosher.None},
			{"Not at all", KeepKosher.None},
			{"not at all|", KeepKosher.None},
			{"Not at all|", KeepKosher.None},

			{"home_on", KeepKosher.Home_Only},
			{"Home_On", KeepKosher.Home_Only},
			{"Home_on", KeepKosher.Home_Only},
			{"Home_only", KeepKosher.Home_Only},
			{"home_only", KeepKosher.Home_Only},
			{"Home_only|", KeepKosher.Home_Only},
			{"home_only|", KeepKosher.Home_Only},
			{"Home only", KeepKosher.Home_Only},
			{"home only", KeepKosher.Home_Only},
			{"Home only|", KeepKosher.Home_Only},
			{"home only|", KeepKosher.Home_Only},
			{"at home only", KeepKosher.Home_Only},
			{"At home only", KeepKosher.Home_Only},
			{"at_home_only", KeepKosher.Home_Only},
			{"At_home_only", KeepKosher.Home_Only},
			{"at home only|", KeepKosher.Home_Only},
			{"At home only|", KeepKosher.Home_Only},
			{"at_home_only|", KeepKosher.Home_Only},
			{"At_home_only|", KeepKosher.Home_Only},

			{"At home and outside", KeepKosher.Home_Outside},
			{"At home and outside|", KeepKosher.Home_Outside},
			{"at home and outside", KeepKosher.Home_Outside},
			{"at home and outside|", KeepKosher.Home_Outside},
			{"Athomeandoutside", KeepKosher.Home_Outside},
			{"athomeandoutside", KeepKosher.Home_Outside},
			{"Home_Outside", KeepKosher.Home_Outside},
			{"Home_outside", KeepKosher.Home_Outside},
			{"home_outside", KeepKosher.Home_Outside},
			{"home_outside|", KeepKosher.Home_Outside},
			{"Home Outside", KeepKosher.Home_Outside},
			{"Home outside", KeepKosher.Home_Outside},
			{"home outside", KeepKosher.Home_Outside},

			{"veg", KeepKosher.Home_Vegetarian_Outside},
			{"Veg", KeepKosher.Home_Vegetarian_Outside},
			{"v", KeepKosher.Home_Vegetarian_Outside},
			{"V", KeepKosher.Home_Vegetarian_Outside},
			{"ve", KeepKosher.Home_Vegetarian_Outside},
			{"Ve", KeepKosher.Home_Vegetarian_Outside},
			{"vege", KeepKosher.Home_Vegetarian_Outside},
			{"Vege", KeepKosher.Home_Vegetarian_Outside},
			{"vegetarian", KeepKosher.Home_Vegetarian_Outside},
			{"Vegetarian", KeepKosher.Home_Vegetarian_Outside},
			{"vegetarian outside", KeepKosher.Home_Vegetarian_Outside},
			{"Vegetarian outside", KeepKosher.Home_Vegetarian_Outside},
			{"Vegetarian Outside", KeepKosher.Home_Vegetarian_Outside},

			{"s", KeepKosher.Some_Degree},
			{"S", KeepKosher.Some_Degree},
			{"so", KeepKosher.Some_Degree},
			{"So", KeepKosher.Some_Degree},
			{"Som", KeepKosher.Some_Degree},
			{"som", KeepKosher.Some_Degree},
			{"some", KeepKosher.Some_Degree},
			{"Some", KeepKosher.Some_Degree},
			{"some degree", KeepKosher.Some_Degree},
			{"Some degree", KeepKosher.Some_Degree},
			{"Some Degree", KeepKosher.Some_Degree},
			{"some_degree", KeepKosher.Some_Degree},
			{"Some_degree", KeepKosher.Some_Degree},
			{"Some_Degree", KeepKosher.Some_Degree},
			{"To some degree|", KeepKosher.Some_Degree},
			{"To some degree", KeepKosher.Some_Degree},
			{"to some degree", KeepKosher.Some_Degree},

			{"will", KeepKosher.Will_tell_you_later},
			{"Will", KeepKosher.Will_tell_you_later},
			{"w", KeepKosher.Will_tell_you_later},
			{"W", KeepKosher.Will_tell_you_later},
			{"Will tell you later", KeepKosher.Will_tell_you_later},
			{"Will tell you later|", KeepKosher.Will_tell_you_later},
			{"will tell you later", KeepKosher.Will_tell_you_later},
			{"will tell you later|", KeepKosher.Will_tell_you_later},
		};

		#endregion KeepKosher

		#region Language

		private readonly Dictionary<String, Language> language = new Dictionary<String, Language>
		{
			{String.Empty, Language.English},

			{"arabic", Language.Arabic},
			{"Arabic", Language.Arabic},

			{"bengali", Language.Bengali},
			{"Bengali", Language.Bengali},

			{"bulgarian", Language.Bulgarian},
			{"Bulgarian", Language.Bulgarian},

			{"chinese", Language.Chinese},
			{"Chinese", Language.Chinese},

			{"czech", Language.Czech},
			{"Czech", Language.Czech},

			{"dutch", Language.Dutch},
			{"Dutch", Language.Dutch},

			{"english", Language.English},
			{"English", Language.English},

			{"farsi", Language.Farsi},
			{"Farsi", Language.Farsi},
			{"persian", Language.Farsi},
			{"Persian", Language.Farsi},

			{"Finnish", Language.Finnish},

			{"French", Language.French},

			{"German", Language.German},

			{"Greek", Language.Greek},

			{"heb", Language.Hebrew},
			{"Heb", Language.Hebrew},
			{"hebrew", Language.Hebrew},
			{"Hebrew", Language.Hebrew},
			{"hebrew|", Language.Hebrew},
			{"Hebrew|", Language.Hebrew},

			{"Hindi", Language.Hindi},

			{"Italian", Language.Italian},

			{"Japanese", Language.Japanese},

			{"Korean", Language.Korean},

			{"Malay", Language.Malay},

			{"Norwegian", Language.Norwegian},

			{"Other", Language.Other},

			{"Polish", Language.Polish},

			{"Portuguese", Language.Portuguese},

			{"Romanian", Language.Romanian},

			{"Russian", Language.Russian},

			{"Spanish", Language.Spanish},

			{"Swedish", Language.Swedish},

			{"Tagalog", Language.Tagalog},

			{"Thai", Language.Thai},

			{"Urdu", Language.Urdu},

			{"Vietnamese", Language.Vietnamese},

			{"Yiddish", Language.Yiddish},

		};

		#endregion Language

		#region MaritalStatus

		private readonly Dictionary<String, MaritalStatus> maritalStatus = new Dictionary<String, MaritalStatus>
		{
			{String.Empty, MaritalStatus.Single},

			{"d", MaritalStatus.Divorced},
			{"D", MaritalStatus.Divorced},
			{"di", MaritalStatus.Divorced},
			{"Di", MaritalStatus.Divorced},
			{"div", MaritalStatus.Divorced},
			{"Div", MaritalStatus.Divorced},
			{"divorce", MaritalStatus.Divorced},
			{"Divorce", MaritalStatus.Divorced},
			{"divorced", MaritalStatus.Divorced},
			{"Divorced", MaritalStatus.Divorced},
			{"divorced|", MaritalStatus.Divorced},
			{"Divorced|", MaritalStatus.Divorced},

			{"m", MaritalStatus.Married},
			{"M", MaritalStatus.Married},
			{"ma", MaritalStatus.Married},
			{"Ma", MaritalStatus.Married},
			{"mar", MaritalStatus.Married},
			{"Mar", MaritalStatus.Married},
			{"marr", MaritalStatus.Married},
			{"Marr", MaritalStatus.Married},
			{"marri", MaritalStatus.Married},
			{"Marri", MaritalStatus.Married},
			{"married", MaritalStatus.Married},
			{"Married", MaritalStatus.Married},
			{"married|", MaritalStatus.Married},
			{"Married|", MaritalStatus.Married},

			{"se", MaritalStatus.Separated},
			{"Se", MaritalStatus.Separated},
			{"sep", MaritalStatus.Separated},
			{"Sep", MaritalStatus.Separated},
			{"sepa", MaritalStatus.Separated},
			{"Sepa", MaritalStatus.Separated},
			{"separated", MaritalStatus.Separated},
			{"Separated", MaritalStatus.Separated},
			{"separated|", MaritalStatus.Separated},
			{"Separated|", MaritalStatus.Separated},

			{"si", MaritalStatus.Single},
			{"Si", MaritalStatus.Single},
			{"sin", MaritalStatus.Single},
			{"Sin", MaritalStatus.Single},
			{"single", MaritalStatus.Single},
			{"Single", MaritalStatus.Single},
			{"single|", MaritalStatus.Single},
			{"Single|", MaritalStatus.Single},

			{"w", MaritalStatus.Widowed},
			{"W", MaritalStatus.Widowed},
			{"wi", MaritalStatus.Widowed},
			{"Wi", MaritalStatus.Widowed},
			{"wid", MaritalStatus.Widowed},
			{"Wid", MaritalStatus.Widowed},
			{"wido", MaritalStatus.Widowed},
			{"Wido", MaritalStatus.Widowed},
			{"widow", MaritalStatus.Widowed},
			{"Widow", MaritalStatus.Widowed},
			{"widowed", MaritalStatus.Widowed},
			{"Widowed", MaritalStatus.Widowed},
			{"widowed|", MaritalStatus.Widowed},
			{"Widowed|", MaritalStatus.Widowed},
		};

		#endregion MaritalStatus

		#region Relocate

		private readonly Dictionary<String, Relocate> relocate = new Dictionary<String, Relocate>
		{
			{String.Empty, Relocate.No},

			{"N", Relocate.No},
			{"n", Relocate.No},
			{"No", Relocate.No},
			{"no", Relocate.No},

			{"un", Relocate.Unsure},
			{"Un", Relocate.Unsure},
			{"Unsure", Relocate.Unsure},
			{"unsure", Relocate.Unsure},

			{"Y", Relocate.Yes},
			{"y", Relocate.Yes},
			{"Ye", Relocate.Yes},
			{"ye", Relocate.Yes},
			{"Yes", Relocate.Yes},
			{"yes", Relocate.Yes},
		};

		#endregion Relocate

		#region Smoking

		private readonly Dictionary<String, Smoking> smoking = new Dictionary<String, Smoking>
		{
			{String.Empty, Smoking.Will_tell_you_later},

			{"Non", Smoking.Non_Smoker},
			{"non", Smoking.Non_Smoker},
			{"Non ", Smoking.Non_Smoker},
			{"non ", Smoking.Non_Smoker},
			{"Non_", Smoking.Non_Smoker},
			{"non_", Smoking.Non_Smoker},
			{"Nons", Smoking.Non_Smoker},
			{"nons", Smoking.Non_Smoker},
			{"NonS", Smoking.Non_Smoker},
			{"nonS", Smoking.Non_Smoker},
			{"Non-Smoker|", Smoking.Non_Smoker},
			{"Non Smoker|", Smoking.Non_Smoker},
			{"Non_Smoker", Smoking.Non_Smoker},
			{"Non-Smoker", Smoking.Non_Smoker},
			{"Non Smoker", Smoking.Non_Smoker},
			{"non-smoker|", Smoking.Non_Smoker},
			{"non-smoker", Smoking.Non_Smoker},
			{"non smoker", Smoking.Non_Smoker},
			{"non smoker|", Smoking.Non_Smoker},
			{"non_smoker", Smoking.Non_Smoker},

			{"Occ", Smoking.Occasional_Smoker},
			{"occ", Smoking.Occasional_Smoker},
			{"Occasional Smoker|", Smoking.Occasional_Smoker},
			{"Occasional_Smoker|", Smoking.Occasional_Smoker},
			{"OccasionalSmoker|", Smoking.Occasional_Smoker},
			{"Occasional-Smoker|", Smoking.Occasional_Smoker},
			{"Occasional Smoker", Smoking.Occasional_Smoker},
			{"Occasional-Smoker", Smoking.Occasional_Smoker},
			{"Occasional_Smoker", Smoking.Occasional_Smoker},
			{"OccasionalSmoker", Smoking.Occasional_Smoker},
			{"occasional smoker|", Smoking.Occasional_Smoker},
			{"occasional_smoker|", Smoking.Occasional_Smoker},
			{"occasionalsmoker|", Smoking.Occasional_Smoker},
			{"occasional-smoker|", Smoking.Occasional_Smoker},
			{"occasional smoker", Smoking.Occasional_Smoker},
			{"occasional-smoker", Smoking.Occasional_Smoker},
			{"occasional_smoker", Smoking.Occasional_Smoker},
			{"occasionalsmoker", Smoking.Occasional_Smoker},
			
			{"Smo", Smoking.Smokes_Regularly},
			{"smo", Smoking.Smokes_Regularly},
			{"Smokes Regularly|", Smoking.Smokes_Regularly},
			{"Smokes-Regularly|", Smoking.Smokes_Regularly},
			{"Smokes_Regularly|", Smoking.Smokes_Regularly},
			{"SmokesRegularly|", Smoking.Smokes_Regularly},
			{"Smokes Regularly", Smoking.Smokes_Regularly},
			{"Smokes_Regularly", Smoking.Smokes_Regularly},
			{"Smokes-Regularly", Smoking.Smokes_Regularly},
			{"SmokesRegularly", Smoking.Smokes_Regularly},
			{"smokes regularly|", Smoking.Smokes_Regularly},
			{"smokes-regularly|", Smoking.Smokes_Regularly},
			{"smokes_regularly|", Smoking.Smokes_Regularly},
			{"smokesregularly|", Smoking.Smokes_Regularly},
			{"smokes regularly", Smoking.Smokes_Regularly},
			{"smokes_regularly", Smoking.Smokes_Regularly},
			{"smokes-regularly", Smoking.Smokes_Regularly},
			{"smokesregularly", Smoking.Smokes_Regularly},
			
			{"Try", Smoking.Trying_To_Quit},
			{"try", Smoking.Trying_To_Quit},
			{"Trying to quit|", Smoking.Trying_To_Quit},
			{"Tryingtoquit|", Smoking.Trying_To_Quit},
			{"Trying_to_quit|", Smoking.Trying_To_Quit},
			{"Trying-to-quit|", Smoking.Trying_To_Quit},
			{"trying to quit|", Smoking.Trying_To_Quit},
			{"trying-to-quit|", Smoking.Trying_To_Quit},
			{"trying_to_quit|", Smoking.Trying_To_Quit},
			{"Trying to quit", Smoking.Trying_To_Quit},
			{"Trying_to_quit", Smoking.Trying_To_Quit},
			{"Trying-to-quit", Smoking.Trying_To_Quit},
			{"Tryingtoquit", Smoking.Trying_To_Quit},
			{"trying to quit", Smoking.Trying_To_Quit},
			{"trying-to-quit", Smoking.Trying_To_Quit},
			{"trying_to_quit", Smoking.Trying_To_Quit},
			{"tryingtoquit", Smoking.Trying_To_Quit},
		};

		#endregion Smoking

		#region TempleAttendance

		private readonly Dictionary<String, TempleAttendance> templeAttendance = new Dictionary<String, TempleAttendance>
		{
			{String.Empty, TempleAttendance.Will_tell_you_later},

			{"eve", TempleAttendance.Every_shabbat},
			{"Eve", TempleAttendance.Every_shabbat},
			{"ever", TempleAttendance.Every_shabbat},
			{"Ever", TempleAttendance.Every_shabbat},
			{"Every shabbat|", TempleAttendance.Every_shabbat},
			{"every shabbat|", TempleAttendance.Every_shabbat},
			{"Every_shabbat|", TempleAttendance.Every_shabbat},
			{"every_shabbat|", TempleAttendance.Every_shabbat},
			{"Every-shabbat|", TempleAttendance.Every_shabbat},
			{"every-shabbat|", TempleAttendance.Every_shabbat},
			{"Everyshabbat|", TempleAttendance.Every_shabbat},
			{"everyshabbat|", TempleAttendance.Every_shabbat},
			{"Every shabbat", TempleAttendance.Every_shabbat},
			{"every shabbat", TempleAttendance.Every_shabbat},
			{"Every_shabbat", TempleAttendance.Every_shabbat},
			{"every_shabbat", TempleAttendance.Every_shabbat},
			{"Every-shabbat", TempleAttendance.Every_shabbat},
			{"every-shabbat", TempleAttendance.Every_shabbat},
			{"Everyshabbat", TempleAttendance.Every_shabbat},
			{"everyshabbat", TempleAttendance.Every_shabbat},

			{"nev", TempleAttendance.Never},
			{"Nev", TempleAttendance.Never},
			{"neve", TempleAttendance.Never},
			{"Neve", TempleAttendance.Never},
			{"Never", TempleAttendance.Never},
			{"never", TempleAttendance.Never},
			{"Never|", TempleAttendance.Never},
			{"never|", TempleAttendance.Never},

			{"Onh", TempleAttendance.On_high_holidays},
			{"onh", TempleAttendance.On_high_holidays},
			{"On_h", TempleAttendance.On_high_holidays},
			{"on_h", TempleAttendance.On_high_holidays},
			{"On h", TempleAttendance.On_high_holidays},
			{"on h", TempleAttendance.On_high_holidays},
			{"Onhi", TempleAttendance.On_high_holidays},
			{"onhi", TempleAttendance.On_high_holidays},
			{"On_high_holidays|", TempleAttendance.On_high_holidays},
			{"on_high_holidays|", TempleAttendance.On_high_holidays},
			{"On high holidays|", TempleAttendance.On_high_holidays},
			{"on high holidays|", TempleAttendance.On_high_holidays},
			{"On-high-holidays|", TempleAttendance.On_high_holidays},
			{"on-high-holidays|", TempleAttendance.On_high_holidays},
			{"On_high_holidays", TempleAttendance.On_high_holidays},
			{"on_high_holidays", TempleAttendance.On_high_holidays},
			{"On high holidays", TempleAttendance.On_high_holidays},
			{"on high holidays", TempleAttendance.On_high_holidays},
			{"On-high-holidays", TempleAttendance.On_high_holidays},
			{"on-high-holidays", TempleAttendance.On_high_holidays},
			
			{"Ons", TempleAttendance.On_some_shabbat},
			{"ons", TempleAttendance.On_some_shabbat},
			{"Onso", TempleAttendance.On_some_shabbat},
			{"onso", TempleAttendance.On_some_shabbat},
			{"On_s", TempleAttendance.On_some_shabbat},
			{"on_s", TempleAttendance.On_some_shabbat},
			{"On s", TempleAttendance.On_some_shabbat},
			{"on s", TempleAttendance.On_some_shabbat},
			{"On_some_shabbat|", TempleAttendance.On_some_shabbat},
			{"on_some_shabbat|", TempleAttendance.On_some_shabbat},
			{"On some shabbat|", TempleAttendance.On_some_shabbat},
			{"on some shabbat|", TempleAttendance.On_some_shabbat},
			{"On-some-shabbat|", TempleAttendance.On_some_shabbat},
			{"on-some-shabbat|", TempleAttendance.On_some_shabbat},
			{"On_some_shabbat", TempleAttendance.On_some_shabbat},
			{"on_some_shabbat", TempleAttendance.On_some_shabbat},
			{"on some shabbat", TempleAttendance.On_some_shabbat},
			{"On some shabbat", TempleAttendance.On_some_shabbat},
			{"On-some-shabbat", TempleAttendance.On_some_shabbat},
			{"on-some-shabbat", TempleAttendance.On_some_shabbat},

			{"som", TempleAttendance.Sometimes},
			{"Som", TempleAttendance.Sometimes},
			{"some", TempleAttendance.Sometimes},
			{"Some", TempleAttendance.Sometimes},
			{"Sometimes|", TempleAttendance.Sometimes},
			{"sometimes|", TempleAttendance.Sometimes},
			{"Sometimes", TempleAttendance.Sometimes},
			{"sometimes", TempleAttendance.Sometimes},

			{"Wil", TempleAttendance.Will_tell_you_later},
			{"wil", TempleAttendance.Will_tell_you_later},
			{"Will", TempleAttendance.Will_tell_you_later},
			{"will", TempleAttendance.Will_tell_you_later},
			{"Will_tell_you_later|", TempleAttendance.Will_tell_you_later},
			{"will_tell_you_later|", TempleAttendance.Will_tell_you_later},
			{"Will tell you later|", TempleAttendance.Will_tell_you_later},
			{"will tell you later|", TempleAttendance.Will_tell_you_later},
			{"Will-tell-you-later|", TempleAttendance.Will_tell_you_later},
			{"will-tell-you-later|", TempleAttendance.Will_tell_you_later},
			{"Will_tell_you_later", TempleAttendance.Will_tell_you_later},
			{"will_tell_you_later", TempleAttendance.Will_tell_you_later},
			{"Will tell you later", TempleAttendance.Will_tell_you_later},
			{"will tell you later", TempleAttendance.Will_tell_you_later},
			{"Will-tell-you-later", TempleAttendance.Will_tell_you_later},
			{"will-tell-you-later", TempleAttendance.Will_tell_you_later},
		};

		#endregion TempleAttendance

		#region WantsChildren

		private readonly Dictionary<String, WantsChildren> wantsChildren = new Dictionary<String, WantsChildren>
		{
			{String.Empty, WantsChildren.Any},

			{"a", WantsChildren.Any},
			{"A", WantsChildren.Any},
			{"an", WantsChildren.Any},
			{"An", WantsChildren.Any},
			{"any", WantsChildren.Any},
			{"Any", WantsChildren.Any},
			{"any|", WantsChildren.Any},
			{"Any|", WantsChildren.Any},

			{"n", WantsChildren.No},
			{"N", WantsChildren.No},
			{"no", WantsChildren.No},
			{"No", WantsChildren.No},
			{"no|", WantsChildren.No},
			{"No|", WantsChildren.No},

			{"u", WantsChildren.Unsure},
			{"U", WantsChildren.Unsure},
			{"un", WantsChildren.Unsure},
			{"Un", WantsChildren.Unsure},
			{"uns", WantsChildren.Unsure},
			{"Uns", WantsChildren.Unsure},
			{"unsu", WantsChildren.Unsure},
			{"Unsu", WantsChildren.Unsure},
			{"unsure", WantsChildren.Unsure},
			{"Unsure", WantsChildren.Unsure},
			{"unsure|", WantsChildren.Unsure},
			{"Unsure|", WantsChildren.Unsure},

			{"y", WantsChildren.Yes},
			{"Y", WantsChildren.Yes},
			{"ye", WantsChildren.Yes},
			{"Ye", WantsChildren.Yes},
			{"yes", WantsChildren.Yes},
			{"Yes", WantsChildren.Yes},
			{"yes|", WantsChildren.Yes},
			{"Yes|", WantsChildren.Yes},
		};

		#endregion WantsChildren

		#region JDateReligion

		private readonly Dictionary<String, Religion> jDateReligion = new Dictionary<String, Religion>
		{ 
			{ String.Empty, Religion.Will_tell_you_later },

			{ "rel", Religion.Religious },
			{ "Rel", Religion.Religious },
			{ "Relig", Religion.Religious },
			{ "relig", Religion.Religious },
			{ "religious", Religion.Religious },
			{ "Religious", Religion.Religious },
			{ "religious|", Religion.Religious },
			{ "Religious|", Religion.Religious },

			{ "orthodox baal teshuva", Religion.Orthodox_Baal_Teshuva },
			{ "Orthodox Baal Teshuva", Religion.Orthodox_Baal_Teshuva },
			{ "orthodox baal Teshuva|", Religion.Orthodox_Baal_Teshuva },
			{ "Orthodox Baal Teshuva|", Religion.Orthodox_Baal_Teshuva },
			{ "Orthodox_Baal_Teshuva", Religion.Orthodox_Baal_Teshuva },
			{ "orthodox_Baal_Teshuva", Religion.Orthodox_Baal_Teshuva },
			{ "orthodox_Baal_Teshuva|", Religion.Orthodox_Baal_Teshuva },
			{ "Orthodox_Baal_Teshuva|", Religion.Orthodox_Baal_Teshuva },
			{ "Orthodox(Baal Teshuva)|", Religion.Orthodox_Baal_Teshuva },

			{ "conservative", Religion.Conservative },
			{ "Conservative", Religion.Conservative },
			{ "conservative|", Religion.Conservative },
			{ "Conservative|", Religion.Conservative },

			{ "Conservadox", Religion.Conservadox },
			{ "conservadox", Religion.Conservadox },
			{ "Conservadox|", Religion.Conservadox },
			{ "conservadox|", Religion.Conservadox },

			{ "hassidic", Religion.Hassidic },
			{ "Hassidic", Religion.Hassidic },
			{ "hassidic|", Religion.Hassidic },
			{ "Hassidic|", Religion.Hassidic },

			{ "modern_orthodox", Religion.Modern_Orthodox },
			{ "Modern_Orthodox", Religion.Modern_Orthodox },
			{ "modern_orthodox|", Religion.Modern_Orthodox },
			{ "Modern_Orthodox|", Religion.Modern_Orthodox },
			{ "modern Orthodox", Religion.Modern_Orthodox },
			{ "Modern Orthodox", Religion.Modern_Orthodox },
			{ "modern Orthodox|", Religion.Modern_Orthodox },
			{ "Modern Orthodox|", Religion.Modern_Orthodox },
			{ "modernorthodox", Religion.Modern_Orthodox },
			{ "ModernOrthodox", Religion.Modern_Orthodox },
			{ "modernorthodox|", Religion.Modern_Orthodox },
			{ "ModernOrthodox|", Religion.Modern_Orthodox },

			{ "orthodox_frum", Religion.Orthodox_Frum },
			{ "Orthodox_Frum", Religion.Orthodox_Frum },
			{ "orthodox_frum|", Religion.Orthodox_Frum },
			{ "Orthodox_Frum|", Religion.Orthodox_Frum },
			{ "orthodox frum", Religion.Orthodox_Frum },
			{ "Orthodox Frum", Religion.Orthodox_Frum },
			{ "orthodox frum|", Religion.Orthodox_Frum },
			{ "Orthodox Frum|", Religion.Orthodox_Frum },
			{ "orthodoxfrum", Religion.Orthodox_Frum },
			{ "OrthodoxFrum", Religion.Orthodox_Frum },
			{ "orthodoxfrum|", Religion.Orthodox_Frum },
			{ "OrthodoxFrum|", Religion.Orthodox_Frum },
			{ "Orthodox (frum from birth)|", Religion.Orthodox_Frum},

			{ "another_stream", Religion.Another_Stream },
			{ "Another_Stream", Religion.Another_Stream },
			{ "another_stream|", Religion.Another_Stream },
			{ "Another_Stream|", Religion.Another_Stream },
			{ "another stream", Religion.Another_Stream },
			{ "Another Stream", Religion.Another_Stream },
			{ "another stream|", Religion.Another_Stream },
			{ "Another Stream|", Religion.Another_Stream },
			{ "anotherstream", Religion.Another_Stream },
			{ "AnotherStream", Religion.Another_Stream },
			{ "anotherstream|", Religion.Another_Stream },
			{ "AnotherStream|", Religion.Another_Stream },

			{ "reconstructionist", Religion.Reconstructionist },
			{ "Reconstructionist", Religion.Reconstructionist },
			{ "reconstructionist|", Religion.Reconstructionist },
			{ "Reconstructionist|", Religion.Reconstructionist },

			{ "reform", Religion.Reform },
			{ "Reform", Religion.Reform },
			{ "reform|", Religion.Reform },
			{ "Reform|", Religion.Reform },

			{ "secular", Religion.Secular },
			{ "Secular", Religion.Secular },
			{ "secular|", Religion.Secular },
			{ "Secular|", Religion.Secular },

			{ "traditional", Religion.Traditional },
			{ "Traditional", Religion.Traditional },
			{ "traditional|", Religion.Traditional },
			{ "Traditional|", Religion.Traditional },

			{ "masorti", Religion.Masorti },
			{ "Masorti", Religion.Masorti },
			{ "masorti|", Religion.Masorti },
			{ "Masorti|", Religion.Masorti },

			{ "willing_to_Convert", Religion.Willing_to_Convert },
			{ "Willing_to_Convert", Religion.Willing_to_Convert },
			{ "willing_to_Convert|", Religion.Willing_to_Convert },
			{ "Willing_to_Convert|", Religion.Willing_to_Convert },
			{ "willing to convert", Religion.Willing_to_Convert },
			{ "Willing to Convert", Religion.Willing_to_Convert },
			{ "willing to convert|", Religion.Willing_to_Convert },
			{ "Willing to Convert|", Religion.Willing_to_Convert },

			{ "Not_willing_to_Convert", Religion.Willing_to_Convert },
			{ "Not_Willing_to_Convert", Religion.Willing_to_Convert },
			{ "Not_willing_to_Convert|", Religion.Willing_to_Convert },
			{ "Not_Willing_to_Convert|", Religion.Willing_to_Convert },
			{ "not willing to convert", Religion.Willing_to_Convert },
			{ "Not Willing to Convert", Religion.Willing_to_Convert },
			{ "not willing to convert|", Religion.Willing_to_Convert },
			{ "Not Willing to Convert|", Religion.Willing_to_Convert },

			{ "unsure convert", Religion.Unsure_Convert },
			{ "Unsure Convert", Religion.Unsure_Convert },
			{ "unsure convert|", Religion.Unsure_Convert },
			{ "Unsure Convert|", Religion.Unsure_Convert },
			{ "unsure_convert", Religion.Unsure_Convert },
			{ "Unsure_Convert", Religion.Unsure_Convert },
			{ "unsure_convert|", Religion.Unsure_Convert },
			{ "Unsure_Convert|", Religion.Unsure_Convert },

			{ "unaffiliated", Religion.Unafilliated },
			{ "Unaffiliated", Religion.Unafilliated },
			{ "unaffiliated|", Religion.Unafilliated },
			{ "Unaffiliated|", Religion.Unafilliated },

			{"Wil", Religion.Will_tell_you_later},
			{"wil", Religion.Will_tell_you_later},
			{"Will", Religion.Will_tell_you_later},
			{"will", Religion.Will_tell_you_later},
			{"Will_tell_you_later|", Religion.Will_tell_you_later},
			{"will_tell_you_later|", Religion.Will_tell_you_later},
			{"Will tell you later|", Religion.Will_tell_you_later},
			{"will tell you later|", Religion.Will_tell_you_later},
			{"Will-tell-you-later|", Religion.Will_tell_you_later},
			{"will-tell-you-later|", Religion.Will_tell_you_later},
			{"Will_tell_you_later", Religion.Will_tell_you_later},
			{"will_tell_you_later", Religion.Will_tell_you_later},
			{"Will tell you later", Religion.Will_tell_you_later},
			{"will tell you later", Religion.Will_tell_you_later},
			{"Will-tell-you-later", Religion.Will_tell_you_later},
			{"will-tell-you-later", Religion.Will_tell_you_later},
		};

		#endregion JDateReligion

		public Drinking GetDrinkingValue(String s)
		{
			return String.IsNullOrEmpty(s) ? drinking[String.Empty] : drinking[s];
		}

		public Education GetEducationValue(String s)
		{
			return String.IsNullOrEmpty(s) ? education[String.Empty] : education[s];
		}

		public Ethnicity GetEthnicityValue(String s)
		{
			return String.IsNullOrEmpty(s) ? ethnicity[String.Empty] : ethnicity[s];
		}

		public KeepKosher GetKeepKosherValue(String s)
		{
			return String.IsNullOrEmpty(s) ? keepKosher[String.Empty] : keepKosher[s];
		}

		public Language GetLanguageValue(String s)
		{
			return String.IsNullOrEmpty(s) ? language[String.Empty] : language[s];
		}

		public MaritalStatus GetMaritalStatusValue(String s)
		{
			return String.IsNullOrEmpty(s) ? maritalStatus[String.Empty] : maritalStatus[s];
		}

		public Relocate GetRelocateValue(String s)
		{
			return String.IsNullOrEmpty(s) ? relocate[String.Empty] : relocate[s];
		}

		public Smoking GetSmokingValue(String s)
		{
			return String.IsNullOrEmpty(s) ? smoking[String.Empty] : smoking[s];
		}

		public TempleAttendance GetTempleAttendanceValue(String s)
		{
			return String.IsNullOrEmpty(s) ? templeAttendance[String.Empty] : templeAttendance[s];
		}

		public WantsChildren GetWantsChildrenValue(String s)
		{
			return String.IsNullOrEmpty(s) ? wantsChildren[String.Empty] : wantsChildren[s];
		}

		public Religion GetJDateReligion(String s)
		{
			return String.IsNullOrEmpty(s) ? jDateReligion[String.Empty] : jDateReligion[s];
		}
	}
}
