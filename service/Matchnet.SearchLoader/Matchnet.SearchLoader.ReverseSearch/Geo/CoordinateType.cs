﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Geo
{
    [Serializable]
    public enum CoordinateType
    {
        Degrees,
        Radians
    }
}