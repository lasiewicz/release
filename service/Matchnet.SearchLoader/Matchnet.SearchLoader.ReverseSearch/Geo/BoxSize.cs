﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Geo
{
    [Serializable]
    public class BoxSize
    {
        public Double SideX { get; set; }

        public Double SideY { get; set; }
    }
}