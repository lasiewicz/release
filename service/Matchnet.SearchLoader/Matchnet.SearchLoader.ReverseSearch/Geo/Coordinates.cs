using System;
using Matchnet.SearchLoader.ReverseSearch.Interfaces;

namespace Matchnet.SearchLoader.ReverseSearch.Geo
{
    [Serializable]
    public class Coordinates : ICoordinate
    {
        public const Double RadiansToDegrees = 180 / Math.PI;

        public Double Latitude { get; set; }

        public Double Longitude { get; set; }

        public CoordinateType TypeOfCoordinate { get; private set; }

        public Coordinates(Double latitude, Double longitude, Boolean convertToDegrees)
        {
            Latitude = convertToDegrees ? latitude * RadiansToDegrees : latitude;
            Longitude = convertToDegrees ? longitude * RadiansToDegrees : longitude;
            TypeOfCoordinate = convertToDegrees ? CoordinateType.Degrees : CoordinateType.Radians;
        }

        public Coordinates(Decimal latitude, Decimal longitude, CoordinateType type)
        {
            Latitude = (Double)latitude;
            Longitude = (Double)longitude;
            TypeOfCoordinate = type;
        }

        public Coordinates(Double latitude, Double longitude, CoordinateType type)
        {
            Latitude = latitude;
            Longitude = longitude;
            TypeOfCoordinate = type;
        }

        public ICoordinate Convert()
        {
            switch (TypeOfCoordinate)
            {
                case CoordinateType.Degrees:

                    Latitude = Latitude / RadiansToDegrees;
                    Longitude = Longitude / RadiansToDegrees;
                    TypeOfCoordinate = CoordinateType.Radians;

                    break;

                case CoordinateType.Radians:

                    Latitude = Latitude * RadiansToDegrees;
                    Longitude = Longitude * RadiansToDegrees;
                    TypeOfCoordinate = CoordinateType.Degrees;

                    break;
            }

            return this;
        }

        public Coordinates Convert(CoordinateType coordinateType)
        {
            switch (coordinateType)
            {
                case CoordinateType.Degrees:
                    if (TypeOfCoordinate == CoordinateType.Degrees) break;

                    Latitude = Latitude / RadiansToDegrees;
                    Longitude = Longitude / RadiansToDegrees;
                    TypeOfCoordinate = CoordinateType.Radians;
                    break;

                case CoordinateType.Radians:
                    if (TypeOfCoordinate == CoordinateType.Radians) break;

                    Latitude = Latitude * RadiansToDegrees;
                    Longitude = Longitude * RadiansToDegrees;
                    TypeOfCoordinate = CoordinateType.Degrees;
                    break;
            }

            return this;
        }
    }
}