﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Geo
{
    [Serializable]
    public class Box
    {
        public BoxSize Size { get; set; }

        public BoxKey Key { get; set; }

        public BoxSearchArea Area { get; set; }

        public Box()
        {
            Size = new BoxSize();
            Key = new BoxKey();
            Area = new BoxSearchArea();
        }
    }
}