using System;

namespace Matchnet.SearchLoader.ReverseSearch.Geo
{
    public static class GeoUtil
    {
        public static Double DegreeToRadian(Double degree)
        {
            return degree * Math.PI / 180;
        }

        public static Double CalculateDistance(Coordinates coord1, Coordinates coord2, bool metric)
        {
            var multiplier = (metric ? Constants.ConversionKmMiles : 1);
            var radius = Constants.EarthRadiusMiles * multiplier;

            return radius * (2.0 * Math.Asin(Math.Sqrt(Math.Pow(Math.Sin((coord2.Latitude - coord1.Latitude) / 2.0), 2) +
                                                       (Math.Cos(coord1.Latitude) * Math.Cos(coord2.Latitude) *
                                                        Math.Pow(Math.Sin((coord2.Longitude - coord1.Longitude) / 2.0), 2)))));
        }

        public static BoxSize CalculateBoxSize(Coordinates startCoord, double radiant, bool metric)
        {
            var size = new BoxSize();
            var shiftLongitude = MinMax(startCoord.Longitude + radiant,
                                        Constants.Longitude.Minimum, Constants.Longitude.Maximum);
            var shiftCoord = new Coordinates(startCoord.Latitude, shiftLongitude, false);
            var dist = CalculateDistance(startCoord, shiftCoord, metric);
            size.SideX = dist;

            var shiftLatitude = MinMax(startCoord.Latitude + radiant,
                                       Constants.Latitude.Minimum, Constants.Latitude.Maximum);
            shiftCoord.Latitude = shiftLatitude;
            shiftCoord.Longitude = startCoord.Longitude;
            dist = CalculateDistance(startCoord, shiftCoord, metric);
            size.SideY = dist;

            return size;

        }

        public static void CalculateSearchArea(Box searchBox, Double radius)
        {
            if (searchBox == null) return;

            var shiftX = 0;
            var shiftY = 0;

            if (searchBox.Size.SideX != 0)
            {
                shiftX = (int)(radius / searchBox.Size.SideX) + (radius % searchBox.Size.SideX > 0 ? 1 : 0);
            }

            if (searchBox.Size.SideY != 0)
            {
                shiftY = (int)(radius / searchBox.Size.SideY) + (radius % searchBox.Size.SideY > 0 ? 1 : 0);
            }

            searchBox.Area.BoxXMin = searchBox.Key.X - shiftX;
            searchBox.Area.BoxXMax = searchBox.Key.X + shiftX;

            searchBox.Area.BoxYMin = searchBox.Key.Y - shiftY;
            searchBox.Area.BoxYMax = searchBox.Key.Y + shiftY;
        }

        public static Double MinMax(double val, double min, double max)
        {
            return (val >= min && val <= max ? val : (val > max ? max : min));
        }
    }
}