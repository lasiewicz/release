﻿using System;
using Matchnet.SearchLoader.ReverseSearch.Interfaces;

namespace Matchnet.SearchLoader.ReverseSearch.Geo
{
    [Serializable]
    public sealed class CoordinateBox : ICoordinate
    {
        #region Constants

        public const CoordinateType DefaultType = CoordinateType.Radians;

        private const Double RadiansToDegrees = 180 / Math.PI;

        #endregion Constants

        #region Properties

        public Double LongitudeMinimum { get; set; }

        public Double LongitudeMaximum { get; set; }

        public Double LatitudeMinimum { get; set; }

        public Double LatitudeMaximum { get; set; }

        public Double Latitude
        {
            get { return (LatitudeMinimum + LatitudeMaximum) / 2; }
        }

        public Double Longitude
        {
            get { return (LongitudeMinimum + LongitudeMaximum) / 2; }
        }

        public CoordinateType TypeOfCoordinate { get; set; }

        #endregion Properties

        #region Constructors

        public CoordinateBox()
        {
            TypeOfCoordinate = DefaultType;
        }

        public CoordinateBox(
            Double longitudeMinimum, Double longitudeMaximum,
            Double latitudeMinimum, Double latitudeMaximum,
            CoordinateType type)
        {
            LongitudeMinimum = longitudeMinimum;
            LongitudeMaximum = longitudeMaximum;
            LatitudeMinimum = latitudeMinimum;
            LatitudeMaximum = latitudeMaximum;

            TypeOfCoordinate = type;
        }

        #endregion Constructors

        #region ICoordinate Members

        public ICoordinate Convert()
        {
            switch (TypeOfCoordinate)
            {
                case CoordinateType.Degrees:

                    LongitudeMinimum = LongitudeMinimum / RadiansToDegrees;
                    LongitudeMaximum = LongitudeMaximum / RadiansToDegrees;
                    LatitudeMinimum = LatitudeMinimum / RadiansToDegrees;
                    LatitudeMaximum = LatitudeMaximum / RadiansToDegrees;
                    TypeOfCoordinate = CoordinateType.Radians;

                    break;

                case CoordinateType.Radians:

                    LongitudeMinimum = LongitudeMinimum * RadiansToDegrees;
                    LongitudeMaximum = LongitudeMaximum * RadiansToDegrees;
                    LatitudeMinimum = LatitudeMinimum * RadiansToDegrees;
                    LatitudeMaximum = LatitudeMaximum * RadiansToDegrees;
                    TypeOfCoordinate = CoordinateType.Degrees;

                    break;
            }

            return this;
        }

        #endregion

        public CoordinateBox Convert(CoordinateType coordinateType)
        {
            switch (coordinateType)
            {
                case CoordinateType.Degrees:
                    if (TypeOfCoordinate == CoordinateType.Degrees) break;

                    LongitudeMinimum = LongitudeMinimum / RadiansToDegrees;
                    LongitudeMaximum = LongitudeMaximum / RadiansToDegrees;
                    LatitudeMinimum = LatitudeMinimum / RadiansToDegrees;
                    LatitudeMaximum = LatitudeMaximum / RadiansToDegrees;
                    TypeOfCoordinate = CoordinateType.Radians;
                    break;

                case CoordinateType.Radians:
                    if (TypeOfCoordinate == CoordinateType.Radians) break;

                    LongitudeMinimum = LongitudeMinimum * RadiansToDegrees;
                    LongitudeMaximum = LongitudeMaximum * RadiansToDegrees;
                    LatitudeMinimum = LatitudeMinimum * RadiansToDegrees;
                    LatitudeMaximum = LatitudeMaximum * RadiansToDegrees;

                    TypeOfCoordinate = CoordinateType.Degrees;
                    break;
            }

            return this;
        }
    }
}