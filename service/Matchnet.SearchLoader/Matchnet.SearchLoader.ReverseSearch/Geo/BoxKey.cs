﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Geo
{
    [Serializable]
    public class BoxKey
    {
        public Int32 X { get; set; }

        public Int32 Y { get; set; }
    }
}