﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Geo
{
    [Serializable]
    public class BoxSearchArea
    {
        public Int32 BoxXMin { get; set; }

        public Int32 BoxXMax { get; set; }

        public Int32 BoxYMin { get; set; }

        public Int32 BoxYMax { get; set; }
    }
}