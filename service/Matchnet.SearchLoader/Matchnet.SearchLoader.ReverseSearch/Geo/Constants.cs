﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Geo
{
    public static class Constants
    {
        public const Double ConversionKmMiles = 0.621371192;

        public const Double ConversionMilesKm = 1.609344;
        public const Double EarthRadiusMiles = 3963.191;

        #region Nested type: Latitude

        public static class Latitude
        {
            public const Double Maximum = Math.PI / 2;

            public const Double Minimum = -Math.PI / 2;
        }

        #endregion

        #region Nested type: Longitude

        public static class Longitude
        {
            public const Double Maximum = Math.PI;

            public const Double Minimum = -Math.PI;
        }

        #endregion
    }
}