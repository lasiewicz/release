﻿using System;
using Matchnet.Exceptions;

namespace Matchnet.SearchLoader.ReverseSearch.Exceptions
{
    public sealed class DataSourceTimeoutException : ExceptionBase
    {
        public Int32 Timeout { get; private set; }

        public Int64 ElapsedTime { get; private set; }

        public DataSourceTimeoutException(Int32 timeout, Int64 elapsedTime)
        {
            Timeout = timeout;
            ElapsedTime = elapsedTime;
        }
    }
}