﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Serialization;
using Matchnet.SearchLoader.ReverseSearch.Data;
using Matchnet.SearchLoader.ReverseSearch.Enumerations;
using Matchnet.SearchLoader.ReverseSearch.Geo;
using Matchnet.SearchLoader.ReverseSearch.Interfaces;
using org.apache.solr.SolrSharp.Indexing;

namespace Matchnet.SearchLoader.ReverseSearch.Core
{
	[Serializable]
	[XmlRoot("add")]
	public class ReverseSearchDocument : UpdateIndexDocument, IDocument
	{
		#region Properties

		[XmlIgnore]
		public String Id
		{
			get { return this["id"]; }
			set { Add(new IndexFieldValue("id", value)); }
		}

		[XmlIgnore]
		public Int32 MemberId
		{
			get { return Int32.Parse(this["memberid"]); }
			set { Add(new IndexFieldValue("memberid", value.ToString())); }
		}

		[XmlIgnore]
		public Int32 SiteId
		{
			get { return Int32.Parse(this["site"]); }
			set { Add(new IndexFieldValue("site", value.ToString())); }
		}

		[XmlIgnore]
		public String Gender
		{
			get { return this["gender"]; }
			set { Add(new IndexFieldValue("gender", value)); }
		}

		[XmlIgnore]
		public String SeekingGender
		{
			get { return this["seekinggender"]; }
			set { Add(new IndexFieldValue("seekinggender", value)); }
		}

		[XmlIgnore]
		public Boolean HasPhoto
		{
			get { return Boolean.Parse(this["hasphoto"]); }
			set { Add(new IndexFieldValue("hasphoto", value.ToString().ToLower())); }
		}

		[XmlIgnore]
		public DateTime LastLogin
		{
			get { return DateTime.Parse(this["lastlogin"]); }
			set { Add(new IndexFieldValue("lastlogin", FormatDate(value))); }
		}

		[XmlIgnore]
		public DateTime Signup
		{
			get { return DateTime.Parse(this["signup"]); }
			set { Add(new IndexFieldValue("signup", FormatDate(value))); }
		}

		[XmlIgnore]
		public Int32 PreferredHeightMinimum
		{
			get { return Int32.Parse(this["preferredheightmin"]); }
			set { Add(new IndexFieldValue("preferredheightmin", value.ToString())); }
		}

		[XmlIgnore]
		public Int32 PreferredHeightMaximum
		{
			get { return Int32.Parse(this["preferredheightmax"]); }
			set { Add(new IndexFieldValue("preferredheightmax", value.ToString())); }
		}

		[XmlIgnore]
		public Int32 PreferredWeightMinimum
		{
			get { return Int32.Parse(this["preferredweightmin"]); }
			set { Add(new IndexFieldValue("preferredweightmin", value.ToString())); }
		}

		[XmlIgnore]
		public Int32 PreferredWeightMaximum
		{
			get { return Int32.Parse(this["preferredweightmax"]); }
			set { Add(new IndexFieldValue("preferredweightmax", value.ToString())); }
		}

		[XmlIgnore]
		public Int32 PreferredAgeMinimum
		{
			get { return Int32.Parse(this["preferredagemin"]); }
			set { Add(new IndexFieldValue("preferredagemin", value.ToString())); }
		}

		[XmlIgnore]
		public Int32 PreferredAgeMaximum
		{
			get { return Int32.Parse(this["preferredagemax"]); }
			set { Add(new IndexFieldValue("preferredagemax", value.ToString())); }
		}

		[XmlIgnore]
		public Int32 PreferredSearchRadius { get; set; }

		[XmlIgnore]
		public IEnumerable<String> PreferredLanguage
		{
			get { return from fieldValue in fieldValues where fieldValue.Name == "preferredlanguage" select fieldValue.Value; }
			set
			{
				if (value == null || value.Count() < 1) return;

				Array.ForEach(value.ToArray(), status => Add(new IndexFieldValue("preferredlanguage", status)));
			}
		}

		[XmlIgnore]
		public IEnumerable<String> WantsChildren
		{
			get { return from fieldValue in fieldValues where fieldValue.Name == "preferredwantschildren" select fieldValue.Value; }
			set
			{
				if (value == null || value.Count() < 1) return;

				Array.ForEach(value.ToArray(), status => Add(new IndexFieldValue("preferredwantschildren", status)));
			}
		}

		[XmlIgnore]
		public IEnumerable<String> PreferredActivityLevel
		{
			get { return from fieldValue in fieldValues where fieldValue.Name == "preferredactivitylevel" select fieldValue.Value; }
			set
			{
				if (value == null || value.Count() < 1) return;

				Array.ForEach(value.ToArray(), status => Add(new IndexFieldValue("preferredactivitylevel", status)));
			}
		}

		[XmlIgnore]
		public IEnumerable<String> PreferredMaritalStatus
		{
			get { return from fieldValue in fieldValues where fieldValue.Name == "preferredmaritalstatus" select fieldValue.Value; }
			set
			{
				if (value == null || value.Count() < 1) return;

				Array.ForEach(value.ToArray(), status => Add(new IndexFieldValue("preferredmaritalstatus", status)));
			}
		}

		[XmlIgnore]
		public IEnumerable<String> PreferredRelocateStatus
		{
			get { return from fieldValue in fieldValues where fieldValue.Name == "preferredrelocatestatus" select fieldValue.Value; }
			set
			{
				if (value == null || value.Count() < 1) return;

				Array.ForEach(value.ToArray(), status => Add(new IndexFieldValue("preferredrelocatestatus", status)));
			}
		}

		[XmlIgnore]
		public IEnumerable<String> PreferredEducation
		{
			get { return from fieldValue in fieldValues where fieldValue.Name == "preferrededucation" select fieldValue.Value; }
			set
			{
				if (value == null || value.Count() < 1) return;

				Array.ForEach(value.ToArray(), status => Add(new IndexFieldValue("preferrededucation", status)));
			}
		}

		[XmlIgnore]
		public IEnumerable<String> PreferredSmokerStatus
		{
			get { return from fieldValue in fieldValues where fieldValue.Name == "preferredsmokingstatus" select fieldValue.Value; }
			set
			{
				if (value == null || value.Count() < 1) return;

				Array.ForEach(value.ToArray(), status => Add(new IndexFieldValue("preferredsmokingstatus", status)));
			}
		}

		[XmlIgnore]
		public IEnumerable<String> PreferredKosherStatus
		{
			get { return from fieldValue in fieldValues where fieldValue.Name == "preferredkeepkosher" select fieldValue.Value; }
			set
			{
				if (value == null || value.Count() < 1) return;

				Array.ForEach(value.ToArray(), status => Add(new IndexFieldValue("preferredkeepkosher", status)));
			}
		}

		[XmlIgnore]
		public IEnumerable<String> PreferredTempleAttendance
		{
			get { return from fieldValue in fieldValues where fieldValue.Name == "preferredtempleattendance" select fieldValue.Value; }
			set
			{
				if (value == null || value.Count() < 1) return;

				Array.ForEach(value.ToArray(), status => Add(new IndexFieldValue("preferredtempleattendance", status)));
			}
		}

		[XmlIgnore]
		public IEnumerable<String> PreferredEthnicity
		{
			get { return from fieldValue in fieldValues where fieldValue.Name == "preferredethnicity" select fieldValue.Value; }
			set
			{
				if (value == null || value.Count() < 1) return;

				Array.ForEach(value.ToArray(), status => Add(new IndexFieldValue("preferredethnicity", status)));
			}
		}

		[XmlIgnore]
		public IEnumerable<String> PreferredReligion
		{
			get { return from fieldValue in fieldValues where fieldValue.Name == "preferredreligion" select fieldValue.Value; }
			set
			{
				if (value == null || value.Count() < 1) return;

				Array.ForEach(value.ToArray(), status => Add(new IndexFieldValue("preferredreligion", status)));
			}
		}

		[XmlIgnore]
		public IEnumerable<String> PreferredDrinking
		{
			get { return from fieldValue in fieldValues where fieldValue.Name == "preferreddrinking" select fieldValue.Value; }
			set
			{
				if (value == null || value.Count() < 1) return;

				Array.ForEach(value.ToArray(), status => Add(new IndexFieldValue("preferreddrinking", status)));
			}
		}

		[XmlIgnore]
		public Double Latitude
		{
			get { return Double.Parse(this["latitude"]); }
			set { Add(new IndexFieldValue("latitude", value.ToString())); }
		}

		[XmlIgnore]
		public Double Longitude
		{
			get { return Double.Parse(this["longitude"]); }
			set { Add(new IndexFieldValue("longitude", value.ToString())); }
		}

		[XmlIgnore]
		public Double PreferredLatitudeMinimum
		{
			get { return Double.Parse(this["preferredlatitudemin"]); }
			set { Add(new IndexFieldValue("preferredlatitudemin", value.ToString())); }
		}

		[XmlIgnore]
		public Double PreferredLatitudeMaximum
		{
			get { return Double.Parse(this["preferredlatitudemax"]); }
			set { Add(new IndexFieldValue("preferredlatitudemax", value.ToString())); }
		}

		[XmlIgnore]
		public Double PreferredLongitudeMinimum
		{
			get { return Double.Parse(this["preferredlongitudemin"]); }
			set { Add(new IndexFieldValue("preferredlongitudemin", value.ToString())); }
		}

		[XmlIgnore]
		public Double PreferredLongitudeMaximum
		{
			get { return Double.Parse(this["preferredlongitudemax"]); }
			set { Add(new IndexFieldValue("preferredlongitudemax", value.ToString())); }
		}

		[XmlIgnore]
		public Double PreferredLatitudeMinimum160
		{
			get { return Double.Parse(this["preferredlatitudemin160"]); }
			set { Add(new IndexFieldValue("preferredlatitudemin160", value.ToString())); }
		}

		[XmlIgnore]
		public Double PreferredLatitudeMaximum160
		{
			get { return Double.Parse(this["preferredlatitudemax160"]); }
			set { Add(new IndexFieldValue("preferredlatitudemax160", value.ToString())); }
		}

		[XmlIgnore]
		public Double PreferredLongitudeMinimum160
		{
			get { return Double.Parse(this["preferredlongitudemin160"]); }
			set { Add(new IndexFieldValue("preferredlongitudemin160", value.ToString())); }
		}

		[XmlIgnore]
		public Double PreferredLongitudeMaximum160
		{
			get { return Double.Parse(this["preferredlongitudemax160"]); }
			set { Add(new IndexFieldValue("preferredlongitudemax160", value.ToString())); }
		}

		private Double specificity;

		[XmlIgnore]
		public Double Specificity
		{
			get { return Double.Parse(this["specificity"]); }
			set { Add(new IndexFieldValue("specificity", value.ToString())); }
		}

		#endregion Properties

		public static void AssignSpecificity(ReverseSearchDocument document)
		{
			document.Specificity = document.specificity;
		}

		public static ReverseSearchDocument Construct(IDataRecord record)
		{
			var document = new ReverseSearchDocument { Id = GetId(record) };

			SetSiteData(record, document);

			SetPersonalData(record, document);

			var latitude = GetValue<Decimal>(record["Latitude"]);

			var longitude = GetValue<Decimal>(record["Longitude"]);

			SetLocationData(document, latitude, longitude);

			SetDateData(record, document);

			SetAboutData(record, document);

			SetAgePreferences(record, document);

			SetHeightPreferences(record, document);

			SetPreferences(record, document);

			AssignSpecificity(document);

			var preferredLatitude = GetValue<Decimal>(record["PreferredLatitude"]);
			if (preferredLatitude == 0) preferredLatitude = latitude;

			var preferredLongitude = GetValue<Decimal>(record["PreferredLongitude"]);
			if (preferredLongitude == 0) preferredLongitude = longitude;

			var distance = GetValue<Int32>(record["PreferredDistance"]);
			if (distance == 0) distance = Constants.Profile.Default.Radius;

			SetLocationPreferences(document, preferredLatitude, preferredLongitude, distance);

			Set160MileLocationPreferences(document, preferredLatitude, preferredLongitude);

			return document;
		}

		private static void SetDateData(IDataRecord record, ReverseSearchDocument document)
		{
			document.LastLogin = GetValue<DateTime>(record["BrandLastLogonDate"]);
			document.Signup = GetValue<DateTime>(record["BrandInsertDate"]);
		}

		private static void SetAboutData(IDataRecord record, ReverseSearchDocument document)
		{
			document.SeekingGender = GetSeekingGender(record["GenderMask"]);

			document.HasPhoto = GetPhotoCount(record["PhotoCount"]) > 0;
		}

		private static void SetPersonalData(IDataRecord record, ReverseSearchDocument document)
		{
			document.Gender = GetGender(record["GenderMask"]);
		}

		private static void SetLocationData(ReverseSearchDocument document, Decimal latitude, Decimal longitude)
		{
			document.Latitude = (Double)latitude;
			document.Longitude = (Double)longitude;
		}

		private static void SetSiteData(IDataRecord record, ReverseSearchDocument document)
		{
			document.MemberId = GetValue<Int32>(record["MemberID"]);
			document.SiteId = GetValue<Int32>(record["SiteID"]);
		}

		public static String GetId(Int32 siteID, Int32 memberId)
		{
			return String.Format(
				"{0}-{1}",
				GetValue<Int32>(siteID),
				GetValue<Int32>(memberId)
				);
		}

		private static String GetId(IDataRecord record)
		{
			return String.Format(
				"{0}-{1}",
				GetValue<Int32>(record["SiteID"]),
				GetValue<Int32>(record["MemberID"]));
		}

		private static void SetHeightPreferences(IDataRecord record, ReverseSearchDocument document)
		{
			Int32 minimumHeight;

			if (Int32.TryParse(GetValue<String>(record["PreferredMinHeight"]), out minimumHeight) == false ||
				 minimumHeight < Constants.Profile.Default.Height.Minimum)
			{
				minimumHeight = Constants.Profile.Default.Height.Minimum;
			}

			document.PreferredHeightMinimum = minimumHeight;

			Int32 maximumHeight;

			if (Int32.TryParse(GetValue<String>(record["PreferredMaxHeight"]), out maximumHeight) == false ||
				 maximumHeight < minimumHeight ||
				 maximumHeight > Constants.Profile.Default.Height.Maximum)
			{
				maximumHeight = Constants.Profile.Default.Height.Maximum;
			}

			document.PreferredHeightMaximum = maximumHeight;
		}

		private static void SetAgePreferences(IDataRecord record, ReverseSearchDocument document)
		{
			Int32 minimumAge;

			if (Int32.TryParse(GetValue<String>(record["PreferredMinAge"]), out minimumAge) == false ||
				 minimumAge < Constants.Profile.Default.Age.Minimum)
			{
				minimumAge = Constants.Profile.Default.Age.Minimum;
			}

			document.PreferredAgeMinimum = minimumAge;

			Int32 maximumAge;

			if (Int32.TryParse(GetValue<String>(record["PreferredMaxAge"]), out maximumAge) == false ||
				 maximumAge < minimumAge ||
				 maximumAge > Constants.Profile.Default.Age.Maximum)
			{
				maximumAge = Constants.Profile.Default.Age.Maximum;
			}

			document.PreferredAgeMaximum = maximumAge;
		}

		public static String FormatValue(String value)
		{
			if (String.IsNullOrEmpty(value)) return String.Empty;

			return value.
				ToLower().
				Trim().
				Replace(Constants.Preferences.Ingestion.Apostrophe, String.Empty).
				Replace(Constants.Preferences.Ingestion.Dash, Constants.Preferences.Ingestion.Space).
				Replace(Constants.Preferences.Ingestion.LeftParenthesesPlusSpace, Constants.Preferences.Ingestion.SpaceString).
				Replace(Constants.Preferences.Ingestion.LeftParentheses, Constants.Preferences.Ingestion.SpaceString).
				Replace(Constants.Preferences.Ingestion.RightParentheses, String.Empty);
		}

		private static IEnumerable<String> ParseValue(Object field)
		{
			if (field == null || field == DBNull.Value) return null;

			var value = (String)field;

			if (String.IsNullOrEmpty(value)) return null;

			value = FormatValue(value);

			if (value == Constants.Preferences.Ingestion.Blank || value == Constants.Preferences.Ingestion.BlankWithBar) return null;

			return value.Split(new[] { Constants.Preferences.Ingestion.Bar }).Where(item => String.IsNullOrEmpty(item) == false);
		}

		public const Double BaseParameter = 1024;

		public const Double DefaultCoefficient = 1;

		public static void IncrementSpecificity(ReverseSearchDocument document, Double coefficient, Int32 preferenceCount)
		{
			document.specificity += coefficient * BaseParameter / Math.Pow(2, preferenceCount);
		}

		private static void SetPreferences(IDataRecord record, ReverseSearchDocument document)
		{
			var preferredLanguages = ParseValue(record["PreferredLanguages"]) ?? Constants.Preferences.Languages;

			IncrementSpecificity(document, DefaultCoefficient, preferredLanguages.Count());

			document.PreferredLanguage = preferredLanguages;

			var wantsChildren = ParseValue(record["PreferredHaveChildren"]) ?? Constants.Preferences.HaveChildren;

			IncrementSpecificity(document, DefaultCoefficient, wantsChildren.Count());

			document.WantsChildren = wantsChildren;

			var maritalStatus = ParseValue(record["PreferredMaritalStatus"]) ?? Constants.Preferences.MaritalStatuses;

			IncrementSpecificity(document, DefaultCoefficient, maritalStatus.Count());

			document.PreferredMaritalStatus = maritalStatus;

			var relocateStatus = ParseValue(record["PreferredRelocation"]) ?? Constants.Preferences.RelocateStatuses;

			IncrementSpecificity(document, DefaultCoefficient, relocateStatus.Count());

			document.PreferredRelocateStatus = relocateStatus;

			var education = ParseValue(record["PreferredEducation"]) ?? Constants.Preferences.EducationLevels;

			IncrementSpecificity(document, DefaultCoefficient, education.Count());

			document.PreferredEducation = education;

			var smokerStatus = ParseValue(record["PreferredSmokingHabit"]) ?? Constants.Preferences.SmokingHabits;

			IncrementSpecificity(document, DefaultCoefficient, smokerStatus.Count());

			document.PreferredSmokerStatus = smokerStatus;

			var kosherStatus = ParseValue(record["PreferredKosherStatus"]) ?? Constants.Preferences.KosherStatuses;

			IncrementSpecificity(document, DefaultCoefficient, kosherStatus.Count());

			document.PreferredKosherStatus = kosherStatus;

			var templeAttendance = ParseValue(record["PreferredSynagogueAttendance"]) ?? Constants.Preferences.TempleAttendances;

			IncrementSpecificity(document, DefaultCoefficient, templeAttendance.Count());

			document.PreferredTempleAttendance = templeAttendance;

			var ethnicity = ParseValue(record["PreferredJdateEthnicity"]) ?? Constants.Preferences.JDateEthnicities;

			IncrementSpecificity(document, DefaultCoefficient, ethnicity.Count());

			document.PreferredEthnicity = ethnicity;

			var religion = ParseValue(record["PreferredJdateReligion"]) ?? Constants.Preferences.JDateReligions;

			IncrementSpecificity(document, DefaultCoefficient, religion.Count());

			document.PreferredReligion = religion;

			var drinking = ParseValue(record["PreferredDrinkingHabit"]) ??
				Constants.Preferences.DrinkingHabits;

			IncrementSpecificity(document, DefaultCoefficient, drinking.Count());

			document.PreferredDrinking = drinking;

			var activityLevels = ParseValue(record["PreferredActivityLevel"]) ?? Constants.Preferences.ActivityLevels;

			IncrementSpecificity(document, DefaultCoefficient, activityLevels.Count());

			document.PreferredActivityLevel = activityLevels;
		}

		private static void Set160MileLocationPreferences(ReverseSearchDocument document, Decimal latitude, Decimal longitude)
		{
			var preferenceCoordinates = GeoDataConnector.GetBoxCoordinates(
				new Coordinates(latitude, longitude, CoordinateType.Radians),
				Constants.Profile.Default.Radius160Miles
				);

			if (preferenceCoordinates.TypeOfCoordinate == CoordinateType.Radians)
			{
				preferenceCoordinates.Convert();
			}

			document.PreferredLatitudeMinimum160 = preferenceCoordinates.LatitudeMinimum;

			document.PreferredLatitudeMaximum160 = preferenceCoordinates.LatitudeMaximum;

			document.PreferredLongitudeMinimum160 = preferenceCoordinates.LongitudeMinimum;

			document.PreferredLongitudeMaximum160 = preferenceCoordinates.LongitudeMaximum;
		}

		private static void SetLocationPreferences(ReverseSearchDocument document, Decimal latitude, Decimal longitude,
																 Int32 distance)
		{
			CoordinateBox preferenceCoordinates = GeoDataConnector.GetBoxCoordinates(
				new Coordinates(latitude, longitude, CoordinateType.Radians),
				distance
				);

			if (preferenceCoordinates.TypeOfCoordinate == CoordinateType.Radians)
			{
				preferenceCoordinates.Convert();
			}

			document.PreferredLatitudeMinimum = preferenceCoordinates.LatitudeMinimum;

			document.PreferredLatitudeMaximum = preferenceCoordinates.LatitudeMaximum;

			document.PreferredLongitudeMinimum = preferenceCoordinates.LongitudeMinimum;

			document.PreferredLongitudeMaximum = preferenceCoordinates.LongitudeMaximum;
		}

		private static String GetSeekingGender(Object field)
		{
			return (GetValue<Int32>(field) & (Int32)GenderMask.SeekingFemale) > 0
						?
							Constants.Profile.Gender.Female
						: Constants.Profile.Gender.Male;
		}

		public static String GetSeekingGender(Int32 mask)
		{
			return (mask & (Int32)GenderMask.SeekingFemale) > 0
						?
							Constants.Profile.Gender.Female
						: Constants.Profile.Gender.Male;
		}

		public static String GetGender(Int32 mask)
		{
			return (mask & (Int32)GenderMask.Female) > 0
						?
							Constants.Profile.Gender.Female
						: Constants.Profile.Gender.Male;
		}

		private static String GetGender(Object field)
		{
			return (GetValue<Int32>(field) & (Int32)GenderMask.Female) > 0
						?
							Constants.Profile.Gender.Female
						: Constants.Profile.Gender.Male;
		}

		private static Int32 GetPhotoCount(Object field)
		{
			if (field == null || field == DBNull.Value) return 0;

			return Int32.Parse(((Byte)field).ToString());
		}

		internal static T GetValue<T>(Object field)
		{
			return (field == null || field == DBNull.Value) ? default(T) : (T)field;
		}

		public static String FormatDate(DateTime dateTime)
		{
			return dateTime.ToString("u").Replace(" ", "T");
		}
	}
}