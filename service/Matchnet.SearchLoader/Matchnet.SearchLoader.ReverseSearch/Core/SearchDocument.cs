﻿using System;

namespace Matchnet.SearchLoader.ReverseSearch.Core
{
	public class SearchDocument
	{
		public Int32 MemberId { get; set; }

		public Int32 Age { get; set; }

		public Double Latitude { get; set; }

		public String Gender { get; set; }

		public Double Longitude { get; set; }

		public String TempleStatus { get; set; }

		public String SmokingHabits { get; set; }

		public String SeekingGender { get; set; }

		public String DrinkingHabits { get; set; }

		public String ActivityLevel { get; set; }

		public String Education { get; set; }

		public String KeepKosher { get; set; }

		public String JDateReligion { get; set; }

		public String Ethnicity { get; set; }

		public String Relocate { get; set; }

		public String HaveChildren { get; set; }

		public String Relationship { get; set; }

		public String Languages { get; set; }
	}
}