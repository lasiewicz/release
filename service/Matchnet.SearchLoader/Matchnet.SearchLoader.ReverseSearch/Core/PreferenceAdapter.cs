﻿using System;
using System.Collections.Generic;
using System.Linq;
using Matchnet.SearchLoader.ReverseSearch.Enumerations;

namespace Matchnet.SearchLoader.ReverseSearch.Core
{
	public static class PreferenceAdapter
	{
		private static IEnumerable<String> CleanEnumResults(String results)
		{
			return results.ToLower().Split(Constants.Characters.Comma).Select(status => status.Replace(
				Constants.Text.Underscore, Constants.Text.Space).
				Trim());
		}

		public static IEnumerable<String> GetPreferredWantsChildren(Int32 status)
		{
			if (status < 1) status = Constants.Profile.Default.WantsChildren;

			return CleanEnumResults(((WantsChildren)status).ToString());
		}

		public static IEnumerable<String> GetPreferredLanguage(Int32 status)
		{
			if (status < 1) status = Constants.Profile.Default.Language;

			return CleanEnumResults(((Language)status).ToString());
		}

		public static IEnumerable<String> GetPreferredActivityLevel(Int32 status)
		{
			if (status < 1) status = Constants.Profile.Default.ActivityLevel;

			return CleanEnumResults(((ActivityLevel)status).ToString());
		}

		public static IEnumerable<String> GetPreferredDrinking(Int32 status)
		{
			if (status < 1) status = Constants.Profile.Default.Drinking;

			return CleanEnumResults(((Drinking)status).ToString());
		}

		public static IEnumerable<String> GetPreferredReligion(Int32 status)
		{
			if (status < 1) status = Constants.Profile.Default.Religion;

			return CleanEnumResults(((Religion)status).ToString());
		}

		public static String GetRelationshipStatus(String raw)
		{
			return String.IsNullOrEmpty(raw)
						? Constants.WillTellYouLater
						: raw.Replace(Constants.Characters.BarString, String.Empty);
		}

		public static IEnumerable<String> GetPreferredEthnicity(Int32 status)
		{
			if (status < 1) status = Constants.Profile.Default.Ethnicity;

			return CleanEnumResults(((Ethnicity)status).ToString());
		}

		public static IEnumerable<String> GetPreferredTempleAttendance(Int32 status)
		{
			if (status < 1) status = Constants.Profile.Default.TempleAttendance;

			return CleanEnumResults(((TempleAttendance)status).ToString());
		}

		public static IEnumerable<String> GetPreferredKosherStatus(Int32 status)
		{
			if (status < 1) status = Constants.Profile.Default.KeepKosher;

			return CleanEnumResults(((KeepKosher)status).ToString());
		}

		public static IEnumerable<String> GetPreferredSmokingStatus(Int32 status)
		{
			if (status < 1) status = Constants.Profile.Default.SmokingStatus;

			return CleanEnumResults(((Smoking)status).ToString());
		}

		public static IEnumerable<String> GetPreferredEducationLevel(Int32 status)
		{
			if (status < 1) status = Constants.Profile.Default.EducationLevel;

			return CleanEnumResults(((Education)status).ToString());
		}

		public static IEnumerable<String> GetLanguages(String languageString)
		{
			if (String.IsNullOrEmpty(languageString)) return new[] { Language.English.ToString() };

			return languageString.Split(Constants.Characters.Bar);
		}

		public static IEnumerable<String> GetPreferredRelocationStatus(Int32 status)
		{
			if (status < 1) status = Constants.Profile.Default.Relocate;

			return CleanEnumResults(((Relocate)status).ToString());
		}

		public static IEnumerable<String> GetDesiredMaritalStatus(Int32 status)
		{
			if (status < 1) status = Constants.Profile.Default.DesiredMaritalStatus;

			return CleanEnumResults(((MaritalStatus)status).ToString());
		}
	}
}