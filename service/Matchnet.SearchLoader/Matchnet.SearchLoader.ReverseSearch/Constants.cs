﻿using System;
using Matchnet.SearchLoader.ReverseSearch.Geo;

namespace Matchnet.SearchLoader.ReverseSearch
{
	public static class Constants
	{
		public const String Any = "Any";
		public const Int32 JDate = 3;
        public const Int32 SparkCom = 1;

        public const String SolrDeleteBody = "<delete><id>{0}</id></delete>";
        public const String Post = "POST";

		public const String Non_Smoker = "Non Smoker";
		public const String NonSmoker = "NonSmoker";

		public const String NullToken = "<-null->";
		public const String Occasionally = "Occasionally";
		public const String On_occassion = "On occasion";
		public const String WillTellYouLater = "Will tell you later";

		public static class Characters
		{
			public static String Apostrophe = ",";
			public static Char Bar = '|';

			public static String BarString = "|";
			public static Char Comma = ',';

			public static String Dash = "-";
			public static Char Space = ' ';

			public static String Three = 3.ToString();
			public static Char Tilda = '~';
		}

		public static class Data
		{
			public const String ConnectionStringName = "SearchLoad";

			public const Int32 DefaultRows = 100;

			public const Int32 DefaultTimeout = 600;
			public const String StoredProcedureName = "up_MemberAttributeFlat_List_v3";

			public static class Parameters
			{
				public const String Community = "@Community";

				public const String Partition = "@Partition";

				public const String RecordsCount = "@RecordsCount";

				public const String SearchFlag = "@SearchFlag";
				public const String StartingRecord = "@StartingRecord";
			}
		}

		public static class Geo
		{
			public static class Default
			{
				private static readonly Coordinates coordinates =
					(Coordinates)(new Coordinates(34.074986, -118.398285, CoordinateType.Degrees)).Convert();

				public static Coordinates Coordinates
				{
					get { return coordinates; }
				}
			}
		}

		public static class Partition
		{
			public const Int32 Maximum = 24;
			public const Int32 Minimum = 1;
		}

		public static class Preferences
		{
			public static readonly String[] ActivityLevels = new[]
       	{
       		"never active",
       		"rarely active",
       		"selected activities",
       		"active",
       		"very active",
       	};

			public static readonly String[] DrinkingHabits = new[]
       	{
       		"socially",
       		"on occasion",
       		"never",
       		"frequently",
       	};

			public static readonly String[] EducationLevels = new[]
        	{
        		"elementary",
        		"high school",
        		"some college",
        		"associates degree",
        		"bachelors degree",
        		"masters degree",
        		"doctorate",
        	};

			public static readonly String[] HaveChildren = new[]
        	{
        		"yes",
        		"no",
        		"not sure",
        	};

			public static readonly String[] JDateEthnicities = new[]
      	{
      		"ashkenazi",
      		"mixed ethnic",
      		"sephardic",
      		"another ethnic",
      		"will tell you later",
      	};

			public static readonly String[] JDateReligions = new[]
       	{
       		"secular",
       		"conservative",
       		"reform",
       		"traditional",
       		"modern orthodox",
       		"another stream",
       		"reconstructionist",
       		"hassidic",
       		"conservadox",
       		"orthodox frum from birth",
       		"orthodox baal teshuva",
       		"willing to convert",
       		"will tell you later",
            "not willing to convert",
            "not sure if im willing to convert",
       	};

			public static readonly String[] KosherStatuses = new[]
       	{
       		"at home only",
       		"at home and outside",
       		"to some degree",
       		"not at all",
       	};

			public static readonly String[] Languages = new[]
        	{
				"other",
				"english",
				"hebrew",
				"french",
				"spanish",
				"german",
				"russian",
				"italian",
				"yiddish", 
				"farsi",
				"dutch",
				"arabic",
				"bengali",
				"bulgarian",
				"chinese",
				"czech",
				"greek",
				"hindi",
				"japanese",
				"korean",
				"malay",
				"norwegian",
				"polish",
				"portuguese",
				"romanian",
				"swedish",
				"tagalog",
				"thai",
				"urdu",
				"vietnamese",
				"finnish"
        	};

			public static readonly String[] MaritalStatuses = new[]
        	{
        		"single",
        		"divorced",
        		"separated",
        		"widowed",
        	};

			public static readonly String[] RelocateStatuses = new[]
      	{
      		"yes",
      		"no",
      		"not sure",
      	};

			public static readonly String[] SmokingHabits = new[]
      	{
      		"non smoker",
      		"trying to quit",
      		"occasional smoker",
      		"smokes regularly",
      	};

			public static readonly String[] TempleAttendances = new[]
       	{
       		"never",
       		"sometimes",
       		"on high holidays",
       		"on some shabbat",
       		"every shabbat",
       		"will tell you later",
       	};

			public static class Ingestion
			{
				public const String Apostrophe = "'";

				public const String Underscore = "_";

				public const Char Bar = '|';

				public const String Blank = "_blank";

				public const String BlankWithBar = "_blank|";

				public const String SpaceString = " ";

				public const String Comma = ",";

				public const Char Dash = '-';

				public const Int32 MinimumLineLength = 20;

				public const String LeftParentheses = "(";

				public const String RightParentheses = ")";

				public const String LeftParenthesesPlusSpace = " (";

				public const Char Space = ' ';

				public static class InvalidCharacter
				{
					public const String Dash = "-";

					public const String LeftParentheses = "(";
					public const String M = "M";
				}
			}
		}

		public static class Profile
		{
			public const Int32 MinimumMaritalStatusLength = 5;

			public static class Default
			{
				public const Int32 ActivityLevel = 126;

				public const Int32 DesiredMaritalStatus = 250;

				public const Int32 Drinking = 62;

				public const Int32 EducationLevel = 1022;

				public const Int32 Ethnicity = 63;

				public const Int32 KeepKosher = 47;

				public const Int32 Language = 524183;

				public const Int32 Radius = 40;

				public const Int32 Radius160Miles = 160;

				public const Int32 Religion = 262143;

				public const Int32 Relocate = 15;

				public const Int32 SmokingStatus = 62;

				public const Int32 TempleAttendance = 63;

				public const Int32 WantsChildren = 7;

				public static class Age
				{
					public const Int32 Maximum = 99;
					public const Int32 Minimum = 18;
				}

				public static class Height
				{
					public const Int32 Maximum = 244;
					public const Int32 Minimum = 100;
				}

				public static class Weight
				{
					public const Int32 Maximum = 400;
					public const Int32 Minimum = 80;
				}
			}

			public static class Gender
			{
				public const String Female = "female";
				public const String Male = "male";
			}
		}

		public static class Text
		{
			public const String Comma = ",";
			public const String Space = " ";

			public const String Underscore = "_";
		}
	}
}
