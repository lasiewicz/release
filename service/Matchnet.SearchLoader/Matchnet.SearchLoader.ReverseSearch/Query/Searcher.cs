﻿using System;
using System.Configuration;
using System.Net;
using System.Xml;
using System.Xml.Linq;

namespace Matchnet.SearchLoader.ReverseSearch.Query
{
    public class Searcher
    {
        public const String SolrUrlKey = "solrUrl";

        public Searcher() :
            this(ConfigurationManager.AppSettings[SolrUrlKey])
        {
        }

        public Searcher(String host)
        {
            Host = host;
        }

        public String Host { get; set; }

        public XElement Get(String url)
        {
            WebResponse response = WebRequest.Create(url).GetResponse();

            using (XmlReader reader = XmlReader.Create(response.GetResponseStream()))
            {
                XElement result = XElement.Load(reader);

                return result;
            }
        }
    }
}