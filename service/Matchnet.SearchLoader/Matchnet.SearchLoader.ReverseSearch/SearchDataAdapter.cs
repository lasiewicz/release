using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Matchnet.SearchLoader.ReverseSearch.Core;
using org.apache.solr.SolrSharp.Configuration;
using org.apache.solr.SolrSharp.Update;

namespace Matchnet.SearchLoader.ReverseSearch
{
    public class SearchDataAdapter
    {
        private readonly SolrSearcher solr;

        private readonly SolrUpdater updater;

        public SearchDataAdapter(String updateUrl)
        {
            solr = new SolrSearcher(updateUrl, Mode.ReadWrite);
            updater = new SolrUpdater(solr);
        }

        private static Int32 totalRecords;

        public static Int32 TotalRecords { get { return totalRecords; } }

        public void Put(ReverseSearchDocument document, Boolean commit)
        {
            if (document == null) return;

            try
            {
                updater.PostToIndex(document, commit);
            }
            finally
            {
                Interlocked.Increment(ref totalRecords);
            }
        }

        public static void Delete(String documentId, string url)
        {
            if (String.IsNullOrEmpty(documentId)) return;

            if (url.Substring(url.Length - 1, 1) != "/")
            {
                url = url + "/";
            }

            var request = (HttpWebRequest)WebRequest.Create(url + "update");
            request.Method = Constants.Post;

            var data = new ASCIIEncoding().GetBytes(String.Format(Constants.SolrDeleteBody, documentId));
            request.ContentLength = data.Length;

            using (var requestStream = request.GetRequestStream())
            {
                requestStream.Write(data, 0, data.Length);
                requestStream.Close();
            }

            var response = (HttpWebResponse)request.GetResponse();

            response.Close();
        }

        public void CommitChanges()
        {
            updater.Commit();
        }
    }
}
