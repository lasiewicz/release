﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet;
using Matchnet.SearchLoader.ValueObjects.ServiceDefinitions;
using Matchnet.SearchLoader.BusinessLogic;

namespace Matchnet.SearchLoader.ServiceManagers
{
    public class ReverseSearchPumpSM : MarshalByRefObject, IServiceManager, ISearchLoaderService
    {
        public void DeleteMember(int communityID, int memberID)
        {
            ReverseSearchPumpBL.Instance.DeleteMember(communityID, memberID);
        }

        #region IDisposable Members
        /// <summary>
        /// Dispose implementation.
        /// </summary>
        public void Dispose()
        {
            
        }
        #endregion

        public void PrePopulateCache()
        {
            // No implementation at this time.
        }

        public override object InitializeLifetimeService()
        { return null; }

    }
}
