using System;
using System.Collections;
using System.Text;
using Matchnet.Exceptions;
using Matchnet.FileExchange.ServiceManagers;

using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.InitialConfiguration;

namespace Matchnet.FileExchange.Service
{
    public partial class FileExchangeService : Matchnet.RemotingServices.RemotingServiceBase
    {

        private FileExchangeSM _fileExchangeSM;

        public FileExchangeService()
        {
            InitializeComponent();
        
        }

       


        private void InitializeComponent()
        {
            //CanPauseAndContinue = true;
            components = new System.ComponentModel.Container();
            this.ServiceName = FileExchangeSM.SERVICE_NAME;
            
            
        }

        protected override void RegisterServiceManagers()
        {
            try
            {
                _fileExchangeSM = new FileExchangeSM();
                base.RegisterServiceManager(_fileExchangeSM);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(FileExchangeSM.SERVICE_NAME, "Error occurred when starting the Windows Service", ex);
            }
        }

        protected override void OnStart(string[] args)
        {
            
            base.ServiceInstanceConfig = RuntimeSettings.GetServiceInstanceConfig(FileExchangeSM.SERVICE_CONSTANT, InitializationSettings.MachineName);			
            base.OnStart(args);
        }

        

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            base.OnStop();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }

                if (_fileExchangeSM != null)
                {
                    _fileExchangeSM.Dispose();
                }

               
            }
            base.Dispose(disposing);
        }
    }
}
