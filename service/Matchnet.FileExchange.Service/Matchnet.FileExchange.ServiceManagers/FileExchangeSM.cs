using System;
using System.Collections.Generic;
using System.Text;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.File;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;

using Matchnet.FileExchange.ServiceDefinitions;
using Matchnet.FileExchange.ValueObjects;
using Matchnet.FileExchange.BusinessLogic;

namespace Matchnet.FileExchange.ServiceManagers
{
    public class FileExchangeSM : MarshalByRefObject,IFileExchangeService, IServiceManager, IDisposable
    {
        public const string SERVICE_NAME = "Matchnet.FileExchange.Service";
        public const string SERVICE_CONSTANT = "FILEEXCHANGE_SVC";
        private const string SERVICE_MANAGER_NAME = "FileExchangeSM";
      

        public FileExchangeSM() { }

		public override object InitializeLifetimeService()
		{
			return null;
		}

        public void PrePopulateCache()
        {
        }

        public void Dispose()
        {
        }

        #region ftp

        public void Upload(string uri,string uid, string pwd, string sourecPath, string sourceFile, string destPath, string destFile)
        {
 try
            {
              FileExchangeBL.Instance.Upload( uri, uid,  pwd,  sourecPath,  sourceFile,  destPath,  destFile);
            }
            catch (Exception e)
            { throw; }

        }

        public void Upload(string processConstant, string sourceFile)
        {
            FileExchangeBL.Instance.Upload(processConstant,sourceFile);
        }
       
        public void Download(string uri, string uid, string pwd, string sourecPath, string sourceFile, string destPath, string destFile)
        {

            try
            {

                FileExchangeBL.Instance.Download(uri, uid, pwd, sourecPath, sourceFile, destPath, destFile);




            }
            catch (Exception e)
            { throw; }

        }

        public void Download(string processConstant, string sourceFile)
        {
           FileExchangeBL.Instance.Download(processConstant, sourceFile);
        }
       
        public void Delete(string uri, string uid, string pwd, string destPath, string destFile)
        {
            try
            {
                FileExchangeBL.Instance.Delete( uri,  uid,  pwd,  destPath,  destFile);
                }
            catch (Exception e)
            { throw; }
        }

        public void Delete(string processConstant, string sourceFile)
        {
            FileExchangeBL.Instance.Delete(processConstant, sourceFile);
        }

        #endregion
    }
}
