using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.FileExchange.ServiceDefinitions
{
    public interface IFileExchangeService
    {

        void Upload(string uri, string uid, string pwd, string sourcePath,string sourceFile, string destPath, string destFile);
        void Upload(string processConstant, string filename);

        void Download(string uri, string uid, string pwd, string sourcePath, string sourceFile, string destPath, string destFile);
        void Download(string processConstant, string filename);

        void Delete(string uri, string uid, string pwd, string destPath, string destFile);
        void Delete(string processConstant, string filename);
    }
}
