using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.FileExchange.ValueObjects
{
    public class SiteConfig
    {
        int id;
        int typeID;
        int siteorder;
        string directory;
        string desc = null;
        string uri = null;
        string uid = null;
        string pwd;
        string config;
        bool activeFlag;
        bool secureFlag;
        
        public int ID
        {
            get { return id; }
            set { id = value; }

        }

        public int TypeID
        {
            get { return typeID; }
            set { typeID = value; }
        }

        public int Order
        {
            get { return siteorder; }
            set { siteorder = value; }

        }
        public string Description
        {
            get { return desc; }
            set { desc = value; }

        }

        public string Uri
        {
            get { return uri; }
            set { uri = value; }

        }

        public string Directory
        {
            get { return directory; }
            set { directory = value; }
        }

        public string FullUri
        {
            get {
                string ret = "";
                if(!String.IsNullOrEmpty(directory))
                    ret=uri + "/" + directory;
                else
                    ret=uri;
                return ret;
            }
            }

        public string UserID
        {
            get { return uid; }
            set { uid = value; }

        }

        public string Password
        {
            get { return pwd; }
            set { pwd = value; }

        }


        public string ConfigStr
        {
            get { return config; }
            set { config = value; }

        }
        public bool IsActive
        {
            get { return activeFlag; }
            set { activeFlag = value; }

        }

        public bool IsSecure
        {
            get { return secureFlag; }
            set { secureFlag = value; }

        }

    }
}
