using System;
using System.Collections.Generic;
using System.Text;

namespace Matchnet.FileExchange.ValueObjects
{
    public class ProcessConfig
    {
        public  const int  STATUS_STARTED=0;
        public const  int STATUS_FILE_READY=1;
        public const  int STATUS_FILE_TRANSFERRED=2;
        public const  int STATUS_FILE_PROCESSED = 3;

        public const int PROCESS_TYPE_EXPORT = 1;
        public const int PROCESS_TYPE_IMPORT = 2;

        public const int FILE_ENCODING_ASCII = 1;
        public const int FILE_ENCODING_UTF8 = 2;
        public const int FILE_ENCODING_UTF16 = 3;
     
        public const int ENCRYPTION_TYPE_PGP=1;
        
        int id;
        string desc=null;
        string code = null;
        string sourceFile = null;
        string sourcePath = null;
        string tempPath = null;
        string destPath = null;
        string destFile = null;
        bool activeFlag;
        int encryptionID;
        int encryptionTypeID;
        string encryptionHomeDir = null;
        string encryptionPublicKey = null;
        string encryptionSecretKey = null;
        string encryptionPassKey = null;
        string encryptedExt = null;
        int encodingID;
        bool asyncFlag;
        int type;

        List<SiteConfig> siteList;

        public int ID
        {
            get { return id; }
            set { id = value; }

        }

        public int Type
        {
            get { return type; }
            set { type = value; }
        }

        public string Description
        {
            get { return desc; }
            set {desc = value; }

        }

        public string Code
        {
            get { return code; }
            set { code=value; }

        }
        public string SourcePath
        {
            get { return sourcePath; }
            set { sourcePath = value; }

        }

        public string TempPath
        {
            get { return tempPath; }
            set { tempPath = value; }

        }

        public string SourceFile
        {
            get { return sourceFile; }
            set { sourceFile = value; }

        }

        public string DestinationFile
        {
            get { return destFile; }
            set { destFile = value; }

        }
       
        public string DestinationPath
        {
            get { return destPath; }
            set { destPath = value; }

        }

        public  string SourceFilePath
        {
            get
            {
                return GetFilePath(sourcePath, sourceFile);
            }
        }

        public  string DestinationFilePath
        {
            get
            {
                return GetFilePath(destPath, destFile);
            }
        }

        public string TempFilePath
        {
            get
            {
                string ext = "";
                if (!String.IsNullOrEmpty(encryptedExt))
                {
                    if(!sourceFile.Contains(encryptedExt))
                    ext = "." + encryptedExt;

                }
                return GetFilePath(tempPath, sourceFile + ext);
            }
        }

        public bool IsActive
        {
            get { return activeFlag; }
            set { activeFlag = value; }

        }

        public int EncryptionID
        {
            get { return encryptionID; }
            set { encryptionID = value; }

        }

        public int EncodingID
        {
            get { return encodingID; }
            set { encodingID = value; }

        }

        public int EncryptionTypeID
        {
            get { return encryptionTypeID; }
            set { encryptionTypeID = value; }

        }

        public string EncryptionHomeDir
        {
            get { return encryptionHomeDir; }
            set { encryptionHomeDir = value; }

        }


        public string EncryptedExtension
        {
            get { return encryptedExt; }
            set { encryptedExt = value; }

        }

        public string EncryptionPublicKey
        {
            get { return encryptionPublicKey; }
            set { encryptionPublicKey = value; }

        }

        public string EncryptionSecretKey
        {
            get { return encryptionSecretKey; }
            set { encryptionSecretKey = value; }

        }

        public string EncryptionPassKey
        {
            get { return encryptionPassKey; }
            set { encryptionPassKey = value; }

        }

        public bool IsAsync
        {
            get { return asyncFlag; }
            set { asyncFlag = value; }

        }

        public List<SiteConfig> Sites
        {
            get { return siteList; }
            set { siteList = value; }
        }

        public void AddSite(SiteConfig s)
        {
            if (siteList == null)
                siteList = new List<SiteConfig>();
            siteList.Add(s);

        }

        public string GetFilePath(string path, string file)
        {
            string res="";
            try
            {
                if ( String.IsNullOrEmpty(path) || path.EndsWith("\\"))
                { res = path + file;}
                else
                { res = path + "\\" + file; }

                return res;
            }
            catch (Exception ex)
            { throw ex; }


        }
    }
}
