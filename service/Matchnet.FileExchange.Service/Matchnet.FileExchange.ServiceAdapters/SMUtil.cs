using System;
using System.Collections.Generic;
using System.Text;
using Matchnet.FileExchange.ServiceDefinitions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;
namespace Matchnet.FileExchange.ServiceAdapters
{
    internal class SMUtil
    {
        private SMUtil()
        {
        }


        internal static IFileExchangeService getService(string uri)
        {
            try
            {
                return (IFileExchangeService)Activator.GetObject(typeof(IFileExchangeService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }


        internal static string getServiceManagerUri()
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(FileExchangeSA.SERVICE_CONSTANT, PartitionMode.Random).ToUri(FileExchangeSA.SERVICE_MANAGER_NAME);
                string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("FILEEXCHANGESVC_SA_HOST_OVERRIDE");

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get configuration settings for remote service manager.", ex));
            }
        }


    }
}
