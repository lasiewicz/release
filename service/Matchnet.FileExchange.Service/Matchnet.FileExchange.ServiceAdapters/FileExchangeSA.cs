using System;
using System.Collections.Generic;
using System.Text;
using Matchnet.RemotingClient;
using Matchnet.FileExchange.ServiceManagers;
using Matchnet.Caching;
using Matchnet.CacheSynchronization.Context;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Configuration;
using Matchnet.Exceptions;

namespace Matchnet.FileExchange.ServiceAdapters
{
    public class FileExchangeSA:SABase
    {
        public const string SERVICE_NAME = "Matchnet.FileExchange.Service";
        public const string SERVICE_CONSTANT = "FILEEXCHANGE_SVC";
        public const string SERVICE_MANAGER_NAME = "FileExchangeSM";
        public static readonly FileExchangeSA Instance = new FileExchangeSA();
        private Cache _cache;

        private FileExchangeSA()
		{
			_cache = Cache.Instance;
		}

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("FILEEXCHANGESVC_SA_CONNECTION_LIMIT"));
        }

        public void Upload(string uri, string uid, string pwd, string sourcePath, string soureFile,string destPath,string destFile)
        {
            string serviceuri = "";

            try
            {
                serviceuri = SMUtil.getServiceManagerUri();
                base.Checkout(serviceuri);
                try
                {
                     SMUtil.getService(serviceuri).Upload(uri, uid, pwd,  sourcePath,soureFile ,destPath,destFile);
                }
                finally
                {
                    base.Checkin(serviceuri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to authenticate member. (uri: " + uri + ")", ex));
            } 
        }

        public void Upload(string processConstant, string sourceFile)
        {
            string serviceuri = "";
            try
            {
                serviceuri = SMUtil.getServiceManagerUri();
                base.Checkout(serviceuri);
                try
                {
                    SMUtil.getService(serviceuri).Upload(processConstant, sourceFile);
                }
                finally
                {
                    base.Checkin(serviceuri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to authenticate member. (uri: " + serviceuri + ")", ex));
            }
        }

        public void Download(string uri, string uid, string pwd, string sourcePath, string sourceFile, string destPath, string destFile)
        {
            string serviceuri = "";

            try
            {
                serviceuri = SMUtil.getServiceManagerUri();
                base.Checkout(serviceuri);
                try
                {
                    SMUtil.getService(serviceuri).Download(uri, uid, pwd, sourcePath, sourceFile, destPath, destFile);
                }
                finally
                {
                    base.Checkin(serviceuri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to authenticate member. (uri: " + uri + ")", ex));
            }
        }

        public void Download(string processConstant, string sourceFile)
        {
            string serviceuri = "";
            try
            {
                serviceuri = SMUtil.getServiceManagerUri();
                base.Checkout(serviceuri);
                try
                {
                    SMUtil.getService(serviceuri).Download(processConstant, sourceFile);
                }
                finally
                {
                    base.Checkin(serviceuri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to authenticate member. (uri: " + serviceuri + ")", ex));
            }
        }

        public  void Delete(string uri, string uid, string pwd, string destPath, string destFile)
            {
              string serviceuri = "";

            try
            {
                serviceuri = SMUtil.getServiceManagerUri();
                base.Checkout(serviceuri);
                try
                {
                     SMUtil.getService(serviceuri).Delete(uri, uid, pwd, destPath,destFile);
                }
                finally
                {
                    base.Checkin(serviceuri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to authenticate member. (uri: " + uri + ")", ex));
            } 
            }

        public void Delete(string processConstant, string sourceFile)
        {
            string serviceuri = "";
            try
            {
                serviceuri = SMUtil.getServiceManagerUri();
                base.Checkout(serviceuri);
                try
                {
                    SMUtil.getService(serviceuri).Delete(processConstant,  sourceFile);
                }
                finally
                {
                    base.Checkin(serviceuri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error occurred while attempting to authenticate member. (uri: " + serviceuri + ")", ex));
            }
        }
        }
        
    }

