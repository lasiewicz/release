using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Security;
using System.Security.Policy;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.IO;

using Matchnet.FileExchange.ValueObjects;

namespace Matchnet.FileExchange.BusinessLogic
{
    public class FileExchangeFTP
    {  
        public readonly static FileExchangeFTP Instance = new FileExchangeFTP();

        private FileExchangeFTP()
		{

        }

        #region public methods
        
        public bool Upload(ProcessConfig p)
        {
            try
            {
                bool res=false;

                if (p.EncodingID == ProcessConfig.FILE_ENCODING_UTF8)
                 {
                    convertUTF8(p.SourceFilePath, p.GetFilePath(p.TempPath, p.SourceFile));
                    p.SourcePath = p.TempPath;
                }

                
                if (p.EncryptionTypeID == ProcessConfig.ENCRYPTION_TYPE_PGP)
                {
                    Matchnet.PGP.Crypto.Instance.Encrypt(p.SourceFilePath, p.GetFilePath(p.EncryptionHomeDir, p.EncryptionPublicKey), p.TempFilePath);
                    p.SourcePath = p.TempPath;
                    if (!String.IsNullOrEmpty(p.EncryptedExtension))
                    {
                        p.SourceFile = p.SourceFile + "." + p.EncryptedExtension;
                    }
                }
                foreach (SiteConfig s in p.Sites)
                {
                    res=put(p, s);
                    if (res)
                    { break; }

                }

                return res;
                //if (!res)
                //{ throw (new Exception("Failed FTP file for " + p.Description)); }
            }
            catch (Exception ex)
            { throw (ex); }
            finally
            {
             
            }
        }
        
        public bool Delete(ProcessConfig p)
        {
            try
            {
                bool res = false;
                foreach (SiteConfig s in p.Sites)
                {
                    res = delete(p, s);
                    if (res)
                    { break; }

                }

                return res;
                //if (!res)
                //{ throw (new Exception("Failed Delete  file for " + p.Description)); }
            }
            catch (Exception ex)
            { throw (ex); }
            finally
            {

            }
        }
        
        public bool Download(ProcessConfig p)
        {
            try
            {
                bool res = false;

               
                foreach (SiteConfig s in p.Sites)
                {
                    res = get(p, s);
                    if (res)
                    { break; }

                }

                return res;
                //if (!res)
                //{ throw (new Exception("Failed to download file for " + p.Description)); }
            }
            catch (Exception ex)
            { throw (ex); }
            finally
            {

            }
        }
        #endregion

        #region private methods

        private Uri getUri(string uri, string path, string sourceFile, string destFile)
        {
            string serverUri = null;
            try
            {
                if (String.IsNullOrEmpty(sourceFile))
                { throw (new Exception("Source file cannot be empty")); }

                if (String.IsNullOrEmpty(destFile))
                { destFile = sourceFile; }

                if (String.IsNullOrEmpty(path))
                { serverUri = uri + "/" + destFile; }
                else
                { serverUri = uri + "/" + path + "/" + destFile; }

                UriBuilder uriBuilder = new UriBuilder(serverUri);

                if (uriBuilder.Uri.Scheme != Uri.UriSchemeFtp)
                { throw (new Exception("Invalid Uri.")); }

                return uriBuilder.Uri;

            }
            catch (Exception ex)
            { throw (ex); }
        }

        private FtpWebRequest setRequest(Uri serverUri, string uid, string pwd, bool secure, bool binary)
        {
            FtpWebRequest req;
            try
            {
                
                req = (FtpWebRequest)FtpWebRequest.Create(serverUri);
                req.EnableSsl =secure;
                
                req.Credentials = new NetworkCredential(uid, pwd);
                req.UseBinary = binary;
                req.KeepAlive = true;
                req.UsePassive = false;
                //req.ClientCertificates
                //infinite
                req.ReadWriteTimeout = -1;
                return req;
            }
            catch (Exception ex)
            { throw (ex); }
        }

        private bool put(ProcessConfig p, SiteConfig s)
        {
            FtpWebRequest req = null;
            FtpWebResponse response = null;
            try 
            {
                ServicePointManager.ServerCertificateValidationCallback =   new RemoteCertificateValidationCallback(CertValidationCb); 
                Uri serverUri = getUri(s.FullUri, p.DestinationPath, p.SourceFile, p.DestinationFile);
                req = setRequest(serverUri, s.UserID, s.Password, s.IsSecure, true);
               
                req.Method = WebRequestMethods.Ftp.UploadFile;
                using (Stream reqStream = req.GetRequestStream())
                {

                    const int bufferLength = 20480;
                    byte[] buffer = new byte[bufferLength];
                    int count = 0;
                    int readBytes = 0;
                    using (FileStream stream = File.OpenRead(p.SourceFilePath))
                    {
                        do
                        {   readBytes = stream.Read(buffer, 0, bufferLength);
                            reqStream.Write(buffer, 0, readBytes);
                            count += readBytes;
                        }
                        while (readBytes != 0);
                    }
                 }
                 response = (FtpWebResponse)req.GetResponse();
                    
               
                return true;
            }
            catch (Exception ex)
            { //return false; 
                throw new Exception("File: " + p.SourceFile + ", " + ex.Source + ", " + ex.Message + ", " + ex.ToString());
            }
            finally
            {   if (response != null)
                {
                    response.Close();
                    response = null;
                }
               
            }


        }

        private bool get(ProcessConfig p, SiteConfig s)
        {
            FtpWebRequest req = null;
            try
            {

                Uri serverUri = getUri(s.FullUri, p.SourcePath, p.SourceFile, p.DestinationFile);
                req = setRequest(serverUri, s.UserID, s.Password, s.IsSecure, true);

                req.Method = WebRequestMethods.Ftp.DownloadFile;
                using (FileStream stream = new FileStream(p.TempFilePath, FileMode.Create))
                {
                    using (Stream respStream = req.GetResponse().GetResponseStream())
                    {
                        const int bufferLength = 20480;
                        byte[] buffer = new byte[bufferLength];
                        int count = 0;
                        int readBytes = 0;

                        do
                        {
                            readBytes = respStream.Read(buffer, 0, bufferLength);
                            stream.Write(buffer, 0, readBytes);
                            count += readBytes;
                        }
                        while (readBytes != 0);


                    }
                    stream.Flush();
                }
                return true;
            }
            catch (Exception ex)
            { //return false;
                throw new Exception("File: " + p.SourceFile + ", " + ex.Source + ", " + ex.Message + ", " + ex.ToString());
            }
            finally
            { req = null; }


        }


        private void convertUTF8(string fileNameIn, string fileNameOut)
        {
            try
            {
                UTF8Encoding utf8 = new UTF8Encoding();
                UnicodeEncoding unicode = new UnicodeEncoding();
            
                using (FileStream stream = File.OpenRead(fileNameIn))
                {
                    using (TextWriter writer = new StreamWriter(fileNameOut))
                    {
                        int readBytes;
                        int bufferLength = 20480;
                        byte[] buffer = new byte[bufferLength];
                        do
                        {
                      
                          
                            readBytes = stream.Read(buffer, 0, bufferLength);
                            char[] unicodeChar = unicode.GetChars(buffer, 0, readBytes);
                            byte[] utf8Byte = utf8.GetBytes(unicodeChar);
                            char[] utfChar=utf8.GetChars(utf8Byte);
                            writer.Write(utfChar, 0, utfChar.GetLength(0));
                          
                        } while (readBytes != 0);
                        
                    }
                }
            }
            catch (Exception ex)
            { throw (ex); }


        }

        private bool delete(ProcessConfig p, SiteConfig s)
        {
            FtpWebRequest req = null;
            FtpWebResponse response = null;
            try
            {

                p.DestinationPath = "";
                Uri serverUri = getUri(s.FullUri, p.DestinationPath, p.DestinationFile, p.DestinationFile);
                req = setRequest(serverUri, s.UserID, s.Password, s.IsSecure, false);
                req.Method = WebRequestMethods.Ftp.DeleteFile;
                response = (FtpWebResponse)req.GetResponse();
                return true;
            }
            catch (Exception ex)
            { //return false;
                throw ex;
            }
            finally
            {
                req = null;
                if (response != null)
                { 
                    response.Close();
                    response = null;
                }
             }


        }

        public static bool CertValidationCb( object sender,  X509Certificate certificate,  X509Chain chain,  SslPolicyErrors sslPolicyErrors)
        {
            //if ((sslPolicyErrors & SslPolicyErrors.RemoteCertificateChainErrors)
            //          == SslPolicyErrors.RemoteCertificateChainErrors)
            //{
            //    return false;
            //}
            //else if ((sslPolicyErrors & SslPolicyErrors.RemoteCertificateNameMismatch)
            //                == SslPolicyErrors.RemoteCertificateNameMismatch)
            //{
            //    Zone z;
            //    z = Zone.CreateFromUrl(((HttpWebRequest)sender).RequestUri.ToString());
            //    if (z.SecurityZone == System.Security.SecurityZone.Intranet
            //      || z.SecurityZone == System.Security.SecurityZone.MyComputer)
            //    {
            //        return true;
            //    }
            //    return false;
            //}
            //return false;

            return true;
        } 
        #endregion
    }
}
