using System;
using System.Net;
using System.IO;
using System.Runtime.Remoting.Messaging;

using System.Data;
using System.Data.SqlClient;

using System.Collections.Generic;
using System.Text;
using Matchnet.FileExchange.ValueObjects;

using Matchnet.Data;
using Matchnet.Data.Hydra;
using Matchnet.Data.Configuration;
using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.Data.Exceptions;
using Matchnet.Security;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.PGP;
using Matchnet.Exceptions;

namespace Matchnet.FileExchange.BusinessLogic
{
    // delegate if file exchange process configured to be asynchronous
    public delegate bool ProcessFileDelegate(ProcessConfig process);
    public class FileExchangeBL
    {
        public readonly static FileExchangeBL Instance = new FileExchangeBL();

        const string LOGICAL_DB = "mnFileExchange";

        private FileExchangeBL()
		{

        }

        #region ftp
        
        public void Upload(string processConstant, string filename)
        {
            try
            {
                ProcessConfig p = getProcess(processConstant);
                p.SourceFile = filename;
                updateLogStatus(p, ProcessConfig.STATUS_FILE_READY,"");

                if (p.IsAsync)
                {
                    AsyncCallback callback = new AsyncCallback(EndProcess);
                    ProcessFileDelegate d = new ProcessFileDelegate(FileExchangeFTP.Instance.Upload);
                    d.BeginInvoke(p, callback, p);
                }
                else
                {
                    FileExchangeFTP.Instance.Upload(p);
                    updateLogStatus(p, ProcessConfig.STATUS_FILE_TRANSFERRED, "");
                }
            }
            catch (Exception ex)
            { //ServiceBoundaryException e= new ServiceBoundaryException("FileExchange.FileExchangeBL.Upload." + ex.Source, ex.Message, ex);
                handleServiceException("FileExchange.FileExchangeBL.Upload.", "", ex);
                ex = null;
            }
        }

        public void Upload(string uri, string uid, string pwd, string sourcePath, string sourceFile, string destPath, string destFile)
        {
            try
            {
                ProcessConfig process = getProcessObject(uri, uid, pwd, sourcePath, sourceFile, destPath, destFile);
                FileExchangeFTP.Instance.Upload(process);
               
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException("FileExchange.FileExchangeBL.Upload." + ex.Source, ex.Message, ex);
               
            }
        }

        public void Download(string uri, string uid, string pwd, string sourcePath, string sourceFile, string destPath, string destFile)
        {
            try
            {
                ProcessConfig process = getProcessObject(uri, uid, pwd, sourcePath, sourceFile, destPath, destFile);
                FileExchangeFTP.Instance.Download(process);
               
            }
            catch (Exception ex)
            {
               throw new ServiceBoundaryException("FileExchange.FileExchangeBL.Download." + ex.Source, ex.Message, ex);
               
            }
        }

        public void Download(string processConstant, string filename)
        {
            try
            {
                ProcessConfig p = getProcess(processConstant);
                p.SourceFile = filename;
                p.DestinationFile = filename;
                //if (p.EncryptionTypeID == ProcessConfig.ENCRYPTION_TYPE_PGP)
                //{ p.DestinationPath = p.TempPath;}

                if (p.IsAsync)
                {
                    AsyncCallback callback = new AsyncCallback(EndProcess);
                    ProcessFileDelegate d = new ProcessFileDelegate(FileExchangeFTP.Instance.Download);
                    d.BeginInvoke(p, callback, p);
                }
                else
                {
                    FileExchangeFTP.Instance.Download(p);
                    decryptPGP(p);
                    updateLogStatus(p, ProcessConfig.STATUS_FILE_TRANSFERRED, "");
                }
               
            }
            catch (Exception ex)
            {
                //ServiceBoundaryException e = new ServiceBoundaryException("FileExchange.FileExchangeBL.Download." + ex.Source, ex.Message, ex);
                handleServiceException("FileExchange.FileExchangeBL.Download.", "File:" + filename, ex);
                ex = null;
            }
        }

        public void Delete(string processConstant, string filename)
        {
            try
            {
                ProcessConfig p = getProcess(processConstant);
                p.DestinationFile = filename;
                FileExchangeFTP.Instance.Delete(p);
            }
            catch (Exception ex)
            {
                //ServiceBoundaryException e = new ServiceBoundaryException("FileExchange.FileExchangeBL.Delete." + ex.Source, ex.Message, ex);
                handleServiceException("FileExchange.FileExchangeBL.Delete.", "File:" + filename, ex);
                ex = null;
            }
        }

        public void Delete(string uri, string uid, string pwd, string destPath, string destFile)
        {
            try
            {
                ProcessConfig process = getProcessObject(uri, uid, pwd, "", "", destPath, destFile);
                FileExchangeFTP.Instance.Delete(process);
            }
            catch (Exception ex)
            { throw new ServiceBoundaryException("FileExchange.FileExchangeBL.Delete." + ex.Source,  ex.Message, ex); }
        }
        #endregion

        //call back for async upload/download
        public void EndProcess(IAsyncResult result)
        {
            AsyncResult ar ;
            ProcessConfig p=null ;
            ProcessFileDelegate d;
            try
            {
                ar = (AsyncResult)result;
                p = (ProcessConfig)ar.AsyncState;
                d = (ProcessFileDelegate)ar.AsyncDelegate;
                bool res = d.EndInvoke(result);
                if (res)
                {
                    decryptPGP(p);
                    updateLogStatus(p, ProcessConfig.STATUS_FILE_TRANSFERRED, ""); 
                }
            }
            catch (Exception ex)
            {
                //ServiceBoundaryException e = new ServiceBoundaryException("FileExchange.FileExchangeBL.EndProcess." + ex.Source, ex.Message, ex);
                handleServiceException("FileExchange.FileExchangeBL.EndProcess." ,"Filename: " + p.SourceFile, ex);
                ex = null;
            }

        }

        #region private functions

        private void handleServiceException(string addSource, string addMsg, Exception ex)
        {
            try
            {
               ServiceBoundaryException e = new ServiceBoundaryException(addSource + ex.Source,addMsg + " " +  ex.Message, ex);
            }
            catch (Exception e)
            {//this will be called in catch blosk so we don't want unhandled exception 
            }
        }


        private void throwException(string addSource, string addMsg, Exception ex)
        {
            try
            {
               Exception ex1=new Exception(string.Format("Source: {0}; {1} - {2}",addSource,addMsg,ex.Message));
               throw (ex1);
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        private ProcessConfig getProcess(string processConstant)
        {
            ProcessConfig p=null;
             DataSet dt=null;
            try
            {
                Command command = new Command(LOGICAL_DB, "dbo.up_Get_Process", 0);
                command.AddParameter("@ProcessConstant", SqlDbType.VarChar, ParameterDirection.Input, processConstant);
               
                dt = Client.Instance.ExecuteDataSet(command);
                p = getProcessObject(dt.Tables[0]);
                getProcessSites(p, dt.Tables[1]);
                return p;
            }
            catch (Exception ex)
            { throw (ex); }
            finally
            {   if(dt!=null)
                dt.Dispose(); }


        }
        
        private ProcessConfig getProcessObject(DataTable dt)
        { ProcessConfig p;
            try
            {
                if (dt == null)
                { throw new Exception("Process not found"); }

                if (dt.Rows.Count == 0)
                { throw new Exception("Process not found"); }

                DataRow rs = dt.Rows[0];
                p=new ProcessConfig();
                p.ID = (int) (rs["processid"]!=Convert.DBNull ? rs["processid"] : 0);
                p.Description = (string) (rs["processdesc"]!=Convert.DBNull ? rs["processdesc"] : "");
                p.Code = (string) (rs["processconstant"]!=Convert.DBNull ? rs["processconstant"] : "");
                p.SourcePath = (string) (rs["sourcepath"]!=Convert.DBNull ? rs["sourcepath"] : "");
                p.TempPath = (string)(rs["temppath"] != Convert.DBNull ? rs["temppath"] : "");
                p.DestinationPath = (string) (rs["destpath"]!=Convert.DBNull ? rs["destpath"] : "");
                p.Type = (int) (rs["typeid"]!=Convert.DBNull ? rs["typeid"] : 0);
                p.IsActive = (bool) (rs["activeflag"]!=Convert.DBNull ? rs["activeflag"] : false);
                p.IsAsync = (bool)(rs["asyncflag"] != Convert.DBNull ? rs["asyncflag"] : false);
                p.EncryptionID = (int) (rs["encryptionid"]!=Convert.DBNull ? rs["encryptionid"] : 0);
                p.EncryptionTypeID = (int)(rs["encryptiontypeid"] != Convert.DBNull ? rs["encryptiontypeid"] : 0);
                p.EncryptionHomeDir = (string)(rs["homedir"] != Convert.DBNull ? rs["homedir"] : "");
                p.EncryptionPublicKey = (string)(rs["publickey"] != Convert.DBNull ? rs["publickey"] : "");
                p.EncryptionSecretKey = (string)(rs["secretkey"] != Convert.DBNull ? rs["secretkey"] : "");
                p.EncryptedExtension = (string)(rs["encryptedfileext"] != Convert.DBNull ? rs["encryptedfileext"] : "");
                p.EncodingID = (int)(rs["encodingid"] != Convert.DBNull ? rs["encodingid"] : 0);

                string key = (string)(rs["passkey"] != Convert.DBNull ? rs["passkey"] : "");
                if(! String.IsNullOrEmpty(key))
                     {p.EncryptionPassKey = Matchnet.Security.Crypto.Decrypt(RuntimeSettings.GetSetting("KEY"), key);}

                return p;

            }
            catch (Exception ex)
            { throw (ex); }
            finally
            { }


        }

        public ProcessConfig getProcessObject(string uri, string uid, string pwd, string sourcePath, string sourceFile, string destPath, string destFile)
        {

            try
            {
                ProcessConfig pconfig = new ProcessConfig();
                SiteConfig sconfig = new SiteConfig();
                pconfig.SourceFile = sourceFile;
                pconfig.SourcePath = sourcePath;
                pconfig.DestinationPath = destPath;
                pconfig.DestinationFile = destFile;
                sconfig.Uri = uri;
                sconfig.UserID = uid;
                sconfig.Password = pwd;
                pconfig.AddSite(sconfig);
                return pconfig;
            }
            catch (Exception ex)
            { throw (ex); }

        }

        private void getProcessSites(ProcessConfig p,DataTable dt)
        {
            
            try
            {
                if (dt == null)
                { throw new Exception("Process Sites not found"); }

                if (dt.Rows.Count == 0)
                { throw new Exception("Process Sites not found"); }

                for (int k=0;k < dt.Rows.Count ;k++)
                {
                    DataRow rs = dt.Rows[k];

                    SiteConfig s = new SiteConfig();
                    s.ID = (int)(rs["siteid"] != Convert.DBNull ? rs["siteid"] : 0);
                    s.TypeID = (int) (rs["typeid"]!=Convert.DBNull ? rs["typeid"] : 0);
                    s.Description = (string) (rs["sitedesc"]!=Convert.DBNull ? rs["sitedesc"] : "");
                    s.Uri = (string) (rs["uri"]!=Convert.DBNull ? rs["uri"] : "");
                    s.UserID = (string) (rs["uid"]!=Convert.DBNull ? rs["uid"] : "");
                    string pwd = (string) (rs["pwd"]!=Convert.DBNull ? rs["pwd"] : "");
                    s.ConfigStr = (string) (rs["configstring"]!=Convert.DBNull ? rs["configstring"] : "");
                    s.IsActive = (bool) (rs["activeflag"]!=Convert.DBNull ? rs["activeflag"] : false);
                    s.Order = (int) (rs["siteorder"]!=Convert.DBNull ? rs["siteorder"] : 0);
                    s.IsSecure = (bool) (rs["secureflag"]!=Convert.DBNull ? rs["secureflag"] : false);
                    s.Directory = (string)(rs["directory"] != Convert.DBNull ? rs["directory"] : "");
                    s.Password = Matchnet.Security.Crypto.Decrypt(RuntimeSettings.GetSetting("KEY"), pwd);
                    p.AddSite(s);
                }
               

            }
            catch (Exception ex)
            { throw (ex); }
            finally
            { }


        }

        private void updateLogStatus(ProcessConfig p, int statusID, string logMsg)
        {
            try
            {
                SqlCommand command = new SqlCommand( "up_UpdateExchangeLog");
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter param = new SqlParameter("@ProcessID", SqlDbType.Int);
                param.Value = p.ID;
                command.Parameters.Add (param);

                if (!String.IsNullOrEmpty(p.EncryptedExtension))
                {
                    p.SourceFile = p.SourceFile.Replace("." + p.EncryptedExtension, "");
                }
                param=new SqlParameter("@FileName", SqlDbType.VarChar, 200);
                param.Value = p.SourceFile;
                command.Parameters.Add(param);


                param=new SqlParameter("@StatusID", SqlDbType.Int);
                param.Value = statusID;
                command.Parameters.Add(param);

                param=new SqlParameter("@LogMsg", SqlDbType.VarChar, 1000);
                param.Value = logMsg;
                command.Parameters.Add(param);

                writeToDatabase(command);
            }
            catch (Exception ex)
            { throwException("FileExchange.FileExchangeBL.updateLogStatus.","", ex); }
            finally
            {}


        }

        private void decryptPGP(ProcessConfig p)
        {
            if (p.EncryptionTypeID == ProcessConfig.ENCRYPTION_TYPE_PGP && p.Type==ProcessConfig.PROCESS_TYPE_IMPORT)
            {
                if (!String.IsNullOrEmpty(p.EncryptedExtension))
                {
                    p.DestinationFile = p.DestinationFile.Replace("." + p.EncryptedExtension,"");
                }
                Matchnet.PGP.Crypto.Instance.Decrypt(p.TempFilePath, p.GetFilePath(p.EncryptionHomeDir, p.EncryptionPublicKey), p.GetFilePath(p.EncryptionHomeDir, p.EncryptionSecretKey), p.EncryptionPassKey, p.DestinationFilePath);
            }
        }


        private void writeToDatabase(SqlCommand cmd)
        {
            try
            {
                PhysicalDatabases physicalDatabases = getPhysicalDB(LOGICAL_DB);
			    Int32 pdbCount = physicalDatabases.Count;
                if (pdbCount == 0)
                { throw new Exception("No active physical database found for " + LOGICAL_DB); }

	            for (int i = 0; i < pdbCount; i++)
                {
                        PhysicalDatabase pdb = physicalDatabases[i];
                        if (pdb.IsActive)
                        {
                            executeCommand(cmd, pdb);
                        }
                }
                }

            catch (Exception ex)
                { throwException("FileExchange.FileExchangeBL.writeToDatabase.", "",ex); }
            finally { }


        }

        private void executeCommand(SqlCommand cmd, PhysicalDatabase db)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(db.ConnectionString))
                {
                     cmd.Connection = conn;
                     conn.Open();
                     cmd.ExecuteNonQuery();
    
                }
            }
            catch (Exception ex)
            { handleServiceException("FileExchange.FileExchangeBL.executeCommand.","DB: " + db.ServerName + '.' + db.PhysicalDatabaseName, ex); }

        }

        private PhysicalDatabases getPhysicalDB(string logicalDB)
        {
            try
            {
                Partition partition = ConnectionDispenser.Instance.GetLogicalDatabase(logicalDB).GetPartition(0);
                PhysicalDatabases physicalDatabases = partition.PhysicalDatabases;
                return physicalDatabases;
            }
            catch (Exception ex)
            {
                throwException("FileExchange.FileExchangeBL.getPhysicalDB.", "", ex);
                throw (ex);
            }


        }
        #endregion
    }
}
