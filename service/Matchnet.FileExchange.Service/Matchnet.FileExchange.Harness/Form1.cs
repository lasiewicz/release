using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Net;
using System.Net.Security;
using System.Security.Policy;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.IO;

using Matchnet.FileExchange.ServiceAdapters;
namespace WindowsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //FileExchangeSA  fileSA;
            try
            {
                //FileExchangeSA.Instance.Upload("ftp://enzo.postdirect.com", "spark", "xuXXDfOg", "C:\\ftp", "test.txt", "", "test.txt");
                //FileExchangeSA.Instance.Upload("MATCHMAIL", "200612071730_users_01.txt");
                FileExchangeSA.Instance.Upload(txtProcessConst.Text, txtFileName.Text);

            }
            catch (Exception ex)
            { }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                //FileExchangeSA.Instance.Delete("MATCHMAIL", "200612071730_users_01.txt");
                FileExchangeSA.Instance.Delete(txtProcessConst.Text, txtFileName.Text);
            }
            catch (Exception ex)
            { }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                FileExchangeSA.Instance.Download(txtProcessConst.Text, txtFileName.Text);
            }
            
            catch (Exception ex)
            { }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void listDir(string serverUri, string uid, string pwd )
        {
            try
            {
                UriBuilder uriBuilder = new UriBuilder(serverUri);
                FtpWebRequest req = (FtpWebRequest)FtpWebRequest.Create(uriBuilder.Uri);

                req.Credentials = new NetworkCredential(uid, pwd);
                req.UseBinary = true;
                req.KeepAlive = true;
                req.UsePassive = false;

                req.ReadWriteTimeout = -1;

                req.Method = WebRequestMethods.Ftp.ListDirectory;
                using (StreamReader reader = new StreamReader(req.GetResponse().GetResponseStream()))
                {
                    string s = reader.ReadLine();
                    while (s != null)
                    {
                        s = reader.ReadLine();
                    }

                }


            }

            catch (Exception e)
            { }
            finally { }

                  }

        private void button4_Click(object sender, EventArgs e)
        {
            listDir("ftp://enzo.postdirect.com/export-profile","spark","xuXXDfOg");

        }

        private void button5_Click(object sender, EventArgs e)
        {
            //NOTE: ensure your source file is saved as unicode
            //encode
            UTF8Encoding utf8 = new UTF8Encoding();
            UnicodeEncoding unicode = new UnicodeEncoding();

            using (FileStream stream = File.OpenRead(@"C:\matchnet\bedrock\ClickMailExportedData_20140418_Test2.txt"))
            {
                using (TextWriter writer = new StreamWriter(@"C:\matchnet\bedrock\ClickMailExportedData_20140418_Test2_Encoded.txt"))
                {
                    int readBytes;
                    int bufferLength = 20480;
                    byte[] buffer = new byte[bufferLength];
                    do
                    {


                        readBytes = stream.Read(buffer, 0, bufferLength);
                        char[] unicodeChar = unicode.GetChars(buffer, 0, readBytes);
                        byte[] utf8Byte = utf8.GetBytes(unicodeChar);
                        char[] utfChar = utf8.GetChars(utf8Byte);
                        writer.Write(utfChar, 0, utfChar.GetLength(0));

                    } while (readBytes != 0);

                }
            }

            //encrypt
            Matchnet.PGP.Crypto.Instance.Encrypt(@"C:\matchnet\bedrock\ClickMailExportedData_20140418_Test2_Encoded.txt", @"C:\matchnet\bedrock\spark.asc", @"C:\matchnet\bedrock\ClickMailExportedData_20140418_Test2.txt.pgp");
        }

          
        
    }
}