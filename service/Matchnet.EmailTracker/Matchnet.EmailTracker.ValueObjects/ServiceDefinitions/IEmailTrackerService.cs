﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.EmailTracker.ValueObjects.ServiceDefinitions
{
    public interface IEmailTrackerService
    {
        bool AddTrackedEmail(int pSenderMemberId, int pRecipientMemberId, int pMessageListId, int pSenderBrandId, int pPrivilegeTypeId);
        bool AddTrackedEmail(int pSenderMemberId, int pRecipientMemberId, int pMessageListId, int pSenderBrandId, int pPrivilegeTypeId, bool startWithReadStatus);

        bool RemoveTrackedEmail(int pMessageListId);

        bool MarkAsRead(int pMessageListId);
        bool MarkAsRead(int pMessageListId, bool isRead);
    }
}
