﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.EmailTracker.ValueObjects.Types
{
    public enum ReadStatus : int
    {
        NotRead = 0,
        Read = 1
    }

    public enum ActionStatus : int
    {
        None = 0,
        Nudged = 1,
        Refunded = 2,
        Removed = 3,
        Error = 4
    }
}
