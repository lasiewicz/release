﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.EmailTracker.ValueObjects
{
    public class ServiceConstants
    {
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_CONSTANT = "EMAILTRACKER_SVC";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_NAME = "Matchnet.EmailTracker.Service";
        /// <summary>
        /// 
        /// </summary>
        public const string SERVICE_MANAGER_NAME = "EmailTrackerSM";
    }
}
