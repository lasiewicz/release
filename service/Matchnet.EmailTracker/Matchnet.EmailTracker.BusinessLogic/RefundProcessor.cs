﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Email.ServiceAdapters;
using Matchnet.EmailTracker.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Spark.Common.Adapter;
using Matchnet.DistributedCaching.ServiceAdapters;
using Spark.Logging;
using Spark.UnifiedPurchaseSystem.Lib.Common;

namespace Matchnet.EmailTracker.BusinessLogic
{
    public class RefundProcessor
    {
        public static readonly RefundProcessor Instance = new RefundProcessor();

        private const string NAMED_CACHE_NAME = "emailtracker";
        private bool _runnable = false;
        private int _threadSleepTime = 5000;
        private int _errorThreadSleepTime = 10000;
        private Thread _workThread = null;
        private ISettingsSA _settingSA = null;
        private IAccessServiceAdapter _accessAdapter = null;
        private IEmailMessageSA _emailMessageSA = null;
        private IExternalMailSA _externalMailSA = null;
        private IGetMember _memberSA = null;

        #region Interfaces to external services RefundProcessor use
        public ISettingsSA SettingSA
        {
            get { return _settingSA ?? (_settingSA = RuntimeSettings.Instance); }
            set { _settingSA = value; }
        }

        public IAccessServiceAdapter AccessAdapter
        {
            get { return _accessAdapter ?? (_accessAdapter = new AccessServiceAdapter()); }
            set { _accessAdapter = value; }
        }

        public IEmailMessageSA EmailMessageSA
        {
            get { return _emailMessageSA ?? (_emailMessageSA = Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance); }
            set { _emailMessageSA = value; }
        }

        public IExternalMailSA ExternalMailSA
        {
            get {
                return _externalMailSA ??
                       (_externalMailSA = Matchnet.ExternalMail.ServiceAdapters.ExternalMailSA.Instance);
            }
            set { _externalMailSA = value; }
        }

        public IGetMember MemberSA
        {
            get
            {
                return _memberSA ??
                       (_memberSA = Matchnet.Member.ServiceAdapters.MemberSA.Instance);
            }
            set { _memberSA = value; }
        }
        #endregion

        private RefundProcessor()
        {}

        public void Start()
        {
            Trace.WriteLine("RefundProcessor.Start()");
            _runnable = true;
            _workThread = new Thread(new ThreadStart(DoWork));
            _workThread.Start();
        }

        public void Stop()
        {
            Trace.WriteLine("RefundProcessor.Stop()");
            _runnable = false;
            if(AccessAdapter != null)
            {
                AccessAdapter.CloseProxyInstance();
            }

            if(_workThread != null)
            {
                _workThread.Join(10000);
            }
        }

        private void DoWork()
        {
            try
            {
                while(_runnable)
                {
                    try
                    {
                        // get the refund jobs from db
                        List<TrackedEmailDO> jobs = TrackedEmailDAL.Instance.GetRefundJobs();

                        if(jobs != null)
                        {
                            foreach(TrackedEmailDO job in jobs)
                            {
                                try
                                {
                                    var brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(job.SenderBrandId);

                                    // give refund by calling Access service
                                    var returnCode = GiveRefund(job.MessageListId, job.RecipientMemberId, brand.Site.Community.CommunityID,
                                        job.SenderMemberId, job.PrivilegeTypeId, brand.Site.SiteID);

                                    // update the db with the "refunded" action status
                                    if (returnCode == 0)
                                    {
                                        TrackedEmailDAL.Instance.MarkAsRefunded(job.MessageListId);
                                        // send external mail to the sender
                                        SendExternalMail(job.SenderMemberId, job.SenderBrandId, brand.Site.Community.CommunityID, job.RecipientMemberId);
                                    }
                                    else
                                    {
                                        // the problem we are experiencing is that Email service just stops responding time to time. when this happens
                                        // Email service is not available for some time, so we are adding the retry logic for that.  if we got an error
                                        // from Access service, don't retry and just mark it as an error
                                        if (returnCode == 2)
                                        {
                                            TrackedEmailDAL.Instance.MarkAsError(job.MessageListId);
                                        }
                                        else
                                        {
                                            // set the next retry date if retry count is not maxed out yet
                                            SetRetryDate(job);
                                        }
                                    }
                                }
                                catch (Exception foreachExc)
                                {
                                    EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "RefundProcessor.DoWork() loop failed. Will proceed to next job. " + foreachExc.Message, EventLogEntryType.Error);

                                }
                            }
                        }

                        // put thread to sleep
                        Thread.Sleep(_threadSleepTime);
                    }
                    catch (Exception ex)
                    {
                        EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "RefundProcessor.DoWork() loop failed. Will sleep and continue with the next iteration. " + ex.Message, EventLogEntryType.Error);
                        Thread.Sleep(_errorThreadSleepTime);
                    }
                }
            }
            catch (Exception exception)
            {
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "RefundProcessor.DoWork() startup failed. Refunding mechanism for VIP emails is completely disabled right now. " + exception.Message, EventLogEntryType.Error);                  
            }
        }

        /// <summary>
        /// If the retry is at max already, the job is marked as Error.  If still within retry max count, set the next retry date.
        /// </summary>
        /// <param name="job"></param>
        private void SetRetryDate(TrackedEmailDO job)
        {
            var maxRetryCount = Convert.ToInt32(SettingSA.GetSettingFromSingleton("EMAILTRACKER_REFUND_RETRY_COUNT"));
            if(job.RetryCount + 1 >= maxRetryCount)
            {
                TrackedEmailDAL.Instance.MarkAsError(job.MessageListId);
                TrackedEmailDAL.Instance.UpdateRetry(job.MessageListId, DateTime.MinValue,
                                                 job.RetryCount + 1);
                return;
            }

            var retryIntervalinMinutes = Convert.ToInt32(SettingSA.GetSettingFromSingleton("EMAILTRACKER_REFUND_RETRY_INTERVAL_MINUTES"));
            TrackedEmailDAL.Instance.UpdateRetry(job.MessageListId, DateTime.Now.AddMinutes(retryIntervalinMinutes),
                                                 job.RetryCount + 1);
        }

        private void SendExternalMail(int pSenderMemberId, int pSenderBrandId, int pCommunityId, int pRecipientMemberId)
        {
            try
            {
                int recipientBrandId;
                var recipient = MemberSA.GetMember(pRecipientMemberId, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
                recipient.GetLastLogonDate(pCommunityId, out recipientBrandId);

                ExternalMailSA.SendAllAccessReturnEmail(pSenderMemberId, pSenderBrandId,
                   pRecipientMemberId, recipientBrandId);
                    
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "SendExternalMail() failed, but the refund was given." + ex.Message, EventLogEntryType.Error);
            }
            
        }

        /// <summary>
        /// This method will try to mark off the free reply flag on the email.  Once this happens successfully 
        /// </summary>
        /// <param name="pMessageListId"></param>
        /// <param name="pRecipientMemberId"></param>
        /// <param name="pCommunityId"></param>
        /// <param name="pSenderMemberId"></param>
        /// <param name="pPrivilegeTypeId"></param>
        /// <param name="pSiteId"></param>
        /// <returns>0 - no error, 1 - failed at marking off free reply flag, 2 - failed at calling Access</returns>
        private int GiveRefund(int pMessageListId, int pRecipientMemberId, int pCommunityId, int pSenderMemberId, int pPrivilegeTypeId, int pSiteId)
        {
            RollingFileLogger.Instance.EnterMethod(ServiceConstants.SERVICE_CONSTANT, "RefundProcessor", "GiveRefund");

            //Spark.Common.Adapter.AccessServiceAdapter adapter = new AccessServiceAdapter();
            Spark.Common.AccessService.AccessResponse resp = null;
            var successfulStep = 0;
            var returnCode = 0;
            try
            {
                var messageListIds = new ArrayList(1);
                messageListIds.Add(pMessageListId);

                bool oneFreeReplyTakenAway = false;
                try
                {
                    // flip the free bit off on the email message
                    RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, "RefundProcessor",
                                                              string.Format(
                                                                  "Calling Email SVC with RecipientMemberID: {0} CommunityID: {1} MessageListID: {2}",
                                                                  pRecipientMemberId, pCommunityId, pMessageListId),
                                                              null);
                    EmailMessageSA.CallerAppName = ServiceConstants.SERVICE_CONSTANT;
                    oneFreeReplyTakenAway = EmailMessageSA.MarkOneFreeReply(pRecipientMemberId,
                                                                            pCommunityId, false, messageListIds, true);
                    
                    if(oneFreeReplyTakenAway)
                    {
                        successfulStep++;
                    }
                }
                catch (Exception ex)
                {
                    var errorMessage =
                        string.Format(
                            "Calling Email Service to mark off FreeReply mask value caused an exception.  Exception message: {0}, InnerException: {1}",
                            ex.Message, ex.InnerException == null ? string.Empty : ex.InnerException.Message);
                    EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, errorMessage, EventLogEntryType.Error);
                    RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_CONSTANT, "RefundProcessor", ex);
                }

                if (!oneFreeReplyTakenAway)
                {
                    // if one free reply wasn't cleared, don't bother with the next step
                    returnCode = 1;
                }
                else
                {
                    // since free reply mask was taken away, call access to give credit back
                    try
                    {
                        //Spark.Common.AccessService.AccessServiceClient client = adapter.GetProxyInstance();
                        RollingFileLogger.Instance.LogInfoMessage(ServiceConstants.SERVICE_CONSTANT, "RefundProcessor",
                                                                  string.Format(
                                                                      "Calling Access SVC with SenderMemberID: {0} PrivilegeTypeID: {1} SiteID: {2} ",
                                                                      pSenderMemberId, pPrivilegeTypeId, pSiteId),
                                                                  null);
                        resp = AccessAdapter.AdjustCountPrivilege(pSenderMemberId, new[] {pPrivilegeTypeId}, 1,
                                                                  Constants.NULL_INT,
                                                                  Constants.NULL_STRING, Constants.NULL_INT,
                                                                  pSiteId,
                                                                  (int)
                                                                  SystemType
                                                                      .EmailTracker);

                        returnCode = resp.response.ToLower() == "success" ? 0 : 2;
                    }
                    catch (Exception ex)
                    {
                        var errorMessage = resp == null
                                               ? "Response from Access is NULL"
                                               : string.Format("Message from Access Response: {0}", resp.message);
                        EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, errorMessage, EventLogEntryType.Error);
                        RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_CONSTANT, "RefundProcessor", ex);
                        returnCode = 2;
                    }
                }

                RollingFileLogger.Instance.LeaveMethod(ServiceConstants.SERVICE_CONSTANT, "RefundProcessor", "GiveRefund");
                return returnCode;
                
            }
            catch (Exception ex)
            {
                var errorMessage =
                   string.Format(
                       "General exception inside GiveRefund().  Exception message: {0}",
                       ex.Message);
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, errorMessage, EventLogEntryType.Error);
                RollingFileLogger.Instance.LogException(ServiceConstants.SERVICE_CONSTANT, "RefundProcessor", ex);

                return successfulStep == 0 ? 1 : 2;
            }
            
        }

        private bool IsInCache(int pMessageListId)
        {
            try
            {
                List<int> idList = DistributedCachingSA.Instance.GetIntList(pMessageListId.ToString(), NAMED_CACHE_NAME);
                if (idList != null && idList.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                //return Matchnet.DistributedCachingDirect.DistributedCachingMT.Instance.Get(pMessageListId.ToString(),
                //                                                                           NAMED_CACHE_NAME) != null;
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "IsInCache exception - " + ex.Message , EventLogEntryType.Error);
                return false;

            }
        }

        private void AddToCache(int pMessageListId)
        {
            List<int> idList = new List<int>();
            idList.Add(pMessageListId);
            DistributedCachingSA.Instance.Put(pMessageListId.ToString(), idList, NAMED_CACHE_NAME);
            //Matchnet.DistributedCachingDirect.DistributedCachingMT.Instance.Put(pMessageListId.ToString(), pMessageListId, NAMED_CACHE_NAME);
        }

    }
}
