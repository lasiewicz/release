﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using Matchnet.Email.ServiceAdapters;
using Matchnet.EmailTracker.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;

namespace Matchnet.EmailTracker.BusinessLogic
{
    public class NudgeProcessor
    {
        public static readonly NudgeProcessor Instance = new NudgeProcessor();

        private bool _runnable = false;
        private int _threadSleepTime = 5000;
        private int _errorThreadSleepTime = 10000;
        private Thread _workThread = null;
        private IEmailMessageSA _emailMessageSA = null;
        private IExternalMailSA _externalMailSA = null;
        private IGetMember _memberSA = null;

        public IEmailMessageSA EmailMessageSA
        {
            get { return _emailMessageSA ?? (_emailMessageSA = Matchnet.Email.ServiceAdapters.EmailMessageSA.Instance); }
            set { _emailMessageSA = value; }
        }

        public IExternalMailSA ExternalMailSA
        {
            get
            {
                return _externalMailSA ??
                       (_externalMailSA = Matchnet.ExternalMail.ServiceAdapters.ExternalMailSA.Instance);
            }
            set { _externalMailSA = value; }
        }

        public IGetMember MemberSA
        {
            get
            {
                return _memberSA ??
                       (_memberSA = Matchnet.Member.ServiceAdapters.MemberSA.Instance);
            }
            set { _memberSA = value; }
        }

        private NudgeProcessor()
        {}

        public void Start()
        {
            Trace.WriteLine("NudgeProcessor.Start()");
            _runnable = true;

            _workThread = new Thread(new ThreadStart(DoWork));
            _workThread.Start();
        }

        public void Stop()
        {
            Trace.WriteLine("NudgeProcessor.Stop()");
            _runnable = false;
            if(_workThread != null)
            {
                _workThread.Join(10000);
            }
        }

        private void DoWork()
        {
            try
            {
                while(_runnable)
                {
                    try
                    {
                        // Get work from DB
                        List<TrackedEmailDO> jobs = GetWorkFromDB();

                        if (jobs != null)
                        {
                            foreach (TrackedEmailDO job in jobs)
                            {
                                // For each row, call ExternalMail to send a nudge email
                                try
                                {
                                    SendExternalMail(job);

                                    // Update the database with the ActionStatus as "nudged"
                                    TrackedEmailDAL.Instance.MarkAsNudged(job.MessageListId);
                                }
                                catch (Exception jobEx)
                                {
                                    string jobformat="RecpMember={0};Sender={1};MessageListID={2};SenderBrand={3};";
                                    if (job != null)
                                        jobformat = String.Format(jobformat, job.RecipientMemberId, job.SenderMemberId, job.MessageListId, job.SenderBrandId);
                                    EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, String.Format("NudgeProcessor.DoWork() Job processing failed. Job={0}. Exception ={1} ", jobformat, jobEx.Message), EventLogEntryType.Error);
                                }
                            }
                        }

                        // When done with this load from DB, thread sleep
                        Thread.Sleep(_threadSleepTime);
                    }
                    catch (Exception ex)
                    {
                        EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "NudgeProcessor.DoWork() loop failed. Will sleep and continue with the next iteration. " + ex.Message, EventLogEntryType.Error);
                        Thread.Sleep(_errorThreadSleepTime);
                    }
                }
            }
            catch (Exception exception)
            {   
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "NudgeProcessor.DoWork() startup failed. Nudge emails are completely disabled right now. " + exception.Message, EventLogEntryType.Error);                  
            }
        }

        private List<TrackedEmailDO> GetWorkFromDB()
        {
            return TrackedEmailDAL.Instance.GetNudgeJobs();
        }

        private void SendExternalMail(TrackedEmailDO pJob)
        {
            var brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(pJob.SenderBrandId);
            
            int recipientBrandId;
            var recipient = MemberSA.GetMember(pJob.RecipientMemberId, Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);
            recipient.GetLastLogonDate(brand.Site.Community.CommunityID, out recipientBrandId);

            // Check to see if the messagelist is still there.  Not doing anything when NULL will mark this tracked email as "nudged" although it hasn't been nudged.
            // This is needed so that the EmailTracker later considers this email for a refund.  We could however mark this email as "removed" (int value of 3 in the db) if
            // we don't have to worry about giving the sender a refund.
            // When the messagelist is missing, it throws an exception, so trap that and treat it as such.
            try
            {
                var emailMessage = EmailMessageSA.RetrieveMessage(pJob.RecipientMemberId, brand.Site.Community.CommunityID, pJob.MessageListId, true);

                if (emailMessage != null)
                {
                    ExternalMailSA.SendAllAccessNudgeEmail(pJob.SenderMemberId, pJob.SenderBrandId, pJob.MessageListId, pJob.RecipientMemberId,
                        recipientBrandId, emailMessage.Subject, emailMessage.Content, emailMessage.InsertDate);
                }
            }
            catch (Exception exception)
            {
                // couldn't find the messagelist.  assume that the recipient deleted this messagelist and let the method complete so that
                // the caller thinks it has been nudged.
                EventLog.WriteEntry(ServiceConstants.SERVICE_NAME, "NudgeProcessor.SendExternalMail() - Unable to find the messagelist. Recipient most likely deleted it before reading it. Exception message: " + exception.Message, EventLogEntryType.Warning);                  
            }
        }
    }
}
