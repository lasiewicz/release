﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Matchnet.Data;
using Matchnet.Exceptions;
using Matchnet.EmailTracker.ValueObjects.Types;

namespace Matchnet.EmailTracker.BusinessLogic
{
    public class EmailTrackerBL
    {
        public static readonly EmailTrackerBL Instance = new EmailTrackerBL();

        private EmailTrackerBL()
        {
            Trace.Write("EmailTrackerBL contructor.");
        }

        public bool AddTrackedEmail(int pSenderMemberId, int pRecipientMemberId, int pMessageListId, int pSenderBrandId, int pPrivilegeTypeId, bool startWithReadStatus)
        {
            return TrackedEmailDAL.Instance.AddTrackedEmail(pSenderMemberId, pRecipientMemberId, pMessageListId, pSenderBrandId, pPrivilegeTypeId, startWithReadStatus);
        }

        public bool RemoveTrackedEmail(int pMessageListId)
        {
            return TrackedEmailDAL.Instance.RemoveTrackedEmail(pMessageListId);
        }

        public bool MarkAsRead(int pMessageListId, bool isRead)
        {
            return TrackedEmailDAL.Instance.MarkAsRead(pMessageListId, isRead);
        }
    }
}
