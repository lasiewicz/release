﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Matchnet.Data;
using Matchnet.EmailTracker.ValueObjects.Types;
using Matchnet.Exceptions;

namespace Matchnet.EmailTracker.BusinessLogic
{
    public sealed class TrackedEmailDAL
    {
        private static readonly TrackedEmailDAL _instance = new TrackedEmailDAL();

        private TrackedEmailDAL()
        { }

        public static TrackedEmailDAL Instance
        {
            get { return _instance; }
        }

        public List<TrackedEmailDO> GetNudgeJobs()
        {
            try
            {
                return GetJobs(ActionStatus.Nudged, 10);
            }
            catch (Exception exception)
            {
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new BLException("TrackedEmailDAL.GetNudgeJobs() exception with SqlException " + sqlException.Message, exception);
                }
                else
                {
                    throw new BLException("TrackedEmailDAL.GetNudgeJobs() exception", exception);
                }
            }
        }

        public List<TrackedEmailDO> GetRefundJobs()
        {
            try
            {
                return GetJobs(ActionStatus.Refunded, 15);
            }
            catch (Exception exception)
            {
                SqlException sqlException = exception.InnerException as SqlException;
                if (sqlException != null)
                {
                    throw new BLException("TrackedEmailDAL.GetRefundJobs() exception with SqlException " + sqlException.Message, exception);
                }
                else
                {
                    throw new BLException("TrackedEmailDAL.GetRefundJobs() exception", exception);
                }
            }
        }

        private List<TrackedEmailDO> GetJobs(ActionStatus pActionStatus, int pDaysToLookBack)
        {
            Command command = new Command("mnAlertWrite", "up_TrackedEmail_List", 0);
            command.AddParameter("@WorkType", SqlDbType.Int, ParameterDirection.Input, (int)pActionStatus);
            command.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now.AddDays(-pDaysToLookBack));
            DataSet ds = Client.Instance.ExecuteDataSet(command);
            DataTable dt = ds.Tables[0];

            if (dt.Rows.Count < 1)
                return null;

            List<TrackedEmailDO> ret = new List<TrackedEmailDO>();
            foreach (DataRow aRow in dt.Rows)
            {
                ret.Add(GetTrackedEmailDO(aRow));
            }

            return ret;
        }

        private TrackedEmailDO GetTrackedEmailDO(DataRow pRow)
        {
            var trackedEmail =  new TrackedEmailDO()
                       {
                           ActionStatus = (ActionStatus)Convert.ToInt32(pRow["ActionStatus"]),
                           MessageListId = Convert.ToInt32(pRow["MessageListID"]),
                           ReadStatus = (ReadStatus)Convert.ToInt32(pRow["ReadStatus"]),
                           RecipientMemberId = Convert.ToInt32(pRow["RecipientMemberID"]),
                           SenderBrandId = Convert.ToInt32(pRow["SenderBrandID"]),
                           SenderMemberId = Convert.ToInt32(pRow["SenderMemberID"]),
                           PrivilegeTypeId = Convert.ToInt32(pRow["PrivilegeTypeID"])
                       };


            trackedEmail.NextRetryDate = DBNull.Value.Equals(pRow["NextRetryDate"])
                                             ? DateTime.MinValue
                                             : Convert.ToDateTime(pRow["NextRetryDate"]);
            
            trackedEmail.RetryCount = DBNull.Value.Equals(pRow["RetryCount"]) ? 0 : Convert.ToInt32(pRow["RetryCount"]);


            return trackedEmail;
        }

        private void UpdateActionStatus(int pMessageListId, ActionStatus pActionStatus)
        {
            Command command = new Command("mnAlertSaveMM", "up_TrackedEmail_SaveStatus", 0);

            command.AddParameter("@MessageListID", SqlDbType.Int, ParameterDirection.Input, pMessageListId);
            command.AddParameter("@ActionStatus", SqlDbType.TinyInt, ParameterDirection.Input, (int)pActionStatus);

            Client.Instance.ExecuteAsyncWrite(command);
        }

        public bool AddTrackedEmail(int pSenderMemberId, int pRecipientMemberId, int pMessageListId, int pSenderBrandId, int pPrivilegeTypeId, bool startWithReadStatus)
        {
            try
            {
                Command command = new Command("mnAlertSaveMM", "up_TrackedEmail_Save", 0);

                command.AddParameter("@MessageListID", SqlDbType.Int, ParameterDirection.Input, pMessageListId);
                command.AddParameter("@PrivilegeTypeID", SqlDbType.Int, ParameterDirection.Input, pPrivilegeTypeId);
                command.AddParameter("@SenderMemberID", SqlDbType.Int, ParameterDirection.Input, pSenderMemberId);
                command.AddParameter("@RecipientMemberID", SqlDbType.Int, ParameterDirection.Input, pRecipientMemberId);
                command.AddParameter("@SenderBrandID", SqlDbType.Int, ParameterDirection.Input, pSenderBrandId);
                command.AddParameter("@ReadStatus", SqlDbType.TinyInt, ParameterDirection.Input, startWithReadStatus ? (int)ReadStatus.Read : (int)ReadStatus.NotRead);
                command.AddParameter("@ActionStatus", SqlDbType.TinyInt, ParameterDirection.Input, (int)ActionStatus.None);

                Client.Instance.ExecuteAsyncWrite(command);
                return true;
            }
            catch (Exception ex)
            {
                throw new BLException("Error in AddTrackedEmail() " + ex.ToString(), ex);
            }
        }

        public bool MarkAsRead(int pMessageListId, bool isRead)
        {
            try
            {
                Command command = new Command("mnAlertSaveMM", "up_TrackedEmail_SaveStatus", 0);

                command.AddParameter("@MessageListID", SqlDbType.Int, ParameterDirection.Input, pMessageListId);
                command.AddParameter("@ReadStatus", SqlDbType.TinyInt, ParameterDirection.Input, isRead ? (int)ReadStatus.Read : (int)ReadStatus.NotRead);

                Client.Instance.ExecuteAsyncWrite(command);
                return true;
            }
            catch (Exception ex)
            {
                throw new BLException("Error in MarkAsRead() " + ex.ToString(), ex);
            }
        }

        public bool RemoveTrackedEmail(int pMessageListId)
        {
            try
            {
                UpdateActionStatus(pMessageListId, ActionStatus.Removed);
                return true;
            }
            catch (Exception ex)
            {
                throw new BLException("Error in RemoveTrackedEmail() " + ex.ToString(), ex);
            }
        }

        public bool MarkAsRefunded(int pMessageListId)
        {
            try
            {
                UpdateActionStatus(pMessageListId, ActionStatus.Refunded);
                return true;
            }
            catch (Exception ex)
            {
                throw new BLException("Error in MarkAsRefunded() " + ex.ToString(), ex);
            }
        }

        public bool MarkAsNudged(int pMessageListId)
        {
            try
            {
                UpdateActionStatus(pMessageListId, ActionStatus.Nudged);
                return true;
            }
            catch (Exception ex)
            {
                throw new BLException("Error in MarkAsNudged() " + ex.ToString(), ex);
            }
        }

        public bool MarkAsError(int pMessageListId)
        {
            try
            {
                UpdateActionStatus(pMessageListId, ActionStatus.Error);
                return true;
            }
            catch (Exception ex)
            {
                throw new BLException("Error in MarkAsError() " + ex.ToString(), ex);
            }
        }

        public bool UpdateRetry(int pMessageListId, DateTime nextRetryDate, int retryCount)
        {
            try
            {
                Command command = new Command("mnAlertSaveMM", "up_TrackedEmail_UpdateRetry", 0);

                command.AddParameter("@MessageListID", SqlDbType.Int, ParameterDirection.Input, pMessageListId);
                
                if(nextRetryDate != DateTime.MinValue)
                {
                    command.AddParameter("@NextRetryDate", SqlDbType.DateTime, ParameterDirection.Input, nextRetryDate);
                }
                
                command.AddParameter("@RetryCount", SqlDbType.TinyInt, ParameterDirection.Input, retryCount);

                Client.Instance.ExecuteAsyncWrite(command);
                return true;
            }
            catch (Exception ex)
            {
                throw new BLException("Error in UpdateRetry() " + ex.ToString(), ex); ;
            }
        }
    }
}
