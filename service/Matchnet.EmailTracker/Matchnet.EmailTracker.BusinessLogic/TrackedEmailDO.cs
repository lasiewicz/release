﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.EmailTracker.ValueObjects.Types;

namespace Matchnet.EmailTracker.BusinessLogic
{
    public class TrackedEmailDO
    {
        public int MessageListId { get; set; }
        public int PrivilegeTypeId { get; set; }
        public int SenderMemberId { get; set; }
        public int RecipientMemberId { get; set; }
        public int SenderBrandId { get; set; }
        public ReadStatus ReadStatus { get; set; }
        public ActionStatus ActionStatus { get; set; }
        public int RetryCount { get; set; }
        public DateTime NextRetryDate { get; set; }
    }
}
