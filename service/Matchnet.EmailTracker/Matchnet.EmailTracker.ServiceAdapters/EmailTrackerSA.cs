﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.EmailTracker.ValueObjects;
using Matchnet.EmailTracker.ValueObjects.ServiceDefinitions;

namespace Matchnet.EmailTracker.ServiceAdapters
{
    public class EmailTrackerSA : SABase, IEmailTrackerService
    {
        public static readonly EmailTrackerSA Instance = new EmailTrackerSA();

        private EmailTrackerSA()
        {}

        private ISettingsSA _settingSA = null;

        public ISettingsSA SettingSA
        {
            get { return _settingSA ?? (_settingSA = RuntimeSettings.Instance); }
        }

        public bool AddTrackedEmail(int pSenderMemberId, int pRecipientMemberId, int pMessageListId, int pSenderBrandId, int pPrivilegeTypeId)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).AddTrackedEmail(pSenderMemberId, pRecipientMemberId, pMessageListId, pSenderBrandId, pPrivilegeTypeId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                
                throw(new SAException(string.Format("AddTrackedEmail() error (uri: {0}).", uri), ex));
            }
        }

        public bool AddTrackedEmail(int pSenderMemberId, int pRecipientMemberId, int pMessageListId, int pSenderBrandId, int pPrivilegeTypeId, bool startWithReadStatus)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).AddTrackedEmail(pSenderMemberId, pRecipientMemberId, pMessageListId, pSenderBrandId, pPrivilegeTypeId, startWithReadStatus);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {

                throw (new SAException(string.Format("AddTrackedEmail() error (uri: {0}).", uri), ex));
            }
        }

        public bool RemoveTrackedEmail(int pMessageListId)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).RemoveTrackedEmail(pMessageListId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {

                throw (new SAException(string.Format("RemoveTrackedEmail() error (uri: {0}).", uri), ex));
            }
        }

        public bool MarkAsRead(int pMessageListId)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).MarkAsRead(pMessageListId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {

                throw (new SAException(string.Format("MarkAsRead() error (uri: {0}).", uri), ex));
            }
        }

        public bool MarkAsRead(int pMessageListId, bool isRead)
        {
            string uri = string.Empty;
            try
            {
                uri = GetServiceManagerUri(ServiceConstants.SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    return getService(uri).MarkAsRead(pMessageListId, isRead);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {

                throw (new SAException(string.Format("MarkAsRead() error (uri: {0}).", uri), ex));
            }
        }

        private IEmailTrackerService getService(string uri)
        {
            try
            {
                return (IEmailTrackerService)Activator.GetObject(typeof(IEmailTrackerService), uri);
            }
            catch (Exception ex)
            {
                throw new SAException("Cannot activate remote service manager at " + uri, ex);
            }
        }

        private string GetServiceManagerUri(string serviceManagerName)
        {
            try
            {
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(serviceManagerName);
                string overrideHostName = SettingSA.GetSettingFromSingleton("EMAILTRACKER_SA_HOST_OVERRIDE");

                if (overrideHostName.Length > 0)
                {
                    UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
                    uri = "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
                }

                return uri;
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot get configuration settings for remote service manager.", ex);
            }
        }

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(SettingSA.GetSettingFromSingleton("EMAILTRACKERSVC_SA_CONNECTION_LIMIT"));
        }
    }
}
