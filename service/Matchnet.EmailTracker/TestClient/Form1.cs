﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Matchnet.EmailTracker.ServiceAdapters;

namespace TestClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private int GetRandomMessageListID()
        {
            Random rand = new Random();

            return rand.Next(1, 100000);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int randomMessageListID = GetRandomMessageListID();
                textBox2.Text = string.Format("random MessageListID is {0}", randomMessageListID);
                textBox3.Text = randomMessageListID.ToString();

                EmailTrackerSA.Instance.AddTrackedEmail(1234, 4564, randomMessageListID, 1001, 7);
                textBox1.Text = "DONE";
            }
            catch (Exception ex)
            {
                textBox1.Text = ex.Message;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                EmailTrackerSA.Instance.RemoveTrackedEmail(Convert.ToInt32(textBox3.Text));
                textBox1.Text = "DONE";
            }
            catch (Exception ex)
            {
                textBox1.Text = ex.Message;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                EmailTrackerSA.Instance.MarkAsRead(Convert.ToInt32(textBox3.Text));
                textBox1.Text = "DONE";
            }
            catch (Exception ex)
            {
                textBox1.Text = ex.Message;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
