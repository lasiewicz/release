﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Matchnet.RemotingServices;
using Matchnet.EmailTracker.ServiceManagers;

namespace Matchnet.EmailTracker.Service
{
    public partial class EmailTracker : RemotingServiceBase
    {
        private EmailTrackerSM _emailTrackerSM;

        public EmailTracker()
        {
            Trace.WriteLine("EmailTracker, EmailTracker() contructor");
            InitializeComponent();
        }

        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
                                {
                                    new EmailTracker()
                                };
            ServiceBase.Run(ServicesToRun);
        }

        protected override void RegisterServiceManagers()
        {
            System.Diagnostics.Trace.WriteLine("EmailTracker, Start RegisterServiceManagers");

            _emailTrackerSM = new EmailTrackerSM();
            base.RegisterServiceManager(_emailTrackerSM);

            base.RegisterServiceManagers();
        }
    }
}
