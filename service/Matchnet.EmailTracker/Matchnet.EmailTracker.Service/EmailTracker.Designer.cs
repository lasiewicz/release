﻿using Matchnet.EmailTracker.ValueObjects;
using System.Diagnostics;

namespace Matchnet.EmailTracker.Service
{
    partial class EmailTracker
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            if(_emailTrackerSM != null)
            {
                _emailTrackerSM.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Trace.WriteLine("EmailTracker, InitializeComponent");
            
            components = new System.ComponentModel.Container();
            this.ServiceName = ServiceConstants.SERVICE_NAME;
            base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
            
            Trace.WriteLine("EmailTracker, Matchnet.InitialConfiguration.InitializationSettings.MachineName: " + Matchnet.InitialConfiguration.InitializationSettings.MachineName);
        }

        #endregion
    }
}
