﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;

namespace Matchnet.EmailTracker.Test.Mocks
{
    class MockEmailMessageSA : IEmailMessageSA
    {
        public bool FailMarkOneFreeReply { get; set; }

        #region Implementation of IEmailMessageSA

        public int GetMemberIDToBlock(EmailMessage pEmailMessage, int pMemberID)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Implementation of IEmailMessageSA

        public bool SaveMessage(MessageSave messageSave)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Implementation of IEmailMessageSA

        public bool SaveMessage(MessageSave messageSave, out int memberMailID)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Implementation of IEmailMessageSA

        public MessageSendResult SendMessage(MessageSave messageSave, bool saveInSent, int replyMessageListID, bool respectQuota)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Implementation of IEmailMessageSA

        public EmailMessage RetrieveMessage(int memberID, int groupID, int messageListID)
        {
            throw new NotImplementedException();
        }

        public EmailMessage RetrieveMessage(int memberID, int groupID, int messageListID, bool ignoreSACache)
        {
            throw new NotImplementedException();
        }

        public EmailMessageCollection RetrieveMessages(int memberID, int communityID, int memberFolderID)
        {
            throw new NotImplementedException();
        }

        public EmailMessageCollection RetrieveMessages(int memberID, int communityID, int memberFolderID, int siteID)
        {
            throw new NotImplementedException();
        }

        public bool MarkRead(int memberID, int groupID, bool read, ArrayList messageListIDs)
        {
            throw new NotImplementedException();
        }

        public void MarkOffFraudHoldSentByMember(int memberID, int groupID, int siteID)
        {
            throw new NotImplementedException();
        }

        public string MarkOffFraudHoldSentByMember(int memberID, int groupID, bool returnTrace)
        {
            throw new NotImplementedException();
        }

        public void MarkDeletedSentByMember(int memberID, int groupID, bool isDelete)
        {
            throw new NotImplementedException();
        }

        public bool MarkOneFreeReply(int pMemberID, int pGroupId, bool pOneFreeReply, ArrayList pMessageListIds)
        {
            if (FailMarkOneFreeReply)
            {
                throw new Exception("EmailService is not responding");
            }

            return true;
        }

        public bool MarkOneFreeReply(int pMemberID, int pGroupId, bool pOneFreeReply, ArrayList pMessageListIds, bool ignoreSACache)
        {
            throw new NotImplementedException();
        }

        public bool MoveToFolder(int memberID, int groupID, ArrayList messageListIDs, int memberFolderID)
        {
            throw new NotImplementedException();
        }

        public bool DeleteMessage(int memberID, int groupID, ArrayList messageListIDs)
        {
            throw new NotImplementedException();
        }

        public bool EmptyFolder(int memberID, int groupID, int memberFolderID)
        {
            throw new NotImplementedException();
        }

        public bool MarkDraftSaved(int memberID, int groupID, bool saved, int messageListID)
        {
            throw new NotImplementedException();
        }

        public Message RetrieveMessageByMessageID(int memberID, int messageID)
        {
            throw new NotImplementedException();
        }

        public bool CanMoveEmailsToFolder(int memberID, int groupID, ArrayList messageListIDs, int memberFolderID)
        {
            throw new NotImplementedException();
        }

        public ArrayList RetrieveMessagesByStatus(int memberID, Brand brand, int memberFolderID, MessageStatus messagestatus, bool messagestatusequal, string @orderby, int numbertoreturn)
        {
            throw new NotImplementedException();
        }

        public int GetMessageCountByStatus(int memberID, Brand brand, int memberFolderID, MessageStatus messageStatus, bool inverseCount, bool includeFlirtAndEcards)
        {
            throw new NotImplementedException();
        }

        public bool MarkOffFraudHold(int memberID, int groupID, ArrayList messageListIDs, MessageListCollection messageListColFromDB, StringBuilder sb)
        {
            throw new NotImplementedException();
        }

        public bool MarkDeleted(int memberID, int groupID, bool isDeleted, ArrayList messageListIDs, MessageListCollection messageListColFromDB)
        {
            throw new NotImplementedException();
        }

        public ICollection RetrieveMessagesFromLogicalFolder(int memberID, int communityID, int memberFolderID, int startRow, int pageSize, string orderBy, Brand brand)
        {
            throw new NotImplementedException();
        }

        public ICollection RetrieveMessages(int memberID, int communityID, int memberFolderID, int startRow, int pageSize, string orderBy, Brand brand)
        {
            throw new NotImplementedException();
        }

        public EmailMessage RetrieveMessage(int groupID, int memberID, int messageListID, string orderBy, Direction messageDirection, bool updateReadFlag, Brand brand)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IEmailMessageSA Members


        public MessageAttachmentSaveResult SaveMessageAttachment(int fromMemberID, int toMemberID, byte[] fileBytes, string extension, AttachmentType attachmentType, int messageID, int communityID, int siteID, Spark.Common.MemberLevelTracking.Application app)
        {
            throw new NotImplementedException();
        }

        public string CallerAppName { get; set; }

        #endregion
    }
}
