﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceAdapters.Interfaces;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Admin;

namespace Matchnet.EmailTracker.Test.Mocks
{
    class MockMemberSA : IGetMember
    {
        #region Implementation of IGetMember

        public Member.ServiceAdapters.Member GetMember(int memberID, MemberLoadFlags memberLoadFlags)
        {
            // we just have to populate enough so that GetLastLogonDate() works which is to popluate the
            // brandlastlogin attribute
            var cachedMember = new CachedMember(123);
            // BrandLastLogonDate for jdate.com
            cachedMember.SetAttributeDate(604, DateTime.Now);
            var member = new Matchnet.Member.ServiceAdapters.Member(cachedMember);

            return member;
        }

        public ImpersonateToken ValidateImpersonateToken(string impersonateTokenGUID)
        {
            throw new NotImplementedException();
        }

        public int GetMemberIDByEmail(string emailAddress)
        {
            throw new NotImplementedException();
        }

        public int ValidateResetPasswordToken(string tokenGuidString)
        {
            throw new NotImplementedException();
        }


        public System.Collections.ArrayList GetMembers(System.Collections.ArrayList memberIDs, MemberLoadFlags memberLoadFlags)
        {
            throw new NotImplementedException();
        }

        public Member.ValueObjects.Interfaces.IMemberDTO GetMemberDTO(int memberID, Member.ValueObjects.MemberDTO.MemberType type)
        {
            // we just have to populate enough so that GetLastLogonDate() works which is to popluate the
            // brandlastlogin attribute
            var cachedMember = new CachedMember(123);
            // BrandLastLogonDate for jdate.com
            cachedMember.SetAttributeDate(604, DateTime.Now);
            var member = new Matchnet.Member.ServiceAdapters.Member(cachedMember);

            return member;
        }


        public System.Collections.ArrayList GetMemberDTOs(System.Collections.ArrayList memberIDs, Member.ValueObjects.MemberDTO.MemberType type)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
