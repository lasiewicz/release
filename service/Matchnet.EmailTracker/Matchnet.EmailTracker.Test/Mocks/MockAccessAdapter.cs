﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Spark.Common.AccessService;
using Spark.Common.Adapter;

namespace Matchnet.EmailTracker.Test.Mocks
{
    class MockAccessAdapter : IAccessServiceAdapter
    {
        #region Implementation of IAccessServiceAdapter

        public AccessServiceClient GetProxyInstance()
        {
            throw new NotImplementedException();
        }

        public AccessServiceClient GetProxyInstance(string URL, string endpointConfigurationName)
        {
            throw new NotImplementedException();
        }

        public AccessServiceClient GetProxyInstanceForBedrock()
        {
            throw new NotImplementedException();
        }

        public AccessResponse AdjustCountPrivilege(int customerID, int[] privilegeTypeID, int count, int AdminID, string AdminUserName, int AccessReasonID, int callingSystemID, int callingSystemTypeID)
        {
            var resp = new AccessResponse() {response = "success"};
            return resp;
        }

        public void CloseProxyInstance()
        {
            // since we didn't create any real connection, do nothing
        }

        #endregion
    }
}
