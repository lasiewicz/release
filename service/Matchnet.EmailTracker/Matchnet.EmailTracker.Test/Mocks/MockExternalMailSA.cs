﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects.Miniprofile;
using Matchnet.ExternalMail.ValueObjects.ServiceDefinitions;
using Matchnet.List.ValueObjects;

namespace Matchnet.EmailTracker.Test.Mocks
{
    class MockExternalMailSA : IExternalMailSA
    {
        #region Implementation of IExternalMailSA

        public bool IsSMSAlertNeeded(int memberID, ExternalMailSA.SMSAlertMask maskToCheck, Brand brand)
        {
            throw new NotImplementedException();
        }

        public bool IsRegulatedByLastLogonDate(int memberID, Brand brand)
        {
            throw new NotImplementedException();
        }

        public bool SendAllAccessNudgeEmail(int pSenderMemberId, int pSenderBrandId, int pMessageListId, int pRecipientMemberId, int pRecipientBrandId, string pEmailSubject, string pEmailContent, DateTime pInitialMailTime)
        {
            throw new NotImplementedException();
        }

        public bool SendAllAccessInitialEmail(int pSenderMemberId, int pSenderBrandId, int pMessageListId, int pRecipientMemberId, int pRecipientBrandId, string pEmailSubject, string pEmailContent)
        {
            throw new NotImplementedException();
        }

        public bool SendAllAccessReplyToInitialEmail(int pSenderMemberId, int pSenderBrandId, int pMessageListId, int pRecipientMemberId, int pRecipientBrandId, string pEmailSubject, string pEmailContent)
        {
            throw new NotImplementedException();
        }

        public bool SendAllAccessNoThanksEmail(int pSenderMemberId, int pSenderBrandId, int pRecipientMemberId, int pRecipientBrandId)
        {
            throw new NotImplementedException();
        }

        public bool SendAllAccessReturnEmail(int pSenderMemberId, int pSenderBrandId, int pRecipientMemberId, int pRecipientBrandId)
        {
            return true;
        }

        public bool SendBOGONotificationEmail(int senderMemberId, int senderBrandID, string recipientEmailAddress, string giftPromo, string giftCode)
        {
            throw new NotImplementedException();
        }

        public void SendProfileChangedConfirmationEmail(int memberId, int brandID, string emailAddress, string firstName, string userName, List<string> fields)
        {
            throw new NotImplementedException();
        }

        public bool SendRenewalFailureEmail(int siteID, string userName, string emailAddress)
        {
            throw new NotImplementedException();
        }

        public bool SendReportAbuseEmail(int brandID, string ReportContent)
        {
            throw new NotImplementedException();
        }

        public bool SendRegistrationVerification(int memberID, int brandID, int siteID)
        {
            throw new NotImplementedException();
        }

        public bool SendEmailVerification(int memberID, int brandID, bool isFromAdmin)
        {
            throw new NotImplementedException();
        }

        public bool SendEmailVerification(int memberID, int brandID)
        {
            throw new NotImplementedException();
        }

        public bool SendPassword(int brandID, string emailAddress)
        {
            throw new NotImplementedException();
        }

        public bool SendCheckSubscriptionConfirmation(int memberID, int brandID, int memberPaymentID, string planDescription)
        {
            throw new NotImplementedException();
        }

        public void SendOptOutNotification(int brandID, string email, string fromHost)
        {
            throw new NotImplementedException();
        }

        public void SendContactUs(int brandID, string userName, string fromEmailAddress, string sMemberID, string browser,
            string remoteIP, string reason, string userComment, string appDeviceInfo = null)
        {
            throw new NotImplementedException();
        }

        public void SendContactUs(int brandID, string userName, string fromEmailAddress, string sMemberID, string browser, string remoteIP, string reason, string userComment)
        {
            throw new NotImplementedException();
        }

        public void SendInternalCorpMail(int brandID, string fromAddress, string toAddress, string body, string toName, string subject)
        {
            throw new NotImplementedException();
        }

        public bool SendMemberToFriend(string friendName, string friendEmail, string userEmail, int sentMemberID, string subject, string message, int brandID)
        {
            throw new NotImplementedException();
        }

        public bool SendColorCodeToFriend(string friendName, string friendEmail, string senderEmail, string senderName, int senderMemberID, string subject, string message, int brandID)
        {
            throw new NotImplementedException();
        }

        public bool SendColorCodeQuizInvite(string recipientEmail, string recipientUserName, string senderEmail, int senderMemberID, string senderUserName, int brandID)
        {
            throw new NotImplementedException();
        }

        public bool SendColorCodeQuizConfirmation(int memberID, string emailAddress, int brandID, string color, string memberUserName)
        {
            throw new NotImplementedException();
        }

        public bool SendCollegeLuvPromo(int memberID, int brandID)
        {
            throw new NotImplementedException();
        }

        public bool SendMutualYesNotification(int memberID, int targetMemberID, int brandID)
        {
            throw new NotImplementedException();
        }

        public bool SendPhotoApprovedNotification(int memberID, int photoID, int brandID, string approvalStatus)
        {
            throw new NotImplementedException();
        }

        public bool SendPhotoApprovedNotification(int memberID, int photoID, int brandID, string approvalStatus, bool sendCaptionRejectedNotification)
        {
            throw new NotImplementedException();
        }

        public bool SendPhotoCaptionRejectNotification(int memberID, int photoID, int brandID)
        {
            throw new NotImplementedException();
        }

        public bool SendHotListedNotification(int memberID, int hotListedByMemberID, int brandID)
        {
            throw new NotImplementedException();
        }

        public bool SendSubscriptionConfirmation(int memberID, int siteID, int brandID, string userName, string emailAddress, string firstName, string lastName, string address1, string city, string state, string postalCode, DateTime dateOfPurchase, int planType, int currencyType, decimal initialCost, int initialDuration, DurationType initialDurationType, decimal renewCost, int renewDuration, DurationType renewDurationType, string last4CC, int creditCardTypeID, string confirmationNumber)
        {
            throw new NotImplementedException();
        }

        public bool SendSubscriptionConfirmation(int memberID, int siteID, string firstName, string lastName, string address1, string city, string state, string postalCode, DateTime dateOfPurchase, int planType, int currencyType, decimal initialCost, int initialDuration, DurationType initialDurationType, decimal renewCost, int renewDuration, DurationType renewDurationType, string last4CC, int creditCardTypeID, string confirmationNumber)
        {
            throw new NotImplementedException();
        }

        public bool SendOneOffPurchaseConfirmation(int memberID, int siteID, string firstName, string lastName, DateTime dateOfPurchase, int currencyType, decimal initialCost, int initialDuration, DurationType initialDurationType, string last4CC, int creditCardTypeID, string confirmationNumber)
        {
            throw new NotImplementedException();
        }

        public bool SendPaymentProfileConfirmation(int memberID, int siteID, int brandID, string emailAddress, string firstName, string lastName, DateTime dateOfPurchase, string last4CC, string confirmationNumber)
        {
            throw new NotImplementedException();
        }

        public bool SendMessageBoardInstantNotification(int brandID, int siteID, int memberID, string boardName, string topicSubject, string replyURL, int newReplies, string unsubscribeURL)
        {
            throw new NotImplementedException();
        }

        public bool SendMessageBoardDailyUpdates(int brandID, int siteID, int memberID, string boardName, string topicSubject, int newReplies, int boardID, int threadID, int lastMessageID, string unsubscribeURL)
        {
            throw new NotImplementedException();
        }

        public bool SendEcardToMember(int brandID, int siteID, int senderMemberID, string cardUrl, string cardThumbnail, int recipientMemberID, DateTime sentDate)
        {
            throw new NotImplementedException();
        }

        public bool SendEcardToFriend(int brandID, int siteID, int senderMemberID, string cardUrl, string cardThumbnail, string recipientEmailAddress, DateTime sentDate)
        {
            throw new NotImplementedException();
        }

        public bool SendPhotoRejectedNotification(int memberID, int photoID, int brandID, string comment)
        {
            throw new NotImplementedException();
        }

        public bool SendNewEmailSMSAlert(int receiverSiteID, int receiverMemberID, int senderMemberID, int senderBrandID, int receiverBrandID, string targetPhoneNumber)
        {
            throw new NotImplementedException();
        }

        public bool SendHotListedSMSAlert(int receiverSiteID, int receiverMemberID, int senderMemberID, int senderBrandID, int receiverBrandID, string targetPhoneNumber)
        {
            throw new NotImplementedException();
        }

        public bool SendNewFlirtSMSAlert(int receiverSiteID, int receiverMemberID, int senderMemberID, int senderBrandID, int receiverBrandID, string targetPhoneNumber)
        {
            throw new NotImplementedException();
        }

        public bool SendClickedSMSAlert(int receiverSiteID, int receiverMemberID, int senderMemberID, int senderBrandID, int receiverBrandID, string targetPhoneNumber)
        {
            throw new NotImplementedException();
        }

        public bool SendNewECardSMSAlert(int receiverSiteID, int receiverMemberID, int senderMemberID, int senderBrandID, int receiverBrandID, string targetPhoneNumber)
        {
            throw new NotImplementedException();
        }

        public bool SendValidationSMSAlert(int siteID, int receivingMemberID, int sendingMemberID, int brandID, string targetPhoneNumber)
        {
            throw new NotImplementedException();
        }

        public bool SendMatchMeterInvitationMail(int brandID, int siteID, int senderMemberID, string senderUsername, string emailAddress, string recipientEmailAddress)
        {
            throw new NotImplementedException();
        }

        public bool SendMatchMeterCongratsMail(int brandID, int siteID, int senderMemberID, string senderUsername, string emailAddr)
        {
            throw new NotImplementedException();
        }

        public bool SendAuthCodeSMSAlert(int brandID, int siteID, int receivingMemberID, string targetPhoneNumber, string authCode)
        {
            throw new NotImplementedException();
        }

        public bool SendRegistrationSuccessSMSAlert(int brandID, int siteID, int receivingMemberID, string targetPhoneNumber)
        {
            throw new NotImplementedException();
        }

        public bool SendMemberQuestion(string friendName, string friendEmail, string senderEmail, string senderName, int senderMemberID, string subject, string message, int brandID, int questionId, string questionText)
        {
            throw new NotImplementedException();
        }

        public bool SendFriendReferralEmail(string senderName, string message, string friendEmail, string prm, string lgid, string subject, int senderMemberID, int brandID)
        {
            throw new NotImplementedException();
        }

        public bool UpdatePhonesList(int memberID, int brandID, string phoneNumber, SMSAlertsDefinitions.Action action)
        {
            throw new NotImplementedException();
        }

        public bool IsPhoneAvailable(int siteID, string phoneNumber)
        {
            throw new NotImplementedException();
        }

        public bool SendJDateMobileRegistrationConfirmation(string userName, string mobileNumber, int memberID, string emailAddress, int brandID)
        {
            throw new NotImplementedException();
        }

        public bool SendScheduledEmail(int groupid, int memberid, int emailscheduleid, string schedulename, string scheduleguid, string email, Hashtable details)
        {
            throw new NotImplementedException();
        }

        public void SendBetaFeedback(int brandID, string senderName, string senderEmailAddress, string senderMemberID, string browser, string remoteIP, string subject, string userComment, int feedbackTypeID)
        {
            throw new NotImplementedException();
        }

        public bool IsMessmoMessageRequired(int memberID, int communityID)
        {
            throw new NotImplementedException();
        }

        public void SendMemberGeneratedQuestion(int brandID, string userName, string fromFirstName, string fromLastName, int memberID, string questionText, bool isAnonymous)
        {
            throw new NotImplementedException();
        }

        public void SendInitialBatchOfMessmoMessages(int brandID, DateTime timestamp, int memberID, string messmoSubscriberID, string status, DateTime setDate, string mobileNumber)
        {
            throw new NotImplementedException();
        }

        public void SendMessmoMessage(int recipientBrandID, DateTime emailDate, string emailBody, string receivingMessmoSubID, string taggle)
        {
            throw new NotImplementedException();
        }

        public bool SendMessmoSubscriptionRequest(int brandID, int memberID, DateTime timestamp, string mobileNumber, string encryptedPassword)
        {
            throw new NotImplementedException();
        }

        public void SendMessmoUserTransStatusRequest(int brandID, DateTime timestamp, string messmoSubscriberID, string status, DateTime setDate)
        {
            throw new NotImplementedException();
        }

        public void SendMessmoUserTransStatusRequestBySiteID(int siteID, DateTime timestamp, string messmoSubscriberID, string status, DateTime setDate)
        {
            throw new NotImplementedException();
        }

        public void SendMessmoFullProfileRequest(int brandID, int memberID, DateTime timestamp)
        {
            throw new NotImplementedException();
        }

        public void SendMessmoSMSRequest(int brandID, DateTime timestamp, string mobileNumber, string message)
        {
            throw new NotImplementedException();
        }

        public bool SendMessmoMultiPartMessage(int recipientBrandID, int senderBrandID, int senderMemberID, DateTime messageDate, string receivingMessmoSubID, string messageBody, string smartMessageTag, string taggle, bool replace, bool notify, HTTPMessaging.Constants.MessmoMessageType messmoMsgType)
        {
            throw new NotImplementedException();
        }

        public bool SendMessmoMessageFromList(int communityID, int recipientBrandID, int senderBrandID, int recipientMemberID, int senderMemberID, HotListCategory hotlistCat)
        {
            throw new NotImplementedException();
        }

        public bool SendMessmoMessageFromEmail(MessageSendResult sendResult)
        {
            throw new NotImplementedException();
        }

        public bool NotifyOfMessmoMessageArrival(int memberID, int brandID, HTTPMessaging.Constants.MessmoMessageType messmoMsgType)
        {
            throw new NotImplementedException();
        }

        public bool UpdatePhonesList(int memberID, int brandID, string phoneNumber, Action action)
        {
            throw new NotImplementedException();
        }

        public bool SendNewMemberMail(int brandID, string emailAddress, MiniprofileInfo miniprofileInfo, MiniprofileInfoCollection miniprofileInfoCollection)
        {
            throw new NotImplementedException();
        }

        public bool SendMatchMail(int brandID, string emailAddress, MiniprofileInfo miniprofileInfo, MiniprofileInfoCollection miniprofileInfoCollection)
        {
            throw new NotImplementedException();
        }

        public bool SendViralEmail(int brandID, string emailAddress, MiniprofileInfo miniprofileInfo, MiniprofileInfoCollection miniprofileInfoCollection)
        {
            throw new NotImplementedException();
        }

        public bool SendInternalMailNotification(EmailMessage emailMessage, int brandID, int unreadEmailCount)
        {
            throw new NotImplementedException();
        }

        public bool IsSMSAlertNeeded(int memberID, Brand brand, SMSAlertsDefinitions.MessageType messageType, bool isFriend)
        {
            throw new NotImplementedException();
        }

        public bool ResetPassword(int brandId, int memberId, string resetPasswordUrl)
        {
            throw new NotImplementedException();
        }

        public bool SendViewedYouEmail(int memberID, int viewedByMemberID, int numberOfMembersWhoViewedSinceLastEmail, int brandID)
        {
            throw new NotImplementedException();
        }

        public void SendBBEPasscodeEmail(int memberId, int brandID, string emailAddress, string passcode)
        {
            throw new NotImplementedException();
        }

        #endregion


        public void SendContactUs(int brandID, string userName, string fromEmailAddress, string sMemberID, string browser, string remoteIP, string reason, string userComment, string appDeviceInfo = null, string contactUsEmail = null)
        {
            throw new NotImplementedException();
        }
    }
}
