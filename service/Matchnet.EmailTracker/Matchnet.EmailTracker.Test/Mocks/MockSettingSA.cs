﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ServiceAdapters;

namespace Matchnet.EmailTracker.Test.Mocks
{
    class MockSettingSA : ISettingsSA
    {
        public List<Matchnet.Configuration.ValueObjects.MembaseConfig> GetMembaseConfigsFromSingleton()
        {
            throw new NotImplementedException();
        }

        public Matchnet.Configuration.ValueObjects.ServiceInstanceConfig GetServiceInstanceConfigFromSingleton(string serviceConstant, string machineName)
        {
            throw new NotImplementedException();
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID, int brandID)
        {
            throw new NotImplementedException();
        }

        public string GetSettingFromSingleton(string constant, int communityID, int siteID)
        {
            throw new NotImplementedException();
        }

        public string GetSettingFromSingleton(string constant, int communityID)
        {
            throw new NotImplementedException();
        }

        public string GetSettingFromSingleton(string constant)
        {
            throw new NotImplementedException();
        }

        public bool IsServerEnabledFromSingleton()
        {
            throw new NotImplementedException();
        }

        public bool SettingExistsFromSingleton(string constant, int communityID, int siteID)
        {
            throw new NotImplementedException();
        }

        public List<Configuration.ValueObjects.ApiAppSetting> GetAPIAppSettingsFromSingleton()
        {
            throw new NotImplementedException();
        }

        public void AddNewOrUpdateSetting(string settingConstant, string globalDefaultValue, string settingDescription,
            int settingCategoryId = 2, bool isRequiredForCommunity = false, bool isRequiredForSite = false)
        {
            throw new NotImplementedException();
        }

        public void UpdateGlobalDefault(string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void SetValueForSite(int siteId, string settingConstant, string globalDefaultValue)
        {
            throw new NotImplementedException();
        }

        public void ResetToGlobalDefault(string settingConstant)
        {
            throw new NotImplementedException();
        }

        public Configuration.ValueObjects.MembaseConfig GetMembaseConfigByBucketFromSingleton(string bucketName)
        {
            throw new NotImplementedException();
        }

    }
}
