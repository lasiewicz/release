﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Matchnet.Data.Hydra;
using Matchnet.EmailTracker.BusinessLogic;
using NUnit.Framework;

namespace Matchnet.EmailTracker.Test
{
    public class EmailTrackerBLTest
    {
        private HydraWriter _hydraWriter = null;

        [TestFixtureSetUp]
        public void StartUp()
        {
            
        }

        [TestFixtureTearDown]
        public void TestFixtureTeardown()
        {

        }

        [SetUp]
        public void SetUp()
        {
            _hydraWriter = new HydraWriter(new string[] { "mnAlertSaveMM" });
            _hydraWriter.Start();
        }

        [TearDown]
        public void Teardown()
        {
            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }
        }

        [Test]
        public void RefundProcessorTestFailEmailService()
        {
            RefundProcessor.Instance.AccessAdapter = new Mocks.MockAccessAdapter();
            
            var mockEmailMessageSA = new Mocks.MockEmailMessageSA();
            mockEmailMessageSA.FailMarkOneFreeReply = true;
            RefundProcessor.Instance.EmailMessageSA = mockEmailMessageSA;

            RefundProcessor.Instance.ExternalMailSA = new Mocks.MockExternalMailSA();
            RefundProcessor.Instance.MemberSA = new Mocks.MockMemberSA();

            RefundProcessor.Instance.Start();
            // let's sleep thread so the worker thread can do its thing enough for testing
            Thread.Sleep(60000);
            RefundProcessor.Instance.Stop();
        }

        [Test]
        public void RefundProcessorTestWithAllMocks()
        {
            RefundProcessor.Instance.AccessAdapter = new Mocks.MockAccessAdapter();

            var mockEmailMessageSA = new Mocks.MockEmailMessageSA();
            mockEmailMessageSA.FailMarkOneFreeReply = false;
            RefundProcessor.Instance.EmailMessageSA = mockEmailMessageSA;

            RefundProcessor.Instance.ExternalMailSA = new Mocks.MockExternalMailSA();
            RefundProcessor.Instance.MemberSA = new Mocks.MockMemberSA();

            RefundProcessor.Instance.Start();
            // let's sleep thread so the worker thread can do its thing enough for testing
            Thread.Sleep(60000);
            RefundProcessor.Instance.Stop();
        }

        [Test]
        public void RefundProcessorTestWithAccessMockOnly()
        {
            RefundProcessor.Instance.AccessAdapter = new Mocks.MockAccessAdapter();

            RefundProcessor.Instance.Start();
            // let's sleep thread so the worker thread can do its thing enough for testing
            Thread.Sleep(60000);
            RefundProcessor.Instance.Stop();
        }

        [Test]
        public void NudgeProcessorTest()
        {
            NudgeProcessor.Instance.Start();

            Thread.Sleep(60000);

            NudgeProcessor.Instance.Stop();
        }
    }
}
