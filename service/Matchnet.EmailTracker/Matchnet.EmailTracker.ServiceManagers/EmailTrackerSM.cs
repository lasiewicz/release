﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Matchnet.EmailTracker.BusinessLogic;
using Matchnet.EmailTracker.ValueObjects.ServiceDefinitions;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.EmailTracker.ValueObjects;

namespace Matchnet.EmailTracker.ServiceManagers
{
    public class EmailTrackerSM : MarshalByRefObject, IEmailTrackerService, IServiceManager, IBackgroundProcessor
    {
        private HydraWriter _hydraWriter;

        public EmailTrackerSM()
        {
            _hydraWriter = new HydraWriter(new string[] {"mnAlertSaveMM"} );

            Trace.WriteLine("EmailTrackerSM constructor completed.");
        }

        /// <summary>
        /// keep the object alive indefinitely
        /// </summary>
        /// <returns></returns>
        public override object InitializeLifetimeService()
        {
            return null;
        }

        #region IEmailTrackerService Members

        public bool AddTrackedEmail(int pSenderMemberId, int pRecipientMemberId, int pMessageListId, int pSenderBrandId, int pPrivilegeTypeId)
        {
            try
            {
                return EmailTrackerBL.Instance.AddTrackedEmail(pSenderMemberId, pRecipientMemberId, pMessageListId,
                                                               pSenderBrandId, pPrivilegeTypeId, false);
            }
            catch (Exception ex)
            {
                
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "AddTrackedEmail() error.", ex);
            }
        }

        public bool AddTrackedEmail(int pSenderMemberId, int pRecipientMemberId, int pMessageListId, int pSenderBrandId, int pPrivilegeTypeId, bool startWithReadStatus)
        {
            try
            {
                return EmailTrackerBL.Instance.AddTrackedEmail(pSenderMemberId, pRecipientMemberId, pMessageListId,
                                                               pSenderBrandId, pPrivilegeTypeId, startWithReadStatus);
            }
            catch (Exception ex)
            {

                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "AddTrackedEmail() error.", ex);
            }
        }

        public bool MarkAsRead(int pMessageListId)
        {
            try
            {
                return EmailTrackerBL.Instance.MarkAsRead(pMessageListId, true);
            }
            catch (Exception ex)
            {

                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "MarkAsRead() error.", ex);
            }
        }

        public bool MarkAsRead(int pMessageListId, bool isRead)
        {
            try
            {
                return EmailTrackerBL.Instance.MarkAsRead(pMessageListId, isRead);
            }
            catch (Exception ex)
            {

                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "MarkAsRead() error.", ex);
            }
        }

        public bool RemoveTrackedEmail(int pMessageListId)
        {
            try
            {
                return EmailTrackerBL.Instance.RemoveTrackedEmail(pMessageListId);
            }
            catch (Exception ex)
            {

                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "RemoveTrackedEmail() error.", ex);
            }
        }

        #endregion

        #region IServiceManager Members

        public void PrePopulateCache()
        {
            // no implementation
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            // no implementation
        }

        #endregion

        #region IBackgroundProcessor Members

        public void Start()
        {
            Trace.WriteLine("EmailTrackerSM start()");
            _hydraWriter.Start();
            NudgeProcessor.Instance.Start();
            RefundProcessor.Instance.Start();
        }

        public void Stop()
        {
            Trace.WriteLine("EmailTrackerSM stop()");

            if(_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }

            NudgeProcessor.Instance.Stop();
            RefundProcessor.Instance.Stop();
        }

        #endregion
    }
}
