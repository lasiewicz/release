using Matchnet.Content.ValueObjects.Admin;

using System;
using Matchnet.Content.ValueObjects.Registration;

namespace Matchnet.Content.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IAdminService.
	/// </summary>
	public interface IAdminService
	{
        AdminActionReasonID GetLastAdminSuspendReasonForMember(int memberID);
        /// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		AdminActionTypeCollection GetAdminActionTypes();

		AdminActivityContainerCollection GetAdminActivity(int memberID, int communityID);

		AdminActionReasonCollection GetAdminActionReasons();
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <returns></returns>
		AdminActionLogCollection GetAdminActionLog(int memberID, int communityID);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="groupID"></param>
		/// <returns></returns>
		AdminActionCollection GetAdminActions(int memberID, int groupID);
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		AdminActionReport GetAdminActionReport(DateTime startDate, DateTime endDate, Int32 adminMemberID, Int32 groupID, Int32 adminActionMask);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="startDate"></param>
		/// <param name="endDate"></param>
		/// <param name="adminMemberID"></param>
		/// <param name="groupID"></param>
		/// <returns></returns>
		ApprovalCountsCollection GetApprovalCountsReport(DateTime startDate, DateTime endDate, Int32 adminMemberID, Int32 groupID);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="adminActionMask"></param>
		/// <param name="adminMemberID"></param>
		/// <param name="languageID"></param>
		void AdminActionLogInsert(int memberID, int communityID, int adminActionMask, int adminMemberID, int languageID);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="adminActionMask"></param>
		/// <param name="adminMemberID"></param>
		/// <param name="adminMemberIDType"></param>
		void AdminActionLogInsert(int memberID, int communityID, int adminActionMask, int adminMemberID, AdminMemberIDType adminMemberIDType);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="adminActionMask"></param>
		/// <param name="adminMemberID"></param>
		/// <param name="adminMemberIDType"></param>
		/// <param name="adminActionReasonID"></param>
		void AdminActionLogInsert(int memberID, int communityID, int adminActionMask, int adminMemberID, AdminMemberIDType adminMemberIDType, AdminActionReasonID adminActionReasonID);

	    void RegistrationAdminActionLogInsert(string adminAccountName, RegAdminActionType actionType, int scenarioId);
        void RegistrationAdminScenarioHistorySave(int regScenarioId, int deviceTypeID, int siteID, double weight, string updatedBy);
        
        /// <summary>
		/// 
		/// </summary>
		/// <param name="newNote"></param>
		/// <param name="targetMemberID"></param>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="maxNoteLength"></param>
		/// <returns></returns>
		void UpdateAdminNote(string newNote, int targetMemberID, int memberID, int communityID, int maxNoteLength);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="targetMemberID"></param>
		/// <param name="communityID"></param>
		/// <param name="startRow"></param>
		/// <param name="pageSize"></param>
		/// <param name="totalRows"></param>
		/// <returns></returns>
		AdminNoteCollection LoadAdminNotes(int targetMemberID, int communityID, int startRow, int pageSize, int totalRows);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="publishObjectID"></param>
		/// <param name="publishObjectType"></param>
		/// <param name="content"></param>
		/// <returns></returns>
		Int32 SavePublish(Int32 publishObjectID, PublishObjectType publishObjectType, Object content);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="publishID"></param>
		/// <param name="publishActionPublishObjectID"></param>
		/// <param name="memberID"></param>
		void SavePublishAction(Int32 publishID, Int32 publishActionPublishObjectID, Int32 memberID);

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		PublishActionPublishObjectCollection GetPublishActionPublishObjects();

		/// <summary>
		/// 
		/// </summary>
		/// <param name="startDate"></param>
		/// <param name="endDate"></param>
		/// <returns></returns>
		PublishCollection GetPublishes(DateTime startDate, DateTime endDate);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="publishID"></param>
		/// <returns></returns>
		Publish GetPublish(Int32 publishObjectID, PublishObjectType publishObjectType);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="publishID"></param>
		/// <param name="publishActionPublishObjectID"></param>
		/// <param name="environmentType"></param>
		void GetNextPublishActionPublishObjectStep(Int32 publishObjectID, PublishObjectType publishObjectType, out Int32 publishActionPublishObjectID, out ServiceEnvironmentTypeEnum nextServiceEnvironmentTypeEnum);
	}
}
