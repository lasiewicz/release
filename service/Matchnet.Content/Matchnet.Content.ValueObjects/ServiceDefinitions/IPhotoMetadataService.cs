﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.Photos;

namespace Matchnet.Content.ValueObjects.ServiceDefinitions
{
    public interface IPhotoMetadataService
    {
        List<PhotoFileTypeRecord> GetPhotoFileTypeRecords();
    }
}
