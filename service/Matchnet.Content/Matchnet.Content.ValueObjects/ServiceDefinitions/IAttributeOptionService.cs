using System;

using Matchnet.Content.ValueObjects.AttributeOption;


namespace Matchnet.Content.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// 
	/// </summary>
	public interface IAttributeOptionService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		Attributes GetAttributes();

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeOptionID"></param>
		/// <param name="pAttributeID"></param>
		/// <param name="pDescription"></param>
		/// <param name="pAttributeValue"></param>
		/// <param name="pResourceKey"></param>
		/// <param name="pListOrder"></param>
		void SaveAttributeOption(int pAttributeOptionID, int pAttributeID, string pDescription, int pAttributeValue, string pResourceKey, int pListOrder);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeOptionGroupID"></param>
		/// <param name="pAttributeOptionID"></param>
		/// <param name="pGroupID"></param>
		/// <param name="pListOrder"></param>
		void SaveAttributeOptionGroup(int pAttributeOptionGroupID, int pAttributeOptionID, int pGroupID, int pListOrder);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeOptionID"></param>
		void DeleteAttributeOption(int pAttributeOptionID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeOptionGroupID"></param>
		/// <param name="pGroupID"></param>
		void DeleteAttributeOptionGroup(int pAttributeOptionGroupID, int pGroupID);
	}
}
