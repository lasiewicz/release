using System;

using Matchnet.Content.ValueObjects.PageConfig;

namespace Matchnet.Content.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// 
	/// </summary>
	public interface IPageConfigService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		SitePages GetSitePages();

		/// <summary>
		/// 
		/// </summary>
		/// <param name="analyticsPageID"></param>
		/// <returns></returns>
		AnalyticsPage GetAnalyticsPage(Int32 analyticsPageID);
	}
}
