﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.Registration;

namespace Matchnet.Content.ValueObjects.ServiceDefinitions
{
    public interface IRegistrationMetadata
    {
        List<Scenario> GetScenarios();
        List<Scenario> GetScenariosForAdmin();
        List<Scenario> GetAllRegScenarios();
        void SaveScenarioStatusWeight(int regScenarioId, bool activate, double weight, string updatedBy, int siteID, int deviceTypeID);
        List<RegControl> GetAllRegControls(int deviceTypeId);
        List<RegControl> GetRegControlsRequiredForRegScenarioSite(int siteId);
        int CreateRegScenario(Scenario scenario);
        void SaveScenario(Scenario scenario);
        void SaveRegControlScenarioOverride(RegControlScenarioOverride regControlegControlScenarioOverride, int regControlSiteID);
        void SaveRegStep(Step step, int scenarioId);
        Dictionary<int, List<Template>> GetTemplatesBySite();
        TemplateSaveResult CreateTemplate(Template regTemplate);
        TemplateSaveResult RemoveTemplate(int regTemplateId, int siteId);
    }
}
