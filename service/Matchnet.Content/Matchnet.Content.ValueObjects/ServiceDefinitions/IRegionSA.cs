﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.Region;

namespace Matchnet.Content.ValueObjects.ServiceDefinitions
{
    public interface IRegionSA
    {
        RegionID FindRegionIdByPostalCode(int countryRegionID, string postalCode);
        RegionID FindRegionIdByCity(int parentRegionID, string description, int languageID);
        Region.Region RetrieveRegionByID(int regionID, int languageID);
        RegionCollection RetrieveCitiesByPostalCode(string strPostalCode);
        RegionCollection RetrieveChildRegions(int parentRegionID, int languageID, string description, int translationID);
        RegionCollection RetrieveChildRegions(int parentRegionID, int languageID, int translationID, bool forceLoad);
        RegionCollection RetrieveChildRegions(int parentRegionID, int languageID);
        RegionCollection RetrieveChildRegions(int parentRegionID, int languageID, string description);
        RegionCollection RetrieveChildRegions(int parentRegionID, int languageID, int translationID);
        RegionCollection RetrieveCountries(int languageID);
        RegionCollection RetrieveBirthCountries(int languageID);
        RegionCollection RetrieveCountries();
        RegionLanguage RetrievePopulatedHierarchy(int regionID, int languageID);
        RegionLanguage RetrievePopulatedHierarchy(int regionID, int languageID, int maxDepth);
        RegionSchoolName RetrieveSchoolName(int schoolRegionID);
        SchoolRegionCollection RetrieveSchoolParents(int schoolRegionID);
        SchoolRegionCollection RetrieveSchoolListByStateRegion(int stateRegionID);
        LanguageCollection GetLanguages();
        String IsValidAreaCodes(Int32[] areaCodes, Int32 countryRegionID);
        Boolean IsValidAreaCode(Int32 areaCode, Int32 countryRegionID);
        AreaCodeDictionary RetrieveAreaCodes();
        RegionAreaCodeDictionary RetrieveAreaCodesByRegion();
    }
}
