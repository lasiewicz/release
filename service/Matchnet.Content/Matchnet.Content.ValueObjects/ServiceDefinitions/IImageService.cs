using Matchnet.Content.ValueObjects.Image;

using System;
using System.Data;

namespace Matchnet.Content.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IImageService.
	/// </summary>
	public interface IImageService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		ImageRoot GetImageUNCRoot();
	}
}
