using Matchnet.Content.ValueObjects.PagePixel;
using Matchnet.CacheSynchronization.ValueObjects;

using System;
using System.Data;

namespace Matchnet.Content.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IPagePixelService.
	/// </summary>
	public interface IPagePixelService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <param name="pageID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		PagePixelCollection RetrievePagePixels(string clientHostName, CacheReference cacheReference, int pageID, int siteID);
	}
}
