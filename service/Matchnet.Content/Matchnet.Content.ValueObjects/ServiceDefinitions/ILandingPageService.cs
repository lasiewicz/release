using System;
using System.Collections.Generic;
using Matchnet.Content.ValueObjects;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Content.ValueObjects.LandingPage;

namespace Matchnet.Content.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for ILandingPageService.
	/// </summary>
	public interface ILandingPageService
	{
        List<TemplateBasedLandingPage> GetLandingPages(Boolean forceLoadFlag);
        TemplateBasedLandingPage GetTemplateBasedLandingPage(int landingPageID);
        TemplateBasedLandingPage GetTemplateBasedLandingPageByURL(string url);
        List<LandingPageTemplate> GetLandingPageTemplates();
        bool CheckNameAvailability(string name, int existingID, CheckAvailabilityType type);
        void SaveLandingPageTemplate(LandingPageTemplate template);
        void SaveLandingPage(TemplateBasedLandingPage page);
        void SaveUnifiedLandingPageIDForLandingPage(int landingPageID, int? unifiedLandingPageID);
        void DeleteLandingPage(int landingPageID);
        void DeleteTemplate(int templateID);
        LandingPage.LandingPage GetLandingPage(int landingPageID);
        LandingPageCollection GetLandingPages(string clientHostName, CacheReference cacheReference, Boolean forceLoadFlag);
        
	}
}
