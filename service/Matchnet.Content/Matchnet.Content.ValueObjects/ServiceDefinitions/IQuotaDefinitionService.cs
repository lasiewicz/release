using System;

using Matchnet.Content.ValueObjects.Quotas;

namespace Matchnet.Content.ValueObjects.ServiceDefinitions
{
	public interface IQuotaDefinitionService
	{
		QuotaDefinitions GetQuotaDefinitions();
	}
}
