using System;

using Matchnet.Content.ValueObjects.AttributeMetadata;


namespace Matchnet.Content.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// 
	/// </summary>
	public interface IAttributeMetadataService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		Attributes GetAttributes();

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		AttributeCollections GetAttributeCollections();

        SearchStoreAttributes GetSearchStoreAttributes();
	}
}
