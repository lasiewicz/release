using Matchnet.Content.ValueObjects.Region;

using System;
using System.Data;

namespace Matchnet.Content.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IRegionService.
	/// </summary>
	public interface IRegionService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="countryRegionID"></param>
		/// <param name="postalCode"></param>
		/// <returns></returns>
		RegionID FindRegionIdByPostalCode(int countryRegionID, string postalCode);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="parentRegionID"></param>
		/// <param name="description"></param>
		/// <param name="languageID"></param>
		/// <returns></returns>
		RegionID FindRegionIdByCity(int parentRegionID, string description, int languageID);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="regionID"></param>
		/// <param name="languageID"></param>
		/// <returns></returns>
		Region.Region RetrieveRegionByID(int regionID, int languageID);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="parentRegionID"></param>
		/// <param name="languageID"></param>
		/// <param name="description"></param>
		/// <param name="translationID"></param>
		/// <returns></returns>
		RegionCollection RetrieveChildRegions(int parentRegionID, int languageID, string description, int translationID);
		
		/// <summary>
		/// Provides a collection of regions (cities)based on provided postal code. 
		/// </summary>
		/// <remarks>This method should only be used for US and Canada. There is no country check occuring during DB call. </remarks>
		/// <param name="postalCode"></param>
		/// <returns>Collection of Region objects representing each city in the provided zip code.</returns>
		///	   
		RegionCollection RetrieveCitiesByPostalCode(string strPostalCode);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="languageID"></param>
		/// <returns></returns>
		RegionCollection RetrieveCountries(int languageID);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="languageID"></param>
		/// <returns></returns>
		RegionCollection RetrieveBirthCountries(int languageID);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="regionID"></param>
		/// <param name="languageID"></param>
		/// <param name="maxDepth"></param>
		/// <returns></returns>
		RegionLanguage RetrievePopulatedHierarchy(int regionID, int languageID, int maxDepth);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="schoolRegionID"></param>
		/// <returns></returns>
		RegionSchoolName RetrieveSchoolName(int schoolRegionID);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="schoolRegionID"></param>
		/// <returns></returns>
		SchoolRegionCollection RetrieveSchoolParents(int schoolRegionID);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="stateRegionID"></param>
		/// <returns></returns>
		SchoolRegionCollection RetrieveSchoolListByStateRegion(int stateRegionID);
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		LanguageCollection GetLanguages();
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		AreaCodeDictionary RetrieveAreaCodes();
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		RegionAreaCodeDictionary RetrieveAreaCodesByRegion();
		/// <summary>
		/// Retrieves an SEORegions object that is used for SEO pages.
		/// </summary>
		/// <returns></returns>
		SEORegions RetrieveSEORegions();
		/// <summary>
		/// Returns DMA code corresponding to passed zip code region ID parameter.
		/// </summary>
		/// <param name="ZipRegionID"></param>
		/// <returns></returns>
		DMA GetDMAByZipRegionID(int ZipRegionID);
		/// <summary>
		/// Retrieves all DMA codes.
		/// </summary>
		/// <returns></returns>
		DMACollection GetAllDMAS();		
	}
}
