using System;

using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.Content.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// 
	/// </summary>
	public interface IBrandConfigService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		Brands GetBrands();

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		Sites GetSites();
	}
}
