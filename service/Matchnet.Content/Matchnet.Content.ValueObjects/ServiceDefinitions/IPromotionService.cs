using Matchnet.Content.ValueObjects.Promotion;

using System;
using System.Data;

namespace Matchnet.Content.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IPromotionService.
	/// </summary>
	public interface IPromotionService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		RegistrationPromotionCollection RetrieveRegistrationPromotions(int communityID, int brandID);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="groupID"></param>
		/// <returns></returns>
		BannerCollection RetrieveBanners(int groupID);
        /// <summary>
        /// Retrieves a list of PromotionID and URL Referral mapping
        /// </summary>
        PRMURLMapperList GetPRMURLMapperList();
	}
}
