using System;

using Matchnet;
using Matchnet.Content.ValueObjects.Events;


namespace Matchnet.Content.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IEventService.
	/// </summary>
	public interface IEventService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="orderBy"></param>
		/// <param name="sorting"></param>
		/// <param name="startRow"></param>
		/// <param name="pageSize"></param>
		/// <param name="publishFlag"></param>
		/// <param name="eventType"></param>
		/// <returns></returns>
		EventCollection GetEvents(Int32 communityID, string orderBy, string sorting, Int32 startRow, Int32 pageSize, string publishFlag, Event.EVENT_TYPE eventType);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="eventType"></param>
		/// <returns></returns>
		EventCollection GetEvents(Int32 communityID, Event.EVENT_TYPE eventType);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <returns></returns>
		EventCollection GetAlbumEvents(Int32 communityID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="eventID"></param>
		/// <returns></returns>
		Event GetEvent(Int32 eventID);

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		HolidayCollection GetHolidayCollection();

		/// <summary>
		/// 
		/// </summary>
		/// <param name="year"></param>
		/// <returns></returns>
		HolidayCollection GetHolidayCollection(Int32 year);
	}
}
