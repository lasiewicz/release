using Matchnet.Content.ValueObjects.Article;
using Matchnet.CacheSynchronization.ValueObjects;

using System;
using System.Data;

namespace Matchnet.Content.ValueObjects.ServiceDefinitions
{
	/// <summary>
	/// Summary description for IArticleService.
	/// </summary>
	public interface IArticleService
	{
		/// <summary>
		/// DEPRECATED.  KEPT FOR BACKWARDS COMBATIBILITY.
		/// </summary>
		/// <param name="articleID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		SiteArticle RetrieveSiteArticle(int articleID, int siteID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <param name="articleID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		SiteArticle RetrieveSiteArticle(string clientHostName, CacheReference cacheReference, int articleID, int siteID);
		
		/// <summary>
		/// DEPRECATED.  KEPT FOR BACKWARDS COMBATIBILITY.
		/// </summary>
		/// <param name="categoryID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		SiteArticleCollection RetrieveCategorySiteArticles(int categoryID, int siteID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <param name="categoryID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		SiteArticleCollection RetrieveCategorySiteArticles(string clientHostName, CacheReference cacheReference, int categoryID, int siteID);

		/// <summary>
		/// DEPRECATED.  KEPT FOR BACKWARDS COMBATIBILITY.
		/// </summary>
		/// <param name="categoryID"></param>
		/// <param name="siteID"></param>
		/// <param name="forceLoadFlag"></param>
		/// <returns></returns>
		SiteArticleCollection RetrieveCategorySiteArticlesAndArticles(int categoryID, int siteID, bool forceLoadFlag);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <param name="categoryID"></param>
		/// <param name="siteID"></param>
		/// <param name="forceLoadFlag"></param>
		/// <returns></returns>
		SiteArticleCollection RetrieveCategorySiteArticlesAndArticles(int categoryID, int siteID);

		/// <summary>
		/// DEPRECATED.  KEPT FOR BACKWARDS COMBATIBILITY.
		/// </summary>
		/// <param name="siteID"></param>
		/// <param name="categoryID"></param>
		/// <returns></returns>
		SiteCategoryCollection RetrieveSiteCategoryChildren(int siteID, int categoryID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <param name="siteID"></param>
		/// <param name="categoryID"></param>
		/// <returns></returns>
		SiteCategoryCollection RetrieveSiteCategoryChildren(string clientHostName, CacheReference cacheReference, int siteID, int categoryID);

		/// <summary>
		/// DEPRECATED.  KEPT FOR BACKWARDS COMBATIBILITY.
		/// </summary>
		/// <param name="siteID"></param>
		/// <param name="forceLoadFlag">If true, we'll load the Categories from the dbase.</param>
		/// <returns></returns>
		SiteCategoryCollection RetrieveSiteCategories(int siteID, bool forceLoadFlag);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		SiteCategoryCollection RetrieveSiteCategories(string clientHostName, CacheReference cacheReference, int siteID, bool forceLoadFlag);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="categoryID"></param>
		/// <returns></returns>
		Category RetrieveCategory(int categoryID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="articleID"></param>
		/// <returns></returns>
		Article.Article RetrieveArticle(int articleID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="article"></param>
		int SaveArticle(Article.Article article, Int32 memberID, Boolean isPublish);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteArticle"></param>
		int SaveSiteArticle(SiteArticle siteArticle, Int32 memberID, Boolean isPublish);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="category"></param>
		int SaveCategory(Category category, Int32 memberID, Boolean isPublish);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteCategory"></param>
		int SaveSiteCategory(SiteCategory siteCategory, Int32 memberID, Boolean isPublish);

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		WSOListCollection RetrieveWSOListCollection();

		/// <summary>
		/// 
		/// </summary>
		/// <param name="wsoList"></param>
		void SaveWSOList(WSOList wsoList);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="name"></param>
		/// <param name="note"></param>
		void CreateWSOList(string name, string note);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="wsoListID"></param>
		/// <returns></returns>
		WSOItemCollection RetrieveWSOItemCollection(int wsoListID);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="wsoItem"></param>
		void SaveWSOItem(WSOItem wsoItem);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="wsoItem"></param>
		void DeleteWSOItem(WSOItem wsoItem);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="wsoListID"></param>
		/// <param name="siteID"></param>
		/// <param name="itemID"></param>
		/// <param name="itemTypeID"></param>
		void CreateWSOItem(int wsoListID, int siteID, int itemID, int itemTypeID);
	}
}
