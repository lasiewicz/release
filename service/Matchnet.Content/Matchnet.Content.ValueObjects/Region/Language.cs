using System;

namespace Matchnet.Content.ValueObjects.Region
{
	/// <summary>
	/// Summary description for Language.
	/// </summary>
	[Serializable]
	public class Language : ICacheable
	{

		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		private int _LanguageID;
		private string _Description;
		private string _CharSet;
		#endregion

		/// <summary>
		/// 
		/// </summary>
		public int LanguageID
		{
			get{return _LanguageID;}
			set{_LanguageID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Description
		{
			get{return _Description;}
			set{_Description = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string CharSet
		{
			get{return _CharSet;}
			set{_CharSet = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Language()
		{
		}

		#region ICacheable Members
		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}
		#endregion
	}
}
