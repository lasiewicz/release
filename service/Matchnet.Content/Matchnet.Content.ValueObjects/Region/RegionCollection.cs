using System;

namespace Matchnet.Content.ValueObjects.Region
{
	/// <summary>
	/// Summary description for RegionCollection.
	/// </summary>
	/// 
	[Serializable()]
	public class RegionCollection : System.Collections.CollectionBase, ICacheable
	{

		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		#endregion

		/// <summary>
		/// 
		/// </summary>
		public RegionCollection()
		{

		}

		/// <summary>
		/// Integer indexer
		/// </summary>
		public Region this[int index]
		{
			get { return ((Region)base.InnerList[index]); }
		}

		/// <summary>
		/// Overloaded Add method
		/// </summary>
		/// <param name="region"></param>
		/// <returns></returns>
		public int Add(Region region)
		{
			return base.InnerList.Add(region);
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}

		#endregion
	}
}
