using System;

namespace Matchnet.Content.ValueObjects.Region
{
	/// <summary>
	/// Summary description for SEOStateCollection.
	/// </summary>
	[Serializable()]
	public class SEOStateCollection :  System.Collections.CollectionBase
	{

		/// <summary>
		/// Parameterless constructor
		/// </summary>
		public SEOStateCollection()
		{

		}

		/// <summary>
		/// Integer indexer
		/// </summary>
		public SEOState this[int index]
		{
			get { return ((SEOState)base.InnerList[index]); }
		}

		/// <summary>
		/// Adds a new item (SEOState) to the collection.
		/// </summary>
		/// <param name="seoState"></param>
		/// <returns></returns>
		public int Add(SEOState seoState)
		{
			return base.InnerList.Add(seoState);
		}		
	}
}
