using System;

namespace Matchnet.Content.ValueObjects.Region
{
	
	/// <summary>
	/// Class implemenation of SEOState object. SEOState is an extension of US state. It composed of a US state (type of region) 
	/// the default city of a state and a collection of popular cities of the state.
	/// </summary>
	
	[Serializable()]
	public class SEOState	: IValueObject
	{

		private Region _State;

		private int _DefaultCityRegionID;// This represents the popular city of each state, which is used as the default city of the state.

		private SEORegionCollection _CitiesInState;
		/// <summary>
		/// Constructor for SEORegion
		/// </summary>
		/// <param name="state"></param>
		/// <param name="defaultCityRegionID"></param>
		/// <param name="cities"></param>
		public SEOState(Region state,int defaultCityRegionID,SEORegionCollection cities)
		{
			_State=state;
			_DefaultCityRegionID=defaultCityRegionID;
			_CitiesInState=cities;
		}

		#region Properties
		/// <summary>
		/// Returns the state object of SEOState
		/// </summary>
		public Region State
		{
			get{ return _State;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int DefaultCity
		{
			get{ return _DefaultCityRegionID;}
		}
		/// <summary>
		/// 
		/// </summary>
		public SEORegionCollection Cities
		{
			get{ return _CitiesInState;}
		}
		#endregion
	}// END CLASS DEFINITION SEOState

	
}
