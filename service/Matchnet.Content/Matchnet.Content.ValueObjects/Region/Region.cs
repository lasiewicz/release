using System;

namespace Matchnet.Content.ValueObjects.Region
{
	/// <summary>
	/// Summary description for Region.
	/// </summary>
	[Serializable]
	public class Region : ICacheable
	{
		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		private int _RegionID;
		private int _Depth;
		private decimal _Latitude;
		private decimal _Longitude;
		private string _Description;
		private string _Abbreviation;
		private int _Mask;
		private int _ChildrenDepth ;
		private Region _ParentRegion = null;
		#endregion

		/// <summary>
		/// Fully parameterized constructor
		/// </summary>
		/// <param name="regionID"></param>
		/// <param name="depth"></param>
		/// <param name="latitude"></param>
		/// <param name="longitude"></param>
		/// <param name="description"></param>
		/// <param name="abbreviation"></param>
		/// <param name="mask"></param>
		/// <param name="childrenDepth"></param>
		/// <param name="parentRegion"></param>
		public Region(int regionID, int depth, decimal latitude, decimal longitude, string description,
			string abbreviation, int mask, int childrenDepth, Region parentRegion)
		{
			_RegionID = regionID;
			_Depth = depth;
			_Latitude = latitude;
			_Longitude = longitude;
			_Description = description;
			_Abbreviation = abbreviation;
			_Mask = mask;
			_ChildrenDepth = childrenDepth;
			_ParentRegion = parentRegion;
		}

		#region Public properties

		/// <summary>
		/// RegionID
		/// </summary>
		public int RegionID
		{
			get
			{
				return _RegionID;
			}
			set
			{
				_RegionID = value;
			}
		}

		/// <summary>
		/// ParentRegionID
		/// </summary>
		public int ParentRegionID
		{
			get
			{
				if (_ParentRegion != null)
				{
					return _ParentRegion.RegionID;
				}
				else
				{
					return Matchnet.Constants.NULL_INT;
				}
			}
		}

		/// <summary>
		/// Depth
		/// </summary>
		public int Depth
		{
			get
			{
				return _Depth;
			}
			set
			{
				_Depth = value;
			}
		}

		/// <summary>
		/// Latitude
		/// </summary>
		public decimal Latitude
		{
			get
			{
				return _Latitude;
			}
			set
			{
				_Latitude = value;
			}
		}

		/// <summary>
		/// Longitude
		/// </summary>
		public decimal Longitude
		{
			get
			{
				return _Longitude;
			}
			set
			{
				_Longitude = value;
			}
		}

		/// <summary>
		/// Description
		/// </summary>
		public string Description
		{
			get
			{
				return _Description;
			}
			set
			{
				_Description = value;
			}
		}

		/// <summary>
		/// Abbreviation
		/// </summary>
		public string Abbreviation
		{
			get
			{
				return _Abbreviation;
			}
			set
			{
				_Abbreviation = value;
			}
		}

		/// <summary>
		/// Mask
		/// </summary>
		public int Mask
		{
			get
			{
				return _Mask;
			}
			set
			{
				_Mask = value;
			}
		}

		/// <summary>
		/// ChildrenDepth
		/// </summary>
		public int ChildrenDepth
		{
			get
			{
				return _ChildrenDepth;
			}
			set
			{
				_ChildrenDepth = value;
			}
		}

		/// <summary>
		/// ParentRegion object
		/// </summary>
		public Region ParentRegion
		{
			get
			{
				return _ParentRegion;
			}
			set
			{
				_ParentRegion = value;
			}
		}

		#endregion

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}

		#endregion
	}
}
