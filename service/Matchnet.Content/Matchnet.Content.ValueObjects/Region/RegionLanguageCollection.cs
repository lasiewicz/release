using System;

namespace Matchnet.Content.ValueObjects.Region
{
	/// <summary>
	/// Summary description for RegionLanguageCollection.
	/// </summary>
	[Serializable()]
	public class RegionLanguageCollection : System.Collections.CollectionBase, ICacheable
	{
		/// <summary>
		/// Parameterless constructor
		/// </summary>
		public RegionLanguageCollection()
		{

		}

		/// <summary>
		/// Integer indexer
		/// </summary>
		public RegionLanguage this[int index]
		{
			get { return ((RegionLanguage)base.InnerList[index]); }
		}

		/// <summary>
		/// Overloaded add method
		/// </summary>
		/// <param name="regionLanguage"></param>
		/// <returns></returns>
		public int Add(RegionLanguage regionLanguage)
		{
			return base.InnerList.Add(regionLanguage);
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				// TODO:  Add RegionLanguageCollection.CacheTTLSeconds getter implementation
				return 0;
			}
			set
			{
				// TODO:  Add RegionLanguageCollection.CacheTTLSeconds setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				// TODO:  Add RegionLanguageCollection.CacheMode getter implementation
				return new Matchnet.CacheItemMode ();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				// TODO:  Add RegionLanguageCollection.CachePriority getter implementation
				return new Matchnet.CacheItemPriorityLevel ();
			}
			set
			{
				// TODO:  Add RegionLanguageCollection.CachePriority setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			// TODO:  Add RegionLanguageCollection.GetCacheKey implementation
			return null;
		}

		#endregion
	}
}
