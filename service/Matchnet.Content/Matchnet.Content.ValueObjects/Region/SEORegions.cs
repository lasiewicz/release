using System;

namespace Matchnet.Content.ValueObjects.Region
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable()]
	public class SEORegions	: IValueObject,ICacheable
	{

		private  SEORegionCollection _SEOMajorCities;

		private SEOStateCollection _SEOStates;
		
		private int _CacheTTLSeconds;

		private CacheItemPriorityLevel _CachePriority;
		
		private string _CacheKey  ;		

		/// <summary>
		/// SEORegions constructor.
		/// </summary>
		/// <param name="USMajorCities"></param>
		/// <param name="seoStates"></param>
		public SEORegions(SEORegionCollection USMajorCities, SEOStateCollection seoStates)
		{
			_CacheTTLSeconds = 60*60;
			_CachePriority = CacheItemPriorityLevel.Normal;						

			_SEOMajorCities=USMajorCities;
			_SEOStates=seoStates;
			
		}

		#region Properties
		/// <summary>
		/// 
		/// </summary>
		public SEORegionCollection MajorUSCities
		{
			get{return _SEOMajorCities;}
		}
		/// <summary>
		/// 
		/// </summary>
		public SEOStateCollection States
		{
			get{return _SEOStates;}
		}
		
		#endregion

		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return Matchnet.CacheItemMode.Sliding ;
			}
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		public string GetCacheKey()
		{
			return _CacheKey;
		}
		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}

		#endregion

	}// END CLASS DEFINITION SEORegions

}
