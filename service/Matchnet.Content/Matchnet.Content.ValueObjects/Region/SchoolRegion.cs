using System;

namespace Matchnet.Content.ValueObjects.Region
{
	/// <summary>
	/// Summary description for SchoolRegion.
	/// </summary>
	[Serializable()]
	public class SchoolRegion
	{
		private int _schoolRegionID;
		private string _name;
		private int _countryRegionID;
		private int _stateProvinceRegionID;
		private int _cityRegionID;
		private int _zipCodeRegionID;

		/// <summary>
		/// Fully parameterized constructor
		/// </summary>
		/// <param name="schoolRegionID"></param>
		/// <param name="name"></param>
		/// <param name="countryRegionID"></param>
		/// <param name="stateProvinceRegionID"></param>
		/// <param name="cityRegionID"></param>
		/// <param name="zipCodeRegionID"></param>
		public SchoolRegion(int schoolRegionID, string name, int countryRegionID, int stateProvinceRegionID, int cityRegionID, int zipCodeRegionID)
		{
			_schoolRegionID = schoolRegionID;
			_name = name;
			_countryRegionID = countryRegionID;
			_stateProvinceRegionID = stateProvinceRegionID;
			_cityRegionID = cityRegionID;
			_zipCodeRegionID = zipCodeRegionID;
		}

		/// <summary>
		/// SchoolRegionID
		/// </summary>
		public int SchoolRegionID
		{
			get { return(_schoolRegionID); }
		}

		/// <summary>
		/// Name
		/// </summary>
		public string Name
		{
			get { return(_name); }
		}

		/// <summary>
		/// CountryRegionID
		/// </summary>
		public int CountryRegionID
		{
			get { return(_countryRegionID); }
		}

		/// <summary>
		/// StateProvinceRegionID
		/// </summary>
		public int StateProvinceRegionID
		{
			get { return(_stateProvinceRegionID); }
		}

		/// <summary>
		/// CityRegionID
		/// </summary>
		public int CityRegionID
		{
			get { return(_cityRegionID); }
		}

		/// <summary>
		/// ZipCodeRegionID
		/// </summary>
		public int ZipCodeRegionID
		{
			get { return(_zipCodeRegionID); }
		}
	}
}
