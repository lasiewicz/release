using System;
using System.Collections;

namespace Matchnet.Content.ValueObjects.Region
{
	/// <summary>
	/// Summary description for RegionAreaCodeDictionary.
	/// </summary>
	[Serializable]
	public class RegionAreaCodeDictionary : ICacheable
	{
		public const string CACHE_KEY = "RegionAreaCodeDictionary";

		private Int32 _cacheTTLSeconds = 3600;
		private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Normal;
		private CacheItemMode _cacheMode = CacheItemMode.Sliding;

		private Hashtable _regionIDs;

		public RegionAreaCodeDictionary(Int32 count)
		{
			_regionIDs = new Hashtable(count);
		}

		public void Add(Int32 regionID, Int32 areaCode)
		{
			if (!_regionIDs.Contains(regionID))
			{
				_regionIDs.Add(regionID, new ArrayList(1));
			}

			(_regionIDs[regionID] as ArrayList).Add(areaCode);
		}

		public bool Contains(Int32 regionID)
		{
			return _regionIDs.ContainsKey(regionID);
		}

		public ArrayList this[Int32 regionID]
		{
			get
			{
				return _regionIDs[regionID] as ArrayList;
			}
		}

		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return _cacheMode;
			}
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}

		public string GetCacheKey()
		{
			return CACHE_KEY;
		}

		#endregion
	}
}
