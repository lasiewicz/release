using System;

namespace Matchnet.Content.ValueObjects.Region
{
	

	/// <summary>
	/// Class implementation for SEORegion object. 
	/// </summary>
	/// 
	[Serializable()]
	public class SEORegion	: IValueObject
	{

		private int _RegionID=int.MinValue;

		private string _Description=string.Empty;

		private string _AbbriviatedDescription=string.Empty;

		private string _ParentAbbreviation=string.Empty;

		private string _ParentDescription=string.Empty;
		/// <summary>
		/// Class constructor for SEORegion object
		/// </summary>
		/// <param name="regionID"></param>
		/// <param name="description"></param>
		/// <param name="abbriviation"></param>
		/// <param name="parentAbbrv"></param>
		/// <param name="ParentDescription"></param>
		public SEORegion(int regionID, string description, string abbriviation, string parentAbbrv,string ParentDescription)
		{
			_RegionID=regionID;
			_Description=description;
			_AbbriviatedDescription=abbriviation;
			_ParentAbbreviation=parentAbbrv;
			_ParentDescription=ParentDescription;
		}
		#region Properties
		/// <summary>
		/// Gets RegionID
		/// </summary>
		public int RegionID
		{
			get{return _RegionID;}			
		}
		/// <summary>
		/// Gets description of the region
		/// </summary>
		public string Description
		{
			get{return _Description; }
		}
		/// <summary>
		/// Gets description of the region
		/// </summary>
		public string DescriptionAbbrv
		{
			get{return _AbbriviatedDescription; }
		}
		/// <summary>
		/// Gets abbriviated description of region's parent
		/// </summary>
		public string ParentAbbrv
		{
			get{return _ParentAbbreviation;}
		}
		/// <summary>
		/// Gets description of region's parent
		/// </summary>
		public string ParentDescription
		{
			get{return _ParentDescription;}
		}

		#endregion


	}// END CLASS DEFINITION SEORegion


}
