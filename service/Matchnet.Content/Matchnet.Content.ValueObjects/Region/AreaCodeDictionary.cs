using System;

namespace Matchnet.Content.ValueObjects.Region
{
	/// <summary>
	/// Summary description for AreaCodeDictionary.
	/// </summary>
	[Serializable]
	public class AreaCodeDictionary : System.Collections.DictionaryBase, ICacheable
	{

		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		#endregion

		/// <summary>
		/// 
		/// </summary>
		public AreaCodeDictionary()
		{
		}

		public Int32 this[Int32 areaCode]
		{
			get
			{
				Object countryRegionID = base.InnerHashtable[areaCode];
				if (countryRegionID != null)
					return (Int32)countryRegionID;

				return Constants.NULL_INT;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public void Add(Int32 areaCode, Int32 countryRegionID)
		{
			base.InnerHashtable.Add(areaCode, countryRegionID);
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}

		#endregion
	}
}
