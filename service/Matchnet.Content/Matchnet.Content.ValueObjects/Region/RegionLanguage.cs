using System;

namespace Matchnet.Content.ValueObjects.Region
{
	/// <summary>
	/// Summary description for RegionLanguage.
	/// </summary>
	[Serializable]
	public class RegionLanguage : ICacheable
	{
		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		private string _PostalCode;
		private int _StateRegionID;
		private string _StateAbbreviation;
		private string _StateDescription;
		private int _CityRegionID;
		private string _CityName;
		private int _PostalCodeRegionID;
		private int _CountryRegionID;
		private string _CountryAbbreviation;
		private string _CountryName;
		private int _HighestDepthRegionTypeID;
		private decimal _Longitude;
		private decimal _Latitude;
		private int _Depth1RegionID;
		#endregion

		/// <summary>
		/// Parameterless constructor
		/// </summary>
		public RegionLanguage()
		{

		}

		#region Public bulk update methods

		/// <summary>
		/// Update postal code info
		/// </summary>
		/// <param name="postalCodeRegionID"></param>
		/// <param name="postalCode"></param>
		/// <param name="longitude"></param>
		/// <param name="latitude"></param>
		public void UpdatePostalCodeInfo(int postalCodeRegionID, string postalCode, decimal longitude, decimal latitude)
		{
			_PostalCodeRegionID = postalCodeRegionID;
			_PostalCode = postalCode;
			_Longitude = longitude;
			_Latitude = latitude;
		}

		/// <summary>
		/// Update city info
		/// </summary>
		/// <param name="cityRegionID"></param>
		/// <param name="cityName"></param>
		/// <param name="latitude"></param>
		/// <param name="longitude"></param>
		public void UpdateCityInfo(int cityRegionID, string cityName, decimal longitude, decimal latitude)
		{
			_CityRegionID = cityRegionID;
			_CityName = cityName;
			_Latitude = latitude;
			_Longitude = longitude;
		}

		/// <summary>
		/// Update state info
		/// </summary>
		/// <param name="stateRegionID"></param>
		/// <param name="stateAbbreviation"></param>
		/// <param name="stateDescription"></param>
		public void UpdateStateInfo(int stateRegionID, string stateAbbreviation, string stateDescription)
		{
			_StateRegionID = stateRegionID;
			_StateAbbreviation = stateAbbreviation;
			_StateDescription = stateDescription;
		}

		/// <summary>
		/// Update country info
		/// </summary>
		/// <param name="countryRegionID"></param>
		/// <param name="countryAbbreviation"></param>
		/// <param name="countryName"></param>
		/// <param name="depth1RegionID"></param>
		public void UpdateCountryInfo(int countryRegionID, string countryAbbreviation, string countryName, int depth1RegionID)
		{
			_CountryRegionID = countryRegionID;
			_CountryAbbreviation = countryAbbreviation;
			_CountryName = countryName;
			_Depth1RegionID = depth1RegionID;
		}

		#endregion

		#region public properties

		/// <summary>
		/// PostalCode
		/// </summary>
		public string PostalCode
		{
			get
			{
				return _PostalCode;
			}
		}

		/// <summary>
		/// StateRegionID
		/// </summary>
		public int StateRegionID
		{
			get
			{
				return _StateRegionID;
			}
		}

		/// <summary>
		/// StateAbbreviation
		/// </summary>
		public string StateAbbreviation
		{
			get
			{
				return _StateAbbreviation;
			}
		}

		/// <summary>
		/// StateDescription
		/// </summary>
		public string StateDescription
		{
			get { return _StateDescription; }
		}

		/// <summary>
		/// PostalCodeRegionID
		/// </summary>
		public int PostalCodeRegionID
		{
			get
			{
				return _PostalCodeRegionID;
			}
		}

		/// <summary>
		/// CityRegionID
		/// </summary>
		public int CityRegionID
		{
			get
			{
				return _CityRegionID;
			}
		}

		/// <summary>
		/// CityName
		/// </summary>
		public string CityName
		{
			get
			{
				return _CityName;
			}
		}

		/// <summary>
		/// CountryRegionID
		/// </summary>
		public int CountryRegionID
		{
			get
			{
				return _CountryRegionID;
			}
		}

		/// <summary>
		/// CountryAbbreviation
		/// </summary>
		public string CountryAbbreviation
		{
			get
			{
				return _CountryAbbreviation;
			}
		}

		/// <summary>
		/// CountryName
		/// </summary>
		public string CountryName
		{
			get
			{
				return _CountryName;
			}
		}

		/// <summary>
		/// HighestDepthRegionTypeID
		/// </summary>
		public int HighestDepthRegionTypeID
		{
			get
			{
				return _HighestDepthRegionTypeID;
			}
			set
			{
				_HighestDepthRegionTypeID = value;
			}
		}

		/// <summary>
		/// Longitude
		/// </summary>
		public decimal Longitude
		{
			get
			{
				return _Longitude;
			}
		}

		/// <summary>
		/// Latitude
		/// </summary>
		public decimal Latitude
		{
			get
			{
				return _Latitude;
			}
		}

		/// <summary>
		/// Depth1RegionID
		/// </summary>
		public int Depth1RegionID
		{
			get
			{
				return _Depth1RegionID;
			}
		}

		#endregion

		#region ICacheable Members
		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}
		#endregion
	}
}
