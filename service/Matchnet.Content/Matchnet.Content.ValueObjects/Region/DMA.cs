using System;

namespace Matchnet.Content.ValueObjects.Region
{
	/// <summary>
	/// Summary description for DMA.
	/// </summary>
	[Serializable()]
	public class DMA : ICacheable
	{
		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		private int _dma= 0;
		private string _dmaDesc;
		#endregion

		/// <summary>
		/// Copy Constructor
		/// </summary>
		/// <param name="dma"></param>
		public DMA (int dma)
		{
			_dma = dma;
		}
		
		#region Properties
		/// <summary>
		/// 
		/// </summary>
		public int DMAID
		{
			get {return _dma;}
			set {_dma = value;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string DMADescription
		{
			get { return _dmaDesc; }
			set { _dmaDesc = value; }
		}
		#endregion
		
		public override bool Equals(object obj)
		{
			if(obj is DMA)
			{
				DMA otherDma = (DMA)obj;
				return this.DMAID.Equals(otherDma.DMAID);
			}
			else
			{
				throw new ArgumentException("Object is not a DMA");
			}
		}


		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}

		#endregion

//		#region IComparable Members
//
//		public int CompareTo(object obj)
//		{
//			if(obj is DMA)
//			{
//				DMA otherDMA = (DMA)obj;
//				return this.DMAID.CompareTo(otherDMA.DMAID);
//			}
//			else
//			{
//				throw new ArgumentException("Object is not a DMA");
//			}
//		}
//
//		#endregion
	}
}
