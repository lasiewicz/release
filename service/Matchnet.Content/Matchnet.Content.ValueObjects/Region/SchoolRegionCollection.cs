using System;

namespace Matchnet.Content.ValueObjects.Region
{
	/// <summary>
	/// Summary description for SchoolRegionCollection.
	/// </summary>
	[Serializable()]
	public class SchoolRegionCollection :  System.Collections.CollectionBase, ICacheable
	{

		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		#endregion

		/// <summary>
		/// Parameterless constructor
		/// </summary>
		public SchoolRegionCollection()
		{

		}

		/// <summary>
		/// Integer indexer
		/// </summary>
		public SchoolRegion this[int index]
		{
			get { return ((SchoolRegion)base.InnerList[index]); }
		}

		/// <summary>
		/// Overloaded add method
		/// </summary>
		/// <param name="schoolRegion"></param>
		/// <returns></returns>
		public int Add(SchoolRegion schoolRegion)
		{
			return base.InnerList.Add(schoolRegion);
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}

		#endregion
	}
}
