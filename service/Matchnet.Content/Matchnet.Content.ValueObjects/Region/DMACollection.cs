using System;

namespace Matchnet.Content.ValueObjects.Region
{
	/// <summary>
	/// Summary description for DMACollection.
	/// </summary>
	[Serializable]
	public class DMACollection : System.Collections.CollectionBase, ICacheable
	{
		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		#endregion

		public DMACollection()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/// <summary>
		/// 
		/// </summary>
		public DMA this[int index]
		{
			get { return ((DMA)base.InnerList[index]); }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pLanguage"></param>
		/// <returns></returns>
		public int Add(DMA pDMA)
		{
			return base.InnerList.Add(pDMA);
		}

		public DMA FindById(int dmaId)
		{
			int idx = base.InnerList.IndexOf(new DMA(dmaId));
			if(idx >= 0)
				return ((DMA)base.InnerList[idx]);
			
			return null;
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}

		#endregion
	}
}
