using System;


namespace Matchnet.Content.ValueObjects.AttributeOption
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class AttributeOption
	{
		private int _attributeOptionID;
		private int _attributeID;
		private int _value;
		private int _listOrder;
		private string _description;
		private string _resourceKey;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeOptionID"></param>
		/// <param name="pAttributeID"></param>
		/// <param name="pValue"></param>
		/// <param name="pListOrder"></param>
		/// <param name="pDescription"></param>
		/// <param name="pResourceKey"></param>
		public AttributeOption(int pAttributeOptionID, int pAttributeID, int pValue, int pListOrder, string pDescription, string pResourceKey)
		{
			_attributeOptionID = pAttributeOptionID;
			_attributeID = pAttributeID;
			_value = pValue;
			_listOrder = pListOrder;
			_description = pDescription;
			_resourceKey = pResourceKey;
		}
		
		/// <summary>
		/// 
		/// </summary>
		public int AttributeOptionID
		{
			get
			{
				return _attributeOptionID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int AttributeID
		{
			get
			{
				return _attributeID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int Value
		{
			get
			{
				return _value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int ListOrder
		{
			get
			{
				return _listOrder;
			}
			set
			{
				_listOrder = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Description
		{
			get
			{
				return _description;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string ResourceKey
		{
		
			get
			{
				return _resourceKey;
			}
		}

		/// <summary>
		/// Checks the mask to see whether the value for this attribute is set.
		/// </summary>
		/// <param name="mask">An integer mask value to check.</param>
		/// <returns></returns>
		public bool MaskContains(int mask)
		{
			// HACK: We are using int.MinValue as a "null" value in a lot of our code.  However, this value is occasionally
			// showing some odd behavior when bitwise compared against Options with value of 1.  The following hack fixes this.
			if (mask == int.MinValue || mask == Constants.NULL_INT)
			{
				if (Value == int.MinValue || Value == Constants.NULL_INT)
					return true;
				else
					return false;
			}
			else
				return ((mask & Value) == Value);
		}
	}
}
