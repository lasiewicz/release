using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Matchnet;

namespace Matchnet.Content.ValueObjects.AttributeOption
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class Attributes : ICacheable
	{
		/// <summary>
		/// 
		/// </summary>
		public const string CACHE_KEY = "Attributes";

		private int DEFAULT_GROUPID = Constants.NULL_INT;

		private Hashtable _attributes;
		private int _cacheTTLSeconds = 60 * 60;
		private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Normal;

		/// <summary>
		/// 
		/// </summary>
		public Attributes()
		{
			_attributes = new Hashtable();
		}

		/// <summary>
		/// Creates a new AttributeOptionCollection (set of options) for the attribute and group specified.
		/// </summary>
		/// <param name="pAttributeName">The name of the attribute.</param>
		/// <param name="pGroupID">The ID of the group.</param>
		/// <param name="pAttributeOptionCollection">The collection of options to add.</param>
		public void Add(string pAttributeName, int pGroupID, AttributeOptionCollection pAttributeOptionCollection)
		{
			Hashtable groupAttributeOptions = GroupAttributeOptions(pAttributeName);

			groupAttributeOptions.Add(pGroupID, pAttributeOptionCollection);
		}

		/// <summary>
		/// Creates the default AttributeOptionCollection for an attribute.  This AttributeOptionCollection is used
		/// when retrieving options for a Group that does not have its own specific set of options.
		/// </summary>
		/// <param name="pAttributeName"></param>
		/// <param name="pAttributeOptionCollection"></param>
		public void Add(string pAttributeName, AttributeOptionCollection pAttributeOptionCollection)
		{
			Add(pAttributeName, DEFAULT_GROUPID, pAttributeOptionCollection);
		}

		/// <summary>
		/// Adds an option to the default AttributeOptionCollection for a particular attribute.
		/// </summary>
		/// <param name="pAttributeName">The name of the attribute for which to add the option.</param>
		/// <param name="pAttributeOption">The option to add.</param>
		public void AddDefaultOption(string pAttributeName, AttributeOption pAttributeOption)
		{
			Hashtable groupAttributeOptions = GroupAttributeOptions(pAttributeName);

			AttributeOptionCollection attributeOptionCollection = (AttributeOptionCollection) groupAttributeOptions[DEFAULT_GROUPID];
			attributeOptionCollection.Add(pAttributeOption);
		}

		/// <summary>
		/// Gets the group-specific AttributeOptionCollection or returns the default AttributeOptionCollection
		/// if no group-specific one exists.
		/// </summary>
		/// <param name="pAttributeName"></param>
		/// <param name="pGroupID"></param>
		/// <returns></returns>
		public AttributeOptionCollection Get(string pAttributeName, int pGroupID)
		{
			Hashtable groupAttributeOptions = GroupAttributeOptions(pAttributeName);

			AttributeOptionCollection attributeOptionCollection = (AttributeOptionCollection) groupAttributeOptions[pGroupID];
			
			if (attributeOptionCollection == null)
				attributeOptionCollection = (AttributeOptionCollection) groupAttributeOptions[DEFAULT_GROUPID];

			return attributeOptionCollection;
		}

		/// <summary>
		/// Gets the default AttributeOptionCollection for the attribute named pAttributeName.
		/// </summary>
		/// <returns></returns>
		public AttributeOptionCollection Get(string pAttributeName)
		{
			return Get(pAttributeName, DEFAULT_GROUPID);
		}

		/// <summary>
		/// Returns true if the attribute named pAttributeName has a set of options specific to the group with ID pGroupID.
		/// </summary>
		/// <param name="pAttributeName">The name of the attribute.</param>
		/// <param name="pGroupID">The ID of the group.</param>
		/// <returns></returns>
		public bool ContainsGroup(string pAttributeName, int pGroupID)
		{
			if (pGroupID == Constants.NULL_INT)
				return false;

			Hashtable groupAttributeOptions = GroupAttributeOptions(pAttributeName);

			return groupAttributeOptions.ContainsKey(pGroupID);
		}

		/// <summary>
		/// Retrieves a hashtable containing all AttributeOptionCollections for the attribute named pAttributeName.
		/// The hashtable is indexed by GroupID.  The default AttributeOptionCollection for all groups
		/// is stored with a key (GroupID) of Constants.NULL_INT.
		/// </summary>
		/// <param name="pAttributeName"></param>
		/// <returns></returns>
		private Hashtable GroupAttributeOptions(string pAttributeName)
		{
			pAttributeName = pAttributeName.ToLower();
			Hashtable retval = (Hashtable) _attributes[pAttributeName];

			//If this attribute does not exist yet, create it
			if (retval == null)
			{
				retval = new Hashtable();
				_attributes[pAttributeName] = retval;
			}

			return retval;
		}

        public List<AttributeOption> GetAttributeOptions(string attributeName)
        {
            attributeName = attributeName.ToLower();
            var hashtable = (Hashtable)_attributes[attributeName];

            if (hashtable == null) return null;

            var optionList = (from object key in hashtable.Keys select (AttributeOption) hashtable[key]).ToList();

            return optionList;
        }

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ICollection GetAttributeNames()
		{
			return _attributes.Keys;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeName"></param>
		/// <returns></returns>
		public ICollection GetGroups(string pAttributeName)
		{
			return GroupAttributeOptions(pAttributeName).Keys;
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return CACHE_KEY;
		}

		#endregion
	}
}
