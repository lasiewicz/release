using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


namespace Matchnet.Content.ValueObjects.AttributeOption
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class AttributeOptionCollection : IValueObject, IEnumerable
	{
		private Hashtable _attributeOptions = new Hashtable();
		private int _maxListOrder = 0;

		/// <summary>
		/// 
		/// </summary>
		public AttributeOptionCollection()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeOption"></param>
		public void Add(AttributeOption pAttributeOption)
		{
			if (FindByID(pAttributeOption.AttributeOptionID) != null)
				return;

			if (pAttributeOption.ListOrder == Constants.NULL_INT)
			{
				_attributeOptions[_maxListOrder] = pAttributeOption;
				_maxListOrder++;
			}
			else
			{
				_attributeOptions[pAttributeOption.ListOrder] = pAttributeOption;

				if (pAttributeOption.ListOrder > _maxListOrder)
					_maxListOrder = pAttributeOption.ListOrder;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeOptionID"></param>
		/// <returns></returns>
		public AttributeOption FindByID(int pAttributeOptionID)
		{
			AttributeOption retVal = null;

			foreach (AttributeOption ao in this)
			{
				if (ao.AttributeOptionID == pAttributeOptionID)
				{
					retVal = ao;
					break;
				}
			}

			return retVal;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pListOrder"></param>
		/// <returns></returns>
		public AttributeOption FindByListOrder(int pListOrder)
		{
			return (AttributeOption) _attributeOptions[pListOrder];
		}

		/// <summary>
		/// 
		/// </summary>
		public AttributeOption this[int pValue]
		{
			get
			{
				AttributeOption retVal = null;

				foreach (AttributeOption ao in this)
				{
					if (ao.Value == pValue)
					{
						retVal = ao;
						break;
					}
				}

				return retVal;
			}
		}

        public List<AttributeOption> GetList()
        {
            return (from object key in _attributeOptions.Keys select (AttributeOption) _attributeOptions[key]).ToList();
        }

	    #region IEnumerable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public IEnumerator GetEnumerator()
		{
			ArrayList retVal = new ArrayList();

			int i;
			for (i = 0; i <= _maxListOrder; i++)
			{
				if (_attributeOptions[i] != null)
					retVal.Add(_attributeOptions[i]);
			}

			return retVal.GetEnumerator();
		}

		#endregion
	}
}
