﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Content.ValueObjects.SystemEvents
{
    public enum SystemEventCategory
    {
        MemberGenerated=0, 
        SystemGenerated=1
    }
}
