﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Content.ValueObjects.SystemEvents.Interfaces;

namespace Matchnet.Content.ValueObjects.SystemEvents
{
    public class MutualYesEvent : SystemEvent, IPushNotificationEvent
    {
        public PushNotificationTypeID NotificationTypeId { get; set; }
        public override int GetOperativeMemberId()
        {
            return TargetMemberId;
        }
    }
}
