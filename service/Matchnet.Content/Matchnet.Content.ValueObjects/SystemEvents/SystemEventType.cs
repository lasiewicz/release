﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Content.ValueObjects.SystemEvents
{
    public enum SystemEventType
    {
        MessageReceived = 0,
        ChatRequest = 1,
        BothSaidYes = 2,
        PhotoApproved = 3,
        PhotoRejected = 4
    }
}
