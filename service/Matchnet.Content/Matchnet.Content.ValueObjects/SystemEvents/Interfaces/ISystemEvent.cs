﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Content.ValueObjects.PushNotification;

namespace Matchnet.Content.ValueObjects.SystemEvents.Interfaces
{
    public interface ISystemEvent
    {
        SystemEventType EventType { get; set; }
        SystemEventCategory EventCategory { get; set; }
        int MemberId { get; set; }
        int TargetMemberId { get; set; }
        int ApplicationId { get; set; }
        int BrandId { get; set; }
        AppGroupID AppGroupId { get; set; }
        int GetOperativeMemberId();
    }
}
