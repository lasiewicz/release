﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Content.ValueObjects.PushNotification;

namespace Matchnet.Content.ValueObjects.SystemEvents.Interfaces
{
    public interface IPushNotificationEvent : ISystemEvent
    {
        PushNotificationTypeID NotificationTypeId { get; set; }
    }
}
