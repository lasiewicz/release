﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Content.ValueObjects.SystemEvents.Interfaces;
using Matchnet.Content.ValueObjects.PushNotification;

namespace Matchnet.Content.ValueObjects.SystemEvents
{
    public abstract class SystemEvent: ISystemEvent
    {
        public SystemEventType EventType { get; set; }
        public SystemEventCategory EventCategory { get; set; }
        public int MemberId { get; set; }
        public int TargetMemberId { get; set; }
        public int ApplicationId { get; set; }
        public int BrandId { get; set; }
        public string Message { get; set; }
        public AppGroupID AppGroupId { get; set; }
        public abstract int GetOperativeMemberId();
    }
}
