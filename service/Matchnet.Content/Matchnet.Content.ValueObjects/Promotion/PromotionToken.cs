using System;

namespace Matchnet.Content.ValueObjects.Promotion
{
	/// <summary>
	/// Summary description for PromotionToken.
	/// </summary>
	[Serializable]
	public class PromotionToken : ICacheable
	{

		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		private string _Name;
		private string _Value;
		#endregion

		/// <summary>
		/// 
		/// </summary>
		public string Name
		{
			get {return _Name;}
			set {_Name = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Value
		{
			get {return _Value;}
			set {_Value = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public PromotionToken()
		{
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}

		#endregion
	}
}
