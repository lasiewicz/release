using System;

namespace Matchnet.Content.ValueObjects.Promotion
{
	/// <summary>
	/// Summary description for Promoter.
	/// </summary>
	[Serializable]
	public class Promoter : ICacheable
	{
		private int _promoterID;
		private int _promoterTypeID;
		private int _countryRegionID;
		private string _emailAddress;
		private DateTime _insertDate;
		private string _firstName;
		private string _lastName;
		private string _company;
		private string _address;
		private string _address2;
		private string _city;
		private string _region;
		private string _postalCode;
		private string _phone;
		private string _fax;
		private string _comments;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="promoterID"></param>
		/// <param name="promoterTypeID"></param>
		/// <param name="promoterType"></param>
		/// <param name="countryRegionID"></param>
		/// <param name="emailAddress"></param>
		/// <param name="insertDate"></param>
		/// <param name="firstName"></param>
		/// <param name="lastName"></param>
		/// <param name="company"></param>
		/// <param name="address"></param>
		/// <param name="address2"></param>
		/// <param name="city"></param>
		/// <param name="region"></param>
		/// <param name="postalCode"></param>
		/// <param name="phone"></param>
		/// <param name="fax"></param>
		/// <param name="comments"></param>
		public Promoter(int promoterID, int promoterTypeID, string promoterType, int countryRegionID, string emailAddress, DateTime insertDate,
						string firstName, string lastName, string company, string address, string address2, string city, string region,
						string postalCode, string phone, string fax, string comments)
		{
			_promoterID = promoterID;
			_promoterTypeID = promoterTypeID;
			_countryRegionID = countryRegionID;
			_emailAddress = emailAddress;
			_insertDate = insertDate;
			_firstName = firstName;
			_lastName = lastName;
			_company = company;
			_address = address;
			_address2 = address2;
			_city = city;
			_region = region;
			_postalCode = postalCode;
			_phone = phone;
			_fax = fax;
			_comments = comments;
		}

		/// <summary>
		/// 
		/// </summary>
		public int PromoterID
		{
			get { return(_promoterID); }
		}

		/// <summary>
		/// 
		/// </summary>
		public int PromoterTypeID
		{
			get { return(_promoterTypeID); }
		}

		/// <summary>
		/// 
		/// </summary>
		public int CountryRegionID
		{
			get { return(_countryRegionID); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string EmailAddress
		{
			get { return(_emailAddress); }
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDate
		{
			get { return(_insertDate); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string FirstName
		{
			get { return(_firstName); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string LastName
		{
			get { return(_lastName); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string Company
		{
			get { return(_company); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string Address
		{
			get { return(_address); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string Address2
		{
			get { return(_address2); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string City
		{
			get { return(_city); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string Region
		{
			get { return(_region); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string Phone
		{
			get { return(_phone); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string Fax
		{
			get { return(_fax); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string Comments
		{
			get { return(_comments); }
		}
		 

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				// TODO:  Add Promoter.CacheTTLSeconds getter implementation
				return 0;
			}
			set
			{
				// TODO:  Add Promoter.CacheTTLSeconds setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				// TODO:  Add Promoter.CacheMode getter implementation
				return new Matchnet.CacheItemMode ();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				// TODO:  Add Promoter.CachePriority getter implementation
				return new Matchnet.CacheItemPriorityLevel ();
			}
			set
			{
				// TODO:  Add Promoter.CachePriority setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			// TODO:  Add Promoter.GetCacheKey implementation
			return null;
		}

		#endregion
	}
}
