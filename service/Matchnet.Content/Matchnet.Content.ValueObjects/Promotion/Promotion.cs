using System;

namespace Matchnet.Content.ValueObjects.Promotion
{
	/// <summary>
	/// Summary description for Promotion.
	/// </summary>
	[Serializable]
	public class Promotion : ICacheable
	{
		private int _promotionID;
		private int _promotionTypeID;
		private string _promotionType;
		private DateTime _insertDate;
		private string _description;
		private string _url;
		private int _promoterID;
		private string _promoterCompany;
		private bool _shortRegFormFlag;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="promotionID"></param>
		/// <param name="promotionTypeID"></param>
		/// <param name="promotionType"></param>
		/// <param name="insertDate"></param>
		/// <param name="description"></param>
		/// <param name="url"></param>
		/// <param name="promoterID"></param>
		/// <param name="promoterCompany"></param>
		/// <param name="shortRegFormFlag"></param>
		public Promotion(int promotionID, int promotionTypeID, string promotionType, DateTime insertDate, string description, string url,
							int promoterID, string promoterCompany, bool shortRegFormFlag)
		{
			_promotionID = promotionID;
			_promotionTypeID = promotionTypeID;
			_promotionType = promotionType;
			_insertDate = insertDate;
			_description = description;
			_url = url;
			_promoterID = promoterID;
			_promoterCompany = promoterCompany;
			_shortRegFormFlag = shortRegFormFlag;
		}

		/// <summary>
		/// 
		/// </summary>
		public int PromotionID
		{
			get { return(_promotionID); }
		}

		/// <summary>
		/// 
		/// </summary>
		public int PromotionTypeID
		{
			get { return(_promotionTypeID); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string PromotionType
		{
			get { return(_promotionType); }
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDate
		{
			get { return(_insertDate); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string Description
		{
			get { return(_description); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string URL
		{
			get { return(_url); }
		}

		/// <summary>
		/// 
		/// </summary>
		public int PromoterID
		{
			get { return(_promoterID); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string PromoterCompany
		{
			get { return(_promoterCompany); }
		}

		/// <summary>
		/// 
		/// </summary>
		public bool ShortRegFormFlag
		{
			get { return(_shortRegFormFlag); }
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				// TODO:  Add Promotion.CacheTTLSeconds getter implementation
				return 0;
			}
			set
			{
				// TODO:  Add Promotion.CacheTTLSeconds setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				// TODO:  Add Promotion.CacheMode getter implementation
				return new Matchnet.CacheItemMode ();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				// TODO:  Add Promotion.CachePriority getter implementation
				return new Matchnet.CacheItemPriorityLevel ();
			}
			set
			{
				// TODO:  Add Promotion.CachePriority setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			// TODO:  Add Promotion.GetCacheKey implementation
			return null;
		}

		#endregion
	}
}
