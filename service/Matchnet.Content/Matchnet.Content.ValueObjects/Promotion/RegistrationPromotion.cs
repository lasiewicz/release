using System;

namespace Matchnet.Content.ValueObjects.Promotion
{
	/// <summary>
	/// Summary description for RegistrationPromotion.
	/// </summary>
	[Serializable]
	public class RegistrationPromotion : IComparable
	{
		private int _registrationPromotionID;
		private int _promotionID;
		private string _resourceConstant;
		private string _description;
		private int _listOrder;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="registrationPromotionID"></param>
		/// <param name="promotionID"></param>
		/// <param name="resourceConstant"></param>
		/// <param name="listOrder"></param>
		public RegistrationPromotion(int registrationPromotionID, int promotionID, string resourceConstant, int listOrder)
		{
			_registrationPromotionID = registrationPromotionID;
			_promotionID = promotionID;
			_resourceConstant = resourceConstant;
			_description = String.Empty;
			_listOrder = listOrder;
		}

		/// <summary>
		/// 
		/// </summary>
		public int RegistrationPromotionID
		{
			get { return(_registrationPromotionID); }
		}

		/// <summary>
		/// 
		/// </summary>
		public int PromotionID
		{
			get { return(_promotionID); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string ResourceConstant
		{
			get { return(_resourceConstant); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string Description
		{
			get { return(_description); }
			set { _description = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public int ListOrder
		{
			get { return _listOrder; }
		}

		#region IComparable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public int CompareTo(object obj)
		{
			RegistrationPromotion rp = (RegistrationPromotion) obj;
			
			return this.ListOrder.CompareTo(rp.ListOrder);
		}

		#endregion
	}
}
