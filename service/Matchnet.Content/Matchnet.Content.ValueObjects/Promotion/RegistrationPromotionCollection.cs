using System;

namespace Matchnet.Content.ValueObjects.Promotion
{
	/// <summary>
	/// Summary description for RegistrationPromotionCollection.
	/// </summary>
	[Serializable]
	public class RegistrationPromotionCollection : System.Collections.CollectionBase, ICacheable
	{

		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		#endregion

		/// <summary>
		/// 
		/// </summary>
		public RegistrationPromotionCollection()
		{
			
		}

		/// <summary>
		/// 
		/// </summary>
		public RegistrationPromotion this[int index]
		{
			get { return ((RegistrationPromotion)base.InnerList[index]); }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="registrationPromotion"></param>
		/// <returns></returns>
		public int Add(RegistrationPromotion registrationPromotion)
		{
			return base.InnerList.Add(registrationPromotion);
		}

		/// <summary>
		/// 
		/// </summary>
		public void Sort()
		{
			base.InnerList.Sort();
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}

		#endregion
	}
}
