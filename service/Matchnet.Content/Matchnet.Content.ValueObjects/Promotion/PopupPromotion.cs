using System;

namespace Matchnet.Content.ValueObjects.Promotion
{
	/// <summary>
	/// Summary description for PopupPromotion.
	/// This is where code from the Matchnet.Web.Framework project's Promotion class was moved.
	/// </summary>
	[Serializable]
	public class PopupPromotion : ICacheable
	{
		#region class variables
		private PromotionType _promotionType = PromotionType.FiveDaysFree;
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		private string _ExitPopupURL = "";
		private int _ExitPopupWidth = 0;
		private int _ExitPopupHeight = 0;
		private string _ExitPopupProps = "";
		private string _ExitPopupDestinationURL = "";
		private System.DateTime _Date = System.DateTime.Now;
		private bool _PopulateFromDBaseFlag = false;
		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="exitPopupURL"></param>
		/// <param name="exitPopupWidth"></param>
		/// <param name="exitPopupHeight"></param>
		/// <param name="exitPopupProps"></param>
		/// <param name="exitPopupDestinationURL"></param>
		/// <param name="promotionType"></param>
		/// <param name="date"></param>
		/// <param name="populatedFromDBaseFlag"></param>
		public PopupPromotion(string exitPopupURL, int exitPopupWidth, int exitPopupHeight, string exitPopupProps, string exitPopupDestinationURL,
								PromotionType promotionType, DateTime date, bool populatedFromDBaseFlag)
		{
			_ExitPopupURL = exitPopupURL;
			_ExitPopupWidth = exitPopupWidth;
			_ExitPopupHeight = exitPopupHeight;
			_ExitPopupProps = exitPopupProps;
			_ExitPopupDestinationURL = exitPopupDestinationURL;
			_promotionType = promotionType;
			_Date = date;
			_PopulateFromDBaseFlag = populatedFromDBaseFlag;
		}

		#region Public Properties

		/// <summary>
		/// 
		/// </summary>
		public string ExitPopupURL
		{
			get { return(_ExitPopupURL); }
		}

		/// <summary>
		/// 
		/// </summary>
		public int ExitPopupWidth
		{
			get { return(_ExitPopupWidth); }
		}

		/// <summary>
		/// 
		/// </summary>
		public int ExitPopupHeight
		{
			get { return(_ExitPopupHeight); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string ExitPopupProps
		{
			get { return(_ExitPopupProps); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string ExitPopupDestinationURL
		{
			get { return(_ExitPopupDestinationURL); }
		}

		/// <summary>
		/// 
		/// </summary>
		public PromotionType PromotionType
		{
			get { return(_promotionType); }
		}

		/// <summary>
		/// Used to see if we need to retrieve the Promotion info from the dbase.
		/// </summary>
		/// <remarks>
		/// This is necessary to avoid a call to the dbase on every page.
		/// This must be set to <c>false</c> after the user registers or subscribes or he might be displayed the wrong info
		/// (i.e. he might be displayed the wrong exit popup).
		/// </remarks>
		public bool CheckPromotionFlag
		{
			get { return(true); }
			/*
			 *	TOFIX - Hopefully the PromotionSA caching will make the every-page DB call
			 * unnecessary.  No way to check from here what is going on in g.  For performance
			 * reasons we are going to need to figure out what this is actually supposed to
			 * be doing. - dcornell
			 * 
			get
			{
				return Util.CBool(_g.Session["CheckPromotionFlag"], false);
			}
			set
			{
				_g.Session.Add("CheckPromotionFlag", Convert.ToString(value), SessionPropertyLifetime.Temporary);
			}
			*/
		}

		/// <remarks>
		/// Currently, <c>Date</c> is always the current date.  This is used to return current Promotions, only.
		/// In the future, Admin apps may be created which use this field in order to see which Promotion
		/// a User was eligible for at any given date in the past or future.
		/// </remarks>
		public System.DateTime Date
		{
			get
			{
				return _Date;
			}
			set
			{
				_Date = value;
			}
		}

		/*
		 *	This uses a bunch of stuff in g, which is obviously inaccessible from here in
		 * the service layer.  Hopefully this isn't in use such that we need to know if from
		 * here rather than just building it from the wrapper control - dcornell
		 * 
		/// <remarks>
		/// Used primarily for testing/debugging.  Shows the UserConditions that the database will
		/// check against to see for which Promotion that the user is eligible.
		/// </remarks>
		public string UserConditions
		{
			get
			{
				return BuildUserConditions();
			}
		}
		*/

		/// <remarks>
		/// Used primarily for testing/debugging.  Shows the the Promotion information was retrieved from
		/// the database (true) or the cookie/session (false).
		/// </remarks>
		public bool PopulateFromDBaseFlag
		{
			get
			{
				return _PopulateFromDBaseFlag;
			}
		}
		#endregion

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}

		#endregion
	}
}
