﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.Promotion
{
    /// <summary>
    /// Cacheable Wrapper class of PRMURLMapper objects
    /// </summary>
    [Serializable]
    public class PRMURLMapperList : IValueObject, ICacheable
    {
        List<PRMURLMapper> _PRMURLMappers = new List<PRMURLMapper>();

        #region Properties
        public List<PRMURLMapper> PRMURLMappers
        {
            get { return _PRMURLMappers; }
            set { _PRMURLMappers = value; }
        }

        #endregion

        #region ICacheable Members
        private int _cacheTTLSeconds = 7200;
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
        private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Absolute;
        const string CACHEKEYFORMAT = "PRMURLMapperList";

        public CacheItemMode CacheMode
        {
            get { return _cacheItemMode; }
            set { _cacheItemMode = value; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return _cachePriority; }
            set { _cachePriority = value; }
        }

        public int CacheTTLSeconds
        {
            get { return _cacheTTLSeconds; }
            set { _cacheTTLSeconds = value; }
        }

        public string GetCacheKey()
        {
            return GetCacheKeyString();
        }

        public static string GetCacheKeyString()
        {
            return CACHEKEYFORMAT;
        }

        #endregion

        #region Public methods
        public PRMURLMapper GetPRMByReferralURL(string referralURL)
        {
            PRMURLMapper prmURLMapper = null;

            if (_PRMURLMappers != null && _PRMURLMappers.Count > 0)
            {
                if (!String.IsNullOrEmpty(referralURL))
                {
                    //check for exact matches
                    foreach (PRMURLMapper p in _PRMURLMappers)
                    {
                        if (!p.IsCatchAll)
                        {
                            if (p.SourceURL.ToLower().Trim() == referralURL.ToLower().Trim())
                            {
                                prmURLMapper = p;
                                break;
                            }
                        }
                    }

                    //check for catch-all based on priority
                    //note: mappers already sorted by priority in desc in db
                    if (prmURLMapper == null)
                    {
                        foreach (PRMURLMapper p in _PRMURLMappers)
                        {
                            if (p.IsCatchAll)
                            {
                                if (referralURL.ToLower().Contains(p.SourceURL.ToLower()) || String.IsNullOrEmpty(p.SourceURL))
                                {
                                    prmURLMapper = p;
                                    break;
                                }
                            }
                        }
                    }
                }

                //get default PRM, should be last one in the list
                //note: mappers already sorted by priority in desc in db
                if (prmURLMapper == null)
                {
                    prmURLMapper = _PRMURLMappers[_PRMURLMappers.Count - 1];
                }
            }

            return prmURLMapper;
        }
        #endregion
    }
}
