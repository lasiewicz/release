using System;

namespace Matchnet.Content.ValueObjects.Promotion
{
	/// <summary>
	/// Summary description for BannerCollection.
	/// </summary>
	[Serializable]
	public class BannerCollection : System.Collections.CollectionBase, IValueObject, ICacheable
	{
		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		#endregion

		/// <summary>
		/// Parameterless constructor
		/// </summary>
		public BannerCollection()
		{

		}

		/// <summary>
		/// Indexer overload
		/// </summary>
		public Banner this[int index]
		{
			get { return ((Banner)base.InnerList[index]); }
		}

		/// <summary>
		/// Overloaded add method
		/// </summary>
		/// <param name="banner">banner to add to collection</param>
		/// <returns></returns>
		public int Add(Banner banner)
		{
			return base.InnerList.Add(banner);
		}

		/// <summary>
		/// Returns a banner given the appropriate BannerID. 
		/// </summary>
		/// <param name="BannerID">The integer representing the Banner's ID.</param>
		/// <returns>If BannerID exists in the banner collection, then that Banner.  Otherwise a null Banner is returned.</returns>
		public Banner GetBanner(int BannerID)
		{
			foreach(Banner banner in base.InnerList)
			{
				if(banner.BannerID == BannerID)
				{
					return banner;
				}
				else
				{
					// if the supplied BannerID was not found, return a random Banner
					int randomBannerOrdinal = (int) (Math.Floor(InnerList.Count * (new Random((int)DateTime.Now.Ticks).NextDouble())));
					return (Banner)InnerList[randomBannerOrdinal];
					//return new Banner();
				}
			}

			return new Banner();
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}
		#endregion
	}
}
