﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.Promotion
{
    /// <summary>
    /// Represents a single PromotionID to Referral URL Mapper
    /// </summary>
    [Serializable]
    public class PRMURLMapper : IValueObject
    {
        private int _PromotionID = Constants.NULL_INT;
        private int _Priority = Constants.NULL_INT;
        private int _PromotionIDURLMapperID = Constants.NULL_INT;
        private bool _IsCatchAll = false;
        private string _SourceURL = "";
        private string _Description = "";

        #region Properties
        public int PromotionID
        {
            get { return _PromotionID; }
            set { _PromotionID = value; }
        }

        public int Priority
        {
            get { return _Priority; }
            set { _Priority = value; }
        }

        public int PromotionIDURLMapperID
        {
            get { return _PromotionIDURLMapperID; }
            set { _PromotionIDURLMapperID = value; }
        }

        public bool IsCatchAll
        {
            get { return _IsCatchAll; }
            set { _IsCatchAll = value; }
        }

        public string SourceURL
        {
            get { return _SourceURL; }
            set { _SourceURL = value; }
        }

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        
        #endregion

    }
}
