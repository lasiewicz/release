using System;

namespace Matchnet.Content.ValueObjects.Promotion
{
	/// <summary>
	/// Summary description for Banner.
	/// </summary>
	[Serializable]
	public class Banner : IValueObject
	{
		private int _bannerID;
		private string _resourceConstant;

		/// <summary>
		/// Parameterized constructor
		/// </summary>
		public Banner(int bannerID, string resourceConstant)
		{
			_bannerID = bannerID;
			_resourceConstant = resourceConstant;
		}

		/// <summary>
		/// For creating a null banner
		/// </summary>
		public Banner()
		{
		}

		/// <summary>
		/// BannerID
		/// </summary>
		public int BannerID
		{
			get { return(_bannerID); }
		}

		/// <summary>
		/// The Resource constant for the banner to display
		/// </summary>
		public string ResourceConstant
		{
			get { return (_resourceConstant); }
		}
		}
}
