using System;

namespace Matchnet.Content.ValueObjects.Promotion
{
	/// <summary>
	/// Summary description for PRMInfo.
	/// </summary>
	[Serializable]
	public class PRMInfo : ICacheable
	{
		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		private string _promoter;
		private string _promotion;
		#endregion
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="promoter"></param>
		/// <param name="promotion"></param>
		public PRMInfo(string promoter, string promotion)
		{
			_promoter = promoter;
			_promotion = promotion;
		}

		/// <summary>
		/// 
		/// </summary>
		public string Promoter
		{
			get { return(_promoter); }
		}

		/// <summary>
		/// 
		/// </summary>
		public string Promotion
		{
			get { return(_promotion); }
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}

		#endregion
	}
}
