using System;

namespace Matchnet.Content.ValueObjects.Promotion
{
	/// <summary>
	/// Summary description for PromotionTokenCollection.
	/// </summary>
	[Serializable]
	public class PromotionTokenCollection : System.Collections.CollectionBase, ICacheable
	{
		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		#endregion

		/// <summary>
		/// 
		/// </summary>
		public PromotionTokenCollection()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		public PromotionToken this[int index]
		{
			get { return ((PromotionToken)base.InnerList[index]); }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pPromotionToken"></param>
		/// <returns></returns>
		public int Add(PromotionToken pPromotionToken)
		{
			return base.InnerList.Add(pPromotionToken);
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}

		#endregion
	}
}
