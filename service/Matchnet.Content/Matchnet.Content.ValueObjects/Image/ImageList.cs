using System;

namespace Matchnet.Content.ValueObjects.Image
{
	/// <summary>
	/// Summary description for ImageList.
	/// </summary>
	[Serializable]
	public class ImageList : ICacheable
	{

		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		private string _ImageName = null;
		private int _ApplicationID = 0;
		private int _PrivateLabelID = 0;
		private int _LanguageID = 0;
		private int _DomainID = 0;
		#endregion

		/// <summary>
		/// 
		/// </summary>
		public string ImageName
		{
			get {return _ImageName;}
			set {_ImageName = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public int ApplicationID
		{
			get {return _ApplicationID;}
			set {_ApplicationID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public int PrivateLabelID
		{
			get {return _PrivateLabelID;}
			set {_PrivateLabelID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public int LanguageID
		{
			get {return _LanguageID;}
			set {_LanguageID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public int DomainID
		{
			get {return _DomainID;}
			set {_DomainID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public ImageList()
		{
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}

		#endregion
	}
}
