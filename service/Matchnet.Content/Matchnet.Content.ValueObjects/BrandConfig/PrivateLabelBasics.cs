using System;

namespace Matchnet.Content.ValueObjects.BrandConfig
{
	/// <summary>
	/// Summary description for PrivateLabelBasics.
	/// </summary>
	[Serializable]
	public class PrivateLabelBasics
	{
		private int _privateLabelID;
		private string _description;
		private string _URI;

		public PrivateLabelBasics(int privateLabelID, string description, string URI)
		{
			_privateLabelID = privateLabelID;
			_description = description;
			_URI = URI;
		}

		public int PrivateLabelID
		{
			get { return(_privateLabelID); }
		}

		public string Description
		{
			get { return(_description); }
		}

		public string URI
		{
			get { return(_URI); }
		}
	}
}
