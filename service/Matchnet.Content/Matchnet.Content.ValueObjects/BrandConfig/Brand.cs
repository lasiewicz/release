using System;

namespace Matchnet.Content.ValueObjects.BrandConfig
{
	/// <summary>
	/// Represents Brand status types
	/// </summary>
	[Serializable, Flags]
	public enum StatusType : int
	{
		/// <summary>
		/// Requires/accepts payment to enable certain features/permissions
		/// </summary>
		PaySite	= 1,

		/// <summary>
		/// 
		/// </summary>
		CoBrand	= 2,

		/// <summary>
		/// 
		/// </summary>
		BlockedAtRegistration = 4,

		/// <summary>
		/// Requires valid subscription to reply to an email
		/// </summary>
		PayToReplySite = 8,

		/// <summary>
		/// 
		/// </summary>
		CobrandHeader = 128,

		/// <summary>
		/// Disables analytics
		/// </summary>
		AnalyticsDisable = 256,

		/// <summary>
		/// Allows members to designate photos as private
		/// </summary>
		PrivatePhotos = 512,

		/// <summary>
		/// Requires payment to send email to other members
		/// </summary>
		PayToSendEmail = 1024,

		/// <summary>
		/// Requires payment to initiate an IM session with another member
		/// </summary>
		PayToInitiateIM = 2048,

		/// <summary>
		/// 
		/// </summary>
		DisableSSLURL = 4096
	}


	/// <summary>
	/// Represents a distinctive name/configuration for a Site. A Brand is a child object of Site.
	/// </summary>
	[Serializable]
	public class Brand
	{
		private Int32 _brandID;
		private Site _site;
		private String _uri;
		private Int32 _statusMask;
		private String _phoneNumber;
		private Int16 _defaultAgeMin;
		private Int16 _defaultAgeMax;
		private Int16 _defaultSearchRadius;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandID">brandID</param>
		/// <param name="site">site</param>
		/// <param name="uri">uri</param>
		/// <param name="statusMask">status</param>
		/// <param name="phoneNumber">CS contact number</param>
		/// <param name="defaultAgeMin">default search minimum age</param>
		/// <param name="defaultAgeMax">default search maximum age</param>
		/// <param name="defaultSearchRadius">default search radius</param>
		public Brand(Int32 brandID,
			Site site,
			String uri,
			Int32 statusMask,
			String phoneNumber,
			Int16 defaultAgeMin,
			Int16 defaultAgeMax,
			Int16 defaultSearchRadius)
		{
			_brandID = brandID;
			_site = site;
			_uri = uri;
			_statusMask = statusMask;
			_phoneNumber = phoneNumber;
			_defaultAgeMin = defaultAgeMin;
			_defaultAgeMax = defaultAgeMax;
			_defaultSearchRadius = defaultSearchRadius;
		}


		/// <summary>
		/// Brand identifier
		/// </summary>
		public Int32 BrandID
		{
			get
			{
				return _brandID;
			}
		}


		/// <summary>
		/// Corresponding Site identifier
		/// </summary>
		public Site Site
		{
			get
			{
				return _site;
			}
		}


		/// <summary>
		/// Uri of which Brand represents
		/// </summary>
		public String Uri
		{
			get
			{
				return _uri;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 StatusMask
		{
			get
			{
				return _statusMask;
			}
		}


		/// <summary>
		/// CS support phone number
		/// </summary>
		public String PhoneNumber
		{
			get
			{
				return _phoneNumber;
			}
		}


		/// <summary>
		/// default search minimum age
		/// </summary>
		public Int16 DefaultAgeMin
		
		{
			get
			{
				return _defaultAgeMin;
			}
		}


		/// <summary>
		/// default search maximum age
		/// </summary>
		public Int16 DefaultAgeMax
		{
			get
			{
				return _defaultAgeMax;
			}
		}


		/// <summary>
		/// default search radius
		/// </summary>
		public Int16 DefaultSearchRadius
		{
			get
			{
				return _defaultSearchRadius;
			}
		}

		/// <summary>
		/// Returns true iff this is a pay site
		/// </summary>
		public bool IsPaySite
		{
			get
			{
				return GetStatusMaskValue(StatusType.PaySite);
			}
		}
		
		/// <summary>
		/// Returns true iff this site requires you to pay to reply to e-mails
		/// </summary>
		public bool IsPayToReplySite
		{
			get
			{
				return GetStatusMaskValue(StatusType.PayToReplySite);
			}
		}

		/// <summary>
		/// Determines whether the bit specified by statusType is set in the StatusMask for this Brand.
		/// </summary>
		/// <param name="statusType"></param>
		/// <returns></returns>
		public bool GetStatusMaskValue(StatusType statusType)
		{
			return (((StatusType)_statusMask & statusType) == statusType);
		}
	}
}
