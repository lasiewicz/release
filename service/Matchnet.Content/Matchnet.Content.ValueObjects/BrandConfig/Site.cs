using System;
using System.Globalization;

namespace Matchnet.Content.ValueObjects.BrandConfig
{
	/// <summary>
	/// Defines the possible Text orientation types for content.
	/// </summary>
	[Serializable]
	public enum DirectionType
	{
		/// <summary>left-to-right (e.g. English)</summary>
		ltr,

		/// <summary>right-to-left (e.g. Hebrew)</summary>
		rtl
	}


	/// <summary>
	/// A Site is a child object of a Community.
	/// </summary>
	[Serializable]
	public class Site
	{
		private Int32 _siteID;
		private Community _community;
		private String _name;
		private Int32 _currencyID;
		private Int32 _languageID;
		//private string _cultureName;

		//[NonSerialized]
		private CultureInfo _cultureInfo;

		private Int32 _defaultRegionID;
		private Int32 _searchTypeMask;
		private Int32 _defaultSearchTypeID;
		private String _cssPath;
		private Int32 _charset;
		private DirectionType _direction;
		private Int16 _gmtOffset;
		private String _defaultHost;
		private String _sslUrl;
		private Int32 _paymentTypeMask;
	    private bool _hasRegistrationMetadata;

		public Site()
		{}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteID"></param>
		/// <param name="community"></param>
		/// <param name="name"></param>
		/// <param name="currencyID"></param>
		/// <param name="languageID"></param>
		/// <param name="cultureName"></param>
		/// <param name="defaultRegionID"></param>
		/// <param name="searchTypeMask"></param>
		/// <param name="defaultSearchTypeID"></param>
		/// <param name="cssPath"></param>
		/// <param name="charset"></param>
		/// <param name="direction"></param>
		/// <param name="gmtOffset"></param>
		/// <param name="defaultHost"></param>
		/// <param name="sslUrl"></param>
		/// <param name="paymentTypeMask"></param>
		public Site(Int32 siteID,
			Community community,
			String name,
			Int32 currencyID,
			Int32 languageID,
			string cultureName,
			Int32 defaultRegionID,
			Int32 searchTypeMask,
			Int32 defaultSearchTypeID,
			String cssPath,
			Int32 charset,
			DirectionType direction,
			Int16 gmtOffset,
			String defaultHost,
			String sslUrl,
			Int32 paymentTypeMask)
		{
			_siteID = siteID;
			_community = community;
			_name = name;
			_currencyID = currencyID;
			_languageID = languageID;
			//_cultureName = cultureName;
			_cultureInfo = new CultureInfo(cultureName);
			_defaultRegionID = defaultRegionID;
			_searchTypeMask = searchTypeMask;
			_defaultSearchTypeID = defaultSearchTypeID;
			_cssPath = cssPath;
			_charset = charset;
			_direction = direction;
			_gmtOffset = gmtOffset;
			_defaultHost = defaultHost;
			_sslUrl = sslUrl;
			_paymentTypeMask = paymentTypeMask;
		}

        public Site(Int32 siteID,
			Community community,
			String name,
			Int32 currencyID,
			Int32 languageID,
			string cultureName,
			Int32 defaultRegionID,
			Int32 searchTypeMask,
			Int32 defaultSearchTypeID,
			String cssPath,
			Int32 charset,
			DirectionType direction,
			Int16 gmtOffset,
			String defaultHost,
			String sslUrl,
			Int32 paymentTypeMask, 
            bool hasRegistrationMetadata): this(siteID, community, name, currencyID, languageID, cultureName, defaultRegionID, 
            searchTypeMask, defaultSearchTypeID, cssPath, charset, direction, gmtOffset, defaultHost, sslUrl, paymentTypeMask)
        {
            _hasRegistrationMetadata = hasRegistrationMetadata;
        }

		/// <summary>
		/// Site identifier
		/// </summary>
		public Int32 SiteID
		{
			get
			{
				return _siteID;
			}
		}


		/// <summary>
		/// Parent community
		/// </summary>
		public Community Community
		{
			get
			{
				return _community;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public String Name
		{
			get
			{
				return _name;
			}
		}


		/// <summary>
		/// Currency type identifier
		/// </summary>
		public Int32 CurrencyID
		{
			get
			{
				return _currencyID;
			}
		}


		/// <summary>
		/// Language type identifier
		/// </summary>
		public Int32 LanguageID
		{
			get
			{
				return _languageID;
			}
		}


		/// <summary>
		/// Culture information
		/// </summary>
		public CultureInfo CultureInfo
		{
			get
			{
				//if (_cultureInfo == null)
				//{
				//	_cultureInfo = new CultureInfo(_cultureName);
				//}
				return _cultureInfo;
			}
		}


		/// <summary>
		/// Default Region identifier
		/// </summary>
		public Int32 DefaultRegionID
		{
			get
			{
				return _defaultRegionID;
			}
		}


		/// <summary>
		/// Supported search types
		/// </summary>
		public Int32 SearchTypeMask
		{
			get
			{
				return _searchTypeMask;
			}
		}


		/// <summary>
		/// Default search type
		/// </summary>
		public Int32 DefaultSearchTypeID
		{
			get
			{
				return _defaultSearchTypeID;
			}
		}


		/// <summary>
		/// Relative url path to CSS stylesheet
		/// </summary>
		public String CSSPath
		{
			get
			{
				return _cssPath;
			}
		}


		/// <summary>
		/// Character set identifier
		/// </summary>
		public Int32 Charset
		{
			get
			{
				return _charset;
			}
		}


		/// <summary>
		/// Text orientation
		/// </summary>
		public DirectionType Direction
		{
			get
			{
				return _direction;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string LAlign
		{
			get
			{
				if (Direction == DirectionType.ltr)
				{
					return "left";
				}
				else
				{
					return "right";
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string RAlign
		{
			get
			{
				if (Direction == DirectionType.ltr)
				{
					return "right";
				}
				else
				{
					return "left";
				}
			}
		}


		/// <summary>
		/// Time zone offset from GMT
		/// </summary>
		public Int16 GMTOffset
		{
			get
			{
				return _gmtOffset;
			}
		}


		/// <summary>
		/// Default hostname
		/// </summary>
		public String DefaultHost
		{
			get
			{
				return _defaultHost;
			}
		}


		/// <summary>
		/// SSL hostname
		/// </summary>
		public String SSLUrl
		{
			get
			{
				return _sslUrl;
			}
		}


		/// <summary>
		/// Accepted forms of payment
		/// </summary>
		public Int32 PaymentTypeMask
		{
			get
			{
				return _paymentTypeMask;
			}
		}

	    public bool HasRegistrationMetadata
	    {
	        get { return _hasRegistrationMetadata; }
	    }
	}
}
