using System;
using System.Collections;

namespace Matchnet.Content.ValueObjects.BrandConfig
{
	/// <summary>
	/// Represents a collection of Brand objects
	/// </summary>
	[Serializable]
	public class Brands : ICollection, IValueObject, ICacheable, IEnumerable
	{
		/// <summary>
		/// 
		/// </summary>
		public const string CACHE_KEY = "Brands";

		private int _cacheTTLSeconds = 60 * 60;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Hashtable _communityTable;
		private Hashtable _siteTable;
		private Hashtable _brandTable;
		private Hashtable _brandAliasTable;
		private Hashtable _uriBrandTable;
		private Hashtable _uriBrandAliasTable;

		/// <summary>
		/// 
		/// </summary>
		public Brands()
		{
			_communityTable = new Hashtable();
			_siteTable = new Hashtable();
			_brandTable = new Hashtable();
			_brandAliasTable = new Hashtable();
			_uriBrandTable = new Hashtable();
			_uriBrandAliasTable = new Hashtable();
		}


		/// <summary>
		/// Adds a Brand to the collection
		/// </summary>
		/// <param name="brand"></param>
		public void AddBrand(Brand brand)
		{
			if (_uriBrandTable.ContainsKey(brand.Uri.ToLower()))
			{
				return;
			}

			_brandTable.Add(brand.BrandID, brand);

			if (!_siteTable.ContainsKey(brand.Site.SiteID))
			{
				_siteTable.Add(brand.Site.SiteID,
					brand.Site);
			}

			if (!_communityTable.ContainsKey(brand.Site.Community.CommunityID))
			{
				_communityTable.Add(brand.Site.Community.CommunityID,
					brand.Site.Community);
			}

			_uriBrandTable.Add(brand.Uri.ToLower(), brand);
		}


		/// <summary>
		/// Adds a BrandAlias to the collection
		/// </summary>
		/// <param name="brandAlias"></param>
		public void AddBrandAlias(BrandAlias brandAlias)
		{
			if (_uriBrandAliasTable.ContainsKey(brandAlias.Uri.ToLower()))
			{
				return;
			}

			_brandAliasTable.Add(brandAlias.BrandAliasID, brandAlias);

			_uriBrandAliasTable.Add(brandAlias.Uri.ToLower(), brandAlias);
		}


		/// <summary>
		/// Retreives a BrandObject for a given uri (case-insensitive)
		/// </summary>
		/// <param name="uri"></param>
		/// <returns>Brand object</returns>
		public Brand GetBrand(string uri)
		{
			Brand brand = null;
			uri = uri.ToLower();
			string[] uriParts = uri.Split('.');
			int uriPartDrop = 0;

			while (true)
			{
				brand = _uriBrandTable[uri] as Brand;

				if (brand != null)
				{
					break;
				}
				else
				{
					if ( uriPartDrop == uriParts.Length - 2 ||
						 uriParts.Length == 1 )
					{
						brand = null;
						break;
					}
					else
					{
						uri = "";

						for (int i = uriParts.Length - 1; i > uriPartDrop; i--)
						{
							if (uri.Length > 0)
							{
								uri = "." + uri;
							}
							uri = uriParts[i] + uri;
						}

						uriPartDrop++;
					}
				}
			}

			return brand;
		}


		/// <summary>
		/// Retreives a BrandObject for a given BrandID
		/// </summary>
		/// <param name="brandID"></param>
		/// <returns>Brand object</returns>
		public Brand GetBrand(Int32 brandID)
		{
			return _brandTable[brandID] as Brand;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="groupID"></param>
		/// <returns></returns>
		public Int32 GetCommunityID(Int32 groupID)
		{
			if (_brandTable.ContainsKey(groupID))
			{
				return ((Brand)_brandTable[groupID]).Site.Community.CommunityID;
			}

			if (_siteTable.ContainsKey(groupID))
			{
				return ((Site)_siteTable[groupID]).Community.CommunityID;
			}

			if (_communityTable.ContainsKey(groupID))
			{
				return groupID;
			}

			return Constants.NULL_INT;
		}

		
		public Site[] GetSites()
		{
			Array sites = Array.CreateInstance(typeof(Site), _siteTable.Values.Count);
			_siteTable.Values.CopyTo(sites, 0);
			return (Site[])sites;
		}


		public Site GetSite(Int32 siteID)
		{
			return _siteTable[siteID] as Site;
		}

        /// <summary>
        /// Returns the siteID with the lowest ID for a given communityID
        /// </summary>
        /// <param name="communityID"></param>
        /// <returns></returns>
	    public int GetDefaultSiteIDForCommunity(int communityID)
        {
            var siteID = Constants.NULL_INT;

            foreach(Site s in _siteTable.Values)
	        {
                if (s.Community.CommunityID == communityID && siteID == Constants.NULL_INT)
	            {
	                siteID = s.SiteID;
	            }
                else if (s.Community.CommunityID == communityID &&  s.SiteID < siteID)
                {
                    siteID = s.SiteID;
                }
	        }

            return siteID;
        }

		/// <summary>
		/// Retreives a BrandAliasObject for a given uri (case-insensitive)
		/// </summary>
		/// <param name="uri"></param>
		/// <returns>BrandAlias object</returns>
		public BrandAlias GetBrandAlias(string uri)
		{
			BrandAlias brandAlias = null;
			uri = uri.ToLower();
			string[] uriParts = uri.Split('.');
			int uriPartDrop = 0;

			while (true)
			{
				brandAlias = _uriBrandAliasTable[uri] as BrandAlias;

				if (brandAlias != null)
				{
					break;
				}
				else
				{
					if ( uriPartDrop == uriParts.Length - 2 ||
						uriParts.Length == 1 )
					{
						brandAlias = null;
						break;
					}
					else
					{
						uri = "";

						for (int i = uriParts.Length - 1; i > uriPartDrop; i--)
						{
							if (uri.Length > 0)
							{
								uri = "." + uri;
							}
							uri = uriParts[i] + uri;
						}

						uriPartDrop++;
					}
				}
			}

			return brandAlias;
		}

		/// <summary>
		/// Retrieve a BrandAlias by ID.
		/// </summary>
		/// <param name="brandAliasID"></param>
		/// <returns></returns>
		public BrandAlias GetBrandAlias(Int32 brandAliasID)
		{
			if (_brandAliasTable[brandAliasID] != null)
				return _brandAliasTable[brandAliasID] as BrandAlias;

			return null;
		}


		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return Matchnet.CacheItemMode.Sliding;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return CACHE_KEY;
		}

		#endregion

		#region IEnumerable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public IEnumerator GetEnumerator()
		{
			return _brandTable.Values.GetEnumerator();
		}

		#endregion

		#region ICollection Members

		/// <summary>
		/// 
		/// </summary>
		public int Count
		{
			get { return(_brandTable.Count); }
		}

		/// <summary>
		/// 
		/// </summary>
		public bool IsSynchronized
		{
			get { return(false); }
		}

		/// <summary>
		/// 
		/// </summary>
		public object SyncRoot
		{
			get { return(null); }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="myArray"></param>
		/// <param name="index"></param>
		public void CopyTo(Array myArray, int index)
		{
			throw new NotImplementedException("The CopyTo(Array, int) method has not been implemented in Brands");
		}

		#endregion

	}
}
