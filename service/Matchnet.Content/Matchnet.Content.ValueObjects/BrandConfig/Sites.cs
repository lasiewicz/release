using System;
using System.Collections;
using System.Text;

namespace Matchnet.Content.ValueObjects.BrandConfig
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class Sites : CollectionBase, IValueObject, ICacheable
	{
		/// <summary>
		/// Cache key for site collection
		/// </summary>
		public const string CONTENT_BRAND_ALL_SITES_CACHE_KEY = "~CONTENTBRANDALLSITES";
		
		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		#endregion

		/// <summary>
		/// 
		/// </summary>
		public Sites()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		public Site this[int index]
		{
			get { return ((Site)base.InnerList[index]); }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="site"></param>
		/// <returns></returns>
		public int Add(Site site)
		{
			return base.InnerList.Add(site);
		}


		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}

		#endregion
	}
}
