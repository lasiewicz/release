using System;

namespace Matchnet.Content.ValueObjects.BrandConfig
{
	/// <summary>
	/// Summary description for PrivateLabel.
	/// </summary>
	[Serializable]
	public class PrivateLabel
	{
		private int _privateLabelID;
		private int _domainID;
		private int _defaultCurrencyID;
		private string _URI;
		private string _description;
		private int _languageID;
		private int _statusMask;
		private int _LCID;
		private int _defaultRegionID;
		private int _disableFlag;
		private string _CSSPath;
		private string _charSet;
		private string _RAlign;
		private string _LAlign;
		private string _direction;
		private string _colorLightest;
		private string _colorLighter;
		private string _colorLight;
		private string _colorMedium;
		private string _colorDark;
		private string _colorDarker;
		private string _colorDarkest;
		private string _bodyProperties;
		private string _imgUrl;
		private int _registrationStepCount;
		private int _defaultAgeMin;
		private int _defaultAgeMax;
		private string _chatDNS;
		private string _chatServer;
		private int _chatPort;
		private int _chatAppletVersion;
		private int _checkImVersion;
		private int _GMTOffset;
		private int _basePrivateLabelID;
		private bool _redirectToBase;
		private string _defaultHost;
		private string _SSLURL;
		private int _defaultSearchTypeID;
		private int _searchTypeMask;
		private int _defaultSearchRadius;
		private int _paymentTypeMask;
		private string _phoneNumber;

		public PrivateLabel(
		int privateLabelID,
		 int domainID,
		 int defaultCurrencyID,
		 string URI,
		 string description,
		 int languageID,
		 int statusMask,
		 int LCID,
		 int defaultRegionID,
		 int disableFlag,
		 string CSSPath,
		 string charSet,
		 string RAlign,
		 string LAlign,
		 string direction,
		 string colorLightest,
		 string colorLighter,
		 string colorLight,
		 string colorMedium,
		 string colorDark,
		 string colorDarker,
		 string colorDarkest,
		 string bodyProperties,
		 string imgUrl,
		 int registrationStepCount,
		 int defaultAgeMin,
		 int defaultAgeMax,
		 string chatDNS,
		 string chatServer,
		 int chatPort,
		 int chatAppletVersion,
		 int checkImVersion,
		 int GMTOffset,
		 int basePrivateLabelID,
		 bool redirectToBase,
		 string defaultHost,
		 string SSLURL,
		 int defaultSearchTypeID,
		 int searchTypeMask,
		 int defaultSearchRadius,
		 int paymentTypeMask,
		 string phoneNumber
			)
		{
			_privateLabelID=privateLabelID;
		  _domainID=domainID;
		  _defaultCurrencyID=defaultCurrencyID;
		  _URI=URI;
		  _description=description;
		  _languageID=languageID;
		  _statusMask=statusMask;
		  _LCID=LCID;
		  _defaultRegionID=defaultRegionID;
		  _disableFlag=disableFlag;
		  _CSSPath=CSSPath;
		  _charSet=charSet;
		  _RAlign=RAlign;
		  _LAlign=LAlign;
		  _direction=direction;
		  _colorLightest=colorLightest;
		  _colorLighter=colorLighter;
		  _colorLight=colorLight;
		  _colorMedium=colorMedium;
		  _colorDark=colorDark;
		  _colorDarker=colorDarker;
		  _colorDarkest=colorDarkest;
		  _bodyProperties=bodyProperties;
		  _imgUrl=imgUrl;
		  _registrationStepCount=registrationStepCount;
		  _defaultAgeMin=defaultAgeMin;
		  _defaultAgeMax=defaultAgeMax;
		  _chatDNS=chatDNS;
		  _chatServer=chatServer;
		  _chatPort=chatPort;
		  _chatAppletVersion=chatAppletVersion;
		  _checkImVersion=checkImVersion;
		  _GMTOffset=GMTOffset;
		  _basePrivateLabelID=basePrivateLabelID;
		  _redirectToBase=redirectToBase;
		  _defaultHost=defaultHost;
		  _SSLURL=SSLURL;
		  _defaultSearchTypeID=defaultSearchTypeID;
		  _searchTypeMask=searchTypeMask;
		  _defaultSearchRadius=defaultSearchRadius;
		  _paymentTypeMask=paymentTypeMask;
		  _phoneNumber=phoneNumber;
		}

		public int PrivateLabelID
		{
			get { return(_privateLabelID); }
		}

		public int DomainID
		{
			get { return(_domainID); }
		}

		public int DefaultCurrencyID
		{
			get { return(_defaultCurrencyID); }
		}

		public string URI
		{
			get { return(_URI); }
		}

		public string Description
		{
			get { return(_description); }
		}

		public int LanguageID
		{
			get { return(_languageID); }
		}

		public int StatusMask
		{
			get { return(_statusMask); }
		}

		public int LCID
		{
			get { return(_LCID); }
		}

		public int DefaultRegionID
		{
			get { return(_defaultRegionID); }
		}

		public int DisableFlag
		{
			get { return(_disableFlag); }
		}

		public string CSSPath
		{
			get { return(_CSSPath); }
		}

		public string CharSet
		{
			get { return(_charSet); }
		}

		public string RAlign
		{
			get { return(_RAlign); }
		}

		public string LAlign
		{
			get { return(_LAlign); }
		}

		public string Direction
		{
			get { return(_direction); }
		}

		public string ColorLightest
		{
			get { return(_colorLightest); }
		}

		public string ColorLighter
		{
			get { return(_colorLighter); }
		}

		public string ColorLight
		{
			get { return(_colorLight); }
		}

		public string ColorMedium
		{
			get { return(_colorMedium); }
		}

		public string ColorDark
		{
			get { return(_colorDark); }
		}

		public string ColorDarker
		{
			get { return(_colorDarker); }
		}

		public string ColorDarkest
		{
			get { return(_colorDarkest); }
		}

		public string BodyProperties
		{
			get { return(_bodyProperties); }
		}

		public string ImgUrl
		{
			get { return(_imgUrl); }
		}

		public int RegistrationStepCount
		{
			get { return(_registrationStepCount); }
		}

		public int DefaultAgeMin
		{
			get { return(_defaultAgeMin); }
		}

		public int DefaultAgeMax
		{
			get { return(_defaultAgeMax); }
		}

		public string ChatDNS
		{
			get { return(_chatDNS); }
		}

		public string ChatServer
		{
			get { return(_chatServer); }
		}

		public int ChatPort
		{
			get { return(_chatPort); }
		}

		public int ChatAppletVersion
		{
			get { return(_chatAppletVersion); }
		}

		public int CheckImVersion
		{
			get { return(_checkImVersion); }
		}

		public int GMTOffset
		{
			get { return(_GMTOffset); }
		}

		public int BasePrivateLabelID
		{
			get { return(_basePrivateLabelID); }
		}

		public bool RedirectToBase
		{
			get { return(_redirectToBase); }
		}

		public string DefaultHost
		{
			get { return(_defaultHost); }
		}

		public string SSLURL
		{
			get { return(_SSLURL); }
		}

		public int DefaultSearchTypeID
		{
			get { return(_defaultSearchTypeID); }
		}

		public int SearchTypeMask
		{
			get { return(_searchTypeMask); }
		}

		public int DefaultSearchRadius
		{
			get { return(_defaultSearchRadius); }
		}

		public int PaymentTypeMask
		{
			get { return(_paymentTypeMask); }
		}

		public string PhoneNumber
		{
			get { return(_phoneNumber); }
		}
	}
}
