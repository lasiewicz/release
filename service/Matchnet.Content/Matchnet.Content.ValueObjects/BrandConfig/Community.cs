using System;

namespace Matchnet.Content.ValueObjects.BrandConfig
{
	/// <summary>
	/// Represents a Community - a grouping of users that can interact with each other
	/// </summary>
	[Serializable]
	public class Community
	{
		private Int32 _communityID;
		private String _name;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="name"></param>
		public Community(Int32 communityID, String name)
		{
			_communityID = communityID;
			_name = name;
		}


		/// <summary>
		/// Community uniquer identifier
		/// </summary>
		public Int32 CommunityID
		{
			get
			{
				return _communityID;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public String Name
		{
			get
			{
				return _name;
			}
		}

	}
}
