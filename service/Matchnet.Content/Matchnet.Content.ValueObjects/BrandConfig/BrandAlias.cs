using System;

namespace Matchnet.Content.ValueObjects.BrandConfig
{
	/// <summary>
	/// Represents a url that is an alias for a Brand. Users who visit a BrandAlias url will be redirected to the underlying Brand.
	/// </summary>
	[Serializable]
	public class BrandAlias : IValueObject
	{
		private Int32 _brandAliasID;
		private Int32 _brandID;
		private String _uri; 
		private String _redirectParameters;
							 
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandAliasID"></param>
		/// <param name="brandID"></param>
		/// <param name="uri"></param>
		/// <param name="redirectParameters"></param>
		public BrandAlias(Int32 brandAliasID,
			Int32 brandID,
			String uri,
			String redirectParameters)
		{
			_brandAliasID = brandAliasID;
			_brandID = brandID;
			_uri = uri.ToLower();
			_redirectParameters = redirectParameters;
		}

		
		/// <summary>
		/// 
		/// </summary>
		public Int32 BrandAliasID
		{
			get
			{
				return _brandAliasID;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 BrandID
		{
			get
			{
				return _brandID;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public String Uri
		{
			get
			{
				return _uri;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public String RedirectParameters
		{
			get
			{
				return _redirectParameters;
			}
		}
		}
}
