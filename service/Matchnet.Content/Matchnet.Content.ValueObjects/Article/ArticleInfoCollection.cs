using System;
using System.Collections;
using System.Text;

namespace Matchnet.Content.ValueObjects.Article
{
	/// <summary>
	/// Summary description for ArticleInfoCollection.
	/// </summary>
	[Serializable]
	public class ArticleInfoCollection : System.Collections.CollectionBase, IValueObject, ICacheable
	{

		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		private int _categoryID;
		private int _translationID;
		private int _startPage;
		private int _pageSize;
		#endregion

		/// <summary>
		/// Creates a new ArticleInfoCollection that is ready to be cached
		/// </summary>
		/// <param name="categoryID"></param>
		/// <param name="translationID"></param>
		/// <param name="startPage"></param>
		/// <param name="pageSize"></param>
		public ArticleInfoCollection(int categoryID, int translationID, int startPage, int pageSize)
		{
			_categoryID = categoryID;
			_translationID = translationID;
			_startPage = startPage;
			_pageSize = pageSize;
		}

		/// <summary>
		/// 
		/// </summary>
		public ArticleInfo this[int index]
		{
			get { return ((ArticleInfo)base.InnerList[index]); }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="articleInfo"></param>
		/// <returns></returns>
		public int Add(ArticleInfo articleInfo)
		{
			return base.InnerList.Add(articleInfo);
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Absolute;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}

		#endregion
	}
}
