using System;

namespace Matchnet.Content.ValueObjects.Article
{
	/// <summary>
	/// TODO:  One day when we have a lot of time we should restructure this and the SparkNetworks.Administration Tool so that
	/// SiteCategory can inherit from Category.
	/// Summary description for SiteCategory.
	/// </summary>
	[Serializable]
	public class SiteCategory : Matchnet.IValueObject, Matchnet.ICacheable
	{
		#region Variables and Constants
		private const string SITECATEGORY_CACHE_KEY_PREFIX = "~SITECATEGORY^{0}{1}";

		private int _cacheTTLSeconds;
		private CacheItemPriorityLevel _cachePriority;
		private string _cacheKey;

		private int _siteCategoryID = int.MinValue;
		private int _siteID = int.MinValue;
		private int _categoryID;
		private string _content;
		private DateTime _lastUpdated;
		private bool _publishedFlag;
		private int _publishID = Constants.NULL_INT;
		private Category _category = new Category();
		#endregion

		#region Constructors
		public SiteCategory()
		{}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="categoryID"></param>
		/// <param name="parentCategoryID"></param>
		/// <param name="content"></param>
		/// <param name="listOrder"></param>
		public SiteCategory(int categoryID, int parentCategoryID, string content, int listOrder)
		{
			_categoryID = categoryID;
			_content = content;
			_category.ListOrder = listOrder;
			_category.ParentCategoryID = parentCategoryID;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteCategoryID"></param>
		/// <param name="categoryID"></param>
		///	<param name="siteID"></param>
		/// <param name="parentCategoryID"></param>
		/// <param name="content"></param>
		/// <param name="constant"></param>
		/// <param name="publishedFlag"></param>
		/// <param name="lastUpdated"></param>
		public SiteCategory(int siteCategoryID, int categoryID, int siteID, int parentCategoryID, string content, string constant, bool publishedFlag, DateTime lastUpdated)
		{
			_siteCategoryID = siteCategoryID;
			_categoryID = categoryID;
			_siteID = siteID;
			_content = content;
			_lastUpdated = lastUpdated;
			_category.ParentCategoryID = parentCategoryID;
			_category.Constant = constant;
			_publishedFlag = publishedFlag;
		}

		public SiteCategory(int siteCategoryID, int categoryID, int siteID, int parentCategoryID, string content, string constant, bool publishedFlag, DateTime lastUpdated, int publishID)
		{
			_siteCategoryID = siteCategoryID;
			_categoryID = categoryID;
			_siteID = siteID;
			_content = content;
			_lastUpdated = lastUpdated;
			_category.ParentCategoryID = parentCategoryID;
			_category.Constant = constant;
			_publishedFlag = publishedFlag;
			_publishID = publishID;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteCategoryID"></param>
		/// <param name="categoryID"></param>
		/// <param name="siteID"></param>
		/// <param name="content"></param>
		/// <param name="publishedFlag"></param>
		public SiteCategory(int siteCategoryID, int categoryID, int siteID, string content, bool publishedFlag)
		{
			_siteCategoryID = siteCategoryID;
			_categoryID = categoryID;
			_siteID = siteID;
			_content = content;
			_publishedFlag = publishedFlag;
		}
		#endregion

		#region Properties
		/// <summary>
		/// 
		/// </summary>
		public int SiteCategoryID
		{
			get { return(_siteCategoryID); }
			set { _siteCategoryID = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public int CategoryID
		{
			get { return(_categoryID); }
			set { _categoryID = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public int SiteID
		{
			get { return(_siteID); }
			set { _siteID = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public string Content
		{
			get { return(_content); }
			set { _content = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime LastUpdated
		{
			get { return(_lastUpdated); }
		}

		/// <summary>
		/// 
		/// </summary>
		public bool PublishedFlag
		{
			get { return(_publishedFlag); }
			set { _publishedFlag = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public Category CategoryData
		{
			get { return(_category); }
		}

		/// <summary>
		/// 
		/// </summary>
		public int PublishID
		{
			get
			{
				return _publishID;
			}
			set
			{
				_publishID = value;
			}
		}

		public String Constant
		{
			get
			{
				return _category.Constant;
			}
		}

		public Int32 ParentCategoryID
		{
			get
			{
				return _category.ParentCategoryID;
			}
		}
		#endregion

		#region ICacheable Members
		/// <summary>
		/// 
		/// </summary>
		/// <param name="CategoryID"></param>
		/// <param name="SiteID"></param>
		/// <returns></returns>
		public static string GetCacheKey(int CategoryID, int SiteID)
		{		
			return string.Format(SITECATEGORY_CACHE_KEY_PREFIX, CategoryID, SiteID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return SiteCategory.GetCacheKey(_categoryID, _siteID);
		}

		/// <summary>
		/// 
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_cacheKey = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Absolute;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}
		#endregion
	}
}
