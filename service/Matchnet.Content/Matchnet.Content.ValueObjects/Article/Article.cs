using System;
using System.Text;

namespace Matchnet.Content.ValueObjects.Article
{
	/// <summary>
	/// Article holds content and metadata for Articles, which are FAQs, site articles (including Love Connections and Singles Talk), and help content present on the MatchNet sites.
	/// </summary>
	[Serializable]
	public class Article : Matchnet.IValueObject
	{	
		#region Variables and Constants
		private int _articleID = int.MinValue;
		private int _categoryID;
		private string _constant;
		private string _defaultContent;
		private DateTime _lastUpdated;
		private int _publishID = Constants.NULL_INT;
		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="articleID"></param>
		/// <param name="categoryID"></param>
		public Article(int articleID, int categoryID)
		{
			_articleID = articleID;
			_categoryID = categoryID;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="articleID"></param>
		/// <param name="categoryID"></param>
		/// <param name="defaultContent"></param>
		/// <param name="constant"></param>
		/// <param name="lastUpdated"></param>
		public Article(int articleID, int categoryID, string defaultContent, string constant, DateTime lastUpdated)
		{
			_articleID = articleID;
			_categoryID = categoryID;
			_defaultContent = defaultContent;
			_constant = constant;
			_lastUpdated = lastUpdated;
		}

		public Article(int articleID, int categoryID, string defaultContent, string constant, DateTime lastUpdated, int publishID)
		{
			_articleID = articleID;
			_categoryID = categoryID;
			_defaultContent = defaultContent;
			_constant = constant;
			_lastUpdated = lastUpdated;
			_publishID = publishID;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="articleID"></param>
		/// <param name="categoryID"></param>
		/// <param name="defaultContent"></param>
		/// <param name="constant"></param>
		public Article(int articleID, int categoryID, string defaultContent, string constant)
		{
			_articleID = articleID;
			_categoryID = categoryID;
			_defaultContent = defaultContent;
			_constant = constant;
		}

		/// <summary>
		/// Used by the Admin tool when adding a new SiteArticle.
		/// </summary>
		/// <param name="categoryID"></param>
		public Article(int categoryID)
		{
			_categoryID = categoryID;
		}

		/// <summary>
		/// 
		/// </summary>
		public Article()
		{}

		/// <summary>
		/// The unique database identifier of the article.
		/// </summary>
		public int ArticleID
		{
			get { return(_articleID); }
			set { _articleID = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public int CategoryID
		{
			get { return(_categoryID); }
			set { _categoryID = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public string Constant
		{
			get { return(_constant); }
			set { _constant = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public string DefaultContent
		{
			get { return(_defaultContent); }
			set { _defaultContent = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime LastUpdated
		{
			get { return(_lastUpdated); }
		}

		/// <summary>
		/// 
		/// </summary>
		public int PublishID
		{
			get
			{
				return _publishID;
			}
			set
			{
				_publishID = value;
			}
		}
	}
}
