using System;
using System.Text;

using Matchnet.CacheSynchronization.Tracking;

namespace Matchnet.Content.ValueObjects.Article
{
	/// <summary>
	/// TODO:  One day when we have a lot of time we should restructure this and the SparkNetworks.Administration Tool so that
	/// SiteArticle can inherit from Article.
	/// SiteArticle holds content and metadata for SiteArticles, which are FAQs, site sitearticles (including Love Connections and Singles Talk), and help content present on the MatchNet sites.
	/// </summary>
	[Serializable]
	public class SiteArticle : Matchnet.IValueObject, Matchnet.ICacheable, IReplicable
	{	
		#region Variables and Constants
		private const string SITEARTICLE_CACHE_KEY_PREFIX = "~SITEARTICLE^{0}{1}";

		private int _cacheTTLSeconds;
		private CacheItemPriorityLevel _cachePriority;
		private string _cacheKey;

		private int _siteArticleID = int.MinValue;
		private int _siteID;
		private Int32 _memberID;
		private int _ordinal;
		private DateTime _lastUpdated;
		private bool _publishedFlag;
		private bool _featuredFlag;
		private Int32 _fileID;
		private string _title;
		private string _content;
		private int _articleID;
		private int _publishID = Constants.NULL_INT;
		private string _pageTitle;
		private string _metaDescription;
		private Article _article = new Article();

		[NonSerialized]
		private ReferenceTracker _referenceTracker;
		#endregion

		#region Constructors
		public SiteArticle()
		{}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteArticleID">The unique database identifier of the sitearticle.  QUESTION: Why does SiteArticle have an siteArticleID, and SiteArticleInfo have an sitearticleExID?</param>
		/// <param name="siteID">Represents the relationship between a language and a Site.  The siteID identifies which translated version of the sitearticle to display.</param>
		/// <param name="constant"></param>
		/// <param name="memberID">PRELIMINARY: The memberID of the Member (Admin) who created/entered the sitearticle.</param>
		/// <param name="ordinal">PRELIMINARY: The order in which the sitearticle should be displayed within the category page for this sitearticle.  For example, "Jody and Ken" have an ordinal of 2 and are displayed second on the "Love Connections" page.  There should never be ordinal conflicts -- this is ensured within the backend.</param>
		/// <param name="publishedFlag">Flag to identify whether or not the sitearticle is published -- ready for display to production sites.  Values: 0=no, 1=yes.</param>
		/// <param name="featuredFlag">PRELIMINARY: Flag to identify whether or not the sitearticle is featured.  Featured sitearticles will appear in on the home page in full or truncated, with a link to additional sitearticle text or pages.  0=no, 1=yes.  There should never be multiple "featured" sitearticles for a given category -- this is ensured within the backend.</param>
		/// <param name="fileID">QUESTION: Why have a fileID when we already have the content in this object?</param>
		/// <param name="title">The title of the sitearticle.</param>
		/// <param name="content">The content (copy) of the sitearticle.</param>
		///	<param name="articleID"></param>
		///	<param name="lastUpdated"></param>
		public SiteArticle(int siteArticleID, int articleID, int siteID, string constant, Int32 memberID, int ordinal, DateTime lastUpdated, bool publishedFlag, bool featuredFlag, Int32 fileID, string title, string content)
		{
			_siteArticleID = siteArticleID;
			_articleID = articleID;
			_siteID = siteID;
			_article.Constant = constant;
			_memberID = memberID;
			_ordinal = ordinal;
			_lastUpdated = lastUpdated;
			_publishedFlag = publishedFlag;
			_featuredFlag = featuredFlag;
			_fileID = fileID;
			_title = title;
			_content = content;
		}

		public SiteArticle(int siteArticleID, int articleID, int siteID, string constant, Int32 memberID, int ordinal, DateTime lastUpdated, bool publishedFlag, bool featuredFlag, Int32 fileID, string title, string content, int publishID)
		{
			_siteArticleID = siteArticleID;
			_articleID = articleID;
			_siteID = siteID;
			_article.Constant = constant;
			_memberID = memberID;
			_ordinal = ordinal;
			_lastUpdated = lastUpdated;
			_publishedFlag = publishedFlag;
			_featuredFlag = featuredFlag;
			_fileID = fileID;
			_title = title;
			_content = content;
			_publishID = publishID;
		}

		public SiteArticle(int siteArticleID, int articleID, int siteID, string constant, Int32 memberID, int ordinal, DateTime lastUpdated, bool publishedFlag, bool featuredFlag, Int32 fileID, string title, string content, int publishID, string pageTitle, string metaDescription)
		{
			_siteArticleID = siteArticleID;
			_articleID = articleID;
			_siteID = siteID;
			_article.Constant = constant;
			_memberID = memberID;
			_ordinal = ordinal;
			_lastUpdated = lastUpdated;
			_publishedFlag = publishedFlag;
			_featuredFlag = featuredFlag;
			_fileID = fileID;
			_title = title;
			_content = content;
			_publishID = publishID;
			_pageTitle = pageTitle;
			_metaDescription = metaDescription;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteArticleID"></param>
		/// <param name="articleID"></param>
		/// <param name="siteID"></param>
		/// <param name="memberID"></param>
		/// <param name="ordinal"></param>
		/// <param name="publishedFlag"></param>
		/// <param name="featuredFlag"></param>
		/// <param name="fileID"></param>
		/// <param name="title"></param>
		/// <param name="content"></param>
		public SiteArticle(int siteArticleID, int articleID, int siteID, Int32 memberID, int ordinal, bool publishedFlag, bool featuredFlag, Int32 fileID, string title, string content)
		{
			_siteArticleID = siteArticleID;
			_articleID = articleID;
			_siteID = siteID;
			_memberID = memberID;
			_ordinal = ordinal;
			_publishedFlag = publishedFlag;
			_featuredFlag = featuredFlag;
			_fileID = fileID;
			_title = title;
			_content = content;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="articleID"></param>
		/// <param name="title"></param>
		/// <param name="content"></param>
		/// <param name="fileID"></param>
		/// <param name="ordinal"></param>
		/// <param name="featuredFlag"></param>
		public SiteArticle(int articleID, string title, string content, int fileID, int ordinal, bool featuredFlag)
		{
			_articleID = articleID;
			_title = title;
			_content = content;
			_fileID = fileID;
			_ordinal = ordinal;
			_featuredFlag = featuredFlag;
		}

		public SiteArticle(int articleID, string title, string content, int fileID, int ordinal, bool featuredFlag, int publishID)
		{
			_articleID = articleID;
			_title = title;
			_content = content;
			_fileID = fileID;
			_ordinal = ordinal;
			_featuredFlag = featuredFlag;
			_publishID = publishID;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteArticleID"></param>
		/// <param name="title"></param>
		/// <param name="memberID"></param>
		/// <param name="fileID"></param>
		public SiteArticle(int siteArticleID, string title, int memberID, int fileID)
		{
			_siteArticleID = siteArticleID;
			_title = title;
			_memberID = memberID;
			_fileID = fileID;
		}

		/// <summary>
		/// Used by the Admin tool when adding a new SiteArticle.
		/// </summary>
		/// <param name="articleID"></param>
		/// <param name="siteID"></param>
		/// <param name="memberID"></param>
		public SiteArticle(int articleID, int siteID, int memberID)
		{
			_articleID = articleID;
			_siteID = siteID;
			_memberID = memberID;
		}
		#endregion

		#region Properties
		/// <summary>
		/// The unique database identifier of the sitearticle.
		/// </summary>
		public int SiteArticleID
		{
			get
			{
				return(_siteArticleID);
			}
			set
			{
				_siteArticleID = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int ArticleID
		{
			get
			{
				return(_articleID);
			}
			set
			{
				_articleID = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int SiteID
		{
			get
			{
				return(_siteID);
			}
			set
			{
				_siteID = value;
			}
		}

		/// <summary>
		/// PRELIMINARY: The memberID of the Member who created/entered the sitearticle.
		/// </summary>
		public Int32 MemberID
		{
			get
			{
				return(_memberID);
			}
			set
			{
				_memberID = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime LastUpdated
		{
			get
			{
				return(_lastUpdated);
			}
		}

		/// <summary>
		/// PRELIMINARY: The order in which the sitearticle should be displayed within the category page for this sitearticle.  For example, "Jody and Ken" have an ordinal of 2 and are displayed second on the "Love Connections" page.  There should never be ordinal conflicts -- this is ensured within the backend.
		/// </summary>
		public int Ordinal
		{
			get
			{
				return(_ordinal);
			}
			set
			{
				_ordinal = value;
			}
		}

		/// <summary>
		/// Flag to identify whether or not the sitearticle is published -- ready for display to production sites.  Values: 0=no, 1=yes.
		/// </summary>
		public bool PublishedFlag
		{
			get
			{
				return(_publishedFlag);
			}
			set
			{
				_publishedFlag = value;
			}
		}

		/// <summary>
		/// PRELIMINARY: Flag to identify whether or not the sitearticle is featured.  Featured sitearticles will appear in on the home page in full or truncated, with a link to additional sitearticle text or pages.  0=no, 1=yes.  There should never be multiple "featured" sitearticles for a given category -- this is ensured within the backend.
		/// </summary>
		public bool FeaturedFlag
		{
			get
			{
				return(_featuredFlag);
			}
			set
			{
				_featuredFlag = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 FileID
		{
			get
			{
				return(_fileID);
			}
			set
			{
				_fileID = value;
			}
		}

		/// <summary>
		/// The title of the sitearticle.
		/// </summary>
		public string Title
		{
			get
			{
				return(_title);
			}
			set
			{
				_title = value;
			}
		}

		/// <summary>
		/// The content (copy) of the sitearticle.
		/// </summary>
		public string Content
		{
			get
			{
				return(_content);
			}
			set
			{
				_content = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Article ArticleData
		{
			get { return(_article); }
		}

		/// <summary>
		/// 
		/// </summary>
		public int PublishID
		{
			get
			{
				return _publishID;
			}
			set
			{
				_publishID = value;
			}
		}

		public String Constant
		{
			get
			{
				return _article.Constant;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public ReferenceTracker ReferenceTracker
		{
			get
			{
				if (_referenceTracker == null)
				{
					_referenceTracker = new ReferenceTracker();
				}

				return _referenceTracker;
			}
		}

		/// <summary>
		/// The html page title - for SEO
		/// </summary>
		public string PageTitle
		{
			get
			{
				return(_pageTitle);
			}
			set
			{
				_pageTitle = value;
			}
		}

		/// <summary>
		/// The html META description - for SEO
		/// </summary>
		public string MetaDescription
		{
			get
			{
				return(_metaDescription);
			}
			set
			{
				_metaDescription = value;
			}
		}

		#endregion
		
		#region ICacheable Members
		/// <summary>
		/// 
		/// </summary>
		/// <param name="ArticleID"></param>
		/// <param name="SiteID"></param>
		/// <returns></returns>
		public static string GetCacheKey(int ArticleID, int SiteID)
		{		
			return string.Format(SITEARTICLE_CACHE_KEY_PREFIX, ArticleID, SiteID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return SiteArticle.GetCacheKey(_articleID, _siteID);
		}

		/// <summary>
		/// 
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_cacheKey = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Absolute;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}
		#endregion

		#region IReplicable Members
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}
		#endregion
	}
}
