using System;
using System.Collections;

namespace Matchnet.Content.ValueObjects.Article
{
	[Serializable]
	public class WSOItemCollection : CollectionBase, IValueObject
	{
		public WSOItemCollection()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		public WSOItem this[int index]
		{
			get { return ((WSOItem)base.InnerList[index]); }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteArticle"></param>
		/// <returns></returns>
		public int Add(WSOItem WSOItem)
		{
			return base.InnerList.Add(WSOItem);
		}
	}
}
