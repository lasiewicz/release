using System;

namespace Matchnet.Content.ValueObjects.Article
{
	/// <summary>
	/// Summary description for CategoryParent.
	/// </summary>
	[Serializable]
	public class Category : Matchnet.IValueObject
	{
		#region Variables and Constants
		private int _categoryID = int.MinValue;
		private int _parentCategoryID = int.MinValue;
		private int _listOrder;
		private string _constant;
		private DateTime _lastUpdated;
		private int _publishID = Constants.NULL_INT;
		#endregion

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		/// <param name="categoryID"></param>
		/// <param name="parentCategoryID"></param>
		/// <param name="listOrder"></param>
		/// <param name="constant"></param>
		/// <param name="lastUpdated"></param>
		public Category(int categoryID, int parentCategoryID, int listOrder, string constant, DateTime lastUpdated)
		{
			_categoryID = categoryID;
			_parentCategoryID = parentCategoryID;
			_listOrder = listOrder;
			_constant = constant;
			_lastUpdated = lastUpdated;
		}

		public Category(int categoryID, int parentCategoryID, int listOrder, string constant, DateTime lastUpdated, int publishID)
		{
			_categoryID = categoryID;
			_parentCategoryID = parentCategoryID;
			_listOrder = listOrder;
			_constant = constant;
			_lastUpdated = lastUpdated;
			_publishID = publishID;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="categoryID"></param>
		/// <param name="parentCategoryID"></param>
		/// <param name="listOrder"></param>
		/// <param name="constant"></param>
		public Category(int categoryID, int parentCategoryID, int listOrder, string constant)
		{
			_categoryID = categoryID;
			_parentCategoryID = parentCategoryID;
			_listOrder = listOrder;
			_constant = constant;
		}

		/// <summary>
		/// 
		/// </summary>
		public Category()
		{}
		#endregion

		#region Properties
		/// <summary>
		/// 
		/// </summary>
		public int CategoryID
		{
			get { return(_categoryID); }
			set { _categoryID = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public int ParentCategoryID
		{
			get { return(_parentCategoryID); }
			set { _parentCategoryID = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public int ListOrder
		{
			get { return(_listOrder); }
			set { _listOrder = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public string Constant
		{
			get { return(_constant); }
			set { _constant = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime LastUpdated
		{
			get { return(_lastUpdated); }
		}

		/// <summary>
		/// 
		/// </summary>
		public int PublishID
		{
			get
			{
				return _publishID;
			}
			set
			{
				_publishID = value;
			}
		}
		#endregion
	}
}
