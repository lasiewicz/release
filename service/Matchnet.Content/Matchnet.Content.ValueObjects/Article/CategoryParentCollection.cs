using System;
using System.Collections;
using System.Text;

namespace Matchnet.Content.ValueObjects.Article
{
	/// <summary>
	/// Summary description for ArticleInfoCollection.
	/// </summary>
	public class CategoryParentCollection : System.Collections.CollectionBase, IValueObject, ICacheable
	{
		private const string CACHEKEY_PREFIX = "Article:Categories:";

		private int _categoryID;
		private int _translationID;

		public CategoryParentCollection(int categoryID, int translationID)
		{
			_categoryID = categoryID;
			_translationID = translationID;
		}

		public CategoryParent this[int index]
		{
			get { return ((CategoryParent)base.InnerList[index]); }
		}

		public int Add(CategoryParent categoryParent)
		{
			return base.InnerList.Add(categoryParent);
		}
		#region ICacheable Members

		public int CacheTTLSeconds
		{
			get
			{
				// TODO:  Add ArticleInfoCollection.CacheTTLSeconds getter implementation
				return 0;
			}
			set
			{
				// TODO:  Add ArticleInfoCollection.CacheTTLSeconds setter implementation
			}
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				// TODO:  Add ArticleInfoCollection.CacheMode getter implementation
				return new Matchnet.CacheItemMode ();
			}
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				// TODO:  Add ArticleInfoCollection.CachePriority getter implementation
				return new Matchnet.CacheItemPriorityLevel ();
			}
			set
			{
				// TODO:  Add ArticleInfoCollection.CachePriority setter implementation
			}
		}

		public string GetCacheKey()
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(CACHEKEY_PREFIX);
			sb.Append("ParentCollection:");
			sb.Append("CagegoryID=");
			sb.Append(_categoryID);
			sb.Append(":TranslationID=");
			sb.Append(_translationID);
			
			string retVal = sb.ToString();
			
			return(retVal);
		}

		#endregion
	}
}
