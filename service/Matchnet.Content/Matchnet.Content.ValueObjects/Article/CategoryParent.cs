using System;

namespace Matchnet.Content.ValueObjects.Article
{
	/// <summary>
	/// Summary description for CategoryParent.
	/// </summary>
	public class CategoryParent
	{
		private int _categoryID;
		private int _translationID;
		private int _categoryExID;
		private int _parentCategoryExID;
		private string _content;

		public CategoryParent(int categoryID, int translationID, int categoryExID, int parentCategoryExID, string content)
		{
			_categoryID = categoryID;
			_translationID = translationID;
			_categoryExID = categoryExID;
			_parentCategoryExID = parentCategoryExID;
			_content = content;
		}

		public int CategoryID
		{
			get { return(_categoryID); }
		}

		public int TranslationID
		{
			get { return(_translationID); }
		}

		public int CategoryExID
		{
			get { return(_categoryExID); }
		}

		public int ParentCategoryExID
		{
			get { return(_parentCategoryExID); }
		}

		public string Content
		{
			get { return(_content); }
		}
	}
}
