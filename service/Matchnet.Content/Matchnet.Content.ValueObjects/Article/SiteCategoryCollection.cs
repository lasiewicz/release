using System;
using System.Collections;
using System.Text;

using Matchnet.CacheSynchronization.Tracking;
using Matchnet;

namespace Matchnet.Content.ValueObjects.Article
{
	/// <summary>
	/// Summary description for ArticleInfoCollection.
	/// </summary>
	[Serializable]
	public class SiteCategoryCollection : System.Collections.CollectionBase, Matchnet.IValueObject, Matchnet.ICacheable, Matchnet.IReplicable
	{

		#region Variables and Constants
		private const string SITECATEGORIES_CACHE_KEY_PREFIX = "~SITECATEGORIES^{0}{1}{2}";

		private int _cacheTTLSeconds;
		private CacheItemPriorityLevel _cachePriority;
		private string _cacheKey;

		private int _categoryID = int.MinValue;
		private int _siteID;

		private Hashtable _siteCategories = new Hashtable();

		[NonSerialized]
		private ReferenceTracker _referenceTracker;
		#endregion

		#region Contructors
		/// <summary>
		/// 
		/// </summary>
		/// <param name="categoryID"></param>
		/// <param name="siteID"></param>
		public SiteCategoryCollection(int categoryID, int siteID)
		{
			_categoryID = categoryID;
			_siteID = siteID;
		}
		#endregion
		/// <summary>
		/// 
		/// </summary>
		public SiteCategory this[int index]
		{
			get { return ((SiteCategory)base.InnerList[index]); }
		}

		/// <summary>
		/// Retrieve a SiteCategory by CategoryID
		/// </summary>
		/// <param name="categoryID"></param>
		/// <returns></returns>
		public SiteCategory GetSiteCategory(int categoryID)
		{
			return (SiteCategory) _siteCategories[categoryID];
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteCategory"></param>
		/// <returns></returns>
		public int Add(SiteCategory siteCategory)
		{
			_siteCategories.Add(siteCategory.CategoryID, siteCategory);

			return base.InnerList.Add(siteCategory);
		}

		/// <summary>
		/// 
		/// </summary>
		public ReferenceTracker ReferenceTracker
		{
			get
			{
				if (_referenceTracker == null)
				{
					_referenceTracker = new ReferenceTracker();
				}

				return _referenceTracker;
			}
		}

		#region ICacheable Members
		/// <summary>
		/// DEPRECATED.  LEFT FOR BACKWARDS COMPATIBILITY.
		/// </summary>
		/// <param name="SiteID"></param>
		/// <param name="CategoryID"></param>
		/// <param name="CacheKeySuffix"></param>
		/// <returns></returns>
		public static string GetCacheKey(int SiteID, int CategoryID, string CacheKeySuffix)
		{		
			return string.Format(SITECATEGORIES_CACHE_KEY_PREFIX, SiteID, CategoryID, CacheKeySuffix);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="SiteID"></param>
		/// <param name="CategoryID"></param>
		/// <param name="CacheKeySuffix"></param>
		/// <returns></returns>
		public static string GetCacheKey(int siteID, int categoryID, SiteCategoryCollectionCacheKeySuffix cacheKeySuffix)
		{		
			return string.Format(SITECATEGORIES_CACHE_KEY_PREFIX, siteID, categoryID, cacheKeySuffix.ToString());
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{		
			return SiteCategoryCollection.GetCacheKey(_siteID, _categoryID, SiteCategoryCollectionCacheKeySuffix.All);
		}

		/// <summary>
		/// 
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_cacheKey = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Absolute;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}
		#endregion

		#region IReplicable Members
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}
		#endregion
	}

	/// <summary>
	/// 
	/// </summary>
	public enum SiteCategoryCollectionCacheKeySuffix
	{
		Children,
		All
	}
}
