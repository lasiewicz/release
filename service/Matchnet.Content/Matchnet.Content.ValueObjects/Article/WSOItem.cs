using System;

namespace Matchnet.Content.ValueObjects.Article
{
	[Serializable]
	public class WSOItem
	{
		private int wsoItemID = Constants.NULL_INT;
		private int wsoListID = Constants.NULL_INT;
		private int itemID = Constants.NULL_INT;
		private int siteID = Constants.NULL_INT;
		private int itemTypeID = Constants.NULL_INT;
		private DateTime insertDateTime = DateTime.MinValue;
		private DateTime updateDateTime = DateTime.MinValue;

		public int WSOItemID
		{
			get
			{
				return wsoItemID;
			}
			set
			{
				wsoItemID = value;
			}
		}
		
		public int WSOListID
		{
			get
			{
				return wsoListID;
			}
			set
			{
				wsoListID = value;
			}
		}

		public int ItemID
		{
			get
			{
				return itemID;
			}
			set
			{
				itemID = value;
			}
		}

		public int ItemTypeID
		{
			get
			{
				return itemTypeID;
			}
			set
			{
				itemTypeID = value;
			}
		}

		public int SiteID
		{
			get
			{
				return siteID;
			}
			set
			{
				siteID = value;
			}
		}

		public DateTime InserDateTime
		{
			get
			{
				return insertDateTime;
			}
			set
			{
				insertDateTime= value;
			}
		}
		public DateTime UpdateDateTime
		{
			get
			{
				return updateDateTime;
			}
			set
			{
				updateDateTime = value;
			}
		}


		public WSOItem()
		{
		}
		public WSOItem(int wsoItemID, int wsoListID, int siteID, int itemID, int itemTypeID, DateTime insertDateTime, DateTime updateDateTime)
		{
			this.wsoItemID = wsoItemID;
			this.wsoListID = wsoListID;
			this.siteID = siteID;
			this.itemID = itemID;
			this.itemTypeID = itemTypeID;
			this.insertDateTime = insertDateTime;
			this.updateDateTime = updateDateTime;
		}

		public WSOItem(int wsoItemID, int wsoListID, int siteID, int itemID, int itemTypeID)
		{
			this.wsoItemID = wsoItemID;
			this.wsoListID = wsoListID;
			this.siteID = siteID;
			this.itemID = itemID;
			this.itemTypeID = itemTypeID;
		}
	}
}
