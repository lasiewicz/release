using System;

namespace Matchnet.Content.ValueObjects.Article
{
	[Serializable]
	public class WSOListCollection : System.Collections.CollectionBase, Matchnet.IValueObject
	{
		public WSOListCollection()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		public WSOList this[int index]
		{
			get { return ((WSOList)base.InnerList[index]); }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteArticle"></param>
		/// <returns></returns>
		public int Add(WSOList wsoList)
		{
			return base.InnerList.Add(wsoList);
		}
	}
}
