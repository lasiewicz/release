using System;

namespace Matchnet.Content.ValueObjects.Article
{
	[Serializable]
	public class WSOList : Matchnet.IValueObject
	{
		private int wsoListID = Constants.NULL_INT;
		private string name = Constants.NULL_STRING;
		private string note = Constants.NULL_STRING;
		private DateTime insertDateTime = DateTime.MinValue;
		private DateTime updateDateTime = DateTime.MinValue;

		public int WSOListID
		{
			get
			{
				return wsoListID;
			}
			set
			{
				wsoListID = value;
			}
		}

		public string Name
		{
			get
			{
				return name;
			}
			set
			{
				name = value;
			}
		}
		public string Note
		{
			get
			{
				return note;
			}
			set
			{
				note = value;
			}
		}

		public DateTime InsertDateTime
		{
			get
			{
				return insertDateTime;
			}
			set
			{
				insertDateTime = value;
			}
		}
		public DateTime UpdateDateTime
		{
			get
			{
				return updateDateTime;
			}
			set
			{
				updateDateTime =value;
			}
		}


		public WSOList()
		{
		}

		/// <summary>
		/// Used for populating from DB
		/// </summary>
		/// <param name="wsoListID"></param>
		/// <param name="name"></param>
		/// <param name="note"></param>
		/// <param name="insertDateTime"></param>
		/// <param name="updateDateTime"></param>
		public WSOList(int wsoListID, string name, string note, DateTime insertDateTime, DateTime updateDateTime) 
		{
			this.wsoListID = wsoListID;
			this.name = name;
			this.note = note;
			this.insertDateTime = insertDateTime;
			this.updateDateTime = updateDateTime;
		}

		/// <summary>
		/// Used for creating
		/// </summary>
		/// <param name="wsoListID"></param>
		/// <param name="note"></param>
		/// <param name="name"></param>
		public WSOList(int wsoListID, string name, string note) 
		{
			this.wsoListID = wsoListID;
			this.name = name;
			this.note = note;
		}

	}
}
