using System;
using System.Collections;
using System.Text;

namespace Matchnet.Content.ValueObjects.Article
{
	/// <summary>
	/// Summary description for ArticleInfoCollection.
	/// </summary>
	[Serializable]
	public class CategoryCollection : System.Collections.CollectionBase, Matchnet.IValueObject, Matchnet.ICacheable
	{
		#region Variables and Constants
		private const string CATEGORIES_CACHE_KEY_PREFIX = "~CATEGORIES^";

		private int _cacheTTLSeconds;
		private CacheItemPriorityLevel _cachePriority;
		private string _cacheKey;

		private int _categoryID;
		private int _siteID;
		#endregion

		#region Constructors
		public CategoryCollection(int categoryID, int siteID)
		{
			_categoryID = categoryID;
			_siteID = siteID;
		}
		#endregion

		/// <summary>
		/// 
		/// </summary>
		public Category this[int index]
		{
			get { return ((Category)base.InnerList[index]); }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="category"></param>
		/// <returns></returns>
		public int Add(Category category)
		{
			return base.InnerList.Add(category);
		}

		#region ICacheable Members
		public string GetCacheKey()
		{		
			return string.Format(CATEGORIES_CACHE_KEY_PREFIX);
		}

		public string SetCacheKey
		{
			set
			{
				_cacheKey = value;
			}
		}

		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Absolute;
			}
		}

		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}
		#endregion
	}
}
