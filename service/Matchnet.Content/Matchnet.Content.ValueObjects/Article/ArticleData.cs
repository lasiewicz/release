using System;
using System.Text;

namespace Matchnet.Content.ValueObjects.Article
{
	/// <summary>
	/// ArticleData holds content and metadata for Articles, which are FAQs, site articles (including Love Connections and Singles Talk), and help content present on the MatchNet sites.
	/// </summary>
	[Serializable]
	public class ArticleData: Matchnet.ICacheable
	{	
		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		private int _articleID;
		private int _translationID;
		private Int32 _memberID;
		private int _ordinal;
		private bool _publishedFlag;
		private bool _featuredFlag;
		private Int32 _fileID;
		private string _title;
		private string _content;
		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="articleID">The unique database identifier of the article.  QUESTION: Why does ArticleData have an articleID, and ArticleInfo have an articleExID?</param>
		/// <param name="translationID">Represents the relationship between a language and a Site.  The translationID identifies which translated version of the article to display.</param>
		/// <param name="memberID">PRELIMINARY: The memberID of the Member (Admin) who created/entered the article.</param>
		/// <param name="ordinal">PRELIMINARY: The order in which the article should be displayed within the category page for this article.  For example, "Jody and Ken" have an ordinal of 2 and are displayed second on the "Love Connections" page.  There should never be ordinal conflicts -- this is ensured within the backend.</param>
		/// <param name="publishedFlag">Flag to identify whether or not the article is published -- ready for display to production sites.  Values: 0=no, 1=yes.</param>
		/// <param name="featuredFlag">PRELIMINARY: Flag to identify whether or not the article is featured.  Featured articles will appear in on the home page in full or truncated, with a link to additional article text or pages.  0=no, 1=yes.  There should never be multiple "featured" articles for a given category -- this is ensured within the backend.</param>
		/// <param name="fileID">QUESTION: Why have a fileID when we already have the content in this object?</param>
		/// <param name="title">The title of the article.</param>
		/// <param name="content">The content (copy) of the article.</param>
		public ArticleData(int articleID, int translationID, Int32 memberID, int ordinal, bool publishedFlag, bool featuredFlag, Int32 fileID, string title, string content)
		{
			_articleID = articleID;
			_translationID = translationID;
			_memberID = memberID;
			_ordinal = ordinal;
			_publishedFlag = publishedFlag;
			_featuredFlag = featuredFlag;
			_fileID = fileID;
			_title = title;
			_content = content;
		}

		/// <summary>
		/// The unique database identifier of the article.
		/// </summary>
		public int ArticleID
		{
			get { return(_articleID); }
		}

		/// <summary>
		/// Represents the relationship between a language and a Site.  The translationID identifies which translated version of the article to display.
		/// </summary>
		public int TranslationID
		{
			get { return(_translationID); }
		}

		/// <summary>
		/// PRELIMINARY: The memberID of the Member who created/entered the article.
		/// </summary>
		public Int32 MemberID
		{
			get { return(_memberID); }
		}

		/// <summary>
		/// PRELIMINARY: The order in which the article should be displayed within the category page for this article.  For example, "Jody and Ken" have an ordinal of 2 and are displayed second on the "Love Connections" page.  There should never be ordinal conflicts -- this is ensured within the backend.
		/// </summary>
		public int Ordinal
		{
			get { return(_ordinal); }
		}

		/// <summary>
		/// Flag to identify whether or not the article is published -- ready for display to production sites.  Values: 0=no, 1=yes.
		/// </summary>
		public bool PublishedFlag
		{
			get { return(_publishedFlag); }
		}

		/// <summary>
		/// PRELIMINARY: Flag to identify whether or not the article is featured.  Featured articles will appear in on the home page in full or truncated, with a link to additional article text or pages.  0=no, 1=yes.  There should never be multiple "featured" articles for a given category -- this is ensured within the backend.
		/// </summary>
		public bool FeaturedFlag
		{
			get { return(_featuredFlag); }
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 FileID
		{
			get { return(_fileID); }
		}

		/// <summary>
		/// The title of the article.
		/// </summary>
		public string Title
		{
			get { return(_title); }
		}

		/// <summary>
		/// The content (copy) of the article.
		/// </summary>
		public string Content
		{
			get { return(_content); }
		}
		
		#region ICacheable Members
		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Absolute;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}
		#endregion
	}
}
