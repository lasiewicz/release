using System;

namespace Matchnet.Content.ValueObjects.Article
{
	/// <summary>
	/// PRELIMINARY: The ArticleInfo object holds the basic information that describes an article.  Unlike ArticleData, which has a content parameter and a 
	/// translationID for presenting the actual article, ArticleInfo maintains only the article's database identifier, title, memberID and fileID.
	/// </summary>
	/// <seealso cref="ArticleData">The ArticleData Value Object.</seealso>
	[Serializable]
	public class ArticleInfo
	{
		private Int32 _articleExID;
		private string _title;
		private Int32 _memberID;
		private Int32 _fileID;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="articleExID">QUESTION: Why does ArticleData have an articleID, and ArticleInfo have an articleExID?</param>
		/// <param name="title">The title of the article.</param>
		/// <param name="memberID">PRELIMINARY: The memberID of the Member (Admin) who created or posted the article.  </param>
		/// <param name="fileID">QUESTION: Why have a fileID?  I thought fileIDs were only for email texts and Member photos.</param>
		public ArticleInfo(Int32 articleExID, string title, Int32 memberID, Int32 fileID)
		{
			_articleExID = articleExID;
			_title = title;
			_memberID = memberID;
			_fileID = fileID;
		}

		/// <summary>
		/// QUESTION: Why does ArticleData have an articleID, and ArticleInfo have an articleExID?
		/// </summary>
		public Int32 ArticleExID
		{
			get { return(_articleExID); }
		}

		/// <summary>
		/// The title of the article.
		/// </summary>
		public string Title
		{
			get { return(_title); }
		}

		/// <summary>
		/// PRELIMINARY: The memberID of the Member (Admin) who created or posted the article.  
		/// </summary>
		public Int32 MemberID
		{
			get { return(_memberID); }
		}

		/// <summary>
		/// QUESTION: Why have a fileID?  I thought fileIDs were only for email texts and Member photos.
		/// </summary>
		public Int32 FileID
		{
			get { return(_fileID); }
		}
	}
}
