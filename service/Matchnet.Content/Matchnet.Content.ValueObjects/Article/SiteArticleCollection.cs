using System;
using System.Collections;
using System.Text;

using Matchnet.CacheSynchronization.Tracking;

namespace Matchnet.Content.ValueObjects.Article
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class SiteArticleCollection : System.Collections.CollectionBase, Matchnet.IValueObject, Matchnet.ICacheable, IReplicable
	{
		#region Variables and Constants
		private const string SITEARTICLES_CACHE_KEY_PREFIX = "~SITEARTICLES^{0}{1}";

		private int _cacheTTLSeconds;
		private CacheItemPriorityLevel _cachePriority;
		private string _cacheKey;
		
		private int _categoryID;
		private int _siteID;

		[NonSerialized]
		private ReferenceTracker _referenceTracker;
		#endregion

		#region Constructors
		/// <summary>
		/// Creates a new SiteArticleCollection that is ready to be cached
		/// </summary>
		/// <param name="categoryID"></param>
		/// <param name="siteID"></param>
		public SiteArticleCollection(int categoryID, int siteID)
		{
			_categoryID = categoryID;
			_siteID = siteID;
		}
		#endregion

		/// <summary>
		/// 
		/// </summary>
		public SiteArticle this[int index]
		{
			get { return ((SiteArticle)base.InnerList[index]); }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteArticle"></param>
		/// <returns></returns>
		public int Add(SiteArticle siteArticle)
		{
			return base.InnerList.Add(siteArticle);
		}

		/// <summary>
		/// 
		/// </summary>
		public ReferenceTracker ReferenceTracker
		{
			get
			{
				if (_referenceTracker == null)
				{
					_referenceTracker = new ReferenceTracker();
				}

				return _referenceTracker;
			}
		}

		#region ICacheable Members
		/// <summary>
		/// 
		/// </summary>
		/// <param name="CategoryID"></param>
		/// <param name="SiteID"></param>
		/// <returns></returns>
		public static string GetCacheKey(int CategoryID, int SiteID)
		{		
			return string.Format(SITEARTICLES_CACHE_KEY_PREFIX, CategoryID, SiteID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{		
			return SiteArticleCollection.GetCacheKey(_categoryID, _siteID);
		}

		/// <summary>
		/// 
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_cacheKey = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Absolute;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}
		#endregion

		#region IReplicable Members
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}
		#endregion
	}
}
