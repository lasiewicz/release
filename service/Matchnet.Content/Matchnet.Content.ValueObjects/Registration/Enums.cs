﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.Registration
{
    public enum ControlDisplayType
    {
        None=0,
        DropdownList=1,
        CheckBox=2,
        Captcha=3,
        TextBox=4,
        TextArea=5,
        Password=6,
        RadioList=7,
        Region=8,
        DateOfBirth=9,
        File=10,
        ColorCode=11,
        ListBox=12,
        RegionAjax=13,
        Info=14
    }

    public enum ValidationType
    {
        MinLength=1,
        MaxLength=2,
        RegEx=3
    }

    public enum TemplateType
    {
        Splash=1,
        Registration=2,
        Confirmation=3
    }

    public enum SelectionConditionType
    {
        URLParameter=1
    }

    //if you change this enum, it needs to be updated in the RestConsumer Scenario model as well
    public enum DeviceType
    {
        Desktop=1,
        Handeheld=2,
        Tablet=3
    }

    public enum ExternalSessionType
    {
        Bedrock = 0,
        MOS = 1
    }   

    public enum TemplateResultType
    {
        Success=0,
        ExceptionFailure=1,
        TemplateAlreadyExistsForTypeAndSite=2,
        TemplateInUseByRegScenario=3
    }
}
