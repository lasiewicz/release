﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.Registration
{
    [Serializable]
    public class RegControl
    {
        public int? AttributeID { get; set; }
        public string Name { get; set; }
        public ControlDisplayType DisplayType { get; set; }
        public bool HasOptions { get; set; }
        public bool Required { get; set; }
        public string RequiredErrorMessage { get; set; }
        public string Label { get; set; }
        public string AdditionalText { get; set; }
        public bool EnableAutoAdvance { get; set; }
        public bool AllowZeroValue { get; set; }
        public string DefaultValue { get; set; }
        public int RegControlSiteID { get; set; }
        public int RegControlID { get; set; }
        public int SiteID { get; set; }
        public int Order { get; set; }
        public List<ControlValidation> Validations { get; set; }
        public List<ControlCustomOption> CustomOptions { get; set; }
        public List<RegControlDeviceOverride> DeviceOverrides { get; set; }
        public List<RegControlScenarioOverride> ScenarioOverrides { get; set; }

        public RegControl(){}

        public RegControl(int attributeID, string name, ControlDisplayType displayType, bool hasOptions, bool required, string requiredErrorMessage, string label, string additionalText, bool enableAutoAdvance, bool allowZeroValue, List<ControlValidation> validations)
        {
            AttributeID = attributeID;
            Name = name;
            DisplayType = displayType;
            HasOptions = hasOptions;
            Required = required;
            RequiredErrorMessage = requiredErrorMessage;
            Label = label;
            AdditionalText = additionalText;
            EnableAutoAdvance = enableAutoAdvance;
            AllowZeroValue = allowZeroValue;
            Validations = validations;
        }

        public RegControl(string name, ControlDisplayType displayType, bool hasOptions, bool required, string requiredErrorMessage, string label, string additionalText, bool enableAutoAdvance, bool allowZeroValue, List<ControlValidation> validations)
        {
            Name = name;
            DisplayType = displayType;
            HasOptions = hasOptions;
            Required = required;
            RequiredErrorMessage = requiredErrorMessage;
            Label = label;
            AdditionalText = additionalText;
            EnableAutoAdvance = enableAutoAdvance;
            AllowZeroValue = allowZeroValue;
            Validations = validations;
        }


        public RegControl(int attributeID, string name, ControlDisplayType displayType, bool hasOptions, bool required, string requiredErrorMessage, string label, string additionalText, bool enableAutoAdvance, bool allowZeroValue, int regControlSiteId, int regControlId, int siteId, List<ControlValidation> validations)
        {
            AttributeID = attributeID;
            Name = name;
            DisplayType = displayType;
            HasOptions = hasOptions;
            Required = required;
            RequiredErrorMessage = requiredErrorMessage;
            Label = label;
            AdditionalText = additionalText;
            EnableAutoAdvance = enableAutoAdvance;
            AllowZeroValue = allowZeroValue;
            Validations = validations;
            RegControlSiteID = regControlSiteId;
            RegControlID = regControlId;
            SiteID = siteId;
        }

        public RegControl(string name, ControlDisplayType displayType, bool hasOptions, bool required, string requiredErrorMessage, string label, string additionalText, bool enableAutoAdvance, bool allowZeroValue, int regControlSiteId, int regControlId, int siteId, List<ControlValidation> validations)
        {
            Name = name;
            DisplayType = displayType;
            HasOptions = hasOptions;
            Required = required;
            RequiredErrorMessage = requiredErrorMessage;
            Label = label;
            AdditionalText = additionalText;
            EnableAutoAdvance = enableAutoAdvance;
            AllowZeroValue = allowZeroValue;
            Validations = validations;
            RegControlSiteID = regControlSiteId;
            RegControlID = regControlId;
            SiteID = siteId;
        }
    }
}
