﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.Registration
{
    [Serializable]
    public class RegControlScenarioOverride
    {
        public int RegScenarioID { get; set; }
        public ControlDisplayType DisplayType { get; set; }
        public bool Required { get; set; }
        public bool EnableAutoAdvance { get; set; }
        public string RequiredErrorMessage { get; set; }
        public string Label { get; set; }
        public string AdditionalText { get; set; }
    }
}
