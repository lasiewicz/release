﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.Registration
{
    [Serializable]
    public class Template
    {
        public TemplateType Type { get; set; }
        public string Name { get; set; }
        public int ID { get; set; }
        public int SiteID { get; set; }

        public Template() {}
        public Template(TemplateType type, string name)
        {
            Type = type;
            Name = name;
        }

        public Template(int siteId, TemplateType type, string name)
        {
            Type = type;
            Name = name;
            SiteID = siteId;
        }

        public Template(TemplateType type, string name, int id)
        {
            Type = type;
            Name = name;
            ID = id;
        }

        public Template(int siteId, TemplateType type, string name, int id)
        {
            Type = type;
            Name = name;
            ID = id;
            SiteID = siteId;
        }
    }
}
