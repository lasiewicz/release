﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.Registration
{
    public enum RegAdminActionType
    {
        CreateScenario=1,
        EditScenario=2, 
        ToggleScenarioActive=3,
        ToggleScenarioInactive=4,
        EditWeight=5
    }
}
