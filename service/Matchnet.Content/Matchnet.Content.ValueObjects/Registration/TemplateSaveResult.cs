﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.Registration
{
    [Serializable]
    public class TemplateSaveResult
    {
        public TemplateResultType ResultType { get; set; }
        public Template RegTemplate { get; set; }
        public Exception Exception { get; set; }

    }
}
