﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.Registration
{
    [Serializable]
    public class RegControlDeviceOverride
    {
        public DeviceType DeviceType { get; set; }
        public string AdditionalText { get; set; }
        public string Label { get; set; }
        public string RequiredErrorMessage { get; set; }
    }
}
