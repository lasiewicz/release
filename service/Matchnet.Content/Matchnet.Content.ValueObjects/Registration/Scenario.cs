﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.Registration
{
    [Serializable]
    public class Scenario
    {
        public static string CACHE_KEY = "^RegScenarios";
        public int ID { get; set; }
        public int SiteID { get; set; }
        public string Name { get; set; }
        public bool IsOnePageReg { get; set; }
        public Template SplashTemplate { get; set; }
        public Template RegistrationTemplate { get; set; }
        public Template ConfirmationTemplate { get; set; }
        public List<Step> Steps { get; set; }
        public double Weight { get; set; }
        public bool IsControl { get; set; }
        public DeviceType DeviceType { get; set; }
        public ExternalSessionType ExternalSessionType { get; set; }
        public bool Active { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdateDate { get; set; }

        public Scenario() {}

        public Scenario(int id, int siteID, string name, bool isOnePageReg, Template splashTemplate, Template registrationTemplate, Template confirmationTemplate, double weight, bool isControl, DeviceType deviceType, ExternalSessionType externalSessionType, List<Step> steps)
        {
            ID = id;
            SiteID = siteID;
            Name = name;
            IsOnePageReg = isOnePageReg;
            SplashTemplate = splashTemplate;
            RegistrationTemplate = registrationTemplate;
            ConfirmationTemplate = confirmationTemplate;
            Weight = weight;
            IsControl = isControl;
            DeviceType = deviceType;
            ExternalSessionType = externalSessionType;
            Steps = steps;
        }

        public Scenario(int id, int siteID, string name, bool isOnePageReg, Template splashTemplate, Template registrationTemplate, Template confirmationTemplate, double weight, bool isControl, DeviceType deviceType, ExternalSessionType externalSessionType, string desc,string createdBy, string updatedBy, DateTime? createDate, DateTime? updateDate, bool active, List<Step> steps)
        {
            ID = id;
            SiteID = siteID;
            Name = name;
            IsOnePageReg = isOnePageReg;
            SplashTemplate = splashTemplate;
            RegistrationTemplate = registrationTemplate;
            ConfirmationTemplate = confirmationTemplate;
            Weight = weight;
            IsControl = isControl;
            DeviceType = deviceType;
            ExternalSessionType = externalSessionType;
            Steps = steps;
            Description = desc;
            CreatedBy = createdBy;
            UpdatedBy = updatedBy;
            CreateDate = createDate;
            UpdateDate = updateDate;
            Active = active;
        }
    }
}
