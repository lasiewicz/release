﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.Registration
{
    public class CachedTemplatesBySite : ICacheable
    {
        public static string CacheKey = "^TemplatesBySite";

        public Dictionary<int, List<Template>> Templates { get; set; }

        public CacheItemMode CacheMode
        {
            get { return CacheItemMode.Absolute; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return CacheItemPriorityLevel.Normal; }
            set { }
        }

        public int CacheTTLSeconds
        {
            get
            {
                //1 day
                return 86400;
            }
            set
            {
                
            }
        }

        public string GetCacheKey()
        {
            return CacheKey;
        }
    }
}
