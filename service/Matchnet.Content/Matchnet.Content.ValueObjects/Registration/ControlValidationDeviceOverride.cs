﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.Registration
{
    [Serializable]
    public class ControlValidationDeviceOverride
    {
        public DeviceType DeviceType { get; set; }
        public string ErrorMessage { get; set; }
    }
}
