﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.Registration
{
    [Serializable]
    public class ControlValidation
    {
        public ValidationType Type { get; set; }
        public string Value { get; set; }
        public string ErrorMessage { get; set; }
        public List<ControlValidationDeviceOverride> DeviceOverrides { get; set; }

        public ControlValidation() {}

        public ControlValidation(ValidationType type, string value, string errorMessage)
        {
            Type = type;
            Value = value;
            ErrorMessage = errorMessage;
        }
    }
}
