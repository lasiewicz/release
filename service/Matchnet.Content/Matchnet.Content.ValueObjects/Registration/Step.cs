﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.Registration
{
    [Serializable]
    public class Step
    {
        public int StepId { get; set; }
        public string CSSClass { get; set; }
        public string TipText { get; set; }
        public int Order { get; set; }
        public string HeaderText { get; set; }
        public string TitleText { get; set; }
        public List<RegControl> Controls { get; set; }

        public Step() { }

        public Step(string cssClass, string tipText, int order, List<RegControl> controls)
        {
            CSSClass = cssClass;
            TipText = tipText;
            Controls = controls;
            Order = order;
        }

        public Step(string cssClass, string tipText, int order, string headerText, string titleText, List<RegControl> controls)
        {
            CSSClass = cssClass;
            TipText = tipText;
            Controls = controls;
            Order = order;
            HeaderText = headerText;
            TitleText = titleText;
        }

        public Step(int stepId, string cssClass, string tipText, int order, string headerText, string titleText, List<RegControl> controls)
        {
            StepId = stepId;
            CSSClass = cssClass;
            TipText = tipText;
            Controls = controls;
            Order = order;
            HeaderText = headerText;
            TitleText = titleText;
        }

        public override bool Equals(Object step)
        {
            // If parameter is null return false.
            if (step == null)
            {
                return false;
            }

            // If parameter cannot be cast to Step return false.
            var s = step as Step;
            if ((System.Object)s == null)
            {
                return false;
            }

            return StepsEqual(s);
        }

        public bool Equals(Step step)
        {
            // If parameter is null return false.
            if (step == null)
            {
                return false;
            }

            return StepsEqual(step);
        }

        private bool StepsEqual(Step step)
        {
            var thisHeaderText = string.IsNullOrEmpty(this.HeaderText) ? "" : this.HeaderText;
            var compareHeaderText = string.IsNullOrEmpty(step.HeaderText) ? "" : step.HeaderText;
            var thisTipText = string.IsNullOrEmpty(this.TipText) ? "" : this.TipText;
            var compareTipText = string.IsNullOrEmpty(step.TipText) ? "" : step.TipText;
            var thisTitleText = string.IsNullOrEmpty(this.TitleText) ? "" : this.TitleText;
            var compareTitleText = string.IsNullOrEmpty(step.TitleText) ? "" : step.TitleText;

            // Return true if the fields match:
            return (step.StepId == this.StepId && thisHeaderText.Trim() == compareHeaderText.Trim()
                    && thisTipText.Trim() == compareTipText.Trim() && thisTitleText.Trim() == compareTitleText.Trim());
        }
    }
}
