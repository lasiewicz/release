﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.Registration
{
    [Serializable]
    public class ControlCustomOption
    {
        public int Value { get; set; }
        public int ListOrder { get; set; }
        public string Description { get; set; }

        public ControlCustomOption() {}

        public ControlCustomOption(int value, int listOrder, string description)
        {
            Value = value;
            ListOrder = listOrder;
            Description = description;
        }
    }
}
