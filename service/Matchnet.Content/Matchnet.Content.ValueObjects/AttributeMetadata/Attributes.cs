using System;
using System.Collections;
using Matchnet.Content.ValueObjects.BrandConfig;


namespace Matchnet.Content.ValueObjects.AttributeMetadata
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public sealed class Attributes : IValueObject, ICacheable
	{
		/// <summary>
		/// 
		/// </summary>
		public const string CACHE_KEY = "AttributeMetadata";

		private int _cacheTTLSeconds = 60 * 60;  // Default to 1-hour
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;

		private int _siteID;
		private string _constant;
		private Hashtable _groupScopeByID;
		private Hashtable _attributeGroupByID;
		private Hashtable _attributeGroupIDsByAttributeID;
		private Hashtable _attributeGroupByGroupIDAttributeID;
		private Hashtable _attributeByID;
		private Hashtable _attributeByName;

		/// <summary>
		/// 
		/// </summary>
		public Attributes()
		{
			_groupScopeByID = new Hashtable();
			_attributeGroupByID = new Hashtable();
			_attributeGroupIDsByAttributeID = new Hashtable();
			_attributeGroupByGroupIDAttributeID = new Hashtable();
			_attributeByID = new Hashtable();
			_attributeByName = new Hashtable();
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <param name="scope"></param>
		public void AddGroupScope(Int32 id, ScopeType scope)
		{
			if (!_groupScopeByID.ContainsKey(id))
			{
				_groupScopeByID.Add(id, scope);
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <param name="groupID"></param>
		/// <param name="attributeID"></param>
		/// <param name="status"></param>
		/// <param name="length"></param>
		/// <param name="encryptFlag"></param>
		/// <param name="defaultValue"></param>
		public void AddAttributeGroup(Int32 id,
			Int32 groupID,
			Int32 attributeID,
			StatusType status,
			Int16 length,
			bool encryptFlag,
			string defaultValue,
			Int32 oldValueContainerID)
		{
			Hashtable attributeIDs;

			AttributeGroup attributeGroup = new AttributeGroup(id,
				groupID,
				attributeID,
				status,
				length,
				encryptFlag,
				defaultValue,
				oldValueContainerID);

			if (!_attributeGroupByID.ContainsKey(attributeGroup.ID))
			{
				_attributeGroupByID.Add(attributeGroup.ID, attributeGroup);
			}

			ArrayList attributeGroupIDs = _attributeGroupIDsByAttributeID[attributeID] as ArrayList;
			if (attributeGroupIDs == null)
			{
				attributeGroupIDs = new ArrayList();
				_attributeGroupIDsByAttributeID.Add(attributeID, attributeGroupIDs);
			}

			if (!attributeGroupIDs.Contains(id))
			{
				attributeGroupIDs.Add(id);
			}

			attributeIDs = _attributeGroupByGroupIDAttributeID[groupID] as Hashtable;

			if (attributeIDs == null)
			{
				attributeIDs = new Hashtable();
				_attributeGroupByGroupIDAttributeID.Add(groupID, attributeIDs);
			}

			if (!attributeIDs.ContainsKey(attributeID))
			{
				attributeIDs.Add(attributeID, attributeGroup);
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <param name="scope"></param>
		/// <param name="name"></param>
		/// <param name="dataType"></param>
		public void AddAttribute(Int32 id,
			ScopeType scope,
			string name,
			DataType dataType,
			Int32 oldValueContainerID)
		{
			Attribute attribute = new Attribute(id,
				scope,
				name,
				dataType,
				oldValueContainerID);

			if (!_attributeByID.ContainsKey(attribute.ID))
			{
				_attributeByID.Add(attribute.ID, attribute);
			}

			if (!_attributeByName.ContainsKey(attribute.Name))
			{
				_attributeByName.Add(attribute.Name.ToLower(), attribute);
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public ScopeType GetGroupScope(Int32 id)
		{
			return (ScopeType)_groupScopeByID[id];
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public AttributeGroup GetAttributeGroup(Int32 id)
		{
			return _attributeGroupByID[id] as AttributeGroup;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Hashtable GetAttributeGroups()
        {
            return _attributeGroupByID;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Hashtable GetAllAttributes()
        {
            return _attributeByID;
        }

        public AttributeGroup GetAttributeGroup(Attribute attribute, Brand brand)
        {
            int groupId = Constants.GROUP_PERSONALS;

            switch (attribute.Scope)
            {
                case ScopeType.Community:
                    groupId = brand.Site.Community.CommunityID;
                    break;
                case ScopeType.Site:
                    groupId = brand.Site.SiteID;
                    break;
                case ScopeType.Brand:
                case ScopeType.BrandAlias:
                    groupId = brand.BrandID;
                    break;
            }

            return GetAttributeGroup(groupId, attribute.ID);
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="groupID"></param>
		/// <param name="attributeID"></param>
		/// <returns></returns>
		public AttributeGroup GetAttributeGroup(Int32 groupID,
			Int32 attributeID)
		{
			Hashtable attributeIDs = _attributeGroupByGroupIDAttributeID[groupID] as Hashtable;
			if (attributeIDs == null)
			{
				throw new Exception("Attribute groups not found for groupID " + groupID.ToString());
			}
			AttributeGroup ag = attributeIDs[attributeID] as AttributeGroup;
			if (ag == null)
			{
				throw new Exception("Attribute group not found for groupID " + groupID.ToString() + ", attributeID " + attributeID.ToString());
			}
			return ag;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="attributeID"></param>
		/// <returns></returns>
		public ArrayList GetAttributeGroupIDs(Int32 attributeID)
		{
			ArrayList attributeGroupIDs = _attributeGroupIDsByAttributeID[attributeID] as ArrayList;

			if (attributeGroupIDs == null)
			{
				attributeGroupIDs = new ArrayList();
			}

			return attributeGroupIDs;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public Attribute GetAttribute(Int32 id)
		{
			Attribute attribute = _attributeByID[id] as Attribute;

			if (attribute == null)
			{
				throw new Exception("Attribute ID " + id.ToString() + " not found.");
			}

			return attribute;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public Attribute GetAttribute(string name)
		{
			Attribute attribute = _attributeByName[name.ToLower()] as Attribute;

			if (attribute == null)
			{
				throw new Exception("Attribute name " + name + " not found.");
			}

			return attribute;
		}


		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return Matchnet.CacheItemMode.Sliding;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return CACHE_KEY;
		}

		#endregion
	}
}
