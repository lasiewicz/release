using System;
using System.Collections;

using Matchnet;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.Content.ValueObjects.AttributeMetadata
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class AttributeCollections : IValueObject, ICacheable
	{
		/// <summary>
		/// 
		/// </summary>
		public const string CACHE_KEY = "AttributeCollections";

		private int _cacheTTLSeconds = 60 * 60;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;

		private Hashtable _collectionNames;
		private Hashtable _collections;

		/// <summary>
		/// 
		/// </summary>
		public AttributeCollections()
		{
			_collectionNames = new Hashtable();
			_collections = new Hashtable();
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="collectionName"></param>
		/// <param name="collectionID"></param>
		public void AddCollection(string collectionName,
			Int32 collectionID)
		{
			collectionName = collectionName.ToLower();

			if (!_collectionNames.ContainsKey(collectionName))
			{
				_collectionNames.Add(collectionName, collectionID);
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="collectionID"></param>
		/// <param name="groupID"></param>
		/// <param name="attributeID"></param>
		/// <param name="isOptional"></param>
		public void AddCollectionAttribute(Int32 collectionID,
			Int32 groupID,
			Int32 attributeID,
			bool isOptional)
		{
			string key = collectionID.ToString() + "|" + groupID.ToString();
	
			AttributeCollection attributeCollection = _collections[key] as AttributeCollection;

			if (attributeCollection == null)
			{
				attributeCollection = new AttributeCollection();
				_collections.Add(key, attributeCollection);
			}
			
			attributeCollection.Add(new AttributeCollectionAttribute(attributeID, isOptional));
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="collectionName"></param>
		/// <param name="brand"></param>
		/// <returns></returns>
		public AttributeCollection GetCollection(string collectionName,
			Brand brand)
		{
			return GetCollection(collectionName,
				brand.Site.Community.CommunityID,
				brand.Site.SiteID,
				brand.BrandID);
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="collectionName"></param>
		/// <param name="communityID"></param>
		/// <param name="siteID"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public AttributeCollection GetCollection(string collectionName,
			Int32 communityID,
			Int32 siteID,
			Int32 brandID)
		{
			collectionName = collectionName.ToLower();
			Int32 collectionID  = Constants.NULL_INT;

			if (_collectionNames.ContainsKey(collectionName))
			{
				collectionID = (Int32)_collectionNames[collectionName];
			}

			if (collectionID == Constants.NULL_INT)
			{
				return null;
			}

			AttributeCollection collection = _collections[collectionID.ToString() + "|" + communityID.ToString()] as AttributeCollection;
			if (collection != null)
			{
				return collection;
			}

			collection = _collections[collectionID.ToString() + "|" + siteID.ToString()] as AttributeCollection;
			if (collection != null)
			{
				return collection;
			}

			collection = _collections[collectionID.ToString() + "|" + brandID.ToString()] as AttributeCollection;
			if (collection != null)
			{
				return collection;
			}

			return null;
		}


		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return Matchnet.CacheItemMode.Sliding;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return CACHE_KEY;
		}

		#endregion
	}
}
