using System;
using System.Collections;

using Matchnet;


namespace Matchnet.Content.ValueObjects.AttributeMetadata
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable, Flags]
	public enum StatusType : byte
	{
		/// <summary> </summary>
		InActive = 0,
		/// <summary> </summary>
		Active = 1,
		/// <summary> </summary>
		Immutable = 2,
		/// <summary> </summary>
		RequiresTextApproval = 4
	};

	
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public sealed class AttributeGroup
	{
		private Int32 _id;
		private Int32 _groupID;
		private Int32 _attributeID;
		private StatusType _status;
		private Int16 _length;
		private bool _encryptFlag;
		private string _defaultValue;
		private Int32 _oldValueContainerID;

		internal AttributeGroup(Int32 id,
			Int32 groupID,
			Int32 attributeID,
			StatusType status,
			Int16 length,
			bool encryptFlag,
			string defaultValue,
			Int32 oldValueContainerID)
		{
			_id = id;
			_groupID = groupID;
			_attributeID = attributeID;
			_status = status;
			_length = length;
			_encryptFlag = encryptFlag;
			_defaultValue = defaultValue;
			_oldValueContainerID = oldValueContainerID;
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 ID
		{
			get
			{
				return _id;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 GroupID
		{
			get
			{
				return _groupID;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 AttributeID
		{
			get
			{
				return _attributeID;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public StatusType Status
		{
			get
			{
				return _status;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Int16 Length
		{
			get
			{
				return _length;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public bool EncryptFlag
		{
			get
			{
				return _encryptFlag;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string DefaultValue
		{
			get
			{
				return _defaultValue;
			}
		}

		/// <summary>
		/// AttributeGroupID of the AttributeGroup used to hold the old value of this AttributeGroup.
		/// </summary>
		public Int32 OldValueContainerID
		{
			get
			{
				return _oldValueContainerID;
			}
		}
	}
}
