using System;
using System.Collections;


namespace Matchnet.Content.ValueObjects.AttributeMetadata
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class AttributeCollection : System.Collections.CollectionBase
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="attributeCollectionAttribute"></param>
		public void Add(AttributeCollectionAttribute attributeCollectionAttribute)
		{
			base.InnerList.Add(attributeCollectionAttribute);
		}

		/// <summary>
		/// 
		/// </summary>
		public AttributeCollectionAttribute this[Int32 index]
		{
			get
			{
				return base.InnerList[index] as AttributeCollectionAttribute;
			}
		}

		/*
		public Int32 Count
		{
			get
			{
				return base.InnerList.Count;
			}
		}
		*/
	}
}
