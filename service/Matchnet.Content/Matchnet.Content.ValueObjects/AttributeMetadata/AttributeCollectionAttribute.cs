using System;

namespace Matchnet.Content.ValueObjects.AttributeMetadata
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class AttributeCollectionAttribute
	{
		private Int32 _attributeID;
		private bool _isOptional;
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="attributeID"></param>
		/// <param name="isOptional"></param>
		public AttributeCollectionAttribute(Int32 attributeID,
			bool isOptional)
		{
			_attributeID = attributeID;
			_isOptional = isOptional;
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 AttributeID
		{
			get
			{
				return _attributeID;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public bool IsOptional
		{
			get
			{
				return _isOptional;
			}
		}

	}
}
