using System;

namespace Matchnet.Content.ValueObjects.AttributeMetadata
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public enum ScopeType
	{
		/// <summary> </summary>
		EnterpriseGlobal = 1,
		/// <summary> </summary>
		Personals = 10,
		/// <summary> </summary>
		Community = 100,
		/// <summary> </summary>
		Site = 1000,
		/// <summary> </summary>
		Brand = 10000,
		/// <summary> </summary>
		BrandAlias = 100000
	};

	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public enum DataType
	{
		/// <summary> </summary>
		Bit,
		/// <summary> </summary>
		Date,
		/// <summary> </summary>
		Mask,
		/// <summary> </summary>
		Number,
		/// <summary> </summary>
		Text
	};

	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class Attribute
	{
		/// <summary> </summary>		
		protected Int32 _id;
		/// <summary> </summary>
		protected ScopeType _scope;
		/// <summary> </summary>
		protected string _name;
		/// <summary> </summary>
		protected DataType _dataType;
		/// <summary>attributeID of the attribute that holds the old value of this attribute.</summary>
		protected Int32 _oldValueContainerID;

		internal Attribute(Int32 id,
			ScopeType scope,
			string name,
			DataType dataType,
			Int32 oldValueContainerID)
		{
			_id = id;
			_scope = scope;
			_name = name;
			_dataType = dataType;
			_oldValueContainerID = oldValueContainerID;
		}


		/// <summary>
		/// 
		/// </summary>
		public Int32 ID
		{
			get
			{
				return _id;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public ScopeType Scope
		{
			get
			{
				return _scope;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public DataType DataType
		{
			get
			{
				return _dataType;
			}
		}

		/// <summary>
		/// attributeID of the attribute that holds the old value of this attribute.
		/// </summary>
		public Int32 OldValueContainerID
		{
			get
			{
				return _oldValueContainerID;
			}
		}
	}
}
