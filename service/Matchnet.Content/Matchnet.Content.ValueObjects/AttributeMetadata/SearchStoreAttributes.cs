﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.AttributeMetadata
{
    [Serializable]
    public class SearchStoreAttributes: IValueObject, ICacheable
    {

        List<String> _attributes = null;

        public List<String> SearchAttributes { get { return _attributes; } set { _attributes = value; } }



        /// </summary>
        public const string CACHE_KEY = "SearchStoreAttributes";
        public const int CACHE_TTL = 360;  // Default to 1-hour
        private int _cacheTTLSeconds = CACHE_TTL;  // Default to 1-hour
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
         /// <summary>
        /// 
        /// </summary>
        public void Add(string attributename)
        {
            try
            {

                if (_attributes == null)
                    _attributes = new List<string>();


                string exists = _attributes.Find(delegate(string name) { return attributename.ToLower().Trim() == name.ToLower().Trim(); });
                if (String.IsNullOrEmpty(exists))
                {
                    _attributes.Add(attributename);
                }
            }
            catch (Exception ex)
            {

            }

        }
        #region ICacheable Members

        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get
            {
                return _cacheTTLSeconds;
            }
            set
            {
                _cacheTTLSeconds = value;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get
            {
                return Matchnet.CacheItemMode.Absolute;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _cachePriority;
            }
            set
            {
                _cachePriority = value;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetCacheKey()
        {
            return CACHE_KEY;
        }

        #endregion
    
    }
}
