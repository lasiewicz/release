﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.AttributeMetadata
{
    public static class AttributeConstants
    {
        public const string GENDERMASK = "GenderMask";
        public const string REGIONID = "RegionID";
        public const string GLOBALSTATUSMASK = "GlobalStatusMask";
    }
}
