﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.IO;

namespace Matchnet.Content.ValueObjects.LandingPage
{
    [Serializable]
    public class LandingPageTemplate: IValueObject
    {
        public int LandingPageTemplateID { get; private set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Definition { get; set; }
        public List<String> UserDefinedFields { get; set; }

        public LandingPageTemplate()
        {

        }

        public LandingPageTemplate(int landingPageTemplateID)
        {
            LandingPageTemplateID = landingPageTemplateID;
        }

        public LandingPageTemplate(int landingPageTemplateID, string name, string description,
            string definition, List<String> userDefinedFields)
        {
            LandingPageTemplateID = landingPageTemplateID;
            Name = name;
            Description = description;
            Definition = definition;
            UserDefinedFields = userDefinedFields;
        }

        public LandingPageTemplate(int landingPageTemplateID, string name, string description,
            string definition, string xmlForUserDefinedFields)
        {
            LandingPageTemplateID = landingPageTemplateID;
            Name = name;
            Description = description;
            Definition = definition;
            UserDefinedFields = GetUserDefinedFieldsFromXMLString(xmlForUserDefinedFields);
        }

        public LandingPageTemplate(int landingPageTemplateID, string name, string description,
            string definition)
        {
            LandingPageTemplateID = landingPageTemplateID;
            Name = name;
            Description = description;
            Definition = definition;
        }

        public static List<String> GetUserDefinedFieldsFromXMLString(string xml)
        {
            List<String> fields = null;

            XPathDocument xDoc = new XPathDocument(new StringReader(xml));
            XPathNavigator nav = xDoc.CreateNavigator();
            XPathNodeIterator nodeIter = nav.Select("/UserDefinedFields/Field");

            while (nodeIter.MoveNext())
            {
                if (fields == null) fields = new List<string>();
                fields.Add(nodeIter.Current.GetAttribute("Name", ""));
            };

            return fields;
        }

        public static string GetXMLStringFromUserDefinedFields(List<string> userDefinedFields)
        {
            if (userDefinedFields == null)
            {
                return null;
            }
            StringBuilder sb = new StringBuilder();
            sb.Append("<UserDefinedFields>");
            foreach (string field in userDefinedFields)
            {
                sb.Append(string.Format(@"<Field Name=""{0}"" />", field));
            }
            sb.Append("</UserDefinedFields>");
            return sb.ToString();
        }

    }
}
