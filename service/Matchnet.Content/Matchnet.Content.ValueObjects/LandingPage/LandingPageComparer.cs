using System;
using System.Collections;

namespace Matchnet.Content.ValueObjects.LandingPage
{
	/// <summary>
	/// Summary description for LandingPageComparer.
	/// </summary>
	public class LandingPageComparer : IComparer
	{
		private SortType _sortType;
		private Direction _direction;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sortType"></param>
		/// <param name="direction"></param>
		public LandingPageComparer(SortType sortType, Direction direction)
		{
			_sortType = sortType;
			_direction = direction;
		}

		/// <summary>
		/// 
		/// </summary>
		public enum SortType
		{
			/// <summary></summary>
			LandingPageID,
			/// <summary></summary>
			LandingPageName,
			/// <summary></summary>
			StaticURL,
			/// <summary></summary>
			Description,
			/// <summary></summary>
			IsActive
		}

		/// <summary>
		/// 
		/// </summary>
		public enum Direction
		{
			/// <summary></summary>
			Ascending,
			/// <summary></summary>
			Descending
		}

		#region IComparer Members

		public int Compare(object obj1, object obj2)
		{
			LandingPage landingPage1;
			LandingPage landingPage2;

			if (_direction == Direction.Ascending)
			{
				landingPage1 = obj1 as LandingPage;
				landingPage2 = obj2 as LandingPage;
			}
			else
			{
				landingPage2 = obj1 as LandingPage;
				landingPage1 = obj2 as LandingPage;
			}

			switch (_sortType)
			{
				case SortType.IsActive:
					return landingPage1.IsActive.CompareTo(landingPage2.IsActive);
				case SortType.Description:
					return landingPage1.Description.CompareTo(landingPage2.Description);
				case SortType.LandingPageID:
					return landingPage1.LandingPageID.CompareTo(landingPage2.LandingPageID);
				case SortType.LandingPageName:
					return landingPage1.LandingPageName.CompareTo(landingPage2.LandingPageName);
				case SortType.StaticURL:
					return landingPage1.StaticURL.CompareTo(landingPage2.StaticURL);
				default:
					return 0;
			}
		}

		#endregion
	}
}
