﻿using System;
using System.Runtime.Serialization;
using System.IO;
using System.Security.Permissions;
using System.Collections.Generic;
using System.Xml;
using System.Xml.XPath;
using System.Text;

namespace Matchnet.Content.ValueObjects.LandingPage
{
    public enum CheckAvailabilityType
    {
        LandingPage=1,
        Template=2
    }
    
    /// <summary>
    /// Summary description for LandingPage.
    /// </summary>
    [Serializable]
    public class TemplateBasedLandingPage : IValueObject
    {
        public int LandingPageID { get; private set; }
        public int? UnifiedLandingPageID { get; private set; }
        public LandingPageTemplate Template { get; set; }
        public string Name { get; set; }
        public string AnalyticsPageName { get; set; }
        public string Description { get; set; }
        public string Tags { get; set; }
        public string URL { get; set; }
        public bool Published { get; set; }
        public List<UserDefinedField> UserDefinedFields { get; set; }
        public List<int> Sites { get; set; }

        public TemplateBasedLandingPage()
        {

        }

        public TemplateBasedLandingPage(int landingPageID)
        {
            LandingPageID = landingPageID;
        }

        public TemplateBasedLandingPage(int landingPageID, int? unifiedLandingPageID, LandingPageTemplate template, string name,
            string analyticsPageName, string description, string tags, string url, bool published,
            List<UserDefinedField> userDefinedFields, List<int> sites)
        {
            LandingPageID = landingPageID;
            UnifiedLandingPageID = unifiedLandingPageID;
            Template = template;
            Name = name;
            AnalyticsPageName = analyticsPageName;
            Description = description;
            Tags = tags;
            URL = url;
            Published = published;
            UserDefinedFields = userDefinedFields;
            Sites = sites;
        }

        public TemplateBasedLandingPage(int landingPageID, int? unifiedLandingPageID, LandingPageTemplate template, string name,
            string analyticsPageName, string description, string tags, string url, bool published,
            string xmlForUserDefinedFields, List<int> sites)
        {
            LandingPageID = landingPageID;
            UnifiedLandingPageID = unifiedLandingPageID;
            Template = template;
            Name = name;
            AnalyticsPageName = analyticsPageName;
            Description = description;
            Tags = tags;
            URL = url;
            Published = published;
            UserDefinedFields = GetUserDefinedFieldsFromXMLString(xmlForUserDefinedFields);
            Sites = sites;
        }

        public TemplateBasedLandingPage(int landingPageID, int? unifiedLandingPageID, LandingPageTemplate template, string name,
            string analyticsPageName, string description, string tags, string url, bool published,
            List<int> sites)
        {
            LandingPageID = landingPageID;
            UnifiedLandingPageID = unifiedLandingPageID;
            Template = template;
            Name = name;
            AnalyticsPageName = analyticsPageName;
            Description = description;
            Tags = tags;
            URL = url;
            Published = published;
            Sites = sites;
        }


        public TemplateBasedLandingPage(int landingPageID, LandingPageTemplate template, string name,
            string analyticsPageName, string description, string tags, string url, bool published,
            List<UserDefinedField> userDefinedFields, List<int> sites)
        {
            LandingPageID = landingPageID;
            Template = template;
            Name = name;
            AnalyticsPageName = analyticsPageName;
            Description = description;
            Tags = tags;
            URL = url;
            Published = published;
            UserDefinedFields = userDefinedFields;
            Sites = sites;
        }

        public TemplateBasedLandingPage(int landingPageID,  LandingPageTemplate template, string name,
           string analyticsPageName, string description, string tags, string url, bool published,
           List<int> sites)
        {
            LandingPageID = landingPageID;
            Template = template;
            Name = name;
            AnalyticsPageName = analyticsPageName;
            Description = description;
            Tags = tags;
            URL = url;
            Published = published;
            Sites = sites;
        }

        public TemplateBasedLandingPage(int landingPageID, LandingPageTemplate template, string name,
           string analyticsPageName, string description, string tags, string url, bool published,
           string xmlForUserDefinedFields, List<int> sites)
        {
            LandingPageID = landingPageID;
            Template = template;
            Name = name;
            AnalyticsPageName = analyticsPageName;
            Description = description;
            Tags = tags;
            URL = url;
            Published = published;
            UserDefinedFields = GetUserDefinedFieldsFromXMLString(xmlForUserDefinedFields);
            Sites = sites;
        }

        
        public static List<UserDefinedField> GetUserDefinedFieldsFromXMLString(string xml)
        {
            List<UserDefinedField> fields = null;

            XPathDocument xDoc = new XPathDocument(new StringReader(xml));
            XPathNavigator nav = xDoc.CreateNavigator();
            XPathNodeIterator nodeIter = nav.Select("/UserDefinedFields/Field");

            while (nodeIter.MoveNext())
            {
                if (fields == null) fields = new List<UserDefinedField>();
                fields.Add(new UserDefinedField(nodeIter.Current.GetAttribute("Name", ""), nodeIter.Current.GetAttribute("Value", "")));
            };

            return fields;
        }

        public static string GetXMLStringFromUserDefinedFields(List<UserDefinedField> userDefinedFields)
        {
            if (userDefinedFields == null) return null;
            
            StringBuilder sb = new StringBuilder();
            sb.Append("<UserDefinedFields>");
            foreach (UserDefinedField field in userDefinedFields)
            {
                sb.Append(string.Format(@"<Field Name=""{0}"" Value=""{1}"" />", field.Name, field.Value));
            }
            sb.Append("</UserDefinedFields>");
            return sb.ToString();
        }
    }
}
