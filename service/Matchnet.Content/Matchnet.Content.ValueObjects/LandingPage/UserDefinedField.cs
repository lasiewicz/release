﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.LandingPage
{
    [Serializable]
    public class UserDefinedField
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public UserDefinedField()
        {

        }

        public UserDefinedField(string name, string value)
        {
            Name = name;
            Value = value;
        }
    }
}
