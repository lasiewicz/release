using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.IO;

using Matchnet.CacheSynchronization.Tracking;

namespace Matchnet.Content.ValueObjects.LandingPage
{
	/// <summary>
	/// Summary description for LandingPageCollection.
	/// </summary>
	[Serializable]
	public class LandingPageCollection : IValueObject, ICacheable, IEnumerable, ICollection, ISerializable, IByteSerializable, IReplicable
	{
		/// <summary>
		/// 
		/// </summary>
		public const string CACHE_KEY = "LandingPageCollection";

        private Hashtable _landingPages;

		private CacheItemPriorityLevel _cachePriority = CacheItemPriorityLevel.Normal;
		private int _cacheTTLSeconds = 60 * 60;

		[NonSerialized]
		private ReferenceTracker _referenceTracker;

		/// <summary>
		/// 
		/// </summary>
		public LandingPageCollection()
		{
			_landingPages = new Hashtable();
		}

		protected LandingPageCollection(SerializationInfo info, StreamingContext context)
		{
			FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
		}

		[SecurityPermissionAttribute(SecurityAction.Demand,SerializationFormatter=true)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("bytearray",ToByteArray());
		}

		/// <summary>
		///  
		/// </summary>
		/// <param name="pLandingPage"></param>
		/// <param name="pLandingPageID"></param>
		public void Add(LandingPage pLandingPage, int pLandingPageID)
		{
			_landingPages[pLandingPageID] = pLandingPage;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pLandingPageID"></param>
		/// <returns></returns>
		public LandingPage Get( int pLandingPageID )
		{
			return (LandingPage) _landingPages[pLandingPageID];
		}

		public ArrayList Sort(LandingPageComparer comparer)
		{
			ArrayList sorted = new ArrayList(this._landingPages.Values);
			sorted.Sort(comparer);
			return sorted;
		}

		/// <summary>
		/// 
		/// </summary>
		public ReferenceTracker ReferenceTracker
		{
			get
			{
				if (_referenceTracker == null)
				{
					_referenceTracker = new ReferenceTracker();
				}

				return _referenceTracker;
			}
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Absolute;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return CACHE_KEY;
		}

		#endregion

		#region IEnumerable Members

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public IEnumerator GetEnumerator()
		{
			return _landingPages.Values.GetEnumerator();
		}

		#endregion

		#region ICollection Members

		/// <summary>
		/// 
		/// </summary>
		public bool IsSynchronized
		{
			get
			{
				return _landingPages.IsSynchronized;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int Count
		{
			get
			{
				return _landingPages.Count;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="array"></param>
		/// <param name="index"></param>
		public void CopyTo(Array array, int index)
		{
			_landingPages.Values.CopyTo(array, index);
		}

		/// <summary>
		/// 
		/// </summary>
		public object SyncRoot
		{
			get
			{
				return _landingPages.SyncRoot;
			}
		}

		#endregion

		#region IByteSerializable Members
		public void FromByteArray(byte[] bytes)
		{
			Int32 landingPageCount;

			MemoryStream ms = new MemoryStream(bytes);
			BinaryReader br = new BinaryReader(ms);

			landingPageCount = br.ReadInt32();

			_landingPages = new Hashtable(landingPageCount);

			for (Int32 i = 0; i < landingPageCount; i++)
			{
				LandingPage landingPage = new LandingPage(br);
				Add(landingPage, landingPage.LandingPageID);
			}
		}

		public byte[] ToByteArray()
		{
			MemoryStream ms = new MemoryStream();
			BinaryWriter bw = new BinaryWriter(ms);

			bw.Write(_landingPages.Count);
			lock (_landingPages.SyncRoot)
			{
				foreach (LandingPage landingPage in _landingPages.Values)
				{
					((LandingPage)landingPage).ToByteArray(ms, bw);
				}
			}
			return ms.ToArray();
		}
		#endregion

		#region IReplicable Members
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}
		#endregion
	}
}
