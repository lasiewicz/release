using System;
using System.Runtime.Serialization;
using System.IO;
using System.Security.Permissions;

namespace Matchnet.Content.ValueObjects.LandingPage
{
    /// <summary>
    /// Summary description for LandingPage.
    /// </summary>
    [Serializable]
    public class LandingPage : IValueObject, ISerializable, IByteSerializable
    {
        public const byte VERSION_001 = 1;
        public const byte CURRENT_VERSION = VERSION_001;

        #region Private Properties
        private int _LandingPageID = Matchnet.Constants.NULL_INT;
        private string _LandingPageName = string.Empty;
        private string _Description = string.Empty;
        private string _ControlName = string.Empty;
        private string _StaticURL = string.Empty;
        private bool _IsStaticLandingPage = true;
        private int _AdminMemberID = Matchnet.Constants.NULL_INT;
        private bool _IsActive = false;
        private DateTime _UpdateDate = System.DateTime.MinValue;
        private int _PublishID = Matchnet.Constants.NULL_INT;
        #endregion

        public LandingPage()
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pLandingPageID"></param>
        /// <param name="pLandingPageName"></param>
        /// <param name="pDescription"></param>
        /// <param name="pIsStaticLandingPage"></param>
        /// <param name="pStaticURL"></param>
        /// <param name="pControlName"></param>
        /// <param name="pAdminMemberID"></param>
        /// <param name="pIsActive"></param>
        /// <param name="pUpdateDate"></param>
        public LandingPage(int pLandingPageID, string pLandingPageName, string pDescription, string pControlName, string pStaticURL, bool pIsStaticLandingPage, int pAdminMemberID, bool pIsActive, DateTime pUpdateDate)
        {
            _LandingPageID = pLandingPageID;
            _LandingPageName = pLandingPageName;
            _Description = pDescription;
            _ControlName = pControlName;
            _StaticURL = pStaticURL;
            _IsStaticLandingPage = pIsStaticLandingPage;
            _AdminMemberID = pAdminMemberID;
            _IsActive = pIsActive;
            _UpdateDate = pUpdateDate;
        }

        public LandingPage(int pLandingPageID, string pLandingPageName, string pDescription, string pControlName, string pStaticURL, bool pIsStaticLandingPage, int pAdminMemberID, bool pIsActive, DateTime pUpdateDate, int pPublishID)
            : this(pLandingPageID, pLandingPageName, pDescription, pControlName, pStaticURL, pIsStaticLandingPage, pAdminMemberID, pIsActive, pUpdateDate)
        {
            _PublishID = pPublishID;
        }

        protected LandingPage(SerializationInfo info, StreamingContext context)
        {
            FromByteArray((byte[])info.GetValue("bytearray", typeof(byte[])));
        }

        public LandingPage(BinaryReader br)
        {
            Populate(br);
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("bytearray", ToByteArray());
        }

        #region Public Properties

        /// <summary>
        /// 
        /// </summary>
        public int LandingPageID
        {
            get
            {
                return _LandingPageID;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string LandingPageName
        {
            get
            {
                return _LandingPageName;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Description
        {
            get
            {
                return _Description;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string ControlName
        {
            get
            {
                return _ControlName;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string StaticURL
        {
            get
            {
                return _StaticURL;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsStaticLandingPage
        {
            get
            {
                return _IsStaticLandingPage;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int AdminMemberID
        {
            get
            {
                return _AdminMemberID;
            }
            set
            {
                _AdminMemberID = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsActive
        {
            get
            {
                return _IsActive;
            }
            set
            {
                _IsActive = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime UpdateDate
        {
            get
            {
                return _UpdateDate;
            }
            set
            {
                _UpdateDate = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int PublishID
        {
            get
            {
                return _PublishID;
            }
            set
            {
                _PublishID = value;
            }
        }
        #endregion

        #region IByteSerializable Members
        public void FromByteArray(byte[] bytes)
        {
            MemoryStream ms = new MemoryStream(bytes);
            BinaryReader br = new BinaryReader(ms);

            Populate(br);
        }

        private void Populate(BinaryReader br)
        {
            byte version = br.ReadByte();

            switch (version)
            {
                case VERSION_001:
                    _LandingPageID = br.ReadInt32();
                    _LandingPageName = br.ReadString();
                    _Description = br.ReadString();
                    _ControlName = br.ReadString();
                    _StaticURL = br.ReadString();
                    _IsStaticLandingPage = br.ReadBoolean();
                    _AdminMemberID = br.ReadInt32();
                    _IsActive = br.ReadBoolean();
                    _UpdateDate = DateTime.FromFileTime(br.ReadInt64());
                    _PublishID = br.ReadInt32();
                    break;

                default:
                    throw new Exception("Unsupported serialization version (" + version.ToString() + ").");
            }
        }

        public byte[] ToByteArray()
        {
            MemoryStream ms = new MemoryStream(844);	//844 = # of bytes in all private members + 1 byte version #
            BinaryWriter bw = new BinaryWriter(ms);

            ToByteArray(ms, bw);

            return ms.ToArray();
        }

        // Used by the Collection object for this object.
        public void ToByteArray(MemoryStream ms, BinaryWriter bw)
        {
            byte version = CURRENT_VERSION;

            bw.Write(version);
            bw.Write(_LandingPageID);
            bw.Write(_LandingPageName);
            bw.Write(_Description);
            bw.Write(_ControlName);
            bw.Write(_StaticURL);
            bw.Write(_IsStaticLandingPage);
            bw.Write(_AdminMemberID);
            bw.Write(_IsActive);
            bw.Write(_UpdateDate.ToFileTime());
            bw.Write(_PublishID);
        }
        #endregion
    }
}
