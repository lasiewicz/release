using System;
using System.Collections;

namespace Matchnet.Content.ValueObjects.Admin
{
	/// <summary>
	/// Summary description for AdminActionReport.
	/// </summary>
	[Serializable]
	public class AdminActionReport : CollectionBase, IValueObject, ICacheable
	{
		/// <summary>
		/// 
		/// </summary>
		public enum AdminActionReportSortType
		{
			/// <summary> </summary>
			AdminMemberID,
			/// <summary> </summary>
			GroupID,
			/// <summary> </summary>
			InsertDate,
			/// <summary> </summary>
			ActionCount
		}

		/// <summary>
		/// 
		/// </summary>
		public AdminActionReport()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="adminActionReportItem"></param>
		/// <returns></returns>
		public int Add(AdminActionReportItem adminActionReportItem)
		{
			return base.InnerList.Add(adminActionReportItem);
		}

		/// <summary>
		/// 
		/// </summary>
		public AdminActionReportItem this[Int32 index]
		{
			get
			{
				return base.InnerList[index] as AdminActionReportItem;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="comparer"></param>
		public void Sort(IComparer comparer)
		{
			base.InnerList.Sort(comparer);
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				// TODO:  Add AdminActionReport.CacheTTLSeconds getter implementation
				return 0;
			}
			set
			{
				// TODO:  Add AdminActionReport.CacheTTLSeconds setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				// TODO:  Add AdminActionReport.CacheMode getter implementation
				return new Matchnet.CacheItemMode ();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				// TODO:  Add AdminActionReport.CachePriority getter implementation
				return new Matchnet.CacheItemPriorityLevel ();
			}
			set
			{
				// TODO:  Add AdminActionReport.CachePriority setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			// TODO:  Add AdminActionReport.GetCacheKey implementation
			return null;
		}

		#endregion

		#region AdminActionReportComparer
		/// <summary>
		/// 
		/// </summary>
		public class AdminActionReportComparer : IComparer 
		{
			private AdminActionReportSortType compareBy;

			/// <summary>
			/// 
			/// </summary>
			/// <param name="pCompareBy"></param>
			public AdminActionReportComparer( AdminActionReportSortType pCompareBy ) 
			{
				compareBy = pCompareBy;
			}

			/// <summary>
			/// 
			/// </summary>
			/// <param name="x"></param>
			/// <param name="y"></param>
			/// <returns></returns>
			public int Compare(object x, object y)
			{
				switch(compareBy)
				{
					case AdminActionReportSortType.ActionCount :
						return ((AdminActionReportItem)x).ActionCount.CompareTo(((AdminActionReportItem)y).ActionCount);
					case AdminActionReportSortType.AdminMemberID :
						return ((AdminActionReportItem)x).AdminMemberID.CompareTo(((AdminActionReportItem)y).AdminMemberID);
					case AdminActionReportSortType.GroupID :
						return ((AdminActionReportItem)x).GroupID.CompareTo(((AdminActionReportItem)y).GroupID);
					default : //InsertDate
						return System.DateTime.Compare(((AdminActionReportItem)x).InsertDate, ((AdminActionReportItem)y).InsertDate);
				}
			}
		}
		#endregion
	}
}
