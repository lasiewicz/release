using System;
using System.Collections;

namespace Matchnet.Content.ValueObjects.Admin
{
	/// <summary>
	/// Summary description for PublishCollection.
	/// </summary>
	[Serializable]
	public class PublishCollection : DictionaryBase, IValueObject
	{
		/// <summary>
		/// 
		/// </summary>
		public PublishCollection()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="publish"></param>
		/// <returns></returns>
		public void Add(Publish publish)
		{
			base.InnerHashtable.Add(publish.PublishID, publish);
		}

		/// <summary>
		/// 
		/// </summary>
		public Publish this[Int32 publishID]
		{
			get
			{
				return ((Publish)base.InnerHashtable[publishID]);
			}
			set
			{
				base.InnerHashtable[publishID] = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool Contains(Int32 publishID)
		{
			return base.InnerHashtable.ContainsKey(publishID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ICollection Values()
		{
			return base.InnerHashtable.Values;
		}
	}
}
