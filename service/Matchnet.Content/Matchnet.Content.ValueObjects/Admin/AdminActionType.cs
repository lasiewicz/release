using System;

namespace Matchnet.Content.ValueObjects.Admin
{
	/// <summary>
	/// Summary description for AdminActionType.
	/// </summary>
	[Serializable]
	public class AdminActionType
	{
		private Int32 _adminActionTypeID;
		private string _description;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="adminActionTypeID"></param>
		/// <param name="description"></param>
		public AdminActionType(int adminActionTypeID, string description)
		{
			_adminActionTypeID = adminActionTypeID;
			_description = description;
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 AdminActionTypeID
		{
			get
			{
				return _adminActionTypeID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Description
		{
			get
			{
				return _description;
			}
		}
	}

	/// <summary>
	/// 
	/// </summary>
	[Flags]
	public enum AdminAction
	{
		/// <summary>
		/// 
		/// </summary>
		Default = 0,
		/// <summary>
		/// 
		/// </summary>
		ApprovedFreeText = 1,
		/// <summary>
		/// 
		/// </summary>
		DeletedFreeTextAttribute = 2,
		/// <summary>
		/// 
		/// </summary>
		AdminSuspendMember = 4,
		/// <summary>
		/// 
		/// </summary>
		ApprovePhoto = 8,
		/// <summary>
		/// 
		/// </summary>
		RejectPhoto = 16,
		/// <summary>
		/// 
		/// </summary>
		ChangedEmailVerified = 32,
		/// <summary>
		/// 
		/// </summary>
		ChangedBadEmail = 64,
		/// <summary>
		/// 
		/// </summary>
		AlteredExternalVideoLinkURL = 128,
		/// <summary>
		/// 
		/// </summary>
		AdminUnsuspendMember = 256,
		/// <summary>
		/// 
		/// </summary>
		AdminOnlyPromoUsed = 512,
		/// <summary>
		/// 
		/// </summary>
		ApprovedQuestionAnswer = 1024,
		/// <summary>
		/// 
		/// </summary>
		DeletedQuestionAnswer = 2048,
		ChangedEmail = 4096,
		ChangedPassword = 8192,
		SendPassword = 16384,
		ChangedDNEStatus = 32768,
        DeletedPhotoFromQueue = 65536,
        UploadMemberPhotos = 131072,
        ChangedOffsiteNotifications = 262144,
        BlockedFromChatRoom = 524288,
        GrantedAccessToChatRoom = 1048576,
        ChangedFraudToYes = 2097152,
        ChangedFraudToNo = 4194304,
        ResetColorCode = 8388608,
        DeletedMemberPhoto = 16777216,
        BannedEmails = 33554432,
        UnbannedEmails = 67108864,
        ApprovedFacebookLike = 134217728,
        DeletedFacebookLike = 268435456

	}

	public enum AdminMemberIDType {
		/// <summary>
		/// AdminMemberID is a profile on the dating sites and is and Admin
		/// </summary>
		AdminProfileMemberID = 1,
		/// <summary>
		/// AdminMemberID is a ticket ID from the spark web services
		/// </summary>
		SparkWSTicketID = 2
	}
}
