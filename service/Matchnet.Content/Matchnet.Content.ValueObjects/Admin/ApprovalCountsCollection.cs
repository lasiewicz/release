using System;
using System.Collections;

namespace Matchnet.Content.ValueObjects.Admin
{
	/// <summary>
	/// Summary description for ApprovalCountsCollection.
	/// </summary>
	[Serializable]
	public class ApprovalCountsCollection : DictionaryBase, IValueObject
	{
		/// <summary>
		/// 
		/// </summary>
		public ApprovalCountsCollection()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="approvalCounts"></param>
		/// <returns></returns>
		public void Add(ApprovalCounts approvalCounts)
		{
			base.InnerHashtable.Add(approvalCounts.MemberID, approvalCounts);
		}

		/// <summary>
		/// 
		/// </summary>
		public ApprovalCounts this[int memberID]
		{
			get
			{
				return (ApprovalCounts) base.InnerHashtable[memberID];
			}
			set
			{
				base.InnerHashtable[memberID] = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <returns></returns>
		public bool Contains(int memberID)
		{
			return base.InnerHashtable.ContainsKey(memberID);
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 TextCount
		{
			get
			{
				int textCount = 0;

				foreach(ApprovalCounts approvalCounts in base.InnerHashtable.Values)
				{
					textCount += approvalCounts.TextCount;
				}

				return textCount;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 PhotoCount
		{
			get
			{
				int photoCount = 0;

				foreach(ApprovalCounts approvalCounts in base.InnerHashtable.Values)
				{
					photoCount += approvalCounts.PhotoCount;
				}

				return photoCount;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 QuestionAnswerCount
		{
			get
			{
				int questionAnswerCount = 0;

				foreach(ApprovalCounts approvalCounts in base.InnerHashtable.Values)
				{
					questionAnswerCount += approvalCounts.QuestionAnswerCount;
				}

				return questionAnswerCount;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 TotalCount
		{
			get
			{
				return TextCount + PhotoCount + QuestionAnswerCount;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ICollection Values()
		{
			return base.InnerHashtable.Values;
		}
	}
}
