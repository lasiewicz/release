using System;
using System.Collections;

namespace Matchnet.Content.ValueObjects.Admin
{
	[Serializable]
	public class AdminActivityContainerCollection: CollectionBase, IValueObject
	{
		public AdminActivityContainerCollection()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public void Add(AdminActivityContainer adminActivityContainer)
		{
			base.InnerList.Add(adminActivityContainer);
		}

		/// <summary>
		/// 
		/// </summary>
		public AdminActivityContainer this[Int32 index]
		{
			get
			{
				return base.InnerList[index] as AdminActivityContainer;
			}
		}
	}
}
