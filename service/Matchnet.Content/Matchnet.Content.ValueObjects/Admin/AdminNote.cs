using System;

namespace Matchnet.Content.ValueObjects.Admin
{
	/// <summary>
	/// Summary description for AdminNote.
	/// </summary>
	[Serializable]
	public class AdminNote : IComparable
	{
		#region Private Members
		int _memberID;
		int _adminNoteID;
		int _adminMemberID;
		DateTime _insertDate;
		string _note;
		string _adminEmailAddress = string.Empty;
		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="adminNoteID"></param>
		/// <param name="adminMemberID"></param>
		/// <param name="insertDate"></param>
		/// <param name="note"></param>
		public AdminNote(int memberID, int adminNoteID, int adminMemberID, DateTime insertDate, string note)
		{
			 _memberID = memberID;
			_adminNoteID = adminNoteID;
			_adminMemberID = adminMemberID;
			_insertDate = insertDate;
			_note = note;
		}

		public AdminNote(int memberID, int adminNoteID, int adminMemberID, DateTime insertDate, string note, string adminEmailAddress)
		{
			_memberID = memberID;
			_adminNoteID = adminNoteID;
			_adminMemberID = adminMemberID;
			_insertDate = insertDate;
			_note = note;
			_adminEmailAddress = adminEmailAddress;
		}

		#region Public Properties
		/// <summary>
		/// 
		/// </summary>
		public int MemberID
		{
			get{return _memberID;}
			set{_memberID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public int AdminNoteID
		{
			get{return _adminNoteID;}
			set{_adminNoteID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public int AdminMemberID
		{
			get{return _adminMemberID;}
			set{_adminMemberID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDate
		{
			get{return _insertDate;}
			set{_insertDate = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Note
		{
			get{return _note;}
			set{_note = value;}
		}

		public string AdminEmailAddress
		{
			get{return _adminEmailAddress;}
			set{_adminEmailAddress = value;}
		}
		#endregion

		#region IComparable Members

		/// <summary>
		/// 
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public int CompareTo(object obj)
		{
			AdminNote adminNote = (AdminNote) obj;
			return -this.InsertDate.CompareTo(adminNote.InsertDate);
		}

		#endregion
	}
}
