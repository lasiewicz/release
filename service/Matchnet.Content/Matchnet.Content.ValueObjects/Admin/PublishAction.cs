using System;
using System.Collections;

namespace Matchnet.Content.ValueObjects.Admin
{
	public enum ServiceEnvironmentTypeEnum
	{ 
		Dev = 1,
		ContentStg = 2,
		Prod = 3
	}

	/// <summary>
	/// NOTE:  All these values MUST end with the environment name to which they apply (Dev, Stage, ContentStg, Prod).
	/// </summary>
	public enum PublishActionTypeEnum
	{
		AddInDev = 1,
		EditInDev = 2,
		DeleteInDev = 3,
		VerifyInDev = 4,
		PublishToContentStg = 5,
		VerifyInContentStg = 6,
		ApproveInDev = 7,
		VerifyWithPartnerInContentStg = 8,
		PublishToProd = 9,
		VerifyInProd = 10,
		VerifyWithPartnerInProd = 11
	}

	/// <summary>
	/// Summary description for PublishAction.
	/// </summary>
	[Serializable]
	public class PublishAction : IValueObject
	{
		#region Private Members
		private Int32 publishID;
		private Int32 publishActionPublishObjectID;
		private PublishActionTypeEnum publishActionType;
		private Int32 workflowOrder;
		private Int32 memberID;
		private DateTime insertDate;
		#endregion

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		public PublishAction(Int32 publishID, Int32 publishActionPublishObjectID, PublishActionTypeEnum publishActionType, Int32 workflowOrder, Int32 memberID, DateTime insertDate)
		{
			this.publishID = publishID;
			this.publishActionPublishObjectID = publishActionPublishObjectID;
			this.publishActionType = publishActionType;
			this.workflowOrder = workflowOrder;
			this.memberID = memberID;
			this.insertDate = insertDate;
		}
		#endregion
		
		#region Public Properties
		/// <summary>
		/// 
		/// </summary>
		public Int32 PublishID
		{
			get { return publishID; }
			set { publishID = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public Int32 PublishActionPublishObjectID
		{
			get { return publishActionPublishObjectID; }
			set { publishActionPublishObjectID = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public PublishActionTypeEnum PublishActionType
		{
			get { return publishActionType; }
			set { publishActionType = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 WorkflowOrder
		{
			get { return workflowOrder; }
			set { workflowOrder = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public Int32 MemberID
		{
			get { return memberID; }
			set { memberID = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDate
		{
			get { return insertDate; }
			set { insertDate = value; }
		}
		#endregion
	}
}
