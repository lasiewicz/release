using System;
using System.Collections;

namespace Matchnet.Content.ValueObjects.Admin
{
	[Serializable]
	public class AdminActionReasonCollection: CollectionBase, IValueObject, ICacheable
	{
		public const string CACHE_KEY = "ADMIN_ACTION_REASONS";

		private int _cacheTTLSeconds = 60 * 60;
		private CacheItemPriorityLevel _cacheItemPriorityLevel = CacheItemPriorityLevel.Normal;
		
		public AdminActionReasonCollection()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public void Add(AdminActionReason adminActionReason)
		{
			base.InnerList.Add(adminActionReason);
		}

		/// <summary>
		/// 
		/// </summary>
		public AdminActionReason this[Int32 index]
		{
			get
			{
				return base.InnerList[index] as AdminActionReason;
			}
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cacheItemPriorityLevel;
			}
			set
			{
				_cacheItemPriorityLevel = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return CACHE_KEY;
		}

		#endregion
	}
}
