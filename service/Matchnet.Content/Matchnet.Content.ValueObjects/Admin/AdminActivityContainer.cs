using System;

namespace Matchnet.Content.ValueObjects.Admin
{
	public enum ActivityType
	{
		Action = 1,
		Note = 2
	}

	[Serializable]
	public class AdminActivityContainer
	{
		private ActivityType _activityType;
		private AdminNote _adminNote;
		private AdminActionLog _adminActionLog;
		
		public ActivityType Type
		{
			get
			{
				return _activityType;
				
			}
		}

		public AdminNote AdminNote
		{
			get
			{
				return _adminNote;
			}
		}
		
		public AdminActionLog AdminActionLog
		{
			get
			{
				return _adminActionLog;
			}
		}

		public AdminActivityContainer(AdminNote adminNote)
		{
			_adminNote = adminNote;
			_activityType = ActivityType.Note;
		}

		public AdminActivityContainer(AdminActionLog adminActionLog)
		{
			_adminActionLog = adminActionLog;
			_activityType = ActivityType.Action;
		}
	}
}
