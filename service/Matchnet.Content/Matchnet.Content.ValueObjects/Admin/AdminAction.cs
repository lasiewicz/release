using System;

namespace Matchnet.Content.ValueObjects.Admin
{
	/// <summary>
	/// Summary description for AdminAction
	/// </summary>
	[Serializable]
	public class AdminActionItem : IValueObject
	{
		#region Private Members
		private Int32 _memberID;
		private Int32 _adminMemberID;
		private AdminAction _adminAction;
		private DateTime _insertDate;
		private AdminActionReason _reason;
		#endregion

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="adminMemberID"></param>
		/// <param name="adminAction"></param>
		/// <param name="insertDate"></param>
		public AdminActionItem(Int32 memberID, Int32 adminMemberID, AdminAction adminAction, DateTime insertDate)
		{
			_memberID = memberID;
			_adminMemberID = adminMemberID;
			_adminAction = adminAction;
			_insertDate = insertDate;
		}

		public AdminActionItem(Int32 memberID, Int32 adminMemberID, AdminAction adminAction, DateTime insertDate, AdminActionReason reason)
		{
			_memberID = memberID;
			_adminMemberID = adminMemberID;
			_adminAction = adminAction;
			_insertDate = insertDate;
			_reason = reason;
		}
		#endregion
		
		#region Public Properties
		/// <summary>
		/// 
		/// </summary>
		public Int32 MemberID
		{
			get { return(_memberID); }
			set { _memberID = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public Int32 AdminMemberID
		{
			get { return(_adminMemberID); }
			set { _adminMemberID = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public AdminAction AdminAction
		{
			get { return(_adminAction); }
			set { _adminAction = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDate
		{
			get { return(_insertDate); }
			set { _insertDate = value; }
		}
		#endregion
	}
}
