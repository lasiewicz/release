using System;

namespace Matchnet.Content.ValueObjects.Admin
{
	/// <summary>
	/// Summary description for AdminActionLog.
	/// </summary>
	[Serializable]
	public class AdminActionLog : IValueObject
	{
		#region Private Members
		private Int32 _memberID;
		private string _emailAddress;
		private string _actionDescription;
		private DateTime _insertDate;
		private string _reasonDescription;
		#endregion

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="emailAddress"></param>
		/// <param name="actionDescription"></param>
		/// <param name="insertDate"></param>
		public AdminActionLog(Int32 memberID, string emailAddress, string actionDescription, DateTime insertDate)
		{
			_memberID = memberID;
			_emailAddress = emailAddress;
			_actionDescription = actionDescription;
			_insertDate = insertDate;
		}

		public AdminActionLog(Int32 memberID, string emailAddress, string actionDescription, DateTime insertDate, string reasonDescription)
		{
			_memberID = memberID;
			_emailAddress = emailAddress;
			_actionDescription = actionDescription;
			_insertDate = insertDate;
			_reasonDescription = reasonDescription;
		}
		#endregion
		
		#region Public Properties
		/// <summary>
		/// 
		/// </summary>
		public Int32 MemberID
		{
			get { return(_memberID); }
			set { _memberID = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public string EmailAddress
		{
			get { return(_emailAddress); }
			set { _emailAddress = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public string ActionDescription
		{
			get { return(_actionDescription); }
			set { _actionDescription = value; }
		}

		public string ReasonDescription
		{
			get { return(_reasonDescription); }
			set { _reasonDescription = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDate
		{
			get { return(_insertDate); }
			set { _insertDate = value; }
		}
		#endregion
	}
}
