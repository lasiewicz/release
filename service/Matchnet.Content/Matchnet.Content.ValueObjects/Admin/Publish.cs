using System;
using System.Collections;

namespace Matchnet.Content.ValueObjects.Admin
{
	/// <summary>
	/// 
	/// </summary>
	public enum PublishObjectType
	{
		Pixel = 1,
		SiteCategory = 2,
		SiteArticle = 3,
		LandingPage = 4,
		Category = 5,
		Article = 6
	}

	/// <summary>
	/// Summary description for Publish.
	/// </summary>
	[Serializable]
	public class Publish : IValueObject
	{
		#region Private Members
		private Int32 publishID;
		private Int32 publishObjectID;
		private PublishObjectType publishObjectType;
		private Object content;
		private DateTime insertDate;
		private PublishActionCollection publishActions;
		#endregion

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		public Publish()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		public Publish(Int32 publishID, Int32 publishObjectID, PublishObjectType publishObjectType, Object content, DateTime insertDate)
		{
			this.publishID = publishID;
			this.publishObjectID = publishObjectID;
			this.publishObjectType = publishObjectType;
			this.content = content;
			this.insertDate = insertDate;
		}
		#endregion
		
		#region Public Properties
		/// <summary>
		/// 
		/// </summary>
		public Int32 PublishID
		{
			get { return publishID; }
			set { publishID = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public Int32 PublishObjectID
		{
			get { return publishObjectID; }
			set { publishObjectID = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public PublishObjectType PublishObjectType
		{
			get { return publishObjectType; }
			set { publishObjectType = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public Object Content
		{
			get { return content; }
			set { content = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDate
		{
			get { return insertDate; }
			set { insertDate = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public PublishActionCollection PublishActions
		{
			get { return publishActions; }
			set { publishActions = value; }
		}
		#endregion
	}
}
