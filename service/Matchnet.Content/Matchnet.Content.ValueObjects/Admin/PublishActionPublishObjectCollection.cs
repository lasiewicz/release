using System;
using System.Collections;

namespace Matchnet.Content.ValueObjects.Admin
{
	/// <summary>
	/// Summary description for PublishActionPublishObjectCollection.
	/// </summary>
	[Serializable]
	public class PublishActionPublishObjectCollection : DictionaryBase, IValueObject, ICacheable
	{
		public const string CACHE_KEY_PREFIX = "~PUBLISHACTIONPUBLISHOBJECTCOLLECTION^";
		private CacheItemPriorityLevel _cachePriority;
		private String _cacheKey;
		private Int32 _cacheTTLSeconds = 60 * 60;

		/// <summary>
		/// 
		/// </summary>
		public PublishActionPublishObjectCollection()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="publishAction"></param>
		/// <returns></returns>
		public void Add(PublishActionPublishObject publishActionPublishObject)
		{
			base.InnerHashtable.Add(publishActionPublishObject.PublishActionPublishObjectID, publishActionPublishObject);
		}

		/// <summary>
		/// 
		/// </summary>
		public PublishActionPublishObject this[Int32 publishActionPublishObjectID]
		{
			get
			{
				return ((PublishActionPublishObject)base.InnerHashtable[publishActionPublishObjectID]);
			}
			set
			{
				base.InnerHashtable[publishActionPublishObjectID] = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool Contains(Int32 publishActionPublishObjectID)
		{
			return base.InnerHashtable.ContainsKey(publishActionPublishObjectID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ICollection Values()
		{
			return base.InnerHashtable.Values;
		}

		#region ICacheable Members
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{		
			return CACHE_KEY_PREFIX;
		}

		/// <summary>
		/// 
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_cacheKey = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}
		#endregion
	}
}
