using System;

namespace Matchnet.Content.ValueObjects.Admin
{
	/// <summary>
	/// Summary description for AdminActionReportItem.
	/// </summary>
	[Serializable]
	public class AdminActionReportItem : IValueObject
	{
		private int _adminMemberID;
		private int _groupID;
		private DateTime _insertDate;
		private int _actionCount;

		/// <summary>
		/// 
		/// </summary>
		public AdminActionReportItem(int adminMemberID, int groupID, DateTime insertDate, int actionCount)
		{
			_adminMemberID = adminMemberID;
			_groupID = groupID;
			_insertDate = insertDate;
			_actionCount = actionCount;
		}

		/// <summary>
		/// 
		/// </summary>
		public int AdminMemberID
		{
			get
			{
				return _adminMemberID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int GroupID
		{
			get
			{
				return _groupID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public DateTime InsertDate
		{
			get
			{
				return _insertDate;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int ActionCount
		{
			get
			{
				return _actionCount;
			}
		}
		}
}
