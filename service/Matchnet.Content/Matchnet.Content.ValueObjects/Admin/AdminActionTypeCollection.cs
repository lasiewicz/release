using System;
using System.Collections;

namespace Matchnet.Content.ValueObjects.Admin
{
	/// <summary>
	/// Summary description for AdminActionTypeCollection.
	/// </summary>
	[Serializable]
	public class AdminActionTypeCollection : CollectionBase, IValueObject, ICacheable
	{
		/// <summary>
		/// 
		/// </summary>
		public const string CACHE_KEY = "ADMIN_ACTION_TYPES";

		private int _cacheTTLSeconds = 60 * 60;
		private CacheItemPriorityLevel _cacheItemPriorityLevel = CacheItemPriorityLevel.Normal;

		/// <summary>
		/// 
		/// </summary>
		public AdminActionTypeCollection()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="adminActionType"></param>
		public void Add(AdminActionType adminActionType)
		{
			base.InnerList.Add(adminActionType);
		}

		/// <summary>
		/// 
		/// </summary>
		public AdminActionType this[Int32 index]
		{
			get
			{
				return base.InnerList[index] as AdminActionType;
			}
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cacheItemPriorityLevel;
			}
			set
			{
				_cacheItemPriorityLevel = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return CACHE_KEY;
		}

		#endregion
	}
}
