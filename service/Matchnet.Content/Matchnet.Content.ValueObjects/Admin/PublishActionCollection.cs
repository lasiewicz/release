using System;
using System.Collections;

namespace Matchnet.Content.ValueObjects.Admin
{
	/// <summary>
	/// Summary description for PublishActionCollection.
	/// </summary>
	[Serializable]
	public class PublishActionCollection : DictionaryBase, IValueObject
	{
		/// <summary>
		/// 
		/// </summary>
		public PublishActionCollection()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="publishAction"></param>
		/// <returns></returns>
		public void Add(PublishAction publishAction)
		{
			base.InnerHashtable.Add(publishAction.WorkflowOrder, publishAction);
		}

		/// <summary>
		/// 
		/// </summary>
		public PublishAction this[Int32 workflowOrder]
		{
			get
			{
				return ((PublishAction)base.InnerHashtable[workflowOrder]);
			}
			set
			{
				base.InnerHashtable[workflowOrder] = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool Contains(Int32 workflowListOrder)
		{
			return base.InnerHashtable.ContainsKey(workflowListOrder);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ICollection Values()
		{
			return base.InnerHashtable.Values;
		}
	}
}
