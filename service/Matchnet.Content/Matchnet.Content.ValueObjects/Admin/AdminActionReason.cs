using System;

namespace Matchnet.Content.ValueObjects.Admin
{
	public enum AdminActionReasonID
	{
		None = 0,
		FraudScammer = 1,
		ContactInfo = 2,
		RequestedByMember = 3,
		AbuseInappropriate = 4,
		AbuseChat = 5,
		Married = 6,
		MultipleProfiles = 7,
		FraudAffiliate = 8,
		ChargeBack = 9,
		FraudTool = 10,
		Other = 11, 
		ROLF = 12, 
        PhotoBlurry=13,
        PhotoCopyright=14,
        PhotoFilesize=15,
        PhotoGeneral=16,
        PhotoSuggestive=17,
        PhotoUnknownMember=18,
        PhotoBadFileFormat=19,
        MultipleProfilesByMistake=20,
        Unknown=9999
	}

	public enum AdminActionReasonType
	{
		AdminSuspend = 1
	}
	
	
	[Serializable]
	public class AdminActionReason: IValueObject
	{
		private AdminActionReasonID _adminActionReasonID;
		private AdminActionReasonType _adminActionReasonType;
		private string _description;
        private bool _display; 

		public AdminActionReasonID AdminActionReasonID
		{
			get 
			{
				return _adminActionReasonID;
			}
		}

		public AdminActionReasonType AdminActionReasonType
		{
			get 
			{
				return _adminActionReasonType;
			}
		}

		public string Description
		{
			get 
			{
				return _description;
			}
		}

        public bool Display
        {
            get
            {
                return _display;
            }
        }
		

		/// <summary>
		/// 
		/// </summary>
		/// <param name="adminActionTypeID"></param>
		/// <param name="description"></param>
		public AdminActionReason(AdminActionReasonID adminActionReasonID, AdminActionReasonType adminActionReasonType, string description)
		{
			_adminActionReasonID = adminActionReasonID;
			_adminActionReasonType = adminActionReasonType;
			_description = description;
		}

        public AdminActionReason(AdminActionReasonID adminActionReasonID, AdminActionReasonType adminActionReasonType, string description, bool display)
        {
            _adminActionReasonID = adminActionReasonID;
            _adminActionReasonType = adminActionReasonType;
            _description = description;
            _display = display;
        }
	}
}
