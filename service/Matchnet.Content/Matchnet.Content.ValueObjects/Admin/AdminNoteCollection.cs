using System;

namespace Matchnet.Content.ValueObjects.Admin
{
	/// <summary>
	/// Summary description for AdminActionLogCollection.
	/// </summary>
	[Serializable]
	public class AdminNoteCollection : System.Collections.CollectionBase, IValueObject, ICacheable
	{
		/// <summary>
		/// 
		/// </summary>
		public AdminNoteCollection()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="adminNote"></param>
		/// <returns></returns>
		public int Add(AdminNote adminNote)
		{
			return base.InnerList.Add(adminNote);
		}

		/// <summary>
		/// 
		/// </summary>
		public void Sort()
		{
			base.InnerList.Sort();
		}

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				// TODO:  Add AdminActionLogCollection.CacheTTLSeconds getter implementation
				return 0;
			}
			set
			{
				// TODO:  Add AdminActionLogCollection.CacheTTLSeconds setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				// TODO:  Add AdminActionLogCollection.CacheMode getter implementation
				return new Matchnet.CacheItemMode ();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				// TODO:  Add AdminActionLogCollection.CachePriority getter implementation
				return new Matchnet.CacheItemPriorityLevel ();
			}
			set
			{
				// TODO:  Add AdminActionLogCollection.CachePriority setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			// TODO:  Add AdminActionLogCollection.GetCacheKey implementation
			return null;
		}

		#endregion
	}
}
