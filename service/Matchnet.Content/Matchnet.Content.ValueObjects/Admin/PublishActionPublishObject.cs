using System;
using System.Collections;

namespace Matchnet.Content.ValueObjects.Admin
{
	/// <summary>
	/// Summary description for PublishActionPublishObject.
	/// </summary>
	[Serializable]
	public class PublishActionPublishObject : IValueObject
	{
		#region Private Members
		private Int32 publishActionPublishObjectID;
		private PublishActionTypeEnum publishActionType;
		private PublishObjectType publishObjectType;
		private Int32 workflowOrder;
		private ServiceEnvironmentTypeEnum serviceEnvironmentTypeEnum;
		#endregion

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		public PublishActionPublishObject(Int32 publishActionPublishObjectID, PublishActionTypeEnum publishActionType, PublishObjectType publishObjectType, Int32 workflowOrder, ServiceEnvironmentTypeEnum serviceEnvironmentTypeEnum)
		{
			this.publishActionPublishObjectID = publishActionPublishObjectID;
			this.publishActionType = publishActionType;
			this.publishObjectType = publishObjectType;
			this.workflowOrder = workflowOrder;
			this.serviceEnvironmentTypeEnum = serviceEnvironmentTypeEnum;
		}
		#endregion
		
		#region Public Properties
		/// <summary>
		/// 
		/// </summary>
		public Int32 PublishActionPublishObjectID
		{
			get { return publishActionPublishObjectID; }
			set { publishActionPublishObjectID = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public PublishActionTypeEnum PublishActionType
		{
			get { return publishActionType; }
			set { publishActionType = value; }
		}
		
		/// <summary>
		/// 
		/// </summary>
		public PublishObjectType PublishObjectType
		{
			get { return publishObjectType; }
			set { publishObjectType = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 WorkflowOrder
		{
			get { return workflowOrder; }
			set { workflowOrder = value; }
		}

		/// <summary>
		/// 
		/// </summary>
		public ServiceEnvironmentTypeEnum ServiceEnvironmentTypeEnum
		{
			get { return serviceEnvironmentTypeEnum; }
			set { serviceEnvironmentTypeEnum = value; }
		}
		#endregion
	}
}
