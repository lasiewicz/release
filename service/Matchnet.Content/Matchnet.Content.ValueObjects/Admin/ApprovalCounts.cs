using System;

namespace Matchnet.Content.ValueObjects.Admin
{
	/// <summary>
	/// Summary description for ApprovalCounts.
	/// </summary>
	[Serializable]
	public class ApprovalCounts : IValueObject
	{
		private Int32 _memberID;
		private Int32 _textCount;
		private Int32 _photoCount;
		private Int32 _questionAnswerCount;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="memberID"></param>
		public ApprovalCounts(Int32 memberID)
		{
			_memberID = memberID;
			_textCount = 0;
			_photoCount = 0;
			_questionAnswerCount = 0;
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 MemberID
		{
			get
			{
				return _memberID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 TextCount
		{
			get
			{
				return _textCount;
			}
			set
			{
				_textCount = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 PhotoCount
		{
			get
			{
				return _photoCount;
			}
			set
			{
				_photoCount = value;
			}
		}

		public Int32 QuestionAnswerCount
		{
			get
			{
				return _questionAnswerCount;
			}
			set
			{
				_questionAnswerCount= value;
			}
		}
	}
}
