using System;

using Matchnet;

namespace Matchnet.Content.ValueObjects.Quotas
{
	[Serializable]
	public class QuotaDefinition : IValueObject
	{
		private Int32 _maxAllowed;
		private bool _sliding;
		private Int32 _duration;
		private DurationType _durationType;

		public QuotaDefinition(Int32 maxAllowed,
			bool sliding,
			Int32 duration,
			DurationType durationType)
		{
			_maxAllowed = maxAllowed;
			_sliding = sliding;
			_duration = duration;
			_durationType = durationType;
		}


		public Int32 MaxAllowed
		{
			get
			{
				return _maxAllowed;
			}
		}


		public bool Sliding
		{
			get
			{
				return _sliding;
			}
		}


		public Int32 Duration
		{
			get
			{
				return _duration;
			}
		}


		public DurationType DurationType
		{
			get
			{
				return _durationType;
			}
		}
	}
}
