using System;
using System.Collections;


namespace Matchnet.Content.ValueObjects.Quotas
{
    [Serializable]
    public enum QuotaType : int
    {
        CustomListCategories = 1,
        SentEmail = 2,
        IMList = 3,
        Tease = 4,
        TeaseList = 5,
        EmailList = 6,
        MatchesList = 7,
        Ignore = 8,
        IgnoreList = 9,
        ViewProfileList = 10,
        FavoriteList = 11,
        ReportAbuse = 12,
        MissedIM = 13,
        SentECard = 14,
        SendJmeterInvitation = 15,
        ReceiveJmeterInvite = 16,
        MissedGame = 17,
        MissedOmnidate = 18,
        ExcludeList = 19,
        ExcludeFromList = 20,
        ViewInPushFlirtsList = 21
    };


    [Serializable]
    public class QuotaDefinitions : IValueObject, ICacheable
    {
        public const string CACHE_KEY = "QuotaDefinitions";

        private int _cacheTTLSeconds = 60 * 60;  // Default to 1-hour
        private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;

        private Hashtable _ht;

        public QuotaDefinitions(Hashtable ht)
        {
            _ht = ht;
        }


        public QuotaDefinition GetQuotaDefinition(QuotaType quotaType,
            Int32 groupID)
        {
            QuotaDefinition quotaDefinition = null;

            if (groupID != Constants.NULL_INT)
            {
                quotaDefinition = _ht[quotaType.ToString() + "_" + groupID.ToString()] as QuotaDefinition;
            }

            if (quotaDefinition == null)
            {
                quotaDefinition = _ht[quotaType.ToString()] as QuotaDefinition;
            }

            return quotaDefinition;
        }

        #region ICacheable Members

        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get
            {
                return _cacheTTLSeconds;
            }
            set
            {
                _cacheTTLSeconds = value;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get
            {
                return Matchnet.CacheItemMode.Sliding;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _cachePriority;
            }
            set
            {
                _cachePriority = value;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetCacheKey()
        {
            return CACHE_KEY;
        }
        #endregion
    }
}
