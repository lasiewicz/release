﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.PagePixel
{
    public class PixelRequest
    {
        public int PageID { get; private set; }
        public int SiteID { get; private set; }
        public int? PromotionID { get; set; }
        public int LanguageID { get; set; }
        public string ReferralURL { get; set; }
        public string LuggageID { get; set; }
        public PixelRequestMemberData MemberData { get; private set; }

        public PixelRequest(int pageID, int siteID, int memberID, int regionID, int genderMask)
        {
            PageID = pageID;
            SiteID = siteID;
            MemberData = new PixelRequestMemberData();
            MemberData.MemberID = memberID;
            MemberData.RegionID = regionID;
            MemberData.GenderMask = genderMask;
            MemberData.Birthdate = DateTime.MinValue;
        }

        public class PixelRequestMemberData
        {
            public int MemberID { get; set; }
            public int RegionID { get; set; }
            public int GenderMask { get; set; }
            public DateTime Birthdate { get; set; }
            public string SubscriptionAmount { get; set; }
            public string SubscriptionDuration { get; set; }
        }
    }

    
}
