using System;

using Matchnet.CacheSynchronization.Tracking;

namespace Matchnet.Content.ValueObjects.PagePixel
{
	/// <summary>
	/// Summary description for PagePixelCollection.
	/// </summary>
	[Serializable]
	public class PagePixelCollection : System.Collections.CollectionBase, ICacheable, IReplicable
	{
		private const string PAGE_PIXEL_CACHE_KEY_PREFIX = "~PAGEPIXEL^{0}{1}";

		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		private int _PageID;
		private int _SiteID;

		[NonSerialized]
		private ReferenceTracker _referenceTracker;
		#endregion

		public PagePixelCollection()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pageID"></param>
		/// <param name="siteID"></param>
		public PagePixelCollection(int pageID, int siteID)
		{
			_PageID = pageID;
			_SiteID = siteID;
		}

		/// <summary>
		/// 
		/// </summary>
		public int PageID
		{
			get { return(_PageID); }
		}

		/// <summary>
		/// 
		/// </summary>
		public int SiteID
		{
			get { return(_SiteID); }
		}
		
		/// <summary>
		/// Indexer overload
		/// </summary>
		public PagePixel this[int index]
		{
			get { return ((PagePixel)base.InnerList[index]); }
		}

		/// <summary>
		/// Overloaded add method
		/// </summary>
		/// <param name="pagePixel">banner to add to collection</param>
		/// <returns></returns>
		public int Add(PagePixel pagePixel)
		{
			return base.InnerList.Add(pagePixel);
		}

		/// <summary>
		/// 
		/// </summary>
		public ReferenceTracker ReferenceTracker
		{
			get
			{
				if (_referenceTracker == null)
				{
					_referenceTracker = new ReferenceTracker();
				}

				return _referenceTracker;
			}
		}

		#region ICacheable Members
		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _CacheTTLSeconds;
			}
			set
			{
				_CacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Absolute;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _CachePriority;
			}
			set
			{
				_CachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _CacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_CacheKey = value;
			}
		}

		public static string GetCacheKey(int pageID, int siteID)
		{		
			return string.Format(PAGE_PIXEL_CACHE_KEY_PREFIX, pageID, siteID);
		}
		#endregion

		#region IReplicable Members
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ReplicationPlaceholder GetReplicationPlaceholder()
		{
			return new ReplicationPlaceholder(GetCacheKey());
		}
		#endregion
	}
}
