using System;

namespace Matchnet.Content.ValueObjects.PagePixel
{
	/// <summary>
	/// Summary description for PagePixel.
	/// </summary>
	[Serializable]
	public class PagePixel
	{
		#region Constants
		private const string CACHE_KEY_PREFIX = "~PAGEPIXEL^{0}{1}{2}";
		#endregion

		#region class variables
		private int _CacheTTLSeconds;
		private CacheItemPriorityLevel _CachePriority;
		private string _CacheKey;
		private int pageID = Constants.NULL_INT;
		private int siteID = Constants.NULL_INT;
		private TargetingConditionCollection targetingConditionCollection;
		private string code = Constants.NULL_STRING;
		#endregion

		public PagePixel()
		{
			this.targetingConditionCollection = new TargetingConditionCollection();
		}

		#region Public properties

		/// <summary>
		/// 
		/// </summary>
		public int PageID
		{
			get
			{
				return pageID;
			}
			set
			{
				pageID = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int SiteID
		{
			get
			{
				return siteID;
			}
			set
			{
				siteID = value;
			}
		}

		/// <summary>
		/// Collection of targeting parameters.
		/// </summary>
		public Matchnet.Content.ValueObjects.PagePixel.TargetingConditionCollection TargetingConditionCollection
		{
			get { return targetingConditionCollection; }
			set { targetingConditionCollection = value; }
		}
		/// <summary>
		/// Pixel Content
		/// </summary>
		public string Code
		{
			get { return code; }
			set { code = value; }
		}
		#endregion
	}

	
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class TargetingCondition
	{
		private string name;
		/// <summary>
		/// 
		/// </summary>
		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		private string comparison;
		/// <summary>
		///   
		/// </summary>
		public string Comparison
		{
			get { return comparison; }
			set { comparison = value; }
		}

		private string conditionValue;
		/// <summary>
		/// 
		/// </summary>
		public string ConditionValue
		{
			get { return conditionValue; }
			set { conditionValue = value; }
		}
	}

	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class TargetingConditionCollection : System.Collections.CollectionBase
	{
		/// <summary>
		/// 
		/// </summary>
		public TargetingConditionCollection()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		public TargetingCondition this[int index]
		{
			get { return ((TargetingCondition)base.InnerList[index]); }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="targetingCondition"></param>
		/// <returns></returns>
		public int Add(TargetingCondition targetingCondition)
		{
			return base.InnerList.Add(targetingCondition);
		}
	}
}
