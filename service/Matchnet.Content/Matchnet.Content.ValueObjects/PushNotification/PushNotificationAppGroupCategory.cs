﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Content.ValueObjects.PushNotification
{
    [Serializable]
    public class PushNotificationAppGroupCategory
    {
        public PushNotificationCategory Category { get; set; }
        public AppGroupID AppGroupId { get; set; }
        public int Order { get; set; }
        public string NotificationCategoryTag { get; set; }
        public List<PushNotificationAppGroupCategoryType> NotificationAppGroupCategoryTypes { get; set; }
        
    }
}
