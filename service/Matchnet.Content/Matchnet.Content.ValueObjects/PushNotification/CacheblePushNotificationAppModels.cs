﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Content.ValueObjects.PushNotification
{
    public class CacheblePushNotificationAppGroupModels : ICacheable
    {
        public static string CACHE_KEY = "^PushNotificationAppGroupModels";

        public Dictionary<AppGroupID, PushNotificationAppGroupModel> PushNotificationAppGroupDictionary { get; private set; }

        public CacheblePushNotificationAppGroupModels(Dictionary<AppGroupID, PushNotificationAppGroupModel> pushNotificationAppGroupDictionary)
        {
            PushNotificationAppGroupDictionary = pushNotificationAppGroupDictionary;
        }

        public CacheItemMode CacheMode
        {
            get { return CacheItemMode.Absolute; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get
            {
                return CacheItemPriorityLevel.Normal;
            }
            set { }
        }

        public int CacheTTLSeconds
        {
            get
            {
                return 21600; //6 hours
            }
            set
            {

            }
        }

        public string GetCacheKey()
        {
            return CACHE_KEY;
        }
    }
}
