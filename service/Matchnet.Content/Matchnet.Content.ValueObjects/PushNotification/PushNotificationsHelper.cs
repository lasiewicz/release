﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Content.ValueObjects.PushNotification
{
    public static class PushNotificationsHelper
    {
        public static long GetDefaultEnabledNotificationsValue(PushNotificationAppGroupModel appModel)
        {
            if (appModel == null || appModel.NotificationAppGroupCategories == null)
            {
                throw new ArgumentException("Appmodel can not be null and must have categories");
            }

            long defaultValue = 0;

            foreach (var category in appModel.NotificationAppGroupCategories)
            {
                if (category.NotificationAppGroupCategoryTypes != null)
                {
                    foreach (var notificationType in category.NotificationAppGroupCategoryTypes)
                    {
                        if (notificationType.NotificationType != null)
                        {
                            defaultValue = defaultValue +
                                (long)Math.Pow(2, (double)notificationType.NotificationType.ID);
                        }
                    }
                }
            }

            return defaultValue;
        }

        public static List<PushNotificationTypeID> GetNotificationTypesFromModelForCategory(PushNotificationAppGroupModel appModel, PushNotificationCategoryId categoryId)
        {
            List<PushNotificationTypeID> notificationTypes = null;

            if (appModel == null || appModel.NotificationAppGroupCategories == null)
            {
                throw new ArgumentException("Appmodel can not be null and must have categories");
            }

            var appCategory = (from c in appModel.NotificationAppGroupCategories where c.Category.Id == categoryId select c).FirstOrDefault();
            if (appCategory != null && appCategory.NotificationAppGroupCategoryTypes != null && appCategory.NotificationAppGroupCategoryTypes.Count > 0)
            {
                notificationTypes = new List<PushNotificationTypeID>();
                foreach (var nt in appCategory.NotificationAppGroupCategoryTypes)
                {
                    notificationTypes.Add(nt.NotificationType.ID);
                }
            }

            return notificationTypes;
        }

        public static PushNotificationAppGroupCategory GetCategoryForNotificationType(PushNotificationAppGroupModel appModel, PushNotificationTypeID notificationTypeId)
        {
            if (appModel == null || appModel.NotificationAppGroupCategories == null)
            {
                throw new ArgumentException("Appmodel can not be null and must have categories");
            }

            foreach (var category in appModel.NotificationAppGroupCategories)
            {
                if ((from pn in category.NotificationAppGroupCategoryTypes where pn.NotificationType.ID == notificationTypeId select pn).Count() > 0)
                {
                    return category;
                }
            }

            return null;
        }

        public static PushNotificationAppGroupCategoryType GetAppTypeForNotificationType(PushNotificationAppGroupModel appModel, PushNotificationTypeID notificationTypeId)
        {
            PushNotificationAppGroupCategory appCategory = GetCategoryForNotificationType(appModel, notificationTypeId);
            if (appCategory != null && appCategory.NotificationAppGroupCategoryTypes != null)
            {
                foreach (var nt in appCategory.NotificationAppGroupCategoryTypes)
                {
                    if (nt.NotificationType.ID == notificationTypeId)
                    {
                        return nt;
                    }
                }
            }


            return null;
        }
    }
}