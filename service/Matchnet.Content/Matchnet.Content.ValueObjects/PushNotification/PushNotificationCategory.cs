﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Content.ValueObjects.PushNotification
{
    public enum PushNotificationCategoryId
    {
        Activity = 1,
        Photos = 2,
        Promotional = 3
    }

    [Serializable]
    public class PushNotificationCategory 
    {
        public PushNotificationCategoryId Id { get; set; }
        public string Name { get; set; }
        public string TitleResourceKey { get; set; }
        public string DescriptionResourceKey { get; set; }
    }
}
