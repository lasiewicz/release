﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Content.ValueObjects.PushNotification
{
    public enum AppGroupID
    {
        JDateAppGroup=1
    }
        
    [Serializable]
    public class PushNotificationAppGroupModel
    {
        public AppGroupID AppGroupId { get; set; }
        public List<PushNotificationAppGroupCategory> NotificationAppGroupCategories { get; set; }
        public List<PushNotificationType> NotificationTypes { get; set; }
    }
}
