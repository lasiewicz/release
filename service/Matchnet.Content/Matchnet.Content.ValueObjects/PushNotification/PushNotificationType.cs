﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Content.ValueObjects.PushNotification
{
    public enum PushNotificationTypeID
    {
        MessageReceived = 1,
        ChatRequest = 2,
        MutualYes = 3,
        PhotoApproved = 4,
        PhotoRejected = 5,
        SubscriptionLapse = 6,
        PromotionalAnnouncement = 7,
        ChatRequestV2 = 8,
        ChatRequestV3 = 9
    }

    [Serializable]
    public class PushNotificationType 
    {
        public PushNotificationTypeID ID { get; set; }
        public string Name { get; set; }
        public string TitleResourceKey { get; set; }
        public string DescriptionResourceKey { get; set; }
        public string NotificationTextResourceKey { get; set; }
    }
}
