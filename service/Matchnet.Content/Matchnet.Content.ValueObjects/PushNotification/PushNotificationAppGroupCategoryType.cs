﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Content.ValueObjects.PushNotification
{
    [Serializable]
    public class PushNotificationAppGroupCategoryType
    {
        public PushNotificationAppGroupCategory AppGroupCategory { get; set; }
        public PushNotificationType NotificationType { get; set; }
        public AppGroupID AppGroupId { get; set; }
        public int Order { get; set; }
        public string Title { get; set; }
        public string DefaultValue { get; set; }
        public string DeepLink { get; set; }
        public int BadgeIncrementValue { get; set; }
    }
}
