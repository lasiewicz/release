using System;

namespace Matchnet.Content.ValueObjects.PageConfig
{
	/// <summary>
	/// Each logical page on the site can have a different AnalyticsPageID and thus a different AnalyticsName.
	/// </summary>
	[Serializable]
	public class AnalyticsPage : ICacheable
	{
		#region Constants
		private const String ANALYTICSNAME_CACHE_KEY_PREFIX = "~ANALYTICSNAME^{0}";
		#endregion

		private Int32 _cacheTTLSeconds;
		private CacheItemPriorityLevel _cachePriority;
		private String _cacheKey;
		private Int32 _analyticsPageID;
		private String _analyticsName = Constants.NULL_STRING;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="analyticsPageID"></param>
		public AnalyticsPage(Int32 analyticsPageID)
		{
			_analyticsPageID = analyticsPageID;

			_cacheKey = GetCacheKey(analyticsPageID);
		}

		/// <summary>
		/// 
		/// </summary>
		public Int32 AnalyticsPageID
		{
			get
			{
				return _analyticsPageID;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public String AnalyticsName
		{
			get
			{
				return _analyticsName;
			}
			set
			{
				_analyticsName = value;
			}
		}

		#region ICacheable Members
		/// <summary>
		/// 
		/// </summary>
		/// <param name="analyticsPageID"></param>
		/// <returns></returns>
		public static String GetCacheKey(Int32 analyticsPageID)
		{		
			return String.Format(ANALYTICSNAME_CACHE_KEY_PREFIX, analyticsPageID.ToString());
		}

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return CacheItemMode.Sliding;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return _cacheKey;
		}

		/// <summary>
		/// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
		/// </summary>
		public string SetCacheKey
		{
			set
			{
				_cacheKey = value;
			}
		}
		#endregion
	}
}
