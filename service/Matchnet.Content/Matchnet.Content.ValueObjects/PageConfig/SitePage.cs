using System;

namespace Matchnet.Content.ValueObjects.PageConfig
{
	/// <summary>
	/// Defines the possible security permissions required to access page. The Presentation Layer uses this information to determine
	/// when to present a login or Subscribe page to unauthenticated users in place of the requested page.
	/// </summary>
	[Serializable, Flags]
	public enum SecurityMask : int
	{
		/// <summary>
		/// Page is only accessible to authenticated Members and Subscribers.
		/// </summary>
		RequiresLogin = 1,

		/// <summary>
		/// Page is only accessible to authenticated Subscribers.
		/// THIS IS NOT CORRECT
		/// It appears as though 2 is unused in the db, and 3 represents Admin rights.  It doesnt seem like there is a way to auth by reg and not sub.
		/// </summary>
		RequiresPrivilege = 2,

		/// <summary>
		/// PRELIMINARY: Some pages are accessible to authenticated Members only if the Member has completed a predetermined number of registration steps.
		/// In that case OverrideRegistration will be set. 
		/// </summary>
		OverrideRegistration = 4,

		/// <summary>
		/// AutoLogin - to define page that should ignore auto login rules. For example, subscription pages should *always* require a hard login.
		/// </summary>
		RequiresHardLogin = 8
	}

	/// <summary>
	/// 
	/// </summary>
	[Serializable, Flags]
	public enum LayoutTemplate : int
	{
		/// <summary>
		/// 
		/// </summary>
		None = Constants.NULL_INT,
		/// <summary>
		/// 
		/// </summary>
		Standard = 1,
		/// <summary>
		/// 
		/// </summary>
		Popup = 2,
		/// <summary>
		/// 
		/// </summary>
		PopupOnce = 3,
		/// <summary>
		/// 
		/// </summary>
		Simple = 4,
		/// <summary>
		/// 
		/// </summary>
		Binary = 7,
		/// <summary>
		/// 
		/// </summary>
		Plain = 8,
		/// <summary>
		/// 
		/// </summary>
		Frameset = 9,
		/// <summary>
		/// 
		/// </summary>
		LandingPage = 10,
		/// <summary>
		/// 
		/// </summary>
		PopupProfile = 11,
		/// <summary>
		/// 
		/// </summary>
		StandardAllowFrames = 12, 
		/// <summary>
		/// 
		/// </summary>
		SEOPages = 13,
		/// <summary>
		/// 
		/// </summary>
		Wide = 14,
		Wide2Columns =15,
		WideSingleColumn =16,
		WideSimple =17,
		WidePopup=18,
		WideReset=19,
		NRGStandard=20, 
		IFrame = 21,
        Wide2ColumnsJMeter = 22,
        WideT = 23,
        FullWidthColumn = 24
	}


	/// <summary>
	/// Representation of site to page WebUserControl mapping/configuration
	/// </summary>
	[Serializable]
	public class SitePage
	{
		private Int32 _siteID;
		private string _controlName;
		private SecurityMask _securityMask;
		private string _layoutTemplateName;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteID"></param>
		/// <param name="controlName"></param>
		/// <param name="securityMask"></param>
		/// <param name="layoutTemplateName"></param>
		public SitePage(Int32 siteID,
			string controlName,
			SecurityMask securityMask,
			string layoutTemplateName)
		{
			_siteID = siteID;
			_controlName = controlName;
			_securityMask = securityMask;
			_layoutTemplateName = layoutTemplateName;
		}


		/// <summary>
		/// Site identifier
		/// </summary>
		public Int32 SiteID
		{
			get
			{
				return _siteID;
			}
		}


		/// <summary>
		/// Filename of WebUserControl that renders page. Does not include path or file extension.
		/// </summary>
		public string ControlName
		{
			get
			{
				return _controlName;
			}
		}


		/// <summary>
		/// Represents security permissions required to access page 
		/// </summary>
		public SecurityMask SecurityMask
		{
			get
			{
				return _securityMask;
			}
		}


		/// <summary>
		/// Page layout template control name.
		/// </summary>
		public string LayoutTemplateName
		{
			get
			{
				return _layoutTemplateName;
			}
		}
	}
}
