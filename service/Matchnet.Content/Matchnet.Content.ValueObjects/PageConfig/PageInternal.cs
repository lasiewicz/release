using System;
using System.Collections;


namespace Matchnet.Content.ValueObjects.PageConfig
{	
	/// <summary>
	/// Internal representation of web Page configuration, contains a collection of site to page mappings
	/// </summary>
	[Serializable]
	public class PageInternal
	{
		private Int32 _id;
		private string _name;
		private string _resourceConstant;
		private Hashtable _sitePages;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <param name="name"></param>
		/// <param name="resourceConstant"></param>
		public PageInternal(Int32 id, string name, string resourceConstant)
		{
			_sitePages = new Hashtable();

			_id = id;
			_name = name.ToLower();
			_resourceConstant = resourceConstant;
		}


		/// <summary>
		/// Adds a SitePage object to the collection
		/// </summary>
		/// <param name="sitePage"></param>
		public void AddSitePage(SitePage sitePage)
		{
			_sitePages.Add(sitePage.SiteID, sitePage);
		}

		
		/// <summary>
		/// Retreives a SitePage for a given SiteID
		/// </summary>
		/// <param name="siteID"></param>
		/// <returns></returns>
		public SitePage GetSitePage(Int32 siteID)
		{
			return _sitePages[siteID] as SitePage;
		}


		/// <summary>
		/// Page identifier
		/// </summary>
		public Int32 ID
		{
			get
			{
				return _id;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
		}


		/// <summary>
		/// Key of resource that represents textual description of page
		/// </summary>
		public string ResourceConstant
		{
			get
			{
				return _resourceConstant;
			}
		}
	}
}
