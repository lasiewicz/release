using System;
using System.Collections;

namespace Matchnet.Content.ValueObjects.PageConfig
{
	/// <summary>
	/// Internal representation of web App configuration, contains a collection of page configuration objects
	/// </summary>
	[Serializable]
	public class AppInternal
	{
		private int			_id;
		private string		_path;
		private string		_resourceConstant;
		private string		_name;
		private string		_defaultPagePath;
		private Hashtable	_pages;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <param name="path"></param>
		/// <param name="resourceConstant"></param>
		/// <param name="name"></param>
		public AppInternal(Int32 id, string path, string resourceConstant, string name)
		{
			_pages = new Hashtable();
			_id = id;
			_path = AppInternal.CleanPath(path);
			_resourceConstant = resourceConstant;
			_name = name;
		}


		/// <summary>
		/// Adds a PageInternal object to the collection
		/// </summary>
		/// <param name="pageInternal"></param>
		public void AddPage(PageInternal pageInternal)
		{
			_pages.Add(pageInternal.Name, pageInternal);
		}


		/// <summary>
		/// The full path (including application path) for the default page of this application.
		/// </summary>
		public string DefaultPagePath
		{
			get
			{
				return _defaultPagePath;
			}
		}

		/// <summary>
		/// Retreives a PageInteral object by name (i.e. SearchResults)
		/// </summary>
		/// <param name="pageName"></param>
		/// <returns></returns>
		public PageInternal GetPage(string pageName)
		{
			return _pages[pageName.ToLower()] as PageInternal;
		}


		/// <summary>
		/// App identifier
		/// </summary>
		public Int32 ID
		{
			get
			{
				return _id;
			}
		}


		/// <summary>
		/// App file path (i.e. "/Applications/Search Results")
		/// </summary>
		public string Path
		{
			get
			{
				return _path;
			}
		}


		/// <summary>
		/// App resource constant, corresponds to text description of application (i.e. "Search Results")
		/// </summary>
		public string ResourceConstant
		{
			get
			{
				return _resourceConstant;
			}
		}

		/// <summary>
		/// Application name (used in admin for dropdown list of applications)
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
		}

		/// <summary>
		/// Sets the full application and page path for the Page object that corresponds to the PageID of the 
		/// supplied parameter. (i.e. Applications/MemberServices/DefaultPage.aspx)
		/// </summary>
		/// <param name="pPageID"></param>
		public void SetPageDefault(int pPageID)
		{
			// _pages is keyed by the Page name which the caller of this method does not have visibility to
			// we must iterate the collection looking at the ID of each object until we find a match or reach the end.
			foreach (PageInternal page in _pages.Values)
			{
				if (page.ID == pPageID)
				{
					// set the default page path on this application object.
					_defaultPagePath = _path + "/" + page.Name + ".aspx";
				}
			}
		}

		/// <summary>
		/// Removes trailing slash from path and coverts to lower case
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		internal static string CleanPath(string path)
		{
			if (path.EndsWith("/"))
			{
				path = path.Substring(0, path.Length - 1);
			}

			return path.ToLower();
		}
	}
}
