using System;
using System.Collections;


namespace Matchnet.Content.ValueObjects.PageConfig
{
	/// <summary>
	/// Collection of internal App/Page/SitePage objects used to map a uri path to a WebUserControl
	/// </summary>
	[Serializable]
	public class SitePages : IValueObject, ICacheable
	{
		/// <summary>
		/// 
		/// </summary>
		public const string CACHE_KEY = "SitePages";

		private int _cacheTTLSeconds = 60 * 60;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Hashtable _appsID;
		private Hashtable _appsPath;

		/// <summary>
		/// 
		/// </summary>
		public SitePages()
		{
			_appsID = new Hashtable();
			_appsPath = new Hashtable();
		}


		/// <summary>
		/// Adds an App (and child objects) to the collection
		/// </summary>
		/// <param name="appInternal"></param>
		public void AddApp(AppInternal appInternal)
		{
			_appsID.Add(appInternal.ID, appInternal);
			_appsPath.Add(appInternal.Path, appInternal);
		}


		/// <summary>
		/// Retreives an App for a given App ID
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public AppInternal GetApp(Int32 id)
		{
			return _appsID[id] as AppInternal;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAppID"></param>
		/// <param name="pPageID"></param>
		public void SetPageDefault(int pAppID, int pPageID)
		{
			AppInternal app = _appsID[pAppID] as AppInternal;
			app.SetPageDefault(pPageID);
		}

		/// <summary>
		/// Retreives an App for a given uri path
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public AppInternal GetApp(string path)
		{
			return _appsPath[AppInternal.CleanPath(path)] as AppInternal;
		}


		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				return _cacheTTLSeconds;
			}
			set
			{
				_cacheTTLSeconds = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				return Matchnet.CacheItemMode.Sliding;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				return _cachePriority;
			}
			set
			{
				_cachePriority = value;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return CACHE_KEY;
		}

		#endregion
	}
}
