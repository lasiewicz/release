using System;

namespace Matchnet.Content.ValueObjects
{
	/// <summary>
	/// 
	/// </summary>
	public class ServiceConstants
	{
		/// <summary>
		/// 
		/// </summary>
		public const string SERVICE_CONSTANT = "CONTENT_SVC";
		/// <summary>
		/// 
		/// </summary>
		public const string SERVICE_NAME = "Matching.Content.Service";

		private ServiceConstants()
		{
		}
	}
}
