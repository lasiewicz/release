﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.Photos
{
    [Serializable]
    public class CachedPhotoFileTypeRecords: ICacheable
    {
        #region ICacheable Members

        private int _cacheTTLSeconds = 3600;
        public static string CacheKey = "^PhotoFileTypeRecords";

        public CacheItemMode CacheMode
        {
            get { return CacheItemMode.Absolute; }
        }

        public CacheItemPriorityLevel CachePriority
        {
            get { return CacheItemPriorityLevel.Normal; }
            set { }
        }

        public int CacheTTLSeconds
        {
            get
            {
                return _cacheTTLSeconds;
            }
            set { }
        }

        public string GetCacheKey()
        {
            return CacheKey;
        }

        #endregion

        public List<PhotoFileTypeRecord> PhotoFileTypeRecords { get; set; } 
    }
}
