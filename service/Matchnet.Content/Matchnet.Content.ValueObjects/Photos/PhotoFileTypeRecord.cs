﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matchnet.Content.ValueObjects.Photos
{
    public enum BasicPhotoFileType
    {
        FWS=1, 
        Thumbnail=2,
        Original=3
    }
    
    [Serializable]
    public class PhotoFileTypeRecord
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public int MaxWidth { get; set; }
        public int MaxHeight { get; set; }
        public bool Active { get; set; }

        public List<PhotoFileTypeSiteRecord> SiteRecords { get; set; }

        public PhotoFileTypeRecord()
        {
            SiteRecords = new List<PhotoFileTypeSiteRecord>();
        }
    }
}
