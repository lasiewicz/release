﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Content.ValueObjects.Photos
{
    [Serializable]
    public class PhotoFileTypeSiteRecord
    {
        public int SiteId { get; set; }
        public int MaxWidth { get; set; }
        public int MaxHeight { get; set; }
        public bool Active { get; set; }
    }
}
