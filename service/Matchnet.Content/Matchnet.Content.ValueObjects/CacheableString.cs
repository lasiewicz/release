using System;

namespace Matchnet.Content.ValueObjects
{
	/// <summary>
	/// Summary description for CacheableString.
	/// </summary>
	public class CacheableString : ICacheable
	{
		private string _myValue;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="myValue"></param>
		public CacheableString(string myValue)
		{
			_myValue = myValue;
		}

		/// <summary>
		/// 
		/// </summary>
		public string MyValue
		{
			get { return(_myValue); }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return(_myValue);
		}


		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get
			{
				// TODO:  Add CacheableString.CacheTTLSeconds getter implementation
				return 0;
			}
			set
			{
				// TODO:  Add CacheableString.CacheTTLSeconds setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get
			{
				// TODO:  Add CacheableString.CacheMode getter implementation
				return new Matchnet.CacheItemMode ();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get
			{
				// TODO:  Add CacheableString.CachePriority getter implementation
				return new Matchnet.CacheItemPriorityLevel ();
			}
			set
			{
				// TODO:  Add CacheableString.CachePriority setter implementation
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			// TODO:  Add CacheableString.GetCacheKey implementation
			return null;
		}

		#endregion
	}
}
