using System;
using System.Collections;

using Matchnet;

namespace Matchnet.Content.ValueObjects.Events
{
	/// <summary>
	/// Summary description for EventCollection.
	/// </summary>
	[Serializable]
	public class EventCollection  :  System.Collections.CollectionBase, IValueObject, ICacheable
	{
		#region Private Members
		private int _cacheTTLSeconds = 60 * 60;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Sliding;

		private int _communityID = Constants.NULL_INT;
		private Event.EVENT_TYPE _eventType;
		private bool _albumEvent;
		#endregion

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		public EventCollection()
		{

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="eventType"></param>
		public EventCollection(int communityID, Event.EVENT_TYPE eventType, bool albumEvent)
		{
			_communityID = communityID;
			_eventType = eventType;
			_albumEvent = albumEvent;
		}
		#endregion

		#region Public Methods
		/// <summary>
		/// 
		/// </summary>
		public Event this[int index]
		{
			get{return (Event)base.InnerList[index];}
						
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="oEvent"></param>
		/// <returns></returns>
		public int Add(Event oEvent)
		{
			return base.InnerList.Add(oEvent);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="eventID"></param>
		/// <returns></returns>
		public Event FindByID(int eventID)
		{
			foreach(Event oEvent in this)
			{
				if(oEvent.EventID == eventID)
				{
					return oEvent;
				}
			}

			// Returns null if the specified Event cannot be found in the collection
			return null;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return EventCollection.GetCacheKey(_communityID, _eventType, _albumEvent);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="eventType"></param>
		/// <param name="albumEvent"></param>
		/// <returns></returns>
		public static string GetCacheKey(int communityID, Event.EVENT_TYPE eventType, bool albumEvent)
		{
			return "~EventCollection^" + communityID.ToString() + eventType.ToString() + albumEvent.ToString();
		}
		#endregion

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get {return _cacheTTLSeconds;}
			set {_cacheTTLSeconds = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get {return _cacheItemMode;}
			set {_cacheItemMode = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get {return _cachePriority;}
			set {_cachePriority = value;}
		}

		#endregion

	}
}
