using System;
using Matchnet;

namespace Matchnet.Content.ValueObjects.Events
{
	/// <summary>
	/// Summary description for HolidayDetail.
	/// </summary>
	[Serializable]
	public class HolidayDetail : IValueObject, ICacheable
	{
		private int _cacheTTLSeconds = 60 * 60;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Sliding;

		private int holidayID ;
		private int holidayDetailID;
		private string holidayDescription="";
		private DateTime holidayDate;
		
		public HolidayDetail()
		{
		}

		public HolidayDetail(int holidayid, int holidaydetailid, string holidaydesc, DateTime holidaydate)
		{
			holidayID=holidayid;
			holidayDetailID=holidaydetailid;
			holidayDescription=holidaydesc;
			holidayDate=holidaydate;
		}

		#region properties

		public int HolidayID
		{
			get{return holidayID;}
			set{holidayID=value;}
		}

		public int HolidayDetailID
		{
			get{return holidayDetailID;}
			set{holidayDetailID=value;}
		}
		public string HolidayDescription
		{
			get{return holidayDescription;}
			set{holidayDescription=value;}
		}

		public DateTime HolidayDate
		{
			get{return holidayDate;}
			set{holidayDate=value;}
		}

		public string CollectionKey
		{get 
		 { return String.Format("{0:d}",HolidayDate);
		 }
		}
		#endregion

		
		#region ICacheable Members
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return GetCacheKey(holidayDetailID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="eventID"></param>
		/// <returns></returns>
		public static string GetCacheKey(int detailID)
		{
			return "~Holiday^" + detailID.ToString();
		}

			/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get {return _cacheTTLSeconds;}
			set {_cacheTTLSeconds = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get {return _cacheItemMode;}
			set {_cacheItemMode = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get {return _cachePriority;}
			set {_cachePriority = value;}
		}

		#endregion

	}
}
