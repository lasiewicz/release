using System;

using Matchnet;

namespace Matchnet.Content.ValueObjects.Events
{
	/// <summary>
	/// Summary description for Event.
	/// </summary>
	[Serializable]
	public class Event : IValueObject, ICacheable
	{
		#region Private Members
		private int _cacheTTLSeconds = 60 * 60;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Sliding;

		private int _eventID = Constants.NULL_INT;
		private EVENT_TYPE _eventType = EVENT_TYPE.None;
		private string _description = Constants.NULL_STRING;
		private string _location = Constants.NULL_STRING;
		private string _eventDateText = Constants.NULL_STRING;
		private System.DateTime _eventDate = System.DateTime.MinValue;
		private string _eventThumbPath = Constants.NULL_STRING;
		private int _communityID = Constants.NULL_INT;
		private int _listOrderEvent = Constants.NULL_INT;
		private bool _publishedFlag = false;
		private System.DateTime _insertedDate = System.DateTime.MinValue;
		private string _eventExtraInfo = Constants.NULL_STRING;
		private string _content = Constants.NULL_STRING;
		private int _listOrderAlbum = Constants.NULL_INT;
		private bool _albumPublishedFlag = false;
		private string _albumThumbPath = Constants.NULL_STRING;
		private string _albumExtraInfo = Constants.NULL_STRING;
		private string _albumContent = Constants.NULL_STRING;

		#endregion

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		public Event()
		{
			
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="eventID"></param>
		public Event(int eventID)
		{
			_eventID = eventID;
		}
		#endregion

		#region Enumerators
		/// <summary>
		/// 
		/// </summary>
		public enum EVENT_TYPE 
		{
			/// <summary> </summary>
			None = 0,
			/// <summary> </summary>
			Party = 1,
			/// <summary> </summary>
			Travel = 2
		}
		#endregion

		#region Properties
		/// <summary>
		/// 
		/// </summary>
		public int EventID 
		{
			get{return _eventID;}
			set{_eventID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public EVENT_TYPE EventType 
		{
			get{return _eventType;}
			set{_eventType = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Description
		{
			get{return _description;}
			set{_description = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Location
		{
			get{return _location;}
			set{_location = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string EventDateText
		{
			get{return _eventDateText;}
			set{_eventDateText = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public System.DateTime EventDate
		{
			get{return _eventDate;}
			set{_eventDate = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string EventThumbPath
		{
			get{return _eventThumbPath;}
			set{_eventThumbPath = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public int CommunityID
		{
			get{return _communityID;}
			set{_communityID = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public int ListOrderEvent
		{
			get{return _listOrderEvent;}
			set{_listOrderEvent = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool PublishedFlag
		{
			get{return _publishedFlag;}
			set{_publishedFlag = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public System.DateTime InsertedDate
		{
			get{return _insertedDate;}
			set{_insertedDate = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string EventExtraInfo
		{
			get{return _eventExtraInfo;}
			set{_eventExtraInfo = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Content
		{
			get{return _content;}
			set{_content = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public int ListOrderAlbum
		{
			get{return _listOrderAlbum;}
			set{_listOrderAlbum = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool AlbumPublishedFlag
		{
			get{return _albumPublishedFlag;}
			set{_albumPublishedFlag = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string AlbumThumbPath
		{
			get{return _albumThumbPath;}
			set{_albumThumbPath = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public string AlbumExtraInfo
		{
			get{return _albumExtraInfo;}
			set{_albumExtraInfo = value;}
		}
		
		/// <summary>
		/// 
		/// </summary>
		public string AlbumContent
		{
			get{return _albumContent;}
			set{_albumContent = value;}
		}

		#endregion

		#region Public Methods
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public string GetCacheKey()
		{
			return Event.GetCacheKey(_eventID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="eventID"></param>
		/// <returns></returns>
		public static string GetCacheKey(int eventID)
		{
			return "~Event^" + eventID.ToString();
		}
		#endregion

		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get {return _cacheTTLSeconds;}
			set {_cacheTTLSeconds = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get {return _cacheItemMode;}
			set {_cacheItemMode = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get {return _cachePriority;}
			set {_cachePriority = value;}
		}

		#endregion
	}
}
