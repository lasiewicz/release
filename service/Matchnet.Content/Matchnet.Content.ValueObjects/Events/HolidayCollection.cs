using System;

namespace Matchnet.Content.ValueObjects.Events
{
	/// <summary>
	/// Summary description for HolidayCollection.
	/// </summary>
	[Serializable]
	public class HolidayCollection  :  System.Collections.DictionaryBase, IValueObject, ICacheable
	{
		private const string  CACHE_KEY ="HolidayCollection^";
		private int startingYear=0;

		private int _cacheTTLSeconds = 60 * 60;
		private Matchnet.CacheItemPriorityLevel _cachePriority = Matchnet.CacheItemPriorityLevel.Normal;
		private Matchnet.CacheItemMode _cacheItemMode = Matchnet.CacheItemMode.Sliding;

		public HolidayCollection()
		{
		
		}

		public int StartingYear
		{
			get
			{	if(startingYear==0)
					startingYear=DateTime.Now.Year;
				return startingYear;
			}
			set{
				if(value < 1900 || value > 2079)
					startingYear=DateTime.Now.Year;
				else
					startingYear=value;
			
			}

		}

		public HolidayDetail this[string key]
		{
			get{return (HolidayDetail)base.Dictionary[key];}
						
		}

	
		public void Add(HolidayDetail oHoliday)
		{
			try
			{
				string key=oHoliday.CollectionKey;
				base.Dictionary.Add(key, oHoliday);
			}
			catch(Exception ex)
			{
				
			}
		}

		public bool Contains(DateTime key)
		{
				bool res=false;
			try
			{  string skey=String.Format("{0:d}",key);
				return (this[skey] != null);
			}
			catch(Exception ex)
			{return res;}
		}

		public bool Contains(string key)
		{ bool res=false;
			try
			{
			return (this[key] != null);
			}
			catch(Exception ex)
			{return res;}
		}

		public string GetCacheKey()
		{
			return GetCacheKey(startingYear);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="eventType"></param>
		/// <param name="albumEvent"></param>
		/// <returns></returns>
		public static string GetCacheKey(int year)
		{
			return CACHE_KEY + year.ToString() ;
		}

		
		
		#region ICacheable Members

		/// <summary>
		/// 
		/// </summary>
		public int CacheTTLSeconds
		{
			get {return _cacheTTLSeconds;}
			set {_cacheTTLSeconds = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemMode CacheMode
		{
			get {return _cacheItemMode;}
			set {_cacheItemMode = value;}
		}

		/// <summary>
		/// 
		/// </summary>
		public Matchnet.CacheItemPriorityLevel CachePriority
		{
			get {return _cachePriority;}
			set {_cachePriority = value;}
		}
		#endregion

	}
}
