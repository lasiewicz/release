using System;

using Matchnet.Content.ValueObjects.PageConfig;


namespace Matchnet.Content.ServiceAdapters
{
	/// <summary>
	/// Site-specific representation of a web page configuration
	/// </summary>
	public class Page
	{
		private App _app;
		private Int32 _id;
		private string _controlName;
		private string _resourceConstant;
		private SecurityMask _securityMask;
		private string _layoutTemplateName;
	    private string _pageName;

		/// <summary>
		/// Constructor, creates a Page object from App/PageInternal/SiteInternal objects
		/// </summary>
		/// <param name="app">App value object</param>
		/// <param name="pageInternal">PageInternal value object</param>
		/// <param name="sitePage">SitePage value object</param>
		public Page(App app,
			PageInternal pageInternal,
			SitePage sitePage)
		{
			_app = app;
			_id = pageInternal.ID;
			_controlName = sitePage.ControlName;
			_resourceConstant = pageInternal.ResourceConstant;
			_securityMask = sitePage.SecurityMask;
			_layoutTemplateName = sitePage.LayoutTemplateName;
		    _pageName = pageInternal.Name;
		}


		/// <summary>
		/// App object
		/// </summary>
		public App App
		{
			get
			{
				return _app;
			}
		}


		/// <summary>
		/// PageID
		/// </summary>
		public Int32 ID
		{
			get
			{
				return _id;
			}
		}


		/// <summary>
		/// Filename of WebUserControl that renders page. Does not include path or file extension.
		/// </summary>
		public string ControlName
		{
			get
			{
				return _controlName;
			}
		}


		/// <summary>
		/// Key of resource that represents textual description of page
		/// </summary>
		public string ResourceConstant
		{
			get
			{
				return _resourceConstant;
			}
		}

		/// <summary>
		/// Represents security permissions required to access page 
		/// </summary>
		public SecurityMask SecurityMask
		{
			get
			{
				return _securityMask;
			}
		}


		/// <summary>
		/// Page layout template control name.
		/// </summary>
		public string LayoutTemplateName
		{
			get
			{
				return _layoutTemplateName;
			}
		}

        /// <summary>
        /// Page name.
        /// </summary>
        public string PageName
        {
            get
            {
                return _pageName;
            }
        }
        
        /// <summary>
		/// 
		/// </summary>
		/// <param name="pLayoutTemplateName"></param>
		/// <returns></returns>
		public static string GetFullLayoutTemplatePath(string pLayoutTemplateName)
		{
			return "/LayoutTemplates/" + pLayoutTemplateName + ".ascx";
		}

		/// <summary>
		/// Retreives full LayoutTemplate control path including extension.
		/// </summary>
		/// <returns>/LayoutTemplates/[LayoutTemplateName].ascx</returns>
		public string GetFullLayoutTemplatePath()
		{
			return GetFullLayoutTemplatePath(this.LayoutTemplateName);
		}

		/// <summary>
		/// Retreives full NotificationTemplate control path including extension.
		/// </summary>
		/// <returns>/LayoutTemplates/[LayoutTemplateName]Notification.ascx</returns>
		public string GetFullNotificationTemplatePath()
		{
			return "/LayoutTemplates/" + this.LayoutTemplateName + "Notification" + ".ascx";
		}
		
		/// <summary>
		/// Retreives full App control path including extension.
		/// </summary>
		/// <returns></returns>
		public string GetFullControlPath()
		{
			return this.App.Path + "/" + this.ControlName + ".ascx";
		}
	}
}
