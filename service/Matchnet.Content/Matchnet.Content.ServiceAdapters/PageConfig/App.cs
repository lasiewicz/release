using System;

using Matchnet.Content.ValueObjects.PageConfig;


namespace Matchnet.Content.ServiceAdapters
{
	/// <summary>
	/// Representation of a web App configuration
	/// </summary>
	public class App
	{
		private Int32 _id;
		private string _path;
		private string _resourceConstant;
		private string _defaultPagePath;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="appInternal">AppInternal</param>
		public App(AppInternal appInternal)
		{
			_id = appInternal.ID;
			_path = appInternal.Path;
			_resourceConstant = appInternal.ResourceConstant;
			_defaultPagePath = appInternal.DefaultPagePath;
		}


		/// <summary>
		/// App ID, corresponds to record in mnSystem.dbo.App
		/// </summary>
		public Int32 ID
		{
			get
			{
				return _id;
			}
		}


		/// <summary>
		/// App file path (i.e. "/Applications/Search Results")
		/// </summary>
		public string Path
		{
			get
			{
				return _path;
			}
		}

		/// <summary>
		/// Default file path for the default page of this application (i.e. /Applications/MemberServices/default.aspx")
		/// </summary>
		public string DefaultPagePath
		{
			get
			{
				return _defaultPagePath;
			}
		}

		/// <summary>
		/// App resource key, corresponds to text description of application (i.e. "Search Results")
		/// </summary>
		public string ResourceConstant
		{
			get
			{
				return _resourceConstant;
			}
		}
	}
}
