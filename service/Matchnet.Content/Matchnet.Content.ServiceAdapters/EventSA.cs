using System;
using System.Web;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Proxies;

using Matchnet;
using Matchnet.Caching;
using Matchnet.RemotingClient;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Exceptions;
using Matchnet.Content.ValueObjects.Events;
using Matchnet.Content.ValueObjects.ServiceDefinitions;

namespace Matchnet.Content.ServiceAdapters
{
	/// <summary>
	/// Summary description for EventSA.
	/// </summary>
	public class EventSA : SABase
	{

		
		#region Private Members
		private const string SERVICE_MANAGER_NAME = "EventSM";
		
		private Cache _cache;
		#endregion

		#region Constructors
		/// <summary>
		/// 
		/// </summary>
		public static readonly EventSA Instance = new EventSA();

		private EventSA()
		{
			_cache = Cache.Instance;
		}
		#endregion

		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_SA_CONNECTION_LIMIT"));
		}

		#region Public Methods
		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="orderBy"></param>
		/// <param name="sorting"></param>
		/// <param name="startRow"></param>
		/// <param name="pageSize"></param>
		/// <param name="publishFlag"></param>
		/// <param name="eventType"></param>
		/// <returns></returns>
		public EventCollection GetEvents(Int32 communityID,
			string orderBy,
			string sorting,
			int startRow,
			int pageSize,
			string publishFlag,
			Event.EVENT_TYPE eventType)
		{
			string uri = "";

			try
			{
				EventCollection Result = null;

				Result = _cache.Get(EventCollection.GetCacheKey(communityID, eventType, false)) as EventCollection;

				if (Result == null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						Result = getService(uri).GetEvents(communityID, orderBy, sorting, startRow, pageSize, publishFlag, eventType);
					}
					finally
					{
						base.Checkin(uri);
					}
				}

				return Result;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve events (uri: " + uri + ")"+ ex.Message, ex));
			}			
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="eventType"></param>
		/// <returns></returns>
		public EventCollection GetEvents(Int32 communityID,
			Event.EVENT_TYPE eventType)
		{
			string uri = "";

			try
			{
				EventCollection Result = null;

				Result = _cache.Get(EventCollection.GetCacheKey(communityID, eventType, false)) as EventCollection;

				if (Result == null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						Result = getService(uri).GetEvents(communityID, eventType);
					}
					finally
					{
						base.Checkin(uri);
					}
				}

				return Result;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve events (uri: " + uri + ")"+ ex.Message, ex));
			}			
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <returns></returns>
		public EventCollection GetAlbumEvents(Int32 communityID)
		{
			string uri = "";
			Event.EVENT_TYPE eventType = Event.EVENT_TYPE.None;

			try
			{
				EventCollection Result = null;

				Result = _cache.Get(EventCollection.GetCacheKey(communityID, eventType, true)) as EventCollection;
				if (Result == null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						Result = getService(uri).GetAlbumEvents(communityID);
					}
					finally
					{
						base.Checkin(uri);
					}
				}

				return Result;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve album events (uri: " + uri + ")"+ ex.Message, ex));
			}			
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="eventID"></param>
		/// <returns></returns>
		public Event GetEvent(Int32 eventID)
		{
			string uri = "";

			try
			{
				Event Result = null;

				Result = _cache.Get(Event.GetCacheKey(eventID)) as Event;

				if (Result == null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						Result = getService(uri).GetEvent(eventID);
					}
					finally
					{
						base.Checkin(uri);
					}
				}

				return Result;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve event (uri: " + uri + ")"+ ex.Message, ex));
			}		
		}



		/// <returns></returns>
		public HolidayCollection GetHolidays()
		{
			string uri = "";

			try
			{
				HolidayCollection Result = null;

				Result = _cache.Get(HolidayCollection.GetCacheKey(DateTime.Now.Year)) as HolidayCollection;

				if (Result == null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						Result = getService(uri).GetHolidayCollection();
					}
					finally
					{
						base.Checkin(uri);
					}
					_cache.Insert(Result);
				}

				return Result;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve holidays (uri: " + uri + ")"+ ex.Message, ex));
			}			
		}


		public HolidayCollection GetHolidays(int year)
		{
			string uri = "";

			try
			{
				HolidayCollection Result = null;

				Result = _cache.Get(HolidayCollection.GetCacheKey(year)) as HolidayCollection;

				if (Result == null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						Result = getService(uri).GetHolidayCollection(year);
					}
					finally
					{
						base.Checkin(uri);
					}
					_cache.Insert(Result);
				}

				return Result;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve holidays (uri: " + uri + ")"+ ex.Message, ex));
			}			
		}


		public DateTime GetNextBusinessDate(DateTime date, int period)
		{
			string uri = "";
			DateTime result;
			try
			{
				double dPeriod=Conversion.CDouble(period.ToString());

				HolidayCollection holidays = GetHolidays(date.Year);

				result=date.AddDays(dPeriod);
				if(holidays==null)
					return date.AddDays(dPeriod);


				for (int i=1; i <= period; i++)
				{
					result=date.AddDays(i);
					string resultkey=String.Format("{0:d}",result);
					if (holidays.Contains(resultkey))
						period +=1;
					else if( result.DayOfWeek==DayOfWeek.Saturday || result.DayOfWeek==DayOfWeek.Sunday)
						period +=1;

				}


				return result;
			
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve holidays (uri: " + uri + ")"+ ex.Message, ex));
			}			
		}

		#endregion

		#region Private Service Adapter Methods
		private IEventService getService(string uri)
		{
			try
			{
				return (IEventService)Activator.GetObject(typeof(IEventService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri + ex.Message, ex));
			}
		}
		#endregion
	}
}
