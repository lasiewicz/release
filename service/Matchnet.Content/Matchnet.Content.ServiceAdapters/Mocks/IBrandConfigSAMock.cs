﻿using Matchnet.Content.ServiceAdapters.ServiceDefinitions;
using Matchnet.Content.ValueObjects.BrandConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Content.ServiceAdapters.Mocks
{
    public class IBrandConfigSAMock : IBrandConfigSA
    {
        private Brand _brand;

        public void SetBrand(Brand brand)
        {
            _brand = brand;
        }

        Brand IBrandConfigSA.GetBrandByID(int brandID)
        {
            return _brand;
        }

        Matchnet.Content.ValueObjects.BrandConfig.Brands IBrandConfigSA.GetBrands()
        {
            throw new NotImplementedException();
        }
    }
}
