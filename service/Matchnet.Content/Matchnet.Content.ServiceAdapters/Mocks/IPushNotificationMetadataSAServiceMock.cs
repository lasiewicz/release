﻿using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matchnet.Content.ServiceAdapters.Mocks
{
    public class IPushNotificationMetadataSAServiceMock : IPushNotificationMetadataSAService
    {
        private PushNotificationAppGroupModel _notificationAppModel;

        public void SetNotificationAppGroupModel(PushNotificationAppGroupModel notificationAppModel)
        {
            _notificationAppModel = notificationAppModel;
        }

        public PushNotificationAppGroupModel GetPushNotificationAppGroupModel(AppGroupID appGroupId)
        {
            return _notificationAppModel;
        }
    }
}
