using System;

using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Content.ValueObjects.Quotas;
using Matchnet.Content.ValueObjects.ServiceDefinitions;


namespace Matchnet.Content.ServiceAdapters
{
    public class QuotaDefinitionSA : SABase
    {
        private const string SERVICE_MANAGER_NAME = "QuotaDefinitionSM";

        public static readonly QuotaDefinitionSA Instance = new QuotaDefinitionSA();

        private Cache _cache;
        private QuotaDefinitions _lastLoadedQuotaDefinitions = null;


        private QuotaDefinitionSA()
        {
            _cache = Cache.Instance;
        }


        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_SA_CONNECTION_LIMIT"));
        }


        public QuotaDefinitions GetQuotaDefinitions()
        {
            string uri = "";

            try
            {
                QuotaDefinitions quotaDefinitions = _cache.Get(QuotaDefinitions.CACHE_KEY) as QuotaDefinitions;

                if (quotaDefinitions == null)
                {
                    uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);

                    base.Checkout(uri);
                    try
                    {
                        quotaDefinitions = getService(uri).GetQuotaDefinitions();
                        _lastLoadedQuotaDefinitions = quotaDefinitions;
                    }
                    catch (Exception ex)
                    {
                        //use last loaded quotaDefinitions if we cannot get them from svc

                        if (_lastLoadedQuotaDefinitions == null)
                        {
                            throw ex;
                        }

                        new SAException("Unable to load quotaDefinitions, reverting to last successful load.", ex);
                        _lastLoadedQuotaDefinitions.CacheTTLSeconds = 60;
                        _cache.Insert(_lastLoadedQuotaDefinitions);
                        return _lastLoadedQuotaDefinitions;
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                    _cache.Insert(quotaDefinitions);
                }

                return quotaDefinitions;
            }
            catch (Exception ex)
            {
                throw new BLException("Error retrieving quota definitions (uri: " + uri + ").",
                    ex);
            }
        }


        private IQuotaDefinitionService getService(string uri)
        {
            try
            {
                return (IQuotaDefinitionService)Activator.GetObject(typeof(IQuotaDefinitionService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }
    }
}
