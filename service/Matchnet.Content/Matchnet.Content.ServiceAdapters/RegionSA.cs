using System;
using System.Data;

using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Content.ValueObjects.ServiceDefinitions;


namespace Matchnet.Content.ServiceAdapters
{
	/// <summary>
	/// Summary description for RegionSA.
	/// </summary>
    public class RegionSA : SABase, IRegionSA
	{
		#region constants
		private const string SERVICE_MANAGER_NAME = "RegionSM";
		private const string REGION_CACHE_KEY_PREFIX = "~CONTENTREGION^{0}{1}";
		private const string CHILD_REGIONS_CACHE_KEY_PREFIX = "~CONTENTCHILDREGION^{0}{1}{2}{3}";
		private const string CITIES_BY_POSTAL_CACHE_KEY_PREFIX = "~CONTENTCITIESBYPOSTAL^{0}";
		private const string COUNTRY_REGIONS_CACHE_KEY_PREFIX = "~CONTENTCOUNTRYREGION^{0}";
		//added for TT 17739 s
		private const string COUNTRY_BIRTH_REGIONS_CACHE_KEY_PREFIX = "~CONTENTCOUNTRYBIRTHREGION^{0}";
		private const string POPULATED_HIERARCHY_CACHE_KEY_PREFIX = "~CONTENTPOPULATEDHIERARCHYREGION^{0}{1}{2}";
		private const string REGION_SCHOOL_NAME_CACHE_KEY_PREFIX = "~CONTENTREGIONSCHOOLNAME^{0}";
		private const string SCHOOL_REGION_SCHOOL_CACHE_KEY_PREFIX = "~CONTENTSCHOOLREGIONSCHOOL^{0}";
		private const string SCHOOL_REGION_STATE_CACHE_KEY_PREFIX = "~CONTENTSCHOOLREGIONSTATE~{0}";
		private const string REGION_ID_BY_POSTAL_CACHE_KEY_PREFIX = "~CONTENTREGIONIDBYPOSTAL^{0}{1}";
		private const string REGION_ID_BY_CITY_CACHE_KEY_PREFIX = "~CONTENTREGIONIDBYCITY^{0}{1}{2}";
		private const string REGION_LANGUAGE_CACHE_KEY_PREFIX = "~CONTENTREGIONLANGUAGE";
		private const string REGION_AREACODES_CACHE_KEY_PREFIX = "~CONTENTREGIONAREACODES";
		private const string REGION_SEOREGIONS_CACHE_KEY_PREFIX = "~CONTENTSEOREGIONS";
		private const string DMA_BY_ZIPREGIONID_CACHE_KEY_PREFIX = "~CONTENTDMABYZIPREGIONID^{0}";
		private const string ALL_DMAS_CACHE_KEY_PREFIX = "~CONTENTALLDMAS";

		#endregion
		
		#region class variables
		private Cache _cache = null;
		#endregion

		#region singleton
		/// <summary>
		/// 
		/// </summary>
		public static readonly RegionSA Instance = new RegionSA();
		/// <summary>
		/// Parameterless constructor
		/// </summary>
		private RegionSA()
		{
			_cache = Cache.Instance;
		}
		#endregion


		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_SA_CONNECTION_LIMIT"));
		}

		
		#region public methods
		/// <summary>
		/// Find the integer region ID given a country and postal code
		/// </summary>
		/// <param name="countryRegionID">countryRegionID</param>
		/// <param name="postalCode">postalCode</param>
		/// <returns>integer region ID referenced by the postal code for the given country</returns>
		public RegionID FindRegionIdByPostalCode(int countryRegionID, string postalCode)
		{
			RegionID oResult = null;
			string uri = "";
			string lookupValue = "";

			try 
			{	
				// Remove any blank spaces from the postal code (Canadian support)
				if (postalCode != null)
				{
					lookupValue = postalCode.Replace(" ", string.Empty);
				}

				oResult = _cache.Get(GetRegionIDByPostal(countryRegionID, lookupValue)) as RegionID;

				if(oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).FindRegionIdByPostalCode(countryRegionID, lookupValue);
					}
					finally
					{
						base.Checkin(uri);
					}
					if(oResult != null) 
					{
						oResult.SetCacheKey = GetRegionIDByPostal(countryRegionID, lookupValue);
						CacheRegionID(oResult);
					}
				}
				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve region ID by postal code (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// Find the integer region ID given a region ID and a city name
		/// </summary>
		/// <param name="parentRegionID">region where we are trying to find the city</param>
		/// <param name="description">city name</param>
		/// <param name="languageID"></param>
		/// <returns></returns>
		public RegionID FindRegionIdByCity(int parentRegionID, string description, int languageID)
		{
			RegionID oResult = null;
			string uri = "";

			if (description == string.Empty || description == null) return null;

			try 
			{
				oResult = _cache.Get(GetRegionIDByCity(parentRegionID, description, languageID)) as RegionID;

				if(oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).FindRegionIdByCity(parentRegionID, description, languageID);
					}
					finally
					{
						base.Checkin(uri);
					}
					if(oResult != null) 
					{
						oResult.SetCacheKey = GetRegionIDByCity(parentRegionID, description, languageID);
						CacheRegionID(oResult);
					}
				}
				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve region ID by city (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// Retrieve a region given the ID and a language
		/// </summary>
		/// <param name="regionID"></param>
		/// <param name="languageID"></param>
		/// <returns></returns>
		public Region RetrieveRegionByID(int regionID, int languageID)
		{
			Region oResult = null;
			string uri = "";

			try 
			{
				oResult = _cache.Get(GetRegionCacheKey(regionID, languageID)) as Region;

				if(oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).RetrieveRegionByID(regionID, languageID);
					}
					finally
					{
						base.Checkin(uri);
					}
					if(oResult != null) 
					{
						oResult.SetCacheKey = GetRegionCacheKey(regionID, languageID);
						CacheRegion(oResult);
					}
				}
				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve region (uri: " + uri + ")", ex));
			}
		}
		/// <summary>
		/// Provides a collection of regions (cities)based on provided postal code. 
		/// </summary>
		/// <remarks>This method should only be used for US and Canada. There is no country check occuring during DB call. </remarks>
		/// <param name="postalCode"></param>
		/// <returns>Collection of Region objects representing each city in the provided zip code.</returns>
		///	   
		public RegionCollection RetrieveCitiesByPostalCode(string strPostalCode) 
		{
			RegionCollection oResult = null;
			string uri = "";

			try 
			{
				oResult = _cache.Get(GetCitiesByPostalCodeCacheKey(strPostalCode)) as RegionCollection;

				if(oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).RetrieveCitiesByPostalCode(strPostalCode);
					}
					finally
					{
						base.Checkin(uri);
					}
					if(oResult != null) 
					{
						oResult.SetCacheKey = GetCitiesByPostalCodeCacheKey(strPostalCode);
						CacheRegionCollection(oResult);
					}
				}
				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve Cities by Postal Code (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="parentRegionID"></param>
		/// <param name="languageID"></param>
		/// <param name="description"></param>
		/// <param name="translationID"></param>
		/// <returns></returns>
		public RegionCollection RetrieveChildRegions(int parentRegionID, int languageID, string description, int translationID)
		{
			RegionCollection oResult = null;
			string uri = "";

			try 
			{
				oResult = _cache.Get(GetChildRegionsCacheKey(parentRegionID, languageID, description, translationID)) as RegionCollection;

				if(oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).RetrieveChildRegions(parentRegionID, languageID, description, translationID);
					}
					finally
					{
						base.Checkin(uri);
					}
					if(oResult != null) 
					{
						oResult.SetCacheKey = GetChildRegionsCacheKey(parentRegionID, languageID, description, translationID);
						CacheRegionCollection(oResult);
					}
				}
				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve child regions (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="parentRegionID"></param>
		/// <param name="languageID"></param>
		/// <param name="translationID"></param>
		/// <param name="forceLoad"></param>
		/// <returns></returns>
		public RegionCollection RetrieveChildRegions(int parentRegionID, int languageID, int translationID, bool forceLoad) 
		{
			return RetrieveChildRegions(parentRegionID, languageID, translationID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="parentRegionID"></param>
		/// <param name="languageID"></param>
		/// <returns></returns>
		public RegionCollection RetrieveChildRegions(int parentRegionID, int languageID)
		{
			return RetrieveChildRegions(parentRegionID, languageID, Matchnet.Constants.NULL_STRING);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="parentRegionID"></param>
		/// <param name="languageID"></param>
		/// <param name="description"></param>
		/// <returns></returns>
		public RegionCollection RetrieveChildRegions(int parentRegionID, int languageID, string description)
		{
			return RetrieveChildRegions(parentRegionID, languageID, description, Matchnet.Constants.NULL_INT);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="parentRegionID"></param>
		/// <param name="languageID"></param>
		/// <param name="translationID"></param>
		/// <returns></returns>
		public RegionCollection RetrieveChildRegions(int parentRegionID, int languageID, int translationID)
		{
			return RetrieveChildRegions(parentRegionID, languageID, Matchnet.Constants.NULL_STRING, translationID);
		}

		/// <summary>
		/// Retrieve country Regions given a language
		/// </summary>
		/// <param name="languageID"></param>
		/// <returns></returns>
		public RegionCollection RetrieveCountries(int languageID)
		{
			RegionCollection oResult = null;
			string uri = "";

			try 
			{
				oResult = _cache.Get(GetCountryRegionsCacheKey(languageID)) as RegionCollection;

				if(oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).RetrieveCountries(languageID);
					}
					finally
					{
						base.Checkin(uri);
					}
					if(oResult != null) 
					{
						oResult.SetCacheKey = GetCountryRegionsCacheKey(languageID);
						CacheRegionCollection(oResult);
					}
				}
				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve countries (uri: " + uri + ")", ex));
			}
		}


		/// <summary>
		/// Retrieve country Regions given a language
		/// </summary>
		/// <param name="languageID"></param>
		/// <returns></returns>
		public RegionCollection RetrieveBirthCountries(int languageID)
		{
			RegionCollection oResult = null;
			string uri = "";

			try 
			{
				oResult = _cache.Get(GetBirthCountryRegionsCacheKey(languageID)) as RegionCollection;

				if(oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).RetrieveBirthCountries(languageID);
					}
					finally
					{
						base.Checkin(uri);
					}
					if(oResult != null) 
					{
						oResult.SetCacheKey = GetBirthCountryRegionsCacheKey(languageID);
						CacheRegionCollection(oResult);
					}
				}
				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve countries (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// Retrieve country regions that speak english
		/// </summary>
		/// <returns></returns>
		public RegionCollection RetrieveCountries()
		{
			//	TOFIX - No one would be seriously injured if we pulled this 2 as
			//	a constant from somewhere - dcornell

			return(RetrieveCountries(2)); // english
		}

		/// <summary>
		/// Retrieve a fully populated region hierarchy
		/// </summary>
		/// <param name="regionID"></param>
		/// <param name="languageID"></param>
		/// <returns></returns>
		public RegionLanguage RetrievePopulatedHierarchy(int regionID, int languageID)
		{
			return(RetrievePopulatedHierarchy(regionID, languageID, Matchnet.Constants.NULL_INT));
		}

		/// <summary>
		/// Retrieve a fully populated region hierarchy
		/// </summary>
		/// <param name="regionID"></param>
		/// <param name="languageID"></param>
		/// <param name="maxDepth"></param>
		/// <returns></returns>
		public RegionLanguage RetrievePopulatedHierarchy(int regionID, int languageID, int maxDepth)
		{
			RegionLanguage oResult = null;
			string uri = "";

			try 
			{
				oResult = _cache.Get(GetPopulateHierarchyCacheKey(regionID, languageID, maxDepth)) as RegionLanguage;

				if(oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).RetrievePopulatedHierarchy(regionID, languageID, maxDepth);
					}
					finally
					{
						base.Checkin(uri);
					}
					if(oResult != null) 
					{
						oResult.SetCacheKey = GetPopulateHierarchyCacheKey(regionID, languageID, maxDepth);
						CachePopulatedHierarchy(oResult);
					}
				}
				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve populated hierarchy (uri: " + uri + ")", ex));
			}
		}

        public bool IsRegionInCountry(int regionId, int languageId, CountryConstants country)
        {
            var returnValue = false;

            var region = RetrievePopulatedHierarchy(regionId, languageId, 4);
            if (region != null && region.CountryRegionID == (int)country)
            {
                returnValue = true;
            }

            return returnValue;
        }


		/// <summary>
		/// Retrieve a school name given a school region ID
		/// </summary>
		/// <param name="schoolRegionID"></param>
		/// <returns></returns>
		public RegionSchoolName RetrieveSchoolName(int schoolRegionID)
		{
			RegionSchoolName oResult = null;
			string uri = "";

			try 
			{
				oResult = _cache.Get(GetRegionSchoolNameCacheKey(schoolRegionID)) as RegionSchoolName;

				if(oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).RetrieveSchoolName(schoolRegionID);
					}
					finally
					{
						base.Checkin(uri);
					}
					if(oResult != null) 
					{
						oResult.SetCacheKey = GetRegionSchoolNameCacheKey(schoolRegionID);
						CacheRegionSchoolName(oResult);
					}
				}
				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve school name (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="schoolRegionID"></param>
		/// <returns></returns>
		public SchoolRegionCollection RetrieveSchoolParents(int schoolRegionID)
		{
			SchoolRegionCollection oResult = null;
			string uri = "";

			try 
			{
				oResult = _cache.Get(GetSchoolRegionBySchoolCacheKey(schoolRegionID)) as SchoolRegionCollection;

				if(oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).RetrieveSchoolParents(schoolRegionID);
					}
					finally
					{
						base.Checkin(uri);
					}
					if(oResult != null) 
					{
						oResult.SetCacheKey = GetSchoolRegionBySchoolCacheKey(schoolRegionID);
						CacheSchoolRegionCollection(oResult);
					}
				}
				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve child regions (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// Region school regions for a given state region ID
		/// </summary>
		/// <param name="stateRegionID"></param>
		/// <returns></returns>
		public SchoolRegionCollection RetrieveSchoolListByStateRegion(int stateRegionID)
		{
			SchoolRegionCollection oResult = null;
			string uri = "";

			try 
			{
				oResult = _cache.Get(GetSchoolRegionByStateCacheKey(stateRegionID)) as SchoolRegionCollection;

				if(oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).RetrieveSchoolListByStateRegion(stateRegionID);
					}
					finally
					{
						base.Checkin(uri);
					}
					if(oResult != null) 
					{
						oResult.SetCacheKey = GetSchoolRegionByStateCacheKey(stateRegionID);
						CacheSchoolRegionCollection(oResult);
					}
				}
				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve schools by state (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public LanguageCollection GetLanguages()
		{
			LanguageCollection oResult = null;
			string uri = "";

			try 
			{
				oResult = _cache.Get(GetRegionLanguageCacheKey()) as LanguageCollection;

				if(oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).GetLanguages();
					}
					finally
					{
						base.Checkin(uri);
					}
					oResult.SetCacheKey = GetRegionLanguageCacheKey();
					CacheRegionLanguage(oResult);
				}
				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve languages (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="areaCodes"></param>
		/// <param name="countryRegionID"></param>
		/// <returns>Returns a comma delimited string containing the indexs of the AreaCodes
		/// that were invalid.</returns>
		public String IsValidAreaCodes(Int32[] areaCodes, Int32 countryRegionID)
		{
			String retVal = String.Empty;

			for (Int32 i = 0; i < areaCodes.Length; i++)
			{
				if (!IsValidAreaCode(areaCodes[i], countryRegionID))
					retVal += areaCodes[i].ToString() + ", ";
			}

			if (retVal != String.Empty)
				retVal = retVal.Substring(0, retVal.Length - 2);	// Remove trailing ", ".

			return retVal;
		}

		/// <summary>
		/// This is public but will probably only be used by other methods in this class.
		/// </summary>
		/// <param name="areaCode"></param>
		/// <param name="countryRegionID"></param>
		/// <returns></returns>
		public Boolean IsValidAreaCode(Int32 areaCode, Int32 countryRegionID)
		{
			if (RetrieveAreaCodes()[areaCode] == countryRegionID)
				return true;
			else
				return false;
		}

		/// <summary>
		/// This is public but will probably only be used by other methods in this class.
		/// </summary>
		/// <returns></returns>
		public AreaCodeDictionary RetrieveAreaCodes()
		{
			AreaCodeDictionary oResult = null;
			string uri = "";

			try 
			{
				oResult = _cache.Get(GetAreaCodeDictionaryCacheKey()) as AreaCodeDictionary;

				if (oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).RetrieveAreaCodes();
					}
					finally
					{
						base.Checkin(uri);
					}
					if (oResult != null) 
					{
						oResult.SetCacheKey = GetAreaCodeDictionaryCacheKey();
						CacheAreaCodeDictionary(oResult);
					}
				}
				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve AreaCodes (uri: " + uri + ")", ex));
			}
		}

		public RegionAreaCodeDictionary RetrieveAreaCodesByRegion()
		{
			RegionAreaCodeDictionary oResult = null;
			string uri = "";

			try 
			{
				oResult = _cache.Get(RegionAreaCodeDictionary.CACHE_KEY) as RegionAreaCodeDictionary;

				if (oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).RetrieveAreaCodesByRegion();
					}
					finally
					{
						base.Checkin(uri);
					}

					if (oResult != null) 
					{
						_cache.Insert(oResult);
					}
				}
				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve AreaCodes (uri: " + uri + ")", ex));
			}
		}
		/// <summary>
		/// Returns list of US major cities for SEO pages usage.
		/// </summary>
		/// <returns></returns>
		public SEORegionCollection GetSEOMajorUSCities ()
		{
			
			SEORegions objseoRegions=null;
			string uri="";
			try
			{

				objseoRegions= _cache.Get(GetSEORegionsCacheKey()) as SEORegions;
				if (objseoRegions==null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						objseoRegions = getService(uri).RetrieveSEORegions();
					}
					finally
					{
						base.Checkin(uri);
					}

					if (objseoRegions != null) 
					{
						objseoRegions.SetCacheKey=GetSEORegionsCacheKey();
						_cache.Insert(objseoRegions);					
					}
				}
			}
			catch(Exception ex) 
			{
				throw(new SAException("GetSEOMajorUSCities(). Error retrieving SEO Major Cities.(uri: " + uri + ")", ex));
			}
			return objseoRegions.MajorUSCities;
			
		}
		/// <summary>
		/// Returns all US states defined by SEO pages business requirements.
		/// </summary>
		/// <returns></returns>
		public SEORegionCollection GetSEOUSStates ()
		{
			SEORegionCollection oResult= new SEORegionCollection();
			SEORegions objseoRegions=null;
			string uri="";
			try
			{
				objseoRegions= _cache.Get(GetSEORegionsCacheKey()) as SEORegions;
				if (objseoRegions==null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						objseoRegions = getService(uri).RetrieveSEORegions();
					}
					finally
					{
						base.Checkin(uri);
					}

					if (objseoRegions != null) 
					{
						objseoRegions.SetCacheKey=GetSEORegionsCacheKey();
						_cache.Insert(objseoRegions);						
					}
				}
				foreach (SEOState seoState in objseoRegions.States)
				{
					oResult.Add(new SEORegion(seoState.State.RegionID,seoState.State.Description,seoState.State.Abbreviation,"",""));
				}
			}
			catch(Exception ex) 
			{
				throw(new SAException("GetSEOUSStates(). Error retrieving SEO US States. (uri: " + uri + ")", ex));
			}
			return oResult;
		}
		
		/// <summary>
		/// Returns all popular cities of a US state according to SEO pages business requirements.
		/// </summary>
		/// <param name="StateRegionID"></param>
		/// <returns></returns>
		public SEORegionCollection GetSEOCitiesInState(int StateRegionID)
		{
			SEORegionCollection oResult= new SEORegionCollection();
			SEORegions objseoRegions=null;
			string uri="";
			try
			{

				objseoRegions= _cache.Get(GetSEORegionsCacheKey()) as SEORegions;
				if (objseoRegions==null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						objseoRegions = getService(uri).RetrieveSEORegions();
					}
					finally
					{
						base.Checkin(uri);
					}

					if (objseoRegions != null) 
					{
						objseoRegions.SetCacheKey=GetSEORegionsCacheKey();
						_cache.Insert(objseoRegions);
						
					}
				}
				foreach (SEOState seoState in objseoRegions.States)
				{
					if (seoState.State.RegionID==StateRegionID)
					{
						oResult= seoState.Cities;
					}
				}
			}
			catch(Exception ex) 
			{
				throw(new SAException("GetSEOCitiesInState(). Error retrieving SEO Cities in state.(uri: " + uri + ")", ex));
			}
			return oResult;
		}
		/// <summary>
		/// Returns a random zipcode (city) based on provided SEO defined default city regionID. 
		/// </summary>
		/// <param name="StateRegionID"></param>
		/// <returns></returns>
		public int  GetSEOStatesPopularCity (int StateRegionID)
		{
			
			int intResult=0;
			SEORegions objseoRegions=null;
			string uri="";
			try
			{
				objseoRegions= _cache.Get(GetSEORegionsCacheKey()) as SEORegions;
				if (objseoRegions==null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						objseoRegions = getService(uri).RetrieveSEORegions();
					}
					finally
					{
						base.Checkin(uri);
					}
					if (objseoRegions != null) 
					{
						objseoRegions.SetCacheKey=GetSEORegionsCacheKey();
						_cache.Insert(objseoRegions);
					}
				}
				foreach (SEOState seoState in objseoRegions.States)
				{
					if (seoState.State.RegionID==StateRegionID)
					{
						/*// Retrieveing all zipcodes under the default city.						
						RegionCollection seoDefaultCityZipCodes=RetrieveChildRegions(seoState.DefaultCity,2); // passing languageID=2 for English						
						// Pick one zipcode randomly
						intResult=seoDefaultCityZipCodes[(int) (Math.Floor(seoDefaultCityZipCodes.Count * (new Random((int)DateTime.Now.Ticks).NextDouble())))].RegionID;
						*/
						intResult=GetRandomZipCode(seoState.DefaultCity);
						break;
					}
				}
			}
			catch(Exception ex) 
			{
				throw(new SAException("GetSEOStatesPopularCity(). Error retrieving SEO state's popular city.(uri: " + uri + ")", ex));
			}
			return intResult;
		}
		/// <summary>
		/// Returns a random zipcode (city) based on provided City region ID.
		/// </summary>
		/// <param name="CityRegionID"></param>
		/// <returns></returns>
		public int GetRandomZipCode(int CityRegionID)
		{
			// Retrieveing all zipcodes under the default city.						
			RegionCollection seoDefaultCityZipCodes=RetrieveChildRegions(CityRegionID,2); // passing languageID=2 for English						
			// Pick one zipcode randomly
			return seoDefaultCityZipCodes[(int) (Math.Floor(seoDefaultCityZipCodes.Count * (new Random((int)DateTime.Now.Ticks).NextDouble())))].RegionID;
		}
		public DMA GetDMAByZipRegionID(int ZipRegionID)
		{

			DMA oResult = null;
			string uri = "";

			try 
			{
				oResult = _cache.Get(GetDMAByZipRegionIDCacheKey(ZipRegionID)) as DMA;

				if(oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).GetDMAByZipRegionID(ZipRegionID);
					}
					finally
					{
						base.Checkin(uri);
					}
					if(oResult != null) 
					{
						oResult.SetCacheKey = GetDMAByZipRegionIDCacheKey(ZipRegionID);
						CacheDMAByZipRegionID(oResult);
					}
				}
				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve DMA for ZipRegionID="+ ZipRegionID.ToString() +" (uri: " + uri + ")", ex));
			}

		}
		public DMA GetDMAByDMAID(int dmaId)
		{
			return GetAllDMAS().FindById(dmaId);
		}
		public DMACollection GetAllDMAS()
		{
			DMACollection oResult = null;
			string uri = "";

			try
			{
				oResult = _cache.Get(GetAllDMASCacheKey()) as DMACollection;

				if(oResult == null)
				{
					uri=UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).GetAllDMAS();
					}
					finally
					{
						base.Checkin(uri);
					}
					if(oResult != null)
					{
						oResult.SetCacheKey = GetAllDMASCacheKey();
						CacheAllDMAS(oResult);
					}
				}

				return oResult;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannon retrieve All DMA codes (uri: " + uri + ")", ex));
			}
		}
		#endregion

		#region Private Service Adapter Methods
		private IRegionService getService(string uri)
		{
			try
			{
				return (IRegionService)Activator.GetObject(typeof(IRegionService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}


		#endregion

		#region private Cache Routines
		private void CacheRegion(Region pRegion)
		{
			pRegion.CachePriority = CacheItemPriorityLevel.Normal;
			pRegion.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SA"));
			_cache.Insert(pRegion);
		}
		private void CacheRegionCollection(RegionCollection pRegionCollection) 
		{
			pRegionCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pRegionCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SA"));
			_cache.Insert(pRegionCollection);
		}
		private void CachePopulatedHierarchy(RegionLanguage pRegionLanguage) 
		{
			pRegionLanguage.CachePriority = CacheItemPriorityLevel.Normal;
			pRegionLanguage.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SA"));
			_cache.Insert(pRegionLanguage);
		}
		private void CacheRegionSchoolName(RegionSchoolName pRegionSchoolName) 
		{
			pRegionSchoolName.CachePriority = CacheItemPriorityLevel.Normal;
			pRegionSchoolName.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SA"));
			_cache.Insert(pRegionSchoolName);
		}
		private void CacheSchoolRegionCollection(SchoolRegionCollection pSchoolRegionCollection) 
		{
			pSchoolRegionCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pSchoolRegionCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SA"));
			_cache.Insert(pSchoolRegionCollection);
		}
		private void CacheRegionID(RegionID pRegionID) 
		{
			pRegionID.CachePriority = CacheItemPriorityLevel.Normal;
			pRegionID.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SA"));
			_cache.Insert(pRegionID);
		}
		private void CacheRegionLanguage(LanguageCollection pLanguageCollection) 
		{
			pLanguageCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pLanguageCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SVC"));
			_cache.Insert(pLanguageCollection);
		}
		private void CacheAreaCodeDictionary(AreaCodeDictionary pAreaCodeDictionary) 
		{
			pAreaCodeDictionary.CachePriority = CacheItemPriorityLevel.Normal;
			pAreaCodeDictionary.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SVC"));
			_cache.Insert(pAreaCodeDictionary);
		}
		private void CacheDMAByZipRegionID(DMA pDMA) 
		{
			pDMA.CachePriority = CacheItemPriorityLevel.Normal;
			pDMA.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SA"));
			_cache.Insert(pDMA);
		}
		private void CacheAllDMAS(DMACollection pDMACollection)
		{
			pDMACollection.CachePriority = CacheItemPriorityLevel.Normal;
			pDMACollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SA"));
			_cache.Insert(pDMACollection);
		}
		
		private string GetRegionCacheKey(int pRegionID, int pLanguageID)
		{
			return string.Format(REGION_CACHE_KEY_PREFIX, pRegionID, pLanguageID);
		}
		private string GetCitiesByPostalCodeCacheKey( string strPostalCode) 
		{		
			return string.Format(CITIES_BY_POSTAL_CACHE_KEY_PREFIX, strPostalCode);
		}
		private string GetChildRegionsCacheKey(int parentRegionID, int languageID, string description, int translationID) 
		{
			return string.Format(CHILD_REGIONS_CACHE_KEY_PREFIX, parentRegionID, languageID, description, translationID);
		}
		private string GetCountryRegionsCacheKey(int pLanguageID) 
		{
			return string.Format(COUNTRY_REGIONS_CACHE_KEY_PREFIX, pLanguageID);
		}
		//added for TT 17739 
		private string GetBirthCountryRegionsCacheKey(int pLanguageID) 
		{
			return string.Format(COUNTRY_BIRTH_REGIONS_CACHE_KEY_PREFIX, pLanguageID);
		}
		private string GetPopulateHierarchyCacheKey(int regionID, int languageID, int maxDepth) 
		{
			return string.Format(POPULATED_HIERARCHY_CACHE_KEY_PREFIX, regionID, languageID, maxDepth);
		}
		private string GetRegionSchoolNameCacheKey(int schoolRegionID) 
		{
			return string.Format(REGION_SCHOOL_NAME_CACHE_KEY_PREFIX, schoolRegionID);
		}
		private string GetSchoolRegionBySchoolCacheKey(int schoolRegionID) 
		{
			return string.Format(SCHOOL_REGION_SCHOOL_CACHE_KEY_PREFIX, schoolRegionID);
		}
		private string GetSchoolRegionByStateCacheKey(int stateRegionID) 
		{
			return string.Format(SCHOOL_REGION_STATE_CACHE_KEY_PREFIX, stateRegionID);
		}
		private string GetRegionIDByPostal(int countryRegionID, string postalCode) 
		{
			return string.Format(REGION_ID_BY_POSTAL_CACHE_KEY_PREFIX, countryRegionID, postalCode);
		}
		private string GetRegionIDByCity(int parentRegionID, string description, int languageID)
		{
			return string.Format(REGION_ID_BY_CITY_CACHE_KEY_PREFIX, parentRegionID, description, languageID);
		}
		private string GetRegionLanguageCacheKey()
		{
			return REGION_LANGUAGE_CACHE_KEY_PREFIX;
		}
		private string GetAreaCodeDictionaryCacheKey()
		{
			return REGION_AREACODES_CACHE_KEY_PREFIX;
		}
		private string GetSEORegionsCacheKey()
		{
			return REGION_SEOREGIONS_CACHE_KEY_PREFIX;
		}
		private string GetDMAByZipRegionIDCacheKey(int zipRegionId)
		{
			return string.Format(DMA_BY_ZIPREGIONID_CACHE_KEY_PREFIX,zipRegionId);
		}
		private string GetAllDMASCacheKey()
		{
			return ALL_DMAS_CACHE_KEY_PREFIX;
		}

		#endregion
	}
}
