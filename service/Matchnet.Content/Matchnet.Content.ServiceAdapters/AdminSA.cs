using System;

using Matchnet.Caching;
using Matchnet.Content.ValueObjects.Registration;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.ServiceDefinitions;


namespace Matchnet.Content.ServiceAdapters
{
	/// <summary>
	/// Summary description for RegionSA.
	/// </summary>
	public class AdminSA : SABase
	{
		private Cache _cache = Cache.Instance;

		#region constants
		private const string SERVICE_MANAGER_NAME = "AdminSM";
		#endregion

		#region Constructors
		/// <summary>
		/// Singleton instance
		/// </summary>
		public static readonly AdminSA Instance = new AdminSA();

		/// <summary>
		/// Parameterless constructor
		/// </summary>
		public AdminSA()
		{
		}
		#endregion

		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_SA_CONNECTION_LIMIT"));
		}

		#region public methods

        public AdminActionReasonID GetLastAdminSuspendReasonForMember(int memberID)
        {
            string uri = string.Empty;
            AdminActionReasonID reason = AdminActionReasonID.None;

            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                   reason = getService(uri).GetLastAdminSuspendReasonForMember(memberID);
                }
                finally
                {
                    base.Checkin(uri);
                }

                return reason;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get Last Admin Suspend Reason For Member (uri: " + uri + ")", ex));
            }
        }


		public AdminActionReasonCollection GetAdminActionReasons()
		{
			string uri = "";
			AdminActionReasonCollection oResult = null;

			try
			{
				oResult = _cache.Get(AdminActionReasonCollection.CACHE_KEY) as AdminActionReasonCollection;

				if (oResult == null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).GetAdminActionReasons();
					}
					finally
					{
						base.Checkin(uri);
					}
					_cache.Insert(oResult);
				}

				return oResult;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot get admin action reasons (uri: " + uri + ")", ex));
			}
		}

		public AdminActivityContainerCollection GetAdminActivity(int memberID, int communityID)
		{
			string uri = "";
			AdminActivityContainerCollection oResult = null;

			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					oResult = getService(uri).GetAdminActivity(memberID, communityID);
				}
				finally
				{
					base.Checkin(uri);
				}
				return oResult;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot get admin activity (uri: " + uri + ")", ex));
			}
		}


		public AdminActionReasonCollection GetAdminActionReasonsByType(AdminActionReasonType type)
		{
			AdminActionReasonCollection allReasons = GetAdminActionReasons();
			AdminActionReasonCollection filteredReasons = new AdminActionReasonCollection();

			foreach(AdminActionReason reason in allReasons)
			{
				if(reason.AdminActionReasonType == type)
				{
					filteredReasons.Add(reason);
				}
			}

			return filteredReasons;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public AdminActionTypeCollection GetAdminActionTypes()
		{
			string uri = "";
			AdminActionTypeCollection oResult = null;

			try
			{
				oResult = _cache.Get(AdminActionTypeCollection.CACHE_KEY) as AdminActionTypeCollection;

				if (oResult == null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).GetAdminActionTypes();
					}
					finally
					{
						base.Checkin(uri);
					}
					_cache.Insert(oResult);
				}

				return oResult;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot get admin action types (uri: " + uri + ")", ex));
			}
		}
		
		/// <summary>
		/// NOTE: Deprecated.  See AdminBL.GetAdminActionLog for details.
		/// TOFIX: Remove this method and supporting service methods after the next
		/// deployment of the web code to production.
		/// Get admin action log
		/// </summary>
		public AdminActionLogCollection GetAdminActionLog(int memberID, int communityID)
		{
			string uri = "";
			AdminActionLogCollection oResult = null;

			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					oResult = getService(uri).GetAdminActionLog(memberID, communityID);
				}
				finally
				{
					base.Checkin(uri);
				}
				return oResult;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot get admin action log (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// Get admin actions for a particular member
		/// </summary>
		public AdminActionCollection GetAdminActions(int memberID, int communityID)
		{
			string uri = "";
			AdminActionCollection oResult = null;

			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					oResult = getService(uri).GetAdminActions(memberID, communityID);
				}
				finally
				{
					base.Checkin(uri);
				}
				return oResult;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot get admin action log (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// Get admin action report, sort by value of sortBy
		/// </summary>
		/// <returns></returns>
		public AdminActionReport GetAdminActionReport(DateTime startDate, DateTime endDate, Int32 adminMemberID, Int32 groupID, Int32 adminActionMask, string sortBy )
		{
			AdminActionReport reportToSort = GetAdminActionReport( startDate, endDate, adminMemberID, groupID, adminActionMask );
			reportToSort.Sort( new AdminActionReport.AdminActionReportComparer( (AdminActionReport.AdminActionReportSortType)Enum.Parse( typeof(AdminActionReport.AdminActionReportSortType), sortBy) ) );
			return reportToSort;
		}

		/// <summary>
		/// Get admin action report
		/// </summary>
		public AdminActionReport GetAdminActionReport(DateTime startDate, DateTime endDate, Int32 adminMemberID, Int32 groupID, Int32 adminActionMask)
		{
			string uri = "";
			AdminActionReport oResult = null;

			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					oResult = getService(uri).GetAdminActionReport(startDate, endDate, adminMemberID, groupID, adminActionMask);
				}
				finally
				{
					base.Checkin(uri);
				}
				return oResult;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot get admin action log (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// Get admin action report
		/// </summary>
		public ApprovalCountsCollection GetApprovalCountsReport(DateTime startDate, DateTime endDate, Int32 adminMemberID, Int32 groupID)
		{
			string uri = "";
			ApprovalCountsCollection oResult = null;

			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					oResult = getService(uri).GetApprovalCountsReport(startDate, endDate, adminMemberID, groupID);
				}
				finally
				{
					base.Checkin(uri);
				}
				return oResult;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot get admin action log (uri: " + uri + ")", ex));
			}
		}
		
		/// <summary>
		/// Overlaoded method for calls without a languageID but with a brandID
		/// </summary>
		/// <param name="memberID"></param>
		/// <param name="communityID"></param>
		/// <param name="adminActionMask"></param>
		/// <param name="adminMemberID"></param>
		public void AdminActionLogInsert(int memberID, int communityID, int adminActionMask, int adminMemberID)
		{
			AdminActionLogInsert(memberID, communityID, adminActionMask, adminMemberID, Constants.NULL_INT);
		}

		//overload for calls without a reasonID
		public void AdminActionLogInsert(int memberID, int communityID, int adminActionMask, int adminMemberID, AdminMemberIDType adminMemberIDType)
		{
			AdminActionLogInsert(memberID, communityID, adminActionMask, adminMemberID, adminMemberIDType, AdminActionReasonID.None);
		}

		/// <summary>
		/// Adds a record to the Admin Action Log
		/// </summary>
		/// <param name="memberID">The memberId of the member who's free text has just been acted on</param>
		/// <param name="communityID">The domainId of the member who's free text has just been acted on</param>
		/// <param name="adminActionMask">The action taken on this member</param>
		/// <param name="adminMemberID">The the memberId of the customer service rep that applied the action</param>
		/// <param name="languageID"></param>
		public void AdminActionLogInsert(int memberID, int communityID, int adminActionMask, int adminMemberID, int languageID)
		{
			string uri = "";

			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					getService(uri).AdminActionLogInsert(memberID, communityID, adminActionMask, adminMemberID, languageID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot insert admin action log (uri: " + uri + ")", ex));
			}
		}

        public void RegistrationAdminActionLogInsert(string adminAccountName, RegAdminActionType actionType, int scenarioId)
        {
            string uri = "";

            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).RegistrationAdminActionLogInsert(adminAccountName, actionType, scenarioId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot insert reg admin action log (uri: " + uri + ")", ex));
            }
        }

        public void RegistrationAdminScenarioHistorySave(int regScenarioId, int deviceTypeID, int siteID, double weight, string updatedBy)
        {
            string uri = "";

            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).RegistrationAdminScenarioHistorySave(regScenarioId, deviceTypeID, siteID, weight, updatedBy);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot save reg admin scenario history (uri: " + uri + ")", ex));
            }
        }

		/// <summary>
		/// Adds a record to the Admin Action Log
		/// </summary>
		/// <param name="memberID">The memberId of the member who's free text has just been acted on</param>
		/// <param name="communityID">The domainId of the member who's free text has just been acted on</param>
		/// <param name="adminActionMask">The action taken on this member</param>
		/// <param name="adminMemberID">The the memberId of the customer service rep that applied the action</param>
		/// <param name="adminMemberIDType">What type of id is adminMemberID</param>
		public void AdminActionLogInsert(int memberID, int communityID, int adminActionMask, int adminMemberID, AdminMemberIDType adminMemberIDType, AdminActionReasonID adminActionReasonID)
		{
			string uri = "";

			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					getService(uri).AdminActionLogInsert(memberID, communityID, adminActionMask, adminMemberID, adminMemberIDType, adminActionReasonID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot insert admin action log (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// Save new admin note
		/// </summary>
		public void UpdateAdminNote(string newNote, int targetMemberID, int memberID, int communityID, int maxNoteLength)
		{
			string uri = "";
			bool oResult = false;

			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					getService(uri).UpdateAdminNote(newNote, targetMemberID, memberID, communityID, maxNoteLength);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot update admin note (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// Load admin notes for member.
		/// </summary>
		public AdminNoteCollection LoadAdminNotes(int targetMemberID, int communityID, int startRow, int pageSize, int totalRows)
		{
			string uri = "";
			AdminNoteCollection oResult = null;

			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					oResult = getService(uri).LoadAdminNotes(targetMemberID, communityID, startRow, pageSize, totalRows);
				}
				finally
				{
					base.Checkin(uri);
				}
				oResult.Sort();
				return oResult;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot load admin notes (uri: " + uri + ")", ex));
			}
		}
		#endregion

		private IAdminService getService(string uri)
		{
			try
			{
				return (IAdminService)Activator.GetObject(typeof(IAdminService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		#region Publish
		/// <summary>
		/// 
		/// </summary>
		/// <param name="startDate"></param>
		/// <param name="endDate"></param>
		/// <returns></returns>
		public PublishCollection GetPublishes(DateTime startDate, DateTime endDate)
		{
			string uri = "";
			PublishCollection oResult = null;

			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					oResult = getService(uri).GetPublishes(startDate, endDate);
				}
				finally
				{
					base.Checkin(uri);
				}
				return oResult;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot get Publishes (uri: " + uri
					+ ")(startDate: " + startDate.ToString() 
					+ ")(endDate: " + endDate.ToString() + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="publishID"></param>
		/// <returns></returns>
		public Publish GetPublish(Int32 publishObjectID, PublishObjectType publishObjectType)
		{
			string uri = "";
			Publish oResult = null;

			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					oResult = getService(uri).GetPublish(publishObjectID, publishObjectType);
				}
				finally
				{
					base.Checkin(uri);
				}
				return oResult;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot get Publishes (uri: " + uri
					+ ")(publishObjectID: " + publishObjectID.ToString()
					+ ")(publishObjectType: " + publishObjectType.ToString() + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public PublishActionPublishObjectCollection GetPublishActionPublishObjects()
		{
			string uri = "";
			PublishActionPublishObjectCollection oResult = null;

			try
			{
				oResult = _cache.Get(PublishActionPublishObjectCollection.CACHE_KEY_PREFIX) as PublishActionPublishObjectCollection;

				if (oResult == null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).GetPublishActionPublishObjects();
					}
					finally
					{
						base.Checkin(uri);
					}
				}
				return oResult;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot get Publishes (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="publishObjectID"></param>
		/// <param name="publishObjectType"></param>
		/// <param name="publishObject"></param>
		/// <returns></returns>
		public Int32 SavePublish(Int32 publishObjectID,
			PublishObjectType publishObjectType,
			Object content)
		{
			string uri = "";
			Int32 publishID = Constants.NULL_INT;

			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					publishID = getService(uri).SavePublish(publishObjectID, publishObjectType, content);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot SavePublish (uri: " + uri 
					+ ")(publishObjectID: " + publishObjectID.ToString() 
					+ ")(publishObjectType: " + publishObjectType.ToString() + ")", ex));
			}

			return publishID;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="publishID"></param>
		/// <param name="publishActionPublishObjectID"></param>
		/// <param name="memberID"></param>
		public void SavePublishAction(Int32 publishID,
			Int32 publishActionPublishObjectID,
			Int32 memberID)
		{
			string uri = "";
			
			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					getService(uri).SavePublishAction(publishID, publishActionPublishObjectID, memberID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot SavePublishAction (uri: " + uri 
					+ ")(publishID: " + publishID.ToString() 
					+ ")(publishActionPublishObjectID: " + publishActionPublishObjectID.ToString()
					+ ")(memberID: " + memberID.ToString() + ")", ex));
			}
		}

		public void GetNextPublishActionPublishObjectStep(Int32 publishObjectID, PublishObjectType publishObjectType, out Int32 publishActionPublishObjectID, out ServiceEnvironmentTypeEnum nextServiceEnvironmentTypeEnum)
		{
			string uri = "";
			
			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					getService(uri).GetNextPublishActionPublishObjectStep(publishObjectID, publishObjectType, out publishActionPublishObjectID, out nextServiceEnvironmentTypeEnum);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot GetNextPublishActionPublishObjectStep (uri: " + uri 
					+ ")(publishObjectID: " + publishObjectID.ToString()
					+ ")(publishObjectType: " + publishObjectType.ToString() + ")", ex));
			}
		}
		#endregion
	}
}
