using System;
using System.Collections;
using System.IO;

namespace Matchnet.Content.ServiceAdapters
{
	public class RegionFile
	{
		public static readonly RegionFile Instance = new RegionFile();

		private Hashtable _regions;

		private RegionFile()
		{
			_regions = new Hashtable();
			StreamReader sr = new StreamReader(@"c:\matchnet\Region.txt");
			string line = null;
			string[] lineParts;
			Int32 regionID = 0;
			Int32 parentRegionID = 0;
			byte depth = 0;
			byte childrenDepth = 0;
			RegionStatic parentRegion = null;

			line = sr.ReadLine();
			while (line != null)
			{
				//"RegionID","ParentRegionID","Depth","Latitude","Longitude","Description","Abbreviation","Mask","ChildrenDepth"
				lineParts = line.Split('\t');
				
				regionID = Convert.ToInt32(lineParts[0]);

				if (lineParts[1].Length > 0)
				{
					parentRegionID = Convert.ToInt32(lineParts[1]);
				}
				else
				{
					parentRegionID = 0;
				}

				depth = Convert.ToByte(lineParts[2]);

				if (lineParts[8].Length > 0)
				{
					childrenDepth = Convert.ToByte(lineParts[8]);
				}
				else
				{
					childrenDepth = 0;
				}

				if (depth > 1)
				{
					parentRegion = _regions[parentRegionID] as RegionStatic;
				}
				else
				{
					parentRegion = null;
				}

				_regions.Add(regionID, new RegionStatic(regionID,
					parentRegion,
					depth,
					childrenDepth,
					Convert.ToDecimal(lineParts[3]),
					Convert.ToDecimal(lineParts[4]),
					lineParts[5],
					lineParts[6],
					Convert.ToInt32(lineParts[7])));

				line = sr.ReadLine();
			}
		}

		private class RegionStatic
		{
			private Int32 _regionID;
			private Hashtable _children;
			private RegionStatic _parent;
			private byte _depth;
			private byte _childrenDepth;
			private decimal _latitude;
			private decimal _longitude;
			private string _description;
			private string _abbreviation;
			private Int32 _mask;

			public RegionStatic(Int32 regionID,
				RegionStatic parent,
				byte depth,
				byte childrenDepth,
				decimal latitude,
				decimal longitude,
				string description,
				string abbreviation,
				Int32 mask)
			{
				_regionID = regionID;
				_children = new Hashtable();
				_parent = parent;
				_depth = depth;
				_childrenDepth = childrenDepth;
				_latitude = latitude;
				_longitude = longitude;
				_description = description;
				_abbreviation = abbreviation;
				_mask = mask;
			}
		}
	}
}
