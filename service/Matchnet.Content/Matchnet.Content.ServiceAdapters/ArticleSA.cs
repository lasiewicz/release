using System;
using System.Data;
using System.Text;

using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.Article;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.CacheSynchronization.Context;

namespace Matchnet.Content.ServiceAdapters
{
	/// <summary>
	/// Summary description for ArticleSA.
	/// </summary>
	public class ArticleSA : SABase
	{
		#region constants
		private const string SERVICE_MANAGER_NAME = "ArticleSM";
		#endregion

		#region class variables
		private Cache _cache = null;
		private Random _random = null;
		#endregion

		#region singleton
		/// <summary>
		/// 
		/// </summary>
		public static readonly ArticleSA Instance = new ArticleSA();

		/// <summary>
		/// 
		/// </summary>
		private ArticleSA()
		{
			_cache = Cache.Instance;
			_random = new Random();
		}
		#endregion

		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_SA_CONNECTION_LIMIT"));
		}

		#region Article and Category Methods
		/// <summary>
		/// 
		/// </summary>
		/// <param name="articleID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		public SiteArticle RetrieveSiteArticle(int articleID, int siteID)
		{
			SiteArticle oResult = null;
			string uri = string.Empty;

			try 
			{
				string cacheKey = SiteArticle.GetCacheKey(articleID, siteID);
				oResult = _cache.Get(cacheKey) as SiteArticle;

				if (oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					Int32 cacheTTL = GenerateRandomTTL(Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_ARTICLE_SA")));

					base.Checkout(uri);
					try
					{
						oResult = getService(uri).RetrieveSiteArticle(System.Environment.MachineName,
							Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
							articleID,
							siteID);
					}
					finally
					{
						base.Checkin(uri);
					}

					if (oResult != null) 
					{
						oResult.SetCacheKey = cacheKey;
						oResult.CacheTTLSeconds = cacheTTL;
						
						_cache.Insert(oResult);
					}
				}

				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve SiteArticle (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="categoryID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		public SiteArticleCollection RetrieveCategorySiteArticles(int categoryID, int siteID)
		{
			SiteArticleCollection oResult = null;
			string uri = string.Empty;

			try 
			{
				string cacheKey = SiteArticleCollection.GetCacheKey(categoryID, siteID);
				oResult = _cache.Get(cacheKey) as SiteArticleCollection;

				if (oResult == null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					Int32 cacheTTL = GenerateRandomTTL(Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_ARTICLE_SA")));

					base.Checkout(uri);
					try
					{
						oResult = getService(uri).RetrieveCategorySiteArticles(System.Environment.MachineName,
							Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
							categoryID, 
							siteID);
					}
					finally
					{
						base.Checkin(uri);
					}

					if (oResult != null)
					{
						oResult.CacheTTLSeconds = cacheTTL;
						oResult.SetCacheKey = cacheKey;

						_cache.Insert(oResult);
					}
				}

				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve all CategorySiteArticles (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="categoryID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		public SiteArticleCollection RetrieveCategorySiteArticlesAndArticles(int categoryID, int siteID)
		{
			SiteArticleCollection oResult = null;
			string uri = string.Empty;

			try 
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);

				base.Checkout(uri);
				try
				{
					oResult = getService(uri).RetrieveCategorySiteArticlesAndArticles(categoryID, siteID);
				}
				finally
				{
					base.Checkin(uri);
				}

				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve all CategorySiteArticlesAndArticles (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteID"></param>
		/// <param name="categoryID"></param>
		/// <returns></returns>
		public SiteCategoryCollection RetrieveSiteCategoryChildren(int siteID, int categoryID)
		{
			SiteCategoryCollection oResult = null;
			string uri = string.Empty;

			try 
			{
				string cacheKey = SiteCategoryCollection.GetCacheKey(siteID, categoryID, SiteCategoryCollectionCacheKeySuffix.Children);
				
				oResult = _cache.Get(cacheKey) as SiteCategoryCollection;
				if (oResult == null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					Int32 cacheTTL = GenerateRandomTTL(Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_ARTICLE_SA")));

					base.Checkout(uri);
					try
					{
						oResult = getService(uri).RetrieveSiteCategoryChildren(System.Environment.MachineName,
							Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
							siteID, 
							categoryID);
					}
					finally
					{
						base.Checkin(uri);
					}

					if (oResult != null)
					{
						oResult.CacheTTLSeconds = cacheTTL;
						oResult.SetCacheKey = cacheKey;

						_cache.Insert(oResult);
					}
				}

				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve all SiteCategoryChildren (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// Retrieves all Categories for a given SiteID.  Used only for the Admin tool directly, however, used by
		/// the web code indirectly (RetrieveSiteCategory).
		/// </summary>
		/// <param name="siteID"></param>
		/// <returns></returns>
		public SiteCategoryCollection RetrieveSiteCategories(int siteID, bool forceLoadFlag)
		{
			SiteCategoryCollection oResult = null;
			string uri = string.Empty;

			try 
			{
				string cacheKey = SiteCategoryCollection.GetCacheKey(siteID, int.MinValue, SiteCategoryCollectionCacheKeySuffix.All);

				if (!forceLoadFlag)
				{
					oResult = _cache.Get(cacheKey) as SiteCategoryCollection;
				}
				
				if (oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					Int32 cacheTTL = GenerateRandomTTL(Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_ARTICLE_SA")));

					base.Checkout(uri);
					try
					{
						oResult = getService(uri).RetrieveSiteCategories(System.Environment.MachineName,
							Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
							siteID, forceLoadFlag);
					}
					finally
					{
						base.Checkin(uri);
					}

					if (oResult != null && !forceLoadFlag)
					{
						oResult.CacheTTLSeconds = cacheTTL;
						oResult.SetCacheKey = cacheKey;

						_cache.Insert(oResult);
					}
				}

				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve all SiteCategories (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// Retrieve an individual SiteCategory.
		/// </summary>
		/// <param name="siteID"></param>
		/// <param name="categoryID"></param>
		/// <returns></returns>
		public SiteCategory RetrieveSiteCategory(int siteID, int categoryID)
		{
			return RetrieveSiteCategories(siteID, false).GetSiteCategory(categoryID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="categoryID"></param>
		/// <returns></returns>
		public Category RetrieveCategory(int categoryID)
		{
			Category oResult = null;
			string uri = string.Empty;

			try 
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					oResult = getService(uri).RetrieveCategory(categoryID);
				}
				finally
				{
					base.Checkin(uri);
				}

				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve Category (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="articleID"></param>
		/// <returns></returns>
		public Article RetrieveArticle(int articleID)
		{
			Article oResult = null;
			string uri = string.Empty;

			try 
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					oResult = getService(uri).RetrieveArticle(articleID);
				}
				finally
				{
					base.Checkin(uri);
				}

				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve Article (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="category"></param>
		public int SaveCategory(Category category, Int32 memberID, Boolean isPublish)
		{
			string uri = string.Empty;

			try 
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					return getService(uri).SaveCategory(category, memberID, isPublish);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot save Category (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteCategory"></param>
		public int SaveSiteCategory(SiteCategory siteCategory, Int32 memberID, Boolean isPublish)
		{
			string uri = string.Empty;

			try 
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					return getService(uri).SaveSiteCategory(siteCategory, memberID, isPublish);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot save SiteCategory (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// This was created but most likely this will never be used at the SA level.  The ArticleBL.SaveArticle()
		/// will probably only be called by ArticleBL.SaveSiteArticle().
		/// </summary>
		/// <param name="article"></param>
		public int SaveArticle(Article article, Int32 memberID, Boolean isPublish)
		{
			string uri = string.Empty;

			try 
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					return getService(uri).SaveArticle(article, memberID, isPublish);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot save Article (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteArticle"></param>
		public int SaveSiteArticle(SiteArticle siteArticle, Int32 memberID, Boolean isPublish)
		{
			string uri = string.Empty;

			try 
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					return getService(uri).SaveSiteArticle(siteArticle, memberID, isPublish);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot save SiteArticle (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public WSOListCollection RetrieveWSOListCollection()
		{
			string uri = string.Empty;

			try 
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					return getService(uri).RetrieveWSOListCollection();
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve WSOListCollection (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="wsoList"></param>
		public void SaveWSOList(WSOList wsoList)
		{
			string uri = string.Empty;

			try 
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					getService(uri).SaveWSOList(wsoList);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve WSOList (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="name"></param>
		/// <param name="note"></param>
		public void CreateWSOList(string name, string note)
		{
			string uri = string.Empty;

			try 
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					getService(uri).CreateWSOList(name, note);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot create WSOList (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public WSOItemCollection RetrieveWSOItemCollection(int wsoListID)
		{
			string uri = string.Empty;

			try 
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					return getService(uri).RetrieveWSOItemCollection(wsoListID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve WSOItemCollection (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="wsoItem"></param>
		public void SaveWSOItem(WSOItem wsoItem)
		{
			string uri = string.Empty;

			try 
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					getService(uri).SaveWSOItem(wsoItem);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot save WSOItem (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="wsoItem"></param>
		public void DeleteWSOItem(WSOItem wsoItem)
		{
			string uri = string.Empty;

			try 
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					getService(uri).DeleteWSOItem(wsoItem);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot delete WSOItem (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="name"></param>
		/// <param name="note"></param>
		public void CreateWSOItem(int wsoListID, int siteID, int itemID, int itemTypeID)
		{
			string uri = string.Empty;

			try 
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					getService(uri).CreateWSOItem(wsoListID, siteID, itemID, itemTypeID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot create WSOItem (uri: " + uri + ")", ex));
			}
		}

		#endregion

		private IArticleService getService(string uri)
		{
			try
			{
				return (IArticleService)Activator.GetObject(typeof(IArticleService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		private int GenerateRandomTTL(int ttl)
		{
			return ttl + _random.Next(ttl / 2);
		}
	}
}
