using System;

using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ValueObjects.AttributeOption;


namespace Matchnet.Content.ServiceAdapters
{
	/// <summary>
	/// Summary description for AttributeOptionSA.
	/// </summary>
	public class AttributeOptionSA : SABase
	{
		private Cache _cache;
		private const string SERVICE_MANAGER_NAME = "AttributeOptionSM";

		/// <summary>
		/// 
		/// </summary>
		public static AttributeOptionSA Instance = new AttributeOptionSA();

		private AttributeOptionSA()
		{
			_cache = Cache.Instance;
		}


		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_SA_CONNECTION_LIMIT"));
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public Attributes GetAttributes()
		{
			string uri = "";
			try
			{
				Attributes attributes = _cache.Get(Attributes.CACHE_KEY) as Attributes;

				if (attributes == null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						attributes = getService(uri).GetAttributes();
					}
					finally
					{
						base.Checkin(uri);
					}
					attributes.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_BRAND_SA"));
					_cache.Insert(attributes);
				}

				return attributes;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve Attributes (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeName"></param>
		/// <param name="pGroupID"></param>
		/// <returns></returns>
		public AttributeOptionCollection GetAttributeOptionCollection(string pAttributeName, int pGroupID)
		{
			return GetAttributes().Get(pAttributeName, pGroupID);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeName"></param>
		/// <param name="pCommunityID"></param>
		/// <param name="pSiteID"></param>
		/// <param name="pBrandID"></param>
		/// <returns></returns>
		public AttributeOptionCollection GetAttributeOptionCollection(string pAttributeName, int pCommunityID, int pSiteID, int pBrandID)
		{
			Attributes attributes = GetAttributes();
			AttributeOptionCollection attributeOptionCollection = null;

			if (attributes.ContainsGroup(pAttributeName, pBrandID))
				attributeOptionCollection = GetAttributeOptionCollection(pAttributeName, pBrandID);
			else if (attributes.ContainsGroup(pAttributeName, pSiteID))
				attributeOptionCollection = GetAttributeOptionCollection(pAttributeName, pSiteID);
			else if (attributes.ContainsGroup(pAttributeName, pCommunityID))
				attributeOptionCollection = GetAttributeOptionCollection(pAttributeName, pCommunityID);

			if (attributeOptionCollection == null)
				attributeOptionCollection = GetAttributeOptionCollection(pAttributeName, Constants.NULL_INT);

			return attributeOptionCollection;
		}

		/// <summary>
		/// Saves an existing option or creates a new option.
		/// </summary>
		/// <param name="pAttributeOptionID">The AttributeOptionID of the option to save.  If this is null, a new option will be created.</param>
		/// <param name="pAttributeID"></param>
		/// <param name="pDescription"></param>
		/// <param name="pAttributeValue"></param>
		/// <param name="pResourceKey"></param>
		/// <param name="pListOrder"></param>
		public void SaveAttributeOption(int pAttributeOptionID, int pAttributeID, string pDescription, int pAttributeValue, string pResourceKey, int pListOrder)
		{
			string uri = "";
			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					getService(uri).SaveAttributeOption(pAttributeOptionID, pAttributeID, pDescription, pAttributeValue, pResourceKey, pListOrder);
				}
				finally
				{
					base.Checkin(uri);
				}
				_cache.Remove(Attributes.CACHE_KEY);
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve Attributes (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// Saves an existing group-specific option or creates a new group-specific option.
		/// </summary>
		/// <param name="pAttributeOptionGroupID"></param>
		/// <param name="pAttributeOptionID"></param>
		/// <param name="pGroupID"></param>
		/// <param name="pListOrder"></param>
		private void SaveAttributeOptionGroup(int pAttributeOptionGroupID, int pAttributeOptionID, int pGroupID, int pListOrder)
		{
			string uri = "";
			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					getService(uri).SaveAttributeOptionGroup(pAttributeOptionGroupID, pAttributeOptionID, pGroupID, pListOrder);
				}
				finally
				{
					base.Checkin(uri);
				}
				_cache.Remove(Attributes.CACHE_KEY);
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve Attributes (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeOptionID"></param>
		public void DeleteAttributeOption(int pAttributeOptionID)
		{
			string uri = "";
			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					getService(uri).DeleteAttributeOption(pAttributeOptionID);
				}
				finally
				{
					base.Checkin(uri);
				}
				_cache.Remove(Attributes.CACHE_KEY);
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve Attributes (uri: " + uri + ")", ex));
			}
		}

		//AttributeOptionGroupIDs are never exposed to the code, so this function isn't needed for now.
		/*private void DeleteAttributeOptionGroup(int pAttributeOptionGroupID)
		{
			string uri = "";
			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				getService(uri).DeleteAttributeOptionGroup(pAttributeOptionGroupID);
				_cache.Remove(Attributes.CACHE_KEY);
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve Attributes (uri: " + uri + ")", ex));
			}
		}*/

		/// <summary>
		/// Removes an option from a particular group.
		/// </summary>
		/// <param name="pAttributeOptionID"></param>
		/// <param name="pGroupID"></param>
		public void DeleteAttributeOptionGroup(int pAttributeOptionID, int pGroupID)
		{
			string uri = "";
			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					getService(uri).DeleteAttributeOptionGroup(pAttributeOptionID, pGroupID);
				}
				finally
				{
					base.Checkin(uri);
				}
				_cache.Remove(Attributes.CACHE_KEY);
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve Attributes (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeID"></param>
		/// <param name="pDescription"></param>
		/// <param name="pAttributeValue"></param>
		/// <param name="pResourceKey"></param>
		public void AddAttributeOption(int pAttributeID, string pDescription, int pAttributeValue, string pResourceKey)
		{
			int listOrder = 0;

			SaveAttributeOption(Constants.NULL_INT, pAttributeID, pDescription, pAttributeValue, pResourceKey, listOrder);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeOptionID"></param>
		/// <param name="pGroupID"></param>
		/// <param name="pListOrder"></param>
		public void AddAttributeOptionGroup(int pAttributeOptionID, int pGroupID, int pListOrder)
		{
			SaveAttributeOptionGroup(Constants.NULL_INT, pAttributeOptionID, pGroupID, pListOrder);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeOptionID"></param>
		/// <param name="pGroupID"></param>
		/// <param name="pListOrder"></param>
		public void SaveAttributeOptionGroup(int pAttributeOptionID, int pGroupID, int pListOrder)
		{
			SaveAttributeOptionGroup(Constants.NULL_INT, pAttributeOptionID, pGroupID, pListOrder);
		}

		//AttributeOptionGroupIDs are never exposed to the code, so this function isn't needed for now.
		/*public void SaveAttributeOptionGroup(int pAttributeOptionID, int pGroupID, int pListOrder)
		{
			SaveAttributeOptionGroup(pAttributeOptionGroupID, Constants.NULL_INT, Constants.NULL_INT, pListOrder);
		}*/

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeName"></param>
		/// <param name="pAttributeOptionID"></param>
		public void MoveUpAttributeOption(string pAttributeName, int pAttributeOptionID)
		{
			AttributeOptionCollection attributeOptionCollection = GetAttributes().Get(pAttributeName);
			MoveUpAttributeOption(attributeOptionCollection, pAttributeOptionID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeName"></param>
		/// <param name="pGroupID"></param>
		/// <param name="pAttributeOptionID"></param>
		public void MoveUpAttributeOption(string pAttributeName, int pAttributeOptionID, int pGroupID)
		{
			AttributeOptionCollection attributeOptionCollection = GetAttributes().Get(pAttributeName, pGroupID);
			MoveUpAttributeOption(attributeOptionCollection, pAttributeOptionID, pGroupID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="attributeOptionCollection"></param>
		/// <param name="pAttributeOptionID"></param>
		public void MoveUpAttributeOption(AttributeOptionCollection attributeOptionCollection, int pAttributeOptionID)
		{
			AttributeOption attributeOption = attributeOptionCollection.FindByID(pAttributeOptionID);

			if (attributeOption.ListOrder <= 0)
				return;

			AttributeOption swapAttributeOption = attributeOptionCollection.FindByListOrder(attributeOption.ListOrder - 1);

			if (swapAttributeOption != null)
			{
				SwapAttributeOptions(attributeOption, swapAttributeOption);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="attributeOptionCollection"></param>
		/// <param name="pAttributeOptionID"></param>
		/// <param name="pGroupID"></param>
		public void MoveUpAttributeOption(AttributeOptionCollection attributeOptionCollection, int pAttributeOptionID, int pGroupID)
		{
			AttributeOption attributeOption = attributeOptionCollection.FindByID(pAttributeOptionID);

			if (attributeOption.ListOrder <= 0)
				return;

			AttributeOption swapAttributeOption = attributeOptionCollection.FindByListOrder(attributeOption.ListOrder - 1);

			if (swapAttributeOption != null)
			{
				SwapAttributeOptions(attributeOption, swapAttributeOption, pGroupID);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeName"></param>
		/// <param name="pAttributeOptionID"></param>
		public void MoveDownAttributeOption(string pAttributeName, int pAttributeOptionID)
		{
			AttributeOptionCollection attributeOptionCollection = GetAttributes().Get(pAttributeName);
			MoveDownAttributeOption(attributeOptionCollection, pAttributeOptionID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeName"></param>
		/// <param name="pGroupID"></param>
		/// <param name="pAttributeOptionID"></param>
		public void MoveDownAttributeOption(string pAttributeName, int pAttributeOptionID, int pGroupID)
		{
			AttributeOptionCollection attributeOptionCollection = GetAttributes().Get(pAttributeName, pGroupID);
			MoveDownAttributeOption(attributeOptionCollection, pAttributeOptionID, pGroupID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="attributeOptionCollection"></param>
		/// <param name="pAttributeOptionID"></param>
		public void MoveDownAttributeOption(AttributeOptionCollection attributeOptionCollection, int pAttributeOptionID)
		{
			AttributeOption attributeOption = attributeOptionCollection.FindByID(pAttributeOptionID);

			if (attributeOption.ListOrder <= 0)
				return;

			AttributeOption swapAttributeOption = attributeOptionCollection.FindByListOrder(attributeOption.ListOrder + 1);

			if (swapAttributeOption != null)
			{
				SwapAttributeOptions(attributeOption, swapAttributeOption);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="attributeOptionCollection"></param>
		/// <param name="pAttributeOptionID"></param>
		/// <param name="pGroupID"></param>
		public void MoveDownAttributeOption(AttributeOptionCollection attributeOptionCollection, int pAttributeOptionID, int pGroupID)
		{
			AttributeOption attributeOption = attributeOptionCollection.FindByID(pAttributeOptionID);

			if (attributeOption.ListOrder <= 0)
				return;

			AttributeOption swapAttributeOption = attributeOptionCollection.FindByListOrder(attributeOption.ListOrder + 1);

			if (swapAttributeOption != null)
			{
				SwapAttributeOptions(attributeOption, swapAttributeOption, pGroupID);
			}
		}

		private void SwapAttributeOptions(AttributeOption attributeOption1, AttributeOption attributeOption2)
		{
			SaveAttributeOption(attributeOption1.AttributeOptionID, attributeOption1.AttributeID, attributeOption1.Description, attributeOption1.Value, attributeOption1.ResourceKey, attributeOption2.ListOrder);
			SaveAttributeOption(attributeOption2.AttributeOptionID, attributeOption2.AttributeID, attributeOption2.Description, attributeOption2.Value, attributeOption2.ResourceKey, attributeOption1.ListOrder);
		}

		private void SwapAttributeOptions(AttributeOption pAttributeOption1, AttributeOption pAttributeOption2, int pGroupID)
		{
			SaveAttributeOptionGroup(pAttributeOption1.AttributeOptionID, pGroupID, pAttributeOption2.ListOrder);
			SaveAttributeOptionGroup(pAttributeOption2.AttributeOptionID, pGroupID, pAttributeOption1.ListOrder);
		}

		private IAttributeOptionService getService(string uri)
		{
			try
			{
				return (IAttributeOptionService)Activator.GetObject(typeof(IAttributeOptionService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}
	
	}
}
