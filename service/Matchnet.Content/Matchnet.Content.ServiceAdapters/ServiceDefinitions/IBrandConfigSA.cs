﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.Content.ServiceAdapters.ServiceDefinitions
{
    public interface IBrandConfigSA
    {
        Brands GetBrands();
        Brand GetBrandByID(int brandID);
    }
}
