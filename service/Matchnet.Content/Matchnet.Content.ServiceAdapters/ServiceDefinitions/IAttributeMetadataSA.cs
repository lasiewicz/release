﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.AttributeMetadata;

namespace Matchnet.Content.ServiceAdapters.ServiceDefinitions
{
    public interface IAttributeMetadataSA
    {
        Attributes GetAttributes();
    }
}
