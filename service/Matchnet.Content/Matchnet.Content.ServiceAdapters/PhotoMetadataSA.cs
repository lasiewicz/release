﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Caching;
using Matchnet.Content.ValueObjects.Photos;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;

namespace Matchnet.Content.ServiceAdapters
{
    public class PhotoMetadataSA: SABase
    {
        private const string SERVICE_MANAGER_NAME = "PhotoMetadataSM";

		/// <summary>
		/// Singleton instance
		/// </summary>
		public static readonly PhotoMetadataSA Instance = new PhotoMetadataSA();

		private Cache _cache;

        private PhotoMetadataSA()
		{
			_cache = Cache.Instance;
		}

        public List<PhotoFileTypeRecord> GetPhotoFileTypeRecords()
        {
            var records = new List<PhotoFileTypeRecord>();
            var uri = String.Empty;

            try
            {
                var cachedPhotoFileTypeRecords = _cache.Get(CachedPhotoFileTypeRecords.CacheKey) as CachedPhotoFileTypeRecords;

                if (cachedPhotoFileTypeRecords == null || cachedPhotoFileTypeRecords.PhotoFileTypeRecords == null || cachedPhotoFileTypeRecords.PhotoFileTypeRecords.Count < 1)
                {
                    uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);

                    base.Checkout(uri);
                    try
                    {
                        records = getService(uri).GetPhotoFileTypeRecords();
                        cachedPhotoFileTypeRecords = new CachedPhotoFileTypeRecords {PhotoFileTypeRecords = records};
                        _cache.Add(cachedPhotoFileTypeRecords);
                    }
                    catch (Exception ex)
                    {
                        throw new SAException("Unable to load PhotoFileTypeRecords", ex);
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }
                else
                {
                    records = cachedPhotoFileTypeRecords.PhotoFileTypeRecords;
                }

            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot load PhotoFileTypeRecords (uri: " + uri + ")", ex));
            }

            return records;

        }

        public List<PhotoFileTypeRecord> GetPhotoFileTypeRecords(int siteId)
        {
            var records = GetPhotoFileTypeRecords();
            var siteAdjustedRecords = new List<PhotoFileTypeRecord>();

            foreach(var record in records)
            {
                var siteRecord = (from phts in record.SiteRecords where phts.SiteId == siteId select phts).FirstOrDefault();
                if(siteRecord != null)
                {
                    siteAdjustedRecords.Add(new PhotoFileTypeRecord { ID = record.ID, MaxHeight = siteRecord.MaxHeight, 
                        MaxWidth = siteRecord.MaxWidth, Active = siteRecord.Active, Description = record.Description });
                }
                else
                {
                    siteAdjustedRecords.Add(record); 
                }
            }

            return siteAdjustedRecords;
        }


		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_SA_CONNECTION_LIMIT"));
		}

        private IPhotoMetadataService getService(string uri)
        {
            try
            {
                return (IPhotoMetadataService)Activator.GetObject(typeof(IPhotoMetadataService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }
    }
}
