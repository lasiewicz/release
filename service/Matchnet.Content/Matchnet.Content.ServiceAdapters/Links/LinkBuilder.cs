using System;

using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PageConfig;

namespace Matchnet.Content.ServiceAdapters.Links
{
	/// <summary>
	/// Summary description for LinkBuilder.
	/// </summary>
	public class LinkBuilder : ILinkBuilder
	{
		/// <summary> </summary>
		protected string _url;
		/// <summary> </summary>
		protected bool _ssl;
		/// <summary> </summary>
		protected Brand _brand;
		private bool _newWindow;
		private string _name;
		private int _width;
		private int _height;
		private string _properties;
		private LayoutTemplate _layoutTemplate = LayoutTemplate.None;
		private bool _persistLayoutTemplate;
		private string _requestUrl;
		/// <summary> </summary>
		protected ParameterCollection _parameterCollection;
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="pUrl"></param>
		/// <param name="pBrand"></param>
		/// <param name="pParameterCollection"></param>
		public LinkBuilder(string pUrl, Brand pBrand, ParameterCollection pParameterCollection)
		{
			_url = pUrl;
			_brand = pBrand;
			_parameterCollection = pParameterCollection;
		}

		#region Properties

		/// <summary>
		/// 
		/// </summary>
		public string Url
		{
			get
			{
				return GetUrl();
			}
			set
			{
				_url = value;
			}
		}
		/// <summary>
		/// 
		/// </summary>
		public bool SSL
		{
			get
			{
				return _ssl;
			}
			set
			{
				_ssl = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool NewWindow
		{
			get
			{
				return _newWindow;
			}
			set
			{
				_newWindow = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int Width
		{
			get
			{
				return _width;
			}
			set
			{
				_width = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public int Height
		{
			get
			{
				return _height;
			}
			set
			{
				_height = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Properties
		{
			get
			{
				return _properties;
			}
			set
			{
				_properties = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public LayoutTemplate LayoutTemplate
		{
			get
			{
				return _layoutTemplate;
			}
			set
			{
				_layoutTemplate = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public bool PersistLayoutTemplate
		{
			get
			{
				return _persistLayoutTemplate;
			}
			set
			{
				_persistLayoutTemplate = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string RequestUrl
		{
			get
			{
				return _requestUrl;
			}
			set
			{
				_requestUrl = value;
			}
		}

		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public virtual string GetUrl()
		{
			string returnUrl = string.Empty;

			if (!_url.StartsWith("http"))
			{
				string host;
				string s = _ssl ? "s" : string.Empty;  //Include "s" after http if _ssl is set

				if (!_ssl || (Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("LOCAL_SECURE_MODE"))))
				{
					host = GetHost();
					returnUrl = string.Format("http{0}://{1}{2}", s, host, _url);
				}
				else
				{
					host = _brand.Site.SSLUrl;
					returnUrl = host + _url;
				}
			}
			else
			{
				returnUrl = _url;
			}

			//If a destination URL was present in the Request URL, add it to this URL:
			/*if (GetRequestParam("DestinationURL") != null)
			{
				_parameterCollection.Add("DestinationURL", GetRequestParam("DestinationURL"));
			}*/

			if (_persistLayoutTemplate || GetRequestParam("PersistLayoutTemplate") != null)
			{
				_parameterCollection.Add("PersistLayoutTemplate", "1");
			}

			//If PersistLayoutTemplate was set in the request URL, keep the same Layout Template used for the current page request
			if (GetRequestParam("PersistLayoutTemplate") != null && _layoutTemplate == LayoutTemplate.None)
			{
				_layoutTemplate = GetRequestLayoutTemplate();
			}

			if (_layoutTemplate != LayoutTemplate.None)
			{
				_parameterCollection.Add("LayoutTemplateID", ((int)_layoutTemplate).ToString());
			}

			returnUrl = _parameterCollection.AddParameters(returnUrl);

			if (_newWindow)
			{
				returnUrl = GetNewWindowURL(returnUrl, _name, _width, _height, _properties);
			}

			return returnUrl;
		}

		private string GetHost()
		{
			return new Uri(_requestUrl).Host;
		}

		private string GetRequestParam(string paramName)
		{
			return GetUrlParam(_requestUrl, paramName);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="url"></param>
		/// <param name="paramName"></param>
		/// <returns></returns>
		public static string GetUrlParam(string url, string paramName)
		{
			string [] parts = url.Split(new char[] {'?'}, 2);

			if (parts.Length < 2)
				return null;

			string [] parameters = parts[1].Split(new char[] {'&'});

			for (int i = 0; i < parameters.Length; i++)
			{
				if (parameters[i].StartsWith(paramName))
				{
					return parameters[i].Substring(paramName.Length + 1);
				}
			}

			return null;
		}

		private LayoutTemplate GetRequestLayoutTemplate()
		{
            LayoutTemplate layout = ValueObjects.PageConfig.LayoutTemplate.None;
			string layoutTemplateID = GetRequestParam("LayoutTemplateID");

			if (layoutTemplateID != null)
			{
                if (Enum.IsDefined(typeof(LayoutTemplate), layoutTemplateID))
                {
                    layout = (LayoutTemplate)Enum.Parse(typeof(LayoutTemplate), layoutTemplateID);
                }
			}

            return layout;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pURL"></param>
		/// <param name="pName"></param>
		/// <param name="pWidth"></param>
		/// <param name="pHeight"></param>
		/// <param name="pProperties"></param>
		/// <returns></returns>
		protected static string GetNewWindowURL(string pURL, string pName, int pWidth, int pHeight, string pProperties)
		{
			return string.Format("javascript:launchWindow('{0}', '{1}', {2}, {3}, '{4}')", pURL, pName, pWidth, pHeight, pProperties);
		}
	}
}
