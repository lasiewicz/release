using System;
using System.Collections;

namespace Matchnet.Content.ServiceAdapters.Links
{
	/// <summary>
	/// Summary description for ParameterCollection.
	/// </summary>
	public class ParameterCollection
	{
		private Hashtable _parameters = new Hashtable();

		/// <summary>
		/// 
		/// </summary>
		public ParameterCollection()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pName"></param>
		/// <param name="pValue"></param>
		public void Add(string pName, string pValue)
		{
			_parameters[pName] = pValue;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pName"></param>
		/// <returns></returns>
		public string Get(string pName)
		{
			return _parameters[pName].ToString();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pName"></param>
		public void Delete(string pName)
		{
			_parameters.Remove(pName);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pUrl"></param>
		/// <returns></returns>
		public string AddParameters(string pUrl)
		{
			string returnUrl = pUrl;
			string delim = (returnUrl.IndexOf("?") == -1) ? "?" : "&";

			foreach (string name in _parameters.Keys)
			{
				if (LinkBuilder.GetUrlParam(returnUrl, name) == null)
				{
					returnUrl += delim + name + "=" + _parameters[name];
					delim = "&";
				}
			}

			return returnUrl;
		}
	}
}
