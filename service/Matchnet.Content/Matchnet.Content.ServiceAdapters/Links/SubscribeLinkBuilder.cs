using System;

using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.Content.ServiceAdapters.Links
{
	/// <summary>
	/// Summary description for SubscribeLinkBuilder.
	/// </summary>
	public class SubscribeLinkBuilder : LinkBuilder
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="pUrl"></param>
		/// <param name="pBrand"></param>
		/// <param name="pParameterCollection"></param>
		public SubscribeLinkBuilder(string pUrl, Brand pBrand, ParameterCollection pParameterCollection) : base(pUrl, pBrand, pParameterCollection)
		{
			
		}

		/*private enum ParameterName
		{
			srid,
			prtid,
			pid
		}

		protected int _sourceID;
		protected int _purchaseReasonTypeID;
		protected int _planID;
		public int SourceID
		{
			get
			{
				return GetParameterInt(ParameterName.srid);
			}
			set
			{
				_parameterCollection.Add(Enum.GetName(typeof(ParameterName), ParameterName.srid), value.ToString());
			}
		}

		public int PurchaseReasonTypeID
		{
			get
			{
				return GetParameterInt(ParameterName.prtid);
			}
			set
			{
				_parameterCollection.Add(Enum.GetName(typeof(ParameterName), ParameterName.prtid), value.ToString());
			}
		}

		public int PlanID
		{
			get
			{
				return GetParameterInt(ParameterName.pid);
			}
			set
			{
				_parameterCollection.Add(Enum.GetName(typeof(ParameterName), ParameterName.pid), value.ToString());
			}
		}

		private int GetParameterInt(ParameterName pName)
		{
			return Matchnet.Conversion.CInt(_parameterCollection.Get(Enum.GetName(typeof(ParameterName), pName)));
		}*/

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override string GetUrl()
		{
            // No longer required with UPS PUI. 12/21/2011
			//base._ssl = true;

			return base.GetUrl();
		}
	}

	/// <summary>
	/// 
	/// </summary>
	public enum PurchaseReasonType
	{
		/// <summary></summary>
		AnotherMember = 1,
		/// <summary></summary>
		SubscribeNowBanner = 2,
		/// <summary></summary>
		CombinedRegSub = 3,
		/// <summary></summary>
		FiveDayFreeTrialEmail = 4,
		/// <summary></summary>
		FiveDayFreeTrialExitPopup = 5,
		/// <summary></summary>
		YourMatchesEmail = 6,
		/// <summary></summary>
		YNMViralEmail = 7,
		/// <summary></summary>
		YNMMutualEmail = 8,
		/// <summary></summary>
		ExternalEmailAlert = 9,
		/// <summary></summary>
		AttemptToAccessInbox = 10,
		/// <summary></summary>
		AttemptToAccessWhosIMdYou = 11,
		/// <summary></summary>
		AttemptToAccessWhosEmailedYou = 12,
		/// <summary></summary>
		AttemptToIMFullProfile = 13,
		/// <summary></summary>
		AttemptToEmailFullProfile = 14,
		/// <summary></summary>
		SubscribeButton = 15,
		/// <summary></summary>
		SubscribeLinkInFooter = 16,
		/// <summary></summary>
		HelpSectionLinks = 17,
		/// <summary></summary>
		ThirdPartyPromotion = 18,
		/// <summary></summary>
		AdminPurchasesOnBehalfOfMember = 19,
		/// <summary></summary>
		AdminAdjustmentsAndOrCredits = 20,
		/// <summary></summary>
		AttemptToIMMiniProfile = 21,
		/// <summary></summary>
		AttemptToEmailMiniProfile = 22,
		/// <summary></summary>
		AttemptToIMGalleryMiniProfile = 23,
		/// <summary></summary>
		AttemptToEmailGalleryMiniProfile = 24,
		/// <summary></summary>
		AttemptToChat = 25,
		/// <summary></summary>
		AstrologyBasic = 26,
		/// <summary></summary>
		FiveDayFreeTrialReg = 27,
		/// <summary></summary>
		MessageBoardSubscription = 28,
		/// <summary></summary>
		ECardSubscription = 29,
		/// <summary></summary>
		IMailPermissionsEmailSubjectLine = 50,
		/// <summary></summary>
		IMailPermissionsMissedIMSubjectLine = 46,
		/// <summary></summary>
		IMailPermissionsEcardUpSellBanner = 47,
		/// <summary></summary>
		IMailPermissionsEcardReplyByEmail = 48,
		/// <summary></summary>
		IMailPermisisonsComposePromptReplyToFlirt = 49,
		/// <summary></summary>
		AttemptToMessageSecondTime = 131,
		/// <summary></summary>
		AttemptToReplyToAFlirt = 132,
		/// <summary></summary>
		AttemptToReadFlirtFromNonFrenchSpeaker = 133,
		/// <summary></summary>
		AttemptToReadMessageFromNonFrenchSpeaker = 134,
		AttemptToEmailHomePageMicroProfile = 24, 
		AttemptToEmailHomePageSpotlightProfile = 138,
		/// <summary></summary>
		AttemptToAccessPremiumServiceSettings = 157,	
		/// <summary></summary>
		ViewedHighlightProfileInfo = 158,
		/// <summary></summary>
        /// <summary></summary>
        AttemptToAccessUpsalePackagesWithNoSubscription = 159,
        AttemptToAccessFixedPricingUpsalePackagesWithNoSubscription = 160,	
        JmeterInvitationEmailAttempt = 282,
		/// <summary></summary>
		JmeterOneOnOneIMAttempt = 280,
		/// <summary></summary>
		JmeterOneOnOneEmailAttempt = 281,
		/// <summary>MPR-673</summary>
		ImUpgradeToChatLink = 323,
		/// <summary>MPR-673</summary>
		AttemptToEmailHomePageHeroProfile=424,
		/// <summary>MPR-1166</summary>
		AttemptToEmailHomePageMatches=426,
		AttemptToEmailMiniProfileBIG=427,
		AttemptToEmailGalleryMiniProfileBIG=428,
		AttemptToIMFavoritsOnline=787,
		AttemptToAccessWhoSentYouECard=850,
        HomePageAnnouncementBanner = 886,
        QuestionAnswerEmailButton = 887,
        CommentOnMyProfileUpgrade = 892,
        AttemptToReadMaskedEmail = 921,
        AttemptToReplyToRepliedVIPEmail = 922,
        AddAllAccessToCurrentSub = 929,
        AddAllAccessToCurrentSubFromOverlay = 932,
        AddAllAccessFromPremiumServicesLink = 933,
        RefillAllAccessEmails = 934,
        RefillAllAccessEmailsFromOverlay = 935,
        AttemptToIMFullProfileM2M = 1054,
        EmailOnMyProfileUpgrade = 1055,
        EmailOnMiniProfileHover = 1174,
        AttemptToIMMiniProfileHover = 1175,
        AttemptToBlockSearchResultsPage = 1186,
        AttemptToEmailSubBlockComposePage = 1239,
        AttemptToEmailSubBlockProfilePage = 1240,
        AttemptToEmailMiniProfileListView = 1292,
        AttemptToIMMiniProfileListView = 1293,
        HideRightAdProfilePage = 1294, 
        DontGo=1479,
        WantToGuaranteeAllAccessViewProfileComposeInitialPurchase=4148,
        RebuyAdditionalAllAccessEmailsViewProfileCompose=4149,
        WantToGuaranteeAllAccessViewProfileComposeInitialPurchaseYesOnOverlay=4170,
        RebuyAdditionalAllAccessEmailsViewProfileComposeYesOnOverlay=4171,
        HotlistViewedYourProfile=4175,
        SubscriptionPurchaseFromSpotlightedProfileUpgrade=4258,
        SubscriptionPurchaseFromIMHistory=4780
	}

	/// <summary>
	/// 
	/// </summary>
	public enum LinkParent
	{
		/// <summary>
		/// 
		/// </summary>
		FullProfile = 1,
		/// <summary>
		/// 
		/// </summary>
        MiniProfile = 2,
		/// <summary>
		/// 
		/// </summary>
		GalleryMiniProfile = 3,
		/// <summary>
		/// MatchMeterSendInvitation (Jmeter)
		/// </summary>
		MatchMeterSendInvitation = 4,
		/// <summary>
		/// MatchMeterOneOnOne (Jmeter)
		/// </summary>
		MatchMeterOneOnOne = 5, 
		HomePageMicroProfile = 6, 
		HomePageSpotlightProfile = 7,
		HomePageHeroProfile = 8,
		MiniProfileTop = 9,
		GalleryMiniProfileTop = 10,
		HomePageMicroProfileMatches = 11,
		QuestionPageAnswerProfile = 12,
		FavoritsOnline=13,
        QuestionAnswerEmailButton = 14
	}
}
