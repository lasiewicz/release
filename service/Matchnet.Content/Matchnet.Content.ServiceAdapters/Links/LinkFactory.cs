using System;

using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PageConfig;

namespace Matchnet.Content.ServiceAdapters.Links
{
	/// <summary>
	/// Summary description for LinkFactory.
	/// </summary>
	public class LinkFactory
	{
		/// <summary>
		/// Singleton
		/// </summary>
		public static LinkFactory Instance = new LinkFactory();

		private LinkFactory()
		{
		}

		/// <summary>
		/// Bare minimum GetLink
		/// </summary>
		public string GetLink(string pUrl, Brand pBrand, string pRequestUrl)
		{
			return GetLink(pUrl, new ParameterCollection(), pBrand, pRequestUrl, false);
		}

		/// <summary>
		/// Bare minimum GetLink (with params)
		/// </summary>
		public string GetLink(string pUrl, ParameterCollection pParameterCollection, Brand pBrand, string pRequestUrl)
		{
			return GetLink(pUrl, pParameterCollection, pBrand, pRequestUrl, false);
		}

		/// <summary>
		/// GetLink with SSL option
		/// </summary>
		public string GetLink(string pUrl, Brand pBrand, string pRequestUrl, bool pSSL)
		{
			return GetLink(pUrl, new ParameterCollection(), pBrand, pRequestUrl, pSSL, false, string.Empty, 0, 0, string.Empty, false, LayoutTemplate.None);
		}

		/// <summary>
		/// GetLink with SSL option (with params)
		/// </summary>
		public string GetLink(string pUrl, ParameterCollection pParameterCollection, Brand pBrand, string pRequestUrl, bool pSSL)
		{
			return GetLink(pUrl, pParameterCollection, pBrand, pRequestUrl, pSSL, false, string.Empty, 0, 0, string.Empty, false, LayoutTemplate.None);
		}

		/// <summary>
		/// Layout Template, no Popup
		/// </summary>
		public string GetLink(string pUrl, ParameterCollection pParameterCollection, Brand pBrand, string pRequestUrl, bool pSSL, bool pPersistLayoutTemplate, LayoutTemplate pLayoutTemplate)
		{
			return GetLink(pUrl, pParameterCollection, pBrand, pRequestUrl, pSSL, false, string.Empty, 0, 0, string.Empty, pPersistLayoutTemplate, pLayoutTemplate);
		}

		/// <summary>
		/// Popup, no LayoutTemplate
		/// </summary>
		public string GetLink(string pUrl, ParameterCollection pParameterCollection, Brand pBrand, string pRequestUrl, bool pSSL, bool pNewWindow, string pName, int pWidth, int pHeight, string pProperties)
		{
			return GetLink(pUrl, pParameterCollection, pBrand, pRequestUrl, pSSL, pNewWindow, pName, pWidth, pHeight, pProperties, false, LayoutTemplate.None);
		}

		/// <summary>
		/// All parameters
		/// </summary>
		public string GetLink(string pUrl, ParameterCollection pParameterCollection, Brand pBrand, string pRequestUrl, bool pSSL, bool pNewWindow, string pName, int pWidth, int pHeight, string pProperties, bool pPersistLayoutTemplate, LayoutTemplate pLayoutTemplate)
		{
			ILinkBuilder lb = GetLinkBuilder(pUrl, pBrand, pParameterCollection);

			lb.RequestUrl = pRequestUrl;
			lb.SSL = pSSL;
			lb.NewWindow = pNewWindow;
			lb.Name = pName;
			lb.Width = pWidth;
			lb.Height = pHeight;
			lb.Properties = pProperties;
			lb.PersistLayoutTemplate = pPersistLayoutTemplate;
			lb.LayoutTemplate = pLayoutTemplate;

			return lb.GetUrl();
		}

		private ILinkBuilder GetLinkBuilder(string pUrl, Brand pBrand, ParameterCollection pParameterCollection)
		{
			ILinkBuilder returnLB;
			
			switch (PageConfigSA.Instance.GetAppPath(pUrl))
			{
				case "/Applications/Subscription":
					returnLB = new SubscribeLinkBuilder(pUrl, pBrand, pParameterCollection);
					break;
				default:
					return new LinkBuilder(pUrl, pBrand, pParameterCollection);
			}

			return returnLB;
		}
	}
}
