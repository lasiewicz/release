using System;

using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PageConfig;

namespace Matchnet.Content.ServiceAdapters.Links
{
	/// <summary>
	/// Summary description for ILinkBuilder.
	/// </summary>
	public interface ILinkBuilder
	{
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		string GetUrl();

		/// <summary>
		/// 
		/// </summary>
		string Url
		{
			get;
			set;
		}
		/// <summary> </summary>
		bool SSL {get;set;}
		/// <summary> </summary>
		bool NewWindow{get;set;}
		/// <summary> </summary>
		string Name{get;set;}
		/// <summary> </summary>
		int Width{get;set;}
		/// <summary> </summary>
		int Height{get;set;}
		/// <summary> </summary>
		string Properties{get;set;}
		/// <summary> </summary>
		LayoutTemplate LayoutTemplate{get;set;}
		/// <summary> </summary>
		bool PersistLayoutTemplate{get;set;}
		/// <summary> </summary>
		string RequestUrl{get;set;}
	}
}
