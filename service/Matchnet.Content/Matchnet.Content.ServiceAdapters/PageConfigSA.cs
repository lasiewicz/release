using System;
using System.Data;
using System.IO;

using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Content.ValueObjects.ServiceDefinitions;


namespace Matchnet.Content.ServiceAdapters
{
	/// <summary>
	/// Brand configuration service adapter
	/// </summary>
	public class PageConfigSA : SABase
	{
		private const string SERVICE_MANAGER_NAME = "PageConfigSM";

		/// <summary>
		/// Singleton instance
		/// </summary>
		public static readonly PageConfigSA Instance = new PageConfigSA();

		private Cache _cache;
		private SitePages _lastLoadedSitePages = null;
		private AnalyticsPage _lastLoadedAnalyticsPage = null;

		private PageConfigSA()
		{
			_cache = Cache.Instance;
		}


		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_SA_CONNECTION_LIMIT"));
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="fullPath"></param>
		/// <returns></returns>
		public string GetAppPath(string fullPath)
		{
            if (fullPath == "/") return fullPath;
            
            if (fullPath.Length > 259)
			{
				fullPath = fullPath.Substring(0, 259);
			}
			return Path.GetDirectoryName(fullPath).Replace(@"\", "/");
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="fullPath"></param>
		/// <returns></returns>
		public string GetPageName(string fullPath)
		{
			if (fullPath.Length > 259)
			{
				fullPath = fullPath.Substring(0, 259);
			}
			return Path.GetFileNameWithoutExtension(fullPath);
		}

		/// <summary>
		/// Mmaps a url path to a page/app object
		/// </summary>
		/// <param name="siteID">siteID</param>
		/// <param name="fullPath">url path, (i.e. /Applications/foo/bar.aspx)</param>
		/// <returns>Page object with app object, null if not found</returns>
		public Page GetPage(int siteID, string fullPath)
		{
			string appPath = GetAppPath(fullPath);
			string pageName = GetPageName(fullPath);

			SitePages sitePages = getSitePages();

			AppInternal appInternal = sitePages.GetApp(appPath);

			if (appInternal == null)
			{
				//not found
				return null;
			}

			PageInternal pageInternal = appInternal.GetPage(pageName);

			if (pageInternal == null)
			{
				//not found
				return null;
			}

			SitePage sitePage = pageInternal.GetSitePage(siteID);

			if (sitePage == null)
			{
				//not found
				return null;
			}

			App app = new App(appInternal);
			return new Page(app, pageInternal, sitePage);
		}


		/// <summary>
		/// Look up the Analytics data (i.e. AnalyticsName) for the given page in the AnalyticsPage table.
		/// </summary>
		/// <param name="analyticsPageID"></param>
		/// <returns></returns>
		public AnalyticsPage GetAnalyticsPage(Int32 analyticsPageID)
		{
			string uri = String.Empty;
			try
			{
				AnalyticsPage analyticsPage = _cache.Get(AnalyticsPage.GetCacheKey(analyticsPageID)) as AnalyticsPage;

				if (analyticsPage == null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						analyticsPage = getService(uri).GetAnalyticsPage(analyticsPageID);
						_lastLoadedAnalyticsPage = analyticsPage;
					}
					catch (Exception ex)
					{
						//use last loaded analyticsPage if we cannot get them from svc

						if (_lastLoadedAnalyticsPage == null)
						{
							throw ex;
						}

						new SAException("Unable to load analyticsPage, reverting to last successful load.", ex);
						_lastLoadedAnalyticsPage.CacheTTLSeconds = 60;
						_cache.Insert(_lastLoadedAnalyticsPage);
						return _lastLoadedAnalyticsPage;
					}
					finally
					{
						base.Checkin(uri);
					}
					analyticsPage.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_ANALYTICSPAGE_SA"));
					_cache.Insert(analyticsPage);
				}

				return analyticsPage;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve AnalyticsPage (uri: " + uri + ")", ex));
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="analyticsPageID"></param>
		/// <returns></returns>
		public String GetAnalyticsName(Int32 analyticsPageID)
		{
			return ((AnalyticsPage)(GetAnalyticsPage(analyticsPageID))).AnalyticsName;
		}


		private SitePages getSitePages()
		{
			string uri = "";
			try
			{
				SitePages sitePages = _cache.Get(SitePages.CACHE_KEY) as SitePages;

				if (sitePages == null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						sitePages = getService(uri).GetSitePages();
						_lastLoadedSitePages = sitePages;
					}
					catch (Exception ex)
					{
						//use last loaded sitePages if we cannot get them from svc

						if (_lastLoadedSitePages == null)
						{
							throw ex;
						}

						new SAException("Unable to load sitePages, reverting to last successful load.", ex);
						_lastLoadedSitePages.CacheTTLSeconds = 60;
						_cache.Insert(_lastLoadedSitePages);
						return _lastLoadedSitePages;
					}
					finally
					{
						base.Checkin(uri);
					}
					sitePages.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_SITEPAGE_SA"));
					_cache.Insert(sitePages);
				}

				return sitePages;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve site pages (uri: " + uri + ")", ex));
			}
		}


		private IPageConfigService getService(string uri)
		{
			try
			{
				return (IPageConfigService)Activator.GetObject(typeof(IPageConfigService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}
	}
}
