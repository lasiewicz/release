using System;

using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.Image;
using Matchnet.Content.ValueObjects.ServiceDefinitions;


namespace Matchnet.Content.ServiceAdapters
{
	/// <summary>
	/// Summary description for ImageSA.
	/// </summary>
	public class ImageSA : SABase
	{
		#region constants
		private const string SERVICE_MANAGER_NAME = "ImageSM";
		private const string IMAGE_ROOT_CACHE_KEY_PREFIX = "~IMAGEUNCROOT";
		private const string IMAGE_LIST_CACHE_KEY_PREFIX = "~IMAGELIST^{0}{1}{2}{3}{4}{5}";
		#endregion

		#region class variables
		private Cache _cache = null;
		#endregion

		#region singleton
		/// <summary>
		/// 
		/// </summary>
		public static readonly ImageSA Instance = new ImageSA();

		/// <summary>
		/// Private constructor removes the public no-arg constructor so you have to go through Instance
		/// </summary>
		private ImageSA()
		{
			_cache = Cache.Instance;
		}
		#endregion

		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_SA_CONNECTION_LIMIT"));
		}

		/// <summary>
		/// Retrieves the UNC root for images
		/// </summary>
		/// <returns>UNC root as a string</returns>
		public ImageRoot GetImageUNCRoot()
		{
			ImageRoot oResult = null;
			string uri = "";

			try 
			{
				oResult = _cache.Get(GetImageRootCacheKey()) as ImageRoot;
				
				if(oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).GetImageUNCRoot();
					}
					finally
					{
						base.Checkin(uri);
					}
					oResult.SetCacheKey = GetImageRootCacheKey();
					CacheImageRoot(oResult);
				}
				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve Image Root (uri: " + uri + ")", ex));
			}
		}

		#region private Cache Routines
		private void CacheImageRoot(ImageRoot pImageRoot) 
		{
			pImageRoot.CachePriority = CacheItemPriorityLevel.Normal;
			pImageRoot.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_IMAGE_SA"));
			_cache.Insert(pImageRoot);
		}

		private void CacheImageList(ImageList pImageList) 
		{
			pImageList.CachePriority = CacheItemPriorityLevel.Normal;
			pImageList.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_IMAGE_SVC"));
			_cache.Insert(pImageList);
		}

		private string GetImageRootCacheKey()
		{
			return IMAGE_ROOT_CACHE_KEY_PREFIX;
		}

		private string GetImageListCacheKey(string filename, int applicationID, int privateLabelID, int basePrivateLabelID, int languageID, int domainID)
		{
			return String.Format(IMAGE_LIST_CACHE_KEY_PREFIX, filename, applicationID, privateLabelID, basePrivateLabelID, languageID, domainID);
		}
		#endregion


		private IImageService getService(string uri)
		{
			try
			{
				return (IImageService)Activator.GetObject(typeof(IImageService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}
	}
}
