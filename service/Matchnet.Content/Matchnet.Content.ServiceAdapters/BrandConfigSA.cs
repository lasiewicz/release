using System;
using System.Collections;
using System.Linq;
using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ServiceAdapters.ServiceDefinitions;


namespace Matchnet.Content.ServiceAdapters
{
	/// <summary>
	/// Brand configuration service adapter
	/// </summary>
    public class BrandConfigSA : SABase, IBrandConfigSA
	{
		private const string SERVICE_MANAGER_NAME = "BrandConfigSM";

		/// <summary>
		/// Singleton instance
		/// </summary>
		public static readonly BrandConfigSA Instance = new BrandConfigSA();

		private Cache _cache;
		private Brands _lastLoadedBrands = null;
		private Sites _lastLoadedSites = null;

		private BrandConfigSA()
		{
			_cache = Cache.Instance;
		}


		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_SA_CONNECTION_LIMIT"));
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public Brands GetBrands()
		{
			return GetBrands(false);
		}
			
		/// <summary>
		/// Retreives brands collection
		/// </summary>
		/// <returns>Brands object</returns>
		public Brands GetBrands(bool ignoreSACache)
		{
			string uri = "";
			try
			{
				Brands brands = null;
				
				if (!ignoreSACache)
				{
					brands = _cache.Get(Brands.CACHE_KEY) as Brands;
				}

				if (brands == null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						brands = getService(uri).GetBrands();
						_lastLoadedBrands = brands;
					}
					catch (Exception ex)
					{
						//use last loaded brands if we cannot get them from svc

						if (_lastLoadedBrands == null)
						{
							throw ex;
						}

						new SAException("Unable to load brands, reverting to last successful load.", ex);
						_lastLoadedBrands.CacheTTLSeconds = 60;
						_cache.Insert(_lastLoadedBrands);
						return _lastLoadedBrands;
					}
					finally
					{
						base.Checkin(uri);
					}

					if (!ignoreSACache)
					{
						brands.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_BRAND_SA"));
						_cache.Insert(brands);
					}
				}

				return brands;
			}
			catch (Exception ex)
			{
				throw(new SAException("Cannot retrieve brands (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// Retrieves sites collection
		/// </summary>
		/// <returns>Sites collection</returns>
		public Sites GetSites()
		{
			string uri = String.Empty;

			try
			{
				Sites sites = null;

				sites = (Sites) _cache.Get(Sites.CONTENT_BRAND_ALL_SITES_CACHE_KEY);

				if (sites != null)
					return sites;

				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				base.Checkout(uri);
				try
				{
					sites = getService(uri).GetSites();
					_lastLoadedSites = sites;
				}
				catch (Exception ex)
				{
					//use last loaded sites if we cannot get them from svc

					if (_lastLoadedSites == null)
					{
						throw ex;
					}

					new SAException("Unable to load sites, reverting to last successful load.", ex);
					_lastLoadedSites.CacheTTLSeconds = 60;
					_cache.Insert(_lastLoadedSites);
					return _lastLoadedSites;
				}
				finally
				{
					base.Checkin(uri);
				}
			
				return sites;
			}
			catch (Exception ex)
			{
				throw (new SAException("Cannot retrieve sites (uri: " + uri + ")", ex));
			}
		}

		#region TOFIX - These methods need caching/optimization -tbankston
		/// <summary>
		/// Retrieves all the brands for a particular site
		/// </summary>
		/// <param name="siteID">The SiteID of the site for which to retrieve brands.</param>
		/// <returns>Brands object</returns>
		public Brands GetBrandsBySite(int siteID)
		{
			Brands allBrands = GetBrands();
			Brands brands = new Brands();

			foreach (Brand brand in allBrands)
			{
				if (brand.Site.SiteID == siteID)
				{
					brands.AddBrand(brand);
				}
			}

			return brands;
 		}

        public Site GetSiteByID(int siteId)
        {
            var sites = GetSites();
            return sites.Cast<object>().Where(site => ((Site) site).SiteID == siteId).Cast<Site>().FirstOrDefault();

        }

		/// <summary>
		/// Retrieves all the brands for a particular community
		/// </summary>
		/// <param name="communityID">The CommunityID of the community for which to retrieve brands.</param>
		/// <returns>Brands object</returns>
		public Brands GetBrandsByCommunity(int communityID)
		{
			Brands allBrands = GetBrands();
			Brands brands = new Brands();

			foreach(Brand brand in allBrands)
			{
				if(brand.Site.Community.CommunityID == communityID)
				{
					brands.AddBrand(brand);
				}
			}

			return(brands);
		}

		/// <summary>
		/// Retrieves all the brands for all the sites in a particular language
		/// </summary>
		/// <param name="languageID">The LanguageID of the language for which to retrieve brands.</param>
		/// <returns>Brands object</returns>
		public Brands GetBrandsByLanguageID(int languageID)
		{
			Brands allBrands = GetBrands();
			Brands brands = new Brands();

			foreach (Brand brand in allBrands)
			{
				if (brand.Site.LanguageID == languageID)
				{
					brands.AddBrand(brand);
				}
			}

			return brands;
		}
		

		/// <summary>
		/// Retrieves all communities
		/// </summary>
		/// <returns>An ArrayList of communities</returns>
		public Hashtable GetCommunities()
		{
			Hashtable retVal = new Hashtable();

			foreach (Brand brand in GetBrands())
			{
				if (!retVal.Contains(brand.Site.Community.CommunityID))
				{
					retVal.Add(brand.Site.Community.CommunityID, brand.Site.Community);
				}
			}

			return retVal;
		}

		#endregion

		#region TOFIX - Perhaps add result caching to these methods - dcornell

		/// <summary>
		/// 
		/// </summary>
		/// <param name="uri"></param>
		/// <returns></returns>
		public Brand GetBrandByURI(string uri)
		{
			Brand retVal = null;

			Brands myBrands = GetBrands();
			foreach(Brand myBrand in myBrands)
			{
				if(string.Compare(myBrand.Uri, uri, true) == 0)
				{
					retVal = myBrand;
					break;
				}
			}

			return(retVal);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public Brand GetBrandByID(int brandID)
		{
			Brand retVal = GetBrands().GetBrand(brandID);

			return(retVal);
		}

		#endregion

		private IBrandConfigService getService(string uri)
		{
			try
			{
				return (IBrandConfigService)Activator.GetObject(typeof(IBrandConfigService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}
	}
}
