using System;

using Matchnet.Exceptions;
using Matchnet.Content.ValueObjects;


namespace Matchnet.Content.ServiceAdapters
{
	/// <summary>
	/// 
	/// </summary>
	public class UriUtil
	{
		internal static readonly UriUtil Instance = new UriUtil();

		private UriUtil()
		{
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="serviceManagerName"></param>
		/// <returns></returns>
		public string GetServiceManagerUri(string serviceManagerName)
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(serviceManagerName);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}
	}
}
