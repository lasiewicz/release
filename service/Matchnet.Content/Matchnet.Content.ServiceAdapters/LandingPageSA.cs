using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using Matchnet.Caching;
using Matchnet.RemotingClient;
using Matchnet.Content.ValueObjects.LandingPage;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Exceptions;
using Matchnet.CacheSynchronization.Context;

namespace Matchnet.Content.ServiceAdapters
{
	 /// <summary>
	/// Summary description for LandingPageSA.
	/// </summary>
	public class LandingPageSA : SABase
	{
		private Cache _cache = Cache.Instance;
		private Random _random = null;
		private const string SERVICE_MANAGER_NAME = "LandingPageSM";

		/// <summary>
		/// Singleton instance.
		/// </summary>
		public static LandingPageSA Instance = new LandingPageSA();
		
		/// <summary>
		/// 
		/// </summary>
		private LandingPageSA()
		{
			_random = new Random();
		}

		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_SA_CONNECTION_LIMIT"));
		}

        public List<UserDefinedField> GetFieldsForLandingPage(TemplateBasedLandingPage page)
        {
            if (page.Template.UserDefinedFields == null) return null;
            
            List<UserDefinedField> userDefiendFields = new List<UserDefinedField>();
            
            foreach (string field in page.Template.UserDefinedFields)
            {
                string value = string.Empty;
                if (page.UserDefinedFields != null)
                {
                    UserDefinedField f = (from UserDefinedField udf in page.UserDefinedFields where udf.Name == field select udf).FirstOrDefault();
                    if (f != null)
                    {
                        value = f.Value;
                    }
                }
                userDefiendFields.Add(new UserDefinedField(field, value));
            }
            return userDefiendFields;
        }


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
        public List<TemplateBasedLandingPage> GetLandingPages(Boolean forceLoadFlag)
		{
            List<TemplateBasedLandingPage> landingPages = null;
			string uri = "";

			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
				//Int32 cacheTTL = GenerateRandomTTL(Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PAGE_PIXEL_SA")));

				base.Checkout(uri);
				try
				{
                    landingPages = getService(uri).GetLandingPages(forceLoadFlag);
				}
				finally
				{
					base.Checkin(uri);
				}

                return landingPages;
			}
			catch (Exception ex)
			{
                throw (new SAException("Error in GetLandingPages (uri: " + uri + ")", ex));
			}
		}

        public List<LandingPageTemplate> GetLandingPageTemplates()
        {
            List<LandingPageTemplate> templates = null;
            string uri = "";

            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                //Int32 cacheTTL = GenerateRandomTTL(Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PAGE_PIXEL_SA")));

                base.Checkout(uri);
                try
                {
                    templates = getService(uri).GetLandingPageTemplates();
                }
                finally
                {
                    base.Checkin(uri);
                }

                return templates;
            }
            catch (Exception ex)
            {
                throw (new SAException("Error in GetLandingPageTemplates (uri: " + uri + ")", ex));
            }
        }

        public LandingPage GetLandingPage(int landingPageID)
        {
            LandingPage page = null;
            string uri = "";

            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                //Int32 cacheTTL = GenerateRandomTTL(Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PAGE_PIXEL_SA")));

                base.Checkout(uri);
                try
                {
                    page = getService(uri).GetLandingPage(landingPageID);
                }
                finally
                {
                    base.Checkin(uri);
                }

                return page;
            }
            catch (Exception ex)
            {
                throw (new SAException("Error in GetLandingPage (old) (uri: " + uri + ")", ex));
            }
        }

        public void SaveLandingPage(TemplateBasedLandingPage page)
        {
            string uri = "";

            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                //Int32 cacheTTL = GenerateRandomTTL(Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PAGE_PIXEL_SA")));

                base.Checkout(uri);
                try
                {
                    getService(uri).SaveLandingPage(page);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error in SaveLandingPage (uri: " + uri + ")", ex));
            }
        }

        
        public void SaveUnifiedLandingPageIDForLandingPage(int landingPageID, int? unifiedLandingPageID)
        {
            string uri = "";

            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
              
                base.Checkout(uri);
                try
                {
                    getService(uri).SaveUnifiedLandingPageIDForLandingPage(landingPageID, unifiedLandingPageID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error in SaveLandingPage (uri: " + uri + ")", ex));
            }
        }


        public void SaveLandingPageTemplate(LandingPageTemplate template)
        {
            string uri = "";

            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                //Int32 cacheTTL = GenerateRandomTTL(Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PAGE_PIXEL_SA")));

                base.Checkout(uri);
                try
                {
                    getService(uri).SaveLandingPageTemplate(template);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error in SaveLandingPageTemplate (uri: " + uri + ")", ex));
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pLandingPageID"></param>
		/// <param name="forceLoadFlag"></param>
		/// <returns></returns>
        public TemplateBasedLandingPage GetTemplateBasedLandingPage(int landingPageID)
		{
            return (from TemplateBasedLandingPage lp in GetLandingPages(false) where lp.LandingPageID == landingPageID select lp).FirstOrDefault();
		}

        public TemplateBasedLandingPage GetTemplateBasedLandingPageByURL(string url)
        {
            string uri = "";
            TemplateBasedLandingPage landingPage = null;

            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                
                base.Checkout(uri);
                try
                {
                    landingPage = getService(uri).GetTemplateBasedLandingPageByURL(url);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error in GetTemplateBasedLandingPageByURL (uri: " + uri + ")", ex));
            }

            return landingPage;
        }

        public bool CheckNameAvailability(string name, int existingID, CheckAvailabilityType type)
        {
            string uri = "";
            bool available = false;

            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);

                base.Checkout(uri);
                try
                {
                    available = getService(uri).CheckNameAvailability(name, existingID, type);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error in CheckNameAvailibility (uri: " + uri + ")", ex));
            }

            return available;
        }

        /// <summary>
		/// 
		/// </summary>
		/// <param name="pLandingPageID"></param>
		public void DeleteLandingPage(int landingPageID)
		{
			string uri = "";
			try
			{
				uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);

				base.Checkout(uri);
				try
				{
                    getService(uri).DeleteLandingPage(landingPageID);
				}
				finally
				{
					base.Checkin(uri);
				}
			}
			catch (Exception ex)
			{
                throw (new SAException("Error in DeleteLandingPage (uri: " + uri + ")", ex));
			}
		}

        public void DeleteTemplate(int templateID)
        {
            string uri = "";
            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);

                base.Checkout(uri);
                try
                {
                    getService(uri).DeleteTemplate(templateID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Error in DeleteTemplate (uri: " + uri + ")", ex));
            }
        }
		

		private ILandingPageService getService(string uri)
		{
			try
			{
				return (ILandingPageService)Activator.GetObject(typeof(ILandingPageService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		/// <summary>
		/// There is a fear that perhaps having all Content objects have the same TTL was causing
		/// a mass loading of Content objects by the web tier.  To alleviate this, we will be taking the TTL and 
		/// generating a random TTL that is between ttl and 2 x ttl.
		/// </summary>
		/// <param name="ttl"></param>
		/// <returns></returns>
		private int GenerateRandomTTL(int ttl)
		{
			return ttl + _random.Next(ttl / 2);
		}
	}
}
