﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Caching;
using Matchnet.Content.ValueObjects.Registration;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Spark.Common.Utilities;

namespace Matchnet.Content.ServiceAdapters
{
    public class RegistrationMetadataSA: SABase
    {
        private Cache _cache = null;
        private const string SERVICE_MANAGER_NAME = "RegistrationMetadataSM";
        private IRegistrationMetadata _registrationMetadataService;
        
        #region singleton
		/// <summary>
		/// 
		/// </summary>
		public static readonly RegistrationMetadataSA Instance = new RegistrationMetadataSA();
		/// <summary>
		/// Parameterless constructor
		/// </summary>
        private RegistrationMetadataSA()
		{
			_cache = Cache.Instance;
		}
		#endregion

        public void SetService(IRegistrationMetadata registrationMetadataService)
        {
            _registrationMetadataService = registrationMetadataService;
        }

        public void ClearMetadataCache()
        {
            _cache.Remove(CacheableScenarios.CACHE_KEY);
            _cache.Remove(CacheableScenariosForAdmin.CACHE_KEY);
        }

        public string GetScenarioCacheKey()
        {
            return CacheableScenarios.CACHE_KEY;
        }

        public string GetAdminScenariosCacheKey()
        {
            return CacheableScenariosForAdmin.CACHE_KEY;
        }

        public List<Scenario> GetScenariosForAdmin()
        {
            List<Scenario> allScenarios = null;
            string uri = string.Empty;

            try
            {
                var cachedScenarios = _cache.Get(CacheableScenariosForAdmin.CACHE_KEY) as CacheableScenariosForAdmin;

                if (cachedScenarios != null && cachedScenarios.Scenarios != null && cachedScenarios.Scenarios.Count > 0)
                {
                    allScenarios = cachedScenarios.Scenarios;
                }

                if (allScenarios == null)
                {
                    uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                    base.Checkout(uri);
                    try
                    {
                        allScenarios = getService(uri).GetScenariosForAdmin();
                        _cache.Insert(new CacheableScenariosForAdmin(allScenarios));
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }

            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve registration scenario metadata (uri: " + uri + ")", ex));
            }

            return allScenarios;
        }

        public List<Scenario> GetScenariosBySite(int siteID)
        {
            List<Scenario> scenarios;
            List<Scenario> allScenarios = null;
            string uri = string.Empty;

            try
            {
                var cachedScenarios = _cache.Get(CacheableScenarios.CACHE_KEY) as CacheableScenarios;
                
                if(cachedScenarios != null && cachedScenarios.Scenarios != null && cachedScenarios.Scenarios.Count > 0)
                {
                    allScenarios = cachedScenarios.Scenarios;
                }
                 
                if (allScenarios == null)
                {
                    uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                    base.Checkout(uri);
                    try
                    {
                        allScenarios = getService(uri).GetScenarios();
                        _cache.Insert(new CacheableScenarios(allScenarios));
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }

                scenarios = (from Scenario s in allScenarios where s.SiteID == siteID select s).ToList();
            }
            catch(Exception ex)
            {
                throw (new SAException("Cannot retrieve registration scenario metadata (uri: " + uri + ")", ex));
            }

            return scenarios;
        }

        public Scenario GetRandomScenarioBySite(int siteID)
        {
            Scenario randomScenario = null;
            
            List<Scenario> scenarios = GetScenariosBySite(siteID);

            if(scenarios == null || scenarios.Count <1)
            {
                //no scenarios for site, throw exception 
                throw (new SAException("SA error occured: No scenarios found for site " + siteID.ToString() + "!"));
            }

            //first, make sure weights add up to 1
            double totalWeight = (from s in scenarios select s.Weight).Sum();
            if(totalWeight != 1)
            {
                //weighting isn't set up correctly, so just pick random scenario
                randomScenario = scenarios[0];
            }
            else
            {
                var proportionValues = (from Scenario s in scenarios where s.Weight > 0 select ProportionValue.Create(s.Weight, s)).ToList();
                
                randomScenario = proportionValues.ChooseByRandom();
            }

            return randomScenario;
        }

        public Scenario GetRandomScenarioBySiteAndDeviceType(int siteID, DeviceType deviceType)
        {
            Scenario randomScenario = null;

            List<Scenario> scenarios = GetScenariosBySite(siteID);

            if (scenarios == null || scenarios.Count < 1)
            {
                //no scenarios for site, throw exception 
                throw (new SAException("SA error occured: No scenarios found for site " + siteID.ToString() + "!"));
            }

            //first, make sure weights add up to 1
            var deviceFilteredScenarios = (from s in scenarios where s.DeviceType == deviceType select s).ToList();
            double totalWeight = deviceFilteredScenarios.Sum(s => s.Weight);
            if (totalWeight != 1)
            {
                //weighting isn't set up correctly, so just pick random scenario
                int randomScenarioIndex = new Random().Next(0, (deviceFilteredScenarios.Count - 1));
                randomScenario = deviceFilteredScenarios[randomScenarioIndex];
            }
            else
            {
                var proportionValues = (from Scenario s in scenarios where s.Weight > 0 && s.DeviceType == deviceType select ProportionValue.Create(s.Weight, s)).ToList();

                randomScenario = proportionValues.ChooseByRandom();
            }

            return randomScenario;
        }

        public List<Scenario>  GetAllRegScenarios()
        {
            List<Scenario> allScenarios = null;
            string uri = string.Empty;

            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    allScenarios = getService(uri).GetAllRegScenarios();
                    _cache.Insert(new CacheableScenariosForAdmin(allScenarios));
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve registration scenario metadata (uri: " + uri + ")", ex));
            }

            return allScenarios;
        }

        /// <summary>
        /// Activate or deactivate the reg scenario and update its weight
        /// </summary>
        /// <param name="regScenarioId"></param>
        public void SaveScenarioStatusWeight(int regScenarioId, bool activate, double weight, string updatedBy, int siteID, int deviceTypeID)
        {
            string uri = string.Empty;
            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).SaveScenarioStatusWeight(regScenarioId, activate, weight, updatedBy, siteID, deviceTypeID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot toggle registration scenario (uri: " + uri + ")", ex));
            }
        }

        public void SaveRegStep(Step step, int scenarioId)
        {
            string uri = string.Empty;
            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).SaveRegStep(step, scenarioId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot save reg step (uri: " + uri + ")", ex));
            }
        }

        public void SaveRegControlScenarioOverride(RegControlScenarioOverride regControlegControlScenarioOverride, int regControlSiteID)
        {
            string uri = string.Empty;
            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).SaveRegControlScenarioOverride(regControlegControlScenarioOverride, regControlSiteID);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot save reg control scenario override (uri: " + uri + ")", ex));
            }
        }


        public Dictionary<int, List<Template>> GetTemplatesBySite()
        {
            Dictionary<int, List<Template>> templates = null;
            string uri = string.Empty;
            try
            {
                var cachedTemplates = _cache.Get(CachedTemplatesBySite.CacheKey) as CachedTemplatesBySite;
                
                if (cachedTemplates == null || cachedTemplates.Templates == null || cachedTemplates.Templates.Count == 0)
                {
                    uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                    base.Checkout(uri);
                    try
                    {
                        templates = getService(uri).GetTemplatesBySite();
                        cachedTemplates = new CachedTemplatesBySite {Templates = templates};
                        _cache.Add(cachedTemplates);

                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
                }
                else
                {
                    templates = cachedTemplates.Templates;
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get all reg controls (uri: " + uri + ")", ex));
            }
            return templates;
        }


        public TemplateSaveResult CreateTemplate(Template regTemplate)
        {
            TemplateSaveResult templateSaveResult = null;
            string uri = string.Empty;
            try
            {
                    uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                    base.Checkout(uri);
                    try
                    {
                        templateSaveResult = getService(uri).CreateTemplate(regTemplate);
                        if (templateSaveResult.ResultType == TemplateResultType.Success)
                        {
                            regTemplate.ID = templateSaveResult.RegTemplate.ID;
                            var cachedTemplates = _cache.Get(CachedTemplatesBySite.CacheKey) as CachedTemplatesBySite;
                            if (null != cachedTemplates)
                            {
                                if (cachedTemplates.Templates.ContainsKey(regTemplate.SiteID))
                                {
                                    cachedTemplates.Templates[regTemplate.SiteID].Add(regTemplate);
                                }
                            }
                        }
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot create reg template (uri: " + uri + ")", ex));
            }
            return templateSaveResult;
        }

        public TemplateSaveResult RemoveTemplate(int regTemplateId, int siteId)
        {
            TemplateSaveResult templateSaveResult = null;
            string uri = string.Empty;
            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    templateSaveResult = getService(uri).RemoveTemplate(regTemplateId, siteId);
                    if (templateSaveResult.ResultType==TemplateResultType.Success)
                    {
                        var cachedTemplates = _cache.Get(CachedTemplatesBySite.CacheKey) as CachedTemplatesBySite;
                        if (null != cachedTemplates)
                        {
                            if (cachedTemplates.Templates.ContainsKey(siteId))
                            {
                                List<Template> templates = cachedTemplates.Templates[siteId];
                                Template delete=templates.Find(delegate(Template template){ return (template.ID == regTemplateId); });
                                templates.Remove(delete);
                            }
                        }
                    }
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot create reg template (uri: " + uri + ")", ex));
            }
            return templateSaveResult;
        }

        /// <summary>
        /// Get all reg controls
        /// </summary>
        public List<RegControl> GetAllRegControls(int deviceTypeId)
        {
            List<RegControl> allControls = null;
            string uri = string.Empty;
            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    allControls = getService(uri).GetAllRegControls(deviceTypeId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get all reg controls (uri: " + uri + ")", ex));
            }
            return allControls;
        }

        /// <summary>
        /// Get all reg controls
        /// </summary>
        public List<RegControl> GetRegControlsRequiredForRegScenarioSite(int siteId)
        {
            List<RegControl> requiredControls = null;
            string uri = string.Empty;
            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    requiredControls = getService(uri).GetRegControlsRequiredForRegScenarioSite(siteId);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot get required reg controls (uri: " + uri + ")", ex));
            }
            return requiredControls;
        }

        /// <summary>
        /// Create new registration scenario
        /// </summary>
        /// <param name="scenario"></param>
        public int CreateRegScenario(Scenario scenario)
        {
            int regScenarioId = 0;
            string uri = string.Empty;
            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    regScenarioId = getService(uri).CreateRegScenario(scenario);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot create new registration scenario (uri: " + uri + ")", ex));
            }

            return regScenarioId;
        }

        public void SaveScenario(Scenario scenario)
        {
            string uri = string.Empty;
            try
            {
                uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                base.Checkout(uri);
                try
                {
                    getService(uri).SaveScenario(scenario);
                }
                finally
                {
                    base.Checkin(uri);
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot save registration scenario (uri: " + uri + ")", ex));
            }
        }

        #region Service Adapter Methods

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_SA_CONNECTION_LIMIT"));
        }

        private IRegistrationMetadata getService(string uri)
        {
            try
            {
                if (_registrationMetadataService != null)
                {
                    return _registrationMetadataService;
                }
                
                return (IRegistrationMetadata)Activator.GetObject(typeof(IRegistrationMetadata), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }


        #endregion


        private class CacheableScenarios : ICacheable
        {
            public static string CACHE_KEY = "^AllScenarioCacheKey";
            public List<Scenario> Scenarios { get; private set; }

            public CacheableScenarios(List<Scenario> scenarios)
            {
                Scenarios = scenarios;
            }

            #region ICacheable Members

            public CacheItemMode CacheMode
            {
                get { return CacheItemMode.Absolute; }
            }

            public CacheItemPriorityLevel CachePriority
            {
                get
                {
                    return CacheItemPriorityLevel.High;
                }
                set { }
            }

            public int CacheTTLSeconds
            {
                get
                {
                    return 21600; //6 hours
                }
                set
                {

                }
            }

            public string GetCacheKey()
            {
                return CACHE_KEY;
            }

            #endregion
        }

        private class CacheableScenariosForAdmin : ICacheable
        {
            public static string CACHE_KEY = "^AllScenarioForAdminCacheKey";
            public List<Scenario> Scenarios { get; private set; }

            public CacheableScenariosForAdmin(List<Scenario> scenarios)
            {
                Scenarios = scenarios;
            }

            #region ICacheable Members

            public CacheItemMode CacheMode
            {
                get { return CacheItemMode.Absolute; }
            }

            public CacheItemPriorityLevel CachePriority
            {
                get
                {
                    return CacheItemPriorityLevel.High;
                }
                set { }
            }

            public int CacheTTLSeconds
            {
                get
                {
                    return 21600; //6 hours
                }
                set
                {

                }
            }

            public string GetCacheKey()
            {
                return CACHE_KEY;
            }

            #endregion
        }
    }
}
