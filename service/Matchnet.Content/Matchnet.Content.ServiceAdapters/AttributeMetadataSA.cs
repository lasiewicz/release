using System;

using Matchnet.Caching;
using Matchnet.Content.ServiceAdapters.ServiceDefinitions;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ValueObjects.AttributeMetadata;


namespace Matchnet.Content.ServiceAdapters
{
	/// <summary>
	/// 
	/// </summary>
    public class AttributeMetadataSA : SABase, IAttributeMetadataSA
	{
		private const string SERVICE_MANAGER_NAME = "AttributeMetadataSM";

		/// <summary>
		/// 
		/// </summary>
		public static readonly AttributeMetadataSA Instance = new AttributeMetadataSA();

		private Attributes _lastLoadedAttributes = null;
		private AttributeCollections _lastLoadedAttributeCollections = null;

		private AttributeMetadataSA()
		{
		}


		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_SA_CONNECTION_LIMIT"));
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public Attributes GetAttributes()
		{
			string uri = "";

			try
			{
				Attributes attributes = Cache.Instance.Get(Attributes.CACHE_KEY) as Attributes;

				if (attributes == null)
				{
					uri = getServiceManagerUri();
					base.Checkout(uri);
					try
					{
						attributes = getService(uri).GetAttributes();
						_lastLoadedAttributes = attributes;
					}
					catch (Exception ex)
					{
						//use last loaded attributes if we cannot get them from svc

						if (_lastLoadedAttributes == null)
						{
							throw ex;
						}

						new SAException("Unable to load attributes, reverting to last successful load.", ex);
						_lastLoadedAttributes.CacheTTLSeconds = 60;
						Cache.Instance.Insert(_lastLoadedAttributes);
						return _lastLoadedAttributes;
					}
					finally
					{
						base.Checkin(uri);
					}
					Cache.Instance.Insert(attributes);
				}

				return attributes;
			}
			catch (Exception ex)
			{
				throw(new SAException("Error retreiving Attributes. (uri: " + uri + ")", ex));
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public AttributeCollections GetAttributeCollections()
		{
			string uri = "";

			try
			{
				AttributeCollections attributeCollections = Cache.Instance.Get(AttributeCollections.CACHE_KEY) as AttributeCollections;

				if (attributeCollections == null)
				{
					uri = getServiceManagerUri();
					base.Checkout(uri);
					try
					{
						attributeCollections = getService(uri).GetAttributeCollections();
						_lastLoadedAttributeCollections = attributeCollections;
					}
					catch (Exception ex)
					{
						//use last loaded attributeCollections if we cannot get them from svc

						if (_lastLoadedAttributeCollections == null)
						{
							throw ex;
						}

						new SAException("Unable to load attributeCollections, reverting to last successful load.", ex);
						_lastLoadedAttributeCollections.CacheTTLSeconds = 60;
						Cache.Instance.Insert(_lastLoadedAttributeCollections);
						return _lastLoadedAttributeCollections;
					}
					finally
					{
						base.Checkin(uri);
					}
					Cache.Instance.Insert(attributeCollections);
				}

				return attributeCollections;
			}
			catch (Exception ex)
			{
				throw(new SAException("Error retreiving attribute collections. (uri: " + uri + ")", ex));
			}
		}

        public SearchStoreAttributes GetSearchStoreAttributes()
        {
            string uri = "";
            try
            {
                bool enablefiltering = Conversion.CBool( Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENABLE_SEARCHSTOREATTRIBUTES_FILTER"));
                if (!enablefiltering)
                    return null;
                    
                SearchStoreAttributes attributes = (SearchStoreAttributes)Cache.Instance.Get(SearchStoreAttributes.CACHE_KEY);

                if (attributes == null)
                {
                   
                    int cacheexprtime = SearchStoreAttributes.CACHE_TTL;
                    try
                    {

                        string cachettl = Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SEARCHSTOREATTRIBUTES_CACHE_TTL");
                        cacheexprtime = Conversion.CInt(cachettl);

                       
                    }
                    catch (Exception ex1)
                    {
                        SAException nosettingex = new SAException("GetSearchStoreAttributes - SEARCHSTOREATTRIBUTES_CACHE_TTL not found setting. ", ex1);
                    }
                    

                     

                     uri = getServiceManagerUri();
                     base.Checkout(uri);
                     attributes = getService(uri).GetSearchStoreAttributes();
                     attributes.CacheTTLSeconds = cacheexprtime;
                     Cache.Instance.Insert(attributes);
                    
                   
                    
                }
                return attributes;
            }
            catch (Exception ex)
            {
                throw new SAException("Error occurred while retrieving  search store attributes.", ex);
            }finally
            {
                        base.Checkin(uri);
             }
                    

        }
		private IAttributeMetadataService getService(string uri)
		{
			try
			{
				return (IAttributeMetadataService)Activator.GetObject(typeof(IAttributeMetadataService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		
		private string getServiceManagerUri()
		{
			try
			{
				string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, PartitionMode.Random).ToUri(SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}
	}
}
