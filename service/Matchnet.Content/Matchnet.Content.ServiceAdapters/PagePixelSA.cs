using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Lib;
using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.PagePixel;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.CacheSynchronization.Context;

namespace Matchnet.Content.ServiceAdapters
{
	/// <summary>
	/// Summary description for PagePixelSA.
	/// </summary>
	public class PagePixelSA : SABase
	{
		#region constants
		private const string SERVICE_MANAGER_NAME = "PagePixelSM";
		#endregion

		#region class variables
		private Cache _cache = null;
		private Random _random = null;
		#endregion

		#region singleton
		/// <summary>
		/// 
		/// </summary>
		public static readonly PagePixelSA Instance = new PagePixelSA();

		/// <summary>
		/// Private constructor removes the public no-arg constructor so you have to go through Instance
		/// </summary>
		private PagePixelSA()
		{
			_cache = Cache.Instance;
			_random = new Random();
		}
		#endregion


		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_SA_CONNECTION_LIMIT"));
		}



		public PagePixelCollection RetrievePagePixels(int pageID, int siteID)
		{
			PagePixelCollection oResult = null;
			string uri = "";

			try 
			{
				string cacheKey = PagePixelCollection.GetCacheKey(pageID, siteID);

				oResult = _cache.Get(cacheKey) as PagePixelCollection;

				if (oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					Int32 cacheTTL = GenerateRandomTTL(Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PAGE_PIXEL_SA")));
					
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).RetrievePagePixels(System.Environment.MachineName,
							Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(cacheTTL)),
							pageID,
							siteID);
					}
					finally
					{
						base.Checkin(uri);
					}
					
					if (oResult != null) 
					{
						oResult.CacheTTLSeconds = cacheTTL;
						oResult.SetCacheKey = cacheKey;

						_cache.Insert(oResult);
					}
				}

				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve page pixels (uri: " + uri + ")", ex));
			}
		}

        public List<string> RetrieveFilteredPagePixels(PixelRequest request)
        {
            List<string> pixelOutput = null;
            PagePixelCollection pixels = null;
            RegionLanguage region = null;

            try
            {
                pixels = RetrievePagePixels(request.PageID, request.SiteID);
            }
            catch(Exception ex)
            {
                throw (new SAException("Cannot retrieve filtered page pixels", ex));
            }

            if(pixels != null)
            {
                foreach (PagePixel pixel in pixels)
                {
                    try
                    {
                        if (pixel != null)
                        {
                            if(pixelOutput == null) pixelOutput = new List<string>();
                            
                            if(region == null)
                            {
                                try
                                {
                                    region = RegionSA.Instance.RetrievePopulatedHierarchy(request.MemberData.RegionID, request.LanguageID);
                                }
                                catch (Exception ex)
                                {
                                    throw (new SAException("Cannot retrieve filtered page pixels - error getting region for region "  + request.MemberData.RegionID.ToString() , ex));
                                }
                            }

                            PagePixel p = new PagePixel();
                            p.Code = pixel.Code;
                            p.PageID = pixel.PageID;
                            p.SiteID = pixel.SiteID;
                            p.TargetingConditionCollection = pixel.TargetingConditionCollection;
                            
                            var renderedPixel = RenderPixel(p, request, region);
                            if(!string.IsNullOrEmpty(renderedPixel))
                            {
                                pixelOutput.Add(renderedPixel);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw (new SAException("Cannot retrieve filtered page pixels", ex));
                    }
                }
            }

            return pixelOutput;
        }

        private string RenderPixel(PagePixel pixel, PixelRequest request, RegionLanguage region)
        {
            Nullable<Boolean> showPixel = true;

            Nullable<Boolean> showcountryid = null, showlgid = false, showgenderid = false,
                showpromoid = null, showage = null, showpageid = null;

            Dictionary<String, Nullable<Boolean>> checkList = new Dictionary<string, Nullable<Boolean>>();

            TargetingConditionCollection conditions = pixel.TargetingConditionCollection;
            bool temp = false;

            foreach (TargetingCondition condition in conditions)
            {
                switch (condition.Name)
                {
                    case "countryid":
                        temp = FilterCompare(condition.ConditionValue, region.CountryRegionID.ToString(), condition.Comparison);

                        UpdateDictionaryItem(checkList, "countryid", temp);
                        break;
                    case "lgid":
                        string lgid = Constants.NULL_STRING;

                        if(!string.IsNullOrEmpty(request.LuggageID))
                        {
                            lgid = request.LuggageID;
                        }
                        
                        temp = FilterCompare(condition.ConditionValue, lgid, condition.Comparison);

                        UpdateDictionaryItem(checkList, "lgid", temp);

                        break;
                    case "genderid":
                        temp = FilterCompare(condition.ConditionValue, request.MemberData.GenderMask.ToString(), condition.Comparison);
                        UpdateDictionaryItem(checkList, "genderid", temp);
                        break;
                    case "promoid":
                        string promoID = Constants.NULL_STRING;

                        if (request.PromotionID.HasValue)
                        {
                            promoID = request.PromotionID.Value.ToString();
                        }

                        temp = FilterCompare(condition.ConditionValue, promoID, condition.Comparison);

                        UpdateDictionaryItem(checkList, "promoid", temp);
                        break;
                    case "age":
                        if (request.MemberData.Birthdate != DateTime.MinValue)
                        {
                            temp = FilterCompare(condition.ConditionValue, GetAge(request.MemberData.Birthdate).ToString(), condition.Comparison);

                            UpdateDictionaryItem(checkList, "age", temp);
                        }
                        break;
                    case "pageid":
                        temp = FilterCompare(condition.ConditionValue, request.PageID.ToString(), condition.Comparison);

                        UpdateDictionaryItem(checkList, "pageid", temp);

                        break;
                    default:
                        throw (new SAException("Cannot retrieve filtered page pixels, invalid condition: " +  condition.Name));
                        break;
                }
            }

            if (conditions.Count > 0)
            {
                foreach (KeyValuePair<string, Nullable<Boolean>> hmm in checkList)
                {
                    if (hmm.Value == null)
                        continue;

                    showPixel &= hmm.Value;
                }
            }
            else
            {
                showPixel = true;
            }

            // None of the filters met, do not render anything for this pixel.
            if (showPixel == false)
            {
                return String.Empty;
            }


            MatchCollection matchCollection = Regex.Matches(pixel.Code, @"{(.*?)}");

            string[] fields = new string[matchCollection.Count];

            for (int i = 0; i < fields.Length; i++)
            {
                string argumentName = matchCollection[i].Groups[1].Value.ToString();

                switch (argumentName.ToLower())
                {
                    case "birthdate":
                        if(request.MemberData.Birthdate != DateTime.MinValue)
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{birthdate}", request.MemberData.Birthdate.ToString("yyyyMMdd"));
                        }
                        else
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{birthdate}", string.Empty);
                        }
                        break;
                    case "gender":
                            pixel.Code = ReplaceString(pixel.Code, "{gender}", request.MemberData.GenderMask.ToString());
                        break;
                    case "memberid":
                        pixel.Code = ReplaceString(pixel.Code, "{memberid}", request.MemberData.MemberID.ToString());
                        break;
                    case "country":
                        try
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{country}", region.CountryRegionID.ToString());
                        }
                        catch
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{country}", string.Empty);
                        }
                        break;
                    case "referring_url":
                        pixel.Code = ReplaceString(pixel.Code, "{referring_url}", request.ReferralURL);
                        break;
                    case "state":
                        try
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{state}", region.StateRegionID.ToString());
                        }
                        catch
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{state}", string.Empty);
                        }
                        break;
                    case "dma":
                        try
                        {
                            DMA dma = RegionSA.Instance.GetDMAByZipRegionID(request.MemberData.RegionID);
                            if(dma != null)
                            {
                                pixel.Code = ReplaceString(pixel.Code, "{dma}", dma.DMAID.ToString());
                            }
                            else
                            {
                                pixel.Code = ReplaceString(pixel.Code, "{dma}", string.Empty);
                            }
                        }
                        catch
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{dma}", string.Empty);
                        }
                        break;
                    case "age":
                        if(request.MemberData.Birthdate != DateTime.MinValue)
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{age}",
                                                       GetAge(request.MemberData.Birthdate).ToString());
                        }
                        else
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{age}", string.Empty);
                        }
                        break;
                    case "age_group":
                        if (request.MemberData.Birthdate != DateTime.MinValue)
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{age_group}",
                                                      GetAgeGroup(GetAge(request.MemberData.Birthdate)).
                                                          ToString());
                        }
                        else
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{age_group}", string.Empty);
                        }
                        break;
                    case "prm":
                        //09252008 TL MPR-457, Updating to ensure the promotion ID rendered for page pixel is based on member's registered promotionID
                        if (request.PromotionID.HasValue)
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{prm}", request.PromotionID.Value.ToString());
                        }
                        else
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{prm}", string.Empty);
                        }
                        break;
                    case "lgid":
                        if (!string.IsNullOrEmpty(request.LuggageID))
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{lgid}", request.LuggageID);
                        }
                        else
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{lgid}", string.Empty);
                        }
                        break;
                    case "subscription_amount":
                        pixel.Code = ReplaceString(pixel.Code, "{subscription_amount}", request.MemberData.SubscriptionAmount);
                        break;
                    case "subscription_duration":
                        pixel.Code = ReplaceString(pixel.Code, "{subscription_duration}", request.MemberData.SubscriptionDuration);
                        break;
                    case "timestamp":
                        pixel.Code = ReplaceString(pixel.Code, "{timestamp}", DateTime.Now.ToString("yyyyMMddHHmmss"));
                        break;
                    default:
                        break;
                }
            }

            return pixel.Code + System.Environment.NewLine;
        }

        private int GetAge(DateTime birthDate)
        {
            if (birthDate == DateTime.MinValue) return 0;
            return (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
        }

        private int GetAgeGroup(int Age)
        {
            if (Age >= 18 && Age <= 25)
                return ConstantsTemp.AGE_GROUP_18_TO_25;
            else if (Age >= 26 && Age <= 34)
                return ConstantsTemp.AGE_GROUP_26_TO_34;
            else if (Age >= 35 && Age <= 45)
                return ConstantsTemp.AGE_GROUP_35_TO_45;
            else if (Age >= 46 && Age <= 60)
                return ConstantsTemp.AGE_GROUP_46_TO_60;
            else if (Age >= 60)
                return ConstantsTemp.AGE_GROUP_60_TO_99;
            else
                return 0;
        }

        private void UpdateDictionaryItem(Dictionary<String, Nullable<Boolean>> checkList, string key, bool temp)
        {
            if (!checkList.ContainsKey(key))
            {
                checkList.Add(key, temp);
            }
            else
            {
                temp |= checkList[key].Value;
                checkList.Remove(key);
                checkList.Add(key, temp);
            }
        }

        private bool FilterCompare(string value1, string value2, string op)
        {
            try
            {
                int value1int = Convert.ToInt32(value1);
                int value2int = Convert.ToInt32(value2);

                switch (op)
                {
                    case "1":
                        return (value1int == value2int);
                    case "2":
                        return (value1int != value2int);
                    case "3":
                        return (value1int < value2int);
                    case "4":
                        return (value1int <= value2int);
                    case "5":
                        return (value1int > value2int);
                    case "6":
                        return (value1int >= value2int);
                    case "7":
                        return (value2.IndexOf(value1) != -1);
                    default:
                        throw (new SAException("Cannot retrieve filtered page pixels, unknown filter operation. " + op.ToString()));
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve filtered page pixels, error running comparison", ex));
                return false;
            }
        }

        /// <summary>
        /// Case insensitive string replacer.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="find"></param>
        /// <param name="replace"></param>
        /// <returns></returns>
        private string ReplaceString(string source, string find, string replace)
        {
            return System.Text.RegularExpressions.Regex.Replace(source, find, replace, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        }

		private IPagePixelService getService(string uri)
		{
			try
			{
				return (IPagePixelService)Activator.GetObject(typeof(IPagePixelService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		/// <summary>
		/// There is a fear that perhaps having all Content objects have the same TTL was causing
		/// a mass loading of Content objects by the web tier.  To alleviate this, we will be taking the TTL and 
		/// generating a random TTL that is between ttl and 2 x ttl.
		/// </summary>
		/// <param name="ttl"></param>
		/// <returns></returns>
		private int GenerateRandomTTL(int ttl)
		{
			return ttl + _random.Next(ttl / 2);
		}
	}
}
