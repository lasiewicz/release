﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matchnet.RemotingClient;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Caching;
using Matchnet.Exceptions;

namespace Matchnet.Content.ServiceAdapters
{
    public class PushNotificationMetadataSA : SABase, IPushNotificationMetadataSAService
    {
        public const string SERVICE_MANAGER_NAME = "PushNotificationMetadataSM";
        public static readonly PushNotificationMetadataSA Instance = new PushNotificationMetadataSA();
        private Cache _cache = null;

        private PushNotificationMetadataSA()
        {
            _cache = Cache.Instance;
        }

        protected override void GetConnectionLimit()
        {
            base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_SA_CONNECTION_LIMIT"));
        }

        protected IPushNotificationMetadataService getService(string uri)
        {
            try
            {
                return (IPushNotificationMetadataService)Activator.GetObject(typeof(IPushNotificationMetadataService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }

        public PushNotificationAppGroupModel GetPushNotificationAppGroupModel(AppGroupID appGroupId)
        {
            //hard data
            //return GetPushNotificationAppGroupModelHardData(appGroupId);

            //db driven data
            string uri = "";

            try
            {
                //oResult = _cache.Get(GetRegionLanguageCacheKey()) as LanguageCollection;
                CacheblePushNotificationAppGroupModels cachedAppGroupModels = _cache.Get(CacheblePushNotificationAppGroupModels.CACHE_KEY) as CacheblePushNotificationAppGroupModels;

                if (cachedAppGroupModels == null || cachedAppGroupModels.PushNotificationAppGroupDictionary.Count <= 0)
                {
                    uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                    base.Checkout(uri);
                    try
                    {
                        cachedAppGroupModels = new CacheblePushNotificationAppGroupModels(getService(uri).GetPushNotificationAppGroupModels());
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }

                    _cache.Insert(cachedAppGroupModels);
                }

                if (!cachedAppGroupModels.PushNotificationAppGroupDictionary.ContainsKey(appGroupId))
                    return null;
                else
                    return cachedAppGroupModels.PushNotificationAppGroupDictionary[appGroupId];
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve PushNotificationAppGroupModels (uri: " + uri + ")", ex));
            }

        }

        private PushNotificationAppGroupModel GetPushNotificationAppGroupModelHardData(AppGroupID appGroupId)
        {
            //dummied up for now
            var model = new PushNotificationAppGroupModel();
            model.AppGroupId = appGroupId;
            model.NotificationAppGroupCategories = new List<PushNotificationAppGroupCategory>();
            model.NotificationTypes = new List<PushNotificationType>();

            //activity app category types
            var activityCategory = new PushNotificationAppGroupCategory();
            activityCategory.AppGroupId = appGroupId;
            activityCategory.Category = new PushNotificationCategory();
            activityCategory.Category.Id = PushNotificationCategoryId.Activity;
            activityCategory.NotificationCategoryTag = "JDatePushTypeActivity";
            activityCategory.NotificationAppGroupCategoryTypes = new List<PushNotificationAppGroupCategoryType>();

            activityCategory.NotificationAppGroupCategoryTypes.Add(new PushNotificationAppGroupCategoryType { AppGroupId = appGroupId, Order = 1, NotificationType = new PushNotificationType { ID = PushNotificationTypeID.MessageReceived, NotificationTextResourceKey = "RECEIVED_EMAIL" }, DeepLink = "spark-jdate://message/{messagelistid}/?sub={issub}", BadgeIncrementValue = 1 });
            model.NotificationTypes.Add(new PushNotificationType { ID = PushNotificationTypeID.MessageReceived, NotificationTextResourceKey = "RECEIVED_EMAIL" });

            activityCategory.NotificationAppGroupCategoryTypes.Add(new PushNotificationAppGroupCategoryType { AppGroupId = appGroupId, Order = 2, NotificationType = new PushNotificationType { ID = PushNotificationTypeID.ChatRequest, NotificationTextResourceKey = "CHAT_REQUEST" }, DeepLink = "spark-jdate://chat/{frommemberid}/?sub={issub}", BadgeIncrementValue = 1 });
            model.NotificationTypes.Add(new PushNotificationType { ID = PushNotificationTypeID.ChatRequest, NotificationTextResourceKey = "CHAT_REQUEST" });

            activityCategory.NotificationAppGroupCategoryTypes.Add(new PushNotificationAppGroupCategoryType { AppGroupId = appGroupId, Order = 3, NotificationType = new PushNotificationType { ID = PushNotificationTypeID.MutualYes, NotificationTextResourceKey = "MUTUAL_YES" }, DeepLink = "spark-jdate://mutualyes/{frommemberid}/?sub={issub}", BadgeIncrementValue = 1 });
            model.NotificationTypes.Add(new PushNotificationType { ID = PushNotificationTypeID.MutualYes, NotificationTextResourceKey = "MUTUAL_YES" });

            //photos app category types
            var accountCategory = new PushNotificationAppGroupCategory();
            accountCategory.AppGroupId = appGroupId;
            accountCategory.Category = new PushNotificationCategory();
            accountCategory.Category.Id = PushNotificationCategoryId.Photos;
            accountCategory.NotificationCategoryTag = "JDatePushTypePhotos";
            accountCategory.NotificationAppGroupCategoryTypes = new List<PushNotificationAppGroupCategoryType>();

            accountCategory.NotificationAppGroupCategoryTypes.Add(new PushNotificationAppGroupCategoryType { AppGroupId = appGroupId, Order = 1, NotificationType = new PushNotificationType { ID = PushNotificationTypeID.PhotoApproved, NotificationTextResourceKey = "PHOTO_APPROVED" }, DeepLink = "spark-jdate://photo/approved/?sub={issub}", BadgeIncrementValue = 0 });
            model.NotificationTypes.Add(new PushNotificationType { ID = PushNotificationTypeID.PhotoApproved, NotificationTextResourceKey = "PHOTO_APPROVED" });

            accountCategory.NotificationAppGroupCategoryTypes.Add(new PushNotificationAppGroupCategoryType { AppGroupId = appGroupId, Order = 2, NotificationType = new PushNotificationType { ID = PushNotificationTypeID.PhotoRejected, NotificationTextResourceKey = "PHOTO_REJECTED" }, DeepLink = "spark-jdate://photo/declined/?sub={issub}", BadgeIncrementValue = 0 });
            model.NotificationTypes.Add(new PushNotificationType { ID = PushNotificationTypeID.PhotoRejected, NotificationTextResourceKey = "PHOTO_REJECTED" });

            //promo app category types
            var promotionalCategory = new PushNotificationAppGroupCategory();
            promotionalCategory.AppGroupId = appGroupId;
            promotionalCategory.Category = new PushNotificationCategory();
            promotionalCategory.Category.Id = PushNotificationCategoryId.Promotional;
            promotionalCategory.NotificationCategoryTag = "JDatePushTypePromo";
            promotionalCategory.NotificationAppGroupCategoryTypes = new List<PushNotificationAppGroupCategoryType>();

            promotionalCategory.NotificationAppGroupCategoryTypes.Add(new PushNotificationAppGroupCategoryType { AppGroupId = appGroupId, Order = 1, NotificationType = new PushNotificationType { ID = PushNotificationTypeID.PromotionalAnnouncement, NotificationTextResourceKey = "PROMO_ANNOUNCEMENT" }, BadgeIncrementValue = 0 });
            model.NotificationTypes.Add(new PushNotificationType { ID = PushNotificationTypeID.PromotionalAnnouncement, NotificationTextResourceKey = "PROMO_ANNOUNCEMENT" });


            model.NotificationAppGroupCategories.Add(activityCategory);
            model.NotificationAppGroupCategories.Add(accountCategory);
            model.NotificationAppGroupCategories.Add(promotionalCategory);

            return model;
        }
    }
}
