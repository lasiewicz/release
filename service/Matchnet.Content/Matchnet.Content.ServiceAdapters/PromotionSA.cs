using System;
using System.Data;
using System.Text;


using Matchnet.Caching;
using Matchnet.Exceptions;
using Matchnet.RemotingClient;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.Promotion;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.ServiceDefinitions;


namespace Matchnet.Content.ServiceAdapters
{
	/// <summary>
	/// Summary description for PromotionSA.
	/// </summary>
	public class PromotionSA : SABase
	{
		
		#region constants
		private const string PROMOTIONS_CACHE_KEY_PREFIX = "~CONTENTPROMOTIONS^{0}";
		private const string PROMOTERS_CACHE_KEY_PREFIX = "~CONTENTPROMOTERS";
		private const string POPUP_PROMOTION_CACHE_KEY_PREFIX = "~CONTENTPOPUPPROMOTION^{0}{1}{2}{3}{4}{5}{6}";
		private const string PROMOTION_TEXT_CACHE_KEY_PREFIX = "~CONTENTPROMOTIONTEXT^{0}{1}{2}{3}";
		private const string PROMOTION_PRM_INFO_CACHE_KEY_PREFIX = "~CONTENTPRMINFO^{0}";
		private const string PROMOTION_REGISTRATION_PROMOTION_CACHE_KEY_PREFIX = "~CONTENTREGISTRATIONPROMOTION~|^{0}{1}";
		private const string PROMOTION_TOKEN_CACHE_KEY_PREFIX = "~CONTENTPROMOTIONTOKEN^{0}{1}";
		private const string BANNERS_CACHE_KEY_PREFIX = "~CONTENTBANNERS^{0}";
		private const string SERVICE_MANAGER_NAME = "PromotionSM";
		#endregion

		#region class variables
		private Cache _cache = null;
		#endregion

		#region Constructors
		/// <summary>
		/// Singleton instance
		/// </summary>
		public static readonly PromotionSA Instance = new PromotionSA();

		/// <summary>
		/// Constructor
		/// </summary>
		public PromotionSA()
		{
			_cache = Cache.Instance;
		}
		#endregion


		protected override void GetConnectionLimit()
		{
			base.MaxConnections = Convert.ToByte(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_SA_CONNECTION_LIMIT"));
		}

		
		#region public methods
		/// <summary>
		/// 
		/// </summary>
		/// <param name="brand"></param>
		/// <returns></returns>
		public RegistrationPromotionCollection RetrieveRegistrationPromotions(Brand brand)
		{
			string uri = "";
			RegistrationPromotionCollection oResult = null;
			try 
			{
				oResult = _cache.Get(GetRegistrationPromotionCacheKey(brand.Site.Community.CommunityID, brand.BrandID)) as RegistrationPromotionCollection;

				if(oResult == null) 
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).RetrieveRegistrationPromotions(brand.Site.Community.CommunityID, brand.BrandID);
					}
					finally
					{
						base.Checkin(uri);
					}
					oResult.SetCacheKey = GetRegistrationPromotionCacheKey(brand.Site.Community.CommunityID, brand.BrandID);
					CacheRegistrationPromotion(oResult);
				}
				return oResult;
			}
			catch(Exception ex) 
			{
				throw(new SAException("Cannot retrieve registration promotions (uri: " + uri + ")", ex));
			}
		}

		/// <summary>
		/// Retrieve all Banners for a given promotion in a private label
		/// </summary>
		/// <param name="groupID">The current group ID that the banners belong to</param>
		/// <returns>All banners for the promotion and privete label</returns>
        [Obsolete]
		public BannerCollection RetrieveBanners(int groupID)
		{
			string uri = "";
			BannerCollection oResult = null;
            /*REMOVED as part of DB efforts to clean up database*/
            /*
			try
			{
				oResult = _cache.Get(GetBannersCacheKey(groupID)) as BannerCollection;

				if (oResult == null)
				{
					uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
					base.Checkout(uri);
					try
					{
						oResult = getService(uri).RetrieveBanners(groupID);
					}
					finally
					{
						base.Checkin(uri);
					}
					oResult.SetCacheKey = GetBannersCacheKey(groupID);
					CacheBanners(oResult);
				}

				return oResult;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot retrieve banners (uri: " + uri + ")", ex));
			}
            */

            return oResult;
		}

        /// <summary>
        /// Retrieves a list of PromotionID and URL Referral mapping
        /// </summary>
        public PRMURLMapperList GetPRMURLMapperList()
        {
            string uri = "";
            PRMURLMapperList oResult = null;
            try
            {
                oResult = _cache.Get(PRMURLMapperList.GetCacheKeyString()) as PRMURLMapperList;

                if (oResult == null)
                {
                    uri = UriUtil.Instance.GetServiceManagerUri(SERVICE_MANAGER_NAME);
                    base.Checkout(uri);
                    try
                    {
                        oResult = getService(uri).GetPRMURLMapperList();
                    }
                    finally
                    {
                        base.Checkin(uri);
                    }

                    if (oResult != null)
                    {
                        _cache.Insert(oResult);
                    }
                }

                return oResult;
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot retrieve PRMURLMapperList (uri: " + uri + ")", ex));
            }
        }

		#endregion

		#region private Cache Routines
		private void CachePromotions(PromotionCollection pPromotionCollection)
		{
			pPromotionCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pPromotionCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PROMOTION_SA"));
			_cache.Insert(pPromotionCollection);
		}

		private void CachePromoters(PromoterCollection pPromoterCollection) 
		{
			pPromoterCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pPromoterCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PROMOTION_SA"));
			_cache.Insert(pPromoterCollection);
		}

		private void CachePopupPromotion(PopupPromotion pPopupPromotion) 
		{
			pPopupPromotion.CachePriority = CacheItemPriorityLevel.Normal;
			pPopupPromotion.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PROMOTION_SA"));
			_cache.Insert(pPopupPromotion);
		}

		private void CachePromotionText(PromotionText pPromotionText) 
		{
			pPromotionText.CachePriority = CacheItemPriorityLevel.Normal;
			pPromotionText.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PROMOTION_SA"));
			_cache.Insert(pPromotionText);
		}

		private void CachePromotionPRMInfo(PRMInfo pPRMInfo) 
		{
			pPRMInfo.CachePriority = CacheItemPriorityLevel.Normal;
			pPRMInfo.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PROMOTION_SA"));
			_cache.Insert(pPRMInfo);
		}

		private void CacheRegistrationPromotion(RegistrationPromotionCollection pRegistrationPromotionCollection)
		{
			pRegistrationPromotionCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pRegistrationPromotionCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PROMOTION_SA"));
			_cache.Insert(pRegistrationPromotionCollection);
		}

		private void CachePromotionTokens(PromotionTokenCollection pPromotionTokens) 
		{
			pPromotionTokens.CachePriority = CacheItemPriorityLevel.Normal;
			pPromotionTokens.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PROMOTION_SA"));
			_cache.Insert(pPromotionTokens);
		}

		private void CacheBanners(BannerCollection pBannerCollection)
		{
			pBannerCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pBannerCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PROMOTION_SA"));
			_cache.Insert(pBannerCollection);
		}

		private string GetPromotionsCacheKey(int promoterID)
		{
			return string.Format(PROMOTIONS_CACHE_KEY_PREFIX, promoterID);
		}

		private string GetPromotersCacheKey()
		{
			return PROMOTERS_CACHE_KEY_PREFIX;
		}

		private string GetPopUpPromotionCacheKey(int promotionID, int brandID, 
			int siteID, int pageID, DateTime date, string userConditions, string baseURL)
		{
			return string.Format(POPUP_PROMOTION_CACHE_KEY_PREFIX, promotionID, brandID,
				siteID, pageID, date, userConditions, baseURL);
		}

		private string GetPromotionTextCacheKey(int promotionID, int actionPageID, int communityID, 
			int memberID) 
		{
			return string.Format(PROMOTION_TEXT_CACHE_KEY_PREFIX, promotionID, actionPageID, communityID,
				memberID);
		}

		private string GetPromotionPRMInfoCacheKey(int prm) 
		{
			return string.Format(PROMOTION_PRM_INFO_CACHE_KEY_PREFIX, prm);
		}

		private string GetRegistrationPromotionCacheKey(int communityID, int brandID) 
		{
			return string.Format(PROMOTION_REGISTRATION_PROMOTION_CACHE_KEY_PREFIX, communityID, brandID);
		}

		private string GetPromotionTokensCacheKey(int communityID, int memberID) 
		{
			return string.Format(PROMOTION_TOKEN_CACHE_KEY_PREFIX, communityID, memberID);
		}

		private string GetBannersCacheKey(int groupID)
		{
			return string.Format(BANNERS_CACHE_KEY_PREFIX, groupID);
		}
		#endregion


		private IPromotionService getService(string uri)
		{
			try
			{
				return (IPromotionService)Activator.GetObject(typeof(IPromotionService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}
	}
}
