using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeOption;

namespace Matchnet.Content.Harness
{
	/// <summary>
	/// Summary description for AttributeOptionHarness.
	/// </summary>
	public class AttributeOptionHarness : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnGetAttributeOptions;
		private System.Windows.Forms.TextBox tbAttributeName;
		private System.Windows.Forms.TextBox tbGroupID;
		private System.Windows.Forms.TextBox tbAttributeOptions;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public AttributeOptionHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnGetAttributeOptions = new System.Windows.Forms.Button();
			this.tbAttributeName = new System.Windows.Forms.TextBox();
			this.tbGroupID = new System.Windows.Forms.TextBox();
			this.tbAttributeOptions = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// btnGetAttributeOptions
			// 
			this.btnGetAttributeOptions.Location = new System.Drawing.Point(80, 232);
			this.btnGetAttributeOptions.Name = "btnGetAttributeOptions";
			this.btnGetAttributeOptions.Size = new System.Drawing.Size(48, 24);
			this.btnGetAttributeOptions.TabIndex = 0;
			this.btnGetAttributeOptions.Text = "Go";
			this.btnGetAttributeOptions.Click += new System.EventHandler(this.btnGetAttributeOptions_Click);
			// 
			// tbAttributeName
			// 
			this.tbAttributeName.Location = new System.Drawing.Point(16, 184);
			this.tbAttributeName.Name = "tbAttributeName";
			this.tbAttributeName.Size = new System.Drawing.Size(72, 20);
			this.tbAttributeName.TabIndex = 1;
			this.tbAttributeName.Text = "MaritalStatus";
			// 
			// tbGroupID
			// 
			this.tbGroupID.Location = new System.Drawing.Point(128, 184);
			this.tbGroupID.Name = "tbGroupID";
			this.tbGroupID.Size = new System.Drawing.Size(72, 20);
			this.tbGroupID.TabIndex = 1;
			this.tbGroupID.Text = "-2147483647";
			// 
			// tbAttributeOptions
			// 
			this.tbAttributeOptions.Location = new System.Drawing.Point(40, 24);
			this.tbAttributeOptions.Multiline = true;
			this.tbAttributeOptions.Name = "tbAttributeOptions";
			this.tbAttributeOptions.Size = new System.Drawing.Size(224, 112);
			this.tbAttributeOptions.TabIndex = 2;
			this.tbAttributeOptions.Text = "";
			// 
			// AttributeOptionHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.tbAttributeOptions);
			this.Controls.Add(this.tbAttributeName);
			this.Controls.Add(this.tbGroupID);
			this.Controls.Add(this.btnGetAttributeOptions);
			this.Name = "AttributeOptionHarness";
			this.Text = "AttributeOptionHarness";
			this.ResumeLayout(false);

		}
		#endregion

		private void btnGetAttributeOptions_Click(object sender, System.EventArgs e)
		{
			this.tbAttributeOptions.Text = "";

			int groupID = Conversion.CInt(this.tbGroupID.Text);
			AttributeOptionCollection aoc = AttributeOptionSA.Instance.GetAttributeOptionCollection(this.tbAttributeName.Text, 3, 103, 1003);
			foreach (AttributeOption ao in aoc)
			{
				this.tbAttributeOptions.Text += ao.Value + ": " + ao.Description + ": " + ao.ListOrder + System.Environment.NewLine;
			}


		}
	}
}
