using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceManagers;
using Matchnet.Content.ValueObjects.Promotion;

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Matchnet.Content.Harness
{
	/// <summary>
	/// Summary description for PromotionHarness.
	/// </summary>
	public class PromotionHarness : System.Windows.Forms.Form
	{

		#region enumerations
		private enum SERVICE_LEVEL 
		{
			SERVICE_ADAPTER = 0,
			SERVICE_MANAGER = 1,
			BUSINESS_LOGIC = 2
		}
		#endregion

		#region class variables
		private SERVICE_LEVEL testLevel = SERVICE_LEVEL.BUSINESS_LOGIC;
		private System.Windows.Forms.Label lblPromoterID;
		private System.Windows.Forms.TextBox tbPromoterID;
		private System.Windows.Forms.Button btnPromotions;
		private System.Windows.Forms.ListBox lbPromotions;
		private System.Windows.Forms.ComboBox cbServiceLevel;
		private System.Windows.Forms.Label lblServiceLevel;
		private System.Windows.Forms.TextBox tbPromoterCompany;
		private System.Windows.Forms.Label lblPromoterCompany;
		private System.Windows.Forms.TextBox tbPromotionInsertDate;
		private System.Windows.Forms.Label lblPromotionInsertDate;
		private System.Windows.Forms.Button btnPromoters;
		private System.Windows.Forms.ListBox lbPromoters;
		private System.Windows.Forms.TextBox tbPromoterFirst;
		private System.Windows.Forms.Label lblPromoterFirst;
		private System.Windows.Forms.TextBox tbPromoterAddressOne;
		private System.Windows.Forms.Label lblPromoterAddressOne;
		private System.Windows.Forms.TextBox tbPromoterAddressTwo;
		private System.Windows.Forms.Label lblPromoterAddressTwo;
		private System.Windows.Forms.TextBox tbPromoterCity;
		private System.Windows.Forms.Label lblPromoterCity;
		private System.Windows.Forms.TextBox tbPromotersCompany;
		private System.Windows.Forms.Label lblPromotersCompany;
		private System.Windows.Forms.TextBox tbPromotersEmail;
		private System.Windows.Forms.Label lblPromotersEmail;
		private System.Windows.Forms.TextBox tbPRM;
		private System.Windows.Forms.Label lblPRM;
		private System.Windows.Forms.TextBox tbPRMPromoterID;
		private System.Windows.Forms.Label lblPRMPromoterID;
		private System.Windows.Forms.TextBox tbPRMPromotion;
		private System.Windows.Forms.Label lblPRMPromotion;
		private System.Windows.Forms.Button btnPRM;
		private System.Windows.Forms.TextBox tbRegistrationPromotionsCommunityID;
		private System.Windows.Forms.Label lblRegistrationPromotionsCommunityID;
		private System.Windows.Forms.TextBox tbRegistrationPromotionsBrandID;
		private System.Windows.Forms.Label lblRegistrationPromotionsBrandID;
		private System.Windows.Forms.Button btnRegistrationPromotions;
		private System.Windows.Forms.ListBox lbRegistrationPromotions;
		#endregion
		private System.Windows.Forms.Button btnPromotionToken;
		private System.Windows.Forms.ListBox lbPromotionTokens;
		private System.Windows.Forms.ListBox lbBanners;
		private System.Windows.Forms.TextBox tbGroupID;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnBanners;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public PromotionHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			PopulateServiceLevel();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblPromoterID = new System.Windows.Forms.Label();
			this.tbPromoterID = new System.Windows.Forms.TextBox();
			this.btnPromotions = new System.Windows.Forms.Button();
			this.lbPromotions = new System.Windows.Forms.ListBox();
			this.cbServiceLevel = new System.Windows.Forms.ComboBox();
			this.lblServiceLevel = new System.Windows.Forms.Label();
			this.tbPromoterCompany = new System.Windows.Forms.TextBox();
			this.lblPromoterCompany = new System.Windows.Forms.Label();
			this.tbPromotionInsertDate = new System.Windows.Forms.TextBox();
			this.lblPromotionInsertDate = new System.Windows.Forms.Label();
			this.btnPromoters = new System.Windows.Forms.Button();
			this.lbPromoters = new System.Windows.Forms.ListBox();
			this.tbPromoterFirst = new System.Windows.Forms.TextBox();
			this.lblPromoterFirst = new System.Windows.Forms.Label();
			this.tbPromoterAddressOne = new System.Windows.Forms.TextBox();
			this.lblPromoterAddressOne = new System.Windows.Forms.Label();
			this.tbPromoterAddressTwo = new System.Windows.Forms.TextBox();
			this.lblPromoterAddressTwo = new System.Windows.Forms.Label();
			this.tbPromoterCity = new System.Windows.Forms.TextBox();
			this.lblPromoterCity = new System.Windows.Forms.Label();
			this.tbPromotersCompany = new System.Windows.Forms.TextBox();
			this.lblPromotersCompany = new System.Windows.Forms.Label();
			this.tbPromotersEmail = new System.Windows.Forms.TextBox();
			this.lblPromotersEmail = new System.Windows.Forms.Label();
			this.tbPRM = new System.Windows.Forms.TextBox();
			this.lblPRM = new System.Windows.Forms.Label();
			this.tbPRMPromotion = new System.Windows.Forms.TextBox();
			this.lblPRMPromotion = new System.Windows.Forms.Label();
			this.tbPRMPromoterID = new System.Windows.Forms.TextBox();
			this.lblPRMPromoterID = new System.Windows.Forms.Label();
			this.btnPRM = new System.Windows.Forms.Button();
			this.tbRegistrationPromotionsCommunityID = new System.Windows.Forms.TextBox();
			this.lblRegistrationPromotionsCommunityID = new System.Windows.Forms.Label();
			this.tbRegistrationPromotionsBrandID = new System.Windows.Forms.TextBox();
			this.lblRegistrationPromotionsBrandID = new System.Windows.Forms.Label();
			this.btnRegistrationPromotions = new System.Windows.Forms.Button();
			this.lbRegistrationPromotions = new System.Windows.Forms.ListBox();
			this.btnPromotionToken = new System.Windows.Forms.Button();
			this.lbPromotionTokens = new System.Windows.Forms.ListBox();
			this.lbBanners = new System.Windows.Forms.ListBox();
			this.tbGroupID = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btnBanners = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lblPromoterID
			// 
			this.lblPromoterID.Location = new System.Drawing.Point(24, 56);
			this.lblPromoterID.Name = "lblPromoterID";
			this.lblPromoterID.Size = new System.Drawing.Size(68, 12);
			this.lblPromoterID.TabIndex = 0;
			this.lblPromoterID.Text = "Promoter ID";
			// 
			// tbPromoterID
			// 
			this.tbPromoterID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbPromoterID.Location = new System.Drawing.Point(92, 52);
			this.tbPromoterID.Name = "tbPromoterID";
			this.tbPromoterID.Size = new System.Drawing.Size(120, 20);
			this.tbPromoterID.TabIndex = 1;
			this.tbPromoterID.Text = "";
			// 
			// btnPromotions
			// 
			this.btnPromotions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnPromotions.Location = new System.Drawing.Point(136, 76);
			this.btnPromotions.Name = "btnPromotions";
			this.btnPromotions.TabIndex = 2;
			this.btnPromotions.Text = "Promotions";
			this.btnPromotions.Click += new System.EventHandler(this.btnPromotions_Click);
			// 
			// lbPromotions
			// 
			this.lbPromotions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lbPromotions.Location = new System.Drawing.Point(232, 20);
			this.lbPromotions.Name = "lbPromotions";
			this.lbPromotions.Size = new System.Drawing.Size(336, 93);
			this.lbPromotions.TabIndex = 3;
			this.lbPromotions.SelectedIndexChanged += new System.EventHandler(this.lbPromotions_SelectedIndexChanged);
			// 
			// cbServiceLevel
			// 
			this.cbServiceLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbServiceLevel.Location = new System.Drawing.Point(92, 20);
			this.cbServiceLevel.Name = "cbServiceLevel";
			this.cbServiceLevel.Size = new System.Drawing.Size(121, 21);
			this.cbServiceLevel.TabIndex = 4;
			// 
			// lblServiceLevel
			// 
			this.lblServiceLevel.Location = new System.Drawing.Point(16, 24);
			this.lblServiceLevel.Name = "lblServiceLevel";
			this.lblServiceLevel.Size = new System.Drawing.Size(76, 12);
			this.lblServiceLevel.TabIndex = 5;
			this.lblServiceLevel.Text = "Service Level";
			// 
			// tbPromoterCompany
			// 
			this.tbPromoterCompany.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbPromoterCompany.Location = new System.Drawing.Point(300, 116);
			this.tbPromoterCompany.Name = "tbPromoterCompany";
			this.tbPromoterCompany.ReadOnly = true;
			this.tbPromoterCompany.Size = new System.Drawing.Size(268, 20);
			this.tbPromoterCompany.TabIndex = 7;
			this.tbPromoterCompany.Text = "";
			// 
			// lblPromoterCompany
			// 
			this.lblPromoterCompany.Location = new System.Drawing.Point(240, 120);
			this.lblPromoterCompany.Name = "lblPromoterCompany";
			this.lblPromoterCompany.Size = new System.Drawing.Size(56, 12);
			this.lblPromoterCompany.TabIndex = 6;
			this.lblPromoterCompany.Text = "Company";
			// 
			// tbPromotionInsertDate
			// 
			this.tbPromotionInsertDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbPromotionInsertDate.Location = new System.Drawing.Point(300, 140);
			this.tbPromotionInsertDate.Name = "tbPromotionInsertDate";
			this.tbPromotionInsertDate.ReadOnly = true;
			this.tbPromotionInsertDate.Size = new System.Drawing.Size(268, 20);
			this.tbPromotionInsertDate.TabIndex = 9;
			this.tbPromotionInsertDate.Text = "";
			// 
			// lblPromotionInsertDate
			// 
			this.lblPromotionInsertDate.Location = new System.Drawing.Point(232, 144);
			this.lblPromotionInsertDate.Name = "lblPromotionInsertDate";
			this.lblPromotionInsertDate.Size = new System.Drawing.Size(60, 12);
			this.lblPromotionInsertDate.TabIndex = 8;
			this.lblPromotionInsertDate.Text = "Insert Date";
			// 
			// btnPromoters
			// 
			this.btnPromoters.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnPromoters.Location = new System.Drawing.Point(136, 244);
			this.btnPromoters.Name = "btnPromoters";
			this.btnPromoters.Size = new System.Drawing.Size(75, 24);
			this.btnPromoters.TabIndex = 10;
			this.btnPromoters.Text = "Promoters";
			this.btnPromoters.Click += new System.EventHandler(this.btnPromoters_Click);
			// 
			// lbPromoters
			// 
			this.lbPromoters.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lbPromoters.Location = new System.Drawing.Point(232, 176);
			this.lbPromoters.Name = "lbPromoters";
			this.lbPromoters.Size = new System.Drawing.Size(336, 93);
			this.lbPromoters.TabIndex = 11;
			this.lbPromoters.SelectedIndexChanged += new System.EventHandler(this.lbPromoters_SelectedIndexChanged);
			// 
			// tbPromoterFirst
			// 
			this.tbPromoterFirst.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbPromoterFirst.Location = new System.Drawing.Point(296, 272);
			this.tbPromoterFirst.Name = "tbPromoterFirst";
			this.tbPromoterFirst.ReadOnly = true;
			this.tbPromoterFirst.Size = new System.Drawing.Size(104, 20);
			this.tbPromoterFirst.TabIndex = 13;
			this.tbPromoterFirst.Text = "";
			// 
			// lblPromoterFirst
			// 
			this.lblPromoterFirst.Location = new System.Drawing.Point(260, 276);
			this.lblPromoterFirst.Name = "lblPromoterFirst";
			this.lblPromoterFirst.Size = new System.Drawing.Size(28, 12);
			this.lblPromoterFirst.TabIndex = 12;
			this.lblPromoterFirst.Text = "First";
			// 
			// tbPromoterAddressOne
			// 
			this.tbPromoterAddressOne.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbPromoterAddressOne.Location = new System.Drawing.Point(296, 296);
			this.tbPromoterAddressOne.Name = "tbPromoterAddressOne";
			this.tbPromoterAddressOne.ReadOnly = true;
			this.tbPromoterAddressOne.Size = new System.Drawing.Size(104, 20);
			this.tbPromoterAddressOne.TabIndex = 15;
			this.tbPromoterAddressOne.Text = "";
			// 
			// lblPromoterAddressOne
			// 
			this.lblPromoterAddressOne.Location = new System.Drawing.Point(236, 300);
			this.lblPromoterAddressOne.Name = "lblPromoterAddressOne";
			this.lblPromoterAddressOne.Size = new System.Drawing.Size(56, 12);
			this.lblPromoterAddressOne.TabIndex = 14;
			this.lblPromoterAddressOne.Text = "Addr One";
			// 
			// tbPromoterAddressTwo
			// 
			this.tbPromoterAddressTwo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbPromoterAddressTwo.Location = new System.Drawing.Point(296, 320);
			this.tbPromoterAddressTwo.Name = "tbPromoterAddressTwo";
			this.tbPromoterAddressTwo.ReadOnly = true;
			this.tbPromoterAddressTwo.Size = new System.Drawing.Size(104, 20);
			this.tbPromoterAddressTwo.TabIndex = 17;
			this.tbPromoterAddressTwo.Text = "";
			// 
			// lblPromoterAddressTwo
			// 
			this.lblPromoterAddressTwo.Location = new System.Drawing.Point(236, 324);
			this.lblPromoterAddressTwo.Name = "lblPromoterAddressTwo";
			this.lblPromoterAddressTwo.Size = new System.Drawing.Size(56, 12);
			this.lblPromoterAddressTwo.TabIndex = 16;
			this.lblPromoterAddressTwo.Text = "Addr Two";
			// 
			// tbPromoterCity
			// 
			this.tbPromoterCity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbPromoterCity.Location = new System.Drawing.Point(464, 272);
			this.tbPromoterCity.Name = "tbPromoterCity";
			this.tbPromoterCity.ReadOnly = true;
			this.tbPromoterCity.Size = new System.Drawing.Size(104, 20);
			this.tbPromoterCity.TabIndex = 19;
			this.tbPromoterCity.Text = "";
			// 
			// lblPromoterCity
			// 
			this.lblPromoterCity.Location = new System.Drawing.Point(432, 276);
			this.lblPromoterCity.Name = "lblPromoterCity";
			this.lblPromoterCity.Size = new System.Drawing.Size(28, 12);
			this.lblPromoterCity.TabIndex = 18;
			this.lblPromoterCity.Text = "City";
			// 
			// tbPromotersCompany
			// 
			this.tbPromotersCompany.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbPromotersCompany.Location = new System.Drawing.Point(464, 296);
			this.tbPromotersCompany.Name = "tbPromotersCompany";
			this.tbPromotersCompany.ReadOnly = true;
			this.tbPromotersCompany.Size = new System.Drawing.Size(104, 20);
			this.tbPromotersCompany.TabIndex = 21;
			this.tbPromotersCompany.Text = "";
			// 
			// lblPromotersCompany
			// 
			this.lblPromotersCompany.Location = new System.Drawing.Point(404, 300);
			this.lblPromotersCompany.Name = "lblPromotersCompany";
			this.lblPromotersCompany.Size = new System.Drawing.Size(56, 12);
			this.lblPromotersCompany.TabIndex = 20;
			this.lblPromotersCompany.Text = "Company";
			// 
			// tbPromotersEmail
			// 
			this.tbPromotersEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbPromotersEmail.Location = new System.Drawing.Point(464, 320);
			this.tbPromotersEmail.Name = "tbPromotersEmail";
			this.tbPromotersEmail.ReadOnly = true;
			this.tbPromotersEmail.Size = new System.Drawing.Size(104, 20);
			this.tbPromotersEmail.TabIndex = 23;
			this.tbPromotersEmail.Text = "";
			// 
			// lblPromotersEmail
			// 
			this.lblPromotersEmail.Location = new System.Drawing.Point(424, 324);
			this.lblPromotersEmail.Name = "lblPromotersEmail";
			this.lblPromotersEmail.Size = new System.Drawing.Size(36, 12);
			this.lblPromotersEmail.TabIndex = 22;
			this.lblPromotersEmail.Text = "Email";
			// 
			// tbPRM
			// 
			this.tbPRM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbPRM.Location = new System.Drawing.Point(92, 360);
			this.tbPRM.Name = "tbPRM";
			this.tbPRM.Size = new System.Drawing.Size(120, 20);
			this.tbPRM.TabIndex = 25;
			this.tbPRM.Text = "";
			// 
			// lblPRM
			// 
			this.lblPRM.Location = new System.Drawing.Point(60, 364);
			this.lblPRM.Name = "lblPRM";
			this.lblPRM.Size = new System.Drawing.Size(32, 12);
			this.lblPRM.TabIndex = 24;
			this.lblPRM.Text = "PRM";
			// 
			// tbPRMPromotion
			// 
			this.tbPRMPromotion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbPRMPromotion.Location = new System.Drawing.Point(296, 384);
			this.tbPRMPromotion.Name = "tbPRMPromotion";
			this.tbPRMPromotion.ReadOnly = true;
			this.tbPRMPromotion.Size = new System.Drawing.Size(272, 20);
			this.tbPRMPromotion.TabIndex = 29;
			this.tbPRMPromotion.Text = "";
			// 
			// lblPRMPromotion
			// 
			this.lblPRMPromotion.Location = new System.Drawing.Point(232, 388);
			this.lblPRMPromotion.Name = "lblPRMPromotion";
			this.lblPRMPromotion.Size = new System.Drawing.Size(56, 12);
			this.lblPRMPromotion.TabIndex = 28;
			this.lblPRMPromotion.Text = "Promotion";
			// 
			// tbPRMPromoterID
			// 
			this.tbPRMPromoterID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbPRMPromoterID.Location = new System.Drawing.Point(296, 360);
			this.tbPRMPromoterID.Name = "tbPRMPromoterID";
			this.tbPRMPromoterID.ReadOnly = true;
			this.tbPRMPromoterID.Size = new System.Drawing.Size(272, 20);
			this.tbPRMPromoterID.TabIndex = 27;
			this.tbPRMPromoterID.Text = "";
			// 
			// lblPRMPromoterID
			// 
			this.lblPRMPromoterID.Location = new System.Drawing.Point(224, 364);
			this.lblPRMPromoterID.Name = "lblPRMPromoterID";
			this.lblPRMPromoterID.Size = new System.Drawing.Size(68, 12);
			this.lblPRMPromoterID.TabIndex = 26;
			this.lblPRMPromoterID.Text = "Promoter ID";
			// 
			// btnPRM
			// 
			this.btnPRM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnPRM.Location = new System.Drawing.Point(136, 384);
			this.btnPRM.Name = "btnPRM";
			this.btnPRM.Size = new System.Drawing.Size(75, 20);
			this.btnPRM.TabIndex = 30;
			this.btnPRM.Text = "PRM";
			this.btnPRM.Click += new System.EventHandler(this.btnPRM_Click);
			// 
			// tbRegistrationPromotionsCommunityID
			// 
			this.tbRegistrationPromotionsCommunityID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbRegistrationPromotionsCommunityID.Location = new System.Drawing.Point(92, 424);
			this.tbRegistrationPromotionsCommunityID.Name = "tbRegistrationPromotionsCommunityID";
			this.tbRegistrationPromotionsCommunityID.Size = new System.Drawing.Size(120, 20);
			this.tbRegistrationPromotionsCommunityID.TabIndex = 32;
			this.tbRegistrationPromotionsCommunityID.Text = "";
			// 
			// lblRegistrationPromotionsCommunityID
			// 
			this.lblRegistrationPromotionsCommunityID.Location = new System.Drawing.Point(16, 428);
			this.lblRegistrationPromotionsCommunityID.Name = "lblRegistrationPromotionsCommunityID";
			this.lblRegistrationPromotionsCommunityID.Size = new System.Drawing.Size(76, 12);
			this.lblRegistrationPromotionsCommunityID.TabIndex = 31;
			this.lblRegistrationPromotionsCommunityID.Text = "Community ID";
			// 
			// tbRegistrationPromotionsBrandID
			// 
			this.tbRegistrationPromotionsBrandID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbRegistrationPromotionsBrandID.Location = new System.Drawing.Point(92, 456);
			this.tbRegistrationPromotionsBrandID.Name = "tbRegistrationPromotionsBrandID";
			this.tbRegistrationPromotionsBrandID.Size = new System.Drawing.Size(120, 20);
			this.tbRegistrationPromotionsBrandID.TabIndex = 34;
			this.tbRegistrationPromotionsBrandID.Text = "";
			// 
			// lblRegistrationPromotionsBrandID
			// 
			this.lblRegistrationPromotionsBrandID.Location = new System.Drawing.Point(40, 460);
			this.lblRegistrationPromotionsBrandID.Name = "lblRegistrationPromotionsBrandID";
			this.lblRegistrationPromotionsBrandID.Size = new System.Drawing.Size(52, 12);
			this.lblRegistrationPromotionsBrandID.TabIndex = 33;
			this.lblRegistrationPromotionsBrandID.Text = "Brand ID";
			// 
			// btnRegistrationPromotions
			// 
			this.btnRegistrationPromotions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnRegistrationPromotions.Location = new System.Drawing.Point(136, 480);
			this.btnRegistrationPromotions.Name = "btnRegistrationPromotions";
			this.btnRegistrationPromotions.Size = new System.Drawing.Size(75, 20);
			this.btnRegistrationPromotions.TabIndex = 35;
			this.btnRegistrationPromotions.Text = "Registrations";
			this.btnRegistrationPromotions.Click += new System.EventHandler(this.btnRegistrationPromotions_Click);
			// 
			// lbRegistrationPromotions
			// 
			this.lbRegistrationPromotions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lbRegistrationPromotions.Location = new System.Drawing.Point(232, 424);
			this.lbRegistrationPromotions.Name = "lbRegistrationPromotions";
			this.lbRegistrationPromotions.Size = new System.Drawing.Size(336, 93);
			this.lbRegistrationPromotions.TabIndex = 36;
			// 
			// btnPromotionToken
			// 
			this.btnPromotionToken.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnPromotionToken.Location = new System.Drawing.Point(136, 536);
			this.btnPromotionToken.Name = "btnPromotionToken";
			this.btnPromotionToken.Size = new System.Drawing.Size(75, 20);
			this.btnPromotionToken.TabIndex = 37;
			this.btnPromotionToken.Text = "Token";
			this.btnPromotionToken.Click += new System.EventHandler(this.btnPromotionToken_Click);
			// 
			// lbPromotionTokens
			// 
			this.lbPromotionTokens.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lbPromotionTokens.Location = new System.Drawing.Point(232, 536);
			this.lbPromotionTokens.Name = "lbPromotionTokens";
			this.lbPromotionTokens.Size = new System.Drawing.Size(336, 93);
			this.lbPromotionTokens.TabIndex = 38;
			// 
			// lbBanners
			// 
			this.lbBanners.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lbBanners.Location = new System.Drawing.Point(232, 644);
			this.lbBanners.Name = "lbBanners";
			this.lbBanners.Size = new System.Drawing.Size(336, 132);
			this.lbBanners.TabIndex = 39;
			// 
			// tbGroupID
			// 
			this.tbGroupID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbGroupID.Location = new System.Drawing.Point(92, 644);
			this.tbGroupID.Name = "tbGroupID";
			this.tbGroupID.Size = new System.Drawing.Size(120, 20);
			this.tbGroupID.TabIndex = 41;
			this.tbGroupID.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(40, 648);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(52, 12);
			this.label1.TabIndex = 40;
			this.label1.Text = "Group ID";
			// 
			// btnBanners
			// 
			this.btnBanners.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBanners.Location = new System.Drawing.Point(136, 672);
			this.btnBanners.Name = "btnBanners";
			this.btnBanners.Size = new System.Drawing.Size(75, 20);
			this.btnBanners.TabIndex = 42;
			this.btnBanners.Text = "Banners";
			this.btnBanners.Click += new System.EventHandler(this.btnBanners_Click);
			// 
			// PromotionHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(616, 790);
			this.Controls.Add(this.btnBanners);
			this.Controls.Add(this.tbGroupID);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.lbBanners);
			this.Controls.Add(this.lbPromotionTokens);
			this.Controls.Add(this.btnPromotionToken);
			this.Controls.Add(this.lbRegistrationPromotions);
			this.Controls.Add(this.btnRegistrationPromotions);
			this.Controls.Add(this.tbRegistrationPromotionsBrandID);
			this.Controls.Add(this.lblRegistrationPromotionsBrandID);
			this.Controls.Add(this.tbRegistrationPromotionsCommunityID);
			this.Controls.Add(this.lblRegistrationPromotionsCommunityID);
			this.Controls.Add(this.btnPRM);
			this.Controls.Add(this.tbPRMPromotion);
			this.Controls.Add(this.lblPRMPromotion);
			this.Controls.Add(this.tbPRMPromoterID);
			this.Controls.Add(this.lblPRMPromoterID);
			this.Controls.Add(this.tbPRM);
			this.Controls.Add(this.lblPRM);
			this.Controls.Add(this.tbPromotersEmail);
			this.Controls.Add(this.lblPromotersEmail);
			this.Controls.Add(this.tbPromotersCompany);
			this.Controls.Add(this.lblPromotersCompany);
			this.Controls.Add(this.tbPromoterCity);
			this.Controls.Add(this.lblPromoterCity);
			this.Controls.Add(this.tbPromoterAddressTwo);
			this.Controls.Add(this.lblPromoterAddressTwo);
			this.Controls.Add(this.tbPromoterAddressOne);
			this.Controls.Add(this.lblPromoterAddressOne);
			this.Controls.Add(this.tbPromoterFirst);
			this.Controls.Add(this.lblPromoterFirst);
			this.Controls.Add(this.lbPromoters);
			this.Controls.Add(this.btnPromoters);
			this.Controls.Add(this.tbPromotionInsertDate);
			this.Controls.Add(this.lblPromotionInsertDate);
			this.Controls.Add(this.tbPromoterCompany);
			this.Controls.Add(this.lblPromoterCompany);
			this.Controls.Add(this.lblServiceLevel);
			this.Controls.Add(this.cbServiceLevel);
			this.Controls.Add(this.lbPromotions);
			this.Controls.Add(this.btnPromotions);
			this.Controls.Add(this.tbPromoterID);
			this.Controls.Add(this.lblPromoterID);
			this.Name = "PromotionHarness";
			this.Text = "PromotionHarness";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.ResumeLayout(false);

		}
		#endregion

		#region button click event handlers
		private void btnPromotionToken_Click(object sender, System.EventArgs e)
		{
			// currently testing hard coded parameters
			PromotionSM promotionServiceManager = null;
			PromotionTokenCollection result = null;
			try 
			{
				testLevel = getSelectedLevel();

//				switch(testLevel) 
//				{
//					case SERVICE_LEVEL.BUSINESS_LOGIC:
//						result = PromotionBL.Instance.RetrievePromotionReplacementTokens(2, 2);
//						break;
//					case SERVICE_LEVEL.SERVICE_MANAGER:
//						promotionServiceManager = new PromotionSM();
//						result = promotionServiceManager.RetrievePromotionReplacementTokens(2, 2);
//						break;
//					case SERVICE_LEVEL.SERVICE_ADAPTER:
//						result = PromotionSA.Instance.RetrievePromotionReplacementTokens(2, 2);
//						break;
//					default:
//						result = PromotionBL.Instance.RetrievePromotionReplacementTokens(2, 2);
//						break;
//				}
				DisplayPromotionTokens(result);
			} 
			catch(Exception ex) 
			{
				MessageBox.Show("Error Encountered: " + ex.Message);
			}
		}

		private void btnPromotions_Click(object sender, System.EventArgs e)
		{
			int promoterID = 0;
			PromotionSM promotionServiceManager = null;
			PromotionCollection result = null;

			try 
			{
//				testLevel = getSelectedLevel();
//
//				promoterID = int.Parse(tbPromoterID.Text.Trim());
//				switch(testLevel) 
//				{
//					case SERVICE_LEVEL.BUSINESS_LOGIC:
//						result = PromotionBL.Instance.RetrievePromotions(promoterID);
//						break;
//					case SERVICE_LEVEL.SERVICE_MANAGER:
//						promotionServiceManager = new PromotionSM();
//						result = promotionServiceManager.RetrievePromotions(promoterID);
//						break;
//					case SERVICE_LEVEL.SERVICE_ADAPTER:
//						result = PromotionSA.Instance.RetrievePromotions(promoterID);
//						break;
//					default:
//						result = PromotionBL.Instance.RetrievePromotions(promoterID);
//						break;
//				}

				DisplayPromotionResults(result);
			} 
			catch(FormatException) 
			{
				MessageBox.Show("Invalid Promoter ID");
			}
			catch(Exception ex) 
			{
				MessageBox.Show("Error Encountered: " + ex.Message);
			}
		}

		private void btnPromoters_Click(object sender, System.EventArgs e)
		{
			PromoterCollection result = null;
			PromotionSM promotionServiceManager = null;

			try 
			{
//				testLevel = getSelectedLevel();
//				switch(testLevel) 
//				{
//					case SERVICE_LEVEL.BUSINESS_LOGIC:
//						result = PromotionBL.Instance.RetrievePromoters();
//						break;
//					case SERVICE_LEVEL.SERVICE_MANAGER:
//						promotionServiceManager = new PromotionSM();
//						result = promotionServiceManager.RetrievePromoters();
//						break;
//					case SERVICE_LEVEL.SERVICE_ADAPTER:
//						result = PromotionSA.Instance.RetrievePromoters();
//						break;
//					default:
//						result = PromotionSA.Instance.RetrievePromoters();
//						break;
//				}

				DisplayPromotersResults(result);
			} 
			catch(Exception ex) 
			{
				MessageBox.Show("Error Encountered: " + ex.Message);
			}
		}

		private void btnBanners_Click(object sender, System.EventArgs e)
		{
			BannerCollection result = null;
			PromotionSM promotionServiceManager = null;

			try
			{
				int groupID = System.Convert.ToInt32(tbGroupID.Text);

				testLevel = getSelectedLevel();
                //switch(testLevel)
                //{
                //    case SERVICE_LEVEL.BUSINESS_LOGIC:
                //        result = PromotionBL.Instance.RetrieveBanners(groupID);
                //        break;
                //    case SERVICE_LEVEL.SERVICE_MANAGER:
                //        promotionServiceManager = new PromotionSM();
                //        result = promotionServiceManager.RetrieveBanners(groupID);
                //        break;
                //    case SERVICE_LEVEL.SERVICE_ADAPTER:
                //        result = PromotionSA.Instance.RetrieveBanners(groupID);
                //        break;
                //    default:
                //        result = PromotionSA.Instance.RetrieveBanners(groupID);
                //        break;
                //}
			}
			catch(Exception ex)
			{
				MessageBox.Show("Error Encountered: " + ex.Message);
			}
		
			DisplayBannersResult(result);
		}

		private void btnPRM_Click(object sender, System.EventArgs e)
		{
			PRMInfo result = null;
			PromotionSM promotionServiceManager = null;
			int prm = 0;

			try 
			{
				testLevel = getSelectedLevel();
				prm = int.Parse(tbPRM.Text.Trim());

//				switch(testLevel) 
//				{
//					case SERVICE_LEVEL.BUSINESS_LOGIC:
//						result = PromotionBL.Instance.RetrievePRMInfo(prm);
//						break;
//					case SERVICE_LEVEL.SERVICE_MANAGER:
//						promotionServiceManager = new PromotionSM();
//						result = promotionServiceManager.RetrievePRMInfo(prm);
//						break;
//					case SERVICE_LEVEL.SERVICE_ADAPTER:
//						result = PromotionSA.Instance.RetrievePRMInfo(prm);
//						break;
//					default:
//						result = PromotionSA.Instance.RetrievePRMInfo(prm);
//						break;
//				}

				DisplayPRMResult(result);
			}
			catch(FormatException) 
			{
				MessageBox.Show("Invalid PRM");
			}
			catch(Exception ex) 
			{
				MessageBox.Show("Error Encountered: " + ex.Message);
			}
		}

		private void btnRegistrationPromotions_Click(object sender, System.EventArgs e)
		{
			/*
			PromotionSM promotionServiceManager = null;
			RegistrationPromotionCollection result = null;
			int communityID = 0;
			int brandID = 0;

			try
			{
				testLevel = getSelectedLevel();
				communityID = int.Parse(tbRegistrationPromotionsCommunityID.Text.Trim());
				brandID = int.Parse(tbRegistrationPromotionsBrandID.Text.Trim());

				switch(testLevel) 
				{
					case SERVICE_LEVEL.BUSINESS_LOGIC:
						result = PromotionBL.Instance.RetrieveRegistrationPromotions(communityID, brandID);
						break;
					case SERVICE_LEVEL.SERVICE_MANAGER:
						promotionServiceManager = new PromotionSM();
						result = promotionServiceManager.RetrieveRegistrationPromotions(communityID, brandID);
						break;
					case SERVICE_LEVEL.SERVICE_ADAPTER:
						result = PromotionSA.Instance.RetrieveRegistrationPromotions(communityID, brandID);
						break;
				}

				DisplayRegistrationResults(result);
			}
			catch(FormatException)
			{
				MessageBox.Show("Invalid Community ID / Brand ID");
			}
			catch(Exception ex) 
			{
				MessageBox.Show("Error Encountered: " + ex.Message);
			}
			*/
		}
		#endregion

		#region result display routine
		private void DisplayPromotionTokens(PromotionTokenCollection pResult) 
		{
			lbPromotionTokens.Items.Clear();
			if(pResult != null) 
			{
				lbPromotionTokens.DisplayMember = "Value";
				foreach(PromotionToken currentToken in pResult)
				{
					lbPromotionTokens.Items.Add(currentToken);
				}
			}
		}

		private void DisplayPromotionResults(PromotionCollection pResult) 
		{
			lbPromotions.Items.Clear();
			if(pResult != null) 
			{
				lbPromotions.DisplayMember = "Description";
				foreach(Promotion currentPromotion in pResult) 
				{
					lbPromotions.Items.Add(currentPromotion);
				}
			}
		}

		private void DisplayPromotersResults(PromoterCollection pResult) 
		{
			lbPromoters.Items.Clear();
			if(pResult != null) 
			{
				lbPromoters.DisplayMember = "LastName";
				foreach(Promoter currentPromoter in pResult) 
				{
					lbPromoters.Items.Add(currentPromoter);
				}
			}
		}

		private void DisplayPRMResult(PRMInfo pResult) 
		{
			tbPRMPromoterID.Text = "";
			tbPRMPromotion.Text = "";
			if(pResult != null) 
			{
				tbPRMPromoterID.Text = pResult.Promoter;
				tbPRMPromotion.Text = pResult.Promotion;
			}
		}

		private void DisplayBannersResult(BannerCollection pResult)
		{
			lbBanners.Items.Clear();

			if(pResult != null)
			{
				lbBanners.DisplayMember = "ResourceConstant";

				foreach(Banner banner in pResult)
				{
					lbBanners.Items.Add(banner);
				}
			}
		}

		private void DisplayRegistrationResults(RegistrationPromotionCollection pResult) 
		{
			lbRegistrationPromotions.Items.Clear();
			if(pResult != null) 
			{
				lbRegistrationPromotions.DisplayMember = "PromotionID";
				foreach(RegistrationPromotion currentPromotion in pResult) 
				{
					lbRegistrationPromotions.Items.Add(currentPromotion);
				}
			}
		}
		#endregion

		#region listbox selection logic
		private void lbPromotions_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Promotion selectedPromotion = null;

			tbPromoterCompany.Text = "";
			tbPromotionInsertDate.Text = "";

			selectedPromotion = (Promotion)lbPromotions.SelectedItem;
			if(selectedPromotion != null) 
			{
				tbPromoterCompany.Text = selectedPromotion.PromoterCompany;
				tbPromotionInsertDate.Text = selectedPromotion.InsertDate.ToString();
			}
		}

		private void lbPromoters_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Promoter selectedPromoter = null;

			tbPromoterAddressOne.Text = "";
			tbPromoterAddressTwo.Text = "";
			tbPromoterCity.Text = "";
			tbPromoterFirst.Text = "";
			tbPromotersCompany.Text = "";
			tbPromotersEmail.Text = "";

			selectedPromoter = (Promoter)lbPromoters.SelectedItem;
			if(selectedPromoter != null) 
			{
				tbPromoterAddressOne.Text = selectedPromoter.Address;
				tbPromoterAddressTwo.Text = selectedPromoter.Address2;
				tbPromoterCity.Text = selectedPromoter.City;
				tbPromoterFirst.Text = selectedPromoter.FirstName;
				tbPromotersCompany.Text = selectedPromoter.Company;
				tbPromotersEmail.Text = selectedPromoter.EmailAddress;
			}
		}
		#endregion

		#region utilities
		private SERVICE_LEVEL getSelectedLevel() 
		{
			return (SERVICE_LEVEL)Enum.Parse(typeof(SERVICE_LEVEL), cbServiceLevel.SelectedItem.ToString());
		}

		private void PopulateServiceLevel() 
		{
			cbServiceLevel.Items.Clear();
			foreach(String currentLevel in Enum.GetNames(typeof(SERVICE_LEVEL))) 
			{
				cbServiceLevel.Items.Add(currentLevel);
			}
			cbServiceLevel.SelectedIndex = 0;
		}
		#endregion

	}
}
