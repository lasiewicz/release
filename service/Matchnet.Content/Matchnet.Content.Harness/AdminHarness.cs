using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceManagers;
using Matchnet.Content.ValueObjects.Admin;

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using Matchnet.Content.ValueObjects.LandingPage;

namespace Matchnet.Content.Harness
{
	/// <summary>
	/// Summary description for AdminHarness.
	/// </summary>
	public class AdminHarness : System.Windows.Forms.Form
	{
		#region enumerations
		private enum SERVICE_LEVEL 
		{
			SERVICE_ADAPTER = 0,
			SERVICE_MANAGER = 1,
			BUSINESS_LOGIC = 2
		}
		#endregion

		private Object testObject;

		private SERVICE_LEVEL testLevel = SERVICE_LEVEL.BUSINESS_LOGIC;
		private System.Windows.Forms.Button btnAdminActionLogs;
		private System.Windows.Forms.ComboBox cbServiceLevel;
		private System.Windows.Forms.TextBox tbAdminMemberID;
		private System.Windows.Forms.TextBox tbCommunityID;
		private System.Windows.Forms.Label lblAdminMemberID;
		private System.Windows.Forms.Label lblCommunityID;
		private System.Windows.Forms.Label lblServiceLevel;
		private System.Windows.Forms.ListBox lbActionLogs;
		private System.Windows.Forms.Label lblActionLogs;
		private System.Windows.Forms.Label lbActionLogMemberID;
		private System.Windows.Forms.Label lblActionLogEmailAddress;
		private System.Windows.Forms.Label lblActionLogActionDescription;
		private System.Windows.Forms.Label lbActionLogInsertDate;
		private System.Windows.Forms.TextBox tbActionLogMemberID;
		private System.Windows.Forms.TextBox tbActionLogEmailAddress;
		private System.Windows.Forms.TextBox tbActionLogActionDescription;
		private System.Windows.Forms.TextBox tbActionLogInsertDate;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TextBox PublishIDTextBox;
		private System.Windows.Forms.Label PublishIDLabel;
		private System.Windows.Forms.TextBox PublishesTextBox;
		private System.Windows.Forms.Button GetPublishesButton;
		private System.Windows.Forms.Label StartDateLabel;
		private System.Windows.Forms.Label EndDateLabel;
		private System.Windows.Forms.DateTimePicker StartDatePicker;
		private System.Windows.Forms.DateTimePicker EndDatePicker;
		private System.Windows.Forms.Button SavePublishButton;
		private System.Windows.Forms.TextBox ObjectIDTextBox;
		private System.Windows.Forms.Label ObjectIDLabel;
		private System.Windows.Forms.Button SavePublishActionButton;
		private System.Windows.Forms.Button GetNextPublishActionPublishObjectButton;
		private System.Windows.Forms.Button GetLandingPages;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

        public AdminHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			PopulateServiceLevel();
		}

		private void PopulateServiceLevel() 
		{
			cbServiceLevel.Items.Clear();
			foreach(String currentLevel in Enum.GetNames(typeof(SERVICE_LEVEL))) 
			{
				cbServiceLevel.Items.Add(currentLevel);
			}
			cbServiceLevel.SelectedIndex = 0;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnAdminActionLogs = new System.Windows.Forms.Button();
			this.cbServiceLevel = new System.Windows.Forms.ComboBox();
			this.tbAdminMemberID = new System.Windows.Forms.TextBox();
			this.tbCommunityID = new System.Windows.Forms.TextBox();
			this.lblAdminMemberID = new System.Windows.Forms.Label();
			this.lblCommunityID = new System.Windows.Forms.Label();
			this.lblServiceLevel = new System.Windows.Forms.Label();
			this.lbActionLogs = new System.Windows.Forms.ListBox();
			this.lblActionLogs = new System.Windows.Forms.Label();
			this.lbActionLogMemberID = new System.Windows.Forms.Label();
			this.lblActionLogEmailAddress = new System.Windows.Forms.Label();
			this.lblActionLogActionDescription = new System.Windows.Forms.Label();
			this.lbActionLogInsertDate = new System.Windows.Forms.Label();
			this.tbActionLogMemberID = new System.Windows.Forms.TextBox();
			this.tbActionLogEmailAddress = new System.Windows.Forms.TextBox();
			this.tbActionLogActionDescription = new System.Windows.Forms.TextBox();
			this.tbActionLogInsertDate = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.PublishIDTextBox = new System.Windows.Forms.TextBox();
			this.PublishIDLabel = new System.Windows.Forms.Label();
			this.GetPublishesButton = new System.Windows.Forms.Button();
			this.PublishesTextBox = new System.Windows.Forms.TextBox();
			this.StartDateLabel = new System.Windows.Forms.Label();
			this.EndDateLabel = new System.Windows.Forms.Label();
			this.StartDatePicker = new System.Windows.Forms.DateTimePicker();
			this.EndDatePicker = new System.Windows.Forms.DateTimePicker();
			this.SavePublishButton = new System.Windows.Forms.Button();
			this.ObjectIDTextBox = new System.Windows.Forms.TextBox();
			this.ObjectIDLabel = new System.Windows.Forms.Label();
			this.SavePublishActionButton = new System.Windows.Forms.Button();
			this.GetNextPublishActionPublishObjectButton = new System.Windows.Forms.Button();
			this.GetLandingPages = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnAdminActionLogs
			// 
			this.btnAdminActionLogs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnAdminActionLogs.Location = new System.Drawing.Point(120, 120);
			this.btnAdminActionLogs.Name = "btnAdminActionLogs";
			this.btnAdminActionLogs.Size = new System.Drawing.Size(116, 23);
			this.btnAdminActionLogs.TabIndex = 0;
			this.btnAdminActionLogs.Text = "Admin Action Logs";
			this.btnAdminActionLogs.Click += new System.EventHandler(this.btnAdminActionLogs_Click);
			// 
			// cbServiceLevel
			// 
			this.cbServiceLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbServiceLevel.Location = new System.Drawing.Point(120, 16);
			this.cbServiceLevel.Name = "cbServiceLevel";
			this.cbServiceLevel.Size = new System.Drawing.Size(152, 21);
			this.cbServiceLevel.TabIndex = 1;
			// 
			// tbAdminMemberID
			// 
			this.tbAdminMemberID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbAdminMemberID.Location = new System.Drawing.Point(120, 48);
			this.tbAdminMemberID.Name = "tbAdminMemberID";
			this.tbAdminMemberID.Size = new System.Drawing.Size(148, 20);
			this.tbAdminMemberID.TabIndex = 2;
			this.tbAdminMemberID.Text = "988268688";
			// 
			// tbCommunityID
			// 
			this.tbCommunityID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbCommunityID.Location = new System.Drawing.Point(120, 80);
			this.tbCommunityID.Name = "tbCommunityID";
			this.tbCommunityID.Size = new System.Drawing.Size(148, 20);
			this.tbCommunityID.TabIndex = 3;
			this.tbCommunityID.Text = "1";
			// 
			// lblAdminMemberID
			// 
			this.lblAdminMemberID.Location = new System.Drawing.Point(16, 52);
			this.lblAdminMemberID.Name = "lblAdminMemberID";
			this.lblAdminMemberID.Size = new System.Drawing.Size(96, 12);
			this.lblAdminMemberID.TabIndex = 4;
			this.lblAdminMemberID.Text = "Admin Member ID";
			// 
			// lblCommunityID
			// 
			this.lblCommunityID.Location = new System.Drawing.Point(36, 84);
			this.lblCommunityID.Name = "lblCommunityID";
			this.lblCommunityID.Size = new System.Drawing.Size(76, 12);
			this.lblCommunityID.TabIndex = 5;
			this.lblCommunityID.Text = "Community ID";
			// 
			// lblServiceLevel
			// 
			this.lblServiceLevel.Location = new System.Drawing.Point(40, 20);
			this.lblServiceLevel.Name = "lblServiceLevel";
			this.lblServiceLevel.Size = new System.Drawing.Size(72, 12);
			this.lblServiceLevel.TabIndex = 6;
			this.lblServiceLevel.Text = "Service Level";
			// 
			// lbActionLogs
			// 
			this.lbActionLogs.Location = new System.Drawing.Point(384, 16);
			this.lbActionLogs.Name = "lbActionLogs";
			this.lbActionLogs.Size = new System.Drawing.Size(272, 82);
			this.lbActionLogs.TabIndex = 7;
			this.lbActionLogs.SelectedIndexChanged += new System.EventHandler(this.lbActionLogs_SelectedIndexChanged);
			// 
			// lblActionLogs
			// 
			this.lblActionLogs.Location = new System.Drawing.Point(316, 20);
			this.lblActionLogs.Name = "lblActionLogs";
			this.lblActionLogs.Size = new System.Drawing.Size(64, 12);
			this.lblActionLogs.TabIndex = 8;
			this.lblActionLogs.Text = "Action Logs";
			// 
			// lbActionLogMemberID
			// 
			this.lbActionLogMemberID.Location = new System.Drawing.Point(308, 112);
			this.lbActionLogMemberID.Name = "lbActionLogMemberID";
			this.lbActionLogMemberID.Size = new System.Drawing.Size(60, 12);
			this.lbActionLogMemberID.TabIndex = 9;
			this.lbActionLogMemberID.Text = "Member ID";
			// 
			// lblActionLogEmailAddress
			// 
			this.lblActionLogEmailAddress.Location = new System.Drawing.Point(288, 136);
			this.lblActionLogEmailAddress.Name = "lblActionLogEmailAddress";
			this.lblActionLogEmailAddress.Size = new System.Drawing.Size(80, 12);
			this.lblActionLogEmailAddress.TabIndex = 10;
			this.lblActionLogEmailAddress.Text = "Email Address";
			// 
			// lblActionLogActionDescription
			// 
			this.lblActionLogActionDescription.Location = new System.Drawing.Point(272, 160);
			this.lblActionLogActionDescription.Name = "lblActionLogActionDescription";
			this.lblActionLogActionDescription.Size = new System.Drawing.Size(96, 12);
			this.lblActionLogActionDescription.TabIndex = 11;
			this.lblActionLogActionDescription.Text = "Action Description";
			// 
			// lbActionLogInsertDate
			// 
			this.lbActionLogInsertDate.Location = new System.Drawing.Point(308, 188);
			this.lbActionLogInsertDate.Name = "lbActionLogInsertDate";
			this.lbActionLogInsertDate.Size = new System.Drawing.Size(60, 12);
			this.lbActionLogInsertDate.TabIndex = 12;
			this.lbActionLogInsertDate.Text = "Insert Date";
			// 
			// tbActionLogMemberID
			// 
			this.tbActionLogMemberID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbActionLogMemberID.Location = new System.Drawing.Point(384, 108);
			this.tbActionLogMemberID.Name = "tbActionLogMemberID";
			this.tbActionLogMemberID.ReadOnly = true;
			this.tbActionLogMemberID.Size = new System.Drawing.Size(272, 20);
			this.tbActionLogMemberID.TabIndex = 13;
			this.tbActionLogMemberID.Text = "";
			// 
			// tbActionLogEmailAddress
			// 
			this.tbActionLogEmailAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbActionLogEmailAddress.Location = new System.Drawing.Point(384, 132);
			this.tbActionLogEmailAddress.Name = "tbActionLogEmailAddress";
			this.tbActionLogEmailAddress.ReadOnly = true;
			this.tbActionLogEmailAddress.Size = new System.Drawing.Size(272, 20);
			this.tbActionLogEmailAddress.TabIndex = 14;
			this.tbActionLogEmailAddress.Text = "";
			// 
			// tbActionLogActionDescription
			// 
			this.tbActionLogActionDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbActionLogActionDescription.Location = new System.Drawing.Point(384, 156);
			this.tbActionLogActionDescription.Name = "tbActionLogActionDescription";
			this.tbActionLogActionDescription.ReadOnly = true;
			this.tbActionLogActionDescription.Size = new System.Drawing.Size(272, 20);
			this.tbActionLogActionDescription.TabIndex = 15;
			this.tbActionLogActionDescription.Text = "";
			// 
			// tbActionLogInsertDate
			// 
			this.tbActionLogInsertDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbActionLogInsertDate.Location = new System.Drawing.Point(384, 180);
			this.tbActionLogInsertDate.Name = "tbActionLogInsertDate";
			this.tbActionLogInsertDate.ReadOnly = true;
			this.tbActionLogInsertDate.Size = new System.Drawing.Size(272, 20);
			this.tbActionLogInsertDate.TabIndex = 16;
			this.tbActionLogInsertDate.Text = "";
			// 
			// button1
			// 
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Location = new System.Drawing.Point(120, 244);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(116, 23);
			this.button1.TabIndex = 17;
			this.button1.Text = "Insert Log";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button2.Location = new System.Drawing.Point(120, 288);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(116, 23);
			this.button2.TabIndex = 18;
			this.button2.Text = "SA INsert";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// PublishIDTextBox
			// 
			this.PublishIDTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.PublishIDTextBox.Location = new System.Drawing.Point(88, 404);
			this.PublishIDTextBox.Name = "PublishIDTextBox";
			this.PublishIDTextBox.Size = new System.Drawing.Size(116, 20);
			this.PublishIDTextBox.TabIndex = 20;
			this.PublishIDTextBox.Text = "";
			// 
			// PublishIDLabel
			// 
			this.PublishIDLabel.Location = new System.Drawing.Point(4, 408);
			this.PublishIDLabel.Name = "PublishIDLabel";
			this.PublishIDLabel.Size = new System.Drawing.Size(76, 12);
			this.PublishIDLabel.TabIndex = 21;
			this.PublishIDLabel.Text = "PublishID";
			this.PublishIDLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// GetPublishesButton
			// 
			this.GetPublishesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.GetPublishesButton.Location = new System.Drawing.Point(88, 488);
			this.GetPublishesButton.Name = "GetPublishesButton";
			this.GetPublishesButton.Size = new System.Drawing.Size(116, 23);
			this.GetPublishesButton.TabIndex = 19;
			this.GetPublishesButton.Text = "Get Publishes";
			this.GetPublishesButton.Click += new System.EventHandler(this.GetPublishesButton_Click);
			// 
			// PublishesTextBox
			// 
			this.PublishesTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.PublishesTextBox.Location = new System.Drawing.Point(296, 404);
			this.PublishesTextBox.Multiline = true;
			this.PublishesTextBox.Name = "PublishesTextBox";
			this.PublishesTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.PublishesTextBox.Size = new System.Drawing.Size(388, 108);
			this.PublishesTextBox.TabIndex = 22;
			this.PublishesTextBox.Text = "";
			// 
			// StartDateLabel
			// 
			this.StartDateLabel.Location = new System.Drawing.Point(4, 440);
			this.StartDateLabel.Name = "StartDateLabel";
			this.StartDateLabel.Size = new System.Drawing.Size(76, 12);
			this.StartDateLabel.TabIndex = 24;
			this.StartDateLabel.Text = "StartDate";
			this.StartDateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// EndDateLabel
			// 
			this.EndDateLabel.Location = new System.Drawing.Point(4, 464);
			this.EndDateLabel.Name = "EndDateLabel";
			this.EndDateLabel.Size = new System.Drawing.Size(76, 12);
			this.EndDateLabel.TabIndex = 26;
			this.EndDateLabel.Text = "EndDate";
			this.EndDateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// StartDatePicker
			// 
			this.StartDatePicker.Location = new System.Drawing.Point(88, 436);
			this.StartDatePicker.Name = "StartDatePicker";
			this.StartDatePicker.TabIndex = 27;
			this.StartDatePicker.Value = new System.DateTime(2000, 2, 12, 0, 0, 0, 0);
			// 
			// EndDatePicker
			// 
			this.EndDatePicker.Location = new System.Drawing.Point(88, 460);
			this.EndDatePicker.Name = "EndDatePicker";
			this.EndDatePicker.TabIndex = 28;
			// 
			// SavePublishButton
			// 
			this.SavePublishButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.SavePublishButton.Location = new System.Drawing.Point(88, 568);
			this.SavePublishButton.Name = "SavePublishButton";
			this.SavePublishButton.Size = new System.Drawing.Size(116, 23);
			this.SavePublishButton.TabIndex = 29;
			this.SavePublishButton.Text = "Save Publish";
			this.SavePublishButton.Click += new System.EventHandler(this.SavePublishButton_Click);
			// 
			// ObjectIDTextBox
			// 
			this.ObjectIDTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.ObjectIDTextBox.Location = new System.Drawing.Point(88, 544);
			this.ObjectIDTextBox.Name = "ObjectIDTextBox";
			this.ObjectIDTextBox.Size = new System.Drawing.Size(116, 20);
			this.ObjectIDTextBox.TabIndex = 30;
			this.ObjectIDTextBox.Text = "";
			// 
			// ObjectIDLabel
			// 
			this.ObjectIDLabel.Location = new System.Drawing.Point(4, 548);
			this.ObjectIDLabel.Name = "ObjectIDLabel";
			this.ObjectIDLabel.Size = new System.Drawing.Size(76, 12);
			this.ObjectIDLabel.TabIndex = 31;
			this.ObjectIDLabel.Text = "ObjectID";
			this.ObjectIDLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// SavePublishActionButton
			// 
			this.SavePublishActionButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.SavePublishActionButton.Location = new System.Drawing.Point(208, 568);
			this.SavePublishActionButton.Name = "SavePublishActionButton";
			this.SavePublishActionButton.Size = new System.Drawing.Size(116, 23);
			this.SavePublishActionButton.TabIndex = 32;
			this.SavePublishActionButton.Text = "Save PublishAction";
			this.SavePublishActionButton.Click += new System.EventHandler(this.SavePublishActionButton_Click);
			// 
			// GetNextPublishActionPublishObjectButton
			// 
			this.GetNextPublishActionPublishObjectButton.Location = new System.Drawing.Point(480, 376);
			this.GetNextPublishActionPublishObjectButton.Name = "GetNextPublishActionPublishObjectButton";
			this.GetNextPublishActionPublishObjectButton.Size = new System.Drawing.Size(204, 23);
			this.GetNextPublishActionPublishObjectButton.TabIndex = 33;
			this.GetNextPublishActionPublishObjectButton.Text = "GetNextPublishActionPublishObject";
			this.GetNextPublishActionPublishObjectButton.Click += new System.EventHandler(this.GetNextPublishActionPublishObjectButton_Click);
			// 
			// GetLandingPages
			// 
			this.GetLandingPages.Location = new System.Drawing.Point(548, 584);
			this.GetLandingPages.Name = "GetLandingPages";
			this.GetLandingPages.Size = new System.Drawing.Size(136, 23);
			this.GetLandingPages.TabIndex = 34;
			this.GetLandingPages.Text = "Test GetLandingPages";
			this.GetLandingPages.Click += new System.EventHandler(this.GetLandingPages_Click);
			// 
			// AdminHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(696, 617);
			this.Controls.Add(this.GetLandingPages);
			this.Controls.Add(this.GetNextPublishActionPublishObjectButton);
			this.Controls.Add(this.SavePublishActionButton);
			this.Controls.Add(this.ObjectIDLabel);
			this.Controls.Add(this.ObjectIDTextBox);
			this.Controls.Add(this.SavePublishButton);
			this.Controls.Add(this.EndDatePicker);
			this.Controls.Add(this.StartDatePicker);
			this.Controls.Add(this.EndDateLabel);
			this.Controls.Add(this.StartDateLabel);
			this.Controls.Add(this.PublishesTextBox);
			this.Controls.Add(this.PublishIDTextBox);
			this.Controls.Add(this.PublishIDLabel);
			this.Controls.Add(this.GetPublishesButton);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.tbActionLogInsertDate);
			this.Controls.Add(this.tbActionLogActionDescription);
			this.Controls.Add(this.tbActionLogEmailAddress);
			this.Controls.Add(this.tbActionLogMemberID);
			this.Controls.Add(this.tbCommunityID);
			this.Controls.Add(this.tbAdminMemberID);
			this.Controls.Add(this.lbActionLogInsertDate);
			this.Controls.Add(this.lblActionLogActionDescription);
			this.Controls.Add(this.lblActionLogEmailAddress);
			this.Controls.Add(this.lbActionLogMemberID);
			this.Controls.Add(this.lblActionLogs);
			this.Controls.Add(this.lbActionLogs);
			this.Controls.Add(this.lblServiceLevel);
			this.Controls.Add(this.lblCommunityID);
			this.Controls.Add(this.lblAdminMemberID);
			this.Controls.Add(this.cbServiceLevel);
			this.Controls.Add(this.btnAdminActionLogs);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "AdminHarness";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "AdminHarness";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.ResumeLayout(false);

		}
		#endregion

		private void populateAdminLogs(AdminActionLogCollection oResult) 
		{
			lbActionLogs.Items.Clear();
			lbActionLogs.DisplayMember = "ActionDescription";
			foreach(AdminActionLog currentLog in oResult) 
			{
				lbActionLogs.Items.Add(currentLog);
			}
		}

		private void btnAdminActionLogs_Click(object sender, System.EventArgs e)
		{
			AdminActionLogCollection oResult = null;
			int adminID = getAdminMemberID();
			int communityID = getCommunityID();


			clearDetails();

			try
			{
				if(adminID > -1 && communityID > -1) 
				{
					testLevel = getSelectedLevel();

					switch(testLevel) 
					{
						case SERVICE_LEVEL.BUSINESS_LOGIC:
							oResult = AdminBL.Instance.GetAdminActionLog(adminID,communityID);
							break;
						case SERVICE_LEVEL.SERVICE_MANAGER:
							AdminSM adminServiceManager = new AdminSM();
							oResult = adminServiceManager.GetAdminActionLog(adminID,communityID);
							break;
						case SERVICE_LEVEL.SERVICE_ADAPTER:
							oResult = AdminSA.Instance.GetAdminActionLog(adminID,communityID);
							break;
						default:
							oResult = AdminBL.Instance.GetAdminActionLog(adminID,communityID);
							break;
					}

					populateAdminLogs(oResult);
				} 
				else 
				{
					MessageBox.Show("Please provide valid values for Admin and Community IDs");
				}
			} 
			catch(Exception ex) 
			{
				MessageBox.Show("Error Retrieving Admin Action Logs: " + ex.StackTrace);
			}
		}

		private int getAdminMemberID()
		{
			int adminID = -1;

			try
			{
				if(tbAdminMemberID.Text.Trim().Length > 0) 
				{
					adminID = int.Parse(tbAdminMemberID.Text.Trim());
				}
			}
			catch(Exception) 
			{
				return adminID;
			}
			return adminID;
		}

		private int getCommunityID()
		{
			int communityID = -1;

			try
			{
				if(tbCommunityID.Text.Trim().Length > 0) 
				{
					communityID = int.Parse(tbCommunityID.Text.Trim());
				}
			}
			catch(Exception) 
			{
				return communityID;
			}
			return communityID;
		}

		private SERVICE_LEVEL getSelectedLevel() 
		{
			return (SERVICE_LEVEL)Enum.Parse(typeof(SERVICE_LEVEL), cbServiceLevel.SelectedItem.ToString());
		}

		private void lbActionLogs_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			AdminActionLog selectedLog = null;

			clearDetails();

			if(lbActionLogs.SelectedItem != null) 
			{
				selectedLog = (AdminActionLog)lbActionLogs.SelectedItem;
				tbActionLogEmailAddress.Text = selectedLog.EmailAddress;
				tbActionLogMemberID.Text = selectedLog.MemberID.ToString();
				tbActionLogActionDescription.Text = selectedLog.ActionDescription;
				tbActionLogInsertDate.Text = selectedLog.InsertDate.ToLongDateString();
			}
		}

		private void clearDetails() 
		{
			tbActionLogEmailAddress.Text = "";
			tbActionLogMemberID.Text = "";
			tbActionLogActionDescription.Text = "";
			tbActionLogInsertDate.Text = "";
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			// this works
		//	AdminBL.Instance.AdminActionLogInsert(16000206, 3, 1, 16000206, AdminMemberIDType.AdminProfileMemberID);

			// this works
//			AdminSM sm = new AdminSM();
//			sm.AdminActionLogInsert(16000206, 3, 1, 16000206, 2);

			//AdminSA.Instance.AdminActionLogInsert(100766, 3, 1, 100766, 2);
			int langid = 3;
			AdminSA.Instance.AdminActionLogInsert(100766, 3, 1, 100766, langid);
			
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			AdminSA.Instance.UpdateAdminNote("SHIT!", 16000206, 16000206, 3, 1000);
		}

		private void GetPublishesButton_Click(object sender, System.EventArgs e)
		{
			PublishesTextBox.Text = String.Empty;

			testLevel = getSelectedLevel();
			
			// Get Publishes.
			if (PublishIDTextBox.Text == String.Empty)
			{
				PublishCollection publishCollection;
				switch(testLevel) 
				{
					case SERVICE_LEVEL.BUSINESS_LOGIC:
						publishCollection = AdminBL.Instance.GetPublishes(StartDatePicker.Value, EndDatePicker.Value);
						break;
//					case SERVICE_LEVEL.SERVICE_MANAGER:
//						AdminSM adminServiceManager = new AdminSM();
//						oResult = adminServiceManager.GetAdminActionLog(adminID,communityID);
//						break;
//					case SERVICE_LEVEL.SERVICE_ADAPTER:
//						oResult = AdminSA.Instance.GetAdminActionLog(adminID,communityID);
//						break;
					default:
						publishCollection = AdminBL.Instance.GetPublishes(StartDatePicker.Value, EndDatePicker.Value);
						break;
				}

				foreach (Publish publish in publishCollection.Values())
				{
					PublishesTextBox.Text += publish.PublishID.ToString() + "\r\n";
				}
			}
			else
			{
//				Publish publish;
//				switch(testLevel) 
//				{
//					case SERVICE_LEVEL.BUSINESS_LOGIC:
//						publish = AdminBL.Instance.GetPublish(Convert.ToInt32(PublishIDTextBox.Text));
//						break;
//					case SERVICE_LEVEL.SERVICE_MANAGER:
//						AdminSM adminServiceManager = new AdminSM();
//						publish = adminServiceManager.GetPublish(Convert.ToInt32(PublishIDTextBox.Text));
//						break;
//					case SERVICE_LEVEL.SERVICE_ADAPTER:
//						publish = AdminSA.Instance.GetPublish(Convert.ToInt32(PublishIDTextBox.Text));
//						break;
//					default:
//						publish = AdminBL.Instance.GetPublish(Convert.ToInt32(PublishIDTextBox.Text));
//						break;
//				}
//
//				foreach (PublishAction publishAction in publish.PublishActions.Values())
//				{
//					PublishesTextBox.Text += publishAction.WorkflowOrder.ToString() + "\r\n";
//				}
//
//				PublishesTextBox.Text += publish.Content.ToString() + "\r\n";
			}
		}

		private void SavePublishButton_Click(object sender, System.EventArgs e)
		{
//			PublishesTextBox.Text = String.Empty;
//
//			testLevel = getSelectedLevel();
//
//			testObject = ArticleBL.Instance.RetrieveSiteArticle(14, 109);
//
//			Int32 publishID;
//			switch(testLevel) 
//			{
//				case SERVICE_LEVEL.BUSINESS_LOGIC:
//					publishID = AdminBL.Instance.SavePublish(Convert.ToInt32(ObjectIDTextBox.Text), PublishObjectType.Pixel, testObject);
//					break;
//				case SERVICE_LEVEL.SERVICE_MANAGER:
//					AdminSM adminServiceManager = new AdminSM();
//					publishID = adminServiceManager.SavePublish(Convert.ToInt32(ObjectIDTextBox.Text), PublishObjectType.Pixel, testObject);
//					break;
//				case SERVICE_LEVEL.SERVICE_ADAPTER:
//					publishID = AdminSA.Instance.SavePublish(Convert.ToInt32(ObjectIDTextBox.Text), PublishObjectType.Pixel, testObject);
//					break;
//				default:
//					publishID = AdminBL.Instance.SavePublish(Convert.ToInt32(ObjectIDTextBox.Text), PublishObjectType.Pixel, testObject);
//					break;
//			}
//
//			PublishesTextBox.Text = publishID.ToString();
		}

		private void SavePublishActionButton_Click(object sender, System.EventArgs e)
		{
		
		}

		private void GetNextPublishActionPublishObjectButton_Click(object sender, System.EventArgs e)
		{
			AdminSM adminServiceManager = null;

			Int32 publishObjectID = 100000861;
			PublishObjectType publishObjectType = PublishObjectType.SiteArticle;
			Int32 publishActionPublishObjectID;
			ServiceEnvironmentTypeEnum nextServiceEnvironmentTypeEnum;

			testLevel = getSelectedLevel();

			switch(testLevel) 
			{
				case SERVICE_LEVEL.BUSINESS_LOGIC:
					AdminBL.Instance.GetNextPublishActionPublishObjectStep(publishObjectID, publishObjectType, out publishActionPublishObjectID, out nextServiceEnvironmentTypeEnum);
					break;
				case SERVICE_LEVEL.SERVICE_MANAGER:
					adminServiceManager = new AdminSM();
					adminServiceManager.GetNextPublishActionPublishObjectStep(publishObjectID, publishObjectType, out publishActionPublishObjectID, out nextServiceEnvironmentTypeEnum);
					break;
				case SERVICE_LEVEL.SERVICE_ADAPTER:
					AdminSA.Instance.GetNextPublishActionPublishObjectStep(publishObjectID, publishObjectType, out publishActionPublishObjectID, out nextServiceEnvironmentTypeEnum);
					break;
				default:
					AdminBL.Instance.GetNextPublishActionPublishObjectStep(publishObjectID, publishObjectType, out publishActionPublishObjectID, out nextServiceEnvironmentTypeEnum);
					break;
			}

			MessageBox.Show("publishActionPublishObjectID: " + publishActionPublishObjectID.ToString()
				+ "nextServiceEnvironmentTypeEnum: " + nextServiceEnvironmentTypeEnum.ToString());
		}

		private void GetLandingPages_Click(object sender, System.EventArgs e)
		{
//			LandingPageCollection landingPageCollection;
//			try
//			{
////				LandingPage lp = new LandingPage(1000, "test Landing Page", "this is a test of landing page serialization", "test control name"
////					, "http://www.yahoo.com", true, 10000000, true, DateTime.Now, 1000000);
////
////				Byte[] bytes = lp.ToByteArray();
////
////				LandingPage lp2 = new LandingPage();
////
////				lp2.FromByteArray(bytes);
//
//				landingPageCollection = LandingPageBL.Instance.GetLandingPages();
//				//landingPageCollection = LandingPageSA.Instance.GetLandingPages();
//
//				//LandingPageSM landingPageSM = new LandingPageSM();
//				//landingPageCollection = landingPageSM.GetLandingPages();
//
//				MessageBox.Show(landingPageCollection.Count.ToString());
//			}
//			catch (Exception ex)
//			{
//				String msg = ex.Message;
//
//				MessageBox.Show(msg);
//			}
		}
	}
}
