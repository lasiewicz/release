using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceManagers;
using Matchnet.Content.ValueObjects.Region;

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Matchnet.Content.Harness
{
	/// <summary>
	/// Summary description for RegionHarness.
	/// </summary>
	public class RegionHarness : System.Windows.Forms.Form
	{

		#region enumerations
		private enum SERVICE_LEVEL 
		{
			SERVICE_ADAPTER = 0,
			SERVICE_MANAGER = 1,
			BUSINESS_LOGIC = 2
		}
		#endregion

		#region class variables
		private SERVICE_LEVEL testLevel = SERVICE_LEVEL.BUSINESS_LOGIC;
		#endregion
		private System.Windows.Forms.ComboBox cbServiceLevel;
		private System.Windows.Forms.Label lblRegionID;
		private System.Windows.Forms.Label lblLanguageID;
		private System.Windows.Forms.TextBox tbRegionID;
		private System.Windows.Forms.TextBox tbLanguageID;
		private System.Windows.Forms.Button btnRegion;
		private System.Windows.Forms.TextBox tbRegionDepth;
		private System.Windows.Forms.Label lblRegionDepth;
		private System.Windows.Forms.TextBox tbRegionLatitude;
		private System.Windows.Forms.Label lblRegionLatitude;
		private System.Windows.Forms.TextBox tbRegionLongitude;
		private System.Windows.Forms.Label lblRegionLongitude;
		private System.Windows.Forms.TextBox tbRegionDescription;
		private System.Windows.Forms.Label lblRegionDescription;
		private System.Windows.Forms.TextBox tbRegionAbbreviation;
		private System.Windows.Forms.Label lblRegionAbbreviation;
		private System.Windows.Forms.Button btnLanguage;
		private System.Windows.Forms.ListBox lbLanguage;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button GetAreaCodesBtn;
		private System.Windows.Forms.TextBox txtSEORegion;
		private System.Windows.Forms.Label lblRegion;
		private System.Windows.Forms.Button btnSEOStates;
		private System.Windows.Forms.Button btnSEOUSCities;
		private System.Windows.Forms.GroupBox grpSEO;
		private System.Windows.Forms.Button btnSEOStatesCities;
		private System.Windows.Forms.Button btnStateDefaultCity;
		private System.Windows.Forms.Label lblDefaultCity;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox txtZipRegionID;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnGetDMA;
		private System.Windows.Forms.Label lblDMA;
		private System.Windows.Forms.Label label2;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public RegionHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			PopulateServiceLevel();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cbServiceLevel = new System.Windows.Forms.ComboBox();
			this.lblRegionID = new System.Windows.Forms.Label();
			this.lblLanguageID = new System.Windows.Forms.Label();
			this.tbRegionID = new System.Windows.Forms.TextBox();
			this.tbLanguageID = new System.Windows.Forms.TextBox();
			this.btnRegion = new System.Windows.Forms.Button();
			this.tbRegionDepth = new System.Windows.Forms.TextBox();
			this.lblRegionDepth = new System.Windows.Forms.Label();
			this.tbRegionLatitude = new System.Windows.Forms.TextBox();
			this.lblRegionLatitude = new System.Windows.Forms.Label();
			this.tbRegionLongitude = new System.Windows.Forms.TextBox();
			this.lblRegionLongitude = new System.Windows.Forms.Label();
			this.tbRegionDescription = new System.Windows.Forms.TextBox();
			this.lblRegionDescription = new System.Windows.Forms.Label();
			this.tbRegionAbbreviation = new System.Windows.Forms.TextBox();
			this.lblRegionAbbreviation = new System.Windows.Forms.Label();
			this.btnLanguage = new System.Windows.Forms.Button();
			this.lbLanguage = new System.Windows.Forms.ListBox();
			this.button1 = new System.Windows.Forms.Button();
			this.GetAreaCodesBtn = new System.Windows.Forms.Button();
			this.grpSEO = new System.Windows.Forms.GroupBox();
			this.lblDefaultCity = new System.Windows.Forms.Label();
			this.btnStateDefaultCity = new System.Windows.Forms.Button();
			this.btnSEOStatesCities = new System.Windows.Forms.Button();
			this.txtSEORegion = new System.Windows.Forms.TextBox();
			this.lblRegion = new System.Windows.Forms.Label();
			this.btnSEOStates = new System.Windows.Forms.Button();
			this.btnSEOUSCities = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.txtZipRegionID = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btnGetDMA = new System.Windows.Forms.Button();
			this.lblDMA = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.grpSEO.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// cbServiceLevel
			// 
			this.cbServiceLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbServiceLevel.Location = new System.Drawing.Point(12, 12);
			this.cbServiceLevel.Name = "cbServiceLevel";
			this.cbServiceLevel.Size = new System.Drawing.Size(204, 21);
			this.cbServiceLevel.TabIndex = 0;
			// 
			// lblRegionID
			// 
			this.lblRegionID.Location = new System.Drawing.Point(24, 44);
			this.lblRegionID.Name = "lblRegionID";
			this.lblRegionID.Size = new System.Drawing.Size(56, 16);
			this.lblRegionID.TabIndex = 1;
			this.lblRegionID.Text = "Region ID";
			// 
			// lblLanguageID
			// 
			this.lblLanguageID.Location = new System.Drawing.Point(12, 68);
			this.lblLanguageID.Name = "lblLanguageID";
			this.lblLanguageID.Size = new System.Drawing.Size(72, 16);
			this.lblLanguageID.TabIndex = 2;
			this.lblLanguageID.Text = "Language ID";
			// 
			// tbRegionID
			// 
			this.tbRegionID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbRegionID.Location = new System.Drawing.Point(88, 40);
			this.tbRegionID.Name = "tbRegionID";
			this.tbRegionID.Size = new System.Drawing.Size(128, 20);
			this.tbRegionID.TabIndex = 3;
			this.tbRegionID.Text = "";
			// 
			// tbLanguageID
			// 
			this.tbLanguageID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbLanguageID.Location = new System.Drawing.Point(88, 68);
			this.tbLanguageID.Name = "tbLanguageID";
			this.tbLanguageID.Size = new System.Drawing.Size(128, 20);
			this.tbLanguageID.TabIndex = 4;
			this.tbLanguageID.Text = "";
			// 
			// btnRegion
			// 
			this.btnRegion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnRegion.Location = new System.Drawing.Point(140, 96);
			this.btnRegion.Name = "btnRegion";
			this.btnRegion.TabIndex = 5;
			this.btnRegion.Text = "Region";
			this.btnRegion.Click += new System.EventHandler(this.btnRegion_Click);
			// 
			// tbRegionDepth
			// 
			this.tbRegionDepth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbRegionDepth.Location = new System.Drawing.Point(392, 24);
			this.tbRegionDepth.Name = "tbRegionDepth";
			this.tbRegionDepth.ReadOnly = true;
			this.tbRegionDepth.Size = new System.Drawing.Size(128, 20);
			this.tbRegionDepth.TabIndex = 7;
			this.tbRegionDepth.Text = "";
			// 
			// lblRegionDepth
			// 
			this.lblRegionDepth.Location = new System.Drawing.Point(348, 28);
			this.lblRegionDepth.Name = "lblRegionDepth";
			this.lblRegionDepth.Size = new System.Drawing.Size(36, 16);
			this.lblRegionDepth.TabIndex = 6;
			this.lblRegionDepth.Text = "Depth";
			// 
			// tbRegionLatitude
			// 
			this.tbRegionLatitude.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbRegionLatitude.Location = new System.Drawing.Point(392, 48);
			this.tbRegionLatitude.Name = "tbRegionLatitude";
			this.tbRegionLatitude.ReadOnly = true;
			this.tbRegionLatitude.Size = new System.Drawing.Size(128, 20);
			this.tbRegionLatitude.TabIndex = 9;
			this.tbRegionLatitude.Text = "";
			// 
			// lblRegionLatitude
			// 
			this.lblRegionLatitude.Location = new System.Drawing.Point(336, 52);
			this.lblRegionLatitude.Name = "lblRegionLatitude";
			this.lblRegionLatitude.Size = new System.Drawing.Size(48, 16);
			this.lblRegionLatitude.TabIndex = 8;
			this.lblRegionLatitude.Text = "Latitude";
			// 
			// tbRegionLongitude
			// 
			this.tbRegionLongitude.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbRegionLongitude.Location = new System.Drawing.Point(392, 72);
			this.tbRegionLongitude.Name = "tbRegionLongitude";
			this.tbRegionLongitude.ReadOnly = true;
			this.tbRegionLongitude.Size = new System.Drawing.Size(128, 20);
			this.tbRegionLongitude.TabIndex = 11;
			this.tbRegionLongitude.Text = "";
			// 
			// lblRegionLongitude
			// 
			this.lblRegionLongitude.Location = new System.Drawing.Point(328, 76);
			this.lblRegionLongitude.Name = "lblRegionLongitude";
			this.lblRegionLongitude.Size = new System.Drawing.Size(56, 16);
			this.lblRegionLongitude.TabIndex = 10;
			this.lblRegionLongitude.Text = "Longitude";
			// 
			// tbRegionDescription
			// 
			this.tbRegionDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbRegionDescription.Location = new System.Drawing.Point(392, 96);
			this.tbRegionDescription.Name = "tbRegionDescription";
			this.tbRegionDescription.ReadOnly = true;
			this.tbRegionDescription.Size = new System.Drawing.Size(128, 20);
			this.tbRegionDescription.TabIndex = 13;
			this.tbRegionDescription.Text = "";
			// 
			// lblRegionDescription
			// 
			this.lblRegionDescription.Location = new System.Drawing.Point(320, 100);
			this.lblRegionDescription.Name = "lblRegionDescription";
			this.lblRegionDescription.Size = new System.Drawing.Size(64, 16);
			this.lblRegionDescription.TabIndex = 12;
			this.lblRegionDescription.Text = "Description";
			// 
			// tbRegionAbbreviation
			// 
			this.tbRegionAbbreviation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbRegionAbbreviation.Location = new System.Drawing.Point(392, 120);
			this.tbRegionAbbreviation.Name = "tbRegionAbbreviation";
			this.tbRegionAbbreviation.ReadOnly = true;
			this.tbRegionAbbreviation.Size = new System.Drawing.Size(128, 20);
			this.tbRegionAbbreviation.TabIndex = 15;
			this.tbRegionAbbreviation.Text = "";
			// 
			// lblRegionAbbreviation
			// 
			this.lblRegionAbbreviation.Location = new System.Drawing.Point(316, 124);
			this.lblRegionAbbreviation.Name = "lblRegionAbbreviation";
			this.lblRegionAbbreviation.Size = new System.Drawing.Size(68, 16);
			this.lblRegionAbbreviation.TabIndex = 14;
			this.lblRegionAbbreviation.Text = "Abbreviation";
			// 
			// btnLanguage
			// 
			this.btnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnLanguage.Location = new System.Drawing.Point(28, 436);
			this.btnLanguage.Name = "btnLanguage";
			this.btnLanguage.TabIndex = 16;
			this.btnLanguage.Text = "Language";
			this.btnLanguage.Click += new System.EventHandler(this.btnLanguage_Click);
			// 
			// lbLanguage
			// 
			this.lbLanguage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lbLanguage.Location = new System.Drawing.Point(120, 436);
			this.lbLanguage.Name = "lbLanguage";
			this.lbLanguage.Size = new System.Drawing.Size(368, 132);
			this.lbLanguage.TabIndex = 17;
			// 
			// button1
			// 
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Location = new System.Drawing.Point(24, 588);
			this.button1.Name = "button1";
			this.button1.TabIndex = 18;
			this.button1.Text = "Language";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// GetAreaCodesBtn
			// 
			this.GetAreaCodesBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.GetAreaCodesBtn.Location = new System.Drawing.Point(404, 596);
			this.GetAreaCodesBtn.Name = "GetAreaCodesBtn";
			this.GetAreaCodesBtn.Size = new System.Drawing.Size(112, 23);
			this.GetAreaCodesBtn.TabIndex = 19;
			this.GetAreaCodesBtn.Text = "Get AreaCodes";
			this.GetAreaCodesBtn.Click += new System.EventHandler(this.GetAreaCodesBtn_Click);
			// 
			// grpSEO
			// 
			this.grpSEO.Controls.Add(this.lblDefaultCity);
			this.grpSEO.Controls.Add(this.btnStateDefaultCity);
			this.grpSEO.Controls.Add(this.btnSEOStatesCities);
			this.grpSEO.Controls.Add(this.txtSEORegion);
			this.grpSEO.Controls.Add(this.lblRegion);
			this.grpSEO.Controls.Add(this.btnSEOStates);
			this.grpSEO.Controls.Add(this.btnSEOUSCities);
			this.grpSEO.Location = new System.Drawing.Point(112, 168);
			this.grpSEO.Name = "grpSEO";
			this.grpSEO.Size = new System.Drawing.Size(412, 140);
			this.grpSEO.TabIndex = 24;
			this.grpSEO.TabStop = false;
			this.grpSEO.Text = "SEO Regions";
			// 
			// lblDefaultCity
			// 
			this.lblDefaultCity.Location = new System.Drawing.Point(356, 116);
			this.lblDefaultCity.Name = "lblDefaultCity";
			this.lblDefaultCity.Size = new System.Drawing.Size(48, 16);
			this.lblDefaultCity.TabIndex = 32;
			// 
			// btnStateDefaultCity
			// 
			this.btnStateDefaultCity.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnStateDefaultCity.Location = new System.Drawing.Point(232, 112);
			this.btnStateDefaultCity.Name = "btnStateDefaultCity";
			this.btnStateDefaultCity.Size = new System.Drawing.Size(120, 23);
			this.btnStateDefaultCity.TabIndex = 30;
			this.btnStateDefaultCity.Text = "State\'s default city ";
			this.btnStateDefaultCity.Click += new System.EventHandler(this.btnStateDefaultCity_Click);
			// 
			// btnSEOStatesCities
			// 
			this.btnSEOStatesCities.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSEOStatesCities.Location = new System.Drawing.Point(232, 84);
			this.btnSEOStatesCities.Name = "btnSEOStatesCities";
			this.btnSEOStatesCities.Size = new System.Drawing.Size(120, 23);
			this.btnSEOStatesCities.TabIndex = 28;
			this.btnSEOStatesCities.Text = "Get Cities in state";
			this.btnSEOStatesCities.Click += new System.EventHandler(this.btnSEOStatesCities_Click);
			// 
			// txtSEORegion
			// 
			this.txtSEORegion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtSEORegion.Location = new System.Drawing.Point(160, 100);
			this.txtSEORegion.Name = "txtSEORegion";
			this.txtSEORegion.Size = new System.Drawing.Size(66, 20);
			this.txtSEORegion.TabIndex = 27;
			this.txtSEORegion.Text = "";
			// 
			// lblRegion
			// 
			this.lblRegion.Location = new System.Drawing.Point(24, 104);
			this.lblRegion.Name = "lblRegion";
			this.lblRegion.Size = new System.Drawing.Size(132, 16);
			this.lblRegion.TabIndex = 26;
			this.lblRegion.Text = "Enter State\'s Region ID:";
			// 
			// btnSEOStates
			// 
			this.btnSEOStates.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSEOStates.Location = new System.Drawing.Point(232, 28);
			this.btnSEOStates.Name = "btnSEOStates";
			this.btnSEOStates.Size = new System.Drawing.Size(120, 23);
			this.btnSEOStates.TabIndex = 25;
			this.btnSEOStates.Text = "SEO US States";
			this.btnSEOStates.Click += new System.EventHandler(this.btnSEOStates_Click);
			// 
			// btnSEOUSCities
			// 
			this.btnSEOUSCities.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSEOUSCities.Location = new System.Drawing.Point(232, 56);
			this.btnSEOUSCities.Name = "btnSEOUSCities";
			this.btnSEOUSCities.Size = new System.Drawing.Size(120, 23);
			this.btnSEOUSCities.TabIndex = 24;
			this.btnSEOUSCities.Text = "SEO US Major cities";
			this.btnSEOUSCities.Click += new System.EventHandler(this.btnSEOUSCities_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.lblDMA);
			this.groupBox1.Controls.Add(this.btnGetDMA);
			this.groupBox1.Controls.Add(this.txtZipRegionID);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(88, 316);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(460, 104);
			this.groupBox1.TabIndex = 25;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "DMA";
			// 
			// txtZipRegionID
			// 
			this.txtZipRegionID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtZipRegionID.Location = new System.Drawing.Point(136, 40);
			this.txtZipRegionID.Name = "txtZipRegionID";
			this.txtZipRegionID.Size = new System.Drawing.Size(84, 20);
			this.txtZipRegionID.TabIndex = 5;
			this.txtZipRegionID.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(20, 44);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(112, 16);
			this.label1.TabIndex = 4;
			this.label1.Text = "Enter Zip Region ID:";
			// 
			// btnGetDMA
			// 
			this.btnGetDMA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnGetDMA.Location = new System.Drawing.Point(224, 40);
			this.btnGetDMA.Name = "btnGetDMA";
			this.btnGetDMA.Size = new System.Drawing.Size(60, 23);
			this.btnGetDMA.TabIndex = 26;
			this.btnGetDMA.Text = "GetDMA";
			this.btnGetDMA.Click += new System.EventHandler(this.btnGetDMA_Click);
			// 
			// lblDMA
			// 
			this.lblDMA.Location = new System.Drawing.Point(364, 44);
			this.lblDMA.Name = "lblDMA";
			this.lblDMA.Size = new System.Drawing.Size(56, 24);
			this.lblDMA.TabIndex = 33;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(292, 44);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(68, 24);
			this.label2.TabIndex = 34;
			this.label2.Text = "DMA Code=";
			// 
			// RegionHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(604, 633);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.grpSEO);
			this.Controls.Add(this.GetAreaCodesBtn);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.lbLanguage);
			this.Controls.Add(this.btnLanguage);
			this.Controls.Add(this.tbRegionAbbreviation);
			this.Controls.Add(this.tbRegionDescription);
			this.Controls.Add(this.tbRegionLongitude);
			this.Controls.Add(this.tbRegionLatitude);
			this.Controls.Add(this.tbRegionDepth);
			this.Controls.Add(this.tbLanguageID);
			this.Controls.Add(this.tbRegionID);
			this.Controls.Add(this.lblRegionAbbreviation);
			this.Controls.Add(this.lblRegionDescription);
			this.Controls.Add(this.lblRegionLongitude);
			this.Controls.Add(this.lblRegionLatitude);
			this.Controls.Add(this.lblRegionDepth);
			this.Controls.Add(this.btnRegion);
			this.Controls.Add(this.lblLanguageID);
			this.Controls.Add(this.lblRegionID);
			this.Controls.Add(this.cbServiceLevel);
			this.Name = "RegionHarness";
			this.Text = "RegionHarness";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.grpSEO.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		#region utilities
		private SERVICE_LEVEL getSelectedLevel() 
		{
			return (SERVICE_LEVEL)Enum.Parse(typeof(SERVICE_LEVEL), cbServiceLevel.SelectedItem.ToString());
		}

		private void PopulateServiceLevel() 
		{
			cbServiceLevel.Items.Clear();
			foreach(String currentLevel in Enum.GetNames(typeof(SERVICE_LEVEL))) 
			{
				cbServiceLevel.Items.Add(currentLevel);
			}
			cbServiceLevel.SelectedIndex = 0;
		}
		#endregion

		private void btnRegion_Click(object sender, System.EventArgs e)
		{
			int regionID = 0;
			int languageID = 0;
			RegionSM regionServiceManager = null;
			Matchnet.Content.ValueObjects.Region.Region result = null;

			try 
			{
				testLevel = getSelectedLevel();

				regionID = int.Parse(tbRegionID.Text.Trim());
				languageID = int.Parse(tbLanguageID.Text.Trim());
				switch(testLevel) 
				{
					case SERVICE_LEVEL.BUSINESS_LOGIC:
						RegionLanguage results = RegionBL.Instance.RetrievePopulatedHierarchy(regionID, languageID);
						break;
					case SERVICE_LEVEL.SERVICE_MANAGER:
						regionServiceManager = new RegionSM();
						result = regionServiceManager.RetrieveRegionByID(regionID, languageID);
						break;
					case SERVICE_LEVEL.SERVICE_ADAPTER:
						result = RegionSA.Instance.RetrieveRegionByID(regionID, languageID);
						break;
					default:
						result = RegionBL.Instance.RetrieveRegionByID(regionID, languageID);
						break;
				}

				DisplayRegionResult(result);
			} 
			catch(FormatException) 
			{
				MessageBox.Show("Invalid Region or Language ID");
			}
			catch(Exception ex) 
			{
				MessageBox.Show("Error Encountered: " + ex.Message);
			}
		}

		private void DisplayRegionResult(Matchnet.Content.ValueObjects.Region.Region pResult) 
		{
			tbRegionAbbreviation.Text = "";
			tbRegionDepth.Text = "";
			tbRegionDescription.Text = "";
			tbRegionLatitude.Text = "";
			tbRegionLongitude.Text = "";

			if(pResult != null) 
			{
				tbRegionAbbreviation.Text = pResult.Abbreviation;
				tbRegionDepth.Text = pResult.Depth.ToString();
				tbRegionDescription.Text = pResult.Description;
				tbRegionLatitude.Text = pResult.Latitude.ToString();
				tbRegionLongitude.Text = pResult.Longitude.ToString();
			}
		}

		private void btnLanguage_Click(object sender, System.EventArgs e)
		{
			RegionSM regionServiceManager = null;
			LanguageCollection result = null;

			try 
			{
				testLevel = getSelectedLevel();

				switch(testLevel) 
				{
					case SERVICE_LEVEL.BUSINESS_LOGIC:
						result = RegionBL.Instance.GetLanguages();
						break;
					case SERVICE_LEVEL.SERVICE_MANAGER:
						regionServiceManager = new RegionSM();
						result = regionServiceManager.GetLanguages();
						break;
					case SERVICE_LEVEL.SERVICE_ADAPTER:
						result = RegionSA.Instance.GetLanguages();
						break;
					default:
						result = RegionBL.Instance.GetLanguages();
						break;
				}

				DisplayLanguageResult(result);
			} 
			catch(Exception ex) 
			{
				MessageBox.Show("Error Encountered: " + ex.Message);
			}
		}

		private void DisplayLanguageResult(LanguageCollection pLanguageCollection) 
		{
			lbLanguage.Items.Clear();

			if(pLanguageCollection != null) 
			{
				lbLanguage.DisplayMember = "Description";

				foreach(Language currentLanguage in pLanguageCollection) 
				{
					lbLanguage.Items.Add(currentLanguage);
				}
			}
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			RegionID temp = RegionSA.Instance.FindRegionIdByCity(3485884, "Ban Lec", 2);
			temp = null;
		}

		private void GetAreaCodesBtn_Click(object sender, System.EventArgs e)
		{
			Object notUsed = RegionSA.Instance.IsValidAreaCodes(new Int32[]{999,213,444}, 223);
		}

		private void btnSEOUSCities_Click(object sender, System.EventArgs e)
		{
			DisplaySEORegionResult(RegionSA.Instance.GetSEOMajorUSCities());
		
		}
		private void btnSEOStates_Click(object sender, System.EventArgs e)
		{
			DisplaySEORegionResult(RegionSA.Instance.GetSEOUSStates());
		}

		private void btnSEOStatesCities_Click(object sender, System.EventArgs e)
		{
			if (txtSEORegion.Text.Length>0)
			{
				DisplaySEORegionResult(RegionSA.Instance.GetSEOCitiesInState(Int32.Parse(txtSEORegion.Text)));
			}
		}

		private void DisplaySEORegionResult(SEORegionCollection SEOUSCities) 
		{
			lbLanguage.Items.Clear();

			if(SEOUSCities != null) 
			{
				lbLanguage.DisplayMember = "Description";

				foreach(SEORegion seoCity in SEOUSCities) 
				{
					lbLanguage.Items.Add(seoCity);
				}
			}
		}

		private void btnStateDefaultCity_Click(object sender, System.EventArgs e)
		{
			if (txtSEORegion.Text.Length>0)
			{
				int result=RegionSA.Instance.GetSEOStatesPopularCity(Int32.Parse(txtSEORegion.Text));
				lblDefaultCity.Text=result.ToString();
			}
		}

		private void btnGetDMA_Click(object sender, System.EventArgs e)
		{
			if (txtZipRegionID.Text.Length>0)
			{
				DMA result= RegionSA.Instance.GetDMAByZipRegionID(Int32.Parse(txtZipRegionID.Text));
				lblDMA.Text=result.DMAID.ToString();
			}
		}

		

		
	}
}
