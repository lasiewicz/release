using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PageConfig;

namespace Matchnet.Content.Harness
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.ComboBox cbCommunities;
		private System.Windows.Forms.Label lblCommunities;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.cbCommunities = new System.Windows.Forms.ComboBox();
			this.lblCommunities = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(8, 8);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			this.button1.Text = "button1";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// cbCommunities
			// 
			this.cbCommunities.Location = new System.Drawing.Point(120, 56);
			this.cbCommunities.Name = "cbCommunities";
			this.cbCommunities.Size = new System.Drawing.Size(104, 21);
			this.cbCommunities.TabIndex = 1;
			// 
			// lblCommunities
			// 
			this.lblCommunities.Location = new System.Drawing.Point(16, 56);
			this.lblCommunities.Name = "lblCommunities";
			this.lblCommunities.Size = new System.Drawing.Size(88, 24);
			this.lblCommunities.TabIndex = 2;
			this.lblCommunities.Text = "Communities";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.lblCommunities);
			this.Controls.Add(this.cbCommunities);
			this.Controls.Add(this.button1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void button1_Click(object sender, System.EventArgs e)
		{
			Page page = PageConfigSA.Instance.GetPage(1, "/Applications/Home/Home.aspx");
			Console.WriteLine(page == null);
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			cbCommunities.DataSource = BrandConfigSA.Instance.GetCommunities();
			cbCommunities.DisplayMember = "Name";
			cbCommunities.ValueMember = "CommunityID";
		}
	}
}
