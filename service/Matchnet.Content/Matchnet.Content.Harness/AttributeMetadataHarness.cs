
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeOption;

namespace Matchnet.Content.Harness
{
	/// <summary>
	/// Summary description for AttributeOptionHarness.
	/// </summary>
	public class AttributeMetadataHarness : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnGetAttributeOptions;
		private System.Windows.Forms.TextBox tbAttributeID;
		private System.Windows.Forms.TextBox tbOutput;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public AttributeMetadataHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnGetAttributeOptions = new System.Windows.Forms.Button();
			this.tbAttributeID = new System.Windows.Forms.TextBox();
			this.tbOutput = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// btnGetAttributeOptions
			// 
			this.btnGetAttributeOptions.Location = new System.Drawing.Point(216, 208);
			this.btnGetAttributeOptions.Name = "btnGetAttributeOptions";
			this.btnGetAttributeOptions.Size = new System.Drawing.Size(48, 24);
			this.btnGetAttributeOptions.TabIndex = 0;
			this.btnGetAttributeOptions.Text = "Go";
			this.btnGetAttributeOptions.Click += new System.EventHandler(this.btnGetAttributeOptions_Click);
			// 
			// tbAttributeID
			// 
			this.tbAttributeID.Location = new System.Drawing.Point(40, 208);
			this.tbAttributeID.Name = "tbAttributeID";
			this.tbAttributeID.Size = new System.Drawing.Size(144, 20);
			this.tbAttributeID.TabIndex = 1;
			this.tbAttributeID.Text = "110";
			// 
			// tbOutput
			// 
			this.tbOutput.Location = new System.Drawing.Point(40, 24);
			this.tbOutput.Multiline = true;
			this.tbOutput.Name = "tbOutput";
			this.tbOutput.Size = new System.Drawing.Size(224, 112);
			this.tbOutput.TabIndex = 2;
			this.tbOutput.Text = "";
			// 
			// AttributeMetadataHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.tbOutput);
			this.Controls.Add(this.tbAttributeID);
			this.Controls.Add(this.btnGetAttributeOptions);
			this.Name = "AttributeMetadataHarness";
			this.Text = "AttributeOptionHarness";
			this.ResumeLayout(false);

		}
		#endregion

		private void btnGetAttributeOptions_Click(object sender, System.EventArgs e)
		{
			Matchnet.Content.ValueObjects.AttributeMetadata.Attributes attrs = Matchnet.Content.ServiceAdapters.AttributeMetadataSA.Instance.GetAttributes();

			ArrayList groupIDs = attrs.GetAttributeGroupIDs( Int32.Parse(this.tbAttributeID.Text));
			
			foreach( int groupid in groupIDs)
				this.tbOutput.Text += groupid + System.Environment.NewLine;
			//tbAttributeOptions.Text += attr.Name + ": " + attr.ID + ": " + attr.Scope + System.Environment.NewLine;

			
		}
	}
}
