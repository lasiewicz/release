using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceManagers;
using Matchnet.Content.ValueObjects.Article;

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;

namespace Matchnet.Content.Harness
{
	/// <summary>
	/// Summary description for ArticleHarness.
	/// </summary>
	public class ArticleHarness : System.Windows.Forms.Form
	{

		#region enumerations
		private enum SERVICE_LEVEL 
		{
			SERVICE_ADAPTER = 0,
			SERVICE_MANAGER = 1,
			BUSINESS_LOGIC = 2
		}
		#endregion

		private SERVICE_LEVEL testLevel = SERVICE_LEVEL.BUSINESS_LOGIC;
		private System.Windows.Forms.ComboBox cbServiceLevel;
		private System.Windows.Forms.Label lblServiceLevel;
		private System.Windows.Forms.TextBox tbxSiteID;
		private System.Windows.Forms.Button btnGetSiteCategoryChildren;
		private System.Windows.Forms.Label lblSiteID;
		private System.Windows.Forms.Label lblCategoryID;
		private System.Windows.Forms.Label lblOrdinal;
		private System.Windows.Forms.Button btnGetSiteArticles;
		private System.Windows.Forms.CheckBox cbxIsPublished;
		private System.Windows.Forms.CheckBox cbxIsFeatured;
		private System.Windows.Forms.Button btnGetSiteArticle;
		private System.Windows.Forms.TextBox tbxArticleID;
		private System.Windows.Forms.Label lblArticleID;
		private System.Windows.Forms.TextBox tbxOrdinal;
		private System.Windows.Forms.Label lblLastUpdated;
		private System.Windows.Forms.TextBox tbxLastUpdated;
		private System.Windows.Forms.TextBox tbxSiteArticleID;
		private System.Windows.Forms.Label lblSiteArticleID;
		private System.Windows.Forms.TextBox tbxMemberID;
		private System.Windows.Forms.Label lblMemberID;
		private System.Windows.Forms.TextBox tbxTitle;
		private System.Windows.Forms.Label lblTitle;
		private System.Windows.Forms.TextBox tbxFileID;
		private System.Windows.Forms.Label lblFileID;
		private System.Windows.Forms.TextBox tbxCategoryID;
		private System.Windows.Forms.CheckBox cbxForceLoad;
		private System.Windows.Forms.Button btnGetSiteCategories;
		private System.Windows.Forms.TextBox tbxParentCategoryID;
		private System.Windows.Forms.Label lblParentCategoryID;
		private System.Windows.Forms.TextBox tbxListOrder;
		private System.Windows.Forms.Label lblListOrder;
		private System.Windows.Forms.TextBox tbxConstant;
		private System.Windows.Forms.Label lblConstant;
		private System.Windows.Forms.Button btnGetCategory;
		private System.Windows.Forms.Button btnSaveCategory;
		private System.Windows.Forms.TextBox tbxSiteCategoryID;
		private System.Windows.Forms.Label lblSiteCategoryID;
		private System.Windows.Forms.Button btnSaveSiteCategory;
		private System.Windows.Forms.Label lblOutput;
		private System.Windows.Forms.TextBox tbxOutput;
		private System.Windows.Forms.Label lblContent;
		private System.Windows.Forms.TextBox tbxContent;
		private System.Windows.Forms.Button btnSaveSiteArticle;
		private System.Windows.Forms.Button btnAddArticle;
		private System.Windows.Forms.Button btnGetArticle;
		private System.Windows.Forms.Button btnGetSiteArticlesAndArticles;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ArticleHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			PopulateServiceLevel();
		}

		private void PopulateServiceLevel() 
		{
			cbServiceLevel.Items.Clear();
			foreach(String currentLevel in Enum.GetNames(typeof(SERVICE_LEVEL))) 
			{
				cbServiceLevel.Items.Add(currentLevel);
			}
			cbServiceLevel.SelectedIndex = 0;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cbServiceLevel = new System.Windows.Forms.ComboBox();
			this.lblServiceLevel = new System.Windows.Forms.Label();
			this.btnGetSiteCategoryChildren = new System.Windows.Forms.Button();
			this.tbxSiteID = new System.Windows.Forms.TextBox();
			this.lblOutput = new System.Windows.Forms.Label();
			this.tbxOutput = new System.Windows.Forms.TextBox();
			this.lblOrdinal = new System.Windows.Forms.Label();
			this.btnGetSiteArticles = new System.Windows.Forms.Button();
			this.cbxIsPublished = new System.Windows.Forms.CheckBox();
			this.cbxIsFeatured = new System.Windows.Forms.CheckBox();
			this.lblSiteID = new System.Windows.Forms.Label();
			this.btnGetSiteArticle = new System.Windows.Forms.Button();
			this.tbxCategoryID = new System.Windows.Forms.TextBox();
			this.lblCategoryID = new System.Windows.Forms.Label();
			this.tbxArticleID = new System.Windows.Forms.TextBox();
			this.lblArticleID = new System.Windows.Forms.Label();
			this.tbxOrdinal = new System.Windows.Forms.TextBox();
			this.lblLastUpdated = new System.Windows.Forms.Label();
			this.tbxLastUpdated = new System.Windows.Forms.TextBox();
			this.tbxSiteArticleID = new System.Windows.Forms.TextBox();
			this.lblSiteArticleID = new System.Windows.Forms.Label();
			this.tbxMemberID = new System.Windows.Forms.TextBox();
			this.lblMemberID = new System.Windows.Forms.Label();
			this.tbxTitle = new System.Windows.Forms.TextBox();
			this.lblTitle = new System.Windows.Forms.Label();
			this.tbxFileID = new System.Windows.Forms.TextBox();
			this.lblFileID = new System.Windows.Forms.Label();
			this.cbxForceLoad = new System.Windows.Forms.CheckBox();
			this.btnGetSiteCategories = new System.Windows.Forms.Button();
			this.tbxParentCategoryID = new System.Windows.Forms.TextBox();
			this.lblParentCategoryID = new System.Windows.Forms.Label();
			this.tbxListOrder = new System.Windows.Forms.TextBox();
			this.lblListOrder = new System.Windows.Forms.Label();
			this.tbxConstant = new System.Windows.Forms.TextBox();
			this.lblConstant = new System.Windows.Forms.Label();
			this.btnGetCategory = new System.Windows.Forms.Button();
			this.btnSaveCategory = new System.Windows.Forms.Button();
			this.tbxSiteCategoryID = new System.Windows.Forms.TextBox();
			this.lblSiteCategoryID = new System.Windows.Forms.Label();
			this.btnSaveSiteCategory = new System.Windows.Forms.Button();
			this.lblContent = new System.Windows.Forms.Label();
			this.tbxContent = new System.Windows.Forms.TextBox();
			this.btnSaveSiteArticle = new System.Windows.Forms.Button();
			this.btnAddArticle = new System.Windows.Forms.Button();
			this.btnGetArticle = new System.Windows.Forms.Button();
			this.btnGetSiteArticlesAndArticles = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// cbServiceLevel
			// 
			this.cbServiceLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbServiceLevel.Location = new System.Drawing.Point(124, 28);
			this.cbServiceLevel.Name = "cbServiceLevel";
			this.cbServiceLevel.Size = new System.Drawing.Size(121, 21);
			this.cbServiceLevel.TabIndex = 0;
			// 
			// lblServiceLevel
			// 
			this.lblServiceLevel.Location = new System.Drawing.Point(40, 32);
			this.lblServiceLevel.Name = "lblServiceLevel";
			this.lblServiceLevel.Size = new System.Drawing.Size(80, 16);
			this.lblServiceLevel.TabIndex = 1;
			this.lblServiceLevel.Text = "Service Level";
			this.lblServiceLevel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// btnGetSiteCategoryChildren
			// 
			this.btnGetSiteCategoryChildren.Location = new System.Drawing.Point(124, 196);
			this.btnGetSiteCategoryChildren.Name = "btnGetSiteCategoryChildren";
			this.btnGetSiteCategoryChildren.Size = new System.Drawing.Size(144, 28);
			this.btnGetSiteCategoryChildren.TabIndex = 50;
			this.btnGetSiteCategoryChildren.Text = "Get SiteCategoryChildren";
			this.btnGetSiteCategoryChildren.Click += new System.EventHandler(this.btnGetSiteCategoryChildren_Click);
			// 
			// tbxSiteID
			// 
			this.tbxSiteID.Location = new System.Drawing.Point(124, 60);
			this.tbxSiteID.Name = "tbxSiteID";
			this.tbxSiteID.Size = new System.Drawing.Size(120, 20);
			this.tbxSiteID.TabIndex = 50;
			this.tbxSiteID.Text = "";
			// 
			// lblOutput
			// 
			this.lblOutput.Location = new System.Drawing.Point(304, 80);
			this.lblOutput.Name = "lblOutput";
			this.lblOutput.Size = new System.Drawing.Size(48, 23);
			this.lblOutput.TabIndex = 51;
			this.lblOutput.Text = "Output";
			this.lblOutput.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxOutput
			// 
			this.tbxOutput.Location = new System.Drawing.Point(352, 8);
			this.tbxOutput.Multiline = true;
			this.tbxOutput.Name = "tbxOutput";
			this.tbxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.tbxOutput.Size = new System.Drawing.Size(276, 92);
			this.tbxOutput.TabIndex = 47;
			this.tbxOutput.Text = "";
			// 
			// lblOrdinal
			// 
			this.lblOrdinal.Location = new System.Drawing.Point(368, 512);
			this.lblOrdinal.Name = "lblOrdinal";
			this.lblOrdinal.Size = new System.Drawing.Size(56, 20);
			this.lblOrdinal.TabIndex = 21;
			this.lblOrdinal.Text = "Ordinal";
			this.lblOrdinal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// btnGetSiteArticles
			// 
			this.btnGetSiteArticles.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnGetSiteArticles.Location = new System.Drawing.Point(124, 228);
			this.btnGetSiteArticles.Name = "btnGetSiteArticles";
			this.btnGetSiteArticles.Size = new System.Drawing.Size(108, 23);
			this.btnGetSiteArticles.TabIndex = 45;
			this.btnGetSiteArticles.Text = "Get SiteArticles";
			this.btnGetSiteArticles.Click += new System.EventHandler(this.btnGetSiteArticles_Click);
			// 
			// cbxIsPublished
			// 
			this.cbxIsPublished.Location = new System.Drawing.Point(428, 468);
			this.cbxIsPublished.Name = "cbxIsPublished";
			this.cbxIsPublished.Size = new System.Drawing.Size(104, 16);
			this.cbxIsPublished.TabIndex = 44;
			this.cbxIsPublished.Text = "Is Published?";
			// 
			// cbxIsFeatured
			// 
			this.cbxIsFeatured.Location = new System.Drawing.Point(428, 536);
			this.cbxIsFeatured.Name = "cbxIsFeatured";
			this.cbxIsFeatured.Size = new System.Drawing.Size(104, 16);
			this.cbxIsFeatured.TabIndex = 43;
			this.cbxIsFeatured.Text = "Is Featured?";
			// 
			// lblSiteID
			// 
			this.lblSiteID.Location = new System.Drawing.Point(24, 60);
			this.lblSiteID.Name = "lblSiteID";
			this.lblSiteID.Size = new System.Drawing.Size(96, 20);
			this.lblSiteID.TabIndex = 36;
			this.lblSiteID.Text = "Site ID";
			this.lblSiteID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// btnGetSiteArticle
			// 
			this.btnGetSiteArticle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnGetSiteArticle.Location = new System.Drawing.Point(124, 472);
			this.btnGetSiteArticle.Name = "btnGetSiteArticle";
			this.btnGetSiteArticle.Size = new System.Drawing.Size(108, 23);
			this.btnGetSiteArticle.TabIndex = 17;
			this.btnGetSiteArticle.Text = "Get SiteArticle";
			this.btnGetSiteArticle.Click += new System.EventHandler(this.btnGetSiteArticle_Click);
			// 
			// tbxCategoryID
			// 
			this.tbxCategoryID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbxCategoryID.Location = new System.Drawing.Point(124, 172);
			this.tbxCategoryID.Name = "tbxCategoryID";
			this.tbxCategoryID.Size = new System.Drawing.Size(120, 20);
			this.tbxCategoryID.TabIndex = 44;
			this.tbxCategoryID.Text = "";
			// 
			// lblCategoryID
			// 
			this.lblCategoryID.Location = new System.Drawing.Point(52, 176);
			this.lblCategoryID.Name = "lblCategoryID";
			this.lblCategoryID.Size = new System.Drawing.Size(72, 16);
			this.lblCategoryID.TabIndex = 43;
			this.lblCategoryID.Text = "Category ID";
			this.lblCategoryID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxArticleID
			// 
			this.tbxArticleID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbxArticleID.Location = new System.Drawing.Point(124, 444);
			this.tbxArticleID.Name = "tbxArticleID";
			this.tbxArticleID.Size = new System.Drawing.Size(120, 20);
			this.tbxArticleID.TabIndex = 16;
			this.tbxArticleID.Text = "";
			// 
			// lblArticleID
			// 
			this.lblArticleID.Location = new System.Drawing.Point(68, 444);
			this.lblArticleID.Name = "lblArticleID";
			this.lblArticleID.Size = new System.Drawing.Size(52, 16);
			this.lblArticleID.TabIndex = 15;
			this.lblArticleID.Text = "Article ID";
			this.lblArticleID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxOrdinal
			// 
			this.tbxOrdinal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbxOrdinal.Location = new System.Drawing.Point(428, 512);
			this.tbxOrdinal.Name = "tbxOrdinal";
			this.tbxOrdinal.Size = new System.Drawing.Size(200, 20);
			this.tbxOrdinal.TabIndex = 24;
			this.tbxOrdinal.Text = "";
			// 
			// lblLastUpdated
			// 
			this.lblLastUpdated.Location = new System.Drawing.Point(320, 444);
			this.lblLastUpdated.Name = "lblLastUpdated";
			this.lblLastUpdated.Size = new System.Drawing.Size(104, 16);
			this.lblLastUpdated.TabIndex = 38;
			this.lblLastUpdated.Text = "Last Updated";
			this.lblLastUpdated.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxLastUpdated
			// 
			this.tbxLastUpdated.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbxLastUpdated.Location = new System.Drawing.Point(428, 440);
			this.tbxLastUpdated.Name = "tbxLastUpdated";
			this.tbxLastUpdated.ReadOnly = true;
			this.tbxLastUpdated.Size = new System.Drawing.Size(200, 20);
			this.tbxLastUpdated.TabIndex = 39;
			this.tbxLastUpdated.Text = "";
			// 
			// tbxSiteArticleID
			// 
			this.tbxSiteArticleID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbxSiteArticleID.Location = new System.Drawing.Point(428, 560);
			this.tbxSiteArticleID.Name = "tbxSiteArticleID";
			this.tbxSiteArticleID.ReadOnly = true;
			this.tbxSiteArticleID.Size = new System.Drawing.Size(200, 20);
			this.tbxSiteArticleID.TabIndex = 41;
			this.tbxSiteArticleID.Text = "";
			// 
			// lblSiteArticleID
			// 
			this.lblSiteArticleID.Location = new System.Drawing.Point(344, 560);
			this.lblSiteArticleID.Name = "lblSiteArticleID";
			this.lblSiteArticleID.Size = new System.Drawing.Size(84, 20);
			this.lblSiteArticleID.TabIndex = 40;
			this.lblSiteArticleID.Text = "SiteArticle ID";
			this.lblSiteArticleID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxMemberID
			// 
			this.tbxMemberID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbxMemberID.Location = new System.Drawing.Point(428, 584);
			this.tbxMemberID.Name = "tbxMemberID";
			this.tbxMemberID.Size = new System.Drawing.Size(200, 20);
			this.tbxMemberID.TabIndex = 23;
			this.tbxMemberID.Text = "";
			// 
			// lblMemberID
			// 
			this.lblMemberID.Location = new System.Drawing.Point(364, 588);
			this.lblMemberID.Name = "lblMemberID";
			this.lblMemberID.Size = new System.Drawing.Size(60, 16);
			this.lblMemberID.TabIndex = 20;
			this.lblMemberID.Text = "Member ID";
			this.lblMemberID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxTitle
			// 
			this.tbxTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbxTitle.Location = new System.Drawing.Point(428, 608);
			this.tbxTitle.Name = "tbxTitle";
			this.tbxTitle.Size = new System.Drawing.Size(200, 20);
			this.tbxTitle.TabIndex = 31;
			this.tbxTitle.Text = "";
			// 
			// lblTitle
			// 
			this.lblTitle.Location = new System.Drawing.Point(396, 612);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(28, 16);
			this.lblTitle.TabIndex = 28;
			this.lblTitle.Text = "Title";
			this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxFileID
			// 
			this.tbxFileID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbxFileID.Location = new System.Drawing.Point(428, 632);
			this.tbxFileID.Name = "tbxFileID";
			this.tbxFileID.Size = new System.Drawing.Size(200, 20);
			this.tbxFileID.TabIndex = 48;
			this.tbxFileID.Text = "";
			// 
			// lblFileID
			// 
			this.lblFileID.Location = new System.Drawing.Point(368, 636);
			this.lblFileID.Name = "lblFileID";
			this.lblFileID.Size = new System.Drawing.Size(56, 16);
			this.lblFileID.TabIndex = 49;
			this.lblFileID.Text = "File ID";
			this.lblFileID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// cbxForceLoad
			// 
			this.cbxForceLoad.Location = new System.Drawing.Point(124, 116);
			this.cbxForceLoad.Name = "cbxForceLoad";
			this.cbxForceLoad.Size = new System.Drawing.Size(144, 16);
			this.cbxForceLoad.TabIndex = 52;
			this.cbxForceLoad.Text = "Force Load from DB";
			// 
			// btnGetSiteCategories
			// 
			this.btnGetSiteCategories.Location = new System.Drawing.Point(124, 84);
			this.btnGetSiteCategories.Name = "btnGetSiteCategories";
			this.btnGetSiteCategories.Size = new System.Drawing.Size(144, 28);
			this.btnGetSiteCategories.TabIndex = 53;
			this.btnGetSiteCategories.Text = "Get SiteCategories";
			this.btnGetSiteCategories.Click += new System.EventHandler(this.btnGetSiteCategories_Click);
			// 
			// tbxParentCategoryID
			// 
			this.tbxParentCategoryID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbxParentCategoryID.Location = new System.Drawing.Point(428, 364);
			this.tbxParentCategoryID.Name = "tbxParentCategoryID";
			this.tbxParentCategoryID.Size = new System.Drawing.Size(200, 20);
			this.tbxParentCategoryID.TabIndex = 55;
			this.tbxParentCategoryID.Text = "";
			// 
			// lblParentCategoryID
			// 
			this.lblParentCategoryID.Location = new System.Drawing.Point(320, 368);
			this.lblParentCategoryID.Name = "lblParentCategoryID";
			this.lblParentCategoryID.Size = new System.Drawing.Size(104, 16);
			this.lblParentCategoryID.TabIndex = 54;
			this.lblParentCategoryID.Text = "Parent Category ID";
			this.lblParentCategoryID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxListOrder
			// 
			this.tbxListOrder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbxListOrder.Location = new System.Drawing.Point(428, 388);
			this.tbxListOrder.Name = "tbxListOrder";
			this.tbxListOrder.Size = new System.Drawing.Size(200, 20);
			this.tbxListOrder.TabIndex = 57;
			this.tbxListOrder.Text = "";
			// 
			// lblListOrder
			// 
			this.lblListOrder.Location = new System.Drawing.Point(320, 392);
			this.lblListOrder.Name = "lblListOrder";
			this.lblListOrder.Size = new System.Drawing.Size(104, 16);
			this.lblListOrder.TabIndex = 56;
			this.lblListOrder.Text = "List Order";
			this.lblListOrder.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxConstant
			// 
			this.tbxConstant.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbxConstant.Location = new System.Drawing.Point(428, 412);
			this.tbxConstant.Name = "tbxConstant";
			this.tbxConstant.Size = new System.Drawing.Size(200, 20);
			this.tbxConstant.TabIndex = 59;
			this.tbxConstant.Text = "";
			// 
			// lblConstant
			// 
			this.lblConstant.Location = new System.Drawing.Point(320, 416);
			this.lblConstant.Name = "lblConstant";
			this.lblConstant.Size = new System.Drawing.Size(104, 16);
			this.lblConstant.TabIndex = 58;
			this.lblConstant.Text = "Constant";
			this.lblConstant.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// btnGetCategory
			// 
			this.btnGetCategory.Location = new System.Drawing.Point(124, 256);
			this.btnGetCategory.Name = "btnGetCategory";
			this.btnGetCategory.Size = new System.Drawing.Size(108, 28);
			this.btnGetCategory.TabIndex = 60;
			this.btnGetCategory.Text = "Get Category";
			this.btnGetCategory.Click += new System.EventHandler(this.btnGetCategory_Click);
			// 
			// btnSaveCategory
			// 
			this.btnSaveCategory.Location = new System.Drawing.Point(124, 288);
			this.btnSaveCategory.Name = "btnSaveCategory";
			this.btnSaveCategory.Size = new System.Drawing.Size(108, 28);
			this.btnSaveCategory.TabIndex = 61;
			this.btnSaveCategory.Text = "Save Category";
			this.btnSaveCategory.Click += new System.EventHandler(this.btnSaveCategory_Click);
			// 
			// tbxSiteCategoryID
			// 
			this.tbxSiteCategoryID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbxSiteCategoryID.Location = new System.Drawing.Point(428, 252);
			this.tbxSiteCategoryID.Name = "tbxSiteCategoryID";
			this.tbxSiteCategoryID.Size = new System.Drawing.Size(200, 20);
			this.tbxSiteCategoryID.TabIndex = 63;
			this.tbxSiteCategoryID.Text = "";
			// 
			// lblSiteCategoryID
			// 
			this.lblSiteCategoryID.Location = new System.Drawing.Point(320, 256);
			this.lblSiteCategoryID.Name = "lblSiteCategoryID";
			this.lblSiteCategoryID.Size = new System.Drawing.Size(104, 16);
			this.lblSiteCategoryID.TabIndex = 62;
			this.lblSiteCategoryID.Text = "Site Category ID";
			this.lblSiteCategoryID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// btnSaveSiteCategory
			// 
			this.btnSaveSiteCategory.Location = new System.Drawing.Point(124, 320);
			this.btnSaveSiteCategory.Name = "btnSaveSiteCategory";
			this.btnSaveSiteCategory.Size = new System.Drawing.Size(108, 28);
			this.btnSaveSiteCategory.TabIndex = 64;
			this.btnSaveSiteCategory.Text = "Save SiteCategory";
			this.btnSaveSiteCategory.Click += new System.EventHandler(this.btnSaveSiteCategory_Click);
			// 
			// lblContent
			// 
			this.lblContent.Location = new System.Drawing.Point(304, 336);
			this.lblContent.Name = "lblContent";
			this.lblContent.Size = new System.Drawing.Size(48, 12);
			this.lblContent.TabIndex = 66;
			this.lblContent.Text = "Content";
			this.lblContent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxContent
			// 
			this.tbxContent.Location = new System.Drawing.Point(352, 276);
			this.tbxContent.Multiline = true;
			this.tbxContent.Name = "tbxContent";
			this.tbxContent.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.tbxContent.Size = new System.Drawing.Size(276, 72);
			this.tbxContent.TabIndex = 65;
			this.tbxContent.Text = "";
			// 
			// btnSaveSiteArticle
			// 
			this.btnSaveSiteArticle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnSaveSiteArticle.Location = new System.Drawing.Point(124, 500);
			this.btnSaveSiteArticle.Name = "btnSaveSiteArticle";
			this.btnSaveSiteArticle.Size = new System.Drawing.Size(108, 23);
			this.btnSaveSiteArticle.TabIndex = 67;
			this.btnSaveSiteArticle.Text = "Save SiteArticle";
			this.btnSaveSiteArticle.Click += new System.EventHandler(this.btnSaveSiteArticle_Click);
			// 
			// btnAddArticle
			// 
			this.btnAddArticle.Location = new System.Drawing.Point(124, 352);
			this.btnAddArticle.Name = "btnAddArticle";
			this.btnAddArticle.Size = new System.Drawing.Size(108, 28);
			this.btnAddArticle.TabIndex = 68;
			this.btnAddArticle.Text = "Add Article";
			this.btnAddArticle.Click += new System.EventHandler(this.btnAddArticle_Click);
			// 
			// btnGetArticle
			// 
			this.btnGetArticle.Location = new System.Drawing.Point(124, 524);
			this.btnGetArticle.Name = "btnGetArticle";
			this.btnGetArticle.Size = new System.Drawing.Size(108, 28);
			this.btnGetArticle.TabIndex = 69;
			this.btnGetArticle.Text = "Get Article";
			this.btnGetArticle.Click += new System.EventHandler(this.btnGetArticle_Click);
			// 
			// btnGetSiteArticlesAndArticles
			// 
			this.btnGetSiteArticlesAndArticles.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnGetSiteArticlesAndArticles.Location = new System.Drawing.Point(12, 216);
			this.btnGetSiteArticlesAndArticles.Name = "btnGetSiteArticlesAndArticles";
			this.btnGetSiteArticlesAndArticles.Size = new System.Drawing.Size(100, 52);
			this.btnGetSiteArticlesAndArticles.TabIndex = 70;
			this.btnGetSiteArticlesAndArticles.Text = "Get SiteArticlesAndArticles";
			this.btnGetSiteArticlesAndArticles.Click += new System.EventHandler(this.btnGetSiteArticlesAndArticles_Click);
			// 
			// ArticleHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(636, 661);
			this.Controls.Add(this.btnGetSiteArticlesAndArticles);
			this.Controls.Add(this.btnGetArticle);
			this.Controls.Add(this.btnAddArticle);
			this.Controls.Add(this.btnSaveSiteArticle);
			this.Controls.Add(this.lblContent);
			this.Controls.Add(this.tbxContent);
			this.Controls.Add(this.tbxSiteCategoryID);
			this.Controls.Add(this.tbxConstant);
			this.Controls.Add(this.tbxListOrder);
			this.Controls.Add(this.tbxParentCategoryID);
			this.Controls.Add(this.tbxFileID);
			this.Controls.Add(this.tbxSiteArticleID);
			this.Controls.Add(this.tbxMemberID);
			this.Controls.Add(this.tbxTitle);
			this.Controls.Add(this.tbxOutput);
			this.Controls.Add(this.tbxSiteID);
			this.Controls.Add(this.tbxLastUpdated);
			this.Controls.Add(this.tbxArticleID);
			this.Controls.Add(this.tbxCategoryID);
			this.Controls.Add(this.tbxOrdinal);
			this.Controls.Add(this.btnSaveSiteCategory);
			this.Controls.Add(this.lblSiteCategoryID);
			this.Controls.Add(this.btnSaveCategory);
			this.Controls.Add(this.btnGetCategory);
			this.Controls.Add(this.lblConstant);
			this.Controls.Add(this.lblListOrder);
			this.Controls.Add(this.lblParentCategoryID);
			this.Controls.Add(this.btnGetSiteCategories);
			this.Controls.Add(this.cbxForceLoad);
			this.Controls.Add(this.lblFileID);
			this.Controls.Add(this.btnGetSiteCategoryChildren);
			this.Controls.Add(this.lblOutput);
			this.Controls.Add(this.lblOrdinal);
			this.Controls.Add(this.lblServiceLevel);
			this.Controls.Add(this.cbServiceLevel);
			this.Controls.Add(this.lblSiteArticleID);
			this.Controls.Add(this.lblMemberID);
			this.Controls.Add(this.lblTitle);
			this.Controls.Add(this.cbxIsPublished);
			this.Controls.Add(this.lblSiteID);
			this.Controls.Add(this.lblArticleID);
			this.Controls.Add(this.lblCategoryID);
			this.Controls.Add(this.cbxIsFeatured);
			this.Controls.Add(this.btnGetSiteArticles);
			this.Controls.Add(this.btnGetSiteArticle);
			this.Controls.Add(this.lblLastUpdated);
			this.Name = "ArticleHarness";
			this.Text = "ArticleHarness";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.ResumeLayout(false);

		}
		#endregion

		private void btnCategoryParent_Click(object sender, System.EventArgs e)
		{
//			CategoryCollection oResult = null;
//			ArticleSM articleServiceManager = null;
//			int categoryID = 0;
//			int translationID = 0;
//
//			try 
//			{
//				lbCategories.Items.Clear();
//				testLevel = getSelectedLevel();
//
//				if(tbCategoryID.Text.Trim().Length > 0 && tbTranslationID.Text.Trim().Length > 0) 
//				{
//					categoryID = int.Parse(tbCategoryID.Text.Trim());
//					translationID = int.Parse(tbTranslationID.Text.Trim());
//
//					switch(testLevel) 
//					{
//						case SERVICE_LEVEL.BUSINESS_LOGIC:
//							oResult = ArticleBL.Instance.RetrieveCategoryParentInfo(categoryID, translationID);
//							break;
//						case SERVICE_LEVEL.SERVICE_MANAGER:
//							articleServiceManager = new ArticleSM();
//							oResult = articleServiceManager.RetrieveCategoryParentInfo(categoryID, translationID);
//							break;
//						case SERVICE_LEVEL.SERVICE_ADAPTER:
//							oResult = ArticleSA.Instance.RetrieveCategoryParentInfo(categoryID, translationID);
//							break;
//						default:
//							oResult = ArticleBL.Instance.RetrieveCategoryParentInfo(categoryID, translationID);
//							break;
//					}
//					DisplayCategoryResults(oResult);
//				}
//			} 
//			catch(FormatException) 
//			{
//				MessageBox.Show("Please enter valid category and translation IDs");
//			} 
//			catch(Exception ex) 
//			{
//				MessageBox.Show("Error retrieving category: " + ex.Message + "\n" + ex.StackTrace);
//			}
		}

		private void btnGetSiteCategoryChildren_Click(object sender, System.EventArgs e)
		{
//			SiteCategoryCollection foundData = null;
//			ArticleSM articleServiceManager = null;
//			int siteID = int.MinValue;
//			int categoryID = int.MinValue;
//
//			InitializeControls();
//
//			try 
//			{
//				siteID = int.Parse(tbxSiteID.Text.Trim());
//				try
//				{
//					categoryID = int.Parse(tbxCategoryID.Text.Trim());
//				}
//				catch
//				{
//					categoryID = int.MinValue;
//				}
//
//				testLevel = getSelectedLevel();
//
//				switch(testLevel) 
//				{
//					case SERVICE_LEVEL.BUSINESS_LOGIC:
//						foundData = ArticleBL.Instance.RetrieveSiteCategoryChildren(siteID, categoryID);
//						break;
//					case SERVICE_LEVEL.SERVICE_MANAGER:
//						articleServiceManager = new ArticleSM();
//						foundData = articleServiceManager.RetrieveSiteCategoryChildren(siteID, categoryID);
//						break;
//					case SERVICE_LEVEL.SERVICE_ADAPTER:
//						foundData = ArticleSA.Instance.RetrieveSiteCategoryChildren(siteID, categoryID);
//						break;
//					default:
//						foundData = ArticleBL.Instance.RetrieveSiteCategoryChildren(siteID, categoryID);
//						break;
//				}
//
//				if(foundData != null) 
//				{
//					StringBuilder sb = new StringBuilder();
//
//					foreach (SiteCategory siteCategory in (SiteCategoryCollection)foundData)
//					{
//						sb.Append("[CategoryID: ").Append(siteCategory.CategoryID).Append("]\r\n");
//					}
//
//					tbxOutput.Text = sb.ToString();
//				}
//			} 
//			catch(FormatException) 
//			{
//				MessageBox.Show("Error parsing input data.");
//			}
//			catch(Exception ex) 
//			{
//				MessageBox.Show("Error retrieving SiteCategoryChildren information: " + ex.StackTrace);
//			}
		}

		public void lbCategories_IndexChanged(object sender, System.EventArgs e) 
		{
//			Category selectedCategory = (Category)lbCategories.SelectedItem;
//			tbCategoryContent.Text = "";
//			tbCategoryExID.Text = "";
//			tbCategoryParentExID.Text = "";
//
//			if (selectedCategory != null) 
//			{
//				tbCategoryContent.Text = selectedCategory.Content;
//				tbCategoryExID.Text = selectedCategory.CategoryExID.ToString();
//				tbCategoryParentExID.Text = selectedCategory.ParentCategoryExID.ToString();
//			}
		}

		//private void DisplayCategoryResults(CategoryCollection oResult) 
		//{
//			lbCategories.DisplayMember = "Content";
//
//			foreach(Category currentCategory in oResult) 
//			{
//				lbCategories.Items.Add(currentCategory);
//			}
		//}

		private SERVICE_LEVEL getSelectedLevel() 
		{
			return (SERVICE_LEVEL)Enum.Parse(typeof(SERVICE_LEVEL), cbServiceLevel.SelectedItem.ToString());
		}

		private void InitializeControls()
		{
			tbxSiteArticleID.Text = string.Empty;
			tbxMemberID.Text = string.Empty;
			tbxTitle.Text = string.Empty;
			tbxOutput.Text = string.Empty;
			tbxFileID.Text = string.Empty;
			tbxOrdinal.Text = string.Empty;
			tbxLastUpdated.Text = string.Empty;
			cbxIsPublished.Checked = false;
			cbxIsFeatured.Checked = false;
		}

		private void btnGetSiteArticle_Click(object sender, System.EventArgs e)
		{
//			SiteArticle foundData = null;
//			ArticleSM articleServiceManager = null;
//			int articleID = 0;
//			int siteID = 0;
//
//			InitializeControls();
//
//			try 
//			{
//				articleID = int.Parse(tbxArticleID.Text.Trim());
//				siteID = int.Parse(tbxSiteID.Text.Trim());
//
//				testLevel = getSelectedLevel();
//
//				switch(testLevel) 
//				{
//					case SERVICE_LEVEL.BUSINESS_LOGIC:
//						foundData = ArticleBL.Instance.RetrieveSiteArticle(articleID, siteID);
//						break;
//					case SERVICE_LEVEL.SERVICE_MANAGER:
//						articleServiceManager = new ArticleSM();
//						foundData = articleServiceManager.RetrieveSiteArticle(articleID, siteID);
//						break;
//					case SERVICE_LEVEL.SERVICE_ADAPTER:
//						foundData = ArticleSA.Instance.RetrieveSiteArticle(articleID, siteID);
//						break;
//					default:
//						foundData = ArticleBL.Instance.RetrieveSiteArticle(articleID, siteID);
//						break;
//				}
//
//				if(foundData != null) 
//				{
//					tbxSiteArticleID.Text = foundData.SiteArticleID.ToString();
//					tbxMemberID.Text = foundData.MemberID.ToString();
//					tbxTitle.Text = foundData.Title;
//					tbxContent.Text = foundData.Content;
//					tbxFileID.Text = foundData.FileID.ToString();
//					tbxOrdinal.Text = foundData.Ordinal.ToString();
//					tbxLastUpdated.Text = foundData.LastUpdated.ToString();
//					cbxIsPublished.Checked = Convert.ToBoolean(foundData.PublishedFlag);
//					cbxIsFeatured.Checked = Convert.ToBoolean(foundData.FeaturedFlag);
//				}
//			} 
//			catch(FormatException) 
//			{
//				MessageBox.Show("Error parsing input data.");
//			}
//			catch(Exception ex) 
//			{
//				MessageBox.Show("Error retrieving SiteArticle information: " + ex.StackTrace);
//			}
		}

		private void btnGetSiteArticles_Click(object sender, System.EventArgs e)
		{
//			SiteArticleCollection foundData = null;
//			ArticleSM articleServiceManager = null;
//			int categoryID = 0;
//			int siteID = 0;
//
//			InitializeControls();
//
//			try 
//			{
//				categoryID = int.Parse(tbxCategoryID.Text.Trim());
//				siteID = int.Parse(tbxSiteID.Text.Trim());
//
//				testLevel = getSelectedLevel();
//
//				switch(testLevel) 
//				{
//					case SERVICE_LEVEL.BUSINESS_LOGIC:
//						foundData = ArticleBL.Instance.RetrieveCategorySiteArticles(categoryID, siteID);
//						break;
//					case SERVICE_LEVEL.SERVICE_MANAGER:
//						articleServiceManager = new ArticleSM();
//						foundData = articleServiceManager.RetrieveCategorySiteArticles(categoryID, siteID);
//						break;
//					case SERVICE_LEVEL.SERVICE_ADAPTER:
//						foundData = ArticleSA.Instance.RetrieveCategorySiteArticles(categoryID, siteID);
//						break;
//					default:
//						foundData = ArticleBL.Instance.RetrieveCategorySiteArticles(categoryID, siteID);
//						break;
//				}
//
//				if(foundData != null) 
//				{
//					StringBuilder sb = new StringBuilder();
//
//					foreach (SiteArticle siteArticle in (SiteArticleCollection)foundData)
//					{
//						sb.Append("[ArticleID: ").Append(siteArticle.ArticleID).Append("]\r\n");
//					}
//
//					tbxOutput.Text = sb.ToString();
//				}
//			} 
//			catch(FormatException) 
//			{
//				MessageBox.Show("Error parsing input data.");
//			}
//			catch(Exception ex) 
//			{
//				MessageBox.Show("Error retrieving SiteArticles information: " + ex.StackTrace);
//			}
		}

		private void btnGetSiteCategories_Click(object sender, System.EventArgs e)
		{
//			SiteCategoryCollection foundData = null;
//			ArticleSM articleServiceManager = null;
//			int siteID = 0;
//			bool forceLoadFlag = false;
//
//			InitializeControls();
//
//			try 
//			{
//				siteID = int.Parse(tbxSiteID.Text.Trim());
//				forceLoadFlag = cbxForceLoad.Checked;
//
//				testLevel = getSelectedLevel();
//
//				switch(testLevel) 
//				{
//					case SERVICE_LEVEL.BUSINESS_LOGIC:
//						foundData = ArticleBL.Instance.RetrieveSiteCategories(siteID, forceLoadFlag);
//						break;
//					case SERVICE_LEVEL.SERVICE_MANAGER:
//						articleServiceManager = new ArticleSM();
//						foundData = articleServiceManager.RetrieveSiteCategories(siteID, forceLoadFlag);
//						break;
//					case SERVICE_LEVEL.SERVICE_ADAPTER:
//						foundData = ArticleSA.Instance.RetrieveSiteCategories(siteID, forceLoadFlag);
//						break;
//					default:
//						foundData = ArticleBL.Instance.RetrieveSiteCategories(siteID, forceLoadFlag);
//						break;
//				}
//
//				if (foundData != null) 
//				{
//					StringBuilder sb = new StringBuilder();
//
//					foreach (SiteCategory siteCategory in (SiteCategoryCollection)foundData)
//					{
//						sb.Append("[SiteCategoryID: ").Append(siteCategory.SiteCategoryID).Append("][CategoryID: ").Append(siteCategory.CategoryID).Append("]\r\n");
//					}
//
//					tbxOutput.Text = sb.ToString();
//				}
//			} 
//			catch(FormatException) 
//			{
//				MessageBox.Show("Error parsing input data.");
//			}
//			catch(Exception ex) 
//			{
//				MessageBox.Show("Error retrieving SiteCategories information: " + ex.StackTrace);
//			}
		}

		private void btnGetCategory_Click(object sender, System.EventArgs e)
		{
			Category foundData = null;
			ArticleSM articleServiceManager = null;
			int categoryID = int.MinValue;

			InitializeControls();

			try 
			{
				categoryID = int.Parse(tbxCategoryID.Text.Trim());
				
				testLevel = getSelectedLevel();

				switch(testLevel) 
				{
					case SERVICE_LEVEL.BUSINESS_LOGIC:
						foundData = ArticleBL.Instance.RetrieveCategory(categoryID);
						break;
					case SERVICE_LEVEL.SERVICE_MANAGER:
						articleServiceManager = new ArticleSM();
						foundData = articleServiceManager.RetrieveCategory(categoryID);
						break;
					case SERVICE_LEVEL.SERVICE_ADAPTER:
						foundData = ArticleSA.Instance.RetrieveCategory(categoryID);
						break;
					default:
						foundData = ArticleBL.Instance.RetrieveCategory(categoryID);
						break;
				}

				if (foundData != null) 
				{
					tbxCategoryID.Text = foundData.CategoryID.ToString();
					tbxParentCategoryID.Text = foundData.ParentCategoryID.ToString();
					tbxListOrder.Text = foundData.ListOrder.ToString();
					tbxConstant.Text = foundData.Constant;
					tbxLastUpdated.Text = foundData.LastUpdated.ToString();
				}
			} 
			catch(FormatException) 
			{
				MessageBox.Show("Error parsing input data.");
			}
			catch(Exception ex) 
			{
				MessageBox.Show("Error retrieving SiteCategories information: " + ex.StackTrace);
			}
		}

		private void btnSaveCategory_Click(object sender, System.EventArgs e)
		{
			Category category;
			ArticleSM articleServiceManager = null;

			try 
			{
				int categoryID;
				try
				{
					categoryID = int.Parse(tbxCategoryID.Text.Trim());
				}
				catch
				{
					categoryID = int.MinValue;
				}
				int parentCategoryID;
				try
				{
					parentCategoryID = int.Parse(tbxParentCategoryID.Text.Trim());
				}
				catch
				{
					parentCategoryID = int.MinValue;
				}
				int listOrder = int.Parse(tbxListOrder.Text.Trim());
				string constant = tbxConstant.Text;
				bool publishedFlag = cbxIsPublished.Checked;
				
				category = new Category(categoryID, parentCategoryID, listOrder, constant);

				testLevel = getSelectedLevel();

//				switch(testLevel) 
//				{
//					case SERVICE_LEVEL.BUSINESS_LOGIC:
//						ArticleBL.Instance.SaveCategory(category);
//						break;
//					case SERVICE_LEVEL.SERVICE_MANAGER:
//						articleServiceManager = new ArticleSM();
//						articleServiceManager.SaveCategory(category);
//						break;
//					case SERVICE_LEVEL.SERVICE_ADAPTER:
//						ArticleSA.Instance.SaveCategory(category);
//						break;
//					default:
//						ArticleBL.Instance.SaveCategory(category);
//						break;
//				}
			} 
			catch(FormatException) 
			{
				MessageBox.Show("Error parsing input data.");
			}
			catch(Exception ex) 
			{
				MessageBox.Show("Error saving Category information: " + ex.StackTrace);
			}
		}

		private void btnSaveSiteCategory_Click(object sender, System.EventArgs e)
		{
			SiteCategory siteCategory;
			ArticleSM articleServiceManager = null;

			try 
			{
				int siteCategoryID;
				try
				{
					siteCategoryID = int.Parse(tbxSiteCategoryID.Text.Trim());
				}
				catch
				{
					siteCategoryID = int.MinValue;
				}
				int categoryID = int.Parse(tbxCategoryID.Text.Trim());
				int siteID = int.Parse(tbxSiteID.Text.Trim());
				string content = tbxContent.Text;
				bool publishedFlag = cbxIsPublished.Checked;
				
				siteCategory = new SiteCategory(siteCategoryID, categoryID, siteID, content, publishedFlag);

				testLevel = getSelectedLevel();

//				switch(testLevel) 
//				{
//					case SERVICE_LEVEL.BUSINESS_LOGIC:
//						ArticleBL.Instance.SaveSiteCategory(siteCategory);
//						break;
//					case SERVICE_LEVEL.SERVICE_MANAGER:
//						articleServiceManager = new ArticleSM();
//						articleServiceManager.SaveSiteCategory(siteCategory);
//						break;
//					case SERVICE_LEVEL.SERVICE_ADAPTER:
//						ArticleSA.Instance.SaveSiteCategory(siteCategory);
//						break;
//					default:
//						ArticleBL.Instance.SaveSiteCategory(siteCategory);
//						break;
//				}
			} 
			catch(FormatException) 
			{
				MessageBox.Show("Error parsing input data.");
			}
			catch(Exception ex) 
			{
				MessageBox.Show("Error saving SiteCategory information: " + ex.StackTrace);
			}
		}

		private void btnSaveSiteArticle_Click(object sender, System.EventArgs e)
		{
			SiteArticle siteArticle;
			ArticleSM articleServiceManager = null;

			try 
			{
				int siteArticleID;
				try
				{
					siteArticleID = int.Parse(tbxSiteArticleID.Text.Trim());
				}
				catch
				{
					siteArticleID = int.MinValue;
				}
				int articleID = int.Parse(tbxArticleID.Text.Trim());
				int siteID = int.Parse(tbxSiteID.Text.Trim());
				int memberID = int.Parse(tbxMemberID.Text.Trim());
				int fileID = int.Parse(tbxFileID.Text.Trim());
				string title = tbxTitle.Text;
				string content = tbxContent.Text;
				int ordinal = int.Parse(tbxOrdinal.Text.Trim());
				bool publishedFlag = cbxIsPublished.Checked;
				bool featuredFlag = cbxIsFeatured.Checked;
				
				siteArticle = new SiteArticle(siteArticleID, articleID, siteID, memberID, ordinal, publishedFlag, featuredFlag, fileID, title, content);

				testLevel = getSelectedLevel();

//				switch(testLevel) 
//				{
//					case SERVICE_LEVEL.BUSINESS_LOGIC:
//						ArticleBL.Instance.SaveSiteArticle(siteArticle);
//						break;
//					case SERVICE_LEVEL.SERVICE_MANAGER:
//						articleServiceManager = new ArticleSM();
//						articleServiceManager.SaveSiteArticle(siteArticle);
//						break;
//					case SERVICE_LEVEL.SERVICE_ADAPTER:
//						ArticleSA.Instance.SaveSiteArticle(siteArticle);
//						break;
//					default:
//						ArticleBL.Instance.SaveSiteArticle(siteArticle);
//						break;
//				}
			} 
			catch (FormatException) 
			{
				MessageBox.Show("Error parsing input data.");
			}
			catch (Exception ex) 
			{
				MessageBox.Show("Error saving SiteArticle information: " + ex.StackTrace);
			}
		}

		private void btnAddArticle_Click(object sender, System.EventArgs e)
		{
			Article article;
			ArticleSM articleServiceManager = null;

			try 
			{
				int categoryID = int.Parse(tbxCategoryID.Text.Trim());
				
				article = new Article(categoryID);

				testLevel = getSelectedLevel();

//				switch(testLevel) 
//				{
//					case SERVICE_LEVEL.BUSINESS_LOGIC:
//						ArticleBL.Instance.SaveArticle(article);
//						break;
//					case SERVICE_LEVEL.SERVICE_MANAGER:
//						articleServiceManager = new ArticleSM();
//						articleServiceManager.SaveArticle(article);
//						break;
//					case SERVICE_LEVEL.SERVICE_ADAPTER:
//						ArticleSA.Instance.SaveArticle(article);
//						break;
//					default:
//						ArticleBL.Instance.SaveArticle(article);
//						break;
//				}
			}
			catch (FormatException) 
			{
				MessageBox.Show("Error parsing input data.");
			}
			catch (Exception ex) 
			{
				MessageBox.Show("Error saving Article information: " + ex.StackTrace);
			}
		}

		private void btnGetArticle_Click(object sender, System.EventArgs e)
		{
			Article foundData = null;
			ArticleSM articleServiceManager = null;
			int articleID = int.MinValue;

			InitializeControls();

			try 
			{
				articleID = int.Parse(tbxArticleID.Text.Trim());
				
				testLevel = getSelectedLevel();

				switch(testLevel) 
				{
					case SERVICE_LEVEL.BUSINESS_LOGIC:
						foundData = ArticleBL.Instance.RetrieveArticle(articleID);
						break;
					case SERVICE_LEVEL.SERVICE_MANAGER:
						articleServiceManager = new ArticleSM();
						foundData = articleServiceManager.RetrieveArticle(articleID);
						break;
					case SERVICE_LEVEL.SERVICE_ADAPTER:
						foundData = ArticleSA.Instance.RetrieveArticle(articleID);
						break;
					default:
						foundData = ArticleBL.Instance.RetrieveArticle(articleID);
						break;
				}

				if (foundData != null) 
				{
					tbxArticleID.Text = foundData.ArticleID.ToString();
					tbxCategoryID.Text = foundData.CategoryID.ToString();
					tbxContent.Text = foundData.DefaultContent.ToString();
					tbxConstant.Text = foundData.Constant;
					tbxLastUpdated.Text = foundData.LastUpdated.ToString();
				}
			} 
			catch(FormatException) 
			{
				MessageBox.Show("Error parsing input data.");
			}
			catch(Exception ex) 
			{
				MessageBox.Show("Error retrieving Article information: " + ex.StackTrace);
			}
		}

		private void btnGetSiteArticlesAndArticles_Click(object sender, System.EventArgs e)
		{
//			SiteArticleCollection foundData = null;
//			ArticleSM articleServiceManager = null;
//			int categoryID = 0;
//			int siteID = 0;
//
//			InitializeControls();
//
//			try 
//			{
//				categoryID = int.Parse(tbxCategoryID.Text.Trim());
//				siteID = int.Parse(tbxSiteID.Text.Trim());
//
//				testLevel = getSelectedLevel();
//
//				switch(testLevel) 
//				{
//					case SERVICE_LEVEL.BUSINESS_LOGIC:
//						foundData = ArticleBL.Instance.RetrieveCategorySiteArticlesAndArticles(categoryID, siteID, cbxForceLoad.Checked);
//						break;
//					case SERVICE_LEVEL.SERVICE_MANAGER:
//						articleServiceManager = new ArticleSM();
//						foundData = articleServiceManager.RetrieveCategorySiteArticlesAndArticles(categoryID, siteID, cbxForceLoad.Checked);
//						break;
//					case SERVICE_LEVEL.SERVICE_ADAPTER:
//						foundData = ArticleSA.Instance.RetrieveCategorySiteArticlesAndArticles(categoryID, siteID, cbxForceLoad.Checked);
//						break;
//					default:
//						foundData = ArticleBL.Instance.RetrieveCategorySiteArticlesAndArticles(categoryID, siteID, cbxForceLoad.Checked);
//						break;
//				}
//
//				if(foundData != null) 
//				{
//					StringBuilder sb = new StringBuilder();
//
//					foreach (SiteArticle siteArticle in (SiteArticleCollection)foundData)
//					{
//						sb.Append("[ArticleID: ").Append(siteArticle.ArticleID).Append("]\r\n");
//					}
//
//					tbxOutput.Text = sb.ToString();
//				}
//			} 
//			catch(FormatException) 
//			{
//				MessageBox.Show("Error parsing input data.");
//			}
//			catch(Exception ex) 
//			{
//				MessageBox.Show("Error retrieving SiteArticles information: " + ex.StackTrace);
//			}
		}
	}
}
