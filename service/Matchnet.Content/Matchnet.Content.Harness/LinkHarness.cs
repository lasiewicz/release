using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Matchnet.Content.Harness
{
	/// <summary>
	/// Summary description for LinkHarness.
	/// </summary>
	public class LinkHarness : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox tbRawUrl;
		private System.Windows.Forms.Button btnGo;
		private System.Windows.Forms.TextBox tbOutput;
		private System.Windows.Forms.CheckBox cbSSL;
		private System.Windows.Forms.CheckBox cbNewWindow;
		private System.Windows.Forms.Label NewURLLabel;
		private System.Windows.Forms.GroupBox groupNewWindow;
		private System.Windows.Forms.TextBox tbName;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tbWidth;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbHeight;
		private System.Windows.Forms.TextBox tbProperties;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.CheckBox cbPersistLayoutTemplate;
		private System.Windows.Forms.TextBox tbRequestURL;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox tbBrandID;
		private System.Windows.Forms.Label label7;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.ListBox lbParameters;
		private System.Windows.Forms.TextBox tbParameterName;
		private System.Windows.Forms.TextBox tbParameterValue;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Button btnAddParameter;
		private System.Windows.Forms.Label lblParameters;
		private System.Windows.Forms.Button btnDeleteParameter;

		protected ParameterCollection parameterCollection = new ParameterCollection();

		public LinkHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tbRawUrl = new System.Windows.Forms.TextBox();
			this.btnGo = new System.Windows.Forms.Button();
			this.tbOutput = new System.Windows.Forms.TextBox();
			this.cbSSL = new System.Windows.Forms.CheckBox();
			this.cbNewWindow = new System.Windows.Forms.CheckBox();
			this.NewURLLabel = new System.Windows.Forms.Label();
			this.groupNewWindow = new System.Windows.Forms.GroupBox();
			this.label4 = new System.Windows.Forms.Label();
			this.tbProperties = new System.Windows.Forms.TextBox();
			this.tbHeight = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.tbWidth = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tbName = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.cbPersistLayoutTemplate = new System.Windows.Forms.CheckBox();
			this.tbRequestURL = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.tbBrandID = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.lbParameters = new System.Windows.Forms.ListBox();
			this.tbParameterName = new System.Windows.Forms.TextBox();
			this.tbParameterValue = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.btnAddParameter = new System.Windows.Forms.Button();
			this.lblParameters = new System.Windows.Forms.Label();
			this.btnDeleteParameter = new System.Windows.Forms.Button();
			this.groupNewWindow.SuspendLayout();
			this.SuspendLayout();
			// 
			// tbRawUrl
			// 
			this.tbRawUrl.Location = new System.Drawing.Point(16, 24);
			this.tbRawUrl.Name = "tbRawUrl";
			this.tbRawUrl.Size = new System.Drawing.Size(344, 20);
			this.tbRawUrl.TabIndex = 0;
			this.tbRawUrl.Text = "/Applications/Subscription/Subscribe.aspx";
			// 
			// btnGo
			// 
			this.btnGo.Location = new System.Drawing.Point(376, 16);
			this.btnGo.Name = "btnGo";
			this.btnGo.Size = new System.Drawing.Size(56, 24);
			this.btnGo.TabIndex = 1;
			this.btnGo.Text = "Go";
			this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
			// 
			// tbOutput
			// 
			this.tbOutput.Location = new System.Drawing.Point(16, 392);
			this.tbOutput.Name = "tbOutput";
			this.tbOutput.Size = new System.Drawing.Size(344, 20);
			this.tbOutput.TabIndex = 2;
			this.tbOutput.Text = "";
			// 
			// cbSSL
			// 
			this.cbSSL.Location = new System.Drawing.Point(312, 120);
			this.cbSSL.Name = "cbSSL";
			this.cbSSL.Size = new System.Drawing.Size(48, 24);
			this.cbSSL.TabIndex = 3;
			this.cbSSL.Text = "SSL";
			// 
			// cbNewWindow
			// 
			this.cbNewWindow.Location = new System.Drawing.Point(8, 16);
			this.cbNewWindow.Name = "cbNewWindow";
			this.cbNewWindow.Size = new System.Drawing.Size(112, 24);
			this.cbNewWindow.TabIndex = 4;
			this.cbNewWindow.Text = "Use New Window";
			// 
			// NewURLLabel
			// 
			this.NewURLLabel.Location = new System.Drawing.Point(16, 376);
			this.NewURLLabel.Name = "NewURLLabel";
			this.NewURLLabel.Size = new System.Drawing.Size(64, 16);
			this.NewURLLabel.TabIndex = 5;
			this.NewURLLabel.Text = "New URL:";
			// 
			// groupNewWindow
			// 
			this.groupNewWindow.Controls.Add(this.label4);
			this.groupNewWindow.Controls.Add(this.tbProperties);
			this.groupNewWindow.Controls.Add(this.tbHeight);
			this.groupNewWindow.Controls.Add(this.label3);
			this.groupNewWindow.Controls.Add(this.label2);
			this.groupNewWindow.Controls.Add(this.tbWidth);
			this.groupNewWindow.Controls.Add(this.label1);
			this.groupNewWindow.Controls.Add(this.tbName);
			this.groupNewWindow.Controls.Add(this.cbNewWindow);
			this.groupNewWindow.Location = new System.Drawing.Point(16, 112);
			this.groupNewWindow.Name = "groupNewWindow";
			this.groupNewWindow.Size = new System.Drawing.Size(272, 120);
			this.groupNewWindow.TabIndex = 6;
			this.groupNewWindow.TabStop = false;
			this.groupNewWindow.Text = "New Window Items";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 88);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(56, 16);
			this.label4.TabIndex = 12;
			this.label4.Text = "Properties";
			// 
			// tbProperties
			// 
			this.tbProperties.Location = new System.Drawing.Point(72, 88);
			this.tbProperties.Name = "tbProperties";
			this.tbProperties.Size = new System.Drawing.Size(192, 20);
			this.tbProperties.TabIndex = 11;
			this.tbProperties.Text = "";
			// 
			// tbHeight
			// 
			this.tbHeight.Location = new System.Drawing.Point(64, 56);
			this.tbHeight.Name = "tbHeight";
			this.tbHeight.Size = new System.Drawing.Size(40, 20);
			this.tbHeight.TabIndex = 10;
			this.tbHeight.Text = "";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(64, 40);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(40, 16);
			this.label3.TabIndex = 9;
			this.label3.Text = "Height";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 40);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(40, 16);
			this.label2.TabIndex = 8;
			this.label2.Text = "Width";
			// 
			// tbWidth
			// 
			this.tbWidth.Location = new System.Drawing.Point(8, 56);
			this.tbWidth.Name = "tbWidth";
			this.tbWidth.Size = new System.Drawing.Size(40, 20);
			this.tbWidth.TabIndex = 7;
			this.tbWidth.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(136, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(112, 16);
			this.label1.TabIndex = 6;
			this.label1.Text = "New Window Name";
			// 
			// tbName
			// 
			this.tbName.Location = new System.Drawing.Point(136, 40);
			this.tbName.Name = "tbName";
			this.tbName.TabIndex = 5;
			this.tbName.Text = "";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(16, 8);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 16);
			this.label5.TabIndex = 7;
			this.label5.Text = "Target URL:";
			// 
			// cbPersistLayoutTemplate
			// 
			this.cbPersistLayoutTemplate.Location = new System.Drawing.Point(312, 152);
			this.cbPersistLayoutTemplate.Name = "cbPersistLayoutTemplate";
			this.cbPersistLayoutTemplate.Size = new System.Drawing.Size(104, 32);
			this.cbPersistLayoutTemplate.TabIndex = 8;
			this.cbPersistLayoutTemplate.Text = "Persist Layout Template";
			// 
			// tbRequestURL
			// 
			this.tbRequestURL.Location = new System.Drawing.Point(16, 72);
			this.tbRequestURL.Name = "tbRequestURL";
			this.tbRequestURL.Size = new System.Drawing.Size(344, 20);
			this.tbRequestURL.TabIndex = 9;
			this.tbRequestURL.Text = "http://dv-fred.spark.com/Applications/Home/Default.aspx";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(16, 56);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(80, 16);
			this.label6.TabIndex = 10;
			this.label6.Text = "Request URL:";
			// 
			// tbBrandID
			// 
			this.tbBrandID.Location = new System.Drawing.Point(312, 208);
			this.tbBrandID.Name = "tbBrandID";
			this.tbBrandID.TabIndex = 11;
			this.tbBrandID.Text = "1017";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(312, 192);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(56, 16);
			this.label7.TabIndex = 12;
			this.label7.Text = "Brand ID";
			// 
			// lbParameters
			// 
			this.lbParameters.Location = new System.Drawing.Point(104, 296);
			this.lbParameters.Name = "lbParameters";
			this.lbParameters.Size = new System.Drawing.Size(192, 69);
			this.lbParameters.TabIndex = 13;
			// 
			// tbParameterName
			// 
			this.tbParameterName.Location = new System.Drawing.Point(104, 264);
			this.tbParameterName.Name = "tbParameterName";
			this.tbParameterName.Size = new System.Drawing.Size(80, 20);
			this.tbParameterName.TabIndex = 14;
			this.tbParameterName.Text = "";
			// 
			// tbParameterValue
			// 
			this.tbParameterValue.Location = new System.Drawing.Point(216, 264);
			this.tbParameterValue.Name = "tbParameterValue";
			this.tbParameterValue.Size = new System.Drawing.Size(80, 20);
			this.tbParameterValue.TabIndex = 15;
			this.tbParameterValue.Text = "";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(192, 264);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(24, 24);
			this.label8.TabIndex = 16;
			this.label8.Text = "=";
			// 
			// btnAddParameter
			// 
			this.btnAddParameter.Location = new System.Drawing.Point(312, 264);
			this.btnAddParameter.Name = "btnAddParameter";
			this.btnAddParameter.Size = new System.Drawing.Size(48, 24);
			this.btnAddParameter.TabIndex = 17;
			this.btnAddParameter.Text = "Add";
			this.btnAddParameter.Click += new System.EventHandler(this.btnAddParameter_Click);
			// 
			// lblParameters
			// 
			this.lblParameters.Location = new System.Drawing.Point(16, 264);
			this.lblParameters.Name = "lblParameters";
			this.lblParameters.Size = new System.Drawing.Size(80, 16);
			this.lblParameters.TabIndex = 18;
			this.lblParameters.Text = "Parameters";
			// 
			// btnDeleteParameter
			// 
			this.btnDeleteParameter.Location = new System.Drawing.Point(312, 296);
			this.btnDeleteParameter.Name = "btnDeleteParameter";
			this.btnDeleteParameter.Size = new System.Drawing.Size(48, 24);
			this.btnDeleteParameter.TabIndex = 19;
			this.btnDeleteParameter.Text = "Delete";
			this.btnDeleteParameter.Click += new System.EventHandler(this.btnDeleteParameter_Click);
			// 
			// LinkHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(528, 429);
			this.Controls.Add(this.btnDeleteParameter);
			this.Controls.Add(this.lblParameters);
			this.Controls.Add(this.btnAddParameter);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.tbParameterValue);
			this.Controls.Add(this.tbParameterName);
			this.Controls.Add(this.lbParameters);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.tbBrandID);
			this.Controls.Add(this.tbRequestURL);
			this.Controls.Add(this.tbOutput);
			this.Controls.Add(this.tbRawUrl);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.cbPersistLayoutTemplate);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.groupNewWindow);
			this.Controls.Add(this.NewURLLabel);
			this.Controls.Add(this.cbSSL);
			this.Controls.Add(this.btnGo);
			this.Name = "LinkHarness";
			this.Text = "LinkHarness";
			this.groupNewWindow.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void btnGo_Click(object sender, System.EventArgs e)
		{
			string url = this.tbRawUrl.Text;
			string requesturl = this.tbRequestURL.Text;
			Brand brand = BrandConfigSA.Instance.GetBrandByID(Conversion.CInt(this.tbBrandID.Text));

			if( !this.cbNewWindow.Checked )
			{
				this.tbOutput.Text = LinkFactory.Instance.GetLink(url, parameterCollection, brand, requesturl, this.cbSSL.Checked);
			}
			else
			{
				this.tbOutput.Text = LinkFactory.Instance.GetLink(url, parameterCollection, brand, requesturl, this.cbSSL.Checked, 
					this.cbNewWindow.Checked, this.tbName.Text, int.Parse(this.tbWidth.Text), int.Parse(this.tbHeight.Text), this.tbProperties.Text);
			}
		}

		private void btnAddParameter_Click(object sender, System.EventArgs e)
		{
			parameterCollection.Add(tbParameterName.Text, tbParameterValue.Text);

			lbParameters.Items.Add(tbParameterName.Text + "=" + tbParameterValue.Text);
		}

		private void btnDeleteParameter_Click(object sender, System.EventArgs e)
		{
			if (this.lbParameters.SelectedItem == null)
			{
				MessageBox.Show("Please select an item, meng.");
			}
			else
			{
				string name = this.lbParameters.SelectedItem.ToString();

				lbParameters.Items.Remove(this.lbParameters.SelectedItem);

				name = name.Split(new char[] {'='})[0];

				parameterCollection.Delete(name);
			}
		}		
	}
}
