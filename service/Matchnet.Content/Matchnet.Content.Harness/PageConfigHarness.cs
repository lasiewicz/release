using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.PageConfig;

namespace Matchnet.Content.Harness
{
	/// <summary>
	/// Summary description for PageConfigHarness.
	/// </summary>
	public class PageConfigHarness : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnGetPages;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public PageConfigHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnGetPages = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnGetPages
			// 
			this.btnGetPages.Location = new System.Drawing.Point(16, 16);
			this.btnGetPages.Name = "btnGetPages";
			this.btnGetPages.Size = new System.Drawing.Size(136, 23);
			this.btnGetPages.TabIndex = 0;
			this.btnGetPages.Text = "Get Pages";
			this.btnGetPages.Click += new System.EventHandler(this.btnGetPages_Click);
			// 
			// PageConfigHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(584, 373);
			this.Controls.Add(this.btnGetPages);
			this.Name = "PageConfigHarness";
			this.Text = "PageConfigHarness";
			this.ResumeLayout(false);

		}
		#endregion

		private void btnGetPages_Click(object sender, System.EventArgs e)
		{
			//SitePages sitePages = PageConfigBL.Instance.GetSitePages();
AnalyticsPage analyticsPage = PageConfigBL.Instance.GetAnalyticsPage(1000);
		}
	}
}
