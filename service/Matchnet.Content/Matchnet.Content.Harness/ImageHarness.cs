using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceManagers;
using Matchnet.Content.ValueObjects.Image;

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Matchnet.Content.Harness
{
	/// <summary>
	/// Summary description for ImageHarness.
	/// </summary>
	public class ImageHarness : System.Windows.Forms.Form
	{
		#region enumerations
		private enum SERVICE_LEVEL 
		{
			SERVICE_ADAPTER = 0,
			SERVICE_MANAGER = 1,
			BUSINESS_LOGIC = 2
		}
		#endregion

		private SERVICE_LEVEL testLevel = SERVICE_LEVEL.BUSINESS_LOGIC;
		private System.Windows.Forms.Button btnImageRoot;
		private System.Windows.Forms.TextBox tbImageRoot;
		private System.Windows.Forms.ComboBox cbServiceLevel;
		private System.Windows.Forms.Button btnImageList;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ImageHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			PopulateServiceLevel();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnImageRoot = new System.Windows.Forms.Button();
			this.tbImageRoot = new System.Windows.Forms.TextBox();
			this.cbServiceLevel = new System.Windows.Forms.ComboBox();
			this.btnImageList = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnImageRoot
			// 
			this.btnImageRoot.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnImageRoot.Location = new System.Drawing.Point(20, 80);
			this.btnImageRoot.Name = "btnImageRoot";
			this.btnImageRoot.TabIndex = 0;
			this.btnImageRoot.Text = "Image Root";
			this.btnImageRoot.Click += new System.EventHandler(this.btnImageRoot_Click);
			// 
			// tbImageRoot
			// 
			this.tbImageRoot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbImageRoot.Location = new System.Drawing.Point(20, 48);
			this.tbImageRoot.Name = "tbImageRoot";
			this.tbImageRoot.ReadOnly = true;
			this.tbImageRoot.Size = new System.Drawing.Size(328, 20);
			this.tbImageRoot.TabIndex = 1;
			this.tbImageRoot.Text = "";
			// 
			// cbServiceLevel
			// 
			this.cbServiceLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbServiceLevel.Location = new System.Drawing.Point(20, 12);
			this.cbServiceLevel.Name = "cbServiceLevel";
			this.cbServiceLevel.Size = new System.Drawing.Size(200, 21);
			this.cbServiceLevel.TabIndex = 2;
			// 
			// btnImageList
			// 
			this.btnImageList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnImageList.Location = new System.Drawing.Point(20, 128);
			this.btnImageList.Name = "btnImageList";
			this.btnImageList.TabIndex = 3;
			this.btnImageList.Text = "Image List";
			this.btnImageList.Click += new System.EventHandler(this.btnImageList_Click);
			// 
			// ImageHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(368, 345);
			this.Controls.Add(this.btnImageList);
			this.Controls.Add(this.cbServiceLevel);
			this.Controls.Add(this.tbImageRoot);
			this.Controls.Add(this.btnImageRoot);
			this.Name = "ImageHarness";
			this.Text = "ImageHarness";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.ResumeLayout(false);

		}
		#endregion

		private void PopulateServiceLevel() 
		{
			cbServiceLevel.Items.Clear();
			foreach(String currentLevel in Enum.GetNames(typeof(SERVICE_LEVEL))) 
			{
				cbServiceLevel.Items.Add(currentLevel);
			}
			cbServiceLevel.SelectedIndex = 0;
		}

		private void btnImageRoot_Click(object sender, System.EventArgs e)
		{
			ImageRoot oResult = null;
			ImageSM imageServiceManager = null;

			try
			{
				tbImageRoot.Text = "";
				testLevel = getSelectedLevel();

				switch(testLevel) 
				{
					case SERVICE_LEVEL.BUSINESS_LOGIC:
						oResult = ImageBL.Instance.GetImageUNCRoot();
						break;
					case SERVICE_LEVEL.SERVICE_MANAGER:
						imageServiceManager = new ImageSM();
						oResult = imageServiceManager.GetImageUNCRoot();
						break;
					case SERVICE_LEVEL.SERVICE_ADAPTER:
						oResult = ImageSA.Instance.GetImageUNCRoot();
						break;
					default:
						oResult = ImageBL.Instance.GetImageUNCRoot();
						break;
				}

				tbImageRoot.Text = oResult.Path;
			} 
			catch(Exception ex) 
			{
				MessageBox.Show("Error Retrieving Admin Action Logs: " + ex.StackTrace);
			}
		}

		private SERVICE_LEVEL getSelectedLevel() 
		{
			return (SERVICE_LEVEL)Enum.Parse(typeof(SERVICE_LEVEL), cbServiceLevel.SelectedItem.ToString());
		}

		private void btnImageList_Click(object sender, System.EventArgs e)
		{
// This all relies on image table... deprecated.
//			ImageSM imageServiceManager = null;
//			Matchnet.Content.ValueObjects.Image.ImageList oResult = null;	 
//			testLevel = getSelectedLevel();
//
//			switch(testLevel) 
//			{
//				case SERVICE_LEVEL.BUSINESS_LOGIC:
//					oResult = ImageBL.Instance.RetrieveImageDataTable("c:\tem", 2,2,2,2,2);
//					break;
//				case SERVICE_LEVEL.SERVICE_MANAGER:
//					imageServiceManager = new ImageSM();
//					oResult = imageServiceManager.RetrieveImageDataTable("c:\tem",0,0,0,0,0);
//					break;
//				case SERVICE_LEVEL.SERVICE_ADAPTER:
//					oResult = ImageSA.Instance.RetrieveImageDataTable("c:\tem",0,0,0,0,0);
//					break;
//				default:
//					oResult = ImageBL.Instance.RetrieveImageDataTable("c:\tem",0,0,0,0,0);
//					break;
//			}
		}
	}
}
