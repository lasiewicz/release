using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceManagers;
using Matchnet.Content.ValueObjects.PagePixel;

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Matchnet.Content.Harness
{
	/// <summary>
	/// Summary description for PagePixelHarness.
	/// </summary>
	public class PagePixelHarness : System.Windows.Forms.Form
	{

		#region enumerations
		private enum SERVICE_LEVEL 
		{
			SERVICE_ADAPTER = 0,
			SERVICE_MANAGER = 1,
			BUSINESS_LOGIC = 2
		}
		#endregion

		private SERVICE_LEVEL testLevel = SERVICE_LEVEL.BUSINESS_LOGIC;
		private System.Windows.Forms.Label lblPageID;
		private System.Windows.Forms.Label lblPrivateLabelID;
		private System.Windows.Forms.ComboBox cbServiceLevel;
		private System.Windows.Forms.TextBox tbPageID;
		private System.Windows.Forms.TextBox tbSiteID;
		private System.Windows.Forms.Button btnPagePixels;
		private System.Windows.Forms.ListBox lbPagePixels;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public PagePixelHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			PopulateServiceLevel();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private void PopulateServiceLevel() 
		{
			cbServiceLevel.Items.Clear();
			foreach(String currentLevel in Enum.GetNames(typeof(SERVICE_LEVEL))) 
			{
				cbServiceLevel.Items.Add(currentLevel);
			}
			cbServiceLevel.SelectedIndex = 0;
		}

		private SERVICE_LEVEL getSelectedLevel() 
		{
			return (SERVICE_LEVEL)Enum.Parse(typeof(SERVICE_LEVEL), cbServiceLevel.SelectedItem.ToString());
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblPageID = new System.Windows.Forms.Label();
			this.lblPrivateLabelID = new System.Windows.Forms.Label();
			this.cbServiceLevel = new System.Windows.Forms.ComboBox();
			this.tbPageID = new System.Windows.Forms.TextBox();
			this.tbSiteID = new System.Windows.Forms.TextBox();
			this.btnPagePixels = new System.Windows.Forms.Button();
			this.lbPagePixels = new System.Windows.Forms.ListBox();
			this.SuspendLayout();
			// 
			// lblPageID
			// 
			this.lblPageID.Location = new System.Drawing.Point(68, 48);
			this.lblPageID.Name = "lblPageID";
			this.lblPageID.Size = new System.Drawing.Size(48, 16);
			this.lblPageID.TabIndex = 0;
			this.lblPageID.Text = "Page ID";
			// 
			// lblPrivateLabelID
			// 
			this.lblPrivateLabelID.Location = new System.Drawing.Point(28, 76);
			this.lblPrivateLabelID.Name = "lblPrivateLabelID";
			this.lblPrivateLabelID.Size = new System.Drawing.Size(88, 16);
			this.lblPrivateLabelID.TabIndex = 1;
			this.lblPrivateLabelID.Text = "Site ID";
			// 
			// cbServiceLevel
			// 
			this.cbServiceLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbServiceLevel.Location = new System.Drawing.Point(28, 12);
			this.cbServiceLevel.Name = "cbServiceLevel";
			this.cbServiceLevel.Size = new System.Drawing.Size(200, 21);
			this.cbServiceLevel.TabIndex = 3;
			// 
			// tbPageID
			// 
			this.tbPageID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbPageID.Location = new System.Drawing.Point(116, 44);
			this.tbPageID.Name = "tbPageID";
			this.tbPageID.Size = new System.Drawing.Size(112, 20);
			this.tbPageID.TabIndex = 4;
			this.tbPageID.Text = "549104";
			// 
			// tbSiteID
			// 
			this.tbSiteID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbSiteID.Location = new System.Drawing.Point(116, 72);
			this.tbSiteID.Name = "tbSiteID";
			this.tbSiteID.Size = new System.Drawing.Size(112, 20);
			this.tbSiteID.TabIndex = 5;
			this.tbSiteID.Text = "101";
			// 
			// btnPagePixels
			// 
			this.btnPagePixels.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnPagePixels.Location = new System.Drawing.Point(28, 108);
			this.btnPagePixels.Name = "btnPagePixels";
			this.btnPagePixels.Size = new System.Drawing.Size(196, 140);
			this.btnPagePixels.TabIndex = 6;
			this.btnPagePixels.Text = "Get Page Pixels";
			this.btnPagePixels.Click += new System.EventHandler(this.btnPagePixels_Click);
			// 
			// lbPagePixels
			// 
			this.lbPagePixels.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lbPagePixels.Location = new System.Drawing.Point(268, 12);
			this.lbPagePixels.Name = "lbPagePixels";
			this.lbPagePixels.Size = new System.Drawing.Size(380, 483);
			this.lbPagePixels.TabIndex = 7;
			// 
			// PagePixelHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(736, 549);
			this.Controls.Add(this.tbSiteID);
			this.Controls.Add(this.tbPageID);
			this.Controls.Add(this.lbPagePixels);
			this.Controls.Add(this.btnPagePixels);
			this.Controls.Add(this.cbServiceLevel);
			this.Controls.Add(this.lblPrivateLabelID);
			this.Controls.Add(this.lblPageID);
			this.Name = "PagePixelHarness";
			this.Text = "PagePixelHarness";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.ResumeLayout(false);

		}
		#endregion

		private void btnPagePixels_Click(object sender, System.EventArgs e)
		{
			PagePixelCollection oResult = null;
			PagePixelSM pagePixelServiceManager = null;
			int pageID = Convert.ToInt32(tbPageID.Text);
			int siteID = Convert.ToInt32(tbSiteID.Text);

			try
			{
				testLevel = getSelectedLevel();

				switch(testLevel) 
				{
					case SERVICE_LEVEL.BUSINESS_LOGIC:

                        //oResult = PagePixelBL.Instance.RetrievePagePixels(@"localhost", 
                        //    Matchnet.CacheSynchronization.Context.Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(333)), pageID, siteID);
                        break;
					case SERVICE_LEVEL.SERVICE_MANAGER:
						pagePixelServiceManager = new PagePixelSM();
						//oResult = pagePixelServiceManager.RetrievePagePixels(pageID, siteID);
						break;
					case SERVICE_LEVEL.SERVICE_ADAPTER:
						oResult = PagePixelSA.Instance.RetrievePagePixels(pageID, siteID);
						break;
					default:
						//oResult = PagePixelBL.Instance.RetrievePagePixels(pageID, siteID);
						break;
				}
				MessageBox.Show(oResult.Count.ToString());

				PopulatePagePixels(oResult);
			} 
			catch(FormatException) 
			{
				MessageBox.Show("Please provide valid parameters");
			}
			catch(Exception ex) 
			{
				MessageBox.Show("Error Retrieving Page Pixels: " + ex.StackTrace);
			}
		}

		private void PopulatePagePixels(PagePixelCollection oResult) 
		{
			lbPagePixels.DataSource = oResult;
			
			lbPagePixels.DisplayMember = "Code";
			lbPagePixels.ValueMember ="Code";
		}


	}
}
