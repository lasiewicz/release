using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Matchnet.Content.Harness
{
	/// <summary>
	/// Summary description for MainHarness.
	/// </summary>
	public class MainHarness : System.Windows.Forms.Form
	{
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem miHarnesses;
		private System.Windows.Forms.MenuItem miAdmin;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem miExit;
		private System.Windows.Forms.MenuItem miArticle;
		private System.Windows.Forms.MenuItem miPromotion;
		private System.Windows.Forms.MenuItem miImage;
		private System.Windows.Forms.MenuItem miPagePixel;
		private System.Windows.Forms.MenuItem miRegion;
		private System.Windows.Forms.MenuItem miAttribute;
		private System.Windows.Forms.MenuItem miBrand;
		private System.Windows.Forms.MenuItem miPageConfig;
		private System.Windows.Forms.MenuItem miLink;
		private System.Windows.Forms.MenuItem miEvents;
		private System.Windows.Forms.MenuItem miAttributeMetaData;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public MainHarness()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Entry Point
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new MainHarness());
		}
		#endregion

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.miHarnesses = new System.Windows.Forms.MenuItem();
			this.miAdmin = new System.Windows.Forms.MenuItem();
			this.miArticle = new System.Windows.Forms.MenuItem();
			this.miImage = new System.Windows.Forms.MenuItem();
			this.miPagePixel = new System.Windows.Forms.MenuItem();
			this.miPromotion = new System.Windows.Forms.MenuItem();
			this.miRegion = new System.Windows.Forms.MenuItem();
			this.miEvents = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.miAttribute = new System.Windows.Forms.MenuItem();
			this.miBrand = new System.Windows.Forms.MenuItem();
			this.miPageConfig = new System.Windows.Forms.MenuItem();
			this.miLink = new System.Windows.Forms.MenuItem();
			this.miExit = new System.Windows.Forms.MenuItem();
			this.miAttributeMetaData = new System.Windows.Forms.MenuItem();
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.miHarnesses});
			// 
			// miHarnesses
			// 
			this.miHarnesses.Index = 0;
			this.miHarnesses.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						this.miAdmin,
																						this.miArticle,
																						this.miImage,
																						this.miPagePixel,
																						this.miPromotion,
																						this.miRegion,
																						this.miEvents,
																						this.menuItem2,
																						this.miAttribute,
																						this.miBrand,
																						this.miPageConfig,
																						this.miLink,
																						this.miExit,
																						this.miAttributeMetaData});
			this.miHarnesses.Text = "Harnesses";
			// 
			// miAdmin
			// 
			this.miAdmin.Index = 0;
			this.miAdmin.Text = "Admin";
			this.miAdmin.Click += new System.EventHandler(this.miAdmin_Click);
			// 
			// miArticle
			// 
			this.miArticle.Index = 1;
			this.miArticle.Text = "Article";
			this.miArticle.Click += new System.EventHandler(this.miArticle_Click);
			// 
			// miImage
			// 
			this.miImage.Index = 2;
			this.miImage.Text = "Image";
			this.miImage.Click += new System.EventHandler(this.miImage_Click);
			// 
			// miPagePixel
			// 
			this.miPagePixel.Index = 3;
			this.miPagePixel.Text = "PagePixel";
			this.miPagePixel.Click += new System.EventHandler(this.miPagePixel_Click);
			// 
			// miPromotion
			// 
			this.miPromotion.Index = 4;
			this.miPromotion.Text = "Promotion";
			this.miPromotion.Click += new System.EventHandler(this.miPromotion_Click);
			// 
			// miRegion
			// 
			this.miRegion.Index = 5;
			this.miRegion.Text = "Region";
			this.miRegion.Click += new System.EventHandler(this.miRegion_Click);
			// 
			// miEvents
			// 
			this.miEvents.Index = 6;
			this.miEvents.Text = "Events";
			this.miEvents.Click += new System.EventHandler(this.miEvents_Click);
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 7;
			this.menuItem2.Text = "-";
			// 
			// miAttribute
			// 
			this.miAttribute.Index = 8;
			this.miAttribute.Text = "Attribute Options";
			this.miAttribute.Click += new System.EventHandler(this.miAttribute_Click);
			// 
			// miBrand
			// 
			this.miBrand.Index = 9;
			this.miBrand.Text = "Brand";
			this.miBrand.Click += new System.EventHandler(this.miBrand_Click);
			// 
			// miPageConfig
			// 
			this.miPageConfig.Index = 10;
			this.miPageConfig.Text = "PageConfig";
			this.miPageConfig.Click += new System.EventHandler(this.miPageConfig_Click);
			// 
			// miLink
			// 
			this.miLink.Index = 11;
			this.miLink.Text = "Link";
			this.miLink.Click += new System.EventHandler(this.miLink_Click);
			// 
			// miAttributeMetaData
			// 
			this.miAttributeMetaData.Index = 12;
			this.miAttributeMetaData.Text = "Attribute Meta Data";
			this.miAttributeMetaData.Click +=new EventHandler(miAttributeMetaData_Click);
			// 
			// miExit
			// 
			this.miExit.Index = 13;
			this.miExit.Text = "Exit";
			// 
			// MainHarness
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(688, 737);
			this.IsMdiContainer = true;
			this.Menu = this.mainMenu1;
			this.Name = "MainHarness";
			this.Text = "MainHarness";

		}
		#endregion

		private void miAdmin_Click(object sender, System.EventArgs e)
		{
			AdminHarness anAdminHarness = new AdminHarness();
			anAdminHarness.MdiParent = this;
			anAdminHarness.Show();
		}

		private void miArticle_Click(object sender, System.EventArgs e) 
		{
			ArticleHarness anArticleHarness = new ArticleHarness();
			anArticleHarness.MdiParent = this;
			anArticleHarness.Show();
		}

		private void miPromotion_Click(object sender, System.EventArgs e) 
		{
			PromotionHarness aPromotionHarness = new PromotionHarness();
			aPromotionHarness.MdiParent = this;
			aPromotionHarness.Show();
		}

		private void miImage_Click(object sender, System.EventArgs e) 
		{
			ImageHarness anImageHarness = new ImageHarness();
			anImageHarness.MdiParent = this;
			anImageHarness.Show();
		}

		private void miPagePixel_Click(object sender, System.EventArgs e)
		{
			PagePixelHarness aPagePixelHarness = new PagePixelHarness();
			aPagePixelHarness.MdiParent = this;
			aPagePixelHarness.Show();
		}

		private void miRegion_Click(object sender, EventArgs e)
		{
			RegionHarness aRegionHarness = new RegionHarness();
			aRegionHarness.MdiParent = this;
			aRegionHarness.Show();
		}

		private void miAttribute_Click(object sender, EventArgs e)
		{
			AttributeOptionHarness aoh = new AttributeOptionHarness();
			aoh.MdiParent = this;
			aoh.Show();
		}

		private void miBrand_Click(object sender, EventArgs e)
		{
			BrandHarness aoh = new BrandHarness();
			aoh.MdiParent = this;
			aoh.Show();
		}

		private void miPageConfig_Click(object sender, EventArgs e)
		{
			PageConfigHarness harness = new PageConfigHarness();
			harness.MdiParent = this;
			harness.Show();
		}

		private void miLink_Click(object sender, EventArgs e)
		{
			LinkHarness harness = new LinkHarness();
			harness.MdiParent = this;
			harness.Show();
		}

		private void miEvents_Click(object sender, System.EventArgs e)
		{
			EventHarness events = new EventHarness();
			events.MdiParent = this;
			events.Show();
		}

		private void miAttributeMetaData_Click(object sender, System.EventArgs e)
		{
			AttributeMetadataHarness amd = new AttributeMetadataHarness();
			amd.MdiParent = this;
			amd.Show();
		}
	}
}
