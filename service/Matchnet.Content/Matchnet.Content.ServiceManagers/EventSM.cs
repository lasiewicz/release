using System;
using System.Diagnostics;

using Matchnet.Exceptions;
using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ValueObjects.Events;

namespace Matchnet.Content.ServiceManagers
{
	/// <summary>
	/// Summary description for EventSM.
	/// </summary>
	public class EventSM : MarshalByRefObject, IServiceManager, IEventService
	{
		#region Private Members
		private PerformanceCounter _perfCount;
		private PerformanceCounter _perfRequestsSec;
		private PerformanceCounter _perfHitRate;
		private PerformanceCounter _perfHitRate_Base;
		#endregion

		#region Constructors
		public EventSM()
		{
			initPerfCounters();
			EventBL.Instance.EventRequested += new Matchnet.Content.BusinessLogic.EventBL.EventRequestEventHandler(EventBL_EventRequested);
			EventBL.Instance.EventAdded += new Matchnet.Content.BusinessLogic.EventBL.EventAddEventHandler(EventBL_EventAdded);
			EventBL.Instance.EventRemoved += new Matchnet.Content.BusinessLogic.EventBL.EventRemoveEventHandler(EventBL_EventRemoved);

		}
		#endregion
		
		public void PrePopulateCache()
		{
		}


		public override object InitializeLifetimeService()
		{
			return null;
		}


		#region Public Methods
		public EventCollection GetEvents(Int32 communityID, string orderBy, string sorting, Int32 startRow, Int32 pageSize, string publishFlag, Event.EVENT_TYPE eventType)
		{
			try 
			{
				EventCollection Result = null;

				Result = EventBL.Instance.GetEvents(communityID, 
					orderBy, 
					sorting, 
					startRow, 
					pageSize, 
					publishFlag, 
					eventType);

				return Result;
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving events."+ ex.Message, ex);
			}		
		}

		public EventCollection GetEvents(Int32 communityID, Event.EVENT_TYPE eventType)
		{
			try 
			{
				EventCollection Result = null;

				Result = EventBL.Instance.GetEvents(communityID, eventType);

				return Result;
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving events."+ ex.Message, ex);
			}		
		}

		public EventCollection GetAlbumEvents(Int32 communityID)
		{
			try 
			{
				EventCollection Result = null;

				Result = EventBL.Instance.GetAlbumEvents(communityID);

				return Result;
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving album events."+ ex.Message, ex);
			}		
		}

		public Event GetEvent(Int32 eventID)
		{
			try 
			{
				Event Result = null;

				Result = EventBL.Instance.GetEvent(eventID);

				return Result;
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving event."+ ex.Message, ex);
			}
		}


		public HolidayCollection GetHolidayCollection()
		{
			try 
			{
				HolidayCollection Result = null;

				Result = EventBL.Instance.GetHolidays();

				return Result;
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving holidays."+ ex.Message, ex);
			}		
		}


		public HolidayCollection GetHolidayCollection(int year)
		{
			try 
			{
				HolidayCollection Result = null;

				Result = EventBL.Instance.GetHolidays(year);

				return Result;
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving holidays."+ ex.Message, ex);
			}		
		}

		public static void PerfCounterInstall()
		{	
			/*
			CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
			ccdc.AddRange(new CounterCreationData [] {	
														 new CounterCreationData("Event Items", "Event Items", PerformanceCounterType.NumberOfItems64),
														 new CounterCreationData("Event Requests/second", "Event Requests/second", PerformanceCounterType.RateOfCountsPerSecond32),
														 new CounterCreationData("Event Hit Rate", "Event Hit Rate", PerformanceCounterType.RawFraction),
														 new CounterCreationData("Event Hit Rate_Base","Event Hit Rate_Base", PerformanceCounterType.RawBase),
			});
			PerformanceCounterCategory.Create(ServiceConstants.SERVICE_NAME, ServiceConstants.SERVICE_NAME, ccdc);
			*/
		}


		public static void PerfCounterUninstall()
		{	
			//PerformanceCounterCategory.Delete(ServiceConstants.SERVICE_NAME);
		}

		#endregion

		#region Private Methods
		private void EventBL_EventRequested(bool cacheHit)
		{
			/*
			_perfRequestsSec.Increment();
			_perfHitRate_Base.Increment();

			if (cacheHit)
			{
				_perfHitRate.Increment();
			}
			*/
		}


		private void EventBL_EventAdded()
		{
			//_perfCount.Increment();
		}

		
		private void EventBL_EventRemoved()
		{
			//_perfCount.Decrement();
		}

		private void initPerfCounters()
		{
			/*
			_perfCount = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Event Items", false);
			_perfRequestsSec = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Event Requests/second", false);
			_perfHitRate = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Event Hit Rate", false);
			_perfHitRate_Base = new PerformanceCounter(ServiceConstants.SERVICE_NAME, "Event Hit Rate_Base", false);

			resetPerfCounters();
			*/
		}


		private void resetPerfCounters()
		{
			/*
			_perfCount.RawValue = 0;
			_perfRequestsSec.RawValue = 0;
			_perfHitRate.RawValue = 0;
			_perfHitRate_Base.RawValue = 0;
			*/
		}
		#endregion

		#region IDisposable Members
		public void Dispose()
		{
			//resetPerfCounters();
		}
		#endregion
	}
}
