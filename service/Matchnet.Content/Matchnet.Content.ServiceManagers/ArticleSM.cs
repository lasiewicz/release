using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ValueObjects.Article;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Exceptions;
using Matchnet.Data.Hydra;
using Matchnet.CacheSynchronization;
using Matchnet.Replication;
using Matchnet.CacheSynchronization.ValueObjects;

using System;
using System.Data;

namespace Matchnet.Content.ServiceManagers
{
	/// <summary>
	/// Summary description for ArticleSM.
	/// </summary>
	public class ArticleSM : MarshalByRefObject, IServiceManager, IArticleService, IDisposable, IReplicationRecipient
	{
		#region constants
		private const string SERVICE_MANAGER_NAME = "ArticleSM";
		#endregion

		internal static Replicator replicator = null;
		internal static Synchronizer synchronizer = null;

		/// <summary>
		/// </summary>
		#region constructor
		public ArticleSM()
		{
			ArticleBL.Instance.SynchronizationRequested += new Matchnet.Content.BusinessLogic.ArticleBL.SynchronizationEventHandler(ArticleBL_SynchronizationRequested);
			ArticleBL.Instance.ReplicationRequested += new Matchnet.Content.BusinessLogic.ArticleBL.ReplicationEventHandler(ArticleBL_ReplicationRequested);

			String machineName = System.Environment.MachineName;
			String overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
			if(overrideMachineName != null)
			{
				machineName = overrideMachineName;
			}

			String replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_REPLICATION_OVERRIDE");
			if (replicationURI.Length == 0)
			{
				replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, SERVICE_MANAGER_NAME, machineName);
			}

			replicator = new Replicator(SERVICE_MANAGER_NAME);
			replicator.SetDestinationUri(replicationURI);
			replicator.Start();

			synchronizer = new Synchronizer(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_NAME);
			synchronizer.Start();
		}
		#endregion

		void IReplicationRecipient.Receive(IReplicable replicable)
		{
			try
			{
				ArticleBL.Instance.CacheReplicatedObject(replicable);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_NAME, "Failure processing replication object.", ex);
			}
		}

		private void ArticleBL_ReplicationRequested(IReplicable replicableObject)
		{
			replicator.Enqueue(replicableObject);
		}

		private void ArticleBL_SynchronizationRequested(string key, System.Collections.Hashtable cacheReferences)
		{
			synchronizer.Enqueue(key, cacheReferences);
		}

		// DEPRECATED.  KEPT FOR BACKWARDS COMBATIBILITY.
		public SiteArticle RetrieveSiteArticle(int articleID, int siteID)
		{
			return RetrieveSiteArticle(null, null, articleID, siteID);
		}

		public SiteArticle RetrieveSiteArticle(string clientHostName, CacheReference cacheReference, int articleID, int siteID)
		{
			SiteArticle oResult = null;
			
			try
			{
				oResult = ArticleBL.Instance.RetrieveSiteArticle(clientHostName, cacheReference, articleID, siteID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving SiteArticle.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving SiteArticle.", ex);
			}

			return oResult;
		}

		// DEPRECATED.  KEPT FOR BACKWARDS COMBATIBILITY.
		public SiteArticleCollection RetrieveCategorySiteArticles(int categoryID, int siteID)
		{
			return RetrieveCategorySiteArticles(null, null, categoryID, siteID);
		}

		public SiteArticleCollection RetrieveCategorySiteArticles(string clientHostName, CacheReference cacheReference, int categoryID, int siteID)
		{
			SiteArticleCollection oResult = null;

			try
			{
				oResult = ArticleBL.Instance.RetrieveCategorySiteArticles(clientHostName, cacheReference, categoryID, siteID);
			}
			catch(ExceptionBase ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving CategorySiteArticles.", ex);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving CategorySiteArticles.", ex);
			}

			return oResult;
		}

		// DEPRECATED.  KEPT FOR BACKWARDS COMBATIBILITY.
		public SiteArticleCollection RetrieveCategorySiteArticlesAndArticles(int categoryID, int siteID, bool forceLoadFlag)
		{
			return RetrieveCategorySiteArticlesAndArticles(categoryID, siteID);
		}

		public SiteArticleCollection RetrieveCategorySiteArticlesAndArticles(int categoryID, int siteID)
		{
			SiteArticleCollection oResult = null;

			try
			{
				oResult = ArticleBL.Instance.RetrieveCategorySiteArticlesAndArticles(categoryID, siteID);
			}
			catch(ExceptionBase ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving CategorySiteArticlesAndArticles.", ex);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving CategorySiteArticlesAndArticles.", ex);
			}

			return oResult;
		}

		// DEPRECATED.  KEPT FOR BACKWARDS COMBATIBILITY.
		public SiteCategoryCollection RetrieveSiteCategoryChildren(int siteID, int categoryID)
		{
			return RetrieveSiteCategoryChildren(null, null, siteID, categoryID);
		}

		public SiteCategoryCollection RetrieveSiteCategoryChildren(string clientHostName, CacheReference cacheReference, int siteID, int categoryID)
		{
			SiteCategoryCollection oResult = null;

			try
			{
				oResult = ArticleBL.Instance.RetrieveSiteCategoryChildren(clientHostName, cacheReference, siteID, categoryID);
			}
			catch(ExceptionBase ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving SiteCategoryChildren.", ex);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving SiteCategoryChildren.", ex);
			}

			return oResult;
		}

		// DEPRECATED.  KEPT FOR BACKWARDS COMBATIBILITY.
		public SiteCategoryCollection RetrieveSiteCategories(int siteID, bool forceLoadFlag)
		{
			return RetrieveSiteCategories(null, null, siteID, forceLoadFlag);
		}

		public SiteCategoryCollection RetrieveSiteCategories(string clientHostName, CacheReference cacheReference, int siteID, bool forceLoadFlag)
		{
			SiteCategoryCollection oResult = null;

			try
			{
				oResult = ArticleBL.Instance.RetrieveSiteCategories(clientHostName, cacheReference, siteID, forceLoadFlag);
			}
			catch(ExceptionBase ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving SiteCategories.", ex);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving SiteCategories.", ex);
			}

			return oResult;
		}

		public Category RetrieveCategory(int categoryID)
		{
			Category oResult = null;
			
			try
			{
				oResult = ArticleBL.Instance.RetrieveCategory(categoryID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving Category.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving Category.", ex);
			}

			return oResult;
		}

		public Article RetrieveArticle(int articleID)
		{
			Article oResult = null;
			
			try
			{
				oResult = ArticleBL.Instance.RetrieveArticle(articleID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving Article.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving Article.", ex);
			}

			return oResult;
		}

		public int SaveCategory(Category category, Int32 memberID, Boolean isPublish)
		{
			try
			{
				return ArticleBL.Instance.SaveCategory(category, memberID, isPublish);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving Category.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving Category.", ex);
			}
		}

		public int SaveSiteCategory(SiteCategory siteCategory, Int32 memberID, Boolean isPublish)
		{
			try
			{
				return ArticleBL.Instance.SaveSiteCategory(siteCategory, memberID, isPublish);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving SiteCategory.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving SiteCategory.", ex);
			}
		}

		public int SaveArticle(Article article, Int32 memberID, Boolean isPublish)
		{
			try
			{
				return ArticleBL.Instance.SaveArticle(article, memberID, isPublish);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving Article.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving Article.", ex);
			}
		}

		public int SaveSiteArticle(SiteArticle siteArticle, Int32 memberID, Boolean isPublish)
		{
			try
			{
				return ArticleBL.Instance.SaveSiteArticle(siteArticle, memberID, isPublish);
			} 
			catch(ExceptionBase ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving SiteArticle.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving SiteArticle.", ex);
			}
		}


		#region WSO Operations

		public WSOListCollection RetrieveWSOListCollection()
		{
			try
			{
				return ArticleBL.Instance.RetrieveWSOListCollection();
			} 
			catch(ExceptionBase ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving WSOlistcollection.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving WSOlistcollection", ex);
			}
		}

		public void SaveWSOList(WSOList wsoList)
		{
			try
			{
				ArticleBL.Instance.SaveWSOList(wsoList);
			} 
			catch(ExceptionBase ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving WSOList.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving WSOList.", ex);
			}
		}

		public void CreateWSOList(string name, string note)
		{
			try
			{
				ArticleBL.Instance.CreateWSOList(name, note);
			} 
			catch(ExceptionBase ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure creating WSOList.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure creating WSOList.", ex);
			}
		}

		public WSOItemCollection RetrieveWSOItemCollection(int wsoListID)
		{
			try
			{
				return ArticleBL.Instance.RetrieveWSOItemCollection(wsoListID);
			} 
			catch(ExceptionBase ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving WSOItemcollection.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving WSOItemcollection", ex);
			}
		}

		public void SaveWSOItem(WSOItem wsoItem)
		{
			try
			{
				ArticleBL.Instance.SaveWSOItem(wsoItem);
			} 
			catch(ExceptionBase ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving WSOItem.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving WSOItem.", ex);
			}
		}

		public void DeleteWSOItem(WSOItem wsoItem)
		{
			try
			{
				ArticleBL.Instance.DeleteWSOItem(wsoItem);
			} 
			catch(ExceptionBase ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure deleting WSOItem.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure deleting WSOItem.", ex);
			}
		}

		public void CreateWSOItem(int wsoListID, int siteID, int itemID, int itemTypeID)
		{
			try
			{
				ArticleBL.Instance.CreateWSOItem(wsoListID, siteID, itemID, itemTypeID);
			} 
			catch(ExceptionBase ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure creating WSOItem.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure creating WSOItem.", ex);
			}
		}


		#endregion

		#region IServiceManager Members
		public void PrePopulateCache()
		{
			// no implementation at this time
		}
		#endregion

		#region IDisposable Members
		public void Dispose()
		{
			if (replicator != null)
			{
				replicator.Stop();
			}

			if (synchronizer != null)
			{
				synchronizer.Stop();
			}
		}
		#endregion

		public override object InitializeLifetimeService()
		{
			return null;
		}

	}
}
