﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ValueObjects.Registration;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;

namespace Matchnet.Content.ServiceManagers
{
    public class RegistrationMetadataSM : MarshalByRefObject, IServiceManager, IRegistrationMetadata, IDisposable
    {
        #region constants
        private const string SERVICE_MANAGER_NAME = "RegistrationMetadataSM";
        public const string SERVICE_NAME = "Matchnet.Content.Service";
        #endregion

        private HydraWriter _hydraWriter;

        public RegistrationMetadataSM()
        {
            _hydraWriter = new HydraWriter(new string[] { "mnSystem" });
            _hydraWriter.Start();
        }
        
        #region IRegistrationMetadata Members

        public List<Scenario> GetScenarios()
        {
            List<Scenario> scenarios = null;

            try
            {
                scenarios = RegistrationMetadataBL.Instance.GetScenarios();
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving registration scenarios.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving registration scenarios.", ex);
            }
            return scenarios;
        }

        public List<Scenario> GetScenariosForAdmin()
        {
            List<Scenario> scenarios = null;

            try
            {
                scenarios = RegistrationMetadataBL.Instance.GetScenariosForAdmin();
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving registration scenarios for admin.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving registration scenarios fr admin.", ex);
            }
            return scenarios;
        }

        public List<Scenario> GetAllRegScenarios()
        {
            List<Scenario> scenarios = null;

            try
            {
                scenarios = RegistrationMetadataBL.Instance.GetAllRegScenarios();
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving registration metdata.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving registration metdata.", ex);
            }
            return scenarios;
        }

        public void SaveScenarioStatusWeight(int regScenarioId, bool activate, double weight, string updatedBy, int siteID, int deviceTypeID)
        {
            try
            {
                RegistrationMetadataBL.Instance.SaveScenarioStatusWeight(regScenarioId, activate, weight, updatedBy, siteID, deviceTypeID);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure updating status and weight for registration scenario " + regScenarioId.ToString(), ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure updating status and weight for registration scenario " + regScenarioId.ToString(), ex);
            }
        }

        public List<RegControl> GetAllRegControls(int deviceTypeId)
        {
            List<RegControl> allControls = null;
            try
            {
                allControls = RegistrationMetadataBL.Instance.GetAllRegControls(deviceTypeId);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure retirieving registration controls.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure toggling registration controls.", ex);
            }
            return allControls;
        }

        public List<RegControl> GetRegControlsRequiredForRegScenarioSite(int siteId)
        {
            List<RegControl> requiredRegControls = null;
            try
            {
                requiredRegControls = RegistrationMetadataBL.Instance.GetRegControlsRequiredForRegScenarioSite(siteId);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure retirieving required controls.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure retirieving required controls.", ex);
            }
            return requiredRegControls;
        }

        public int CreateRegScenario(Scenario scenario)
        {
            try
            {
                return RegistrationMetadataBL.Instance.CreateRegScenario(scenario);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure creating registration scenario.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure creating registration scenario.", ex);
            }
        }

        public void SaveScenario(Scenario scenario)
        {
            try
            {
                RegistrationMetadataBL.Instance.SaveScenario(scenario);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure saving registration scenario.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure saving registration scenario.", ex);
            }
        }

        public void SaveRegStep(Step step, int scenarioId)
        {
            try
            {
                RegistrationMetadataBL.Instance.SaveRegStep(step, scenarioId);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure saving reg step.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure saving reg step.", ex);
            }
        }


        public void SaveRegControlScenarioOverride(RegControlScenarioOverride regControlegControlScenarioOverride, int regControlSiteID)
        {
            try
            {
                RegistrationMetadataBL.Instance.SaveRegControlScenarioOverride(regControlegControlScenarioOverride, regControlSiteID);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure saving reg control scenario override.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure saving reg control scenario override.", ex);
            }
        }

        public Dictionary<int, List<Template>> GetTemplatesBySite()
        {
            try
            {
                return RegistrationMetadataBL.Instance.GetTemplatesBySite();
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving registration templates.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving registration templates.", ex);
            }
        }

        public TemplateSaveResult CreateTemplate(Template regTemplate)
        {
            TemplateSaveResult templateSaveResult = null;
            try
            {
                templateSaveResult = RegistrationMetadataBL.Instance.CreateTemplate(regTemplate);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure creating reg template.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure creating reg template.", ex);
            }
            return templateSaveResult;
        }

        public TemplateSaveResult RemoveTemplate(int regTemplateId, int siteId)
        {
            TemplateSaveResult templateSaveResult = null;
            try
            {
                templateSaveResult = RegistrationMetadataBL.Instance.RemoveTemplate(regTemplateId, siteId);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure removing reg template id:"+regTemplateId, ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure removing reg template id:"+regTemplateId, ex);
            }
            return templateSaveResult;
        }

        #endregion

        #region IServiceManager Members
        public void PrePopulateCache()
        {
            // no implementation at this time
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }
        }
        #endregion

        public override object InitializeLifetimeService()
        {
            return null;
        }


    }
}
