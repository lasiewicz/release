using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ValueObjects.Image;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Exceptions;

using System;
using System.Data;

namespace Matchnet.Content.ServiceManagers
{
	/// <summary>
	/// Summary description for ImageSM.
	/// </summary>
	public class ImageSM : MarshalByRefObject, IServiceManager, IImageService, IDisposable
	{
		#region constants
		private const string SERVICE_MANAGER_NAME = "ImageSM";
		#endregion

		public ImageRoot GetImageUNCRoot()
		{
			ImageRoot oResult = null;

			try 
			{
				oResult = ImageBL.Instance.GetImageUNCRoot();
			}
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving image UNC root.", ex);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving image UNC root.", ex);
			}
			return oResult;
		}

		public ImageSM()
		{
		}

		#region IServiceManager Members
		public void PrePopulateCache()
		{
			// no implementation at this time
		}
		#endregion

		#region IDisposable Members
		public void Dispose()
		{

		}

		#endregion

		public override object InitializeLifetimeService()
		{
			return null;
		}
	}
}
