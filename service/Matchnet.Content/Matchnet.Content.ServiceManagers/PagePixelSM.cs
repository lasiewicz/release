using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ValueObjects.PagePixel;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Exceptions;
using Matchnet.Data.Hydra;
using Matchnet.Replication;
using Matchnet.CacheSynchronization;
using Matchnet.CacheSynchronization.ValueObjects;

using System;
using System.Data;

namespace Matchnet.Content.ServiceManagers
{
	/// <summary>
	/// Summary description for PagePixelSM.
	/// </summary>
	public class PagePixelSM : MarshalByRefObject, IServiceManager, IPagePixelService, IDisposable, IReplicationRecipient
	{
		#region constants
		private const string SERVICE_MANAGER_NAME = "PagePixelSM";
		#endregion

		private HydraWriter hydraWriter; 
		internal static Replicator replicator = null;
		internal static Synchronizer synchronizer = null;
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="replicableObject"></param>
		public PagePixelSM()
		{
			// NOTE:  Only 1 instance of hydraWriter should exist per thread.  Since we are instantiating and starting
			// it here, there is no need to add it to ArticleSM, LandingPageSM, etc.  This instantiation code should
			// probably be moved to some global location, eventually.

			hydraWriter = new HydraWriter(new string[]{"mnContent"});

			hydraWriter.Start();

			PagePixelBL.Instance.SynchronizationRequested += new Matchnet.Content.BusinessLogic.PagePixelBL.SynchronizationEventHandler(PagePixelBL_SynchronizationRequested);
			PagePixelBL.Instance.ReplicationRequested += new Matchnet.Content.BusinessLogic.PagePixelBL.ReplicationEventHandler(PagePixelBL_ReplicationRequested);

			String machineName = System.Environment.MachineName;
			String overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
			if(overrideMachineName != null)
			{
				machineName = overrideMachineName;
			}

			String replicationURI = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_REPLICATION_OVERRIDE");
			if (replicationURI.Length == 0)
			{
				replicationURI = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetPartitions().GetReplicationPartnerUri(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, SERVICE_MANAGER_NAME, machineName);
			}

			replicator = new Replicator(SERVICE_MANAGER_NAME);
			replicator.SetDestinationUri(replicationURI);
			replicator.Start();

			synchronizer = new Synchronizer(ValueObjects.ServiceConstants.SERVICE_NAME);
			synchronizer.Start();
		}

		void IReplicationRecipient.Receive(IReplicable replicable)
		{
			try
			{
				PagePixelBL.Instance.CacheReplicatedObject(replicable);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ValueObjects.ServiceConstants.SERVICE_NAME, "Failure processing replication object.", ex);
			}
		}

		private void PagePixelBL_ReplicationRequested(IReplicable replicableObject)
		{
			replicator.Enqueue(replicableObject);
		}

		private void PagePixelBL_SynchronizationRequested(string key, System.Collections.Hashtable cacheReferences)
		{
			synchronizer.Enqueue(key, cacheReferences);
		}

		public PagePixelCollection RetrievePagePixels(string clientHostName, CacheReference cacheReference,
			int pageID, int siteID)
		{
			PagePixelCollection oResult = null;
	
			try
			{
				oResult = PagePixelBL.Instance.RetrievePagePixels(clientHostName, cacheReference, pageID, siteID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ValueObjects.ServiceConstants.SERVICE_NAME,"Failure retrieving page pixels.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ValueObjects.ServiceConstants.SERVICE_NAME,"Failure retrieving page pixels.", ex);
			}
			return oResult;
		}

		#region IServiceManager Members
		public void PrePopulateCache()
		{
			// no implementation at this time
		}
		#endregion

		#region IDisposable Members
		public void Dispose()
		{
			if (replicator != null)
			{
				replicator.Stop();
			}

			if (synchronizer != null)
			{
				synchronizer.Stop();
			}

			if (hydraWriter != null)
			{
				hydraWriter.Stop();
			}
		}
		#endregion

		public override object InitializeLifetimeService()
		{
			return null;
		}
	}
}
