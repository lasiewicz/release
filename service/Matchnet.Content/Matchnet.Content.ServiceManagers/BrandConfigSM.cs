using System;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ValueObjects.BrandConfig;


namespace Matchnet.Content.ServiceManagers
{
	public class BrandConfigSM : MarshalByRefObject, IBrandConfigService, IServiceManager
	{
		public BrandConfigSM()
		{
		}


		public Brands GetBrands()
		{
			try 
			{
				return BrandConfigBL.Instance.GetBrands();
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting Brands.", ex);
			}
		}

		public Sites GetSites()
		{
			try
			{
				return BrandConfigBL.Instance.GetSites();
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failture getting Sites.", ex);
			}
		}

		public override object InitializeLifetimeService()
		{
			return null;
		}


		public void PrePopulateCache()
		{
		}


		public void Dispose()
		{
		}
	}
}
