using System;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ValueObjects.PageConfig;


namespace Matchnet.Content.ServiceManagers
{
	public class PageConfigSM : MarshalByRefObject, IPageConfigService, IServiceManager
	{
		public PageConfigSM()
		{
		}


		public SitePages GetSitePages()
		{
			try 
			{
				return PageConfigBL.Instance.GetSitePages();
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting site pages.", ex);
			}
		}


		public AnalyticsPage GetAnalyticsPage(Int32 analytcisPageID)
		{
			try 
			{
				return PageConfigBL.Instance.GetAnalyticsPage(analytcisPageID);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting AnalyticsPage.", ex);
			}
		}


		public override object InitializeLifetimeService()
		{
			return null;
		}


		public void PrePopulateCache()
		{
		}


		public void Dispose()
		{
		}
	}
}
