﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;

namespace Matchnet.Content.ServiceManagers
{
    public class PushNotificationMetadataSM : MarshalByRefObject, IServiceManager, IPushNotificationMetadataService, IDisposable
    {
        #region constants
        private const string SERVICE_MANAGER_NAME = "PushNotificationMetadataSM";
        public const string SERVICE_NAME = "Matchnet.Content.Service";
        #endregion

        private HydraWriter _hydraWriter;

        public PushNotificationMetadataSM()
        {
            _hydraWriter = new HydraWriter(new string[] { "mnSystem" });
            _hydraWriter.Start();
        }

        #region IPushNotificationMetadataSAService Members

        public PushNotificationAppGroupModel GetPushNotificationAppGroupModel(AppGroupID appGroupId)
        {
            try
            {
                return PushNotificationMetadataBL.Instance.GetPushNotificationAppGroupModel(appGroupId);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving push notifications metadata.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving push notifications metadata.", ex);
            }
        }

        public Dictionary<AppGroupID, PushNotificationAppGroupModel> GetPushNotificationAppGroupModels()
        {
            try
            {
                return PushNotificationMetadataBL.Instance.GetPushNotificationAppGroupModels();
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving push notifications metadata.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving push notifications metadata.", ex);
            }
        }

        #endregion

        #region IServiceManager Members
        public void PrePopulateCache()
        {
            // no implementation at this time
        }
        #endregion

        #region IDisposable Members
        public void Dispose()
        {
            if (_hydraWriter != null)
            {
                _hydraWriter.Stop();
            }
        }
        #endregion

        public override object InitializeLifetimeService()
        {
            return null;
        }
       
    }
}
