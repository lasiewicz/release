using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Exceptions;

using System;
using System.Data;

namespace Matchnet.Content.ServiceManagers
{
	/// <summary>
	/// Summary description for RegionSM.
	/// </summary>
	public class RegionSM : MarshalByRefObject, IServiceManager, IRegionService, IDisposable
	{

		#region constants
		private const string SERVICE_MANAGER_NAME = "RegionSM";
		public const string SERVICE_NAME = "Matchnet.Content.Service";
		#endregion

		#region constructor
		public RegionSM()
		{
		}
		#endregion

		public RegionID FindRegionIdByPostalCode(int countryRegionID, string postalCode)
		{
			RegionID oResult = null;
			
			try
			{
				oResult = RegionBL.Instance.FindRegionIdByPostalCode(countryRegionID, postalCode);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure finding Region ID by Postal Code.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure finding Region ID by Postal Code.", ex);
			}
			return oResult;
		}
		
		public RegionID FindRegionIdByCity(int parentRegionID, string description, int languageID)
		{
			RegionID oResult = null;
			
			try
			{
				oResult = RegionBL.Instance.FindRegionIdByCity(parentRegionID, description, languageID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure finding Region ID by City.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure finding Region ID by City.", ex);
			}
			return oResult;
		}

		public Region RetrieveRegionByID(int regionID, int languageID) 
		{
			Region oResult = null;
			
			try
			{
				oResult = RegionBL.Instance.RetrieveRegionByID(regionID, languageID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving Region by ID.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving Region by ID.", ex);
			}
			return oResult;
		}

		/// <summary>
		/// Provides a collection of regions (cities)based on provided postal code. 
		/// </summary>
		/// <remarks>This method should only be used for US and Canada. There is no country check occuring during DB call. </remarks>
		/// <param name="postalCode"></param>
		/// <returns>Collection of Region objects representing each city in the provided zip code.</returns>
		///	   
		public RegionCollection RetrieveCitiesByPostalCode(string strPostalCode)
		{
			RegionCollection oResult = null;
			
			try
			{
				oResult = RegionBL.Instance.RetrieveCitiesByPostalCode( strPostalCode);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving Cities by postal code.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving Cities by postal code.", ex);
			}
			return oResult;
		}
		public RegionCollection RetrieveChildRegions(int parentRegionID, int languageID, string description, int translationID) 
		{
			RegionCollection oResult = null;
			
			try
			{
				oResult = RegionBL.Instance.RetrieveChildRegions(parentRegionID, languageID, description, translationID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving Child Regions.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving Child Regions.", ex);
			}
			return oResult;
		}

		public RegionCollection RetrieveCountries(int languageID)
		{
			RegionCollection oResult = null;
			
			try
			{
				oResult = RegionBL.Instance.RetrieveCountries(languageID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving Countries.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving Countries.", ex);
			}
			return oResult;
		}


		public RegionCollection RetrieveBirthCountries(int languageID)
		{
			RegionCollection oResult = null;
			
			try
			{
				oResult = RegionBL.Instance.RetrieveBirthCountries(languageID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving Countries.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving Countries.", ex);
			}
			return oResult;
		}


		public RegionLanguage RetrievePopulatedHierarchy(int regionID, int languageID, int maxDepth)
		{
			RegionLanguage oResult = null;
			
			try
			{
				oResult = RegionBL.Instance.RetrievePopulatedHierarchy(regionID, languageID, maxDepth);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving Populate Hierarchy.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving Populate Hierarchy.", ex);
			}
			return oResult;
		}

		public RegionSchoolName RetrieveSchoolName(int schoolRegionID) 
		{
			RegionSchoolName oResult = null;
			
			try
			{
				oResult = RegionBL.Instance.RetrieveSchoolName(schoolRegionID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving School Name.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving School Name.", ex);
			}
			return oResult;
		}

		public SchoolRegionCollection RetrieveSchoolParents(int schoolRegionID)
		{
			SchoolRegionCollection oResult = null;
			
			try
			{
				oResult = RegionBL.Instance.RetrieveSchoolParents(schoolRegionID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving School Parents.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving School Parents.", ex);
			}
			return oResult;
		}

		public SchoolRegionCollection RetrieveSchoolListByStateRegion(int stateRegionID)
		{
			SchoolRegionCollection oResult = null;
			
			try
			{
				oResult = RegionBL.Instance.RetrieveSchoolListByStateRegion(stateRegionID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving School List by State Region.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving School List by State Region.", ex);
			}
			return oResult;
		}

		public LanguageCollection GetLanguages()
		{
			LanguageCollection oResult = null;
			
			try
			{
				oResult = RegionBL.Instance.GetLanguages();
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving Languages.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving Languages.", ex);
			}
			return oResult;
		}

		public AreaCodeDictionary RetrieveAreaCodes() 
		{
			System.Diagnostics.Trace.WriteLine("__RetrieveAreaCodes()");
			AreaCodeDictionary oResult = null;
			
			try
			{
				oResult = RegionBL.Instance.RetrieveAreaCodes();
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving AreaCodes.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving AreaCodes.", ex);
			}
			return oResult;
		}

		public RegionAreaCodeDictionary RetrieveAreaCodesByRegion()
		{
			System.Diagnostics.Trace.WriteLine("__RetrieveAreaCodesByRegion()");
			RegionAreaCodeDictionary oResult = null;
			
			try
			{
				oResult = RegionBL.Instance.RetrieveAreaCodesByRegion();
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving AreaCodes.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving AreaCodes.", ex);
			}
			return oResult;
		}

		/// <summary>
		/// Retrieves an SEORegions object that is used for SEO pages.
		/// </summary>
		/// <returns></returns>		
		public SEORegions RetrieveSEORegions()
		{
			System.Diagnostics.Trace.WriteLine("__RetrieveSEORegions()");
			SEORegions oResult = null;
			
			try
			{
				oResult = RegionBL.Instance.RetrieveSEORegions();
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving SEORegions.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving SEORegions.", ex);
			}
			return oResult;
		}
		public DMA GetDMAByZipRegionID(int ZipRegionID)
		{
			DMA oResult = null;
			
			try
			{
				oResult = RegionBL.Instance.GetDMAByZipRegionID(ZipRegionID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving DMA for ZipRegionID=" + ZipRegionID.ToString()+ ".", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving DMA for ZipRegionID=" + ZipRegionID.ToString()+ ".", ex);
			}
			return oResult;
		}
		public DMACollection GetAllDMAS()
		{
			DMACollection oResult = null;

			try
			{
				oResult = RegionBL.Instance.GetAllDMAS();
			}
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving All DMA.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving All DMA.", ex);
			}
			return oResult;
		}
		#region IServiceManager Members
		public void PrePopulateCache()
		{
			// no implementation at this time
		}
		#endregion

		#region IDisposable Members
		public void Dispose()
		{

		}
		#endregion

		public override object InitializeLifetimeService()
		{
			return null;
		}
	}
}
