﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ValueObjects.Photos;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Exceptions;

namespace Matchnet.Content.ServiceManagers
{
    public class PhotoMetadataSM : MarshalByRefObject, IPhotoMetadataService, IServiceManager
    {
        #region constants
        private const string SERVICE_MANAGER_NAME = "PhotoMetadataSM";
        public const string SERVICE_NAME = "Matchnet.Content.Service";
        #endregion

        public override object InitializeLifetimeService()
        {
            return null;
        }

        #region IServiceManager Members
        public void PrePopulateCache()
        {
            // no implementation at this time
        }

        public void Dispose()
        {

        }
        #endregion
        
        public List<PhotoFileTypeRecord> GetPhotoFileTypeRecords()
        {
            try
            {
                return PhotoMetadataBL.Instance.GetPhotoFileTypeRecords();
            }
            catch (Exception ex)
            {

                throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving photofiletype records.", ex);
            }
            
        }
    }
}
