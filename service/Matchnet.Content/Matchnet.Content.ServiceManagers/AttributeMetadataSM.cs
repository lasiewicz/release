using System;
using System.Diagnostics;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ValueObjects.AttributeMetadata;


namespace Matchnet.Content.ServiceManagers
{
	public class AttributeMetadataSM : MarshalByRefObject, IServiceManager, IDisposable, IAttributeMetadataService
	{
		public AttributeMetadataSM()
		{
		}

		
		public override object InitializeLifetimeService()
		{
			return null;
		}

		
		public void PrePopulateCache()
		{
		}
		
		
		public Attributes GetAttributes()
		{
			try
			{
				return AttributeMetadataBL.Instance.GetAttributes();
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occurred while retrieving attribute metadata.", ex);
			}
		}


		public AttributeCollections GetAttributeCollections()
		{
			try
			{
				AttributeCollections ac = AttributeMetadataBL.Instance.GetAttributeCollections();
				return ac;
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occurred while retrieving attribute collections.", ex);
			}
		}

        public SearchStoreAttributes GetSearchStoreAttributes()
        {

            try
            {
                SearchStoreAttributes attributes = AttributeMetadataBL.Instance.GetSearchStoreAttributes();
             
                return attributes;

            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occurred while retrieving  search store attributes.", ex);
            }


        }
		#region IDisposable Members
		public void Dispose()
		{
		}
		#endregion
	}
}
