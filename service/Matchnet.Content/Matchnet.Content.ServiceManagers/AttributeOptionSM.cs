using System;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ValueObjects.AttributeOption;


namespace Matchnet.Content.ServiceManagers
{
	public class AttributeOptionSM : MarshalByRefObject, IAttributeOptionService, IServiceManager
	{
		public AttributeOptionSM()
		{
		}

		public override object InitializeLifetimeService()
		{
			return null;
		}


		public void PrePopulateCache()
		{
		}


		public Attributes GetAttributes()
		{
			try 
			{
				return AttributeOptionBL.Instance.GetAttributes();
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting attribute options.", ex);
			}
		}

		public void SaveAttributeOption(int pAttributeOptionID, int pAttributeID, string pDescription, int pAttributeValue, string pResourceKey, int pListOrder)
		{
			try 
			{
				AttributeOptionBL.Instance.SaveAttributeOption(pAttributeOptionID, pAttributeID, pDescription, pAttributeValue, pResourceKey, pListOrder);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving attribute option.", ex);
			}
		}

		public void SaveAttributeOptionGroup(int pAttributeOptionGroupID, int pAttributeOptionID, int pGroupID, int pListOrder)
		{
			try 
			{
				AttributeOptionBL.Instance.SaveAttributeOptionGroup(pAttributeOptionGroupID, pAttributeOptionID, pGroupID, pListOrder);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving attribute option group.", ex);
			}
		}

		public void DeleteAttributeOption(int pAttributeOptionID)
		{
			try 
			{
				AttributeOptionBL.Instance.DeleteAttributeOption(pAttributeOptionID);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure deleting attribute option.", ex);
			}
		}

		public void DeleteAttributeOptionGroup(int pAttributeOptionID, int pGroupID)
		{
			try 
			{
				AttributeOptionBL.Instance.DeleteAttributeOptionGroup(pAttributeOptionID, pGroupID);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure deleting attribute option group.", ex);
			}
		}

		public void Dispose()
		{
		}
	}
}
