using System;
using Matchnet.Content.ValueObjects.Registration;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.ServiceDefinitions;


namespace Matchnet.Content.ServiceManagers
{
	/// <summary>
	/// Summary description for AdminSM.
	/// </summary>
	public class AdminSM : MarshalByRefObject, IServiceManager, IAdminService, IDisposable
	{
		private const string SERVICE_MANAGER_NAME = "AdminSM";
		private HydraWriter _hydraWriter;

		public AdminSM()
		{
			_hydraWriter = new HydraWriter(new string[]{"mnAdmin"});
			_hydraWriter.Start();
		}


		public void PrePopulateCache()
		{
			// no implementation at this time
		}

        public AdminActionReasonID GetLastAdminSuspendReasonForMember(int memberID)
        {
            try
            {
                return AdminBL.Instance.GetLastAdminSuspendReasonForMember(memberID);
            }
            catch (ExceptionBase ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving Last Admin Suspend Reason For Member.", ex);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving Last Admin Suspend Reason For Member.", ex);
            }
        }

		public AdminActionTypeCollection GetAdminActionTypes()
		{
			AdminActionTypeCollection oResult = null;
			
			try
			{
				oResult = AdminBL.Instance.GetAdminActionTypes();
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving admin action types.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving admin action types.", ex);
			}
			return oResult;
		}

		public AdminActionReasonCollection GetAdminActionReasons()
		{
			AdminActionReasonCollection oResult = null;
			
			try
			{
				oResult = AdminBL.Instance.GetAdminActionReasons();
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving admin action reasons.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving admin action reasons.", ex);
			}
			return oResult;
		}
				
		public AdminActivityContainerCollection GetAdminActivity(int memberID, int communityID)
		{
			AdminActivityContainerCollection oResult = null;
			
			try
			{
				oResult = AdminBL.Instance.GetAdminActivity(memberID, communityID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving admin activity.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving admin activity.", ex);
			}
			return oResult;
		}

		public AdminActionLogCollection GetAdminActionLog(int memberID, int communityID)
		{
			AdminActionLogCollection oResult = null;
			
			try
			{
				oResult = AdminBL.Instance.GetAdminActionLog(memberID, communityID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving admin action log.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving admin action log.", ex);
			}
			return oResult;
		}

		public AdminActionCollection GetAdminActions(int memberID, int communityID)
		{
			AdminActionCollection oResult = null;
			
			try
			{
				oResult = AdminBL.Instance.GetAdminActions(memberID, communityID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving admin action log.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving admin action log.", ex);
			}
			return oResult;
		}

		public AdminActionReport GetAdminActionReport(DateTime startDate, DateTime endDate, Int32 adminMemberID, Int32 groupID, Int32 adminActionMask)
		{
			AdminActionReport oResult = null;
			
			try
			{
				oResult = AdminBL.Instance.GetAdminActionReport(startDate, endDate, adminMemberID, groupID, adminActionMask);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving admin action report.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving admin action report.", ex);
			}
			return oResult;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="startDate"></param>
		/// <param name="endDate"></param>
		/// <param name="adminMemberID"></param>
		/// <param name="groupID"></param>
		/// <returns></returns>
		public ApprovalCountsCollection GetApprovalCountsReport(DateTime startDate, DateTime endDate, Int32 adminMemberID, Int32 groupID)
		{
			ApprovalCountsCollection oResult = null;
			
			try
			{
				oResult = AdminBL.Instance.GetApprovalCountsReport(startDate, endDate, adminMemberID, groupID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving admin approval counts report.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieving admin approval counts report.", ex);
			}
			return oResult;
		}

		public void AdminActionLogInsert(int memberID, int communityID, int adminActionMask, int adminMemberID, int languageID)
		{
			// note: languageID deprecated!
			try
			{
				AdminBL.Instance.AdminActionLogInsert(memberID, communityID, adminActionMask, adminMemberID, AdminMemberIDType.AdminProfileMemberID, AdminActionReasonID.None);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure inserting admin action log.", ex);
			}
		}
		
		public void AdminActionLogInsert(int memberID, int communityID, int adminActionMask, int adminMemberID, AdminMemberIDType adminMemberIDType)
		{
			// note: languageID deprecated!
			try
			{
				AdminBL.Instance.AdminActionLogInsert(memberID, communityID, adminActionMask, adminMemberID, adminMemberIDType, AdminActionReasonID.None);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure inserting admin action log.", ex);
			}
		}

		public void AdminActionLogInsert(int memberID, int communityID, int adminActionMask, int adminMemberID, AdminMemberIDType adminMemberIDType, AdminActionReasonID adminActionReasonID)
		{
			// note: languageID deprecated!
			try
			{
				AdminBL.Instance.AdminActionLogInsert(memberID, communityID, adminActionMask, adminMemberID, adminMemberIDType, adminActionReasonID);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure inserting admin action log.", ex);
			}
		}

        public void RegistrationAdminActionLogInsert(string adminAccountName, RegAdminActionType actionType, int scenarioId)
        {
            try
            {
                AdminBL.Instance.RegistrationAdminActionLogInsert(adminAccountName, actionType, scenarioId);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure inserting reg admin action log.", ex);
            }
        }

        public void RegistrationAdminScenarioHistorySave(int regScenarioId, int deviceTypeID, int siteID, double weight, string updatedBy)
        {
            try
            {
                AdminBL.Instance.RegistrationAdminScenarioHistorySave(regScenarioId, deviceTypeID, siteID, weight, DateTime.MinValue, DateTime.MinValue, updatedBy);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving reg admin scenario history.", ex);
            }
        }

		public void UpdateAdminNote(string newNote, int targetMemberID, int memberID, int communityID, int maxNoteLength)
		{
			bool result = false;

			try
			{
				AdminBL.Instance.UpdateAdminNote(newNote, targetMemberID, memberID, communityID, maxNoteLength);
			}
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure updating admin note.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure updating admin note.", ex);
			}
		}


		public AdminNoteCollection LoadAdminNotes(int targetMemberID, int communityID, int startRow, int pageSize, int totalRows)
		{
			AdminNoteCollection oResult = null;

			try
			{
				oResult = AdminBL.Instance.LoadAdminNotes(targetMemberID, communityID, startRow, pageSize, totalRows);
			}
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure loading admin notes.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure loading admin notes.", ex);
			}
			return oResult;
		}


		#region IDisposable Members
		public void Dispose()
		{
			if (_hydraWriter != null)
			{
				_hydraWriter.Stop();
			}
		}
		#endregion

		public override object InitializeLifetimeService()
		{
			return null;
		}

		#region Publish
		public Int32 SavePublish(Int32 publishObjectID,
			PublishObjectType publishObjectType,
			Object content)
		{
			Int32 oResult = Constants.NULL_INT;
			
			try
			{
				oResult = AdminBL.Instance.SavePublish(publishObjectID, publishObjectType, content);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving Publish record.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving Publish record.", ex);
			}

			return oResult;
		}

		public void SavePublishAction(Int32 publishID,
			Int32 publishActionPublishObjectID,
			Int32 memberID)
		{
			try
			{
				AdminBL.Instance.SavePublishAction(publishID, publishActionPublishObjectID, memberID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving PublishAction record.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure saving PublishAction record.", ex);
			}
		}

		public PublishActionPublishObjectCollection GetPublishActionPublishObjects()
		{
			PublishActionPublishObjectCollection oResult = null;

			try
			{
				oResult = AdminBL.Instance.GetPublishActionPublishObjects();
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting PublishActionPublishObjects.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting PublishActionPublishObjects.", ex);
			}

			return oResult;
		}

		public PublishCollection GetPublishes(DateTime startDate, DateTime endDate)
		{
			PublishCollection oResult = null;

			try
			{
				oResult = AdminBL.Instance.GetPublishes(startDate, endDate);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting PublishCollection.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting PublishCollection.", ex);
			}

			return oResult;
		}

		public Publish GetPublish(Int32 publishObjectID, PublishObjectType publishObjectType)
		{
			Publish oResult = null;

			try
			{
				oResult = AdminBL.Instance.GetPublish(publishObjectID, publishObjectType);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting Publish.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure getting Publish.", ex);
			}

			return oResult;
		}

		public void GetNextPublishActionPublishObjectStep(Int32 publishObjectID, PublishObjectType publishObjectType, out Int32 publishActionPublishObjectID, out ServiceEnvironmentTypeEnum nextServiceEnvironmentTypeEnum)
		{
			try
			{
				AdminBL.Instance.GetNextPublishActionPublishObjectStep(publishObjectID, publishObjectType, out publishActionPublishObjectID, out nextServiceEnvironmentTypeEnum);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure in GetNextPublishActionPublishObjectStep.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure in GetNextPublishActionPublishObjectStep.", ex);
			}
		}
		#endregion
	}
}
