using System;
using System.Collections.Generic;

using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.LandingPage;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;
using Matchnet.Replication;
using Matchnet.CacheSynchronization;
using Matchnet.CacheSynchronization.ValueObjects;

namespace Matchnet.Content.ServiceManagers
{
	/// <summary>
	/// Summary description for LandingPageSM.
	/// </summary>
	public class LandingPageSM : MarshalByRefObject, ILandingPageService, IServiceManager
	{
		#region constants
		private const string SERVICE_MANAGER_NAME = "LandingPageSM";
		#endregion

		public LandingPageSM()
		{
			String machineName = System.Environment.MachineName;
			String overrideMachineName = Matchnet.InitialConfiguration.InitializationSettings.Get("configuration/settings/MachineNameOverride");
			if(overrideMachineName != null)
			{
				machineName = overrideMachineName;
			}
		}

		public override object InitializeLifetimeService()
		{
			return null;
		}

		public void PrePopulateCache()
		{
		}

		#region ILandingPageService Implementation

        public List<TemplateBasedLandingPage> GetLandingPages(Boolean forceLoadFlag)
		{
            try
            {
                return LandingPageBL.Instance.GetLandingPages(forceLoadFlag);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure in GetLandingPages.", ex);
            }
		}

        public LandingPage GetLandingPage(int landingPageID)
        {
            try
            {
                return LandingPageBL.Instance.GetLandingPage(landingPageID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure in Get LandingPage.", ex);
            }
        }

        public List<LandingPageTemplate> GetLandingPageTemplates()
        {
            try
            {
                return LandingPageBL.Instance.GetLandingPageTemplates();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure in GetLandingPageTemplates.", ex);
            }
        }

        public TemplateBasedLandingPage GetTemplateBasedLandingPage(int landingPageID)
		{
            try
            {
                return LandingPageBL.Instance.GetTemplateBasedLandingPage(landingPageID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure in GetTemplateBasedLandingPage.", ex);
            }
		}

        public TemplateBasedLandingPage GetTemplateBasedLandingPageByURL(string url)
		{
            try
            {
                return LandingPageBL.Instance.GetTemplateBasedLandingPageByURL(url);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure in GetTemplateBasedLandingPageByURL.", ex);
            }
		}

        public bool CheckNameAvailability(string name, int existingID, CheckAvailabilityType type)
        {
            try
            {
                return LandingPageBL.Instance.CheckNameAvailability(name, existingID, type);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure in CheckNameAvailibility.", ex);
            }
        }

        public void SaveLandingPageTemplate(LandingPageTemplate template)
        {
            try
            {
                LandingPageBL.Instance.SaveLandingPageTemplate(template);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure in SaveLandingPageTemplate.", ex);
            }
        }

        public void SaveLandingPage(TemplateBasedLandingPage page)
        {
            try
            {
                LandingPageBL.Instance.SaveLandingPage(page);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure in SaveLandingPage.", ex);
            }
        }

        
        public void SaveUnifiedLandingPageIDForLandingPage(int landingPageID, int? unifiedLandingPageID)
        {
            try
            {
                LandingPageBL.Instance.SaveUnifiedLandingPageIDForLandingPage(landingPageID,unifiedLandingPageID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure in SaveUnifiedLandingPageIDForLandingPage.", ex);
            }
        }

		public void DeleteLandingPage(int pLandingPageID)
		{
			try 
			{
				LandingPageBL.Instance.DeleteLandingPage(pLandingPageID);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure deleting Landing Page.", ex);
			}
		}

        public void DeleteTemplate(int templateID)
        {
            try
            {
                LandingPageBL.Instance.DeleteTemplate(templateID);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure deleting template.", ex);
            }
        }

        public LandingPageCollection GetLandingPages(string clientHostName, CacheReference cacheReference, Boolean forceLoadFlag)
        {
            try
            {
                return LandingPageBL.Instance.GetLandingPages(clientHostName, null, forceLoadFlag);
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Failure retrieve old Landing Pages.", ex);
            }
        }
		#endregion

		public void Dispose()
		{
			
		}
	}
}
