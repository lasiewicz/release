using System;
using System.Diagnostics;

using Matchnet;
using Matchnet.Exceptions;
using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ValueObjects.Quotas;

namespace Matchnet.Content.ServiceManagers
{
	public class QuotaDefinitionSM : MarshalByRefObject, IServiceManager, IDisposable, IQuotaDefinitionService
	{
		public QuotaDefinitionSM()
		{
		}


		public override object InitializeLifetimeService()
		{
			return null;
		}

		
		public void PrePopulateCache()
		{
		}


		public QuotaDefinitions GetQuotaDefinitions()
		{
			try
			{
				return QuotaDefinitionBL.Instance.GetQuotaDefinitions();
			}
			catch (Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occurred while retrieving quota definitions.", ex);
			}
		}


		public void Dispose()
		{
		}
	}
}
