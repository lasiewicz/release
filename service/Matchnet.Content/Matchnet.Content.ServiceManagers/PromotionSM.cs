using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ValueObjects.Promotion;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Exceptions;

using System;
using System.Data;

namespace Matchnet.Content.ServiceManagers
{
	/// <summary>
	/// Summary description for PromotionSM.
	/// </summary>
	public class PromotionSM : MarshalByRefObject, IServiceManager, IPromotionService, IDisposable
	{

		#region constants
		private const string SERVICE_MANAGER_NAME = "PromotionSM";
		public const string SERVICE_NAME = "Matchnet.Content.Service";
		#endregion

		#region public methods
		public RegistrationPromotionCollection RetrieveRegistrationPromotions(int communityID, int brandID) 
		{
			RegistrationPromotionCollection oResult = null;
			
			try
			{
				oResult = PromotionBL.Instance.RetrieveRegistrationPromotions(communityID, brandID);
			} 
			catch(ExceptionBase ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving registration promotions.", ex);
			} 
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME,"Failure retrieving registration promotions.", ex);
			}
			return oResult;
		}

        [Obsolete]
		public BannerCollection RetrieveBanners(int groupID)
		{
			BannerCollection oResult = null;

            /*REMOVED as part of DB efforts to clean up database*/
            /*
			try
			{
				oResult = PromotionBL.Instance.RetrieveBanners(groupID);
			}
			catch(ExceptionBase ex)
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Failture retrieving banners.", ex);
			}
			catch(Exception ex) 
			{
				throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving banners.", ex);
			}
             */
			return oResult;
		}

        /// <summary>
        /// Retrieves a list of PromotionID and URL Referral mapping
        /// </summary>
        public PRMURLMapperList GetPRMURLMapperList()
        {
            PRMURLMapperList oResult = null;

            try
            {
                oResult = PromotionBL.Instance.GetPRMURLMapperList();
            }
            catch (Exception ex)
            {
                throw new ServiceBoundaryException(SERVICE_NAME, "Failure retrieving PRMURLMapperList.", ex);
            }
            return oResult;
        }

		#endregion

		#region IServiceManager Members
		public void PrePopulateCache()
		{
			// no implementation at this time
		}
		#endregion

		#region IDisposable Members
		public void Dispose()
		{

		}
		#endregion

		#region constructor
		public PromotionSM()
		{}
		#endregion

		public override object InitializeLifetimeService()
		{
			return null;
		}

	}
}
