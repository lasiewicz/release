﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.Registration;
using Matchnet.Content.ValueObjects.ServiceDefinitions;

namespace Matchnet.Content.Tests.Mocks
{
    public class RegistrationMetadataServiceMock : IRegistrationMetadata
    {
        #region IRegistrationMetadata Members

        private List<Scenario> _scenarios;

        public List<Scenario> GetScenarios()
        {
            return _scenarios;
        }

        public List<Scenario> GetScenariosForAdmin()
        {
            return _scenarios;
        }


        public void SetScenarios(List<Scenario> scenarios)
        {
            _scenarios = scenarios;
        }


        public List<Scenario> GetAllRegScenarios()
        {
            throw new NotImplementedException();
        }


        public void ToggleRegScenario(int regScenarioId)
        {
            throw new NotImplementedException();
        }


        public void ToggleRegScenario(int regScenarioId, string updatedBy)
        {
            throw new NotImplementedException();
        }

        public List<RegControl> GetAllRegControls()
        {
            throw new NotImplementedException();
        }


        List<Scenario> IRegistrationMetadata.GetScenarios()
        {
            throw new NotImplementedException();
        }

        List<Scenario> IRegistrationMetadata.GetScenariosForAdmin()
        {
            throw new NotImplementedException();
        }

        List<Scenario> IRegistrationMetadata.GetAllRegScenarios()
        {
            throw new NotImplementedException();
        }

        void IRegistrationMetadata.SaveScenarioStatusWeight(int regScenarioId, bool activate, double weight, string updatedBy, int siteID, int deviceTypeID)
        {
            throw new NotImplementedException();
        }

        List<RegControl> IRegistrationMetadata.GetAllRegControls(int deviceTypeId)
        {
            throw new NotImplementedException();
        }

        List<RegControl> IRegistrationMetadata.GetRegControlsRequiredForRegScenarioSite(int siteId)
        {
            throw new NotImplementedException();
        }

        int IRegistrationMetadata.CreateRegScenario(Scenario scenario)
        {
            throw new NotImplementedException();
        }

        void IRegistrationMetadata.SaveRegControlScenarioOverride(RegControlScenarioOverride regControlegControlScenarioOverride, int regControlSiteID)
        {
            throw new NotImplementedException();
        }

        void IRegistrationMetadata.SaveRegStep(Step step, int scenarioId)
        {
            throw new NotImplementedException();
        }

        Dictionary<int, List<Template>> IRegistrationMetadata.GetTemplatesBySite()
        {
            throw new NotImplementedException();
        }

        public void SaveScenario(Scenario scenario)
        {
            throw new NotImplementedException();
        }

        public TemplateSaveResult CreateTemplate(Template regTemplate)
        {
            throw new NotImplementedException();
        }

        public TemplateSaveResult RemoveTemplate(int regTemplateId, int siteId)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
