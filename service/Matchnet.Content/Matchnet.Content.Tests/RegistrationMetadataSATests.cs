﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.Tests.Mocks;
using Matchnet.Content.ValueObjects.Registration;
using NUnit.Framework;

namespace Matchnet.Content.Tests
{
    public  class RegistrationMetadataSATests
    {
        [Test]
        public void LoadScenarioBasicTest()
        {
            var serviceMock = new RegistrationMetadataServiceMock();
            serviceMock.SetScenarios(GenerateBasicTestScenarios());
            RegistrationMetadataSA.Instance.SetService(serviceMock);

            RegistrationMetadataSA.Instance.ClearMetadataCache();
            List<Scenario> scenarios = RegistrationMetadataSA.Instance.GetScenariosBySite(103);
            Assert.IsNotNull(scenarios);
            Assert.IsTrue(scenarios.Count == 2);
        }

        [Test]
        public void GetScenariosForAdminTest()
        {
            var serviceMock = new RegistrationMetadataServiceMock();
            RegistrationMetadataSA.Instance.ClearMetadataCache();

            var scenarios = RegistrationMetadataSA.Instance.GetScenariosForAdmin();
            Assert.IsNotNull(scenarios);
        }

        [Test]
        public void GetRandomScenarioBySiteBasicTest()
        {
            var serviceMock = new RegistrationMetadataServiceMock();
            serviceMock.SetScenarios(GenerateBasicTestScenarios());
            RegistrationMetadataSA.Instance.SetService(serviceMock);

            RegistrationMetadataSA.Instance.ClearMetadataCache();
            var scenario = RegistrationMetadataSA.Instance.GetRandomScenarioBySite(103);
            Assert.IsNotNull(scenario);
        }

        [Test]
        [ExpectedException()]
        public void GetRandomScenarioBySiteThrowsExceptionForNullScenarios()
        {
            
            var serviceMock = new RegistrationMetadataServiceMock();
            RegistrationMetadataSA.Instance.SetService(serviceMock);
            serviceMock.SetScenarios(null);

            RegistrationMetadataSA.Instance.ClearMetadataCache();
            var scenario = RegistrationMetadataSA.Instance.GetRandomScenarioBySite(103);
        }

        [Test]
        [ExpectedException()]
        public void GetRandomScenarioBySiteThrowsExceptionForEmptyScenarios()
        {

            var serviceMock = new RegistrationMetadataServiceMock();
            RegistrationMetadataSA.Instance.SetService(serviceMock);
            serviceMock.SetScenarios(new List<Scenario>());

            RegistrationMetadataSA.Instance.ClearMetadataCache();
            var scenario = RegistrationMetadataSA.Instance.GetRandomScenarioBySite(103);
        }

        [Test]
        public void GetRandomScenarioHandlesIncorrectWeighting()
        {
            var serviceMock = new RegistrationMetadataServiceMock();
            RegistrationMetadataSA.Instance.SetService(serviceMock);
            serviceMock.SetScenarios(GenerateIncorrectlyWeightedTestScenarios());

            RegistrationMetadataSA.Instance.ClearMetadataCache();
            var scenario = RegistrationMetadataSA.Instance.GetRandomScenarioBySite(103);
            Assert.IsNotNull(scenario);
        }

        [Test]
        public void GetRandomScenarioDoesNotReturnZeroWeightedScenarios()
        {
            var serviceMock = new RegistrationMetadataServiceMock();
            RegistrationMetadataSA.Instance.SetService(serviceMock);
            serviceMock.SetScenarios(GenerateWeightedTestScenariosWithZeroWeightedScenario());

            RegistrationMetadataSA.Instance.ClearMetadataCache();
            var zeroWeightedScenarioReturned = false;

            //loop through a couple of hundred times to feel sure that 0 weighted scenario is not returned
            for (int x = 1; x < 200; x++)
            {
                var scenario = RegistrationMetadataSA.Instance.GetRandomScenarioBySite(103);
                if(scenario.Weight == 0)
                {
                    zeroWeightedScenarioReturned = true;
                }
            }
            
            Assert.IsFalse(zeroWeightedScenarioReturned);
        }

        [Test]
        [ExpectedException()]
        public void GetRandomScenarioBySiteAndDeviceThrowsExceptionForNullScenarios()
        {

            var serviceMock = new RegistrationMetadataServiceMock();
            RegistrationMetadataSA.Instance.SetService(serviceMock);
            serviceMock.SetScenarios(null);

            RegistrationMetadataSA.Instance.ClearMetadataCache();
            var scenario = RegistrationMetadataSA.Instance.GetRandomScenarioBySiteAndDeviceType(103, DeviceType.Desktop);
        }

        [Test]
        [ExpectedException()]
        public void GetRandomScenarioBySiteAndDeviceThrowsExceptionForEmptyScenarios()
        {
            var serviceMock = new RegistrationMetadataServiceMock();
            RegistrationMetadataSA.Instance.SetService(serviceMock);
            serviceMock.SetScenarios(new List<Scenario>());

            RegistrationMetadataSA.Instance.ClearMetadataCache();
            var scenario = RegistrationMetadataSA.Instance.GetRandomScenarioBySiteAndDeviceType(103, DeviceType.Desktop);
        }

        [Test]
        public void GetRandomScenarioByDeviceTypeReturnsCorrectType()
        {
            var serviceMock = new RegistrationMetadataServiceMock();
            RegistrationMetadataSA.Instance.SetService(serviceMock);
            serviceMock.SetScenarios(GenerateScenariosWithDeviceTypes());

            RegistrationMetadataSA.Instance.ClearMetadataCache();
            var scenario = RegistrationMetadataSA.Instance.GetRandomScenarioBySiteAndDeviceType(103, DeviceType.Desktop);
            Assert.IsNotNull(scenario);
            Assert.IsTrue(scenario.DeviceType == DeviceType.Desktop);

            scenario = RegistrationMetadataSA.Instance.GetRandomScenarioBySiteAndDeviceType(103, DeviceType.Handeheld);
            Assert.IsNotNull(scenario);
            Assert.IsTrue(scenario.DeviceType == DeviceType.Handeheld);

            scenario = RegistrationMetadataSA.Instance.GetRandomScenarioBySiteAndDeviceType(103, DeviceType.Tablet);
            Assert.IsNotNull(scenario);
            Assert.IsTrue(scenario.DeviceType == DeviceType.Tablet);
        }

        [Test]
        public void GetRandomScenarioByDeviceTypeHandlesIncorrectWeighting()
        {
            var serviceMock = new RegistrationMetadataServiceMock();
            RegistrationMetadataSA.Instance.SetService(serviceMock);

            var scenarios = GenerateScenariosWithDeviceTypes();
            scenarios[0].Weight = .9;
            serviceMock.SetScenarios(scenarios);

            RegistrationMetadataSA.Instance.ClearMetadataCache();
            var scenario = RegistrationMetadataSA.Instance.GetRandomScenarioBySiteAndDeviceType(103, DeviceType.Desktop);
            Assert.IsNotNull(scenario);
            Assert.IsTrue(scenario.DeviceType == DeviceType.Desktop);
        }

        [Test]
        public void GetRandomScenarioByDeviceTypeHandlesMissingDeviceType()
        {
            var serviceMock = new RegistrationMetadataServiceMock();
            RegistrationMetadataSA.Instance.SetService(serviceMock);

            var scenarios = GenerateScenariosWithDeviceTypes();
            serviceMock.SetScenarios(scenarios);
            scenarios[4].DeviceType = DeviceType.Desktop;

            RegistrationMetadataSA.Instance.ClearMetadataCache();
            var scenario = RegistrationMetadataSA.Instance.GetRandomScenarioBySiteAndDeviceType(103, DeviceType.Tablet);
            Assert.IsNotNull(scenario);
            Assert.IsTrue(scenario.DeviceType != DeviceType.Tablet);
        }

        private List<Scenario> GenerateBasicTestScenarios()
        {
            List<Scenario> scenarios = new List<Scenario>();
            scenarios.Add(new Scenario(1, 103, "scenario1", false, 

                new Template(TemplateType.Splash, "splash1"),
                new Template(TemplateType.Registration, "registration1"), 
                new Template(TemplateType.Confirmation, "confirmation1"), 
                .50, true, DeviceType.Handeheld, ExternalSessionType.Bedrock, null));

            scenarios.Add(new Scenario(2, 103, "scenario2", false,
                new Template(TemplateType.Splash, "splash1"),
                new Template(TemplateType.Registration, "registration1"),
                new Template(TemplateType.Confirmation, "confirmation1"),
                .50, true, DeviceType.Handeheld, ExternalSessionType.Bedrock, null));

            return scenarios;
        }

        private List<Scenario> GenerateIncorrectlyWeightedTestScenarios()
        {
            List<Scenario> scenarios = new List<Scenario>();
            scenarios.Add(new Scenario(1, 103, "scenario1", false,

                new Template(TemplateType.Splash, "splash1"),
                new Template(TemplateType.Registration, "registration1"),
                new Template(TemplateType.Confirmation, "confirmation1"),
                .70, true, DeviceType.Handeheld, ExternalSessionType.Bedrock, null));

            scenarios.Add(new Scenario(2, 103, "scenario2", false,
                new Template(TemplateType.Splash, "splash1"),
                new Template(TemplateType.Registration, "registration1"),
                new Template(TemplateType.Confirmation, "confirmation1"),
                .50, true, DeviceType.Handeheld, ExternalSessionType.Bedrock, null));

            return scenarios;
        }

        private List<Scenario> GenerateWeightedTestScenariosWithZeroWeightedScenario()
        {
            List<Scenario> scenarios = new List<Scenario>();
            scenarios.Add(new Scenario(1, 103, "scenario1", false,

                new Template(TemplateType.Splash, "splash1"),
                new Template(TemplateType.Registration, "registration1"),
                new Template(TemplateType.Confirmation, "confirmation1"),
                .50, true, DeviceType.Handeheld, ExternalSessionType.Bedrock, null));

            scenarios.Add(new Scenario(2, 103, "scenario2", false,
                new Template(TemplateType.Splash, "splash1"),
                new Template(TemplateType.Registration, "registration1"),
                new Template(TemplateType.Confirmation, "confirmation1"),
                .50, true, DeviceType.Handeheld, ExternalSessionType.Bedrock, null));

            scenarios.Add(new Scenario(3, 103, "scenario3", false,
                new Template(TemplateType.Splash, "splash1"),
                new Template(TemplateType.Registration, "registration1"),
                new Template(TemplateType.Confirmation, "confirmation1"),
                0, true, DeviceType.Handeheld, ExternalSessionType.Bedrock, null));

            return scenarios;
        }

        private List<Scenario> GenerateScenariosWithDeviceTypes()
        {
            List<Scenario> scenarios = new List<Scenario>();
            scenarios.Add(new Scenario(1, 103, "scenario1", false,

                new Template(TemplateType.Splash, "splash1"),
                new Template(TemplateType.Registration, "registration1"),
                new Template(TemplateType.Confirmation, "confirmation1"),
                .50, true, DeviceType.Desktop, ExternalSessionType.Bedrock, null));

            scenarios.Add(new Scenario(2, 103, "scenario2", false,
                new Template(TemplateType.Splash, "splash1"),
                new Template(TemplateType.Registration, "registration1"),
                new Template(TemplateType.Confirmation, "confirmation1"),
                .50, true, DeviceType.Desktop, ExternalSessionType.Bedrock, null));

            scenarios.Add(new Scenario(3, 103, "scenario3", false,
                new Template(TemplateType.Splash, "splash1"),
                new Template(TemplateType.Registration, "registration1"),
                new Template(TemplateType.Confirmation, "confirmation1"),
                .75, true, DeviceType.Handeheld, ExternalSessionType.Bedrock, null));

            scenarios.Add(new Scenario(4, 103, "scenario4", false,
                new Template(TemplateType.Splash, "splash1"),
                new Template(TemplateType.Registration, "registration1"),
                new Template(TemplateType.Confirmation, "confirmation1"),
                .25, true, DeviceType.Handeheld, ExternalSessionType.Bedrock, null));

            scenarios.Add(new Scenario(5, 103, "scenario5", false,
                new Template(TemplateType.Splash, "splash1"),
                new Template(TemplateType.Registration, "registration1"),
                new Template(TemplateType.Confirmation, "confirmation1"),
                1, true, DeviceType.Tablet, ExternalSessionType.Bedrock, null));

            return scenarios;
        }

    }
}
