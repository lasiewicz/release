﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ValueObjects.Registration;

namespace Matchnet.Content.Tests
{
    [TestFixture]
    public class RegistrationBLTests
    {
        [Test]
        public void LoadScenarioTest()
        {
            List<Scenario> scenarios = RegistrationMetadataBL.Instance.GetScenarios();
            int x = 1;
        }

        [Test]
        public void GetAllJdateComRegControlsRequiredForRegScenario()
        {
            List<RegControl> regControlIDsRequiredForRegScenarioSite = RegistrationMetadataBL.Instance.GetRegControlsRequiredForRegScenarioSite(103);
            Assert.IsNotNull(regControlIDsRequiredForRegScenarioSite);
            Assert.Greater(regControlIDsRequiredForRegScenarioSite.Count, 0);
            Assert.IsTrue(AssertRegControlExists(3, regControlIDsRequiredForRegScenarioSite));
            Assert.IsTrue(AssertRegControlExists(14, regControlIDsRequiredForRegScenarioSite));
            Assert.IsTrue(AssertRegControlExists(15, regControlIDsRequiredForRegScenarioSite));
            Assert.IsTrue(AssertRegControlExists(16, regControlIDsRequiredForRegScenarioSite));
            Assert.IsTrue(AssertRegControlExists(17, regControlIDsRequiredForRegScenarioSite));
            Assert.IsTrue(AssertRegControlExists(18, regControlIDsRequiredForRegScenarioSite));
            Assert.IsTrue(AssertRegControlExists(19, regControlIDsRequiredForRegScenarioSite));
        }

        [Test]
        public void RegTemplateInsertAvoidDuplicates()
        {
            var random = new Random();
            string templateName = "splash" + random.Next();
            Template regTemplate = new Template(101, TemplateType.Splash, templateName);
            TemplateSaveResult templateSaveResult = RegistrationMetadataBL.Instance.CreateTemplate(regTemplate);
            Assert.IsNotNull(templateSaveResult);
            Assert.AreEqual(TemplateResultType.Success, templateSaveResult.ResultType);
            Assert.Greater(templateSaveResult.RegTemplate.ID, 0);
            System.Threading.Thread.Sleep(3000);
            TemplateSaveResult templateSaveResult2 = RegistrationMetadataBL.Instance.CreateTemplate(regTemplate);
            Assert.IsNotNull(templateSaveResult2);
            Assert.AreEqual(TemplateResultType.TemplateAlreadyExistsForTypeAndSite, templateSaveResult2.ResultType);
        }

        [Test]
        public void RegTemplateRemoveAvoidTemplatesInUse()
        {
            int regTemplateId = 7;
            int siteId = 101;
            TemplateSaveResult templateSaveResult = RegistrationMetadataBL.Instance.RemoveTemplate(regTemplateId,siteId);
            Assert.IsNotNull(templateSaveResult);
            Assert.AreEqual(TemplateResultType.TemplateInUseByRegScenario, templateSaveResult.ResultType);
        }


        private bool AssertRegControlExists(int regControlId, List<RegControl> regControlIDsRequiredForRegScenarioSite)
        {
            bool match = false;
            foreach (RegControl rc in regControlIDsRequiredForRegScenarioSite)
            {
                if (rc.RegControlID == regControlId)
                {
                    match = true;
                    break;
                }
            }
            return match;
        }
    }
}
