﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Content.BusinessLogic;
using Matchnet.Content.ValueObjects.PushNotification;
using NUnit.Framework;

namespace Matchnet.Content.Tests
{
    [TestFixture]
    public class PushNotificationMetadataBLTest
    {
        [Test]
        public void TestGetPushNotificationAppGroupModel()
        {
            PushNotificationAppGroupModel pushNotificationApp =
                PushNotificationMetadataBL.Instance.GetPushNotificationAppGroupModel(AppGroupID.JDateAppGroup);

            Assert.IsNotNull(pushNotificationApp, "PushNotificationAppGroupModel is null");
            Assert.IsTrue(pushNotificationApp.AppGroupId == AppGroupID.JDateAppGroup, "Wrong pushNotificationApp.AppId");
            Assert.IsNotNull(pushNotificationApp.NotificationAppGroupCategories, "NotificationAppGroupCategories is null");
            Assert.IsNotNull(pushNotificationApp.NotificationTypes, "NotificationTypes is null");

            var notificationApps = PushNotificationMetadataBL.Instance.GetPushNotificationAppGroupModels();
            Assert.IsNotNull(notificationApps[AppGroupID.JDateAppGroup]);
        }
    }
}
