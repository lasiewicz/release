using Matchnet.Caching;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Data;

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Matchnet.Content.BusinessLogic
{
	/// <summary>
	/// Summary description for RegionBL.
	/// </summary>
	public class RegionBL
	{

		#region constants
		private const int HEBREW_LANGUAGEID = 262144;
		private const Int32 REGIONID_AREACODE_SIZE = 48000; //Approx number of rows in AreaCode table
		private const string DB_REGION = "mnRegion";
		private const string REGION_CACHE_KEY_PREFIX = "~CONTENTREGION^{0}{1}";
		private const string CHILD_REGIONS_CACHE_KEY_PREFIX = "~CONTENTCHILDREGION^{0}{1}{2}{3}";
		private const string CITIES_BY_POSTAL_CACHE_KEY_PREFIX = "~CONTENTCITIESBYPOSTAL^{0}";
		private const string COUNTRY_REGIONS_CACHE_KEY_PREFIX = "~CONTENTCOUNTRYREGION^{0}";
	    //added for TT 17739 
		private const string COUNTRY_BIRTH_REGIONS_CACHE_KEY_PREFIX = "~CONTENTCOUNTRYBIRTHREGION^{0}";
		private const string POPULATED_HIERARCHY_CACHE_KEY_PREFIX = "~CONTENTPOPULATEDHIERARCHYREGION^{0}{1}{2}";
		private const string REGION_SCHOOL_NAME_CACHE_KEY_PREFIX = "~CONTENTREGIONSCHOOLNAME^{0}";
		private const string SCHOOL_REGION_SCHOOL_CACHE_KEY_PREFIX = "~CONTENTSCHOOLREGIONSCHOOL^{0}";
		private const string SCHOOL_REGION_STATE_CACHE_KEY_PREFIX = "~CONTENTSCHOOLREGIONSTATE^{0}";
		private const string REGION_ID_BY_POSTAL_CACHE_KEY_PREFIX = "~CONTENTREGIONIDBYPOSTAL^{0}{1}";
		private const string REGION_ID_BY_CITY_CACHE_KEY_PREFIX = "~CONTENTREGIONIDBYCITY^{0}{1}{2}";
		private const string REGION_LANGUAGE_CACHE_KEY_PREFIX = "~CONTENTREGIONLANGUAGE";
		private const string REGION_AREACODES_CACHE_KEY_PREFIX = "~CONTENTREGIONAREACODES";		
		private const string REGION_SEOREGIONS_CACHE_KEY_PREFIX = "~CONTENTSEOREGIONS";
		private const string DMA_BY_ZIPREGIONID_CACHE_KEY_PREFIX = "~CONTENTDMABYZIPREGIONID^{0}";
		private const string ALL_DMAS_CACHE_KEY_PREFIX = "~CONTENTALLDMAS";
		#endregion

		#region class variables
		private Cache _cache = null;
		#endregion

		#region singleton
		/// <summary>
		/// Singleton instance
		/// </summary>
		public static readonly RegionBL Instance = new RegionBL();
		#endregion

		#region public methods
		public AreaCodeDictionary RetrieveAreaCodes()
		{
			AreaCodeDictionary retVal = null;
			DataTable dt = null;

			retVal = _cache.Get(GetAreaCodeDictionaryCacheKey()) as AreaCodeDictionary;

			if (retVal == null) 
			{
				Command command = new Command(DB_REGION, "up_AreaCodeCountryRegionID_List", 0);

				dt = Client.Instance.ExecuteDataTable(command);
				if (dt.Rows.Count > 0)
				{
					retVal = new AreaCodeDictionary();

					foreach (DataRow dataRow in dt.Rows)
					{
						Int32 areaCode = Convert.ToInt32(dataRow["AreaCode"]);

						retVal.Add(Convert.ToInt32(dataRow["AreaCode"]), Convert.ToInt32(dataRow["CountryRegionID"]));
					}

					retVal.SetCacheKey = GetAreaCodeDictionaryCacheKey();
					CacheAreaCodeDictionary(retVal);
				}
			}

			return (retVal);
		}

		public RegionAreaCodeDictionary RetrieveAreaCodesByRegion()
		{
			RegionAreaCodeDictionary retVal = new RegionAreaCodeDictionary(REGIONID_AREACODE_SIZE);
			Command command = new Command(DB_REGION, "up_AreaCode_List", 0);
			SqlDataReader dataReader = null;

			bool colLookupDone = false;
			Int32 areaCodeOrdinal = 0;
			Int32 regionIDOrdinal = 0;

			try
			{
				dataReader = Client.Instance.ExecuteReader(command);

				while (dataReader.Read())
				{
					if (!colLookupDone)
					{
						areaCodeOrdinal = dataReader.GetOrdinal("AreaCode");
						regionIDOrdinal = dataReader.GetOrdinal("RegionID");
						colLookupDone = true;
					}

					Int32 regionID = Conversion.CInt(dataReader.GetInt32(regionIDOrdinal));
					Int32 areaCode = Conversion.CInt(dataReader.GetInt32(areaCodeOrdinal));

					retVal.Add(regionID, areaCode);
				}
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}

			return retVal;
		}

		/// <summary>
		/// Find the integer region ID given a country and postal code
		/// </summary>
		/// <param name="countryRegionID">countryRegionID</param>
		/// <param name="postalCode">postalCode</param>
		/// <returns>integer region ID referenced by the postal code for the given country</returns>
		public RegionID FindRegionIdByPostalCode(int countryRegionID, string postalCode)
		{
			RegionID retVal = null;
			DataTable dt = null;

			retVal = _cache.Get(GetRegionIDByPostal(countryRegionID, postalCode)) as RegionID;

			if(retVal == null) 
			{
				Command command = new Command(DB_REGION, "up_Region_ListPostalCodeRegionID", 0);
				command.AddParameter("@CountryRegionID", SqlDbType.Int, ParameterDirection.Input, countryRegionID);
				command.AddParameter("@PostalCode", SqlDbType.NVarChar, ParameterDirection.Input, postalCode);

				dt = Client.Instance.ExecuteDataTable(command);
				if (dt.Rows.Count > 0)
				{
					retVal = new RegionID(Convert.ToInt32(dt.Rows[0]["RegionID"]));
					retVal.SetCacheKey = GetRegionIDByPostal(countryRegionID, postalCode);
					CacheRegionID(retVal);
				}
			}
			return(retVal);
		}

		/// <summary>
		/// Find the integer region ID given a region ID and a city name
		/// </summary>
		/// <param name="parentRegionID">region where we are trying to find the city</param>
		/// <param name="description">city name</param>
		/// <param name="languageID"></param>
		/// <returns></returns>
		public RegionID FindRegionIdByCity(int parentRegionID, string description, int languageID)
		{
			RegionID retVal = null;
			DataTable dt = null;

			retVal = _cache.Get(GetRegionIDByCity(parentRegionID, description, languageID)) as RegionID;
			if(retVal == null) 
			{
				Command command = new Command(DB_REGION, "up_Region_ListCityRegionID", 0);
				command.AddParameter("@ParentRegionID", SqlDbType.Int, ParameterDirection.Input, parentRegionID);
				command.AddParameter("@Description", SqlDbType.NVarChar, ParameterDirection.Input, description);
				command.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
				command.AddParameter("@RETURN_VALUE", SqlDbType.Int, ParameterDirection.ReturnValue);

				dt = Client.Instance.ExecuteDataTable(command);
				if(dt.Rows.Count > 0)
				{
					retVal = new RegionID(Convert.ToInt32(dt.Rows[0]["RegionID"]));
					retVal.SetCacheKey = GetRegionIDByCity(parentRegionID, description, languageID);
					CacheRegionID(retVal);
				}
			}
			return(retVal);
		}

		/// <summary>
		/// Retrieve a region given the ID and a language
		/// </summary>
		/// <param name="regionID"></param>
		/// <param name="languageID"></param>
		/// <returns></returns>
		public Region RetrieveRegionByID(int regionID, int languageID)
		{
			try
			{
				Region retVal = null;
				retVal = _cache.Get(GetRegionCacheKey(regionID, languageID)) as Region;

				if(retVal == null) 
				{
					SqlDataReader dataReader = null;

					bool colLookupDone = false;
					Int32 ordinalDepth = 0;
					Int32 ordinalLatitude = 0;
					Int32 ordinalLongitude = 0;
					Int32 ordinalDescription = 0;
					Int32 ordinalAbbreviation = 0;
					Int32 ordinalMask = 0;
					Int32 ordinalChildrenDepth = 0;

					try
					{
						Command command = new Command(DB_REGION, "up_Region_List", 0);
						command.AddParameter("@RegionID", SqlDbType.Int, ParameterDirection.Input, regionID);
						command.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
						dataReader = Client.Instance.ExecuteReader(command);

						while (dataReader.Read())
						{
							if (!colLookupDone)
							{
								ordinalDepth = dataReader.GetOrdinal("Depth");
								ordinalLatitude = dataReader.GetOrdinal("Latitude");
								ordinalLongitude = dataReader.GetOrdinal("Longitude");
								ordinalDescription = dataReader.GetOrdinal("Description");
								ordinalAbbreviation = dataReader.GetOrdinal("Abbreviation");
								ordinalMask = dataReader.GetOrdinal("Mask");
								ordinalChildrenDepth = dataReader.GetOrdinal("ChildrenDepth");
								colLookupDone = true;
							}

							int depth = Constants.NULL_INT;
							if (!dataReader.IsDBNull(ordinalDepth))
							{
								depth = (Int32)dataReader.GetByte(ordinalDepth);
							}

							decimal latitude = Constants.NULL_DECIMAL;
							if (!dataReader.IsDBNull(ordinalLatitude))
							{
								latitude = dataReader.GetDecimal(ordinalLatitude);
							}

							decimal longitude = Constants.NULL_DECIMAL;
							if (!dataReader.IsDBNull(ordinalLongitude))
							{
								longitude = dataReader.GetDecimal(ordinalLongitude);
							}

							string description = null;
							if (!dataReader.IsDBNull(ordinalDescription))
							{
								description = dataReader.GetString(ordinalDescription);
							}

							string abbreviation = null;
							if (!dataReader.IsDBNull(ordinalAbbreviation))
							{
								abbreviation = dataReader.GetString(ordinalAbbreviation);
							}

							int mask = Constants.NULL_INT;
							if (!dataReader.IsDBNull(ordinalMask))
							{
								mask = dataReader.GetInt32(ordinalMask);
							}

							int childrenDepth = Constants.NULL_INT;
							if (!dataReader.IsDBNull(ordinalChildrenDepth))
							{
								childrenDepth = dataReader.GetInt32(ordinalChildrenDepth);
							}

							retVal = new Region(regionID, depth, latitude, longitude, description, abbreviation, mask, childrenDepth, null);

							retVal.SetCacheKey = GetRegionCacheKey(regionID, languageID);
							CacheRegion(retVal);
						}
					}
					finally
					{
						if (dataReader != null)
						{
							dataReader.Close();
						}
					}
				}

				return(retVal);
			}
			catch (Exception ex)
			{
				throw new Exception("RetrieveRegionByID() error (regionID: " + regionID.ToString() + ", languageID: " + languageID.ToString() + ").", ex);
			}
		}
		/// <summary>
		/// Provides a collection of regions (cities)based on provided postal code. 
		/// </summary>
		/// <remarks>This method should only be used for US and Canada. There is no country check occuring during DB call. </remarks>
		/// <param name="postalCode"></param>
		/// <returns>Collection of Region objects representing each city in the provided zip code.</returns>
		///	   
		public RegionCollection RetrieveCitiesByPostalCode(string strPostalCode)
		{
			RegionCollection retVal = null;			
			
			retVal = _cache.Get(GetCitiesByPostalCodeCacheKey(strPostalCode)) as RegionCollection;

			if(retVal == null) 
			{
				retVal = new RegionCollection();
				SqlDataReader dataReader = null;

				bool colLookupDone = false;
				Int32 ordinalRegionID = 0;
				Int32 ordinalDepth = 0;
				Int32 ordinalLatitude = 0;
				Int32 ordinalLongitude = 0;
				Int32 ordinalDescription = 0;
				Int32 ordinalAbbreviation = 0;
				Int32 ordinalMask = 0;
				Int32 ordinalChildrenDepth = 0;

				try
				{
					Command command = new Command(DB_REGION, "up_Region_ListCitiesByPostalCode", 0);				
					command.AddParameter("@PostalCode", SqlDbType.NVarChar, ParameterDirection.Input, strPostalCode);
					dataReader = Client.Instance.ExecuteReader(command);
					while (dataReader.Read())
					{
						if (!colLookupDone)
						{
							ordinalRegionID = dataReader.GetOrdinal("ZipRegionID");
							ordinalDepth = dataReader.GetOrdinal("Depth");
							ordinalLatitude = dataReader.GetOrdinal("Latitude");
							ordinalLongitude = dataReader.GetOrdinal("Longitude");
							ordinalDescription = dataReader.GetOrdinal("Description");
							ordinalAbbreviation = dataReader.GetOrdinal("Abbreviation");
							ordinalMask = dataReader.GetOrdinal("Mask");
							ordinalChildrenDepth = dataReader.GetOrdinal("ChildrenDepth");
							colLookupDone = true;
						}

						int regionID = dataReader.GetInt32(ordinalRegionID);
						int depth = (Int32)dataReader.GetByte(ordinalDepth);
						decimal latitude = dataReader.GetDecimal(ordinalLatitude);
						decimal longitude = dataReader.GetDecimal(ordinalLongitude);
						string descriptionNew = dataReader.GetString(ordinalDescription);
						string abbreviation = null;
						if (!dataReader.IsDBNull(ordinalAbbreviation))
						{
							abbreviation = dataReader.GetString(ordinalAbbreviation);
						}
						int mask = dataReader.GetInt32(ordinalMask);
						int childrenDepth = Constants.NULL_INT;
						if (!dataReader.IsDBNull(ordinalChildrenDepth))
						{
							childrenDepth = (Int32)dataReader.GetByte(ordinalChildrenDepth);
						}

						retVal.Add(new Region(regionID, depth, latitude, longitude, descriptionNew, abbreviation, mask, childrenDepth, null));
					}
				}
				finally
				{
					if (dataReader != null)
					{
						dataReader.Close();
					}
				}

				if(retVal != null) 
				{
					retVal.SetCacheKey = GetCitiesByPostalCodeCacheKey(strPostalCode);
					CacheRegionCollection(retVal);
				}
			}

			return(retVal);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="parentRegionID"></param>
		/// <param name="languageID"></param>
		/// <param name="description"></param>
		/// <param name="translationID"></param>
		/// <returns></returns>
		public RegionCollection RetrieveChildRegions(int parentRegionID, int languageID, string description, int translationID)
		{
			RegionCollection retVal = null;
			Region myRegion = null;
			DataTable dt = null;
			
			retVal = _cache.Get(GetChildRegionsCacheKey(parentRegionID, languageID, description, translationID)) as RegionCollection;

			if(retVal == null) 
			{
				retVal = new RegionCollection();
				SqlDataReader dataReader = null;

				bool colLookupDone = false;
				Int32 ordinalRegionID = 0;
				Int32 ordinalDepth = 0;
				Int32 ordinalLatitude = 0;
				Int32 ordinalLongitude = 0;
				Int32 ordinalDescription = 0;
				Int32 ordinalAbbreviation = 0;
				Int32 ordinalMask = 0;
				Int32 ordinalChildrenDepth = 0;

				try
				{
					Command command = new Command(DB_REGION, "up_Region_ListChildren", 0);
					command.AddParameter("@ParentRegionID", SqlDbType.Int, ParameterDirection.Input, parentRegionID);
					command.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
					if (description != Matchnet.Constants.NULL_STRING)
					{
						command.AddParameter("@Description", SqlDbType.NVarChar, ParameterDirection.Input, description);
					}
					dataReader = Client.Instance.ExecuteReader(command);

					while (dataReader.Read())
					{
						if (!colLookupDone)
						{
							ordinalRegionID = dataReader.GetOrdinal("RegionID");
							ordinalDepth = dataReader.GetOrdinal("Depth");
							ordinalLatitude = dataReader.GetOrdinal("Latitude");
							ordinalLongitude = dataReader.GetOrdinal("Longitude");
							ordinalDescription = dataReader.GetOrdinal("Description");
							ordinalAbbreviation = dataReader.GetOrdinal("Abbreviation");
							ordinalMask = dataReader.GetOrdinal("Mask");
							ordinalChildrenDepth = dataReader.GetOrdinal("ChildrenDepth");
							colLookupDone = true;
						}

						int regionID = dataReader.GetInt32(ordinalRegionID);
						int depth = (Int32)dataReader.GetByte(ordinalDepth);
						decimal latitude = dataReader.GetDecimal(ordinalLatitude);
						decimal longitude = dataReader.GetDecimal(ordinalLongitude);
						string descriptionNew = dataReader.GetString(ordinalDescription);
						string abbreviation = null;
						if (!dataReader.IsDBNull(ordinalAbbreviation))
						{
							abbreviation = dataReader.GetString(ordinalAbbreviation);
						}
						int mask = dataReader.GetInt32(ordinalMask);
						int childrenDepth = Constants.NULL_INT;
						if (!dataReader.IsDBNull(ordinalChildrenDepth))
						{
							childrenDepth = (Int32)dataReader.GetByte(ordinalChildrenDepth);
						}

						retVal.Add(new Region(regionID, depth, latitude, longitude, descriptionNew, abbreviation, mask, childrenDepth, null));
					}
				}
				finally
				{
					if (dataReader != null)
					{
						dataReader.Close();
					}
				}

				if(retVal != null) 
				{
					retVal.SetCacheKey = GetChildRegionsCacheKey(parentRegionID, languageID, description, translationID);
					CacheRegionCollection(retVal);
				}
			}

			return(retVal);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="parentRegionID"></param>
		/// <param name="languageID"></param>
		/// <param name="translationID"></param>
		/// <param name="forceLoad"></param>
		/// <returns></returns>
		public RegionCollection RetrieveChildRegions(int parentRegionID, int languageID, int translationID, bool forceLoad) 
		{
			return RetrieveChildRegions(parentRegionID, languageID, translationID);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="parentRegionID"></param>
		/// <param name="languageID"></param>
		/// <returns></returns>
		public RegionCollection RetrieveChildRegions(int parentRegionID, int languageID)
		{
			return RetrieveChildRegions(parentRegionID, languageID, Matchnet.Constants.NULL_STRING);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="parentRegionID"></param>
		/// <param name="languageID"></param>
		/// <param name="description"></param>
		/// <returns></returns>
		public RegionCollection RetrieveChildRegions(int parentRegionID, int languageID, string description)
		{
			return RetrieveChildRegions(parentRegionID, languageID, description, Matchnet.Constants.NULL_INT);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="parentRegionID"></param>
		/// <param name="languageID"></param>
		/// <param name="translationID"></param>
		/// <returns></returns>
		public RegionCollection RetrieveChildRegions(int parentRegionID, int languageID, int translationID)
		{
			return RetrieveChildRegions(parentRegionID, languageID, Matchnet.Constants.NULL_STRING, translationID);
		}

		/// <summary>
		/// Retrieve country Regions given a language
		/// </summary>
		/// <param name="languageID"></param>
		/// <returns></returns>
		public RegionCollection RetrieveCountries(int languageID)
		{
			Region myRegion = null;
			RegionCollection retVal = null;
			DataTable dt = null;

			SqlDataReader dataReader = null;

			bool colLookupDone = false;
			Int32 ordinalRegionID = 0;
			Int32 ordinalDepth = 0;
			Int32 ordinalLatitude = 0;
			Int32 ordinalLongitude = 0;
			Int32 ordinalDescription = 0;
			Int32 ordinalAbbreviation = 0;
			Int32 ordinalMask = 0;
			Int32 ordinalChildrenDepth = 0;

			try
			{
				Command command = new Command(DB_REGION, "up_Region_ListTop", 0);
				command.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
				dataReader = Client.Instance.ExecuteReader(command);

				while (dataReader.Read())
				{
					if (!colLookupDone)
					{
						retVal = new RegionCollection();
						ordinalRegionID = dataReader.GetOrdinal("RegionID");
						ordinalDepth = dataReader.GetOrdinal("Depth");
						ordinalLatitude = dataReader.GetOrdinal("Latitude");
						ordinalLongitude = dataReader.GetOrdinal("Longitude");
						ordinalDescription = dataReader.GetOrdinal("Description");
						ordinalAbbreviation = dataReader.GetOrdinal("Abbreviation");
						ordinalMask = dataReader.GetOrdinal("Mask");
						ordinalChildrenDepth = dataReader.GetOrdinal("ChildrenDepth");
						colLookupDone = true;
					}

					Int32 regionID = dataReader.GetInt32(ordinalRegionID);
					Int32 depth = (Int32)dataReader.GetByte(ordinalDepth);
					decimal latitude = dataReader.GetDecimal(ordinalLatitude);
					decimal longitude = dataReader.GetDecimal(ordinalLongitude);
					string description = dataReader.GetString(ordinalDescription);
					string abbreviation = null;
					if (!dataReader.IsDBNull(ordinalAbbreviation))
					{
						abbreviation = dataReader.GetString(ordinalAbbreviation);
					}
					int mask = dataReader.GetInt32(ordinalMask);
					int childrenDepth = Constants.NULL_INT;
					if (!dataReader.IsDBNull(ordinalChildrenDepth))
					{
						childrenDepth = (Int32)dataReader.GetByte(ordinalChildrenDepth);
					}

					retVal.Add(new Region(regionID, depth, latitude, longitude, description, abbreviation, mask, childrenDepth, null));
				}
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
			
			if(retVal != null) 
			{
				retVal.SetCacheKey = GetCountryRegionsCacheKey(languageID);
				CacheRegionCollection(retVal);
			}

			return(retVal);
		}



		/// <summary>
		/// Retrieve Birth country Regions given a language
		/// </summary>
		/// <param name="languageID"></param>
		/// <returns></returns>
		public RegionCollection RetrieveBirthCountries(int languageID)
		{
			Region myRegion = null;
			RegionCollection retVal = null;
			DataTable dt = null;

			SqlDataReader dataReader = null;

			bool colLookupDone = false;
			Int32 ordinalRegionID = 0;
			Int32 ordinalDepth = 0;
			Int32 ordinalLatitude = 0;
			Int32 ordinalLongitude = 0;
			Int32 ordinalDescription = 0;
			Int32 ordinalAbbreviation = 0;
			Int32 ordinalMask = 0;
			Int32 ordinalChildrenDepth = 0;

			try
			{
				Command command = new Command(DB_REGION, "up_Region_BirthPlace_ListTop", 0);
				command.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
				dataReader = Client.Instance.ExecuteReader(command);

				while (dataReader.Read())
				{
					if (!colLookupDone)
					{
						retVal = new RegionCollection();
						ordinalRegionID = dataReader.GetOrdinal("RegionID");
						ordinalDepth = dataReader.GetOrdinal("Depth");
						ordinalLatitude = dataReader.GetOrdinal("Latitude");
						ordinalLongitude = dataReader.GetOrdinal("Longitude");
						ordinalDescription = dataReader.GetOrdinal("Description");
						ordinalAbbreviation = dataReader.GetOrdinal("Abbreviation");
						ordinalMask = dataReader.GetOrdinal("Mask");
						ordinalChildrenDepth = dataReader.GetOrdinal("ChildrenDepth");
						colLookupDone = true;
					}

					Int32 regionID = dataReader.GetInt32(ordinalRegionID);
					Int32 depth = (Int32)dataReader.GetByte(ordinalDepth);
					decimal latitude = dataReader.GetDecimal(ordinalLatitude);
					decimal longitude = dataReader.GetDecimal(ordinalLongitude);
					string description = dataReader.GetString(ordinalDescription);
					string abbreviation = null;
					if (!dataReader.IsDBNull(ordinalAbbreviation))
					{
						abbreviation = dataReader.GetString(ordinalAbbreviation);
					}
					int mask = dataReader.GetInt32(ordinalMask);
					int childrenDepth = Constants.NULL_INT;
					if (!dataReader.IsDBNull(ordinalChildrenDepth))
					{
						childrenDepth = (Int32)dataReader.GetByte(ordinalChildrenDepth);
					}

					retVal.Add(new Region(regionID, depth, latitude, longitude, description, abbreviation, mask, childrenDepth, null));
				}
			}
			finally
			{
				if (dataReader != null)
				{
					dataReader.Close();
				}
			}
			
			if(retVal != null) 
			{
				retVal.SetCacheKey = GetBirthCountryRegionsCacheKey(languageID);
				CacheRegionCollection(retVal);
			}

			return(retVal);
		}

		/// <summary>
		/// Retrieve country regions that speak english
		/// </summary>
		/// <returns></returns>
		public RegionCollection RetrieveCountries()
		{
			//	TOFIX - No one would be seriously injured if we pulled this 2 as
			//	a constant from somewhere - dcornell

			return(RetrieveCountries(2)); // english
		}

		/// <summary>
		/// Retrieve a fully populated region hierarchy
		/// </summary>
		/// <param name="regionID"></param>
		/// <param name="languageID"></param>
		/// <returns></returns>
		public RegionLanguage RetrievePopulatedHierarchy(int regionID, int languageID)
		{
			return(RetrievePopulatedHierarchy(regionID, languageID, Matchnet.Constants.NULL_INT));
		}

		/// <summary>
		/// Retrieve a fully populated region hierarchy
		/// </summary>
		/// <param name="regionID"></param>
		/// <param name="languageID"></param>
		/// <param name="maxDepth"></param>
		/// <returns></returns>
		public RegionLanguage RetrievePopulatedHierarchy(int regionID, int languageID, int maxDepth)
		{
			RegionLanguage retVal = new RegionLanguage();
			Region cachedRegion = null;
			Region currentRegion = null;
			
			retVal = _cache.Get(GetPopulateHierarchyCacheKey(regionID, languageID, maxDepth)) as RegionLanguage;

			if(retVal == null) 
			{
				retVal = new RegionLanguage();

				SqlDataReader dataReader = null;

				bool colLookupDone = false;
				Int32 ordinalRegionID = 0;
				Int32 ordinalDepth = 0;
				Int32 ordinalLatitude = 0;
				Int32 ordinalLongitude = 0;
				Int32 ordinalDescription = 0;
				Int32 ordinalAbbreviation = 0;
				Int32 ordinalMask = 0;
				Int32 ordinalChildrenDepth = 0;

				try
				{
					Command command = new Command(DB_REGION, "up_Region_ListHierarchy", 0);
					command.AddParameter("@RegionID", SqlDbType.Int, ParameterDirection.Input, regionID);
					if (maxDepth != Matchnet.Constants.NULL_INT)
					{
						command.AddParameter("@MaxDepth", SqlDbType.Int, ParameterDirection.Input, maxDepth);
					}
					command.AddParameter("@LanguageID", SqlDbType.Int, ParameterDirection.Input, languageID);
					dataReader = Client.Instance.ExecuteReader(command);

					while (dataReader.Read())
					{
						if (!colLookupDone)
						{
							ordinalRegionID = dataReader.GetOrdinal("RegionID");
							ordinalDepth = dataReader.GetOrdinal("Depth");
							ordinalLatitude = dataReader.GetOrdinal("Latitude");
							ordinalLongitude = dataReader.GetOrdinal("Longitude");
							ordinalDescription = dataReader.GetOrdinal("Description");
							ordinalAbbreviation = dataReader.GetOrdinal("Abbreviation");
							ordinalMask = dataReader.GetOrdinal("Mask");
							ordinalChildrenDepth = dataReader.GetOrdinal("ChildrenDepth");
							colLookupDone = true;
						}

						Int32 regionIDCurrent = dataReader.GetInt32(ordinalRegionID);
						int depth = (Int32)dataReader.GetByte(ordinalDepth);
						decimal latitude = dataReader.GetDecimal(ordinalLatitude);
						decimal longitude = dataReader.GetDecimal(ordinalLongitude);
						string description = dataReader.GetString(ordinalDescription);
						string abbreviation = null;
						if (!dataReader.IsDBNull(ordinalAbbreviation))
						{
							abbreviation = dataReader.GetString(ordinalAbbreviation);
						}
						int mask = dataReader.GetInt32(ordinalMask);
						int childrenDepth = Constants.NULL_INT;
						if (!dataReader.IsDBNull(ordinalChildrenDepth))
						{
							childrenDepth = (Int32)dataReader.GetByte(ordinalChildrenDepth);
						}

						currentRegion = new Region(regionIDCurrent, depth, latitude, longitude, description, abbreviation, mask, childrenDepth, null);

						if (cachedRegion == null)
						{
							cachedRegion = currentRegion;
						}
						else
						{
							currentRegion.ParentRegion = cachedRegion;
							cachedRegion = currentRegion;
						}
					}
				}
				finally
				{
					if (dataReader != null)
					{
						dataReader.Close();
					}
				}

				if (cachedRegion != null)
				{
					do
					{
						switch (cachedRegion.Depth)
						{
							case 4:
								retVal.UpdatePostalCodeInfo(cachedRegion.RegionID, cachedRegion.Description,
									cachedRegion.Longitude, cachedRegion.Latitude);
								break;
							case 3:
								retVal.UpdateCityInfo(cachedRegion.RegionID, cachedRegion.Description,
									cachedRegion.Longitude, cachedRegion.Latitude);
								break;
							case 2:
								retVal.UpdateStateInfo(cachedRegion.RegionID, cachedRegion.Abbreviation, cachedRegion.Description);
								break;
							case 1:
								retVal.UpdateCountryInfo(cachedRegion.RegionID, cachedRegion.Abbreviation,
									cachedRegion.Description, cachedRegion.RegionID);
								break;
						}

						if (cachedRegion.Depth != Matchnet.Constants.NULL_INT)
						{
							int depth = cachedRegion.Depth;
							if (depth > retVal.HighestDepthRegionTypeID)
							{
								retVal.HighestDepthRegionTypeID = depth;
							}
						}

						cachedRegion = cachedRegion.ParentRegion;
					}
					while (cachedRegion != null);
				}

				if(retVal != null) 
				{
					retVal.SetCacheKey = GetPopulateHierarchyCacheKey(regionID, languageID, maxDepth);
					CachePopulatedHierarchy(retVal);
				}
			}

			return(retVal);
		}

		/// <summary>
		/// Retrieve a school name given a school region ID
		/// </summary>
		/// <param name="schoolRegionID"></param>
		/// <returns></returns>
		public RegionSchoolName RetrieveSchoolName(int schoolRegionID)
		{
			RegionSchoolName retVal = null;
			DataTable dt = null;
			
			retVal = _cache.Get(GetRegionSchoolNameCacheKey(schoolRegionID)) as RegionSchoolName;

			if(retVal == null) 
			{
				Command command = new Command(DB_REGION, "up_SchoolRegion_List", 0);
				command.AddParameter("@SchoolRegionID", SqlDbType.Int, ParameterDirection.Input, schoolRegionID);
				dt = Client.Instance.ExecuteDataTable(command);

				if (dt.Rows.Count == 1)
				{
					retVal = new RegionSchoolName(dt.Rows[0]["Name"].ToString());
					retVal.SetCacheKey = GetRegionSchoolNameCacheKey(schoolRegionID);
					CacheRegionSchoolName(retVal);
				}
			}
			return(retVal);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="schoolRegionID"></param>
		/// <returns></returns>
		public SchoolRegionCollection RetrieveSchoolParents(int schoolRegionID)
		{
			SchoolRegionCollection retVal = null;
			SchoolRegion mySchoolRegion = null;
			DataTable dt = null;

			retVal = _cache.Get(GetSchoolRegionBySchoolCacheKey(schoolRegionID)) as SchoolRegionCollection;

			if(retVal == null) 
			{
				retVal = new SchoolRegionCollection();
				SqlDataReader dataReader = null;
				retVal = new SchoolRegionCollection();
				bool colLookupDone = false;

				Int32 ordinalName = 0;
				Int32 ordinalCountryRegionID = 0;
				Int32 ordinalStateProvinceRegionID = 0;
				Int32 ordinalCityRegionID = 0;
				Int32 ordinalZipCodeRegionID = 0;

				try
				{
					Command command = new Command(DB_REGION, "up_SchoolRegion_List", 0);
					command.AddParameter("@SchoolRegionID", SqlDbType.Int, ParameterDirection.Input, schoolRegionID);
					dataReader = Client.Instance.ExecuteReader(command);

					while (dataReader.Read())
					{
						if (!colLookupDone)
						{
							ordinalName = dataReader.GetOrdinal("Name");
							ordinalCountryRegionID = dataReader.GetOrdinal("CountryRegionID");
							ordinalStateProvinceRegionID = dataReader.GetOrdinal("StateProvinceRegionID");
							ordinalCityRegionID = dataReader.GetOrdinal("CityRegionID");
							ordinalZipCodeRegionID = dataReader.GetOrdinal("ZipCodeRegionID");
							colLookupDone = true;
						}

						string name = dataReader.GetString(ordinalName);

						Int32 countryRegionID = Constants.NULL_INT;
						if (!dataReader.IsDBNull(ordinalCountryRegionID))
						{
							countryRegionID = dataReader.GetInt32(ordinalCountryRegionID);
						}

						Int32 stateProvinceRegionID = Constants.NULL_INT;
						if (!dataReader.IsDBNull(ordinalStateProvinceRegionID))
						{
							stateProvinceRegionID = dataReader.GetInt32(ordinalStateProvinceRegionID);
						}

						Int32 cityRegionID = Constants.NULL_INT;
						if (!dataReader.IsDBNull(ordinalCityRegionID))
						{
							cityRegionID = dataReader.GetInt32(ordinalCityRegionID);
						}

						Int32 zipCodeRegionID = Constants.NULL_INT;
						if (!dataReader.IsDBNull(ordinalZipCodeRegionID))
						{
							zipCodeRegionID = dataReader.GetInt32(ordinalZipCodeRegionID);
						}

						retVal.Add(new SchoolRegion(schoolRegionID, name, countryRegionID, stateProvinceRegionID, cityRegionID, zipCodeRegionID));
					}
				}
				finally
				{
					if (dataReader != null)
					{
						dataReader.Close();
					}
				}

				if(retVal != null) 
				{
					retVal.SetCacheKey = GetSchoolRegionBySchoolCacheKey(schoolRegionID);
					CacheSchoolRegionCollection(retVal);
				}
			}

			return(retVal);
		}

		/// <summary>
		/// Region school regions for a given state region ID
		/// </summary>
		/// <param name="stateRegionID"></param>
		/// <returns></returns>
		public SchoolRegionCollection RetrieveSchoolListByStateRegion(int stateRegionID)
		{
			SchoolRegionCollection retVal = null;

			retVal = _cache.Get(GetSchoolRegionByStateCacheKey(stateRegionID)) as SchoolRegionCollection;

			if(retVal == null) 
			{
				SqlDataReader dataReader = null;
				retVal = new SchoolRegionCollection();
				bool colLookupDone = false;

				Int32 ordinalSchoolRegionID = 0;
				Int32 ordinalName = 0;
				Int32 ordinalCountryRegionID = 0;
				Int32 ordinalStateProvinceRegionID = 0;
				Int32 ordinalCityRegionID = 0;
				Int32 ordinalZipCodeRegionID = 0;

				try
				{
					Command command = new Command(DB_REGION, "up_SchoolRegion_List", 0);
					command.AddParameter("@StateRegionID", SqlDbType.Int, ParameterDirection.Input, stateRegionID);
					dataReader = Client.Instance.ExecuteReader(command);

					while (dataReader.Read())
					{
						if (!colLookupDone)
						{
							ordinalSchoolRegionID = dataReader.GetOrdinal("SchoolRegionID");
							ordinalName = dataReader.GetOrdinal("Name");
							ordinalCountryRegionID = dataReader.GetOrdinal("CountryRegionID");
							ordinalStateProvinceRegionID = dataReader.GetOrdinal("StateProvinceRegionID");
							ordinalCityRegionID = dataReader.GetOrdinal("CityRegionID");
							ordinalZipCodeRegionID = dataReader.GetOrdinal("ZipCodeRegionID");
							colLookupDone = true;
						}

						Int32 schoolRegionID = dataReader.GetInt32(ordinalSchoolRegionID);
						string name = dataReader.GetString(ordinalName);

						Int32 countryRegionID = Constants.NULL_INT;
						if (!dataReader.IsDBNull(ordinalCountryRegionID))
						{
							countryRegionID = dataReader.GetInt32(ordinalCountryRegionID);
						}

						Int32 stateProvinceRegionID = Constants.NULL_INT;
						if (!dataReader.IsDBNull(ordinalStateProvinceRegionID))
						{
							stateProvinceRegionID = dataReader.GetInt32(ordinalStateProvinceRegionID);
						}

						Int32 cityRegionID = Constants.NULL_INT;
						if (!dataReader.IsDBNull(ordinalCityRegionID))
						{
							cityRegionID = dataReader.GetInt32(ordinalCityRegionID);
						}

						Int32 zipCodeRegionID = Constants.NULL_INT;
						if (!dataReader.IsDBNull(ordinalZipCodeRegionID))
						{
							zipCodeRegionID = dataReader.GetInt32(ordinalZipCodeRegionID);
						}

						retVal.Add(new SchoolRegion(schoolRegionID, name, countryRegionID, stateProvinceRegionID, cityRegionID, zipCodeRegionID));
					}
				}
				finally
				{
					if (dataReader != null)
					{
						dataReader.Close();
					}
				}
								
				if(retVal != null) 
				{
					retVal.SetCacheKey = GetSchoolRegionByStateCacheKey(stateRegionID);
					CacheSchoolRegionCollection(retVal);
				}
			}
			return(retVal);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public LanguageCollection GetLanguages()
		{
			LanguageCollection retVal = null;
			DataTable dt = null;
			Matchnet.Content.ValueObjects.Region.Language myLanguage = null;

			retVal = _cache.Get(GetRegionLanguageCacheKey()) as LanguageCollection;

			if(retVal == null) 
			{
				Command command = new Command("mnSystem", "up_Language_List", 0);
				dt = Client.Instance.ExecuteDataTable(command);

				if(dt.Rows.Count > 0) 
				{
					retVal = new LanguageCollection();
					for(int i = 0; i < dt.Rows.Count; i++) 
					{
						myLanguage = new Matchnet.Content.ValueObjects.Region.Language();
						myLanguage.CharSet = dt.Rows[i]["LanguageCharSet"].ToString();
						myLanguage.Description = dt.Rows[i]["LanguageName"].ToString();
						myLanguage.LanguageID = int.Parse(dt.Rows[i]["LanguageID"].ToString());

						retVal.Add(myLanguage);
					}
				}
				if(retVal != null && retVal.Count > 0) 
				{
					retVal.SetCacheKey = GetRegionLanguageCacheKey();
					CacheRegionLanguage(retVal);
				}
			}
			return retVal;
		}

		/// <summary>
		/// Returns SEORegions object that is consumed by SEO pages.
		/// </summary>
		/// <returns></returns>
		public SEORegions RetrieveSEORegions()
		{
			SEORegions objseoRegions=null;
			SqlDataReader dataReader=null;
			objseoRegions= _cache.Get(GetSEORegionsCacheKey()) as SEORegions;
			if (objseoRegions==null)
			{
				// populate datareader from databse
				try
				{
					Command command = new Command(DB_REGION, "up_SEORegions_List", 0);
					dataReader = Client.Instance.ExecuteReader(command);
				}
				catch(Exception ex)
				{
					throw new Exception("RetrieveSEORegions(). Error executing up_SEORegions_List stored proc.\n", ex);
				}
				#region ordinal variables for columns

				Int32 ordinalCountryRegionID = 0;
				Int32 ordinalStateRegionID = 0;
				Int32 ordinalStateName = 0;
				Int32 ordinalStateAbbreviation = 0;
				Int32 ordinalCityRegionID = 0;
				Int32 ordinalCityName = 0;				
				Int32 ordinalIsTopStateCity = 0;
				Int32 ordinalIsUSTop20City = 0;
				bool colLookupDone = false;

				#endregion	
				try
				{
					bool readFlag=dataReader.HasRows;
					string prevStateAbbrv="";
					
					SEORegionCollection colSeoUSMajorCities=new SEORegionCollection();
					SEOStateCollection colSeoStates= new SEOStateCollection();
					while (readFlag)  
					{
						
						#region Initialize ordinal variables at the first iteration(read). 
							
						if (!colLookupDone)
						{
							dataReader.Read();
							ordinalCountryRegionID = dataReader.GetOrdinal("CountryRegionID");
							ordinalStateRegionID = dataReader.GetOrdinal("StateRegionID");
							ordinalStateName= dataReader.GetOrdinal("StateName");
							ordinalStateAbbreviation = dataReader.GetOrdinal("StateAbbreviation");
						
							ordinalCityRegionID = dataReader.GetOrdinal("CityRegionID");
							ordinalCityName= dataReader.GetOrdinal("CityName");							
							ordinalIsTopStateCity=dataReader.GetOrdinal("IsTopStateCity");

							ordinalIsUSTop20City=dataReader.GetOrdinal("IsUSTop20City");

							colLookupDone = true;
						}
						#endregion
						
						//if (!dataReader.GetString(ordinalStateAbbreviation).Equals(prevStateAbbrv))
						//{
						// Create the state object
						int intDefaultCityRegionID=0;

						Region state = new Region(dataReader.GetInt32(ordinalStateRegionID),2,0,0,dataReader.GetString(ordinalStateName),dataReader.GetString(ordinalStateAbbreviation),0,3,null);

						SEORegionCollection seoStateCities= new SEORegionCollection();

						prevStateAbbrv=state.Abbreviation;												   
						while(readFlag && dataReader.GetString(ordinalStateAbbreviation).Equals(prevStateAbbrv))
						{
							// Add the city to state's cities collection
							seoStateCities.Add(new SEORegion(dataReader.GetInt32(ordinalCityRegionID),dataReader.GetString(ordinalCityName),"",state.Abbreviation,state.Description));
							// populate state's default city
							if (dataReader.GetBoolean(ordinalIsTopStateCity))
							{
								intDefaultCityRegionID=dataReader.GetInt32(ordinalCityRegionID);
							}
							// Add US Major city to its collection
							if (dataReader.GetBoolean(ordinalIsUSTop20City))
							{
								colSeoUSMajorCities.Add(new SEORegion(dataReader.GetInt32(ordinalCityRegionID),dataReader.GetString(ordinalCityName),"",state.Abbreviation,state.Description));
							}
							// advance to next row
							readFlag=dataReader.Read();
						} // end of while
						if (state!=null)
						{
							colSeoStates.Add(new SEOState(state,intDefaultCityRegionID,seoStateCities));
						}
							
						// }//
					}// end of while
					objseoRegions= new SEORegions(colSeoUSMajorCities,colSeoStates);
				}
				catch(Exception ex)
				{
					throw new Exception("RetrieveSEORegions() error.\n", ex);
				}
				finally
				{
					if (dataReader != null)
					{
						dataReader.Close();
					}
				}
				
				if (objseoRegions != null) 
				{
					objseoRegions.SetCacheKey=GetSEORegionsCacheKey();
					_cache.Insert(objseoRegions);
				}

			}
			return objseoRegions;
		}

		public DMA GetDMAByZipRegionID(int ZipRegionID)
		{
			DMA retVal = null;
			DataTable dt = null;

			retVal = _cache.Get(GetDMAByZipRegionIDCacheKey(ZipRegionID)) as DMA;

			if(retVal == null) 
			{
				Command command = new Command(DB_REGION, "up_Region_DMA", 0);				
				command.AddParameter("@RegionID", SqlDbType.Int, ParameterDirection.Input, ZipRegionID);

				dt = Client.Instance.ExecuteDataTable(command);
				if (dt.Rows.Count > 0)
				{
					retVal = new DMA(Convert.ToInt32(dt.Rows[0]["DMACode"]));
					retVal.SetCacheKey = GetDMAByZipRegionIDCacheKey(ZipRegionID);
					CacheDMAByZipRegionID(retVal);
				}
			}
			return(retVal);
		}
		/// <summary>
		/// Returns all DMA codes.
		/// </summary>
		/// <returns></returns>
		public DMACollection GetAllDMAS()
		{
			DMACollection retVal = null;
			DataTable dt = null;
			DMA aDMA = null;

			retVal = _cache.Get(GetAllDMASCacheKey()) as DMACollection;

			if(retVal == null)
			{
				Command command = new Command(DB_REGION, "up_DMA_Get", 0);
				
				dt = Client.Instance.ExecuteDataTable(command);
				if(dt.Rows.Count > 0)
				{
					retVal = new DMACollection();
					for(int i=0; i < dt.Rows.Count; i++)
					{
						aDMA = new DMA(Convert.ToInt32(dt.Rows[i]["DMACode"]));
						aDMA.DMADescription = dt.Rows[i]["Description"].ToString();
						retVal.Add(aDMA);
					}
				}
				if(retVal != null && retVal.Count > 0) 
				{
					retVal.SetCacheKey = GetAllDMASCacheKey();
					CacheAllDMAS(retVal);
				}
			}

			return retVal;
		}
		#endregion

		#region private Cache Routines
		private void CacheRegion(Region pRegion)
		{
			pRegion.CachePriority = CacheItemPriorityLevel.Normal;
			pRegion.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SVC"));
			_cache.Insert(pRegion);
		}
		private void CacheRegionCollection(RegionCollection pRegionCollection) 
		{
			pRegionCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pRegionCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SVC"));
			_cache.Insert(pRegionCollection);
		}
		private void CachePopulatedHierarchy(RegionLanguage pRegionLanguage) 
		{
			pRegionLanguage.CachePriority = CacheItemPriorityLevel.Normal;
			pRegionLanguage.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SVC"));
			_cache.Insert(pRegionLanguage);
		}
		private void CacheRegionSchoolName(RegionSchoolName pRegionSchoolName) 
		{
			pRegionSchoolName.CachePriority = CacheItemPriorityLevel.Normal;
			pRegionSchoolName.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SVC"));
			_cache.Insert(pRegionSchoolName);
		}
		private void CacheSchoolRegionCollection(SchoolRegionCollection pSchoolRegionCollection) 
		{
			pSchoolRegionCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pSchoolRegionCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SVC"));
			_cache.Insert(pSchoolRegionCollection);
		}
		private void CacheRegionID(RegionID pRegionID) 
		{
			pRegionID.CachePriority = CacheItemPriorityLevel.Normal;
			pRegionID.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SVC"));
			_cache.Insert(pRegionID);
		}
		private void CacheRegionLanguage(LanguageCollection pLanguageCollection) 
		{
			pLanguageCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pLanguageCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SVC"));
			_cache.Insert(pLanguageCollection);
		}
		private void CacheAreaCodeDictionary(AreaCodeDictionary pAreaCodeDictionary) 
		{
			pAreaCodeDictionary.CachePriority = CacheItemPriorityLevel.Normal;
			pAreaCodeDictionary.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SVC"));
			_cache.Insert(pAreaCodeDictionary);
		}
		private void CacheDMAByZipRegionID(DMA pDMA) 
		{
			pDMA.CachePriority = CacheItemPriorityLevel.Normal;
			pDMA.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SVC"));
			_cache.Insert(pDMA);
		}

		private void CacheAllDMAS(DMACollection pDMACollection)
		{
			pDMACollection.CachePriority = CacheItemPriorityLevel.Normal;
			pDMACollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_REGION_SA"));
			_cache.Insert(pDMACollection);
		}
		

		private string GetRegionCacheKey(int pRegionID, int pLanguageID)
		{
			return string.Format(REGION_CACHE_KEY_PREFIX, pRegionID, pLanguageID);
		}
		private string GetCitiesByPostalCodeCacheKey( string strPostalCode) 
		{
			return string.Format(CITIES_BY_POSTAL_CACHE_KEY_PREFIX , strPostalCode);
		}
		private string GetChildRegionsCacheKey(int parentRegionID, int languageID, string description, int translationID) 
		{
			return string.Format(CHILD_REGIONS_CACHE_KEY_PREFIX, parentRegionID, languageID, description, translationID);
		}
		private string GetCountryRegionsCacheKey(int pLanguageID) 
		{
			return string.Format(COUNTRY_REGIONS_CACHE_KEY_PREFIX, pLanguageID);
		}
		//added for TT 17739 
		private string GetBirthCountryRegionsCacheKey(int pLanguageID) 
		{
			return string.Format(COUNTRY_BIRTH_REGIONS_CACHE_KEY_PREFIX, pLanguageID);
		}
		private string GetPopulateHierarchyCacheKey(int regionID, int languageID, int maxDepth) 
		{
			return string.Format(POPULATED_HIERARCHY_CACHE_KEY_PREFIX, regionID, languageID, maxDepth);
		}
		private string GetRegionSchoolNameCacheKey(int schoolRegionID) 
		{
			return string.Format(REGION_SCHOOL_NAME_CACHE_KEY_PREFIX, schoolRegionID);
		}
		private string GetSchoolRegionBySchoolCacheKey(int schoolRegionID) 
		{
			return string.Format(SCHOOL_REGION_SCHOOL_CACHE_KEY_PREFIX, schoolRegionID);
		}
		private string GetSchoolRegionByStateCacheKey(int stateRegionID) 
		{
			return string.Format(SCHOOL_REGION_STATE_CACHE_KEY_PREFIX, stateRegionID);
		}
		private string GetRegionIDByPostal(int countryRegionID, string postalCode) 
		{
			return string.Format(REGION_ID_BY_POSTAL_CACHE_KEY_PREFIX, countryRegionID, postalCode);
		}
		private string GetRegionIDByCity(int parentRegionID, string description, int languageID)
		{
			return string.Format(REGION_ID_BY_CITY_CACHE_KEY_PREFIX, parentRegionID, description, languageID);
		}
		private string GetRegionLanguageCacheKey()
		{
			return REGION_LANGUAGE_CACHE_KEY_PREFIX;
		}
		private string GetAreaCodeDictionaryCacheKey()
		{
			return REGION_AREACODES_CACHE_KEY_PREFIX;
		}
		private string GetSEORegionsCacheKey()
		{
			return REGION_SEOREGIONS_CACHE_KEY_PREFIX;
		}
		private string GetDMAByZipRegionIDCacheKey(int zipRegionId)
		{
			return string.Format(DMA_BY_ZIPREGIONID_CACHE_KEY_PREFIX,zipRegionId);
		}
		private string GetAllDMASCacheKey()
		{
			return ALL_DMAS_CACHE_KEY_PREFIX;
		}

		#endregion

		#region constructor 
		private RegionBL()
		{
			_cache = Cache.Instance;
		}
		#endregion

		#region Private methods
		/*
		private Region MakeRegionFromDataRow(DataRow myRow)
		{
			Region retVal = null;

			int regionID = Conversion.CInt((myRow["RegionID"]));
			int depth = Conversion.CInt(myRow["Depth"]);
			decimal latitude = Conversion.CDecimal(myRow["Latitude"]);
			decimal longitude = Conversion.CDecimal(myRow["Longitude"]);
			string description = myRow["Description"].ToString();
			string abbreviation = null;
			if (myRow["Abbreviation"] != DBNull.Value)
			{
				abbreviation = myRow["Abbreviation"].ToString();
			}
			int mask = Conversion.CInt(myRow["Mask"]);
			int childrenDepth = Conversion.CInt(myRow["ChildrenDepth"]);

			//	Do we need to make the ParentRegion here or is that only done in the methods that
			//	return a whole tree of Regions? - dcornell

			retVal = new Region(regionID, depth, latitude, longitude, description, abbreviation, mask, childrenDepth, null);

			return(retVal);
		}
		*/
		#endregion
	}
}