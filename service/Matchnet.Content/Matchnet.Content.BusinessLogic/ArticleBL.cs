using Matchnet.Caching;
using Matchnet.Content.ValueObjects.Article;
using Matchnet.Data;
using Matchnet.Data.Exceptions;
using Matchnet.Exceptions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Data.Hydra;

using System;
using System.Data;
using System.Text;
using System.Collections;
using System.Web.Caching;

namespace Matchnet.Content.BusinessLogic
{
	/// <summary>
	/// Summary description for ArticleBL.
	/// </summary>
	public class ArticleBL
	{
		#region class variables
		private Matchnet.Caching.Cache _cache = null;
		String contentDatabaseName = "mnContent";

		private CacheItemRemovedCallback expireCallback;
		public delegate void ReplicationEventHandler(IReplicable replicableObject);
		public event ReplicationEventHandler ReplicationRequested;
		public delegate void SynchronizationEventHandler(string key, Hashtable cacheReferences);
		public event SynchronizationEventHandler SynchronizationRequested;
		#endregion

		#region singleton
		/// <summary>
		/// Singleton instance
		/// </summary>
		public static readonly ArticleBL Instance = new ArticleBL();
		#endregion
		
		#region constructor
		private ArticleBL()
		{
			_cache = Matchnet.Caching.Cache.Instance;
			expireCallback = new CacheItemRemovedCallback(this.ExpireCallback);
			contentDatabaseName = Convert.ToString(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CONTENT_LOGICAL_DATABASE_TO_USE"));
		}
		#endregion

		#region Article and Category methods
		public SiteArticle RetrieveSiteArticle(string clientHostName, CacheReference cacheReference, int articleID, int siteID)
		{
			SiteArticle siteArticle = null;

			try
			{
				string cacheKey = SiteArticle.GetCacheKey(articleID, siteID);

				siteArticle = _cache.Get(cacheKey) as SiteArticle;

				if (siteArticle == null)
				{
					try
					{
						Command cmd = new Command(contentDatabaseName, "up_SiteArticle_List", 0);

						cmd.AddParameter("@ArticleID", SqlDbType.Int, ParameterDirection.Input, articleID);
						cmd.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);

						DataTable dt = Client.Instance.ExecuteDataTable(cmd);

						if (dt.Rows.Count <= 0)
							return null;

						siteArticle = MakeSiteArticleFromDataRow(dt.Rows[0]);

						// We need the siteID to correctly retrieve the item from cache.
						siteArticle.SiteID = siteID;

						siteArticle.SetCacheKey = cacheKey;
						CacheSiteArticle(siteArticle);
					}
					catch (Exception ex)
					{
						throw(new BLException("BL error occured when retrieving SiteArticle [articleID: " + articleID.ToString() + "][siteID: " + siteID.ToString() + "].", ex));
					}
				}

				if (clientHostName != null && cacheReference != null)
				{
					siteArticle.ReferenceTracker.Add(clientHostName, cacheReference);
				}
			}
			catch (Exception ex)
			{
				throw(new BLException("BL error occured in RetrieveSiteArticle() [articleID: " + articleID.ToString() + "][siteID: " + siteID.ToString() + "].", ex));
			}

			return siteArticle;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="categoryID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		public SiteArticleCollection RetrieveCategorySiteArticles(string clientHostName, CacheReference cacheReference, int categoryID, int siteID)
		{
			SiteArticleCollection siteArticleCollection = null;

			try
			{
				string cacheKey = SiteArticleCollection.GetCacheKey(categoryID, siteID);

				siteArticleCollection = _cache.Get(cacheKey) as SiteArticleCollection;

				if (siteArticleCollection == null)
				{
					siteArticleCollection = new SiteArticleCollection(categoryID, siteID);

					try
					{
						Command cmd = new Command(contentDatabaseName, "up_SiteArticle_List", 0);

						cmd.AddParameter("@CategoryID", SqlDbType.Int, ParameterDirection.Input, categoryID);
						cmd.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);

						DataTable dt = Client.Instance.ExecuteDataTable(cmd);

						foreach(DataRow row in dt.Rows)
						{
							siteArticleCollection.Add(MakeSiteArticleFromDataRow(row));
						}

						siteArticleCollection.SetCacheKey = cacheKey;
						CacheSiteArticleCollection(siteArticleCollection);
					}
					catch (Exception ex)
					{
						throw(new BLException("BL error occured when retrieving SiteArticleCollection [categoryID: " + categoryID.ToString() + "][siteID: " + siteID.ToString() + "].", ex));
					}
				}

				if (clientHostName != null && cacheReference != null)
				{
					siteArticleCollection.ReferenceTracker.Add(clientHostName, cacheReference);
				}
			}
			catch (Exception ex)
			{
				throw(new BLException("BL error occured in RetrieveCategorySiteArticles() [categoryID: " + categoryID.ToString() + "][siteID: " + siteID.ToString() + "].", ex));
			}

			return siteArticleCollection;
		}

		/// <summary>
		/// Used by Admin Tool only.  This will return both SiteArticle and Article data.  The Article data will
		/// be presented in a SiteArticle object as well.  It will contain null/default values for the values that
		/// it does not have in common with SiteCategory.
		/// </summary>
		/// <param name="categoryID"></param>
		/// <param name="siteID"></param>
		/// <param name="forceLoadFlag"></param>
		/// <returns></returns>
		public SiteArticleCollection RetrieveCategorySiteArticlesAndArticles(int categoryID, int siteID)
		{
			SiteArticleCollection siteArticleCollection = new SiteArticleCollection(categoryID, siteID);

			try
			{
				Command cmd = new Command(contentDatabaseName, "up_SiteArticleAndArticle_List", 0);

				cmd.AddParameter("@CategoryID", SqlDbType.Int, ParameterDirection.Input, categoryID);
				cmd.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);

				DataTable dt = Client.Instance.ExecuteDataTable(cmd);

				foreach(DataRow row in dt.Rows)
				{
					siteArticleCollection.Add(MakeSiteArticleAndArticleFromDataRow(row));
				}
			}
			catch (Exception ex)
			{
				throw(new BLException("BL error occured when retrieving SiteArticleAndArticleCollection [categoryID: " + categoryID.ToString() + "][siteID: " + siteID.ToString() + "].", ex));
			}

			return siteArticleCollection;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="siteID"></param>
		/// <param name="categoryID">A categoryID that is equal to int.MinValue will return all the base level
		/// categories (categories without parent categories) for the siteID.</param>
		/// <returns></returns>
		public SiteCategoryCollection RetrieveSiteCategoryChildren(string clientHostName, CacheReference cacheReference, int siteID, int categoryID)
		{
			SiteCategoryCollection siteCategoryCollection = null;

			try
			{
				string cacheKey = SiteCategoryCollection.GetCacheKey(siteID, categoryID, SiteCategoryCollectionCacheKeySuffix.Children);

				siteCategoryCollection = _cache.Get(cacheKey) as SiteCategoryCollection;

				if (siteCategoryCollection == null)
				{
					siteCategoryCollection = new SiteCategoryCollection(categoryID, siteID);

					try
					{
						Command cmd = new Command(contentDatabaseName, "up_SiteCategory_ListChild", 0);

						cmd.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
						if (categoryID > 0)
							cmd.AddParameter("@CategoryID", SqlDbType.Int, ParameterDirection.Input, categoryID);

						DataTable dt = Client.Instance.ExecuteDataTable(cmd);

						foreach(DataRow row in dt.Rows)
						{
							siteCategoryCollection.Add(MakeSiteCategoryChildrenFromDataRow(row));
						}

						siteCategoryCollection.SetCacheKey = cacheKey;
						CacheSiteCategoryCollection(siteCategoryCollection);
					}
					catch (Exception ex)
					{
						throw(new BLException("BL error occured when retrieving children SiteCategoryCollection [categoryID: " + categoryID.ToString() + "][siteID: " + siteID.ToString() + "].", ex));
					}
				}

				if (clientHostName != null && cacheReference != null)
				{
					siteCategoryCollection.ReferenceTracker.Add(clientHostName, cacheReference);
				}
			}
			catch (Exception ex)
			{
				throw(new BLException("BL error occured in RetrieveSiteCategoryChildren() [categoryID: " + categoryID.ToString() + "][siteID: " + siteID.ToString() + "].", ex));
			}

			return siteCategoryCollection;
		}

		/// <summary>
		/// Retrieves all Categories for a given SiteID.  Used only for the Admin tool.
		/// </summary>
		/// <param name="siteID"></param>
		/// <param name="forceLoadFlag">If true, we'll load the Categories from the dbase.</param>
		/// <returns></returns>
		public SiteCategoryCollection RetrieveSiteCategories(string clientHostName, CacheReference cacheReference, 
			int siteID, bool forceLoadFlag)
		{
			SiteCategoryCollection siteCategoryCollection = null;

			try
			{
				string cacheKey = SiteCategoryCollection.GetCacheKey(siteID, int.MinValue, SiteCategoryCollectionCacheKeySuffix.All);

				if (!forceLoadFlag)
					siteCategoryCollection = _cache.Get(cacheKey) as SiteCategoryCollection;

				if (siteCategoryCollection == null)
				{
					siteCategoryCollection = new SiteCategoryCollection(int.MinValue, siteID);

					try
					{
						Command cmd = new Command(contentDatabaseName, "up_SiteCategory_ListAll", 0);

						cmd.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);

						DataTable dt = Client.Instance.ExecuteDataTable(cmd);

						foreach(DataRow row in dt.Rows)
						{
							siteCategoryCollection.Add(MakeSiteCategoryFromDataRow(row, siteID));
						}

						siteCategoryCollection.SetCacheKey = cacheKey;
						CacheSiteCategoryCollection(siteCategoryCollection);
					}
					catch (Exception ex)
					{
						throw(new BLException("BL error occured when retrieving SiteCategoryCollection [forceLoadFlag: " + forceLoadFlag.ToString() + "][siteID: " + siteID.ToString() + "].", ex));
					}
				}

				if ((forceLoadFlag == false) && (clientHostName != null) && (cacheReference != null))
				{
					siteCategoryCollection.ReferenceTracker.Add(clientHostName, cacheReference);
				}
			}
			catch (Exception ex)
			{
				throw(new BLException("BL error occured in RetrieveSiteCategoryChildren() [forceLoadFlag: " + forceLoadFlag.ToString() + "][siteID: " + siteID.ToString() + "].", ex));
			}

			return siteCategoryCollection;
		}

		/// <summary>
		/// Always load from DB.  This should only be used by the Admin tool.
		/// </summary>
		/// <param name="categoryID"></param>
		/// <returns></returns>
		public Category RetrieveCategory(int categoryID)
		{
			Category category = null;

			try
			{
				Command cmd = new Command(contentDatabaseName, "up_Category_List", 0);

				cmd.AddParameter("@CategoryID", SqlDbType.Int, ParameterDirection.Input, categoryID);

				DataTable dt = Client.Instance.ExecuteDataTable(cmd);

				category = MakeCategoryFromDataRow(dt.Rows[0]);
			}
			catch (Exception ex)
			{
				throw(new BLException("BL error occured when retrieving Category [categoryID: " + categoryID.ToString() + "].", ex));
			}

			return category;
		}

		/// <summary>
		/// Always load from DB.  This should only be used by the Admin tool.
		/// </summary>
		/// <param name="categoryID"></param>
		/// <returns></returns>
		public Article RetrieveArticle(int articleID)
		{
			Article article = null;

			try
			{
				Command cmd = new Command(contentDatabaseName, "up_Article_List", 0);

				cmd.AddParameter("@ArticleID", SqlDbType.Int, ParameterDirection.Input, articleID);

				DataTable dt = Client.Instance.ExecuteDataTable(cmd);

				article = MakeArticleFromDataRow(dt.Rows[0]);
			}
			catch (Exception ex)
			{
				throw(new BLException("BL error occured when retrieving Article [articleID: " + articleID.ToString() + "].", ex));
			}

			return article;
		}

		public int SaveCategory(Category category, Int32 memberID, Boolean isPublish)
		{
			try
			{
				Boolean isAdd = false;

				if (category.CategoryID == int.MinValue)
				{
					category.CategoryID = KeySA.Instance.GetKey("CategoryID");

					isAdd = true;
				}

				// Add a Publish and PublishAction record if this is not a Publish.
				Int32 publishID;
				if (isPublish)
				{
					publishID = category.PublishID;
				}
				else
				{
					publishID = AdminBL.Instance.SavePublish(category.CategoryID, PublishObjectType.Category, category);

					if (isAdd)
						AdminBL.Instance.SavePublishAction(publishID, 500, memberID);	// PubActPubObjID = 500 = Add.
					else
						AdminBL.Instance.SavePublishAction(publishID, 501, memberID);	// PubActPubObjID = 501 = Edit.
				}

				Command cmd = new Command(contentDatabaseName, "up_Category_Save", 0);

				cmd.AddParameter("@CategoryID", SqlDbType.Int, ParameterDirection.Input, category.CategoryID);
				if (category.ParentCategoryID != int.MinValue)
					cmd.AddParameter("@ParentCategoryID", SqlDbType.Int, ParameterDirection.Input, category.ParentCategoryID);
				cmd.AddParameter("@ListOrder", SqlDbType.Int, ParameterDirection.Input, category.ListOrder);
				cmd.AddParameter("@Constant", SqlDbType.VarChar, ParameterDirection.Input, category.Constant);
				cmd.AddParameter("@PublishID", SqlDbType.Int, ParameterDirection.Input, publishID);

				Client.Instance.ExecuteAsyncWrite(cmd);

				return category.CategoryID;
			}
			catch (Exception ex)
			{
				throw(new BLException("BL error occured when saving Category.", ex));
			}
		}

		public int SaveSiteCategory(SiteCategory siteCategory, Int32 memberID, Boolean isPublish)
		{
			try
			{
				Boolean isAdd = false;

				if (siteCategory.SiteCategoryID == Constants.NULL_INT)
				{
					siteCategory.SiteCategoryID = KeySA.Instance.GetKey("SiteCategoryID");

					isAdd = true;
				}

				// Add a Publish and PublishAction record if this is not a Publish.
				Int32 publishID;
				if (isPublish)
				{
					publishID = siteCategory.PublishID;
				}
				else
				{
					publishID = AdminBL.Instance.SavePublish(siteCategory.SiteCategoryID, PublishObjectType.SiteCategory, siteCategory);

					if (isAdd)
						AdminBL.Instance.SavePublishAction(publishID, 200, memberID);	// PubActPubObjID = 200 = Add.
					else
						AdminBL.Instance.SavePublishAction(publishID, 201, memberID);	// PubActPubObjID = 201 = Edit.
				}

				Command cmd = new Command(contentDatabaseName, "up_SiteCategory_Save", 0);

				cmd.AddParameter("@SiteCategoryID", SqlDbType.Int, ParameterDirection.Input, siteCategory.SiteCategoryID);
				cmd.AddParameter("@CategoryID", SqlDbType.Int, ParameterDirection.Input, siteCategory.CategoryID);
				cmd.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteCategory.SiteID);
				cmd.AddParameter("@Content", SqlDbType.NText, ParameterDirection.Input, siteCategory.Content);
				cmd.AddParameter("@PublishedFlag", SqlDbType.Bit, ParameterDirection.Input, siteCategory.PublishedFlag);
				cmd.AddParameter("@PublishID", SqlDbType.Int, ParameterDirection.Input, publishID);

				// Need to make this a synchronous write because we are seeing cache synch errors as a result of 
				// synchronicity problems.  If replication is asynchronous, it is possible that the cache synchronization will
				// happen before the save occurs.  Thus, the web boxes will all load up the old version of the object.
				SyncWriter syncWriter = new SyncWriter();
				Exception swEx;
				syncWriter.Execute(cmd, out swEx);
				syncWriter.Dispose();
				if (swEx != null)
				{
					throw swEx;
				}

				SyncAndReplSiteCategoryCollections(siteCategory.SiteID, siteCategory.CategoryID);

				return siteCategory.SiteCategoryID;
			}
			catch (Exception ex)
			{
				throw(new BLException("BL error occured when saving SiteCategory.", ex));
			}
		}

		public int SaveArticle(Article article, Int32 memberID, Boolean isPublish)
		{
			try
			{
				Boolean isAdd = false;

				if (article.ArticleID == int.MinValue)
				{
					article.ArticleID = KeySA.Instance.GetKey("ArticleID");

					isAdd = true;
				}

				// Add a Publish and PublishAction record if this is not a Publish.
				Int32 publishID;
				if (isPublish)
				{
					publishID = article.PublishID;
				}
				else
				{
					publishID = AdminBL.Instance.SavePublish(article.ArticleID, PublishObjectType.Article, article);

					if (isAdd)
						AdminBL.Instance.SavePublishAction(publishID, 600, memberID);	// PubActPubObjID = 600 = Add.
					else
						AdminBL.Instance.SavePublishAction(publishID, 601, memberID);	// PubActPubObjID = 601 = Edit.
				}

				Command cmd = new Command(contentDatabaseName, "up_Article_Save", 0);

				cmd.AddParameter("@ArticleID", SqlDbType.Int, ParameterDirection.Input, article.ArticleID);
				cmd.AddParameter("@CategoryID", SqlDbType.Int, ParameterDirection.Input, article.CategoryID);
				cmd.AddParameter("@Constant", SqlDbType.NVarChar, ParameterDirection.Input, article.Constant);
				cmd.AddParameter("@Content", SqlDbType.NText, ParameterDirection.Input, article.DefaultContent);
				cmd.AddParameter("@PublishID", SqlDbType.Int, ParameterDirection.Input, publishID);

				Client.Instance.ExecuteAsyncWrite(cmd);

				return article.ArticleID;
			}
			catch (Exception ex)
			{
				throw(new BLException("BL error occured when saving Article [articleID: " + article.ArticleID.ToString() + "][categoryID: " + article.CategoryID.ToString() + "].", ex));
			}
		}

		public int SaveSiteArticle(SiteArticle siteArticle, Int32 memberID, Boolean isPublish)
		{
			try
			{
				Boolean isAdd = false;

				if (siteArticle.SiteArticleID == int.MinValue || siteArticle.SiteArticleID == Matchnet.Constants.NULL_INT)
				{
					siteArticle.SiteArticleID = KeySA.Instance.GetKey("SiteArticleID");

					isAdd = true;
				}

				// Add a Publish and PublishAction record if this is not a Publish.
				Int32 publishID;
				if (isPublish)
				{
					publishID = siteArticle.PublishID;
				}
				else
				{
					publishID = AdminBL.Instance.SavePublish(siteArticle.SiteArticleID, PublishObjectType.SiteArticle, siteArticle);

					if (isAdd)
						AdminBL.Instance.SavePublishAction(publishID, 300, memberID);	// PubActPubObjID = 300 = Add.
					else
						AdminBL.Instance.SavePublishAction(publishID, 301, memberID);	// PubActPubObjID = 301 = Edit.
				}

				Command cmd = new Command(contentDatabaseName, "up_SiteArticle_Save", 0);

				cmd.AddParameter("@SiteArticleID", SqlDbType.Int, ParameterDirection.Input, siteArticle.SiteArticleID);
				cmd.AddParameter("@ArticleID", SqlDbType.Int, ParameterDirection.Input, siteArticle.ArticleID);
				cmd.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteArticle.SiteID);
				cmd.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, siteArticle.MemberID);
				if (siteArticle.FileID != int.MinValue)
					cmd.AddParameter("@FileID", SqlDbType.Int, ParameterDirection.Input, siteArticle.FileID);
				cmd.AddParameter("@Title", SqlDbType.NVarChar, ParameterDirection.Input, siteArticle.Title);
				cmd.AddParameter("@Content", SqlDbType.NText, ParameterDirection.Input, siteArticle.Content);
				cmd.AddParameter("@Ordinal", SqlDbType.Int, ParameterDirection.Input, siteArticle.Ordinal);
				cmd.AddParameter("@PublishedFlag", SqlDbType.Bit, ParameterDirection.Input, siteArticle.PublishedFlag);
				cmd.AddParameter("@FeaturedFlag", SqlDbType.Bit, ParameterDirection.Input, siteArticle.FeaturedFlag);
				cmd.AddParameter("@PublishID", SqlDbType.Int, ParameterDirection.Input, publishID);
				cmd.AddParameter("@PageTitle", SqlDbType.NVarChar, ParameterDirection.Input, siteArticle.PageTitle);
				cmd.AddParameter("@MetaDescription", SqlDbType.NVarChar, ParameterDirection.Input, siteArticle.MetaDescription);

				// Need to make this a synchronous write because we are seeing cache synch errors as a result of 
				// synchronicity problems.  If replication is asynchronous, it is possible that the cache synchronization will
				// happen before the save occurs.  Thus, the web boxes will all load up the old version of the object.
				SyncWriter syncWriter = new SyncWriter();
				Exception swEx;
				syncWriter.Execute(cmd, out swEx);
				syncWriter.Dispose();
				if (swEx != null)
				{
					throw swEx;
				}

				SyncAndReplSiteArticle(siteArticle.SiteID, siteArticle.ArticleID);
				SyncAndReplSiteArticleCollection(siteArticle.SiteID, siteArticle.ArticleData.CategoryID);

				return siteArticle.SiteArticleID;
			}
			catch (Exception ex)
			{
				throw(new BLException("BL error occured when saving SiteArticle.", ex));
			}
		}

		private void SyncAndReplSiteArticle(Int32 siteID, Int32 articleID)
		{
			String cacheKey = SiteArticle.GetCacheKey(articleID, siteID);

			// Removing the local copy also causes an ExpireCallback (synchronization) so no need to call it here.
			_cache.Remove(cacheKey);

			SiteArticle siteArticle = RetrieveSiteArticle(null, null, articleID, siteID);
			
			if (siteArticle != null)
				ReplicationRequested(siteArticle);
		}

		private void SyncAndReplSiteArticleCollection(Int32 siteID, Int32 categoryID)
		{
			String cacheKey = SiteArticleCollection.GetCacheKey(categoryID, siteID);

			// Removing the local copy also causes an ExpireCallback (synchronization) so no need to call it here.
			_cache.Remove(cacheKey);

			SiteArticleCollection siteArticleCollection = RetrieveCategorySiteArticles(null, null, categoryID, siteID);
			
			if (siteArticleCollection != null)
				ReplicationRequested(siteArticleCollection);
		}

		/// <summary>
		/// Syncs and replicates all SiteCategoryCollections from the cache.
		/// </summary>
		/// <param name="categoryID"></param>
		private void SyncAndReplSiteCategoryCollections(Int32 siteID, Int32 categoryID)
		{
			// Delete every type of SiteCategoryCollection.
			foreach (String suffix in Enum.GetNames(typeof(SiteCategoryCollectionCacheKeySuffix)))
			{
				String cacheKey = SiteCategoryCollection.GetCacheKey(siteID, categoryID, suffix);

				// Removing the local copy also causes an ExpireCallback (synchronization) so no need to call it here.
				_cache.Remove(cacheKey);

				SiteCategoryCollection siteCategoryCollection;
				if (suffix == SiteCategoryCollectionCacheKeySuffix.Children.ToString())
				{
					// Children.
					siteCategoryCollection = RetrieveSiteCategoryChildren(null, null, siteID, categoryID);
				}
				else
				{
					// All.
					siteCategoryCollection = RetrieveSiteCategories(null, null, siteID, true);
				}
				
				if (siteCategoryCollection != null)
					ReplicationRequested(siteCategoryCollection);
			}
		}

		private SiteArticle MakeSiteArticleFromDataRow(DataRow row)
		{
			int articleID = System.Convert.ToInt32(row["ArticleID"]);
			string title = System.Convert.ToString(row["Title"]);
			string content = System.Convert.ToString(row["Content"]);
			int fileID = System.Convert.ToInt32(row["FileID"] != DBNull.Value ? row["FileID"] : int.MinValue);
			int ordinal = System.Convert.ToInt16(row["Ordinal"]);
			bool featuredFlag = System.Convert.ToBoolean(row["FeaturedFlag"]);
			string pageTitle = System.Convert.ToString(row["PageTitle"]);
			string metaDescription = System.Convert.ToString(row["MetaDescription"]);
			
			int publishID = Constants.NULL_INT;
			if (row["PublishID"] != DBNull.Value)
			{
				publishID = Convert.ToInt32(row["PublishID"]);
			}

			SiteArticle siteArticle;
			if (publishID != Constants.NULL_INT)
				siteArticle = new SiteArticle(articleID, title, content, fileID, ordinal, featuredFlag, publishID);
			else
				siteArticle = new SiteArticle(articleID, title, content, fileID, ordinal, featuredFlag);

			siteArticle.PageTitle = pageTitle;
			siteArticle.MetaDescription = metaDescription;

			return siteArticle;
		}

		private SiteArticle MakeSiteArticleAndArticleFromDataRow(DataRow row)
		{
			int siteArticleID = System.Convert.ToInt32(row["SiteArticleID"] != DBNull.Value ? row["SiteArticleID"] : int.MinValue);
			int articleID = System.Convert.ToInt32(row["ArticleID"]);
			int siteID = System.Convert.ToInt32(row["SiteID"]);
			string constant = System.Convert.ToString(row["Constant"]);
			int memberID = System.Convert.ToInt32(row["MemberID"] != DBNull.Value ? row["MemberID"] : int.MinValue);
			string title = System.Convert.ToString(row["Title"] != DBNull.Value ? row["Title"] : string.Empty);
			string content = System.Convert.ToString(row["Content"]);
			int fileID = System.Convert.ToInt32(row["FileID"] != DBNull.Value ? row["FileID"] : int.MinValue);
			int ordinal = System.Convert.ToInt32(row["Ordinal"] != DBNull.Value ? row["Ordinal"] : int.MinValue);
			DateTime lastUpdated = System.Convert.ToDateTime(row["LastUpdated"]);
			bool publishedFlag = System.Convert.ToBoolean(row["PublishedFlag"]);
			bool featuredFlag = System.Convert.ToBoolean(row["FeaturedFlag"]);
			string pageTitle = System.Convert.ToString(row["PageTitle"]);
			string metaDescription = System.Convert.ToString(row["MetaDescription"]);

			int publishID = Constants.NULL_INT;
			if (row["PublishID"] != DBNull.Value)
			{
				publishID = Convert.ToInt32(row["PublishID"]);
			}

			SiteArticle siteArticle;
			if (publishID != Constants.NULL_INT)
				siteArticle = new SiteArticle(siteArticleID, articleID, siteID, constant, memberID, ordinal, lastUpdated, publishedFlag,
					featuredFlag, fileID, title, content, publishID);
			else
				siteArticle = new SiteArticle(siteArticleID, articleID, siteID, constant, memberID, ordinal, lastUpdated, publishedFlag,
					featuredFlag, fileID, title, content);

			siteArticle.PageTitle = pageTitle;
			siteArticle.MetaDescription = metaDescription;

			return siteArticle;
		}

		private SiteCategory MakeSiteCategoryChildrenFromDataRow(DataRow row)
		{
			int categoryID = System.Convert.ToInt32(row["CategoryID"]);
			int parentCategoryID = row["ParentCategoryID"] == DBNull.Value ? int.MinValue : System.Convert.ToInt32(row["ParentCategoryID"]);
			string content = System.Convert.ToString(row["Content"]);
			int listOrder = System.Convert.ToInt32(row["ListOrder"]);

			SiteCategory siteCategory = new SiteCategory(categoryID, parentCategoryID, content, listOrder);

			return siteCategory;
		}

		private SiteCategory MakeSiteCategoryFromDataRow(DataRow row, int siteID)
		{
			int siteCategoryID = row["SiteCategoryID"] == DBNull.Value ? int.MinValue : Convert.ToInt32(row["SiteCategoryID"]);
			int categoryID = System.Convert.ToInt32(row["CategoryID"]);
			int parentCategoryID = row["parentCategoryID"] == DBNull.Value ? int.MinValue : Convert.ToInt32(row["parentCategoryID"]);
			string constant = System.Convert.ToString(row["Constant"]);
			string content = row["Content"] == DBNull.Value ? "[" + constant + "]" : System.Convert.ToString(row["Content"]);
			bool publishedFlag = row["PublishedFlag"] == DBNull.Value ? false : System.Convert.ToBoolean(row["PublishedFlag"]);
			DateTime lastUpdated = row["LastUpdated"] == DBNull.Value ? DateTime.MinValue : System.Convert.ToDateTime(row["LastUpdated"]);
			
			int publishID = Constants.NULL_INT;
			if (row["PublishID"] != DBNull.Value)
			{
				publishID = Convert.ToInt32(row["PublishID"]);
			}

			SiteCategory siteCategory = new SiteCategory(siteCategoryID, categoryID, siteID, parentCategoryID, content, constant, publishedFlag, lastUpdated, publishID);

			return siteCategory;
		}

		private Category MakeCategoryFromDataRow(DataRow row)
		{
			int categoryID = System.Convert.ToInt32(row["CategoryID"]);
			int parentCategoryID = row["parentCategoryID"] == DBNull.Value ? int.MinValue : Convert.ToInt32(row["parentCategoryID"]);
			int listOrder = System.Convert.ToInt32(row["ListOrder"]);
			string constant = System.Convert.ToString(row["Constant"]);
			DateTime lastUpdated = row["LastUpdated"] == DBNull.Value ? DateTime.MinValue : System.Convert.ToDateTime(row["LastUpdated"]);

			int publishID = Constants.NULL_INT;
			if (row["PublishID"] != DBNull.Value)
			{
				publishID = Convert.ToInt32(row["PublishID"]);
			}
			
			Category category = new Category(categoryID, parentCategoryID, listOrder, constant, lastUpdated, publishID);

			return category;
		}

		private Article MakeArticleFromDataRow(DataRow row)
		{
			int articleID = System.Convert.ToInt32(row["ArticleID"]);
			int categoryID = row["CategoryID"] == DBNull.Value ? int.MinValue : Convert.ToInt32(row["CategoryID"]);
			string defaultContent = System.Convert.ToString(row["Content"]);
			string constant = System.Convert.ToString(row["Constant"]);
			DateTime lastUpdated = row["LastUpdated"] == DBNull.Value ? DateTime.MinValue : System.Convert.ToDateTime(row["LastUpdated"]);
			
			int publishID = Constants.NULL_INT;
			if (row["PublishID"] != DBNull.Value)
			{
				publishID = Convert.ToInt32(row["PublishID"]);
			}

			Article article = new Article(articleID, categoryID, defaultContent, constant, lastUpdated, publishID);

			return article;
		}

		private void ExpireCallback(string key, object value, CacheItemRemovedReason callbackreason) 
		{
			switch (value.GetType().Name)
			{
				case "SiteArticle":
					SiteArticle siteArticle = value as SiteArticle;
					SynchronizationRequested(key,
						siteArticle.ReferenceTracker.Purge(Constants.NULL_STRING));
					break;

				case "SiteArticleCollection":
					SiteArticleCollection siteArticleCollection = value as SiteArticleCollection;
					SynchronizationRequested(key,
						siteArticleCollection.ReferenceTracker.Purge(Constants.NULL_STRING));
					break;

				case "SiteCategoryCollection":
					SiteCategoryCollection siteCategoryCollection = value as SiteCategoryCollection;
					SynchronizationRequested(key,
						siteCategoryCollection.ReferenceTracker.Purge(Constants.NULL_STRING));
					break;
			}
		}

		public void CacheReplicatedObject(IReplicable replicableObject)
		{
			switch (replicableObject.GetType().Name)
			{
				case "SiteArticle":
					CacheSiteArticle((SiteArticle)replicableObject);
					break;

				case "SiteArticleCollection":
					CacheSiteArticleCollection((SiteArticleCollection)replicableObject);
					break;

				case "SiteCategoryCollection":
					CacheSiteCategoryCollection((SiteCategoryCollection)replicableObject);
					break;

				default:
					throw new Exception("invalid replication object type received (" + replicableObject.GetType().ToString() + ")");
			}
		}
		#endregion

		#region WSO Methods
		// WSO Methods should only deal with mnContent on dev.

		public WSOListCollection RetrieveWSOListCollection()
		{
			try
			{
				WSOListCollection wsoListCollection = new WSOListCollection();

				Command cmd = new Command(contentDatabaseName, "up_WSOList_List", 0);

				DataTable dt = Client.Instance.ExecuteDataTable(cmd);

				foreach(DataRow row in dt.Rows)
				{
					WSOList wsoList = new WSOList(
						Convert.ToInt32(row["WSOListID"]),
						Convert.ToString(row["Name"]),
						Convert.ToString(row["Note"]),
						Convert.ToDateTime(row["InsertDateTime"]),
						Convert.ToDateTime(row["UpdateDateTime"])
						);

					wsoListCollection.Add(wsoList);
				}

				return wsoListCollection;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured in RetrieveWSOListCollection().", ex));
			}
		}

		public void SaveWSOList(WSOList wsoList)
		{
			try
			{
				Command cmd = new Command(contentDatabaseName, "up_WSOList_Save", 0);

				cmd.AddParameter("@WSOListID", SqlDbType.Int, ParameterDirection.Input, wsoList.WSOListID);
				cmd.AddParameter("@Name", SqlDbType.NVarChar, ParameterDirection.Input, wsoList.Name);
				cmd.AddParameter("@Note", SqlDbType.NVarChar, ParameterDirection.Input, wsoList.Note);

				Client.Instance.ExecuteAsyncWrite(cmd);
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured in SaveWSOList().", ex));
			}
		}

		public void CreateWSOList(string name, string note)
		{
			try
			{
				WSOList wsoList = new WSOList(KeySA.Instance.GetKey("WSOListID"), name, note);

				SaveWSOList(wsoList);
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured in CreateWSOList().", ex));
			}
		}


		public WSOItemCollection RetrieveWSOItemCollection(int wsoListID)
		{
			try
			{
				WSOItemCollection wsoItemCollection = new WSOItemCollection();

				Command cmd = new Command(contentDatabaseName, "up_WSOItem_List", 0);
				
				cmd.AddParameter("@WSOListID", SqlDbType.Int, ParameterDirection.Input, wsoListID);
				
				DataTable dt = Client.Instance.ExecuteDataTable(cmd);

				foreach(DataRow row in dt.Rows)
				{
					WSOItem wsoItem = new WSOItem(
						Convert.ToInt32(row["WSOItemID"]),
						Convert.ToInt32(row["WSOListID"]),
						Convert.ToInt32(row["SiteID"]),
						Convert.ToInt32(row["ItemID"]),
						Convert.ToInt32(row["ItemTypeID"]),
						Convert.ToDateTime(row["InsertDateTime"]),
						Convert.ToDateTime(row["UpdateDateTime"])
						);

					wsoItemCollection.Add(wsoItem);
				}

				return wsoItemCollection;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured in RetrieveWSOListCollection().", ex));
			}
		}
		public void SaveWSOItem(WSOItem wsoItem)
		{
			try
			{
				Command cmd = new Command(contentDatabaseName, "up_WSOItem_Save", 0);

				cmd.AddParameter("@WSOItemID", SqlDbType.Int, ParameterDirection.Input, wsoItem.WSOItemID);
				cmd.AddParameter("@WSOListID", SqlDbType.Int, ParameterDirection.Input, wsoItem.WSOListID);
				cmd.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, wsoItem.SiteID);
				cmd.AddParameter("@ItemID", SqlDbType.Int, ParameterDirection.Input, wsoItem.ItemID);
				cmd.AddParameter("@ItemTypeID", SqlDbType.Int, ParameterDirection.Input, wsoItem.ItemTypeID);

				Client.Instance.ExecuteAsyncWrite(cmd);
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured in SaveWSOItem().", ex));
			}
		}
		public void DeleteWSOItem(WSOItem wsoItem)
		{
			try
			{
				Command cmd = new Command(contentDatabaseName, "up_WSOItem_Delete", 0);

				cmd.AddParameter("@WSOItemID", SqlDbType.Int, ParameterDirection.Input, wsoItem.WSOItemID);

				Client.Instance.ExecuteAsyncWrite(cmd);
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured in DeleteWSOItem().", ex));
			}
		}
		public void CreateWSOItem(int wsoListID, int siteID, int itemID, int itemTypeID)
		{
			try
			{
				WSOItem wsoItem = new WSOItem(KeySA.Instance.GetKey("WSOItemID"), wsoListID, siteID, itemID, itemTypeID);

				SaveWSOItem(wsoItem);
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured in CreateWSOItem().", ex));
			}
		}


		#endregion

		#region Cache Methods
		private void CacheSiteArticle(SiteArticle pSiteArticle) 
		{
			pSiteArticle.CachePriority = CacheItemPriorityLevel.Normal;
			pSiteArticle.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_ARTICLE_SVC"));
			_cache.Insert(pSiteArticle, expireCallback);
		}

		private void CacheSiteArticleCollection(SiteArticleCollection pSiteArticleCollection)
		{
			pSiteArticleCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pSiteArticleCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_ARTICLE_SVC"));
			_cache.Insert(pSiteArticleCollection, expireCallback);
		}

		private void CacheSiteCategoryCollection(SiteCategoryCollection pSiteCategoryCollection)
		{
			pSiteCategoryCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pSiteCategoryCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_ARTICLE_SVC"));
			_cache.Insert(pSiteCategoryCollection, expireCallback);
		}
		#endregion
	}
}