using Matchnet.Caching;
using Matchnet.Content.ValueObjects.Image;
using Matchnet.Data;
using Matchnet.Data.Exceptions;
using Matchnet.Exceptions;

using System;
using System.Data;

namespace Matchnet.Content.BusinessLogic
{
	/// <summary>
	/// Summary description for ImageBL.
	/// </summary>
	public class ImageBL
	{

		#region constants
		private const string IMAGE_ROOT_CACHE_KEY_PREFIX = "~IMAGEUNCROOT";
		private const string IMAGE_LIST_CACHE_KEY_PREFIX = "~IMAGELIST^{0}{1}{2}{3}{4}{5}";
		#endregion

		#region class variables
		Cache _cache = null;
		#endregion
		
		#region singleton
		/// <summary>
		/// Singleton instance
		/// </summary>
		public static readonly ImageBL Instance = new ImageBL();
		#endregion

		#region constructor
		private ImageBL()
		{
			_cache = Cache.Instance;
		}
		#endregion

		/// <summary>
		/// Retrieves the UNC root for images
		/// </summary>
		/// <returns>UNC root as a string</returns>
		public ImageRoot GetImageUNCRoot()
		{
			ImageRoot retVal = null;

			retVal = _cache.Get(GetImageRootCacheKey()) as ImageRoot;
			if(retVal == null) 
			{
				retVal = new ImageRoot(
					Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_IMAGEBL_IMAGEROOT"));

				retVal.SetCacheKey = GetImageRootCacheKey();
				CacheImageRoot(retVal);
			}
			return(retVal);
		}

		#region Replication
		/// <summary>
		/// Accepts an IReplicable object and updates the local service cache.
		/// </summary>
		/// <param name="pReplicableObject"></param>
		public void CacheReplicatedObject(IReplicable pReplicableObject)
		{
			switch (pReplicableObject.GetType().Name)
			{
				case "ImageRoot":
					CacheImageRoot((ImageRoot)pReplicableObject);
					break;
				default:
					throw new Exception("invalid replication object type received (" + pReplicableObject.GetType().ToString() + ")");
			}
		}
		#endregion

		#region private Cache Routines
		private void CacheImageRoot(ImageRoot pImageRoot) 
		{
			pImageRoot.CachePriority = CacheItemPriorityLevel.Normal;
			pImageRoot.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_IMAGE_SVC"));
			_cache.Insert(pImageRoot);
		}
		private void CacheImageList(ImageList pImageList) 
		{
			pImageList.CachePriority = CacheItemPriorityLevel.Normal;
			pImageList.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_IMAGE_SVC"));
			_cache.Insert(pImageList);
		}

		private string GetImageRootCacheKey()
		{
			return IMAGE_ROOT_CACHE_KEY_PREFIX;
		}

		private string GetImageListCacheKey(string filename, int applicationID, int privateLabelID, int basePrivateLabelID, int languageID, int domainID)
		{
			return String.Format(IMAGE_LIST_CACHE_KEY_PREFIX, filename, applicationID, privateLabelID, basePrivateLabelID, languageID, domainID);
		}
		#endregion
	}
}
