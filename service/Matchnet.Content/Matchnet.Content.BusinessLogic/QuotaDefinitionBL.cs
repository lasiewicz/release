using System;
using System.Collections;
using System.Data;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Data;
using Matchnet.Exceptions;
using Matchnet.Content.ValueObjects.Quotas;


namespace Matchnet.Content.BusinessLogic
{
    public class QuotaDefinitionBL
    {
        public readonly static QuotaDefinitionBL Instance = new QuotaDefinitionBL();

        private Matchnet.Caching.Cache _cache;

        private QuotaDefinitionBL()
        {
            _cache = Matchnet.Caching.Cache.Instance;
        }


        public QuotaDefinitions GetQuotaDefinitions()
        {
            try
            {
                QuotaDefinitions quotaDefinitions = _cache.Get(QuotaDefinitions.CACHE_KEY) as QuotaDefinitions;

                if (quotaDefinitions == null)
                {
                    Command command = new Command("mnSystem", "dbo.up_QuotaType_List", 0);
                    DataSet ds = Client.Instance.ExecuteDataSet(command);
                    DataTable dt = ds.Tables[0];
                    DataTable dt2 = ds.Tables[1];
                    Hashtable ht = new Hashtable();

                    #region Global
                    for (Int32 rowNum = 0; rowNum < dt.Rows.Count; rowNum++)
                    {
                        Int32 duration = Constants.NULL_INT;
                        DurationType durationType = DurationType.None;
                        DataRow row = dt.Rows[rowNum];

                        QuotaType quotaType = (QuotaType)Enum.Parse(typeof(QuotaType),
                            row["QuotaTypeID"].ToString());

                        if (row["Duration"] != DBNull.Value && row["DurationTypeID"] != DBNull.Value)
                        {
                            duration = Convert.ToInt32(row["Duration"]);
                            durationType = (DurationType)Enum.Parse(typeof(DurationType),
                                row["DurationTypeID"].ToString());
                        }

                        QuotaDefinition quotaDefinition = new QuotaDefinition(Convert.ToInt32(row["MaxAllowed"]),
                            Convert.ToBoolean(row["Sliding"]),
                            duration,
                            durationType);

                        ht.Add(quotaType.ToString(), quotaDefinition);
                    }
                    #endregion

                    #region Community
                    for (Int32 rowNum = 0; rowNum < dt2.Rows.Count; rowNum++)
                    {
                        Int32 duration = Constants.NULL_INT;
                        DurationType durationType = DurationType.None;
                        DataRow row = dt2.Rows[rowNum];

                        QuotaType quotaType = (QuotaType)Enum.Parse(typeof(QuotaType),
                            row["QuotaTypeID"].ToString());

                        if (row["Duration"] != DBNull.Value && row["DurationTypeID"] != DBNull.Value)
                        {
                            duration = Convert.ToInt32(row["Duration"]);
                            durationType = (DurationType)Enum.Parse(typeof(DurationType),
                                row["DurationTypeID"].ToString());
                        }

                        QuotaDefinition quotaDefinition = new QuotaDefinition(Convert.ToInt32(row["MaxAllowed"]),
                            Convert.ToBoolean(row["Sliding"]),
                            duration,
                            durationType);

                        ht.Add(quotaType.ToString() + "_" + Convert.ToInt32(row["CommunityID"]), quotaDefinition);
                    }
                    #endregion

                    quotaDefinitions = new QuotaDefinitions(ht);

                    _cache.Add(quotaDefinitions);
                }

                return quotaDefinitions;
            }
            catch (Exception ex)
            {
                throw new BLException("Error retrieving quota definitions.",
                    ex);
            }
        }
    }
}
