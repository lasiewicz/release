﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Registration;
using Matchnet.Data;
using Matchnet.Data.Configuration;
using Matchnet.Exceptions;

namespace Matchnet.Content.BusinessLogic
{
    public class RegistrationMetadataBL
    {
        public readonly static RegistrationMetadataBL Instance = new RegistrationMetadataBL();

		private Matchnet.Caching.Cache _cache;

        private RegistrationMetadataBL()
		{
			_cache = Matchnet.Caching.Cache.Instance;
		}

        public List<Scenario> GetScenarios()
        {
            List<Scenario> scenarios = null;

            try
            {
                var sites = BrandConfigSA.Instance.GetSites();

                foreach(Site site in sites)
                {
                    if(site.HasRegistrationMetadata)
                    {
                        if(scenarios == null) { scenarios = new List<Scenario>();}
                        try
                        {
                            var command = new Command("mnSystem", "up_RegScenarioMetadata_Get_By_Site", 0);
                            command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, site.SiteID);
                            var ds = Client.Instance.ExecuteDataSet(command);
                            var siteScenarios = DataTableToScenarios(ds);
                            scenarios.AddRange(siteScenarios);
                        }
                        catch (Exception ex)
                        {
                            new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
                                                         "Failed to load registration metadata for site " + site.SiteID,
                                                         ex);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                throw (new BLException("BL error occured when retrieving registration scenario metadata", ex));
            }

            return scenarios;
        }

        /// <summary>
        /// Returns all active/inactive scenarios. It only retruns scenario info from the RegScenario table; no condition, step, template, or control info.
        /// </summary>
        /// <returns></returns>
        public List<Scenario> GetScenariosForAdmin()
        {
            List<Scenario> scenarios = null;
            try
            {
                Command cmd = new Command("mnSystem", "up_RegScenarioMetadata_Get_ForAdmin", 0);
                var dt = Client.Instance.ExecuteDataTable(cmd);

                if (dt == null)
                    return null;

                scenarios = new List<Scenario>();
                foreach(DataRow row in dt.Rows)
                {
                    scenarios.Add(new Scenario() {ID=Convert.ToInt32(row["RegScenarioID"]),
                        SiteID=Convert.ToInt32(row["SiteID"]),
                        Name=row["Name"].ToString(),
                        IsOnePageReg = Convert.ToBoolean(row["IsOnePageReg"]),
                        Weight = Convert.ToDouble(row["Weight"]),
                        IsControl = Convert.ToBoolean(row["IsControl"]),
                        DeviceType = (DeviceType)(Convert.ToInt32(row["DeviceTypeID"])),
                        ExternalSessionType = (ExternalSessionType)(Convert.ToInt32(row["ExternalSessionTypeID"]))
                        });
                }

                return scenarios;
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when retrieving registration scenario metadata for admin", ex));
            }
        }

        /// <summary>
        /// Returns all scenarios for reg admintool. 
        /// </summary>
        /// <returns></returns>
        public List<Scenario> GetAllRegScenarios()
        {
            List<Scenario> scenarios = null;

            try
            {
                var sites = BrandConfigSA.Instance.GetSites();

                foreach (Site site in sites)
                {
                    if (site.HasRegistrationMetadata)
                    {
                        if (scenarios == null) { scenarios = new List<Scenario>(); }
                        try
                        {
                            var command = new Command("mnSystem", "up_RegScenarios_All_Get_By_Site", 0);
                            command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, site.SiteID);
                            var ds = Client.Instance.ExecuteDataSet(command);
                            var siteScenarios = DataTableToScenarios(ds);
                            scenarios.AddRange(siteScenarios);
                        }
                        catch (Exception ex)
                        {
                            new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
                                                         "Failed to load registration metadata for site " + site.SiteID,
                                                         ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when retrieving registration scenario metadata", ex));
            }

            return scenarios;
        }

        public void SaveScenarioStatusWeight(int regScenarioId, bool activate, double weight, string updatedBy, int siteID, int deviceTypeID)
        {
            try
            {
                var command = new Command("mnSystem", "up_RegScenarioStatusWeightUpdate", 0);
                command.AddParameter("@ScenarioId", SqlDbType.Int, ParameterDirection.Input, regScenarioId);
                command.AddParameter("@UpdatedBy", SqlDbType.NVarChar, ParameterDirection.Input, updatedBy);
                command.AddParameter("@Active", SqlDbType.Bit, ParameterDirection.Input, (activate) ? 1 : 0);
                command.AddParameter("@Weight", SqlDbType.Decimal, ParameterDirection.Input, weight);
                Client.Instance.ExecuteAsyncWrite(command);

            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when saving registration scenario status and weight", ex));
            }
        }

        public List<RegControl> GetAllRegControls(int deviceTypeId)
        {
            var allcontrols = new List<RegControl>();
            try
            {
                var command = new Command("mnSystem", "up_RegControls_All_Get", 0);
                command.AddParameter("@DeviceTypeId", SqlDbType.Int, ParameterDirection.Input, deviceTypeId);
                DataSet ds = Client.Instance.ExecuteDataSet(command);

                DataTable controlsTable = ds.Tables[0];
                DataTable overridesTable = ds.Tables[1];

                controlsTable.ChildRelations.Add(
                    "fk_control_Overrides",
                    new DataColumn[] { controlsTable.Columns["RegControlSiteID"] },
                    new DataColumn[] { overridesTable.Columns["RegControlSiteID"] }
                    );

                foreach (DataRow controlRow in controlsTable.Rows)
                {
                    var control = DataRowToRegControl(controlRow);

                    DataRow[] overRideRows = controlRow.GetChildRows("fk_control_Overrides");
                    {
                        if(overRideRows != null && overRideRows.Count() == 1)
                        {
                            var deviceOverride = DataRowToRegControlDeviceOverride(overRideRows[0]);
                            control.AdditionalText = deviceOverride.AdditionalText;
                            control.Label = deviceOverride.Label;
                            control.RequiredErrorMessage = deviceOverride.RequiredErrorMessage;
                        }
                    }

                    allcontrols.Add(control);
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when retrieving registration scenario controls", ex));
            }
            return allcontrols;
        }

        public List<RegControl> GetRegControlsRequiredForRegScenarioSite(int siteId)
        {
            var requiredRegControls = new List<RegControl>();
            try
            {
                var command = new Command("mnSystem", "up_GetRegControlsRequiredForScenario", 0);
                command.AddParameter("@SiteID",SqlDbType.Int, ParameterDirection.Input, siteId);
               
                DataSet ds = Client.Instance.ExecuteDataSet(command);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        RegControl dataRowToRegControl = DataRowToRegControl(row);
                        requiredRegControls.Add(dataRowToRegControl);
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when retrieving required reg controls for site:"+siteId, ex));
            }
            return requiredRegControls;
        }

        private int SaveScenarioToDB(Scenario scenario)
        {
            if (scenario.ID <= 0)
            {
                scenario.ID = Configuration.ServiceAdapters.KeySA.Instance.GetKey("RegScenarioID");
            }

            string savedBy = scenario.UpdatedBy ?? scenario.CreatedBy;
            if (string.IsNullOrEmpty(savedBy))
                savedBy = "";

            var command = new Command("mnSystem", "up_RegScenario_Save", 0);

            command.AddParameter("@RegScenarioId", SqlDbType.Int, ParameterDirection.Input, scenario.ID);
            command.AddParameter("@SiteId", SqlDbType.Int, ParameterDirection.Input, scenario.SiteID);
            command.AddParameter("@SplashTemplateId", SqlDbType.Int, ParameterDirection.Input, scenario.SplashTemplate.ID);
            command.AddParameter("@RegTemplateId", SqlDbType.Int, ParameterDirection.Input, scenario.RegistrationTemplate.ID);
            command.AddParameter("@ConfirmationTemplateId", SqlDbType.Int, ParameterDirection.Input, scenario.ConfirmationTemplate.ID);
            command.AddParameter("@Name", SqlDbType.NVarChar, ParameterDirection.Input, scenario.Name);
            command.AddParameter("@IsOnePageReg", SqlDbType.Bit, ParameterDirection.Input, scenario.IsOnePageReg);
            command.AddParameter("@DeviceTypeId", SqlDbType.Int, ParameterDirection.Input, (int)scenario.DeviceType);
            command.AddParameter("@ExternalSessionTypeId", SqlDbType.Int, ParameterDirection.Input, (int)scenario.ExternalSessionType);
            command.AddParameter("@Description", SqlDbType.NVarChar, ParameterDirection.Input, scenario.Description);
            command.AddParameter("@SavedBy ", SqlDbType.NVarChar, ParameterDirection.Input, savedBy);
            command.AddParameter("@Active ", SqlDbType.Bit, ParameterDirection.Input, scenario.Active);
            command.AddParameter("@Weight ", SqlDbType.Decimal, ParameterDirection.Input, scenario.Weight);
            Client.Instance.ExecuteAsyncWrite(command);

            return scenario.ID;
        }
        
        private void RemoveRegStep(int scenarioId, int stepId)
        {
            var command = new Command("mnSystem", "up_RegStep_Remove", 0);

            command.AddParameter("@RegScenarioID", SqlDbType.Int, ParameterDirection.Input, scenarioId);
            command.AddParameter("@RegStepID", SqlDbType.Int, ParameterDirection.Input, stepId);
            
            Client.Instance.ExecuteAsyncWrite(command);
        }

        private void RemoveRegStepControl(int scenarioId, int stepId, int regControlSiteId)
        {
            var command = new Command("mnSystem", "up_RegStepControl_Remove", 0);

            command.AddParameter("@RegScenarioID", SqlDbType.Int, ParameterDirection.Input, scenarioId);
            command.AddParameter("@RegStepID", SqlDbType.Int, ParameterDirection.Input, stepId);
            command.AddParameter("@RegControlSiteID", SqlDbType.Int, ParameterDirection.Input, regControlSiteId);

            Client.Instance.ExecuteAsyncWrite(command);
        }

        private void SaveRegStepControl(int stepId, int regControlSiteId, int controlOrder, string defaultValue)
        {
            var command = new Command("mnSystem", "up_RegStepControl_Save", 0);

            command.AddParameter("@RegStepID", SqlDbType.Int, ParameterDirection.Input, stepId);
            command.AddParameter("@RegControlSiteID", SqlDbType.Int, ParameterDirection.Input, regControlSiteId);
            command.AddParameter("@ControlOrder", SqlDbType.Int, ParameterDirection.Input, controlOrder);

            if (string.IsNullOrEmpty(defaultValue))
            {
                command.AddParameter("@ControlDefaultValue", SqlDbType.Int, ParameterDirection.Input, DBNull.Value);
            }
            else
            {
                command.AddParameter("@ControlDefaultValue", SqlDbType.Int, ParameterDirection.Input, defaultValue);
            }

            Client.Instance.ExecuteAsyncWrite(command);
        }

        public int SaveRegStep(Step step, int scenarioId)
        {
            if(step.StepId <=0)
            {
                step.StepId = Configuration.ServiceAdapters.KeySA.Instance.GetKey("RegStepID");
            }

            var command = new Command("mnSystem", "up_RegStep_Save", 0);
            command.AddParameter("@RegStepID", SqlDbType.Int, ParameterDirection.Input, step.StepId);
            command.AddParameter("@RegScenarioID", SqlDbType.Int, ParameterDirection.Input, scenarioId);
            command.AddParameter("@StepOrder", SqlDbType.Int, ParameterDirection.Input, step.Order);
            
            if(!string.IsNullOrEmpty(step.TipText))
            {
                command.AddParameter("@TipText", SqlDbType.NVarChar, ParameterDirection.Input, step.TipText);
            }
            if (!string.IsNullOrEmpty(step.HeaderText))
            {
                command.AddParameter("@HeaderText", SqlDbType.NVarChar, ParameterDirection.Input, step.HeaderText);
            }
            if (!string.IsNullOrEmpty(step.TitleText))
            {
                command.AddParameter("@TitleText", SqlDbType.NVarChar, ParameterDirection.Input, step.TitleText);
            }
            if (!string.IsNullOrEmpty(step.CSSClass))
            {
                command.AddParameter("@CssClass", SqlDbType.VarChar, ParameterDirection.Input, step.CSSClass);
            }

            Client.Instance.ExecuteAsyncWrite(command);
            return step.StepId;
        }

        public void SaveRegControlScenarioOverride(RegControlScenarioOverride regControlegControlScenarioOverride, int regControlSiteID)
        {
            var command = new Command("mnSystem", "up_RegControlSiteScenarioOverride_Save", 0);
            command.AddParameter("@RegControlSiteID", SqlDbType.Int, ParameterDirection.Input, regControlSiteID);
            command.AddParameter("@RegScenarioID", SqlDbType.Int, ParameterDirection.Input, regControlegControlScenarioOverride.RegScenarioID);
            command.AddParameter("@RegControlDisplayType", SqlDbType.Int, ParameterDirection.Input, (int)regControlegControlScenarioOverride.DisplayType);
            command.AddParameter("@Required", SqlDbType.Bit, ParameterDirection.Input, regControlegControlScenarioOverride.Required);
            command.AddParameter("@EnableAutoAdvance", SqlDbType.Bit, ParameterDirection.Input, regControlegControlScenarioOverride.EnableAutoAdvance);

            if (!string.IsNullOrEmpty(regControlegControlScenarioOverride.RequiredErrorMessage))
            {
                command.AddParameter("@RequiredErrorMessage", SqlDbType.NVarChar, ParameterDirection.Input, regControlegControlScenarioOverride.RequiredErrorMessage);
            }

            if (!string.IsNullOrEmpty(regControlegControlScenarioOverride.AdditionalText))
            {
                command.AddParameter("@AdditionalText", SqlDbType.NVarChar, ParameterDirection.Input, regControlegControlScenarioOverride.AdditionalText);
            }

            if (!string.IsNullOrEmpty(regControlegControlScenarioOverride.Label))
            {
                command.AddParameter("@Label", SqlDbType.NVarChar, ParameterDirection.Input, regControlegControlScenarioOverride.Label);
            }
            
            Client.Instance.ExecuteAsyncWrite(command);
        }

        public void SaveScenario(Scenario scenario)
        {
            try
            {
                var existingScenario = (from s in GetAllRegScenarios() where s.ID == scenario.ID select s).FirstOrDefault();
                if(existingScenario == null)
                {
                    throw new Exception("Scenario does not exist");
                }

                if(existingScenario.Active)
                {
                    throw new Exception("Active scenarios can not be edited");
                }
                
                //save main scenario properties
                SaveScenarioToDB(scenario);

                //to make things easy, we'll just remove all the old steps and start fresh
                foreach(var step in existingScenario.Steps)
                {
                    RemoveRegStep(scenario.ID, step.StepId);
                }

                //Now save the new steps
                SaveScenarioStepsAndControls(scenario);

            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured while saving registration scenario", ex));
            }
        }


        public int CreateRegScenario(Scenario scenario)
        {
            try
            {
                scenario.Active = false;
                scenario.Weight = 0.0;
                //Insert into RegScenario table
                scenario.ID = SaveScenarioToDB(scenario);
                SaveScenarioStepsAndControls(scenario);
            }
            catch (Exception ex)
            {
                scenario.ID = 0;
                throw (new BLException("BL error occured when creating registration scenario", ex));
            }

            return scenario.ID;
        }

        private void SaveScenarioStepsAndControls(Scenario scenario)
        {
            //get all the congtrols
            var allControls = GetAllRegControls((int)scenario.DeviceType);

            //Insert into RegStep table
            foreach (Step regStep in scenario.Steps)
            {
                //we will always create regStep with a new ID to prevent concurrency issues where the delete of steps might happen after saving of steps
                regStep.StepId = 0;

                var regStepId = SaveRegStep(regStep, scenario.ID);

                foreach (RegControl regControl in regStep.Controls)
                {
                    //Insert into RegStepControls table
                    var command = new Command("mnSystem", "up_RegScenarioStepControl_Insert", 0);
                    command.AddParameter("@RegStepId", SqlDbType.Int, ParameterDirection.Input, regStepId);
                    command.AddParameter("@RegControlSiteId", SqlDbType.Int, ParameterDirection.Input, regControl.RegControlSiteID);
                    command.AddParameter("@ControlOrder", SqlDbType.Int, ParameterDirection.Input, regControl.Order);
                    command.AddParameter("@ControlDefaulValue", SqlDbType.NVarChar, ParameterDirection.Input, regControl.DefaultValue);
                    Client.Instance.ExecuteAsyncWrite(command);

                    var existingControl = (from c in allControls where c.RegControlSiteID == regControl.RegControlSiteID select c).First();
                    if ((regControl.AdditionalText != null && regControl.AdditionalText.Trim() != existingControl.AdditionalText.Trim()) ||
                        (regControl.Label != null && regControl.Label.Trim() != existingControl.Label.Trim()) ||
                        (regControl.RequiredErrorMessage != null && regControl.RequiredErrorMessage.Trim() != existingControl.RequiredErrorMessage.Trim()) ||
                        (regControl.Required != existingControl.Required) ||
                        (regControl.EnableAutoAdvance != existingControl.EnableAutoAdvance) ||
                        (regControl.DisplayType != existingControl.DisplayType))
                    {
                        var regControlScenarioOverride = new RegControlScenarioOverride
                        {
                            AdditionalText = regControl.AdditionalText,
                            Label = regControl.Label,
                            RequiredErrorMessage = regControl.RequiredErrorMessage,
                            Required = regControl.Required,
                            EnableAutoAdvance = regControl.EnableAutoAdvance,
                            DisplayType = regControl.DisplayType,
                            RegScenarioID = scenario.ID
                        };
                        SaveRegControlScenarioOverride(regControlScenarioOverride, regControl.RegControlSiteID);
                    }

                }
            }
        }

        private List<Scenario> DataTableToScenarios(DataSet ds)
        {
            List<Scenario> scenarios = null;
            if (ds.Tables[0].Rows.Count > 0)
            {
                scenarios = new List<Scenario>();

                DataTable scenarioTable = ds.Tables[0];
                DataTable conditionsTable = ds.Tables[1];
                DataTable stepsTable = ds.Tables[2];
                DataTable controlsStepsTable = ds.Tables[3];
                DataTable siteControls = ds.Tables[4];
                DataTable controlsCustomOptionsTable = ds.Tables[5];
                DataTable validationsTable = ds.Tables[6];
                DataTable controlDeviceOverrides = ds.Tables[7];
                DataTable validationDeviceOverrides = ds.Tables[8];
                DataTable controlScenarioOverridesTable = ds.Tables[9];

                scenarioTable.ChildRelations.Add(
                    "fk_Scenario_Conditions",
                    new DataColumn[] { scenarioTable.Columns["RegScenarioID"] },
                    new DataColumn[] { conditionsTable.Columns["RegScenarioID"] }
                    );

                scenarioTable.ChildRelations.Add(
                    "fk_Scenario_Steps",
                    new DataColumn[] { scenarioTable.Columns["RegScenarioID"] },
                    new DataColumn[] { stepsTable.Columns["RegScenarioID"] }
                    );

                stepsTable.ChildRelations.Add(
                    "fk_Steps_Controls",
                    new DataColumn[] { stepsTable.Columns["RegStepID"] },
                    new DataColumn[] { controlsStepsTable.Columns["RegStepID"] }
                    );

                siteControls.ChildRelations.Add(
                    "fk_Controls_CustomOptions",
                    new DataColumn[] { siteControls.Columns["RegControlSiteID"] },
                    new DataColumn[] { controlsCustomOptionsTable.Columns["RegControlSiteID"] }
                );

                siteControls.ChildRelations.Add(
                    "fk_Controls_Validations",
                    new DataColumn[] { siteControls.Columns["RegControlSiteID"] },
                    new DataColumn[] { validationsTable.Columns["RegControlSiteID"] }
                    );

                validationsTable.ChildRelations.Add(
                    "fk_Validations_DeviceOverrides",
                    new DataColumn[] { validationsTable.Columns["RegControlSiteID"], validationsTable.Columns["RegControlValidationTypeID"] },
                    new DataColumn[] { validationDeviceOverrides.Columns["RegControlSiteID"], validationDeviceOverrides.Columns["RegControlValidationTypeID"] }
                    );

                siteControls.ChildRelations.Add(
                    "fk_Controls_DeviceOverrides",
                    new DataColumn[] { siteControls.Columns["RegControlSiteID"] },
                    new DataColumn[] { controlDeviceOverrides.Columns["RegControlSiteID"] }
                    );

                siteControls.ChildRelations.Add(
                  "fk_Controls_ScenarioOverrides",
                  new DataColumn[] { siteControls.Columns["RegControlSiteID"] },
                  new DataColumn[] { controlScenarioOverridesTable.Columns["RegControlSiteID"] }
                  );

                foreach (DataRow scenarioRow in scenarioTable.Rows)
                {
                    Scenario scenario = DataRowToScenario(scenarioRow);

                    DataRow[] stepRows = scenarioRow.GetChildRows("fk_Scenario_Steps");

                    List<Step> steps = new List<Step>();

                    foreach (DataRow stepRow in stepRows)
                    {
                        Step step = DataRowToStep(stepRow);

                        DataRow[] stepControls = (from dr in stepRow.GetChildRows("fk_Steps_Controls")
                                                  orderby Convert.ToInt32(dr["ControlOrder"])
                                                  select dr).ToArray();

                        List<RegControl> controls = new List<RegControl>();

                        foreach (DataRow stepControlRow in stepControls)
                        {
                            try
                            {
                                List<ControlValidation> validations = null;
                                List<ControlCustomOption> customOptions = null;
                                List<RegControlDeviceOverride> controlOverrides = null;
                                List<RegControlScenarioOverride> controlScenarioOverrides = null;

                                DataRow controlRow = (from DataRow r in siteControls.Rows
                                                      where
                                                          Convert.ToInt32(r["RegControlSiteID"]) ==
                                                          Convert.ToInt32(stepControlRow["RegControlSiteID"])
                                                      select r).First();

                                DataRow[] controlDeviceOverrideRows = controlRow.GetChildRows("fk_Controls_DeviceOverrides");

                                if (controlDeviceOverrideRows != null && controlDeviceOverrideRows.Length > 0)
                                {
                                    controlOverrides = new List<RegControlDeviceOverride>();
                                    foreach (DataRow controlDeviceOverrideRow in controlDeviceOverrideRows)
                                    {
                                        RegControlDeviceOverride controlOverride =
                                            DataRowToRegControlDeviceOverride(controlDeviceOverrideRow);
                                        controlOverrides.Add(controlOverride);
                                    }
                                }

                                DataRow[] validationRows = controlRow.GetChildRows("fk_Controls_Validations");
                                if (validationRows != null && validationRows.Length > 0)
                                {
                                    validations = new List<ControlValidation>();
                                    foreach (DataRow validationRow in validationRows)
                                    {
                                        List<ControlValidationDeviceOverride> validationOverrides = null;
                                        ControlValidation validation = DataRowToValidation(validationRow);

                                        DataRow[] validationDeviceOverrideRows = validationRow.GetChildRows("fk_Validations_DeviceOverrides");

                                        if (validationDeviceOverrideRows != null && validationDeviceOverrideRows.Length > 0)
                                        {
                                            validationOverrides = new List<ControlValidationDeviceOverride>();

                                            foreach (DataRow validationDeviceOverrideRow in validationDeviceOverrideRows)
                                            {
                                                ControlValidationDeviceOverride validationDeviceOverride =
                                                    DataRowToControlValidationDeviceOverride(validationDeviceOverrideRow);
                                                validationOverrides.Add(validationDeviceOverride);
                                            }
                                        }

                                        validation.DeviceOverrides = validationOverrides;
                                        validations.Add(validation);
                                    }
                                }

                                DataRow[] customOptionRows = controlRow.GetChildRows("fk_Controls_CustomOptions");
                                if (customOptionRows != null && customOptionRows.Length > 0)
                                {
                                    customOptions = new List<ControlCustomOption>();
                                    foreach (DataRow customOptionRow in customOptionRows)
                                    {
                                        ControlCustomOption customOption = DataRowToCustomOption(customOptionRow);
                                        customOptions.Add(customOption);
                                    }
                                }

                                DataRow[] controlScenarioOverrideRows = controlRow.GetChildRows("fk_Controls_ScenarioOverrides");

                                if (controlScenarioOverrideRows != null && controlScenarioOverrideRows.Length > 0)
                                {
                                    controlScenarioOverrides = new List<RegControlScenarioOverride>();
                                    foreach (DataRow controlScenarioOverrideRow in controlScenarioOverrideRows)
                                    {
                                        RegControlScenarioOverride controlScenarioOverride =
                                            DataRowToRegControlScenarioOverride(controlScenarioOverrideRow);
                                        controlScenarioOverrides.Add(controlScenarioOverride);
                                    }
                                }

                                RegControl control = DataRowToRegControl(controlRow);
                                control.Validations = validations;
                                control.CustomOptions = customOptions;
                                control.DeviceOverrides = controlOverrides;
                                control.ScenarioOverrides = controlScenarioOverrides;
                                //Set control's default value
                                control.DefaultValue = !Convert.IsDBNull(stepControlRow["ControlDefaultValue"]) ? stepControlRow["ControlDefaultValue"].ToString() : string.Empty;

                                controls.Add(control);
                            }
                            catch (Exception ex)
                            {
                                new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
                                                         string.Format("Error loading step control. Scenario: {0}, StepID: {1}", scenario.ID, step.StepId),
                                                         ex);
                            }
                        }

                        step.Controls = controls;
                        steps.Add(step);
                    }
                    scenario.Steps = steps;
                    scenarios.Add(scenario);
                }
            }
            return scenarios;
        }

        public Dictionary<int, List<Template>> GetTemplatesBySite()
        {
            var templatesBySite = new Dictionary<int, List<Template>>();

            try
            {
                var command = new Command("mnSystem", "up_RegTemplates_List", 0);
                DataSet ds = Client.Instance.ExecuteDataSet(command);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        int siteId;
                        var template = DataRowToTemplate(row, out siteId);
                        if(!templatesBySite.ContainsKey(siteId))
                        {
                            templatesBySite.Add(siteId, new List<Template>());
                        }
                        templatesBySite[siteId].Add(template);
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when retrieving registration templates", ex));
            }

            return templatesBySite;
        }

        public TemplateSaveResult CreateTemplate(Template regTemplate)
        {
            TemplateSaveResult templateSaveResult = new TemplateSaveResult();
            templateSaveResult.ResultType=TemplateResultType.ExceptionFailure;
            int regScenarioTemplateId = 0;
            int returnValue = 0;
            try
            {
                var checkCommand = new Command("mnSystem", "up_RegScenarioTemplate_Exists_Check", 0);
                checkCommand.AddParameter("@RegScenarioTemplateTypeID", SqlDbType.Int, ParameterDirection.Input, (int)regTemplate.Type);
                checkCommand.AddParameter("@SiteId", SqlDbType.Int, ParameterDirection.Input, regTemplate.SiteID);
                checkCommand.AddParameter("@Name", SqlDbType.NVarChar, ParameterDirection.Input, regTemplate.Name);
                checkCommand.AddParameter("@ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue, returnValue);
                SqlParameterCollection sqlParameterCollection = Client.Instance.ExecuteNonQuery(checkCommand);

                foreach (SqlParameter sqlParam in sqlParameterCollection)
                {
                    if (sqlParam.ParameterName == "@ReturnValue")
                    {
                        returnValue = (int)sqlParam.Value;
                        break;
                    }
                }

                if (returnValue > 0)
                {
                    templateSaveResult.ResultType = TemplateResultType.TemplateAlreadyExistsForTypeAndSite;
                }
                else
                {
                    regScenarioTemplateId = Matchnet.Configuration.ServiceAdapters.KeySA.Instance.GetKey("RegScenarioTemplateID");

                    //Insert into RegScenario table
                    var command = new Command("mnSystem", "up_RegScenarioTemplate_Insert", 0);
                    command.AddParameter("@RegScenarioTemplateID", SqlDbType.Int, ParameterDirection.Input, regScenarioTemplateId);
                    command.AddParameter("@RegScenarioTemplateTypeID", SqlDbType.Int, ParameterDirection.Input, (int) regTemplate.Type);
                    command.AddParameter("@SiteId", SqlDbType.Int, ParameterDirection.Input, regTemplate.SiteID); 
                    command.AddParameter("@Name", SqlDbType.NVarChar, ParameterDirection.Input, regTemplate.Name);
                    Client.Instance.ExecuteAsyncWrite(command);

                    templateSaveResult.ResultType = TemplateResultType.Success;
                    regTemplate.ID = regScenarioTemplateId;
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when creating registration templates", ex));                
            }
            templateSaveResult.RegTemplate = regTemplate;
            return templateSaveResult;
        }

        public TemplateSaveResult RemoveTemplate(int regTemplateId, int siteId)
        {
            TemplateSaveResult templateSaveResult = new TemplateSaveResult();
            templateSaveResult.ResultType=TemplateResultType.ExceptionFailure;
            int returnValue = 0;
            try
            {
                var checkCommand = new Command("mnSystem", "up_RegScenarioTemplate_InUse_Check", 0);
                checkCommand.AddParameter("@RegScenarioTemplateID", SqlDbType.Int, ParameterDirection.Input, regTemplateId);
                checkCommand.AddParameter("@SiteId", SqlDbType.Int, ParameterDirection.Input, siteId);
                checkCommand.AddParameter("@ReturnValue", SqlDbType.Int, ParameterDirection.ReturnValue, returnValue);
                SqlParameterCollection sqlParameterCollection = Client.Instance.ExecuteNonQuery(checkCommand);

                foreach (SqlParameter sqlParam in sqlParameterCollection)
                {
                    if (sqlParam.ParameterName == "@ReturnValue")
                    {
                        returnValue = (int)sqlParam.Value;
                        break;
                    }
                }

                if (returnValue > 0)
                {
                    templateSaveResult.ResultType = TemplateResultType.TemplateInUseByRegScenario;
                }
                else
                {
                    //Insert into RegScenario table
                    var command = new Command("mnSystem", "up_RegScenarioTemplate_Remove", 0);
                    command.AddParameter("@RegScenarioTemplateID", SqlDbType.Int, ParameterDirection.Input, regTemplateId);
                    command.AddParameter("@SiteId", SqlDbType.Int, ParameterDirection.Input, siteId);
                    Client.Instance.ExecuteAsyncWrite(command);

                    templateSaveResult.ResultType = TemplateResultType.Success;
                    templateSaveResult.RegTemplate = new Template(siteId, TemplateType.Splash, string.Empty, regTemplateId);
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when removing registration template", ex));
            }
            return templateSaveResult;
        }

        private ControlValidation DataRowToValidation(DataRow dataRow)
        {
            ValidationType type = (ValidationType) Convert.ToInt32(dataRow["RegControlValidationTypeID"].ToString());
            string value = dataRow["Value"].ToString();
            string errorMessage = dataRow["ErrorMessage"].ToString();
            return new ControlValidation(type, value, errorMessage);
        }

        private ControlCustomOption DataRowToCustomOption(DataRow dataRow)
        {
            int value = Convert.ToInt32(dataRow["Value"].ToString());
            int listOrder = Convert.ToInt32(dataRow["ListOrder"].ToString());
            string description = dataRow["Description"].ToString();

            return new ControlCustomOption(value, listOrder, description);
        }

        private RegControl DataRowToRegControl(DataRow dataRow)
        {
            //columns from RegControl table
            int attributeID = !Convert.IsDBNull(dataRow["AttributeID"]) ? Convert.ToInt32(dataRow["AttributeID"].ToString()) : Constants.NULL_INT;
            string name = dataRow["Name"].ToString();
            bool hasOptions = !Convert.IsDBNull(dataRow["HasOptions"]) ? Convert.ToBoolean(dataRow["HasOptions"].ToString()) : false;
            int regControlId = dataRow.Table.Columns.Contains("RegControlID") && dataRow["RegControlID"] != DBNull.Value ? Convert.ToInt32(dataRow["RegControlID"]) : 0;
            int siteId = dataRow.Table.Columns.Contains("SiteID") && dataRow["SiteID"] != DBNull.Value ? Convert.ToInt32(dataRow["SiteID"]) : 0;

            //columns from stored procs
            ControlDisplayType displayType = ControlDisplayType.None;
            if(dataRow.Table.Columns.Contains("RegControlDisplayTypeID")) displayType = (ControlDisplayType)Convert.ToInt32(dataRow["RegControlDisplayTypeID"].ToString());
            bool required=false;
            if (dataRow.Table.Columns.Contains("Required")) required = Convert.ToBoolean(dataRow["Required"]);
            string label=string.Empty;
            if (dataRow.Table.Columns.Contains("Label")) label = !Convert.IsDBNull(dataRow["Label"]) ? dataRow["Label"].ToString() : string.Empty;
            string requiredErrorMessage=string.Empty;
            if (dataRow.Table.Columns.Contains("RequiredErrorMessage")) requiredErrorMessage = !Convert.IsDBNull(dataRow["RequiredErrorMessage"]) ? dataRow["RequiredErrorMessage"].ToString() : string.Empty;
            string additionalText=string.Empty;
            if (dataRow.Table.Columns.Contains("AdditionalText")) additionalText = !Convert.IsDBNull(dataRow["AdditionalText"]) ? dataRow["AdditionalText"].ToString() : string.Empty;
            bool enableAutoAdvance=false;
            if (dataRow.Table.Columns.Contains("EnableAutoAdvance")) enableAutoAdvance = Convert.ToBoolean(dataRow["EnableAutoAdvance"]);
            bool allowZeroValue=false;
            if (dataRow.Table.Columns.Contains("AllowZeroValue")) allowZeroValue = !Convert.IsDBNull(dataRow["AllowZeroValue"]) ? Convert.ToBoolean(dataRow["AllowZeroValue"].ToString()) : false;
            int regControlSiteId=0;
            if (dataRow.Table.Columns.Contains("RegControlSiteID")) regControlSiteId = dataRow.Table.Columns.Contains("RegControlSiteID") && dataRow["RegControlSiteID"] != DBNull.Value ? Convert.ToInt32(dataRow["RegControlSiteID"]) : 0;
            
            if(attributeID == Constants.NULL_INT)
            {
                return new RegControl(name, displayType, hasOptions, required, requiredErrorMessage, label, additionalText, enableAutoAdvance, allowZeroValue,
                    regControlSiteId, regControlId, siteId, null);
            }
            else
            {
                return new RegControl(attributeID, name, displayType, hasOptions, required, requiredErrorMessage, label, additionalText, enableAutoAdvance, 
                    allowZeroValue, regControlSiteId, regControlId, siteId, null);
            }
        }

        private Step DataRowToStep(DataRow dataRow)
        {
            string cssClass = dataRow["CssClass"].ToString();
            string tipText = dataRow["TipText"].ToString();
            int order = Convert.ToInt32(dataRow["StepOrder"].ToString());
            string headerText = dataRow["HeaderText"].ToString();
            string titleText = dataRow["TitleText"].ToString();
            int stepId = Convert.ToInt32(dataRow["RegStepID"].ToString());
            return new Step(stepId, cssClass, tipText, order, headerText, titleText, null);
        }

        private Scenario DataRowToScenario(DataRow dataRow)
        {
            int scenarioID = Convert.ToInt32(dataRow["RegScenarioID"].ToString());
            int siteID = Convert.ToInt32(dataRow["SiteID"].ToString());
            string name = dataRow["Name"].ToString();
            bool isOnePageReg = Convert.ToBoolean(dataRow["IsOnePageReg"]);
            int splashTemplateId = Convert.ToInt32(dataRow["SplashTemplateID"].ToString());
            string splashTemplateName = dataRow["SplashTemplateName"].ToString();
            int regTemplateId = Convert.ToInt32(dataRow["RegTemplateID"].ToString());
            string regTemplateName = dataRow["RegTemplateName"].ToString();
            int confirmationTemplateId = Convert.ToInt32(dataRow["ConfirmationTemplateID"].ToString());
            string confirmationTemplateName = dataRow["ConfirmationTemplateName"].ToString();
            double weight = Convert.ToDouble(dataRow["Weight"].ToString());
            bool isControl = Convert.ToBoolean(dataRow["IsControl"]);
            DeviceType deviceType = (DeviceType)Convert.ToInt32(dataRow["DeviceTypeID"].ToString());
            ExternalSessionType sessionType = (ExternalSessionType)Convert.ToInt32(dataRow["ExternalSessionTypeID"].ToString());
            bool active = dataRow.Table.Columns.Contains("Active") && dataRow["Active"] != DBNull.Value && Convert.ToBoolean(dataRow["Active"]);
            string description = dataRow.Table.Columns.Contains("Description") && dataRow["Description"] != DBNull.Value ? dataRow["Description"].ToString() : "";
            string createdBy = dataRow.Table.Columns.Contains("CreatedBy") && dataRow["CreatedBy"] != DBNull.Value ? dataRow["CreatedBy"].ToString() : "";
            string updatedBy = dataRow.Table.Columns.Contains("UpdatedBy") && dataRow["UpdatedBy"] != DBNull.Value ? dataRow["UpdatedBy"].ToString() : "";
            DateTime? insertDate = dataRow.Table.Columns.Contains("InsertDate") && dataRow["InsertDate"] != DBNull.Value ? Convert.ToDateTime(dataRow["InsertDate"]) : (DateTime?)null;
            DateTime? updateDate = dataRow.Table.Columns.Contains("UpdateDate") && dataRow["UpdateDate"] != DBNull.Value ? Convert.ToDateTime(dataRow["UpdateDate"]) : (DateTime?)null;

            return new Scenario(scenarioID, siteID, name, isOnePageReg, 
                new Template(siteID, TemplateType.Splash,splashTemplateName, splashTemplateId),
                new Template(siteID, TemplateType.Registration, regTemplateName, regTemplateId),
                new Template(siteID, TemplateType.Confirmation, confirmationTemplateName, confirmationTemplateId),
                weight, isControl, deviceType, sessionType, description, createdBy, updatedBy, insertDate, updateDate, active, null);
        }


        private Template DataRowToTemplate(DataRow dataRow, out int siteId)
        {
            //RegScenariotemplateid, RegScenarioTempateTypeid, Siteid, Name
            var templateType = (TemplateType)Convert.ToInt32(dataRow["RegScenarioTempateTypeid"].ToString());
            var templateId = Convert.ToInt32(dataRow["RegScenariotemplateid"].ToString());
            var name = dataRow["Name"].ToString();
            siteId = Convert.ToInt32(dataRow["siteId"].ToString());

            return new Template(siteId, templateType, name, templateId);
        }

        private RegControlDeviceOverride DataRowToRegControlDeviceOverride(DataRow dataRow)
        {
            DeviceType deviceType = (DeviceType)Convert.ToInt32(dataRow["DeviceTypeID"].ToString());
            string label = !Convert.IsDBNull(dataRow["Label"]) ? dataRow["Label"].ToString() : string.Empty;
            string requiredErrorMessage = !Convert.IsDBNull(dataRow["RequiredErrorMessage"]) ? dataRow["RequiredErrorMessage"].ToString() : string.Empty;
            string additionalText = !Convert.IsDBNull(dataRow["AdditionalText"]) ? dataRow["AdditionalText"].ToString() : string.Empty;

            return new RegControlDeviceOverride
                       {
                           DeviceType = deviceType,
                           AdditionalText = additionalText,
                           Label = label,
                           RequiredErrorMessage = requiredErrorMessage
                       };
        }

        private RegControlScenarioOverride DataRowToRegControlScenarioOverride(DataRow dataRow)
        {
            int regScenarioId = Convert.ToInt32(dataRow["RegScenarioId"].ToString());
            var displayType = (ControlDisplayType)Convert.ToInt32(dataRow["RegControlDisplayType"].ToString());
            bool required = Convert.ToBoolean(dataRow["Required"]);
            bool enableAutoAdvance = Convert.ToBoolean(dataRow["EnableAutoAdvance"]);
            string label = !Convert.IsDBNull(dataRow["Label"]) ? dataRow["Label"].ToString() : string.Empty;
            string requiredErrorMessage = !Convert.IsDBNull(dataRow["RequiredErrorMessage"]) ? dataRow["RequiredErrorMessage"].ToString() : string.Empty;
            string additionalText = !Convert.IsDBNull(dataRow["AdditionalText"]) ? dataRow["AdditionalText"].ToString() : string.Empty;

            return new RegControlScenarioOverride
            {
                RegScenarioID = regScenarioId,
                DisplayType = displayType,
                Required = required,
                EnableAutoAdvance = enableAutoAdvance,
                AdditionalText = additionalText,
                Label = label,
                RequiredErrorMessage = requiredErrorMessage
            };
        }

        private ControlValidationDeviceOverride DataRowToControlValidationDeviceOverride(DataRow dataRow)
        {
            DeviceType deviceType = (DeviceType)Convert.ToInt32(dataRow["DeviceTypeID"].ToString());
            string errorMessage = !Convert.IsDBNull(dataRow["ErrorMessage"]) ? dataRow["ErrorMessage"].ToString() : string.Empty;

            return new ControlValidationDeviceOverride {DeviceType = deviceType, ErrorMessage = errorMessage};
        }


    }
}
