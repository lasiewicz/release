using System;
using System.Data;
using System.Diagnostics;
using System.Threading;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Configuration;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Data;
using Matchnet.Data.Exceptions;
using Matchnet.Exceptions;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;


namespace Matchnet.Content.BusinessLogic
{
	public class AttributeMetadataBL
	{
		public readonly static AttributeMetadataBL Instance = new AttributeMetadataBL();

		private Matchnet.Caching.Cache _cache;

		private AttributeMetadataBL()
		{
			_cache = Matchnet.Caching.Cache.Instance;
		}


		public Attributes GetAttributes()
		{
			try
			{
				Attributes attributes = _cache.Get(Attributes.CACHE_KEY) as Attributes;

				if (attributes == null)
				{
					Command command = new Command("mnSystem", "up_AttributeMetadata_List", 0);
					DataSet ds = Client.Instance.ExecuteDataSet(command);
					DataTable dtGroup = ds.Tables[0];
					DataTable dtAttributeGroup = ds.Tables[1];
					DataTable dtAttribute = ds.Tables[2];

					attributes = new Attributes();

					foreach (DataRow row in dtGroup.Rows)
					{
						attributes.AddGroupScope(Convert.ToInt32(row["GroupID"]), 
							(ScopeType)Enum.Parse(typeof(ScopeType), row["ScopeID"].ToString()));
					}

					foreach (DataRow row in dtAttributeGroup.Rows)
					{
						Int16 length = 0;
						string defaultValue = Constants.NULL_STRING;

						if (row["Length"] != DBNull.Value)
						{
							length = Convert.ToInt16(row["Length"]);
						}

						if (row["DefaultValue"] != DBNull.Value)
						{
							defaultValue = row["DefaultValue"].ToString();
						}

						attributes.AddAttributeGroup(Convert.ToInt32(row["AttributeGroupID"]),
							Convert.ToInt32(row["GroupID"]),
							Convert.ToInt32(row["AttributeID"]),
							(StatusType)Enum.Parse(typeof(StatusType), row["AttributeStatusMask"].ToString()),
							length,
							Convert.ToByte(row["EncryptFlag"]) == 1 ? true : false,
							defaultValue,
							row["AttributeGroupIDOldValueContainer"] is DBNull ? Constants.NULL_INT : Convert.ToInt32(row["AttributeGroupIDOldValueContainer"]) );
					}

					foreach (DataRow row in dtAttribute.Rows)
					{
						attributes.AddAttribute(Convert.ToInt32(row["AttributeID"]),
							(ScopeType)Enum.Parse(typeof(ScopeType), row["ScopeID"].ToString()),
							row["AttributeName"].ToString(),
							(DataType)Enum.Parse(typeof(DataType), row["DataType"].ToString(), true),
							row["AttributeIDOldValueContainer"] is DBNull ? Constants.NULL_INT : Convert.ToInt32(row["AttributeIDOldValueContainer"]));
					}

					_cache.Add(attributes);
				}

				return attributes;
			}
			catch(Exception ex)
			{
				throw(new BLException("Error occurred while retrieving attribute metadata.", ex));
			}
		}


		public AttributeCollections GetAttributeCollections()
		{
			try
			{
				AttributeCollections attributeCollections = _cache.Get(AttributeCollections.CACHE_KEY) as AttributeCollections;

				if (attributeCollections == null)
				{
					Command command = new Command("mnSystem", "up_AttributeCollectionAttribute_List", 0);
					DataSet ds = Client.Instance.ExecuteDataSet(command);
					DataTable dtCollection = ds.Tables[0];
					DataTable dtAttribute = ds.Tables[1];

					attributeCollections = new AttributeCollections();

					foreach (DataRow row in dtCollection.Rows)
					{
						attributeCollections.AddCollection(row["Constant"].ToString(),
							Convert.ToInt32(row["AttributeCollectionID"]));
					}

					foreach (DataRow row in dtAttribute.Rows)
					{
						attributeCollections.AddCollectionAttribute(Convert.ToInt32(row["AttributeCollectionID"]),
							Convert.ToInt32(row["GroupID"]),
							Convert.ToInt32(row["AttributeID"]),
							(Convert.ToInt32(row["OptionalFlag"]) == 1) ? true : false);
					}

					_cache.Add(attributeCollections);
				}

				return attributeCollections;
			}
			catch(Exception ex)
			{
				throw(new BLException("Error occurred while retrieving attribute collections.", ex));
			}
		}


        public SearchStoreAttributes GetSearchStoreAttributes()
        {
            System.Data.SqlClient.SqlDataReader rs = null;

            try
            {

                SearchStoreAttributes attributes =(SearchStoreAttributes)_cache.Get(SearchStoreAttributes.CACHE_KEY);
               
                if (attributes == null)
                {
                    int cacheexprtime = SearchStoreAttributes.CACHE_TTL;
                    try
                    {

                        Command command = new Command("mnSystem", "up_SearchStoreAttributes_List", 0);
                         rs = Client.Instance.ExecuteReader(command);
                         attributes = new SearchStoreAttributes();
                        while (rs.Read())
                        {
                            int ordinal = rs.GetOrdinal("SearchStoreAttributeName");
                            string attrname = rs.GetString(ordinal);
                            attributes.Add(attrname);

                        }


                        string cachettl = Configuration.ServiceAdapters.RuntimeSettings.GetSetting("SEARCHSTOREATTRIBUTES_CACHE_TTL");
                        cacheexprtime = Conversion.CInt(cachettl);

                        attributes.CacheTTLSeconds = cacheexprtime;
                        rs.Dispose();
                    }
                    catch (Exception ex1)
                    {
                        BLException nosettingex = new BLException("GetSearchStoreAttributes - no cache ttl setting", ex1);
                    }
                   

                    _cache.Insert(attributes);
                }

                return attributes;

            }
            catch (Exception ex)
            {
                throw new BLException("Error occurred while retrieving  search store attributes.", ex);
            }
            finally
            {
                if (rs != null)
                { rs.Dispose(); }
            }

        }
	}
}
