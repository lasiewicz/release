using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
//using System.Web.Caching;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Data;
using Matchnet.Data.Exceptions;
using Matchnet.Content.ValueObjects.Events;
using Matchnet.Exceptions;

namespace Matchnet.Content.BusinessLogic
{
	/// <summary>
	/// Summary description for EventBL.
	/// </summary>
	public class EventBL : MarshalByRefObject
	{
		#region Private Members

		private Matchnet.Caching.Cache _cache;

		private const string EVENT_DATABASE_FILE = "mnSystem";
		private const string GET_EVENT_LIST = "up_Event_List";
		private const string GET_EVENT = "up_Event_ListByID";
		private const int PAGE_SIZE = 20;

		private const string HOLIDAY_DATABASE_FILE="mnContent";
		private const string GET_HOLIDAY_LIST = "up_getHolidays";
		#endregion

		#region Public Events and Event Handlers
		public delegate void EventRequestEventHandler(bool cacheHit);
		public event EventRequestEventHandler EventRequested;

		public delegate void EventAddEventHandler();
		public event EventAddEventHandler EventAdded;

		public delegate void EventRemoveEventHandler();
		public event EventRemoveEventHandler EventRemoved;
		#endregion

		#region Constructors
		public readonly static EventBL Instance = new EventBL();

		private EventBL()
		{
			_cache = Matchnet.Caching.Cache.Instance;
		}
		#endregion

		#region Public Methods
		public EventCollection GetEvents(Int32 comunityID, string orderBy, string sorting, Int32 startRow, Int32 pageSize, string publishFlag, Event.EVENT_TYPE eventType)
		{
			EventCollection events = null;
			bool albumEvent = false;
					
			try
			{
				bool cacheHit = true;
				events = _cache.Get(EventCollection.GetCacheKey(comunityID, eventType, albumEvent)) as EventCollection;

				if (events == null)
				{
					cacheHit = false;
					events = LoadEventCollection(comunityID, orderBy, sorting, startRow, pageSize, publishFlag, eventType, albumEvent);
				}	

				EventRequested(cacheHit);
				return events;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when getting events."+ ex.Message, ex));
			}
		}

		public EventCollection GetEvents(Int32 comunityID, Event.EVENT_TYPE eventType)
		{
			EventCollection events = null;
			bool albumEvent = false;
					
			try
			{
				bool cacheHit = true;
				events = _cache.Get(EventCollection.GetCacheKey(comunityID, eventType, albumEvent)) as EventCollection;

				if (events == null)
				{
					cacheHit = false;
					events = LoadEventCollection(comunityID, "EventDate", "asc", 1, PAGE_SIZE, "PublishedFlag=1", eventType, albumEvent);
				}	

				EventRequested(cacheHit);
				return events;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when getting events."+ ex.Message, ex));
			}
		}


		public EventCollection GetAlbumEvents(Int32 comunityID)
		{
			EventCollection events = null;
			bool albumEvent = true;
			Event.EVENT_TYPE eventType = Event.EVENT_TYPE.None;
			try
			{
				bool cacheHit = true;
				events = _cache.Get(EventCollection.GetCacheKey(comunityID, eventType, albumEvent)) as EventCollection;

				if (events == null)
				{
					cacheHit = false;
					events = LoadEventCollection(comunityID, "EventDate", "desc", 1, PAGE_SIZE, "AlbumPublishedFlag=1", eventType, albumEvent);
				}	
	
				EventRequested(cacheHit);
				return events;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when getting album events."+ ex.Message, ex));
			}
		}
			
		public Event GetEvent(Int32 eventID)
		{
			try
			{
				Event _event = null;
				bool cacheHit = true;

				_event = _cache.Get(Event.GetCacheKey(eventID)) as Event;

				if (_event == null)
				{
					cacheHit = false;

					Command command = new Command(EVENT_DATABASE_FILE, GET_EVENT, eventID);
					command.AddParameter("@eventID", SqlDbType.Int, ParameterDirection.Input, eventID);
					DataTable dt = Client.Instance.ExecuteDataTable(command);

					if (dt.Rows.Count > 0) 
					{
						_event = convertToEventObject(dt.Rows[0], true);
						_event.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EVENTSVC_CACHE_TTL_SVC"));
						_cache.Insert(_event);
						EventAdded();
					}
				}

				EventRequested(cacheHit);
				return _event;

			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when getting event."+ ex.Message, ex));
			}
		}


		public HolidayCollection GetHolidays()
		{
			HolidayCollection holidays = null;
			
					
			try
			{
				bool cacheHit = true;
				holidays = _cache.Get(HolidayCollection.GetCacheKey(DateTime.Now.Year)) as HolidayCollection;

				if (holidays == null)
				{
					cacheHit = false;
					holidays = LoadHolidayCollection();
				}	
				_cache.Insert(holidays);
				
				return holidays;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when getting holidays."+ ex.Message, ex));
			}
		}



		public HolidayCollection GetHolidays(int year)
		{
			HolidayCollection holidays = null;
			
					
			try
			{
				bool cacheHit = true;
				holidays = _cache.Get(HolidayCollection.GetCacheKey(year)) as HolidayCollection;

				if (holidays == null)
				{
					cacheHit = false;
					holidays = LoadHolidayCollection(year);
				}	

				_cache.Insert(holidays);
				return holidays;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when getting holidays."+ ex.Message, ex));
			}
		}


		#endregion

		#region Private Methods
		private EventCollection LoadEventCollection(Int32 comunityID, string orderBy, string sorting, int startRow, int pageSize, string publishFlag, Event.EVENT_TYPE eventType, bool albumEvent)
		{
			try
			{
				EventCollection events = new EventCollection(comunityID, eventType, albumEvent);
				
				Command command = new Command(EVENT_DATABASE_FILE, GET_EVENT_LIST, (int)eventType);

				if (eventType != Event.EVENT_TYPE.None)
				{
					command.AddParameter("@EventTypeID", SqlDbType.Int, ParameterDirection.Input, (int)eventType);
				}

				command.AddParameter("@TotalRows", SqlDbType.Int, ParameterDirection.Output, null);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, comunityID);
				command.AddParameter("@StartRow", SqlDbType.Int, ParameterDirection.Input, startRow);
				command.AddParameter("@PageSize", SqlDbType.Int, ParameterDirection.Input, pageSize);
				command.AddParameter("@OrderBy", SqlDbType.VarChar, ParameterDirection.Input, orderBy);
				command.AddParameter("@Sorting", SqlDbType.VarChar, ParameterDirection.Input, sorting);
				command.AddParameter("@ExtraSQL", SqlDbType.VarChar, ParameterDirection.Input, publishFlag);
				DataTable dt = Client.Instance.ExecuteDataTable(command);

				if (dt.Rows.Count > 0) 
				{
					events = convertToEventCollection(dt);	
					events.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("EVENTSVC_CACHE_TTL_SVC"));
					_cache.Insert(events);
					EventAdded();
				}
				return events;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when getting events."+ ex.Message, ex));
			}		
		}

		private EventCollection convertToEventCollection(System.Data.DataTable dt)
		{
			EventCollection events = new EventCollection();
					
			foreach(DataRow DataRow in dt.Rows)
			{
				// Do not include Event Content field per Proc.
				events.Add(convertToEventObject(DataRow, false));
			}

			return events;
		}

		private Event convertToEventObject(System.Data.DataRow dr, bool withContent)
		{
			Event _event = new Event(int.Parse(dr["eventID"].ToString()));
			_event.EventDateText = dr["EventDateText"].ToString();
			_event.Description = dr["Description"].ToString();
			_event.CommunityID = int.Parse(dr["GroupID"].ToString());
			_event.EventExtraInfo = dr["EventExtraInfo"].ToString();
			_event.EventThumbPath = dr["EventThumbPath"].ToString();
			_event.ListOrderEvent = int.Parse(dr["ListOrderEvent"].ToString());
			_event.Location = dr["Location"].ToString();
			_event.PublishedFlag = Convert.ToBoolean(dr["PublishedFlag"].ToString());
			_event.AlbumExtraInfo = dr["AlbumExtraInfo"].ToString();
			_event.AlbumPublishedFlag = Convert.ToBoolean(dr["AlbumPublishedFlag"]);
			_event.AlbumThumbPath = dr["AlbumThumbPath"].ToString();

			if (dr["ListOrderAlbum"] !=null && dr["ListOrderAlbum"] != DBNull.Value)
			{
				_event.ListOrderAlbum = int.Parse(dr["ListOrderAlbum"].ToString());
			}
			if(dr["EventDate"] != null && dr["EventDate"] != DBNull.Value) 
			{
				_event.EventDate = DateTime.Parse(dr["EventDate"].ToString());
			}
			if(dr["InsertedDate"] != null && dr["InsertedDate"] != DBNull.Value) 
			{
				_event.InsertedDate = DateTime.Parse(dr["InsertedDate"].ToString());
			}

			//Only populate Content if content exists
			if (withContent == true)
			{
				if(dr["Content"] != null && dr["Content"] != DBNull.Value) 
				{
					_event.Content = dr["Content"].ToString();
				}
				if(dr["AlbumContent"] != null && dr["AlbumContent"] != DBNull.Value) 
				{
					_event.AlbumContent = dr["AlbumContent"].ToString();
				}
			}

			return _event;
		}
		#endregion

		#region Holidays

		private HolidayCollection LoadHolidayCollection()
		{
			try
			{
				HolidayCollection holidays = new HolidayCollection();
				
				Command command = new Command(HOLIDAY_DATABASE_FILE, GET_HOLIDAY_LIST,0);

				
				DataTable dt = Client.Instance.ExecuteDataTable(command);

				if (dt.Rows.Count > 0) 
				{
					holidays = convertToHolidayCollection(dt);	
					holidays.CacheTTLSeconds = 3600;
					holidays.StartingYear=DateTime.Now.Year;
					_cache.Insert(holidays);
				
				}
				return holidays;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when getting holidays."+ ex.Message, ex));
			}		
		

	}

		private HolidayCollection LoadHolidayCollection(int Year)
		{
			try
			{
				HolidayCollection holidays = new HolidayCollection();
				
				Command command = new Command(HOLIDAY_DATABASE_FILE, GET_HOLIDAY_LIST,0);
				command.AddParameter("@year", SqlDbType.Int, ParameterDirection.Input, null);
				
				DataTable dt = Client.Instance.ExecuteDataTable(command);

				if (dt.Rows.Count > 0) 
				{
					holidays = convertToHolidayCollection(dt);	
					holidays.CacheTTLSeconds = 3600;
					holidays.StartingYear=Year;
					_cache.Insert(holidays);
				
				}
				return holidays;
			}
			catch(Exception ex)
			{
				throw(new BLException("BL error occured when getting holidays."+ ex.Message, ex));
			}		
		}

		
		private HolidayCollection convertToHolidayCollection(System.Data.DataTable dt)
		{
			HolidayCollection holidays = new HolidayCollection();
					
			foreach(DataRow DataRow in dt.Rows)
			{
				// Do not include Event Content field per Proc.
				holidays.Add(convertToHolidayObject(DataRow));
			}

			return holidays;
		}

		private HolidayDetail convertToHolidayObject(System.Data.DataRow dr)
		{
			HolidayDetail holiday = new HolidayDetail();
			holiday.HolidayID=Conversion.CInt(dr["holidayid"].ToString());
			holiday.HolidayDetailID=Conversion.CInt(dr["holidaydetailid"].ToString());
			holiday.HolidayDescription=dr["holidaydescription"].ToString();
			holiday.HolidayDate= Conversion.CDateTime(dr["holidaydate"].ToString());
			return holiday;
		}

		#endregion
	}
}
