using System;
using System.Collections;
using System.Data;
using System.Diagnostics;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Data;
using Matchnet.Data.Exceptions;
using Matchnet.Exceptions;
using Matchnet.Content.ValueObjects.BrandConfig;


namespace Matchnet.Content.BusinessLogic
{
	public class BrandConfigBL
	{
		public readonly static BrandConfigBL Instance = new BrandConfigBL();

		private Matchnet.Caching.Cache _cache;

		private BrandConfigBL()
		{
			_cache = Matchnet.Caching.Cache.Instance;
		}

		
		public Brands GetBrands()
		{
			Brands brands = _cache.Get(Brands.CACHE_KEY) as Brands;
			Sites sites = _cache.Get(Sites.CONTENT_BRAND_ALL_SITES_CACHE_KEY) as Sites;

			DataTable dtCommunity;
			DataTable dtSite;
			DataTable dtBrand;
			DataTable dtBrandAlias;

			if ((brands == null) | (sites == null))
			{
				brands = new Brands();
				brands.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_BRAND_SVC"));

				try
				{
					Command commandAttributes = new Command("mnSystem", "dbo.up_Brand_List", 0);
					DataSet ds = Client.Instance.ExecuteDataSet(commandAttributes);

					dtCommunity = ds.Tables[0];
					dtSite = ds.Tables[1];
					dtBrand = ds.Tables[2];
					dtBrandAlias = ds.Tables[3];
				}
				catch(Exception ex)
				{
					throw(new BLException("BL error occured when retrieving brands.", ex));
				}

				Hashtable communityTable = new Hashtable();
				foreach (DataRow row in dtCommunity.Rows)
				{
					Community community = new Community(Convert.ToInt32(row["CommunityID"]),
						row["CommunityName"].ToString());

					communityTable.Add(community.CommunityID, community);
				}

				Hashtable siteTable = new Hashtable();
				sites = new Sites();
				foreach (DataRow row in dtSite.Rows)
				{
				    bool hasRegistrationMetadata = Convert.IsDBNull(row["HasRegistrationMetadata"])
				                                       ? false
				                                       : Convert.ToBoolean(row["HasRegistrationMetadata"]);
                    
                    Site site = new Site(Convert.ToInt32(row["SiteID"]),
						(Community)communityTable[Convert.ToInt32(row["CommunityID"])],
						row["SiteName"].ToString(),
						Convert.ToInt32(row["CurrencyID"]),
						Convert.ToInt32(row["LanguageID"]),
						row["CultureName"].ToString(),
						Convert.ToInt32(row["DefaultRegionID"]),
						Convert.ToInt32(row["SearchTypeMask"]),
						Convert.ToInt32(row["DefaultSearchTypeID"]),
						row["CSSPath"].ToString(),
						Convert.ToInt32(row["CharSet"]),
						(DirectionType)Enum.Parse(typeof(DirectionType), row["Direction"].ToString(), true),
						Convert.ToInt16(row["GMTOffset"]),
						row["DefaultHost"].ToString(),
						row["SSLUrl"].ToString(),
						Convert.ToInt32(row["PaymentTypeMask"]),
                        hasRegistrationMetadata);

					sites.Add(site);
					siteTable.Add(site.SiteID, site);
				}
				
				sites.SetCacheKey = Sites.CONTENT_BRAND_ALL_SITES_CACHE_KEY;
				sites.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_BRAND_SVC"));
				_cache.Insert(sites);

				foreach (DataRow row in dtBrand.Rows)
				{
					Brand brand = new Brand(Convert.ToInt32(row["BrandID"]),
						(Site)siteTable[Convert.ToInt32(row["SiteID"])],
						row["Uri"].ToString(),
						Convert.ToInt32(row["StatusMask"]),
						row["PhoneNumber"].ToString(),
						Convert.ToInt16(row["DefaultAgeMin"]),
						Convert.ToInt16(row["DefaultAgeMax"]),
						Convert.ToInt16(row["DefaultSearchRadius"]));

					brands.AddBrand(brand);
				}

				foreach (DataRow row in dtBrandAlias.Rows)
				{
					BrandAlias brandAlias = new BrandAlias(Conversion.CInt(row["BrandAliasID"]),
						Conversion.CInt(row["BrandID"]),
						row["Uri"].ToString(),
						row["RedirectParameters"].ToString());

					brands.AddBrandAlias(brandAlias);
				}

				_cache.Insert(brands);
			}
 
			return brands;
		}


		/// <summary>
		/// Returns all sites
		/// </summary>
		/// <returns>All sites' meta data</returns>
		public Sites GetSites()
		{
			Sites sites = null;

			sites = (Sites) _cache.Get(Sites.CONTENT_BRAND_ALL_SITES_CACHE_KEY);

			if (sites != null)
				return sites;

			// Why does brands get all the info.. 
			GetBrands();

			sites = (Sites) _cache.Get(Sites.CONTENT_BRAND_ALL_SITES_CACHE_KEY);

			return sites;
		}
	}
}
