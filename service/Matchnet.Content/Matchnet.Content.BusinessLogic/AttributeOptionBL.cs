using System;
using System.Data;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Data;
using Matchnet.Content.ValueObjects.AttributeOption;


namespace Matchnet.Content.BusinessLogic
{
	public class AttributeOptionBL
	{
		public const string ATTRIBUTE_OPTION_GROUP_KEY_NAME = "AttributeOptionGroupID";
		public const string ATTRIBUTE_OPTION_KEY_NAME = "AttributeOptionID";
		
		public static readonly AttributeOptionBL Instance = new AttributeOptionBL();

		private Cache _cache;

		private AttributeOptionBL()
		{
			_cache = Cache.Instance;
		}


		public Attributes GetAttributes()
		{
			Attributes attributes = _cache.Get(Attributes.CACHE_KEY) as Attributes;

			if (attributes == null)
			{
				attributes = new Attributes();

				Command command = new Command("mnSystem", "up_AttributeOption_ListAll", 0);		
				DataTable dt = Client.Instance.ExecuteDataTable(command);

				foreach(DataRow row in dt.Rows)
				{
					//Create the AttributeOption represented by this row.
					int attributeOptionID = Convert.ToInt32(row["AttributeOptionID"]);

					int attributeValue = Constants.NULL_INT;
					if (row["AttributeValue"] != DBNull.Value)
					{
						attributeValue = Convert.ToInt32(row["AttributeValue"]);
					}

					int listOrder = Constants.NULL_INT;
					int defaultListOrder = Conversion.CInt(row["DefaultListOrder"]);

					if (row["ListOrder"] != DBNull.Value)
					{
						listOrder = Convert.ToInt32(row["ListOrder"]);
					}
					else
					{
						listOrder = defaultListOrder;
					}

					int attributeID = Conversion.CInt(row["AttributeID"]);
					string description = row["Description"].ToString();
					string resourceKey = row["ResourceKey"].ToString();

					AttributeOption attributeOption = new AttributeOption(attributeOptionID, attributeID, attributeValue, listOrder, description, resourceKey);

					string attributeName = row["AttributeName"].ToString();

					int groupID = Constants.NULL_INT;
					if (row["GroupID"] != DBNull.Value)
					{
						groupID = Convert.ToInt32(row["GroupID"]);
					}

					AttributeOptionCollection attributeOptionCollection;

					//If this is a group-specific option, add it to the appropriate AttributeOptionCollection
					if (groupID != Constants.NULL_INT)
					{
						//If there isn't already a group-specific AttributeOptionCollection for this attribute/group, create one
						if (!attributes.ContainsGroup(attributeName, groupID))
						{
							attributeOptionCollection = new AttributeOptionCollection();
							attributes.Add(attributeName, groupID, attributeOptionCollection);
						}
						else
						{
							attributeOptionCollection = attributes.Get(attributeName, groupID);
						}

						attributeOptionCollection.Add(attributeOption);
					}

					//Now add this option to the default AttributeOptionCollection
					attributeOptionCollection = attributes.Get(attributeName);

					if (attributeOptionCollection == null)
					{
						attributeOptionCollection = new AttributeOptionCollection();
						attributes.Add(attributeName, attributeOptionCollection);
					}

					//Use defaultListOrder since this is being added to the default AttributeOptionCollection
					attributeOption = new AttributeOption(attributeOptionID, attributeID, attributeValue, defaultListOrder, description, resourceKey);
					attributes.AddDefaultOption(attributeName, attributeOption);
				}

				_cache.Add(attributes);
			}

			return attributes;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeOptionID"></param>
		/// <param name="pAttributeID"></param>
		/// <param name="pDescription"></param>
		/// <param name="pAttributeValue"></param>
		/// <param name="pResourceKey"></param>
		/// <param name="pListOrder"></param>
		public void SaveAttributeOption(int pAttributeOptionID, int pAttributeID, string pDescription, int pAttributeValue, string pResourceKey, int pListOrder)
		{
			if (pAttributeOptionID == Constants.NULL_INT)
				pAttributeOptionID = Matchnet.Configuration.ServiceAdapters.KeySA.Instance.GetKey(ATTRIBUTE_OPTION_KEY_NAME);

			Matchnet.Data.Command command = new Matchnet.Data.Command("mnSystem", "up_AttributeOption_Save", 0);
			command.AddParameter("@AttributeOptionID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, pAttributeOptionID);
			command.AddParameter("@AttributeID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, pAttributeID);
			command.AddParameter("@Description", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, pDescription);
			command.AddParameter("@AttributeValue", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, pAttributeValue);
			command.AddParameter("@ResourceKey", System.Data.SqlDbType.VarChar, System.Data.ParameterDirection.Input, pResourceKey);
			command.AddParameter("@ListOrder", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, pListOrder);

			Matchnet.Data.Client.Instance.ExecuteNonQuery(command);

			_cache.Remove(Attributes.CACHE_KEY);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pAttributeOptionGroupID"></param>
		/// <param name="pAttributeOptionID"></param>
		/// <param name="pGroupID"></param>
		/// <param name="pListOrder"></param>
		public void SaveAttributeOptionGroup(int pAttributeOptionGroupID, int pAttributeOptionID, int pGroupID, int pListOrder)
		{
			if (pAttributeOptionGroupID == Constants.NULL_INT)
				pAttributeOptionGroupID = Matchnet.Configuration.ServiceAdapters.KeySA.Instance.GetKey(ATTRIBUTE_OPTION_GROUP_KEY_NAME);

			Matchnet.Data.Command command = new Matchnet.Data.Command("mnSystem", "up_AttributeOptionGroup_Save", 0);
			command.AddParameter("@AttributeOptionGroupID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, pAttributeOptionGroupID);
			command.AddParameter("@AttributeOptionID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, pAttributeOptionID);
			command.AddParameter("@GroupID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, pGroupID);
			command.AddParameter("@ListOrder", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, pListOrder);

			Matchnet.Data.Client.Instance.ExecuteNonQuery(command);

			_cache.Remove(Attributes.CACHE_KEY);
		}

		public void DeleteAttributeOption(int pAttributeOptionID)
		{
			Matchnet.Data.Command command = new Matchnet.Data.Command("mnSystem", "up_AttributeOption_Delete", 0);
			command.AddParameter("@AttributeOptionID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, pAttributeOptionID);

			Matchnet.Data.Client.Instance.ExecuteNonQuery(command);

			_cache.Remove(Attributes.CACHE_KEY);
		}

		public void DeleteAttributeOptionGroup(int pAttributeOptionID, int pGroupID)
		{
			Matchnet.Data.Command command = new Matchnet.Data.Command("mnSystem", "up_AttributeOptionGroup_Delete", 0);
			command.AddParameter("@AttributeOptionID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, pAttributeOptionID);
			command.AddParameter("@GroupID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, pGroupID);

			Matchnet.Data.Client.Instance.ExecuteNonQuery(command);

			_cache.Remove(Attributes.CACHE_KEY);
		}
	}
}
