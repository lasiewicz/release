﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.Article;
using Matchnet.Content.ValueObjects.PushNotification;
using Matchnet.Data;
using Matchnet.Exceptions;

namespace Matchnet.Content.BusinessLogic
{
    public class PushNotificationMetadataBL
    {
        public readonly static PushNotificationMetadataBL Instance = new PushNotificationMetadataBL();

        private Matchnet.Caching.Cache _cache;

        private PushNotificationMetadataBL()
        {
            _cache = Matchnet.Caching.Cache.Instance;
        }

        public PushNotificationAppGroupModel GetPushNotificationAppGroupModel(AppGroupID appId)
        {
            Dictionary<AppGroupID, PushNotificationAppGroupModel> pushNotificationAppsDictionary = GetPushNotificationsAppsFromDB();
            if (pushNotificationAppsDictionary == null)
            {
                throw (new BLException("PushNotificationMetadataBL - Push notifications app list is null."));
            }

            if (!pushNotificationAppsDictionary.ContainsKey(appId))
            {
                throw (new BLException(string.Format("PushNotificationMetadataBL - AppID {0} was not found in the push notifications list.", appId)));
            }

            return pushNotificationAppsDictionary[appId];
        }

        public Dictionary<AppGroupID, PushNotificationAppGroupModel> GetPushNotificationAppGroupModels()
        {
            Dictionary<AppGroupID, PushNotificationAppGroupModel> pushNotificationAppsDictionary = null;

            var cacheblePushNotificationAppGroupModels = _cache.Get(CacheblePushNotificationAppGroupModels.CACHE_KEY) as CacheblePushNotificationAppGroupModels;
            if (cacheblePushNotificationAppGroupModels != null &&
                    cacheblePushNotificationAppGroupModels.PushNotificationAppGroupDictionary.Count > 0)
            {
                pushNotificationAppsDictionary = cacheblePushNotificationAppGroupModels.PushNotificationAppGroupDictionary;
            }
            else
            {
                pushNotificationAppsDictionary = GetPushNotificationsAppsFromDB();
                _cache.Insert(new CacheblePushNotificationAppGroupModels(pushNotificationAppsDictionary));

            }

            return pushNotificationAppsDictionary;
        }

        private Dictionary<AppGroupID, PushNotificationAppGroupModel> GetPushNotificationsAppsFromDB()
        {
            Dictionary<AppGroupID, PushNotificationAppGroupModel> pushNotificationAppsDictionary = new Dictionary<AppGroupID, PushNotificationAppGroupModel>();

            try
            {
                var command = new Command("mnSystem", "up_PushNotification_List", 0);
                var ds = Client.Instance.ExecuteDataSet(command);

                DataTable pushNotificationCategoryTable = ds.Tables[0];
                DataTable pushNotificationTypeTable = ds.Tables[1];
                DataTable pushNotificationAppGroupCategoryTypeTable = ds.Tables[2];

                var pushNotificationCategories = new Dictionary<int, PushNotificationCategory>();
                var pushNotificationTypes = new Dictionary<int, PushNotificationType>();
                var pushNotificationApps = new Dictionary<int, PushNotificationAppGroupModel>();

                //notification categories
                foreach (DataRow dr in pushNotificationCategoryTable.Rows)
                {
                    PushNotificationCategoryId pushNotificationCategoryId =
                        (PushNotificationCategoryId)int.Parse(dr["PushNotificationCategoryID"].ToString());
                    pushNotificationCategories.Add(int.Parse(dr["PushNotificationCategoryID"].ToString()), new PushNotificationCategory()
                    {
                        DescriptionResourceKey = dr["DescriptionResourceKey"].ToString(),
                        Id = pushNotificationCategoryId,
                        Name = dr["Name"].ToString(),
                        TitleResourceKey = dr["TitleResourceKey"].ToString()
                    });
                }

                //notification types
                foreach (DataRow dr in pushNotificationTypeTable.Rows)
                {
                    PushNotificationTypeID pushNotificationTypeId =
                        (PushNotificationTypeID)int.Parse(dr["PushNotificationTypeID"].ToString());
                    pushNotificationTypes.Add(int.Parse(dr["PushNotificationTypeID"].ToString()), new PushNotificationType()
                    {
                        ID = pushNotificationTypeId,
                        DescriptionResourceKey = dr["DescriptionResourceKey"].ToString(),
                        Name = dr["Name"].ToString(),
                        TitleResourceKey = dr["TitleResourceKey"].ToString(),
                        NotificationTextResourceKey = dr["NotificationTextResourceKey"].ToString()
                    });
                }

                //notification app group + category + type mapping
                foreach (DataRow dr in pushNotificationAppGroupCategoryTypeTable.Rows)
                {
                    AppGroupID appGroupID = (AppGroupID)int.Parse(dr["AppGroupID"].ToString());

                    // Add App Group
                    if (!pushNotificationAppsDictionary.ContainsKey(appGroupID))
                    {
                        var appGroup = new PushNotificationAppGroupModel()
                        {
                            AppGroupId = (AppGroupID)appGroupID,
                            NotificationAppGroupCategories = new List<PushNotificationAppGroupCategory>(),
                            NotificationTypes = new List<PushNotificationType>()
                        };
                        pushNotificationAppsDictionary.Add(appGroupID, appGroup);
                    }
                    // Add Category
                    int categoryID = int.Parse(dr["PushNotificationCategoryID"].ToString());
                    if (pushNotificationAppsDictionary[appGroupID].NotificationAppGroupCategories.Find(cat => (int)cat.Category.Id == categoryID) == null)
                    {
                        pushNotificationAppsDictionary[appGroupID].NotificationAppGroupCategories.Add(new PushNotificationAppGroupCategory()
                        {
                            AppGroupId = (AppGroupID)appGroupID,
                            Category = pushNotificationCategories[categoryID],
                            Order = int.Parse(dr["CategoryListOrder"].ToString()),
                            NotificationCategoryTag = dr["CategoryTag"].ToString()
                        });
                    }
                    // Add Type
                    int typeID = int.Parse(dr["PushNotificationTypeID"].ToString());
                    PushNotificationAppGroupCategory appCategory =
                        pushNotificationAppsDictionary[appGroupID].NotificationAppGroupCategories.Find(
                            cat => (int)cat.Category.Id == categoryID);
                    if (appCategory.NotificationAppGroupCategoryTypes == null)
                    {
                        appCategory.NotificationAppGroupCategoryTypes = new List<PushNotificationAppGroupCategoryType>();
                    }
                    if (appCategory.NotificationAppGroupCategoryTypes.Find(type => (int)type.NotificationType.ID == typeID) == null)
                    {
                        int i = pushNotificationAppsDictionary[appGroupID].NotificationAppGroupCategories.IndexOf(appCategory);
                        pushNotificationAppsDictionary[appGroupID].NotificationAppGroupCategories[i].NotificationAppGroupCategoryTypes.Add(new PushNotificationAppGroupCategoryType()
                        {
                            AppGroupCategory = appCategory,
                            AppGroupId = (AppGroupID)appGroupID,
                            NotificationType = pushNotificationTypes[typeID],
                            Order = int.Parse(dr["TypeListOrder"].ToString()),
                            DeepLink = dr["DeepLink"].ToString(),
                            BadgeIncrementValue = int.Parse(dr["BadgeIncrementValue"].ToString())
                        });
                    }

                    // Add type to PushNotificationAppGroupModel.NotificationTypes
                    pushNotificationAppsDictionary[appGroupID].NotificationTypes.Add(pushNotificationTypes[typeID]);
                }

            }
            catch (Exception ex)
            {
                new ServiceBoundaryException(ServiceConstants.SERVICE_NAME,
                                                "Failed to load push notifications metadata from db up_PushNotification_List",
                                                ex);
            }


            return pushNotificationAppsDictionary;
        }
    }
}
