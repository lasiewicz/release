using Matchnet.Caching;
using Matchnet.Content.ValueObjects.Promotion;
using Matchnet.Data;

using System;
using System.Data;
using System.Text;

namespace Matchnet.Content.BusinessLogic
{
	/// <summary>
	/// Summary description for PromotionBL.
	/// </summary>
	public class PromotionBL
	{
		#region constants
		private const string PROMOTIONS_CACHE_KEY_PREFIX = "~CONTENTPROMOTIONS^{0}";
		private const string PROMOTERS_CACHE_KEY_PREFIX = "~CONTENTPROMOTERS";
		private const string POPUP_PROMOTION_CACHE_KEY_PREFIX = "~CONTENTPOPUPPROMOTION^{0}{1}{2}{3}{4}{5}{6}";
		private const string PROMOTION_TEXT_CACHE_KEY_PREFIX = "~CONTENTPROMOTIONTEXT^{0}{1}{2}{3}";
		private const string PROMOTION_PRM_INFO_CACHE_KEY_PREFIX = "~CONTENTPRMINFO^{0}";
		private const string PROMOTION_REGISTRATION_PROMOTION_CACHE_KEY_PREFIX = "~CONTENTREGISTRATIONPROMOTION^{0}{1}";
		private const string PROMOTION_TOKEN_CACHE_KEY_PREFIX = "~CONTENTPROMOTIONTOKEN^{0}{1}";
		private const string BANNERS_CACHE_KEY_PREFIX = "~CONTENTBANNERS^{0}";
		#endregion

		#region singleton
		/// <summary>
		/// Singleton instance
		/// </summary>
		public static readonly PromotionBL Instance = new PromotionBL();
		#endregion

		#region class variables
		private Cache _cache = null;
		#endregion

		#region constructor
		private PromotionBL()
		{
			_cache = Cache.Instance;
		}
		#endregion

		#region public methods
		/// <summary>
		/// 
		/// </summary>
		/// <param name="communityID"></param>
		/// <param name="brandID"></param>
		/// <returns></returns>
		public RegistrationPromotionCollection RetrieveRegistrationPromotions(int communityID, int brandID)
		{
			DataTable dt = null;
			RegistrationPromotionCollection retVal = null;
			RegistrationPromotion myPromotion = null;

			retVal = _cache.Get(GetRegistrationPromotionCacheKey(communityID, brandID)) as RegistrationPromotionCollection;

			if(retVal == null) 
			{
				Command command = new Command("mnSystem", "dbo.up_RegistrationPromotion_List", 0);
				command.AddParameter("@DomainID", SqlDbType.Int, ParameterDirection.Input, communityID);
				command.AddParameter("@PrivateLabelID", SqlDbType.Int, ParameterDirection.Input, brandID);
				dt = Client.Instance.ExecuteDataTable(command);

				retVal = new RegistrationPromotionCollection();
				foreach(DataRow myRow in dt.Rows)
				{
					myPromotion = MakeRegistrationPromotionFromDataRow(myRow);
					retVal.Add(myPromotion);
				}
				retVal.Sort();
				retVal.SetCacheKey = GetRegistrationPromotionCacheKey(communityID, brandID);
				CacheRegistrationPromotion(retVal);
			}

			return(retVal);
		}

        /// <summary>
        /// Retrieves a list of PromotionID and URL Referral mapping
        /// </summary>
        public PRMURLMapperList GetPRMURLMapperList()
        {
            DataTable dt = null;
            PRMURLMapperList retVal = null;

            retVal = _cache.Get(PRMURLMapperList.GetCacheKeyString()) as PRMURLMapperList;

            if (retVal == null)
            {
                Command command = new Command("mnSystem", "dbo.up_PromotionIDURLMapper_List", 0);
                dt = Client.Instance.ExecuteDataTable(command);

                retVal = new PRMURLMapperList();

                PRMURLMapper prmURLMapper = null;
                foreach (DataRow row in dt.Rows)
                {
                    //Note: Presorted in database by priority descending
                    prmURLMapper = new PRMURLMapper();
                    prmURLMapper.PromotionID = Conversion.CInt(row["PromotionID"]);
                    prmURLMapper.PromotionIDURLMapperID = Conversion.CInt(row["PromotionIDURLMapperID"]);
                    prmURLMapper.Priority = Conversion.CInt(row["Priority"]);
                    prmURLMapper.Description = row["Description"].ToString();
                    prmURLMapper.SourceURL = row["SourceURL"].ToString();
                    prmURLMapper.IsCatchAll = Conversion.CBool(row["IsCatchAll"]);

                    retVal.PRMURLMappers.Add(prmURLMapper);
                }

                _cache.Insert(retVal);
            }

            return (retVal);
        }

		#endregion

		#region Replication
		/// <summary>
		/// Accepts an IReplicable object and updates the local service cache.
		/// </summary>
		/// <param name="pReplicableObject"></param>
		public void CacheReplicatedObject(IReplicable pReplicableObject)
		{
			switch (pReplicableObject.GetType().Name)
			{
				case "RegistrationPromotionCollection":
					CacheRegistrationPromotion((RegistrationPromotionCollection)pReplicableObject);
					break;
				case "PopupPromotion":
					CachePopupPromotion((PopupPromotion)pReplicableObject);
					break;
				case "PromoterCollection":
					CachePromoters((PromoterCollection)pReplicableObject);
					break;
				case "PRMInfo":
					CachePromotionPRMInfo((PRMInfo)pReplicableObject);
					break;
				case "PromotionCollection":
					CachePromotions((PromotionCollection)pReplicableObject);
					break;
				case "PromotionText":
					CachePromotionText((PromotionText)pReplicableObject);
					break;
				default:
					throw new Exception("invalid replication object type received (" + pReplicableObject.GetType().ToString() + ")");
			}
		}
		#endregion

		#region private Cache Routines
		private void CachePromotions(PromotionCollection pPromotionCollection)
		{
			pPromotionCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pPromotionCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PROMOTION_SVC"));
			_cache.Insert(pPromotionCollection);
		}

		private void CachePromoters(PromoterCollection pPromoterCollection) 
		{
			pPromoterCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pPromoterCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PROMOTION_SVC"));
			_cache.Insert(pPromoterCollection);
		}

		private void CachePopupPromotion(PopupPromotion pPopupPromotion) 
		{
			pPopupPromotion.CachePriority = CacheItemPriorityLevel.Normal;
			pPopupPromotion.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PROMOTION_SVC"));
			_cache.Insert(pPopupPromotion);
		}

		private void CachePromotionText(PromotionText pPromotionText) 
		{
			pPromotionText.CachePriority = CacheItemPriorityLevel.Normal;
			pPromotionText.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PROMOTION_SVC"));
			_cache.Insert(pPromotionText);
		}

		private void CachePromotionPRMInfo(PRMInfo pPRMInfo) 
		{
			pPRMInfo.CachePriority = CacheItemPriorityLevel.Normal;
			pPRMInfo.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PROMOTION_SVC"));
			_cache.Insert(pPRMInfo);
		}

		private void CacheRegistrationPromotion(RegistrationPromotionCollection pRegistrationPromotionCollection)
		{
			pRegistrationPromotionCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pRegistrationPromotionCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PROMOTION_SVC"));
			_cache.Insert(pRegistrationPromotionCollection);
		}

		private void CachePromotionTokens(PromotionTokenCollection pPromotionTokens) 
		{
			pPromotionTokens.CachePriority = CacheItemPriorityLevel.Normal;
			pPromotionTokens.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PROMOTION_SA"));
			_cache.Insert(pPromotionTokens);
		}

		private void CacheBanners(BannerCollection pBannerCollection)
		{
			pBannerCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pBannerCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PROMOTION_SA"));
			_cache.Insert(pBannerCollection);
		}
		private string GetPromotionsCacheKey(int promoterID)
		{
			return string.Format(PROMOTIONS_CACHE_KEY_PREFIX, promoterID);
		}

		private string GetPromotersCacheKey()
		{
			return PROMOTERS_CACHE_KEY_PREFIX;
		}

		private string GetPopUpPromotionCacheKey(int promotionID, int brandID, 
			int siteID, int pageID, DateTime date, string userConditions, string baseURL)
		{
			return string.Format(POPUP_PROMOTION_CACHE_KEY_PREFIX, promotionID, brandID,
				siteID, pageID, date, userConditions, baseURL);
		}

		private string GetPromotionTextCacheKey(int promotionID, int actionPageID, int communityID, 
			int memberID) 
		{
			return string.Format(PROMOTION_TEXT_CACHE_KEY_PREFIX, promotionID, actionPageID, communityID,
				memberID);
		}

		private string GetPromotionPRMInfoCacheKey(int prm) 
		{
			return string.Format(PROMOTION_PRM_INFO_CACHE_KEY_PREFIX, prm);
		}

		private string GetRegistrationPromotionCacheKey(int communityID, int brandID) 
		{
			return string.Format(PROMOTION_REGISTRATION_PROMOTION_CACHE_KEY_PREFIX, communityID, brandID);
		}

		private string GetPromotionTokensCacheKey(int communityID, int memberID) 
		{
			return string.Format(PROMOTION_TOKEN_CACHE_KEY_PREFIX, communityID, memberID);
		}
		private string GetBannersCacheKey(int groupID)
		{
			return string.Format(BANNERS_CACHE_KEY_PREFIX, groupID);
		}
		#endregion

		#region utility methods
		/// <summary>
		/// 
		/// </summary>
		/// <param name="myRow"></param>
		/// <param name="date"></param>
		/// <param name="baseURL"></param>
		/// <returns></returns>
		private PopupPromotion MakePopupPromotionFromDataRow(DataRow myRow, DateTime date, string baseURL)
		{
			PopupPromotion retVal;

			string exitPopupURL = myRow["ExitPopupURL"].ToString();
			// If the URL is relative make it absolute.
			if (exitPopupURL.Substring(0, 4).ToLower() != "http")
			{
				exitPopupURL = MakeAbsoluteURL(baseURL, exitPopupURL);
			}
			int exitPopupWidth = System.Convert.ToInt32(myRow["ExitPopupWidth"]);
			int exitPopupHeight = System.Convert.ToInt32(myRow["ExitPopupHeight"]);
			string exitPopupProps = myRow["ExitPopupProps"].ToString();
			string exitPopupDestinationURL = myRow["ExitPopupDestinationURL"].ToString();
			// If the URL is relative make it absolute.
			if (exitPopupDestinationURL.Substring(0, 4).ToLower() != "http")
			{
				exitPopupDestinationURL = MakeAbsoluteURL(baseURL, exitPopupDestinationURL);
			}
			int exitPopupResourceID = System.Convert.ToInt32(myRow["ExitPopupResourceID"]);
			
			// TOFIX: there is currently just one promotion type being used in the table, the 5Day promo.
			// this area needs work to come up with a more general approach to handling promotions and promo text.
			PromotionType promotionType = PromotionType.FiveDaysFree;
			if (exitPopupResourceID != 602296)
			{
				throw new ArgumentOutOfRangeException("ExitPopupResourceID", exitPopupResourceID, "Unexpected exit popup resource ID value.");
			}

			retVal = new PopupPromotion(exitPopupURL, exitPopupWidth, exitPopupHeight, exitPopupProps, exitPopupDestinationURL, promotionType, date, true);

			return(retVal);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="baseURL"></param>
		/// <param name="restOfURL"></param>
		/// <returns></returns>
		private string MakeAbsoluteURL(string baseURL, string restOfURL)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(baseURL);
			sb.Append("/");
			sb.Append(restOfURL);
			return(sb.ToString());
		}

		/// <summary>
		/// Make a Promoter VO from a DataRow
		/// </summary>
		/// <param name="myRow"></param>
		/// <returns></returns>
		private Promoter MakePromoterFromDataRow(DataRow myRow)
		{
			Promoter retVal;

			int promoterID = Conversion.CInt(myRow["PromoterID"]);
			int promoterTypeID = Conversion.CInt(myRow["PromoterTypeID"]);
			string promoterType = myRow["PromoterType"].ToString();
			int countryRegionID = Conversion.CInt(myRow["CountryRegionID"]);
			string emailAddress = myRow["EmailAddress"].ToString();
			DateTime insertDate = Conversion.CDateTime(myRow["InsertDate"]);
			string firstName = myRow["FirstName"].ToString();
			string lastName = myRow["LastName"].ToString();
			string company = myRow["Company"].ToString();
			string address = myRow["Address"].ToString();
			string address2 = myRow["Address2"].ToString();
			string city = myRow["City"].ToString();
			string region = myRow["Region"].ToString();
			string postalCode = myRow["PostalCode"].ToString();
			string phone = myRow["Phone"].ToString();
			string fax = myRow["Fax"].ToString();
			string comments = myRow["Comments"].ToString();

			retVal = new Promoter(promoterID, promoterTypeID, promoterType, countryRegionID, emailAddress, insertDate,
				firstName, lastName, company, address, address2, city, region, postalCode, phone, fax, comments);

			return(retVal);
		}

		/// <summary>
		/// Make a promotion VO from a DataRow
		/// </summary>
		/// <param name="myRow"></param>
		/// <returns></returns>
		private Promotion MakePromotionFromDataRow(DataRow myRow)
		{
			Promotion retVal;

			int promotionID = Conversion.CInt(myRow["PromotionID"]);
			int promotionTypeID = Conversion.CInt(myRow["PromotionTypeID"]);
			string promotionType = myRow["PromotionType"].ToString();
			DateTime insertDate = Conversion.CDateTime(myRow["InsertDate"]);
			string description = myRow["Description"].ToString();
			string url = myRow["URL"].ToString();
			int promoterID = Conversion.CInt(myRow["PromoterID"]);
			string promoterCompany = myRow["PromoterCompany"].ToString();
			bool shortRegFormFlag = Conversion.CBool(myRow["ShortRegFormFlag"], false);

			retVal = new Promotion(promotionID, promotionTypeID, promotionType, insertDate, description, url, promoterID, promoterCompany, shortRegFormFlag);

			return(retVal);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="myRow"></param>
		/// <returns></returns>
		private RegistrationPromotion MakeRegistrationPromotionFromDataRow(DataRow myRow)
		{
			RegistrationPromotion retVal;

			int registrationPromotionID = Conversion.CInt(myRow["RegistrationPromotionID"]);
			int promotionID = Conversion.CInt(myRow["PromotionID"]);
			int listOrder = Conversion.CInt(myRow["ListOrder"]);

			retVal = new RegistrationPromotion(registrationPromotionID, promotionID, myRow["ResourceConstant"].ToString(), listOrder);

			return(retVal);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="myRow"></param>
		/// <returns></returns>
		private Banner MakeBannerFromDataRow(DataRow myRow)
		{
			int bannerID = Matchnet.Conversion.CInt(myRow["BannerID"]);

			string resourceConstant = myRow["ResourceConstant"].ToString();

			return(new Banner(bannerID, resourceConstant));
		}
		#endregion

        #region Obsolete
        /*REMOVED as part of DB efforts to clean up database*/
        //public BannerCollection RetrieveBanners(int groupID)
        //{
        //    DataTable dt = null;
        //    BannerCollection retVal = null;
        //    Banner banner = null;

        //    retVal = _cache.Get(GetBannersCacheKey(groupID)) as BannerCollection;

        //    if (retVal == null)
        //    {
        //        Command command = new Command("mnSystem", "dbo.up_Banner_List", 0);
        //        command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);

        //        dt = Client.Instance.ExecuteDataTable(command);

        //        retVal = new BannerCollection();

        //        foreach (DataRow row in dt.Rows)
        //        {
        //            banner = MakeBannerFromDataRow(row);
        //            retVal.Add(banner);
        //        }


        //        retVal.SetCacheKey = GetBannersCacheKey(groupID);
        //        CacheBanners(retVal);
        //    }

        //    return (retVal);
        //}

        #endregion
    }
}
