using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Collections;

using Matchnet;
using Matchnet.Caching;
using Matchnet.Content.ValueObjects.Registration;
using Matchnet.Data;
using Matchnet.Exceptions;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Data.Hydra;

namespace Matchnet.Content.BusinessLogic
{
	/// <summary>
	/// Summary description for AdminBL.
	/// </summary>
	public class AdminBL
	{
		private const Int32 MaxPublishRecords = 30;
		public static readonly AdminBL Instance = new AdminBL();
		private Cache _cache;

		private AdminBL()
		{
			_cache = Cache.Instance;
		}

		public AdminActionReasonCollection GetAdminActionReasons()
		{
			AdminActionReasonCollection reasons;

			try
			{
				reasons = _cache.Get(AdminActionReasonCollection.CACHE_KEY) as AdminActionReasonCollection;

				if (reasons == null)
				{
					reasons = new AdminActionReasonCollection();

					Command command = new Command("mnAdmin", "up_AdminActionReason_List", 0);
					DataTable dataTable = Client.Instance.ExecuteDataTable(command);

					foreach(DataRow row in dataTable.Rows)
					{
						AdminActionReason adminActionReason = new AdminActionReason((AdminActionReasonID) Conversion.CInt(row["AdminActionReasonID"]),
							(AdminActionReasonType)Conversion.CInt(row["AdminActionReasonTypeID"]),row["Description"].ToString(), Convert.ToBoolean(row["Display"]));

						reasons.Add(adminActionReason);
					}

					_cache.Insert(reasons);
				}
			}
			catch (Exception ex)
			{
				throw(new BLException("Unable to retrieve Admin Action Reasons.", ex));
			}

			return reasons;
		}

		/// <summary>
		/// Gets the list of admin action types.
		/// </summary>
		/// <returns></returns>
		public AdminActionTypeCollection GetAdminActionTypes()
		{
			AdminActionTypeCollection adminActionTypeCollection;

			try
			{
				adminActionTypeCollection = _cache.Get(AdminActionTypeCollection.CACHE_KEY) as AdminActionTypeCollection;

				if (adminActionTypeCollection == null)
				{
					adminActionTypeCollection = new AdminActionTypeCollection();

					Command command = new Command("mnAdmin", "up_AdminActionType_List", 0);
					DataTable dataTable = Client.Instance.ExecuteDataTable(command);

					foreach(DataRow row in dataTable.Rows)
					{
						AdminActionType adminActionType = new AdminActionType(Conversion.CInt(row["AdminActionTypeID"]),
							row["Description"].ToString());

						adminActionTypeCollection.Add(adminActionType);
					}

					_cache.Insert(adminActionTypeCollection);
				}
			}
			catch (Exception ex)
			{
				throw(new BLException("Unable to retrieve Admin Action Types.", ex));
			}

			return adminActionTypeCollection;
		}

		/// <summary>
		/// Get admin action log
		/// </summary>
		public AdminActionLogCollection GetAdminActionLog(int memberID, int communityID)
		{
			AdminActionLogCollection actionLogs = new AdminActionLogCollection();
			try
			{
				//TOFIX: Change this proc to pull the AdminActionMask instead of the ActionDescription.
				//Modify the code below, web code, and AdminActionLog VO accordingly.  This will allow
				//the web code to display the descriptions for log entries that contain multiple admin actions.
				Command command = new Command("mnAdmin", "up_AdminActionLogReport_List", 0);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, communityID);
				DataTable dataTable = Client.Instance.ExecuteDataTable(command);

				foreach(DataRow thisRow in dataTable.Rows)
				{
					AdminActionLog tempAdminActionLog = new AdminActionLog(Convert.ToInt32(thisRow["MemberID"])
						, thisRow["EmailAddress"].ToString()
						, thisRow["ActionDescription"].ToString()
						, Convert.ToDateTime(thisRow["InsertDate"])
						);
				
					actionLogs.Add(tempAdminActionLog);
				}
			}
			catch (Exception ex)
			{
				throw(new BLException("Unable to retreive Admin Action Log.  MemberID:" + memberID +
					", CommunityID:" + communityID, ex));
			}
			
			return actionLogs;
		}

		public AdminActivityContainerCollection GetAdminActivity(int memberID, int communityID)
		{
			AdminActivityContainerCollection adminActivityContainerCollection = new AdminActivityContainerCollection();
			try
			{
				Command command = new Command("mnAdmin", "up_AdminReportAll_List", 0);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, communityID);
				DataTable dataTable = Client.Instance.ExecuteDataTable(command);
				
				foreach(DataRow thisRow in dataTable.Rows)
				{
					ActivityType type = (ActivityType) Conversion.CInt(thisRow["ActivityType"]);
					AdminActivityContainer container;

					if(type == ActivityType.Action)
					{
						AdminActionLog tempAdminActionLog = new AdminActionLog(Convert.ToInt32(thisRow["MemberID"])
							, thisRow["EmailAddress"].ToString()
							, thisRow["ActionDescription"].ToString()
							, Convert.ToDateTime(thisRow["InsertDate"])
							, thisRow["ActionReason"].ToString()	
							);

						container = new AdminActivityContainer(tempAdminActionLog);
					}
					else
					{
						//note
						AdminNote tempAdminNote = new AdminNote(
							Convert.ToInt32(thisRow["MemberID"])
							, Convert.ToInt32(thisRow["ID"])
							, Convert.ToInt32(thisRow["AdminMemberID"])
							, Convert.ToDateTime(thisRow["InsertDate"])
							, thisRow["Note"].ToString()
							, thisRow["EmailAddress"].ToString());
						container = new AdminActivityContainer(tempAdminNote);
					}
					adminActivityContainerCollection.Add(container);					
				}

			}
			catch (Exception ex)
			{
				throw(new BLException("Unable to retreive Admin Activity Log. MemberID: " + memberID +
					", GroupID:" + communityID, ex));
			}
			
			return adminActivityContainerCollection;
		}

		/// <summary>
		/// Get admin actions performed on a particular member
		/// </summary>
		public AdminActionCollection GetAdminActions(int memberID, int groupID)
		{
			AdminActionCollection adminActionCollection = new AdminActionCollection();
			try
			{
				Command command = new Command("mnAdmin", "up_AdminActionLog_List_Member", 0);
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
				DataTable dataTable = Client.Instance.ExecuteDataTable(command);

				foreach(DataRow thisRow in dataTable.Rows)
				{
					AdminActionItem adminActionItem = new AdminActionItem(Conversion.CInt(thisRow["MemberID"])
						, Conversion.CInt(thisRow["AdminMemberID"])
						, (AdminAction) Conversion.CInt(thisRow["AdminActionMask"])
						, Conversion.CDateTime(thisRow["InsertDate"])
						);
				
					adminActionCollection.Add(adminActionItem);
				}
			}
			catch (Exception ex)
			{
				throw(new BLException("Unable to retreive Admin Action Log. MemberID: " + memberID +
					", GroupID:" + groupID, ex));
			}
			
			return adminActionCollection;
		}

		/// <summary>
		/// Get admin action report
		/// </summary>
		public AdminActionReport GetAdminActionReport(DateTime startDate, DateTime endDate, Int32 adminMemberID, Int32 groupID, Int32 adminActionMask)
		{
			AdminActionReport adminActionReport = new AdminActionReport();
			try
			{
				Command command = new Command("mnAdmin", "up_AdminActionLog_List", 0);
				command.AddParameter("@StartDate", SqlDbType.SmallDateTime, ParameterDirection.Input, startDate);
				command.AddParameter("@EndDate", SqlDbType.SmallDateTime, ParameterDirection.Input, endDate);
				if (adminMemberID != Constants.NULL_INT)
				{
					command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
				}
				if (groupID != Constants.NULL_INT)
				{
					command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
				}
				if (adminActionMask != Constants.NULL_INT)
				{
					command.AddParameter("@AdminActionMask", SqlDbType.Int, ParameterDirection.Input, adminActionMask);
				}
				
				DataTable dataTable = Client.Instance.ExecuteDataTable(command);

				foreach(DataRow thisRow in dataTable.Rows)
				{
					AdminActionReportItem adminActionReportItem = new AdminActionReportItem(Conversion.CInt(thisRow["AdminMemberID"])
						, Conversion.CInt(thisRow["GroupID"])
						, Conversion.CDateTime(thisRow["InsertDate"])
						, Conversion.CInt(thisRow["ActionCount"]));
				
					adminActionReport.Add(adminActionReportItem);
				}
			}
			catch (Exception ex)
			{
				throw(new BLException("Unable to retreive Admin Action Report.", ex));
			}
			
			return adminActionReport;
		}

		/// <summary>
		/// Get approval counts report
		/// </summary>
		public ApprovalCountsCollection GetApprovalCountsReport(DateTime startDate, DateTime endDate, Int32 adminMemberID, Int32 groupID)
		{
			ApprovalCountsCollection approvalCountReport = new ApprovalCountsCollection();
			try
			{
				Command command = new Command("mnAdmin", "up_AdminApprovalCounts_Report", 0);
				command.AddParameter("@StartDate", SqlDbType.SmallDateTime, ParameterDirection.Input, startDate);
				command.AddParameter("@EndDate", SqlDbType.SmallDateTime, ParameterDirection.Input, endDate);
				if (adminMemberID != Constants.NULL_INT)
				{
					command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
				}
				if (groupID != Constants.NULL_INT)
				{
					command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, groupID);
				}
				
				DataTable dataTable = Client.Instance.ExecuteDataTable(command);

				foreach(DataRow thisRow in dataTable.Rows)
				{
					Int32 memberID = Conversion.CInt(thisRow["AdminMemberID"]);
					Int32 adminActionMask = Conversion.CInt(thisRow["AdminActionMask"]);
					Int32 actionCount = Conversion.CInt(thisRow["ActionCount"]);

					//Find the current ApprovalCounts object for this admin member, or create a new ApprovalCounts object
					ApprovalCounts approvalCounts;
					if (approvalCountReport.Contains(memberID))
					{
						approvalCounts = approvalCountReport[memberID];
					}
					else
					{
						approvalCounts = new ApprovalCounts(memberID);
						approvalCountReport.Add(approvalCounts);
					}

					//Update the counts based on the action mask:
					if (maskContains(adminActionMask, (Int32) AdminAction.ApprovedFreeText)
						|| maskContains(adminActionMask, (Int32) AdminAction.DeletedFreeTextAttribute))
					{
						approvalCounts.TextCount += actionCount;
					}

					if (maskContains(adminActionMask, (Int32) AdminAction.ApprovePhoto)
						|| maskContains(adminActionMask, (Int32) AdminAction.RejectPhoto))
					{
						approvalCounts.PhotoCount += actionCount;
					}

					if (maskContains(adminActionMask, (Int32) AdminAction.ApprovedQuestionAnswer)
						|| maskContains(adminActionMask, (Int32) AdminAction.DeletedQuestionAnswer))
					{
						approvalCounts.QuestionAnswerCount += actionCount;
					}
				}
			}
			catch (Exception ex)
			{
				throw(new BLException("Unable to retreive Admin Approval Counts Report.", ex));
			}
			
			return approvalCountReport;
		}

		private static bool maskContains(Int32 mask, Int32 val)
		{
			return ((mask & val) == val);
		}

		/// <summary>
		/// Adds a record to the Admin Action Log
		/// </summary>
		/// <param name="memberID">The memberId of the member who's free text has just been acted on</param>
		/// <param name="domainID">The domainId of the member who's free text has just been acted on</param>
		/// <param name="adminActionMask">The action taken on this member</param>
		/// <param name="adminMemberID">The the memberId of the customer service rep that applied the action</param>
		public void AdminActionLogInsert(int memberID, int communityID, int adminActionMask, int adminMemberID, AdminMemberIDType adminMemberIDType, AdminActionReasonID reasonID)
		{
			
			// Check for a valid admin memberid, if we don't have one, complain!!!
			if ( adminMemberID == 0 )
			{
				//	TOFIX - Redo the exception throwing here once this is moved into the
				//	actual service - dcornell
				throw new Exception("Invalid admin memberid, please log in again - AdminActionLogInsert");
			}
			
			try
			{
				if ( memberID > 0 && communityID > 0 )
				{
					Command command = new Command("mnAdmin", "up_AdminActionLog_Insert", memberID);
					command.AddParameter("@AdminActionLogID", SqlDbType.Int, ParameterDirection.Input, Matchnet.Configuration.ServiceAdapters.KeySA.Instance.GetKey("AdminActionLogID"));
					command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
					command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, communityID);
					command.AddParameter("@AdminActionMask", SqlDbType.Int, ParameterDirection.Input, adminActionMask);
					command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, adminMemberID);
					command.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, DateTime.Now.ToString("G"));
					command.AddParameter("@AdminMemberIDTypeID", SqlDbType.Int,ParameterDirection.Input, (int)adminMemberIDType);
					command.AddParameter("@AdminActionReasonID", SqlDbType.Int,ParameterDirection.Input, (int)reasonID);
					Client.Instance.ExecuteAsyncWrite(command);
				}
			}
			catch(Exception ex)
			{
				throw new BLException("Unable to insert admin action log" +
					"MemberID: " + memberID + 
					" CommunityID: " + communityID + 
					" AdminActionMask: " + adminActionMask + 
					" AdminMemberID: " + adminMemberID,
					ex);
			}
		}

        public void RegistrationAdminActionLogInsert(string adminAccountName, RegAdminActionType actionType, int scenarioId)
        {
            try
            {
                Command command = new Command("mnAdmin", "up_RegAdminActionLog_Insert", 0);
                command.AddParameter("@RegAdminActionLogID", SqlDbType.Int, ParameterDirection.Input, KeySA.Instance.GetKey("RegAdminActionLogID"));
                command.AddParameter("@AdminAccountName", SqlDbType.VarChar, ParameterDirection.Input, adminAccountName);
                command.AddParameter("@ActionType", SqlDbType.Int, ParameterDirection.Input, Convert.ToInt32(actionType));
                command.AddParameter("@ScenarioID", SqlDbType.Int, ParameterDirection.Input, scenarioId);
                Client.Instance.ExecuteAsyncWrite(command);
               
            }
            catch (Exception ex)
            {
                throw new BLException("Unable to insertreg admin action log" +
                    "AdminName: " + adminAccountName +
                    " ActionType: " + actionType +
                    " ScenarioId: " + scenarioId,
                    ex);
            }
        }

        public void RegistrationAdminScenarioHistorySave(int regScenarioId, int deviceTypeID, int siteID, double weight, DateTime activeDate, DateTime inactiveDate, string updatedBy)
        {
            try
            {
                DateTime nowDate = DateTime.Now;
                int regAdminScenarioHistoryID = 0;
                if (weight > 0)
                {
                    regAdminScenarioHistoryID = Configuration.ServiceAdapters.KeySA.Instance.GetKey("RegAdminScenarioHistoryID");
                }

                if (activeDate == DateTime.MinValue)
                {
                    activeDate = nowDate;
                }

                if (inactiveDate == DateTime.MinValue)
                {
                    inactiveDate = nowDate;
                }

                var command = new Command("mnAdmin", "up_RegAdminScenarioHistory_Save", 0);
                command.AddParameter("@RegAdminScenarioHistoryID", SqlDbType.Int, ParameterDirection.Input, regAdminScenarioHistoryID);
                command.AddParameter("@ScenarioID", SqlDbType.Int, ParameterDirection.Input, regScenarioId);
                command.AddParameter("@AdminAccountName", SqlDbType.NVarChar, ParameterDirection.Input, updatedBy);
                command.AddParameter("@Weight", SqlDbType.Decimal, ParameterDirection.Input, weight);
                command.AddParameter("@DeviceTypeID", SqlDbType.Int, ParameterDirection.Input, deviceTypeID);
                command.AddParameter("@SiteID", SqlDbType.Int, ParameterDirection.Input, siteID);
                command.AddParameter("@ActiveDate", SqlDbType.DateTime, ParameterDirection.Input, activeDate);
                command.AddParameter("@InactiveDate", SqlDbType.DateTime, ParameterDirection.Input, inactiveDate);

                Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when saving scenario history", ex));
            }
        }

		/// <summary>
		/// Save new admin note
		/// </summary>
		public void UpdateAdminNote(string newNote, int targetMemberID, int memberID, int communityID, int maxNoteLength)
		{
			int adminNoteID = Matchnet.Configuration.ServiceAdapters.KeySA.Instance.GetKey("AdminNoteID");
			Command command = new Command("mnAdmin", "up_AdminNote_Insert", 0);
			command.AddParameter("@AdminNoteID", SqlDbType.Int, ParameterDirection.Input, adminNoteID);
			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
			command.AddParameter("@AdminMemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, communityID);
			command.AddParameter("@Note", SqlDbType.VarChar, ParameterDirection.Input, newNote);
			command.AddParameter("@InsertDate", SqlDbType.SmallDateTime, ParameterDirection.Input, DateTime.Now);
			Client.Instance.ExecuteAsyncWrite(command);
		}

		/// <summary>
		/// Load admin notes for member.
		/// </summary>
		public AdminNoteCollection LoadAdminNotes(int targetMemberID, int communityID, int startRow, int pageSize, int totalRows)
		{
			AdminNoteCollection adminNotes = new AdminNoteCollection();
			try
			{
				Command command = new Command("mnAdmin", "up_AdminNote_List", 0);
			
				command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, targetMemberID);
				command.AddParameter("@GroupID", SqlDbType.Int, ParameterDirection.Input, communityID);
				DataTable dataTable = Client.Instance.ExecuteDataTable(command);

				foreach(DataRow thisRow in dataTable.Rows)
				{
					AdminNote adminNote = new AdminNote(
						Convert.ToInt32(thisRow["MemberID"])
						, Convert.ToInt32(thisRow["AdminNoteID"])
						, Convert.ToInt32(thisRow["AdminMemberID"])
						, Convert.ToDateTime(thisRow["InsertDate"])
						, thisRow["Note"].ToString());
				
					adminNotes.Add(adminNote);
				}
			}
			catch (Exception ex)
			{
				throw(new BLException("Unable to retreive Admin Notes.", ex));
			}
			
			return adminNotes;
		}

        public AdminActionReasonID GetLastAdminSuspendReasonForMember(int memberID)
        {
            AdminActionReasonID reasonID = AdminActionReasonID.None;
            try
            {
                Command command = new Command("mnAdmin", "up_AdminActionLog_LastSuspendReason_List", 0);

                command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
                
                DataTable dataTable = Client.Instance.ExecuteDataTable(command);

                if (dataTable.Rows.Count == 1 && dataTable.Rows[0]["AdminActionReasonID"] != DBNull.Value)
                {
                    reasonID = (AdminActionReasonID)Convert.ToInt32(dataTable.Rows[0]["AdminActionReasonID"]);
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("Unable to retreive Last Admin Suspend Reason For Member.", ex));
            }

            return reasonID;
        }

		#region Publish
		/// <summary>
		///
		/// </summary>
		public PublishCollection GetPublishes(DateTime startDate, DateTime endDate)
		{
			PublishCollection publishCollection = new PublishCollection();
			try
			{
				Command command = new Command("mnAdmin", "up_Publish_List", 0);
				command.AddParameter("@StartDate", SqlDbType.DateTime, ParameterDirection.Input, startDate);
				command.AddParameter("@EndDate", SqlDbType.DateTime, ParameterDirection.Input, endDate);
				DataTable dataTable = Client.Instance.ExecuteDataTable(command);

				Int32 counter = 0;
				String publishIDHolder = String.Empty;
				foreach(DataRow row in dataTable.Rows)
				{
					// Only one record for each publishID.
					if (publishIDHolder != Convert.ToString(row["publishID"]) && counter < MaxPublishRecords)
					{
						Publish publish = new Publish(Convert.ToInt32(row["PublishID"])
							, Convert.ToInt32(row["PublishObjectID"])
							, ((PublishObjectType)Convert.ToInt32(row["PublishObjectTypeID"]))
							, null
							, Convert.ToDateTime(row["InsertDate"])
							);

						PublishActionCollection publishActionCollection = new PublishActionCollection();
						publishActionCollection.Add(new PublishAction(publish.PublishID
							, Convert.ToInt32(row["PublishActionPublishObjectID"])
							, ((PublishActionTypeEnum)Convert.ToInt32(row["PublishActionTypeID"]))
							, Convert.ToInt32(row["WorkflowOrder"])
							, Convert.ToInt32(row["MemberID"])
							, Convert.ToDateTime(row["PublishActionInsertDate"])
							));

						publish.PublishActions = publishActionCollection;

						publishCollection.Add(publish);
					}

					publishIDHolder = Convert.ToString(row["publishID"]);
					counter++;
				}
			}
			catch (Exception ex)
			{
				throw(new BLException("Unable to get Publishes information.  StartDate: " + startDate.ToString() +
					", EndDate:" + endDate.ToString(), ex));
			}
			
			return publishCollection;
		}

		/// <summary>
		///
		/// </summary>
		public Publish GetPublish(Int32 publishObjectID, PublishObjectType publishObjectType)
		{
			Publish publish = new Publish();
			try
			{
				Command command = new Command("mnAdmin", "up_Publish_List", 0);
				command.AddParameter("@PublishObjectID", SqlDbType.Int, ParameterDirection.Input, publishObjectID);
				command.AddParameter("@PublishObjectTypeID", SqlDbType.Int, ParameterDirection.Input, Convert.ToInt32(publishObjectType));
				DataTable dataTable = Client.Instance.ExecuteDataTable(command);

				Boolean firstRecord = true;
				PublishActionCollection publishActionCollection = new PublishActionCollection();
				foreach(DataRow row in dataTable.Rows)
				{
					if (firstRecord)
					{
						Object content = null;
						if (row["Content"].ToString() != String.Empty && row["Content"].ToString().Length != 0)
						{
							try
							{
								content = MiscUtils.BinaryDeserializeObject(((Byte[])row["Content"]));
							}
							catch
							{
								// If the object could not be serialized, it's probably a VO problem.  Nothing we can do
								// except swallow the error and return null.
								content = null;
							}
						}

						publish = new Publish(Convert.ToInt32(row["PublishID"])
							, Convert.ToInt32(row["PublishObjectID"])
							, ((PublishObjectType)Convert.ToInt32(row["PublishObjectTypeID"]))
							, content
							, Convert.ToDateTime(row["InsertDate"])
							);
					}

					publishActionCollection.Add(new PublishAction(publish.PublishID
						, Convert.ToInt32(row["PublishActionPublishObjectID"])
						, ((PublishActionTypeEnum)Convert.ToInt32(row["PublishActionTypeID"]))
						, Convert.ToInt32(row["WorkflowOrder"])
						, Convert.ToInt32(row["MemberID"])
						, Convert.ToDateTime(row["PublishActionInsertDate"])
						));

					firstRecord = false;
				}

				publish.PublishActions = publishActionCollection;
			}
			catch (Exception ex)
			{
				throw(new BLException("Unable to get Publishes information.  PublishObjectID: " + publishObjectID.ToString() + ";  PublishObjectType: " + publishObjectType.ToString(), ex));
			}
			
			return publish;
		}

		public Int32 SavePublish(Int32 publishObjectID,
			PublishObjectType publishObjectType,
			Object content)
		{
			Int32 publishID = KeySA.Instance.GetKey("PublishID");

			Command command = new Command("mnAdmin", "dbo.up_Publish_Save", 0);
			command.AddParameter("@PublishID", SqlDbType.Int, ParameterDirection.Input, publishID);
			command.AddParameter("@PublishObjectID", SqlDbType.Int, ParameterDirection.Input, publishObjectID);
			command.AddParameter("@PublishObjectTypeID", SqlDbType.Int, ParameterDirection.Input, Convert.ToInt32(publishObjectType));

			if (content != null)
			{
				command.AddParameter("@Content", SqlDbType.VarBinary, ParameterDirection.Input, MiscUtils.BinarySerializeObject(content));
			}

			// Need to make this a synchronous write because we are seeing errors as a result of synchronicity problems
			// (A "save" occurs for an object write, then a call is made to this method.  If an attempt is made to "save"
			// the object again before hydra writes the original message, then an error will be thrown because there will
			// be 2 items in the queue for writing this one record).
			SyncWriter syncWriter = new SyncWriter();
			Exception swEx;
			syncWriter.Execute(command, out swEx);
			syncWriter.Dispose();
			if (swEx != null)
			{
				throw swEx;
			}

			return publishID;
		}

		public void SavePublishAction(Int32 publishID,
			Int32 publishActionPublishObjectID,
			Int32 memberID)
		{
			Command command = new Command("mnAdmin", "dbo.up_PublishAction_Save", 0);
			command.AddParameter("@PublishID", SqlDbType.Int, ParameterDirection.Input, publishID);
			command.AddParameter("@PublishActionPublishObjectID", SqlDbType.Int, ParameterDirection.Input, publishActionPublishObjectID);
			command.AddParameter("@MemberID", SqlDbType.Int, ParameterDirection.Input, memberID);
			
			// Need to make this a synchronous write because we are seeing errors as a result of synchronicity problems
			// (A "save" occurs for an object write, then a call is made to this method.  If an attempt is made to "save"
			// the object again before hydra writes the original message, then an error will be thrown because there will
			// be 2 items in the queue for writing this one record).
			SyncWriter syncWriter = new SyncWriter();
			Exception swEx;
			syncWriter.Execute(command, out swEx);
			syncWriter.Dispose();
			if (swEx != null)
			{
				throw swEx;
			}
		}

		public PublishActionPublishObjectCollection GetPublishActionPublishObjects()
		{
			PublishActionPublishObjectCollection pubActPubObjCol;
			try
			{
				pubActPubObjCol = _cache.Get(PublishActionPublishObjectCollection.CACHE_KEY_PREFIX) as PublishActionPublishObjectCollection;

				if (pubActPubObjCol == null)
				{
					pubActPubObjCol = new PublishActionPublishObjectCollection();

					Command command = new Command("mnAdmin", "up_PublishActionPublishObject_List", 0);
					DataTable dataTable = Client.Instance.ExecuteDataTable(command);

					foreach(DataRow row in dataTable.Rows)
					{
						pubActPubObjCol.Add(new PublishActionPublishObject(Convert.ToInt32(row["PublishActionPublishObjectID"])
							, ((PublishActionTypeEnum)Convert.ToInt32(row["PublishActionPublishObjectID"]))
							, ((PublishObjectType)Convert.ToInt32(row["PublishObjectTypeID"]))
							, Convert.ToInt32(row["WorkflowOrder"])
							, ((ServiceEnvironmentTypeEnum)(row["ServiceEnvironmentTypeID"]))
							));
					}

					_cache.Insert(pubActPubObjCol);
				}
			}
			catch (Exception ex)
			{
				throw(new BLException("Unable to get PublishActionPublishObject data.", ex));
			}
			
			return pubActPubObjCol;
		}

		/// <summary>
		/// </summary>
		public void GetNextPublishActionPublishObjectStep(Int32 publishObjectID, PublishObjectType publishObjectType, out Int32 publishActionPublishObjectID, out ServiceEnvironmentTypeEnum nextServiceEnvironmentTypeEnum)
		{
			Publish publish = GetPublish(publishObjectID, publishObjectType);
			
			// Find the entry with the highest WorkFlowOrder.
			Int32 workFlowOrderHolder = Constants.NULL_INT;
			IDictionaryEnumerator pubActsEnumerator = publish.PublishActions.GetEnumerator();
			while (pubActsEnumerator.MoveNext())
			{
				Int32 currentWorkFlowOrder = ((Int32)pubActsEnumerator.Key);
				if (currentWorkFlowOrder > workFlowOrderHolder)
					workFlowOrderHolder = currentWorkFlowOrder;
			}
			
			PublishActionPublishObjectCollection pubActPubObjs = GetPublishActionPublishObjects();

			// Iterate through the PublishActionPublishObjectCollection to find the next one.
			IDictionaryEnumerator pubActPubObjsEnumerator = pubActPubObjs.GetEnumerator();
			while (pubActPubObjsEnumerator.MoveNext())
			{
				PublishActionPublishObject pubActPubObj = ((PublishActionPublishObject)pubActPubObjsEnumerator.Value);
				if (pubActPubObj.PublishObjectType == publish.PublishObjectType && (pubActPubObj.WorkflowOrder == workFlowOrderHolder + 1))
				{
					publishActionPublishObjectID = pubActPubObj.PublishActionPublishObjectID;
					nextServiceEnvironmentTypeEnum = pubActPubObj.ServiceEnvironmentTypeEnum;

					return;
				}
			}

			publishActionPublishObjectID = Constants.NULL_INT;
			nextServiceEnvironmentTypeEnum = ServiceEnvironmentTypeEnum.Dev;
		}
		#endregion
	}
}
