using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Web.Caching;
using Matchnet.Caching;
using Matchnet.Data;
using Matchnet.Exceptions;
using Matchnet.Data.Hydra;
using Matchnet.Content.ValueObjects.LandingPage;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.CacheSynchronization.ValueObjects;

namespace Matchnet.Content.BusinessLogic
{
    /// <summary>
    /// Summary description for LandingPageBL.
    /// </summary>
    public class LandingPageBL
    {
        private const string LANDINGPAGE_KEY_NAME = "LandingPageID";
        private const string LANDINGPAGETEMPLATE_KEY_NAME = "LandingPageTemplateID";
        private const string LANDINGPAGE_CACHE_NAME = "LandingPages";
        private const string LANDINGPAGE_CACHE_LP_KEY = "AllLandingPages";
        private const string LANDINGPAGE_CACHE_TEMPLATES_KEY = "AllLandingPageTemplates";

        private Matchnet.Caching.Cache _cache;
        private String contentDatabaseName = "mnContent";



        #region singleton
        /// <summary>
        /// Singleton instance
        /// </summary
        public static readonly LandingPageBL Instance = new LandingPageBL();
        #endregion

        private LandingPageBL()
        {
            _cache = Matchnet.Caching.Cache.Instance;
            contentDatabaseName = Convert.ToString(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CONTENT_LOGICAL_DATABASE_TO_USE"));
        }

        public List<TemplateBasedLandingPage> GetLandingPages(Boolean forceLoadFlag)
        {
            List<TemplateBasedLandingPage> landingPages = null;
            bool retrievedFromCache = false;

            try
            {
                /*if (!forceLoadFlag)
                {
                    object cachedLandingPageTemplates = GetFromCache(LANDINGPAGE_CACHE_LP_KEY);
                    if (cachedLandingPageTemplates != null)
                    {
                        landingPages = (List<TemplateBasedLandingPage>)cachedLandingPageTemplates;
                        retrievedFromCache = true;
                    }
                }*/

                if (forceLoadFlag || !retrievedFromCache)
                {
                    landingPages = new List<TemplateBasedLandingPage>();
                    Command command = new Command(contentDatabaseName, "up_TemplateBasedLandingPage_ListAll", 0);
                    DataSet ds = Client.Instance.ExecuteDataSet(command);

                    DataTable lpTable = ds.Tables[0];
                    DataTable sites = ds.Tables[1];
                    lpTable.ChildRelations.Add(
                        "fk_LandingPage_Sites",
                        new DataColumn[] { lpTable.Columns["LandingPageID"] },
                        new DataColumn[] { sites.Columns["LandingPageID"] }
                        );

                    foreach (DataRow row in lpTable.Rows)
                    {
                        landingPages.Add(getTemplateBasedLandingPageFromDataRow(row));
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when retrieving LandingPages.", ex));
            }

            return landingPages;
        }

        public TemplateBasedLandingPage GetTemplateBasedLandingPage(int landingPageID)
        {
            TemplateBasedLandingPage landingPage = null;

            try
            {
                Command command = new Command(contentDatabaseName, "up_TemplateBasedLandingPage_List", 0);
                DataSet ds = Client.Instance.ExecuteDataSet(command);

                DataTable lpTable = ds.Tables[0];
                DataTable sites = ds.Tables[1];
                lpTable.ChildRelations.Add(
                    "fk_LandingPage_Sites",
                    new DataColumn[] { lpTable.Columns["LandingPageID"] },
                    new DataColumn[] { sites.Columns["LandingPageID"] }
                    );

                if (lpTable.Rows.Count == 1)
                {
                    landingPage = getTemplateBasedLandingPageFromDataRow(lpTable.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when retrieving LandingPage.", ex));
            }

            return landingPage;
        }

        public TemplateBasedLandingPage GetTemplateBasedLandingPageByURL(string url)
        {
            TemplateBasedLandingPage landingPage = null;

            try
            {
                Command command = new Command(contentDatabaseName, "up_TemplateBasedLandingPage_List_BY_URL", 0);
                command.AddParameter("@URL", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, url);
                DataSet ds = Client.Instance.ExecuteDataSet(command);

                DataTable lpTable = ds.Tables[0];
                DataTable sites = ds.Tables[1];
                lpTable.ChildRelations.Add(
                    "fk_LandingPage_Sites",
                    new DataColumn[] { lpTable.Columns["LandingPageID"] },
                    new DataColumn[] { sites.Columns["LandingPageID"] }
                    );

                if (lpTable.Rows.Count == 1)
                {
                    landingPage = getTemplateBasedLandingPageFromDataRow(lpTable.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when retrieving LandingPage By URL.", ex));
            }

            return landingPage;
        }

        public LandingPage GetLandingPage(int landingPageID)
        {
            LandingPage page = null;

            try
            {
                Command command = new Command(contentDatabaseName, "up_LandingPage_List", 0);
                command.AddParameter("@LandingPageID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, landingPageID);
                DataTable dt = Client.Instance.ExecuteDataTable(command);

                if (dt.Rows.Count == 1)
                {
                    page = dataRowToLandingPage(dt.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured when retrieving LandingPage.", ex));
            }

            return page;
        }

        public void SaveLandingPageTemplate(LandingPageTemplate template)
        {
            try
            {
                int landingPageTemplateID = template.LandingPageTemplateID;
                if (landingPageTemplateID <= 0)
                {
                    landingPageTemplateID = Matchnet.Configuration.ServiceAdapters.KeySA.Instance.GetKey(LANDINGPAGETEMPLATE_KEY_NAME);
                }

                Command command = new Matchnet.Data.Command(contentDatabaseName, "up_LandingPageTemplate_Save", 0);
                command.AddParameter("@LandingPageTemplateID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, landingPageTemplateID);
                command.AddParameter("@Name", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, template.Name);
                command.AddParameter("@Description", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, template.Description);
                command.AddParameter("@Definition", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, template.Definition);
                command.AddParameter("@UserDefinedFields", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, LandingPageTemplate.GetXMLStringFromUserDefinedFields(template.UserDefinedFields));
                Matchnet.Data.Client.Instance.ExecuteAsyncWrite(command);
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured in SaveLandingPageTemplate.", ex));
            }
        }

        public void SaveLandingPage(TemplateBasedLandingPage page)
        {
            try
            {

                int landingPageID = page.LandingPageID;
                if (landingPageID <= 0)
                {
                    landingPageID = Matchnet.Configuration.ServiceAdapters.KeySA.Instance.GetKey(LANDINGPAGE_KEY_NAME);
                }

                Command command = new Matchnet.Data.Command(contentDatabaseName, "up_TemplateBasedLandingPage_Save", 0);
                command.AddParameter("@LandingPageID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, landingPageID);
                command.AddParameter("@LandingPageTemplateID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, page.Template.LandingPageTemplateID);
                command.AddParameter("@Name", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, page.Name);
                command.AddParameter("@AnalyticsPageName", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, page.AnalyticsPageName);
                command.AddParameter("@Description", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, page.Description);
                command.AddParameter("@Tags", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, page.Tags);
                command.AddParameter("@URL", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, page.URL);
                command.AddParameter("@Published", System.Data.SqlDbType.Bit, System.Data.ParameterDirection.Input, page.Published);
                command.AddParameter("@UserDefinedValues", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, TemplateBasedLandingPage.GetXMLStringFromUserDefinedFields(page.UserDefinedFields));
                Matchnet.Data.Client.Instance.ExecuteAsyncWrite(command);

                int siteCount = 0;
                command = new Matchnet.Data.Command(contentDatabaseName, "up_TemplateBasedLandingPage_Sites_Save", 0);
                command.AddParameter("@LandingPageID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, landingPageID);

                foreach (int site in page.Sites)
                {
                    siteCount++;
                    command.AddParameter("@Site" + siteCount.ToString(), System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, site);
                  
                }
                Matchnet.Data.Client.Instance.ExecuteAsyncWrite(command);

            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured in SaveLandingPage.", ex));
            }
        }


        public void SaveUnifiedLandingPageIDForLandingPage(int landingPageID, int? unifiedLandingPageID)
        {
            try
            {

                if (landingPageID <= 0)
                {
                    landingPageID = Matchnet.Configuration.ServiceAdapters.KeySA.Instance.GetKey(LANDINGPAGE_KEY_NAME);
                }

                Command command = new Matchnet.Data.Command(contentDatabaseName, "up_TemplateBasedLandingPage_Save_UnifiedLandingPageID", 0);
                command.AddParameter("@LandingPageID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, landingPageID);
                command.AddParameter("@UnifiedLandingPageID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, unifiedLandingPageID);
                Matchnet.Data.Client.Instance.ExecuteAsyncWrite(command);


            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured in SaveUnifiedLandingPageIDForLandingPage.", ex));
            }
        }


        public List<LandingPageTemplate> GetLandingPageTemplates()
        {
            List<LandingPageTemplate> templates = null;

            try
            {
                /*object cachedLandingPageTemplates = GetFromCache(LANDINGPAGE_CACHE_TEMPLATES_KEY);
                if (cachedLandingPageTemplates != null)
                {
                    templates = (List<LandingPageTemplate>)cachedLandingPageTemplates;
                }
                else*/
                {
                    templates = new List<LandingPageTemplate>();
                    Command command = new Command(contentDatabaseName, "up_LandingPageTemplate_ListAll", 0);
                    DataTable dt = Client.Instance.ExecuteDataTable(command);

                    foreach (DataRow row in dt.Rows)
                    {
                        templates.Add(getLandingPageTemplateFromDataRow(row));
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured in GetLandingPageTemplates.", ex));
            }

            return templates;
        }

        public bool CheckNameAvailability(string name, int existingID, CheckAvailabilityType type)
        {
            bool available = false;

            try
            {
                Matchnet.Data.Command command = new Matchnet.Data.Command(contentDatabaseName, "up_LandingPage_Check_Name_Availibility", 0);
                command.AddParameter("@Name", System.Data.SqlDbType.NVarChar, System.Data.ParameterDirection.Input, name);
                command.AddParameter("@ExistingID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, existingID);
                command.AddParameter("@Type", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, (int)type);
                DataTable table = Client.Instance.ExecuteDataTable(command);
                if (table.Rows.Count > 0 && table.Rows[0]["ExistingCount"] != DBNull.Value)
                {
                    int existingCount = Convert.ToInt32(table.Rows[0]["ExistingCount"]);
                    if ((existingCount == 0)) available = true;
                }
            }
            catch (Exception ex)
            {
                throw (new BLException("BL error occured in CheckNameAvailibility.", ex));
            }

            return available;
        }

        public void DeleteLandingPage(int landingPageID)
        {
            Matchnet.Data.Command command = new Matchnet.Data.Command(contentDatabaseName, "up_TemplateBasedLandingPage_Delete", 0);
            command.AddParameter("@LandingPageID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, landingPageID);

            Matchnet.Data.Client.Instance.ExecuteAsyncWrite(command);
        }

        public void DeleteTemplate(int templateID)
        {
            Matchnet.Data.Command command = new Matchnet.Data.Command(contentDatabaseName, "up_LandingPageTemplate_Delete", 0);
            command.AddParameter("@LandingPageTemplateID", System.Data.SqlDbType.Int, System.Data.ParameterDirection.Input, templateID);

            Matchnet.Data.Client.Instance.ExecuteAsyncWrite(command);
        }

        public LandingPageCollection GetLandingPages(string clientHostName, CacheReference cacheReference, Boolean forceLoadFlag)
        {
            LandingPageCollection landingPageCollection = null;

            if (!forceLoadFlag)
                landingPageCollection = (LandingPageCollection)_cache.Get(LandingPageCollection.CACHE_KEY);

            if (landingPageCollection == null)
            {
                landingPageCollection = new LandingPageCollection();
                Command command = new Command(contentDatabaseName, "up_LandingPage_ListAll", 0);

                DataTable dt = Client.Instance.ExecuteDataTable(command);

                foreach (DataRow row in dt.Rows)
                {
                    int landingPageID = Conversion.CInt(row["LandingPageID"]);
                    string landingPageName = row["LandingPageName"].ToString();
                    string description = row["Description"].ToString();
                    string controlName = row["ControlName"].ToString();
                    string staticURL = row["StaticURL"].ToString();

                    bool isStaticLandingPage = true;
                    if (row["IsStaticLandingPage"] != DBNull.Value)
                        isStaticLandingPage = Convert.ToBoolean(row["IsStaticLandingPage"]);

                    int adminMemberID = Conversion.CInt(row["AdminMemberID"]);

                    bool isActive = true;
                    if (row["IsActive"] != DBNull.Value)
                        isActive = Convert.ToBoolean(row["IsActive"]);

                    DateTime updateDate = Convert.ToDateTime(row["UpdateDate"]);

                    int publishID = Constants.NULL_INT;
                    if (row["PublishID"] != DBNull.Value)
                    {
                        publishID = Convert.ToInt32(row["PublishID"]);
                    }

                    LandingPage landingPage;
                    if (publishID == Constants.NULL_INT)
                    {
                        landingPage = new LandingPage(landingPageID, landingPageName, description, controlName, staticURL, isStaticLandingPage, adminMemberID, isActive, updateDate);
                    }
                    else
                    {
                        landingPage = new LandingPage(landingPageID, landingPageName, description, controlName, staticURL, isStaticLandingPage, adminMemberID, isActive, updateDate, publishID);
                    }

                    landingPageCollection.Add(landingPage, landingPageID);
                }

                if ((!forceLoadFlag) && (landingPageCollection.Count > 0))
                {
                    CacheLandingPageCollection(landingPageCollection);
                }
            }

            if ((!forceLoadFlag) && (clientHostName != null) && (cacheReference != null))
            {
                landingPageCollection.ReferenceTracker.Add(clientHostName, cacheReference);
            }

            return landingPageCollection;
        }

        private void CacheLandingPageCollection(LandingPageCollection pLandingPageCollection)
        {
            pLandingPageCollection.CachePriority = CacheItemPriorityLevel.Normal;
            pLandingPageCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_LANDINGPAGE_SVC"));
            _cache.Insert(pLandingPageCollection);
        }

        private LandingPageTemplate getLandingPageTemplateFromDataRow(DataRow row)
        {
            LandingPageTemplate template;
            int templateID = Conversion.CInt(row["LandingPageTemplateID"]);
            string templateName = row["Name"].ToString();
            string templateDescription = row["Description"].ToString();
            string templateDefinition = row["Definition"].ToString();

            string userDefinedFields = string.Empty;
            if (row["UserDefinedFields"] != DBNull.Value) userDefinedFields = row["UserDefinedFields"].ToString();

            if (userDefinedFields == string.Empty)
            {
                template = new LandingPageTemplate(templateID, templateName, templateDescription, templateDefinition);
            }
            else
            {
                template = new LandingPageTemplate(templateID, templateName, templateDescription, templateDefinition, userDefinedFields);
            }

            return template;
        }

        private TemplateBasedLandingPage getTemplateBasedLandingPageFromDataRow(DataRow row)
        {
            int landingPageID = Conversion.CInt(row["LandingPageID"]);

            int? unifiedLandingPageID = null;
            if (row["UnifiedLandingPageID"] != DBNull.Value)
                unifiedLandingPageID = Conversion.CInt(row["UnifiedLandingPageID"]);

            int templateID = Conversion.CInt(row["LandingPageTemplateID"]);
            string landingPageName = row["Name"].ToString();
            string analyticsPageName = row["AnalyticsPageName"].ToString();
            string tags = row["Tags"].ToString();
            string url = row["URL"].ToString();
            string description = row["Description"].ToString();

            string templateName = row["LandingPageTemplateName"].ToString();
            string templateDescription = row["LandingPageTemplateDescription"].ToString();
            string templateDefinition = row["Definition"].ToString();

            bool published = true;
            if (row["Published"] != DBNull.Value) published = Convert.ToBoolean(row["Published"]);

            string userDefinedFields = string.Empty;
            if (row["UserDefinedFields"] != DBNull.Value) userDefinedFields = row["UserDefinedFields"].ToString();

            string userDefinedValues = string.Empty;
            if (row["UserDefinedValues"] != DBNull.Value) userDefinedValues = row["UserDefinedValues"].ToString();

            //get template
            LandingPageTemplate template;
            if (userDefinedFields == string.Empty)
            {
                template = new LandingPageTemplate(templateID, templateName, templateDescription, templateDefinition);
            }
            else
            {
                template = new LandingPageTemplate(templateID, templateName, templateDescription, templateDefinition, userDefinedFields);
            }

            //get siteids
            List<int> siteIDs = new List<int>();
            DataRow[] siteRows = row.GetChildRows("fk_LandingPage_Sites");
            foreach (DataRow siteRow in siteRows)
            {
                siteIDs.Add(Convert.ToInt32(siteRow["SiteID"]));
            }

            //get landing page
            TemplateBasedLandingPage landingPage;
            if (userDefinedValues == string.Empty)
            {
                if (unifiedLandingPageID.HasValue)
                    landingPage = new TemplateBasedLandingPage(landingPageID, unifiedLandingPageID, template, landingPageName, analyticsPageName, description, tags, url, published, siteIDs);
                else
                    landingPage = new TemplateBasedLandingPage(landingPageID, template, landingPageName, analyticsPageName, description, tags, url, published, siteIDs);
            }
            else
            {
                if (unifiedLandingPageID.HasValue)
                    landingPage = new TemplateBasedLandingPage(landingPageID, unifiedLandingPageID, template, landingPageName, analyticsPageName, description, tags, url, published, userDefinedValues, siteIDs);
                else
                    landingPage = new TemplateBasedLandingPage(landingPageID, unifiedLandingPageID, template, landingPageName, analyticsPageName, description, tags, url, published, userDefinedValues, siteIDs);
            }

            return landingPage;
        }

        private LandingPage dataRowToLandingPage(DataRow row)
        {
            int landingPageID = Conversion.CInt(row["LandingPageID"]);
            int adminMemberID = Conversion.CInt(row["AdminMemberID"]);
            int publishID = Conversion.CInt(row["PublishID"]);
            bool isStaticLandingPage = Conversion.CBool(row["IsStaticLandingPage"]);
            bool isActive = Conversion.CBool(row["IsActive"]);
            DateTime updateDate = Conversion.CDateTime(row["UpdateDate"]);
            string staticURL = row["StaticURL"].ToString();
            string landingPageName = row["LandingPageName"].ToString();
            string controlName = row["ControlName"].ToString();
            string description = row["Description"].ToString();

            return new LandingPage(landingPageID, landingPageName, description, controlName, staticURL, isStaticLandingPage, adminMemberID, isActive, updateDate);
        }

    }
}
