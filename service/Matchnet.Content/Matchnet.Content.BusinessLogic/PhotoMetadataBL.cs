﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.Photos;
using Matchnet.Data;
using Matchnet.Exceptions;

namespace Matchnet.Content.BusinessLogic
{
    public class PhotoMetadataBL
    {
        public readonly static PhotoMetadataBL Instance = new PhotoMetadataBL();

		private PhotoMetadataBL()
		{
			
		}

        public List<PhotoFileTypeRecord> GetPhotoFileTypeRecords()
        {
            var records = new List<PhotoFileTypeRecord>();
            SqlDataReader reader = null;

            try
            {
                var ordinalPhotoFileTypeID = Constants.NULL_INT;
                var ordinalDescription = Constants.NULL_INT;
                var ordinalMaxWidth = Constants.NULL_INT;
                var ordinalMaxHeight = Constants.NULL_INT;
                var ordinalActive = Constants.NULL_INT;
                var ordinalSiteID = Constants.NULL_INT;
                var colLookupDone = false;

                var command = new Command("mnSystem", "up_PhotoFileType_List", 0);
                reader = Client.Instance.ExecuteReader(command);

                while (reader.Read())
                {
                    if (!colLookupDone)
                    {
                        ordinalPhotoFileTypeID = reader.GetOrdinal("PhotoFileTypeID");
                        ordinalDescription = reader.GetOrdinal("Description");
                        ordinalMaxWidth = reader.GetOrdinal("MaxWidth");
                        ordinalMaxHeight = reader.GetOrdinal("MaxHeight");
                        ordinalActive = reader.GetOrdinal("Active");
                        colLookupDone = true;
                    }

                    var photoFileTypeID = reader.GetInt32(ordinalPhotoFileTypeID);
                    var description = reader.GetString(ordinalDescription);
                    var maxWidth =  reader.GetInt32(ordinalMaxWidth);
                    var maxHeight = reader.GetInt32(ordinalMaxHeight);
                    var active = reader.GetBoolean(ordinalActive);

                    var photoFileTypeRecord = new PhotoFileTypeRecord()
                                                  {
                                                      ID = photoFileTypeID,
                                                      Description = description,
                                                      MaxWidth = maxWidth,
                                                      MaxHeight = maxHeight,
                                                      Active = active
                                                  };
                    records.Add(photoFileTypeRecord);
                }

                reader.NextResult();
                colLookupDone = false;

                while (reader.Read())
                {
                    if (!colLookupDone)
                    {
                        ordinalPhotoFileTypeID = reader.GetOrdinal("PhotoFileTypeID");
                        ordinalSiteID = reader.GetOrdinal("SiteID");
                        ordinalMaxWidth = reader.GetOrdinal("MaxWidth");
                        ordinalMaxHeight = reader.GetOrdinal("MaxHeight");
                        ordinalActive = reader.GetOrdinal("Active");
                        colLookupDone = true;
                    }

                    var photoFileTypeID = reader.GetInt32(ordinalPhotoFileTypeID);
                    var siteID = reader.GetInt32(ordinalSiteID);
                    var maxWidth = reader.GetInt32(ordinalMaxWidth);
                    var maxHeight = reader.GetInt32(ordinalMaxHeight);
                    var active = reader.GetBoolean(ordinalActive);

                    var photoFileTypeRecord = (from pht in records where pht.ID == photoFileTypeID select pht).FirstOrDefault();
                    if(photoFileTypeRecord != null)
                    {
                        photoFileTypeRecord.SiteRecords.Add(new PhotoFileTypeSiteRecord { SiteId = siteID, MaxHeight = maxHeight, MaxWidth = maxWidth, Active = active });
                    }
                }
            }
            catch (Exception ex)
            {
                throw new BLException("Error in GetPhotoFileTypeRecords()", ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }

            return records;
        }
    }
}
