using System;
using System.Data;
using System.Collections;
using System.Web.Caching;

using Matchnet.Caching;
using Matchnet.Content.ValueObjects.PagePixel;
using Matchnet.Data;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Article;
using Matchnet.Content.ValueObjects.LandingPage;
using Matchnet.CacheSynchronization.ValueObjects;
using Matchnet.Data.Hydra;
using Matchnet.Exceptions;

using System.Xml;
using System.Text;
using System.IO;
using System.Net;

namespace Matchnet.Content.BusinessLogic
{
	/// <summary>
	/// Summary description for PagePixelBL.
	/// </summary>
	public class PagePixelBL
	{
		#region class variables
		Matchnet.Caching.Cache _cache = null;
		String contentDatabaseName = "mnContent";

		private CacheItemRemovedCallback expireCallback;
		public delegate void ReplicationEventHandler(IReplicable replicableObject);
		public event ReplicationEventHandler ReplicationRequested;
		public delegate void SynchronizationEventHandler(string key, Hashtable cacheReferences);
		public event SynchronizationEventHandler SynchronizationRequested;
		#endregion
		
		#region singleton
		/// <summary>
		/// Singleton instance
		/// </summary>
		public static readonly PagePixelBL Instance = new PagePixelBL();
		#endregion

		#region constructor
		private PagePixelBL()
		{
			_cache = Matchnet.Caching.Cache.Instance;
			expireCallback = new CacheItemRemovedCallback(this.ExpireCallback);
			contentDatabaseName = Convert.ToString(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CONTENT_LOGICAL_DATABASE_TO_USE"));
		}
		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clientHostName"></param>
		/// <param name="cacheReference"></param>
		/// <param name="pageID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		public PagePixelCollection RetrievePagePixels(string clientHostName, CacheReference cacheReference,
			int pageID, int siteID)
		{
			try
			{

				PagePixelCollection pagePixelCollection = null;
				PagePixel pixel = null;
				string cacheKey = PagePixelCollection.GetCacheKey(pageID, siteID);

				pagePixelCollection = _cache.Get(cacheKey) as PagePixelCollection;

				if (pagePixelCollection == null) 
				{
					pagePixelCollection = RetrievePixelsFromWebService(pageID, siteID);

					if (clientHostName != null && cacheReference != null)
					{
						pagePixelCollection.ReferenceTracker.Add(clientHostName, cacheReference);
						pagePixelCollection.SetCacheKey = cacheKey;
						CachePagePixel(pagePixelCollection);
					}
				}

				return(pagePixelCollection);
			}
			catch (System.Exception ex)
			{
				throw new BLException("Error RetrievePagePixels(). PageID:" + pageID.ToString()
					+ ", SiteID:" + siteID.ToString());
			}
		}

		private void ExpireCallback(string key, object value, CacheItemRemovedReason callbackreason) 
		{
			switch (value.GetType().Name)
			{
				case "PagePixelCollection":
					PagePixelCollection pagePixelCollection = value as PagePixelCollection;
					SynchronizationRequested(key,
						pagePixelCollection.ReferenceTracker.Purge(Constants.NULL_STRING));
					break;
			}
		}

		public void CacheReplicatedObject(IReplicable replicableObject)
		{
			switch (replicableObject.GetType().Name)
			{
				case "PagePixelCollection":
					CachePagePixel((PagePixelCollection)replicableObject);
					
					break;

				default:
					throw new Exception("invalid replication object type received (" + replicableObject.GetType().ToString() + ")");
			}
		}


		#region private Cache Routines
		private void CachePagePixel(PagePixelCollection pPagePixelCollection) 
		{
			pPagePixelCollection.CachePriority = CacheItemPriorityLevel.Normal;
			pPagePixelCollection.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_PAGE_PIXEL_SVC"));
			_cache.Insert(pPagePixelCollection, expireCallback);
		}
		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pageID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		private PagePixelCollection RetrievePixelsFromWebService(int pageID, int siteID)
		{
			PagePixelCollection pagePixelCollection = new PagePixelCollection(pageID, siteID);

            if (!Conversion.CBool(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PIXEL_WEBSERVICE_ENABLE")))
		        return pagePixelCollection;

			try
			{
				string url = buildRESTUrl(pageID, siteID);
				string response = GetHttpResponse(url);
					
				XmlDocument xmlDocument = new XmlDocument();
				xmlDocument.Load(new StringReader(response));
				
				// - <pagePixelsResponse>
				//		<result>1</result> 
				//		   - <pixels>
				//			  - <pixel>
				XmlNodeList xmlNodeList = xmlDocument.GetElementsByTagName("pixel");
				
				for (int i=0; i < xmlNodeList.Count; i++)
				{
					PagePixel pagePixel = new PagePixel();

					pagePixel.PageID = pageID;
					pagePixel.SiteID = siteID;

					//- <pixel>
					//	   <code>spark.net/bob/kdjlkj</code> 
					foreach (XmlNode childNode in xmlNodeList[i])
					{
						if (childNode.NodeType != XmlNodeType.Element)
							continue;

						if (childNode.Name == "code")
						{	
							// Code is stored as XML and not as text.
							pagePixel.Code = childNode.InnerXml;
						}
						
						if (childNode.Name == "targeting")
						{
							if (childNode.HasChildNodes)
							{
								foreach (XmlNode targetingNode in childNode.ChildNodes)
								{
									if (targetingNode.NodeType == XmlNodeType.Element)
									{
										bool hasChildElements = false;

										foreach (XmlNode unknownNode in targetingNode.ChildNodes)
										{
											if (unknownNode.NodeType == XmlNodeType.Element)
												hasChildElements = true;

										}

										if ((!hasChildElements) && (targetingNode.InnerText.Length != 0))
											System.Diagnostics.Debug.WriteLine(i + " targeting:" + targetingNode.Name + ":" + targetingNode.InnerText);
										else
										{
											foreach(XmlNode babyNode in targetingNode.ChildNodes)
											{
												if (babyNode.InnerText.Length != 0 && babyNode.NodeType == XmlNodeType.Element)
												{
													TargetingCondition targetingCondition = new TargetingCondition();

													targetingCondition.Name = babyNode.Name;
													targetingCondition.ConditionValue = babyNode.InnerText;
													targetingCondition.Comparison = babyNode.Attributes["filterid"].InnerText;

													pagePixel.TargetingConditionCollection.Add(targetingCondition);
												}
											}
										}
									}
								}	
							}
						}
					}

					pagePixelCollection.Add(pagePixel);
				}
					
				return pagePixelCollection;
			}
			catch (System.Exception ex)
			{
				throw new BLException("Error parsing pixels", ex);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pageID"></param>
		/// <param name="siteID"></param>
		/// <returns></returns>
		private string buildRESTUrl(int pageID, int siteID)
		{
			try
			{
				string url = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PAGE_PIXEL_API_URL");
				string key = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("PAGE_PIXEL_API_KEY");

				url += @"?version=1&auth=" + key;
				url += @"&pageid=" + pageID.ToString() + @"&siteid=" + siteID.ToString();

				return url;
			}
			catch (System.Exception ex)
			{
				throw new Exception("Error building REST url.", ex);
			}
		}

		private string GetHttpResponse(string url)
		{
			HttpWebRequest request;
			HttpWebResponse response;
			string output = String.Empty;

			try
			{
				request = (HttpWebRequest) WebRequest.Create(url);
				request.Timeout = 15000; // was initially 4 seconds but they take .. 10 seconds.. 
				request.UserAgent = "BH Page Pixel Client";

				response = (HttpWebResponse) request.GetResponse();

				StreamReader responseStream = new StreamReader(response.GetResponseStream(), Encoding.UTF8);

				output = responseStream.ReadToEnd();

				response.Close();
				responseStream.Close();

				if (output == String.Empty)
					throw new Exception("Response from REST reqeust was empty.");

				return output;
			}
			catch (System.Exception ex)
			{
				throw new Exception("Error requesting page pixel data from " + url, ex);
			}
		}

	}
}
