using System;
using System.Collections;
using System.Data;

using Matchnet.Data;
using Matchnet.Data.Exceptions;
using Matchnet.Exceptions;
using Matchnet.Content.ValueObjects.PageConfig;


namespace Matchnet.Content.BusinessLogic
{
	public class PageConfigBL
	{
		public readonly static PageConfigBL Instance = new PageConfigBL();

		private Matchnet.Caching.Cache _cache;

		private PageConfigBL()
		{
			_cache = Matchnet.Caching.Cache.Instance;
		}


		public SitePages GetSitePages()
		{
			SitePages sitePages = _cache.Get(SitePages.CACHE_KEY) as SitePages;

			DataTable dtApp;
			DataTable dtPage;
			DataTable dtSitePage;
			DataTable dtPageDefault;

			//TODO: also need a DataTable for dtDefaultPage, when an app gets created, it will have the
			// default (site page) object applied as an attribute.	K.Landrus 2/4/05

			if (sitePages == null)
			{
				sitePages = new SitePages();

				sitePages.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_SITEPAGE_SVC"));


				try
				{
					Command commandAttributes = new Command("mnSystem", "dbo.up_SitePage_List", 0);
					DataSet ds = Client.Instance.ExecuteDataSet(commandAttributes);

					dtApp = ds.Tables[0];
					dtPage = ds.Tables[1];
					dtSitePage = ds.Tables[2];
					dtPageDefault = ds.Tables[3];
				}
				catch(Exception ex)
				{
					throw(new BLException("BL error occured when retrieving site pages.", ex));
				}

				foreach (DataRow row in dtApp.Rows)
				{
					AppInternal app = new AppInternal(Convert.ToInt32(row["AppID"]),
						row["Path"].ToString(),
						row["ResourceConstant"].ToString(),
						row["Name"].ToString());

					sitePages.AddApp(app);
				}

				Hashtable pages = new Hashtable();
				foreach (DataRow row in dtPage.Rows)
				{
					PageInternal page = new PageInternal(Convert.ToInt32(row["PageID"]),		
						row["PageName"].ToString(),
						row["ResourceConstant"].ToString());

					sitePages.GetApp(Convert.ToInt32(row["AppID"])).AddPage(page);
					pages.Add(page.ID, page);
				}

				foreach (DataRow row in dtSitePage.Rows)
				{
					SitePage sitePage = new SitePage(Convert.ToInt32(row["SiteID"]),
						row["ControlName"].ToString(),
						(SecurityMask)Enum.Parse(typeof(SecurityMask), row["SecurityMask"].ToString()),
						row["LayoutTemplateName"].ToString());

					PageInternal page = pages[Convert.ToInt32(row["PageID"])] as PageInternal;
					page.AddSitePage(sitePage);
				}

				foreach (DataRow row in dtPageDefault.Rows)
				{
					if (row["PageID"] != DBNull.Value)
					{
						sitePages.SetPageDefault(Convert.ToInt32(row["AppID"]), Convert.ToInt32(row["PageID"]));
					}
				}

				_cache.Insert(sitePages);
			}

			return sitePages;
		}
	

		public AnalyticsPage GetAnalyticsPage(Int32 analyticsPageID)
		{
			AnalyticsPage analyticsPage = _cache.Get(AnalyticsPage.GetCacheKey(analyticsPageID)) as AnalyticsPage;

			if (analyticsPage == null)
			{
				analyticsPage = new AnalyticsPage(analyticsPageID);

				analyticsPage.CacheTTLSeconds = Convert.ToInt32(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("CONTENTSVC_CACHE_TTL_ANALYTICSPAGE_SVC"));

				try
				{
					Command command = new Command("mnSystem", "dbo.up_AnalyticsPage_List", 0);
					command.AddParameter("@AnalyticsPageID", SqlDbType.Int, ParameterDirection.Input, analyticsPageID);
					DataTable dt = Client.Instance.ExecuteDataTable(command);

					if (dt.Rows.Count > 0)
						analyticsPage.AnalyticsName = dt.Rows[0]["AnalyticsName"].ToString();
				}
				catch(Exception ex)
				{
					throw(new BLException("BL error occured when retrieving AnalyticsPage.", ex));
				}

				_cache.Insert(analyticsPage);
			}

			return analyticsPage;
		}
	}
}
