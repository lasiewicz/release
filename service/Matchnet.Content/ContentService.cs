using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using Matchnet.Exceptions;
using Matchnet.RemotingServices;
using Matchnet.Content.ServiceManagers;
using Matchnet.Content.ValueObjects;


namespace Matchnet.ContentService
{
	public class ContentService : Matchnet.RemotingServices.RemotingServiceBase
	{
		private System.ComponentModel.Container components = null;

		private AdminSM _adminSM;
		private ArticleSM _articleSM;
		private AttributeMetadataSM _attributeMetadataSM;
		private AttributeOptionSM _attributeOptionSM;
		private BrandConfigSM _brandConfigSM;
		private EventSM _eventSM;
		private PageConfigSM _pageConfigSM;
		private PromotionSM _promotionSM;
		private ImageSM _imageSM;
		private PagePixelSM _pagePixelSM;
		private RegionSM _regionSM;
		private LandingPageSM _landingPageSM;
		private QuotaDefinitionSM _quotaDefinitionSM;
	    private RegistrationMetadataSM _registrationMetadataSM;
        private PhotoMetadataSM _photoMetadataSM;
        private PushNotificationMetadataSM _pushNotificationMetadataSM;

		public ContentService()
		{
			InitializeComponent();
		}


		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new ContentService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}


		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = ServiceConstants.SERVICE_NAME;
			base.ServiceInstanceConfig = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetServiceInstanceConfig(ServiceConstants.SERVICE_CONSTANT, Matchnet.InitialConfiguration.InitializationSettings.MachineName);
		}


		protected override void RegisterServiceManagers()
		{
			try
			{
				_articleSM = new ArticleSM();
				base.RegisterServiceManager(_articleSM);

				_attributeMetadataSM = new AttributeMetadataSM();
				base.RegisterServiceManager(_attributeMetadataSM);

				_attributeOptionSM = new AttributeOptionSM();
				base.RegisterServiceManager(_attributeOptionSM);

				_brandConfigSM = new BrandConfigSM();
				base.RegisterServiceManager(_brandConfigSM);

				_eventSM = new EventSM();
				base.RegisterServiceManager(_eventSM);

				_pageConfigSM = new PageConfigSM();
				base.RegisterServiceManager(_pageConfigSM);

				_adminSM = new AdminSM();
				base.RegisterServiceManager(_adminSM);

				_promotionSM = new PromotionSM();
				base.RegisterServiceManager(_promotionSM);

				_imageSM = new ImageSM();
				base.RegisterServiceManager(_imageSM);

				_pagePixelSM = new PagePixelSM();
				base.RegisterServiceManager(_pagePixelSM);

				_regionSM = new RegionSM();
				base.RegisterServiceManager(_regionSM);

				_landingPageSM = new LandingPageSM();
				base.RegisterServiceManager(_landingPageSM);

				_quotaDefinitionSM = new QuotaDefinitionSM();
				base.RegisterServiceManager(_quotaDefinitionSM);

                _registrationMetadataSM = new RegistrationMetadataSM();
                base.RegisterServiceManager(_registrationMetadataSM);

                _photoMetadataSM = new PhotoMetadataSM();
                base.RegisterServiceManager(_photoMetadataSM);

                _pushNotificationMetadataSM = new PushNotificationMetadataSM();
                base.RegisterServiceManager(_pushNotificationMetadataSM);
			}
			catch(Exception ex)
			{
				throw new ServiceBoundaryException(ServiceConstants.SERVICE_NAME, "Error occurred when starting the Windows Service", ex);
			}
		}

	
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}

			if (_articleSM != null) 
			{
				_articleSM.Dispose();
			}

			if (_attributeMetadataSM != null) 
			{
				_attributeMetadataSM.Dispose();
			}

			if (_attributeOptionSM != null) 
			{
				_attributeOptionSM.Dispose();
			}

			if (_brandConfigSM != null)
			{
				_brandConfigSM.Dispose();
			}

			if (_eventSM != null)
			{
				_eventSM.Dispose();
			}

			if (_pageConfigSM != null)
			{
				_pageConfigSM.Dispose();
			}

			if (_adminSM != null) 
			{
				_adminSM.Dispose();
			}

			if (_promotionSM != null) 
			{
				_promotionSM.Dispose();
			}

			if (_imageSM != null) 
			{
				_imageSM.Dispose();
			}

			if (_pagePixelSM != null) 
			{
				_pagePixelSM.Dispose();
			}

			if (_regionSM != null) 
			{
				_regionSM.Dispose();
			}

			if (_landingPageSM != null)
			{
				_landingPageSM.Dispose();
			}

			if (_quotaDefinitionSM != null)
			{
				_quotaDefinitionSM.Dispose();
			}

            if (_registrationMetadataSM != null)
            {
                _registrationMetadataSM.Dispose();
            }


            if (_photoMetadataSM != null)
            {
                _photoMetadataSM.Dispose();
            }

            if (_pushNotificationMetadataSM != null)
            {
                _pushNotificationMetadataSM.Dispose();
            }

			base.Dispose( disposing );
		}
	}
}
