﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.MembersOnlineRefresher.ValueObjects
{
    public class ServiceConstants
    {
        public const string SERVICE_CONSTANT = "MEMBERSONLINEREFRESHER_SVC";
        public const string SERVICE_NAME = "Spark.MembersOnlineRefresher.Service";
        public const string SERVICE_MANAGER_NAME = "MembersOnlineRefresherSM";
    }
}
