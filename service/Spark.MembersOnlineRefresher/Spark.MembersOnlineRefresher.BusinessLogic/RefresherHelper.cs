﻿using Matchnet.Configuration.ServiceAdapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.MembersOnlineRefresher.BusinessLogic
{
    public static class RefresherHelper
    {
        public static int GetRefreshIntervalSeconds(int communityId)
        {
            try
            {
                return Convert.ToInt32(RuntimeSettings.Instance.GetSettingFromSingleton("MEMBERS_ONLINE_REFRESHER_INTERVAL_SECONDS", communityId));
            }
            catch (Exception ex)
            {
                //probably missing setting
                return 60;
            }
        }

        public static int GetRefreshLastLogonSeconds(int communityId)
        {
            try
            {
                return Convert.ToInt32(RuntimeSettings.Instance.GetSettingFromSingleton("MEMBERS_ONLINE_REFRESHER_LASTLOGON_SECONDS", communityId));
            }
            catch (Exception ex)
            {
                //probably missing setting
                return 120;
            }
        }

        public static int GetRefreshLastUpdateSeconds(int communityId)
        {
            try
            {
                return Convert.ToInt32(RuntimeSettings.Instance.GetSettingFromSingleton("MEMBERS_ONLINE_REFRESHER_LASTUPDATE_SECONDS", communityId));
            }
            catch (Exception ex)
            {
                //probably missing setting
                return 600;
            }
        }

        public static int GetRefreshPreviousLimitToPreventEmptySwap(int communityId)
        {
            try
            {
                return Convert.ToInt32(RuntimeSettings.Instance.GetSettingFromSingleton("MOL_REFRESHER_LIMIT_TO_PREVENT_EMPTY_SWAP", communityId));
            }
            catch (Exception ex)
            {
                //probably missing setting
                return 10;
            }
        }

        public static Matchnet.ICaching GetMOLCacheBucket()
        {
            return Spark.Caching.MembaseCaching.GetSingleton(RuntimeSettings.Instance.GetMembaseConfigByBucketFromSingleton("searchresults"));
        }

        public static string GetUtahApiAppId(int communityId)
        {
            try
            {
                return RuntimeSettings.Instance.GetSettingFromSingleton("UTAH_API_APPID", communityId);
            }
            catch (Exception ex)
            {
                //probably missing setting
                return "450758";
            }
        }

        public static string GetUtahApiClientSecret(int communityId)
        {
            try
            {
                return RuntimeSettings.Instance.GetSettingFromSingleton("UTAH_API_CLIENTSECRET", communityId);
            }
            catch (Exception ex)
            {
                //probably missing setting
                return "RvWytIEO6lZmX7hrp1LK25UM2e";
            }
        }

        public static string GetUtahApiBaseURL(int communityId)
        {
            try
            {
                return RuntimeSettings.Instance.GetSettingFromSingleton("UTAH_API_BASEURL", communityId);
            }
            catch (Exception ex)
            {
                //probably missing setting
                return "https://api.securemingle.com/v3";
            }
        }

        public static bool IsNoisyLoggingEnabled()
        {
            try
            {
                if (System.Configuration.ConfigurationManager.AppSettings["EnableNoisyLogging"].ToLower() == "true")
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                //probably missing setting
                return false;
            }
        }
    }
}
