﻿using log4net;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Matchnet.MembersOnline.ValueObjects;
using Newtonsoft.Json;
using RestSharp;
using Spark.SearchEngine.ServiceAdaptersNRT;
using Spark.SearchEngine.ValueObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Spark.MembersOnlineRefresher.BusinessLogic
{
    public class MembersOnlineRefresherUtah : IRefresher
    {        
        private static readonly ILog Log = LogManager.GetLogger(typeof(MembersOnlineRefresherUtah));
        private Thread _threadRefresh;
        private Matchnet.ICaching _cacheBucket;
        private MOLCommunity _MOLCommunity;
        private Matchnet.Content.ValueObjects.AttributeMetadata.Attributes _Attributes;
        private Brands _Brands;

        private bool _runnable { get; set; }
        public int CommunityId { get; private set; }
        

        public MembersOnlineRefresherUtah()
        {
            _cacheBucket = RefresherHelper.GetMOLCacheBucket();
            _Attributes = AttributeMetadataSA.Instance.GetAttributes();
            _Brands = BrandConfigSA.Instance.GetBrands();
        }

        public void Start(int communityId)
        {
            if (!_runnable)
            {
                Log.InfoFormat("Starting MembersOnlineRefresherUtah. Community:{0}", communityId);

                _runnable = true;
                CommunityId = communityId;
                _MOLCommunity = new MOLCommunity();
                _MOLCommunity.CommunityId = communityId;

                _threadRefresh = new Thread(() => RefreshCycle());
                _threadRefresh.Start();
            }

        }

        public void Stop()
        {
            Log.InfoFormat("Stopping MembersOnlineRefresherUtah. Community:{0}", CommunityId);
            _runnable = false;
        }

        private void RefreshCycle()
        {
            while (_runnable)
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                int nrtOnline = 0;
                int nrtOffline = 0;
                
                try
                {
                    Log.InfoFormat("Starting Utah refresh cycle. Community: {0}, Base URL:{1}", CommunityId, RefresherHelper.GetUtahApiBaseURL(CommunityId));

                    //call Utah API to get MOL list for specific community
                    var rawResponseContent = "";
                    try
                    {
                        var client = new RestClient(RefresherHelper.GetUtahApiBaseURL(CommunityId));
                        var request = new RestRequest("103/membersonline/getallonline/{appid}/{communityid}", Method.GET);
                        request.AddHeader("Content-Type", "application/json");
                        request.AddParameter("communityid", CommunityId, ParameterType.UrlSegment);
                        request.AddParameter("appid", RefresherHelper.GetUtahApiAppId(CommunityId), ParameterType.UrlSegment);
                        request.AddParameter("client_secret", RefresherHelper.GetUtahApiClientSecret(CommunityId), ParameterType.QueryString);

                        var response = client.Execute(request);
                        if (response == null || string.IsNullOrEmpty(response.Content))
                        {
                            Log.ErrorFormat("No response from Utah getallonline, Base URL: {0}, AppID: {1}, Client Secret: {2}", RefresherHelper.GetUtahApiBaseURL(CommunityId), RefresherHelper.GetUtahApiAppId(CommunityId), RefresherHelper.GetUtahApiClientSecret(CommunityId));
                        }
                        else
                        {
                            rawResponseContent = response.Content; // raw content as string
                            var utahGetAllOnlineResponse = JsonConvert.DeserializeObject<UtahGetAllOnlineResponse>(rawResponseContent);

                            if (utahGetAllOnlineResponse.status.ToLower() != "ok")
                            {
                                Log.ErrorFormat("Failed response from Utah getallonline, raw response: {0}", rawResponseContent);
                            }
                            else
                            {
                                //process new MOL list
                                MOLCommunity newMOLCommunity = new MOLCommunity();
                                newMOLCommunity.CommunityId = CommunityId;

                                bool replaceMOLCommunity = false;
                                bool isEmptyMOL = true;

                                if (utahGetAllOnlineResponse.data != null && utahGetAllOnlineResponse.data.Count > 0)
                                {
                                    //community
                                    foreach (UtahGetAllOnlineCommunity utahGetAllOnlineCommunity in utahGetAllOnlineResponse.data)
                                    {
                                        Log.InfoFormat("Utah response mol list community: {0}, count: {1}", utahGetAllOnlineCommunity.communityid, (utahGetAllOnlineCommunity.members == null) ? "null" : utahGetAllOnlineCommunity.members.Count.ToString());
                                        if (utahGetAllOnlineCommunity.communityid != CommunityId)
                                        {
                                            isEmptyMOL = false;
                                            replaceMOLCommunity = false;
                                            Log.WarnFormat("Unexpected community returned from Utah. Expected communityId: {0}, Received communityId: {1}", CommunityId, utahGetAllOnlineCommunity.communityid);
                                        }
                                        else if (utahGetAllOnlineCommunity.members != null && utahGetAllOnlineCommunity.members.Count > 0)
                                        {
                                            isEmptyMOL = false;
                                            replaceMOLCommunity = true;

                                            //online members
                                            foreach (UtahGetAllOnlineMember utahGetAllOnlineMember in utahGetAllOnlineCommunity.members)
                                            {
                                                try
                                                {
                                                    if (RefresherHelper.IsNoisyLoggingEnabled())
                                                    {
                                                        Log.InfoFormat("Processing community {0}, memberid: {1}, last_modified: {2}", utahGetAllOnlineCommunity.communityid, utahGetAllOnlineMember.memberid, utahGetAllOnlineMember.last_modified);
                                                    }

                                                    MOLMember newMolMember = new MOLMember();
                                                    newMolMember.CommunityId = CommunityId;
                                                    newMolMember.MemberId = utahGetAllOnlineMember.memberid;
                                                    newMolMember.LastUpdateDate = DateTime.Now;                                                    

                                                    //search (E2) NRT for online
                                                    UpdateReasonEnum updateReason = UpdateReasonEnum.none;
                                                    Member member = newMolMember.MemberId > 0 ? MemberSA.Instance.GetMember(newMolMember.MemberId) : null;
                                                    Brand brand = member != null ? getLastBrand(newMolMember.CommunityId, member) : null;

                                                    if (member == null)
                                                    {
                                                        Log.InfoFormat("Member not found. Community:{0}, Member:{1}", CommunityId, newMolMember.MemberId);
                                                    }
                                                    else if (brand == null)
                                                    {
                                                        Log.InfoFormat("Brand not found. Community:{0}, Member:{1}", CommunityId, newMolMember.MemberId);
                                                    }
                                                    else
                                                    {
                                                        newMolMember.LastBrandId = brand.BrandID;

                                                        MOLMember molMember = _MOLCommunity.GetMember(newMolMember.MemberId);
                                                        if (molMember == null)
                                                        {
                                                            //verify user did not log in recently, because login already triggers NRT
                                                            if (DateTime.Now.Subtract(member.GetAttributeDate(brand, "BrandLastLogonDate", DateTime.MinValue)).TotalSeconds > RefresherHelper.GetRefreshLastLogonSeconds(CommunityId))
                                                            {
                                                                updateReason = UpdateReasonEnum.online;
                                                            }
                                                        }
                                                        else if (newMolMember.LastUpdateDate.Subtract(molMember.LastUpdateDate).TotalSeconds > RefresherHelper.GetRefreshLastUpdateSeconds(CommunityId))
                                                        {
                                                            updateReason = UpdateReasonEnum.online;
                                                        }

                                                        if (updateReason == UpdateReasonEnum.online)
                                                        {
                                                            nrtOnline++;
                                                            try
                                                            {
                                                                SearchMemberUpdate smu = new SearchMemberUpdate();
                                                                smu.MemberID = newMolMember.MemberId;
                                                                smu.CommunityID = CommunityId;
                                                                smu.UpdateMode = UpdateModeEnum.update;
                                                                smu.UpdateReason = updateReason;
                                                                SearcherNRTSA.Instance.NRTUpdateMember(smu, brand.BrandID);
                                                                Log.InfoFormat("NRT ONline Update. Community:{0}, Member:{1}", CommunityId, newMolMember.MemberId);
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                Log.InfoFormat("Error calling E2 NRT Online update for memberID:{0}, communityID:{1}, update reason: {2}. Error: {3}", newMolMember.MemberId, CommunityId, updateReason, ex.ToString());
                                                            }
                                                        }

                                                        newMOLCommunity.AddMember(newMolMember);
                                                    }
                                                }
                                                catch (Exception exMember)
                                                {
                                                    Log.ErrorFormat("Error processing community {0}, memberid: {1}, last_modified: {2}, error: {3}", utahGetAllOnlineCommunity.communityid, utahGetAllOnlineMember.memberid, utahGetAllOnlineMember.last_modified, exMember.ToString());
                                                }

                                            }

                                            //offline members
                                            if (_MOLCommunity != null && _MOLCommunity.Count() > 0)
                                            {
                                                foreach (var member in _MOLCommunity)
                                                {
                                                    if (newMOLCommunity.GetMember(member.MemberId) == null)
                                                    {
                                                        nrtOffline++;
                                                        if (member.LastBrandId <= 0)
                                                        {
                                                            Log.InfoFormat("NRT OFFline update.  LastBrandId missing, bad cache from testing, this should never be true on prod. ");
                                                        }
                                                        else
                                                        {
                                                            try
                                                            {
                                                                SearchMemberUpdate smu = new SearchMemberUpdate();
                                                                smu.MemberID = member.MemberId;
                                                                smu.CommunityID = CommunityId;
                                                                smu.UpdateMode = UpdateModeEnum.update;
                                                                smu.UpdateReason = UpdateReasonEnum.offline;
                                                                SearcherNRTSA.Instance.NRTUpdateMember(smu, member.LastBrandId);
                                                                Log.InfoFormat("NRT OFFline Update. Community:{0}, Member:{1}", CommunityId, member.MemberId);
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                Log.InfoFormat("Error calling E2 NRT Offline update for memberID:{0}, communityID:{1}, update reason: {2}. Error: {3}", member.MemberId, CommunityId, UpdateReasonEnum.offline, ex.ToString());
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Log.InfoFormat("No NRT OFFline Updates. Community:{0}", CommunityId);
                                            }
                                        }
                                    }
                                }

                                if (isEmptyMOL)
                                {
                                    int previousMOLCount = (_MOLCommunity != null) ? _MOLCommunity.Count() : 0;

                                    int previousMOLLimitCheck = RefresherHelper.GetRefreshPreviousLimitToPreventEmptySwap(CommunityId);
                                    if (previousMOLCount > previousMOLLimitCheck)
                                    {
                                        Log.InfoFormat("Utah response mol list is empty, but previous is greater than {3} so likely Utah issue. Will keep existing mol list til next attempt. Community:{0}, Previous MOL Count: {2} Raw response: {1}", CommunityId, rawResponseContent, previousMOLCount, previousMOLLimitCheck);
                                    }
                                    else
                                    {
                                        replaceMOLCommunity = true;

                                        //offline members
                                        if (_MOLCommunity != null && _MOLCommunity.Count() > 0)
                                        {
                                            foreach (var member in _MOLCommunity)
                                            {
                                                nrtOffline++;
                                                try
                                                {
                                                    SearchMemberUpdate smu = new SearchMemberUpdate();
                                                    smu.MemberID = member.MemberId;
                                                    smu.CommunityID = CommunityId;
                                                    smu.UpdateMode = UpdateModeEnum.update;
                                                    smu.UpdateReason = UpdateReasonEnum.offline;
                                                    Log.InfoFormat("NRT OFFline Update. Community:{0}, Member:{1}", CommunityId, member.MemberId);
                                                    SearcherNRTSA.Instance.NRTUpdateMember(smu, member.LastBrandId);
                                                }
                                                catch (Exception ex)
                                                {
                                                    Log.InfoFormat("Error calling E2 NRT All Offline update for memberID:{0}, communityID:{1}, update reason: {2}. Error: {3}", member.MemberId, CommunityId, UpdateReasonEnum.offline, ex.ToString());
                                                }

                                            }
                                        }
                                        else
                                        {
                                            Log.InfoFormat("No NRT OFFline Updates. Community:{0}", CommunityId);
                                        }

                                        Log.InfoFormat("Utah response mol list is empty, but previous is less than {3} so assuming nobody is online. Community:{0}, Previous MOL Count: {2} Raw response: {1}", CommunityId, rawResponseContent, previousMOLCount, previousMOLLimitCheck);
                                    }
                                }

                                if (replaceMOLCommunity)
                                {
                                    newMOLCommunity.LastUpdatedDate = DateTime.Now;

                                    //keep copy of online members
                                    _MOLCommunity = newMOLCommunity;

                                    //save MOL list to cache, this will get picked up by MOL service to provide presence info
                                    Log.InfoFormat("Updating MOL Cache. Community: {0}, Count: {1}", newMOLCommunity.CommunityId, newMOLCommunity.Count());
                                    _cacheBucket.Insert(newMOLCommunity);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat("Error calling Utah getallonline. Community:{0}, Raw response: {1}", CommunityId, rawResponseContent);
                        throw;
                    }
                }
                catch (Exception ex)
                {
                    StringBuilder sbError = new StringBuilder();
                    sbError.Append(ex.ToString());
                    Exception innerException = ex.InnerException;
                    while (innerException != null)
                    {
                        sbError.Append(innerException.ToString());
                        innerException = innerException.InnerException;
                    }
                    Log.ErrorFormat("RefreshCycle error. Community:{0}, Error: {1}", CommunityId, sbError.ToString());
                }

                //pause for next cycle
                stopWatch.Stop();
                int pauseSeconds = RefresherHelper.GetRefreshIntervalSeconds(CommunityId);
                Log.InfoFormat("Ended Utah refresh cycle. Pause Seconds: {2}, Community: {0}, Time elapsed ms: {3}, NRT Online: {4}, NRT Offline: {5}, Base URL:{1}", CommunityId, RefresherHelper.GetUtahApiBaseURL(CommunityId), pauseSeconds, stopWatch.Elapsed.Milliseconds, nrtOnline, nrtOffline);
                Thread.Sleep(pauseSeconds * 1000);
            }
        }

        private Brand getLastBrand(Int32 communityID, Member member)
        {
            ArrayList communities = new ArrayList();
            DateTime lastLogonDate = DateTime.MinValue;
            Brand lastBrand = null;

            Int32 attributeID = _Attributes.GetAttribute("BrandLastLogonDate").ID;
            ArrayList attributeGroupIDs = _Attributes.GetAttributeGroupIDs(attributeID);

            for (Int32 num = 0; num < attributeGroupIDs.Count; num++)
            {
                Int32 attributeGroupID = (Int32)attributeGroupIDs[num];
                Brand brand = _Brands.GetBrand(_Attributes.GetAttributeGroup(attributeGroupID).GroupID);
                if (brand.Site.Community.CommunityID == communityID)
                {
                    DateTime currentDtm = member.GetAttributeDate(brand, "BrandLastLogonDate", DateTime.MinValue);
                    if (currentDtm > lastLogonDate)
                    {
                        lastLogonDate = currentDtm;
                        lastBrand = brand;
                    }
                }
            }

            return lastBrand;
        }
    }
}
