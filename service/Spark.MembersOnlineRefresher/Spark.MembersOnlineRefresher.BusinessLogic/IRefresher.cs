﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.MembersOnlineRefresher.BusinessLogic
{
    public interface IRefresher
    {
        int CommunityId { get; }
        void Start(int communityId);
        void Stop();

    }
}
