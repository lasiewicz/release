﻿using log4net;
using log4net.Config;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Newtonsoft.Json;
using RestSharp;
using Spark.MembersOnlineRefresher.BusinessLogic;
using Spark.SearchEngine.ServiceAdaptersNRT;
using Spark.SearchEngine.ValueObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Matchnet.MembersOnline.BusinessLogic
{
    public class MembersOnlineRefresherProcessor
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(MembersOnlineRefresherProcessor));
        private Dictionary<int, IRefresher> _refreshers = new Dictionary<int, IRefresher>();
        
        private bool _runnable = false;

        public MembersOnlineRefresherProcessor()
        {
            XmlConfigurator.Configure();

            //utah api uses ssl and may be self-signed
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true; 
        }

        public void Start()
        {
            if (!_runnable)
            {
                Log.DebugFormat("Starting MembersOnlineRefresherProcessor");
                _runnable = true;

                Hashtable communityList = BrandConfigSA.Instance.GetCommunities();
                foreach (Community community in communityList.Values)
                {
                    //Presence and NRT are currently tied together so there's no need to perform presence processing for communities that don't have an E2 searcher
                    if (!_refreshers.ContainsKey(community.CommunityID) && SearcherNRTSA.Instance.HasSearcher(community.CommunityID))
                    {
                        _refreshers[community.CommunityID] = GetMembersOnlineRefresher(community.CommunityID);
                        _refreshers[community.CommunityID].Start(community.CommunityID);
                    }
                }

            }
        }

        public void Stop()
        {
            if (_runnable)
            {
                Log.InfoFormat("Stopping MembersOnlineRefresherProcessor");
                _runnable = false;

                foreach (var key in _refreshers.Keys)
                {
                    _refreshers[key].Stop();
                }
            }
        }

        public IRefresher GetMembersOnlineRefresher(int communityId)
        {
            return new MembersOnlineRefresherUtah();
        }

        

    }
}
