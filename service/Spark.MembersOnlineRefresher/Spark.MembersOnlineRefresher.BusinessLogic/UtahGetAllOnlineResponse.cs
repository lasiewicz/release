﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.MembersOnlineRefresher.BusinessLogic
{
    public class UtahGetAllOnlineResponse
    {
        public int code { get; set; }
        public string status { get; set; }
        public List<UtahGetAllOnlineCommunity> data { get; set; }
        public UtahError error { get; set; }
    }

    public class UtahError
    {
        public int code { get; set; }
        public string message { get; set; }
    }

    public class UtahGetAllOnlineCommunity
    {
        public int communityid { get; set; }
        public List<UtahGetAllOnlineMember> members { get; set; }

    }

    public class UtahGetAllOnlineMember
    {
        public int memberid { get; set; }
        public DateTime last_modified { get; set; }
    }
}
