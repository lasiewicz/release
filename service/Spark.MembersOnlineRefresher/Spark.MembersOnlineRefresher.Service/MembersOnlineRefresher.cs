﻿using Matchnet.Exceptions;
using Spark.MembersOnlineRefresher.ServiceManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Spark.MembersOnlineRefresher.Service
{
    public partial class MembersOnlineRefresher : Matchnet.RemotingServices.RemotingServiceBase
    {
        private MembersOnlineRefresherSM _membersOnlineRefresherSM;

        public MembersOnlineRefresher()
        {
            _membersOnlineRefresherSM = new MembersOnlineRefresherSM();
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            _membersOnlineRefresherSM.Start();
        }

        protected override void OnStop()
        {
            base.OnStop();
            _membersOnlineRefresherSM.Stop();
        }

        protected override void RegisterServiceManagers()
        {
            RegisterServiceManager(_membersOnlineRefresherSM);
            base.RegisterServiceManagers();
        }

        public static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            new ServiceBoundaryException(Spark.MembersOnlineRefresher.ValueObjects.ServiceConstants.SERVICE_NAME,
                                         "An unhandled exception occured.", e.ExceptionObject as Exception);
        }
    }
}
