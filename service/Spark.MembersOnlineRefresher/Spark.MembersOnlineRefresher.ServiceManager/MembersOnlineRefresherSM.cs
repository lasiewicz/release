﻿using Matchnet;
using Matchnet.MembersOnline.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spark.MembersOnlineRefresher.ServiceManager
{
    public class MembersOnlineRefresherSM : MarshalByRefObject, IServiceManager, IBackgroundProcessor
    {
        private MembersOnlineRefresherProcessor _MembersOnlineRefresherProcessor;

        public MembersOnlineRefresherSM()
        {
            _MembersOnlineRefresherProcessor = new MembersOnlineRefresherProcessor();
        }

        public void PrePopulateCache()
        {

        }

        public void Dispose()
        {

        }

        public void Start()
        {
            _MembersOnlineRefresherProcessor.Start();
        }

        public void Stop()
        {
            _MembersOnlineRefresherProcessor.Stop();

        }

        public override object InitializeLifetimeService()
        {
            return null;
        }
    }
}
