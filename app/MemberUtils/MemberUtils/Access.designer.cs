﻿namespace MemberAccess
{
    partial class Access
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtURI = new System.Windows.Forms.TextBox();
            this.Acce = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMemberID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBrandID = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvAccess1 = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvAccess2 = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMemberRI = new System.Windows.Forms.TextBox();
            this.btnGetAccess = new System.Windows.Forms.Button();
            this.txtDuration = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnAddDuration = new System.Windows.Forms.Button();
            this.btnAddEmails = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtEmails = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtSubMonths = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnAddSubDuration = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtHighlightMonths = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAddHighlight = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtContentURI = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccess1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccess2)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtURI
            // 
            this.txtURI.Location = new System.Drawing.Point(135, 12);
            this.txtURI.Name = "txtURI";
            this.txtURI.Size = new System.Drawing.Size(541, 20);
            this.txtURI.TabIndex = 0;
            // 
            // Acce
            // 
            this.Acce.AutoSize = true;
            this.Acce.Location = new System.Drawing.Point(12, 15);
            this.Acce.Name = "Acce";
            this.Acce.Size = new System.Drawing.Size(103, 13);
            this.Acce.TabIndex = 1;
            this.Acce.Text = "Access Service URI";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Member ID";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtMemberID
            // 
            this.txtMemberID.Location = new System.Drawing.Point(135, 100);
            this.txtMemberID.Name = "txtMemberID";
            this.txtMemberID.Size = new System.Drawing.Size(128, 20);
            this.txtMemberID.TabIndex = 2;
            this.txtMemberID.TextChanged += new System.EventHandler(this.txtMemberID_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(297, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Brand ID";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtBrandID
            // 
            this.txtBrandID.Location = new System.Drawing.Point(420, 100);
            this.txtBrandID.Name = "txtBrandID";
            this.txtBrandID.Size = new System.Drawing.Size(128, 20);
            this.txtBrandID.TabIndex = 4;
            this.txtBrandID.TextChanged += new System.EventHandler(this.txtBrandID_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvAccess1);
            this.groupBox1.Location = new System.Drawing.Point(255, 158);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(863, 303);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cached Member Access";
            // 
            // dgvAccess1
            // 
            this.dgvAccess1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAccess1.Location = new System.Drawing.Point(6, 19);
            this.dgvAccess1.Name = "dgvAccess1";
            this.dgvAccess1.Size = new System.Drawing.Size(851, 260);
            this.dgvAccess1.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgvAccess2);
            this.groupBox2.Location = new System.Drawing.Point(255, 476);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(863, 303);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Access Service";
            // 
            // dgvAccess2
            // 
            this.dgvAccess2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAccess2.Location = new System.Drawing.Point(6, 24);
            this.dgvAccess2.Name = "dgvAccess2";
            this.dgvAccess2.Size = new System.Drawing.Size(851, 255);
            this.dgvAccess2.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Member Service URI";
            // 
            // txtMemberRI
            // 
            this.txtMemberRI.Location = new System.Drawing.Point(135, 38);
            this.txtMemberRI.Name = "txtMemberRI";
            this.txtMemberRI.Size = new System.Drawing.Size(541, 20);
            this.txtMemberRI.TabIndex = 8;
            // 
            // btnGetAccess
            // 
            this.btnGetAccess.Location = new System.Drawing.Point(729, 15);
            this.btnGetAccess.Name = "btnGetAccess";
            this.btnGetAccess.Size = new System.Drawing.Size(75, 23);
            this.btnGetAccess.TabIndex = 10;
            this.btnGetAccess.Text = "Get";
            this.btnGetAccess.UseVisualStyleBackColor = true;
            this.btnGetAccess.Click += new System.EventHandler(this.btnGetAccess_Click);
            // 
            // txtDuration
            // 
            this.txtDuration.Location = new System.Drawing.Point(6, 32);
            this.txtDuration.Name = "txtDuration";
            this.txtDuration.Size = new System.Drawing.Size(71, 20);
            this.txtDuration.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(88, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "month(s)";
            // 
            // btnAddDuration
            // 
            this.btnAddDuration.Location = new System.Drawing.Point(156, 30);
            this.btnAddDuration.Name = "btnAddDuration";
            this.btnAddDuration.Size = new System.Drawing.Size(93, 23);
            this.btnAddDuration.TabIndex = 14;
            this.btnAddDuration.Text = "Add Duration";
            this.btnAddDuration.UseVisualStyleBackColor = true;
            this.btnAddDuration.Click += new System.EventHandler(this.btnAddDuration_Click);
            // 
            // btnAddEmails
            // 
            this.btnAddEmails.Location = new System.Drawing.Point(156, 79);
            this.btnAddEmails.Name = "btnAddEmails";
            this.btnAddEmails.Size = new System.Drawing.Size(93, 23);
            this.btnAddEmails.TabIndex = 18;
            this.btnAddEmails.Text = "Add Emails";
            this.btnAddEmails.UseVisualStyleBackColor = true;
            this.btnAddEmails.Click += new System.EventHandler(this.btnAddEmails_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(88, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "email(s)";
            // 
            // txtEmails
            // 
            this.txtEmails.Location = new System.Drawing.Point(6, 79);
            this.txtEmails.Name = "txtEmails";
            this.txtEmails.Size = new System.Drawing.Size(71, 20);
            this.txtEmails.TabIndex = 15;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnAddEmails);
            this.groupBox3.Controls.Add(this.txtDuration);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txtEmails);
            this.groupBox3.Controls.Add(this.btnAddDuration);
            this.groupBox3.Location = new System.Drawing.Point(0, 476);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(255, 151);
            this.groupBox3.TabIndex = 19;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "AllAccess";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtSubMonths);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.btnAddSubDuration);
            this.groupBox4.Location = new System.Drawing.Point(0, 158);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(255, 83);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Basic Sub";
            // 
            // txtSubMonths
            // 
            this.txtSubMonths.Location = new System.Drawing.Point(6, 32);
            this.txtSubMonths.Name = "txtSubMonths";
            this.txtSubMonths.Size = new System.Drawing.Size(71, 20);
            this.txtSubMonths.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(88, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "month(s)";
            // 
            // btnAddSubDuration
            // 
            this.btnAddSubDuration.Location = new System.Drawing.Point(156, 30);
            this.btnAddSubDuration.Name = "btnAddSubDuration";
            this.btnAddSubDuration.Size = new System.Drawing.Size(93, 23);
            this.btnAddSubDuration.TabIndex = 14;
            this.btnAddSubDuration.Text = "Add Duration";
            this.btnAddSubDuration.UseVisualStyleBackColor = true;
            this.btnAddSubDuration.Click += new System.EventHandler(this.btnAddSubDuration_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtHighlightMonths);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.btnAddHighlight);
            this.groupBox5.Location = new System.Drawing.Point(0, 247);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(255, 83);
            this.groupBox5.TabIndex = 21;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Highlight";
            // 
            // txtHighlightMonths
            // 
            this.txtHighlightMonths.Location = new System.Drawing.Point(6, 32);
            this.txtHighlightMonths.Name = "txtHighlightMonths";
            this.txtHighlightMonths.Size = new System.Drawing.Size(71, 20);
            this.txtHighlightMonths.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(88, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "month(s)";
            // 
            // btnAddHighlight
            // 
            this.btnAddHighlight.Location = new System.Drawing.Point(156, 30);
            this.btnAddHighlight.Name = "btnAddHighlight";
            this.btnAddHighlight.Size = new System.Drawing.Size(93, 23);
            this.btnAddHighlight.TabIndex = 14;
            this.btnAddHighlight.Text = "Add Duration";
            this.btnAddHighlight.UseVisualStyleBackColor = true;
            this.btnAddHighlight.Click += new System.EventHandler(this.btnAddHighlight_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(22, 67);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "Content SVC URI";
            // 
            // txtContentURI
            // 
            this.txtContentURI.Location = new System.Drawing.Point(135, 64);
            this.txtContentURI.Name = "txtContentURI";
            this.txtContentURI.Size = new System.Drawing.Size(541, 20);
            this.txtContentURI.TabIndex = 25;
            // 
            // Access
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1130, 827);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtContentURI);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnGetAccess);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtMemberRI);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBrandID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtMemberID);
            this.Controls.Add(this.Acce);
            this.Controls.Add(this.txtURI);
            this.Name = "Access";
            this.Text = "MemberAccess";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccess1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccess2)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtURI;
        private System.Windows.Forms.Label Acce;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMemberID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBrandID;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvAccess1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgvAccess2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtMemberRI;
        private System.Windows.Forms.Button btnGetAccess;
        private System.Windows.Forms.TextBox txtDuration;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnAddDuration;
        private System.Windows.Forms.Button btnAddEmails;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtEmails;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtSubMonths;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnAddSubDuration;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtHighlightMonths;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnAddHighlight;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtContentURI;
    }
}

