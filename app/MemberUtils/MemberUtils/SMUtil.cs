using System;

using Matchnet;
using System.Collections;
using Matchnet.Exceptions;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ServiceDefinitions;
using  Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;
using Matchnet.CacheSynchronization.Context;
using Matchnet.CacheSynchronization.ValueObjects;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;

namespace MemberUtils
{
	internal class SMUtil
	{
		private SMUtil()
		{
		}


		internal static IMemberService getService(string uri)
		{
			try
			{
				return (IMemberService)Activator.GetObject(typeof(IMemberService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		
		internal static IMemberService getService(string uri, int memberID)
		{
			try
			{
				return (IMemberService)Activator.GetObject(typeof(IMemberService), uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}
        public static IAdapterConfigurationService getAdapterConfigurationService(string uri)
        {
            try
            {
                return (IAdapterConfigurationService)Activator.GetObject(typeof(IAdapterConfigurationService), uri);
            }
            catch (Exception ex)
            {
                throw (new SAException("Cannot activate remote service manager at " + uri, ex));
            }
        }

		public static string getServiceManagerUri()
		{
			try
			{
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MEMBERSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}

		
		public static string getServiceManagerUri(int memberID)
		{
			try
			{
                string uri = Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, Matchnet.Configuration.ServiceAdapters.PartitionMode.Calculated, memberID).ToUri(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME);
				string overrideHostName = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("MEMBERSVC_SA_HOST_OVERRIDE");

				if (overrideHostName.Length > 0)
				{
					UriBuilder uriBuilder = new UriBuilder(new Uri(uri));
					return "tcp://" + overrideHostName + ":" + uriBuilder.Port + uriBuilder.Path;
				}

				return uri;
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.",ex));
			}
		}


        public static Matchnet.Configuration.ValueObjects.ServicePartition GetServicePartition(string serviceConstant, PartitionMode partitionMode, int value, string uri)
        {
            if (partitionMode == PartitionMode.None)
            {
                throw (new Exception("PartitionMode.None cannot be selected if a partition offset value is passed to the ServiceAdapterConfiguration.GetServiceUri() method."));
            }

            ArrayList offsets = getServicePartitions(uri)[serviceConstant] as ArrayList;

            if (offsets == null)
            {
                throw new Exception("No service parititions found for " + serviceConstant + ".");
            }

            return offsets[value % offsets.Count] as Matchnet.Configuration.ValueObjects.ServicePartition;
        }
        private static Hashtable getServicePartitions(string uri)
        {
               Hashtable _services= refresh(uri);
         

            return _services;
        }
        private static Hashtable refresh(string uri)
        {
            
            Hashtable _services = null;
            Matchnet.Configuration.ValueObjects.ServicePartitions _servicePartitions = null;
            Matchnet.Configuration.ValueObjects.ServicePartitions servicePartitionsFugly = null;
            try
            {
              
                servicePartitionsFugly = getAdapterConfigurationService(uri).GetAll();

                if (servicePartitionsFugly.Count > 0)
                {
                    _servicePartitions = servicePartitionsFugly;
                    Hashtable services = new Hashtable();
                    ArrayList offsets = null;
                    string constantLast = null;

                    //this is fugly, converting OG structure to something that performs *much* better
                    for (Int32 spNum = 0; spNum < servicePartitionsFugly.Count; spNum++)
                    {
                        Matchnet.Configuration.ValueObjects.ServicePartition servicePartition = servicePartitionsFugly[spNum];

                        if (servicePartition.Constant != constantLast)
                        {
                            constantLast = servicePartition.Constant;
                            offsets = new ArrayList();
                            services[constantLast] = offsets;
                        }

                        offsets.Add(servicePartition);
                    }

                    _services = services;
                }
                return _services;
            }
            catch (Exception ex)
            {
                new SAException("Unable to load servicePartitions, reverting to last successful load.", ex);
                return _services;
            }
        }


      


	}
}
