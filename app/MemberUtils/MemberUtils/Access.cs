﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Spark.Common.AccessService;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.CacheSynchronization.Context;
using Matchnet.CacheSynchronization.ValueObjects;
namespace MemberAccess
{
    public partial class Access : Form
    {
        MemberUtils.AttributeUtils _attributes = new MemberUtils.AttributeUtils();
        public Access()
        {
            InitializeComponent();
            txtURI.Text = "http://bhd-tlam:6788/AccessService.svc";
            txtMemberRI.Text = "tcp://devapp01:42000/MemberSM.rem";
            txtContentURI.Text = "tcp://devapp01:41000/AttributeMetadataSM.rem";
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtMemberID_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtBrandID_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnGetAccess_Click(object sender, EventArgs e)
        {
            try
            {

                string uri = txtURI.Text;
                int memberid = Int32.Parse(txtMemberID.Text);
                int brandid = Int32.Parse(txtBrandID.Text);

                Brand brand = BrandConfigSA.Instance.GetBrandByID(brandid);

                List<AccessPrivilege> privileges = GetAccess(uri, memberid, brand);
                dgvAccess2.DataSource = privileges;


               CachedMemberAccess member = getCachedMemberAccess(memberid, txtMemberRI.Text, 2);
                List<AccessPrivilege> privilegesmember=new List<AccessPrivilege>();
                foreach(AccessPrivilege priv in privileges)
                {
                  AccessPrivilege p = member.GetAccessPrivilege(priv.UnifiedPrivilegeType,  brand.Site.SiteID);
                  privilegesmember.Add(p);  
                }

                dgvAccess1.DataSource = privilegesmember;
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
        }


        private Spark.Common.AccessService.AccessServiceClient GetAccessProxy(string uri)
        {
            Spark.Common.AccessService.AccessServiceClient proxy = null;
            Spark.Common.Adapter.AccessServiceAdapter sa = new Spark.Common.Adapter.AccessServiceAdapter();
            try
            {
                proxy = sa.GetProxyInstance(uri, "BasicHttpBinding_IAccessService");
                return proxy;
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }
        private List<AccessPrivilege> GetAccess(string uri, int memberid, Brand brand)
        {
            List<AccessPrivilege> privilleges=null;
            Spark.Common.AccessService.AccessServiceClient proxy=null;
            try
            {
                 proxy = GetAccessProxy(uri);

                AccessPrivilege[] privarray = proxy.GetCustomerPrivilegeList(memberid, brand.Site.SiteID);

                privilleges = privarray.ToList<AccessPrivilege>();
                return privilleges;
            }
            catch (Exception ex)
            { throw (ex); }
            finally
            {
                if(proxy != null)
                proxy.Close();
            }
        }
        private Matchnet.Member.ValueObjects.CachedMemberAccess getCachedMemberAccess(int memberid, string uri, int version)
        {


            try
            {
                 IMemberService svc = ((IMemberService)Activator.GetObject(typeof(IMemberService), uri));

              
                CachedMemberAccess memberaccess = svc.GetCachedMemberAccess(System.Environment.MachineName,
                    Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(300)), memberid, false);


                return memberaccess;
            }
            catch (Exception ex)
            {
                return null;

            }


        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnAddDuration_Click(object sender, EventArgs e)
        {
            try
            {

                string uri = txtURI.Text;
                int memberid = Int32.Parse(txtMemberID.Text);
                int brandid = Int32.Parse(txtBrandID.Text);
                int months=Int32.Parse(txtDuration.Text);
                Brand brand = BrandConfigSA.Instance.GetBrandByID(brandid);

                string result=GiveAccessForTesting(uri,memberid,brand.Site.SiteID,months);
                MessageBox.Show(result);

            }catch(Exception ex)
            {MessageBox.Show(ex.ToString());}
        }


        public  string GiveAccessForTesting(string uri, int memberid, int siteid, int months)
        {
            Spark.Common.AccessService.AccessServiceClient proxy = null;

            try
            {
                 proxy = GetAccessProxy(uri);
                // DurationTypeID 5 -> month, PrivilegeTypeID 6 -> All Access
                 AccessResponse result = proxy.AdjustTimePrivilege(memberid, new int[] { 6 }, months, 5, Int32.MinValue, null, Int32.MinValue, siteid, 1);

                string resultString = result.response;
                return resultString;
            }
            catch (Exception ex)
            { return ex.Message; }
            finally
            {
                if (proxy != null)
                {
                    proxy.Close();
                }
            }
        }


        public string GiveAccessCountForTesting(string uri, int memberid, int siteid, int count)
        {
            Spark.Common.AccessService.AccessServiceClient proxy = null;

            try
            {
                proxy = GetAccessProxy(uri);
                // DurationTypeID 5 -> month, PrivilegeTypeID 6 -> All Access
                AccessResponse result = proxy.AdjustCountPrivilege(memberid, new int[] { 7 }, count,  Int32.MinValue, null, Int32.MinValue, siteid, 1);

                string resultString = result.response;
                return resultString;
            }
            catch (Exception ex)
            { return ex.Message; }
            finally
            {
                if (proxy != null)
                {
                    proxy.Close();
                }
            }
        }

        private void btnAddEmails_Click(object sender, EventArgs e)
        {
            try
            {

                string uri = txtURI.Text;
                int memberid = Int32.Parse(txtMemberID.Text);
                int brandid = Int32.Parse(txtBrandID.Text);
                int count = Int32.Parse(txtEmails.Text);
                Brand brand = BrandConfigSA.Instance.GetBrandByID(brandid);

                string result = GiveAccessCountForTesting(uri, memberid, brand.Site.SiteID, count);
                MessageBox.Show(result);

            }
            catch (Exception ex)
            { MessageBox.Show(ex.ToString()); }
        }


        public string GiveDurationPrivillege(string uri, int memberid, int siteid,int privillegeid, int months)
        {
            Spark.Common.AccessService.AccessServiceClient proxy = null;

            try
            {
                proxy = GetAccessProxy(uri);
                // DurationTypeID 5 -> month, PrivilegeTypeID 6 -> All Access
                AccessResponse result = proxy.AdjustTimePrivilege(memberid, new int[] { privillegeid }, months, 5, Int32.MinValue, null, Int32.MinValue, siteid, 1);

                string resultString = result.response;
                return resultString;
            }
            catch (Exception ex)
            { return ex.Message; }
            finally
            {
                if (proxy != null)
                {
                    proxy.Close();
                }
            }
        }

        private void btnAddSubDuration_Click(object sender, EventArgs e)
        {
            try
            {

                string uri = txtURI.Text;
                int memberid = Int32.Parse(txtMemberID.Text);
                int brandid = Int32.Parse(txtBrandID.Text);
                int months = Int32.Parse(txtSubMonths.Text);
                Brand brand = BrandConfigSA.Instance.GetBrandByID(brandid);
                int privillegeid = 1;
                string result = GiveDurationPrivillege(uri, memberid, brand.Site.SiteID, privillegeid, months);
                MessageBox.Show(result);

            }
            catch (Exception ex)
            { MessageBox.Show(ex.ToString()); }
        }

        private void btnAddHighlight_Click(object sender, EventArgs e)
        {
            try
            {

                string uri = txtMemberRI.Text;

                string accessuri = txtURI.Text;
                int memberid = Int32.Parse(txtMemberID.Text);
                int brandid = Int32.Parse(txtBrandID.Text);
                int months = Int32.Parse(txtHighlightMonths.Text);
                Brand brand = BrandConfigSA.Instance.GetBrandByID(brandid);
                int privillegeid = 2;
                string result = GiveDurationPrivillege(accessuri, memberid, brand.Site.SiteID, privillegeid, months);
                MemberUtils.Member member = getMember(memberid, uri, 2);
                MemberUpdate mupdate = new MemberUpdate(memberid);
                
             
                member.SetAttributeInt(brand, "HighlightedFlag", 1);
                saveMember(member, uri);
                MessageBox.Show(result);
                

            }
            catch (Exception ex)
            { MessageBox.Show(ex.ToString()); }
        }


        public MemberUtils.Member getMember(int memberid, string uri, int version)
        {


            try
            {
                IMemberService svc = ((IMemberService)Activator.GetObject(typeof(IMemberService), uri));

                byte[] res1 = svc.GetCachedMemberBytes(System.Environment.MachineName,
                    Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(300)),
                    memberid,
                    false, version);

                CachedMember m = new CachedMember(res1, 1);
                MemberUtils.Member member = new MemberUtils.Member(m);
                member.attributes = _attributes.GetAttributes(txtContentURI.Text);
                member.AttributeUtils = _attributes;
                member.ContentURI = txtContentURI.Text;
                return member;
                
            }
            catch (Exception ex)
            {
                return null;

            }


        }


        public bool saveMember(MemberUtils.Member member, string uri)
        {


            try
            {
                IMemberService svc = ((IMemberService)Activator.GetObject(typeof(IMemberService), uri));
                svc.SaveMember("", member.MemberUpdate);
                return true;

            }
            catch (Exception ex)
            {
                return false;

            }


        }

    }
}
