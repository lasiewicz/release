﻿namespace MemberUtils
{
    partial class frmMemberAttributes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtURI1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtURI2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMemberID = new System.Windows.Forms.TextBox();
            this.txtSiteID = new System.Windows.Forms.TextBox();
            this.SITEID = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBrandID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCommunityID = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lblPartition = new System.Windows.Forms.DataGridView();
            this.dgvAttributesInt1 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgvAttributesText2 = new System.Windows.Forms.DataGridView();
            this.dgvAttributesText1 = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dgvAttributesDate2 = new System.Windows.Forms.DataGridView();
            this.dgvAttributesDate1 = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.dgvAttributesBool2 = new System.Windows.Forms.DataGridView();
            this.dgvAttributesBool1 = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dgvPhotos2 = new System.Windows.Forms.DataGridView();
            this.dgvPhotos1 = new System.Windows.Forms.DataGridView();
            this.btnGetMember1 = new System.Windows.Forms.Button();
            this.btnGetMember2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtLanguageID = new System.Windows.Forms.TextBox();
            this.cboConfigurations = new System.Windows.Forms.ComboBox();
            this.txtConfigurationURI = new System.Windows.Forms.TextBox();
            this.btnGetMemberPartition = new System.Windows.Forms.Button();
            this.btnBustCache1 = new System.Windows.Forms.Button();
            this.btnBustCache2 = new System.Windows.Forms.Button();
            this.lblDBPartition = new System.Windows.Forms.Label();
            this.txtContentURI = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.btnFindID = new System.Windows.Forms.Button();
            this.lblPartition1 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblPartition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttributesInt1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttributesText2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttributesText1)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttributesDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttributesDate1)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttributesBool2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttributesBool1)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPhotos2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPhotos1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtURI1
            // 
            this.txtURI1.Location = new System.Drawing.Point(117, 65);
            this.txtURI1.Name = "txtURI1";
            this.txtURI1.Size = new System.Drawing.Size(505, 20);
            this.txtURI1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Service URI 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Service URI 2";
            // 
            // txtURI2
            // 
            this.txtURI2.Location = new System.Drawing.Point(117, 91);
            this.txtURI2.Name = "txtURI2";
            this.txtURI2.Size = new System.Drawing.Size(505, 20);
            this.txtURI2.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "MemberID";
            // 
            // txtMemberID
            // 
            this.txtMemberID.Location = new System.Drawing.Point(117, 121);
            this.txtMemberID.Name = "txtMemberID";
            this.txtMemberID.Size = new System.Drawing.Size(127, 20);
            this.txtMemberID.TabIndex = 4;
            // 
            // txtSiteID
            // 
            this.txtSiteID.Location = new System.Drawing.Point(590, 121);
            this.txtSiteID.Name = "txtSiteID";
            this.txtSiteID.Size = new System.Drawing.Size(84, 20);
            this.txtSiteID.TabIndex = 6;
            // 
            // SITEID
            // 
            this.SITEID.AutoSize = true;
            this.SITEID.Location = new System.Drawing.Point(540, 124);
            this.SITEID.Name = "SITEID";
            this.SITEID.Size = new System.Drawing.Size(35, 13);
            this.SITEID.TabIndex = 7;
            this.SITEID.Text = "SiteiD";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(372, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "BrandID";
            // 
            // txtBrandID
            // 
            this.txtBrandID.Location = new System.Drawing.Point(424, 143);
            this.txtBrandID.Name = "txtBrandID";
            this.txtBrandID.Size = new System.Drawing.Size(85, 20);
            this.txtBrandID.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(335, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "CommunityID";
            // 
            // txtCommunityID
            // 
            this.txtCommunityID.Location = new System.Drawing.Point(424, 121);
            this.txtCommunityID.Name = "txtCommunityID";
            this.txtCommunityID.Size = new System.Drawing.Size(85, 20);
            this.txtCommunityID.TabIndex = 10;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 183);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1393, 554);
            this.tabControl1.TabIndex = 12;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lblPartition);
            this.tabPage1.Controls.Add(this.dgvAttributesInt1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1385, 528);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Attributes Int";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // lblPartition
            // 
            this.lblPartition.AllowUserToOrderColumns = true;
            this.lblPartition.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lblPartition.Location = new System.Drawing.Point(697, 26);
            this.lblPartition.Name = "lblPartition";
            this.lblPartition.Size = new System.Drawing.Size(651, 481);
            this.lblPartition.TabIndex = 1;
            // 
            // dgvAttributesInt1
            // 
            this.dgvAttributesInt1.AllowUserToOrderColumns = true;
            this.dgvAttributesInt1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAttributesInt1.Location = new System.Drawing.Point(15, 26);
            this.dgvAttributesInt1.Name = "dgvAttributesInt1";
            this.dgvAttributesInt1.Size = new System.Drawing.Size(643, 481);
            this.dgvAttributesInt1.TabIndex = 0;
            this.dgvAttributesInt1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAttributesInt1_CellContentClick);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgvAttributesText2);
            this.tabPage2.Controls.Add(this.dgvAttributesText1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1385, 528);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Attributes Text";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgvAttributesText2
            // 
            this.dgvAttributesText2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAttributesText2.Location = new System.Drawing.Point(683, 30);
            this.dgvAttributesText2.Name = "dgvAttributesText2";
            this.dgvAttributesText2.Size = new System.Drawing.Size(643, 476);
            this.dgvAttributesText2.TabIndex = 2;
            // 
            // dgvAttributesText1
            // 
            this.dgvAttributesText1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAttributesText1.Location = new System.Drawing.Point(15, 30);
            this.dgvAttributesText1.Name = "dgvAttributesText1";
            this.dgvAttributesText1.Size = new System.Drawing.Size(643, 476);
            this.dgvAttributesText1.TabIndex = 1;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dgvAttributesDate2);
            this.tabPage3.Controls.Add(this.dgvAttributesDate1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1385, 528);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Attributes Date";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dgvAttributesDate2
            // 
            this.dgvAttributesDate2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAttributesDate2.Location = new System.Drawing.Point(674, 23);
            this.dgvAttributesDate2.Name = "dgvAttributesDate2";
            this.dgvAttributesDate2.Size = new System.Drawing.Size(643, 476);
            this.dgvAttributesDate2.TabIndex = 3;
            // 
            // dgvAttributesDate1
            // 
            this.dgvAttributesDate1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAttributesDate1.Location = new System.Drawing.Point(25, 23);
            this.dgvAttributesDate1.Name = "dgvAttributesDate1";
            this.dgvAttributesDate1.Size = new System.Drawing.Size(643, 476);
            this.dgvAttributesDate1.TabIndex = 2;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.dgvAttributesBool2);
            this.tabPage5.Controls.Add(this.dgvAttributesBool1);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1385, 528);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Attributes Bool";
            this.tabPage5.UseVisualStyleBackColor = true;
            this.tabPage5.Click += new System.EventHandler(this.tabPage5_Click);
            // 
            // dgvAttributesBool2
            // 
            this.dgvAttributesBool2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAttributesBool2.Location = new System.Drawing.Point(699, 28);
            this.dgvAttributesBool2.Name = "dgvAttributesBool2";
            this.dgvAttributesBool2.Size = new System.Drawing.Size(643, 476);
            this.dgvAttributesBool2.TabIndex = 2;
            // 
            // dgvAttributesBool1
            // 
            this.dgvAttributesBool1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAttributesBool1.Location = new System.Drawing.Point(32, 28);
            this.dgvAttributesBool1.Name = "dgvAttributesBool1";
            this.dgvAttributesBool1.Size = new System.Drawing.Size(643, 476);
            this.dgvAttributesBool1.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dgvPhotos2);
            this.tabPage4.Controls.Add(this.dgvPhotos1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1385, 528);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Photos";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dgvPhotos2
            // 
            this.dgvPhotos2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPhotos2.Location = new System.Drawing.Point(687, 29);
            this.dgvPhotos2.Name = "dgvPhotos2";
            this.dgvPhotos2.Size = new System.Drawing.Size(643, 476);
            this.dgvPhotos2.TabIndex = 4;
            // 
            // dgvPhotos1
            // 
            this.dgvPhotos1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPhotos1.Location = new System.Drawing.Point(15, 29);
            this.dgvPhotos1.Name = "dgvPhotos1";
            this.dgvPhotos1.Size = new System.Drawing.Size(643, 476);
            this.dgvPhotos1.TabIndex = 3;
            // 
            // btnGetMember1
            // 
            this.btnGetMember1.Location = new System.Drawing.Point(639, 63);
            this.btnGetMember1.Name = "btnGetMember1";
            this.btnGetMember1.Size = new System.Drawing.Size(127, 23);
            this.btnGetMember1.TabIndex = 13;
            this.btnGetMember1.Text = "Get Member 1";
            this.btnGetMember1.UseVisualStyleBackColor = true;
            this.btnGetMember1.Click += new System.EventHandler(this.btnGetMember1_Click);
            // 
            // btnGetMember2
            // 
            this.btnGetMember2.Location = new System.Drawing.Point(639, 94);
            this.btnGetMember2.Name = "btnGetMember2";
            this.btnGetMember2.Size = new System.Drawing.Size(127, 23);
            this.btnGetMember2.TabIndex = 14;
            this.btnGetMember2.Text = "Get Member 2";
            this.btnGetMember2.UseVisualStyleBackColor = true;
            this.btnGetMember2.Click += new System.EventHandler(this.btnGetMember2_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(519, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Language ID";
            // 
            // txtLanguageID
            // 
            this.txtLanguageID.Location = new System.Drawing.Point(590, 146);
            this.txtLanguageID.Name = "txtLanguageID";
            this.txtLanguageID.Size = new System.Drawing.Size(114, 20);
            this.txtLanguageID.TabIndex = 15;
            // 
            // cboConfigurations
            // 
            this.cboConfigurations.FormattingEnabled = true;
            this.cboConfigurations.Location = new System.Drawing.Point(16, 12);
            this.cboConfigurations.Name = "cboConfigurations";
            this.cboConfigurations.Size = new System.Drawing.Size(104, 21);
            this.cboConfigurations.TabIndex = 17;
            this.cboConfigurations.SelectedIndexChanged += new System.EventHandler(this.cboConfigurations_SelectedIndexChanged);
            // 
            // txtConfigurationURI
            // 
            this.txtConfigurationURI.Location = new System.Drawing.Point(126, 12);
            this.txtConfigurationURI.Name = "txtConfigurationURI";
            this.txtConfigurationURI.Size = new System.Drawing.Size(496, 20);
            this.txtConfigurationURI.TabIndex = 18;
            // 
            // btnGetMemberPartition
            // 
            this.btnGetMemberPartition.Location = new System.Drawing.Point(639, 10);
            this.btnGetMemberPartition.Name = "btnGetMemberPartition";
            this.btnGetMemberPartition.Size = new System.Drawing.Size(127, 23);
            this.btnGetMemberPartition.TabIndex = 19;
            this.btnGetMemberPartition.Text = "Get Member Partition";
            this.btnGetMemberPartition.UseVisualStyleBackColor = true;
            this.btnGetMemberPartition.Click += new System.EventHandler(this.btnGetMemberPartition_Click);
            // 
            // btnBustCache1
            // 
            this.btnBustCache1.Location = new System.Drawing.Point(772, 65);
            this.btnBustCache1.Name = "btnBustCache1";
            this.btnBustCache1.Size = new System.Drawing.Size(94, 24);
            this.btnBustCache1.TabIndex = 20;
            this.btnBustCache1.Text = "Bust Cache";
            this.btnBustCache1.UseVisualStyleBackColor = true;
            this.btnBustCache1.Click += new System.EventHandler(this.btnBustCache1_Click);
            // 
            // btnBustCache2
            // 
            this.btnBustCache2.Location = new System.Drawing.Point(772, 94);
            this.btnBustCache2.Name = "btnBustCache2";
            this.btnBustCache2.Size = new System.Drawing.Size(94, 23);
            this.btnBustCache2.TabIndex = 21;
            this.btnBustCache2.Text = "Bust Cache";
            this.btnBustCache2.UseVisualStyleBackColor = true;
            this.btnBustCache2.Click += new System.EventHandler(this.btnBustCache2_Click);
            // 
            // lblDBPartition
            // 
            this.lblDBPartition.AutoSize = true;
            this.lblDBPartition.Location = new System.Drawing.Point(114, 153);
            this.lblDBPartition.Name = "lblDBPartition";
            this.lblDBPartition.Size = new System.Drawing.Size(0, 13);
            this.lblDBPartition.TabIndex = 22;
            // 
            // txtContentURI
            // 
            this.txtContentURI.Location = new System.Drawing.Point(126, 39);
            this.txtContentURI.Name = "txtContentURI";
            this.txtContentURI.Size = new System.Drawing.Size(496, 20);
            this.txtContentURI.TabIndex = 23;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 13);
            this.label7.TabIndex = 24;
            this.label7.Text = "Content SVC URI";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(28, 164);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "Email";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(117, 161);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(164, 20);
            this.txtEmail.TabIndex = 25;
            // 
            // btnFindID
            // 
            this.btnFindID.Location = new System.Drawing.Point(291, 159);
            this.btnFindID.Name = "btnFindID";
            this.btnFindID.Size = new System.Drawing.Size(94, 23);
            this.btnFindID.TabIndex = 27;
            this.btnFindID.Text = "Find Member ID";
            this.btnFindID.UseVisualStyleBackColor = true;
            this.btnFindID.Click += new System.EventHandler(this.btnFindID_Click);
            // 
            // lblPartition1
            // 
            this.lblPartition1.AutoSize = true;
            this.lblPartition1.Location = new System.Drawing.Point(421, 168);
            this.lblPartition1.Name = "lblPartition1";
            this.lblPartition1.Size = new System.Drawing.Size(35, 13);
            this.lblPartition1.TabIndex = 28;
            this.lblPartition1.Text = "label9";
            // 
            // frmMemberAttributes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1417, 760);
            this.Controls.Add(this.lblPartition1);
            this.Controls.Add(this.btnFindID);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtContentURI);
            this.Controls.Add(this.lblDBPartition);
            this.Controls.Add(this.btnBustCache2);
            this.Controls.Add(this.btnBustCache1);
            this.Controls.Add(this.btnGetMemberPartition);
            this.Controls.Add(this.txtConfigurationURI);
            this.Controls.Add(this.cboConfigurations);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtLanguageID);
            this.Controls.Add(this.btnGetMember2);
            this.Controls.Add(this.btnGetMember1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCommunityID);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtBrandID);
            this.Controls.Add(this.SITEID);
            this.Controls.Add(this.txtSiteID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtMemberID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtURI2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtURI1);
            this.Name = "frmMemberAttributes";
            this.Text = "frmMemberAttributes";
            this.Load += new System.EventHandler(this.frmMemberAttributes_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblPartition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttributesInt1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttributesText2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttributesText1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttributesDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttributesDate1)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttributesBool2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttributesBool1)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPhotos2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPhotos1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtURI1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtURI2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtMemberID;
        private System.Windows.Forms.TextBox txtSiteID;
        private System.Windows.Forms.Label SITEID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBrandID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCommunityID;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dgvAttributesInt1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView lblPartition;
        private System.Windows.Forms.Button btnGetMember1;
        private System.Windows.Forms.Button btnGetMember2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtLanguageID;
        private System.Windows.Forms.DataGridView dgvAttributesText2;
        private System.Windows.Forms.DataGridView dgvAttributesText1;
        private System.Windows.Forms.DataGridView dgvAttributesDate2;
        private System.Windows.Forms.DataGridView dgvAttributesDate1;
        private System.Windows.Forms.DataGridView dgvPhotos2;
        private System.Windows.Forms.DataGridView dgvPhotos1;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView dgvAttributesBool2;
        private System.Windows.Forms.DataGridView dgvAttributesBool1;
        private System.Windows.Forms.ComboBox cboConfigurations;
        private System.Windows.Forms.TextBox txtConfigurationURI;
        private System.Windows.Forms.Button btnGetMemberPartition;
        private System.Windows.Forms.Button btnBustCache1;
        private System.Windows.Forms.Button btnBustCache2;
        private System.Windows.Forms.Label lblDBPartition;
        private System.Windows.Forms.TextBox txtContentURI;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Button btnFindID;
        private System.Windows.Forms.Label lblPartition1;
    }
}