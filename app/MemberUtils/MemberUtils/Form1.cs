﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MemberUtils
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnMemberAttributes_Click(object sender, EventArgs e)
        {
            frmMemberAttributes attr = new frmMemberAttributes();
            attr.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MemberAccess.Access frmAccess = new MemberAccess.Access();
            frmAccess.Show();
        }
    }
}
