﻿namespace MemberUtils
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMemberAttributes = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnMemberAttributes
            // 
            this.btnMemberAttributes.Location = new System.Drawing.Point(30, 29);
            this.btnMemberAttributes.Name = "btnMemberAttributes";
            this.btnMemberAttributes.Size = new System.Drawing.Size(226, 41);
            this.btnMemberAttributes.TabIndex = 0;
            this.btnMemberAttributes.Text = "Member Attributes";
            this.btnMemberAttributes.UseMnemonic = false;
            this.btnMemberAttributes.UseVisualStyleBackColor = true;
            this.btnMemberAttributes.Click += new System.EventHandler(this.btnMemberAttributes_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(30, 99);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(226, 41);
            this.button1.TabIndex = 1;
            this.button1.Text = "Member Access";
            this.button1.UseMnemonic = false;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnMemberAttributes);
            this.Name = "Form1";
            this.Text = "Main";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnMemberAttributes;
        private System.Windows.Forms.Button button1;
    }
}

