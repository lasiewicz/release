﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.ServiceDefinitions;

namespace MemberUtils
{
    public class MemberAttribute<T>
    {

   
        public string AttributeName
        { get; set; }
        public string AttributeID
        { get; set; }
        public string AttributeGroupID
        { get; set; }
        public string GroupID
        { get; set; }
        public T AttributeValue
        { get; set; }



    }

    public class AttributeUtils
    {
        public Attributes attributes;
        public string URI = "";
        public Attributes GetAttributes(string uri)
        {
           
            try
            {
                if (uri == URI && attributes != null)
                    return attributes;

                if (attributes == null)
                {
                    attributes = getService(uri).GetAttributes();
                }
                URI = uri;
                return attributes;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        private IAttributeMetadataService getService(string uri)
        {
            try
            {
                return (IAttributeMetadataService)Activator.GetObject(typeof(IAttributeMetadataService), uri);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

    }
}
