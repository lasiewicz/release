﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ServiceDefinitions;
using Matchnet.Member.ValueObjects;
using Matchnet.CacheSynchronization.Context;
using Matchnet.CacheSynchronization.ValueObjects;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using System.Configuration;

namespace MemberUtils
{
    public partial class frmMemberAttributes : Form
    {
        AttributeUtils _attributes = new AttributeUtils();
        public frmMemberAttributes()
        {
            InitializeComponent();
            txtURI1.Text = "tcp://lasvcmember01:42000/MemberSM.rem";
            txtURI2.Text = "tcp://lasvcmember02:42000/MemberSM.rem";
            txtSiteID.Text = "103";
            txtCommunityID.Text = "3";
            txtBrandID.Text = "1003";
            txtLanguageID.Text = "2";
        }

        private void dgvAttributesInt1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
          
        }

        private void frmMemberAttributes_Load(object sender, EventArgs e)
        {
            try
            {
                ConnectionStringSettingsCollection connections = ConfigurationManager.ConnectionStrings;
                List<string> connectionkeys=new List<string>();
                foreach (ConnectionStringSettings c in connections)
                {
                    if (c.Name.IndexOf("Local") < 0 && c.Name.IndexOf("content") < 0)
                    connectionkeys.Add(c.Name);
                }
                cboConfigurations.DataSource = connectionkeys;

            }
            catch (Exception ex)
            { MessageBox.Show(ex.ToString()); }
        }

     

        private void btnGetMember1_Click(object sender, EventArgs e)
        {
            try
            {

                int mid = Int32.Parse(txtMemberID.Text);
               Member m1 = getMember(mid, txtURI1.Text, 1);

                dgvAttributesInt1.DataSource = GetAttributeIntList(m1);

                dgvAttributesText1.DataSource = GetAttributeTextList(m1);


                dgvAttributesDate1.DataSource = GetAttributeDateList(m1);

                dgvAttributesBool1.DataSource = GetAttributeBitList(m1);
                dgvPhotos1.DataSource = GetPhotos(m1);

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }
        }


        private List<MemberAttribute<int>> GetAttributeIntList(Member m)
        {
            try
            {
                List<MemberAttribute<int>> attributes = new List<MemberAttribute<int>>();

                int siteid=Int32.Parse(txtSiteID.Text);
                int communityid=Int32.Parse(txtCommunityID.Text);
                int brandid=Int32.Parse(txtBrandID.Text);

                for (int i = 0; i < Utils.AttributeIntName.Length; i++)
                {
                    try
                    {
                        MemberAttribute<int> a = new MemberAttribute<int>();

                        a.AttributeName = Utils.AttributeIntName[i];
                        a.AttributeValue = m.GetAttributeInt(communityid, siteid, brandid, a.AttributeName, Int32.MinValue);
                        populateAttribute<int>(a,txtContentURI.Text);
                        if( a.AttributeValue >  Int32.MinValue)
                            attributes.Add(a);
                    }
                    catch (Exception ex) { }

                }

                return attributes;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
                return null;
            }

        }


        private List<MemberAttribute<string>> GetAttributeTextList(Member m)
        {
            try
            {
                List<MemberAttribute<string>> attributes = new List<MemberAttribute<string>>();

                int siteid = Int32.Parse(txtSiteID.Text);
                int communityid = Int32.Parse(txtCommunityID.Text);
                int brandid = Int32.Parse(txtBrandID.Text);
                int languageid = Int32.Parse(txtLanguageID.Text);
                for (int i = 0; i < Utils.AttributeTextName.Length; i++)
                {
                    try
                    {
                        MemberAttribute<string> a = new MemberAttribute<string>();
                        a.AttributeName = Utils.AttributeTextName[i];
                        a.AttributeValue = m.GetAttributeText(communityid, siteid, brandid, languageid, a.AttributeName, "null");
                        
                        populateAttribute<string>(a, txtContentURI.Text);
                        if(a.AttributeValue != "null")
                         attributes.Add(a);
                    }
                    catch (Exception ex) { }

                }

                return attributes;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
                return null;
            }

        }


        private List<MemberAttribute<DateTime>> GetAttributeDateList(Member m)
        {
            try
            {
                List<MemberAttribute<DateTime>> attributes = new List<MemberAttribute<DateTime>>();

                int siteid = Int32.Parse(txtSiteID.Text);
                int communityid = Int32.Parse(txtCommunityID.Text);
                int brandid = Int32.Parse(txtBrandID.Text);

                for (int i = 0; i < Utils.AttributeDateName.Length; i++)
                {
                    try
                    {
                        MemberAttribute<DateTime> a = new MemberAttribute<DateTime>();
                        a.AttributeName = Utils.AttributeDateName[i];
                        a.AttributeValue = m.GetAttributeDate(communityid, siteid, brandid, a.AttributeName, DateTime.MinValue);
                        populateAttribute<DateTime>(a, txtContentURI.Text);
                        if(a.AttributeValue > DateTime.MinValue)
                            attributes.Add(a);
                    }
                    catch (Exception ex) { }

                }

                return attributes;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
                return null;
            }

        }



        private List<MemberAttribute<bool>> GetAttributeBitList(Member m)
        {
            try
            {
                List<MemberAttribute<bool>> attributes = new List<MemberAttribute<bool>>();

                int siteid = Int32.Parse(txtSiteID.Text);
                int communityid = Int32.Parse(txtCommunityID.Text);
                int brandid = Int32.Parse(txtBrandID.Text);

                for (int i = 0; i < Utils.AttributeBitName.Length; i++)
                {
                    try
                    {
                        MemberAttribute<bool> a = new MemberAttribute<bool>();
                        a.AttributeName = Utils.AttributeBitName[i];
                        a.AttributeValue = m.GetAttributeBool(communityid, siteid, brandid, a.AttributeName);
                        populateAttribute<bool>(a, txtContentURI.Text);
                        attributes.Add(a);
                    }
                    catch (Exception ex)
                    { }
                }

                return attributes;

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
                return null;
            }

        }

        private void btnGetMember2_Click(object sender, EventArgs e)
        {
            try
            {

                int mid = Int32.Parse(txtMemberID.Text);
               Member m1 = getMember(mid, txtURI2.Text, 1);

                lblPartition.DataSource = GetAttributeIntList(m1);

                dgvAttributesText2.DataSource = GetAttributeTextList(m1);


                dgvAttributesDate2.DataSource = GetAttributeDateList(m1);

                dgvAttributesBool2.DataSource = GetAttributeBitList(m1);
                dgvPhotos2.DataSource = GetPhotos(m1);


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }
        }

        private ArrayList GetPhotos(Member m)
        {
            try
            {
                int communityid = Int32.Parse(txtCommunityID.Text);
                Matchnet.Member.ValueObjects.Photos.PhotoCommunity photoCommunity = m.GetPhotos(communityid);
                ArrayList photos1 = new ArrayList();
                for (byte i = 0; i < photoCommunity.Count; i++)
                {
                    photos1.Add(photoCommunity[i]);
                }
                return photos1;
            }
            catch (Exception ex)
            { MessageBox.Show(ex.ToString());
            return null;
            }

        }

        private void tabPage5_Click(object sender, EventArgs e)
        {

        }

        private void populateAttribute<T>(MemberAttribute<T> attr, string uri)
        {
            try
            {  int siteid = Int32.Parse(txtSiteID.Text);
                int communityid = Int32.Parse(txtCommunityID.Text);
                int brandid = Int32.Parse(txtBrandID.Text);
                
                 Matchnet.Content.ValueObjects.AttributeMetadata.Attributes attrs= AttributeMetadataSA.Instance.GetAttributes();
                 Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute=  attrs.GetAttribute(attr.AttributeName);
                 Matchnet.Content.ValueObjects.AttributeMetadata.AttributeGroup group = null;
                 switch (attribute.Scope)
                 {case ScopeType.Personals:
                         group = _attributes.GetAttributes(uri).GetAttributeGroup(8383, attribute.ID);
                         break;
                     case ScopeType.Community:
                         group = _attributes.GetAttributes(uri).GetAttributeGroup(communityid, attribute.ID);
                         break;
                     case ScopeType.Site:
                         group = _attributes.GetAttributes(uri).GetAttributeGroup(siteid, attribute.ID);
                         break;
                     case ScopeType.Brand:
                         group = _attributes.GetAttributes(uri).GetAttributeGroup(brandid, attribute.ID);
                         break;
                 }
                 

            
                

                 attr.AttributeID = attribute.ID.ToString();
                 int attributegroupid=0;
                 int groupid=0;
                 if (group != null)
                 {
                     attributegroupid = group.ID;
                     groupid = group.GroupID;
                 }
               

                 attr.GroupID = groupid.ToString();
                 attr.AttributeGroupID = attributegroupid.ToString();
            }
            catch (Exception ex)
            {

            }

        }

        private Matchnet.Content.ValueObjects.AttributeMetadata.AttributeGroup  getAttributeGroup(int groupid, int attributeid,Matchnet.Content.ValueObjects.AttributeMetadata.Attributes attrs)
           {
            try
            {
                Matchnet.Content.ValueObjects.AttributeMetadata.AttributeGroup group = attrs.GetAttributeGroup(groupid, attributeid);
                return group;

            }catch(Exception ex)
            {
                return null;
            }


        }

        private void cboConfigurations_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtConfigurationURI.Text = Utils.GetConnectionString(cboConfigurations.Text);
            txtContentURI.Text = Utils.GetConnectionString(cboConfigurations.Text + "content");
        }

        private void btnGetMemberPartition_Click(object sender, EventArgs e)
        {
            try
            { string uriformat="tcp://{0}:42000/MemberSM.rem";
                int memberid = Int32.Parse(txtMemberID.Text);
               
                 Matchnet.Configuration.ValueObjects.ServicePartition partition=  SMUtil.GetServicePartition("MEMBER_SVC", Matchnet.Configuration.ServiceAdapters.PartitionMode.Calculated,memberid,txtConfigurationURI.Text);
                 txtURI1.Text = string.Format(uriformat, partition[0].HostName);
                 txtURI2.Text = string.Format(uriformat, partition[1].HostName);
                 lblDBPartition.Text = (memberid % 24).ToString();
                 lblPartition1.Text = (memberid % 24).ToString();
                
                
            }
            catch (Exception ex)
            { MessageBox.Show(ex.ToString()); }
        }

        private void btnBustCache1_Click(object sender, EventArgs e)
        {
            try
            {
                int memberid=Int32.Parse(txtMemberID.Text);
                string key = "Member" + memberid.ToString().Trim();
                string accesskey = "CachedMemberAccess" + memberid.ToString().Trim();
                string uri=txtURI1.Text.Replace("MemberSM","CacheManagerSM");
                Utils.BustService(uri, key);
                Utils.BustService(uri, accesskey);

                MessageBox.Show("Success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnBustCache2_Click(object sender, EventArgs e)
        {
            try
            {
                int memberid = Int32.Parse(txtMemberID.Text);
                string key = "Member" + memberid.ToString().Trim();
                string uri = txtURI2.Text.Replace("MemberSM", "CacheManagerSM");
                Utils.BustService(uri, key);

                MessageBox.Show("Success");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnFindID_Click(object sender, EventArgs e)
        {
            try
            {
                IMemberService svc = ((IMemberService)Activator.GetObject(typeof(IMemberService), txtURI1.Text));
                int mid = svc.GetMemberIDByEmail(txtEmail.Text);
                txtMemberID.Text = mid.ToString();
            }catch(Exception ex)
                {MessageBox.Show(ex.ToString());}
        }


        public  Member getMember(int memberid, string uri, int version)
        {


            try
            {
                IMemberService svc = ((IMemberService)Activator.GetObject(typeof(IMemberService), uri));

                byte[] res1 = svc.GetCachedMemberBytes(System.Environment.MachineName,
                    Evaluator.Instance.GetReference(DateTime.Now.AddSeconds(300)),
                    memberid,
                    false, version);

                CachedMember m = new CachedMember(res1, 1);
                Member member = new Member(m);
                member.attributes = _attributes.GetAttributes(txtContentURI.Text);
                member.AttributeUtils = _attributes;
                member.ContentURI = txtContentURI.Text;
                return member;
            }
            catch (Exception ex)
            {
                return null;

            }


        }
    }
}
