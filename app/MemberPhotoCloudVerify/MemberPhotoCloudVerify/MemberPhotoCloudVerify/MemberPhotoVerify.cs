﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace MemberPhotoCloudVerify
{
    public class MemberPhotoVerify
    {
        public int MemberPhotoID { get; set; }
        public int FileID { get; set; }
        public int ThumbFileID { get; set; }
        public int CommunityID { get; set; }
        public string FileWebPath { get; set; }
        public string ThumbFileWebPath { get; set; }
        public string FileCloudPath { get; set; }
        public string ThumbFileCloudPath { get; set; }
        
        public MemberPhotoVerify()
        {
        }

        public MemberPhotoVerify(DataRow row)
        {
            MemberPhotoID = Convert.ToInt32(row["MemberPhotoID"]);
            FileID = Convert.ToInt32(row["FileID"]);
            CommunityID = Convert.ToInt32(row["GroupID"]);
            FileWebPath = row["FileWebPath"].ToString();
            ThumbFileWebPath = string.Empty;
            FileCloudPath = string.Empty;
            ThumbFileCloudPath = string.Empty;

            if (!Convert.IsDBNull(row["ThumbFileWebPath"]))
            {
                ThumbFileWebPath = row["ThumbFileWebPath"].ToString();
            }

            if (!Convert.IsDBNull(row["FileCloudPath"]))
            {
                FileCloudPath = row["FileCloudPath"].ToString();
            }

            if (!Convert.IsDBNull(row["ThumbFileCloudPath"]))
            {
                ThumbFileCloudPath = row["ThumbFileCloudPath"].ToString();
            }

            if (!Convert.IsDBNull(row["ThumbFileID"]))
            {
                ThumbFileID = Convert.ToInt32(row["ThumbFileID"]);
            }
        }

        
    }
}
