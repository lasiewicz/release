﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.File.ServiceAdapters;
using Matchnet.File.ValueObjects;
using Spark.CloudStorage;
using System.Data.SqlClient;
using System.Threading;

namespace MemberPhotoCloudVerify
{
    public enum VerifyStatus
    {
        None = 0,
        Processing = 1,
        Verified = 2,
        Uploaded = 3,
        NoPhysicalFile = 4,
        ErrorUploading = 5,
        DoesntExist = 6,
        ErrorProcessing=7
    }
    
    public class Verifier
    {
        private List<MemberPartitionData> _partitions = null;
        private List<MemberPartitionData> _partitionsToProcess = null;
        private bool _processing = false;
        private string _verifyDBConnectionString = string.Empty;
        private int _numThreads;
        public DateTime _beginDate;
        public DateTime _endDate;
        private List<Thread> _threads;

        static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            System.Diagnostics.Debug.WriteLine("UnhandledExceptionHandler caught : " + e.Message);
        }

        public Verifier(int threads, DateTime beginDate, DateTime endDate)
        {
            _numThreads = threads;
            _beginDate = beginDate;
            _endDate = endDate;
            _partitions = new List<MemberPartitionData>();
            _partitionsToProcess = new List<MemberPartitionData>();
            _verifyDBConnectionString = System.Configuration.ConfigurationManager.AppSettings["VerifyDBConnectionString"];
            _threads = new List<Thread>();

            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledExceptionHandler);

        }

        public void AddMemberPartition(string name, List<string> connectionStrings)
        {
            string verifyDBName = GetVerifyDBName(name);
            int recordsVerified = GetRecordsVerified(verifyDBName);
            int totalPhotoRecords = GetTotalPhotoCount(verifyDBName);

            MemberPartitionData partitionData = new MemberPartitionData(name, verifyDBName, totalPhotoRecords, recordsVerified, connectionStrings);
            _partitions.Add(partitionData);

            if(recordsVerified!=totalPhotoRecords)
            {
                _partitionsToProcess.Add(partitionData);
            }
        }

        public void Start()
        {
            _processing = true;

            for (int i = 0; i < _numThreads; i++)
            {
                Thread t = new Thread(new ThreadStart(Process));
                t.Name = "Process Thread " + i.ToString();
                t.Start();
            }

        }

        public void Stop()
        {
            _processing = false;

            foreach(Thread thread in _threads)
            {
                thread.Join(10000);
            }
        }

        public List<MemberPartitionData> GetCurrentStatus()
        {
            List<MemberPartitionData> currentPartitionData = new List<MemberPartitionData>();

            foreach (MemberPartitionData partition in _partitions)
            {
                int recordsVerified = GetRecordsVerified(partition.VerifyDBName);
                int totalPhotoRecords = GetTotalPhotoCount(partition.VerifyDBName);

                currentPartitionData.Add(new MemberPartitionData(partition.Name, partition.VerifyDBName, totalPhotoRecords, recordsVerified, partition.ConnectionStrings));
            }

            return currentPartitionData;
        }


        private void Process()
        {
            Random rnd1 = new Random();
            
            while (_processing && _partitionsToProcess.Count > 0)
            {
                MemberPartitionData randomPartition = _partitionsToProcess[rnd1.Next(_partitionsToProcess.Count)];
                System.Diagnostics.Debug.WriteLine(Thread.CurrentThread.Name + " grabbed "  + randomPartition.Name);
                DataRow row = GetPhotoToProcess(randomPartition.VerifyDBName);
                if(row != null)
                {
                    MemberPhotoVerify record = new MemberPhotoVerify(row);
                    System.Diagnostics.Debug.WriteLine(Thread.CurrentThread.Name + " to verify " + record.MemberPhotoID.ToString());
                    MarkVerifyRecordAsProcessing(record.MemberPhotoID, randomPartition.VerifyDBName);
                    VerifyRecord(record, randomPartition);
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine(Thread.CurrentThread.Name + " found no records in " + randomPartition.Name);
                    _partitionsToProcess.Remove(randomPartition);
                }
            }
        }

        private void VerifyRecord(MemberPhotoVerify verifyRecord, MemberPartitionData partitionData)
        {

            VerifyStatus fullSizeStatus = ProcessPhotoFile(verifyRecord.FileID, verifyRecord.FileWebPath, verifyRecord.CommunityID, verifyRecord.MemberPhotoID);
            string fixedFullCloudPath = string.Empty;

            if (fullSizeStatus == VerifyStatus.Uploaded || fullSizeStatus == VerifyStatus.Verified)
            {
                fixedFullCloudPath = GetFixedCloudPath(verifyRecord.FileWebPath);
            }

            VerifyStatus thumbnailStatus = VerifyStatus.DoesntExist;
            string thumbnailCloudPath = string.Empty;
            if (verifyRecord.ThumbFileID > 0 && verifyRecord.ThumbFileWebPath != string.Empty)
            {
                thumbnailStatus = ProcessPhotoFile(verifyRecord.ThumbFileID, verifyRecord.ThumbFileWebPath, verifyRecord.CommunityID, verifyRecord.MemberPhotoID);

                if (thumbnailStatus == VerifyStatus.Uploaded || thumbnailStatus == VerifyStatus.Verified)
                {
                    thumbnailCloudPath = GetFixedCloudPath(verifyRecord.ThumbFileWebPath);
                }
            }

            UpdateVerifyRecord(verifyRecord.MemberPhotoID, partitionData.VerifyDBName, fullSizeStatus, thumbnailStatus);

            if (!String.IsNullOrEmpty(fixedFullCloudPath))
            {
                UpdateMemberPhotoRecord(verifyRecord.MemberPhotoID, fixedFullCloudPath, thumbnailCloudPath, partitionData.ConnectionStrings);
            }
        }


        private int GetRecordsVerified(string verifyDBName)
        {
            int number = 0;
            string commandText = string.Format("SELECT COUNT(1) AS LogCount FROM {0} (nolock) WHERE " +
                                         "InsertDate >= '{1}' AND InsertDate <= '{2}'AND " +
                                         "FullSizeStatusID IS NOT NULL AND ThumbnailStatusID IS NOT NULL", 
                                         verifyDBName, _beginDate.ToString(), _endDate.ToString());

            SqlConnection connection = new SqlConnection(_verifyDBConnectionString);
            SqlCommand command = new SqlCommand(commandText, connection);

            try
            {
                connection.Open();
                number = Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                HandleException(ex, "Error getting verified record count. VerifyDB: " + verifyDBName);
            }
            finally
            {
                if (connection.State != ConnectionState.Closed) connection.Close();
            }

            return number;
        }

        private int GetTotalPhotoCount(string verifyDBName)
        {
            int number = 0;
            string commandText = string.Format("SELECT COUNT(1) AS LogCount FROM {0} (nolock) WHERE " +
                                         "InsertDate >= '{1}' AND InsertDate <= '{2}'",
                                         verifyDBName, _beginDate.ToString(), _endDate.ToString());

            SqlConnection connection = new SqlConnection(_verifyDBConnectionString);
            SqlCommand command = new SqlCommand(commandText, connection);

            try
            {
                connection.Open();
                number = Convert.ToInt32(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                HandleException(ex, "Error getting total photo count. VerifyDB: " + verifyDBName);
            }
            finally
            {
                if (connection.State != ConnectionState.Closed) connection.Close();
            }

            return number;
        }

        private DataRow GetPhotoToProcess(string verifyDBName)
        {
            string commandText = string.Format("SELECT top 1 MemberPhotoID, FileID, ThumbFileID, GroupID, FileWebPath,ThumbFileWebPath,FileCloudPath,ThumbFileCloudPath FROM {0} (nolock) WHERE " +
                                "InsertDate >= '{1}' AND InsertDate <= '{2}'AND " +
                                "FullSizeStatusID IS NULL AND ThumbnailStatusID IS NULL",
                                verifyDBName, _beginDate.ToString(), _endDate.ToString());

            SqlConnection connection = new SqlConnection(_verifyDBConnectionString);
            SqlCommand command = new SqlCommand(commandText, connection);
            command.CommandType = CommandType.Text;
            DataSet ds = new DataSet();
            DataRow dr = null;

            try
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count == 1)
                {
                    dr = ds.Tables[0].Rows[0];
                }

            }
            catch (Exception ex)
            {
                HandleException(ex, "Error getting photo to process. VerifyDB: " + verifyDBName);
            }
            finally
            {
                if (connection.State != ConnectionState.Closed) connection.Close();
            }

            return dr;
        }

        private void MarkVerifyRecordAsProcessing(int memberPhotoID, string verifyDBName)
        {
            string commandText = string.Format("UPDATE {0} SET FullSizeStatusID = {1}, ThumbnailStatusID ={2} WHERE MemberPhotoID = {3}",
                                verifyDBName, (int)VerifyStatus.Processing, (int)VerifyStatus.Processing, memberPhotoID);
          
            SqlConnection connection = new SqlConnection(_verifyDBConnectionString);
            SqlCommand command = new SqlCommand(commandText, connection);
            command.CommandType = CommandType.Text;

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                HandleException(ex, "Error marking verify record as processing", memberPhotoID);
            }
            finally
            {
                if (connection.State != ConnectionState.Closed) connection.Close();
            }
        }

        private void UpdateVerifyRecord(int memberPhotoID, string verifyDBName, VerifyStatus fullSizeStatus, VerifyStatus thumbnailStatus)
        {
           string commandText = string.Format("UPDATE {0} SET FullSizeStatusID = {1}, ThumbnailStatusID ={2} WHERE MemberPhotoID = {3}",
                                verifyDBName, (int)fullSizeStatus, (int)thumbnailStatus, memberPhotoID);
          
            SqlConnection connection = new SqlConnection(_verifyDBConnectionString);
            SqlCommand command = new SqlCommand(commandText, connection);
            command.CommandType = CommandType.Text;

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                HandleException(ex, "Error updating verify record", memberPhotoID);
            }
            finally
            {
                if (connection.State != ConnectionState.Closed) connection.Close();
            }
        }


        private string GetVerifyDBName(string memberPartitionName)
        {
            return System.Configuration.ConfigurationManager.AppSettings["VerifyDBBaseName"] + Convert.ToInt32(memberPartitionName.Substring(8));    
        }
        

        private VerifyStatus ProcessPhotoFile(int fileID, string webPath, int communityID, int memberPhotoID)
        {
            try
            {
                FileInstance[] instances = FileSA.Instance.GetFileInstances(fileID);
                if (instances.Count() > 0)
                {
                    bool ourFileExists = System.IO.File.Exists(instances[0].Path);
                    if (!ourFileExists) return VerifyStatus.NoPhysicalFile;

                    string fixedCloudPath = GetFixedCloudPath(webPath);
                    Client cloudClient = new Client(communityID, Matchnet.Constants.NULL_INT);
                    bool CloudFileExists = cloudClient.Exists(fixedCloudPath, FileType.MemberPhoto);
                    if (CloudFileExists) return VerifyStatus.Verified;

                    //if we've gotten here the file exists and needs to be uploaded
                    OperationResult result = UploadPhoto(instances[0].Path, fixedCloudPath, communityID, memberPhotoID);
                    if (result == OperationResult.Success)
                    {
                        return VerifyStatus.Uploaded;
                    }
                    else
                    {
                        return VerifyStatus.ErrorUploading;
                    }

                }
                else
                {
                    return VerifyStatus.NoPhysicalFile;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex, "Error processing photo file", memberPhotoID);
                return VerifyStatus.ErrorProcessing;
            }
        }

        private string GetFixedCloudPath(string relativeWebPath)
        {
            string properPath = relativeWebPath;
            int p = relativeWebPath.IndexOf("/Photo");
            if (p >= 0)
            {
                string st = relativeWebPath.Substring(p + 6);
                properPath = st.Substring(st.IndexOf("/"));
            }

            return properPath;
        }

        private string TrimFileFromPath(string fullPath)
        {
            return fullPath.Substring(0, fullPath.LastIndexOf("/") + 1);
        }

        private OperationResult UploadPhoto(string physicalPath, string cloudPath, int communityID, int memberPhotoID)
        {

            try
            {
                Client cloudClient = new Client(communityID, Matchnet.Constants.NULL_INT);
                return cloudClient.UploadFile(physicalPath, TrimFileFromPath(cloudPath), FileType.MemberPhoto);
            }
            catch (Exception ex)
            {
                HandleException(ex, "Error processing photo file", memberPhotoID);
                return OperationResult.Failure;
            }

        }

        private void UpdateMemberPhotoRecord(int memberPhotoID, string fileCloudPath, string thumbFileCloudPath, List<string> connectionStrings)
        {
            foreach (string connectionString in connectionStrings)
            {
                string commandText = "up_MemberPhoto_Update_Cloud_Paths";
                SqlConnection connection = new SqlConnection(connectionString);
                SqlCommand command = new SqlCommand(commandText, connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@MemberPhotoID", memberPhotoID);
                command.Parameters.AddWithValue("@FileCloudPath", fileCloudPath);

                if (!String.IsNullOrEmpty(thumbFileCloudPath))
                {
                    command.Parameters.AddWithValue("@ThumbFileCloudPath", thumbFileCloudPath);
                }

                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    HandleException(ex, "Error updating member photo record", memberPhotoID);
                }
                finally
                {
                    if (connection.State != ConnectionState.Closed) connection.Close();
                }
            }
        }

        private void HandleException(Exception ex, string message)
        {
            HandleException(ex, message, Matchnet.Constants.NULL_INT);
        }

        private void HandleException(Exception ex, string message, int memberPhotoID)
        {
            string commandText = string.Format("INSERT INTO ErrorLog (MemberPhotoID,Exception,[Message],InsertDate) VALUES ({0},'{1}','{2}', '{3}')",
                                memberPhotoID, ex.Message.Replace("'", "").Replace("\"", ""), message, DateTime.Now.ToString());

            SqlConnection connection = new SqlConnection(_verifyDBConnectionString);
            SqlCommand command = new SqlCommand(commandText, connection);
            command.CommandType = CommandType.Text;

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex2)
            {
                throw ex2;
            }
            finally
            {
                if (connection.State != ConnectionState.Closed) connection.Close();
            }
        }


    }
}
