﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MemberPhotoCloudVerify
{
    public class MemberPartitionData
    {
        public string Name { get; set; }
        public List<string> ConnectionStrings { get; private set; }
        public string VerifyDBName { get; set; }
        public int TotalRecords { get; set; }
        public int RecordsVerified { get; set; }

        public void AddString(string connectionString)
        {
            ConnectionStrings.Add(connectionString);
        }

        public MemberPartitionData(string name, string verifyDBName, int totalRecords, int recordsVerified, List<string> connectionStrings )
        {
            Name = name;
            TotalRecords = totalRecords;
            RecordsVerified = recordsVerified;
            VerifyDBName = verifyDBName;
            ConnectionStrings = connectionStrings;
        }
    }
}
