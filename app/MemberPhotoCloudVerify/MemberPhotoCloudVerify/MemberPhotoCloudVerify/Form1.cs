﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.File.ServiceAdapters;
using Matchnet.File.ValueObjects;
using Spark.CloudStorage;
using System.Data.SqlClient;

namespace MemberPhotoCloudVerify
{
    
    
    public partial class Form1 : Form
    {
        public DateTime _beginDate;
        public DateTime _endDate;
        public Verifier _verifier;
        private List<string> _databases = null;
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _databases = GetDatabaseList();
            InitializeVerifier(_databases);
            InitializeGrid();
            UpdateStatus();
        }

        private void InitializeVerifier(List<string> databases)
        {
            int threads = Convert.ToInt32(txtThreads.Text);
            _beginDate = dtBeginDate.Value;
            _endDate = dtEndDate.Value;
            _verifier = new Verifier(threads, _beginDate, _endDate);

            foreach(string database in databases)
            {
                List<string> connectionStrings = GetConnectionStringsForDBName(database);
                _verifier.AddMemberPartition(database, connectionStrings);
            }

        }

        private void InitializeGrid()
        {
            dgStatus.Columns.Clear();
            dgStatus.Columns.Add("Partition", "Partition");
            dgStatus.Columns.Add("TotalRecords", "Total Records");
            dgStatus.Columns.Add("Processed", "Processed");
            dgStatus.Columns.Add("Remaining", "Remaining");
        }

        private List<string> GetDatabaseList()
        {
            List<string> databases = new List<string>();
            int partitionCount = LpdSA.Instance.GetLogicalDatabases()["mnMember"].PartitionCount;

            for (int partition = 0; partition < partitionCount; partition++)
            {
                PhysicalDatabases physicalDatabases = LpdSA.Instance.GetLogicalDatabases()["mnMember"][Convert.ToInt16(partition)].PhysicalDatabases;
                for (int pdbCount = 0; pdbCount < physicalDatabases.Count; pdbCount++)
                {
                    if (!databases.Contains(physicalDatabases[pdbCount].PhysicalDatabaseName))
                        databases.Add(physicalDatabases[pdbCount].PhysicalDatabaseName);
                }
            }

            return databases;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            btnStart.Enabled = false;
            _beginDate = dtBeginDate.Value;
            _endDate = dtEndDate.Value;

            InitializeVerifier(_databases);
            InitializeGrid();
            UpdateStatus();

            _verifier.Start();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            btnStart.Enabled = true;
            _verifier.Stop();
        }
        
        private List<string> GetConnectionStringsForDBName(string dbName)
        {
            List<string> connectionStrings = null;
            int partitionCount = LpdSA.Instance.GetLogicalDatabases()["mnMember"].PartitionCount;

            for (int partition = 0; partition < partitionCount; partition++)
            {
                PhysicalDatabases physicalDatabases = LpdSA.Instance.GetLogicalDatabases()["mnMember"][Convert.ToInt16(partition)].PhysicalDatabases;
                for (int pdbCount = 0; pdbCount < physicalDatabases.Count; pdbCount++)
                {
                    if (physicalDatabases[pdbCount].PhysicalDatabaseName == dbName)
                    {
                        if(connectionStrings == null) connectionStrings = new List<string>();

                        connectionStrings.Add(physicalDatabases[pdbCount].ConnectionString);
                    }
                }
            }

            return connectionStrings;
        }

        private void UpdateStatus()
        {
            List<MemberPartitionData> statusData = _verifier.GetCurrentStatus();
            dgStatus.Rows.Clear();
            int totalRecords = 0;
            int verifiedRecords = 0;
            int remainingRecords = 0;

            foreach (MemberPartitionData partition in statusData)
            {
                List<string> values = new List<string>();
                values.Add(partition.Name);
                values.Add(partition.TotalRecords.ToString());
                values.Add(partition.RecordsVerified.ToString());
                values.Add((partition.TotalRecords - partition.RecordsVerified).ToString());

                dgStatus.Rows.Add(values.ToArray());

                totalRecords = totalRecords + partition.TotalRecords;
                verifiedRecords = verifiedRecords + partition.RecordsVerified;
                remainingRecords = remainingRecords + (partition.TotalRecords - partition.RecordsVerified);
            }

            List<string> totalValues = new List<string>();

            totalValues.Add("TOTAL");
            totalValues.Add(totalRecords.ToString());
            totalValues.Add(verifiedRecords.ToString());
            totalValues.Add(remainingRecords.ToString());
            dgStatus.Rows.Add(totalValues.ToArray());
        }

        private void btnStatus_Click(object sender, EventArgs e)
        {
            UpdateStatus();
        }

        private void dtBeginDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dtEndDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            InitializeVerifier(_databases);
            InitializeGrid();
            UpdateStatus();
        }

    }
}
