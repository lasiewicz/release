using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using System.IO;
using System.Net;
using System.Text;
using System.Security.Cryptography.X509Certificates;

using Matchnet.RemotingServices.ValueObjects;
using Matchnet.RemotingServices.ValueObjects.ServiceDefinitions;

namespace CacheBuster
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox CacheKey;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button BustWeb;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button BustMT;
		private System.Windows.Forms.TextBox MTServer;
		private System.Windows.Forms.TextBox WebServer;
		private System.Windows.Forms.TextBox Output;
		private System.Windows.Forms.CheckBox AllWebs;
		private System.Windows.Forms.TextBox FileName;
		private System.Windows.Forms.TextBox FilePath;
		private System.Windows.Forms.Label label4;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Button BustResource;
		private System.Windows.Forms.CheckBox IsImage;

		private static string[] Servers = {
											  "laweb00",
											  "laweb01",
											  "laweb02",
											  "laweb03",
											  "laweb04",
											  "laweb05",
											  "laweb06",
											  "laweb07",
											  "laweb08",
											  "laweb09",
											  "laweb10",
											  "laweb11",
											  "laweb12",
											  "laweb13",
											  "laweb14",
											  "laweb15",
											  "laweb16",
											  "laweb17",
											  "laweb18",
											  "laweb19",
											  "laweb20",
											  "laweb21",
											  "laweb22",
											  "laweb23",
											  "laweb24",
											  "laweb25",
											  "lawebadmin01",
											  "lawebadmin02",
											  "lawebadmin03",
											  "lawebadmin04",
			"lassl01", "lassl02"
};


		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.CacheKey = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.BustWeb = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.MTServer = new System.Windows.Forms.TextBox();
			this.WebServer = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.BustMT = new System.Windows.Forms.Button();
			this.Output = new System.Windows.Forms.TextBox();
			this.AllWebs = new System.Windows.Forms.CheckBox();
			this.FileName = new System.Windows.Forms.TextBox();
			this.FilePath = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.BustResource = new System.Windows.Forms.Button();
			this.IsImage = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// CacheKey
			// 
			this.CacheKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.CacheKey.Location = new System.Drawing.Point(136, 16);
			this.CacheKey.Name = "CacheKey";
			this.CacheKey.Size = new System.Drawing.Size(152, 21);
			this.CacheKey.TabIndex = 0;
			this.CacheKey.Text = "Member16000206";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(16, 16);
			this.label1.Name = "label1";
			this.label1.TabIndex = 1;
			this.label1.Text = "Cache Key:";
			// 
			// BustWeb
			// 
			this.BustWeb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.BustWeb.Location = new System.Drawing.Point(232, 88);
			this.BustWeb.Name = "BustWeb";
			this.BustWeb.TabIndex = 2;
			this.BustWeb.Text = "Bust Web";
			this.BustWeb.Click += new System.EventHandler(this.Bust_Click);
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label2.Location = new System.Drawing.Point(16, 272);
			this.label2.Name = "label2";
			this.label2.TabIndex = 3;
			this.label2.Text = "MT TCP:";
			// 
			// MTServer
			// 
			this.MTServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.MTServer.Location = new System.Drawing.Point(136, 272);
			this.MTServer.Name = "MTServer";
			this.MTServer.Size = new System.Drawing.Size(152, 21);
			this.MTServer.TabIndex = 4;
			this.MTServer.Text = "devapp01:42000";
			// 
			// WebServer
			// 
			this.WebServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.WebServer.Location = new System.Drawing.Point(136, 88);
			this.WebServer.Name = "WebServer";
			this.WebServer.Size = new System.Drawing.Size(80, 21);
			this.WebServer.TabIndex = 5;
			this.WebServer.Text = "devweb01";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label3.Location = new System.Drawing.Point(16, 56);
			this.label3.Name = "label3";
			this.label3.TabIndex = 6;
			this.label3.Text = "Web Server:";
			// 
			// BustMT
			// 
			this.BustMT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.BustMT.Location = new System.Drawing.Point(136, 304);
			this.BustMT.Name = "BustMT";
			this.BustMT.TabIndex = 7;
			this.BustMT.Text = "Bust MT";
			this.BustMT.Click += new System.EventHandler(this.BustMT_Click);
			// 
			// Output
			// 
			this.Output.Location = new System.Drawing.Point(16, 336);
			this.Output.Multiline = true;
			this.Output.Name = "Output";
			this.Output.Size = new System.Drawing.Size(376, 160);
			this.Output.TabIndex = 8;
			this.Output.Text = "";
			// 
			// AllWebs
			// 
			this.AllWebs.Location = new System.Drawing.Point(136, 56);
			this.AllWebs.Name = "AllWebs";
			this.AllWebs.TabIndex = 9;
			this.AllWebs.Text = "Hit All Webs";
			// 
			// FileName
			// 
			this.FileName.Location = new System.Drawing.Point(128, 152);
			this.FileName.Name = "FileName";
			this.FileName.Size = new System.Drawing.Size(208, 20);
			this.FileName.TabIndex = 10;
			this.FileName.Text = "Default.ascx.103.en-US.resx";
			// 
			// FilePath
			// 
			this.FilePath.Location = new System.Drawing.Point(16, 192);
			this.FilePath.Name = "FilePath";
			this.FilePath.Size = new System.Drawing.Size(384, 20);
			this.FilePath.TabIndex = 11;
			this.FilePath.Text = "C:\\Inetpub\\wwwroot\\Framework\\Ui\\LeftNav\\_Resources\\Default.ascx.103.en-US.resx";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(16, 144);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(100, 40);
			this.label4.TabIndex = 12;
			this.label4.Text = "Invalidate Resource";
			// 
			// BustResource
			// 
			this.BustResource.Location = new System.Drawing.Point(144, 232);
			this.BustResource.Name = "BustResource";
			this.BustResource.Size = new System.Drawing.Size(88, 23);
			this.BustResource.TabIndex = 13;
			this.BustResource.Text = "Bust Resource";
			this.BustResource.Click += new System.EventHandler(this.ButResource_Click);
			// 
			// IsImage
			// 
			this.IsImage.Location = new System.Drawing.Point(136, 120);
			this.IsImage.Name = "IsImage";
			this.IsImage.TabIndex = 14;
			this.IsImage.Text = "ImageResource";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(408, 509);
			this.Controls.Add(this.IsImage);
			this.Controls.Add(this.BustResource);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.FilePath);
			this.Controls.Add(this.FileName);
			this.Controls.Add(this.Output);
			this.Controls.Add(this.WebServer);
			this.Controls.Add(this.MTServer);
			this.Controls.Add(this.CacheKey);
			this.Controls.Add(this.AllWebs);
			this.Controls.Add(this.BustMT);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.BustWeb);
			this.Controls.Add(this.label1);
			this.Name = "Form1";
			this.Text = "CacheBuster";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		public class MyPolicy : ICertificatePolicy 
		{
			public bool CheckValidationResult(
				ServicePoint srvPoint
				, X509Certificate certificate
				, WebRequest request
				, int certificateProblem) 
			{

				//Return True to force the certificate to be accepted.
				return true;

			} // end CheckValidationResult
		} // class MyPolicy



		private void BustWebServer(string server)
		{
			string url = @"http://" + server + @"/CacheManager.aspx?DeleteKey=" + CacheKey.Text;

			if (server.IndexOf("ssl") > 0)
			{
				System.Net.ServicePointManager.CertificatePolicy = new MyPolicy();
				url = @"http://" + server + @":443/CacheManager.aspx?DeleteKey=" + CacheKey.Text;
			}

			HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);

			HttpWebResponse response = (HttpWebResponse) request.GetResponse();

			Stream responseStream = response.GetResponseStream();

			StringBuilder sb  = new StringBuilder();
			byte[] buf = new byte[8192];
			string tempString = null;
			int count = 0;

			do
			{
				// fill the buffer with data
				count = responseStream.Read(buf, 0, buf.Length);

				// make sure we read some data
				if (count != 0)
				{
					// translate from bytes to ASCII text
					tempString = Encoding.ASCII.GetString(buf, 0, count);

					// continue building the string
					sb.Append(tempString);
				}
			}
			while (count > 0); // any more data to read?

			Output.Text += "CacheBuster: Removed " + CacheKey.Text + " on " + server +
				System.Environment.NewLine + 
				"Server response:" + sb.ToString();
		}

		private void Bust_Click(object sender, System.EventArgs e)
		{
			BustWeb.Enabled = false;

			if (AllWebs.Checked)
			{
				for (int i = 0; i < Servers.Length; i++)
				{
					BustWebServer(Servers[i]);
				}

				Application.DoEvents();
			}
			else
			{
				BustWebServer(WebServer.Text);
			}

			BustWeb.Enabled = true;
		}

		private void BustMT_Click(object sender, System.EventArgs e)
		{
			BustMT.Enabled = false;

			ICacheManagerService cacheSM;

			cacheSM = (ICacheManagerService)Activator.GetObject(typeof(ICacheManagerService), @"tcp://" + MTServer.Text + @"/CacheManagerSM.rem");

			cacheSM.Remove(CacheKey.Text);

			Output.Text = "CacheBuster: Removed " + CacheKey.Text + " on " + MTServer.Text;

			BustMT.Enabled = true;
		}

		private void ButResource_Click(object sender, System.EventArgs e)
		{
			BustResource.Enabled = false;

			if (AllWebs.Checked)
			{
				for (int i = 0; i < Servers.Length; i++)
				{
					BustResourceServer(Servers[i]);
				}

				Application.DoEvents();
			}
			else
			{
				BustResourceServer(WebServer.Text);
			}

			BustResource.Enabled = true;
		}

		private void BustResourceServer(string server)
		{

			if (server.ToLower().IndexOf("ssl") != -1)
				server = server + @":443";

			string url;

			if (!IsImage.Checked)
			{
				url = @"http://" + server + @"/CacheManager.aspx?ResourceFileName=" + FileName.Text.Trim() + "&ResourceFileFullPath=" + FilePath.Text.Trim();
			}
			else
			{
				url = @"http://" + server + @"/CacheManager.aspx?ImageFileName=" + FileName.Text.Trim();
			}
			

			HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);
			
			if (server.ToLower().IndexOf("ssl") != -1)
			{
				System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
			}
			
			HttpWebResponse response = (HttpWebResponse) request.GetResponse();

			Stream responseStream = response.GetResponseStream();

			StringBuilder sb  = new StringBuilder();
			byte[] buf = new byte[8192];
			string tempString = null;
			int count = 0;

			do
			{
				// fill the buffer with data
				count = responseStream.Read(buf, 0, buf.Length);

				// make sure we read some data
				if (count != 0)
				{
					// translate from bytes to ASCII text
					tempString = Encoding.ASCII.GetString(buf, 0, count);

					// continue building the string
					sb.Append(tempString);
				}
			}
			while (count > 0); // any more data to read?

			Output.Text += "CacheBuster: Removed " + FilePath.Text + " on " + server +
				System.Environment.NewLine + 
				"Server response:" + sb.ToString();
		}

		public class TrustAllCertificatePolicy : System.Net.ICertificatePolicy
		{
			public TrustAllCertificatePolicy() 
			{
			}

			public bool CheckValidationResult(ServicePoint sp, 
				System.Security.Cryptography.X509Certificates.X509Certificate cert,WebRequest req, int problem)
			{
				return true;
			}
		}

	}
}
