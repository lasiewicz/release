﻿#region

using Microsoft.VisualStudio.TestTools.UnitTesting;

#endregion

namespace Spark.Chat.ejabberdOAuthBridge.Test
{
    ///<summary>
    ///    This is a test class for OAuthBridgeTest and is intended
    ///    to contain all OAuthBridgeTest Unit Tests
    ///</summary>
    [TestClass]
    public class OAuthBridgeTest
    {
        ///<summary>
        ///    Gets or sets the test context which provides
        ///    information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion

        ///<summary>
        ///    A test for Authorize
        /// If the test fails, please refer to below on selecting a valid access token.
        ///</summary>
        [TestMethod]
        public void AuthorizeTest()
        {
            var loginData = new LoginData
            {
                Username = "16000206-3",
                Domain = "JDate.com",
                // Query mnAPI on devsql01 to get the access token 
                // select AccessToken from mnAPI..AppMember where MemberID = 16000206 and AccessTokenExpirationDateTime > GETDATE()
                Password = @"1/fNBj7PJrAeqdZAGclxRuWoI4bjqps1gyIOPWZQpu1BQ="
            }; 

            var actual = OAuthBridge.Authorize(loginData);
            Assert.AreEqual(true, actual);
        }

        ///<summary>
        ///    A test for IsUser
        ///</summary>
        [TestMethod]
        public void IsUserTest()
        {
            var isUserData = new IsUserData()
            {
                Username = "16000206-3",
                Domain = "JDate.com",
            };

            var actual = OAuthBridge.IsUser(isUserData);
            Assert.AreEqual(true, actual);
        }
    }
}