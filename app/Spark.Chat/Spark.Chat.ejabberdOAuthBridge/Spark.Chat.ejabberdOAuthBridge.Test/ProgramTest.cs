﻿#region

using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Spark.Chat.ejabberdOAuthBridge;
using System;

#endregion

namespace Spark.Chat.ejabberdOAuthBridge.Test
{
    ///<summary>
    ///    This is a test class for ProgramTest and is intended
    ///    to contain all ProgramTest Unit Tests
    ///</summary>
    [TestClass]
    public class ProgramTest
    {
        ///<summary>
        ///    Gets or sets the test context which provides
        ///    information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion

        ///<summary>
        ///    A test for ValidateRequestInput
        ///</summary>
        [TestMethod]
        [DeploymentItem("ejabberdOAuthBridge.exe")]
        public void ValidateRequestInputTest()
        {
            // select AccessToken from mnAPI..AppMember where MemberID = 16000206 and AccessTokenExpirationDateTime > GETDATE()
            var data = "auth:16000206-3:jdate.com:1/fNBj7PJrAeqdZAGclxRuWoI4bjqps1gyIOPWZQpu1BQ=";

            var actual = Program_Accessor.ValidateRequestInput(data);
            Assert.AreEqual(true, actual);
        }

        /// <summary>
        ///A test for ProcessRequest
        /// TODO Had this test working with Console.Readline but ended up using BinaryReader. Make the test work for BynaryReader.
        ///</summary>
        [TestMethod()]
        [DeploymentItem("ejabberdOAuthBridge.exe")]
        public void ProcessRequestTest()
        {
            using (var sw = new StringWriter())
            {
                Console.SetOut(sw);
                
                // This block writes to the same input stream.
                //using (TextReader textReader = 
                //    new StringReader(@" Qauth:16000206-3:jdate.com:1/3x/hfeBgLeiwOMESUXgvLayrZIWBEwibba1ph19E/dg=" 
                //    + Environment.NewLine))
                //{
                //    Console.SetIn(textReader);
                //    // select AccessToken from mnAPI..AppMember where MemberID = 16000206 and AccessTokenExpirationDateTime > GETDATE()
                //    Assert.AreEqual(true, Program_Accessor.ProcessRequest());
                //}

                Assert.AreEqual(true, Program_Accessor.ProcessRequest(@" Qauth:16000206-3:jdate.com:1/3x/hfeBgLeiwOMESUXgvLayrZIWBEwibba1ph19E/dg="));
            }
        }
    }
}