﻿Deploying to a linux box.
Won Oct-2012

1. Copy the solution and the RestConsumer solution to a linux dev box. I used an Ubuntu VM.
2. The linux dev box should have Mono Develop installed.
3. Build Spark.RestConsumer on the box.
4. Re-Reference the DLLs from step3 in Spark.Chat.ejabberdOAuthBridge on the linux box.
5. Re-add RestConsumer.xml to the solution.
6. Tar(zip) the files and scp to an ejabberd linux box that has mono installed.
