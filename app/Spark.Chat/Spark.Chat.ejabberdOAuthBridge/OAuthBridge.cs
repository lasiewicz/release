﻿#region

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using log4net;
using Spark.Common.RestConsumer;
using Spark.Common.RestConsumer.Models.Profile;

#endregion

namespace Spark.Chat.ejabberdOAuthBridge
{
    // known ejabberd operation codes: auth, isuser, setpass, tryregister
    // this program implements auth and isuser
    // examples:
    // isuser:126383962:ladevweb03
    // auth:126383962:ladevweb03:1/D0IpzgyaJiB0DMXwecyh996hvwwVhrFmKl5bZqVxw9I=
    internal class OAuthBridge
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (OAuthBridge));

        /// <summary>
        ///     TODO move to appp config
        /// </summary>
        private static readonly Dictionary<int, int> CommunityToBrandMap = new Dictionary<int, int>
                                                                               {
                                                                                   {1, 1001}, // spark
                                                                                   {3, 1003}, // JDate
                                                                                   {10, 1015}, // cupid
                                                                                   {23, 90410}, // bbw
                                                                                   {24, 90510}, // black singles 
                                                                               };

        /// <summary>
        /// </summary>
        /// <param name = "loginData">Domain is only used for logging purpose.</param>
        /// <returns></returns>
        public static bool Authorize(LoginData loginData)
        {
            int memberId;
            int brandId;

            Log.DebugFormat("opcode {0} username {1} domain {2} password {3}", "auth", loginData.Username,
                            loginData.Domain, loginData.Password);
            Log.Debug("opcode, username and password found - authenticating!");

            if (!GetBrandIdAndMemberId(loginData.Username, out brandId, out memberId))
            {
                return false;
            }
            OAuthConsumer consumer;
            try
            {
                consumer = new OAuthConsumer(memberId, loginData.Password,
                                             DateTime.Now + new TimeSpan(1, 0, 0), brandId);
                // using fake access token expiry time, token should be good, and only 1 request will be made.  
            }
            catch (DirectoryNotFoundException ex)
            {
                Log.Error("Failed to create OAuthConsumer, missing RestConsumer.xml?", ex);
                throw;
            }
            catch (Exception ex)
            {
                Log.Error("Failed to create OAuthConsumer", ex);
                throw;
            }
            var urlParams = new Dictionary<string, string>
                                {{"targetMemberId", memberId.ToString(CultureInfo.InvariantCulture)}};
            try
            {
                var miniProfile = consumer.Get<MiniProfile>(urlParams);
                if (miniProfile == null)
                {
                    Log.InfoFormat("authentication of username {0} domain {1} password {2} - failed",
                                   loginData.Username,
                                   loginData.Domain, loginData.Password);
                    return false;
                }
                Log.InfoFormat("authentication of username {0} domain {1} - succeeded", loginData.Username,
                               loginData.Domain);
                return true; // made a valid request. OAuth token worked, so allow the user in.
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(
                    "Exception calling oauth for authorization for member ID {0}. password: {1}",
                    memberId, loginData.Password);
                Log.Error(ex);
                return false;
            }
        }

        /// <summary>
        ///     ejabberd is asking to confirm the a member is valid, so that another user can initiate chat with him/her
        /// </summary>
        /// <param name = "loginData"></param>
        /// <returns></returns>
        public static bool IsUser(IsUserData loginData)
        {
            int memberId;
            int brandId;
            var username = loginData.Username;
            var domain = loginData.Domain;

            if (!GetBrandIdAndMemberId(username, out brandId, out memberId))
            {
                return false;
            }

            var urlParams = new Dictionary<string, string>
                                {
                                    {
                                        "applicationId",
                                        ConfigurationManager.AppSettings["ApplicationId"].ToString(
                                            CultureInfo.InvariantCulture)
                                        },
                                    {"memberId", memberId.ToString(CultureInfo.InvariantCulture)},
                                };
            var qsPArarms = new Dictionary<string, string>
                                {{"client_secret", ConfigurationManager.AppSettings["ClientSecret"]}};
            RestConsumer restConsumer;
            if (!Program.RestConsumers.TryGetValue(brandId, out restConsumer))
            {
                Log.ErrorFormat("No RestConsumer defined for brand ID {0}", brandId);
            }
            var status = restConsumer.Get<MemberStatus>(urlParams, qsPArarms);
            if (status == null || status.SubscriptionStatus == null ||
                status.SubscriptionStatus == "InvalidMember")
            {
                Log.InfoFormat("{0} request: username {1} domain {2} - invalid user", "isuser", username, domain);
                return false;
            }
            Log.InfoFormat("{0} request: username {1} domain {2} - is a valid user", "isuser", username, domain);
            return true; // valid member
        }

        /// Parse IDs from this string format: (memberId)-(communityID)
        /// gets brandID using community ID
        private static bool GetBrandIdAndMemberId(string username, out int brandId, out int memberId)
        {
            brandId = memberId = 0;

            var parts = username.Split('-');
            if (parts.Length != 2)
            {
                Log.WarnFormat("Invalid communityID/memberID combo: {0}", username);
                return false;
            }

            int communityId;
            if (!Int32.TryParse(parts[1], out communityId))
            {
                Log.WarnFormat("Could not parse community ID: {0}", username);
                return false;
            }

            if (!CommunityToBrandMap.TryGetValue(communityId, out brandId))
            {
                Log.ErrorFormat("No brand ID mapped to community ID {0}", communityId);
                return false;
            }

            username = parts[0];

            if (!Int32.TryParse(username, out memberId))
            {
                Log.WarnFormat("Invalid member ID: {0}", username);
                return false;
            }

            if (memberId == 0 || brandId == 0)
            {
                Log.WarnFormat("MemberId or BrandId cannot be 0");
                return false;
            }

            return true;
        }
    }
}