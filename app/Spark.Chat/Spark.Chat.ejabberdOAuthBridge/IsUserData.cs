﻿namespace Spark.Chat.ejabberdOAuthBridge
{
    internal class IsUserData
    {
        /// <summary>
        /// Format - [MemberId]-[BrandId]
        /// </summary>
        internal string Username { get; set; }
        /// <summary>
        /// e.g. JDate.com
        /// </summary>
        internal string Domain { get; set; }
    }
}