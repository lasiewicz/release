﻿#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using log4net;
using log4net.Appender;
using log4net.Config;
using Spark.Common.RestConsumer;

#endregion

namespace Spark.Chat.ejabberdOAuthBridge
{
    /// <summary>
    ///     This program is called by the chat/IM server, ejabberd.
    ///     ejabberd passes a username (Spark community ID/member ID) , and password (Spark access token), and this program validates the credentials against the REST API.
    ///     This programs runs in a infinte loop.  It needs to loop forever and should not throw exceptions unless it is unable to start up.
    ///     ejabbred "types" to this app, which reads stdin, and responds by returning success/fail codes to stdout.
    ///     Command line arguements are not used.
    /// </summary>
    internal class Program
    {
        #region Private variables

        private const int MaxBufferLength = 1000;
        private static readonly ILog Log = LogManager.GetLogger(typeof (Program));

        // last byte will be changed to indicate success/failure
        private static readonly char[] ResponseChars = new[] {(char) 0, (char) 2, (char) 0, (char) 0};

        // only isuser and auth are needed.  tryregister are setpass are not needed.  
        private static readonly HashSet<string> SupportedCommands = new HashSet<string> {"isuser", "auth"};

        // Members are already registered with Spark, and passwords are access tokens created by the REST API.
        private static readonly Dictionary<string, short> RequiredFieldCountLookup = new Dictionary<string, short>
                                                                                         {
                                                                                             {"isuser", 3},
                                                                                             {"auth", 4},
                                                                                             {"tryregister", 4},
                                                                                             {"setpass", 4}
                                                                                         };

        public static readonly Dictionary<int, RestConsumer> RestConsumers = new Dictionary<int, RestConsumer>
                                                                                 {
                                                                                     {
                                                                                         90510,
                                                                                         RestConsumer.GetInstance(90510)
                                                                                         },
                                                                                     {
                                                                                         1003,
                                                                                         RestConsumer.GetInstance(1003)
                                                                                         }
                                                                                 };

        #endregion

        private static void Main()
        {
            // For log4net
            XmlConfigurator.Configure();

            ChangeLogFileName();

            while (true)
            {
                try
                {
                    string data;
                    //if ((data = Console.ReadLine()) != null)
                    if (GetBinaryInput(out data))
                        SendResponse(ProcessRequest(data));

                }
                catch (Exception exception)
                {
                    Log.Error(exception.ToString());
                }
                finally
                {
                    Thread.Sleep(0);
                }
            }
        }

        private static bool ProcessRequest(string data)
        {
            var timer = new Stopwatch();
            timer.Start();

            if (data == null)
            {
                throw new Exception("Raw request is null.");
            }

            Log.DebugFormat("Raw request: {0}", data);

            if (!ValidateRequestInput(data))
            {
                return false;
            }

            var elements = data.Split(':');
            var opcode = elements[0];
            var passed = false;
            switch (opcode)
            {
                case "auth":
                    if (OAuthBridge.Authorize(new LoginData
                                                  {
                                                      Username = elements[1],
                                                      Domain = elements[2],
                                                      Password = elements[3]
                                                  })) passed = true;
                    break;
                case "isuser":
                    if (OAuthBridge.IsUser(new IsUserData
                                               {
                                                   Username = elements[1],
                                                   Domain = elements[2]
                                               })) passed = true;
                    break;
                default:
                    Log.ErrorFormat("Unknown opcode, {0}", opcode);
                    break;
            }

            timer.Stop();

            Log.InfoFormat("ProcessRequest took {0} milliseconds.", timer.ElapsedMilliseconds);

            return passed;
        }

        private static bool GetBinaryInput(out string data)
        {
            data = "";
            using (var stdinStream = Console.OpenStandardInput())
            {
                using (var binReader = new BinaryReader(stdinStream, Encoding.ASCII))
                {
                    byte b1, b2;
                    try
                    {
                        Log.Debug("waiting for length bytes from stdio");

                        b1 = binReader.ReadByte();
                        b2 = binReader.ReadByte();
                    }
                    catch (Exception exception)
                    {
                        //Log.Error("error reading length bytes from stdio", ex);
                        throw new Exception("error reading length bytes from stdio", exception);
                    }

                    // Perform big endian conversion
                    var len = b2 + (b1 * 256);
                    Log.DebugFormat("first 2 bytes: {0} and {1}  The length: {2}", b1, b2, len);

                    // Check for buffer overrun
                    if (len > MaxBufferLength)
                    {
                        Log.ErrorFormat(
                            "Invalid size given, or size too large, or a human is typing to this program {0} > {1}",
                            len,
                            MaxBufferLength);
                        return false;
                    }

                    // Read message from ejabbered
                    try
                    {
                        var bytes = binReader.ReadBytes(len);
                        data = Encoding.ASCII.GetString(bytes);
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorFormat("Failed to read {0} bytes from stdio", len);
                        Log.Error(ex);
                        return false;
                    }
                }
            }
            return true;
        }

        private static void ChangeLogFileName()
        {
            var rootRep = LogManager.GetRepository();
            if (rootRep == null || rootRep.GetAppenders() == null)
            {
                return;
            }
            foreach (var iApp in rootRep.GetAppenders())
            {
                var appender = iApp as RollingFileAppender;
                if (appender == null)
                {
                    continue;
                }
                var process = Process.GetCurrentProcess();
                var directory = Path.GetDirectoryName(appender.File);
                var fileName = Path.GetFileNameWithoutExtension(appender.File);
                var ext = Path.GetExtension(appender.File);
                if (ext == null || fileName == null || directory == null)
                {
                    continue;
                }
                var newFileName = fileName + "-" + process.Id.ToString(CultureInfo.InvariantCulture) + ext;
                var newPath = Path.Combine(directory, newFileName);
                appender.File = newPath;
                appender.ActivateOptions();
            }
        }

        private static void SendResponse(bool authenticated)
        {
            ResponseChars[3] = authenticated ? (char)1 : (char)0;
            // Send return value
            Console.Out.Write(ResponseChars, 0, 4);
            //Console.Out.Write((authenticated ? "0201" : "0200"));

            Log.DebugFormat("outputting return value, authenticated: {0}", authenticated);
        }

        /// <summary>
        /// </summary>
        /// <param name = "data">Format "[space]Q[opcode]:[username]:[password]"</param>
        /// <returns></returns>
        private static bool ValidateRequestInput(string data)
        {
            if (data.Length > MaxBufferLength)
            {
                Log.ErrorFormat("Data is bigger than the max buffer length.  Raw data: {0}", data);
                return false;
            }

            var elements = data.Split(':');
            if (elements.Length < 1)
            {
                Log.WarnFormat("missing opcode.  Raw data: {0}", data);
                return false;
            }
            var opcode = elements[0];

            if (!SupportedCommands.Contains(opcode))
            {
                Log.WarnFormat("Unsupported opcode {0}.  Raw data: {1}", opcode, data);
                return false;
            }

            short requiredCount;
            // ensure that enough parameters were sent for the given opcode
            if (!RequiredFieldCountLookup.TryGetValue(opcode, out requiredCount))
            {
                Log.WarnFormat("Unknown opcode.  Raw data: {0}", data);
                return false;
            }

            if (elements.Length < requiredCount)
            {
                Log.WarnFormat("Missing fields for {0} request.  Raw data: {1}", elements[0], data);
                return false;
            }

            return true;
        }
    }
}