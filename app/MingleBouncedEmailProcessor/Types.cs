﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MingleBouncedEmailProcessor
{
    public class BouncedEmail
    {
        public string memberid { get; set; }
        public string bounce_message { get; set; }
        public string pass_through { get; set; }
        public string arg1 { get; set; }
        public string arg2 { get; set; }
        public string ymdt { get; set; }

        public BouncedEmail()
        { }
    }
}
