﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Script.Serialization;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.HTTPMessaging.RequestTypes.MingleMailer;
using Matchnet.ExternalMail.ServiceAdapters;

namespace MingleBouncedEmailProcessor
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // get all the global settings first
                string mingleMailerBouncedUrl = RuntimeSettings.GetSetting("MINGLE_MAILER_BOUNCED_URL");
                
                // we are going to assume that this job will run sometime in the am hours. we will strip all the time portion of the current
                // time and keep the date portion only. from this date value, we will substract mingleMailerBouncedWindowDays to determine
                // the time window.
                DateTime sinceymdt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); // since year month date time string
                int mingleMailerBouncedWindowDays = Convert.ToInt32(RuntimeSettings.GetSetting("MINGLE_MAILER_BOUNCED_PROCESSOR_WINDOW"));
                
                sinceymdt = sinceymdt.AddDays(-(mingleMailerBouncedWindowDays));
                
                string siteIDsString = RuntimeSettings.GetSetting("MINGLE_MAILER_BOUNCED_SITEID_LIST");
                string[] delimiters = {","};
                string[] siteIDs = siteIDsString.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

                foreach (string siteID in siteIDs)
                {
                    int siteIDInt = Convert.ToInt32(siteID);
                    // let's get site-specific settings first
                    string shortSiteName = RuntimeSettings.GetSetting("MINGLE_MAILER_SITE_SHORTNAME", GetCommunityID(siteIDInt), siteIDInt);
                    string key = RuntimeSettings.GetSetting("MINGLE_MAILER_SITE_KEY", GetCommunityID(siteIDInt), siteIDInt);

                    GetBouncedEmailsRequest req = new GetBouncedEmailsRequest(shortSiteName, sinceymdt, key);

                    HttpStatusCode statusCode;
                    string jsonResponse = string.Empty;
                    try
                    {
                       // receive the json representation of the bounced emails
                        jsonResponse = Matchnet.HTTPMessaging.SMSProxy.SendPost(req.GetPostData(), mingleMailerBouncedUrl, 60000, "application/x-www-form-urlencoded", out statusCode);
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.EventLog.WriteEntry("MingleBouncedEmailProcessor", "Error during SendPost: " + ex.Message,
                            System.Diagnostics.EventLogEntryType.Error);
                    }

                    if (jsonResponse.Length > 0 && jsonResponse[0] == '[')
                    {
                        IList<BouncedEmail> bouncedEmails = new JavaScriptSerializer().Deserialize<IList<BouncedEmail>>(jsonResponse);

                        foreach (BouncedEmail bEmail in bouncedEmails)
                        {
                            AddToDNE(siteIDInt, bEmail);
                        }
                    }
                  
                }
            }
            catch(Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry("MingleBouncedEmailProcessor", ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }
            
        }

        private static void AddToDNE(int siteID, BouncedEmail bEmail)
        {
            // pass_through property holds the email. add this email to dne list
            DoNotEmailSA.Instance.AddEmailAddressBySiteID(siteID, bEmail.pass_through);   
        }

        private static int GetCommunityID(int siteID)
        {
            int communityID = Matchnet.Constants.NULL_INT;

            Sites sites = BrandConfigSA.Instance.GetSites();
            
            foreach (Site site in sites)
            {
                if (site.SiteID == siteID)
                {
                    communityID = site.Community.CommunityID;
                    break;
                }
            }

            return communityID;
        }
    }
}
