﻿namespace Spark.CacheBusterClient.WindowsInterface
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbResults = new System.Windows.Forms.ListBox();
            this.ddlEnvironment = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnJdateRegSite = new System.Windows.Forms.Button();
            this.btnSparkRegSite = new System.Windows.Forms.Button();
            this.btnAPIScenarioMetadata = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnBBWRegSite = new System.Windows.Forms.Button();
            this.btnMemberService = new System.Windows.Forms.Button();
            this.btnJdateUK = new System.Windows.Forms.Button();
            this.btnBlackRegSite = new System.Windows.Forms.Button();
            this.btnJDILRegSite = new System.Windows.Forms.Button();
            this.btnJDateFRRegsite = new System.Windows.Forms.Button();
            this.btnAPIV1metadata = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbResults
            // 
            this.lbResults.FormattingEnabled = true;
            this.lbResults.Location = new System.Drawing.Point(639, 64);
            this.lbResults.Name = "lbResults";
            this.lbResults.Size = new System.Drawing.Size(299, 225);
            this.lbResults.TabIndex = 0;
            // 
            // ddlEnvironment
            // 
            this.ddlEnvironment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlEnvironment.FormattingEnabled = true;
            this.ddlEnvironment.Items.AddRange(new object[] {
            "DEV",
            "STAGEV1",
            "STAGEV2",
            "STAGEV3",
            "PREPROD",
            "PROD"});
            this.ddlEnvironment.Location = new System.Drawing.Point(101, 21);
            this.ddlEnvironment.Name = "ddlEnvironment";
            this.ddlEnvironment.Size = new System.Drawing.Size(121, 21);
            this.ddlEnvironment.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Environment: ";
            // 
            // btnJdateRegSite
            // 
            this.btnJdateRegSite.Location = new System.Drawing.Point(16, 124);
            this.btnJdateRegSite.Name = "btnJdateRegSite";
            this.btnJdateRegSite.Size = new System.Drawing.Size(206, 23);
            this.btnJdateRegSite.TabIndex = 3;
            this.btnJdateRegSite.Text = "JDate RegSite Scenario Metadata";
            this.btnJdateRegSite.UseVisualStyleBackColor = true;
            this.btnJdateRegSite.Click += new System.EventHandler(this.btnJdateRegSite_Click);
            // 
            // btnSparkRegSite
            // 
            this.btnSparkRegSite.Location = new System.Drawing.Point(16, 171);
            this.btnSparkRegSite.Name = "btnSparkRegSite";
            this.btnSparkRegSite.Size = new System.Drawing.Size(206, 23);
            this.btnSparkRegSite.TabIndex = 4;
            this.btnSparkRegSite.Text = "Spark RegSite Scenario Metadata";
            this.btnSparkRegSite.UseVisualStyleBackColor = true;
            this.btnSparkRegSite.Click += new System.EventHandler(this.btnSparkRegSite_Click);
            // 
            // btnAPIScenarioMetadata
            // 
            this.btnAPIScenarioMetadata.Location = new System.Drawing.Point(16, 48);
            this.btnAPIScenarioMetadata.Name = "btnAPIScenarioMetadata";
            this.btnAPIScenarioMetadata.Size = new System.Drawing.Size(212, 23);
            this.btnAPIScenarioMetadata.TabIndex = 5;
            this.btnAPIScenarioMetadata.Text = "API Scenario Metadata";
            this.btnAPIScenarioMetadata.UseVisualStyleBackColor = true;
            this.btnAPIScenarioMetadata.Click += new System.EventHandler(this.btnAPIScenarioMetadata_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(636, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Results:";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(863, 295);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnBBWRegSite
            // 
            this.btnBBWRegSite.Location = new System.Drawing.Point(16, 214);
            this.btnBBWRegSite.Name = "btnBBWRegSite";
            this.btnBBWRegSite.Size = new System.Drawing.Size(206, 23);
            this.btnBBWRegSite.TabIndex = 8;
            this.btnBBWRegSite.Text = "BBW RegSite Scenario Metadata";
            this.btnBBWRegSite.UseVisualStyleBackColor = true;
            this.btnBBWRegSite.Click += new System.EventHandler(this.btnBBWRegSite_Click);
            // 
            // btnMemberService
            // 
            this.btnMemberService.Location = new System.Drawing.Point(320, 64);
            this.btnMemberService.Name = "btnMemberService";
            this.btnMemberService.Size = new System.Drawing.Size(206, 23);
            this.btnMemberService.TabIndex = 9;
            this.btnMemberService.Text = "Member Service";
            this.btnMemberService.UseVisualStyleBackColor = true;
            this.btnMemberService.Click += new System.EventHandler(this.btnMemberService_Click);
            // 
            // btnJdateUK
            // 
            this.btnJdateUK.Location = new System.Drawing.Point(16, 370);
            this.btnJdateUK.Name = "btnJdateUK";
            this.btnJdateUK.Size = new System.Drawing.Size(206, 23);
            this.btnJdateUK.TabIndex = 10;
            this.btnJdateUK.Text = "JDateUK RegSite Scenario Metadata";
            this.btnJdateUK.UseVisualStyleBackColor = true;
            this.btnJdateUK.Click += new System.EventHandler(this.btnJdateUK_Click);
            // 
            // btnBlackRegSite
            // 
            this.btnBlackRegSite.Location = new System.Drawing.Point(16, 253);
            this.btnBlackRegSite.Name = "btnBlackRegSite";
            this.btnBlackRegSite.Size = new System.Drawing.Size(206, 23);
            this.btnBlackRegSite.TabIndex = 11;
            this.btnBlackRegSite.Text = "Black RegSite Scenario Metadata";
            this.btnBlackRegSite.UseVisualStyleBackColor = true;
            this.btnBlackRegSite.Click += new System.EventHandler(this.btnBlackRegSite_Click);
            // 
            // btnJDILRegSite
            // 
            this.btnJDILRegSite.Location = new System.Drawing.Point(16, 295);
            this.btnJDILRegSite.Name = "btnJDILRegSite";
            this.btnJDILRegSite.Size = new System.Drawing.Size(206, 23);
            this.btnJDILRegSite.TabIndex = 12;
            this.btnJDILRegSite.Text = "JDIL RegSite Scenario Metadata";
            this.btnJDILRegSite.UseVisualStyleBackColor = true;
            this.btnJDILRegSite.Click += new System.EventHandler(this.btnJDILRegSite_Click);
            // 
            // btnJDateFRRegsite
            // 
            this.btnJDateFRRegsite.Location = new System.Drawing.Point(16, 331);
            this.btnJDateFRRegsite.Name = "btnJDateFRRegsite";
            this.btnJDateFRRegsite.Size = new System.Drawing.Size(206, 23);
            this.btnJDateFRRegsite.TabIndex = 13;
            this.btnJDateFRRegsite.Text = "JDFR RegSite Scenario Metadata";
            this.btnJDateFRRegsite.UseVisualStyleBackColor = true;
            this.btnJDateFRRegsite.Click += new System.EventHandler(this.btnJDateFRRegsite_Click);
            // 
            // btnAPIV1metadata
            // 
            this.btnAPIV1metadata.Location = new System.Drawing.Point(16, 86);
            this.btnAPIV1metadata.Name = "btnAPIV1metadata";
            this.btnAPIV1metadata.Size = new System.Drawing.Size(212, 23);
            this.btnAPIV1metadata.TabIndex = 14;
            this.btnAPIV1metadata.Text = "API V1 Scenario Metadata";
            this.btnAPIV1metadata.UseVisualStyleBackColor = true;
            this.btnAPIV1metadata.Click += new System.EventHandler(this.btnAPIV1metadata_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(956, 429);
            this.Controls.Add(this.btnAPIV1metadata);
            this.Controls.Add(this.btnJDateFRRegsite);
            this.Controls.Add(this.btnJDILRegSite);
            this.Controls.Add(this.btnBlackRegSite);
            this.Controls.Add(this.btnJdateUK);
            this.Controls.Add(this.btnMemberService);
            this.Controls.Add(this.btnBBWRegSite);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnAPIScenarioMetadata);
            this.Controls.Add(this.btnSparkRegSite);
            this.Controls.Add(this.btnJdateRegSite);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ddlEnvironment);
            this.Controls.Add(this.lbResults);
            this.Name = "Form1";
            this.Text = "CacheBuster";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbResults;
        private System.Windows.Forms.ComboBox ddlEnvironment;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnJdateRegSite;
        private System.Windows.Forms.Button btnSparkRegSite;
        private System.Windows.Forms.Button btnAPIScenarioMetadata;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnBBWRegSite;
        private System.Windows.Forms.Button btnMemberService;
        private System.Windows.Forms.Button btnJdateUK;
        private System.Windows.Forms.Button btnBlackRegSite;
        private System.Windows.Forms.Button btnJDILRegSite;
        private System.Windows.Forms.Button btnJDateFRRegsite;
        private System.Windows.Forms.Button btnAPIV1metadata;
    }
}

