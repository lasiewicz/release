﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Spark.CacheBusterClient;

namespace Spark.CacheBusterClient.WindowsInterface
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ddlEnvironment.SelectedIndex = 0;
        }

        private List<OperationResponse> ClearWebBasedCache(CacheType cacheType)
        {
            var environment = GetEnvironmentType();
            var cacheBusterClient = new Client();
            return cacheBusterClient.ClearWebBasedCache(cacheType, environment);
        }

        private List<OperationResponse> ClearServiceCache(ServiceCacheType cacheType)
        {
            var environment = GetEnvironmentType();
            var cacheBusterClient = new Client();
            return cacheBusterClient.ClearServiceCache(cacheType, environment);
        }

        private EnivronmentType GetEnvironmentType()
        {
            return (EnivronmentType)Enum.Parse(typeof(EnivronmentType), ddlEnvironment.SelectedItem.ToString());
        }

        private void DisplayResults(List<OperationResponse> results)
        {
            lbResults.Items.Clear();
            
            foreach (var result in results)
            {
                if (result.Success)
                {
                    lbResults.Items.Add(result.HostName + " cleared");
                }
                else if (!result.Success && result.HostName == "None")
                {
                    lbResults.Items.Add(result.ErrorMessage);
                }
                else
                {
                    lbResults.Items.Add(result.HostName + " not cleared (" + result.ErrorMessage + ")");
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            lbResults.Items.Clear();
        }

        private void btnAPIScenarioMetadata_Click(object sender, EventArgs e)
        {
            var results = ClearWebBasedCache(CacheType.APIScenarioMetadata);
            DisplayResults(results);
        }

        private void btnJdateRegSite_Click(object sender, EventArgs e)
        {
            var results = ClearWebBasedCache(CacheType.RegSiteScenarioMetadataJDate);
            DisplayResults(results);
        }

        private void btnSparkRegSite_Click(object sender, EventArgs e)
        {
            var results = ClearWebBasedCache(CacheType.RegSiteScenarioMetadataSpark);
            DisplayResults(results);
        }

        private void btnBBWRegSite_Click(object sender, EventArgs e)
        {
            var results = ClearWebBasedCache(CacheType.RegSiteScenarioMetadataBBW);
            DisplayResults(results);
        }

        private void btnMemberService_Click(object sender, EventArgs e)
        {
            var results = ClearServiceCache(ServiceCacheType.Member);
            DisplayResults(results);
        }

        private void btnJdateUK_Click(object sender, EventArgs e)
        {
            var results = ClearWebBasedCache(CacheType.RegSiteScenarioMetadataJDateUK);
            DisplayResults(results);
        }

        private void btnBlackRegSite_Click(object sender, EventArgs e)
        {
            var results = ClearWebBasedCache(CacheType.RegSiteScenarioMetadataBlack);
            DisplayResults(results);
        }

        private void btnJDILRegSite_Click(object sender, EventArgs e)
        {
            var results = ClearWebBasedCache(CacheType.RegSiteScenarioMetadataJDateIL);
            DisplayResults(results);
        }

        private void btnJDateFRRegsite_Click(object sender, EventArgs e)
        {
            var results = ClearWebBasedCache(CacheType.RegSiteScenarioMetadataJDateFR);
            DisplayResults(results);
        }

        private void btnAPIV1metadata_Click(object sender, EventArgs e)
        {
            var results = ClearWebBasedCache(CacheType.APIScenarioMetadataV1);
            DisplayResults(results);
        }


    }
}
