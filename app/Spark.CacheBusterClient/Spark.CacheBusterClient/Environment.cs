﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Spark.CacheBusterClient
{
    public enum EnivronmentType
    {
        [XmlEnum("DEV")]
        DEV=0,
        [XmlEnum("STAGEV1")]
        STAGEV1=1,
        [XmlEnum("STAGEV2")]
        STAGEV2=2,
        [XmlEnum("STAGEV3")]
        STAGEV3=3,
        [XmlEnum("PREPROD")]
        PREPROD=4,
        [XmlEnum("PROD")]
        PROD=5
    }
    
    [Serializable]
    public class Environment
    {
        [XmlAttribute("Type")]
        public EnivronmentType Type { get; set; }

        [XmlElement("Host")]
        public List<Host> Hosts { get; set; }
    }
}
