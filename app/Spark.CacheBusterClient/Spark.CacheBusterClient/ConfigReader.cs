﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Spark.CacheBusterClient
{
    public class ConfigReader
    {
        public WebBasedCacheList GetWebBasedCacheList()
        {
            WebBasedCacheList cachelist = null;

            var currentDirectory = Directory.GetCurrentDirectory();
            var filePath = currentDirectory.Substring(0, currentDirectory.IndexOf("bin")) + "WebBasedCaches.xml";

            TextReader textReader = null;
            try
            {
                textReader = new StreamReader(filePath);
                var deserializer = new XmlSerializer(typeof(WebBasedCacheList));
                cachelist = deserializer.Deserialize(textReader) as WebBasedCacheList;
            }
            catch (Exception ex)
            {
                string x = ex.Message;
                //it's ok to supress this error for now
            }
            finally
            {
                if (textReader != null)
                {
                    textReader.Close();
                }
            }

            return cachelist;
        }

        public ServiceCacheList GetServiceCacheList()
        {
            ServiceCacheList cachelist = null;

            var currentDirectory = Directory.GetCurrentDirectory();
            var filePath = currentDirectory.Substring(0, currentDirectory.IndexOf("bin")) + "ServiceCaches.xml";

            TextReader textReader = null;
            try
            {
                textReader = new StreamReader(filePath);
                var deserializer = new XmlSerializer(typeof(ServiceCacheList));
                cachelist = deserializer.Deserialize(textReader) as ServiceCacheList;
            }
            catch
            {
                //it's ok to supress this error for now
            }
            finally
            {
                if (textReader != null)
                {
                    textReader.Close();
                }
            }

            return cachelist;
        }
    }
}
