﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace Spark.CacheBusterClient
{
    
    public class Client
    {
        private const string ERROR = "ERROR";

        public List<OperationResponse> ClearWebBasedCache(CacheType cacheType, EnivronmentType environment)
        {
            var responses = new List<OperationResponse>();
            var cleared = false;

            List<Host> hosts = GetHostsForCacheAndEnvironmentTypes(cacheType, environment);

            if(hosts != null && hosts.Count > 0)
            {
                cleared = true;
                foreach(var host in hosts)
                {
                    string response = MakeWebRequest(host.URL);
                    if (!response.Contains("Scenario Cache Cleared"))
                    {
                        cleared = false;
                    }

                    responses.Add(new OperationResponse
                                      {HostName = host.Name, ErrorMessage = string.Empty, Success = cleared});
                }
            }
            else
            {
                responses.Add(new OperationResponse { HostName = "None", ErrorMessage = "No hosts found", Success = false });
            }

            return responses;
        }

        public List<OperationResponse> ClearServiceCache(ServiceCacheType cacheType, EnivronmentType environment)
        {
            var responses = new List<OperationResponse>();
            var cleared = false;

            List<Host> hosts = GetHostsForServiceCacheAndEnvironmentTypes(cacheType, environment);

            if (hosts != null && hosts.Count > 0)
            {
                foreach (var host in hosts)
                {
                    var response = MakeServiceRequest(cacheType, host.URL);
                    responses.Add(new OperationResponse { HostName = host.Name, ErrorMessage = string.Empty, Success = response });
                }
            }
            else
            {
                responses.Add(new OperationResponse { HostName = "None", ErrorMessage = "No hosts found", Success = false });
            }

            return responses;
        }

        public bool ClearRegistrationScenariosCacheAPI(EnivronmentType environment)
        {
            var cleared = false;

            string url = string.Empty;

            switch (environment)
            {
                case EnivronmentType.STAGEV3:
                    url = "http://lawebstage304:79/admin/clearcache/0";
                    break;
            }

            string response = MakeWebRequest(url);
            if (response == "Scenario Cache Cleared")
            {
                cleared = true;
            }

            return cleared;
        }

        private bool MakeServiceRequest(ServiceCacheType cacheType, string uri)
        {
            bool success = true;
            var serviceHelper = new ServiceHelper();
            
            switch(cacheType)
            {
                case ServiceCacheType.Member:
                    success = serviceHelper.ClearMemberCache(uri);
                    break;
                default:
                    success = false;
                    break;
            }

            return success;
        }


        private string MakeWebRequest(string url)
        {
            var responseValue =string.Empty;
            StreamReader reader = null;
            HttpWebResponse response = null;

            try
            {
                var request = WebRequest.Create(url);
                request.Method = "GET";
                request.Timeout = 5000;
                request.ContentType = "application/x-www-form-urlencoded";

                // Execute the query
                response = (HttpWebResponse)request.GetResponse();
                reader = new StreamReader(response.GetResponseStream());
                responseValue = reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                responseValue = ERROR;
            }
            finally
            {
                if (response != null)
                    response.Close();
                if (reader != null)
                    reader.Close();
            }

            return responseValue;
        }

        private List<Host> GetHostsForCacheAndEnvironmentTypes(CacheType cacheType, EnivronmentType environmentType)
        {
            List<Host> hosts = null;
            
            var configReader = new ConfigReader();
            var webBasedCacheList = configReader.GetWebBasedCacheList();

            var webBasedCache = (from c in webBasedCacheList.WebBasedCaches where c.Type == cacheType select c).FirstOrDefault();

            if(webBasedCache != null)
            {
                hosts =(from e in webBasedCache.Environments where e.Type == environmentType select e.Hosts).FirstOrDefault();
            }

            return hosts;
        }

        private List<Host> GetHostsForServiceCacheAndEnvironmentTypes(ServiceCacheType cacheType, EnivronmentType environmentType)
        {
            List<Host> hosts = null;

            var configReader = new ConfigReader();

            var serviceCacheList = configReader.GetServiceCacheList();

            var serviceCache = (from c in serviceCacheList.ServiceCaches where c.Type == cacheType select c).FirstOrDefault();

            if (serviceCache != null)
            {
                hosts = (from e in serviceCache.Environments where e.Type == environmentType select e.Hosts).FirstOrDefault();
            }

            return hosts;
        }
    }
}
