﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Spark.CacheBusterClient
{
    [Serializable]
    public class ServiceCacheList
    {
        [XmlElement("ServiceCache")]
        public List<ServiceCache> ServiceCaches { get; set; }
    }
}
