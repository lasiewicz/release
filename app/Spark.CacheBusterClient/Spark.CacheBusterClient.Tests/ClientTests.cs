﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Spark.CacheBusterClient;

namespace Spark.CacheBusterClient.Tests
{
    public class ClientTests
    {
        [Test]
        public void ClearRegistrationScenariosCacheRegSiteJDateStageV3()
        {
            var client = new Client();
            var busted = client.ClearWebBasedCache(CacheType.RegSiteScenarioMetadataJDate, EnivronmentType.STAGEV3);
            Assert.IsTrue(busted.Count > 0);
            Assert.IsTrue(busted[0].Success);
        }

        [Test]
        public void TestClearRegistrationScenariosCacheAPIStageV3()
        {
            var client = new Client();
            var busted = client.ClearWebBasedCache(CacheType.APIScenarioMetadata, EnivronmentType.STAGEV3);
            Assert.IsTrue(busted.Count > 0);
            Assert.IsTrue(busted[0].Success);
        }

        [Test]
        public void TestConfigReader()
        {
            var reader = new ConfigReader();
            var cacheList = reader.GetWebBasedCacheList();
            Assert.IsTrue(cacheList != null);

        }

        [Test]
        public void TestClearMemberCache()
        {
            var client = new Client();
            var busted = client.ClearServiceCache(ServiceCacheType.Member, EnivronmentType.DEV);
            Assert.IsTrue(busted.Count > 0);
            Assert.IsTrue(busted[0].Success);
        }
    }
}
