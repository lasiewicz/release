<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<CreditTool.Models.ModelView.CreditOrder>" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<script language="javascript" type="text/javascript">
    function UpdateCredit(obj) {
        try {
        
            var itemAmountCreditTotalObj = document.getElementById('ItemAmountCreditTotal');
            var itemTaxCreditTotalObj = document.getElementById('ItemTaxCreditTotal');
            var creditTotalObj = document.getElementById('CreditTotal');
            TotalChanging(500);
            
            creditObject.ItemAmountCreditTotal = 0.00;
            creditObject.ItemTaxCreditTotal = 0.00;
            for (i = 0; i < creditObject.ItemDetailIDs.length; i++)
            {
                orderDetailID = creditObject.ItemDetailIDs[i];
                
                //credit item amount
                var amountObj = document.getElementById('Amount' + orderDetailID);
                var amountCreditObj = document.getElementById('AmountCredit' + orderDetailID);
               
                var amountCredit = 0;
                if (IsNumeric(amountCreditObj.value))
                {
                    amountCredit = roundNumber(parseFloat(amountCreditObj.value.replace(' ', '')));
                    var formattedAmountCredit = new String((new Number(amountCredit)).toFixed(2));
                    if (formattedAmountCredit.toString() != amountCreditObj.value)
                    {
                        amountCreditObj.value = (new Number(amountCredit)).toFixed(2);
                    }
                }
                
                //credit tax
                var taxObj = document.getElementById('Tax' + orderDetailID);
                var taxCreditObj = document.getElementById('TaxCredit' + orderDetailID);
                var itemTaxCredit = 0.00;
                if (amountCredit > 0)
                {
                    if (IsNumeric(taxObj.innerHTML))
                    {
                        itemTaxCredit = (amountCredit / parseFloat(amountObj.innerHTML)) * parseFloat(taxObj.innerHTML);
                        itemTaxCredit = roundNumber(itemTaxCredit);
                    }
                }
                
                taxCreditObj.value = (new Number(itemTaxCredit)).toFixed(2);
                
                //update running totals
                creditObject.ItemAmountCreditTotal += amountCredit;
                creditObject.ItemTaxCreditTotal += itemTaxCredit;
            }
            
            //update total for display
            itemAmountCreditTotalObj.innerHTML = (new Number(roundNumber(creditObject.ItemAmountCreditTotal))).toFixed(2);
            itemTaxCreditTotalObj.innerHTML = (new Number((creditObject.ItemTaxCreditTotal))).toFixed(2);
            creditTotalObj.innerHTML = (new Number(roundNumber(creditObject.ItemAmountCreditTotal + creditObject.ItemTaxCreditTotal))).toFixed(2);
            
        
        }
        catch (e) {
            alert(e);
        }
    }
    
    function TotalChanging(ms)
    {
        var itemAmountCreditTotalObj = document.getElementById('ItemAmountCreditTotal');
        var itemTaxCreditTotalObj = document.getElementById('ItemTaxCreditTotal');
        var creditTotalObj = document.getElementById('CreditTotal');
        
        itemAmountCreditTotalObj.className = "changing";
        itemTaxCreditTotalObj.className = "changing";
        creditTotalObj.className = "changing";
        
        setTimeout("TotalChanged()", ms);
        
    }
    
    function TotalChanged()
    {
        var itemAmountCreditTotalObj = document.getElementById('ItemAmountCreditTotal');
        var itemTaxCreditTotalObj = document.getElementById('ItemTaxCreditTotal');
        var creditTotalObj = document.getElementById('CreditTotal');
        
        itemAmountCreditTotalObj.className = "total";
        itemTaxCreditTotalObj.className = "total";
        creditTotalObj.className = "total";
    }

    function ConfirmCredit() {
        var strMessage = 'Are you sure that you want to credit this order with $' + roundNumber(creditObject.ItemAmountCreditTotal + creditObject.ItemTaxCreditTotal);
        if (confirm(strMessage)) {
            return true;
        }
        else {
            return false;
        }
    }

    function ConfirmVoid() {
        var strMessage = 'Are you sure that you want to void this order with $' + creditObject.AmountTotal;
        if (confirm(strMessage)) {
            return true;
        }
        else {
            return false;
        }
    }
    
    function ConfirmCancel() {
        var strMessage = 'Are you sure that you want to cancel?';
        if (confirm(strMessage)) {
            window.location='/';
            return true;
        }
        else {
            return false;
        }
    }
    
    function roundNumber(rnum) {
        var newnumber = Math.round(Math.abs(rnum)*100)/100; //2 decimals
        return newnumber
    }

    function IsNumeric(object_value) {
        //Returns true if value is a number or is NULL
        //otherwise returns false	

        if (object_value.length == 0) {
            return false;
        }

        //	Returns true if value is a number defined as
        // 	having at most 1 decimal point.
        //	otherwise containing only the characters 0-9.
        var start_format = " -.0123456789";
        var number_format = " .0123456789";
        var check_char;
        var decimal = false;
        var trailing_blank = false;
        var digits = false;

        //The first character can be .  blank or a digit.
        check_char = start_format.indexOf(object_value.charAt(0))
        //Was it a decimal?
        if (check_char == 1) {
            decimal = true;
        } else if (check_char < 1) {
            return false;
        }

        //Remaining characters can be only . or a digit, but only one decimal.
        for (var i = 1; i < object_value.length; i++) {
            check_char = number_format.indexOf(object_value.charAt(i))
            if (check_char < 0) {
                return false;
            } else if (check_char == 1) {
                if (decimal) {		// Second decimal.
                    return false;
                } else {
                    decimal = true;
                }
            } else if (check_char == 0) {
                if (decimal || digits) {
                    trailing_blank = true;
                }
                // ignore leading blanks
            } else if (trailing_blank) {
                return false;
            } else {
                digits = true;
            }
        }
        //All tests passed, so...
        return true
    }

    var creditObject = new function() {
        this.ItemAmountTotal = <%=Model.ItemAmountTotal.HasValue ? Model.ItemAmountTotal.Value.ToString("N2") : "0" %>;
        this.ItemAmountCreditTotal = <%=Model.ItemAmountCreditTotal.HasValue ? Model.ItemAmountCreditTotal.Value.ToString("N2") : "0"%>;
        this.ItemTaxTotal = <%=Model.ItemTaxTotal.HasValue ? Model.ItemTaxTotal.Value.ToString("N2") : "0"%>;
        this.ItemTaxCreditTotal = <%=Model.ItemTaxCreditTotal.HasValue ? Model.ItemTaxCreditTotal.Value.ToString("N2") : "0"%>;
        this.AmountTotal = <%= Model.OrderInfo.TotalAmount.ToString("N2")%>;
        
        this.ItemDetailIDs = [];
    }
    
    <%
        int i = 0;
        foreach (CreditTool.PurchaseService.OrderDetailInfo orderDetailInfo in Model.OrderInfo.OrderDetail)
        { %>
            creditObject.ItemDetailIDs[<%=i%>] = <%=orderDetailInfo.OrderDetailID%>;
        <%
            i++;
        } %>
    
    
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div>
        <%if (Model.TransactionTypeAvailable == Spark.UnifiedPurchaseSystem.Lib.Common.TransactionType.Credit)
          { %>
        <h2>Credit Order</h2>
        Review the order and process a credit.  Please confirm that this is the correct order before proceding.
        <%}
          else
          { %>
        <h2>Void Order</h2>
        Review the order and process a void.  Please confirm that this is the correct order before proceding.
        <%} %>

        
        <br />&nbsp;
        
        <%= Html.ValidationSummary("Please correct the following errors and retry:")%>
        <!--Order Header-->
        <div>
            <table>
                <tr>
                    <td>Date: </td>
                    <td><%= Model.OrderInfo.InsertDate%></td>
                </tr>
                <tr>
                    <td>Amount: </td>
                    <td><%= Model.OrderInfo.TotalAmount.ToString("C") %></td>
                </tr>
                <tr>
                    <td>Member ID: </td>
                    <td><%= Model.OrderInfo.CustomerID%></td>
                </tr>
                <tr>
                    <td>System: </td>
                    <td><%= Model.OrderInfo.CallingSystemID%></td>
                </tr>
                <tr>
                    <td>Order Number: </td>
                    <td><%= Model.OrderInfo.OrderID%></td>
                </tr>
                <tr>
                    <td>Order Type:</td>
                    <td><%= CreditTool.Models.CreditHelper.GetOrderTypeAsString(Model.OrderInfo) %></td>
                </tr>
                <tr>
                    <td>Payment ID: </td>
                    <td><%= Model.ChargeID %></td>
                </tr>
            </table>
        </div>
        
    </div>
    
    <!--Credit Order Form-->
    <div>
        <h2>Order Summary</h2>
        <br />&nbsp;
        <div>
            <table>
                <tr>
                    <th>PackageID</th>
                    <th>ItemID</th>
                    <th>Description</th>
                    <th>Amount</th>
                    <th>Amount Credit</th>
                    <th>Tax</th>
                    <th>Tax Credit</th>
                    <th>Duration</th>
                    <th>Duration Type</th>
                </tr>
                <%foreach (CreditTool.PurchaseService.OrderDetailInfo orderDetailInfo in Model.OrderInfo.OrderDetail)
                  { %>
                  
                  <tr>
                    <td><%= orderDetailInfo.PackageID%></td>
                    <td><%= orderDetailInfo.ItemID%></td>
                    <td><%= orderDetailInfo.ItemDescription %></td>
                    <td>$<span id="<%="Amount" + orderDetailInfo.OrderDetailID.ToString()%>"><%= orderDetailInfo.Amount.ToString("N2")%></span></td>
                    <td>
                        <%if (Model.TransactionTypeAvailable == Spark.UnifiedPurchaseSystem.Lib.Common.TransactionType.Credit)
                          { %>
                                <%= Html.TextBox("AmountCredit" + orderDetailInfo.OrderDetailID.ToString(), (Model.OrderDetailInfoCredit.ContainsKey(orderDetailInfo.OrderDetailID) && Model.OrderDetailInfoCredit[orderDetailInfo.OrderDetailID].Amount != Matchnet.Constants.NULL_DECIMAL) ? Model.OrderDetailInfoCredit[orderDetailInfo.OrderDetailID].Amount.ToString("N2") : "", new { onblur = "javascript:UpdateCredit(this)", Class="input-amount" })%>
                                <%= Html.ValidationMessage("AmountCredit" + orderDetailInfo.OrderDetailID.ToString(), "*") %>
                        <%}
                        else
                          { %>
                                <%= Html.TextBox("AmountCredit" + orderDetailInfo.OrderDetailID.ToString(), orderDetailInfo.Amount.ToString("N2"), new { ReadOnly = "true", Class="readonly" })%>
                          <%} %>
                    </td>
                    <td>$<span id="<%="Tax" + orderDetailInfo.OrderDetailID.ToString()%>"><%= CreditTool.Models.CreditHelper.CalculateTaxTotal(orderDetailInfo).ToString("N2")%></span></td>
                    <td>
                        <%if (Model.TransactionTypeAvailable == Spark.UnifiedPurchaseSystem.Lib.Common.TransactionType.Credit)
                          { %>
                                <%= Html.TextBox("TaxCredit" + orderDetailInfo.OrderDetailID.ToString(), (Model.OrderDetailInfoCredit.ContainsKey(orderDetailInfo.OrderDetailID) && Model.OrderDetailInfoCredit[orderDetailInfo.OrderDetailID].OrderTax != null && Model.OrderDetailInfoCredit[orderDetailInfo.OrderDetailID].OrderTax.Length > 0) ? Model.OrderDetailInfoCredit[orderDetailInfo.OrderDetailID].OrderTax[0].Amount.ToString("N2") : "", new { ReadOnly = "true", Class = "readonly input-amount" })%>
                        <%}
                          else
                          { %>
                                <%= Html.TextBox("TaxCredit" + orderDetailInfo.OrderDetailID.ToString(), CreditTool.Models.CreditHelper.CalculateTaxTotal(orderDetailInfo).ToString("N2"), new { ReadOnly = "true", Class="readonly input-amount" })%>
                          <%} %>
                    </td>
                    <td>
                        <%if (Model.TransactionTypeAvailable == Spark.UnifiedPurchaseSystem.Lib.Common.TransactionType.Credit)
                          { %>
                        <%= Html.TextBox("Duration" + orderDetailInfo.OrderDetailID.ToString(), (Model.OrderDetailInfoCredit.ContainsKey(orderDetailInfo.OrderDetailID) && Model.OrderDetailInfoCredit[orderDetailInfo.OrderDetailID].Duration != Matchnet.Constants.NULL_INT) ? Model.OrderDetailInfoCredit[orderDetailInfo.OrderDetailID].Duration.ToString() : "", new { Class = "input-amount" })%>
                        <%= Html.ValidationMessage("Duration" + orderDetailInfo.OrderDetailID.ToString(), "*")%>
                        <%}
                          else
                          { %>
                          <%= Html.TextBox("Duration" + orderDetailInfo.OrderDetailID.ToString(), (Model.OrderDetailInfoCredit.ContainsKey(orderDetailInfo.OrderDetailID) && Model.OrderDetailInfoCredit[orderDetailInfo.OrderDetailID].Duration != Matchnet.Constants.NULL_INT) ? Model.OrderDetailInfoCredit[orderDetailInfo.OrderDetailID].Duration.ToString() : "", new { ReadOnly = "true", Class = "readonly input-amount" })%>
                          <%} %>
                    </td>
                    <td>
                        <%= Html.DropDownList("DurationTypeID" + orderDetailInfo.OrderDetailID.ToString(), Model.GetDurationTypeList((Model.OrderDetailInfoCredit.ContainsKey(orderDetailInfo.OrderDetailID) && Model.OrderDetailInfoCredit[orderDetailInfo.OrderDetailID].DurationTypeID != Matchnet.Constants.NULL_INT) ? Model.OrderDetailInfoCredit[orderDetailInfo.OrderDetailID].DurationTypeID : 0), new { Class = "DurationType" })%>
                        <%= Html.ValidationMessage("DurationTypeIDError" + orderDetailInfo.OrderDetailID.ToString(), "*")%>
                    </td>
                </tr>
                <%} %>
                <tr>
                    <td colspan="3"><span class="total">Total:</span></td>
                    <td>$<span id="ItemAmountTotal"><%=Model.ItemAmountTotal.HasValue ? Model.ItemAmountTotal.Value.ToString("N2") : "0" %></span></td>
                    <td>$<span id="ItemAmountCreditTotal" class="total"><%=Model.ItemAmountCreditTotal.HasValue ? Model.ItemAmountCreditTotal.Value.ToString("N2") : "0"%></span></td>
                    <td>$<span id="ItemTaxTotal"><%=Model.ItemTaxTotal.HasValue ? Model.ItemTaxTotal.Value.ToString("N2") : "0"%></span></td>
                    <td>$<b><span id="ItemTaxCreditTotal" class="total"><%=Model.ItemTaxCreditTotal.HasValue ? Model.ItemTaxCreditTotal.Value.ToString("N2") : "0"%></span></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="9">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="9"><span class="total">Total Credit (Amount + Tax):</span> $<span id="CreditTotal" class="total"><%=Model.OrderCreditTotal.HasValue ? Model.OrderCreditTotal.Value.ToString("N2") : "0"%></span></td>
                </tr>
                <tr>
                    <td colspan="9"><span class="total">Select a Reason:</span> <%= Html.DropDownList("OrderReasonID", Model.OrderReasonsListItems) %><%= Html.ValidationMessage("OrderReasonIDError", "*")%></td>
                </tr>
                <tr><td colspan="9">&nbsp;</td></tr>
                <tr>
                    <td colspan="5">
                    All credits will be in the currency of the original purchase.
                    </td>
                    <td colspan="4" align="right">
                    <input type="button"name="ProcessCancel" id="ProcessCancel" value="Cancel" onclick="return ConfirmCancel();" />&nbsp;&nbsp;
                    <%if (Model.IsOrderValidForCredit)
                      { %>
                      <% if (Model.TransactionTypeAvailable == Spark.UnifiedPurchaseSystem.Lib.Common.TransactionType.Void)
                         {%>
                        <input type="submit" name="ProcessVoid" id="ProcessVoid" value="Process Void" onclick="return ConfirmVoid();" />
                        <%}
                         else if (Model.TransactionTypeAvailable == Spark.UnifiedPurchaseSystem.Lib.Common.TransactionType.Credit)
                         {%>
                        <input type="submit" name="ProcessCredit" id="ProcessCredit" value="Process Credit" onclick="return ConfirmCredit();" />
                        <%} %>
                    <%} %>
                    </td>
                </tr>
            </table>
        </div>
    
    </div>
    <p>&nbsp;</p>
</asp:Content>


