<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<CreditTool.Models.ModelView.CreditConfirmation>" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

        function popUpPf() {

            var confirmationPf = window.open("/Credit/ConfirmationPF/", "ConfirmationWin", "width=600,height=600,scrollbars=1,resizable=1,location=no,menubar=yes")

        } 

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="printerdisplay"><a href="#" onclick="javascript:popUpPf();return false;">Printer-Friendly <img src="<%= ResolveUrl("~/Images/print.gif")%>" alt="Printer Friendly" border="0" /></a></div>
    <div id="confirmation">
        <h2>Credit Order Confirmation</h2>
        
        <% if (Model.CreditOrderResponse.InternalResponse.InternalResponseCode == "0")
           { %>
        <div>
        The following credit has been applied.  The system has been notified.  You should print this for your records.
        </div>
        <%}
           else
           {%>
           <div class="error">
           An error has occurred while processing the request.<br />
           <%= "Response Code: " + Model.CreditOrderResponse.InternalResponse.InternalResponseCode %><br />
           <%= "Response Description: " + Model.CreditOrderResponse.InternalResponse.InternalResponseDescription %>
           </div>
           <%} %>
        
        <br />&nbsp;
        
        <% if (Model.CreditOrderResponse.OrderInfo != null && Model.CreditOrderResponse.OrderInfo.OrderID > 0)
           {%>
        <!--Credit Order Summary-->
        <div>
            <table>
                <tr>
                    <td>Date: </td>
                    <td><%= Model.CreditOrderResponse.OrderInfo.InsertDate%></td>
                </tr>
                <tr>
                    <td>Order Status: </td>
                    <td><b><%= (Spark.UnifiedPurchaseSystem.Lib.Common.OrderStatus)Enum.Parse(typeof(Spark.UnifiedPurchaseSystem.Lib.Common.OrderStatus), Model.CreditOrderResponse.OrderInfo.OrderStatusID.ToString())%></b></td>
                </tr>
                <tr>
                    <td>Order Type: </td>
                    <td><b><%= CreditTool.Models.CreditHelper.GetOrderTypeAsString(Model.CreditOrderResponse.OrderInfo)%></b></td>
                </tr>
                <tr>
                    <td>Order Number: </td>
                    <td><%= Model.CreditOrderResponse.OrderInfo.OrderID%></td>
                </tr>
                <tr>
                    <td>Original Order Number: </td>
                    <td><%= Model.CreditOrderResponse.OrderInfo.ReferenceOrderID%></td>
                </tr>
                <tr>
                    <td>MemberID: </td>
                    <td><%= Model.CreditOrderResponse.OrderInfo.CustomerID%></td>
                </tr>
                <tr>
                    <td>System: </td>
                    <td><%= Model.CreditOrderResponse.OrderInfo.CallingSystemID%></td>
                </tr>
                <tr>
                    <td>Credited Amount: </td>
                    <td><b><%= Model.CreditOrderResponse.OrderInfo.TotalAmount.ToString("C")%></b></td>
                </tr>
                <tr>
                    <td>Admin: </td>
                    <td><%= Model.CreditOrderResponse.OrderInfo.AdminUserName%></td>
                </tr>
                
            </table>
        </div>
        <%} %>
        <p>&nbsp;</p>
    </div>
</asp:Content>

