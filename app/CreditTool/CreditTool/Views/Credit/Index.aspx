<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<CreditTool.Models.ModelView.OrderSearch>" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    
    <div>
        <h2>Search for Orders</h2>
        You can search for orders by member ID (preferred), transaction or the merchant order number.
        <p>
            <b><a href="<%= ViewData["VirtualTerminalCCSearchURL"] %>" target="_search">Click here to search transactions by Credit Card</a></b>
        </p>
        
        <!--Order Search Form-->
        <div>
            <%= Html.ValidationSummary("Please correct the following errors and retry:")%>
            <table>
                <tr>
                    <td><label for="MemberID">Member ID:</label></td>
                    <td><%= Html.TextBox("MemberID")%></td>
                    <td><%= Html.ValidationMessage("MemberID", "*")%></td>
                </tr>
                <tr>
                    <td><label for="Systems">System:</label></td>
                    <td><%= Html.DropDownList("CallingSystemID", Model.CallingSystemsList)%></td>
                    <td><%= Html.ValidationMessage("CallingSystemIDError", "*")%></td>
                </tr>
                <tr>
                    <td><label for="OrderID">Order Number:</label></td>
                    <td><%= Html.TextBox("OrderID")%></td>
                    <td><%= Html.ValidationMessage("OrderID", "*")%></td>
                </tr>
                <tr>
                    <td><label for="TransactionID">Payment ID:</label></td>
                    <td><%= Html.TextBox("TransactionID")%></td>
                    <td><%= Html.ValidationMessage("TransactionID", "*")%></td>
                </tr>
                <tr>
                    <td colspan="3"><input type='submit' value="Search" /></td>
                </tr>
            </table>
        </div>
    </div>
    
    <!--Search Results-->
    <% if (Model.OrderInfoList != null)
       { %>
    <div>
        <h2>Matching Orders</h2>
        
        <%if (Model.OrderInfoList.Count <= 0)
          { %>
        <span class="error">There are no Orders that match your search criteria.  Please try again.</span>
        <br />&nbsp;
        <%}
          else
          { %>
        <span>The following orders matched your search criteria.  Click on the Order Number to credit the order.</span>
        <br />&nbsp;
        
        <div>
            <table>
                <tr>
                    <th>Date</th>
                    <th>Member ID</th>
                    <th>System</th>
                    <th>Amount</th>
                    <th>Order Number</th>
                    <th>Order Status</th>
                </tr>
                <%foreach (CreditTool.OrderHistoryService.OrderInfo orderInfo in Model.OrderInfoList)
                  { %>
                  
                  <tr>
                    <td><%= orderInfo.InsertDate%></td>
                    <td><%= orderInfo.CustomerID.ToString()%></td>
                    <td><%= (Spark.UnifiedPurchaseSystem.Lib.Common.System)Enum.Parse(typeof(Spark.UnifiedPurchaseSystem.Lib.Common.System), orderInfo.CallingSystemID.ToString())%></td>
                    <td><%= orderInfo.TotalAmount.ToString("C")%></td>
                    <td><%= Html.ActionLink(orderInfo.OrderID.ToString(), "CreditOrder", new { id = orderInfo.OrderID.ToString() })%></td>
                    <td><%= (Spark.UnifiedPurchaseSystem.Lib.Common.OrderStatus)Enum.Parse(typeof(Spark.UnifiedPurchaseSystem.Lib.Common.OrderStatus), orderInfo.OrderStatusID.ToString()) %></td>
                </tr>
                
                <%} %>
            </table>
        </div>
        <%} %>
    
    </div>
    <%} %>
    
    <p>&nbsp;</p>
</asp:Content>


