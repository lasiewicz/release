﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CreditTool
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //general controller mapping
            routes.MapRoute(
                "Default",                                              // Route name
                "{controller}/{action}/{id}",                           // URL with parameters
                new { controller = "Credit", action = "Index", id = "" }  // Parameter defaults
            );

            //handles application root
            //TODO: Should we delete the Default.aspx?
            routes.MapRoute(
                "Root",
                "",
                new { controller = "Credit", action = "Index", id = "" }
                );

        }

        protected void Application_Start()
        {
            RegisterRoutes(RouteTable.Routes);

        }

        protected void Application_EndRequest(Object sender, EventArgs e)
        {
            Models.Adapter.OrderHistoryServiceAdapter.CloseProxyInstance();
            Models.Adapter.PaymentProfileMapperServiceAdapter.CloseProxyInstance();
            Models.Adapter.PurchaseServiceAdapter.CloseProxyInstance();
        }

        
    }
}