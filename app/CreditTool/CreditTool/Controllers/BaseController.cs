using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Configuration;

namespace CreditTool.Controllers
{
    /// <summary>
    /// Base Controller to allow for common initializations that may be needed later for all requests.
    /// </summary>
    public class BaseController : Controller
    {
        protected int applicationOperationID = (int)CreditTool.Models.Enums.CreditToolOperation.CreditOrVoid;

        public BaseController() : base()
        {
            //add common initializations here
            ViewData["VirtualTerminalURL"] = ConfigurationManager.AppSettings["VirtualTerminalURL"];
            ViewData["VirtualTerminalCCSearchURL"] = ConfigurationManager.AppSettings["VirtualTerminalCCSearchURL"];
        }

    }
}
