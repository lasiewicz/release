using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using CreditTool.Models;
using CreditTool.Models.Adapter;
using CreditTool.PurchaseService;
using System.Threading;
using CreditTool.Models.ModelView;

namespace CreditTool.Controllers
{
    /// <summary>
    /// Controller for all Credit Related Requests
    /// </summary>
    [Authorize]
    public class CreditController : BaseController
    {
        //
        // GET: /Credit/
        /// <summary>
        /// This handles request for the order search page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            try
            {
                ActiveDirectoryHelper securityHelper = new ActiveDirectoryHelper();
                if (!securityHelper.CheckUserAccess(HttpContext.User.Identity, applicationOperationID))
                {
                    return RedirectToAction("ErrorAccess");
                }

                OrderSearch orderSearchModelView = new OrderSearch();

                string paymentID = Request.Params["paymentid"];
                if (paymentID != null)
                {
                    OrderSearch orderSearch = new OrderSearch();
                    orderSearch.TransactionID = Convert.ToInt32(paymentID);
                    orderSearchModelView = CreditBL.SearchOrderHistory(orderSearch);
                }

                return View(orderSearchModelView);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("ErrorMessage", ExceptionHelper.GetExceptionMessage(ex, ExceptionHelper.ExceptionMode.Html));
                return View("Error");
            }
        }

        //
        // POST: /Credit/
        /// <summary>
        /// This handles the request for performing an order search
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Index(FormCollection formValues)
        {
            try
            {
                ActiveDirectoryHelper securityHelper = new ActiveDirectoryHelper();
                if (!securityHelper.CheckUserAccess(HttpContext.User.Identity, applicationOperationID))
                {
                    return RedirectToAction("ErrorAccess");
                }

                OrderSearch orderSearchModelView = new OrderSearch();

                //validate search fields
                orderSearchModelView.ValidateSearchFields(formValues, ModelState);

                if (ModelState.IsValid)
                {
                    //perform search
                    orderSearchModelView = CreditBL.SearchOrderHistory(orderSearchModelView);
                }

                //display results
                return View(orderSearchModelView);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("ErrorMessage", ExceptionHelper.GetExceptionMessage(ex, ExceptionHelper.ExceptionMode.Html));
                return View("Error");
            }
        }


        //
        // GET: /Credit/CreditOrder
        /// <summary>
        /// This handles the request for credit order page
        /// </summary>
        /// <returns></returns>
        public ActionResult CreditOrder(int? id)
        {
            try
            {
                ActiveDirectoryHelper securityHelper = new ActiveDirectoryHelper();
                if (!securityHelper.CheckUserAccess(HttpContext.User.Identity, applicationOperationID))
                {
                    return RedirectToAction("ErrorAccess");
                }

                if (id.HasValue)
                {
                    CreditOrder creditOrderModelView = new CreditOrder(id);

                    //get Order Info
                    creditOrderModelView = CreditBL.GetOrderInfo(creditOrderModelView);
                    creditOrderModelView = CreditBL.GetVoidEligibility(creditOrderModelView);

                    //validate Order
                    creditOrderModelView.ValidateOrder(ModelState);
                    if (!ModelState.IsValid)
                    {
                        return View("Error");
                    }

                    //initialize totals
                    creditOrderModelView.InitializeTotals();

                    //display results
                    return View(creditOrderModelView);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("ErrorMessage", ExceptionHelper.GetExceptionMessage(ex, ExceptionHelper.ExceptionMode.Html));
                return View("Error");
            }
        }

        //
        // POST: /Credit/CreditOrder
        /// <summary>
        /// This handles the request for processing a Credit/Void
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreditOrder(FormCollection formValues, int? id)
        {
            try
            {
                ActiveDirectoryHelper securityHelper = new ActiveDirectoryHelper();
                if (!securityHelper.CheckUserAccess(HttpContext.User.Identity, applicationOperationID))
                {
                    return RedirectToAction("ErrorAccess");
                }

                if (id.HasValue)
                {
                    CreditOrder creditOrderModelView = new CreditOrder(id);
                    CreditConfirmation creditConfirmationModelView = new CreditConfirmation();

                    //get Order Info
                    creditOrderModelView = CreditBL.GetOrderInfo(creditOrderModelView);
                    creditOrderModelView = CreditBL.GetVoidEligibility(creditOrderModelView);
                    
                    //validate Order
                    creditOrderModelView.ValidateOrder(ModelState);
                    if (!ModelState.IsValid)
                    {
                        return View("Error");
                    }

                    //initialize totals
                    creditOrderModelView.InitializeTotals();

                    //validate Credit Fields
                    if (creditOrderModelView.TransactionTypeAvailable == TransactionType.Credit)
                    {
                        creditOrderModelView.ValidateCreditOrderFields(formValues, ModelState);
                    }
                    else
                    {
                        creditOrderModelView.ValidateVoidOrderFields(formValues, ModelState);
                    }

                    if (!ModelState.IsValid)
                    {
                        //Redisplay Credit Page with error message 
                        return View(creditOrderModelView);
                    }

                    //Process Credit or Void Order
                    CreditOrderResponse creditOrderResponse = CreditBL.ProcessCreditTransaction(creditOrderModelView);

                    //display confirmation
                    creditConfirmationModelView.CreditOrderResponse = creditOrderResponse;
                    return View("CreditOrderConfirmation", creditConfirmationModelView);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("ErrorMessage", ExceptionHelper.GetExceptionMessage(ex, ExceptionHelper.ExceptionMode.Html));
                return View("Error");
            }
        }

        //
        // GET: /ConfirmationPf/
        /// <summary>
        /// This handles request for confirmation printer-friendly
        /// </summary>
        /// <returns></returns>
        public ActionResult ConfirmationPF()
        {
            ActiveDirectoryHelper securityHelper = new ActiveDirectoryHelper();
            if (!securityHelper.CheckUserAccess(HttpContext.User.Identity, applicationOperationID))
            {
                return RedirectToAction("ErrorAccess");
            }

            return View("ConfirmationPf");
        }

        //
        // GET: /ErrorAccess/
        /// <summary>
        /// </summary>
        /// <returns></returns>
        public ActionResult ErrorAccess()
        {
            ModelState.AddModelError("ErrorMessage", "Your account does not have the required authorization to access the Credit Tool.  Please check with Systems.");
            return View("Error");
        }

    }
}
