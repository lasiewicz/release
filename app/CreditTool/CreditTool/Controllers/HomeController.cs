using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace CreditTool.Controllers
{
    /// <summary>
    /// Home controller
    /// Note: We don't need this now for Credit Tool but if it later is expanded with other admin features then this
    /// can process the home pages.
    /// </summary>
    [HandleError]
    public class HomeController : BaseController
    {
        [Authorize]
        public ActionResult Index()
        {
            ViewData["Message"] = "Admin Credit Tool";
            return View();
        }

        [Authorize]
        public ActionResult About()
        {
            return View();
        }
    }
}
