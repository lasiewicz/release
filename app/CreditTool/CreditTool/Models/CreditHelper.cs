﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using CreditTool.Models.Adapter;
using CreditTool.PurchaseService;
using System.Text;
using CreditTool.Models.ModelView;

namespace CreditTool.Models
{
    public class CreditHelper
    {
        public static List<SelectListItem> GetOrderReasons(int? selectedID)
        {
            List<SelectListItem> _OrderReasonsListItems = new List<SelectListItem>();

            //populate OrderReasons
            List<KeyValuePair<OrderReasons, string>> OrderReasonsList = EnumDescriptions.GetOrderReasonsDescription();
            foreach (KeyValuePair<OrderReasons, string> reason in OrderReasonsList)
            {
                SelectListItem newItem = new SelectListItem() { Text = reason.Value, Value = Convert.ToInt32(reason.Key).ToString() };
                if (selectedID.HasValue && Convert.ToInt32(reason.Key) == selectedID.Value)
                {
                    newItem.Selected = true;
                }
                _OrderReasonsListItems.Add(newItem);
            }

            return _OrderReasonsListItems;
        }

        public static List<SelectListItem> GetSystems(int? selectedID)
        {
            List<SelectListItem> _SystemListItems = new List<SelectListItem>();

            //populate Systems
            _SystemListItems.Add(new SelectListItem() { Text = "Select a system", Value = "0" });
            List<KeyValuePair<Spark.UnifiedPurchaseSystem.Lib.Common.System, string>> SystemList = EnumDescriptions.GetSystemDescription();
            foreach (KeyValuePair<Spark.UnifiedPurchaseSystem.Lib.Common.System, string> system in SystemList)
            {
                SelectListItem newItem = new SelectListItem() { Text = system.Value, Value = Convert.ToInt32(system.Key).ToString() };
                if (selectedID.HasValue && Convert.ToInt32(system.Key) == selectedID.Value)
                {
                    newItem.Selected = true;
                }
                _SystemListItems.Add(newItem);
            }

            return _SystemListItems;
        }

        public static List<SelectListItem> GetDurationType(int? selectedID)
        {
            List<SelectListItem> _DurationTypeListItems = new List<SelectListItem>();

            //populate duration type
            List<KeyValuePair<Spark.UnifiedPurchaseSystem.Lib.Common.DurationType, string>> DurationTypeList = EnumDescriptions.GetDurationTypeDescription();
            foreach (KeyValuePair<Spark.UnifiedPurchaseSystem.Lib.Common.DurationType, string> durationType in DurationTypeList)
            {
                SelectListItem newItem = new SelectListItem() { Text = durationType.Value, Value = Convert.ToInt32(durationType.Key).ToString() };
                if (selectedID.HasValue && Convert.ToInt32(durationType.Key) == selectedID.Value)
                {
                    newItem.Selected = true;
                }

                _DurationTypeListItems.Add(newItem);
            }

            return _DurationTypeListItems;
        }

        public static decimal CalculateTaxTotal(OrderDetailInfo detailInfo)
        {
            decimal taxTotal = 0m;

            if (detailInfo.OrderTax != null)
            {
                foreach (OrderTaxInfo taxInfo in detailInfo.OrderTax)
                {
                    //these tax amounts returned from purchase should already be rounded to 2 decimal, but this is just in case we have bad data
                    taxTotal += taxInfo.Amount;
                }
            }

            return taxTotal;
        }

        public static int? GetChargeIDForCrediting(OrderInfo orderInfo)
        {
            
            int? chargeID = null;

            if (orderInfo != null && orderInfo.OrderUserPayment != null)
            {
                foreach (OrderUserPaymentInfo paymentInfo in orderInfo.OrderUserPayment)
                {
                    if (paymentInfo.ChargeTypeID == 1) //billing
                    {
                        chargeID = paymentInfo.ChargeID;
                        break;
                    }
                }
            }

            return chargeID;
        }

        public static List<TransactionType> GetOrderType(OrderInfo orderInfo)
        {
            List<TransactionType> orderTypes = new List<TransactionType>();

            if (orderInfo != null && orderInfo.OrderDetail != null)
            {
                foreach (OrderDetailInfo detailInfo in orderInfo.OrderDetail)
                {
                    TransactionType orderType = (TransactionType)Enum.Parse(typeof(TransactionType), detailInfo.OrderTypeID.ToString());
                    if (!orderTypes.Contains(orderType))
                    {
                        orderTypes.Add(orderType);
                    }
                }
            }

            return orderTypes;
        }

        public static string GetOrderTypeAsString(OrderInfo orderInfo)
        {
            StringBuilder sb = new StringBuilder();

            List<TransactionType> list = GetOrderType(orderInfo);
            foreach (TransactionType t in list)
            {
                sb.Append(", " + t.ToString());
            }

            if (sb.ToString().Length > 2)
                return sb.ToString().Substring(2);
            else
                return sb.ToString();
        }
        
    }
}
