﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using Microsoft.Interop.Security.AzRoles;
using System.Configuration;

namespace CreditTool.Models
{
    public class ActiveDirectoryHelper
    {
        private const string applicationName = "Credit Tool";

        public bool CheckUserAccess(IIdentity User, int applicationOperationID)
        {
            WindowsIdentity user = (WindowsIdentity)User;
            bool result = false;
            if (VerifyUserAccess(user, applicationOperationID) == 0)
            {
                result = true;
            }
            return result;
        }

        private int VerifyUserAccess(WindowsIdentity User, int applicationOperationID)
        {
            AzAuthorizationStore store = new AzAuthorizationStoreClass();
            store.Initialize(0, ConfigurationManager.ConnectionStrings["ActiveDirectoryAzMan"].ConnectionString, null);
            IAzApplication app = store.OpenApplication(applicationName, null);

            IntPtr token = User.Token;

            IAzClientContext ctx = app.InitializeClientContextFromToken((ulong)token.ToInt64(), null);

            int result = AccessCheck(ctx, "Check Operation Access", applicationOperationID);
            return result;
        }

        private int AccessCheck(IAzClientContext ctx, string objectName, int operation)
        {
            object[] ops = new object[] { (object)operation };

            object[] scopes = new object[] { (object)"" };

            object[] results = (object[])ctx.AccessCheck(objectName, scopes, ops, null, null, null, null, null);
            return (int)results[0];
        }
    }
}
