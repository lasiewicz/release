﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CreditTool.PurchaseService;
using CreditTool.Models.ModelView;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using CreditTool.Models.Adapter;

namespace CreditTool.Models
{
    public class CreditBL
    {
        public static OrderSearch SearchOrderHistory(OrderSearch orderSearch)
        {
            orderSearch.OrderInfoList = new List<CreditTool.OrderHistoryService.OrderInfo>();
            CreditTool.OrderHistoryService.OrderInfo[] orderInfos = OrderHistoryServiceAdapter.GetProxyInstance().SearchOrderHistory(orderSearch.MemberID.HasValue ? orderSearch.MemberID.Value : Constants.NULL_INT
                , orderSearch.CallingSystemID.HasValue ? orderSearch.CallingSystemID.Value : Constants.NULL_INT
                , orderSearch.OrderID.HasValue ? orderSearch.OrderID.Value : Constants.NULL_INT
                , orderSearch.TransactionID.HasValue ? orderSearch.TransactionID.Value : Constants.NULL_INT);

            if (orderInfos != null && orderInfos.Length > 0)
            {
                orderSearch.OrderInfoList.AddRange(orderInfos);
            }

            return orderSearch;
        }

        public static CreditOrder GetOrderInfo(CreditOrder creditOrder)
        {
            if (creditOrder.OrderID.HasValue && creditOrder.OrderID.Value > 0)
            {
                creditOrder.OrderInfo = ConvertOrderInfoBetweenNamespaces_OrderHistoryToPurchase(OrderHistoryServiceAdapter.GetProxyInstance().GetOrderInfo(creditOrder.OrderID.Value));

                if (creditOrder.OrderInfo != null && creditOrder.OrderInfo.OrderUserPayment != null)
                {
                    creditOrder.ChargeID = CreditHelper.GetChargeIDForCrediting(creditOrder.OrderInfo);
                }
            }

            return creditOrder;

        }

        public static OrderInfo ConvertOrderInfoBetweenNamespaces_OrderHistoryToPurchase(CreditTool.OrderHistoryService.OrderInfo orderInfo)
        {
            OrderInfo purchaseOrderInfo = null;
            if (orderInfo != null)
            {
                purchaseOrderInfo = new OrderInfo();
                purchaseOrderInfo.AdminUserID = orderInfo.AdminUserID;
                purchaseOrderInfo.AdminUserName = orderInfo.AdminUserName;
                purchaseOrderInfo.CallingSystemID = orderInfo.CallingSystemID;
                purchaseOrderInfo.CallingSystemTypeID = orderInfo.CallingSystemTypeID;
                purchaseOrderInfo.CurrencyID = orderInfo.CurrencyID;
                purchaseOrderInfo.CustomerID = orderInfo.CustomerID;
                purchaseOrderInfo.CustomerIP = orderInfo.CustomerIP;
                purchaseOrderInfo.Duration = orderInfo.Duration;
                purchaseOrderInfo.DurationTypeID = orderInfo.DurationTypeID;
                purchaseOrderInfo.InsertDate = orderInfo.InsertDate;
                purchaseOrderInfo.InternalResponseStatusID = orderInfo.InternalResponseStatusID;
                
                List<OrderDetailInfo> orderDetails = new List<OrderDetailInfo>();
                foreach (CreditTool.OrderHistoryService.OrderDetailInfo detail in orderInfo.OrderDetail)
                {
                    OrderDetailInfo purchaseOrderDetailInfo = new OrderDetailInfo();
                    purchaseOrderDetailInfo.Amount = detail.Amount;
                    purchaseOrderDetailInfo.CurrencyID = detail.CurrencyID;
                    purchaseOrderDetailInfo.Duration = detail.Duration;
                    purchaseOrderDetailInfo.DurationTypeID = detail.DurationTypeID;
                    purchaseOrderDetailInfo.InsertDate = detail.InsertDate;
                    purchaseOrderDetailInfo.ItemDescription = detail.ItemDescription;
                    purchaseOrderDetailInfo.ItemID = detail.ItemID;
                    purchaseOrderDetailInfo.OrderDetailID = detail.OrderDetailID;
                    purchaseOrderDetailInfo.OrderId = detail.OrderId;
                    List<OrderTaxInfo> orderTaxes = new List<OrderTaxInfo>();
                    foreach (CreditTool.OrderHistoryService.OrderTaxInfo taxInfo in detail.OrderTax)
                    {
                        OrderTaxInfo purchaseTaxInfo = new OrderTaxInfo();
                        purchaseTaxInfo.Amount = taxInfo.Amount;
                        purchaseTaxInfo.AuthorityCode = taxInfo.AuthorityCode;
                        purchaseTaxInfo.AuthorityID = taxInfo.AuthorityID;
                        purchaseTaxInfo.CurrencyID = taxInfo.CurrencyID;
                        purchaseTaxInfo.InsertDate = taxInfo.InsertDate;
                        purchaseTaxInfo.ItemTaxTypeID = taxInfo.ItemTaxTypeID;
                        purchaseTaxInfo.OrderDetailID = taxInfo.OrderDetailID;
                        purchaseTaxInfo.OrderTaxID = taxInfo.OrderTaxID;
                        purchaseTaxInfo.RegionID = taxInfo.RegionID;
                        orderTaxes.Add(purchaseTaxInfo);

                    }
                    purchaseOrderDetailInfo.OrderTax = orderTaxes.ToArray();
                    purchaseOrderDetailInfo.OrderTypeID = detail.OrderTypeID;
                    purchaseOrderDetailInfo.PackageID = detail.PackageID;
                    purchaseOrderDetailInfo.PromoID = detail.PromoID;
                    purchaseOrderDetailInfo.ReferenceOrderDetailID = detail.ReferenceOrderDetailID;
                    purchaseOrderDetailInfo.UpdateDate = detail.UpdateDate;
                    orderDetails.Add(purchaseOrderDetailInfo);

                }
                purchaseOrderInfo.OrderDetail = orderDetails.ToArray();
                purchaseOrderInfo.OrderID = orderInfo.OrderID;
                purchaseOrderInfo.OrderReasonID = orderInfo.OrderReasonID;
                purchaseOrderInfo.OrderStatusID = orderInfo.OrderStatusID;
                List<OrderUserPaymentInfo> orderUserPayments = new List<OrderUserPaymentInfo>();
                foreach (CreditTool.OrderHistoryService.OrderUserPaymentInfo paymentInfo in orderInfo.OrderUserPayment)
                {
                    OrderUserPaymentInfo purchaseOrderUserPaymentInfo = new OrderUserPaymentInfo();
                    purchaseOrderUserPaymentInfo.CallingSystemID = paymentInfo.CallingSystemID;
                    purchaseOrderUserPaymentInfo.CallingSystemTypeID = paymentInfo.CallingSystemTypeID;
                    purchaseOrderUserPaymentInfo.ChargeID = paymentInfo.ChargeID;
                    purchaseOrderUserPaymentInfo.ChargeStatusID = paymentInfo.ChargeStatusID;
                    purchaseOrderUserPaymentInfo.ChargeTypeID = paymentInfo.ChargeTypeID;
                    purchaseOrderUserPaymentInfo.InsertDate = paymentInfo.InsertDate;
                    purchaseOrderUserPaymentInfo.OrderID = paymentInfo.OrderID;
                    purchaseOrderUserPaymentInfo.UpdateDate = paymentInfo.UpdateDate;
                    orderUserPayments.Add(purchaseOrderUserPaymentInfo);

                }
                purchaseOrderInfo.OrderUserPayment = orderUserPayments.ToArray();
                purchaseOrderInfo.PmotionID = orderInfo.PmotionID;
                purchaseOrderInfo.PromoID = orderInfo.PromoID;
                purchaseOrderInfo.ReferenceOrderID = orderInfo.ReferenceOrderID;
                purchaseOrderInfo.RegionID = orderInfo.RegionID;
                purchaseOrderInfo.TotalAmount = orderInfo.TotalAmount;
                purchaseOrderInfo.UpdateDate = orderInfo.UpdateDate;
                purchaseOrderInfo.UserPaymentGuid = orderInfo.UserPaymentGuid;
            }

            return purchaseOrderInfo;
        }

        public static CreditOrder GetVoidEligibility(CreditOrder creditOrder)
        {
            //check if order can still be voided
            if (creditOrder.OrderInfo != null && creditOrder.OrderInfo.OrderUserPayment != null)
            {
                if (!creditOrder.ChargeID.HasValue)
                {
                    creditOrder.ChargeID = CreditHelper.GetChargeIDForCrediting(creditOrder.OrderInfo);
                }

                if (creditOrder.ChargeID.HasValue)
                {
                    if (PurchaseServiceAdapter.GetProxyInstance().IsCreditSetToVoid(creditOrder.ChargeID.Value, creditOrder.OrderInfo.CallingSystemID))
                    {
                        creditOrder.TransactionTypeAvailable = TransactionType.Void;
                    }
                }
            }

            return creditOrder;
        }

        public static CreditOrderResponse ProcessCreditTransaction(CreditOrder creditOrder)
        {
            if (!creditOrder.ChargeID.HasValue)
            {
                creditOrder.ChargeID = CreditHelper.GetChargeIDForCrediting(creditOrder.OrderInfo);
            }

            creditOrder.OrderInfo.CallingSystemTypeID = (int)SystemType.CreditTool;
            creditOrder.OrderInfo.AdminUserID = Constants.NULL_INT;
            creditOrder.OrderInfo.AdminUserName = HttpContext.Current.User.Identity.Name;

            return PurchaseServiceAdapter.GetProxyInstance().CreditOrderFromOrderInfo(creditOrder.OrderInfo, creditOrder.TransactionTypeAvailable, creditOrder.ChargeID.Value, 0);
        }
    }
}
