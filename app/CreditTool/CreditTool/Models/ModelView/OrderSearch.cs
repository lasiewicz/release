﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CreditTool.OrderHistoryService;
using System.Web.Mvc;
using CreditTool.Models.Adapter;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using System.Threading;
using System.Configuration;

namespace CreditTool.Models.ModelView
{
    public class OrderSearch
    {
        private List<OrderInfo> _OrderInfoList = null;
        private int? _MemberID = null;
        private int? _CallingSystemID = null;
        private int? _TransactionID = null;
        private int? _OrderID = null;

        public int? MemberID
        {
            get { return _MemberID; }
            set { _MemberID = value; }
        }

        public int? CallingSystemID
        {
            get { return _CallingSystemID; }
            set { _CallingSystemID = value; }
        }

        public int? TransactionID
        {
            get { return _TransactionID; }
            set { _TransactionID = value; }
        }

        public int? OrderID
        {
            get { return _OrderID; }
            set { _OrderID = value; }
        }
        
        public List<OrderInfo> OrderInfoList
        {
            get { return _OrderInfoList; }
            set { _OrderInfoList = value; }
        }

        public List<SelectListItem> CallingSystemsList
        {
            get
            {
                return CreditHelper.GetSystems(_CallingSystemID);
            }
        }

        public string CCSearchURL
        {
            get { return ConfigurationManager.AppSettings["VirtualTerminalCCSearchURL"]; }
        }


        #region Methods
        public void ValidateSearchFields(FormCollection formValues, ModelStateDictionary modelState)
        {
            _CallingSystemID = Convert.ToInt32(formValues["CallingSystemID"]);

            if (String.IsNullOrEmpty(formValues["MemberID"]) && String.IsNullOrEmpty(formValues["OrderID"])
                && String.IsNullOrEmpty(formValues["TransactionID"]))
            {
                modelState.AddModelError("SearchCriteriaRequired", "Please specify one or more search criterias.");
            }
            else
            {
                //MemberID
                if (!String.IsNullOrEmpty(formValues["MemberID"]))
                {
                    int memberID = Constants.NULL_INT;
                    if (int.TryParse(formValues["MemberID"], out memberID))
                    {
                        _MemberID = memberID;

                        //Required to specify SystemID
                        if (formValues["CallingSystemID"] == "0")
                        {
                            modelState.AddModelError("CallingSystemIDError", "Please select a System when specifying a MemberID.");
                        }

                    }
                    else
                    {
                        modelState.Add("MemberID", new ModelState() { Value = new ValueProviderResult(formValues["MemberID"], formValues["MemberID"], Thread.CurrentThread.CurrentCulture) });
                        modelState.AddModelError("MemberID", "MemberID is Invalid.");
                    }
                }

                //OrderID
                if (!String.IsNullOrEmpty(formValues["OrderID"]))
                {
                    int orderID = Constants.NULL_INT;
                    if (int.TryParse(formValues["OrderID"], out orderID))
                    {
                        _OrderID = orderID;
                    }
                    else
                    {
                        modelState.Add("OrderID", new ModelState() { Value = new ValueProviderResult(formValues["OrderID"], formValues["OrderID"], Thread.CurrentThread.CurrentCulture) });
                        modelState.AddModelError("OrderID", "OrderID is Invalid.");
                    }
                }

                //TransactionID
                if (!String.IsNullOrEmpty(formValues["TransactionID"]))
                {
                    int tranID = Constants.NULL_INT;
                    if (int.TryParse(formValues["TransactionID"], out tranID))
                    {
                        _TransactionID = tranID;
                    }
                    else
                    {
                        modelState.Add("TransactionID", new ModelState() { Value = new ValueProviderResult(formValues["TransactionID"], formValues["TransactionID"], Thread.CurrentThread.CurrentCulture) });
                        modelState.AddModelError("TransactionID", "TransactionID is Invalid.");
                    }
                }
            }

        }

        #endregion
    }
}
