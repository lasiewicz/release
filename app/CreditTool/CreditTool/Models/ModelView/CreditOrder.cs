﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CreditTool.PurchaseService;
using System.Web.Mvc;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using CreditTool.Models.Adapter;
using System.Threading;

namespace CreditTool.Models.ModelView
{
    public class CreditOrder
    {
        private OrderInfo _OrderInfo = null;
        private int? _OrderID = null;
        private bool _IsOrderValidForCredit = true;
        private TransactionType _TransactionType = TransactionType.Credit;
        private Dictionary<int, OrderDetailInfo> _OrderDetailInfoCredit = new Dictionary<int, OrderDetailInfo>();
        private int? _OrderReasonForCredit = null;
        private decimal? _OrderCreditTotal = null;
        private decimal? _ItemAmountTotal = null;
        private decimal? _ItemTaxTotal = null;
        private decimal? _ItemAmountCreditTotal = null;
        private decimal? _ItemTaxCreditTotal = null;
        private int? _ChargeID = null;
        private int? _ChargeTypeID = null;

        public int? OrderID
        {
            get { return _OrderID; }
            set { _OrderID = value; }
        }

        public int? OrderReasonForCredit
        {
            get { return _OrderReasonForCredit; }
            set { _OrderReasonForCredit = value; }
        }

        public OrderInfo OrderInfo
        {
            get { return _OrderInfo; }
            set { _OrderInfo = value; }
        }

        public int? ChargeID
        {
            get { return _ChargeID; }
            set { _ChargeID = value; }
        }

        public int? ChargeTypeID
        {
            get { return _ChargeTypeID; }
            set { _ChargeTypeID = value; }
        }

        public Dictionary<int, OrderDetailInfo> OrderDetailInfoCredit
        {
            get { return _OrderDetailInfoCredit; }
            set { _OrderDetailInfoCredit = value; }
        }

        public bool IsOrderValidForCredit
        {
            get { return _IsOrderValidForCredit; }
            set { _IsOrderValidForCredit = value; }
        }

        public TransactionType TransactionTypeAvailable
        {
            get { return _TransactionType; }
            set { _TransactionType = value; }
        }

        public List<SelectListItem> OrderReasonsListItems
        {
            get
            {
                return CreditHelper.GetOrderReasons(_OrderReasonForCredit);
            }
        }

        public decimal? ItemAmountTotal
        {
            get { return _ItemAmountTotal; }
            set { _ItemAmountTotal = value; }
        }

        public decimal? ItemTaxTotal
        {
            get { return _ItemTaxTotal; }
            set { _ItemTaxTotal = value; }
        }

        public decimal? ItemAmountCreditTotal
        {
            get { return _ItemAmountCreditTotal; }
            set { _ItemAmountCreditTotal = value; }
        }

        public decimal? ItemTaxCreditTotal
        {
            get { return _ItemTaxCreditTotal; }
            set { _ItemTaxCreditTotal = value; }
        }

        public decimal? OrderCreditTotal
        {
            get { return _OrderCreditTotal; }
            set { _OrderCreditTotal = value; }
        }

        #region Methods
        public CreditOrder(int? orderID)
        {
            _OrderID = orderID;
        }

        public List<SelectListItem> GetDurationTypeList(int? selectedID)
        {
            return CreditHelper.GetDurationType(selectedID);
        }

        public void InitializeTotals()
        {
            if (_OrderInfo != null && _OrderInfo.OrderDetail != null)
            {
                _ItemAmountTotal = 0m;
                _ItemTaxTotal = 0m;

                foreach (OrderDetailInfo detailInfo in _OrderInfo.OrderDetail)
                {
                    _ItemAmountTotal += detailInfo.Amount; //Data returned from purchase should already be rounded to 2 decimals, so this should affect it, but this is there for all the bad unrounded data we may have.

                    foreach (OrderTaxInfo taxInfo in detailInfo.OrderTax)
                    {
                        _ItemTaxTotal += taxInfo.Amount;
                    }
                }

                if (TransactionTypeAvailable == TransactionType.Void)
                {
                    _OrderCreditTotal = OrderInfo.TotalAmount;
                    _ItemAmountCreditTotal = _ItemAmountTotal;
                    _ItemTaxCreditTotal = _ItemTaxTotal;
                }
            }
        }

        public void ValidateOrder(ModelStateDictionary modelState)
        {
            if (OrderInfo == null)
            {
                modelState.AddModelError("ErrorMessage", "The Order (" + _OrderID.Value.ToString() + ") was not found.");
            }
            else if ((OrderStatus)Enum.Parse(typeof(OrderStatus), OrderInfo.OrderStatusID.ToString()) != OrderStatus.Successful)
            {
                OrderStatus orderStatus = (OrderStatus)Enum.Parse(typeof(OrderStatus), OrderInfo.OrderStatusID.ToString());
                switch (orderStatus)
                {
                    case OrderStatus.None:
                    case OrderStatus.Failed:
                        //failed order
                        modelState.AddModelError("ErrorMessage", "The Order (" + _OrderID.Value.ToString() + ") has a status of 'failed', which means the customer was never charged, so it is not available for a Credit or Void.");
                        break;
                    case OrderStatus.Pending:
                        modelState.AddModelError("ErrorMessage", "The Order (" + _OrderID.Value.ToString() + ") is pending so it is not available for a Credit or Void.");
                        break;
                }
            }
            else if (OrderInfo.OrderDetail == null || OrderInfo.OrderDetail.Length <= 0)
            {
                modelState.AddModelError("ErrorMessage", "The Order (" + _OrderID.Value.ToString() + ") has no items so it is not available for Credit or Void.");
            }
            else if (_OrderInfo.OrderDetail[0].OrderTypeID == (int)TransactionType.Credit
                    || _OrderInfo.OrderDetail[0].OrderTypeID == (int)TransactionType.Void)
            {
                modelState.AddModelError("ErrorMessage", "The Order (" + _OrderID.Value.ToString() + ") is a Credit or Void order and is not available for a Credit or Void.");
            }
            else if (OrderInfo.OrderUserPayment == null || !ChargeID.HasValue)
            {
                modelState.AddModelError("ErrorMessage", "The Order (" + _OrderID.Value.ToString() + ") is missing an associated billing Payment ID and is therefore unavailable for a credit or void.");
            }
            

            _IsOrderValidForCredit = modelState.IsValid;
        }

        public void ValidateCreditOrderFields(FormCollection formValues, ModelStateDictionary modelState)
        {
            _OrderCreditTotal = 0m;
            _ItemAmountCreditTotal = 0m;
            _ItemTaxCreditTotal = 0m;
            foreach (OrderDetailInfo detailInfo in OrderInfo.OrderDetail)
            {
                //Amount Credit
                OrderDetailInfo orderDetailInfoCredit = null;
                if (!String.IsNullOrEmpty(formValues["AmountCredit" + detailInfo.OrderDetailID.ToString()]))
                {
                    decimal amountCredit = Constants.NULL_DECIMAL;
                    if (decimal.TryParse(formValues["AmountCredit" + detailInfo.OrderDetailID.ToString()], out amountCredit))
                    {
                        amountCredit = Math.Round(Math.Abs(amountCredit), 2);

                        _ItemAmountCreditTotal += amountCredit;
                        if (amountCredit > detailInfo.Amount)
                        {
                            modelState.Add("AmountCredit" + detailInfo.OrderDetailID.ToString(), new ModelState() { Value = new ValueProviderResult(formValues["AmountCredit" + detailInfo.OrderDetailID.ToString()], formValues["AmountCredit" + detailInfo.OrderDetailID.ToString()], Thread.CurrentThread.CurrentCulture) });
                            modelState.AddModelError("AmountCredit" + detailInfo.OrderDetailID.ToString(), "Credit amount for Item ID (" + detailInfo.ItemID.ToString() + ") may not be more than the original item amount charged.");
                        }
                        else if (amountCredit <= 0)
                        {
                            modelState.Add("AmountCredit" + detailInfo.OrderDetailID.ToString(), new ModelState() { Value = new ValueProviderResult(formValues["AmountCredit" + detailInfo.OrderDetailID.ToString()], formValues["AmountCredit" + detailInfo.OrderDetailID.ToString()], Thread.CurrentThread.CurrentCulture) });
                            modelState.AddModelError("AmountCredit" + detailInfo.OrderDetailID.ToString(), "Credit amount for Item ID (" + detailInfo.ItemID.ToString() + ") may not be less than zero.");
                        }

                        //create item to be credited (also used to redisplay in event of validation error)
                        orderDetailInfoCredit = CloneDetailInfoForCredit(detailInfo);
                        orderDetailInfoCredit.Amount = amountCredit;
                        if (orderDetailInfoCredit.OrderTax.Length > 0 && amountCredit > 0)
                        {
                            decimal taxTotal = CreditHelper.CalculateTaxTotal(detailInfo);
                            decimal itemTaxCredit = 0m;
                            if (taxTotal > 0)
                            {
                                itemTaxCredit = Math.Round((amountCredit / detailInfo.Amount) * taxTotal, 2);
                                orderDetailInfoCredit.OrderTax[0].Amount = itemTaxCredit;
                            }
                            else
                            {
                                orderDetailInfoCredit.OrderTax[0].Amount = itemTaxCredit;
                            }

                            _ItemTaxCreditTotal += itemTaxCredit;
                        }

                        //duration
                        if (!String.IsNullOrEmpty(formValues["Duration" + detailInfo.OrderDetailID.ToString()]))
                        {
                            int duration = Constants.NULL_INT;
                            if (int.TryParse(formValues["Duration" + detailInfo.OrderDetailID.ToString()], out duration))
                            {
                                orderDetailInfoCredit.Duration = duration;

                                //TODO: Can duration by less than zero?
                                if (duration < 0)
                                {
                                    //modelState.Add("Duration" + detailInfo.OrderDetailID.ToString(), new ModelState() { Value = new ValueProviderResult(formValues["Duration" + detailInfo.OrderDetailID.ToString()], formValues["Duration" + detailInfo.OrderDetailID.ToString()], Thread.CurrentThread.CurrentCulture) });
                                    //modelState.AddModelError("Duration" + detailInfo.OrderDetailID.ToString(), "Duration for Order Detail (" + detailInfo.OrderDetailID.ToString() + ") may not be less than zero, if specified.");
                                }

                                //duration type is required
                                if (!String.IsNullOrEmpty(formValues["DurationTypeID" + detailInfo.OrderDetailID.ToString()]) && formValues["DurationTypeID" + detailInfo.OrderDetailID.ToString()] != "0")
                                {
                                    orderDetailInfoCredit.DurationTypeID = Convert.ToInt32(formValues["DurationTypeID" + detailInfo.OrderDetailID.ToString()]);
                                }
                                else
                                {
                                    orderDetailInfoCredit.DurationTypeID = 0;
                                    modelState.AddModelError("DurationTypeIDError" + detailInfo.OrderDetailID.ToString(), "Duration Type for Item ID (" + detailInfo.ItemID.ToString() + ") is not valid.");
                                }
                            }
                            else
                            {
                                modelState.Add("Duration" + detailInfo.OrderDetailID.ToString(), new ModelState() { Value = new ValueProviderResult(formValues["Duration" + detailInfo.OrderDetailID.ToString()], formValues["Duration" + detailInfo.OrderDetailID.ToString()], Thread.CurrentThread.CurrentCulture) });
                                modelState.AddModelError("Duration" + detailInfo.OrderDetailID.ToString(), "Duration for Order Detail (" + detailInfo.OrderDetailID.ToString() + ") is not valid.");
                            }
                        }

                        _OrderDetailInfoCredit.Add(detailInfo.OrderDetailID, orderDetailInfoCredit);

                    }
                    else
                    {
                        modelState.Add("AmountCredit" + detailInfo.OrderDetailID.ToString(), new ModelState() { Value = new ValueProviderResult(formValues["AmountCredit" + detailInfo.OrderDetailID.ToString()], formValues["AmountCredit" + detailInfo.OrderDetailID.ToString()], Thread.CurrentThread.CurrentCulture) });
                        modelState.AddModelError("AmountCredit" + detailInfo.OrderDetailID.ToString(), "Credit Amount for Item ID (" + detailInfo.ItemID.ToString() + ") is Invalid.");
                    }
                }
                
            }

            _OrderCreditTotal = _ItemAmountCreditTotal + _ItemTaxCreditTotal;

            //Order Reason
            _OrderReasonForCredit = Convert.ToInt32(formValues["OrderReasonID"]);
            if (formValues["OrderReasonID"] == "0")
            {
                modelState.AddModelError("OrderReasonIDError", "The selected Order Reason is invalid.");
            }

            //Total credit amount
            if (_OrderCreditTotal <= 0)
            {
                modelState.AddModelError("OrderCreditTotal", "Please provide a credit amount to credit the order.");
            }
            else if (_OrderCreditTotal > OrderInfo.TotalAmount)
            {
                modelState.AddModelError("OrderCreditTotal", "The total credit amount may not be greater than the original order amount.");
            }

            if (modelState.IsValid)
            {
                OrderInfo.ReferenceOrderID = OrderInfo.OrderID;
                OrderInfo.TotalAmount = _OrderCreditTotal.Value;
                OrderInfo.OrderReasonID = _OrderReasonForCredit.Value;
                OrderInfo.OrderDetail = _OrderDetailInfoCredit.Values.ToArray();
                
            }
            
        }

        public void ValidateVoidOrderFields(FormCollection formValues, ModelStateDictionary modelState)
        {
            foreach (OrderDetailInfo detailInfo in OrderInfo.OrderDetail)
            {
                detailInfo.ReferenceOrderDetailID = detailInfo.OrderDetailID;
                detailInfo.OrderTypeID = (int)TransactionTypeAvailable;
                detailInfo.DurationTypeID = 0;
                detailInfo.Duration = Constants.NULL_INT;

                //duration
                if (!String.IsNullOrEmpty(formValues["Duration" + detailInfo.OrderDetailID.ToString()]))
                {
                    int duration = Constants.NULL_INT;
                    if (int.TryParse(formValues["Duration" + detailInfo.OrderDetailID.ToString()], out duration))
                    {
                        detailInfo.Duration = duration;

                        //TODO: Can duration by less than zero?
                        if (duration < 0)
                        {
                            //modelState.Add("Duration" + detailInfo.OrderDetailID.ToString(), new ModelState() { Value = new ValueProviderResult(formValues["Duration" + detailInfo.OrderDetailID.ToString()], formValues["Duration" + detailInfo.OrderDetailID.ToString()], Thread.CurrentThread.CurrentCulture) });
                            //modelState.AddModelError("Duration" + detailInfo.OrderDetailID.ToString(), "Duration for Order Detail (" + detailInfo.OrderDetailID.ToString() + ") may not be less than zero, if specified.");
                        }

                        //duration type is required
                        if (!String.IsNullOrEmpty(formValues["DurationTypeID" + detailInfo.OrderDetailID.ToString()]) && formValues["DurationTypeID" + detailInfo.OrderDetailID.ToString()] != "0")
                        {
                            detailInfo.DurationTypeID = Convert.ToInt32(formValues["DurationTypeID" + detailInfo.OrderDetailID.ToString()]);
                        }
                        else
                        {
                            detailInfo.DurationTypeID = 0;
                            modelState.AddModelError("DurationTypeIDError" + detailInfo.OrderDetailID.ToString(), "Duration Type for Item ID (" + detailInfo.ItemID.ToString() + ") is not valid.");
                        }
                    }
                    else
                    {
                        modelState.Add("Duration" + detailInfo.OrderDetailID.ToString(), new ModelState() { Value = new ValueProviderResult(formValues["Duration" + detailInfo.OrderDetailID.ToString()], formValues["Duration" + detailInfo.OrderDetailID.ToString()], Thread.CurrentThread.CurrentCulture) });
                        modelState.AddModelError("Duration" + detailInfo.OrderDetailID.ToString(), "Duration for Item ID (" + detailInfo.ItemID.ToString() + ") is not valid.");
                    }
                }
            }

            //Order Reason
            _OrderReasonForCredit = Convert.ToInt32(formValues["OrderReasonID"]);
            if (formValues["OrderReasonID"] == "0")
            {
                modelState.AddModelError("OrderReasonIDError", "The selected Order Reason is invalid.");
            }

            if (modelState.IsValid)
            {
                OrderInfo.ReferenceOrderID = OrderInfo.OrderID;
                OrderInfo.OrderReasonID = _OrderReasonForCredit.Value;
            }
        }

        private OrderDetailInfo CloneDetailInfoForCredit(OrderDetailInfo detailInfo)
        {
            OrderDetailInfo orderDetailInfoCredit = new OrderDetailInfo();
            orderDetailInfoCredit.Amount = detailInfo.Amount;
            orderDetailInfoCredit.CurrencyID = detailInfo.CurrencyID;
            orderDetailInfoCredit.ItemID = detailInfo.ItemID;
            orderDetailInfoCredit.OrderId = detailInfo.OrderId;
            orderDetailInfoCredit.OrderTypeID = (int)TransactionTypeAvailable;
            orderDetailInfoCredit.PackageID = detailInfo.PackageID;
            orderDetailInfoCredit.PromoID = detailInfo.PromoID;
            orderDetailInfoCredit.ReferenceOrderDetailID = detailInfo.OrderDetailID;
            orderDetailInfoCredit.Duration = Constants.NULL_INT;
            orderDetailInfoCredit.DurationTypeID = 0;

            if (detailInfo.OrderTax != null && detailInfo.OrderTax.Length > 0)
            {
                OrderTaxInfo orderTaxInfoCredit = new OrderTaxInfo();
                orderTaxInfoCredit.AuthorityCode = detailInfo.OrderTax[0].AuthorityCode;
                orderTaxInfoCredit.AuthorityID = detailInfo.OrderTax[0].AuthorityID;
                orderTaxInfoCredit.CurrencyID = detailInfo.OrderTax[0].CurrencyID;
                orderTaxInfoCredit.ItemTaxTypeID = detailInfo.OrderTax[0].ItemTaxTypeID;
                orderTaxInfoCredit.RegionID = detailInfo.OrderTax[0].RegionID;
                orderDetailInfoCredit.OrderTax = new OrderTaxInfo[] { orderTaxInfoCredit };
            }

            return orderDetailInfoCredit;
        }

        #endregion
    }
}
