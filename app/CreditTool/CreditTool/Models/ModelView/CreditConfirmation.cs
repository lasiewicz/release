﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CreditTool.PurchaseService;

namespace CreditTool.Models.ModelView
{
    public class CreditConfirmation
    {
        private CreditOrderResponse _CreditOrderResponse = null;

        public CreditOrderResponse CreditOrderResponse
        {
            get { return _CreditOrderResponse; }
            set { _CreditOrderResponse = value; }
        }

        #region Methods

        #endregion
    }
}
