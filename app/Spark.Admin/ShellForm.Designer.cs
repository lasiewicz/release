namespace Spark.Admin
{
    partial class ShellForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ShellTabWorkspace = new Microsoft.Practices.CompositeUI.WinForms.TabWorkspace();
            this.SuspendLayout();
            // 
            // ShellTabWorkspace
            // 
            this.ShellTabWorkspace.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ShellTabWorkspace.Location = new System.Drawing.Point(0, 0);
            this.ShellTabWorkspace.Name = "ShellTabWorkspace";
            this.ShellTabWorkspace.SelectedIndex = 0;
            this.ShellTabWorkspace.Size = new System.Drawing.Size(792, 573);
            this.ShellTabWorkspace.TabIndex = 0;
            // 
            // ShellForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 573);
            this.Controls.Add(this.ShellTabWorkspace);
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "ShellForm";
            this.Text = "Spark.Admin";
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Practices.CompositeUI.WinForms.TabWorkspace ShellTabWorkspace;

    }
}

