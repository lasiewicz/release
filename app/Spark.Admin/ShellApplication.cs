using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using System.Security.Principal;
using System.Collections;

using Microsoft.Practices.CompositeUI.WinForms;

using Spark.Admin.Common;
using Matchnet.Member.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ValueObjects.Privilege;

namespace Spark.Admin
{
    internal class ShellApplication : FormShellApplication<ShellWorkItem, ShellForm>
    {
        private const String AuthorizedDomainName = "matchnet";
        private const String DomainEmail1 = "spark.net";
        private const String DomainEmail2 = "matchnet.com";
        public const String MsgNotAuthorized = "You are not authorized to use any of the tools in this application.";
        public const String MsgNoMemberID = "You must create an account on one of our DEV sites (i.e. http://dev.collegeluv.com) and one of our PROD sites (i.e. http://www.collegeluv.com) using your XXXXX@" + DomainEmail1 + " email address before accessing this application.";

        private WebServiceProxy webServiceProxy;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            new ShellApplication().Run();
        }

        #region Public Methods
        protected override void BeforeShellCreated()
        {
            try
            {
                base.BeforeShellCreated();

                // Keep an instance of WebServiceProxy in the State.
                webServiceProxy = new WebServiceProxy();

                Sites sites = webServiceProxy.GetSites();

                if (!Authenticate())
                {
                    MessageBox.Show("Authentication failed.", "Spark Admin", MessageBoxButtons.OK, MessageBoxIcon.Hand);

                    Thread.CurrentThread.Abort();
                }
                else
                {
                    // Store GlobalState information.
                    RootWorkItem.State[Constants.StateSites] = webServiceProxy.GetSites();
                    // DELETE?
                    //RootWorkItem.State[Constants.StateIsContentEditingAllowed] = Convert.ToBoolean(webServiceProxy.GetSetting("SPARKADMIN_ISCONTENTEDITINGALLOWED"));
                    //RootWorkItem.State[Constants.StateEnvironmentType] = Convert.ToString(webServiceProxy.GetSetting("ENVIRONMENT_TYPE"));
                }
            }
            catch (ThreadAbortException threadException)
            {
            }
            catch (Exception ex)
            {
                BaseController.HandleError("Spark Admin", ex.ToString());
            }
        }

        /// <summary>
        /// Populate UI elements (toolbars, menus, etc.), here.
        /// </summary>
        protected override void AfterShellCreated()
        {
            base.AfterShellCreated();
        }

        public override void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            base.OnUnhandledException(sender, e);

            Exception ex = e.ExceptionObject as Exception;

            if (ex != null)
            {
                if(!ex.GetType().Equals(typeof(ThreadAbortException))) //Do not show message box for Thread Abort Exception
                    MessageBox.Show(BuildExceptionString(ex));
            }
            else
            {
                MessageBox.Show("An Exception has occured, unable to get details");
            }

            Environment.Exit(0);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// The Composite UI Application Block defines the authentication service, and invokes 
        /// it as the first step in the default startup sequence. This occurs because the module 
        /// catalog reader service can process roles present in the profile and therefore must 
        /// ensure that there is a principal available.  In other words, we area already authenticated
        /// in Windows.  We still need to authenticate against our Matchnet members, however.
        /// 
        /// For non AD authentication(outsourcing, etc), make sure that everyone uses @spark.net address.
        /// Non AD authentication is only made with production service.
        /// To force login form to popup change the user setting value of ADAuthentication to false in proj 
        /// settings.
        /// </summary>
        private Boolean Authenticate()
        {
            String emailAddress = null;
            String password = null;

            // Show the login prompt (up to 3 times)
            for (Byte counter = 1; counter < 4; counter++)
            {
                if (AuthenticateWithMemberService(out emailAddress, out password))
                {
                    break;
                }
                else
                {
                    if (counter == 3)
                        return false;

                    MessageBox.Show("Login failed. Please try again. You have " + (3 - counter) + " attempts left.", "Spark Admin", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            
            ArrayList members = webServiceProxy.GetMembersByEmail(emailAddress);
            Int32 memberID;

            if (members.Count > 0 && ((CachedMember)members[0]).EmailAddress != null) // Sometimes a member is returned by GetMemberByEmail but it is not valid.
            {
                memberID = ((CachedMember)members[0]).MemberID;
                RootWorkItem.State[Constants.StateMemberID] = memberID;
                RootWorkItem.State[Constants.StateMemberEmail] = emailAddress;

                StoreMemberPrivileges(memberID);
            }

            // If we don't have a valid MemberID, exit with Auth notification.
            if (RootWorkItem.State[Constants.StateMemberID] == null)
            {
                MessageBox.Show(MsgNoMemberID, Constants.MsgBoxCaptionAuthenticationError);

                return false;
            }

            return true;
        }

        private Boolean AuthenticateWithMemberService(out String emailAddress, out String password)
        {
            LoginForm loginForm = new LoginForm();

            emailAddress = null;
            password = null;

            DialogResult dialogResult = loginForm.ShowDialog();

            if (dialogResult == DialogResult.Cancel)
            {
                loginForm.Dispose();

                Thread.CurrentThread.Abort();
            }

            AuthenticationResult authenticationResult = webServiceProxy.Authenticate(loginForm.Login, loginForm.Password, Matchnet.Content.ValueObjects.Admin.ServiceEnvironmentTypeEnum.Prod);

            if (authenticationResult.Status != AuthenticationStatus.Authenticated)
            {
                return false;
            }

            emailAddress = loginForm.Login;
            password = loginForm.Password;

            loginForm.Dispose();

            return true;
        }

        private void StoreMemberPrivileges(Int32 memberID)
        {
            MemberPrivilegeCollection memberPrivilegeCollection = webServiceProxy.GetMemberPrivileges(memberID);

            if (memberPrivilegeCollection != null && memberPrivilegeCollection.Count > 0)
            {
                MemberPrivilegeCollection savedMemberPrivilegeCollection;
                if (RootWorkItem.State[Constants.StateMemberPrivileges] != null)
                {
                    // Merge the new privileges with the stored privileges and then resave.
                    // NOTE:  If the privileges in DomainEmail1 (matchnet.com) conflict with the privileges in DomainEmail2 (spark.net), the
                    // privileges in DomainEmail1 win.
                    savedMemberPrivilegeCollection = ((MemberPrivilegeCollection)RootWorkItem.State[Constants.StateMemberPrivileges]);

                    foreach (Object memberPrivilege in savedMemberPrivilegeCollection)
                    {
                        memberPrivilegeCollection.Add(((MemberPrivilege)memberPrivilege));
                    }
                }
                
                RootWorkItem.State[Constants.StateMemberPrivileges] = memberPrivilegeCollection;
            }
        }

        private string BuildExceptionString(Exception exception)
        {
            String errMessage = String.Empty;

            errMessage +=
                exception.Message + Environment.NewLine + exception.StackTrace;

            while (exception.InnerException != null)
            {
                errMessage += BuildInnerExceptionString(exception.InnerException);

                exception = exception.InnerException;
            }

            return errMessage;
        }

        private string BuildInnerExceptionString(Exception innerException)
        {
            String errMessage = String.Empty;

            errMessage += Environment.NewLine + " InnerException ";
            errMessage += Environment.NewLine + innerException.Message + Environment.NewLine + innerException.StackTrace;

            return errMessage;
        }
        #endregion
    }
}