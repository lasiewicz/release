namespace Spark.Admin.PrivilegeModule
{
    partial class GroupList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbGroups = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // cbGroups
            // 
            this.cbGroups.FormattingEnabled = true;
            this.cbGroups.Location = new System.Drawing.Point(360, 35);
            this.cbGroups.Name = "cbGroups";
            this.cbGroups.Size = new System.Drawing.Size(156, 21);
            this.cbGroups.TabIndex = 5;
            // 
            // GroupList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbGroups);
            this.Name = "GroupList";
            this.Controls.SetChildIndex(this.cbGroups, 0);
            this.ResumeLayout(false);
            this.PerformLayout();
            this.Load += new System.EventHandler(this.GroupList_Load);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbGroups;
    }
}
