using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using Spark.Admin.Common;

using Matchnet.Content.ValueObjects.Admin;

namespace Spark.Admin.PrivilegeModule
{
    public partial class BaseList : UserControl
    {
        private IList data;
        private String displayMember;
        private Boolean showAdd = true;
        private Boolean showDelete = true;
        internal PrivilegesController privilegesController;

        #region Events
        public delegate void AddEventHandler(AddEventArgs args);
        public event AddEventHandler Add;

        public delegate void DeleteEventHandler(DeleteEventArgs args);
        public event DeleteEventHandler Delete;

        public delegate void SelectedIndexChangedEventHandler(EventArgs args);
        public event SelectedIndexChangedEventHandler SelectedIndexChanged;

        public delegate void ItemDoubleClickEventHandler(EventArgs args);
        public event ItemDoubleClickEventHandler ItemDoubleClick;
        #endregion

        public BaseList()
        {
            InitializeComponent();
        }

        #region Private Methods
        private void BaseList_Load(object sender, System.EventArgs e)
		{
			this.btnAdd.Visible = ShowAdd;
			this.btnDelete.Visible = ShowDelete;
		}

		private void btnAdd_Click(object sender, System.EventArgs e)
		{
			onAdd();
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
            if (lbData.Items.Count == 0)
            {
                MessageBox.Show("There are no items to delete.", Constants.MsgBoxCaptionWarning);
                return;
            }
			else if (this.SelectedItem == null)
			{
                MessageBox.Show("Please select an item to delete.", Constants.MsgBoxCaptionWarning);
				return;
			}
			
			Object deleteItem = this.SelectedItem;
			data.Remove(deleteItem);
			bindData();

			if (this.Delete != null)
			{
				this.Delete(new DeleteEventArgs(deleteItem));
			}
		}

		private void lbData_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (this.SelectedIndexChanged != null)
			{
				SelectedIndexChanged(e);
			}
		}

		private void lbData_DoubleClick(object sender, System.EventArgs e)
		{
			if (this.ItemDoubleClick != null)
			{
				ItemDoubleClick(e);
			}
		}
        #endregion


        #region Private/Protected Methods
        protected void bindData()
        {
            this.lbData.DataSource = null;
            this.lbData.DataSource = data;
            this.lbData.DisplayMember = this.DisplayMember;
        }

        protected virtual void onAdd()
        {
        }

        protected void onAdd(Object added)
        {
            if (this.Add != null)
            {
                Add(new AddEventArgs(added));
            }
        }
        #endregion

        #region Public Methods
        public virtual void AddItem(object newItem)
        {
            Data.Add(newItem);
            bindData();
        }

        #endregion


        #region Properties
        [Browsable(false)]
        public IList Data
        {
            set
            {
                data = value;
                bindData();
            }
            get
            {
                return data;
            }
        }

        [Browsable(false)]
        public String DisplayMember
        {
            get
            {
                return displayMember;
            }
            set
            {
                displayMember = value;
                this.lbData.DisplayMember = displayMember;
            }
        }

        [Browsable(false)]
        public virtual object SelectedItem
        {
            get
            {
                return this.lbData.SelectedItem;
            }
            set
            {
                if (data != null && data.Contains(value))
                {
                    this.lbData.SelectedItem = value;
                }
            }
        }

        [Browsable(false)]
        public string AddText
        {
            set
            {
                this.lblAdd.Text = value;
            }
        }

        [Browsable(false)]
        public Boolean ShowAdd
        {
            get
            {
                return showAdd;
            }
            set
            {
                showAdd = value;
                this.tbNewItem.Visible = showAdd;
                this.btnAdd.Visible = showAdd;
            }
        }

        [Browsable(false)]
        public Boolean ShowDelete
        {
            get
            {
                return showDelete;
            }
            set
            {
                showDelete = value;
                this.btnDelete.Visible = showDelete;
            }
        }

        public PrivilegesController PrivilegesController
        {
            get
            {
                return privilegesController;
            }
            set
            {
                privilegesController = value;
            }
        }
        #endregion
    }

    public class AddEventArgs : EventArgs
    {
        private Object added;

        public AddEventArgs(object added)
        {
            this.added = added;
        }

        public Object Added
        {
            get
            {
                return added;
            }
        }
    }

    public class DeleteEventArgs : EventArgs
    {
        private Object deleted;

        public DeleteEventArgs(object deleted)
        {
            this.deleted = deleted;
        }

        public Object Deleted
        {
            get
            {
                return deleted;
            }
        }
    }
}
