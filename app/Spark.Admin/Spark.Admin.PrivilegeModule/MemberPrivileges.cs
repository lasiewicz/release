using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using Microsoft.Practices.CompositeUI;

using Spark.Admin.Common;
using Matchnet.Member.ValueObjects.Privilege;
using Matchnet.Member.ValueObjects;

namespace Spark.Admin.PrivilegeModule
{
    public partial class MemberPrivileges : Spark.Admin.Common.Controls.Form
    {
        private PrivilegesController privilegesController;
        private CachedMember member;
        private Hashtable memberPrivilegeItems = new Hashtable();

        #region Constructors
        public MemberPrivileges(State globalState, PrivilegesController privilegesController)
            : base(globalState)
        {
            InitializeComponent();

            this.privilegesController = privilegesController;

            if (!HasPrivilege(ModuleWorkItem.PrivilegeIDGroupPrivilegeEditor)
                && !HasPrivilege(ModuleWorkItem.PrivilegeIDMemberPrivilegeEditor))
            {
                SaveButton.Visible = false;
            }
        }
        #endregion

        #region Private Methods
        private void MemberPrivileges_Load(object sender, System.EventArgs e)
        {
            DisplayMemberPrivileges(false);
        }

        private void DisplayMemberPrivileges(Boolean lookupFlag)
        {
            if (lookupFlag)
            {
                //First try lookup by MemberID
                Int32 memberID = Matchnet.Conversion.CInt(this.MemberIDTextBox.Text.Trim());

                //If no MemberID, try Email
                if (memberID == Matchnet.Constants.NULL_INT && this.EmailTextBox.Text.Trim() != String.Empty)
                {
                    ArrayList members = privilegesController.GetMembersByEmail(this.EmailTextBox.Text.Trim()
                        , privilegesController.CurrentServiceEnvironmentTypeEnum);

                    if (members.Count > 0)
                        memberID = ((CachedMember)members[0]).MemberID;
                }

                //If no Email, try Username
                if (memberID == Matchnet.Constants.NULL_INT && this.UsernameTextBox.Text.Trim() != string.Empty)
                {
                    memberID = ((CachedMember)privilegesController.GetMembersByUsername(this.UsernameTextBox.Text.Trim()
                        , privilegesController.CurrentServiceEnvironmentTypeEnum)[0]).MemberID;
                }

                if (memberID != Matchnet.Constants.NULL_INT
                    && ((CachedMember)privilegesController.GetMembersByMemberID(memberID, privilegesController.CurrentServiceEnvironmentTypeEnum)[0]).EmailAddress != null)
                {
                    member = ((CachedMember)privilegesController.GetMembersByMemberID(memberID, privilegesController.CurrentServiceEnvironmentTypeEnum)[0]);

                    this.Member = member;
                }
                else
                {
                    MessageBox.Show("Member not found.");
                    return;
                }
            }
            
            if (member != null)
            {
                BindData();
            }
        }

        private void BindData()
        {
            this.MemberIDTextBox.Text = Member.MemberID.ToString();
            this.EmailTextBox.Text = Member.EmailAddress;
            this.UsernameTextBox.Text = Member.Username;

            PrivilegesCheckedListBox.Items.Clear();
            memberPrivilegeItems.Clear();
            MemberPrivilegeCollection memberPrivileges = privilegesController.GetMemberPrivileges(Member.MemberID, privilegesController.CurrentServiceEnvironmentTypeEnum);
            for (Int32 i = 0; i < memberPrivileges.Count; i++)
            {
                MemberPrivilege memberPrivilege = memberPrivileges[i];

                CheckState checkState;
                switch (memberPrivilege.PrivilegeState)
                {
                    case PrivilegeState.Denied:
                        checkState = CheckState.Unchecked;
                        break;
                    case PrivilegeState.Individual:
                        checkState = CheckState.Checked;
                        break;
                    case PrivilegeState.SecurityGroup:
                    default:
                        checkState = CheckState.Indeterminate;
                        break;
                }

                memberPrivilegeItems.Add(PrivilegesCheckedListBox.Items.Add(memberPrivilege.Privilege.Description, checkState), memberPrivilege);
            }
        }

        private void ResetForm()
        {
            this.MemberIDTextBox.Clear();
            this.EmailTextBox.Clear();
            this.UsernameTextBox.Clear();

            PrivilegesCheckedListBox.Items.Clear();
        }
        #endregion

        #region Properties
        public CachedMember Member
        {
            get
            {
                return member;
            }
            set
            {
                member = value;
            }
        }
        #endregion

        private void LookupButton_Click(object sender, EventArgs e)
        {
            DisplayMemberPrivileges(true);
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            for (Int32 i = 0; i < this.PrivilegesCheckedListBox.Items.Count; i++)
            {
                MemberPrivilege memberPrivilege = (MemberPrivilege)this.memberPrivilegeItems[i];

                switch (this.PrivilegesCheckedListBox.GetItemCheckState(i))
                {
                    case CheckState.Checked:
                        //if (!privilegesController.HasPrivilege(Member.MemberID, memberPrivilege.Privilege.PrivilegeID
                        //    , privilegesController.CurrentServiceEnvironmentTypeEnum))
                        //{
                            privilegesController.SaveMemberPrivilege(Member.MemberID, memberPrivilege.Privilege.PrivilegeID
                                , privilegesController.CurrentServiceEnvironmentTypeEnum);
                        //}
                        break;
                    case CheckState.Unchecked:
                        //if (privilegesController.HasPrivilege(Member.MemberID, memberPrivilege.Privilege.PrivilegeID
                        //    , privilegesController.CurrentServiceEnvironmentTypeEnum))
                        //{
                            privilegesController.DeleteMemberPrivilege(Member.MemberID, memberPrivilege.Privilege.PrivilegeID
                                , privilegesController.CurrentServiceEnvironmentTypeEnum);
                        //}
                        break;
                }
            }

            MessageBox.Show("This Member's Privileges have been saved.", Constants.MsgBoxCaptionConfirm);
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            ResetForm();
        }
    }
}