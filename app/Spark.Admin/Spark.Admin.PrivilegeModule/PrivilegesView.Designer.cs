namespace Spark.Admin.PrivilegeModule
{
    partial class PrivilegesView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ServiceEnvironmentLabel = new System.Windows.Forms.Label();
            this.ServiceEnvironmentComboBox = new System.Windows.Forms.ComboBox();
            this.SelectPrivilegeLabel = new System.Windows.Forms.Label();
            this.MembersWithPrivilegeLabel = new System.Windows.Forms.Label();
            this.GroupsWithPrivilegeLabel = new System.Windows.Forms.Label();
            this.ManageMembersButton = new System.Windows.Forms.Button();
            this.ManageGroupsButton = new System.Windows.Forms.Button();
            this.GroupList = new Spark.Admin.PrivilegeModule.GroupList();
            this.MemberList = new Spark.Admin.PrivilegeModule.MemberList();
            this.PrivilegeList = new Spark.Admin.PrivilegeModule.BaseList();
            this.SuspendLayout();
            // 
            // ServiceEnvironmentLabel
            // 
            this.ServiceEnvironmentLabel.AutoSize = true;
            this.ServiceEnvironmentLabel.Location = new System.Drawing.Point(677, 0);
            this.ServiceEnvironmentLabel.Name = "ServiceEnvironmentLabel";
            this.ServiceEnvironmentLabel.Size = new System.Drawing.Size(105, 13);
            this.ServiceEnvironmentLabel.TabIndex = 7;
            this.ServiceEnvironmentLabel.Text = "Service Environment";
            // 
            // ServiceEnvironmentComboBox
            // 
            this.ServiceEnvironmentComboBox.FormattingEnabled = true;
            this.ServiceEnvironmentComboBox.Location = new System.Drawing.Point(661, 11);
            this.ServiceEnvironmentComboBox.Name = "ServiceEnvironmentComboBox";
            this.ServiceEnvironmentComboBox.Size = new System.Drawing.Size(121, 21);
            this.ServiceEnvironmentComboBox.TabIndex = 8;
            this.ServiceEnvironmentComboBox.SelectedIndexChanged += new System.EventHandler(this.ServiceEnvironmentComboBox_SelectedIndexChanged);
            // 
            // SelectPrivilegeLabel
            // 
            this.SelectPrivilegeLabel.AutoSize = true;
            this.SelectPrivilegeLabel.Location = new System.Drawing.Point(18, 46);
            this.SelectPrivilegeLabel.Name = "SelectPrivilegeLabel";
            this.SelectPrivilegeLabel.Size = new System.Drawing.Size(377, 13);
            this.SelectPrivilegeLabel.TabIndex = 10;
            this.SelectPrivilegeLabel.Text = "Select a Privilege to see Members and Groups that have the selected Privilege";
            // 
            // MembersWithPrivilegeLabel
            // 
            this.MembersWithPrivilegeLabel.AutoSize = true;
            this.MembersWithPrivilegeLabel.Location = new System.Drawing.Point(18, 208);
            this.MembersWithPrivilegeLabel.Name = "MembersWithPrivilegeLabel";
            this.MembersWithPrivilegeLabel.Size = new System.Drawing.Size(171, 13);
            this.MembersWithPrivilegeLabel.TabIndex = 13;
            this.MembersWithPrivilegeLabel.Text = "Members Who Have This Privilege";
            // 
            // GroupsWithPrivilegeLabel
            // 
            this.GroupsWithPrivilegeLabel.AutoSize = true;
            this.GroupsWithPrivilegeLabel.Location = new System.Drawing.Point(18, 371);
            this.GroupsWithPrivilegeLabel.Name = "GroupsWithPrivilegeLabel";
            this.GroupsWithPrivilegeLabel.Size = new System.Drawing.Size(161, 13);
            this.GroupsWithPrivilegeLabel.TabIndex = 14;
            this.GroupsWithPrivilegeLabel.Text = "Groups That Have This Privilege";
            // 
            // ManageMembersButton
            // 
            this.ManageMembersButton.Location = new System.Drawing.Point(21, 11);
            this.ManageMembersButton.Name = "ManageMembersButton";
            this.ManageMembersButton.Size = new System.Drawing.Size(107, 23);
            this.ManageMembersButton.TabIndex = 15;
            this.ManageMembersButton.Text = "Manage Members";
            this.ManageMembersButton.UseVisualStyleBackColor = true;
            this.ManageMembersButton.Click += new System.EventHandler(this.ManageMembersButton_Click);
            // 
            // ManageGroupsButton
            // 
            this.ManageGroupsButton.Location = new System.Drawing.Point(134, 11);
            this.ManageGroupsButton.Name = "ManageGroupsButton";
            this.ManageGroupsButton.Size = new System.Drawing.Size(107, 23);
            this.ManageGroupsButton.TabIndex = 16;
            this.ManageGroupsButton.Text = "Manage Groups";
            this.ManageGroupsButton.UseVisualStyleBackColor = true;
            this.ManageGroupsButton.Click += new System.EventHandler(this.ManageGroupsButton_Click);
            // 
            // GroupList
            // 
            this.GroupList.AllowNew = false;
            this.GroupList.Data = null;
            this.GroupList.DisplayMember = null;
            this.GroupList.Location = new System.Drawing.Point(21, 387);
            this.GroupList.Name = "GroupList";
            this.GroupList.PrivilegesController = null;
            this.GroupList.SelectedItem = null;
            this.GroupList.ShowAdd = false;
            this.GroupList.ShowDelete = false;
            this.GroupList.Size = new System.Drawing.Size(524, 134);
            this.GroupList.TabIndex = 12;
            this.GroupList.ItemDoubleClick += new Spark.Admin.PrivilegeModule.BaseList.ItemDoubleClickEventHandler(this.GroupList_ItemDoubleClick);
            // 
            // MemberList
            // 
            this.MemberList.Data = null;
            this.MemberList.DisplayMember = "EmailAddress";
            this.MemberList.Location = new System.Drawing.Point(21, 224);
            this.MemberList.Name = "MemberList";
            this.MemberList.NewItemText = "";
            this.MemberList.PrivilegesController = null;
            this.MemberList.SelectedItem = null;
            this.MemberList.ShowAdd = true;
            this.MemberList.ShowDelete = true;
            this.MemberList.Size = new System.Drawing.Size(524, 134);
            this.MemberList.TabIndex = 11;
            this.MemberList.ItemDoubleClick += new Spark.Admin.PrivilegeModule.BaseList.ItemDoubleClickEventHandler(this.MemberList_ItemDoubleClick);
            this.MemberList.Delete += new Spark.Admin.PrivilegeModule.BaseList.DeleteEventHandler(this.MemberList_Delete);
            this.MemberList.Add += new Spark.Admin.PrivilegeModule.BaseList.AddEventHandler(this.MemberList_Add);
            // 
            // PrivilegeList
            // 
            this.PrivilegeList.Data = null;
            this.PrivilegeList.DisplayMember = null;
            this.PrivilegeList.Location = new System.Drawing.Point(21, 62);
            this.PrivilegeList.Name = "PrivilegeList";
            this.PrivilegeList.PrivilegesController = null;
            this.PrivilegeList.SelectedItem = null;
            this.PrivilegeList.ShowAdd = true;
            this.PrivilegeList.ShowDelete = true;
            this.PrivilegeList.Size = new System.Drawing.Size(524, 134);
            this.PrivilegeList.TabIndex = 9;
            this.PrivilegeList.SelectedIndexChanged += new Spark.Admin.PrivilegeModule.BaseList.SelectedIndexChangedEventHandler(this.PrivilegeList_SelectedIndexChanged);
            // 
            // PrivilegesView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.ManageGroupsButton);
            this.Controls.Add(this.ManageMembersButton);
            this.Controls.Add(this.GroupsWithPrivilegeLabel);
            this.Controls.Add(this.MembersWithPrivilegeLabel);
            this.Controls.Add(this.GroupList);
            this.Controls.Add(this.MemberList);
            this.Controls.Add(this.SelectPrivilegeLabel);
            this.Controls.Add(this.PrivilegeList);
            this.Controls.Add(this.ServiceEnvironmentComboBox);
            this.Controls.Add(this.ServiceEnvironmentLabel);
            this.Name = "PrivilegesView";
            this.Size = new System.Drawing.Size(800, 600);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ServiceEnvironmentLabel;
        private System.Windows.Forms.ComboBox ServiceEnvironmentComboBox;
        private BaseList PrivilegeList;
        private System.Windows.Forms.Label SelectPrivilegeLabel;
        private MemberList MemberList;
        private GroupList GroupList;
        private System.Windows.Forms.Label MembersWithPrivilegeLabel;
        private System.Windows.Forms.Label GroupsWithPrivilegeLabel;
        private System.Windows.Forms.Button ManageMembersButton;
        private System.Windows.Forms.Button ManageGroupsButton;


    }
}
