using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.CompositeUI.WinForms;

using Spark.Admin.Common;

namespace Spark.Admin.PrivilegeModule
{
    public class ModuleWorkItem : BaseModuleWorkItem
    {
        public const Int32 PrivilegeIDMemberPrivilegeEditor = 2;
        public const Int32 PrivilegeIDGroupPrivilegeEditor = 6;
        public const Int32 PrivilegeIDPrivilegeViewer = 150;

        private IWorkspace workspace;

        #region Public Methods
        public void Run(IWorkspace workspace)
        {
            this.workspace = workspace;

            PrivilegesView privilegesView = this.Items.AddNew<PrivilegesView>();
            // You must manually assign GlobalState to the ModuleView.
            privilegesView.GlobalState = GlobalState;

            this.Items.Add(privilegesView);

            // Set the Tab properties.
            TabSmartPartInfo tabSmartPartInfo = new TabSmartPartInfo();
            tabSmartPartInfo.Title = privilegesView.Title;

            workspace.Show(privilegesView, tabSmartPartInfo);
        }
        #endregion
    }
}
