using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using Microsoft.Practices.CompositeUI;

using Spark.Admin.Common;
using Spark.Admin.WebServices;

using Matchnet.Member.ValueObjects;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Member.ValueObjects.Privilege;

namespace Spark.Admin.PrivilegeModule
{
    public class PrivilegesController : BaseController
    {
        private WebServiceProxy webServiceProxy;

        #region Constructors
        public PrivilegesController()
        {
            // Initialize the WebServiceProxy if you are using web services.
            webServiceProxy = new WebServiceProxy();        
        }
        #endregion

        #region Public Methods
        public ArrayList GetMembersByMemberID(Int32 memberID, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            return webServiceProxy.GetMembersByMemberID(memberID, serviceEnvironmentEnum);
        }

        public ArrayList GetMembersByEmail(String email, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            return webServiceProxy.GetMembersByEmail(email, serviceEnvironmentEnum);
        }

        public ArrayList GetMembersByUsername(String username, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            return webServiceProxy.GetMembersByUsername(username, serviceEnvironmentEnum);
        }

        public PrivilegeCollection GetPrivileges(ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            return webServiceProxy.GetPrivileges(serviceEnvironmentEnum);
        }

        public ArrayList GetMembersByPrivilege(Int32 privilegeID, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            return webServiceProxy.GetMembersByPrivilege(privilegeID, serviceEnvironmentEnum);
        }

        public SecurityGroupCollection GetGroupsByPrivilege(Int32 privilegeID, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            return webServiceProxy.GetGroupsByPrivilege(privilegeID, serviceEnvironmentEnum);
        }

        public void SaveMemberPrivilege(Int32 memberID, Int32 privilegeID, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            webServiceProxy.SaveMemberPrivilege(memberID, privilegeID, serviceEnvironmentEnum);
        }

        public void DeleteMemberPrivilege(Int32 memberID, Int32 privilegeID, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            webServiceProxy.DeleteMemberPrivilege(memberID, privilegeID, serviceEnvironmentEnum);
        }

        public SecurityGroupMemberCollection GetSecurityGroupMembers(Int32 groupID, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            return webServiceProxy.GetSecurityGroupMembers(groupID, serviceEnvironmentEnum);
        }

        public SecurityGroupCollection GetSecurityGroups(ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            return webServiceProxy.GetSecurityGroups(serviceEnvironmentEnum);
        }

        public void SaveSecurityGroupMember(Int32 memberID, Int32 groupID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            webServiceProxy.SaveSecurityGroupMember(memberID, groupID, currentServiceEnvironmentTypeEnum);
        }

        public void DeleteSecurityGroupMember(Int32 memberID, Int32 groupID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            webServiceProxy.DeleteSecurityGroupMember(memberID, groupID, currentServiceEnvironmentTypeEnum);
        }

        public MemberPrivilegeCollection GetMemberPrivileges(Int32 memberID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            return webServiceProxy.GetMemberPrivileges(memberID, currentServiceEnvironmentTypeEnum);
        }
        #endregion
        
        #region Properties
        public new ModuleWorkItem WorkItem
        {
            get
            {
                return base.WorkItem as ModuleWorkItem;
            }
        }
        #endregion
    }
}
