using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.CompositeUI;


using Spark.Admin.WebServices;

using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Member.ValueObjects.Privilege;
using Matchnet.Member.ValueObjects;
//using Matchnet.Content.ValueObjects.BrandConfig;
//using Matchnet.Content.ValueObjects.Admin;

using Spark.Admin.Common;

namespace Spark.Admin.PrivilegeModule
{
    public partial class PrivilegesView : BaseModuleView
    {
        private const String TitlePrivileges = "Privileges";
        //private const String MsgDeleteMemberPrivilegeConfirm = "Are you sure you want to delete this MemberPrivilege?";
        //private const String MsgSaveMemberPrivilegeConfirm = "Are you sure you want to save this MemberPrivilege?";

        private PrivilegesController privilegesController;
        private ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum;

        #region Contructors
        public PrivilegesView()
        {
            InitializeComponent();

            // Set the Tab title.
            this.Title = TitlePrivileges;
        }
        #endregion

        #region Private Methods
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            //// You must manually assign GlobalState to the Controller.
            privilegesController.GlobalState = GlobalState;

            // Set up ServiceEnvironment ComboBox.
            ServiceEnvironmentComboBox.DataSource = Enum.GetValues(typeof(ServiceEnvironmentTypeEnum));
            
            // Make a call to ServiceEnvironmentComboBox_SelectedIndexChanged initialize currentServiceEnvironmentTypeEnum.
            ServiceEnvironmentComboBox_SelectedIndexChanged(null, null);

            if (!HasPrivilege(ModuleWorkItem.PrivilegeIDGroupPrivilegeEditor)
                && !HasPrivilege(ModuleWorkItem.PrivilegeIDMemberPrivilegeEditor))
            {
                PrivilegeList.ShowAdd = false;
                PrivilegeList.ShowDelete = false;
                MemberList.ShowAdd = false;
                MemberList.ShowDelete = false;
                GroupList.ShowAdd = false;
                GroupList.ShowDelete = false;
            }

            // Initialize controls.
            PrivilegeList.PrivilegesController = privilegesController;
            MemberList.PrivilegesController = privilegesController;
            GroupList.PrivilegesController = privilegesController;

            // No way to Add or Delete privileges, currently.
            PrivilegeList.ShowAdd = false;
            PrivilegeList.ShowDelete = false;

            InitializeLists();
        }

        private void InitializeLists()
        {
            this.MemberList.AddText = "E-mail Address or MemberID";
            this.MemberList.DisplayMember = "EmailAddress";

            this.PrivilegeList.DisplayMember = "Description";   
            this.GroupList.DisplayMember = "Description";

            RefreshPrivilegeList();
        }

        private void PrivilegeList_SelectedIndexChanged(System.EventArgs e)
        {
            RefreshMemberListAndGroupList();
        }

        private void RefreshPrivilegeList()
        {
            this.PrivilegeList.Data = privilegesController.GetPrivileges(currentServiceEnvironmentTypeEnum);
            this.PrivilegeList.Refresh();
        }

        private void RefreshMemberListAndGroupList()
        {
            if (PrivilegeList.SelectedItem != null)
            {
                this.MemberList.Data = privilegesController.GetMembersByPrivilege(((Privilege)PrivilegeList.SelectedItem).PrivilegeID, currentServiceEnvironmentTypeEnum);
                this.MemberList.Refresh();

                this.GroupList.Data = privilegesController.GetGroupsByPrivilege(((Privilege)PrivilegeList.SelectedItem).PrivilegeID, currentServiceEnvironmentTypeEnum);
                this.GroupList.Refresh();
            }
        }

        private void MemberList_Delete(DeleteEventArgs args)
        {
            privilegesController.DeleteMemberPrivilege(((CachedMember)args.Deleted).MemberID, ((Privilege)PrivilegeList.SelectedItem).PrivilegeID, currentServiceEnvironmentTypeEnum);
        }

        private void MemberList_Add(AddEventArgs args)
        {
            privilegesController.SaveMemberPrivilege(((CachedMember)args.Added).MemberID, ((Privilege)PrivilegeList.SelectedItem).PrivilegeID, currentServiceEnvironmentTypeEnum);
        }

        private void GroupList_ItemDoubleClick(EventArgs e)
        {
            ManageGroups(true);
        }

        private void ManageGroups(Boolean useSelectionFlag)
        {
            Groups groups = new Groups(GlobalState, privilegesController);
            if (useSelectionFlag)
                groups.SelectedGroup = (SecurityGroup)GroupList.SelectedItem;
            groups.ShowDialog();
        }

        private void MemberList_ItemDoubleClick(System.EventArgs args)
        {
            ManageMembers(true);
        }

        private void ManageMembers(Boolean useSelectionFlag)
        {
            MemberPrivileges memberPrivileges = new MemberPrivileges(GlobalState, privilegesController);
            if (useSelectionFlag)
                memberPrivileges.Member = (CachedMember)MemberList.SelectedItem;
            memberPrivileges.ShowDialog();
        }

        private void ServiceEnvironmentComboBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            currentServiceEnvironmentTypeEnum = ((ServiceEnvironmentTypeEnum)ServiceEnvironmentComboBox.SelectedValue);

            // Do not allow ContentStg editing in Privileges.
            if (currentServiceEnvironmentTypeEnum == ServiceEnvironmentTypeEnum.ContentStg)
            {
                currentServiceEnvironmentTypeEnum = ServiceEnvironmentTypeEnum.Dev;
                ServiceEnvironmentComboBox.SelectedItem = ServiceEnvironmentTypeEnum.Dev;

                MessageBox.Show("ContentStg is not a valid EnvironmentType for Privileges.");
            }

            // Set the currentServiceEnvironmentTypeEnum so that the other Forms know what ServiceEnvironmentType we're working on.
            privilegesController.CurrentServiceEnvironmentTypeEnum = currentServiceEnvironmentTypeEnum;

            RefreshMemberListAndGroupList();
        }
        #endregion

        #region Properties
        [CreateNew]
        public PrivilegesController PrivilegesController
        {
            set
            {
                privilegesController = value;
            }
        }
        #endregion

        private void ManageMembersButton_Click(object sender, EventArgs e)
        {
            ManageMembers(false);
        }

        private void ManageGroupsButton_Click(object sender, EventArgs e)
        {
            ManageGroups(false);
        }
    }
}
