namespace Spark.Admin.PrivilegeModule
{
    partial class Groups
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GroupList = new Spark.Admin.PrivilegeModule.GroupList();
            this.MemberList = new Spark.Admin.PrivilegeModule.MemberList();
            this.MemberListLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // GroupList
            // 
            this.GroupList.AllowNew = false;
            this.GroupList.Data = null;
            this.GroupList.DisplayMember = null;
            this.GroupList.Location = new System.Drawing.Point(12, 12);
            this.GroupList.Name = "GroupList";
            this.GroupList.PrivilegesController = null;
            this.GroupList.SelectedItem = null;
            this.GroupList.ShowAdd = false;
            this.GroupList.ShowDelete = false;
            this.GroupList.Size = new System.Drawing.Size(524, 134);
            this.GroupList.TabIndex = 0;
            this.GroupList.SelectedIndexChanged += new Spark.Admin.PrivilegeModule.BaseList.SelectedIndexChangedEventHandler(this.GroupList_SelectedIndexChanged);
            // 
            // MemberList
            // 
            this.MemberList.Data = null;
            this.MemberList.DisplayMember = "EmailAddress";
            this.MemberList.Location = new System.Drawing.Point(12, 178);
            this.MemberList.Name = "MemberList";
            this.MemberList.NewItemText = "";
            this.MemberList.PrivilegesController = null;
            this.MemberList.SelectedItem = null;
            this.MemberList.ShowAdd = true;
            this.MemberList.ShowDelete = true;
            this.MemberList.Size = new System.Drawing.Size(524, 134);
            this.MemberList.TabIndex = 1;
            this.MemberList.Delete += new Spark.Admin.PrivilegeModule.BaseList.DeleteEventHandler(this.MemberList_Delete);
            this.MemberList.ItemDoubleClick += new Spark.Admin.PrivilegeModule.BaseList.ItemDoubleClickEventHandler(this.MemberList_ItemDoubleClick);
            this.MemberList.Add += new Spark.Admin.PrivilegeModule.BaseList.AddEventHandler(this.MemberList_Add);

            // 
            // MemberListLabel
            // 
            this.MemberListLabel.AutoSize = true;
            this.MemberListLabel.Location = new System.Drawing.Point(12, 162);
            this.MemberListLabel.Name = "MemberListLabel";
            this.MemberListLabel.Size = new System.Drawing.Size(183, 13);
            this.MemberListLabel.TabIndex = 2;
            this.MemberListLabel.Text = "Members Who Belong To This Group";
            // 
            // Groups
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(540, 324);
            this.Controls.Add(this.MemberListLabel);
            this.Controls.Add(this.MemberList);
            this.Controls.Add(this.GroupList);
            this.Name = "Groups";
            this.Text = "Groups";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GroupList GroupList;
        private MemberList MemberList;
        private System.Windows.Forms.Label MemberListLabel;

    }
}