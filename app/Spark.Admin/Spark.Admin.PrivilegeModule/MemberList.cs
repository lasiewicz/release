using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using Spark.Admin.Common;

using Matchnet.Member.ValueObjects;

namespace Spark.Admin.PrivilegeModule
{
    public partial class MemberList : BaseList
    {
        public MemberList()
        {
            InitializeComponent();
            this.DisplayMember = "EmailAddress";
        }

        #region Overrides
        protected override void onAdd()
        {
            Double memberIDDouble;
            CachedMember cachedMember = null;
            ArrayList cachedMembers;
            String newItemText = tbNewItem.Text.Trim();
            if (newItemText == String.Empty)
            {
                MessageBox.Show("Please enter a valid email address or MemberID.", Constants.MsgBoxCaptionWarning);
                return;
            }
            else if (newItemText.IndexOf('@') != -1)
            {
                cachedMembers = base.privilegesController.GetMembersByEmail(newItemText, base.privilegesController.CurrentServiceEnvironmentTypeEnum);

                if (cachedMembers.Count > 0)
                {
                    cachedMember = ((CachedMember)cachedMembers[0]);
                }
            }
            else if (Double.TryParse(newItemText, out memberIDDouble))
            {
                cachedMembers = base.privilegesController.GetMembersByMemberID((Int32)memberIDDouble, base.privilegesController.CurrentServiceEnvironmentTypeEnum);

                if (((CachedMember)cachedMembers[0]).EmailAddress != null)
                {
                    cachedMember = ((CachedMember)cachedMembers[0]);
                }
            }
            else
            {
                MessageBox.Show("Please enter a valid email address or MemberID.", Constants.MsgBoxCaptionWarning);
                return;
            }

            if (cachedMember != null)
            {
                this.AddItem(cachedMember);
                base.onAdd(cachedMember);
            }
            else
            {
                MessageBox.Show("That Member does not exist.", Constants.MsgBoxCaptionWarning);
                return;
            }
        }

        public override void AddItem(object newItem)
        {
            CachedMember newMember = (CachedMember)newItem;

            foreach (CachedMember member in Data)
            {
                if (member != null && member.MemberID == newMember.MemberID)
                {
                    MessageBox.Show("Cannot add this member.  Member is already in the list.");
                    return;
                }
            }

            base.AddItem(newItem);
        }
        #endregion

        #region Properties
        public String NewItemText
        {
            get
            {
                return base.tbNewItem.Text;
            }
            set 
            {
                base.tbNewItem.Text = value;
            }
        }
        #endregion
    }
}
