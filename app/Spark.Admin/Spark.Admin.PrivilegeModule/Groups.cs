using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI;

using Spark.Admin.Common;
using Matchnet.Member.ValueObjects.Privilege;
using Matchnet.Member.ValueObjects;

namespace Spark.Admin.PrivilegeModule
{
    public partial class Groups : Spark.Admin.Common.Controls.Form
    {
        private PrivilegesController privilegesController;

        #region Constructors
        public Groups(State globalState, PrivilegesController privilegesController)
            : base(globalState)
        {
            InitializeComponent();

            this.privilegesController = privilegesController;

            // Initialize controls.
            MemberList.PrivilegesController = privilegesController;
            GroupList.PrivilegesController = privilegesController;

            if (!HasPrivilege(ModuleWorkItem.PrivilegeIDGroupPrivilegeEditor)
                && !HasPrivilege(ModuleWorkItem.PrivilegeIDMemberPrivilegeEditor))
            {
                MemberList.ShowAdd = false;
                MemberList.ShowDelete = false;
            }

            DisplayGroups();
        }
        #endregion

        #region Private Methods
        private void DisplayGroups()
        {
            this.GroupList.Data = privilegesController.GetSecurityGroups(privilegesController.CurrentServiceEnvironmentTypeEnum);
            this.GroupList.DisplayMember = "Description";
            this.MemberList.AddText = "E-mail Address or MemberID";
        }

        private void GroupList_SelectedIndexChanged(System.EventArgs args)
        {
            SecurityGroupMemberCollection groupMembers = privilegesController.GetSecurityGroupMembers(this.SelectedGroup.SecurityGroupID
                , privilegesController.CurrentServiceEnvironmentTypeEnum);

            List<CachedMember> members = new List<CachedMember>();
            foreach (Int32 memberID in groupMembers)
            {
                try
                {
                    members.Add(((CachedMember)privilegesController.GetMembersByMemberID(memberID, privilegesController.CurrentServiceEnvironmentTypeEnum)[0]));
                }
                catch
                {
                    // Sometimes faulty data causes the GetMember call to fail.  Do not blow up because of data.
                }
            }

            this.MemberList.Data = members;
            this.MemberList.DisplayMember = "EmailAddress";
        }

        private void MemberList_ItemDoubleClick(System.EventArgs args)
        {
            MemberPrivileges memberPrivileges = new MemberPrivileges(GlobalState, privilegesController);
            memberPrivileges.Member = (CachedMember)MemberList.SelectedItem;
            memberPrivileges.ShowDialog();
        }

        private void MemberList_Add(AddEventArgs args)
        {
            privilegesController.SaveSecurityGroupMember(((CachedMember)args.Added).MemberID, this.SelectedGroup.SecurityGroupID
                , privilegesController.CurrentServiceEnvironmentTypeEnum);
        }

        private void MemberList_Delete(DeleteEventArgs args)
        {
            privilegesController.DeleteSecurityGroupMember(((CachedMember)args.Deleted).MemberID, this.SelectedGroup.SecurityGroupID
                , privilegesController.CurrentServiceEnvironmentTypeEnum);
        }
        #endregion

        #region Properties
        public SecurityGroup SelectedGroup
        {
            get
            {
                return (SecurityGroup)GroupList.SelectedItem;
            }
            set
            {
                GroupList.SelectedItem = value;
            }
        }
        #endregion
    }
}