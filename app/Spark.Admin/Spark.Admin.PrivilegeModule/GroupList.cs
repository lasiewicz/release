using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using Matchnet.Member.ValueObjects.Privilege;

namespace Spark.Admin.PrivilegeModule
{
    public partial class GroupList : BaseList
    {
        private Boolean _allowNew;

        public GroupList()
        {
            InitializeComponent();
        }

        #region Properties
        public override object SelectedItem
        {
            get
            {
                return base.SelectedItem;
            }
            set
            {
                if (base.Data != null && value != null)
                {
                    SecurityGroup newGroup = (SecurityGroup)value;

                    foreach (SecurityGroup group in base.Data)  
                    {
                        if (group.SecurityGroupID == newGroup.SecurityGroupID)
                        {
                            base.SelectedItem = group;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// If true, the user can enter a new group description and a new SecurityGroup will be created and added to the list.
        /// Otherwise, the user can select from existing SecurityGroups and add these to the list.
        /// Cannot be changed after the control loads.
        /// </summary>
        public Boolean AllowNew
        {
            get
            {
                return _allowNew;
            }
            set
            {
                _allowNew = value;
            }
        }
        #endregion

        private void GroupList_Load(object sender, System.EventArgs e)
        {
            //Per Garry, cannot modify mnSystem from code, so for now not allowing modifications to groups
            base.ShowAdd = false;
            base.ShowDelete = false;
            //base.tbNewItem.Visible = false;
            this.cbGroups.Visible = false;
            this.AddText = String.Empty;

            /*if (!AllowNew)
            {
                this.tbNewItem.Visible = false;
                this.cbGroups.Visible = true;
                cbGroups.DataSource = MemberPrivilegeSA.Instance.GetSecurityGroups();
                cbGroups.DisplayMember = "Description";
            }
            else
            {
                this.tbNewItem.Visible = true;
                this.cbGroups.Visible = false;
            }*/
        }

        #region Overrides
        //protected override void onAdd()
        //{
        //    this.AddItem(this.cbGroups.SelectedItem);
        //    base.onAdd(this.cbGroups.SelectedItem);
        //}

        /*public override void AddItem(object newItem)
        {
            Member newMember = (Member) newItem;

            foreach (Member member in Data)
            {
                if (member.MemberID == newMember.MemberID)
                {
                    MessageBox.Show("Cannot add this member.  Member is already in the list.");
                    return;
                }
            }

            base.AddItem(newItem);
        }*/
        #endregion
    }
}
