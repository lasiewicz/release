using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Security.Principal;

using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.CompositeUI;
    
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.ResourcePush.ServiceAdapters;
using Matchnet.ResourcePush.ValueObjects;
using Matchnet.FileDeploy.ValueObjects;
using Matchnet.Configuration.ValueObjects.Servers;

using Spark.Admin.Common;

using Matchnet.P4Wrapper.ValueObjects;

namespace Spark.Admin.DeploymentModule
{
    public partial class DeploymentView : BaseModuleView
    {
        #region Global variables
        private const String TitleDeployment = "File/Resource Deployment";

        /// <summary>
        /// The datatable to be used for VSS resource history.  This is cleared and 
        /// added to frequently.
        /// </summary>
        private DataTable FileHistoryTable = new DataTable();
        
        /// <summary>
        /// The datatable to be used for the list of files to deploy.  This is used 
        /// for both images and resources.
        /// </summary>
        Matchnet.ResourcePush.ValueObjects.DeployFileCollection FilesToDeploy = new DeployFileCollection();

        /// <summary>
        /// The server type that the files should be deployed too.  The default is 
        /// staging as a safety measure.  
        /// </summary>
        ServerType serverType = ServerType.Staging;

        OpenFileDialog ofDlg = new OpenFileDialog();

        #endregion

        #region Constructor
        public DeploymentView()
        {
            // Initialize the winforms
            InitializeComponent();

            // Set the Tab title.
            this.Title = TitleDeployment;
        }
        #endregion

        #region Resource Selection Event Handlers
        /// <summary>
        /// When the selected item in the resource list changes, refresh the resource history with that resource's VSS history.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbResources_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResources.SelectedItem != null)
            {
                P4File p4File = (P4File)lbResources.SelectedItem;

                List<P4FileRevision> p4FileRevisions = ResourcePushSA.Instance.GetResourceHistory(p4File);

                FileHistoryTable = Util.CreateTable();

                foreach (P4FileRevision p4FileRevision in p4FileRevisions)
                {
                    DataRow row = FileHistoryTable.NewRow();

                    row["Revision"] = p4FileRevision.Revision;
                    row["Description"] = p4FileRevision.Description;

                    FileHistoryTable.Rows.Add(row);
                }

                FileHistoryTable.DefaultView.Sort = "Revision desc";

                this.dgResourceHistory.DataSource = FileHistoryTable;
                this.dgResourceHistory.Visible = true;
            }
        }

        /// <summary>
        /// Add a resource file and version to the list of resources to deploy  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgResourceHistory_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string revision = this.dgResourceHistory.Rows[e.RowIndex].Cells[0].Value.ToString();
            P4File p4File = (P4File) this.lbResources.SelectedItem;

            Matchnet.ResourcePush.ValueObjects.DeployFile deployFile;

            if (p4File.DepotPath.ToLower().IndexOf(".resx") > -1)
            {
                deployFile = new DeployFile(p4File.LocalPath, revision, FileType.Resource);
            }
            else
            {
                deployFile = new DeployFile(p4File.LocalPath, revision, FileType.Image);
            }

            bool AlreadyExists = false;
            
            foreach (Matchnet.ResourcePush.ValueObjects.DeployFile deployFileIterator in FilesToDeploy)
            {
                if (deployFile.FileName == deployFileIterator.FileName)
                {
                    AlreadyExists = true;
                }
            }

            if (!AlreadyExists)
            {
                FilesToDeploy.Add(deployFile);
                BindResourcesToDeploy();

                this.Tabs.SelectedTab = this.Deploy;
            }
            else
            {
                MessageBox.Show("Another version of this file is already selected for deployment.");
            }
        }

        #endregion
        
        #region Resource Selection Methods
        /// <summary>
        /// Bind a resource to the listbox of resource to deploy.
        /// </summary>
        private void BindResourcesToDeploy()
        {
            lbResourcesToDeploy.Items.Clear();

            foreach (Matchnet.ResourcePush.ValueObjects.DeployFile deployFile in FilesToDeploy)
            {
              lbResourcesToDeploy.Items.Add("Version " + deployFile.Version + " of " + deployFile.FileName);
            }
        }
        #endregion

        #region Deployment Methods
        /// <summary>
        /// Deploys the files to their destined servers.
        /// Results are then bound to the result grid and sorted accordingly.
        /// </summary>
        /// <param name="FilesToDeploy"></param>
        private void DeployFiles(DeployFileCollection FilesToDeploy)
        {
            DataTable ResultsTable = Util.CreateResultsTable();
            lblDeployTime.Text = "Start Time: " + DateTime.Now.ToString() + " " + DateTime.Now.Second.ToString() + "." + DateTime.Now.Millisecond.ToString();
            FileDeploymentResultCollection FileDeploymentResults = ResourcePushSA.Instance.DeployFiles(FilesToDeploy, ServerType.Web | serverType);
            lblResultsTime.Text = "End Time: " + DateTime.Now.ToString() + " " + DateTime.Now.Second.ToString() + "." + DateTime.Now.Millisecond.ToString();

            this.dgResults.Visible = true;

            foreach (FileDeploymentResult fileResult in FileDeploymentResults)
            {
                foreach (DeploymentResult deploymentResult in fileResult)
                {
                    DataRow row = ResultsTable.NewRow();

                    row["Filename"] = fileResult.FileName;
                    row["Server"] = deploymentResult.ServerName;
                    row["Results"] = deploymentResult.Status.ToString();
                    row["Error Message"] = deploymentResult.ErrorText;

                    ResultsTable.Rows.Add(row);

                }

                this.dgResults.DataSource = ResultsTable;
                Application.DoEvents();
            }

            ResultsTable.DefaultView.Sort = "filename desc, server desc";
        }
        #endregion

        #region Deployment Event Handlers

        /// <summary>
        /// When attempting to deploy to stage, we hide the Deployment buttons and active the confirmation panel.  
        /// ServerType must be set to staging.  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeployStage_Click(object sender, EventArgs e)
        {
            if (FilesToDeploy.Count > 0)
            {
                btnDeployStage.Enabled = false;
                btnDeployProduction.Enabled = false;
                btnDeployContentStage.Enabled = false;

                pnlConfirmation.Visible = true;
                serverType = ServerType.Staging;
            }
            else
            {
                MessageBox.Show("No files to deploy, please add files.");
            }
        }

        /// <summary>
        /// When attempting to deploy to production, we hide the Deployment buttons and active the confirmation panel.  
        /// ServerType must be set to production.  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeployProduction_Click(object sender, EventArgs e)
        {
            if (FilesToDeploy.Count > 0)
            {
                btnDeployProduction.Enabled = false;
                btnDeployStage.Enabled = false;
                btnDeployContentStage.Enabled = false;

                pnlConfirmation.Visible = true;
                serverType = ServerType.Production;
            }
            else
            {
                MessageBox.Show("No files to deploy, please add files.");
            }
        }

        /// <summary>
        /// Deploys selected items to the servers specified.  
        /// Removes the list of tabs and creates new tab Results.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnYes_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            this.Tabs.Controls.Remove(this.Resources);
            this.Tabs.Controls.Remove(this.Deploy);

            this.Tabs.Controls.Add(this.Results);
            this.Tabs.Controls.Add(this.DeploymentStatus);
            this.Tabs.SelectedTab = Results;

            DeployFiles(FilesToDeploy);

            Cursor.Current = Cursors.Default;
        }

        /// <summary>
        /// If the user clicks No on the confirmation panel, 
        /// we reeenable the deployment buttons and hide the confirmation panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNo_Click(object sender, EventArgs e)
        {
            btnDeployProduction.Enabled = true;
            btnDeployStage.Enabled = true;
            btnDeployContentStage.Enabled = true;

            pnlConfirmation.Visible = false;
        }

        /// <summary>
        /// Removes a resource file from teh list of files to be deployed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbResourcesToDeploy_DoubleClick(object sender, System.EventArgs e)
        {
            foreach (DeployFile deployFile in FilesToDeploy)
            {
                if (lbResourcesToDeploy.SelectedItem.ToString().Contains(deployFile.FileName))
                {
                    FilesToDeploy.Remove(deployFile);
                    lbResourcesToDeploy.Items.Remove(lbResourcesToDeploy.SelectedItem.ToString());
                    lbResourcesToDeploy.SelectedItem = "";
                    MessageBox.Show("File removed.");
                    break;
                }
            }
        }

        #endregion      

        private void btnRestart_Click(object sender, EventArgs e)
        {
            btnDeployProduction.Enabled = true;
            btnDeployStage.Enabled = true;
            btnDeployContentStage.Enabled = true;
            
            pnlConfirmation.Visible = false;

            this.Tabs.Controls.Add(this.Resources);
            this.Tabs.Controls.Add(this.Deploy);
            this.Tabs.SelectedTab = Resources;

            this.Tabs.Controls.Remove(this.DeploymentStatus);
            this.Tabs.Controls.Remove(this.Results);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (FilesToDeploy.Count > 0)
            {
                btnDeployProduction.Enabled = false;
                btnDeployStage.Enabled = false;
                btnDeployContentStage.Enabled = false;

                pnlConfirmation.Visible = true;
                serverType = ServerType.ContentStaging;
            }
            else
            {
                MessageBox.Show("No files to deploy, please add files.");
            }
        }

        private void btnSavePassword_Click(object sender, EventArgs e)
        {

            // Hide the resource history out of user convenience
            this.dgResourceHistory.Visible = false;
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (SearchTextBox.Text.Trim().Length < 4)
            {
                MessageBox.Show("Search string must be more than 3 characters(i.e. \"103.en-US.resx\"). Please try again.", "Invalid Search String",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            this.lbResources.Items.Clear();
            this.lbResources.ClearSelected();
            this.FileHistoryTable.Clear();
            this.dgResourceHistory.ClearSelection();
            this.dgResourceHistory.Visible = false;

            List<P4File> p4Files = ResourcePushSA.Instance.ResourceList(SearchTextBox.Text.Trim());

            this.lbResources.DisplayMember = "DepotPath";
            this.lbResources.Sorted = true;

            foreach (P4File p4File in p4Files)
            {
                this.lbResources.Items.Add(p4File);
            }

            if (p4Files.Count == 0)
            {
                MessageBox.Show("0 search results. Please try again with a different keyword.", "Spark.Admin", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            Cursor.Current = Cursors.Default;
        }
    }
}