using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.CompositeUI.WinForms;

using Spark.Admin.Common;

namespace Spark.Admin.DeploymentModule
{
    public class ModuleWorkItem : BaseModuleWorkItem
    {
        public const Int32 PrivilegeIDFileAndResourcePublisher = 152;
        private IWorkspace workspace;

        #region Public Methods
        public void Run(IWorkspace workspace)
        {
            this.workspace = workspace;

            DeploymentView deploymentView = this.Items.AddNew<DeploymentView>();
            // You must manually assign GlobalState to the ModuleView.
            deploymentView.GlobalState = GlobalState;

            this.Items.Add(deploymentView);

            // Set the Tab properties.
            TabSmartPartInfo tabSmartPartInfo = new TabSmartPartInfo();
            tabSmartPartInfo.Title = deploymentView.Title;

            workspace.Show(deploymentView, tabSmartPartInfo);
        }
        #endregion
    }
}
