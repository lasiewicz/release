using System.Security.Principal;

namespace Spark.Admin.DeploymentModule
{
    partial class DeploymentView
    {

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        /*protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }*/

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Results = new System.Windows.Forms.TabPage();
            this.dgResults = new System.Windows.Forms.DataGridView();
            this.Deploy = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDeployContentStage = new System.Windows.Forms.Button();
            this.btnDeployStage = new System.Windows.Forms.Button();
            this.btnDeployProduction = new System.Windows.Forms.Button();
            this.pnlConfirmation = new System.Windows.Forms.Panel();
            this.btnNo = new System.Windows.Forms.Button();
            this.btnYes = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblResources = new System.Windows.Forms.Label();
            this.lbResourcesToDeploy = new System.Windows.Forms.ListBox();
            this.Resources = new System.Windows.Forms.TabPage();
            this.ResourceContainer = new System.Windows.Forms.SplitContainer();
            this.SearchButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SearchTextBox = new System.Windows.Forms.TextBox();
            this.ResourceAndHistoryContainer = new System.Windows.Forms.SplitContainer();
            this.lbResources = new System.Windows.Forms.ListBox();
            this.dgResourceHistory = new System.Windows.Forms.DataGridView();
            this.Tabs = new System.Windows.Forms.TabControl();
            this.tbPassword = new System.Windows.Forms.TabPage();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.btnSavePassword = new System.Windows.Forms.Button();
            this.DeploymentStatus = new System.Windows.Forms.TabPage();
            this.btnRestart = new System.Windows.Forms.Button();
            this.lblTimeToDeploy = new System.Windows.Forms.Label();
            this.lblResultsTime = new System.Windows.Forms.Label();
            this.lblDeployTime = new System.Windows.Forms.Label();
            this.Results.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgResults)).BeginInit();
            this.Deploy.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlConfirmation.SuspendLayout();
            this.Resources.SuspendLayout();
            this.ResourceContainer.Panel1.SuspendLayout();
            this.ResourceContainer.Panel2.SuspendLayout();
            this.ResourceContainer.SuspendLayout();
            this.ResourceAndHistoryContainer.Panel1.SuspendLayout();
            this.ResourceAndHistoryContainer.Panel2.SuspendLayout();
            this.ResourceAndHistoryContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgResourceHistory)).BeginInit();
            this.Tabs.SuspendLayout();
            this.tbPassword.SuspendLayout();
            this.DeploymentStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // Results
            // 
            this.Results.Controls.Add(this.dgResults);
            this.Results.Location = new System.Drawing.Point(4, 22);
            this.Results.Name = "Results";
            this.Results.Size = new System.Drawing.Size(675, 522);
            this.Results.TabIndex = 3;
            this.Results.Text = "Results";
            this.Results.UseVisualStyleBackColor = true;
            // 
            // dgResults
            // 
            this.dgResults.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.dgResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgResults.Location = new System.Drawing.Point(0, 0);
            this.dgResults.Name = "dgResults";
            this.dgResults.Size = new System.Drawing.Size(675, 522);
            this.dgResults.TabIndex = 0;
            // 
            // Deploy
            // 
            this.Deploy.Controls.Add(this.tableLayoutPanel1);
            this.Deploy.Location = new System.Drawing.Point(4, 22);
            this.Deploy.Name = "Deploy";
            this.Deploy.Padding = new System.Windows.Forms.Padding(3);
            this.Deploy.Size = new System.Drawing.Size(675, 522);
            this.Deploy.TabIndex = 2;
            this.Deploy.Text = "Files to Deploy";
            this.Deploy.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblResources, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbResourcesToDeploy, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.46888F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 92.53112F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 208F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(695, 577);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnDeployContentStage);
            this.panel1.Controls.Add(this.btnDeployStage);
            this.panel1.Controls.Add(this.btnDeployProduction);
            this.panel1.Controls.Add(this.pnlConfirmation);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 371);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(689, 203);
            this.panel1.TabIndex = 5;
            // 
            // btnDeployContentStage
            // 
            this.btnDeployContentStage.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeployContentStage.Location = new System.Drawing.Point(3, 50);
            this.btnDeployContentStage.Name = "btnDeployContentStage";
            this.btnDeployContentStage.Size = new System.Drawing.Size(193, 33);
            this.btnDeployContentStage.TabIndex = 4;
            this.btnDeployContentStage.Text = "Deploy Files to ContentStage";
            this.btnDeployContentStage.UseVisualStyleBackColor = true;
            this.btnDeployContentStage.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnDeployStage
            // 
            this.btnDeployStage.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeployStage.Location = new System.Drawing.Point(3, 7);
            this.btnDeployStage.Name = "btnDeployStage";
            this.btnDeployStage.Size = new System.Drawing.Size(193, 31);
            this.btnDeployStage.TabIndex = 0;
            this.btnDeployStage.Text = "Deploy Files to Stage";
            this.btnDeployStage.UseVisualStyleBackColor = true;
            this.btnDeployStage.Click += new System.EventHandler(this.btnDeployStage_Click);
            // 
            // btnDeployProduction
            // 
            this.btnDeployProduction.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeployProduction.Location = new System.Drawing.Point(3, 93);
            this.btnDeployProduction.Name = "btnDeployProduction";
            this.btnDeployProduction.Size = new System.Drawing.Size(193, 31);
            this.btnDeployProduction.TabIndex = 0;
            this.btnDeployProduction.Text = "Deploy Files to Production";
            this.btnDeployProduction.UseVisualStyleBackColor = true;
            this.btnDeployProduction.Click += new System.EventHandler(this.btnDeployProduction_Click);
            // 
            // pnlConfirmation
            // 
            this.pnlConfirmation.Controls.Add(this.btnNo);
            this.pnlConfirmation.Controls.Add(this.btnYes);
            this.pnlConfirmation.Controls.Add(this.label1);
            this.pnlConfirmation.Location = new System.Drawing.Point(271, 17);
            this.pnlConfirmation.Name = "pnlConfirmation";
            this.pnlConfirmation.Size = new System.Drawing.Size(312, 107);
            this.pnlConfirmation.TabIndex = 3;
            this.pnlConfirmation.Visible = false;
            // 
            // btnNo
            // 
            this.btnNo.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNo.Location = new System.Drawing.Point(143, 62);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(75, 29);
            this.btnNo.TabIndex = 4;
            this.btnNo.Text = "No";
            this.btnNo.UseVisualStyleBackColor = true;
            this.btnNo.Click += new System.EventHandler(this.btnNo_Click);
            // 
            // btnYes
            // 
            this.btnYes.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnYes.Location = new System.Drawing.Point(35, 62);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(75, 29);
            this.btnYes.TabIndex = 3;
            this.btnYes.Text = "Yes";
            this.btnYes.UseVisualStyleBackColor = true;
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(288, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Are you sure you want to deploy the above files?";
            // 
            // lblResources
            // 
            this.lblResources.AutoSize = true;
            this.lblResources.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblResources.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResources.Location = new System.Drawing.Point(3, 0);
            this.lblResources.Name = "lblResources";
            this.lblResources.Size = new System.Drawing.Size(689, 13);
            this.lblResources.TabIndex = 2;
            this.lblResources.Text = "Resources for Deployment";
            // 
            // lbResourcesToDeploy
            // 
            this.lbResourcesToDeploy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbResourcesToDeploy.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbResourcesToDeploy.HorizontalScrollbar = true;
            this.lbResourcesToDeploy.Location = new System.Drawing.Point(3, 27);
            this.lbResourcesToDeploy.Name = "lbResourcesToDeploy";
            this.lbResourcesToDeploy.Size = new System.Drawing.Size(689, 290);
            this.lbResourcesToDeploy.TabIndex = 1;
            this.lbResourcesToDeploy.DoubleClick += new System.EventHandler(this.lbResourcesToDeploy_DoubleClick);
            // 
            // Resources
            // 
            this.Resources.Controls.Add(this.ResourceContainer);
            this.Resources.Location = new System.Drawing.Point(4, 22);
            this.Resources.Name = "Resources";
            this.Resources.Padding = new System.Windows.Forms.Padding(3);
            this.Resources.Size = new System.Drawing.Size(675, 522);
            this.Resources.TabIndex = 0;
            this.Resources.Text = "Select Resources";
            this.Resources.UseVisualStyleBackColor = true;
            // 
            // ResourceContainer
            // 
            this.ResourceContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ResourceContainer.IsSplitterFixed = true;
            this.ResourceContainer.Location = new System.Drawing.Point(3, 3);
            this.ResourceContainer.Name = "ResourceContainer";
            this.ResourceContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // ResourceContainer.Panel1
            // 
            this.ResourceContainer.Panel1.Controls.Add(this.SearchButton);
            this.ResourceContainer.Panel1.Controls.Add(this.label2);
            this.ResourceContainer.Panel1.Controls.Add(this.label3);
            this.ResourceContainer.Panel1.Controls.Add(this.SearchTextBox);
            this.ResourceContainer.Panel1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // ResourceContainer.Panel2
            // 
            this.ResourceContainer.Panel2.Controls.Add(this.ResourceAndHistoryContainer);
            this.ResourceContainer.Size = new System.Drawing.Size(669, 516);
            this.ResourceContainer.SplitterDistance = 26;
            this.ResourceContainer.TabIndex = 4;
            // 
            // SearchButton
            // 
            this.SearchButton.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchButton.Location = new System.Drawing.Point(415, 3);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(91, 20);
            this.SearchButton.TabIndex = 3;
            this.SearchButton.Text = "Search";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "File Search String:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(438, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 2;
            // 
            // SearchTextBox
            // 
            this.SearchTextBox.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchTextBox.Location = new System.Drawing.Point(142, 1);
            this.SearchTextBox.Name = "SearchTextBox";
            this.SearchTextBox.Size = new System.Drawing.Size(267, 22);
            this.SearchTextBox.TabIndex = 0;
            // 
            // ResourceAndHistoryContainer
            // 
            this.ResourceAndHistoryContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ResourceAndHistoryContainer.Location = new System.Drawing.Point(0, 0);
            this.ResourceAndHistoryContainer.Name = "ResourceAndHistoryContainer";
            this.ResourceAndHistoryContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // ResourceAndHistoryContainer.Panel1
            // 
            this.ResourceAndHistoryContainer.Panel1.Controls.Add(this.lbResources);
            // 
            // ResourceAndHistoryContainer.Panel2
            // 
            this.ResourceAndHistoryContainer.Panel2.Controls.Add(this.dgResourceHistory);
            this.ResourceAndHistoryContainer.Size = new System.Drawing.Size(669, 486);
            this.ResourceAndHistoryContainer.SplitterDistance = 241;
            this.ResourceAndHistoryContainer.TabIndex = 3;
            // 
            // lbResources
            // 
            this.lbResources.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbResources.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbResources.FormattingEnabled = true;
            this.lbResources.Location = new System.Drawing.Point(0, 0);
            this.lbResources.Name = "lbResources";
            this.lbResources.Size = new System.Drawing.Size(669, 238);
            this.lbResources.TabIndex = 1;
            this.lbResources.SelectedIndexChanged += new System.EventHandler(this.lbResources_SelectedIndexChanged);
            // 
            // dgResourceHistory
            // 
            this.dgResourceHistory.AllowUserToAddRows = false;
            this.dgResourceHistory.AllowUserToDeleteRows = false;
            this.dgResourceHistory.AllowUserToOrderColumns = true;
            this.dgResourceHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgResourceHistory.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgResourceHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgResourceHistory.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgResourceHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgResourceHistory.Location = new System.Drawing.Point(0, 0);
            this.dgResourceHistory.MultiSelect = false;
            this.dgResourceHistory.Name = "dgResourceHistory";
            this.dgResourceHistory.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgResourceHistory.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgResourceHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgResourceHistory.Size = new System.Drawing.Size(669, 241);
            this.dgResourceHistory.TabIndex = 2;
            this.dgResourceHistory.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgResourceHistory_CellContentDoubleClick);
            // 
            // Tabs
            // 
            this.Tabs.Controls.Add(this.Resources);
            this.Tabs.Controls.Add(this.Deploy);
            this.Tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tabs.Location = new System.Drawing.Point(0, 0);
            this.Tabs.Name = "Tabs";
            this.Tabs.SelectedIndex = 0;
            this.Tabs.Size = new System.Drawing.Size(683, 548);
            this.Tabs.TabIndex = 2;
            // 
            // tbPassword
            // 
            this.tbPassword.Controls.Add(this.txtPassword);
            this.tbPassword.Controls.Add(this.lblPassword);
            this.tbPassword.Controls.Add(this.btnSavePassword);
            this.tbPassword.Location = new System.Drawing.Point(4, 22);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.Size = new System.Drawing.Size(675, 522);
            this.tbPassword.TabIndex = 3;
            this.tbPassword.Text = "Password";
            this.tbPassword.UseVisualStyleBackColor = true;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(31, 54);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(137, 20);
            this.txtPassword.TabIndex = 3;
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(28, 31);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(140, 13);
            this.lblPassword.TabIndex = 2;
            this.lblPassword.Text = "Please enter your password:";
            // 
            // btnSavePassword
            // 
            this.btnSavePassword.Location = new System.Drawing.Point(31, 86);
            this.btnSavePassword.Name = "btnSavePassword";
            this.btnSavePassword.Size = new System.Drawing.Size(100, 23);
            this.btnSavePassword.TabIndex = 1;
            this.btnSavePassword.Text = "Save Password";
            this.btnSavePassword.UseVisualStyleBackColor = true;
            this.btnSavePassword.Click += new System.EventHandler(this.btnSavePassword_Click);
            // 
            // DeploymentStatus
            // 
            this.DeploymentStatus.Controls.Add(this.btnRestart);
            this.DeploymentStatus.Controls.Add(this.lblTimeToDeploy);
            this.DeploymentStatus.Controls.Add(this.lblResultsTime);
            this.DeploymentStatus.Controls.Add(this.lblDeployTime);
            this.DeploymentStatus.Location = new System.Drawing.Point(4, 22);
            this.DeploymentStatus.Name = "DeploymentStatus";
            this.DeploymentStatus.Padding = new System.Windows.Forms.Padding(3);
            this.DeploymentStatus.Size = new System.Drawing.Size(675, 522);
            this.DeploymentStatus.TabIndex = 4;
            this.DeploymentStatus.Text = "Deployment Time";
            this.DeploymentStatus.UseVisualStyleBackColor = true;
            // 
            // btnRestart
            // 
            this.btnRestart.Location = new System.Drawing.Point(26, 140);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(231, 23);
            this.btnRestart.TabIndex = 3;
            this.btnRestart.Text = "Restart and Deploy More Resources";
            this.btnRestart.UseVisualStyleBackColor = true;
            this.btnRestart.Click += new System.EventHandler(this.btnRestart_Click);
            // 
            // lblTimeToDeploy
            // 
            this.lblTimeToDeploy.AutoSize = true;
            this.lblTimeToDeploy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimeToDeploy.Location = new System.Drawing.Point(23, 30);
            this.lblTimeToDeploy.Name = "lblTimeToDeploy";
            this.lblTimeToDeploy.Size = new System.Drawing.Size(114, 16);
            this.lblTimeToDeploy.TabIndex = 2;
            this.lblTimeToDeploy.Text = "Time to Deploy";
            // 
            // lblResultsTime
            // 
            this.lblResultsTime.AutoSize = true;
            this.lblResultsTime.Location = new System.Drawing.Point(23, 96);
            this.lblResultsTime.Name = "lblResultsTime";
            this.lblResultsTime.Size = new System.Drawing.Size(52, 13);
            this.lblResultsTime.TabIndex = 1;
            this.lblResultsTime.Text = "End Time";
            // 
            // lblDeployTime
            // 
            this.lblDeployTime.AutoSize = true;
            this.lblDeployTime.Location = new System.Drawing.Point(23, 67);
            this.lblDeployTime.Name = "lblDeployTime";
            this.lblDeployTime.Size = new System.Drawing.Size(55, 13);
            this.lblDeployTime.TabIndex = 0;
            this.lblDeployTime.Text = "Start Time";
            // 
            // DeploymentView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.Tabs);
            this.Name = "DeploymentView";
            this.Size = new System.Drawing.Size(683, 548);
            this.Results.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgResults)).EndInit();
            this.Deploy.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.pnlConfirmation.ResumeLayout(false);
            this.pnlConfirmation.PerformLayout();
            this.Resources.ResumeLayout(false);
            this.ResourceContainer.Panel1.ResumeLayout(false);
            this.ResourceContainer.Panel1.PerformLayout();
            this.ResourceContainer.Panel2.ResumeLayout(false);
            this.ResourceContainer.ResumeLayout(false);
            this.ResourceAndHistoryContainer.Panel1.ResumeLayout(false);
            this.ResourceAndHistoryContainer.Panel2.ResumeLayout(false);
            this.ResourceAndHistoryContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgResourceHistory)).EndInit();
            this.Tabs.ResumeLayout(false);
            this.tbPassword.ResumeLayout(false);
            this.tbPassword.PerformLayout();
            this.DeploymentStatus.ResumeLayout(false);
            this.DeploymentStatus.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage Results;
        private System.Windows.Forms.TabPage Deploy;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnDeployProduction;
        private System.Windows.Forms.Panel pnlConfirmation;
        private System.Windows.Forms.Button btnNo;
        private System.Windows.Forms.Button btnYes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblResources;
        private System.Windows.Forms.ListBox lbResourcesToDeploy;
        private System.Windows.Forms.TabPage Resources;
        private System.Windows.Forms.SplitContainer ResourceContainer;
        private System.Windows.Forms.SplitContainer ResourceAndHistoryContainer;
        private System.Windows.Forms.ListBox lbResources;
        private System.Windows.Forms.DataGridView dgResourceHistory;
        private System.Windows.Forms.TabControl Tabs;
        private System.Windows.Forms.DataGridView dgResults;
        private System.Windows.Forms.Button btnDeployStage;
        private System.Windows.Forms.TabPage DeploymentStatus;
        private System.Windows.Forms.Label lblResultsTime;
        private System.Windows.Forms.Label lblDeployTime;
        private System.Windows.Forms.Label lblTimeToDeploy;
        private System.Windows.Forms.Button btnRestart;
        private System.Windows.Forms.Button btnDeployContentStage;
        private System.Windows.Forms.TabPage tbPassword;
        private System.Windows.Forms.Button btnSavePassword;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox SearchTextBox;
    }
}

