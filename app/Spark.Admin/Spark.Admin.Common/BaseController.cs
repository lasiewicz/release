using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

using Microsoft.Practices.CompositeUI;

using Matchnet.Member.ValueObjects.Privilege;
using Matchnet.Content.ValueObjects.Admin;

namespace Spark.Admin.Common
{
    public class BaseController : Controller
    {
        private State globalState;
        private ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum;

        #region Constructors
        #endregion

        #region Public Methods
        public Boolean HasPrivilege(Int32 privilegeID)
        {
            return ((MemberPrivilegeCollection)GlobalState[Constants.StateMemberPrivileges]).HasPrivilege(privilegeID);
        }
        #endregion

        #region Properties
        public State GlobalState
        {
            get
            {
                return globalState;
            }
            set
            {
                globalState = value;
            }
        }

        public ServiceEnvironmentTypeEnum CurrentServiceEnvironmentTypeEnum
        {
            get
            {
                return currentServiceEnvironmentTypeEnum;
            }
            set
            {
                currentServiceEnvironmentTypeEnum = value;
            }
        }

        public static void HandleError(String moduleName, String errorMessage)
        {
            ErrorForm errorForm = new ErrorForm(System.DateTime.Now.ToString("g") + "\t" + moduleName + "\t" + errorMessage);

            System.Diagnostics.EventLog.WriteEntry("Spark.Admin", errorMessage, System.Diagnostics.EventLogEntryType.Error);

            DialogResult dialogResult = errorForm.ShowDialog();

            if (dialogResult == System.Windows.Forms.DialogResult.OK)
            {
                errorForm.Dispose();

                Thread.CurrentThread.Abort();
            }
        }
        #endregion
    }
}
