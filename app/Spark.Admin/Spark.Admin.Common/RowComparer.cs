using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Windows.Forms;

namespace Spark.Admin.Common
{
    public class RowComparer : System.Collections.IComparer
    {
        private Int16 sortOrderModifier = 1;
        private Int32 columnIndex;

        public RowComparer(ListSortDirection listSortDirection, Int32 columnIndex)
        {
            if (listSortDirection == ListSortDirection.Descending)
            {
                sortOrderModifier = -1;
            }
            else if (listSortDirection == ListSortDirection.Ascending)
            {
                sortOrderModifier = 1;
            }

            this.columnIndex = columnIndex;
        }

        public Int32 Compare(Object x, Object y)
        {
            DataGridViewRow dataGridViewRow1 = (DataGridViewRow)x;
            DataGridViewRow dataGridViewRow2 = (DataGridViewRow)y;

            // Try to sort based on the Last Name column.
            Int32 compareResult = System.String.Compare(
                dataGridViewRow1.Cells[columnIndex].Value.ToString(),
                dataGridViewRow2.Cells[columnIndex].Value.ToString());

            //// If the Last Names are equal, sort based on the First Name.
            //if (CompareResult == 0)
            //{
            //    CompareResult = System.String.Compare(
            //        dataGridViewRow1.Cells[0].Value.ToString(),
            //        dataGridViewRow2.Cells[0].Value.ToString());
            //}
            return compareResult * sortOrderModifier;
        }
    }
}
