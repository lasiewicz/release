using System;
using System.Collections.Generic;
using System.Text;

namespace Spark.Admin.Common
{
    public class Constants
    {
        public const String ShellWorkspaceName = "ShellTabWorkspace";
        public const String MsgBoxCaptionWarning = "WARNING";
        public const String MsgBoxCaptionAuthenticationError = "AUTHENTICATION ERROR";
        public const String MsgBoxCaptionSystemError = "SYSTEM ERROR";
        public const String MsgBoxCaptionConfirm = "CONFIRM";
        public const String MsgContactAdministrator = "Please contact a system administrator. ({0})";
        public const String EmailAddressSparkAdminTool = "Spark.Admin@spark.com";
        public const String SmtpClientAddress = "relay.matchnet.com";

        // Add email recipients via the Exchange group, wsoalert.
        public const String EmailsToAlert = "wsoalert@spark.net";
        

        public const String StateMemberID = "MemberID";
        public const String StateMemberPrivileges = "MemberPrivileges";
        public const String StateSites = "Sites";
        public const String StateMemberEmail = "MemberEmail";
    }
}
