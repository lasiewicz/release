using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using Matchnet.Member.ValueObjects.Privilege;

namespace Spark.Admin.Common
{
    public partial class BaseModuleView : Controls.UserControl
    {
        private String title;

        #region Contructors
        public BaseModuleView()
        {
            InitializeComponent();

            //this.Dock = DockStyle.Fill;
        }
        #endregion

        #region Public Methods
        public Boolean HasPrivilege(Int32 privilegeID)
        {
            return ((MemberPrivilegeCollection)GlobalState[Constants.StateMemberPrivileges]).HasPrivilege(privilegeID);
        }
        #endregion

        #region Properties
        public String Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }
        #endregion
    }
}
