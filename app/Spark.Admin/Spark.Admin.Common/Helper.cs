using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Net.Mail;

namespace Spark.Admin.Common
{
    public class Helper
    {
        public enum EmailType
        { 
            Unknown,
            DeploymentToProd
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recipients">If there is more than 1 recipient, then the email address must be
        /// semicolon delimited with no spaces.</param>
        /// <param name="emailType"></param>
        /// <param name="subject"></param>
        /// <param name="authEmail"></param>
        /// <param name="body"></param>
        public static void SendEmail(String recipients
            , EmailType emailType, String subject, String authEmail, String body)
        {
            try
            {
                StringBuilder bodyBuilder = new StringBuilder();
                switch (emailType)
                { 
                    case EmailType.DeploymentToProd:
                        subject = "DEPLOYED TO PRODUCTION: " + subject;

                        bodyBuilder.Append("A record was deployed to the production mnContent database farm.  Here are the details:").Append(Environment.NewLine)
                            .Append(Environment.NewLine)
                            .Append("Deployed By: ").Append(authEmail).Append(Environment.NewLine)
                            .Append(body);

                        break;
                    
                    case EmailType.Unknown:
                    default:
                        bodyBuilder.Append(body);

                        break;
                }

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(Constants.EmailAddressSparkAdminTool);
                // If there is more than one recipient, parse the addresses.
                String[] recipientsArray = recipients.Split(';');
                for (Int32 i = 0; i < recipientsArray.Length; i++)
                {
                    if (recipientsArray[i].IndexOf('@') != -1)
                        mailMessage.To.Add(recipientsArray[i]);
                }
                mailMessage.Subject = subject;
                mailMessage.Body = bodyBuilder.ToString();

                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Host = Constants.SmtpClientAddress;
                smtpClient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                throw (new Exception("Failure sending email.", ex));
            }
        }
    }
}
