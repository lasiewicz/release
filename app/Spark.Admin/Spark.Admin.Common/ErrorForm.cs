﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Spark.Admin.Common
{
    public partial class ErrorForm : Spark.Admin.Common.Controls.Form
    {
        public ErrorForm(String errorDetail)
        {
            InitializeComponent();

            ErrorDetail.Text = errorDetail;
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;

            this.Close();
        }
    }
}