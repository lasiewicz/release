using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI;
using Spark.Admin.Common;

using Matchnet.Member.ValueObjects.Privilege;

namespace Spark.Admin.Common.Controls
{
    /// <summary>
    /// Every form holds an instance of the CommonController.
    /// </summary>
    public partial class Form : System.Windows.Forms.Form
    {
        private State globalState;
        private CommonController commonController;

        #region Constructors
        public Form()
        {
            InitializeComponent();

            commonController = new CommonController();
        }

        public Form(State globalState) : this()
        {
            this.globalState = globalState;
        }
        #endregion

        #region Public Methods
        public Boolean HasPrivilege(Int32 privilegeID)
        {
            return ((MemberPrivilegeCollection)GlobalState[Constants.StateMemberPrivileges]).HasPrivilege(privilegeID);
        }
        #endregion

        #region Properties
        public State GlobalState
        {
            get
            {
                return globalState;
            }
            set
            {
                globalState = value;
            }
        }

        public CommonController CommonController
        {
            get
            {
                return commonController;
            }
        }

        public Int32 MemberID
        {
            get 
            {
                return Convert.ToInt32(globalState[Constants.StateMemberID]);
            }
        }

        public String MemberEmail
        {
            get
            {
                return Convert.ToString(globalState[Constants.StateMemberEmail]);
            }
        }
        #endregion
    }
}