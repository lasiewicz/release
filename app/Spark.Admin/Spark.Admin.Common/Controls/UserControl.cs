using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI;

namespace Spark.Admin.Common.Controls
{
    public partial class UserControl : System.Windows.Forms.UserControl
    {
        private State globalState;

        #region Contructors
        public UserControl()
        {
            InitializeComponent();
        }
        #endregion

        #region Properties
        public State GlobalState
        {
            get
            {
                return globalState;
            }
            set
            {
                globalState = value;
            }
        }

        public Int32 MemberID
        {
            get
            {
                return Convert.ToInt32(globalState[Constants.StateMemberID]);
            }
        }

        public String MemberEmail
        {
            get
            {
                return Convert.ToString(globalState[Constants.StateMemberEmail]);
            }
        }
        #endregion
    }
}
