using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI;

using Matchnet.Member.ValueObjects.Privilege;

namespace Spark.Admin.Common
{
    public class BaseModuleWorkItem : WorkItem
    {
        private State globalState;

        #region Public Methods
        public Boolean HasPrivilege(Int32 privilegeID)
        {
            return ((MemberPrivilegeCollection)GlobalState[Constants.StateMemberPrivileges]).HasPrivilege(privilegeID);
        }
        #endregion

        #region Properties
        public State GlobalState
        {
            get
            {
                return globalState;
            }
            set 
            {
                globalState = value;
            }
        }
        #endregion
    }
}
