using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI;

using Spark.Admin.Common;
using Spark.Admin.WebServices;
using Matchnet.Content.ValueObjects.PagePixel;
using Matchnet.Content.ValueObjects.Admin;

namespace Spark.Admin.Common
{
    /// <summary>
    /// This controller is used for Common functionality, such as Publish methods that will be used in more than
    /// one module.
    /// </summary>
    public class CommonController : BaseController
    {
        private WebServiceProxy webServiceProxy;

        #region Constructors
        public CommonController()
        {
            // Initialize the WebServiceProxy if you are using web services.
            webServiceProxy = new WebServiceProxy();        
        }
        #endregion

        #region Public Methods
        public static void BuildConstant(ref TextBox constantTextBox)
        {
            constantTextBox.Text = constantTextBox.Text.ToUpper().Replace(" ", "_");

            // Place cursor at end of Text.
            constantTextBox.SelectionStart = constantTextBox.Text.Length;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="publishID"></param>
        /// <param name="nextPublishActionPublishObjectID"></param>
        /// <param name="nextEnvironmentType"></param>
        /// <returns>Returns the nextPublishActionPublishObjectID</returns>
        public Int32 GetNextPublishActionPublishObjectStep(Int32 publishObjectID, PublishObjectType publishObjectType, out ServiceEnvironmentTypeEnum serviceEnvrionmentTypeEnum)
        {
            return webServiceProxy.GetNextPublishActionPublishObjectStep(publishObjectID, publishObjectType, out serviceEnvrionmentTypeEnum);
        }

        public void SavePublishAction(Int32 publishID, Int32 publishActionPublishObjectID, Int32 memberID)
        {
            webServiceProxy.SavePublishAction(publishID, publishActionPublishObjectID, memberID);
        }

        public Int32 SavePublish(Int32 publishObjectID, PublishObjectType publishObjectType, Object content)
        {
            return webServiceProxy.SavePublish(publishObjectID, publishObjectType, content);
        }

        #endregion
    }
}
