using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Collections;

using Matchnet;
using Spark.Admin.WebServices;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.Article;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Content.ValueObjects.PagePixel;
using Matchnet.Content.ValueObjects.LandingPage;
using Matchnet.Member.ValueObjects.Privilege;
using Matchnet.Member.ValueObjects;
using Matchnet.ApproveQueue.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;

namespace Spark.Admin
{
    /// <summary>
    /// This wrapper class was created because
    /// all the business objects returned by Spark.Admin.WebServices.Service are binary serialized
    /// (see note in Spark.Admin.WebServices.Service).  This class will make calls to the web service
    /// and then deserialize the objects for use within the Spark.Admin client application.
    /// The reason that this deserialization occurs here instead of in the Service.cs itself is so
    /// that the Service.cs can be regenderated by wsdl.exe without being re-edited in the future.
    /// </summary>
    public class WebServiceProxy
    {
        private Service devService;
        private Service contentStgService;
        private Service prodService;

        #region Constructors
        public WebServiceProxy()
        {
            // HACK:  I keep getting an "Unauthorized" error when trying to call web devServices.
            // Apparently, the web service is requiring credentials and currently, there are
            // no credentials in System.Net.CredentialCache.DefaultCredentials.  
            // Others have experienced this, but their solutions to not appear to work.
            // The NTLM Authorization
            // checkbox in the web service project is unchecked, but this is still a problem for some
            // reason.
            // This needs to be resolved, but for now passing valid Credentials seems to work.
            #region Set up Services

            // For machine pointed to DEV MT for DEVELOPMENT.
            devService = new Service(System.Configuration.ConfigurationSettings.AppSettings["DEV_URL"]);

            // Points to LAWSOSTG01 server. 
            // Following override is needed for the server on the database.
            //    add an override for mncontent logicaldatabase name for this server
            //    insert into serversetting values(8, 13, 10224, null, null, 'mnContentStage', null)

            contentStgService = new Service(System.Configuration.ConfigurationSettings.AppSettings["STAGE_URL"]);

            // 64.16.64.123 is a VIP which is used to load balance to SPWEBADMIN01 and SPWEBADMIN02.
            prodService = new Service(System.Configuration.ConfigurationSettings.AppSettings["PROD_URL"]);

            #endregion
        }
        #endregion

        #region Public Methods
        // For the time being, we will always be using the data in the DEV database.
        public Sites GetSites()
        {
            return GetSites(ServiceEnvironmentTypeEnum.Dev);
        }
        public Sites GetSites(ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return ((Sites)MiscUtils.BinaryDeserializeObject(service.GetSites()));
        }

        public Brand GetBrandByBrandID(Int32 brandID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return (Brand)MiscUtils.BinaryDeserializeObject(service.GetBrandByBrandID(brandID));
        }

        public Brands GetBrands(ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return (Brands)MiscUtils.BinaryDeserializeObject(service.GetBrands());
        }

        public void AdminActionLogInsert(Int32 memberID, Int32 communityID, Int32 adminActionMask, Int32 adminMemberID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            service.AdminActionLogInsert(memberID, communityID, adminActionMask, adminMemberID);
        }

        public AttributeCollections GetAttributeCollections(ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);


            return ((AttributeCollections)MiscUtils.BinaryDeserializeObject(service.GetAttributeCollections()));
        }

        public Attributes GetAttributes(ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return ((Attributes)MiscUtils.BinaryDeserializeObject(service.GetAttributes()));
        }

        public AuthenticationResult Authenticate(String emailAddress, String password, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return ((AuthenticationResult)MiscUtils.BinaryDeserializeObject(service.Authenticate(emailAddress, password)));
        }

        // For the time being, we will always be using the data in the PROD database.
        public ArrayList GetMembersByEmail(String email)
        {
            return GetMembersByEmail(email, ServiceEnvironmentTypeEnum.Prod);
        }
        public ArrayList GetMembersByEmail(String email, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return ((ArrayList)MiscUtils.BinaryDeserializeObject(service.GetMembersByEmail(email)));
        }

        public ArrayList GetMembersByMemberID(Int32 memberID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return ((ArrayList)MiscUtils.BinaryDeserializeObject(service.GetMembersByMemberID(memberID)));
        }

        public ArrayList GetMembersByUsername(String username, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return ((ArrayList)MiscUtils.BinaryDeserializeObject(service.GetMembersByUsername(username)));
        }

        // For the time being, we will always be using the data in the PROD database.
        public MemberPrivilegeCollection GetMemberPrivileges(Int32 memberID)
        {
            return GetMemberPrivileges(memberID, ServiceEnvironmentTypeEnum.Prod);
        }
        public MemberPrivilegeCollection GetMemberPrivileges(Int32 memberID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return ((MemberPrivilegeCollection)MiscUtils.BinaryDeserializeObject(service.GetMemberPrivileges(memberID)));
        }

        // For the time being, we will always be using the data in the DEV database.
        public Int32 GetNextPublishActionPublishObjectStep(Int32 publishObjectID, PublishObjectType publishObjectType, out ServiceEnvironmentTypeEnum serviceEnvrionmentTypeEnum)
        {
            return GetNextPublishActionPublishObjectStep(publishObjectID, publishObjectType, out serviceEnvrionmentTypeEnum, ServiceEnvironmentTypeEnum.Dev);
        }
        public Int32 GetNextPublishActionPublishObjectStep(Int32 publishObjectID, PublishObjectType publishObjectType, out ServiceEnvironmentTypeEnum serviceEnvrionmentTypeEnum, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            Int32 serviceEnvironmentTypeEnumObj;

            Int32 nextPubActPubObjID = service.GetNextPublishActionPublishObjectStep(publishObjectID, Convert.ToInt32(publishObjectType), out serviceEnvironmentTypeEnumObj);

            // Need to convert the Int32 version to ServiceEnvironmentTypeEnum.
            serviceEnvrionmentTypeEnum = ((ServiceEnvironmentTypeEnum)serviceEnvironmentTypeEnumObj);

            return nextPubActPubObjID;
        }

        // For the time being, we will always be using the data in the DEV database.
        public String GetSetting(String constant)
        {
            return GetSetting(constant, ServiceEnvironmentTypeEnum.Dev);
        }
        public String GetSetting(String constant, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return service.GetSetting(constant);
        }

        public void SavePagePixel(PagePixel pagePixel, Int32 memberID, Boolean isPublish, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            service.SavePagePixel(MiscUtils.BinarySerializeObject(pagePixel), memberID, isPublish);
        }

        // For the time being, we will always be using the data in the DEV database.
        public void DeletePagePixel(Int32 pagePixelID, Int32 memberID)
        {
            DeletePagePixel(pagePixelID, memberID, ServiceEnvironmentTypeEnum.Dev);
        }
        public void DeletePagePixel(Int32 pagePixelID, Int32 memberID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            service.DeletePagePixel(pagePixelID, memberID);
        }

        public void ApprovePagePixel(Int32 pagePixelID, Int32 approverMemberID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            service.ApprovePagePixel(pagePixelID, approverMemberID);
        }

        // For the time being, we will always be using the data in the DEV database.
        public void SavePublishAction(Int32 publishID, Int32 publishActionPublishObjectID, Int32 memberID)
        {
            SavePublishAction(publishID, publishActionPublishObjectID, memberID, ServiceEnvironmentTypeEnum.Dev);
        }
        public void SavePublishAction(Int32 publishID, Int32 publishActionPublishObjectID, Int32 memberID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            service.SavePublishAction(publishID, publishActionPublishObjectID, memberID);
        }

        // For the time being, we will always be using the data in the DEV database.
        public Int32 SavePublish(Int32 publishObjectID, PublishObjectType publishObjectType, Object content)
        {
            return SavePublish(publishObjectID, publishObjectType, content, ServiceEnvironmentTypeEnum.Dev);
        }
        public Int32 SavePublish(Int32 publishObjectID, PublishObjectType publishObjectType, Object content, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return service.SavePublish(publishObjectID, Convert.ToInt32(publishObjectType), MiscUtils.BinarySerializeObject(content));
        }

        public SiteCategoryCollection RetrieveSiteCategories(Int32 siteID, Boolean forceLoadFlag, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return ((SiteCategoryCollection)MiscUtils.BinaryDeserializeObject(service.RetrieveSiteCategories(siteID, forceLoadFlag)));
        }

        public SiteArticleCollection RetrieveCategorySiteArticlesAndArticles(Int32 categoryID, Int32 siteID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return ((SiteArticleCollection)MiscUtils.BinaryDeserializeObject(service.RetrieveCategorySiteArticlesAndArticles(categoryID, siteID)));
        }

        public Int32 SaveSiteArticle(SiteArticle siteArticle, Int32 memberID, Boolean isPublish, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return service.SaveSiteArticle(MiscUtils.BinarySerializeObject(siteArticle), memberID, isPublish);
        }

        public Int32 SaveArticle(Article article, Int32 memberID, Boolean isPublish, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        { 
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return service.SaveArticle(MiscUtils.BinarySerializeObject(article), memberID, isPublish);
        }

        public Article RetrieveArticle(Int32 articleID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return ((Article)MiscUtils.BinaryDeserializeObject(service.RetrieveArticle(articleID)));
        }

        public Int32 SaveSiteCategory(SiteCategory siteCategory, Int32 memberID, Boolean isPublish, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return service.SaveSiteCategory(MiscUtils.BinarySerializeObject(siteCategory), memberID, isPublish);
        }

        public Int32 SaveCategory(Category category, Int32 memberID, Boolean isPublish, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return service.SaveCategory(MiscUtils.BinarySerializeObject(category), memberID, isPublish);
        }

        public Category RetrieveCategory(Int32 categoryID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return ((Category)MiscUtils.BinaryDeserializeObject(service.RetrieveCategory(categoryID)));
        }

        public ICollection GetLandingPages(Int32 startRow, Int32 pageSize, Spark.Admin.WebServices.SortType sortType, Spark.Admin.WebServices.Direction direction, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return ((ICollection)MiscUtils.BinaryDeserializeObject(service.GetLandingPages(startRow, pageSize, sortType, direction)));
        }

        public void SaveLandingPage(Int32 landingpageid, String name, String url, String controlName, String description, bool isactive, bool isstatic, Int32 memberid, bool ispublish, Int32 publishid, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            service.SaveLandingPage(landingpageid, name, url, controlName, description, isactive, isstatic, memberid, ispublish, publishid);
        }

        public Int32 AddLandingPage(String name, String url, String controlName, String description, bool isactive, bool isstatic, Int32 memberid, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return service.AddLandingPage(name, url, controlName, description, isactive, isstatic, memberid);
        }

        public LandingPage GetLandingPage(Int32 landingpageid, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return ((LandingPage)MiscUtils.BinaryDeserializeObject(service.GetLandingPage(landingpageid)));
        }

        public PrivilegeCollection GetPrivileges(ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return ((PrivilegeCollection)MiscUtils.BinaryDeserializeObject(service.GetPrivileges()));
        }

        public ArrayList GetMembersByPrivilege(Int32 privilegeID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return ((ArrayList)MiscUtils.BinaryDeserializeObject(service.GetMembersByPrivilege(privilegeID)));
        }

        public SecurityGroupCollection GetGroupsByPrivilege(Int32 privilegeID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return ((SecurityGroupCollection)MiscUtils.BinaryDeserializeObject(service.GetGroupsByPrivilege(privilegeID)));
        }

        public void SaveMemberPrivilege(Int32 memberID, Int32 privilegeID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            service.SaveMemberPrivilege(memberID, privilegeID);
        }

        public void DeleteMemberPrivilege(Int32 memberID, Int32 privilegeID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            service.DeleteMemberPrivilege(memberID, privilegeID);
        }

        public SecurityGroupMemberCollection GetSecurityGroupMembers(Int32 groupID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return ((SecurityGroupMemberCollection)MiscUtils.BinaryDeserializeObject(service.GetSecurityGroupMembers(groupID)));
        }

        public Matchnet.Member.ValueObjects.CachedMember GetCachedMember(Int32 memberID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return (Matchnet.Member.ValueObjects.CachedMember)MiscUtils.BinaryDeserializeObject(service.GetCachedMember(memberID));
        }

        public Matchnet.Member.ValueObjects.MemberSaveResult SaveMemberTextAttributes(Int32 memberID, Int32 brandID, Hashtable textAttributes, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return (Matchnet.Member.ValueObjects.MemberSaveResult)MiscUtils.BinaryDeserializeObject(service.SaveMemberTextAttributes(memberID, brandID, MiscUtils.BinarySerializeObject(textAttributes)));
        }

        public Matchnet.Member.ValueObjects.MemberSaveResult SaveMemberIntAttributes(Int32 memberID, Int32 brandID, Hashtable intAttributes, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return (Matchnet.Member.ValueObjects.MemberSaveResult)MiscUtils.BinaryDeserializeObject(service.SaveMemberIntAttributes(memberID, brandID, MiscUtils.BinarySerializeObject(intAttributes)));
        }

        public SecurityGroupCollection GetSecurityGroups(ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            return ((SecurityGroupCollection)MiscUtils.BinaryDeserializeObject(service.GetSecurityGroups()));
        }

        public void SaveSecurityGroupMember(Int32 memberID, Int32 groupID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            service.SaveSecurityGroupMember(memberID, groupID);
        }

        public void DeleteSecurityGroupMember(Int32 memberID, Int32 groupID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            service.DeleteSecurityGroupMember(memberID, groupID);
        }

        public void CompleteApproval(String queueItemKey, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            Service service = ChooseService(currentServiceEnvironmentTypeEnum);

            service.CompleteApproval(queueItemKey);
        }

       public WSOListCollection RetrieveWSOListCollection()
        {
            Service service = ChooseService(ServiceEnvironmentTypeEnum.Dev);

            return ((WSOListCollection)MiscUtils.BinaryDeserializeObject(service.RetrieveWSOListCollection()));
        }

        public void SaveWSOList(Matchnet.Content.ValueObjects.Article.WSOList wsoList)
        {
            Service service = ChooseService(ServiceEnvironmentTypeEnum.Dev);

            service.SaveWSOList(MiscUtils.BinarySerializeObject(wsoList));
        }

        public void CreateWSOList(string name, string note)
        {
            Service service = ChooseService(ServiceEnvironmentTypeEnum.Dev);

            service.CreateWSOList(name, note);
        }

        public WSOItemCollection RetrieveWSOItemCollection(int wsoListID)
        {
            Service service = ChooseService(ServiceEnvironmentTypeEnum.Dev);

            return ((WSOItemCollection)MiscUtils.BinaryDeserializeObject(service.RetrieveWSOItemCollection(wsoListID)));
        }

        public void SaveWSOItem(Matchnet.Content.ValueObjects.Article.WSOItem wsoItem)
        {
            Service service = ChooseService(ServiceEnvironmentTypeEnum.Dev);

            service.SaveWSOItem(MiscUtils.BinarySerializeObject(wsoItem));
        }

        public void DeleteWSOItem(Matchnet.Content.ValueObjects.Article.WSOItem wsoItem)
        {
            Service service = ChooseService(ServiceEnvironmentTypeEnum.Dev);

            service.DeleteWSOItem(MiscUtils.BinarySerializeObject(wsoItem));
        }

        public void CreateWSOItem(int wsoListID, int siteID, int itemID, int itemTypeID)
        {
            Service service = ChooseService(ServiceEnvironmentTypeEnum.Dev);

            service.CreateWSOItem(wsoListID, siteID, itemID, itemTypeID);
        }

        #endregion

        #region Private Methods
        private Service ChooseService(ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            switch (currentServiceEnvironmentTypeEnum)
            {
                case ServiceEnvironmentTypeEnum.Prod:
                    return prodService;

                case ServiceEnvironmentTypeEnum.ContentStg:
                    return contentStgService;

                case ServiceEnvironmentTypeEnum.Dev:
                default:
                    return devService;
            }
        }
        #endregion
    }
}
