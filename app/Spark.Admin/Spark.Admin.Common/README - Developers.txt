Notes regarding GenerateWsdl.bat:
	If ever you should need to rerun wsdl.exe via GenerateWsdl.bat, please note that the following
	constructor be added to the resulting Service.cs file in order for it
	to utilize the correct web service depending on the build type.

	    /// <summary>
        /// Manually added to work with WebServiceProxy.
        /// </summary>
        /// <param name="serviceUrl"></param>
        public Service(String serviceUrl)
        {
            this.Url = serviceUrl;
        }