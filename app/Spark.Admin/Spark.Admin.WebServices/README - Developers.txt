Setting up the Web Service:
- Set up the Web Service to use Asp.Net 2.0 in IIS.
- You will probably need to create a new Application Pool for Asp.Net 2.0 applications to run under
	in IIS.  I've been naming the new Application Pool DotNet2Dot0AppPool.