using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Services.Description;
using System.Collections;
using System.Collections.Specialized;

using Matchnet;

using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ValueObjects;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.Article;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Content.ValueObjects.PagePixel;
using Matchnet.Content.ValueObjects.LandingPage;
using Matchnet.ApproveQueue.ValueObjects;

using Matchnet.Content.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.ApproveQueue.ServiceAdapters;


/// <summary>
/// NOTE:  Asmx, by default, uses XmlSerialization which poses problems for our complex business objects
/// (i.e. Sites object).  XmlSerialization, by design, has limitations which cause our BOs to, at times,
/// not serialize some of the necessary data within the BO or even not serialize at all.  Two possible
/// ways around this are 1) utilize the IXmlSerializable interface and Schema Importer Extensions
/// to customize the serialization, or 2) BinarySerialize all our BOs before they are sent by the web
/// service and then BinaryDeSerialize them after they are received by the client application.  I chose
/// to do 2) because it is much less work (currently, working with time constraints) and seems like a 
/// much simpler implementation.
/// </summary>
[WebService(Namespace = "Spark.Admin.WebServices")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class Service : System.Web.Services.WebService
{
    #region Constructors
    public Service()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    #endregion

    #region Public Methods
    [WebMethod]
    public Byte[] GetSites()
    {
        return MiscUtils.BinarySerializeObject(BrandConfigSA.Instance.GetSites());
    }

    [WebMethod]
    public Byte[] GetBrandByBrandID(Int32 brandID)
    {
        return MiscUtils.BinarySerializeObject(BrandConfigSA.Instance.GetBrandByID(brandID));
    }

    [WebMethod]
    public Byte[] GetBrands()
    {
        return MiscUtils.BinarySerializeObject(BrandConfigSA.Instance.GetBrands());
    }

    [WebMethod]
    public void AdminActionLogInsert(Int32 memberID, Int32 communityID, Int32 adminActionMask, Int32 adminMemberID)
    {
        AdminSA.Instance.AdminActionLogInsert(memberID, communityID, adminActionMask, adminMemberID);
    }

    [WebMethod]
    public Byte[] GetAttributeCollections()
    {
        return MiscUtils.BinarySerializeObject(AttributeMetadataSA.Instance.GetAttributeCollections());
    }

    [WebMethod]
    public Byte[] GetAttributes()
    {
        return MiscUtils.BinarySerializeObject(AttributeMetadataSA.Instance.GetAttributes());
    }

    [WebMethod]
    public Byte[] Authenticate(String emailAddress, String password)
    {
        //MemberSA.Instance.Authenticate(emailAddress, password) is deprecated. Use the one with brand info.
        //A user should have admin account on jdate to login to Article tool. So passing jdate brand info to Authenticate method.
        Brand brand = BrandConfigSA.Instance.GetBrandByID(1003);

        AuthenticationResult authenticationResult = MemberSA.Instance.Authenticate(brand, emailAddress, password);

        return MiscUtils.BinarySerializeObject(authenticationResult);
    }

    [WebMethod]
    public Byte[] GetMembersByEmail(String email)
    {
        Int32 totalRows = 0;

        return MiscUtils.BinarySerializeObject(MemberSA.Instance.GetMembersByEmail(email, 1, 1, ref totalRows));
    }

    [WebMethod]
    public void DeleteMemberPrivilege(Int32 memberID, Int32 privilegeID)
    {
        MemberPrivilegeSA.Instance.DeleteMemberPrivilege(memberID, privilegeID);
    }

    [WebMethod]
    public Byte[] GetMembersByUsername(String username)
    {
        Int32 totalRows = 0;

        return MiscUtils.BinarySerializeObject(MemberSA.Instance.GetMembersByUserName(username, 1, 1, ref totalRows));
    }

    [WebMethod]
    public Byte[] GetMembersByMemberID(Int32 memberID)
    {
        Int32 totalRows = 0;

        return MiscUtils.BinarySerializeObject(MemberSA.Instance.GetMembersByMemberID(memberID, ref totalRows));
    }

    [WebMethod]
    public String GetSetting(String constant)
    {
        return RuntimeSettings.GetSetting(constant);
    }

    [WebMethod]
    public Byte[] RetrieveSiteCategories(Int32 siteID, Boolean forceLoadFlag)
    {
        return MiscUtils.BinarySerializeObject(ArticleSA.Instance.RetrieveSiteCategories(siteID, forceLoadFlag));
    }

    [WebMethod]
    public Int32 SaveSiteCategory(Byte[] siteCategory, Int32 memberID, Boolean isPublish)
    {
        return ArticleSA.Instance.SaveSiteCategory(((SiteCategory)MiscUtils.BinaryDeserializeObject(siteCategory)), memberID, isPublish);
    }

    [WebMethod]
    public Int32 SaveSiteArticle(Byte[] siteArticle, Int32 memberID, Boolean isPublish)
    {
        
        return ArticleSA.Instance.SaveSiteArticle(((SiteArticle)MiscUtils.BinaryDeserializeObject(siteArticle)), memberID, isPublish);
    }

    [WebMethod]
    public Byte[] RetrieveCategorySiteArticlesAndArticles(Int32 categoryID, Int32 siteID)
    {
        return MiscUtils.BinarySerializeObject(ArticleSA.Instance.RetrieveCategorySiteArticlesAndArticles(categoryID, siteID));
    }

    [WebMethod]
    public Byte[] RetrieveCategory(Int32 categoryID)
    {
        return MiscUtils.BinarySerializeObject(ArticleSA.Instance.RetrieveCategory(categoryID));
    }

    [WebMethod]
    public Int32 SaveCategory(Byte[] category, Int32 memberID, Boolean isPublish)
    {
        return ArticleSA.Instance.SaveCategory(((Category)MiscUtils.BinaryDeserializeObject(category)), memberID, isPublish);
    }

    [WebMethod]
    public Byte[] RetrieveArticle(Int32 articleID)
    {
        return MiscUtils.BinarySerializeObject(ArticleSA.Instance.RetrieveArticle(articleID));
    }

    [WebMethod]
    public Int32 SaveArticle(Byte[] article, Int32 memberID, Boolean isPublish)
    {
        return ArticleSA.Instance.SaveArticle(((Article)MiscUtils.BinaryDeserializeObject(article)), memberID, isPublish);
    }

    [WebMethod]
    public void GetNextPublishActionPublishObjectStep(Int32 publishObjectID, Int32 publishObjectType, out Int32 publishActionPublishObjectID, out Int32 serviceEnvrionmentTypeEnum)
    {
        ServiceEnvironmentTypeEnum serviceEnvironmentTypeEnumObj;

        AdminSA.Instance.GetNextPublishActionPublishObjectStep(publishObjectID, ((PublishObjectType)publishObjectType), out publishActionPublishObjectID, out serviceEnvironmentTypeEnumObj);

        serviceEnvrionmentTypeEnum = Convert.ToInt32(serviceEnvironmentTypeEnumObj);
    }

    [WebMethod]
    public Byte[] GetPrivileges()
    {
        return MiscUtils.BinarySerializeObject(MemberPrivilegeSA.Instance.GetPrivileges());
    }

    [WebMethod]
    public Byte[] GetMembersByPrivilege(Int32 privilegeID)
    {
        return MiscUtils.BinarySerializeObject(MemberPrivilegeSA.Instance.GetMembersByPrivilege(privilegeID, true));
    }

    [WebMethod]
    public Byte[] GetGroupsByPrivilege(Int32 privilegeID)
    {
        return MiscUtils.BinarySerializeObject(MemberPrivilegeSA.Instance.GetGroupsByPrivilege(privilegeID));
    }

    [WebMethod]
    public void SaveMemberPrivilege(Int32 memberID, Int32 privilegeID)
    {
        MemberPrivilegeSA.Instance.SaveMemberPrivilege(memberID, privilegeID);
    }

    [WebMethod]
    public Byte[] GetSecurityGroupMembers(Int32 groupID)
    {
        return MiscUtils.BinarySerializeObject(MemberPrivilegeSA.Instance.GetSecurityGroupMembers(groupID));
    }

    [WebMethod]
    public Byte[] GetCachedMember(Int32 memberID)
    {
        Int32 notUsed = new Int32();

        return MiscUtils.BinarySerializeObject(MemberSA.Instance.GetMembersByMemberID(memberID, ref notUsed)[0]);
    }

    [WebMethod]
    public Byte[] SaveMemberTextAttributes(Int32 memberID, Int32 brandID, Byte[] textAttributes)
    {
        Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);

        Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

        Hashtable textAttributesHash = (Hashtable) MiscUtils.BinaryDeserializeObject(textAttributes);

        foreach (string attributeName in textAttributesHash.Keys)
        {
            TextValue textValue = (TextValue)textAttributesHash[attributeName];

            member.SetAttributeText(brand, attributeName, textValue.Text, textValue.TextStatus);
        }

        return MiscUtils.BinarySerializeObject(MemberSA.Instance.SaveMember(member));
    }

    [WebMethod]
    public Byte[] SaveMemberIntAttributes(Int32 memberID, Int32 brandID, Byte[] intAttributes)
    {
        Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);

        Matchnet.Member.ServiceAdapters.Member member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

        Hashtable intAttributesHash = (Hashtable)MiscUtils.BinaryDeserializeObject(intAttributes);

        foreach (string attributeName in intAttributesHash.Keys)
        {
            member.SetAttributeInt(brand, attributeName, System.Convert.ToInt32(intAttributesHash[attributeName]));
        }

        return MiscUtils.BinarySerializeObject(MemberSA.Instance.SaveMember(member));
    }

    [WebMethod]
    public Byte[] GetSecurityGroups()
    {
        return MiscUtils.BinarySerializeObject(MemberPrivilegeSA.Instance.GetSecurityGroups());
    }

    [WebMethod]
    public void SaveSecurityGroupMember(Int32 memberID, Int32 groupID)
    {
        MemberPrivilegeSA.Instance.SaveSecurityGroupMember(memberID, groupID);
    }

    [WebMethod]
    public void DeleteSecurityGroupMember(Int32 memberID, Int32 groupID)
    {
        MemberPrivilegeSA.Instance.DeleteSecurityGroupMember(memberID, groupID);
    }

    [WebMethod]
    public Byte[] GetMemberPrivileges(Int32 memberID)
    {
        return MiscUtils.BinarySerializeObject(MemberPrivilegeSA.Instance.GetMemberPrivileges(memberID));
    }

    [WebMethod]
    public void SavePublishAction(Int32 publishID, Int32 publishActionPublishObjectID, Int32 memberID)
    {
        AdminSA.Instance.SavePublishAction(publishID, publishActionPublishObjectID, memberID);
    }

    [WebMethod]
    public Int32 SavePublish(Int32 publishObjectID, Int32 publishObjectType, Byte[] content)
    {
        return AdminSA.Instance.SavePublish(publishObjectID, ((PublishObjectType)publishObjectType), content);
    }

    #region Article WSO Methods

    [WebMethod]
    public Byte[] RetrieveWSOListCollection()
    {
        return MiscUtils.BinarySerializeObject(ArticleSA.Instance.RetrieveWSOListCollection());
    }

    [WebMethod]
    public void SaveWSOList(Byte[] wsoList)
    {
        ArticleSA.Instance.SaveWSOList((WSOList)MiscUtils.BinaryDeserializeObject(wsoList));
    }

    [WebMethod]
    public void CreateWSOList(string name, string note)
    {
        ArticleSA.Instance.CreateWSOList(name, note);
    }

    [WebMethod]
    public Byte[] RetrieveWSOItemCollection(int wsoListID)
    {
        return MiscUtils.BinarySerializeObject(ArticleSA.Instance.RetrieveWSOItemCollection(wsoListID));
    }

    [WebMethod]
    public void SaveWSOItem(Byte[] wsoItem)
    {
        ArticleSA.Instance.SaveWSOItem((WSOItem)MiscUtils.BinaryDeserializeObject(wsoItem));
    }

    [WebMethod]
    public void DeleteWSOItem(Byte[] wsoItem)
    {
        ArticleSA.Instance.DeleteWSOItem((WSOItem)MiscUtils.BinaryDeserializeObject(wsoItem));
    }

    [WebMethod]
    public void CreateWSOItem(int wsoListID, int siteID, int itemID, int itemTypeID)
    {
        ArticleSA.Instance.CreateWSOItem(wsoListID, siteID, itemID, itemTypeID);
    }

    #endregion

    #endregion
}
