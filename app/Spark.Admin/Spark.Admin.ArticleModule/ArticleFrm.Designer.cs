namespace Spark.Admin.ArticleModule
{
    partial class ArticleFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ContentRichTextBox = new System.Windows.Forms.RichTextBox();
            this.CommandsPanel = new System.Windows.Forms.Panel();
            this.SCancelButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.ContentLabel = new System.Windows.Forms.Label();
            this.CategoryIDTextBox = new System.Windows.Forms.TextBox();
            this.CategoryIDLabel = new System.Windows.Forms.Label();
            this.ArticleIDTextBox = new System.Windows.Forms.TextBox();
            this.ArticleIDLabel = new System.Windows.Forms.Label();
            this.LastUpdatedDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.LastUpdatedLabel = new System.Windows.Forms.Label();
            this.ConstantLabel = new System.Windows.Forms.Label();
            this.ConstantTextBox = new System.Windows.Forms.TextBox();
            this.NextStepButton = new System.Windows.Forms.Button();
            this.ServiceEnvironmentLabel = new System.Windows.Forms.Label();
            this.CommandsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContentRichTextBox
            // 
            this.ContentRichTextBox.Location = new System.Drawing.Point(83, 85);
            this.ContentRichTextBox.Name = "ContentRichTextBox";
            this.ContentRichTextBox.Size = new System.Drawing.Size(530, 225);
            this.ContentRichTextBox.TabIndex = 64;
            this.ContentRichTextBox.Text = "";
            // 
            // CommandsPanel
            // 
            this.CommandsPanel.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.CommandsPanel.Controls.Add(this.NextStepButton);
            this.CommandsPanel.Controls.Add(this.SCancelButton);
            this.CommandsPanel.Controls.Add(this.SaveButton);
            this.CommandsPanel.Location = new System.Drawing.Point(9, 342);
            this.CommandsPanel.Name = "CommandsPanel";
            this.CommandsPanel.Size = new System.Drawing.Size(605, 53);
            this.CommandsPanel.TabIndex = 57;
            // 
            // SCancelButton
            // 
            this.SCancelButton.Location = new System.Drawing.Point(504, 16);
            this.SCancelButton.Name = "SCancelButton";
            this.SCancelButton.Size = new System.Drawing.Size(92, 23);
            this.SCancelButton.TabIndex = 35;
            this.SCancelButton.Text = "Cancel";
            this.SCancelButton.UseVisualStyleBackColor = true;
            this.SCancelButton.Click += new System.EventHandler(this.SCancelButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(406, 16);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(92, 23);
            this.SaveButton.TabIndex = 33;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // ContentLabel
            // 
            this.ContentLabel.AutoSize = true;
            this.ContentLabel.Location = new System.Drawing.Point(-4, 88);
            this.ContentLabel.Name = "ContentLabel";
            this.ContentLabel.Size = new System.Drawing.Size(81, 13);
            this.ContentLabel.TabIndex = 54;
            this.ContentLabel.Text = "Default Content";
            this.ContentLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // CategoryIDTextBox
            // 
            this.CategoryIDTextBox.Location = new System.Drawing.Point(83, 34);
            this.CategoryIDTextBox.Name = "CategoryIDTextBox";
            this.CategoryIDTextBox.ReadOnly = true;
            this.CategoryIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.CategoryIDTextBox.TabIndex = 46;
            // 
            // CategoryIDLabel
            // 
            this.CategoryIDLabel.AutoSize = true;
            this.CategoryIDLabel.Location = new System.Drawing.Point(17, 37);
            this.CategoryIDLabel.Name = "CategoryIDLabel";
            this.CategoryIDLabel.Size = new System.Drawing.Size(60, 13);
            this.CategoryIDLabel.TabIndex = 45;
            this.CategoryIDLabel.Text = "CategoryID";
            this.CategoryIDLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ArticleIDTextBox
            // 
            this.ArticleIDTextBox.Location = new System.Drawing.Point(83, 9);
            this.ArticleIDTextBox.Name = "ArticleIDTextBox";
            this.ArticleIDTextBox.ReadOnly = true;
            this.ArticleIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.ArticleIDTextBox.TabIndex = 40;
            // 
            // ArticleIDLabel
            // 
            this.ArticleIDLabel.AutoSize = true;
            this.ArticleIDLabel.Location = new System.Drawing.Point(30, 12);
            this.ArticleIDLabel.Name = "ArticleIDLabel";
            this.ArticleIDLabel.Size = new System.Drawing.Size(47, 13);
            this.ArticleIDLabel.TabIndex = 39;
            this.ArticleIDLabel.Text = "ArticleID";
            this.ArticleIDLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // LastUpdatedDateTimePicker
            // 
            this.LastUpdatedDateTimePicker.Enabled = false;
            this.LastUpdatedDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.LastUpdatedDateTimePicker.Location = new System.Drawing.Point(83, 316);
            this.LastUpdatedDateTimePicker.Name = "LastUpdatedDateTimePicker";
            this.LastUpdatedDateTimePicker.Size = new System.Drawing.Size(180, 20);
            this.LastUpdatedDateTimePicker.TabIndex = 76;
            this.LastUpdatedDateTimePicker.Value = new System.DateTime(1999, 1, 1, 0, 0, 0, 0);
            // 
            // LastUpdatedLabel
            // 
            this.LastUpdatedLabel.AutoSize = true;
            this.LastUpdatedLabel.Location = new System.Drawing.Point(6, 320);
            this.LastUpdatedLabel.Name = "LastUpdatedLabel";
            this.LastUpdatedLabel.Size = new System.Drawing.Size(71, 13);
            this.LastUpdatedLabel.TabIndex = 75;
            this.LastUpdatedLabel.Text = "Last Updated";
            // 
            // ConstantLabel
            // 
            this.ConstantLabel.AutoSize = true;
            this.ConstantLabel.Location = new System.Drawing.Point(28, 62);
            this.ConstantLabel.Name = "ConstantLabel";
            this.ConstantLabel.Size = new System.Drawing.Size(49, 13);
            this.ConstantLabel.TabIndex = 52;
            this.ConstantLabel.Text = "Constant";
            this.ConstantLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ConstantTextBox
            // 
            this.ConstantTextBox.Location = new System.Drawing.Point(83, 59);
            this.ConstantTextBox.Name = "ConstantTextBox";
            this.ConstantTextBox.Size = new System.Drawing.Size(530, 20);
            this.ConstantTextBox.TabIndex = 53;
            this.ConstantTextBox.TextChanged += new System.EventHandler(this.ConstantTextBox_TextChanged);
            // 
            // NextStepButton
            // 
            this.NextStepButton.Location = new System.Drawing.Point(11, 9);
            this.NextStepButton.Name = "NextStepButton";
            this.NextStepButton.Size = new System.Drawing.Size(92, 36);
            this.NextStepButton.TabIndex = 36;
            this.NextStepButton.Text = "NextStep";
            this.NextStepButton.UseVisualStyleBackColor = true;
            this.NextStepButton.Click += new System.EventHandler(this.NextStepButton_Click);
            // 
            // ServiceEnvironmentLabel
            // 
            this.ServiceEnvironmentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ServiceEnvironmentLabel.Location = new System.Drawing.Point(249, 9);
            this.ServiceEnvironmentLabel.Name = "ServiceEnvironmentLabel";
            this.ServiceEnvironmentLabel.Size = new System.Drawing.Size(258, 21);
            this.ServiceEnvironmentLabel.TabIndex = 78;
            this.ServiceEnvironmentLabel.Text = "DEV";
            this.ServiceEnvironmentLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ArticleFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 404);
            this.Controls.Add(this.ServiceEnvironmentLabel);
            this.Controls.Add(this.LastUpdatedDateTimePicker);
            this.Controls.Add(this.LastUpdatedLabel);
            this.Controls.Add(this.ContentRichTextBox);
            this.Controls.Add(this.CommandsPanel);
            this.Controls.Add(this.ContentLabel);
            this.Controls.Add(this.ConstantTextBox);
            this.Controls.Add(this.ConstantLabel);
            this.Controls.Add(this.CategoryIDTextBox);
            this.Controls.Add(this.CategoryIDLabel);
            this.Controls.Add(this.ArticleIDTextBox);
            this.Controls.Add(this.ArticleIDLabel);
            this.Name = "ArticleFrm";
            this.Text = "Article";
            this.Load += new System.EventHandler(this.ArticleFrm_Load);
            this.CommandsPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox ContentRichTextBox;
        private System.Windows.Forms.Panel CommandsPanel;
        private System.Windows.Forms.Button SCancelButton;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Label ContentLabel;
        private System.Windows.Forms.TextBox CategoryIDTextBox;
        private System.Windows.Forms.Label CategoryIDLabel;
        private System.Windows.Forms.TextBox ArticleIDTextBox;
        private System.Windows.Forms.Label ArticleIDLabel;
        private System.Windows.Forms.DateTimePicker LastUpdatedDateTimePicker;
        private System.Windows.Forms.Label LastUpdatedLabel;
        private System.Windows.Forms.Label ConstantLabel;
        private System.Windows.Forms.TextBox ConstantTextBox;
        private System.Windows.Forms.Button NextStepButton;
        private System.Windows.Forms.Label ServiceEnvironmentLabel;
    }
}