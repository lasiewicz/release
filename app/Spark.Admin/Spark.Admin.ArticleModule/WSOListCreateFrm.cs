﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI;

namespace Spark.Admin.ArticleModule
{
    public partial class WSOListCreateFrm : Spark.Admin.Common.Controls.Form
    {
        private ArticlesController articlesController;

        public WSOListCreateFrm(State globalState, ArticlesController articlesController) : base(globalState)
        {
            InitializeComponent();

            this.articlesController = articlesController;
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            if (TextBoxName.Text.Trim() == String.Empty)
            {
                MessageBox.Show("Name cannot be empty.");
                return;
            }

            articlesController.CreateWSOList(TextBoxName.Text.Trim(), TextBoxNote.Text.Trim());

            MessageBox.Show("WSO List, " + TextBoxName.Text.Trim() + ", has been created.", "Created WSO List", MessageBoxButtons.OK);

            this.Close();
            this.Dispose();
        }
    }
}
