﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI;

using Matchnet.Content.ValueObjects.Article;

namespace Spark.Admin.ArticleModule
{
    public partial class WSOListEditFrm : Spark.Admin.Common.Controls.Form
    {
        private ArticlesController articlesController;
        private State globalState;
        private WSOList wsoList;
        private WSOListManagerFrm wsoListManagerFrm;

        public WSOListEditFrm(State globalState, ArticlesController articlesController, WSOList wsoList, WSOListManagerFrm wsoListManagerFrm)
            : base(globalState)
        {
            InitializeComponent();

            this.articlesController = articlesController;

            this.wsoList = wsoList;
            this.wsoListManagerFrm = wsoListManagerFrm;

            TextBoxName.Text = wsoList.Name;
            TextBoxNote.Text = wsoList.Note;
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            if (TextBoxName.Text.Trim() == String.Empty)
            {
                MessageBox.Show("Name cannot be empty.");
                return;
            }

            articlesController.SaveWSOList(new WSOList(wsoList.WSOListID, TextBoxName.Text, TextBoxNote.Text));

            MessageBox.Show("WSO List, " + TextBoxName.Text.Trim() + ", has been updated.", "Updated WSO List", MessageBoxButtons.OK);

            this.Close();

            wsoListManagerFrm.PopulateDataGridViewWSOList();

            this.Dispose();
        }
    }
}
