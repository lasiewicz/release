namespace Spark.Admin.ArticleModule
{
    partial class ArticlesView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.SitesComboBox = new System.Windows.Forms.ComboBox();
            this.SiteLabel = new System.Windows.Forms.Label();
            this.RefreshCategoriesButton = new System.Windows.Forms.Button();
            this.AddArticleButton = new System.Windows.Forms.Button();
            this.ServiceEnvironmentLabel = new System.Windows.Forms.Label();
            this.ServiceEnvironmentComboBox = new System.Windows.Forms.ComboBox();
            this.SiteCategoriesTreeView = new System.Windows.Forms.TreeView();
            this.ArticlesDataGridView = new System.Windows.Forms.DataGridView();
            this.EditSiteCategoryButton = new System.Windows.Forms.Button();
            this.AddCategoryButton = new System.Windows.Forms.Button();
            this.ControlPanel = new System.Windows.Forms.Panel();
            this.ButtonWSOHelp = new System.Windows.Forms.Button();
            this.ButtonWSOPublish = new System.Windows.Forms.Button();
            this.CategoryIDTextBox = new System.Windows.Forms.TextBox();
            this.RetrieveButton = new System.Windows.Forms.Button();
            this.ArticleIDTextBox = new System.Windows.Forms.TextBox();
            this.ArticleEntityTypeComboBox = new System.Windows.Forms.ComboBox();
            this.CategoryIDLabel = new System.Windows.Forms.Label();
            this.ArticleEntityTypeLabel = new System.Windows.Forms.Label();
            this.ArticleIDLabel = new System.Windows.Forms.Label();
            this.MainSplitter = new System.Windows.Forms.Splitter();
            ((System.ComponentModel.ISupportInitialize)(this.ArticlesDataGridView)).BeginInit();
            this.ControlPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // SitesComboBox
            // 
            this.SitesComboBox.FormattingEnabled = true;
            this.SitesComboBox.Location = new System.Drawing.Point(17, 13);
            this.SitesComboBox.Name = "SitesComboBox";
            this.SitesComboBox.Size = new System.Drawing.Size(238, 21);
            this.SitesComboBox.TabIndex = 0;
            this.SitesComboBox.SelectedIndexChanged += new System.EventHandler(this.SitesComboBox_SelectedIndexChanged);
            // 
            // SiteLabel
            // 
            this.SiteLabel.AutoSize = true;
            this.SiteLabel.Location = new System.Drawing.Point(14, 0);
            this.SiteLabel.Name = "SiteLabel";
            this.SiteLabel.Size = new System.Drawing.Size(31, 13);
            this.SiteLabel.TabIndex = 1;
            this.SiteLabel.Text = "Sites";
            this.SiteLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // RefreshCategoriesButton
            // 
            this.RefreshCategoriesButton.Location = new System.Drawing.Point(261, 11);
            this.RefreshCategoriesButton.Name = "RefreshCategoriesButton";
            this.RefreshCategoriesButton.Size = new System.Drawing.Size(112, 23);
            this.RefreshCategoriesButton.TabIndex = 4;
            this.RefreshCategoriesButton.Text = "Refresh Categories";
            this.RefreshCategoriesButton.UseVisualStyleBackColor = true;
            this.RefreshCategoriesButton.Click += new System.EventHandler(this.RefreshCategoriesButton_Click);
            // 
            // AddArticleButton
            // 
            this.AddArticleButton.Location = new System.Drawing.Point(580, 11);
            this.AddArticleButton.Name = "AddArticleButton";
            this.AddArticleButton.Size = new System.Drawing.Size(75, 23);
            this.AddArticleButton.TabIndex = 6;
            this.AddArticleButton.Text = "Add Article";
            this.AddArticleButton.UseVisualStyleBackColor = true;
            this.AddArticleButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // ServiceEnvironmentLabel
            // 
            this.ServiceEnvironmentLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ServiceEnvironmentLabel.AutoSize = true;
            this.ServiceEnvironmentLabel.Location = new System.Drawing.Point(670, 0);
            this.ServiceEnvironmentLabel.Name = "ServiceEnvironmentLabel";
            this.ServiceEnvironmentLabel.Size = new System.Drawing.Size(109, 13);
            this.ServiceEnvironmentLabel.TabIndex = 7;
            this.ServiceEnvironmentLabel.Text = "Service Environment";
            // 
            // ServiceEnvironmentComboBox
            // 
            this.ServiceEnvironmentComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ServiceEnvironmentComboBox.FormattingEnabled = true;
            this.ServiceEnvironmentComboBox.Location = new System.Drawing.Point(673, 13);
            this.ServiceEnvironmentComboBox.Name = "ServiceEnvironmentComboBox";
            this.ServiceEnvironmentComboBox.Size = new System.Drawing.Size(121, 21);
            this.ServiceEnvironmentComboBox.TabIndex = 8;
            this.ServiceEnvironmentComboBox.SelectedIndexChanged += new System.EventHandler(this.ServiceEnvironmentComboBox_SelectedIndexChanged);
            // 
            // SiteCategoriesTreeView
            // 
            this.SiteCategoriesTreeView.Dock = System.Windows.Forms.DockStyle.Left;
            this.SiteCategoriesTreeView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SiteCategoriesTreeView.Location = new System.Drawing.Point(0, 81);
            this.SiteCategoriesTreeView.Name = "SiteCategoriesTreeView";
            this.SiteCategoriesTreeView.Size = new System.Drawing.Size(255, 519);
            this.SiteCategoriesTreeView.TabIndex = 10;
            this.SiteCategoriesTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.CategoriesTreeView_AfterSelect);
            // 
            // ArticlesDataGridView
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ArticlesDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.ArticlesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ArticlesDataGridView.DefaultCellStyle = dataGridViewCellStyle8;
            this.ArticlesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ArticlesDataGridView.Location = new System.Drawing.Point(255, 81);
            this.ArticlesDataGridView.Name = "ArticlesDataGridView";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ArticlesDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.ArticlesDataGridView.RowTemplate.Height = 24;
            this.ArticlesDataGridView.Size = new System.Drawing.Size(545, 519);
            this.ArticlesDataGridView.TabIndex = 11;
            this.ArticlesDataGridView.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ArticlesDataGridView_RowHeaderMouseDoubleClick);
            // 
            // EditSiteCategoryButton
            // 
            this.EditSiteCategoryButton.Location = new System.Drawing.Point(379, 11);
            this.EditSiteCategoryButton.Name = "EditSiteCategoryButton";
            this.EditSiteCategoryButton.Size = new System.Drawing.Size(103, 23);
            this.EditSiteCategoryButton.TabIndex = 12;
            this.EditSiteCategoryButton.Text = "Edit SiteCategory";
            this.EditSiteCategoryButton.UseVisualStyleBackColor = true;
            this.EditSiteCategoryButton.Click += new System.EventHandler(this.EditSiteCategoryButton_Click);
            // 
            // AddCategoryButton
            // 
            this.AddCategoryButton.Location = new System.Drawing.Point(488, 11);
            this.AddCategoryButton.Name = "AddCategoryButton";
            this.AddCategoryButton.Size = new System.Drawing.Size(86, 23);
            this.AddCategoryButton.TabIndex = 13;
            this.AddCategoryButton.Text = "Add Category";
            this.AddCategoryButton.UseVisualStyleBackColor = true;
            this.AddCategoryButton.Click += new System.EventHandler(this.AddCategoryButton_Click);
            // 
            // ControlPanel
            // 
            this.ControlPanel.Controls.Add(this.ButtonWSOHelp);
            this.ControlPanel.Controls.Add(this.ButtonWSOPublish);
            this.ControlPanel.Controls.Add(this.CategoryIDTextBox);
            this.ControlPanel.Controls.Add(this.RetrieveButton);
            this.ControlPanel.Controls.Add(this.ServiceEnvironmentComboBox);
            this.ControlPanel.Controls.Add(this.ArticleIDTextBox);
            this.ControlPanel.Controls.Add(this.ArticleEntityTypeComboBox);
            this.ControlPanel.Controls.Add(this.AddCategoryButton);
            this.ControlPanel.Controls.Add(this.SitesComboBox);
            this.ControlPanel.Controls.Add(this.AddArticleButton);
            this.ControlPanel.Controls.Add(this.EditSiteCategoryButton);
            this.ControlPanel.Controls.Add(this.RefreshCategoriesButton);
            this.ControlPanel.Controls.Add(this.CategoryIDLabel);
            this.ControlPanel.Controls.Add(this.ServiceEnvironmentLabel);
            this.ControlPanel.Controls.Add(this.SiteLabel);
            this.ControlPanel.Controls.Add(this.ArticleEntityTypeLabel);
            this.ControlPanel.Controls.Add(this.ArticleIDLabel);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.ControlPanel.Location = new System.Drawing.Point(0, 0);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(800, 81);
            this.ControlPanel.TabIndex = 14;
            // 
            // ButtonWSOHelp
            // 
            this.ButtonWSOHelp.Location = new System.Drawing.Point(764, 54);
            this.ButtonWSOHelp.Name = "ButtonWSOHelp";
            this.ButtonWSOHelp.Size = new System.Drawing.Size(15, 23);
            this.ButtonWSOHelp.TabIndex = 23;
            this.ButtonWSOHelp.Text = "?";
            this.ButtonWSOHelp.UseVisualStyleBackColor = true;
            this.ButtonWSOHelp.Click += new System.EventHandler(this.ButtonWSOHelp_Click);
            // 
            // ButtonWSOPublish
            // 
            this.ButtonWSOPublish.Location = new System.Drawing.Point(662, 54);
            this.ButtonWSOPublish.Name = "ButtonWSOPublish";
            this.ButtonWSOPublish.Size = new System.Drawing.Size(102, 23);
            this.ButtonWSOPublish.TabIndex = 22;
            this.ButtonWSOPublish.Text = "WSO Publish";
            this.ButtonWSOPublish.UseVisualStyleBackColor = true;
            this.ButtonWSOPublish.Click += new System.EventHandler(this.ButtonWSOPublish_Click);
            // 
            // CategoryIDTextBox
            // 
            this.CategoryIDTextBox.Location = new System.Drawing.Point(379, 54);
            this.CategoryIDTextBox.Name = "CategoryIDTextBox";
            this.CategoryIDTextBox.Size = new System.Drawing.Size(103, 22);
            this.CategoryIDTextBox.TabIndex = 20;
            // 
            // RetrieveButton
            // 
            this.RetrieveButton.Location = new System.Drawing.Point(152, 52);
            this.RetrieveButton.Name = "RetrieveButton";
            this.RetrieveButton.Size = new System.Drawing.Size(103, 23);
            this.RetrieveButton.TabIndex = 14;
            this.RetrieveButton.Text = "Retrieve";
            this.RetrieveButton.UseVisualStyleBackColor = true;
            this.RetrieveButton.Click += new System.EventHandler(this.RetrieveButton_Click);
            // 
            // ArticleIDTextBox
            // 
            this.ArticleIDTextBox.Location = new System.Drawing.Point(488, 54);
            this.ArticleIDTextBox.Name = "ArticleIDTextBox";
            this.ArticleIDTextBox.Size = new System.Drawing.Size(167, 22);
            this.ArticleIDTextBox.TabIndex = 15;
            // 
            // ArticleEntityTypeComboBox
            // 
            this.ArticleEntityTypeComboBox.FormattingEnabled = true;
            this.ArticleEntityTypeComboBox.Location = new System.Drawing.Point(261, 54);
            this.ArticleEntityTypeComboBox.Name = "ArticleEntityTypeComboBox";
            this.ArticleEntityTypeComboBox.Size = new System.Drawing.Size(112, 21);
            this.ArticleEntityTypeComboBox.TabIndex = 16;
            this.ArticleEntityTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.ArticleEntityTypeComboBox_SelectedIndexChanged);
            // 
            // CategoryIDLabel
            // 
            this.CategoryIDLabel.AutoSize = true;
            this.CategoryIDLabel.Location = new System.Drawing.Point(379, 41);
            this.CategoryIDLabel.Name = "CategoryIDLabel";
            this.CategoryIDLabel.Size = new System.Drawing.Size(64, 13);
            this.CategoryIDLabel.TabIndex = 21;
            this.CategoryIDLabel.Text = "CategoryID";
            // 
            // ArticleEntityTypeLabel
            // 
            this.ArticleEntityTypeLabel.AutoSize = true;
            this.ArticleEntityTypeLabel.Location = new System.Drawing.Point(258, 40);
            this.ArticleEntityTypeLabel.Name = "ArticleEntityTypeLabel";
            this.ArticleEntityTypeLabel.Size = new System.Drawing.Size(99, 13);
            this.ArticleEntityTypeLabel.TabIndex = 18;
            this.ArticleEntityTypeLabel.Text = "Article Entity Type";
            // 
            // ArticleIDLabel
            // 
            this.ArticleIDLabel.AutoSize = true;
            this.ArticleIDLabel.Location = new System.Drawing.Point(488, 41);
            this.ArticleIDLabel.Name = "ArticleIDLabel";
            this.ArticleIDLabel.Size = new System.Drawing.Size(50, 13);
            this.ArticleIDLabel.TabIndex = 19;
            this.ArticleIDLabel.Text = "ArticleID";
            // 
            // MainSplitter
            // 
            this.MainSplitter.Location = new System.Drawing.Point(255, 81);
            this.MainSplitter.Name = "MainSplitter";
            this.MainSplitter.Size = new System.Drawing.Size(10, 519);
            this.MainSplitter.TabIndex = 15;
            this.MainSplitter.TabStop = false;
            // 
            // ArticlesView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.MainSplitter);
            this.Controls.Add(this.ArticlesDataGridView);
            this.Controls.Add(this.SiteCategoriesTreeView);
            this.Controls.Add(this.ControlPanel);
            this.Name = "ArticlesView";
            this.Size = new System.Drawing.Size(800, 600);
            ((System.ComponentModel.ISupportInitialize)(this.ArticlesDataGridView)).EndInit();
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox SitesComboBox;
        private System.Windows.Forms.Label SiteLabel;
        private System.Windows.Forms.Button RefreshCategoriesButton;
        private System.Windows.Forms.Button AddArticleButton;
        private System.Windows.Forms.Label ServiceEnvironmentLabel;
        private System.Windows.Forms.ComboBox ServiceEnvironmentComboBox;
        private System.Windows.Forms.TreeView SiteCategoriesTreeView;
        private System.Windows.Forms.DataGridView ArticlesDataGridView;
        private System.Windows.Forms.Button EditSiteCategoryButton;
        private System.Windows.Forms.Button AddCategoryButton;
        private System.Windows.Forms.Panel ControlPanel;
        private System.Windows.Forms.Splitter MainSplitter;
        private System.Windows.Forms.Label ArticleIDLabel;
        private System.Windows.Forms.Button RetrieveButton;
        private System.Windows.Forms.TextBox ArticleIDTextBox;
        private System.Windows.Forms.ComboBox ArticleEntityTypeComboBox;
        private System.Windows.Forms.Label ArticleEntityTypeLabel;
        private System.Windows.Forms.Label CategoryIDLabel;
        private System.Windows.Forms.TextBox CategoryIDTextBox;
        private System.Windows.Forms.Button ButtonWSOPublish;
        private System.Windows.Forms.Button ButtonWSOHelp;


    }
}
