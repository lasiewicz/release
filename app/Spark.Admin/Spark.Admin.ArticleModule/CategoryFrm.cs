using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI;

using Spark.Admin.Common;

using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.Article;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Spark.Admin.ArticleModule
{
    public partial class CategoryFrm : Spark.Admin.Common.Controls.Form
    {
        private Category category;
        private ArticlesController articlesController;
        private Int32 nextPublishActionPublishObjectID = Matchnet.Constants.NULL_INT;
        private String nextStepText = String.Empty;

        #region Publish Constants
        private const Int32 PubActPubObjIDCategoryAdd = 500;
        private const Int32 PubActPubObjIDCategoryEdit = 501;
        private const Int32 PubActPubObjIDCategoryDelete = 502;
        private const Int32 PubActPubObjIDCategoryPublishToContentStg = 503;
        private const Int32 PubActPubObjIDCategoryApproveInContentStg = 504;
        private const Int32 PubActPubObjIDCategoryPublishToProd = 505;
        private const Int32 PubActPubObjIDCategoryVerifyInProd = 506;
        private const String PubActPubObjTextCategoryApproveInContentStg = "Approve in ContentStg";
        private const String PubActPubObjTextCategoryPublishToContentStg = "Publish to ContentStg";
        private const String PubActPubObjTextCategoryPublishToProd = "Publish to Prod";
        private const String PubActPubObjTextCategoryVerifyInProd = "Verify in Prod";
        #endregion

        #region Message Constants
        private const String MsgSaveCategoryConfirm = "Are you sure you want to save this Category?";
        private const String MsgNextStepCategoryConfirm = "Are you sure you want to {0} this Category?";
        #endregion

        public CategoryFrm(State globalState, ArticlesController articlesController) : base(globalState)
        {
            InitializeComponent();

            this.articlesController = articlesController;
            
            ServiceEnvironmentLabel.Text = articlesController.CurrentServiceEnvironmentTypeEnum.ToString().ToUpper();

            // Display/Hide Save and Delete buttons.
            if (!HasPrivilege(ModuleWorkItem.PrivilegeIDArticleEditor) || articlesController.CurrentServiceEnvironmentTypeEnum != ServiceEnvironmentTypeEnum.Dev)
            {
                SaveButton.Visible = false;
            }
        }

        public CategoryFrm(State globalState, ArticlesController articlesController, Int32 categoryID) : this(globalState, articlesController)
        {
            category = articlesController.RetrieveCategory(categoryID, articlesController.CurrentServiceEnvironmentTypeEnum);

            InitializeNextStep();
        }

        public CategoryFrm(State globalState, ArticlesController articlesController, Int32 parentCategoryID, Boolean isParentCategoryID) : this(globalState, articlesController)
        {
            category = new Category();
            category.ParentCategoryID = parentCategoryID;

            InitializeNextStep();
        }

        #region Private Methods
        private void CategoryFrm_Load(object sender, System.EventArgs e)
        {
            DisplayCategory();
        }

        private void InitializeNextStep()
        {
            NextStepButton.Visible = false;

            // If this is a new category, there will be no next step.
            if (category.CategoryID != Matchnet.Constants.NULL_INT && category.CategoryID != Int32.MinValue)
            {
                ServiceEnvironmentTypeEnum serviceEnvrionmentTypeEnum;
                // Get information about the next Publish step.
                nextPublishActionPublishObjectID = CommonController.GetNextPublishActionPublishObjectStep(category.CategoryID, PublishObjectType.Category, out serviceEnvrionmentTypeEnum);

                // Display/Hide NextStepButton.
                if (category.CategoryID > 0 && nextPublishActionPublishObjectID != Matchnet.Constants.NULL_INT && (articlesController.CurrentServiceEnvironmentTypeEnum == serviceEnvrionmentTypeEnum))
                {
                    switch (nextPublishActionPublishObjectID)
                    {
                        case PubActPubObjIDCategoryPublishToContentStg:
                            NextStepButton.Visible = true;
                            nextStepText = PubActPubObjTextCategoryPublishToContentStg;

                            break;

                        case PubActPubObjIDCategoryApproveInContentStg:
                            if (HasPrivilege(ModuleWorkItem.PrivilegeIDArticleApprover))
                            {
                                NextStepButton.Visible = true;
                                nextStepText = PubActPubObjTextCategoryApproveInContentStg;
                            }

                            break;

                        case PubActPubObjIDCategoryPublishToProd:
                            if (HasPrivilege(ModuleWorkItem.PrivilegeIDArticlePublisher))
                            {
                                NextStepButton.Visible = true;
                                nextStepText = PubActPubObjTextCategoryPublishToProd;
                            }

                            break;

                        case PubActPubObjIDCategoryVerifyInProd:
                            NextStepButton.Visible = true;
                            nextStepText = PubActPubObjTextCategoryVerifyInProd;

                            break;

                        default:
                            NextStepButton.Visible = false;

                            break;
                    }

                    NextStepButton.Text = nextStepText;
                }
            }
        }

        private void DisplayCategory()
        {
            CategoryIDTextBox.Text = category.CategoryID.ToString();
            ParentCategoryIDTextBox.Text = category.ParentCategoryID.ToString();
            ListOrderTextBox.Text = category.ListOrder.ToString();
            ConstantTextBox.Text = category.Constant;
            if (category.LastUpdated > LastUpdatedDateTimePicker.MinDate)
                LastUpdatedDateTimePicker.Value = category.LastUpdated;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            category = BuildCategory();

            // Are you sure?
            if (MessageBox.Show(MsgSaveCategoryConfirm, Constants.MsgBoxCaptionConfirm, MessageBoxButtons.YesNo) == DialogResult.No)
                return;
            else
            {
                try
                {
                    // Save.
                    articlesController.SaveCategory(category, MemberID, false, ServiceEnvironmentTypeEnum.Dev);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format(Constants.MsgContactAdministrator, ex.Message), Constants.MsgBoxCaptionSystemError);
                    return;
                }

                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private Category BuildCategory()
        {
            Category category = new Category();

            category.CategoryID = Convert.ToInt32(CategoryIDTextBox.Text.Trim());
            category.ParentCategoryID = Convert.ToInt32(ParentCategoryIDTextBox.Text.Trim());
            category.ListOrder = Convert.ToInt32(ListOrderTextBox.Text.Trim());
            category.Constant = ConstantTextBox.Text.Trim();

            return category;
        }

        private void SCancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ConstantTextBox_TextChanged(object sender, System.EventArgs e)
        {
            CommonController.BuildConstant(ref ConstantTextBox);
        }

        private void NextStepButton_Click(object sender, EventArgs e)
        {
            // Are you sure?
            if (MessageBox.Show(String.Format(MsgNextStepCategoryConfirm, NextStepButton.Text), Constants.MsgBoxCaptionConfirm, MessageBoxButtons.YesNo) == DialogResult.No)
                return;
            else
            {
                try
                {
                    // Do the correct action.
                    switch (nextPublishActionPublishObjectID)
                    {
                        case PubActPubObjIDCategoryPublishToContentStg:
                            articlesController.SaveCategory(category, MemberID, true, ServiceEnvironmentTypeEnum.ContentStg);
                            CommonController.SavePublishAction(category.PublishID, PubActPubObjIDCategoryPublishToContentStg, MemberID);

                            break;

                        case PubActPubObjIDCategoryPublishToProd:
                            articlesController.SaveCategory(category, MemberID, true, ServiceEnvironmentTypeEnum.Prod);
                            CommonController.SavePublishAction(category.PublishID, PubActPubObjIDCategoryPublishToProd, MemberID);

                            Helper.SendEmail(Constants.EmailsToAlert, Helper.EmailType.DeploymentToProd
                                , "Category", MemberEmail, BuildEmailBody());

                            break;

                        // All Verifies are handled here.
                        default:
                            CommonController.SavePublishAction(category.PublishID, nextPublishActionPublishObjectID, MemberID);

                            break;
                    }

                    DialogResult = DialogResult.OK;
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format(Constants.MsgContactAdministrator, ex.Message), Constants.MsgBoxCaptionSystemError);
                    return;
                }
            }
        }

        private String BuildEmailBody()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append("CategoryID: ").Append(category.CategoryID.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("ParentCategoryID: ").Append(category.ParentCategoryID).Append(Environment.NewLine);
            stringBuilder.Append("Constant: ").Append(category.Constant).Append(Environment.NewLine);
            stringBuilder.Append("ListOrder: ").Append(category.ListOrder.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("PublishID: ").Append(category.PublishID.ToString()).Append(Environment.NewLine);

            return stringBuilder.ToString();
        }
        #endregion
    }
}