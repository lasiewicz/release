﻿namespace Spark.Admin.ArticleModule
{
    partial class WSOListPublisherFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WSOListPublisherFrm));
            this.DataGridViewWSOItem = new System.Windows.Forms.DataGridView();
            this.ButtonAddNewItem = new System.Windows.Forms.Button();
            this.ButtonDeleteItem = new System.Windows.Forms.Button();
            this.ButtonRefreshList = new System.Windows.Forms.Button();
            this.LabelWSOList = new System.Windows.Forms.Label();
            this.LabelEnvironment = new System.Windows.Forms.Label();
            this.ButtonPublishTo = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ToolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewWSOItem)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // DataGridViewWSOItem
            // 
            this.DataGridViewWSOItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewWSOItem.Location = new System.Drawing.Point(12, 65);
            this.DataGridViewWSOItem.Name = "DataGridViewWSOItem";
            this.DataGridViewWSOItem.RowTemplate.Height = 24;
            this.DataGridViewWSOItem.Size = new System.Drawing.Size(698, 375);
            this.DataGridViewWSOItem.TabIndex = 0;
            // 
            // ButtonAddNewItem
            // 
            this.ButtonAddNewItem.Location = new System.Drawing.Point(100, 447);
            this.ButtonAddNewItem.Name = "ButtonAddNewItem";
            this.ButtonAddNewItem.Size = new System.Drawing.Size(130, 23);
            this.ButtonAddNewItem.TabIndex = 1;
            this.ButtonAddNewItem.Text = "Add New Item";
            this.ButtonAddNewItem.UseVisualStyleBackColor = true;
            this.ButtonAddNewItem.Click += new System.EventHandler(this.ButtonAddNewItem_Click);
            // 
            // ButtonDeleteItem
            // 
            this.ButtonDeleteItem.Location = new System.Drawing.Point(279, 447);
            this.ButtonDeleteItem.Name = "ButtonDeleteItem";
            this.ButtonDeleteItem.Size = new System.Drawing.Size(136, 23);
            this.ButtonDeleteItem.TabIndex = 2;
            this.ButtonDeleteItem.Text = "Delete Selected Item(s)";
            this.ButtonDeleteItem.UseVisualStyleBackColor = true;
            this.ButtonDeleteItem.Click += new System.EventHandler(this.ButtonDeleteItem_Click);
            // 
            // ButtonRefreshList
            // 
            this.ButtonRefreshList.Location = new System.Drawing.Point(453, 446);
            this.ButtonRefreshList.Name = "ButtonRefreshList";
            this.ButtonRefreshList.Size = new System.Drawing.Size(133, 23);
            this.ButtonRefreshList.TabIndex = 3;
            this.ButtonRefreshList.Text = "Refresh List";
            this.ButtonRefreshList.UseVisualStyleBackColor = true;
            this.ButtonRefreshList.Click += new System.EventHandler(this.ButtonRefreshList_Click);
            // 
            // LabelWSOList
            // 
            this.LabelWSOList.AutoSize = true;
            this.LabelWSOList.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelWSOList.Location = new System.Drawing.Point(12, 21);
            this.LabelWSOList.Name = "LabelWSOList";
            this.LabelWSOList.Size = new System.Drawing.Size(110, 21);
            this.LabelWSOList.TabIndex = 4;
            this.LabelWSOList.Text = "LabelWSOList";
            // 
            // LabelEnvironment
            // 
            this.LabelEnvironment.AutoSize = true;
            this.LabelEnvironment.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelEnvironment.Location = new System.Drawing.Point(569, 21);
            this.LabelEnvironment.Name = "LabelEnvironment";
            this.LabelEnvironment.Size = new System.Drawing.Size(141, 21);
            this.LabelEnvironment.TabIndex = 5;
            this.LabelEnvironment.Text = "LabelEnvironment";
            this.LabelEnvironment.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ButtonPublishTo
            // 
            this.ButtonPublishTo.Location = new System.Drawing.Point(227, 487);
            this.ButtonPublishTo.Name = "ButtonPublishTo";
            this.ButtonPublishTo.Size = new System.Drawing.Size(237, 39);
            this.ButtonPublishTo.TabIndex = 6;
            this.ButtonPublishTo.Text = "ButtonPublishTo";
            this.ButtonPublishTo.UseVisualStyleBackColor = true;
            this.ButtonPublishTo.Click += new System.EventHandler(this.ButtonPublishTo_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 542);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(722, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // ToolStripStatusLabel1
            // 
            this.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1";
            this.ToolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // WSOListPublisherFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(722, 564);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.ButtonPublishTo);
            this.Controls.Add(this.LabelEnvironment);
            this.Controls.Add(this.LabelWSOList);
            this.Controls.Add(this.ButtonRefreshList);
            this.Controls.Add(this.ButtonDeleteItem);
            this.Controls.Add(this.ButtonAddNewItem);
            this.Controls.Add(this.DataGridViewWSOItem);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WSOListPublisherFrm";
            this.Text = "WSO List Publisher";
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewWSOItem)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridViewWSOItem;
        private System.Windows.Forms.Button ButtonAddNewItem;
        private System.Windows.Forms.Button ButtonDeleteItem;
        private System.Windows.Forms.Button ButtonRefreshList;
        private System.Windows.Forms.Label LabelWSOList;
        private System.Windows.Forms.Label LabelEnvironment;
        private System.Windows.Forms.Button ButtonPublishTo;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel1;
    }
}