using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.CompositeUI;

using Spark.Admin.WebServices;
using Matchnet.Content.ValueObjects.Article;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Admin;

using Spark.Admin.Common;

namespace Spark.Admin.ArticleModule
{
    public partial class ArticlesView : BaseModuleView
    {
        private const String TitleArticles = "Articles";
      //  public const String MsgSiteIDAndPageIDRequired = "Please select a Site and enter a valid PageID";
        private ArticlesController articlesController;
        private SiteArticleCollection siteArticleCollection;
        private ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum;
        private WSOListManagerFrm wsoListManagerFrm;
        private WSOHelp wsoHelp;

        #region Contructors
        public ArticlesView()
        {
            InitializeComponent();

            // Set the Tab title.
            this.Title = TitleArticles;
        }
        #endregion

        #region Private Methods
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // You must manually assign GlobalState to the Controller.
            articlesController.GlobalState = GlobalState;

            InitializeArticlesDataGridView();

            // Set up SitesComboBox.
            SitesComboBox.DataSource = ((Sites)GlobalState[Constants.StateSites]);
            SitesComboBox.ValueMember = "SiteID";
            SitesComboBox.DisplayMember = "Name";

            // Set up ArticleEntityTypeComboBox.
            ArticleEntityTypeComboBox.DataSource = Enum.GetValues(typeof(ArticleEntityType));

            // Set up ServiceEnvironment ComboBox.
            ServiceEnvironmentComboBox.DataSource = Enum.GetValues(typeof(ServiceEnvironmentTypeEnum));
            // Make a call to ServiceEnvironmentComboBox_SelectedIndexChanged initialize currentServiceEnvironmentTypeEnum.
            ServiceEnvironmentComboBox_SelectedIndexChanged(null, null);

            if (!HasPrivilege(ModuleWorkItem.PrivilegeIDArticleEditor))
            {
                AddArticleButton.Visible = false;
            }

            LoadSiteCategories();
        }

        private void InitializeArticlesDataGridView()
        {
            ArticlesDataGridView.AutoGenerateColumns = false;
            ArticlesDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            ArticlesDataGridView.EditMode = DataGridViewEditMode.EditProgrammatically;
            ArticlesDataGridView.MultiSelect = false;
            ArticlesDataGridView.AllowUserToAddRows = false;
            ArticlesDataGridView.AllowUserToDeleteRows = false;

            DataGridViewColumn column;

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "SiteArticleID";
            column.Name = "SiteArticleID";
            ArticlesDataGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "ArticleID";
            column.Name = "ArticleID";
            ArticlesDataGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "SiteID";
            column.Name = "SiteID";
            ArticlesDataGridView.Columns.Add(column);

            column = new DataGridViewCheckBoxColumn();
            column.DataPropertyName = "PublishedFlag";
            column.Name = "Is Published";
            ArticlesDataGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Constant";
            column.Name = "Constant";
            ArticlesDataGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Title";
            column.Name = "Title";
            ArticlesDataGridView.Columns.Add(column);

            column = new DataGridViewCheckBoxColumn();
            column.DataPropertyName = "ActiveFlag";
            column.Name = "Is Active";
            ArticlesDataGridView.Columns.Add(column);

            column = new DataGridViewCheckBoxColumn();
            column.DataPropertyName = "FeaturedFlag";
            column.Name = "Is Featured";
            ArticlesDataGridView.Columns.Add(column);
        }

        private void LoadSiteCategories()
        {
            ArticlesDataGridView.DataSource = null;

            SiteCategoriesTreeView.Nodes.Clear();
            SiteCategoryNode rootNode = new SiteCategoryNode("[Categories]", Int32.MinValue);
            SiteCategoriesTreeView.Nodes.Add(rootNode);

            try
            {
                SiteCategoryCollection siteCategoryCollection = articlesController.RetrieveSiteCategories(Convert.ToInt32(SitesComboBox.SelectedValue), true, currentServiceEnvironmentTypeEnum);

                foreach (SiteCategory siteCategory in siteCategoryCollection)
                {
                    CategoriesFrm.AddCategoryNode(rootNode, siteCategory);
                }
            }
            catch (Exception ex)
            {
                // HACK:  On initialization, this error is thrown.  Doesn't affect usability so ignore it.
                if (ex.Message != "Unable to cast object of type 'Matchnet.Content.ValueObjects.BrandConfig.Site' to type 'System.IConvertible'.")
                    MessageBox.Show(String.Format(Constants.MsgContactAdministrator, ex.Message), Constants.MsgBoxCaptionSystemError);
            }

            SiteCategoriesTreeView.Nodes[0].Expand();
        }

        private void RefreshCategoriesButton_Click(object sender, EventArgs e)
        {
            LoadSiteCategories();
        }

        private void CategoriesTreeView_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
        {
            LoadArticles();
        }

        public void LoadArticles()
        {
            SiteCategoryNode siteCategoryNode = (SiteCategoryNode)SiteCategoriesTreeView.SelectedNode;
            if (siteCategoryNode != null && siteCategoryNode.CategoryID != Int32.MinValue)
            {
                try
                {
                    siteArticleCollection = articlesController.RetrieveCategorySiteArticlesAndArticles(siteCategoryNode.CategoryID, Convert.ToInt32(SitesComboBox.SelectedValue), currentServiceEnvironmentTypeEnum);

                    ArticlesDataGridView.DataSource = siteArticleCollection;

                    //            // If a value doesn not exist for the SiteArticleID then make it regular.
                    //            // KNOWN ISSUE:  Clipping occurs if we make the default font for the tree as regular and then try
                    //            // to bold individual nodes.  Doing this in reverse avoids this problem.
                    //            if (siteArticle.SiteArticleID == Matchnet.Constants.NULL_INT)
                    //                listViewItem.Font = new Font("Microsoft Sans Serif", (float)8.25, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
                    //            // If the Article is not published, gray it out.
                    //            if (siteArticle.PublishedFlag == false)
                    //                listViewItem.ForeColor = System.Drawing.Color.Gray;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format(Constants.MsgContactAdministrator, ex.Message), Constants.MsgBoxCaptionSystemError);
                }

                SiteCategoriesTreeView.Nodes[0].Expand();
            }
        }

        public void EditSiteArticle()
        {
            if (ArticlesDataGridView.SelectedRows.Count > 0)
            {
                EditSiteArticle(siteArticleCollection[ArticlesDataGridView.SelectedRows[0].Index]);
            }
            else
            {
                MessageBox.Show("Please select the SiteArticle from the SiteArticles list that you would like to edit.", Constants.MsgBoxCaptionWarning);
            }
        }

        public void EditSiteArticle(SiteArticle siteArticle)
        {
            SiteArticleFrm siteArticleFrm = new SiteArticleFrm(GlobalState, articlesController, siteArticle);

            if (siteArticleFrm.ShowDialog() == DialogResult.OK)
            {
                LoadArticles();
            }
        }

        public void EditArticle(Int32 articleID)
        {
            ArticleFrm articleFrm = new ArticleFrm(GlobalState, articlesController, articleID);

            if (articleFrm.ShowDialog() == DialogResult.OK)
            {
                LoadArticles();
            }
        }

        private void EditCategory()
        {
            CategoryFrm categoryFrm = new CategoryFrm(GlobalState, articlesController, Convert.ToInt32(SiteCategoriesTreeView.SelectedNode.Name));

            if (categoryFrm.ShowDialog() == DialogResult.OK)
            {
                LoadSiteCategories();
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            AddArticle();
        }

        private void AddArticle()
        {
            SiteCategoryNode siteCategoryNode = (SiteCategoryNode)SiteCategoriesTreeView.SelectedNode;
            if (siteCategoryNode != null && siteCategoryNode.CategoryID != Int32.MinValue)
            {
                ArticleFrm articleFrm = new ArticleFrm(GlobalState, articlesController, siteCategoryNode.CategoryID, true);

                if (articleFrm.ShowDialog() == DialogResult.OK)
                {
                    LoadSiteCategories();
                }
            }
            else
            {
                MessageBox.Show("Please select the Category from the Categories Tree View to which you would like to add an Article.", Constants.MsgBoxCaptionWarning, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void ArticlesDataGridView_RowHeaderMouseDoubleClick(object sender, System.Windows.Forms.DataGridViewCellMouseEventArgs e)
        {
            EditSiteArticle();
        }

        private void ServiceEnvironmentComboBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            currentServiceEnvironmentTypeEnum = ((ServiceEnvironmentTypeEnum)ServiceEnvironmentComboBox.SelectedValue);

            // Set the currentServiceEnvironmentTypeEnum so that the other Forms know what ServiceEnvironmentType we're working on.
            articlesController.CurrentServiceEnvironmentTypeEnum = currentServiceEnvironmentTypeEnum;

            if (currentServiceEnvironmentTypeEnum != ServiceEnvironmentTypeEnum.Dev)
            {
                AddArticleButton.Visible = false;
                AddCategoryButton.Visible = false;
            }
            else
            {
                AddArticleButton.Visible = true;
                AddCategoryButton.Visible = true;
            }

            LoadSiteCategories();
        }

        private void SitesComboBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadSiteCategories();
        }

        private void EditSiteArticleButton_Click(object sender, EventArgs e)
        {
            EditSiteArticle();
        }
        #endregion

        #region Properties
        [CreateNew]
        public ArticlesController ArticlesController
        {
            set
            {
                articlesController = value;
            }
        }

        private void EditSiteCategoryButton_Click(object sender, EventArgs e)
        {
            EditSiteCategory();
        }

        private void EditSiteCategory()
        {
            SiteCategoryNode siteCategoryNode = (SiteCategoryNode)SiteCategoriesTreeView.SelectedNode;
            if (siteCategoryNode != null && siteCategoryNode.CategoryID != Int32.MinValue)
            {
                if (siteCategoryNode.CategoryID == Int32.MinValue)
                    return;

                SiteCategoryFrm siteCategoryFrm = new SiteCategoryFrm(GlobalState, articlesController, siteCategoryNode.SiteCategory);

                if (siteCategoryFrm.ShowDialog() == DialogResult.OK)
                {
                    LoadSiteCategories();
                }
            }
            else
            {
                MessageBox.Show("Please select the SiteCategory from the Categories Tree View that you would like to edit.", "Category Not Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void AddCategoryButton_Click(object sender, EventArgs e)
        {
            AddCategory();
        }

        private void AddCategory()
        {
            SiteCategoryNode siteCategoryNode = (SiteCategoryNode)SiteCategoriesTreeView.SelectedNode;
            if (siteCategoryNode != null)
            {
                CategoryFrm categoryFrm = new CategoryFrm(GlobalState, articlesController, siteCategoryNode.CategoryID, true);

                if (categoryFrm.ShowDialog() == DialogResult.OK)
                {
                    LoadSiteCategories();
                }
            }
            else
            {
                MessageBox.Show("Please select the Category from the Categories Tree View to which you would like to add a child Category.", Constants.MsgBoxCaptionWarning, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        #endregion

        private void RetrieveButton_Click(object sender, EventArgs e)
        {
            LoadSiteCategories();
                
            // Set the CategoryID entered.
            String categoryIDText = CategoryIDTextBox.Text.Trim();
            Double categoryIDDouble = Matchnet.Constants.NULL_DOUBLE;
            if (Double.TryParse(categoryIDText, out categoryIDDouble))  // Search by CategoryID.
            {
                TreeNode[] nodes = SiteCategoriesTreeView.Nodes.Find(((Int32)categoryIDDouble).ToString(), true);

                if (nodes.Length == 0)
                {
                    MessageBox.Show("\"" + categoryIDText + "\" is not a valid CategoryID.", Constants.MsgBoxCaptionWarning, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else 
                {
                    SiteCategoriesTreeView.SelectedNode = nodes[0];
                }
            }
            else
            {
                MessageBox.Show("\"" + categoryIDText + "\" is not a valid CategoryID.", Constants.MsgBoxCaptionWarning, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            LoadArticles();

            // Retrieve the ArticleEntityID, if necessary.
            Int32 articleID = Matchnet.Constants.NULL_INT;
            if (ArticleIDTextBox.Visible)
            {
                Double articleIDDouble;
                if (Double.TryParse(ArticleIDTextBox.Text, out articleIDDouble))
                {
                    articleID = (Int32)articleIDDouble;
                }
                else 
                {
                    MessageBox.Show("The ArticleEntityID you entered is not valid.", Constants.MsgBoxCaptionWarning, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

            switch (((ArticleEntityType)ArticleEntityTypeComboBox.SelectedValue))
            { 
                case ArticleEntityType.Article:
                    foreach (SiteArticle siteArticle in siteArticleCollection)
                    {
                        if (siteArticle.ArticleID == articleID)
                        {
                            EditArticle(siteArticle.ArticleID);
                            return;
                        }
                    }

                    MessageBox.Show("The ArticleID you entered does not exist for the CategoryID you entered.", Constants.MsgBoxCaptionWarning, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    break;

                case ArticleEntityType.SiteArticle:
                    foreach (SiteArticle siteArticle in siteArticleCollection)
                    {
                        if (siteArticle.ArticleID == articleID)
                        {
                            EditSiteArticle(siteArticle);
                            return;
                        }
                    }

                    MessageBox.Show("The SiteArticleID you entered does not exist for the Site/CategoryID combination you entered.", Constants.MsgBoxCaptionWarning, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    break;

                case ArticleEntityType.Category:
                    EditCategory();

                    break;

                case ArticleEntityType.SiteCategory:
                    EditSiteCategory();

                    break;
            }
        }

        private void ArticleEntityTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Make the appropriate items visible for this type of search.
            switch (((ArticleEntityType)ArticleEntityTypeComboBox.SelectedValue))
            {
                case ArticleEntityType.Article:
                    ArticleIDLabel.Visible = true;
                    ArticleIDTextBox.Visible = true;

                    break;

                case ArticleEntityType.SiteArticle:
                    ArticleIDLabel.Visible = true;
                    ArticleIDTextBox.Visible = true;

                    break;

                case ArticleEntityType.Category:
                    ArticleIDLabel.Visible = false;
                    ArticleIDTextBox.Visible = false;

                    break;

                case ArticleEntityType.SiteCategory:

                    ArticleIDLabel.Visible = false;
                    ArticleIDTextBox.Visible = false;

                    break;
            }
        }

        private void ButtonWSOPublish_Click(object sender, EventArgs e)
        {
            if (wsoListManagerFrm == null || wsoListManagerFrm.Visible == false)
            {
                wsoListManagerFrm = new WSOListManagerFrm(GlobalState, articlesController);
                wsoListManagerFrm.Show();
                
            }

            wsoListManagerFrm.Focus();
        }

        private void ButtonWSOHelp_Click(object sender, EventArgs e)
        {
            if ((wsoHelp == null) || (wsoHelp.Visible == false))
            {
                wsoHelp = new WSOHelp();
                wsoHelp.Show();
            }

            wsoHelp.Focus();
        }
    }

    public enum ArticleEntityType
    { 
        Article,
        Category,
        SiteArticle,
        SiteCategory
    }
}
