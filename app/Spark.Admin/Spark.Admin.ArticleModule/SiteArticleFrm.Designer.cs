namespace Spark.Admin.ArticleModule
{
    partial class SiteArticleFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ContentRichTextBox = new System.Windows.Forms.RichTextBox();
            this.CommandsPanel = new System.Windows.Forms.Panel();
            this.SCancelButton = new System.Windows.Forms.Button();
            this.EditArticleButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.NextStepButton = new System.Windows.Forms.Button();
            this.OrdinalIDLabel = new System.Windows.Forms.Label();
            this.FileIDLabel = new System.Windows.Forms.Label();
            this.ContentLabel = new System.Windows.Forms.Label();
            this.TitleTextBox = new System.Windows.Forms.TextBox();
            this.TitleLabel = new System.Windows.Forms.Label();
            this.ArticleAttributesGroupBox = new System.Windows.Forms.GroupBox();
            this.ConstantTextBox = new System.Windows.Forms.TextBox();
            this.ConstantLabel = new System.Windows.Forms.Label();
            this.ArticleIDTextBox = new System.Windows.Forms.TextBox();
            this.ArticleID = new System.Windows.Forms.Label();
            this.PublishIDTextBox = new System.Windows.Forms.TextBox();
            this.PublishIDLabel = new System.Windows.Forms.Label();
            this.MemberIDTextBox = new System.Windows.Forms.TextBox();
            this.MemberIDLabel = new System.Windows.Forms.Label();
            this.SitesComboBox = new System.Windows.Forms.ComboBox();
            this.SiteLabel = new System.Windows.Forms.Label();
            this.SiteArticleIDTextBox = new System.Windows.Forms.TextBox();
            this.SiteArticleIDLabel = new System.Windows.Forms.Label();
            this.FileIDTextBox = new System.Windows.Forms.TextBox();
            this.OrdinalIDTextBox = new System.Windows.Forms.TextBox();
            this.PublishedCheckBox = new System.Windows.Forms.CheckBox();
            this.FeaturedCheckBox = new System.Windows.Forms.CheckBox();
            this.LastUpdatedDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.LastUpdatedLabel = new System.Windows.Forms.Label();
            this.ServiceEnvironmentLabel = new System.Windows.Forms.Label();
            this.txtPageTitle = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMetaDescription = new System.Windows.Forms.TextBox();
            this.CommandsPanel.SuspendLayout();
            this.ArticleAttributesGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContentRichTextBox
            // 
            this.ContentRichTextBox.Location = new System.Drawing.Point(83, 166);
            this.ContentRichTextBox.Name = "ContentRichTextBox";
            this.ContentRichTextBox.Size = new System.Drawing.Size(530, 225);
            this.ContentRichTextBox.TabIndex = 64;
            this.ContentRichTextBox.Text = "";
            // 
            // CommandsPanel
            // 
            this.CommandsPanel.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.CommandsPanel.Controls.Add(this.SCancelButton);
            this.CommandsPanel.Controls.Add(this.EditArticleButton);
            this.CommandsPanel.Controls.Add(this.SaveButton);
            this.CommandsPanel.Controls.Add(this.NextStepButton);
            this.CommandsPanel.Location = new System.Drawing.Point(9, 564);
            this.CommandsPanel.Name = "CommandsPanel";
            this.CommandsPanel.Size = new System.Drawing.Size(605, 53);
            this.CommandsPanel.TabIndex = 57;
            // 
            // SCancelButton
            // 
            this.SCancelButton.Location = new System.Drawing.Point(504, 16);
            this.SCancelButton.Name = "SCancelButton";
            this.SCancelButton.Size = new System.Drawing.Size(92, 23);
            this.SCancelButton.TabIndex = 35;
            this.SCancelButton.Text = "Cancel";
            this.SCancelButton.UseVisualStyleBackColor = true;
            this.SCancelButton.Click += new System.EventHandler(this.SCancelButton_Click);
            // 
            // EditArticleButton
            // 
            this.EditArticleButton.Location = new System.Drawing.Point(260, 16);
            this.EditArticleButton.Name = "EditArticleButton";
            this.EditArticleButton.Size = new System.Drawing.Size(92, 23);
            this.EditArticleButton.TabIndex = 34;
            this.EditArticleButton.Text = "Edit Article";
            this.EditArticleButton.UseVisualStyleBackColor = true;
            this.EditArticleButton.Click += new System.EventHandler(this.EditArticleButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(406, 16);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(92, 23);
            this.SaveButton.TabIndex = 33;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // NextStepButton
            // 
            this.NextStepButton.Location = new System.Drawing.Point(8, 9);
            this.NextStepButton.Name = "NextStepButton";
            this.NextStepButton.Size = new System.Drawing.Size(92, 36);
            this.NextStepButton.TabIndex = 30;
            this.NextStepButton.Text = "NextStep";
            this.NextStepButton.UseVisualStyleBackColor = true;
            this.NextStepButton.Click += new System.EventHandler(this.NextStepButton_Click);
            // 
            // OrdinalIDLabel
            // 
            this.OrdinalIDLabel.AutoSize = true;
            this.OrdinalIDLabel.Location = new System.Drawing.Point(212, 492);
            this.OrdinalIDLabel.Name = "OrdinalIDLabel";
            this.OrdinalIDLabel.Size = new System.Drawing.Size(51, 13);
            this.OrdinalIDLabel.TabIndex = 56;
            this.OrdinalIDLabel.Text = "OrdinalID";
            this.OrdinalIDLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // FileIDLabel
            // 
            this.FileIDLabel.AutoSize = true;
            this.FileIDLabel.Location = new System.Drawing.Point(43, 492);
            this.FileIDLabel.Name = "FileIDLabel";
            this.FileIDLabel.Size = new System.Drawing.Size(34, 13);
            this.FileIDLabel.TabIndex = 55;
            this.FileIDLabel.Text = "FileID";
            this.FileIDLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ContentLabel
            // 
            this.ContentLabel.AutoSize = true;
            this.ContentLabel.Location = new System.Drawing.Point(33, 169);
            this.ContentLabel.Name = "ContentLabel";
            this.ContentLabel.Size = new System.Drawing.Size(44, 13);
            this.ContentLabel.TabIndex = 54;
            this.ContentLabel.Text = "Content";
            this.ContentLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // TitleTextBox
            // 
            this.TitleTextBox.Location = new System.Drawing.Point(83, 114);
            this.TitleTextBox.Name = "TitleTextBox";
            this.TitleTextBox.Size = new System.Drawing.Size(530, 20);
            this.TitleTextBox.TabIndex = 53;
            // 
            // TitleLabel
            // 
            this.TitleLabel.AutoSize = true;
            this.TitleLabel.Location = new System.Drawing.Point(50, 117);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(27, 13);
            this.TitleLabel.TabIndex = 52;
            this.TitleLabel.Text = "Title";
            this.TitleLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ArticleAttributesGroupBox
            // 
            this.ArticleAttributesGroupBox.Controls.Add(this.ConstantTextBox);
            this.ArticleAttributesGroupBox.Controls.Add(this.ConstantLabel);
            this.ArticleAttributesGroupBox.Controls.Add(this.ArticleIDTextBox);
            this.ArticleAttributesGroupBox.Controls.Add(this.ArticleID);
            this.ArticleAttributesGroupBox.Location = new System.Drawing.Point(83, 63);
            this.ArticleAttributesGroupBox.Name = "ArticleAttributesGroupBox";
            this.ArticleAttributesGroupBox.Size = new System.Drawing.Size(531, 45);
            this.ArticleAttributesGroupBox.TabIndex = 47;
            this.ArticleAttributesGroupBox.TabStop = false;
            this.ArticleAttributesGroupBox.Text = "Article Attributes";
            // 
            // ConstantTextBox
            // 
            this.ConstantTextBox.Location = new System.Drawing.Point(249, 19);
            this.ConstantTextBox.Name = "ConstantTextBox";
            this.ConstantTextBox.ReadOnly = true;
            this.ConstantTextBox.Size = new System.Drawing.Size(270, 20);
            this.ConstantTextBox.TabIndex = 70;
            // 
            // ConstantLabel
            // 
            this.ConstantLabel.AutoSize = true;
            this.ConstantLabel.Location = new System.Drawing.Point(194, 22);
            this.ConstantLabel.Name = "ConstantLabel";
            this.ConstantLabel.Size = new System.Drawing.Size(49, 13);
            this.ConstantLabel.TabIndex = 69;
            this.ConstantLabel.Text = "Constant";
            this.ConstantLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ArticleIDTextBox
            // 
            this.ArticleIDTextBox.Location = new System.Drawing.Point(62, 19);
            this.ArticleIDTextBox.Name = "ArticleIDTextBox";
            this.ArticleIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.ArticleIDTextBox.TabIndex = 68;
            // 
            // ArticleID
            // 
            this.ArticleID.AutoSize = true;
            this.ArticleID.Location = new System.Drawing.Point(9, 22);
            this.ArticleID.Name = "ArticleID";
            this.ArticleID.Size = new System.Drawing.Size(47, 13);
            this.ArticleID.TabIndex = 67;
            this.ArticleID.Text = "ArticleID";
            this.ArticleID.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // PublishIDTextBox
            // 
            this.PublishIDTextBox.Location = new System.Drawing.Point(513, 8);
            this.PublishIDTextBox.Name = "PublishIDTextBox";
            this.PublishIDTextBox.ReadOnly = true;
            this.PublishIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.PublishIDTextBox.TabIndex = 46;
            // 
            // PublishIDLabel
            // 
            this.PublishIDLabel.AutoSize = true;
            this.PublishIDLabel.Location = new System.Drawing.Point(455, 12);
            this.PublishIDLabel.Name = "PublishIDLabel";
            this.PublishIDLabel.Size = new System.Drawing.Size(52, 13);
            this.PublishIDLabel.TabIndex = 45;
            this.PublishIDLabel.Text = "PublishID";
            this.PublishIDLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // MemberIDTextBox
            // 
            this.MemberIDTextBox.Location = new System.Drawing.Point(514, 38);
            this.MemberIDTextBox.Name = "MemberIDTextBox";
            this.MemberIDTextBox.ReadOnly = true;
            this.MemberIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.MemberIDTextBox.TabIndex = 44;
            // 
            // MemberIDLabel
            // 
            this.MemberIDLabel.AutoSize = true;
            this.MemberIDLabel.Location = new System.Drawing.Point(452, 41);
            this.MemberIDLabel.Name = "MemberIDLabel";
            this.MemberIDLabel.Size = new System.Drawing.Size(56, 13);
            this.MemberIDLabel.TabIndex = 43;
            this.MemberIDLabel.Text = "MemberID";
            this.MemberIDLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // SitesComboBox
            // 
            this.SitesComboBox.Enabled = false;
            this.SitesComboBox.FormattingEnabled = true;
            this.SitesComboBox.Location = new System.Drawing.Point(83, 37);
            this.SitesComboBox.Name = "SitesComboBox";
            this.SitesComboBox.Size = new System.Drawing.Size(180, 21);
            this.SitesComboBox.TabIndex = 42;
            // 
            // SiteLabel
            // 
            this.SiteLabel.AutoSize = true;
            this.SiteLabel.Location = new System.Drawing.Point(52, 40);
            this.SiteLabel.Name = "SiteLabel";
            this.SiteLabel.Size = new System.Drawing.Size(25, 13);
            this.SiteLabel.TabIndex = 41;
            this.SiteLabel.Text = "Site";
            // 
            // SiteArticleIDTextBox
            // 
            this.SiteArticleIDTextBox.Location = new System.Drawing.Point(83, 9);
            this.SiteArticleIDTextBox.Name = "SiteArticleIDTextBox";
            this.SiteArticleIDTextBox.ReadOnly = true;
            this.SiteArticleIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.SiteArticleIDTextBox.TabIndex = 40;
            // 
            // SiteArticleIDLabel
            // 
            this.SiteArticleIDLabel.AutoSize = true;
            this.SiteArticleIDLabel.Location = new System.Drawing.Point(12, 12);
            this.SiteArticleIDLabel.Name = "SiteArticleIDLabel";
            this.SiteArticleIDLabel.Size = new System.Drawing.Size(65, 13);
            this.SiteArticleIDLabel.TabIndex = 39;
            this.SiteArticleIDLabel.Text = "SiteArticleID";
            this.SiteArticleIDLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // FileIDTextBox
            // 
            this.FileIDTextBox.Location = new System.Drawing.Point(83, 489);
            this.FileIDTextBox.Name = "FileIDTextBox";
            this.FileIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.FileIDTextBox.TabIndex = 71;
            // 
            // OrdinalIDTextBox
            // 
            this.OrdinalIDTextBox.Location = new System.Drawing.Point(269, 489);
            this.OrdinalIDTextBox.Name = "OrdinalIDTextBox";
            this.OrdinalIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.OrdinalIDTextBox.TabIndex = 72;
            // 
            // PublishedCheckBox
            // 
            this.PublishedCheckBox.AutoSize = true;
            this.PublishedCheckBox.Location = new System.Drawing.Point(83, 515);
            this.PublishedCheckBox.Name = "PublishedCheckBox";
            this.PublishedCheckBox.Size = new System.Drawing.Size(72, 17);
            this.PublishedCheckBox.TabIndex = 73;
            this.PublishedCheckBox.Text = "Published";
            this.PublishedCheckBox.UseVisualStyleBackColor = true;
            // 
            // FeaturedCheckBox
            // 
            this.FeaturedCheckBox.AutoSize = true;
            this.FeaturedCheckBox.Location = new System.Drawing.Point(191, 515);
            this.FeaturedCheckBox.Name = "FeaturedCheckBox";
            this.FeaturedCheckBox.Size = new System.Drawing.Size(68, 17);
            this.FeaturedCheckBox.TabIndex = 74;
            this.FeaturedCheckBox.Text = "Featured";
            this.FeaturedCheckBox.UseVisualStyleBackColor = true;
            // 
            // LastUpdatedDateTimePicker
            // 
            this.LastUpdatedDateTimePicker.Enabled = false;
            this.LastUpdatedDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.LastUpdatedDateTimePicker.Location = new System.Drawing.Point(83, 538);
            this.LastUpdatedDateTimePicker.Name = "LastUpdatedDateTimePicker";
            this.LastUpdatedDateTimePicker.Size = new System.Drawing.Size(180, 20);
            this.LastUpdatedDateTimePicker.TabIndex = 76;
            this.LastUpdatedDateTimePicker.Value = new System.DateTime(1999, 1, 1, 0, 0, 0, 0);
            // 
            // LastUpdatedLabel
            // 
            this.LastUpdatedLabel.AutoSize = true;
            this.LastUpdatedLabel.Location = new System.Drawing.Point(6, 542);
            this.LastUpdatedLabel.Name = "LastUpdatedLabel";
            this.LastUpdatedLabel.Size = new System.Drawing.Size(71, 13);
            this.LastUpdatedLabel.TabIndex = 75;
            this.LastUpdatedLabel.Text = "Last Updated";
            // 
            // ServiceEnvironmentLabel
            // 
            this.ServiceEnvironmentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ServiceEnvironmentLabel.Location = new System.Drawing.Point(191, 9);
            this.ServiceEnvironmentLabel.Name = "ServiceEnvironmentLabel";
            this.ServiceEnvironmentLabel.Size = new System.Drawing.Size(258, 21);
            this.ServiceEnvironmentLabel.TabIndex = 77;
            this.ServiceEnvironmentLabel.Text = "DEV";
            this.ServiceEnvironmentLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPageTitle
            // 
            this.txtPageTitle.Location = new System.Drawing.Point(83, 140);
            this.txtPageTitle.Name = "txtPageTitle";
            this.txtPageTitle.Size = new System.Drawing.Size(530, 20);
            this.txtPageTitle.TabIndex = 79;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 78;
            this.label1.Text = "Page Title";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 397);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 41);
            this.label2.TabIndex = 81;
            this.label2.Text = "Meta\r\nDescription";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtMetaDescription
            // 
            this.txtMetaDescription.Location = new System.Drawing.Point(83, 397);
            this.txtMetaDescription.Multiline = true;
            this.txtMetaDescription.Name = "txtMetaDescription";
            this.txtMetaDescription.Size = new System.Drawing.Size(531, 86);
            this.txtMetaDescription.TabIndex = 82;
            // 
            // SiteArticleFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 625);
            this.Controls.Add(this.txtMetaDescription);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPageTitle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ServiceEnvironmentLabel);
            this.Controls.Add(this.LastUpdatedDateTimePicker);
            this.Controls.Add(this.LastUpdatedLabel);
            this.Controls.Add(this.FeaturedCheckBox);
            this.Controls.Add(this.PublishedCheckBox);
            this.Controls.Add(this.OrdinalIDTextBox);
            this.Controls.Add(this.FileIDTextBox);
            this.Controls.Add(this.ContentRichTextBox);
            this.Controls.Add(this.CommandsPanel);
            this.Controls.Add(this.OrdinalIDLabel);
            this.Controls.Add(this.FileIDLabel);
            this.Controls.Add(this.ContentLabel);
            this.Controls.Add(this.TitleTextBox);
            this.Controls.Add(this.TitleLabel);
            this.Controls.Add(this.ArticleAttributesGroupBox);
            this.Controls.Add(this.PublishIDTextBox);
            this.Controls.Add(this.PublishIDLabel);
            this.Controls.Add(this.MemberIDTextBox);
            this.Controls.Add(this.MemberIDLabel);
            this.Controls.Add(this.SitesComboBox);
            this.Controls.Add(this.SiteLabel);
            this.Controls.Add(this.SiteArticleIDTextBox);
            this.Controls.Add(this.SiteArticleIDLabel);
            this.Name = "SiteArticleFrm";
            this.Text = "SiteArticle";
            this.Load += new System.EventHandler(this.SiteArticleFrm_Load);
            this.CommandsPanel.ResumeLayout(false);
            this.ArticleAttributesGroupBox.ResumeLayout(false);
            this.ArticleAttributesGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox ContentRichTextBox;
        private System.Windows.Forms.Panel CommandsPanel;
        private System.Windows.Forms.Button SCancelButton;
        private System.Windows.Forms.Button EditArticleButton;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button NextStepButton;
        private System.Windows.Forms.Label OrdinalIDLabel;
        private System.Windows.Forms.Label FileIDLabel;
        private System.Windows.Forms.Label ContentLabel;
        private System.Windows.Forms.TextBox TitleTextBox;
        private System.Windows.Forms.Label TitleLabel;
        private System.Windows.Forms.GroupBox ArticleAttributesGroupBox;
        private System.Windows.Forms.TextBox PublishIDTextBox;
        private System.Windows.Forms.Label PublishIDLabel;
        private System.Windows.Forms.TextBox MemberIDTextBox;
        private System.Windows.Forms.Label MemberIDLabel;
        private System.Windows.Forms.ComboBox SitesComboBox;
        private System.Windows.Forms.Label SiteLabel;
        private System.Windows.Forms.TextBox SiteArticleIDTextBox;
        private System.Windows.Forms.Label SiteArticleIDLabel;
        private System.Windows.Forms.TextBox ConstantTextBox;
        private System.Windows.Forms.Label ConstantLabel;
        private System.Windows.Forms.TextBox ArticleIDTextBox;
        private System.Windows.Forms.Label ArticleID;
        private System.Windows.Forms.TextBox FileIDTextBox;
        private System.Windows.Forms.TextBox OrdinalIDTextBox;
        private System.Windows.Forms.CheckBox PublishedCheckBox;
        private System.Windows.Forms.CheckBox FeaturedCheckBox;
        private System.Windows.Forms.DateTimePicker LastUpdatedDateTimePicker;
        private System.Windows.Forms.Label LastUpdatedLabel;
        private System.Windows.Forms.Label ServiceEnvironmentLabel;
        private System.Windows.Forms.TextBox txtPageTitle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMetaDescription;
    }
}