using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.CompositeUI.WinForms;

using Spark.Admin.Common;

namespace Spark.Admin.ArticleModule
{
    public class ModuleWorkItem : BaseModuleWorkItem
    {
        public const Int32 PrivilegeIDArticleEditor = 36;
        public const Int32 PrivilegeIDArticleApprover = 144;
        public const Int32 PrivilegeIDArticlePublisher = 141;

        private IWorkspace workspace;

        #region Public Methods
        public void Run(IWorkspace workspace)
        {
            this.workspace = workspace;

            ArticlesView articlesView = this.Items.AddNew<ArticlesView>();
            // You must manually assign GlobalState to the ModuleView.
            articlesView.GlobalState = GlobalState;

            this.Items.Add(articlesView);

            // Set the Tab properties.
            TabSmartPartInfo tabSmartPartInfo = new TabSmartPartInfo();
            tabSmartPartInfo.Title = articlesView.Title;

            workspace.Show(articlesView, tabSmartPartInfo);
        }
        #endregion
    }
}
