﻿namespace Spark.Admin.ArticleModule
{
    partial class WSOHelp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WSOHelp));
            this.PictureBoxWSOWorkFlowScreenShot = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxWSOWorkFlowScreenShot)).BeginInit();
            this.SuspendLayout();
            // 
            // PictureBoxWSOWorkFlowScreenShot
            // 
            this.PictureBoxWSOWorkFlowScreenShot.BackColor = System.Drawing.SystemColors.Window;
            this.PictureBoxWSOWorkFlowScreenShot.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.PictureBoxWSOWorkFlowScreenShot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PictureBoxWSOWorkFlowScreenShot.InitialImage = ((System.Drawing.Image)(resources.GetObject("PictureBoxWSOWorkFlowScreenShot.InitialImage")));
            this.PictureBoxWSOWorkFlowScreenShot.Location = new System.Drawing.Point(12, 3);
            this.PictureBoxWSOWorkFlowScreenShot.Name = "PictureBoxWSOWorkFlowScreenShot";
            this.PictureBoxWSOWorkFlowScreenShot.Size = new System.Drawing.Size(766, 511);
            this.PictureBoxWSOWorkFlowScreenShot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.PictureBoxWSOWorkFlowScreenShot.TabIndex = 0;
            this.PictureBoxWSOWorkFlowScreenShot.TabStop = false;
            this.PictureBoxWSOWorkFlowScreenShot.Click += new System.EventHandler(this.PictureBoxWSOWorkFlowScreenShot_Click);
            // 
            // WSOHelp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(789, 529);
            this.Controls.Add(this.PictureBoxWSOWorkFlowScreenShot);
            this.Name = "WSOHelp";
            this.Text = "WSOHelp";
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxWSOWorkFlowScreenShot)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PictureBoxWSOWorkFlowScreenShot;
    }
}