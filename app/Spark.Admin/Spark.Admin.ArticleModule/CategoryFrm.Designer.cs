namespace Spark.Admin.ArticleModule
{
    partial class CategoryFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CommandsPanel = new System.Windows.Forms.Panel();
            this.SCancelButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.CategoryIDTextBox = new System.Windows.Forms.TextBox();
            this.CategoryIDLabel = new System.Windows.Forms.Label();
            this.LastUpdatedDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.LastUpdatedLabel = new System.Windows.Forms.Label();
            this.ConstantLabel = new System.Windows.Forms.Label();
            this.ConstantTextBox = new System.Windows.Forms.TextBox();
            this.ParentCategoryIDTextBox = new System.Windows.Forms.TextBox();
            this.ParentCategoryIDLabel = new System.Windows.Forms.Label();
            this.ListOrderTextBox = new System.Windows.Forms.TextBox();
            this.ListOrderLabel = new System.Windows.Forms.Label();
            this.NextStepButton = new System.Windows.Forms.Button();
            this.ServiceEnvironmentLabel = new System.Windows.Forms.Label();
            this.CommandsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // CommandsPanel
            // 
            this.CommandsPanel.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.CommandsPanel.Controls.Add(this.NextStepButton);
            this.CommandsPanel.Controls.Add(this.SCancelButton);
            this.CommandsPanel.Controls.Add(this.SaveButton);
            this.CommandsPanel.Location = new System.Drawing.Point(10, 139);
            this.CommandsPanel.Name = "CommandsPanel";
            this.CommandsPanel.Size = new System.Drawing.Size(605, 53);
            this.CommandsPanel.TabIndex = 57;
            // 
            // SCancelButton
            // 
            this.SCancelButton.Location = new System.Drawing.Point(504, 16);
            this.SCancelButton.Name = "SCancelButton";
            this.SCancelButton.Size = new System.Drawing.Size(92, 23);
            this.SCancelButton.TabIndex = 35;
            this.SCancelButton.Text = "Cancel";
            this.SCancelButton.UseVisualStyleBackColor = true;
            this.SCancelButton.Click += new System.EventHandler(this.SCancelButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(406, 16);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(92, 23);
            this.SaveButton.TabIndex = 33;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // CategoryIDTextBox
            // 
            this.CategoryIDTextBox.Location = new System.Drawing.Point(100, 8);
            this.CategoryIDTextBox.Name = "CategoryIDTextBox";
            this.CategoryIDTextBox.ReadOnly = true;
            this.CategoryIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.CategoryIDTextBox.TabIndex = 46;
            // 
            // CategoryIDLabel
            // 
            this.CategoryIDLabel.AutoSize = true;
            this.CategoryIDLabel.Location = new System.Drawing.Point(38, 11);
            this.CategoryIDLabel.Name = "CategoryIDLabel";
            this.CategoryIDLabel.Size = new System.Drawing.Size(60, 13);
            this.CategoryIDLabel.TabIndex = 45;
            this.CategoryIDLabel.Text = "CategoryID";
            this.CategoryIDLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // LastUpdatedDateTimePicker
            // 
            this.LastUpdatedDateTimePicker.Enabled = false;
            this.LastUpdatedDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.LastUpdatedDateTimePicker.Location = new System.Drawing.Point(100, 113);
            this.LastUpdatedDateTimePicker.Name = "LastUpdatedDateTimePicker";
            this.LastUpdatedDateTimePicker.Size = new System.Drawing.Size(180, 20);
            this.LastUpdatedDateTimePicker.TabIndex = 76;
            this.LastUpdatedDateTimePicker.Value = new System.DateTime(1999, 1, 1, 0, 0, 0, 0);
            // 
            // LastUpdatedLabel
            // 
            this.LastUpdatedLabel.AutoSize = true;
            this.LastUpdatedLabel.Location = new System.Drawing.Point(27, 117);
            this.LastUpdatedLabel.Name = "LastUpdatedLabel";
            this.LastUpdatedLabel.Size = new System.Drawing.Size(71, 13);
            this.LastUpdatedLabel.TabIndex = 75;
            this.LastUpdatedLabel.Text = "Last Updated";
            // 
            // ConstantLabel
            // 
            this.ConstantLabel.AutoSize = true;
            this.ConstantLabel.Location = new System.Drawing.Point(49, 90);
            this.ConstantLabel.Name = "ConstantLabel";
            this.ConstantLabel.Size = new System.Drawing.Size(49, 13);
            this.ConstantLabel.TabIndex = 52;
            this.ConstantLabel.Text = "Constant";
            this.ConstantLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ConstantTextBox
            // 
            this.ConstantTextBox.Location = new System.Drawing.Point(100, 87);
            this.ConstantTextBox.Name = "ConstantTextBox";
            this.ConstantTextBox.Size = new System.Drawing.Size(516, 20);
            this.ConstantTextBox.TabIndex = 53;
            this.ConstantTextBox.TextChanged += new System.EventHandler(this.ConstantTextBox_TextChanged);
            // 
            // ParentCategoryIDTextBox
            // 
            this.ParentCategoryIDTextBox.Location = new System.Drawing.Point(100, 35);
            this.ParentCategoryIDTextBox.Name = "ParentCategoryIDTextBox";
            this.ParentCategoryIDTextBox.ReadOnly = true;
            this.ParentCategoryIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.ParentCategoryIDTextBox.TabIndex = 78;
            // 
            // ParentCategoryIDLabel
            // 
            this.ParentCategoryIDLabel.AutoSize = true;
            this.ParentCategoryIDLabel.Location = new System.Drawing.Point(7, 38);
            this.ParentCategoryIDLabel.Name = "ParentCategoryIDLabel";
            this.ParentCategoryIDLabel.Size = new System.Drawing.Size(91, 13);
            this.ParentCategoryIDLabel.TabIndex = 77;
            this.ParentCategoryIDLabel.Text = "ParentCategoryID";
            this.ParentCategoryIDLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ListOrderTextBox
            // 
            this.ListOrderTextBox.Location = new System.Drawing.Point(100, 61);
            this.ListOrderTextBox.Name = "ListOrderTextBox";
            this.ListOrderTextBox.Size = new System.Drawing.Size(100, 20);
            this.ListOrderTextBox.TabIndex = 80;
            // 
            // ListOrderLabel
            // 
            this.ListOrderLabel.AutoSize = true;
            this.ListOrderLabel.Location = new System.Drawing.Point(46, 64);
            this.ListOrderLabel.Name = "ListOrderLabel";
            this.ListOrderLabel.Size = new System.Drawing.Size(52, 13);
            this.ListOrderLabel.TabIndex = 79;
            this.ListOrderLabel.Text = "List Order";
            this.ListOrderLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // NextStepButton
            // 
            this.NextStepButton.Location = new System.Drawing.Point(12, 9);
            this.NextStepButton.Name = "NextStepButton";
            this.NextStepButton.Size = new System.Drawing.Size(92, 36);
            this.NextStepButton.TabIndex = 37;
            this.NextStepButton.Text = "NextStep";
            this.NextStepButton.UseVisualStyleBackColor = true;
            this.NextStepButton.Click += new System.EventHandler(this.NextStepButton_Click);
            // 
            // ServiceEnvironmentLabel
            // 
            this.ServiceEnvironmentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ServiceEnvironmentLabel.Location = new System.Drawing.Point(250, 7);
            this.ServiceEnvironmentLabel.Name = "ServiceEnvironmentLabel";
            this.ServiceEnvironmentLabel.Size = new System.Drawing.Size(258, 21);
            this.ServiceEnvironmentLabel.TabIndex = 81;
            this.ServiceEnvironmentLabel.Text = "DEV";
            this.ServiceEnvironmentLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CategoryFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 202);
            this.Controls.Add(this.ServiceEnvironmentLabel);
            this.Controls.Add(this.ListOrderTextBox);
            this.Controls.Add(this.ListOrderLabel);
            this.Controls.Add(this.ParentCategoryIDTextBox);
            this.Controls.Add(this.ParentCategoryIDLabel);
            this.Controls.Add(this.LastUpdatedDateTimePicker);
            this.Controls.Add(this.LastUpdatedLabel);
            this.Controls.Add(this.CommandsPanel);
            this.Controls.Add(this.ConstantTextBox);
            this.Controls.Add(this.ConstantLabel);
            this.Controls.Add(this.CategoryIDTextBox);
            this.Controls.Add(this.CategoryIDLabel);
            this.Name = "CategoryFrm";
            this.Text = "Category";
            this.Load += new System.EventHandler(this.CategoryFrm_Load);
            this.CommandsPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel CommandsPanel;
        private System.Windows.Forms.Button SCancelButton;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.TextBox CategoryIDTextBox;
        private System.Windows.Forms.Label CategoryIDLabel;
        private System.Windows.Forms.DateTimePicker LastUpdatedDateTimePicker;
        private System.Windows.Forms.Label LastUpdatedLabel;
        private System.Windows.Forms.Label ConstantLabel;
        private System.Windows.Forms.TextBox ConstantTextBox;
        private System.Windows.Forms.TextBox ParentCategoryIDTextBox;
        private System.Windows.Forms.Label ParentCategoryIDLabel;
        private System.Windows.Forms.TextBox ListOrderTextBox;
        private System.Windows.Forms.Label ListOrderLabel;
        private System.Windows.Forms.Button NextStepButton;
        private System.Windows.Forms.Label ServiceEnvironmentLabel;
    }
}