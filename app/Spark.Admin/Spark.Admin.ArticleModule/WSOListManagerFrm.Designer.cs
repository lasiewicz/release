﻿namespace Spark.Admin.ArticleModule
{
    partial class WSOListManagerFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WSOListManagerFrm));
            this.DataGridViewWSOList = new System.Windows.Forms.DataGridView();
            this.ButtonCreateNewList = new System.Windows.Forms.Button();
            this.ComboBoxDayFilter = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LabelEnvironment = new System.Windows.Forms.Label();
            this.ButtonEdit = new System.Windows.Forms.Button();
            this.ButtonRefresh = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewWSOList)).BeginInit();
            this.SuspendLayout();
            // 
            // DataGridViewWSOList
            // 
            this.DataGridViewWSOList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewWSOList.Location = new System.Drawing.Point(12, 60);
            this.DataGridViewWSOList.Name = "DataGridViewWSOList";
            this.DataGridViewWSOList.RowTemplate.Height = 24;
            this.DataGridViewWSOList.Size = new System.Drawing.Size(581, 413);
            this.DataGridViewWSOList.TabIndex = 0;
            this.DataGridViewWSOList.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridViewWSOList_RowHeaderMouseDoubleClick);
            // 
            // ButtonCreateNewList
            // 
            this.ButtonCreateNewList.Location = new System.Drawing.Point(58, 485);
            this.ButtonCreateNewList.Name = "ButtonCreateNewList";
            this.ButtonCreateNewList.Size = new System.Drawing.Size(122, 23);
            this.ButtonCreateNewList.TabIndex = 1;
            this.ButtonCreateNewList.Text = "Create New List";
            this.ButtonCreateNewList.UseVisualStyleBackColor = true;
            this.ButtonCreateNewList.Click += new System.EventHandler(this.ButtonCreateNewList_Click);
            // 
            // ComboBoxDayFilter
            // 
            this.ComboBoxDayFilter.FormattingEnabled = true;
            this.ComboBoxDayFilter.Items.AddRange(new object[] {
            "10",
            "20",
            "30",
            "40",
            "50",
            "60",
            "90",
            "120",
            "240",
            "360",
            "480",
            "600",
            "720",
            "840",
            "960",
            "1080",
            "1200"});
            this.ComboBoxDayFilter.Location = new System.Drawing.Point(134, 19);
            this.ComboBoxDayFilter.Name = "ComboBoxDayFilter";
            this.ComboBoxDayFilter.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxDayFilter.TabIndex = 3;
            this.ComboBoxDayFilter.SelectedIndexChanged += new System.EventHandler(this.ComboBoxDayFilter_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Filter by created date:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(261, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "days ago";
            // 
            // LabelEnvironment
            // 
            this.LabelEnvironment.AutoSize = true;
            this.LabelEnvironment.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelEnvironment.Location = new System.Drawing.Point(452, 19);
            this.LabelEnvironment.Name = "LabelEnvironment";
            this.LabelEnvironment.Size = new System.Drawing.Size(141, 21);
            this.LabelEnvironment.TabIndex = 6;
            this.LabelEnvironment.Text = "LabelEnvironment";
            this.LabelEnvironment.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ButtonEdit
            // 
            this.ButtonEdit.Location = new System.Drawing.Point(226, 485);
            this.ButtonEdit.Name = "ButtonEdit";
            this.ButtonEdit.Size = new System.Drawing.Size(115, 23);
            this.ButtonEdit.TabIndex = 7;
            this.ButtonEdit.Text = "Edit Selected";
            this.ButtonEdit.UseVisualStyleBackColor = true;
            this.ButtonEdit.Click += new System.EventHandler(this.ButtonEdit_Click);
            // 
            // ButtonRefresh
            // 
            this.ButtonRefresh.Location = new System.Drawing.Point(398, 485);
            this.ButtonRefresh.Name = "ButtonRefresh";
            this.ButtonRefresh.Size = new System.Drawing.Size(75, 23);
            this.ButtonRefresh.TabIndex = 8;
            this.ButtonRefresh.Text = "Refresh";
            this.ButtonRefresh.UseVisualStyleBackColor = true;
            this.ButtonRefresh.Click += new System.EventHandler(this.ButtonRefresh_Click);
            // 
            // WSOListManagerFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 520);
            this.Controls.Add(this.ButtonRefresh);
            this.Controls.Add(this.ButtonEdit);
            this.Controls.Add(this.LabelEnvironment);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ComboBoxDayFilter);
            this.Controls.Add(this.ButtonCreateNewList);
            this.Controls.Add(this.DataGridViewWSOList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WSOListManagerFrm";
            this.Text = "WSO List Manager";
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewWSOList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridViewWSOList;
        private System.Windows.Forms.Button ButtonCreateNewList;
        private System.Windows.Forms.ComboBox ComboBoxDayFilter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LabelEnvironment;
        private System.Windows.Forms.Button ButtonEdit;
        private System.Windows.Forms.Button ButtonRefresh;
    }
}