using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI;

using Spark.Admin.Common;

using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.Article;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Spark.Admin.ArticleModule
{
    public partial class SiteArticleFrm : Spark.Admin.Common.Controls.Form
    {
        private SiteArticle siteArticle;
        private ArticlesController articlesController;
        private Int32 nextPublishActionPublishObjectID = Matchnet.Constants.NULL_INT;
        private String nextStepText = String.Empty;

        #region Publish Constants
        public const Int32 PubActPubObjIDSiteArticleAdd = 300;
        public const Int32 PubActPubObjIDSiteArticleEdit = 301;
        public const Int32 PubActPubObjIDSiteArticleDelete = 302;
        public const Int32 PubActPubObjIDSiteArticlePublishToContentStg = 303;
        public const Int32 PubActPubObjIDSiteArticleApproveInContentStg = 304;
        public const Int32 PubActPubObjIDSiteArticleVerifyInContentStg = 305;
        public const Int32 PubActPubObjIDSiteArticlePublishToProd = 306;
        public const Int32 PubActPubObjIDSiteArticleVerifyInProd = 307;

        private const String PubActPubObjTextSiteArticleApproveInContentStg = "Approve in ContentStg";
        private const String PubActPubObjTextSiteArticleVerifyInContentStg = "Verify in ContentStg";
        private const String PubActPubObjTextSiteArticlePublishToContentStg = "Publish to ContentStg";
        private const String PubActPubObjTextSiteArticlePublishToProd = "Publish to Prod";
        private const String PubActPubObjTextSiteArticleVerifyInProd = "Verify in Prod";
        #endregion

        #region Message Constants
        private const String MsgSaveSiteArticleConfirm = "Are you sure you want to save this SiteArticle?";
        private const String MsgNextStepSiteArticleConfirm = "Are you sure you want to {0} this SiteArticle?";
        #endregion

        public SiteArticleFrm(State globalState, ArticlesController articlesController, SiteArticle siteArticle) : base(globalState)
        {
            InitializeComponent();

            this.siteArticle = siteArticle;
            this.articlesController = articlesController;

            // Set up Sites ComboBox.
            SitesComboBox.DataSource = ((Sites)GlobalState[Constants.StateSites]);
            SitesComboBox.ValueMember = "SiteID";
            SitesComboBox.DisplayMember = "Name";

            ServiceEnvironmentLabel.Text = articlesController.CurrentServiceEnvironmentTypeEnum.ToString().ToUpper();

            // Display/Hide Save and Delete buttons.
            if (!HasPrivilege(ModuleWorkItem.PrivilegeIDArticleEditor))
            {
                //EditArticleButton.Visible = false;
                //SaveButton.Visible = false;
            }
            else if (articlesController.CurrentServiceEnvironmentTypeEnum != ServiceEnvironmentTypeEnum.Dev)
            {
                SaveButton.Visible = false;
            }

            InitializeNextStep();
            //Hiding Edit Article button - editing which affects all the site articles.
            EditArticleButton.Visible = false;

        }

        #region Private Methods
        private void InitializeNextStep()
        {
            NextStepButton.Visible = false;

            // If this is a new siteArticle, there will be no next step.
            if (siteArticle.SiteArticleID != Matchnet.Constants.NULL_INT && siteArticle.SiteArticleID != Int32.MinValue)
            {
                ServiceEnvironmentTypeEnum serviceEnvrionmentTypeEnum;
                // Get information about the next Publish step.
                nextPublishActionPublishObjectID = CommonController.GetNextPublishActionPublishObjectStep(siteArticle.SiteArticleID, PublishObjectType.SiteArticle, out serviceEnvrionmentTypeEnum);

                // Display/Hide NextStepButton.
                if (siteArticle.SiteArticleID > 0 && nextPublishActionPublishObjectID != Matchnet.Constants.NULL_INT && (articlesController.CurrentServiceEnvironmentTypeEnum == serviceEnvrionmentTypeEnum))
                {
                    switch (nextPublishActionPublishObjectID)
                    {
                        case PubActPubObjIDSiteArticlePublishToContentStg:
                            NextStepButton.Visible = true;
                            nextStepText = PubActPubObjTextSiteArticlePublishToContentStg;

                            break;

                        case PubActPubObjIDSiteArticleVerifyInContentStg:
                            NextStepButton.Visible = true;
                            nextStepText = PubActPubObjTextSiteArticleVerifyInContentStg;

                            break;

                        case PubActPubObjIDSiteArticleApproveInContentStg:
                            if (HasPrivilege(ModuleWorkItem.PrivilegeIDArticleApprover))
                            {
                                NextStepButton.Visible = true;
                                nextStepText = PubActPubObjTextSiteArticleApproveInContentStg;
                            }

                            break;

                        case PubActPubObjIDSiteArticlePublishToProd:
                            if (HasPrivilege(ModuleWorkItem.PrivilegeIDArticlePublisher))
                            {
                                NextStepButton.Visible = true;
                                nextStepText = PubActPubObjTextSiteArticlePublishToProd;
                            }

                            break;

                        case PubActPubObjIDSiteArticleVerifyInProd:
                            NextStepButton.Visible = true;
                            nextStepText = PubActPubObjTextSiteArticleVerifyInProd;

                            break;

                        default:
                            NextStepButton.Visible = false;

                            break;
                    }

                    NextStepButton.Text = nextStepText;
                }
            }
        }

        private void SiteArticleFrm_Load(object sender, System.EventArgs e)
        {
            DisplaySiteArticle();
        }

        private void DisplaySiteArticle()
        {
            if (siteArticle.SiteArticleID > 0)
                SiteArticleIDTextBox.Text = siteArticle.SiteArticleID.ToString();
            else
                SiteArticleIDTextBox.Text = Matchnet.Constants.NULL_INT.ToString();
           
            for (int i = 0; i < SitesComboBox.Items.Count; i++)
            {
                if (((Site)SitesComboBox.Items[i]).SiteID == siteArticle.SiteID)
                {
                    SitesComboBox.SelectedIndex = i;
                    break;
                }
            }

            MemberIDTextBox.Text = siteArticle.MemberID.ToString();
            ArticleIDTextBox.Text = siteArticle.ArticleID.ToString();
            ConstantTextBox.Text = siteArticle.Constant;
            TitleTextBox.Text = siteArticle.Title;
            ContentRichTextBox.Text = siteArticle.Content;
            FileIDTextBox.Text = siteArticle.FileID.ToString();
            txtPageTitle.Text = siteArticle.PageTitle;
            txtMetaDescription.Text = siteArticle.MetaDescription;


            if (siteArticle.Ordinal < 0)
                siteArticle.Ordinal = 0;
            OrdinalIDTextBox.Text = siteArticle.Ordinal.ToString();

            if (siteArticle.PublishedFlag)
                PublishedCheckBox.Checked = true;
            else
                PublishedCheckBox.Checked = false;

            if (siteArticle.FeaturedFlag)
                FeaturedCheckBox.Checked = true;
            else
                FeaturedCheckBox.Checked = false;

            LastUpdatedDateTimePicker.Value = siteArticle.LastUpdated;
            PublishIDTextBox.Text = siteArticle.PublishID.ToString();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            siteArticle = BuildSiteArticle();

            // Are you sure?
            if (MessageBox.Show(MsgSaveSiteArticleConfirm, Constants.MsgBoxCaptionConfirm, MessageBoxButtons.YesNo) == DialogResult.No)
                return;
            else
            {
                try
                {
                    // Save.
                    articlesController.SaveSiteArticle(siteArticle, MemberID, false, ServiceEnvironmentTypeEnum.Dev);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format(Constants.MsgContactAdministrator, ex.Message), Constants.MsgBoxCaptionSystemError);
                    return;
                }

                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private SiteArticle BuildSiteArticle()
        {
            SiteArticle siteArticle = new SiteArticle();

            siteArticle.SiteArticleID = Convert.ToInt32(SiteArticleIDTextBox.Text.Trim());
            siteArticle.ArticleID = Convert.ToInt32(ArticleIDTextBox.Text.Trim());
            siteArticle.SiteID = Convert.ToInt32(SitesComboBox.SelectedValue);
            siteArticle.MemberID = Convert.ToInt32(MemberIDTextBox.Text.Trim());
            siteArticle.Title = TitleTextBox.Text.Trim();
            siteArticle.Content = ContentRichTextBox.Text.Trim();
            siteArticle.FileID = Convert.ToInt32(FileIDTextBox.Text.Trim());
            siteArticle.Ordinal = Convert.ToInt32(OrdinalIDTextBox.Text.Trim()); 
            siteArticle.PublishedFlag = PublishedCheckBox.Checked;
            siteArticle.FeaturedFlag = FeaturedCheckBox.Checked;
            siteArticle.PublishID = Convert.ToInt32(PublishIDTextBox.Text.Trim());
            siteArticle.PageTitle = txtPageTitle.Text.Trim();
            siteArticle.MetaDescription = txtMetaDescription.Text.Trim();

            // This is to make sure that SiteArticleCollection VO cache gets invalidated by CategoryID as well.
            Article article = articlesController.RetrieveArticle(siteArticle.ArticleID, articlesController.CurrentServiceEnvironmentTypeEnum);
            siteArticle.ArticleData.CategoryID = article.CategoryID;

            return siteArticle;
        }

        private void SCancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void NextStepButton_Click(object sender, EventArgs e)
        {
            // Are you sure?
            if (MessageBox.Show(String.Format(MsgNextStepSiteArticleConfirm, NextStepButton.Text), Constants.MsgBoxCaptionConfirm, MessageBoxButtons.YesNo) == DialogResult.No)
                return;
            else
            {
                try
                {
                    // Do the correct action.
                    switch (nextPublishActionPublishObjectID)
                    {
                        case PubActPubObjIDSiteArticlePublishToContentStg:
                            try
                            {
                                articlesController.RetrieveArticle(siteArticle.ArticleID, ServiceEnvironmentTypeEnum.ContentStg);
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message.IndexOf("There is no row at position 0") != -1)
                                {
                                    MessageBox.Show("You cannot publish a SiteArticle to ContentStg if its associated Article does not exist on ContentStg. Please publish ArticleID "
                                        + siteArticle.ArticleID.ToString() + " to ContentStg before attempting to publish this.", 
                                        Constants.MsgBoxCaptionWarning);
                                    return;
                                }
                                else
                                {
                                    throw ex;
                                }
                            }

                            articlesController.SaveSiteArticle(siteArticle, MemberID, true, ServiceEnvironmentTypeEnum.ContentStg);
                            CommonController.SavePublishAction(siteArticle.PublishID, PubActPubObjIDSiteArticlePublishToContentStg, MemberID);

                            break;

                        case PubActPubObjIDSiteArticlePublishToProd:
                            try
                            {
                                articlesController.RetrieveArticle(siteArticle.ArticleID, ServiceEnvironmentTypeEnum.ContentStg);
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message.IndexOf("There is no row at position 0") != -1)
                                {
                                    MessageBox.Show("You cannot publish a SiteArticle to Prod if its associated Article does not exist on Prod.  Please publish ArticleID "
                                        + siteArticle.ArticleID.ToString() + " to Prod before attempting to publish this.", 
                                        Constants.MsgBoxCaptionWarning);
                                    return;
                                }
                                else
                                {
                                    throw ex;
                                }
                            }

                            articlesController.SaveSiteArticle(siteArticle, MemberID, true, ServiceEnvironmentTypeEnum.Prod);
                            CommonController.SavePublishAction(siteArticle.PublishID, PubActPubObjIDSiteArticlePublishToProd, MemberID);

                            Helper.SendEmail(Constants.EmailsToAlert, Helper.EmailType.DeploymentToProd
                                , "SiteArticle", MemberEmail, BuildEmailBody());

                            break;

                        // All Verifies are handled here.
                        default:
                            CommonController.SavePublishAction(siteArticle.PublishID, nextPublishActionPublishObjectID, MemberID);

                            break;
                    }

                    DialogResult = DialogResult.OK;
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format(Constants.MsgContactAdministrator, ex.Message), Constants.MsgBoxCaptionSystemError);
                    return;
                }
            }
        }

        private String BuildEmailBody()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append("SiteArticleID: ").Append(siteArticle.SiteArticleID.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("SiteID: ").Append(siteArticle.SiteID.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("ArticleID: ").Append(siteArticle.ArticleID.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("Title: ").Append(siteArticle.Title.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("Content: ").Append(siteArticle.Content.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("FileID: ").Append(siteArticle.FileID.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("Ordinal: ").Append(siteArticle.Ordinal.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("PublishID: ").Append(siteArticle.PublishID.ToString()).Append(Environment.NewLine);
            
            return stringBuilder.ToString();
        }

        private void EditArticleButton_Click(object sender, EventArgs e)
        {
            ArticleFrm articleFrm = new ArticleFrm(GlobalState, articlesController, siteArticle.ArticleID);

            if (articleFrm.ShowDialog() == DialogResult.OK)
            {
                this.DialogResult = DialogResult.OK;
            }
        }
        #endregion
    }
}