﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI;

using Matchnet.Content.ValueObjects.Article;

namespace Spark.Admin.ArticleModule
{
    public partial class WSOListManagerFrm: Spark.Admin.Common.Controls.Form
    {
        private ArticlesController articlesController;
        private State globalState;
        private WSOListCollection filteredWSOListCollection;
        private WSOListPublisherFrm wsoListPublisherFrm;
        
        public WSOListManagerFrm(State globalState, ArticlesController articlesController) : base(globalState)
        {
            InitializeComponent();

            this.globalState = globalState;
            this.articlesController = articlesController;

            LabelEnvironment.Text = articlesController.CurrentServiceEnvironmentTypeEnum.ToString();
        }

        private void ButtonCreateNewList_Click(object sender, EventArgs e)
        {
            WSOListCreateFrm wsoListCreateFrm = new WSOListCreateFrm(globalState, articlesController);

            wsoListCreateFrm.Show();
        }

        private void ComboBoxDayFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateDataGridViewWSOList();
        }
        
        // Render WSO lists based on the number of days.
        public void PopulateDataGridViewWSOList()
        {
            WSOListCollection wsoListCollection = articlesController.RetrieveWSOListCollection();
            filteredWSOListCollection = new WSOListCollection();

            int days = Convert.ToInt16(ComboBoxDayFilter.SelectedItem);

            foreach (WSOList wsoList in wsoListCollection)
            {
                if (wsoList.InsertDateTime >= (DateTime.Now.AddDays(-days)))
                {
                    filteredWSOListCollection.Add(wsoList);
                }
            }

            DataGridViewWSOList.DataSource = filteredWSOListCollection;

            this.Refresh();
            this.Focus();
        }

        void DataGridViewWSOList_RowHeaderMouseDoubleClick(object sender, System.Windows.Forms.DataGridViewCellMouseEventArgs e)
        {
            if (DataGridViewWSOList.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select an entire row.");

                return;
            }

            WSOList selectedWSOList = (WSOList)filteredWSOListCollection[DataGridViewWSOList.SelectedRows[0].Index];

            if (wsoListPublisherFrm == null || wsoListPublisherFrm.Visible == false)
            {
                wsoListPublisherFrm = new WSOListPublisherFrm(selectedWSOList, globalState, articlesController);
            }
            else
            {
                wsoListPublisherFrm.Dispose();
                wsoListPublisherFrm = new WSOListPublisherFrm(selectedWSOList, globalState, articlesController);
            }

            wsoListPublisherFrm.Show();
            wsoListPublisherFrm.Focus();
        }

        private void ButtonEdit_Click(object sender, EventArgs e)
        {
            if (DataGridViewWSOList.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select an entire row.");

                return;
            }

            WSOList selectedWSOList = (WSOList)filteredWSOListCollection[DataGridViewWSOList.SelectedRows[0].Index];

            WSOListEditFrm wsoListEditFrm = new WSOListEditFrm(globalState, articlesController, selectedWSOList, this);

            wsoListEditFrm.Show();
        }

        private void ButtonRefresh_Click(object sender, EventArgs e)
        {
            PopulateDataGridViewWSOList();
        }
    }
}
