﻿namespace Spark.Admin.ArticleModule
{
    partial class WSOListEditFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WSOListEditFrm));
            this.ButtonSave = new System.Windows.Forms.Button();
            this.LabelNote = new System.Windows.Forms.Label();
            this.LabelName = new System.Windows.Forms.Label();
            this.TextBoxNote = new System.Windows.Forms.TextBox();
            this.TextBoxName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // ButtonSave
            // 
            this.ButtonSave.Location = new System.Drawing.Point(162, 310);
            this.ButtonSave.Name = "ButtonSave";
            this.ButtonSave.Size = new System.Drawing.Size(75, 23);
            this.ButtonSave.TabIndex = 9;
            this.ButtonSave.Text = "Save";
            this.ButtonSave.UseVisualStyleBackColor = true;
            this.ButtonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // LabelNote
            // 
            this.LabelNote.AutoSize = true;
            this.LabelNote.Location = new System.Drawing.Point(15, 59);
            this.LabelNote.Name = "LabelNote";
            this.LabelNote.Size = new System.Drawing.Size(34, 13);
            this.LabelNote.TabIndex = 8;
            this.LabelNote.Text = "Note:";
            // 
            // LabelName
            // 
            this.LabelName.AutoSize = true;
            this.LabelName.Location = new System.Drawing.Point(12, 15);
            this.LabelName.Name = "LabelName";
            this.LabelName.Size = new System.Drawing.Size(39, 13);
            this.LabelName.TabIndex = 7;
            this.LabelName.Text = "Name:";
            // 
            // TextBoxNote
            // 
            this.TextBoxNote.Location = new System.Drawing.Point(57, 56);
            this.TextBoxNote.Multiline = true;
            this.TextBoxNote.Name = "TextBoxNote";
            this.TextBoxNote.Size = new System.Drawing.Size(295, 248);
            this.TextBoxNote.TabIndex = 6;
            // 
            // TextBoxName
            // 
            this.TextBoxName.Location = new System.Drawing.Point(57, 12);
            this.TextBoxName.Name = "TextBoxName";
            this.TextBoxName.Size = new System.Drawing.Size(295, 22);
            this.TextBoxName.TabIndex = 5;
            // 
            // WSOListEditFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 345);
            this.Controls.Add(this.ButtonSave);
            this.Controls.Add(this.LabelNote);
            this.Controls.Add(this.LabelName);
            this.Controls.Add(this.TextBoxNote);
            this.Controls.Add(this.TextBoxName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WSOListEditFrm";
            this.Text = "WSO Edit List";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonSave;
        private System.Windows.Forms.Label LabelNote;
        private System.Windows.Forms.Label LabelName;
        private System.Windows.Forms.TextBox TextBoxNote;
        private System.Windows.Forms.TextBox TextBoxName;
    }
}