namespace Spark.Admin.ArticleModule
{
    partial class SiteCategoryFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ContentRichTextBox = new System.Windows.Forms.RichTextBox();
            this.CommandsPanel = new System.Windows.Forms.Panel();
            this.SCancelButton = new System.Windows.Forms.Button();
            this.EditCategoryButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.NextStepButton = new System.Windows.Forms.Button();
            this.ContentLabel = new System.Windows.Forms.Label();
            this.ParentCategoryIDTextBox = new System.Windows.Forms.TextBox();
            this.ParentCategoryIDLabel = new System.Windows.Forms.Label();
            this.CategoryAttributesGroupBox = new System.Windows.Forms.GroupBox();
            this.ConstantTextBox = new System.Windows.Forms.TextBox();
            this.ConstantLabel = new System.Windows.Forms.Label();
            this.CategoryIDTextBox = new System.Windows.Forms.TextBox();
            this.CategoryID = new System.Windows.Forms.Label();
            this.PublishIDTextBox = new System.Windows.Forms.TextBox();
            this.PublishIDLabel = new System.Windows.Forms.Label();
            this.SitesComboBox = new System.Windows.Forms.ComboBox();
            this.SiteLabel = new System.Windows.Forms.Label();
            this.SiteCategoryIDTextBox = new System.Windows.Forms.TextBox();
            this.SiteCategoryIDLabel = new System.Windows.Forms.Label();
            this.PublishedCheckBox = new System.Windows.Forms.CheckBox();
            this.LastUpdatedDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.LastUpdatedLabel = new System.Windows.Forms.Label();
            this.CommandsPanel.SuspendLayout();
            this.CategoryAttributesGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // ContentRichTextBox
            // 
            this.ContentRichTextBox.Location = new System.Drawing.Point(83, 172);
            this.ContentRichTextBox.Name = "ContentRichTextBox";
            this.ContentRichTextBox.Size = new System.Drawing.Size(530, 63);
            this.ContentRichTextBox.TabIndex = 64;
            this.ContentRichTextBox.Text = "";
            // 
            // CommandsPanel
            // 
            this.CommandsPanel.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.CommandsPanel.Controls.Add(this.SCancelButton);
            this.CommandsPanel.Controls.Add(this.EditCategoryButton);
            this.CommandsPanel.Controls.Add(this.SaveButton);
            this.CommandsPanel.Controls.Add(this.NextStepButton);
            this.CommandsPanel.Location = new System.Drawing.Point(9, 292);
            this.CommandsPanel.Name = "CommandsPanel";
            this.CommandsPanel.Size = new System.Drawing.Size(605, 53);
            this.CommandsPanel.TabIndex = 57;
            // 
            // SCancelButton
            // 
            this.SCancelButton.Location = new System.Drawing.Point(504, 16);
            this.SCancelButton.Name = "SCancelButton";
            this.SCancelButton.Size = new System.Drawing.Size(92, 23);
            this.SCancelButton.TabIndex = 35;
            this.SCancelButton.Text = "Cancel";
            this.SCancelButton.UseVisualStyleBackColor = true;
            this.SCancelButton.Click += new System.EventHandler(this.SCancelButton_Click);
            // 
            // EditCategoryButton
            // 
            this.EditCategoryButton.Location = new System.Drawing.Point(260, 16);
            this.EditCategoryButton.Name = "EditCategoryButton";
            this.EditCategoryButton.Size = new System.Drawing.Size(92, 23);
            this.EditCategoryButton.TabIndex = 34;
            this.EditCategoryButton.Text = "Edit Category";
            this.EditCategoryButton.UseVisualStyleBackColor = true;
            this.EditCategoryButton.Click += new System.EventHandler(this.EditCategoryButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(406, 16);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(92, 23);
            this.SaveButton.TabIndex = 33;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // NextStepButton
            // 
            this.NextStepButton.Location = new System.Drawing.Point(8, 9);
            this.NextStepButton.Name = "NextStepButton";
            this.NextStepButton.Size = new System.Drawing.Size(92, 36);
            this.NextStepButton.TabIndex = 30;
            this.NextStepButton.Text = "NextStep";
            this.NextStepButton.UseVisualStyleBackColor = true;
            this.NextStepButton.Click += new System.EventHandler(this.NextStepButton_Click);
            // 
            // ContentLabel
            // 
            this.ContentLabel.AutoSize = true;
            this.ContentLabel.Location = new System.Drawing.Point(33, 172);
            this.ContentLabel.Name = "ContentLabel";
            this.ContentLabel.Size = new System.Drawing.Size(44, 13);
            this.ContentLabel.TabIndex = 54;
            this.ContentLabel.Text = "Content";
            this.ContentLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ParentCategoryIDTextBox
            // 
            this.ParentCategoryIDTextBox.Location = new System.Drawing.Point(112, 45);
            this.ParentCategoryIDTextBox.Name = "ParentCategoryIDTextBox";
            this.ParentCategoryIDTextBox.ReadOnly = true;
            this.ParentCategoryIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.ParentCategoryIDTextBox.TabIndex = 53;
            // 
            // ParentCategoryIDLabel
            // 
            this.ParentCategoryIDLabel.AutoSize = true;
            this.ParentCategoryIDLabel.Location = new System.Drawing.Point(12, 48);
            this.ParentCategoryIDLabel.Name = "ParentCategoryIDLabel";
            this.ParentCategoryIDLabel.Size = new System.Drawing.Size(94, 13);
            this.ParentCategoryIDLabel.TabIndex = 52;
            this.ParentCategoryIDLabel.Text = "Parent CategoryID";
            this.ParentCategoryIDLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // CategoryAttributesGroupBox
            // 
            this.CategoryAttributesGroupBox.Controls.Add(this.ConstantTextBox);
            this.CategoryAttributesGroupBox.Controls.Add(this.ConstantLabel);
            this.CategoryAttributesGroupBox.Controls.Add(this.CategoryIDTextBox);
            this.CategoryAttributesGroupBox.Controls.Add(this.CategoryID);
            this.CategoryAttributesGroupBox.Controls.Add(this.ParentCategoryIDTextBox);
            this.CategoryAttributesGroupBox.Controls.Add(this.ParentCategoryIDLabel);
            this.CategoryAttributesGroupBox.Location = new System.Drawing.Point(83, 63);
            this.CategoryAttributesGroupBox.Name = "CategoryAttributesGroupBox";
            this.CategoryAttributesGroupBox.Size = new System.Drawing.Size(531, 104);
            this.CategoryAttributesGroupBox.TabIndex = 47;
            this.CategoryAttributesGroupBox.TabStop = false;
            this.CategoryAttributesGroupBox.Text = "Category Attributes";
            // 
            // ConstantTextBox
            // 
            this.ConstantTextBox.Location = new System.Drawing.Point(112, 71);
            this.ConstantTextBox.Name = "ConstantTextBox";
            this.ConstantTextBox.ReadOnly = true;
            this.ConstantTextBox.Size = new System.Drawing.Size(402, 20);
            this.ConstantTextBox.TabIndex = 70;
            // 
            // ConstantLabel
            // 
            this.ConstantLabel.AutoSize = true;
            this.ConstantLabel.Location = new System.Drawing.Point(57, 71);
            this.ConstantLabel.Name = "ConstantLabel";
            this.ConstantLabel.Size = new System.Drawing.Size(49, 13);
            this.ConstantLabel.TabIndex = 69;
            this.ConstantLabel.Text = "Constant";
            this.ConstantLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // CategoryIDTextBox
            // 
            this.CategoryIDTextBox.Location = new System.Drawing.Point(112, 19);
            this.CategoryIDTextBox.Name = "CategoryIDTextBox";
            this.CategoryIDTextBox.ReadOnly = true;
            this.CategoryIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.CategoryIDTextBox.TabIndex = 68;
            // 
            // CategoryID
            // 
            this.CategoryID.AutoSize = true;
            this.CategoryID.Location = new System.Drawing.Point(46, 22);
            this.CategoryID.Name = "CategoryID";
            this.CategoryID.Size = new System.Drawing.Size(60, 13);
            this.CategoryID.TabIndex = 67;
            this.CategoryID.Text = "CategoryID";
            this.CategoryID.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // PublishIDTextBox
            // 
            this.PublishIDTextBox.Location = new System.Drawing.Point(513, 8);
            this.PublishIDTextBox.Name = "PublishIDTextBox";
            this.PublishIDTextBox.ReadOnly = true;
            this.PublishIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.PublishIDTextBox.TabIndex = 46;
            // 
            // PublishIDLabel
            // 
            this.PublishIDLabel.AutoSize = true;
            this.PublishIDLabel.Location = new System.Drawing.Point(455, 12);
            this.PublishIDLabel.Name = "PublishIDLabel";
            this.PublishIDLabel.Size = new System.Drawing.Size(52, 13);
            this.PublishIDLabel.TabIndex = 45;
            this.PublishIDLabel.Text = "PublishID";
            this.PublishIDLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // SitesComboBox
            // 
            this.SitesComboBox.Enabled = false;
            this.SitesComboBox.FormattingEnabled = true;
            this.SitesComboBox.Location = new System.Drawing.Point(83, 37);
            this.SitesComboBox.Name = "SitesComboBox";
            this.SitesComboBox.Size = new System.Drawing.Size(180, 21);
            this.SitesComboBox.TabIndex = 42;
            // 
            // SiteLabel
            // 
            this.SiteLabel.AutoSize = true;
            this.SiteLabel.Location = new System.Drawing.Point(52, 40);
            this.SiteLabel.Name = "SiteLabel";
            this.SiteLabel.Size = new System.Drawing.Size(25, 13);
            this.SiteLabel.TabIndex = 41;
            this.SiteLabel.Text = "Site";
            // 
            // SiteCategoryIDTextBox
            // 
            this.SiteCategoryIDTextBox.Location = new System.Drawing.Point(83, 9);
            this.SiteCategoryIDTextBox.Name = "SiteCategoryIDTextBox";
            this.SiteCategoryIDTextBox.ReadOnly = true;
            this.SiteCategoryIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.SiteCategoryIDTextBox.TabIndex = 40;
            // 
            // SiteCategoryIDLabel
            // 
            this.SiteCategoryIDLabel.AutoSize = true;
            this.SiteCategoryIDLabel.Location = new System.Drawing.Point(-1, 12);
            this.SiteCategoryIDLabel.Name = "SiteCategoryIDLabel";
            this.SiteCategoryIDLabel.Size = new System.Drawing.Size(78, 13);
            this.SiteCategoryIDLabel.TabIndex = 39;
            this.SiteCategoryIDLabel.Text = "SiteCategoryID";
            this.SiteCategoryIDLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // PublishedCheckBox
            // 
            this.PublishedCheckBox.AutoSize = true;
            this.PublishedCheckBox.Location = new System.Drawing.Point(83, 243);
            this.PublishedCheckBox.Name = "PublishedCheckBox";
            this.PublishedCheckBox.Size = new System.Drawing.Size(72, 17);
            this.PublishedCheckBox.TabIndex = 73;
            this.PublishedCheckBox.Text = "Published";
            this.PublishedCheckBox.UseVisualStyleBackColor = true;
            // 
            // LastUpdatedDateTimePicker
            // 
            this.LastUpdatedDateTimePicker.Enabled = false;
            this.LastUpdatedDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.LastUpdatedDateTimePicker.Location = new System.Drawing.Point(83, 266);
            this.LastUpdatedDateTimePicker.Name = "LastUpdatedDateTimePicker";
            this.LastUpdatedDateTimePicker.Size = new System.Drawing.Size(180, 20);
            this.LastUpdatedDateTimePicker.TabIndex = 76;
            this.LastUpdatedDateTimePicker.Value = new System.DateTime(1999, 1, 1, 0, 0, 0, 0);
            // 
            // LastUpdatedLabel
            // 
            this.LastUpdatedLabel.AutoSize = true;
            this.LastUpdatedLabel.Location = new System.Drawing.Point(6, 270);
            this.LastUpdatedLabel.Name = "LastUpdatedLabel";
            this.LastUpdatedLabel.Size = new System.Drawing.Size(71, 13);
            this.LastUpdatedLabel.TabIndex = 75;
            this.LastUpdatedLabel.Text = "Last Updated";
            // 
            // SiteCategoryFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 357);
            this.Controls.Add(this.LastUpdatedDateTimePicker);
            this.Controls.Add(this.LastUpdatedLabel);
            this.Controls.Add(this.PublishedCheckBox);
            this.Controls.Add(this.ContentRichTextBox);
            this.Controls.Add(this.CommandsPanel);
            this.Controls.Add(this.ContentLabel);
            this.Controls.Add(this.CategoryAttributesGroupBox);
            this.Controls.Add(this.PublishIDTextBox);
            this.Controls.Add(this.PublishIDLabel);
            this.Controls.Add(this.SitesComboBox);
            this.Controls.Add(this.SiteLabel);
            this.Controls.Add(this.SiteCategoryIDTextBox);
            this.Controls.Add(this.SiteCategoryIDLabel);
            this.Name = "SiteCategoryFrm";
            this.Text = "SiteCategory";
            this.Load += new System.EventHandler(this.SiteCategoryFrm_Load);
            this.CommandsPanel.ResumeLayout(false);
            this.CategoryAttributesGroupBox.ResumeLayout(false);
            this.CategoryAttributesGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox ContentRichTextBox;
        private System.Windows.Forms.Panel CommandsPanel;
        private System.Windows.Forms.Button SCancelButton;
        private System.Windows.Forms.Button EditCategoryButton;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button NextStepButton;
        private System.Windows.Forms.Label ContentLabel;
        private System.Windows.Forms.TextBox ParentCategoryIDTextBox;
        private System.Windows.Forms.Label ParentCategoryIDLabel;
        private System.Windows.Forms.GroupBox CategoryAttributesGroupBox;
        private System.Windows.Forms.TextBox PublishIDTextBox;
        private System.Windows.Forms.Label PublishIDLabel;
        private System.Windows.Forms.ComboBox SitesComboBox;
        private System.Windows.Forms.Label SiteLabel;
        private System.Windows.Forms.TextBox SiteCategoryIDTextBox;
        private System.Windows.Forms.Label SiteCategoryIDLabel;
        private System.Windows.Forms.TextBox ConstantTextBox;
        private System.Windows.Forms.Label ConstantLabel;
        private System.Windows.Forms.TextBox CategoryIDTextBox;
        private System.Windows.Forms.Label CategoryID;
        private System.Windows.Forms.CheckBox PublishedCheckBox;
        private System.Windows.Forms.DateTimePicker LastUpdatedDateTimePicker;
        private System.Windows.Forms.Label LastUpdatedLabel;
    }
}