using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

using Matchnet.Content.ValueObjects.Article;

namespace Spark.Admin.ArticleModule
{
    public class SiteCategoryNode : TreeNode
    {
        private Int32 categoryID;
        private SiteCategory siteCategory;

        public SiteCategoryNode(String caption, Int32 categoryID)
            : base(caption)
        {
            this.categoryID = categoryID;
        }

        public SiteCategoryNode(SiteCategory siteCategory)
            : base(siteCategory.Content + "[" + siteCategory.CategoryID.ToString() + "]")
        {
            // Set the Key for use with TreeNodeCollection.Find();
            base.Name = siteCategory.CategoryID.ToString();

            categoryID = siteCategory.CategoryID;
            this.siteCategory = siteCategory;

            // If a value doesn not exist for the SiteCategoryID then make it regular.
            // KNOWN ISSUE:  Clipping occurs if we make the default font for the tree as regular and then try
            // to bold individual nodes.  Doing this in reverse avoids this problem.
            if (siteCategory.SiteCategoryID == Int32.MinValue)
                this.NodeFont = new Font("Microsoft Sans Serif", (float)8.25, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            // If the base Category is not published, gray it out.
            if (siteCategory.PublishedFlag == false)
                this.ForeColor = System.Drawing.Color.Gray;
        }

        public int CategoryID
        {
            get
            {
                return categoryID;
            }
        }

        public SiteCategory SiteCategory
        {
            get
            {
                return siteCategory;
            }
        }
    }
}
