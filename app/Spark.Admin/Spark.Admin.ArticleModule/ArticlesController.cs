using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI;

using Spark.Admin.Common;
using Spark.Admin.WebServices;
using Matchnet.Content.ValueObjects.Article;
using Matchnet.Content.ValueObjects.Admin;

namespace Spark.Admin.ArticleModule
{
    public class ArticlesController : BaseController
    {
        private WebServiceProxy webServiceProxy;

        #region Constructors
        public ArticlesController()
        {
            // Initialize the WebServiceProxy if you are using web services.
            webServiceProxy = new WebServiceProxy();        
        }
        #endregion

        #region Public Methods
        public SiteCategoryCollection RetrieveSiteCategories(Int32 siteID, Boolean forceLoadFlag, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            return webServiceProxy.RetrieveSiteCategories(siteID, forceLoadFlag, currentServiceEnvironmentTypeEnum);
        }

        public SiteArticleCollection RetrieveCategorySiteArticlesAndArticles(Int32 categoryID, Int32 siteID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            return webServiceProxy.RetrieveCategorySiteArticlesAndArticles(categoryID, siteID, currentServiceEnvironmentTypeEnum);
        }

        public Int32 SaveSiteArticle(SiteArticle siteArticle, Int32 memberID, Boolean isPublish, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            return webServiceProxy.SaveSiteArticle(siteArticle, memberID, isPublish, currentServiceEnvironmentTypeEnum);
        }

        public Int32 SaveArticle(Article article, Int32 memberID, Boolean isPublish, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            return webServiceProxy.SaveArticle(article, memberID, isPublish, currentServiceEnvironmentTypeEnum);
        }

        public Article RetrieveArticle(Int32 articleID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            return webServiceProxy.RetrieveArticle(articleID, currentServiceEnvironmentTypeEnum);
        }

        public Int32 SaveSiteCategory(SiteCategory siteCategory, Int32 memberID, Boolean isPublish, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            return webServiceProxy.SaveSiteCategory(siteCategory, memberID, isPublish, currentServiceEnvironmentTypeEnum);
        }

        public Int32 SaveCategory(Category category, Int32 memberID, Boolean isPublish, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            return webServiceProxy.SaveCategory(category, memberID, isPublish, currentServiceEnvironmentTypeEnum);
        }

        public Category RetrieveCategory(Int32 categoryID, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            return webServiceProxy.RetrieveCategory(categoryID, currentServiceEnvironmentTypeEnum);
        }
 
        public WSOListCollection RetrieveWSOListCollection()
        {
            return webServiceProxy.RetrieveWSOListCollection();
        }

        
        public void SaveWSOList(Matchnet.Content.ValueObjects.Article.WSOList wsoList)
        {
            webServiceProxy.SaveWSOList(wsoList);
        }

        
        public void CreateWSOList(string name, string note)
        {
            webServiceProxy.CreateWSOList(name, note);
        }

        
        public WSOItemCollection RetrieveWSOItemCollection(int wsoListID)
        {
            return webServiceProxy.RetrieveWSOItemCollection(wsoListID);
        }


        public void SaveWSOItem(Matchnet.Content.ValueObjects.Article.WSOItem wsoItem)
        {
            webServiceProxy.SaveWSOItem(wsoItem);
        }

        public void DeleteWSOItem(Matchnet.Content.ValueObjects.Article.WSOItem wsoItem)
        {
            webServiceProxy.DeleteWSOItem(wsoItem);
        }
        
        public void CreateWSOItem(int wsoListID, int siteID, int itemID, int itemTypeID)
        {
            webServiceProxy.CreateWSOItem(wsoListID, siteID, itemID, itemTypeID);
        }

        #endregion
        
        #region Properties
        public new ModuleWorkItem WorkItem
        {
            get
            {
                return base.WorkItem as ModuleWorkItem;
            }
        }
        #endregion
    }
}
