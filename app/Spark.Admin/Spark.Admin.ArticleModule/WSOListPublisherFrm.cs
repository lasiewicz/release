﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Matchnet.Content.ValueObjects.Article;
using Matchnet.Content.ValueObjects.Admin;

using Microsoft.Practices.CompositeUI;

namespace Spark.Admin.ArticleModule
{
    public partial class WSOListPublisherFrm: Spark.Admin.Common.Controls.Form
    {
        private ArticlesController articlesController;
        private State globalState;
        private WSOList wsoList;
        private WSOItemCollection wsoItemCollection;
        private WSOItemAddFrm wsoItemAddFrm;

        public WSOList WSOList
        {
            get
            {
                return wsoList;
            }
        }

        public WSOListPublisherFrm(WSOList wsoList, State globalState, ArticlesController articlesController) : base(globalState)
        {
            InitializeComponent();

            this.articlesController = articlesController;
            this.globalState = globalState;
            this.wsoList = wsoList;

            LabelWSOList.Text = wsoList.Name + " (ID:" + wsoList.WSOListID + ")";
            LabelEnvironment.Text = articlesController.CurrentServiceEnvironmentTypeEnum.ToString();

            if (articlesController.CurrentServiceEnvironmentTypeEnum == ServiceEnvironmentTypeEnum.Dev)
            {
                ButtonPublishTo.Text = "Publish to " + ServiceEnvironmentTypeEnum.ContentStg.ToString();
            }
            else if (articlesController.CurrentServiceEnvironmentTypeEnum == ServiceEnvironmentTypeEnum.ContentStg)
            {
                if (HasPrivilege(ModuleWorkItem.PrivilegeIDArticlePublisher))
                {
                    ButtonAddNewItem.Visible = false;
                    ButtonDeleteItem.Visible = false;
                    ButtonPublishTo.Text = "Publish to " + ServiceEnvironmentTypeEnum.Prod;
                }
                else
                {
                    MessageBox.Show("You do not have article publishing privilege to prod.", "No privilege", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
            else
            {
                ButtonPublishTo.Visible = false;
            }

            PopulateDataGridViewWSOItem(wsoList);
        }

        public void PopulateDataGridViewWSOItem(WSOList wsoList)
        {
            System.Threading.Thread.Sleep(300);

            wsoItemCollection = articlesController.RetrieveWSOItemCollection(wsoList.WSOListID);

            DataGridViewWSOItem.DataSource = wsoItemCollection;

            this.Refresh();
        }

        private void ButtonAddNewItem_Click(object sender, EventArgs e)
        {
            if (wsoItemAddFrm == null || wsoItemAddFrm.Visible == false)
            {
                wsoItemAddFrm = new WSOItemAddFrm(this, globalState, articlesController);
                wsoItemAddFrm.Show();
            }

            wsoItemAddFrm.Focus();
        }

        private void ButtonRefreshList_Click(object sender, EventArgs e)
        {
            PopulateDataGridViewWSOItem(wsoList);
        }

        /// <summary>
        /// See if this article already exists in the target service environment, if not create and publish one automatically.
        /// </summary>
        /// <param name="article"></param>
        /// <param name="publishActionPublishObjectID"></param>
        /// <param name="targetServiceEnvironmentType"></param>
        private void PublishArticle(Article article, int publishActionPublishObjectID, ServiceEnvironmentTypeEnum targetServiceEnvironmentType)
        {
            try
            {
                Article articleTarget = articlesController.RetrieveArticle(article.ArticleID, targetServiceEnvironmentType);
            }
            catch (Exception ex)
            {
                if (ex.Message.IndexOf("There is no row at position 0") != -1)
                {
                    articlesController.SaveArticle(article, MemberID, true, targetServiceEnvironmentType);
                    CommonController.SavePublishAction(article.PublishID, publishActionPublishObjectID, MemberID);
                }
                else
                {
                    throw ex;
                }
            }
        }

        /// <summary>
        /// See if this category already exists in the target service environment, if not create and publish one automatically.
        /// </summary>
        /// <param name="article"></param>
        /// <param name="publishActionPublishObjectID"></param>
        /// <param name="targetServiceEnvironmentType"></param>
        private void PublishCategory(Category category, int publishActionPublishObjectID, ServiceEnvironmentTypeEnum targetServiceEnvironmentType)
        {
            try
            {
                Category categoryTarget = articlesController.RetrieveCategory(category.CategoryID, targetServiceEnvironmentType);
            }
            catch (Exception ex)
            {
                if (ex.Message.IndexOf("There is no row at position 0") != -1)
                {
                    articlesController.SaveCategory(category, MemberID, true, targetServiceEnvironmentType);
                    CommonController.SavePublishAction(category.PublishID, publishActionPublishObjectID, MemberID);
                }
                else
                {
                    throw ex;
                }
            }
        }

        /// <summary>
        /// This method should also publish parent articles if they don't exist in the target environment.
        /// </summary>
        private void PublishSiteArticle(SiteArticle siteArticle, int publishActionPublishObjectID, ServiceEnvironmentTypeEnum targetServiceEnvironmentType)
        {
            switch (publishActionPublishObjectID)
            {
                case SiteArticleFrm.PubActPubObjIDSiteArticlePublishToContentStg:
                    
                    Article articleDev = articlesController.RetrieveArticle(siteArticle.ArticleID, ServiceEnvironmentTypeEnum.Dev);
                    Category categoryDev = articlesController.RetrieveCategory(articleDev.CategoryID, ServiceEnvironmentTypeEnum.Dev);
                    publishSiteCategoryWSOItem(categoryDev.CategoryID, siteArticle.SiteID);

                    PublishArticle(articleDev, ArticleFrm.PubActPubObjIDArticlePublishToContentStg, ServiceEnvironmentTypeEnum.ContentStg);

                    // To ensure SiteArticleCollection VO Cache expires based on CategoryID
                    siteArticle.ArticleData.CategoryID = categoryDev.CategoryID;

                    articlesController.SaveSiteArticle(siteArticle, MemberID, true, ServiceEnvironmentTypeEnum.ContentStg);
                    CommonController.SavePublishAction(siteArticle.PublishID, SiteArticleFrm.PubActPubObjIDSiteArticlePublishToContentStg, MemberID);

                    break;

                case SiteArticleFrm.PubActPubObjIDSiteArticlePublishToProd:
                    
                    Article articleContentStg = articlesController.RetrieveArticle(siteArticle.ArticleID, ServiceEnvironmentTypeEnum.ContentStg);
                    Category categoryStage = articlesController.RetrieveCategory(articleContentStg.CategoryID, ServiceEnvironmentTypeEnum.ContentStg);
                    publishSiteCategoryWSOItem(categoryStage.CategoryID, siteArticle.SiteID);

                    PublishArticle(articleContentStg, ArticleFrm.PubActPubObjIDArticlePublishToProd, ServiceEnvironmentTypeEnum.Prod);

                    // To ensure SiteArticleCollection VO Cache expires based on CategoryID
                    siteArticle.ArticleData.CategoryID = categoryStage.CategoryID;

                    articlesController.SaveSiteArticle(siteArticle, MemberID, true, ServiceEnvironmentTypeEnum.Prod);
                    CommonController.SavePublishAction(siteArticle.PublishID, SiteArticleFrm.PubActPubObjIDSiteArticlePublishToProd, MemberID);
                    
                    break;

                default:
                    throw new Exception("Publish action " + publishActionPublishObjectID + " is not recognized.");
            }
        }

        /// <summary>
        /// This method should also publish parent articles if they don't exist in the target environment.
        /// </summary>
        private void PublishSiteCategory(SiteCategory siteCategory, int publishActionPublishObjectID, ServiceEnvironmentTypeEnum targetServiceEnvironmentType)
        {
            switch (publishActionPublishObjectID)
            {
                case SiteCategoryFrm.PubActPubObjIDSiteCategoryPublishToContentStg:
                    Category categoryDev = articlesController.RetrieveCategory(siteCategory.CategoryID, ServiceEnvironmentTypeEnum.Dev);
                    PublishCategory(categoryDev, SiteCategoryFrm.PubActPubObjIDSiteCategoryPublishToContentStg, ServiceEnvironmentTypeEnum.ContentStg);
                    
                    articlesController.SaveSiteCategory(siteCategory, MemberID, true, ServiceEnvironmentTypeEnum.ContentStg);
                    CommonController.SavePublishAction(siteCategory.PublishID, SiteArticleFrm.PubActPubObjIDSiteArticlePublishToContentStg, MemberID);
                    
                    break;

                case SiteCategoryFrm.PubActPubObjIDSiteCategoryPublishToProd:
                    Category categoryContentStg = articlesController.RetrieveCategory(siteCategory.CategoryID, ServiceEnvironmentTypeEnum.Dev);
                    PublishCategory(categoryContentStg, SiteCategoryFrm.PubActPubObjIDSiteCategoryPublishToProd, ServiceEnvironmentTypeEnum.Prod);

                    articlesController.SaveSiteCategory(siteCategory, MemberID, true, ServiceEnvironmentTypeEnum.Prod);
                    CommonController.SavePublishAction(siteCategory.PublishID, SiteArticleFrm.PubActPubObjIDSiteArticlePublishToProd, MemberID);
                    
                    break;

                default :
                    throw new Exception("Publish action " + publishActionPublishObjectID + " is not recognized.");
            }
        }

        private void publishSiteCategoryWSOItem(int categoryID, int siteID)
        {
            SiteCategoryCollection siteCategoryCollection = articlesController.RetrieveSiteCategories(siteID, false, ServiceEnvironmentTypeEnum.Dev);
            SiteCategory siteCategoryTarget = null;

            foreach (SiteCategory siteCategory in siteCategoryCollection)
            {
                if ((siteCategory.SiteID == siteID) && (siteCategory.CategoryID == categoryID))
                {
                    siteCategoryTarget = siteCategory;
                    break;
                }
            }

            if (siteCategoryTarget == null)
                throw new Exception("SiteCategory not found. CategoryID=" + categoryID + ", SiteID=" + siteID);

            if (articlesController.CurrentServiceEnvironmentTypeEnum == ServiceEnvironmentTypeEnum.Dev)
            {
                PublishSiteCategory(siteCategoryTarget, SiteCategoryFrm.PubActPubObjIDSiteCategoryPublishToContentStg, ServiceEnvironmentTypeEnum.ContentStg);
            }
            else if (articlesController.CurrentServiceEnvironmentTypeEnum == ServiceEnvironmentTypeEnum.ContentStg) 
            {
                PublishSiteCategory(siteCategoryTarget, SiteCategoryFrm.PubActPubObjIDSiteCategoryPublishToProd, ServiceEnvironmentTypeEnum.Prod);
            }
            else
            {
                throw new Exception("Target service environment is not valid. " + articlesController.CurrentServiceEnvironmentTypeEnum.ToString());
            }
        }

        private void publishSiteArticleWSOItem(int articleID, int siteID)
        {
            Article article = articlesController.RetrieveArticle(articleID, ServiceEnvironmentTypeEnum.Dev);

            SiteArticleCollection siteArticleCollection = articlesController.RetrieveCategorySiteArticlesAndArticles(article.CategoryID, siteID, ServiceEnvironmentTypeEnum.Dev);
            SiteArticle siteArticleTarget = null;

            foreach (SiteArticle siteArticle in siteArticleCollection)
            {
                if ((siteArticle.ArticleID == article.ArticleID) && (siteArticle.SiteID == siteID))
                {
                    siteArticleTarget = siteArticle;
                    break;
                }
            }

            if (siteArticleTarget == null)
                throw new Exception("SiteArticle not found. ArticleID=" + articleID + ", CategoryID=" + article.CategoryID + ", SiteID=" + siteID);

            if (articlesController.CurrentServiceEnvironmentTypeEnum == ServiceEnvironmentTypeEnum.Dev)
            {
                PublishSiteArticle(siteArticleTarget, SiteArticleFrm.PubActPubObjIDSiteArticlePublishToContentStg, ServiceEnvironmentTypeEnum.ContentStg);
            }
            else if (articlesController.CurrentServiceEnvironmentTypeEnum == ServiceEnvironmentTypeEnum.ContentStg)
            {
                PublishSiteArticle(siteArticleTarget, SiteArticleFrm.PubActPubObjIDSiteArticlePublishToProd, ServiceEnvironmentTypeEnum.Prod);
            }
            else
            {
                throw new Exception("Target service environment is not valid. " + articlesController.CurrentServiceEnvironmentTypeEnum.ToString());
            }
        }

        private void ButtonDeleteItem_Click(object sender, EventArgs e)
        {
            if (DataGridViewWSOItem.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select entire rows.");

                return;
            }

            for (int i = 0; i < DataGridViewWSOItem.SelectedRows.Count; i++)
            {
                WSOItem wsoItem = (WSOItem)wsoItemCollection[DataGridViewWSOItem.SelectedRows[i].Index];

                articlesController.DeleteWSOItem(wsoItem);
            }

            PopulateDataGridViewWSOItem(wsoList);
        }

        /// <summary>
        /// Iterates through the data list and publishes each item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonPublishTo_Click(object sender, EventArgs e)
        {
            if (wsoList == null)
            {
                throw new Exception("WSO List is undefined on this form.");
            }

            Cursor = Cursors.WaitCursor;

            WSOItemCollection wsoItemCollection = articlesController.RetrieveWSOItemCollection(wsoList.WSOListID);

            System.Text.StringBuilder stringLog = new StringBuilder();

            stringLog.Append("Published WSO List " + System.DateTime.Now.ToString() + System.Environment.NewLine);

            foreach (WSOItem wsoItem in wsoItemCollection)
            {
                if (wsoItem.ItemTypeID == (int)ArticleEntityType.Article)
                {
                    ToolStripStatusLabel1.Text = "Publishing Site Article WSO Item. WSOItemID = " + wsoItem.ItemID;
                    this.Refresh();
                    publishSiteArticleWSOItem(wsoItem.ItemID, wsoItem.SiteID);
                    stringLog.Append("Published Article WSO Item\t" + wsoItem.ItemID + System.Environment.NewLine);
                }
                else if (wsoItem.ItemTypeID == (int)ArticleEntityType.Category)
                {
                    ToolStripStatusLabel1.Text = "Publishing Site Category WSO Item. WSOItemID = " + wsoItem.ItemID;
                    this.Refresh();
                    publishSiteCategoryWSOItem(wsoItem.ItemID, wsoItem.SiteID);
                    stringLog.Append("Published Category WSO Item\t" + wsoItem.ItemID + System.Environment.NewLine);
                }
                else
                {
                    throw new Exception("Unknown article entity type in the WSO list. WSOItemID=" + wsoItem.WSOItemID);
                }
            }

            ToolStripStatusLabel1.Text = "All items have been published. Please verify.";

            articlesController.SaveWSOList(new WSOList(wsoList.WSOListID, wsoList.Name, wsoList.Note + System.Environment.NewLine + stringLog.ToString()));

            Cursor = Cursors.Default;
        }
    }
}
