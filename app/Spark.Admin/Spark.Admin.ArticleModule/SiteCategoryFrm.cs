using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI;

using Spark.Admin.Common;

using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.Article;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Spark.Admin.ArticleModule
{
    public partial class SiteCategoryFrm : Spark.Admin.Common.Controls.Form
    {
        private SiteCategory siteCategory;
        private ArticlesController articlesController;
        private Int32 nextPublishActionPublishObjectID = Matchnet.Constants.NULL_INT;
        private String nextStepText = String.Empty;

        #region Publish Constants
        public const Int32 PubActPubObjIDSiteCategoryAdd = 200;
        public const Int32 PubActPubObjIDSiteCategoryEdit = 201;
        public const Int32 PubActPubObjIDSiteCategoryDelete = 202;
        public const Int32 PubActPubObjIDSiteCategoryPublishToContentStg = 203;
        public const Int32 PubActPubObjIDSiteCategoryApproveInContentStg = 204;
        public const Int32 PubActPubObjIDSiteCategoryVerifyInContentStg = 205;
        public const Int32 PubActPubObjIDSiteCategoryPublishToProd = 206;
        public const Int32 PubActPubObjIDSiteCategoryVerifyInProd = 207;

        private const String PubActPubObjTextSiteCategoryApproveInContentStg = "Approve in ContentStg";
        private const String PubActPubObjTextSiteCategoryVerifyInContentStg = "Verify in ContentStg";
        private const String PubActPubObjTextSiteCategoryPublishToContentStg = "Publish to ContentStg";
        private const String PubActPubObjTextSiteCategoryPublishToProd = "Publish to Prod";
        private const String PubActPubObjTextSiteCategoryVerifyInProd = "Verify in Prod";
        #endregion

        #region Message Constants
        private const String MsgSaveSiteCategoryConfirm = "Are you sure you want to save this SiteCategory?";
        private const String MsgNextStepSiteCategoryConfirm = "Are you sure you want to {0} this SiteCategory?";
        #endregion

        public SiteCategoryFrm(State globalState, ArticlesController articlesController, SiteCategory siteCategory) : base(globalState)
        {
            InitializeComponent();

            this.siteCategory = siteCategory;
            this.articlesController = articlesController;

            // Set up Sites ComboBox.
            SitesComboBox.DataSource = ((Sites)GlobalState[Constants.StateSites]);
            SitesComboBox.ValueMember = "SiteID";
            SitesComboBox.DisplayMember = "Name";

            // Display/Hide Save and Delete buttons.
            if (!HasPrivilege(ModuleWorkItem.PrivilegeIDArticleEditor))
            {
                EditCategoryButton.Visible = false;
                SaveButton.Visible = false;
            }
            else if (articlesController.CurrentServiceEnvironmentTypeEnum != ServiceEnvironmentTypeEnum.Dev)
            {
                SaveButton.Visible = false;
            }

            InitializeNextStep();
        }

        #region Private Methods
        private void InitializeNextStep()
        {
            NextStepButton.Visible = false;

            // If this is a new siteCategory, there will be no next step.
            if (siteCategory.SiteCategoryID != Matchnet.Constants.NULL_INT && siteCategory.SiteCategoryID != Int32.MinValue)
            {
                ServiceEnvironmentTypeEnum serviceEnvrionmentTypeEnum;
                // Get information about the next Publish step.
                nextPublishActionPublishObjectID = CommonController.GetNextPublishActionPublishObjectStep(siteCategory.SiteCategoryID, PublishObjectType.SiteCategory, out serviceEnvrionmentTypeEnum);

                // Display/Hide NextStepButton.
                if (siteCategory.SiteCategoryID > 0 && nextPublishActionPublishObjectID != Matchnet.Constants.NULL_INT && (articlesController.CurrentServiceEnvironmentTypeEnum == serviceEnvrionmentTypeEnum))
                {
                    switch (nextPublishActionPublishObjectID)
                    {
                        case PubActPubObjIDSiteCategoryPublishToContentStg:
                            NextStepButton.Visible = true;
                            nextStepText = PubActPubObjTextSiteCategoryPublishToContentStg;

                            break;

                        case PubActPubObjIDSiteCategoryVerifyInContentStg:
                            NextStepButton.Visible = true;
                            nextStepText = PubActPubObjTextSiteCategoryVerifyInContentStg;

                            break;

                        case PubActPubObjIDSiteCategoryApproveInContentStg:
                            if (HasPrivilege(ModuleWorkItem.PrivilegeIDArticleApprover))
                            {
                                NextStepButton.Visible = true;
                                nextStepText = PubActPubObjTextSiteCategoryApproveInContentStg;
                            }

                            break;

                        case PubActPubObjIDSiteCategoryPublishToProd:
                            if (HasPrivilege(ModuleWorkItem.PrivilegeIDArticlePublisher))
                            {
                                NextStepButton.Visible = true;
                                nextStepText = PubActPubObjTextSiteCategoryPublishToProd;
                            }

                            break;

                        case PubActPubObjIDSiteCategoryVerifyInProd:
                            NextStepButton.Visible = true;
                            nextStepText = PubActPubObjTextSiteCategoryVerifyInProd;

                            break;

                        default:
                            NextStepButton.Visible = false;

                            break;
                    }

                    NextStepButton.Text = nextStepText;
                }
            }
        }

        private void SiteCategoryFrm_Load(object sender, System.EventArgs e)
        {
            DisplaySiteCategory();
        }

        private void DisplaySiteCategory()
        {
            if (siteCategory.SiteCategoryID > 0)
                SiteCategoryIDTextBox.Text = siteCategory.SiteCategoryID.ToString();
            else
                SiteCategoryIDTextBox.Text = Matchnet.Constants.NULL_INT.ToString();
           
            for (int i = 0; i < SitesComboBox.Items.Count; i++)
            {
                if (((Site)SitesComboBox.Items[i]).SiteID == siteCategory.SiteID)
                {
                    SitesComboBox.SelectedIndex = i;
                    break;
                }
            }

            CategoryIDTextBox.Text = siteCategory.CategoryID.ToString();
            ParentCategoryIDTextBox.Text = siteCategory.ParentCategoryID.ToString();
            ConstantTextBox.Text = siteCategory.Constant;
            ContentRichTextBox.Text = siteCategory.Content;

            if (siteCategory.PublishedFlag)
                PublishedCheckBox.Checked = true;
            else
                PublishedCheckBox.Checked = false;

            if (siteCategory.LastUpdated > DateTime.MinValue)
                LastUpdatedDateTimePicker.Value = siteCategory.LastUpdated;
            PublishIDTextBox.Text = siteCategory.PublishID.ToString();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            siteCategory = BuildSiteCategory();

            // Are you sure?
            if (MessageBox.Show(MsgSaveSiteCategoryConfirm, Constants.MsgBoxCaptionConfirm, MessageBoxButtons.YesNo) == DialogResult.No)
                return;
            else
            {
                try
                {
                    // Save.
                    articlesController.SaveSiteCategory(siteCategory, MemberID, false, ServiceEnvironmentTypeEnum.Dev);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format(Constants.MsgContactAdministrator, ex.Message), Constants.MsgBoxCaptionSystemError);
                    return;
                }

                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private SiteCategory BuildSiteCategory()
        {
            SiteCategory siteCategory = new SiteCategory();

            siteCategory.SiteCategoryID = Convert.ToInt32(SiteCategoryIDTextBox.Text.Trim());
            siteCategory.SiteID = Convert.ToInt32(SitesComboBox.SelectedValue);
            siteCategory.CategoryID = Convert.ToInt32(CategoryIDTextBox.Text.Trim());
            siteCategory.Content = ContentRichTextBox.Text.Trim();
            siteCategory.PublishedFlag = PublishedCheckBox.Checked;
            siteCategory.PublishID = Convert.ToInt32(PublishIDTextBox.Text.Trim());

            return siteCategory;
        }

        private void SCancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void NextStepButton_Click(object sender, EventArgs e)
        {
            // Are you sure?
            if (MessageBox.Show(String.Format(MsgNextStepSiteCategoryConfirm, NextStepButton.Text), Constants.MsgBoxCaptionConfirm, MessageBoxButtons.YesNo) == DialogResult.No)
                return;
            else
            {
                try
                {
                    // Do the correct action.
                    switch (nextPublishActionPublishObjectID)
                    {
                        case PubActPubObjIDSiteCategoryPublishToContentStg:
                            try
                            {
                                articlesController.RetrieveCategory(siteCategory.CategoryID, ServiceEnvironmentTypeEnum.ContentStg);
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message.IndexOf("There is no row at position 0") != -1)
                                {
                                    MessageBox.Show("You cannot publish a SiteCategory to ContentStg if its associated Category does not exist on ContentStg.  Please publish CategoryID "
                                        + siteCategory.CategoryID.ToString() + " to ContentStg before attempting to publish this.", 
                                        Constants.MsgBoxCaptionWarning);
                                    return;
                                }
                                else
                                {
                                    throw ex;
                                }
                            }
                            
                            articlesController.SaveSiteCategory(siteCategory, MemberID, true, ServiceEnvironmentTypeEnum.ContentStg);
                            CommonController.SavePublishAction(siteCategory.PublishID, PubActPubObjIDSiteCategoryPublishToContentStg, MemberID);

                            break;

                        case PubActPubObjIDSiteCategoryPublishToProd:
                            try
                            {
                                articlesController.RetrieveCategory(siteCategory.CategoryID, ServiceEnvironmentTypeEnum.Prod);
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message.IndexOf("There is no row at position 0") != -1)
                                {
                                    MessageBox.Show("You cannot publish a SiteCategory to Prod if its associated Category does not exist on Prod.  Please publish CategoryID "
                                        + siteCategory.CategoryID.ToString() + " to Prod before attempting to publish this.", 
                                        Constants.MsgBoxCaptionWarning);
                                    return;
                                }
                                else
                                {
                                    throw ex;
                                }
                            }

                            articlesController.SaveSiteCategory(siteCategory, MemberID, true, ServiceEnvironmentTypeEnum.Prod);
                            CommonController.SavePublishAction(siteCategory.PublishID, PubActPubObjIDSiteCategoryPublishToProd, MemberID);

                            Helper.SendEmail(Constants.EmailsToAlert, Helper.EmailType.DeploymentToProd
                                , "SiteCategory", MemberEmail, BuildEmailBody());

                            break;

                        // All Verifies are handled here.
                        default:
                            CommonController.SavePublishAction(siteCategory.PublishID, nextPublishActionPublishObjectID, MemberID);

                            break;
                    }

                    DialogResult = DialogResult.OK;
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format(Constants.MsgContactAdministrator, ex.Message), Constants.MsgBoxCaptionSystemError);
                    return;
                }
            }
        }

        private String BuildEmailBody()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append("SiteCategoryID: ").Append(siteCategory.SiteCategoryID.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("SiteID: ").Append(siteCategory.SiteID.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("CategoryID: ").Append(siteCategory.CategoryID.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("Content: ").Append(siteCategory.Content.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("PublishID: ").Append(siteCategory.PublishID.ToString()).Append(Environment.NewLine);
            
            return stringBuilder.ToString();
        }

        private void EditCategoryButton_Click(object sender, EventArgs e)
        {
            CategoryFrm categoryFrm = new CategoryFrm(GlobalState, articlesController, siteCategory.CategoryID);

            if (categoryFrm.ShowDialog() == DialogResult.OK)
            {
                this.DialogResult = DialogResult.OK;
            }
        }
        #endregion
    }
}