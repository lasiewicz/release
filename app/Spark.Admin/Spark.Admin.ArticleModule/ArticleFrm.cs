using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI;

using Spark.Admin.Common;

using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.Article;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Spark.Admin.ArticleModule
{
    public partial class ArticleFrm : Spark.Admin.Common.Controls.Form
    {
        private Article article;
        private ArticlesController articlesController;
        private Int32 nextPublishActionPublishObjectID = Matchnet.Constants.NULL_INT;
        private String nextStepText = String.Empty;

        #region Publish Constants
        public const Int32 PubActPubObjIDArticleAdd = 600;
        public const Int32 PubActPubObjIDArticleEdit = 601;
        public const Int32 PubActPubObjIDArticleDelete = 602;
        public const Int32 PubActPubObjIDArticlePublishToContentStg = 603;
        public const Int32 PubActPubObjIDArticleApproveInContentStg = 604;
        public const Int32 PubActPubObjIDArticlePublishToProd = 605;
        public const Int32 PubActPubObjIDArticleVerifyInProd = 606;

        private const String PubActPubObjTextArticleApproveInContentStg = "Approve in ContentStg";
        private const String PubActPubObjTextArticlePublishToContentStg = "Publish to ContentStg";
        private const String PubActPubObjTextArticlePublishToProd = "Publish to Prod";
        private const String PubActPubObjTextArticleVerifyInProd = "Verify in Prod";
        #endregion

        #region Message Constants
        private const String MsgSaveArticleConfirm = "Are you sure you want to save this Article?";
        private const String MsgNextStepArticleConfirm = "Are you sure you want to {0} this Article?";
        #endregion

        public ArticleFrm(State globalState, ArticlesController articlesController) : base(globalState)
        {
            InitializeComponent();

            this.articlesController = articlesController;

            ServiceEnvironmentLabel.Text = articlesController.CurrentServiceEnvironmentTypeEnum.ToString().ToUpper();

            // Display/Hide Save and Delete buttons.
            if (!HasPrivilege(ModuleWorkItem.PrivilegeIDArticleEditor) || articlesController.CurrentServiceEnvironmentTypeEnum != ServiceEnvironmentTypeEnum.Dev)
            {
                SaveButton.Visible = false;
            }
        }

        public ArticleFrm(State globalState, ArticlesController articlesController, Int32 articleID) : this(globalState, articlesController)
        {
            article = articlesController.RetrieveArticle(articleID, articlesController.CurrentServiceEnvironmentTypeEnum);

            InitializeNextStep();
        }

        public ArticleFrm(State globalState, ArticlesController articlesController, Int32 categoryID, Boolean isCategoryID) : this(globalState, articlesController)
		{
			article = new Article();
            article.CategoryID = categoryID;

            InitializeNextStep();
		}

        #region Private Methods
        private void ArticleFrm_Load(object sender, System.EventArgs e)
        {
            DisplayArticle();
        }

        private void InitializeNextStep()
        {
            NextStepButton.Visible = false;

            // If this is a new article, there will be no next step.
            if (article.ArticleID != Matchnet.Constants.NULL_INT && article.ArticleID != Int32.MinValue)
            {
                ServiceEnvironmentTypeEnum serviceEnvrionmentTypeEnum;
                // Get information about the next Publish step.
                nextPublishActionPublishObjectID = CommonController.GetNextPublishActionPublishObjectStep(article.ArticleID, PublishObjectType.Article, out serviceEnvrionmentTypeEnum);

                // Display/Hide NextStepButton.
                if (article.ArticleID > 0 && nextPublishActionPublishObjectID != Matchnet.Constants.NULL_INT && (articlesController.CurrentServiceEnvironmentTypeEnum == serviceEnvrionmentTypeEnum))
                {
                    switch (nextPublishActionPublishObjectID)
                    {
                        case PubActPubObjIDArticlePublishToContentStg:
                            NextStepButton.Visible = true;
                            nextStepText = PubActPubObjTextArticlePublishToContentStg;

                            break;

                        case PubActPubObjIDArticleApproveInContentStg:
                            if (HasPrivilege(ModuleWorkItem.PrivilegeIDArticleApprover))
                            {
                                NextStepButton.Visible = true;
                                nextStepText = PubActPubObjTextArticleApproveInContentStg;
                            }

                            break;

                        case PubActPubObjIDArticlePublishToProd:
                            if (HasPrivilege(ModuleWorkItem.PrivilegeIDArticlePublisher))
                            {
                                NextStepButton.Visible = true;
                                nextStepText = PubActPubObjTextArticlePublishToProd;
                            }

                            break;

                        case PubActPubObjIDArticleVerifyInProd:
                            NextStepButton.Visible = true;
                            nextStepText = PubActPubObjTextArticleVerifyInProd;

                            break;

                        default:
                            NextStepButton.Visible = false;

                            break;
                    }

                    NextStepButton.Text = nextStepText;
                }
            }
        }

        private void DisplayArticle()
        {
            ArticleIDTextBox.Text = article.ArticleID.ToString();
            CategoryIDTextBox.Text = article.CategoryID.ToString();
            ConstantTextBox.Text = article.Constant;
            ContentRichTextBox.Text = article.DefaultContent;
            if (article.LastUpdated > LastUpdatedDateTimePicker.MinDate)
                LastUpdatedDateTimePicker.Value = article.LastUpdated;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            article = BuildArticle();

            // Are you sure?
            if (MessageBox.Show(MsgSaveArticleConfirm, Constants.MsgBoxCaptionConfirm, MessageBoxButtons.YesNo) == DialogResult.No)
                return;
            else
            {
                try
                {
                    // Save.
                    articlesController.SaveArticle(article, MemberID, false, ServiceEnvironmentTypeEnum.Dev);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format(Constants.MsgContactAdministrator, ex.Message), Constants.MsgBoxCaptionSystemError);
                    return;
                }

                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private Article BuildArticle()
        {
            Article article = new Article();

            article.ArticleID = Convert.ToInt32(ArticleIDTextBox.Text.Trim());
            article.CategoryID = Convert.ToInt32(CategoryIDTextBox.Text.Trim());
            article.Constant = ConstantTextBox.Text.Trim();
            article.DefaultContent = ContentRichTextBox.Text.Trim();

            return article;
        }

        private void SCancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ConstantTextBox_TextChanged(object sender, System.EventArgs e)
        {
            CommonController.BuildConstant(ref ConstantTextBox);
        }

        private void NextStepButton_Click(object sender, EventArgs e)
        {
            // Are you sure?
            if (MessageBox.Show(String.Format(MsgNextStepArticleConfirm, NextStepButton.Text), Constants.MsgBoxCaptionConfirm, MessageBoxButtons.YesNo) == DialogResult.No)
                return;
            else
            {
                try
                {
                    // Do the correct action.
                    switch (nextPublishActionPublishObjectID)
                    {
                        case PubActPubObjIDArticlePublishToContentStg:
                            try
                            {
                                articlesController.RetrieveCategory(article.CategoryID, ServiceEnvironmentTypeEnum.ContentStg);
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message.IndexOf("There is no row at position 0") != -1)
                                {
                                    MessageBox.Show("You cannot publish an Article to ContentStg if the Category to which it belongs does not exist on ContentStg.  Please publish CategoryID "
                                        + article.CategoryID.ToString() + " to ContentStg before attempting to publish this.", 
                                        Constants.MsgBoxCaptionWarning);
                                    return;
                                }
                                else
                                {
                                    throw ex;
                                }
                            }

                            articlesController.SaveArticle(article, MemberID, true, ServiceEnvironmentTypeEnum.ContentStg);
                            CommonController.SavePublishAction(article.PublishID, PubActPubObjIDArticlePublishToContentStg, MemberID);

                            break;

                        case PubActPubObjIDArticlePublishToProd:
                            try
                            {
                                articlesController.RetrieveCategory(article.CategoryID, ServiceEnvironmentTypeEnum.Prod);
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message.IndexOf("There is no row at position 0") != -1)
                                {
                                    MessageBox.Show("You cannot publish an Article to Prod if the Category to which it belongs does not exist on Prod.  Please publish CategoryID " 
                                        + article.CategoryID.ToString() + " to Prod before attempting to publish this.",
                                        Constants.MsgBoxCaptionWarning);
                                    return;
                                }
                                else
                                {
                                    throw ex;
                                }
                            }

                            articlesController.SaveArticle(article, MemberID, true, ServiceEnvironmentTypeEnum.Prod);
                            CommonController.SavePublishAction(article.PublishID, PubActPubObjIDArticlePublishToProd, MemberID);

                            Helper.SendEmail(Constants.EmailsToAlert, Helper.EmailType.DeploymentToProd
                                , "Article", MemberEmail, BuildEmailBody());

                            break;

                        // All Verifies are handled here.
                        default:
                            CommonController.SavePublishAction(article.PublishID, nextPublishActionPublishObjectID, MemberID);

                            break;
                    }

                    DialogResult = DialogResult.OK;
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format(Constants.MsgContactAdministrator, ex.Message), Constants.MsgBoxCaptionSystemError);
                    return;
                }
            }
        }

        private String BuildEmailBody()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append("ArticleID: ").Append(article.ArticleID.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("CategoryID: ").Append(article.CategoryID.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("Constant: ").Append(article.Constant).Append(Environment.NewLine);
            stringBuilder.Append("Default Content: ").Append(article.DefaultContent).Append(Environment.NewLine);
            stringBuilder.Append("PublishID: ").Append(article.PublishID.ToString()).Append(Environment.NewLine);

            return stringBuilder.ToString();
        }
        #endregion
    }
}