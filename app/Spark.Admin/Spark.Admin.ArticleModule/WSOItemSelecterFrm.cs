﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI;

using Matchnet.Content.ValueObjects.Article;

namespace Spark.Admin.ArticleModule
{
    public partial class WSOItemSelecterFrm : Spark.Admin.Common.Controls.Form
    {
        private ArticlesController articlesController;
        private State globalState;

        public WSOItemSelecterFrm(State globalState, ArticlesController articlesController) : base(globalState)
        {
            InitializeComponent();

            this.globalState = globalState;
            this.articlesController = articlesController;
        }
    }
}
