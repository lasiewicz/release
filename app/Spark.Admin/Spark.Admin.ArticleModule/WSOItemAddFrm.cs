﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI;

using Matchnet.Content.ValueObjects.Article;

namespace Spark.Admin.ArticleModule
{
    public partial class WSOItemAddFrm : Spark.Admin.Common.Controls.Form
    {
        private ArticlesController articlesController;
        private State globalState;
        private WSOListPublisherFrm wsoListPublisherFrm;

        public WSOItemAddFrm(WSOListPublisherFrm wsoListPublisherFrm, State globalState, ArticlesController articlesController)
            : base(globalState)
        {
            InitializeComponent();

            this.globalState = globalState;
            this.articlesController = articlesController;
            this.wsoListPublisherFrm = wsoListPublisherFrm;
            
            LabelEnvironment.Text = articlesController.CurrentServiceEnvironmentTypeEnum.ToString();
            
            ComboBoxSite.ValueMember = "Name";
            ComboBoxSite.DataSource = ((Matchnet.Content.ValueObjects.BrandConfig.Sites)globalState[Spark.Admin.Common.Constants.StateSites]);
        }

        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            int siteID = ((Matchnet.Content.ValueObjects.BrandConfig.Site)(ComboBoxSite.SelectedItem)).SiteID;
            int itemID = Convert.ToInt32(TextBoxID.Text);   
            ArticleEntityType articleEntityType = (ArticleEntityType)Enum.Parse(typeof(ArticleEntityType), Convert.ToString(ComboBoxType.SelectedItem));

            #region Validation

            bool found = false;

            if (articleEntityType == ArticleEntityType.Category)
            {
                SiteCategoryCollection siteCategoryCollection = articlesController.RetrieveSiteCategories(siteID, false, articlesController.CurrentServiceEnvironmentTypeEnum);
                
                foreach (SiteCategory siteCategory in siteCategoryCollection)
                {
                    if ((siteCategory.SiteID == siteID) && (siteCategory.CategoryID == itemID))
                    {
                        found = true;
                        break;
                    }
                }
            }
            else if (articleEntityType == ArticleEntityType.Article)
            {
                int categoryID = Convert.ToInt32(TextBoxCategoryID.Text);

                SiteArticleCollection siteArticleCollection = articlesController.RetrieveCategorySiteArticlesAndArticles(categoryID, siteID, articlesController.CurrentServiceEnvironmentTypeEnum);

                foreach (SiteArticle siteArticle in siteArticleCollection)
                {
                    if ((siteArticle.SiteID == siteID) && (siteArticle.ArticleID == itemID))
                    {
                        found = true;
                        break;
                    }
                }
            }


            if (!found)
            {
                MessageBox.Show("Validation failed. Please enter correct data and try again.", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            #endregion
            
            articlesController.CreateWSOItem(wsoListPublisherFrm.WSOList.WSOListID, siteID, itemID, (int)articleEntityType);

            wsoListPublisherFrm.PopulateDataGridViewWSOItem(wsoListPublisherFrm.WSOList);
        }

        private void ComboBoxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ArticleEntityType articleEntityType = (ArticleEntityType)Enum.Parse(typeof(ArticleEntityType), Convert.ToString(ComboBoxType.SelectedItem));

            if (articleEntityType == ArticleEntityType.Article)
            {
                LabelID.Text = "Article ID:";
                LabelCategoryID.Visible = true;
                TextBoxCategoryID.Visible = true;
                TextBoxCategoryID.Text = TextBoxID.Text;
                TextBoxID.Text = "";
            }
            else if (articleEntityType == ArticleEntityType.Category)
            {
                LabelID.Text = "Category ID:";
                LabelCategoryID.Visible = false;
                TextBoxCategoryID.Visible = false;
            }
        }
    }
}
