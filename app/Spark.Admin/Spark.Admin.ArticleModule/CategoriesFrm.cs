using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI;

using Spark.Admin.Common;

using Matchnet.Content.ValueObjects.Article;

namespace Spark.Admin.ArticleModule
{
    public partial class CategoriesFrm : Spark.Admin.Common.Controls.Form
    {
        private SiteCategory siteCategory;
        private ArticlesController articlesController;

        #region Constructors
        /// <summary>
        /// You must call the base constructor (base(globalState)) or you will have to assign the
        /// GlobalState manually from the caller.
        /// </summary>
        /// <param name="globalState"></param>
        /// <param name="siteCategory"></param>
        public CategoriesFrm(State globalState, ArticlesController articlesController, SiteCategory siteCategory) : base(globalState)
        {
            InitializeComponent();

            this.siteCategory = siteCategory;
            this.articlesController = articlesController;

            //// Set up Sites ComboBox.
            //SitesComboBox.DataSource = ((Sites)GlobalState[Constants.StateSites]);
            //SitesComboBox.ValueMember = "SiteID";
            //SitesComboBox.DisplayMember = "Name";

            //FiltersListBox.DisplayMember = "DisplayName";
            //ParametersListBox.DisplayMember = "DisplayName";

            //// Display/Hide Save and Delete buttons.
            //if (!HasPrivilege(ModuleWorkItem.PrivilegeIDPixelEditor) || articlesController.CurrentServiceEnvironmentTypeEnum != ServiceEnvironmentTypeEnum.Dev)
            //{
            //    SaveButton.Visible = false;
            //    DeleteButton.Visible = false;
            //}

            //// If this is a new siteCategory, disable Delete button.
            //if (siteCategory.PagePixelID <= 0)
            //{
            //    DeleteButton.Enabled = false;
            //    newPixelFlag = true;
            //}

            //InitializeNextStep();

            //TextRadioButton_CheckedChanged(null, null);
        }
        #endregion

        #region Public Methods
        public static Boolean AddCategoryNode(SiteCategoryNode siteCategoryNode, SiteCategory siteCategory)
        {
            SiteCategoryNode newNode;

            if (siteCategoryNode.CategoryID == siteCategory.CategoryData.ParentCategoryID)
            {
                newNode = new SiteCategoryNode(siteCategory);

                siteCategoryNode.Nodes.Add(newNode);

                return true;
            }

            foreach (SiteCategoryNode node in siteCategoryNode.Nodes)
            {
                if (node.CategoryID == siteCategory.CategoryData.ParentCategoryID)
                {
                    newNode = new SiteCategoryNode(siteCategory);

                    node.Nodes.Add(newNode);

                    return true;
                }
                else
                {
                    if (AddCategoryNode(node, siteCategory))
                        return true;
                }
            }

            return false;
        }
        #endregion
    }
}