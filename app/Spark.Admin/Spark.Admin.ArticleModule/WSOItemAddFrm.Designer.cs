﻿namespace Spark.Admin.ArticleModule
{
    partial class WSOItemAddFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WSOItemAddFrm));
            this.ComboBoxType = new System.Windows.Forms.ComboBox();
            this.TextBoxID = new System.Windows.Forms.TextBox();
            this.ButtonAdd = new System.Windows.Forms.Button();
            this.LabelID = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ComboBoxSite = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TextBoxCategoryID = new System.Windows.Forms.TextBox();
            this.LabelCategoryID = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.LabelEnvironment = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ComboBoxType
            // 
            this.ComboBoxType.FormattingEnabled = true;
            this.ComboBoxType.Items.AddRange(new object[] {
            "Article",
            "Category"});
            this.ComboBoxType.Location = new System.Drawing.Point(92, 94);
            this.ComboBoxType.Name = "ComboBoxType";
            this.ComboBoxType.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxType.TabIndex = 2;
            this.ComboBoxType.SelectedIndexChanged += new System.EventHandler(this.ComboBoxType_SelectedIndexChanged);
            // 
            // TextBoxID
            // 
            this.TextBoxID.Location = new System.Drawing.Point(92, 131);
            this.TextBoxID.Name = "TextBoxID";
            this.TextBoxID.Size = new System.Drawing.Size(121, 22);
            this.TextBoxID.TabIndex = 3;
            this.toolTip1.SetToolTip(this.TextBoxID, "Article ID or Categroy ID.");
            // 
            // ButtonAdd
            // 
            this.ButtonAdd.Location = new System.Drawing.Point(73, 209);
            this.ButtonAdd.Name = "ButtonAdd";
            this.ButtonAdd.Size = new System.Drawing.Size(140, 30);
            this.ButtonAdd.TabIndex = 5;
            this.ButtonAdd.Text = "Validate and Add";
            this.ButtonAdd.UseVisualStyleBackColor = true;
            this.ButtonAdd.Click += new System.EventHandler(this.ButtonAdd_Click);
            // 
            // LabelID
            // 
            this.LabelID.Location = new System.Drawing.Point(3, 140);
            this.LabelID.Name = "LabelID";
            this.LabelID.Size = new System.Drawing.Size(79, 13);
            this.LabelID.TabIndex = 3;
            this.LabelID.Text = "ID:";
            this.LabelID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(49, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Type:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ComboBoxSite
            // 
            this.ComboBoxSite.FormattingEnabled = true;
            this.ComboBoxSite.Location = new System.Drawing.Point(92, 56);
            this.ComboBoxSite.Name = "ComboBoxSite";
            this.ComboBoxSite.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxSite.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(54, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Site:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TextBoxCategoryID
            // 
            this.TextBoxCategoryID.Location = new System.Drawing.Point(92, 166);
            this.TextBoxCategoryID.Name = "TextBoxCategoryID";
            this.TextBoxCategoryID.Size = new System.Drawing.Size(121, 22);
            this.TextBoxCategoryID.TabIndex = 4;
            this.toolTip1.SetToolTip(this.TextBoxCategoryID, "Category ID of the Site Article\r\nyou\'re adding.");
            // 
            // LabelCategoryID
            // 
            this.LabelCategoryID.Location = new System.Drawing.Point(12, 169);
            this.LabelCategoryID.Name = "LabelCategoryID";
            this.LabelCategoryID.Size = new System.Drawing.Size(70, 23);
            this.LabelCategoryID.TabIndex = 8;
            this.LabelCategoryID.Text = "Category ID:";
            this.LabelCategoryID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            // 
            // LabelEnvironment
            // 
            this.LabelEnvironment.AutoSize = true;
            this.LabelEnvironment.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelEnvironment.Location = new System.Drawing.Point(85, 9);
            this.LabelEnvironment.Name = "LabelEnvironment";
            this.LabelEnvironment.Size = new System.Drawing.Size(141, 21);
            this.LabelEnvironment.TabIndex = 9;
            this.LabelEnvironment.Text = "LabelEnvironment";
            this.LabelEnvironment.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // WSOItemAddFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 259);
            this.Controls.Add(this.LabelEnvironment);
            this.Controls.Add(this.LabelCategoryID);
            this.Controls.Add(this.TextBoxCategoryID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ComboBoxSite);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LabelID);
            this.Controls.Add(this.ButtonAdd);
            this.Controls.Add(this.TextBoxID);
            this.Controls.Add(this.ComboBoxType);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WSOItemAddFrm";
            this.Text = "WSO Add Item";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ComboBoxType;
        private System.Windows.Forms.TextBox TextBoxID;
        private System.Windows.Forms.Button ButtonAdd;
        private System.Windows.Forms.Label LabelID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ComboBoxSite;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBoxCategoryID;
        private System.Windows.Forms.Label LabelCategoryID;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label LabelEnvironment;
    }
}