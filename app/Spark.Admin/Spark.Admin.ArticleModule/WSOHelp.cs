﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Spark.Admin.ArticleModule
{
    public partial class WSOHelp : Form
    {
        public WSOHelp()
        {
            InitializeComponent();

            PictureBoxWSOWorkFlowScreenShot.Image = (Bitmap)Properties.Resources.WSO_WorkFlow_Screenshot;
        }

        private void PictureBoxWSOWorkFlowScreenShot_Click(object sender, EventArgs e)
        {

        }
    }
}
