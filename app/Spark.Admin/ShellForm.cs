using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Spark.Admin.Common;

namespace Spark.Admin
{
    public partial class ShellForm : Form
    {
        public ShellForm()
        {
            InitializeComponent();
        }

        #region Properties
        public new Int32 Width
        {
            get
            {
                return base.Width;
            }
            set
            {
                base.Width = value;
                ShellTabWorkspace.Width = value;
            }
        }

        public new Int32 Height
        {
            get
            {
                return base.Height;
            }
            set
            {
                base.Height = value;
                ShellTabWorkspace.Height = value;
            }
        }
        #endregion
    }
}