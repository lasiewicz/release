*** Deployment using VS 2010 *** 

1. Set the build option to release in the solution property.
2. Set the 'Platform Target' of all the projects to 'x86'.
3. Change the publish version number in Spark.Admin properties -> Publish. Make sure to use a different version number each time.
4. Publish Spark.Admin project to \\ladevweb01\c$\Inetpub\Spark.Admin\. 

Each developer who worked on this didn't know the password to Spark.Admin_TemporaryKey.pfx file, so multiple versions were created.  The password
for Spark.Admin_4_TemporaryKey.pfx is "matchnet".  If that doesn't work, create another test certificate under Properites --> Signing.

*** Deployment using VS 2008 *** 

2/20/2009

For making a build on LADEVWEB01

1. Set the build option to release in the solution property. Release|Any CPU
2. Publish Spark.Admin project to \\ladevweb01\c$\Inetpub\Spark.Admin\. Make sure to use a different version number each time.
3. Copy just .dll files from C:\matchnet\bedrock\App\Spark.Admin\bin\Release to the Spark.Admin_X_X_X directory.
4. Rename Spark.Admin.application to Spark.Admin.application.application.
5. Run mageui.exe from VS 2008 command prompt.
6. Select the .manifest file. Repopulate files. This will add .deploy to all the files inside the directory.
7. Create new deployment application, select the manifest file. Save (overwrite) Spark.Admin.application i nthe root directory
8. Move Spark.Admin_X_X_X to the root directory.


### This is from prior VS version ###

This application follows Microsoft's "Smart Client - Composite UI Application Block" - A pattern 
	for creating Smart Client applications.

	For more information:
	http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dnpag2/html/cab.asp
	
	
Notes regarding deployment:
- You must follow the directions at ms-help://MS.VSCC.v80/MS.VSIPCC.v80/ms.practices.2005Nov.cab/CAB/html/05-020-Using%20ClickOnce%20with%20Composite%20UI%20Application%20Block%20Applications.htm
	in order for the ClickOnce deployment to work (the help link above assumes you have the Composite UI Application Block installed).
	For the step entitled "To copy the modules"
	use the DeployModules.bat file with the appropriate arguments.  Be sure to edit the "releaseFolder" variable in the .bat before running it.

	
Notes regarding Module development:
- You must make sure that the Module projects have file references (NOT Project references) to
	the Spark.Admin project or you will receive a "error MSB3113: Could not find file 'Microsoft.Windows.CommonLanguageRuntime, Version=2.0.50727.0'."
	error.  This is a known bug (http://support.microsoft.com/?kbid=907757).
- Make sure to include all DLL files when recreating the manifest.
- mageui.exe crashes the first time, just restart it.
- Make sure to use Release mode when pushing to other servers because debug mode points to localhost



