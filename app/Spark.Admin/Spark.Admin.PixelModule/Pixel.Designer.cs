namespace Spark.Admin.PixelModule
{
    partial class Pixel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PagePixelIDLabel = new System.Windows.Forms.Label();
            this.PagePixelIDTextBox = new System.Windows.Forms.TextBox();
            this.SiteLabel = new System.Windows.Forms.Label();
            this.SitesComboBox = new System.Windows.Forms.ComboBox();
            this.PageIDTextBox = new System.Windows.Forms.TextBox();
            this.PageIDLabel = new System.Windows.Forms.Label();
            this.PublishIDTextBox = new System.Windows.Forms.TextBox();
            this.PublishIDLabel = new System.Windows.Forms.Label();
            this.StatusGroupBox = new System.Windows.Forms.GroupBox();
            this.OnHoldRadioButton = new System.Windows.Forms.RadioButton();
            this.LiveRadioButton = new System.Windows.Forms.RadioButton();
            this.TextRadioButton = new System.Windows.Forms.RadioButton();
            this.ImageRadioButton = new System.Windows.Forms.RadioButton();
            this.TypeGroupBox = new System.Windows.Forms.GroupBox();
            this.EndDateLabel = new System.Windows.Forms.Label();
            this.EndDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.EndDateNotesLabel = new System.Windows.Forms.Label();
            this.DescriptionTextBox = new System.Windows.Forms.TextBox();
            this.DescriptionLabel = new System.Windows.Forms.Label();
            this.ContentLabel = new System.Windows.Forms.Label();
            this.FiltersLabel = new System.Windows.Forms.Label();
            this.ParametersLabel = new System.Windows.Forms.Label();
            this.CommandsPanel = new System.Windows.Forms.Panel();
            this.SCancelButton = new System.Windows.Forms.Button();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.PreviewButton = new System.Windows.Forms.Button();
            this.TestJavaScriptButton = new System.Windows.Forms.Button();
            this.NextStepButton = new System.Windows.Forms.Button();
            this.FilterAddButton = new System.Windows.Forms.Button();
            this.FilterEditButton = new System.Windows.Forms.Button();
            this.FilterDeleteButton = new System.Windows.Forms.Button();
            this.ParameterDeleteButton = new System.Windows.Forms.Button();
            this.ParameterEditButton = new System.Windows.Forms.Button();
            this.ParameterAddButton = new System.Windows.Forms.Button();
            this.ContentRichTextBox = new System.Windows.Forms.RichTextBox();
            this.FiltersListBox = new System.Windows.Forms.ListBox();
            this.ParametersListBox = new System.Windows.Forms.ListBox();
            this.HtmlBrowser = new System.Windows.Forms.WebBrowser();
            this.ServiceEnvironmentLabel = new System.Windows.Forms.Label();
            this.AllPagesLabel = new System.Windows.Forms.Label();
            this.StatusGroupBox.SuspendLayout();
            this.TypeGroupBox.SuspendLayout();
            this.CommandsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PagePixelIDLabel
            // 
            this.PagePixelIDLabel.AutoSize = true;
            this.PagePixelIDLabel.Location = new System.Drawing.Point(12, 9);
            this.PagePixelIDLabel.Name = "PagePixelIDLabel";
            this.PagePixelIDLabel.Size = new System.Drawing.Size(65, 13);
            this.PagePixelIDLabel.TabIndex = 0;
            this.PagePixelIDLabel.Text = "PagePixelID";
            this.PagePixelIDLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // PagePixelIDTextBox
            // 
            this.PagePixelIDTextBox.Location = new System.Drawing.Point(83, 6);
            this.PagePixelIDTextBox.Name = "PagePixelIDTextBox";
            this.PagePixelIDTextBox.ReadOnly = true;
            this.PagePixelIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.PagePixelIDTextBox.TabIndex = 1;
            // 
            // SiteLabel
            // 
            this.SiteLabel.AutoSize = true;
            this.SiteLabel.Location = new System.Drawing.Point(52, 37);
            this.SiteLabel.Name = "SiteLabel";
            this.SiteLabel.Size = new System.Drawing.Size(25, 13);
            this.SiteLabel.TabIndex = 2;
            this.SiteLabel.Text = "Site";
            // 
            // SitesComboBox
            // 
            this.SitesComboBox.FormattingEnabled = true;
            this.SitesComboBox.Location = new System.Drawing.Point(83, 34);
            this.SitesComboBox.Name = "SitesComboBox";
            this.SitesComboBox.Size = new System.Drawing.Size(180, 21);
            this.SitesComboBox.TabIndex = 3;
            // 
            // PageIDTextBox
            // 
            this.PageIDTextBox.Location = new System.Drawing.Point(329, 34);
            this.PageIDTextBox.Name = "PageIDTextBox";
            this.PageIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.PageIDTextBox.TabIndex = 5;
            // 
            // PageIDLabel
            // 
            this.PageIDLabel.AutoSize = true;
            this.PageIDLabel.Location = new System.Drawing.Point(280, 38);
            this.PageIDLabel.Name = "PageIDLabel";
            this.PageIDLabel.Size = new System.Drawing.Size(43, 13);
            this.PageIDLabel.TabIndex = 4;
            this.PageIDLabel.Text = "PageID";
            this.PageIDLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // PublishIDTextBox
            // 
            this.PublishIDTextBox.Location = new System.Drawing.Point(513, 5);
            this.PublishIDTextBox.Name = "PublishIDTextBox";
            this.PublishIDTextBox.ReadOnly = true;
            this.PublishIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.PublishIDTextBox.TabIndex = 7;
            // 
            // PublishIDLabel
            // 
            this.PublishIDLabel.AutoSize = true;
            this.PublishIDLabel.Location = new System.Drawing.Point(455, 9);
            this.PublishIDLabel.Name = "PublishIDLabel";
            this.PublishIDLabel.Size = new System.Drawing.Size(52, 13);
            this.PublishIDLabel.TabIndex = 6;
            this.PublishIDLabel.Text = "PublishID";
            this.PublishIDLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // StatusGroupBox
            // 
            this.StatusGroupBox.Controls.Add(this.OnHoldRadioButton);
            this.StatusGroupBox.Controls.Add(this.LiveRadioButton);
            this.StatusGroupBox.Location = new System.Drawing.Point(83, 60);
            this.StatusGroupBox.Name = "StatusGroupBox";
            this.StatusGroupBox.Size = new System.Drawing.Size(180, 45);
            this.StatusGroupBox.TabIndex = 8;
            this.StatusGroupBox.TabStop = false;
            this.StatusGroupBox.Text = "Status";
            // 
            // OnHoldRadioButton
            // 
            this.OnHoldRadioButton.AutoSize = true;
            this.OnHoldRadioButton.Location = new System.Drawing.Point(96, 18);
            this.OnHoldRadioButton.Name = "OnHoldRadioButton";
            this.OnHoldRadioButton.Size = new System.Drawing.Size(74, 17);
            this.OnHoldRadioButton.TabIndex = 10;
            this.OnHoldRadioButton.TabStop = true;
            this.OnHoldRadioButton.Text = "ON-HOLD";
            this.OnHoldRadioButton.UseVisualStyleBackColor = true;
            // 
            // LiveRadioButton
            // 
            this.LiveRadioButton.AutoSize = true;
            this.LiveRadioButton.Location = new System.Drawing.Point(23, 18);
            this.LiveRadioButton.Name = "LiveRadioButton";
            this.LiveRadioButton.Size = new System.Drawing.Size(48, 17);
            this.LiveRadioButton.TabIndex = 9;
            this.LiveRadioButton.TabStop = true;
            this.LiveRadioButton.Text = "LIVE";
            this.LiveRadioButton.UseVisualStyleBackColor = true;
            // 
            // TextRadioButton
            // 
            this.TextRadioButton.AutoSize = true;
            this.TextRadioButton.Location = new System.Drawing.Point(105, 18);
            this.TextRadioButton.Name = "TextRadioButton";
            this.TextRadioButton.Size = new System.Drawing.Size(53, 17);
            this.TextRadioButton.TabIndex = 10;
            this.TextRadioButton.TabStop = true;
            this.TextRadioButton.Text = "TEXT";
            this.TextRadioButton.UseVisualStyleBackColor = true;
            this.TextRadioButton.CheckedChanged += new System.EventHandler(this.TextRadioButton_CheckedChanged);
            // 
            // ImageRadioButton
            // 
            this.ImageRadioButton.AutoSize = true;
            this.ImageRadioButton.Location = new System.Drawing.Point(23, 18);
            this.ImageRadioButton.Name = "ImageRadioButton";
            this.ImageRadioButton.Size = new System.Drawing.Size(59, 17);
            this.ImageRadioButton.TabIndex = 9;
            this.ImageRadioButton.TabStop = true;
            this.ImageRadioButton.Text = "IMAGE";
            this.ImageRadioButton.UseVisualStyleBackColor = true;
            // 
            // TypeGroupBox
            // 
            this.TypeGroupBox.Controls.Add(this.TextRadioButton);
            this.TypeGroupBox.Controls.Add(this.ImageRadioButton);
            this.TypeGroupBox.Location = new System.Drawing.Point(283, 60);
            this.TypeGroupBox.Name = "TypeGroupBox";
            this.TypeGroupBox.Size = new System.Drawing.Size(180, 45);
            this.TypeGroupBox.TabIndex = 11;
            this.TypeGroupBox.TabStop = false;
            this.TypeGroupBox.Text = "Type";
            // 
            // EndDateLabel
            // 
            this.EndDateLabel.AutoSize = true;
            this.EndDateLabel.Location = new System.Drawing.Point(25, 115);
            this.EndDateLabel.Name = "EndDateLabel";
            this.EndDateLabel.Size = new System.Drawing.Size(52, 13);
            this.EndDateLabel.TabIndex = 12;
            this.EndDateLabel.Text = "End Date";
            // 
            // EndDateDateTimePicker
            // 
            this.EndDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.EndDateDateTimePicker.Location = new System.Drawing.Point(83, 111);
            this.EndDateDateTimePicker.Name = "EndDateDateTimePicker";
            this.EndDateDateTimePicker.Size = new System.Drawing.Size(180, 20);
            this.EndDateDateTimePicker.TabIndex = 13;
            this.EndDateDateTimePicker.Value = new System.DateTime(1999, 1, 1, 0, 0, 0, 0);
            // 
            // EndDateNotesLabel
            // 
            this.EndDateNotesLabel.AutoSize = true;
            this.EndDateNotesLabel.Location = new System.Drawing.Point(283, 115);
            this.EndDateNotesLabel.Name = "EndDateNotesLabel";
            this.EndDateNotesLabel.Size = new System.Drawing.Size(180, 13);
            this.EndDateNotesLabel.TabIndex = 14;
            this.EndDateNotesLabel.Text = "(Select \"1/1/1999\" for no End Date)";
            // 
            // DescriptionTextBox
            // 
            this.DescriptionTextBox.Location = new System.Drawing.Point(83, 137);
            this.DescriptionTextBox.Name = "DescriptionTextBox";
            this.DescriptionTextBox.Size = new System.Drawing.Size(530, 20);
            this.DescriptionTextBox.TabIndex = 16;
            // 
            // DescriptionLabel
            // 
            this.DescriptionLabel.AutoSize = true;
            this.DescriptionLabel.Location = new System.Drawing.Point(17, 140);
            this.DescriptionLabel.Name = "DescriptionLabel";
            this.DescriptionLabel.Size = new System.Drawing.Size(60, 13);
            this.DescriptionLabel.TabIndex = 15;
            this.DescriptionLabel.Text = "Description";
            this.DescriptionLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ContentLabel
            // 
            this.ContentLabel.AutoSize = true;
            this.ContentLabel.Location = new System.Drawing.Point(33, 166);
            this.ContentLabel.Name = "ContentLabel";
            this.ContentLabel.Size = new System.Drawing.Size(44, 13);
            this.ContentLabel.TabIndex = 17;
            this.ContentLabel.Text = "Content";
            this.ContentLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // FiltersLabel
            // 
            this.FiltersLabel.AutoSize = true;
            this.FiltersLabel.Location = new System.Drawing.Point(43, 294);
            this.FiltersLabel.Name = "FiltersLabel";
            this.FiltersLabel.Size = new System.Drawing.Size(34, 13);
            this.FiltersLabel.TabIndex = 19;
            this.FiltersLabel.Text = "Filters";
            this.FiltersLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ParametersLabel
            // 
            this.ParametersLabel.AutoSize = true;
            this.ParametersLabel.Location = new System.Drawing.Point(17, 383);
            this.ParametersLabel.Name = "ParametersLabel";
            this.ParametersLabel.Size = new System.Drawing.Size(60, 13);
            this.ParametersLabel.TabIndex = 21;
            this.ParametersLabel.Text = "Parameters";
            this.ParametersLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // CommandsPanel
            // 
            this.CommandsPanel.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.CommandsPanel.Controls.Add(this.SCancelButton);
            this.CommandsPanel.Controls.Add(this.DeleteButton);
            this.CommandsPanel.Controls.Add(this.SaveButton);
            this.CommandsPanel.Controls.Add(this.PreviewButton);
            this.CommandsPanel.Controls.Add(this.TestJavaScriptButton);
            this.CommandsPanel.Controls.Add(this.NextStepButton);
            this.CommandsPanel.Location = new System.Drawing.Point(12, 469);
            this.CommandsPanel.Name = "CommandsPanel";
            this.CommandsPanel.Size = new System.Drawing.Size(601, 53);
            this.CommandsPanel.TabIndex = 23;
            // 
            // SCancelButton
            // 
            this.SCancelButton.Location = new System.Drawing.Point(498, 16);
            this.SCancelButton.Name = "SCancelButton";
            this.SCancelButton.Size = new System.Drawing.Size(92, 23);
            this.SCancelButton.TabIndex = 35;
            this.SCancelButton.Text = "Cancel";
            this.SCancelButton.UseVisualStyleBackColor = true;
            this.SCancelButton.Click += new System.EventHandler(this.SCancelButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.Location = new System.Drawing.Point(400, 16);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(92, 23);
            this.DeleteButton.TabIndex = 34;
            this.DeleteButton.Text = "Delete";
            this.DeleteButton.UseVisualStyleBackColor = true;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(302, 16);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(92, 23);
            this.SaveButton.TabIndex = 33;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // PreviewButton
            // 
            this.PreviewButton.Location = new System.Drawing.Point(204, 16);
            this.PreviewButton.Name = "PreviewButton";
            this.PreviewButton.Size = new System.Drawing.Size(92, 23);
            this.PreviewButton.TabIndex = 32;
            this.PreviewButton.Text = "Preview";
            this.PreviewButton.UseVisualStyleBackColor = true;
            this.PreviewButton.Click += new System.EventHandler(this.PreviewButton_Click);
            // 
            // TestJavaScriptButton
            // 
            this.TestJavaScriptButton.Location = new System.Drawing.Point(106, 16);
            this.TestJavaScriptButton.Name = "TestJavaScriptButton";
            this.TestJavaScriptButton.Size = new System.Drawing.Size(92, 23);
            this.TestJavaScriptButton.TabIndex = 31;
            this.TestJavaScriptButton.Text = "Test JavaScript";
            this.TestJavaScriptButton.UseVisualStyleBackColor = true;
            this.TestJavaScriptButton.Click += new System.EventHandler(this.TestJavaScriptButton_Click);
            // 
            // NextStepButton
            // 
            this.NextStepButton.Location = new System.Drawing.Point(8, 9);
            this.NextStepButton.Name = "NextStepButton";
            this.NextStepButton.Size = new System.Drawing.Size(92, 36);
            this.NextStepButton.TabIndex = 30;
            this.NextStepButton.Text = "NextStep";
            this.NextStepButton.UseVisualStyleBackColor = true;
            this.NextStepButton.Click += new System.EventHandler(this.NextStepButton_Click);
            // 
            // FilterAddButton
            // 
            this.FilterAddButton.Location = new System.Drawing.Point(538, 291);
            this.FilterAddButton.Name = "FilterAddButton";
            this.FilterAddButton.Size = new System.Drawing.Size(75, 23);
            this.FilterAddButton.TabIndex = 24;
            this.FilterAddButton.Text = "Add";
            this.FilterAddButton.UseVisualStyleBackColor = true;
            this.FilterAddButton.Click += new System.EventHandler(this.FilterAddButton_Click);
            // 
            // FilterEditButton
            // 
            this.FilterEditButton.Location = new System.Drawing.Point(538, 320);
            this.FilterEditButton.Name = "FilterEditButton";
            this.FilterEditButton.Size = new System.Drawing.Size(75, 23);
            this.FilterEditButton.TabIndex = 25;
            this.FilterEditButton.Text = "Edit";
            this.FilterEditButton.UseVisualStyleBackColor = true;
            this.FilterEditButton.Click += new System.EventHandler(this.FilterEditButton_Click);
            // 
            // FilterDeleteButton
            // 
            this.FilterDeleteButton.Location = new System.Drawing.Point(538, 348);
            this.FilterDeleteButton.Name = "FilterDeleteButton";
            this.FilterDeleteButton.Size = new System.Drawing.Size(75, 23);
            this.FilterDeleteButton.TabIndex = 26;
            this.FilterDeleteButton.Text = "Delete";
            this.FilterDeleteButton.UseVisualStyleBackColor = true;
            this.FilterDeleteButton.Click += new System.EventHandler(this.FilterDeleteButton_Click);
            // 
            // ParameterDeleteButton
            // 
            this.ParameterDeleteButton.Location = new System.Drawing.Point(538, 437);
            this.ParameterDeleteButton.Name = "ParameterDeleteButton";
            this.ParameterDeleteButton.Size = new System.Drawing.Size(75, 23);
            this.ParameterDeleteButton.TabIndex = 29;
            this.ParameterDeleteButton.Text = "Delete";
            this.ParameterDeleteButton.UseVisualStyleBackColor = true;
            this.ParameterDeleteButton.Click += new System.EventHandler(this.ParameterDeleteButton_Click);
            // 
            // ParameterEditButton
            // 
            this.ParameterEditButton.Location = new System.Drawing.Point(538, 409);
            this.ParameterEditButton.Name = "ParameterEditButton";
            this.ParameterEditButton.Size = new System.Drawing.Size(75, 23);
            this.ParameterEditButton.TabIndex = 28;
            this.ParameterEditButton.Text = "Edit";
            this.ParameterEditButton.UseVisualStyleBackColor = true;
            this.ParameterEditButton.Click += new System.EventHandler(this.ParameterEditButton_Click);
            // 
            // ParameterAddButton
            // 
            this.ParameterAddButton.Location = new System.Drawing.Point(538, 380);
            this.ParameterAddButton.Name = "ParameterAddButton";
            this.ParameterAddButton.Size = new System.Drawing.Size(75, 23);
            this.ParameterAddButton.TabIndex = 27;
            this.ParameterAddButton.Text = "Add";
            this.ParameterAddButton.UseVisualStyleBackColor = true;
            this.ParameterAddButton.Click += new System.EventHandler(this.ParameterAddButton_Click);
            // 
            // ContentRichTextBox
            // 
            this.ContentRichTextBox.Location = new System.Drawing.Point(83, 163);
            this.ContentRichTextBox.Name = "ContentRichTextBox";
            this.ContentRichTextBox.Size = new System.Drawing.Size(530, 122);
            this.ContentRichTextBox.TabIndex = 36;
            this.ContentRichTextBox.Text = "";
            this.ContentRichTextBox.TextChanged += new System.EventHandler(this.ContentRichTextBox_TextChanged);
            // 
            // FiltersListBox
            // 
            this.FiltersListBox.FormattingEnabled = true;
            this.FiltersListBox.Location = new System.Drawing.Point(83, 291);
            this.FiltersListBox.Name = "FiltersListBox";
            this.FiltersListBox.Size = new System.Drawing.Size(449, 82);
            this.FiltersListBox.TabIndex = 37;
            // 
            // ParametersListBox
            // 
            this.ParametersListBox.FormattingEnabled = true;
            this.ParametersListBox.Location = new System.Drawing.Point(83, 380);
            this.ParametersListBox.Name = "ParametersListBox";
            this.ParametersListBox.Size = new System.Drawing.Size(449, 82);
            this.ParametersListBox.TabIndex = 38;
            // 
            // HtmlBrowser
            // 
            this.HtmlBrowser.Location = new System.Drawing.Point(595, 60);
            this.HtmlBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.HtmlBrowser.Name = "HtmlBrowser";
            this.HtmlBrowser.ScrollBarsEnabled = false;
            this.HtmlBrowser.Size = new System.Drawing.Size(20, 20);
            this.HtmlBrowser.TabIndex = 36;
            // 
            // ServiceEnvironmentLabel
            // 
            this.ServiceEnvironmentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ServiceEnvironmentLabel.Location = new System.Drawing.Point(189, 5);
            this.ServiceEnvironmentLabel.Name = "ServiceEnvironmentLabel";
            this.ServiceEnvironmentLabel.Size = new System.Drawing.Size(260, 21);
            this.ServiceEnvironmentLabel.TabIndex = 39;
            this.ServiceEnvironmentLabel.Text = "DEV";
            this.ServiceEnvironmentLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AllPagesLabel
            // 
            this.AllPagesLabel.AutoSize = true;
            this.AllPagesLabel.Location = new System.Drawing.Point(433, 37);
            this.AllPagesLabel.Name = "AllPagesLabel";
            this.AllPagesLabel.Size = new System.Drawing.Size(147, 13);
            this.AllPagesLabel.TabIndex = 40;
            this.AllPagesLabel.Text = "(Enter \"999999\" for all pages)";
            // 
            // Pixel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 532);
            this.Controls.Add(this.AllPagesLabel);
            this.Controls.Add(this.ServiceEnvironmentLabel);
            this.Controls.Add(this.HtmlBrowser);
            this.Controls.Add(this.ParametersListBox);
            this.Controls.Add(this.FiltersListBox);
            this.Controls.Add(this.ContentRichTextBox);
            this.Controls.Add(this.ParameterDeleteButton);
            this.Controls.Add(this.ParameterEditButton);
            this.Controls.Add(this.ParameterAddButton);
            this.Controls.Add(this.FilterDeleteButton);
            this.Controls.Add(this.FilterEditButton);
            this.Controls.Add(this.FilterAddButton);
            this.Controls.Add(this.CommandsPanel);
            this.Controls.Add(this.ParametersLabel);
            this.Controls.Add(this.FiltersLabel);
            this.Controls.Add(this.ContentLabel);
            this.Controls.Add(this.DescriptionTextBox);
            this.Controls.Add(this.DescriptionLabel);
            this.Controls.Add(this.EndDateNotesLabel);
            this.Controls.Add(this.EndDateDateTimePicker);
            this.Controls.Add(this.EndDateLabel);
            this.Controls.Add(this.TypeGroupBox);
            this.Controls.Add(this.StatusGroupBox);
            this.Controls.Add(this.PublishIDTextBox);
            this.Controls.Add(this.PublishIDLabel);
            this.Controls.Add(this.PageIDTextBox);
            this.Controls.Add(this.PageIDLabel);
            this.Controls.Add(this.SitesComboBox);
            this.Controls.Add(this.SiteLabel);
            this.Controls.Add(this.PagePixelIDTextBox);
            this.Controls.Add(this.PagePixelIDLabel);
            this.Name = "Pixel";
            this.Text = "Pixel";
            this.Load += new System.EventHandler(this.Pixel_Load);
            this.StatusGroupBox.ResumeLayout(false);
            this.StatusGroupBox.PerformLayout();
            this.TypeGroupBox.ResumeLayout(false);
            this.TypeGroupBox.PerformLayout();
            this.CommandsPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label PagePixelIDLabel;
        private System.Windows.Forms.TextBox PagePixelIDTextBox;
        private System.Windows.Forms.Label SiteLabel;
        private System.Windows.Forms.ComboBox SitesComboBox;
        private System.Windows.Forms.TextBox PageIDTextBox;
        private System.Windows.Forms.Label PageIDLabel;
        private System.Windows.Forms.TextBox PublishIDTextBox;
        private System.Windows.Forms.Label PublishIDLabel;
        private System.Windows.Forms.GroupBox StatusGroupBox;
        private System.Windows.Forms.RadioButton LiveRadioButton;
        private System.Windows.Forms.RadioButton OnHoldRadioButton;
        private System.Windows.Forms.RadioButton TextRadioButton;
        private System.Windows.Forms.RadioButton ImageRadioButton;
        private System.Windows.Forms.GroupBox TypeGroupBox;
        private System.Windows.Forms.Label EndDateLabel;
        private System.Windows.Forms.DateTimePicker EndDateDateTimePicker;
        private System.Windows.Forms.Label EndDateNotesLabel;
        private System.Windows.Forms.TextBox DescriptionTextBox;
        private System.Windows.Forms.Label DescriptionLabel;
        private System.Windows.Forms.Label ContentLabel;
        private System.Windows.Forms.Label FiltersLabel;
        private System.Windows.Forms.Label ParametersLabel;
        private System.Windows.Forms.Panel CommandsPanel;
        private System.Windows.Forms.Button FilterAddButton;
        private System.Windows.Forms.Button FilterEditButton;
        private System.Windows.Forms.Button FilterDeleteButton;
        private System.Windows.Forms.Button ParameterDeleteButton;
        private System.Windows.Forms.Button ParameterEditButton;
        private System.Windows.Forms.Button ParameterAddButton;
        private System.Windows.Forms.Button SCancelButton;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button PreviewButton;
        private System.Windows.Forms.Button TestJavaScriptButton;
        private System.Windows.Forms.Button NextStepButton;
        private System.Windows.Forms.RichTextBox ContentRichTextBox;
        private System.Windows.Forms.ListBox FiltersListBox;
        private System.Windows.Forms.ListBox ParametersListBox;
        private System.Windows.Forms.WebBrowser HtmlBrowser;
        private System.Windows.Forms.Label ServiceEnvironmentLabel;
        private System.Windows.Forms.Label AllPagesLabel;
    }
}