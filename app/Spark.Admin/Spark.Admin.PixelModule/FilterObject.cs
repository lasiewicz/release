using System;
using System.Collections;
using System.Text;
using System.Xml;
using System.Windows.Forms;

namespace Spark.Admin.PixelModule
{
    /// <summary>
    /// Summary description for FilterObject.
    /// </summary>
    public class FilterObject
    {
        #region Enums and Constants
        public const string DelimiterOr = "|";
        public static readonly FilterType[] FilterTypeList = new FilterType[] {new FilterType("Country ID Filter", ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID, "Enter a country's RegionID value (i.e. 223 for USA, 38 for Canada, 105 for Israel, 83 for Germany, 222 for UK, 13 for Australia, 76 for France, 10 for Argentina).  Multiple values can be entered by separating with " + FilterObject.DelimiterOr + " (i.e. 223" + FilterObject.DelimiterOr + "224)."),
													new FilterType("GenderMask Filter", ArgumentObject.ArgumentTypeMaskEnum.GenderMask, "Enter a GenderMask value.  Possibilities include:  M seeking F = 9, F seeking M = 6, F seeking F = 10, M seeking M = 5.  Multiple values can be entered by separating with " + FilterObject.DelimiterOr + "."),
													new FilterType("Promotion Filter", ArgumentObject.ArgumentTypeMaskEnum.PromotionID, "Enter a Promotion ID.  Multiple values can be entered by separating with " + FilterObject.DelimiterOr+ " (i.e. 10000" + FilterObject.DelimiterOr + "10001)."),
													new FilterType("Birth Date Filter", ArgumentObject.ArgumentTypeMaskEnum.BirthDate, "Enter a date in the form 'YYYYMMDD'(i.e. 19730630 for June 30th, 1973).  This is good to use with the > or < comparison operators."),
													new FilterType("Member ID Filter", ArgumentObject.ArgumentTypeMaskEnum.MemberID, "Use this along with a > comparison operator to filter Registered users.  'Key > 0' will filter for MemberIDs greater than 0, thus returning all users with valid MemberIDs."),
													new FilterType("Subscribed Filter", ArgumentObject.ArgumentTypeMaskEnum.IsPayingMemberFlag, "Enter 1 to filter all Subscribed members.  Enter 0 for all non-Subscribed members."),
													new FilterType("Landing Page URL Filter", ArgumentObject.ArgumentTypeMaskEnum.LandingPageURL, "Enter a valid URL (i.e. 'http://www.yahoo.com').  Multiple values can be entered by separating with " + FilterObject.DelimiterOr + " (i.e. http://www.msn.com" + FilterObject.DelimiterOr + "http://www.yahoo.com)"),
													new FilterType("Search Preference - GenderMask Filter", ArgumentObject.ArgumentTypeMaskEnum.GenderMask_SearchPref, "Enter a Search Preference GenderMask value.  Possibilities include:  M seeking F = 9, F seeking M = 6, F seeking F = 10, M seeking M = 5.  Multiple values can be entered by separating with " + FilterObject.DelimiterOr + "."),
                                                    new FilterType("LGID Filter", ArgumentObject.ArgumentTypeMaskEnum.LGID, "Enter a LGID.")};
                                                    
        public static readonly ComparisonOperator[] ComparisonOperatorList = new ComparisonOperator[] {new ComparisonOperator("equal to", "="),
													new ComparisonOperator("not equal to", "!="),
													new ComparisonOperator("less than", "<"),
													new ComparisonOperator("less than or equal to", "<="),
													new ComparisonOperator("greater than", ">"),
													new ComparisonOperator("greater than or equal to", ">="),
													new ComparisonOperator("contains the words", "contains")};
        #endregion

        #region Private Variables
        private ArgumentObject.ArgumentTypeMaskEnum argumentTypeMask;
        private String filterTypeName;
        private String comparisonOperator;
        private String comparisonOperatorName;
        private String val;
        private String key;
        #endregion

        #region Constructors
        public FilterObject(ArgumentObject.ArgumentTypeMaskEnum argumentTypeMask, String comparisonOperator, String val)
        {
            this.argumentTypeMask = argumentTypeMask;
            this.comparisonOperator = comparisonOperator;
            this.val = val;

            SetReadOnlyProperties(argumentTypeMask);
        }
        #endregion

        #region Public Methods
        public static string GetComparisonOperatorName(string ComparisonOperator)
        {
            // Note:  < and > are not allowed in XmlDocuments.  Thus, we use their escape character sequence.
            switch (ComparisonOperator)
            {
                case "=":
                    return "equal to";
                case "!=":
                    return "not equal to";
                case "&lt;":
                    return "less than";
                case "&lt;=":
                    return "less than or equal to";
                case "&gt;":
                    return "greater than";
                case "&gt;=":
                    return "greater than or equal to";
                case "contains":
                    return "contains the words";
                default:
                    return ComparisonOperator;
            }
        }

        /// <summary>
        /// The <code>ContentCondition</code> hold information for both <code>Filters</code> and <code>Parameters</code>.  The code
        /// to create the the ContentCondition is located here, in the <code>FilterObject</code> class but it also makes a call to the
        /// <code>ParameterObject</code> class.
        /// </summary>
        /// <param name="filters"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static String BuildContentConditionXml(ListBox filters, ListBox parameters)
        {
            if (filters.Items.Count == 0 && parameters.Items.Count == 0)
            {
                return Matchnet.Constants.NULL_STRING;
            }
            else
            {
                StringBuilder xmlString = new StringBuilder();

                // Add main opening tags.
                xmlString.Append("<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">");
                xmlString.Append("<xsl:template match=\"/\">");
                xmlString.Append("<pagePixel>");

                // Add the ContentCondtions opening tags.
                for (Int16 i = 0; i < filters.Items.Count; i++)
                {
                    String valueHolder = ((FilterObject)(filters.Items[i])).Value;

                    if (((FilterObject)(filters.Items[i])).ComparisonOperator != "contains")
                    {
                        xmlString.Append("<xsl:if test=\"");
                        // Add a loop for possible ";" ("or" clause).
                        do
                        {
                            xmlString.Append(ArgumentObject.GetArgumentInfo(((FilterObject)(filters.Items[i])).ArgumentTypeMask));
                            xmlString.Append(" ");
                            xmlString.Append(System.Web.HttpUtility.HtmlEncode(((FilterObject)(filters.Items[i])).ComparisonOperator));

                            xmlString.Append(" '");
                            // Retrieve the value up to the first DelimiterOr.
                            // If there is no more delimiters, the current Value goes to the end of the string.
                            if (valueHolder.IndexOf(DelimiterOr) != -1)
                            {
                                xmlString.Append(valueHolder.Substring(0, valueHolder.IndexOf(DelimiterOr)));
                                xmlString.Append("' or ");

                                // Update the valueHolder value.
                                valueHolder = System.Web.HttpUtility.HtmlEncode(valueHolder.Substring(valueHolder.IndexOf(DelimiterOr) + DelimiterOr.Length));
                            }
                            else
                            {
                                xmlString.Append(valueHolder);
                                xmlString.Append("'");

                                valueHolder = String.Empty;
                            }
                        }
                        while (valueHolder.Length > 0);

                        xmlString.Append("\">");
                    }
                    else
                    {
                        xmlString.Append("<xsl:if test=\"");
                        // Add a loop for possible ";" ("or" clause).
                        do
                        {
                            xmlString.Append("contains(");
                            xmlString.Append(ArgumentObject.GetArgumentInfo(((FilterObject)(filters.Items[i])).ArgumentTypeMask));
                            xmlString.Append(",'");
                            // Retrieve the value up to the first DelimiterOr.
                            // If there is no more delimiters, the current Value goes to the end of the string.
                            if (valueHolder.IndexOf(DelimiterOr) != -1)
                            {
                                xmlString.Append(valueHolder.Substring(0, valueHolder.IndexOf(DelimiterOr)));
                                xmlString.Append("') or ");

                                // Update the valueHolder value.
                                valueHolder = valueHolder.Substring(valueHolder.IndexOf(DelimiterOr) + DelimiterOr.Length);
                            }
                            else
                            {
                                xmlString.Append(valueHolder);
                                xmlString.Append("')");

                                valueHolder = String.Empty;
                            }
                        }
                        while (valueHolder.Length > 0);

                        xmlString.Append("\">");
                    }
                }

                // Add the Dislay tag.
                xmlString.Append("<display>true</display>");

                // Add the parameters.
                xmlString.Append(ParameterObject.BuildParameterXml(parameters));

                // Add the ContentCondtions closing tags.
                for (Int16 i = 0; i < filters.Items.Count; i++)
                {
                    xmlString.Append("</xsl:if>");
                }

                // Add main closing tags.
                xmlString.Append("</pagePixel>");
                xmlString.Append("</xsl:template>");
                xmlString.Append("</xsl:stylesheet>");

                return xmlString.ToString();
            }
        }
        #endregion

        #region Private Methods
        private void SetReadOnlyProperties(ArgumentObject.ArgumentTypeMaskEnum argumentTypeMask)
        {
            comparisonOperatorName = GetComparisonOperatorName(comparisonOperator);

            switch (argumentTypeMask)
            {
                case ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID:
                    filterTypeName = "Country Filter";
                    key = "Country ID";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.GenderMask:
                    filterTypeName = "GenderMask Filter";
                    key = "GenderMask ID";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.PromotionID:
                    filterTypeName = "Promotion Filter";
                    key = "Promotion ID";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.BirthDate:
                    filterTypeName = "Birth Date Filter";
                    key = "Birth Date";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.MemberID:
                    filterTypeName = "Member ID Filter";
                    key = "Member ID";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.IsPayingMemberFlag:
                    filterTypeName = "Subscribed Filter";
                    key = "Is Subscribed";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.LandingPageURL:
                    filterTypeName = "Landing Page URL Filter";
                    key = "Landing Page URL";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.GenderMask_SearchPref:
                    filterTypeName = "Search Preferences - GenderMask Filter";
                    key = "Search Preferences - GenderMask ID";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.LGID:
                    filterTypeName = "LGID Filter";
                    key = "LGID";
                    break;
                default:
                    break;
            }
        }
        #endregion

        #region Public Properties
        public String FilterTypeName
        {
            get
            {
                return filterTypeName;
            }
        }

        public String Key
        {
            get
            {
                return key;
            }
        }

        public ArgumentObject.ArgumentTypeMaskEnum ArgumentTypeMask
        {
            get
            {
                return argumentTypeMask;
            }
            set
            {
                argumentTypeMask = value;
            }
        }

        public String ComparisonOperator
        {
            get
            {
                return comparisonOperator;
            }
            set
            {
                comparisonOperator = value;
            }
        }

        public String ComparisonOperatorName
        {
            get
            {
                return comparisonOperatorName;
            }
            set
            {
                comparisonOperatorName = value;
            }
        }

        public String Value
        {
            get
            {
                return val;
            }
            set
            {
                val = value;
            }
        }

        public String DisplayName
        {
            get
            {
                return filterTypeName + ": " + key + " " + comparisonOperatorName + " " + val;
            }
        }
        #endregion
    }

    public class FilterType
    {
        private String name;
        private ArgumentObject.ArgumentTypeMaskEnum argumentTypeMaskEnum;
        private String hint;

        #region Constructors
        public FilterType(String name, ArgumentObject.ArgumentTypeMaskEnum argumentTypeMaskEnum, String hint)
        {
            this.name = name;
            this.argumentTypeMaskEnum = argumentTypeMaskEnum;
            this.hint = hint;
        }
        #endregion

        #region Properties
        public String Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public ArgumentObject.ArgumentTypeMaskEnum ArgumentTypeMaskEnum
        {
            get
            {
                return argumentTypeMaskEnum;
            }
            set
            {
                argumentTypeMaskEnum = value;
            }
        }

        public String Hint
        {
            get
            {
                return hint;
            }
            set
            {
                hint = value;
            }
        }
        #endregion
    }

    public class ComparisonOperator
    {
        private String name;
        private String sign;

        #region Constructors
        public ComparisonOperator(String name, String sign)
        {
            this.name = name;
            this.sign = sign;
        }
        #endregion

        #region Properties
        public String Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public String Sign
        {
            get
            {
                return sign;
            }
            set
            {
                sign = value;
            }
        }
        #endregion
    }
}

