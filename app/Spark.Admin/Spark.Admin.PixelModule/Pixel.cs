using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Collections;
using System.Web;

using Microsoft.Practices.CompositeUI;

using Spark.Admin.Common;

using Matchnet.Content.ValueObjects.PagePixel;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Admin;

namespace Spark.Admin.PixelModule
{
    public partial class Pixel : Spark.Admin.Common.Controls.Form
    {
        private static readonly DateTime NoEndDate = new DateTime(1999, 1, 1);
        private const Int32 MaxLengthContentCondition = 2000;
        private const Int32 MaxLengthContentConditionArgs = 2000;
        private const String ParameterStringStart = "[*]";
        public const String HtmlTestTemplate = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\"><html><head><title></title></head><body>THIS IS A TEST OF PIXELS.  PLEASE LOOK FOR JAVASCRIPT ERRORS.<BR /><BR />If no error dialog box appeared before this window opened then the Javascript test has been passed.<BR /><BR />{0}<BR /><BR /></body></html>";
        public const String MsgJSTestComplete = "The JavaScript test is complete.   If no error dialog box appeared, then this pixel passed the test.";

        #region Message Constants
        private const String MsgSiteIDAndPageIDRequired = "Please select a Site and enter a valid PageID";
        private const String MSG_SELECT_PIXEL_TO_EDIT = "Please select a pixel to edit.";
        private const String MSG_NOT_AUTHENTICATED = "Please log in to Windows before attempting to use this application.";
        private const String MSG_NOT_AUTHORIZED = "You are not authorized to use any of the tools in this application.";
        private const String MsgFilterParameterCouldNotLoad = "The Filters and Parameters for this pixel could not be loaded correctly.  This is most likely because this pixel was entered manually (before the creation of the Pixel Administrator).  Please re-add the Filters and Parameters and re-save this pixel.";
        private const String MsgFilterNone = "There are no filters to modify.";
        private const String MsgParameterNone = "There are no parameters to modify.";
        private const String MsgFilterSelect = "Please select a filter to modify.";
        private const String MsgParameterSelect = "Please select a parameter to modify.";
        private const String MsgEndDateIsPast = "EndDate can not be in the past.";
        private const String MsgDescriptionRequired = "A Description is required.";
        private const String MsgContentRequired = "Content is required.";
        private const String MsgSubscriptionSecureLinksOnly = "All pixels on 3000 pages (Subscription) must not contain unsecure links (http:).";
        private const String MsgImageTypeNoImgTag = "IMAGE type pixels can not contain an <IMG> tag.";
        private const String MsgImageTypeNoScriptTag = "IMAGE type pixels can not contain a <SCRIPT> tag.";
        private const String MsgTextTypeImgTagOrScriptTag = "TEXT type pixels begin with either an <IMG>, <SCRIPT>, or <IFRAME> tag.";
        private const String MsgTextTypeWidthHeightBorder = "TEXT type pixels must contain width=\"1\" height=\"1\" border=\"0\".";
        private const String MsgParameterStringStartMissing = "If you include parameters in the pixel you must include a \"" + ParameterStringStart + "\" in the Content.";
        private const String MsgContentConditionMaxLength = "The ContentCondition length exceeds the maximum length of 2000.  Please delete some of your filters and/or parameters, or reduce the number of values in the filters.";
        private const String MsgContentConditionArgsMaxLength = "The ContentConditionArgs length exceeds the maximum length of 2000.  Please delete some of your filters or parameters.";
        private const String MsgSavePixelConfirm = "Are you sure you want to save this pixel?";
        private const String MsgNextStepPixelConfirm = "Are you sure you want to {0} this pixel?";
        private const String MsgDeletePixelConfirm = "Are you sure you want to delete this pixel?";
        private const String MsgTextTypeClosingScriptTag = "Every <SCRIPT> tag must have a closing </SCRIPT> tag.";
        private const String MSG_KEY_REQUIRED_ERROR = "You must enter a Key.";
        private const String MSG_MT_ENVIRONMENT_CHANGE = "The operation you requested requires that the application be restarted.  Please manually restart the application after it closes.";
        private const String MsgJSTesAndPreviewRequired = "You must Preview and/or Test the JavaScript before you can save this pixel.";
        #endregion

        #region Publish Constants
        private const Int32 PubActPubObjIDPixelAdd = 100;
        private const Int32 PubActPubObjIDPixelEdit = 101;
        private const Int32 PubActPubObjIDPixelDelete = 102;
        private const Int32 PubActPubObjIDPixelPublishToContentStg = 103;
        private const Int32 PubActPubObjIDPixelVerifyInContentStg = 104;
        private const Int32 PubActPubObjIDPixelApproveInContentStg = 105;
        private const Int32 PubActPubObjIDPixelVerifyWithPartnerInContentStg = 106;
        private const Int32 PubActPubObjIDPixelPublishToProd = 107;
        private const Int32 PubActPubObjIDPixelVerifyInProd = 108;
        private const Int32 PubActPubObjIDPixelVerifyWithPartnerInProd = 109;
        private const String PubActPubObjTextPixelApproveInContentStg = "Approve in ContentStg";
        private const String PubActPubObjTextPixelVerifyInContentStg = "Verify in ContentStg";
        private const String PubActPubObjTextPixelPublishToContentStg = "Publish to ContentStg";
        private const String PubActPubObjTextPixelVerifyWithPartnerInContentStg = "Verify with Partner in CStg";
        private const String PubActPubObjTextPixelPublishToProd = "Publish to Prod";
        private const String PubActPubObjTextPixelVerifyInProd = "Verify in Prod";
        private const String PubActPubObjTextPixelVerifyWithPartnerInProd = "Verify with Partner in Prod";
        #endregion

        private PagePixel pagePixel;
        private PixelsController pixelsController;
        private Int32 nextPublishActionPublishObjectID = Matchnet.Constants.NULL_INT;
        private String nextStepText = String.Empty;
        private Boolean newPixelFlag = false;
        private Boolean jsTestedFlag = false;
        private Boolean previewedFlag = false;

        #region Constructors
        /// <summary>
        /// You must call the base constructor (base(globalState)) or you will have to assign the
        /// GlobalState manually from the caller.
        /// </summary>
        /// <param name="globalState"></param>
        /// <param name="pagePixel"></param>
        public Pixel(State globalState, PixelsController pixelsController, PagePixel pagePixel) : base(globalState)
        {
            InitializeComponent();

            this.pagePixel = pagePixel;
            this.pixelsController = pixelsController;

            pixelsController.InitializeSitesComboBox(ref SitesComboBox);

            FiltersListBox.DisplayMember = "DisplayName";
            ParametersListBox.DisplayMember = "DisplayName";

            ServiceEnvironmentLabel.Text = pixelsController.CurrentServiceEnvironmentTypeEnum.ToString().ToUpper();

            // Display/Hide Save and Delete buttons.
            if (!HasPrivilege(ModuleWorkItem.PrivilegeIDPixelEditor) || pixelsController.CurrentServiceEnvironmentTypeEnum != ServiceEnvironmentTypeEnum.Dev)
            {
                SaveButton.Visible = false;
                DeleteButton.Visible = false;
            }

            // If this is a new pagePixel, disable Delete button.
            if (pagePixel.PagePixelID <= 0)
            {
                DeleteButton.Enabled = false;
                newPixelFlag = true;
            }
            
            InitializeNextStep();

            TextRadioButton_CheckedChanged(null, null);
        }
        #endregion

        #region Private Methods
        private void InitializeNextStep()
        {
            NextStepButton.Visible = false;

            // If this is a new pixel, there will be no next step.
            if (pagePixel.PagePixelID != Matchnet.Constants.NULL_INT)
            {
                ServiceEnvironmentTypeEnum serviceEnvrionmentTypeEnum;
                // Get information about the next Publish step.
                nextPublishActionPublishObjectID = CommonController.GetNextPublishActionPublishObjectStep(pagePixel.PagePixelID, PublishObjectType.Pixel, out serviceEnvrionmentTypeEnum);

                // Display/Hide NextStepButton.
                if (pagePixel.PagePixelID > 0 && nextPublishActionPublishObjectID != Matchnet.Constants.NULL_INT && (pixelsController.CurrentServiceEnvironmentTypeEnum == serviceEnvrionmentTypeEnum))
                {
                    switch (nextPublishActionPublishObjectID)
                    {
                        case PubActPubObjIDPixelPublishToContentStg:
                            NextStepButton.Visible = true;
                            nextStepText = PubActPubObjTextPixelPublishToContentStg;

                            break;

                        case PubActPubObjIDPixelVerifyInContentStg:
                            NextStepButton.Visible = true;
                            nextStepText = PubActPubObjTextPixelVerifyInContentStg;

                            break;

                        case PubActPubObjIDPixelApproveInContentStg:
                            if (HasPrivilege(ModuleWorkItem.PrivilegeIDPixelApprover))
                            {
                                NextStepButton.Visible = true;
                                nextStepText = PubActPubObjTextPixelApproveInContentStg;
                            }

                            break;

                        case PubActPubObjIDPixelVerifyWithPartnerInContentStg:
                            NextStepButton.Visible = true;
                            nextStepText = PubActPubObjTextPixelVerifyWithPartnerInContentStg;

                            break;

                        case PubActPubObjIDPixelPublishToProd:
                            if (HasPrivilege(ModuleWorkItem.PrivilegeIDPixelPublisher))
                            {
                                NextStepButton.Visible = true;
                                nextStepText = PubActPubObjTextPixelPublishToProd;
                            }

                            break;

                        case PubActPubObjIDPixelVerifyInProd:
                            NextStepButton.Visible = true;
                            nextStepText = PubActPubObjTextPixelVerifyInProd;

                            break;

                        case PubActPubObjIDPixelVerifyWithPartnerInProd:
                            NextStepButton.Visible = true;
                            nextStepText = PubActPubObjTextPixelVerifyWithPartnerInProd;

                            break;

                        default:
                            NextStepButton.Visible = false;

                            break;
                    }

                    NextStepButton.Text = nextStepText;
                }
            }
        }

        private void Pixel_Load(object sender, System.EventArgs e)
        {
            DisplayPixel(true);
        }

        private void DisplayPixel(Boolean isFirstLoad)
        {
            if (isFirstLoad)
            {
                if (pagePixel.PagePixelID != Matchnet.Constants.NULL_INT && pagePixel.PagePixelID != 0)
                    PagePixelIDTextBox.Text = pagePixel.PagePixelID.ToString();

                for (int i = 0; i < SitesComboBox.Items.Count; i++)
                {
                    if (((Site)SitesComboBox.Items[i]).SiteID == pagePixel.SiteID)
                    {
                        SitesComboBox.SelectedIndex = i;
                        break;
                    }
                }
                if (pagePixel.PageID != Matchnet.Constants.NULL_INT && pagePixel.PageID != 0)
                    PageIDTextBox.Text = pagePixel.PageID.ToString();

                if (pagePixel.IsPublishedFlag)
                    LiveRadioButton.Checked = true;
                else
                    OnHoldRadioButton.Checked = true;

                if (pagePixel.ImgFlag)
                    ImageRadioButton.Checked = true;
                else
                    TextRadioButton.Checked = true;

                if (pagePixel.EndDate < EndDateDateTimePicker.MinDate)
                    EndDateDateTimePicker.Value = NoEndDate;
                DescriptionTextBox.Text = pagePixel.Description;
                ContentRichTextBox.Text = pagePixel.Content;
                PublishIDTextBox.Text = pagePixel.PublishID.ToString();
            }

            // Build filters and parameters.
            if (isFirstLoad && pagePixel.ContentCondition != Matchnet.Constants.NULL_STRING)
            {
                // Store the contentCondition info in a collection.
                PopulateFilters(pagePixel.ContentCondition);
                // Store the Paramter info in a collection.
                PopulateParameters(pagePixel.ContentCondition);
            }
        }

        /// <summary>
        /// This method converts the string xml into an XMLDocument, finds the nodes with name <code>xsl:if</code>
        /// and then parses out all the conditions (filters) from them.
        /// NOTE:  This Pixel Administrator tool assumes that all <code>xsl:if</code> conditions are nested (they do not use "and"
        /// in the "test" attribute).  This tool will also create all contentConditions in this nested manner.
        /// </summary>
        /// <param name="contentCondition"></param>
        private void PopulateFilters(String contentCondition)
        {
            String valueHolder;
            XmlNodeList ifNodeList;
            XmlDocument ccXmlDoc = ArgumentObject.StringToXml(contentCondition);

            ifNodeList = ccXmlDoc.GetElementsByTagName("xsl:if");

            try
            {
                // Add a Filter object for each contentCondition.
                foreach (XmlNode ifNode in ifNodeList)
                {
                    String testText;
                    String testTextHolder;
                    Int32 afterLastBrackedIndex;
                    String comparisonOperator;
                    valueHolder = String.Empty;

                    testText = ifNode.Attributes["test"].InnerText;

                    // The index value of the last bracket +2 will be useful in parsing.  It will be the position of the Comparison operator.
                    afterLastBrackedIndex = testText.LastIndexOf(']') + 2;

                    // Parse out the comparisonOperator.
                    // If this fails it is a "contains."
                    try
                    {
                        comparisonOperator = testText.Substring(afterLastBrackedIndex, testText.IndexOf(' ', afterLastBrackedIndex) - afterLastBrackedIndex);
                    }
                    catch
                    {
                        comparisonOperator = "contains";
                    }

                    // Parse out each of the Values.  There can be more than one (or).
                    testTextHolder = testText;
                    if (comparisonOperator != "contains")
                    {
                        do
                        {
                            if (testTextHolder.IndexOf(" or ") != -1)
                            {
                                String CurrentTest = String.Empty;

                                CurrentTest = testTextHolder.Substring(0, testTextHolder.IndexOf(" or "));

                                // The index value of the last bracket +2 will be useful in parsing.  It will be the position of the Comparison operator.
                                afterLastBrackedIndex = CurrentTest.LastIndexOf(']') + 2;

                                valueHolder += ArgumentObject.ClearQuotes(CurrentTest.Substring(CurrentTest.IndexOf(comparisonOperator, afterLastBrackedIndex) + 2));

                                valueHolder += "|";

                                // Update the testText value.
                                testTextHolder = testTextHolder.Substring(testTextHolder.IndexOf(" or ") + 4);
                            }
                            else
                            {
                                valueHolder += ArgumentObject.ClearQuotes(testTextHolder.Substring(testTextHolder.IndexOf(comparisonOperator, afterLastBrackedIndex) + comparisonOperator.Length + 1));

                                testTextHolder = String.Empty;
                            }
                        }
                        while (testTextHolder.Length > 0);
                    }
                    else	// Contains.
                    {
                        do
                        {
                            if (testTextHolder.IndexOf(" or ") != -1)
                            {
                                String CurrentTest = String.Empty;

                                CurrentTest = testTextHolder.Substring(0, testTextHolder.IndexOf(" or "));

                                // The index value of the last bracket +2 will be useful in parsing.  It will be the position of the Comparison operator.
                                afterLastBrackedIndex = CurrentTest.LastIndexOf(']') + 2;

                                valueHolder += ArgumentObject.ClearQuotes(CurrentTest.Substring(afterLastBrackedIndex, CurrentTest.LastIndexOf(')') - afterLastBrackedIndex));

                                valueHolder += "|";

                                // Update the testText value.
                                testTextHolder = testTextHolder.Substring(testTextHolder.IndexOf(" or ") + 4);
                            }
                            else
                            {
                                valueHolder += ArgumentObject.ClearQuotes(testTextHolder.Substring(afterLastBrackedIndex, testTextHolder.LastIndexOf(')') - afterLastBrackedIndex));

                                testTextHolder = String.Empty;
                            }
                        }
                        while (testTextHolder.Length > 0);
                    }

                    if (testText.IndexOf(ArgumentObject.ARG_XML_INFO_COUNTRYREGIONID) != -1)
                    {
                        AddFilterToFilters(new FilterObject(ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID, comparisonOperator, valueHolder));
                    }
                    else if (testText.IndexOf(ArgumentObject.ARG_XML_INFO_GENDERMASK) != -1)
                    {
                        AddFilterToFilters(new FilterObject(ArgumentObject.ArgumentTypeMaskEnum.GenderMask, comparisonOperator, valueHolder));
                    }
                    else if (testText.IndexOf(ArgumentObject.ARG_XML_INFO_BIRTHDATE) != -1)
                    {
                        AddFilterToFilters(new FilterObject(ArgumentObject.ArgumentTypeMaskEnum.BirthDate, comparisonOperator, valueHolder));
                    }
                    else if (testText.IndexOf(ArgumentObject.ARG_XML_INFO_PROMOTIONID) != -1)
                    {
                        AddFilterToFilters(new FilterObject(ArgumentObject.ArgumentTypeMaskEnum.PromotionID, comparisonOperator, valueHolder));
                    }
                    else if (testText.IndexOf(ArgumentObject.ARG_XML_INFO_MEMBERID) != -1)
                    {
                        AddFilterToFilters(new FilterObject(ArgumentObject.ArgumentTypeMaskEnum.MemberID, comparisonOperator, valueHolder));
                    }
                    else if (testText.IndexOf(ArgumentObject.ARG_XML_INFO_ISPAYINGMEMBERFLAG) != -1)
                    {
                        AddFilterToFilters(new FilterObject(ArgumentObject.ArgumentTypeMaskEnum.IsPayingMemberFlag, comparisonOperator, valueHolder));
                    }
                    else if (testText.IndexOf(ArgumentObject.ARG_XML_INFO_LANDINGPAGEURL) != -1)
                    {
                        AddFilterToFilters(new FilterObject(ArgumentObject.ArgumentTypeMaskEnum.LandingPageURL, comparisonOperator, valueHolder));
                    }
                    else if (testText.IndexOf(ArgumentObject.ARG_XML_INFO_GENDERMASK_SEARCHPREF) != -1)
                    {
                        AddFilterToFilters(new FilterObject(ArgumentObject.ArgumentTypeMaskEnum.GenderMask_SearchPref, comparisonOperator, valueHolder));
                    }
                    else if (testText.IndexOf(ArgumentObject.ARG_XML_INFO_LGID) != -1)
                    {
                        AddFilterToFilters(new FilterObject(ArgumentObject.ArgumentTypeMaskEnum.LGID, comparisonOperator, valueHolder));
                    }
                }
            }
            catch
            {
                // If an error was produced while trying to create the pixels then most likely this pagePixel was created manually (before the
                // creation of the PixelAdminTool.  Notify the user that this pagePixel should be thoroughly checked and updated.
                pagePixel.ContentCondition = String.Empty;
                pagePixel.ContentConditionArgs = String.Empty;
                FiltersListBox.Items.Clear();

                MessageBox.Show(MsgFilterParameterCouldNotLoad, Constants.MsgBoxCaptionWarning);
            }
        }

        private void AddFilterToFilters(FilterObject filter)
        {
            FiltersListBox.Items.Add(filter);
        }

        /// <summary>
        /// This method converts the string xml into an XMLDocument, finds the node with name <code>urlArgs</code>
        /// and then parses it (Paramters).  There should be only one such node per pagePixel.
        /// </summary>
        /// <param name="contentCondition"></param>
        private void PopulateParameters(string contentCondition)
        {
            String keyHolder;
            XmlNode urlNode;
            XmlDocument ccXmlDoc = ArgumentObject.StringToXml(contentCondition);
            ArrayList parameterNamesList = new ArrayList();
            String parameterNames;
            Int32 counter = 0;

            try
            {
                // Get the first node named urlArgs (there should be only one).
                try
                {
                    urlNode = ((XmlNodeList)ccXmlDoc.GetElementsByTagName("urlArgs")).Item(0);
                }
                catch
                {
                    urlNode = null;
                }

                if (urlNode != null)
                {
                    // Extract the parameterNamesList from the InnerXml.
                    parameterNames = urlNode.InnerXml;

                    // Each parameter name will end with a "<xsl:".
                    // An HtmlDecode is necessary because the parameters are HtmlEncoded when they are added to the collection.  If we
                    // do not decode them we will encode twice.
                    do
                    {
                        parameterNamesList.Add(HttpUtility.HtmlDecode(parameterNames.Substring(0, parameterNames.IndexOf("<xsl:"))));

                        parameterNames = parameterNames.Substring(parameterNames.IndexOf("/>") + 2);	// +2 to account for the length of "/>".
                    }
                    while (parameterNames.Length > 0);

                    // Scroll through each child node and parse.  We assume that all its children will be "value-of" nodes.
                    foreach (XmlNode ValueOfNode in urlNode.ChildNodes)
                    {
                        if (ValueOfNode.NodeType.ToString() == "Element")
                        {
                            // Retrive the Key.
                            keyHolder = parameterNamesList[counter].ToString();
                            counter += 1;

                            switch (ValueOfNode.Attributes.GetNamedItem("select").Value)
                            {
                                case ArgumentObject.ARG_XML_INFO_BIRTHDATE:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.BirthDate, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_GENDERMASK:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.GenderMask, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_SESSIONID:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.SessionID, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_MEMBERID:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.MemberID, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_COUNTRYREGIONID:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_REFERRINGURL:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.ReferringURL, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_SUBSCRIPTIONDURATION:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.SubscriptionDuration, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_SUBSCRIPTIONAMOUNT:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.SubscriptionAmount, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_STATEABBREVIATION:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.StateAbbreviation, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_AGEGROUP:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.AgeGroup, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_AGE:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.Age, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_GENDERMASK_SEARCHPREF:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.GenderMask_SearchPref, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_COUNTRYREGIONID_SEARCHPREF:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID_SearchPref, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_STATEABBREVIATION_SEARCHPREF:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.StateAbbreviation_SearchPref, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_MINAGE_SEARCHPREF:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.MinAge_SearchPref, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_MAXAGE_SEARCHPREF:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.MaxAge_SearchPref, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_MINAGEGROUP_SEARCHPREF:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.MinAgeGroup_SearchPref, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_MAXAGEGROUP_SEARCHPREF:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.MaxAgeGroup_SearchPref, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_PRM:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.PRM, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_TIMESTAMP:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.Timestamp, keyHolder));
                                    break;
                                case ArgumentObject.ARG_XML_INFO_LGID:
                                    AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.LGID, keyHolder));
                                    break;
                            }
                        }
                    }
                }
            }
            catch
            {
                // If an error was produced while trying to create the pixels then most likely this pagePixel was created manually (before the
                // creation of the PixelAdminTool.  Notify the user that this pagePixel should be thoroughly checked and updated.
                pagePixel.ContentCondition = String.Empty;
                pagePixel.ContentConditionArgs = String.Empty;
                FiltersListBox.Items.Clear();

                MessageBox.Show(MsgFilterParameterCouldNotLoad, Constants.MsgBoxCaptionWarning);
            }
        }

        private void AddParameterToParameters(ParameterObject parameter)
        {
            ParametersListBox.Items.Add(parameter);
        }

        private void TextRadioButton_CheckedChanged(object sender, System.EventArgs e)
        {
            previewedFlag = false;

            if (TextRadioButton.Checked)
            {
                TestJavaScriptButton.Visible = true;
                jsTestedFlag = false;

                // Only TEXT type pixels can contain parameters.  This enforces that rule.
                // We leave the DELETE button active so that the user can clean up bad data.
                ParameterAddButton.Enabled = true;
                ParameterEditButton.Enabled = true;
            }
            else
            {
                TestJavaScriptButton.Visible = false;
                jsTestedFlag = true;

                // Only TEXT type pixels can contain parameters.  This enforces that rule.
                // We leave the DELETE button active so that the user can clean up bad data.
                ParameterAddButton.Enabled = false;
                ParameterEditButton.Enabled = false;
            }
        }

        private void SCancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (!jsTestedFlag || !previewedFlag)
            {
                MessageBox.Show(MsgJSTesAndPreviewRequired, Constants.MsgBoxCaptionWarning);
            }
            else
            {
                pagePixel = BuildPagePixel();

                if (ValidatePixel(pagePixel))
                {
                    // Are you sure?
                    if (MessageBox.Show(MsgSavePixelConfirm, Constants.MsgBoxCaptionConfirm, MessageBoxButtons.YesNo) == DialogResult.No)
                        return;
                    else
                    {
                        try
                        {
                            // Save.
                            pixelsController.SavePagePixel(pagePixel, MemberID, false, ServiceEnvironmentTypeEnum.Dev);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(String.Format(Constants.MsgContactAdministrator, ex.Message), Constants.MsgBoxCaptionSystemError);
                            return;
                        }

                        DialogResult = DialogResult.OK;
                        this.Close();
                    }
                }
            }
        }

        private PagePixel BuildPagePixel()
        {
            Int32 siteID;
            Int32 pagePixelID;
            Int32 pageID;

            PagePixel pagePixel = new PagePixel();

            RetrieveSitePixelPageIDsFromControls(out siteID, out pagePixelID, out pageID);

            pagePixel.PagePixelID = pagePixelID;
            pagePixel.PageID = pageID;
            pagePixel.SiteID = siteID;
            pagePixel.Content = ContentRichTextBox.Text.Trim();
            pagePixel.ContentCondition = FilterObject.BuildContentConditionXml(FiltersListBox, ParametersListBox);
            pagePixel.ContentConditionArgs = ArgumentObject.BuildContentConditionArgsXml(FiltersListBox, ParametersListBox);
            pagePixel.ImgFlag = ImageRadioButton.Checked;
            pagePixel.Description = DescriptionTextBox.Text.Trim();
            if (EndDateDateTimePicker.Value != NoEndDate)
            {
                pagePixel.EndDate = EndDateDateTimePicker.Value;
            }
            pagePixel.IsPublishedFlag = LiveRadioButton.Checked;
            if (newPixelFlag)
            {
                pagePixel.ActiveFlag = true;
                pagePixel.ApprovalFlag = false;
            }
            else 
            {
                pagePixel.ActiveFlag = this.pagePixel.ActiveFlag;
                pagePixel.ApprovalFlag = this.pagePixel.ApprovalFlag;
            }
            
            return pagePixel;
        }

        private void RetrieveSitePixelPageIDsFromControls(out Int32 siteID, out Int32 pagePixelID, out Int32 pageID)
		{
		    siteID = Convert.ToInt32(SitesComboBox.SelectedValue);

            if (PagePixelIDTextBox.Text.Trim() != String.Empty)
            {
                pagePixelID = Convert.ToInt32(PagePixelIDTextBox.Text.Trim());
            }
            else
            {
				pagePixelID = Matchnet.Constants.NULL_INT;
            }

			if (PageIDTextBox.Text.Trim() != String.Empty)
            {
                pageID = Convert.ToInt32(PageIDTextBox.Text.Trim());
            }
            else
            {
				pageID = Matchnet.Constants.NULL_INT;
            }
		}

        private Boolean ValidatePixel(PagePixel pagePixel)
        {
            if (pagePixel.SiteID == Matchnet.Constants.NULL_INT || pagePixel.PageID == Matchnet.Constants.NULL_INT)
            {
                MessageBox.Show(MsgSiteIDAndPageIDRequired, Constants.MsgBoxCaptionWarning);
                return false;
            }

            if (pagePixel.EndDate < DateTime.Now && pagePixel.EndDate != NoEndDate && pagePixel.EndDate != DateTime.MinValue)
            {
                MessageBox.Show(MsgEndDateIsPast, Constants.MsgBoxCaptionWarning);
                return false;
            }

            if (pagePixel.Description == String.Empty)
            {
                MessageBox.Show(MsgDescriptionRequired, Constants.MsgBoxCaptionWarning);
                return false;
            }

            if (pagePixel.Content == String.Empty)
            {
                MessageBox.Show(MsgContentRequired, Constants.MsgBoxCaptionWarning);
                return false;
            }

            // If this is a 3000 page (Subscription), make sure that it contains no "http:" (only https:).
            if (pagePixel.PageID >= 3000 && pagePixel.PageID <= 3999 && pagePixel.Content.ToLower().IndexOf("http:") != -1)
            {
                MessageBox.Show(MsgSubscriptionSecureLinksOnly, Constants.MsgBoxCaptionWarning);
                return false;
            }

            // If this is an IMAGE type, make sure there is no "<img " in the Content.
            if (pagePixel.ImgFlag && pagePixel.Content.ToLower().IndexOf("<img ") != -1)
            {
                MessageBox.Show(MsgImageTypeNoImgTag, Constants.MsgBoxCaptionWarning);
                return false;
            }

            // If this is an IMAGE type, make sure there is no "<script " in the Content.
            if (pagePixel.ImgFlag && pagePixel.Content.ToLower().IndexOf("<script") != -1)
            {
                MessageBox.Show(MsgImageTypeNoScriptTag, Constants.MsgBoxCaptionWarning);
                return false;
            }

            // If this is a TEXT type, make sure that it begins with "<img " or "<script" or "<iframe".
            if (!pagePixel.ImgFlag && (pagePixel.Content.Length <= 7 || pagePixel.Content.ToLower().Substring(0, 5) != "<img " && pagePixel.Content.ToLower().Substring(0, 7) != "<script" && pagePixel.Content.ToLower().Substring(0, 7) != "<iframe"))
            {
                MessageBox.Show(MsgTextTypeImgTagOrScriptTag, Constants.MsgBoxCaptionWarning);
                return false;
            }

            if (!pagePixel.ImgFlag && pagePixel.Content.ToLower().Substring(0, 7) == "<script" && pagePixel.Content.ToLower().IndexOf("</script>") == -1)
            {
                MessageBox.Show(MsgTextTypeClosingScriptTag, Constants.MsgBoxCaptionWarning);
                return false;
            }

            // If this is a TEXT type, make sure that there is "width="1" height="1" if it contains a "<img " tag.
            if (!pagePixel.ImgFlag && pagePixel.Content.ToLower().IndexOf("<img ") != -1)
            {
                if (pagePixel.Content.ToLower().IndexOf("width=\"1\"") == -1 && pagePixel.Content.ToLower().IndexOf("width=1") == -1 && pagePixel.Content.ToLower().IndexOf("vspace=\"0\"") == -1
                    || pagePixel.Content.ToLower().IndexOf("height=\"1\"") == -1 && pagePixel.Content.ToLower().IndexOf("height=1") == -1 && pagePixel.Content.ToLower().IndexOf("hspace=\"0\"") == -1
                    || pagePixel.Content.ToLower().IndexOf("border=\"0\"") == -1 && pagePixel.Content.ToLower().IndexOf("border=0") == -1)
                {
                    MessageBox.Show(MsgTextTypeWidthHeightBorder, Constants.MsgBoxCaptionWarning);
                    return false;
                }
            }

            // If the pagePixel contains PARAMETERS, make sure there is a ParameterStringStart somewhere in the Content.
            if (ParametersListBox.Items.Count > 0 && pagePixel.Content.IndexOf(ParameterStringStart) == -1)
            {
                MessageBox.Show(MsgParameterStringStartMissing, Constants.MsgBoxCaptionWarning);
                return false;
            }

            if (pagePixel.ContentCondition != null && pagePixel.ContentCondition.Length > MaxLengthContentCondition)
            {
                MessageBox.Show(MsgContentConditionMaxLength, Constants.MsgBoxCaptionWarning);
                return false;
            }

            if (pagePixel.ContentConditionArgs != null && pagePixel.ContentConditionArgs.Length > MaxLengthContentConditionArgs)
            {
                MessageBox.Show(MsgContentConditionArgsMaxLength, Constants.MsgBoxCaptionWarning);
                return false;
            }

            return true;
        }

        private void FilterAddButton_Click(object sender, EventArgs e)
        {
            AddFilter();
        }

        private void FilterEditButton_Click(object sender, EventArgs e)
        {
            if (FiltersListBox.Items.Count <= 0)
                MessageBox.Show(MsgFilterNone, Constants.MsgBoxCaptionWarning);
            else if (FiltersListBox.SelectedItem == null)
                MessageBox.Show(MsgFilterSelect, Constants.MsgBoxCaptionWarning);
            else
            {
                Filter filter = new Filter(GlobalState, FiltersListBox, true);

                if (filter.ShowDialog() == DialogResult.OK)
                {
                    DisplayPixel(false);
                }
            }
        }

        private void FilterDeleteButton_Click(object sender, EventArgs e)
        {
            if (FiltersListBox.Items.Count <= 0)
                MessageBox.Show(MsgFilterNone, Constants.MsgBoxCaptionWarning);
            else if (FiltersListBox.SelectedItem == null)
                MessageBox.Show(MsgFilterSelect, Constants.MsgBoxCaptionWarning);
            else
                FiltersListBox.Items.Remove(FiltersListBox.SelectedItem);
        }

        private void ParameterAddButton_Click(object sender, EventArgs e)
        {
            AddParameter();
        }

        private void ParameterEditButton_Click(object sender, EventArgs e)
        {
            if (ParametersListBox.Items.Count <= 0)
                MessageBox.Show(MsgParameterNone, Constants.MsgBoxCaptionWarning);
            else if (ParametersListBox.SelectedItem == null)
                MessageBox.Show(MsgParameterSelect, Constants.MsgBoxCaptionWarning);
            else
            {
                Parameter parameter = new Parameter(GlobalState, ParametersListBox, true);

                if (parameter.ShowDialog() == DialogResult.OK)
                {
                    DisplayPixel(false);
                }
            }
        }

        private void ParameterDeleteButton_Click(object sender, EventArgs e)
        {
            if (ParametersListBox.Items.Count <= 0)
                MessageBox.Show(MsgParameterNone, Constants.MsgBoxCaptionWarning);
            else if (ParametersListBox.SelectedItem == null)
                MessageBox.Show(MsgParameterSelect, Constants.MsgBoxCaptionWarning);
            else
                ParametersListBox.Items.Remove(ParametersListBox.SelectedItem);
        }

        private void AddFilter()
        {
            Filter filter = new Filter(GlobalState, FiltersListBox);

            if (filter.ShowDialog() == DialogResult.OK)
            {
                DisplayPixel(false);
            }
        }

        private void AddParameter()
        {
            Parameter parameter = new Parameter(GlobalState, ParametersListBox);

            if (parameter.ShowDialog() == DialogResult.OK)
            {
                DisplayPixel(false);
            }
        }

        private void DeleteButton_Click(object  sender, EventArgs e)
        {
            // Are you sure?
            if (MessageBox.Show(MsgDeletePixelConfirm, Constants.MsgBoxCaptionConfirm, MessageBoxButtons.YesNo) == DialogResult.No)
                return;
            else
            {
                try
                {
                    // Delete.
                    pixelsController.DeletePagePixel(pagePixel.PagePixelID, MemberID);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format(Constants.MsgContactAdministrator, ex.Message), Constants.MsgBoxCaptionSystemError);
                    return;
                }

                DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void PreviewButton_Click(object sender, EventArgs e)
        {
            previewedFlag = true;

            MessageBox.Show(BuildPreview());
        }

        private String BuildPreview()
        {
            String previewText = String.Empty;

            String parametersText = BuildParametersForPreview(ParametersListBox);

            // Build the Preview.  Add the parameters with placeholder values, where necessary.
            if (ImageRadioButton.Checked)
                previewText += "http://";

            if (parametersText != String.Empty)
                previewText += ContentRichTextBox.Text.Replace(ParameterStringStart, parametersText);
            else
                previewText += ContentRichTextBox.Text;

            return previewText;
        }

        private string BuildParametersForPreview(ListBox parameters)
        {
            string dummyData = String.Empty;
            string stringHolder = String.Empty;

            for (Int16 i = 0; i < parameters.Items.Count; i++)
            {
                switch (((ParameterObject)(parameters.Items[i])).ArgumentTypeMask)
                {
                    case ArgumentObject.ArgumentTypeMaskEnum.BirthDate:
                        dummyData = "19730630";
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.GenderMask:
                        dummyData = "9";
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.SessionID:
                        dummyData = "EDC898CE-8126-465B-9F6F-F2C7C37D51C7";
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.MemberID:
                        dummyData = "988264912";
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID:
                        dummyData = "223";
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.ReferringURL:
                        dummyData = "http%3a%2f%2fwww.americansingles.com%2fdefault.asp%3fp%3d18000";
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.SubscriptionDuration:
                        dummyData = "3";
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.SubscriptionAmount:
                        dummyData = "29.95";
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.StateAbbreviation:
                        dummyData = "CA";
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.Age:
                        dummyData = "27";
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.AgeGroup:
                        dummyData = "2";
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.GenderMask_SearchPref:
                        dummyData = "9";
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID_SearchPref:
                        dummyData = "223";
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.StateAbbreviation_SearchPref:
                        dummyData = "CA";
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.MinAge_SearchPref:
                        dummyData = "23";
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.MaxAge_SearchPref:
                        dummyData = "32";
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.MinAgeGroup_SearchPref:
                        dummyData = "1";
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.MaxAgeGroup_SearchPref:
                        dummyData = "2";
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.PRM:
                        dummyData = "4938273";
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.Timestamp:
                        dummyData = DateTime.Now.ToString("yyyyMMddHHmmss");
                        break;
                    case ArgumentObject.ArgumentTypeMaskEnum.LGID:
                        dummyData = "Xlg123id";
                        break;
                    default:
                        break;
                }

                stringHolder += ((ParameterObject)(parameters.Items[i])).Key + dummyData;
            }

            return stringHolder;
        }

        private void TestJavaScriptButton_Click(object sender, EventArgs e)
        {
            jsTestedFlag = true;
            
            HtmlBrowser.Url = new Uri("about:blank");
            
            HtmlDocument htmlDocument = HtmlBrowser.Document;

            pagePixel = BuildPagePixel();

            htmlDocument.Write(String.Format(HtmlTestTemplate, BuildPreview()));

            MessageBox.Show(MsgJSTestComplete);
        }

        /// <summary>
        /// NOTE:  If changes were made to the Pixel currently open in the window and that pixel was not yet saved,
        /// the pixel that will be approved is the pixel as it was before the unsaved changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NextStepButton_Click(object sender, EventArgs e)
        {
            // Are you sure?
            if (MessageBox.Show(String.Format(MsgNextStepPixelConfirm, NextStepButton.Text), Constants.MsgBoxCaptionConfirm, MessageBoxButtons.YesNo) == DialogResult.No)
                return;
            else
            {
                try
                {
                    // Do the correct action.
                    switch (nextPublishActionPublishObjectID)
                    {
                        case PubActPubObjIDPixelPublishToContentStg:
                            pixelsController.SavePagePixel(pagePixel, MemberID, true, ServiceEnvironmentTypeEnum.ContentStg);
                            CommonController.SavePublishAction(pagePixel.PublishID, PubActPubObjIDPixelPublishToContentStg, MemberID);

                            break;

                        case PubActPubObjIDPixelApproveInContentStg:
                            pixelsController.ApprovePagePixel(pagePixel.PagePixelID, MemberID, ServiceEnvironmentTypeEnum.ContentStg);
                            CommonController.SavePublishAction(pagePixel.PublishID, PubActPubObjIDPixelApproveInContentStg, MemberID);

                            break;

                        case PubActPubObjIDPixelPublishToProd:
                            pixelsController.SavePagePixel(pagePixel, MemberID, true, ServiceEnvironmentTypeEnum.Prod);
                            CommonController.SavePublishAction(pagePixel.PublishID, PubActPubObjIDPixelPublishToProd, MemberID);

                            Helper.SendEmail(Constants.EmailsToAlert, Helper.EmailType.DeploymentToProd
                                , "PagePixel", MemberEmail, BuildEmailBody());

                            break;

                        // All Verifies are handled here.
                        default:
                            CommonController.SavePublishAction(pagePixel.PublishID, nextPublishActionPublishObjectID, MemberID);

                            break;
                    }

                    DialogResult = DialogResult.OK;
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format(Constants.MsgContactAdministrator, ex.Message), Constants.MsgBoxCaptionSystemError);
                    return;
                }
            }
        }

        private String BuildEmailBody()
        { 
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append("PagePixelID: ").Append(pagePixel.PagePixelID.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("SiteID: ").Append(pagePixel.SiteID.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("PageID: ").Append(pagePixel.PageID.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("Description: ").Append(pagePixel.Description.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("Content: ").Append(pagePixel.Content.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("ActiveFlag: ").Append(pagePixel.ActiveFlag.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("ApprovalFlag: ").Append(pagePixel.ApprovalFlag.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("PublishedFlag: ").Append(pagePixel.IsPublishedFlag.ToString()).Append(Environment.NewLine);
            stringBuilder.Append("Publish Date: ").Append(DateTime.Now.ToString()).Append(Environment.NewLine);

            return stringBuilder.ToString();
        }

        private void ContentRichTextBox_TextChanged(object sender, EventArgs e)
        {
            previewedFlag = false;

            if (TextRadioButton.Checked)
            {
                jsTestedFlag = false;
            }
            else
            {
                jsTestedFlag = true;
            }
        }
        #endregion
    }
}