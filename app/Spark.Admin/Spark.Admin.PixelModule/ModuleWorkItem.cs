using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.CompositeUI.WinForms;

using Spark.Admin.Common;

namespace Spark.Admin.PixelModule
{
    public class ModuleWorkItem : BaseModuleWorkItem
    {
        public const Int32 PrivilegeIDPixelEditor = 126;
        public const Int32 PrivilegeIDPixelApprover = 130;
        public const Int32 PrivilegeIDPixelPublisher = 140;

        private IWorkspace workspace;

        #region Public Methods
        public void Run(IWorkspace workspace)
        {
            this.workspace = workspace;

            PixelsView pixelsView = this.Items.AddNew<PixelsView>();
            // You must manually assign GlobalState to the ModuleView.
            pixelsView.GlobalState = GlobalState;

            this.Items.Add(pixelsView);

            // Set the Tab properties.
            TabSmartPartInfo tabSmartPartInfo = new TabSmartPartInfo();
            tabSmartPartInfo.Title = pixelsView.Title;

            workspace.Show(pixelsView, tabSmartPartInfo);
        }

        // EXMPLE:  How to add more "Module leve" views to the Shell from within a Module
        //  (not sure if this will ever be used.  It may make more sense to create child WorkItems
        //  within the ModuleWorkItem.
        //public void AddPixelWorkItem(Int32 pagePixelID)
        //{
        //    PixelView pixelView = this.Items.AddNew<PixelView>();
        //    // You must manually assign GlobalState to the ModuleView.
        //    pixelView.GlobalState = GlobalState;

        //    this.Items.Add(pixelView);

        //    workspace.Show(pixelView);
        //}
        #endregion
    }
}
