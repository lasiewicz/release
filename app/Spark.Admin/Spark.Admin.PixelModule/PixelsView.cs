using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.CompositeUI;

using Spark.Admin.WebServices;
using Matchnet.Content.ValueObjects.PagePixel;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Admin;

using Spark.Admin.Common;

namespace Spark.Admin.PixelModule
{
    public partial class PixelsView : BaseModuleView
    {
        private const String TitlePixels = "Pixels";
        public const String MsgSiteIDAndPageIDRequired = "Please select a Site and enter a valid PageID";
        private PixelsController pixelsController;
        private PagePixelCollection pagePixelCollection;
        private ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum;

        #region Contructors
        public PixelsView()
        {
            InitializeComponent();

            // Set the Tab title.
            this.Title = TitlePixels;
        }
        #endregion

        #region Private Methods
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // You must manually assign GlobalState to the Controller.
            pixelsController.GlobalState = GlobalState;

            InitializePixelsDataGridView();

            pixelsController.InitializeSitesComboBox(ref SitesComboBox);

            // Set up ServiceEnvironment ComboBox.
            ServiceEnvironmentComboBox.DataSource = Enum.GetValues(typeof(ServiceEnvironmentTypeEnum));
            // Make a call to ServiceEnvironmentComboBox_SelectedIndexChanged initialize currentServiceEnvironmentTypeEnum.
            ServiceEnvironmentComboBox_SelectedIndexChanged(null, null);

            if (!HasPrivilege(ModuleWorkItem.PrivilegeIDPixelEditor))
            {
                AddButton.Visible = false;
            }
        }

        private void LoadButton_Click(object sender, EventArgs e)
        {
            if (PageIDTextBox.Text.Trim() == String.Empty)
            {
                MessageBox.Show(MsgSiteIDAndPageIDRequired, Constants.MsgBoxCaptionWarning);
                return;
            }

            LoadPixels();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            EditPixel(Matchnet.Constants.NULL_INT);
        }

        private void LoadPixels()
        {
            pagePixelCollection = pixelsController.RetrievePagePixels(Convert.ToInt32(PageIDTextBox.Text.Trim()), Convert.ToInt32(SitesComboBox.SelectedValue), pixelsController.PixelAdminDisplayTypeMask, ((ServiceEnvironmentTypeEnum)ServiceEnvironmentComboBox.SelectedValue));

            PixelsDataGridView.DataSource = pagePixelCollection;
        }

        private void PixelsDataGridView_RowHeaderMouseDoubleClick(object sender, System.Windows.Forms.DataGridViewCellMouseEventArgs e)
        {
            EditPixel(Convert.ToInt32(PixelsDataGridView.Rows[PixelsDataGridView.SelectedCells[0].RowIndex].Cells["PagePixelID"].Value.ToString()));
        }

        private void EditPixel(Int32 pagePixelID)
        {
            PagePixel pagePixel;

            // Adding a new Pixel.
            if (pagePixelID == Matchnet.Constants.NULL_INT)
            {
                pagePixel = new PagePixel();

                pagePixel.SiteID = Convert.ToInt32(SitesComboBox.SelectedValue);

                if (PageIDTextBox.Text.Trim() != String.Empty)
                {
                    pagePixel.PageID = Convert.ToInt32(PageIDTextBox.Text.Trim());
                }
            }
            else
            {
                pagePixel = pixelsController.GetPagePixelByID(pagePixelCollection, pagePixelID);
            }

            Pixel pixel = new Pixel(GlobalState, pixelsController, pagePixel);

            if (pixel.ShowDialog() == DialogResult.OK)
            {
                LoadPixels();
            }
        }

        private void InitializePixelsDataGridView()
        {
            PixelsDataGridView.AutoGenerateColumns = false;
            PixelsDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            PixelsDataGridView.EditMode = DataGridViewEditMode.EditProgrammatically;
            PixelsDataGridView.MultiSelect = false;
            PixelsDataGridView.AllowUserToAddRows = false;
            PixelsDataGridView.AllowUserToDeleteRows = false;

            DataGridViewColumn column;
            
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "PagePixelID";
            column.Name = "PagePixelID";
            //column.Tag = ListSortDirection.Ascending;
            //column.SortMode = DataGridViewColumnSortMode.Automatic;
            PixelsDataGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "PageID";
            //column.Name = "PageID";
            //column.Tag = ListSortDirection.Ascending;
            PixelsDataGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "SiteID";
            column.Name = "SiteID";
            //column.Tag = ListSortDirection.Ascending;
            //column.SortMode = DataGridViewColumnSortMode.Automatic;
            PixelsDataGridView.Columns.Add(column);

            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Description";
            column.Name = "Description";
            //column.Tag = ListSortDirection.Ascending;
            //column.SortMode = DataGridViewColumnSortMode.Automatic;
            PixelsDataGridView.Columns.Add(column);

            column = new DataGridViewCheckBoxColumn();
            column.DataPropertyName = "ImgFlag";
            column.Name = "Is Image";
            //column.Tag = ListSortDirection.Ascending;
            //column.SortMode = DataGridViewColumnSortMode.Automatic;
            PixelsDataGridView.Columns.Add(column);

            column = new DataGridViewCheckBoxColumn();
            column.DataPropertyName = "ActiveFlag";
            column.Name = "Is Active";
            //column.Tag = ListSortDirection.Ascending;
            //column.SortMode = DataGridViewColumnSortMode.Automatic;
            PixelsDataGridView.Columns.Add(column);

            column = new DataGridViewCheckBoxColumn();
            column.DataPropertyName = "ApprovalFlag";
            column.Name = "Is Approved";
            //column.Tag = ListSortDirection.Ascending;
            //column.SortMode = DataGridViewColumnSortMode.Automatic;
            PixelsDataGridView.Columns.Add(column);

            column = new DataGridViewCheckBoxColumn();
            column.DataPropertyName = "IsPublishedFlag";
            column.Name = "Is Published";
            //column.Tag = ListSortDirection.Ascending;
            //column.SortMode = DataGridViewColumnSortMode.Automatic;
            PixelsDataGridView.Columns.Add(column);
        }

        // TODO:  Need to implement sorting.  Does not work yet.
        private void PixelsDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //ListSortDirection listSortDirection;
            //if (((ListSortDirection)PixelsDataGridView.Columns[e.ColumnIndex].Tag) == ListSortDirection.Ascending)
            //{
            //    listSortDirection = ListSortDirection.Ascending;
            //    PixelsDataGridView.Columns[e.ColumnIndex].Tag = ListSortDirection.Descending;
            //}
            //else
            //{
            //    listSortDirection = ListSortDirection.Descending;
            //    PixelsDataGridView.Columns[e.ColumnIndex].Tag = ListSortDirection.Ascending;
            //}

            ////PixelsDataGridView.Sort(new RowComparer(listSortDirection, e.ColumnIndex));
            //PixelsDataGridView.Sort(PixelsDataGridView.Columns[e.ColumnIndex], listSortDirection);
        }

        private void ServiceEnvironmentComboBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            currentServiceEnvironmentTypeEnum = ((ServiceEnvironmentTypeEnum)ServiceEnvironmentComboBox.SelectedValue);

            // Set the currentServiceEnvironmentTypeEnum so that the other Forms know what ServiceEnvironmentType we're working on.
            pixelsController.CurrentServiceEnvironmentTypeEnum = currentServiceEnvironmentTypeEnum;

            if (currentServiceEnvironmentTypeEnum != ServiceEnvironmentTypeEnum.Dev)
                AddButton.Visible = false;
            else
                AddButton.Visible = true;
        }
        #endregion

        #region Properties
        [CreateNew]
        public PixelsController PixelsController
        {
            set
            {
                pixelsController = value;
            }
        }
        #endregion

        #region Not Yet Implemented
        //protected static String DisplayPixelType(Boolean imgFlag)
        //{
        //    if (imgFlag)
        //        return Constants.PIXELTYPE_IMAGE;
        //    else
        //        return Constants.PIXELTYPE_TEXT;
        //}

        //protected static String DisplayPixelPublishedStatus(Boolean isPublishedFlag, Boolean approvalFlag)
        //{
        //    string status = String.Empty;

        //    if (isPublishedFlag)
        //        status += Constants.PIXELSTATUS_LIVE + ", ";
        //    else
        //        status += Constants.PIXELSTATUS_ONHOLD + ", ";

        //    if (approvalFlag)
        //        status += Constants.PIXELSTATUS_APPROVED;
        //    else
        //        status += Constants.PIXELSTATUS_NOTAPPROVED;

        //    return status;
        //}

        //protected static string DisplayFirstXChars(string text, int length)
        //{
        //    string returnText;

        //    if (text.Length <= length)
        //    {
        //        returnText = text;
        //    }
        //    else
        //    {
        //        returnText = text.Substring(0, length) + " ...";
        //    }

        //    return returnText;
        //}
        #endregion
    }
}
