using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI;

using Spark.Admin.Common;

namespace Spark.Admin.PixelModule
{
    public partial class Parameter : Spark.Admin.Common.Controls.Form
    {
        private ListBox parameterObjects;
        private ParameterObject parameterObject;

        #region Constructors
        public Parameter(State globalState, ListBox parameterObjects, Boolean isEdit)
            : base(globalState)
        {
            InitializeComponent();

            TypeComboBox.DataSource = ParameterObject.ParameterTypeList;
            TypeComboBox.DisplayMember = "Name";
            TypeComboBox.ValueMember = "ArgumentTypeMaskEnum";

            this.parameterObjects = parameterObjects;

            if (isEdit)
                parameterObject = (ParameterObject)(parameterObjects.SelectedItem);

            DisplayParameter();
        }

        public Parameter(State globalState, ListBox parameterObjects)
            : this(globalState, parameterObjects, false)
        {
        }
        #endregion

        #region Public Methods
        private void DisplayParameter()
        {
            if (parameterObject != null)
            {
                TypeComboBox.SelectedValue = parameterObject.ArgumentTypeMask;
                KeyTextBox.Text = parameterObject.Key;
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            AddParameter();
        }
        #endregion

        #region Private Methods
        private void AddParameter()
        {
            ParameterObject thisParameterObject = BuildParameter();

            if (ValidateParameter(thisParameterObject))
            {
                // Remove the existing parameterObject, if one exists.
                if (parameterObject != null)
                    parameterObjects.Items.Remove(parameterObject);

                // Add the new filter.
                parameterObjects.Items.Add(thisParameterObject);

                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private Boolean ValidateParameter(ParameterObject thisParameterObject)
        {
            if ((Int32)thisParameterObject.ArgumentTypeMask == Matchnet.Constants.NULL_INT)
            {
                MessageBox.Show(ArgumentObject.MsgArgumentTypeMaskRequired, Constants.MsgBoxCaptionWarning);
                return false;
            }

            if (thisParameterObject.Key == String.Empty)
            {
                MessageBox.Show(ArgumentObject.MsgKeyRequired, Constants.MsgBoxCaptionWarning);
                return false;
            }

            return true;
        }

        private ParameterObject BuildParameter()
        {
            return new ParameterObject((ArgumentObject.ArgumentTypeMaskEnum)TypeComboBox.SelectedValue, KeyTextBox.Text.Trim());
        }

        private void SCancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}