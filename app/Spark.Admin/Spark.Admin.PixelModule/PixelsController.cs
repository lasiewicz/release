using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI;

using Spark.Admin.Common;
using Spark.Admin.WebServices;
using Matchnet.Content.ValueObjects.PagePixel;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Spark.Admin.PixelModule
{
    public class PixelsController : BaseController
    {
        private WebServiceProxy webServiceProxy;

        #region Constructors
        public PixelsController()
        {
            // Initialize the WebServiceProxy if you are using web services.
            webServiceProxy = new WebServiceProxy();        
        }
        #endregion

        #region Public Methods
        public void DeletePagePixel(Int32 pagePixelID, Int32 memberID)
        {
            webServiceProxy.DeletePagePixel(pagePixelID, memberID);
        }

        public PagePixelCollection RetrievePagePixels(Int32 pageID, Int32 siteID, PagePixelCollection.AdminDisplayTypeMaskEnum adminDisplayTypeMaskEnum, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            return webServiceProxy.RetrievePagePixels(pageID, siteID, adminDisplayTypeMaskEnum, serviceEnvironmentEnum);
        }

        public void SavePagePixel(PagePixel pagePixel, Int32 memberID, Boolean isPublish, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            webServiceProxy.SavePagePixel(pagePixel, memberID, isPublish, serviceEnvironmentEnum);
        }

        public void ApprovePagePixel(Int32 pagePixelID, Int32 approverMemberID, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            webServiceProxy.ApprovePagePixel(pagePixelID, approverMemberID, serviceEnvironmentEnum);
        }

        public PagePixel GetPagePixelByID(PagePixelCollection pagePixelCollection, Int32 pagePixelID)
        {
            for (Int32 i = 0; i < pagePixelCollection.Count; i++)
            {
                if (pagePixelCollection[i].PagePixelID == pagePixelID)
                {
                    return pagePixelCollection[i];
                }
            }

            return null;
        }

        public void InitializeSitesComboBox(ref ComboBox sitesComboBox)
        {
            const Int32 allSitesSiteID = 999999;
            Sites sites = ((Sites)GlobalState[Constants.StateSites]);

            // Only add All Sites if it hasn't already been added.
            if (sites[sites.Count - 1].SiteID != allSitesSiteID)
            {
                // Appened "All Sites" option (SiteID: 999999)
                Site site = new Site(allSitesSiteID, null, "All Sites", 0, 0, String.Empty, 0, 0, 0, String.Empty, 0, DirectionType.ltr, 0, String.Empty, String.Empty, 0);

                sites.Add(site);
            }

            sitesComboBox.DataSource = sites;
            sitesComboBox.ValueMember = "SiteID";
            sitesComboBox.DisplayMember = "Name";
        }

        // EXAMPLE:  How to make a call to the WorkItem.
        //public void EditPixels(Int32 pagePixelID)
        //{
        //    WorkItem.AddPixelWorkItem(pagePixelID);
        //}
        #endregion
        
        #region Properties
        public new ModuleWorkItem WorkItem
        {
            get
            {
                return base.WorkItem as ModuleWorkItem;
            }
        }

        public PagePixelCollection.AdminDisplayTypeMaskEnum PixelAdminDisplayTypeMask
        {
            get
            {
                if (HasPrivilege(ModuleWorkItem.PrivilegeIDPixelEditor) && HasPrivilege(ModuleWorkItem.PrivilegeIDPixelApprover))
                    return PagePixelCollection.AdminDisplayTypeMaskEnum.AdministratorAndApprover;
                else if (HasPrivilege(ModuleWorkItem.PrivilegeIDPixelEditor) || HasPrivilege(ModuleWorkItem.PrivilegeIDPixelPublisher))
                    return PagePixelCollection.AdminDisplayTypeMaskEnum.Administrator;
                else if (HasPrivilege(ModuleWorkItem.PrivilegeIDPixelApprover))
                    return PagePixelCollection.AdminDisplayTypeMaskEnum.Approver;
                else
                    return PagePixelCollection.AdminDisplayTypeMaskEnum.NormalUser;
            }
        }
        #endregion
    }
}
