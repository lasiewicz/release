using System;
using System.Collections;
using System.Text;
using System.Xml;
using System.Windows.Forms;


namespace Spark.Admin.PixelModule
{
    /// <summary>
    /// Summary description for Argument.
    /// </summary>
    public class ArgumentObject
    {
        #region Enums and Constants
        public const String MsgComparisonOperatorRequired = "You must select a Comparison Operator.";
        public const String MsgArgumentTypeMaskRequired = "You must select an ArgumentTypeMask.";
        public const String MsgValueRequired = "You must enter a Value.";
        public const String MsgKeyRequired = "You must enter a Key.";

        // TODO:  Add the [Flags] and make all references of type ArgumentTypeMaskEnum instead of int.
        public enum ArgumentTypeMaskEnum : long
        {
            CountryRegionID = 1,
            GenderMask = 2,
            PromotionID = 4,
            BirthDate = 8,
            MemberID = 16,	// Used with IsPayingMemberFlag to determine "MemberStatus."
            IsPayingMemberFlag = 32,
            LandingPageURL = 64,
            SessionID = 128,
            ReferringURL = 256,
            SubscriptionAmount = 512,
            SubscriptionDuration = 1024,
            StateAbbreviation = 2048,
            Age = 4096,
            AgeGroup = 8192,
            GenderMask_SearchPref = 16384,
            CountryRegionID_SearchPref = 32768,
            StateAbbreviation_SearchPref = 65536,
            MinAge_SearchPref = 131072,
            MaxAge_SearchPref = 262144,
            MinAgeGroup_SearchPref = 524288,
            MaxAgeGroup_SearchPref = 1048576,
            PRM = 2097152,
            Timestamp = 4194304,
            LGID = 17592186044416   // mcho - is this value meaningful?  seems to be for future implementation
        }

        internal const String CCA_XML_INFO_COUNTRYREGIONID = "type=\"Member\" name=\"CountryRegionID\"";
        internal const String CCA_XML_INFO_GENDERMASK = "type=\"Member\" name=\"GenderMask\"";
        internal const String CCA_XML_INFO_PROMOTIONID = "type=\"Member\" name=\"PromotionID\"";
        internal const String CCA_XML_INFO_BIRTHDATE = "type=\"Member\" name=\"BirthDate\"";
        internal const String CCA_XML_INFO_MEMBERID = "type=\"Member\" name=\"MemberID\"";
        internal const String CCA_XML_INFO_ISPAYINGMEMBERFLAG = "type=\"Member\" name=\"IsPayingMemberFlag\"";
        internal const String CCA_XML_INFO_LANDINGPAGEURL = "type=\"Member\" name=\"LandingPageURL\"";
        internal const String CCA_XML_INFO_SESSIONID = "type=\"Member\" name=\"SessionID\"";
        internal const String CCA_XML_INFO_REFERRINGURL = "type=\"Member\" name=\"ReferringURL\"";
        internal const String CCA_XML_INFO_SUBSCRIPTIONDURATION = "type=\"Member\" name=\"SubscriptionDuration\"";
        internal const String CCA_XML_INFO_SUBSCRIPTIONAMOUNT = "type=\"Member\" name=\"SubscriptionAmount\"";
        internal const String CCA_XML_INFO_STATEABBREVIATION = "type=\"Member\" name=\"StateAbbreviation\"";
        internal const String CCA_XML_INFO_AGE = "type=\"Member\" name=\"Age\"";
        internal const String CCA_XML_INFO_AGEGROUP = "type=\"Member\" name=\"AgeGroup\"";
        internal const String CCA_XML_INFO_GENDERMASK_SEARCHPREF = "type=\"SearchPref\" name=\"GenderMask\"";
        internal const String CCA_XML_INFO_COUNTRYREGIONID_SEARCHPREF = "type=\"SearchPref\" name=\"CountryRegionID\"";
        internal const String CCA_XML_INFO_STATEABBREVIATION_SEARCHPREF = "type=\"SearchPref\" name=\"StateAbbreviation\"";
        internal const String CCA_XML_INFO_MINAGE_SEARCHPREF = "type=\"SearchPref\" name=\"MinAge\"";
        internal const String CCA_XML_INFO_MAXAGE_SEARCHPREF = "type=\"SearchPref\" name=\"MaxAge\"";
        internal const String CCA_XML_INFO_MINAGEGROUP_SEARCHPREF = "type=\"SearchPref\" name=\"MinAgeGroup\"";
        internal const String CCA_XML_INFO_MAXAGEGROUP_SEARCHPREF = "type=\"SearchPref\" name=\"MaxAgeGroup\"";
        internal const String CCA_XML_INFO_PRM = "type=\"Member\" name=\"PRM\"";
        internal const String CCA_XML_INFO_TIMESTAMP = "type=\"General\" name=\"Timestamp\"";
        internal const String CCA_XML_INFO_LGID = "type=\"Member\" name=\"LGID\"";

        internal const String ARG_XML_INFO_COUNTRYREGIONID = "/args/arg[@type='Member' and @name='CountryRegionID']";
        internal const String ARG_XML_INFO_GENDERMASK = "/args/arg[@type='Member' and @name='GenderMask']";
        internal const String ARG_XML_INFO_PROMOTIONID = "/args/arg[@type='Member' and @name='PromotionID']";
        internal const String ARG_XML_INFO_BIRTHDATE = "/args/arg[@type='Member' and @name='BirthDate']";
        internal const String ARG_XML_INFO_MEMBERID = "/args/arg[@type='Member' and @name='MemberID']";
        internal const String ARG_XML_INFO_ISPAYINGMEMBERFLAG = "/args/arg[@type='Member' and @name='IsPayingMemberFlag']";
        internal const String ARG_XML_INFO_LANDINGPAGEURL = "/args/arg[@type='Member' and @name='LandingPageURL']";
        internal const String ARG_XML_INFO_SESSIONID = "/args/arg[@type='Member' and @name='SessionID']";
        internal const String ARG_XML_INFO_REFERRINGURL = "/args/arg[@type='Member' and @name='ReferringURL']";
        internal const String ARG_XML_INFO_SUBSCRIPTIONDURATION = "/args/arg[@type='Member' and @name='SubscriptionDuration']";
        internal const String ARG_XML_INFO_SUBSCRIPTIONAMOUNT = "/args/arg[@type='Member' and @name='SubscriptionAmount']";
        internal const String ARG_XML_INFO_STATEABBREVIATION = "/args/arg[@type='Member' and @name='StateAbbreviation']";
        internal const String ARG_XML_INFO_AGE = "/args/arg[@type='Member' and @name='Age']";
        internal const String ARG_XML_INFO_AGEGROUP = "/args/arg[@type='Member' and @name='AgeGroup']";
        internal const String ARG_XML_INFO_GENDERMASK_SEARCHPREF = "/args/arg[@type='SearchPref' and @name='GenderMask']";
        internal const String ARG_XML_INFO_COUNTRYREGIONID_SEARCHPREF = "/args/arg[@type='SearchPref' and @name='CountryRegionID']";
        internal const String ARG_XML_INFO_STATEABBREVIATION_SEARCHPREF = "/args/arg[@type='SearchPref' and @name='StateAbbreviation']";
        internal const String ARG_XML_INFO_MINAGE_SEARCHPREF = "/args/arg[@type='SearchPref' and @name='MinAge']";
        internal const String ARG_XML_INFO_MAXAGE_SEARCHPREF = "/args/arg[@type='SearchPref' and @name='MaxAge']";
        internal const String ARG_XML_INFO_MINAGEGROUP_SEARCHPREF = "/args/arg[@type='SearchPref' and @name='MinAgeGroup']";
        internal const String ARG_XML_INFO_MAXAGEGROUP_SEARCHPREF = "/args/arg[@type='SearchPref' and @name='MaxAgeGroup']";
        internal const String ARG_XML_INFO_PRM = "/args/arg[@type='Member' and @name='PRM']";
        internal const String ARG_XML_INFO_TIMESTAMP = "/args/arg[@type='General' and @name='Timestamp']";
        internal const String ARG_XML_INFO_LGID = "/args/arg[@type='Member' and @name='LGID']";
        #endregion

        #region Private Variables
        private ArgumentTypeMaskEnum argumentTypeMaskVal;
        #endregion

        #region Constructors
        public ArgumentObject(ArgumentTypeMaskEnum argumentTypeMask)
        {
            argumentTypeMaskVal = argumentTypeMask;
        }
        #endregion

        #region Public Methods
        public static string BuildContentConditionArgsXml(ArrayList filters, ArrayList parameters)
        {
            if (filters.Count == 0 && parameters.Count == 0)
            {
                return Matchnet.Constants.NULL_STRING;
            }
            else
            {
                ArrayList arguments = BuildContentConditionArgsCol(filters, parameters);

                StringBuilder xmlString = new StringBuilder();

                // Add main opening tags.
                xmlString.Append("<args>");

                // Add the arg tags for filters.
                foreach (ArgumentObject argument in arguments)
                {
                    xmlString.Append("<arg ");
                    xmlString.Append(GetContentConditionArgInfo(argument.ArgumentTypeMask));
                    xmlString.Append(" />");
                }

                // Add main closing tags.
                xmlString.Append("</args>");

                return xmlString.ToString();
            }
        }

        /// <summary>
        /// Builds the xml <code>args</code> for both the Filters and the Parameters.
        /// </summary>
        /// <returns></returns>
        public static String BuildContentConditionArgsXml(ListBox filters, ListBox parameters)
        {
            if (filters.Items.Count == 0 && parameters.Items.Count == 0)
            {
                return Matchnet.Constants.NULL_STRING;
            }
            else
            {
                ArrayList arguments = BuildContentConditionArgsCol(filters, parameters);

                StringBuilder xmlString = new StringBuilder();

                // Add main opening tags.
                xmlString.Append("<args>");

                // Add the arg tags for filters.
                foreach (ArgumentObject argument in arguments)
                {
                    xmlString.Append("<arg ");
                    xmlString.Append(GetContentConditionArgInfo(argument.ArgumentTypeMask));
                    xmlString.Append(" />");
                }

                // Add main closing tags.
                xmlString.Append("</args>");

                return xmlString.ToString();
            }
        }

        /// <summary>
        /// This will take a collection of <code>Filter</code> and <code>ParameterObject</code>
        /// and return an ArrayList consisting of a collection of <code>Argument</code>.
        /// </summary>
        /// <param name="FilterCol"></param>
        /// <param name="ParameterCol"></param>
        /// <returns></returns>
        private static ArrayList BuildContentConditionArgsCol(ListBox filters, ListBox parameters)
        {
            Int32 argumentTypeMaskHolder = 0;

            ArrayList arguments = new ArrayList();

            // Add the arg object for Filters.
            for (Int16 i = 0; i < filters.Items.Count; i++)
            {
                // Make sure that each argument is only added once.
                if ((argumentTypeMaskHolder & (Int32)((FilterObject)(filters.Items[i])).ArgumentTypeMask) == 0)
                {
                    arguments.Add(new ArgumentObject(((FilterObject)(filters.Items[i])).ArgumentTypeMask));

                    argumentTypeMaskHolder += (Int32)((FilterObject)(filters.Items[i])).ArgumentTypeMask;
                }
            }

            // Add the arg object for Parameters.
            for (Int16 i = 0; i < parameters.Items.Count; i++)
            {
                // Make sure that each argument is only added once.
                if ((argumentTypeMaskHolder & (Int32)((ParameterObject)(parameters.Items[i])).ArgumentTypeMask) == 0)
                {
                    arguments.Add(new ArgumentObject(((ParameterObject)(parameters.Items[i])).ArgumentTypeMask));

                    argumentTypeMaskHolder += (Int32)((ParameterObject)(parameters.Items[i])).ArgumentTypeMask;
                }
            }

            return arguments;
        }

        public static String GetArgumentInfo(ArgumentTypeMaskEnum argumentTypeMask)
        {
            switch (argumentTypeMask)
            {
                case ArgumentTypeMaskEnum.CountryRegionID:
                    return ARG_XML_INFO_COUNTRYREGIONID;
                case ArgumentTypeMaskEnum.GenderMask:
                    return ARG_XML_INFO_GENDERMASK;
                case ArgumentTypeMaskEnum.PromotionID:
                    return ARG_XML_INFO_PROMOTIONID;
                case ArgumentTypeMaskEnum.BirthDate:
                    return ARG_XML_INFO_BIRTHDATE;
                case ArgumentTypeMaskEnum.MemberID:
                    return ARG_XML_INFO_MEMBERID;
                case ArgumentTypeMaskEnum.IsPayingMemberFlag:
                    return ARG_XML_INFO_ISPAYINGMEMBERFLAG;
                case ArgumentTypeMaskEnum.LandingPageURL:
                    return ARG_XML_INFO_LANDINGPAGEURL;
                case ArgumentTypeMaskEnum.SessionID:
                    return ARG_XML_INFO_SESSIONID;
                case ArgumentTypeMaskEnum.ReferringURL:
                    return ARG_XML_INFO_REFERRINGURL;
                case ArgumentTypeMaskEnum.SubscriptionDuration:
                    return ARG_XML_INFO_SUBSCRIPTIONDURATION;
                case ArgumentTypeMaskEnum.SubscriptionAmount:
                    return ARG_XML_INFO_SUBSCRIPTIONAMOUNT;
                case ArgumentTypeMaskEnum.StateAbbreviation:
                    return ARG_XML_INFO_STATEABBREVIATION;
                case ArgumentTypeMaskEnum.Age:
                    return ARG_XML_INFO_AGE;
                case ArgumentTypeMaskEnum.AgeGroup:
                    return ARG_XML_INFO_AGEGROUP;
                case ArgumentTypeMaskEnum.GenderMask_SearchPref:
                    return ARG_XML_INFO_GENDERMASK_SEARCHPREF;
                case ArgumentTypeMaskEnum.CountryRegionID_SearchPref:
                    return ARG_XML_INFO_COUNTRYREGIONID_SEARCHPREF;
                case ArgumentTypeMaskEnum.StateAbbreviation_SearchPref:
                    return ARG_XML_INFO_STATEABBREVIATION_SEARCHPREF;
                case ArgumentTypeMaskEnum.MinAge_SearchPref:
                    return ARG_XML_INFO_MINAGE_SEARCHPREF;
                case ArgumentTypeMaskEnum.MaxAge_SearchPref:
                    return ARG_XML_INFO_MAXAGE_SEARCHPREF;
                case ArgumentTypeMaskEnum.MinAgeGroup_SearchPref:
                    return ARG_XML_INFO_MINAGEGROUP_SEARCHPREF;
                case ArgumentTypeMaskEnum.MaxAgeGroup_SearchPref:
                    return ARG_XML_INFO_MAXAGEGROUP_SEARCHPREF;
                case ArgumentTypeMaskEnum.PRM:
                    return ARG_XML_INFO_PRM;
                case ArgumentTypeMaskEnum.Timestamp:
                    return ARG_XML_INFO_TIMESTAMP;
                case ArgumentTypeMaskEnum.LGID:
                    return ARG_XML_INFO_LGID;
                default:
                    return Matchnet.Constants.NULL_STRING;
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// This will take a collection of <code>Filters</code> and <code>Parameters</code>
        /// and return an ArrayList consisting of a collection of <code>Arguments</code>.
        /// </summary>
        private static ArrayList BuildContentConditionArgsCol(ArrayList filters, ArrayList parameters)
        {
            int argumentTypeMaskHolder = 0;

            ArrayList arguments = new ArrayList();

            // Add the arg object for Filters.
            foreach (FilterObject filter in filters)
            {
                // Make sure that each argument is only added once.
                if ((argumentTypeMaskHolder & (Int32)filter.ArgumentTypeMask) == 0)
                {
                    arguments.Add(new ArgumentObject(filter.ArgumentTypeMask));

                    argumentTypeMaskHolder += (Int32)filter.ArgumentTypeMask;
                }
            }

            // Add the arg object for Parameters.
            foreach (ParameterObject parameter in parameters)
            {
                // Make sure that each argument is only added once.
                if ((argumentTypeMaskHolder & (Int32)parameter.ArgumentTypeMask) == 0)
                {
                    arguments.Add(new ArgumentObject(parameter.ArgumentTypeMask));

                    argumentTypeMaskHolder += (Int32)parameter.ArgumentTypeMask;
                }
            }

            return arguments;
        }

        private static String GetContentConditionArgInfo(ArgumentTypeMaskEnum argumentTypeMask)
        {
            switch (argumentTypeMask)
            {
                case ArgumentTypeMaskEnum.CountryRegionID:
                    return CCA_XML_INFO_COUNTRYREGIONID;
                case ArgumentTypeMaskEnum.GenderMask:
                    return CCA_XML_INFO_GENDERMASK;
                case ArgumentTypeMaskEnum.PromotionID:
                    return CCA_XML_INFO_PROMOTIONID;
                case ArgumentTypeMaskEnum.BirthDate:
                    return CCA_XML_INFO_BIRTHDATE;
                case ArgumentTypeMaskEnum.MemberID:
                    return CCA_XML_INFO_MEMBERID;
                case ArgumentTypeMaskEnum.IsPayingMemberFlag:
                    return CCA_XML_INFO_ISPAYINGMEMBERFLAG;
                case ArgumentTypeMaskEnum.LandingPageURL:
                    return CCA_XML_INFO_LANDINGPAGEURL;
                case ArgumentTypeMaskEnum.SessionID:
                    return CCA_XML_INFO_SESSIONID;
                case ArgumentTypeMaskEnum.ReferringURL:
                    return CCA_XML_INFO_REFERRINGURL;
                case ArgumentTypeMaskEnum.SubscriptionDuration:
                    return CCA_XML_INFO_SUBSCRIPTIONDURATION;
                case ArgumentTypeMaskEnum.SubscriptionAmount:
                    return CCA_XML_INFO_SUBSCRIPTIONAMOUNT;
                case ArgumentTypeMaskEnum.StateAbbreviation:
                    return CCA_XML_INFO_STATEABBREVIATION;
                case ArgumentTypeMaskEnum.Age:
                    return CCA_XML_INFO_AGE;
                case ArgumentTypeMaskEnum.AgeGroup:
                    return CCA_XML_INFO_AGEGROUP;
                case ArgumentTypeMaskEnum.GenderMask_SearchPref:
                    return CCA_XML_INFO_GENDERMASK_SEARCHPREF;
                case ArgumentTypeMaskEnum.CountryRegionID_SearchPref:
                    return CCA_XML_INFO_COUNTRYREGIONID_SEARCHPREF;
                case ArgumentTypeMaskEnum.StateAbbreviation_SearchPref:
                    return CCA_XML_INFO_STATEABBREVIATION_SEARCHPREF;
                case ArgumentTypeMaskEnum.MinAge_SearchPref:
                    return CCA_XML_INFO_MINAGE_SEARCHPREF;
                case ArgumentTypeMaskEnum.MaxAge_SearchPref:
                    return CCA_XML_INFO_MAXAGE_SEARCHPREF;
                case ArgumentTypeMaskEnum.MinAgeGroup_SearchPref:
                    return CCA_XML_INFO_MINAGEGROUP_SEARCHPREF;
                case ArgumentTypeMaskEnum.MaxAgeGroup_SearchPref:
                    return CCA_XML_INFO_MAXAGEGROUP_SEARCHPREF;
                case ArgumentTypeMaskEnum.PRM:
                    return CCA_XML_INFO_PRM;
                case ArgumentTypeMaskEnum.Timestamp:
                    return CCA_XML_INFO_TIMESTAMP;
                case ArgumentTypeMaskEnum.LGID:
                    return CCA_XML_INFO_LGID;
                default:
                    return Matchnet.Constants.NULL_STRING;
            }
        }

        public static XmlDocument StringToXml(String xmlString)
        {
            XmlDocument xmlDoc = new XmlDocument();

            if (xmlString != Matchnet.Constants.NULL_STRING && xmlString != String.Empty)
            {
                xmlDoc.LoadXml(xmlString);
            }

            return xmlDoc;
        }

        /// <summary>
        /// Clear leading and trailing quotes, if any.
        /// NOTE:  Though all pixels created by the Pixel Administrator will contain quotes around their Values,
        /// pixels created before the Pixel Administrator may not.
        /// </summary>
        /// <param name="Text"></param>
        /// <returns></returns>
        public static string ClearQuotes(String text)
        {
            return text.Trim('\'');
        }
        #endregion

        #region Public Properties
        public ArgumentTypeMaskEnum ArgumentTypeMask
        {
            get
            {
                return argumentTypeMaskVal;
            }
            set
            {
                argumentTypeMaskVal = value;
            }
        }
        #endregion
    }
}
