using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.CompositeUI;

using Spark.Admin.Common;

namespace Spark.Admin.PixelModule
{
    public partial class Filter : Spark.Admin.Common.Controls.Form
    {
        private ListBox filterObjects;
        private FilterObject filterObject;

        #region Constructors
        public Filter(State globalState, ListBox filterObjects, Boolean isEdit)
            : base(globalState)
        {
            InitializeComponent();

            TypeComboBox.DataSource = FilterObject.FilterTypeList;
            TypeComboBox.DisplayMember = "Name";
            TypeComboBox.ValueMember = "ArgumentTypeMaskEnum";
            
            ComparisonComboBox.DataSource = FilterObject.ComparisonOperatorList;
            ComparisonComboBox.DisplayMember = "Name";
            ComparisonComboBox.ValueMember = "Sign";

            this.filterObjects = filterObjects;

            if (isEdit)
                filterObject = (FilterObject)(filterObjects.SelectedItem);

            DisplayFilter();
        }

        public Filter(State globalState, ListBox filterObjects)
            : this(globalState, filterObjects, false)
        {
        }
        #endregion

        #region Public Methods
        private void DisplayFilter()
        {
            if (filterObject != null)
            {
                TypeComboBox.SelectedValue = filterObject.ArgumentTypeMask;
                ComparisonComboBox.SelectedValue = filterObject.ComparisonOperator;
                ValueTextBox.Text = filterObject.Value;
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            AddFilter();
        }
        #endregion

        #region Private Methods
        private void AddFilter()
        {
            FilterObject thisFilterObject = BuildFilter();

            if (ValidateFilter(thisFilterObject))
            {
                // Remove the existing filterObject, if one exists.
                if (filterObject != null)
                    filterObjects.Items.Remove(filterObject);

                // Add the new filter.
                filterObjects.Items.Add(thisFilterObject);

                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private Boolean ValidateFilter(FilterObject thisFilterObject)
        {
            if ((Int32)thisFilterObject.ArgumentTypeMask == Matchnet.Constants.NULL_INT)
            {
                MessageBox.Show(ArgumentObject.MsgArgumentTypeMaskRequired, Constants.MsgBoxCaptionWarning);
                return false;
            }

            if (thisFilterObject.ComparisonOperator == String.Empty)
            {
                MessageBox.Show(ArgumentObject.MsgComparisonOperatorRequired, Constants.MsgBoxCaptionWarning);
                return false;
            }

            if (thisFilterObject.Value == String.Empty)
            {
                MessageBox.Show(ArgumentObject.MsgValueRequired, Constants.MsgBoxCaptionWarning);
                return false;
            }

            return true;
        }

        private FilterObject BuildFilter()
        {
            return new FilterObject((ArgumentObject.ArgumentTypeMaskEnum)TypeComboBox.SelectedValue, (String)ComparisonComboBox.SelectedValue, ValueTextBox.Text.Trim());
        }

        private void SCancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TypeComboBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            HintTextBox.Text = ((FilterType)TypeComboBox.SelectedItem).Hint;
        }
        #endregion
    }
}