using System;
using System.Collections;
using System.Text;
using System.Windows.Forms;

namespace Spark.Admin.PixelModule
{
    /// <summary>
    /// Summary description for ParameterObject.
    /// </summary>
    public class ParameterObject
    {
        #region Enums and Constants
        public static readonly ParameterType[] ParameterTypeList = new ParameterType[] {new ParameterType("Birth Date Parameter", ArgumentObject.ArgumentTypeMaskEnum.BirthDate),
													new ParameterType("GenderMask Parameter", ArgumentObject.ArgumentTypeMaskEnum.GenderMask),
													new ParameterType("Session ID Parameter", ArgumentObject.ArgumentTypeMaskEnum.SessionID),
													new ParameterType("Member ID Parameter", ArgumentObject.ArgumentTypeMaskEnum.MemberID),
													new ParameterType("Country ID Parameter", ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID),
													new ParameterType("Referring URL Parameter", ArgumentObject.ArgumentTypeMaskEnum.ReferringURL),
													new ParameterType("Subscription Amount Parameter", ArgumentObject.ArgumentTypeMaskEnum.SubscriptionAmount),
													new ParameterType("Subscription Duration Parameter", ArgumentObject.ArgumentTypeMaskEnum.SubscriptionDuration),
													new ParameterType("State Abbreviation Parameter", ArgumentObject.ArgumentTypeMaskEnum.StateAbbreviation),
													new ParameterType("Age Parameter", ArgumentObject.ArgumentTypeMaskEnum.Age),
													new ParameterType("Age Group Parameter", ArgumentObject.ArgumentTypeMaskEnum.AgeGroup),
													new ParameterType("PRM Parameter", ArgumentObject.ArgumentTypeMaskEnum.PRM),
													new ParameterType("Search Preference - GenderMask Parameter", ArgumentObject.ArgumentTypeMaskEnum.GenderMask),
													new ParameterType("Search Preference - Country ID Parameter", ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID_SearchPref),
													new ParameterType("Search Preference - State Abbreviation Parameter", ArgumentObject.ArgumentTypeMaskEnum.StateAbbreviation_SearchPref),
													new ParameterType("Search Preference - Min Age Parameter", ArgumentObject.ArgumentTypeMaskEnum.MinAge_SearchPref),
													new ParameterType("Search Preference - Max Age Parameter", ArgumentObject.ArgumentTypeMaskEnum.MaxAge_SearchPref),
													new ParameterType("Search Preference - Min Age Group Parameter", ArgumentObject.ArgumentTypeMaskEnum.MinAgeGroup_SearchPref),
													new ParameterType("Search Preference - Max Age Group Parameter", ArgumentObject.ArgumentTypeMaskEnum.MaxAgeGroup_SearchPref),
													new ParameterType("Timestamp Parameter", ArgumentObject.ArgumentTypeMaskEnum.Timestamp),
                                                    new ParameterType("LGID Parameter", ArgumentObject.ArgumentTypeMaskEnum.LGID)};
        #endregion
        #region Private Variables
        private ArgumentObject.ArgumentTypeMaskEnum argumentTypeMaskVal;
        private String parameterTypeName;
        private String key;
        #endregion

        #region Constructors
        public ParameterObject(ArgumentObject.ArgumentTypeMaskEnum ArgumentTypeMask, String keyObj)
        {
            argumentTypeMaskVal = ArgumentTypeMask;
            key = keyObj;

            SetReadOnlyProperties(argumentTypeMaskVal);
        }
        #endregion

        #region Public Methods
        public static String BuildParameterXml(ListBox parameters)
        {
            if (parameters.Items.Count == 0)
            {
                return Matchnet.Constants.NULL_STRING;
            }
            else
            {
                StringBuilder xmlString = new StringBuilder();

                // Add main opening tags.
                xmlString.Append("<urlArgs>");

                for (Int16 i = 0; i < parameters.Items.Count; i++)
                {
                    xmlString.Append(System.Web.HttpUtility.HtmlEncode(((ParameterObject)(parameters.Items[i])).Key));
                    xmlString.Append("<xsl:value-of select=\"");
                    xmlString.Append(ArgumentObject.GetArgumentInfo(((ParameterObject)(parameters.Items[i])).ArgumentTypeMask));
                    xmlString.Append("\" />");
                }

                // Add main closing tags.
                xmlString.Append("</urlArgs>");

                return xmlString.ToString();
            }
        }
        #endregion

        #region Private Methods
        private void SetReadOnlyProperties(ArgumentObject.ArgumentTypeMaskEnum argmentTypeMask)
        {
            switch (argmentTypeMask)
            {
                case ArgumentObject.ArgumentTypeMaskEnum.BirthDate:
                    parameterTypeName = "Birth Date Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.GenderMask:
                    parameterTypeName = "GenderMask Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.SessionID:
                    parameterTypeName = "Session ID Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.MemberID:
                    parameterTypeName = "Member ID Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID:
                    parameterTypeName = "Country Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.ReferringURL:
                    parameterTypeName = "Referring URL Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.SubscriptionDuration:
                    parameterTypeName = "Subscription Duration Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.SubscriptionAmount:
                    parameterTypeName = "Subscription Amount Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.StateAbbreviation:
                    parameterTypeName = "State Abbreviation Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.Age:
                    parameterTypeName = "Age Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.AgeGroup:
                    parameterTypeName = "Age Group Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.GenderMask_SearchPref:
                    parameterTypeName = "Search Preference - GenderMask Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID_SearchPref:
                    parameterTypeName = "Search Preference - Country ID Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.StateAbbreviation_SearchPref:
                    parameterTypeName = "Search Preference - State Abbreviation Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.MinAge_SearchPref:
                    parameterTypeName = "Search Preference - Min Age Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.MaxAge_SearchPref:
                    parameterTypeName = "Search Preference - Max Age Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.MinAgeGroup_SearchPref:
                    parameterTypeName = "Search Preference - Min Age Group Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.MaxAgeGroup_SearchPref:
                    parameterTypeName = "Search Preference - Max Age Group Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.PRM:
                    parameterTypeName = "PRM Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.Timestamp:
                    parameterTypeName = "Timestamp Parameter";
                    break;
                case ArgumentObject.ArgumentTypeMaskEnum.LGID:
                    parameterTypeName = "LGID Parameter";
                    break;
                default:
                    break;
            }
        }
        #endregion

        #region Public Properties
        public string ParameterTypeName
        {
            get
            {
                return parameterTypeName;
            }
        }

        public string Key
        {
            get
            {
                return key;
            }
        }

        public ArgumentObject.ArgumentTypeMaskEnum ArgumentTypeMask
        {
            get
            {
                return argumentTypeMaskVal;
            }
            set
            {
                argumentTypeMaskVal = value;
            }
        }

        public String DisplayName
        {
            get
            {
                return parameterTypeName + ": " + key;
            }
        }
        #endregion
    }

    public class ParameterType
    {
        private String name;
        private ArgumentObject.ArgumentTypeMaskEnum argumentTypeMaskEnum;

        #region Constructors
        public ParameterType(String name, ArgumentObject.ArgumentTypeMaskEnum argumentTypeMaskEnum)
        {
            this.name = name;
            this.argumentTypeMaskEnum = argumentTypeMaskEnum;
        }
        #endregion

        #region Properties
        public String Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public ArgumentObject.ArgumentTypeMaskEnum ArgumentTypeMaskEnum
        {
            get
            {
                return argumentTypeMaskEnum;
            }
            set
            {
                argumentTypeMaskEnum = value;
            }
        }
        #endregion
    }
}