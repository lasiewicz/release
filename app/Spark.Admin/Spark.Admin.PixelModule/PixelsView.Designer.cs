namespace Spark.Admin.PixelModule
{
    partial class PixelsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.SitesComboBox = new System.Windows.Forms.ComboBox();
            this.SiteLabel = new System.Windows.Forms.Label();
            this.PageIDLabel = new System.Windows.Forms.Label();
            this.PageIDTextBox = new System.Windows.Forms.TextBox();
            this.LoadButton = new System.Windows.Forms.Button();
            this.PixelsDataGridView = new System.Windows.Forms.DataGridView();
            this.AddButton = new System.Windows.Forms.Button();
            this.ServiceEnvironmentLabel = new System.Windows.Forms.Label();
            this.ServiceEnvironmentComboBox = new System.Windows.Forms.ComboBox();
            this.ControlPanel = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.PixelsDataGridView)).BeginInit();
            this.ControlPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // SitesComboBox
            // 
            this.SitesComboBox.FormattingEnabled = true;
            this.SitesComboBox.Location = new System.Drawing.Point(44, 11);
            this.SitesComboBox.Name = "SitesComboBox";
            this.SitesComboBox.Size = new System.Drawing.Size(210, 21);
            this.SitesComboBox.TabIndex = 0;
            // 
            // SiteLabel
            // 
            this.SiteLabel.AutoSize = true;
            this.SiteLabel.Location = new System.Drawing.Point(8, 14);
            this.SiteLabel.Name = "SiteLabel";
            this.SiteLabel.Size = new System.Drawing.Size(30, 13);
            this.SiteLabel.TabIndex = 1;
            this.SiteLabel.Text = "Sites";
            this.SiteLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // PageIDLabel
            // 
            this.PageIDLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PageIDLabel.AutoSize = true;
            this.PageIDLabel.Location = new System.Drawing.Point(281, 12);
            this.PageIDLabel.Name = "PageIDLabel";
            this.PageIDLabel.Size = new System.Drawing.Size(43, 13);
            this.PageIDLabel.TabIndex = 2;
            this.PageIDLabel.Text = "PageID";
            this.PageIDLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // PageIDTextBox
            // 
            this.PageIDTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PageIDTextBox.Location = new System.Drawing.Point(330, 10);
            this.PageIDTextBox.Name = "PageIDTextBox";
            this.PageIDTextBox.Size = new System.Drawing.Size(85, 20);
            this.PageIDTextBox.TabIndex = 3;
            // 
            // LoadButton
            // 
            this.LoadButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LoadButton.Location = new System.Drawing.Point(421, 9);
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size(75, 23);
            this.LoadButton.TabIndex = 4;
            this.LoadButton.Text = "Load";
            this.LoadButton.UseVisualStyleBackColor = true;
            this.LoadButton.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // PixelsDataGridView
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PixelsDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.PixelsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.PixelsDataGridView.DefaultCellStyle = dataGridViewCellStyle8;
            this.PixelsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PixelsDataGridView.Location = new System.Drawing.Point(0, 40);
            this.PixelsDataGridView.MultiSelect = false;
            this.PixelsDataGridView.Name = "PixelsDataGridView";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.PixelsDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.PixelsDataGridView.Size = new System.Drawing.Size(800, 560);
            this.PixelsDataGridView.TabIndex = 5;
            this.PixelsDataGridView.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.PixelsDataGridView_RowHeaderMouseDoubleClick);
            this.PixelsDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.PixelsDataGridView_ColumnHeaderMouseClick);
            // 
            // AddButton
            // 
            this.AddButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.AddButton.Location = new System.Drawing.Point(502, 9);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(75, 23);
            this.AddButton.TabIndex = 6;
            this.AddButton.Text = "Add";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // ServiceEnvironmentLabel
            // 
            this.ServiceEnvironmentLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ServiceEnvironmentLabel.AutoSize = true;
            this.ServiceEnvironmentLabel.Location = new System.Drawing.Point(689, 0);
            this.ServiceEnvironmentLabel.Name = "ServiceEnvironmentLabel";
            this.ServiceEnvironmentLabel.Size = new System.Drawing.Size(105, 13);
            this.ServiceEnvironmentLabel.TabIndex = 7;
            this.ServiceEnvironmentLabel.Text = "Service Environment";
            // 
            // ServiceEnvironmentComboBox
            // 
            this.ServiceEnvironmentComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ServiceEnvironmentComboBox.FormattingEnabled = true;
            this.ServiceEnvironmentComboBox.Location = new System.Drawing.Point(673, 11);
            this.ServiceEnvironmentComboBox.Name = "ServiceEnvironmentComboBox";
            this.ServiceEnvironmentComboBox.Size = new System.Drawing.Size(121, 21);
            this.ServiceEnvironmentComboBox.TabIndex = 8;
            this.ServiceEnvironmentComboBox.SelectedIndexChanged += new System.EventHandler(this.ServiceEnvironmentComboBox_SelectedIndexChanged);
            // 
            // ControlPanel
            // 
            this.ControlPanel.Controls.Add(this.ServiceEnvironmentComboBox);
            this.ControlPanel.Controls.Add(this.AddButton);
            this.ControlPanel.Controls.Add(this.LoadButton);
            this.ControlPanel.Controls.Add(this.PageIDTextBox);
            this.ControlPanel.Controls.Add(this.PageIDLabel);
            this.ControlPanel.Controls.Add(this.ServiceEnvironmentLabel);
            this.ControlPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.ControlPanel.Location = new System.Drawing.Point(0, 0);
            this.ControlPanel.Name = "ControlPanel";
            this.ControlPanel.Size = new System.Drawing.Size(800, 40);
            this.ControlPanel.TabIndex = 9;
            // 
            // PixelsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.PixelsDataGridView);
            this.Controls.Add(this.SiteLabel);
            this.Controls.Add(this.SitesComboBox);
            this.Controls.Add(this.ControlPanel);
            this.Name = "PixelsView";
            this.Size = new System.Drawing.Size(800, 600);
            ((System.ComponentModel.ISupportInitialize)(this.PixelsDataGridView)).EndInit();
            this.ControlPanel.ResumeLayout(false);
            this.ControlPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox SitesComboBox;
        private System.Windows.Forms.Label SiteLabel;
        private System.Windows.Forms.Label PageIDLabel;
        private System.Windows.Forms.TextBox PageIDTextBox;
        private System.Windows.Forms.Button LoadButton;
        private System.Windows.Forms.DataGridView PixelsDataGridView;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Label ServiceEnvironmentLabel;
        private System.Windows.Forms.ComboBox ServiceEnvironmentComboBox;
        private System.Windows.Forms.Panel ControlPanel;


    }
}
