﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Spark.Admin
{
    public partial class LoginForm : Spark.Admin.Common.Controls.Form
    {
        public String Login = null;
        public String Password = null;

        public LoginForm()
        {
            InitializeComponent();

            pictureBox1.Image = Properties.Resources.Security.ToBitmap();
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            Login = LoginTextBox.Text;
            Password = PasswordTextBox.Text;

            this.DialogResult = DialogResult.OK;

            this.Close();
        }

        private void LoginCancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            this.Close();
        }
    }
}