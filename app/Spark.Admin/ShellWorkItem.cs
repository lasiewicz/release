using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI;

namespace Spark.Admin
{
    public class ShellWorkItem : WorkItem
    {
        #region Public Methods
        /// <summary>
        /// Create and display child WorkItems that need to execute when the application starts, here.
        /// </summary>
        protected override void OnRunStarted()
        {
            base.OnRunStarted();
        }
        #endregion
    }
}
