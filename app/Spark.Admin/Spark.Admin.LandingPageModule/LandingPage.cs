using System;
using System.Collections.Generic;
using System.Text;

using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ServiceAdapters;

namespace Spark.Admin.LandingPageModule
{
    public class LandingPage
    {
        private String m_name;
        private Int32 m_landingpageid;
        private bool m_isActive;
        private bool m_isStatic;
        private String m_url;
        private String m_controlName;
        private String m_description;
        private WebServiceProxy proxy = new WebServiceProxy();
        private LandingPageController m_landingpageController;

        public LandingPage(LandingPageController landingpageController)
        {
            m_landingpageController = landingpageController;
        }

        public LandingPage(Int32 lpid, LandingPageController landingpageController)
        {
            try
            {
                Matchnet.Content.ValueObjects.LandingPage.LandingPage lp = proxy.GetLandingPage(lpid, landingpageController.CurrentServiceEnvironmentTypeEnum);
                if (lp.LandingPageID > 0)
                {
                    m_landingpageid = lpid;
                    m_name = lp.LandingPageName;
                    m_isActive = lp.IsActive;
                    m_isStatic = lp.IsStaticLandingPage;
                    m_url = lp.StaticURL;
                    m_controlName = lp.ControlName;
                    m_description = lp.Description;
                    m_landingpageController = landingpageController;
                }
            }
            catch { }
        }

        public LandingPage(String name, String url, String controlName, String description, bool isactive, bool isstatic, LandingPageController landingpageController)
        {
            m_name = name;
            m_url = url;
            m_controlName = controlName;
            m_description = description;
            m_isActive = isactive;
            m_isStatic = isstatic;
            m_landingpageController = landingpageController;
        }

        public bool Add(String name, String url, String controlName, String description, bool isactive, bool isstatic, Int32 memberid, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            try
            {
                //m_landingpageid = Matchnet.Content.ServiceAdapters.LandingPageSA.Instance.AddLandingPage(name, description, controlName, url, isstatic, memberid, isactive, memberid);
                m_landingpageid = proxy.AddLandingPage(name, url, controlName, description, isactive, isstatic, memberid, m_landingpageController.CurrentServiceEnvironmentTypeEnum);
                m_name = name;
                m_url = url;
                m_controlName = controlName;
                m_description = description;
                m_isActive = isactive;
                m_isStatic = isstatic;
            }
            catch
            {
                return false;
            }
            return true;
        }

        public bool Update(Int32 landingpageid, String name, String url, String controlName, String description, bool isactive, bool isstatic, Int32 memberid, bool ispublish, Int32 publishid, ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum)
        {
            try
            {
                //Matchnet.Content.ServiceAdapters.LandingPageSA.Instance.SaveLandingPage(landingpageid, name, description,
                //    controlName, url, isstatic, memberid, isactive);
                proxy.SaveLandingPage(landingpageid, name, url, controlName, description, isactive, isstatic, memberid, ispublish, publishid, m_landingpageController.CurrentServiceEnvironmentTypeEnum);
                m_name = name;
                m_url = url;
                m_controlName = controlName;
                m_description = description;
                m_isActive = isactive;
                m_isStatic = isstatic;
            }
            catch {
                return false;
            }
            return true;
        }

        #region properties
        public String Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
            }
        }

        public Int32 LandingPageID
        {
            get
            {
                return m_landingpageid;
            }
            set
            {
                m_landingpageid = value;
            }
        }

        public bool IsActive
        {
            get
            {
                return m_isActive;
            }
            set
            {
                m_isActive = value;
            }
        }

        public bool IsStatic
        {
            get
            {
                return m_isStatic;
            }
            set
            {
                m_isStatic = value;
            }
        }

        public String URL
        {
            get
            {
                return m_url;
            }
            set
            {
                m_url = value;
            }
        }

        public String Description
        {
            get
            {
                return m_description;
            }
            set
            {
                m_description = value;
            }
        }

        public String ControlName
        {
            get
            {
                return m_controlName;
            }
            set
            {
                m_controlName = value;
            }
        }

        #endregion
    }
}
