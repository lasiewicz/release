namespace Spark.Admin.LandingPageModule
{
    partial class LandingPageView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LPDataGridView = new System.Windows.Forms.DataGridView();
            this.URL = new System.Windows.Forms.DataGridViewLinkColumn();
            this.LPID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LandingPageName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Active = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.pnlGrid = new System.Windows.Forms.Panel();
            this.cmdEdit = new System.Windows.Forms.Button();
            this.txtLPID = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmdAdd = new System.Windows.Forms.Button();
            this.ServiceEnvironmentComboBox = new System.Windows.Forms.ComboBox();
            this.ServiceEnvironmentLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.LPDataGridView)).BeginInit();
            this.pnlGrid.SuspendLayout();
            this.SuspendLayout();
            // 
            // LPDataGridView
            // 
            this.LPDataGridView.AllowUserToAddRows = false;
            this.LPDataGridView.AllowUserToDeleteRows = false;
            this.LPDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.LPDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.URL,
            this.LPID,
            this.LandingPageName,
            this.Description,
            this.Active});
            this.LPDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LPDataGridView.Location = new System.Drawing.Point(0, 0);
            this.LPDataGridView.Name = "LPDataGridView";
            this.LPDataGridView.ReadOnly = true;
            this.LPDataGridView.Size = new System.Drawing.Size(781, 497);
            this.LPDataGridView.TabIndex = 0;
            this.LPDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.LPDataGridView_ColumnHeaderMouseClick);
            this.LPDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.LPDataGridView_CellContentClick);
            // 
            // URL
            // 
            this.URL.DataPropertyName = "StaticURL";
            this.URL.HeaderText = "URL";
            this.URL.Name = "URL";
            this.URL.ReadOnly = true;
            // 
            // LPID
            // 
            this.LPID.DataPropertyName = "LandingPageID";
            this.LPID.HeaderText = "LPID";
            this.LPID.Name = "LPID";
            this.LPID.ReadOnly = true;
            // 
            // LandingPageName
            // 
            this.LandingPageName.DataPropertyName = "LandingPageName";
            this.LandingPageName.HeaderText = "Name";
            this.LandingPageName.Name = "LandingPageName";
            this.LandingPageName.ReadOnly = true;
            // 
            // Description
            // 
            this.Description.DataPropertyName = "Description";
            this.Description.HeaderText = "Description";
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            // 
            // Active
            // 
            this.Active.DataPropertyName = "IsActive";
            this.Active.FalseValue = "0";
            this.Active.HeaderText = "Active?";
            this.Active.Name = "Active";
            this.Active.ReadOnly = true;
            this.Active.TrueValue = "1";
            // 
            // pnlGrid
            // 
            this.pnlGrid.Controls.Add(this.LPDataGridView);
            this.pnlGrid.Location = new System.Drawing.Point(3, 50);
            this.pnlGrid.Name = "pnlGrid";
            this.pnlGrid.Size = new System.Drawing.Size(781, 497);
            this.pnlGrid.TabIndex = 1;
            // 
            // cmdEdit
            // 
            this.cmdEdit.Location = new System.Drawing.Point(204, 13);
            this.cmdEdit.Name = "cmdEdit";
            this.cmdEdit.Size = new System.Drawing.Size(75, 20);
            this.cmdEdit.TabIndex = 2;
            this.cmdEdit.Text = "Edit";
            this.cmdEdit.UseVisualStyleBackColor = true;
            this.cmdEdit.Click += new System.EventHandler(this.cmdEdit_Click_1);
            // 
            // txtLPID
            // 
            this.txtLPID.Location = new System.Drawing.Point(88, 13);
            this.txtLPID.Mask = "999999999";
            this.txtLPID.Name = "txtLPID";
            this.txtLPID.Size = new System.Drawing.Size(100, 20);
            this.txtLPID.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Edit by LPID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(296, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "...or select a Landing Page below.";
            // 
            // cmdAdd
            // 
            this.cmdAdd.Location = new System.Drawing.Point(495, 13);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(109, 23);
            this.cmdAdd.TabIndex = 7;
            this.cmdAdd.Text = "Add Landing Page";
            this.cmdAdd.UseVisualStyleBackColor = true;
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // ServiceEnvironmentComboBox
            // 
            this.ServiceEnvironmentComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ServiceEnvironmentComboBox.FormattingEnabled = true;
            this.ServiceEnvironmentComboBox.Location = new System.Drawing.Point(645, 23);
            this.ServiceEnvironmentComboBox.Name = "ServiceEnvironmentComboBox";
            this.ServiceEnvironmentComboBox.Size = new System.Drawing.Size(121, 21);
            this.ServiceEnvironmentComboBox.TabIndex = 10;
            this.ServiceEnvironmentComboBox.SelectedIndexChanged += new System.EventHandler(this.ServiceEnvironmentComboBox_SelectedIndexChanged);
            // 
            // ServiceEnvironmentLabel
            // 
            this.ServiceEnvironmentLabel.AutoSize = true;
            this.ServiceEnvironmentLabel.Location = new System.Drawing.Point(642, 6);
            this.ServiceEnvironmentLabel.Name = "ServiceEnvironmentLabel";
            this.ServiceEnvironmentLabel.Size = new System.Drawing.Size(105, 13);
            this.ServiceEnvironmentLabel.TabIndex = 9;
            this.ServiceEnvironmentLabel.Text = "Service Environment";
            // 
            // LandingPageView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ServiceEnvironmentComboBox);
            this.Controls.Add(this.ServiceEnvironmentLabel);
            this.Controls.Add(this.cmdAdd);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtLPID);
            this.Controls.Add(this.cmdEdit);
            this.Controls.Add(this.pnlGrid);
            this.Name = "LandingPageView";
            this.Size = new System.Drawing.Size(915, 577);
            this.Resize += new System.EventHandler(this.LandingPageView_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.LPDataGridView)).EndInit();
            this.pnlGrid.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView LPDataGridView;
        private System.Windows.Forms.Panel pnlGrid;
        private System.Windows.Forms.DataGridViewLinkColumn URL;
        private System.Windows.Forms.DataGridViewTextBoxColumn LPID;
        private System.Windows.Forms.DataGridViewTextBoxColumn LandingPageName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Active;
        private System.Windows.Forms.Button cmdEdit;
        private System.Windows.Forms.MaskedTextBox txtLPID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button cmdAdd;
        private System.Windows.Forms.ComboBox ServiceEnvironmentComboBox;
        private System.Windows.Forms.Label ServiceEnvironmentLabel;

    }
}
