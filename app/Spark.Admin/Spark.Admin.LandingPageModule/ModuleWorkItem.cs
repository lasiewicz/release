using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.CompositeUI.WinForms;

using Spark.Admin.Common;

namespace Spark.Admin.LandingPageModule
{
    public class ModuleWorkItem : BaseModuleWorkItem
    {
        public const Int32 PrivilegeIDLPEditor = 146;
        public const Int32 PrivilegeIDLPApprover = 148;
        public const Int32 PrivilegeIDLPPublisher = 142;
        private IWorkspace workspace;

        #region Public Methods
        public void Run(IWorkspace workspace)
        {
            this.workspace = workspace;

            LandingPageView landingpageView = this.Items.AddNew<LandingPageView>();
            // You must manually assign GlobalState to the ModuleView.
            landingpageView.GlobalState = GlobalState;

            this.Items.Add(landingpageView);

            // Set the Tab properties.
            TabSmartPartInfo tabSmartPartInfo = new TabSmartPartInfo();
            tabSmartPartInfo.Title = landingpageView.Title;

            workspace.Show(landingpageView, tabSmartPartInfo);
        }
        #endregion
    }
}
