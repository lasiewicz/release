using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI;

using Spark.Admin.Common;
using Spark.Admin.WebServices;
using Matchnet.Content.ValueObjects.Article;
using Matchnet.Content.ValueObjects.Admin;

namespace Spark.Admin.LandingPageModule
{
    public class LandingPageController : BaseController
    {
        private WebServiceProxy webServiceProxy;

        #region Constructors
        public LandingPageController()
        {
            // Initialize the WebServiceProxy if you are using web services.
            webServiceProxy = new WebServiceProxy();        
        }
        #endregion
    }
}
