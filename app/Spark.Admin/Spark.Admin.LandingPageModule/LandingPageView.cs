using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.CompositeUI;

using Spark.Admin.WebServices;
using Matchnet.Content.ValueObjects.Article;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.LandingPage;

using Spark.Admin.Common;
namespace Spark.Admin.LandingPageModule
{
    public partial class LandingPageView : BaseModuleView
    {
        private const String TitleLandingPage = "Landing Pages";
        private LandingPageController landingpageController;
        private State globalState;
        private WebServiceProxy proxy = new WebServiceProxy();
        private ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum = ServiceEnvironmentTypeEnum.Dev;
        
        public LandingPageView()
        {
            InitializeComponent();

            // Set the Tab title.
            this.Title = TitleLandingPage;
        }

        public LandingPageView(State globalstate)
        {
            InitializeComponent();

            // Set the Tab title.
            this.Title = TitleLandingPage;
            globalState = globalstate;
        }

        #region event handlers
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // You must manually assign GlobalState to the Controller.
            landingpageController.GlobalState = GlobalState;
            foreach (DataGridViewColumn c in LPDataGridView.Columns)
                c.Tag = "D";

            // Set up ServiceEnvironment ComboBox.
            ServiceEnvironmentComboBox.DataSource = Enum.GetValues(typeof(ServiceEnvironmentTypeEnum));
            currentServiceEnvironmentTypeEnum = ((ServiceEnvironmentTypeEnum)ServiceEnvironmentComboBox.SelectedValue);

            LoadLandingPages("url", "A");
        }

        private void LPDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            String sortDir;
            if (LPDataGridView.Columns[e.ColumnIndex].Tag.ToString() == "D")
            {
                sortDir = "A";
                LPDataGridView.Columns[e.ColumnIndex].Tag = "A";
            }
            else
            {
                sortDir = "D";
                LPDataGridView.Columns[e.ColumnIndex].Tag = "D";
            }

            switch (e.ColumnIndex)
            { 
                case 0:
                    LoadLandingPages("url", sortDir);
                    break;
                case 1:
                    LoadLandingPages("lpid", sortDir);
                    break;
                case 2:
                    LoadLandingPages("lpname", sortDir);
                    break;
                case 3:
                    LoadLandingPages("dscr", sortDir);
                    break;
                case 4:
                    LoadLandingPages("active", sortDir);
                    break;
                default:
                    LoadLandingPages("url", sortDir);
                    break;
            }
        }

        private void LandingPageView_Resize(object sender, EventArgs e)
        {
            pnlGrid.Width = this.Width;
            pnlGrid.Height = this.Height - 50;
        }

        private void cmdEdit_Click(object sender, EventArgs e)
        {
            /*EditLandingPage editForm = new EditLandingPage(GlobalState, landingpageController, EditLandingPage.EditMode.Edit);

            if (editForm.ShowDialog() == DialogResult.OK)
            {
                return;
            }*/
        }

        private void cmdEdit_Click_1(object sender, EventArgs e)
        {
            try
            {
                int lpid = Convert.ToInt32(txtLPID.Text);
                LandingPage lp = proxy.GetLandingPage(lpid, landingpageController.CurrentServiceEnvironmentTypeEnum);
                if (lp.LandingPageID > 0)
                {
                    EditLandingPage editForm = new EditLandingPage(GlobalState, landingpageController, EditLandingPage.EditMode.Edit, lpid);
                    if (editForm.ShowDialog() == DialogResult.OK)
                    {
                        LoadLandingPages("url", "A");
                        return;
                    }
                }
                else
                    MessageBox.Show("A landing page with this ID was not found. Please check the ID and try again.");
            }
            catch {
                MessageBox.Show("A landing page with this ID was not found. Please check the ID and try again.");
            }
        }
        #endregion

        #region private methods
        private void LoadLandingPages(String sortExp, string sortDir)
        {
            LPDataGridView.DataSource = null;
            LPDataGridView.AutoGenerateColumns = false;
            LPDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            try
            {
                ICollection results = null;
                if (sortDir == "A")
                    results = proxy.GetLandingPages(1, 500, GetSortExp(sortExp), Direction.Ascending, currentServiceEnvironmentTypeEnum);
                else
                    results = proxy.GetLandingPages(1, 500, GetSortExp(sortExp), Direction.Descending, currentServiceEnvironmentTypeEnum);
                LPDataGridView.DataSource = results;
            }
            catch (Exception ex)
            {
                String msg = ex.Message;
            }
        }
        #endregion
        
        #region Properties
        [CreateNew]
        public LandingPageController LandingPageController
        {
            set
            {
                landingpageController = value;
            }
        }
        #endregion
        
        #region properties
        private SortType GetSortExp(string sortExpression)
        {
            if (sortExpression.IndexOf(" ") > 1)
                sortExpression = sortExpression.Substring(0, sortExpression.IndexOf(" "));

            switch (sortExpression)
            {
                case "url":
                    return SortType.StaticURL;
                case "lpid":
                    return SortType.LandingPageID;
                case "lpname":
                    return SortType.LandingPageName;
                case "dscr":
                    return SortType.Description;
                case "active":
                    return SortType.IsActive;
                default:
                    return SortType.Description;
            }
        }
        #endregion

        private void LPDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Int32 lpid = Convert.ToInt32(LPDataGridView[e.ColumnIndex + 1, e.RowIndex].Value);
                /*LandingPage lp = proxy.GetLandingPage(lpid, landingpageController.CurrentServiceEnvironmentTypeEnum);
                if (lp.LandingPageID == lpid)
                {
                    //Landing Page was found */
                    EditLandingPage editForm = new EditLandingPage(GlobalState, landingpageController, EditLandingPage.EditMode.Edit, lpid);
                    if (editForm.ShowDialog() == DialogResult.OK)
                    {
                        LoadLandingPages("url", "A");
                        return;
                    }
                //}
            }
            catch(Exception ex)
            {
                String msg = ex.Message;
            }
        }

        private void ServiceEnvironmentComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentServiceEnvironmentTypeEnum = ((ServiceEnvironmentTypeEnum)ServiceEnvironmentComboBox.SelectedValue);

            // Set the currentServiceEnvironmentTypeEnum so that the other Forms know what ServiceEnvironmentType we're working on.
            landingpageController.CurrentServiceEnvironmentTypeEnum = currentServiceEnvironmentTypeEnum;

            LoadLandingPages("url", "A");

            if (ServiceEnvironmentComboBox.SelectedValue.ToString().ToUpper()!="DEV")
                cmdAdd.Visible = false;
            else
                cmdAdd.Visible = true;
        }

        private void cmdAdd_Click(object sender, EventArgs e)
        {
            EditLandingPage editForm = new EditLandingPage(GlobalState, landingpageController, EditLandingPage.EditMode.Add);
            if (editForm.ShowDialog() == DialogResult.OK)
            {
                LoadLandingPages("url", "A");
                return;
            }
        }
    }
}
