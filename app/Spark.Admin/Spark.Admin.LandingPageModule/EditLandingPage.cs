using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.CompositeUI;

using Spark.Admin.WebServices;
using Spark.Admin.Common;
using Matchnet.Content.ValueObjects.Article;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.LandingPage;

namespace Spark.Admin.LandingPageModule
{
    public partial class EditLandingPage : Spark.Admin.Common.Controls.Form
    {
        private Matchnet.Content.ValueObjects.LandingPage.LandingPage m_landingpage = null;
        private LandingPageController landingpageController;
        private State globalState;
        private WebServiceProxy proxy = new WebServiceProxy();
        private Int32 nextPublishActionPublishObjectID = Matchnet.Constants.NULL_INT;
        private String nextStepText = String.Empty;
        ServiceEnvironmentTypeEnum nextStepServiceEnvironmentTypeEnum;

        #region Publish Constants
        private const Int32 PubActPubObjIDLPgAddDev = 400;
        private const Int32 PubActPubObjIDLPgEditDev = 401;
        private const Int32 PubActPubObjIDLPgDeleteDev = 402;
        private const Int32 PubActPubObjIDLPgVerifyDev = 403;
        private const Int32 PubActPubObjIDLPgPublishFilesToContentStg = 404;
        private const Int32 PubActPubObjIDLPgPublishDBRecordToContentStg = 405;
        private const Int32 PubActPubObjIDLPgVerifyInContentStg = 406;
        private const Int32 PubActPubObjIDLPgPublishFilesToProd = 407;
        private const Int32 PubActPubObjIDLPgPublishDBRecordToProd = 408;
        private const Int32 PubActPubObjIDLPgVerifyInProd = 409;
        #endregion

        #region Message Constants
        private const String MsgSaveLPConfirm = "Are you sure you want to save this Landing Page?";
        private const String MsgNextStepLPConfirm = "Are you sure you want to {0} this Landing Page?";
        #endregion


        #region constructors
        public EditLandingPage()
        {
            InitializeComponent();
        }

        public EditLandingPage(State globalstate, LandingPageController lpController, EditMode editMode) : base(globalstate)
        {
            InitializeComponent();

            this.landingpageController = lpController;
            this.globalState = globalstate;

            //This has to be an add, we weren't given a landingpage object
            lblLandingPageID.Text = "(new)";
            cmdUpdate.Text = "Add";
            this.Text = "Add Landing Page";
            NextStepButton.Visible = false;
        }

        public EditLandingPage(State globalstate, LandingPageController lpController, EditMode editMode, Int32 lpid) : base(globalstate)
        {
            InitializeComponent();

            this.landingpageController = lpController;
            this.globalState = globalstate;

            if (editMode == EditMode.Add)
            {
                lblLandingPageID.Text = "(new)";
                cmdUpdate.Text = "Add";
                this.Text = "Add Landing Page";
                NextStepButton.Visible = false;
            }
            else 
            {
                // Get information about the next Publish step.
                nextPublishActionPublishObjectID = CommonController.GetNextPublishActionPublishObjectStep(lpid, PublishObjectType.LandingPage, out nextStepServiceEnvironmentTypeEnum);
                m_landingpage = proxy.GetLandingPage(lpid, landingpageController.CurrentServiceEnvironmentTypeEnum);
                InitFields();
                InitializeNextStep();
                if (landingpageController.CurrentServiceEnvironmentTypeEnum != ServiceEnvironmentTypeEnum.Dev)
                    cmdUpdate.Visible = false;
            }
        }

        #endregion

        #region event handlers

        private void NextStepButton_Click(object sender, EventArgs e)
        {
            // Are you sure?
            if (MessageBox.Show(String.Format(MsgNextStepLPConfirm, NextStepButton.Tag), Constants.MsgBoxCaptionConfirm, MessageBoxButtons.YesNo) == DialogResult.No)
                return;
            else
            {
                try
                {
                    Main main = null;
                    // Do the correct action.
                    switch (nextPublishActionPublishObjectID)
                    {
                        case PubActPubObjIDLPgPublishFilesToContentStg:
                            main = new Main(nextPublishActionPublishObjectID);
                            if(main.ShowDialog()==DialogResult.OK)
                                CommonController.SavePublishAction(m_landingpage.PublishID, nextPublishActionPublishObjectID, MemberID);
                            break;
                        case PubActPubObjIDLPgPublishDBRecordToContentStg:
                            try
                            {
                                ServiceEnvironmentTypeEnum oldEnv = landingpageController.CurrentServiceEnvironmentTypeEnum;
                                landingpageController.CurrentServiceEnvironmentTypeEnum = ServiceEnvironmentTypeEnum.ContentStg;
                                UpdateLandingPage();
                                landingpageController.CurrentServiceEnvironmentTypeEnum = oldEnv;
                                //LandingPage lp = proxy.GetLandingPage(m_landingpage.LandingPageID, landingpageController.CurrentServiceEnvironmentTypeEnum);
                                //InitializeNextStep();
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                            CommonController.SavePublishAction(m_landingpage.PublishID, nextPublishActionPublishObjectID, MemberID);

                            break;

                        case PubActPubObjIDLPgPublishFilesToProd:
                            main = new Main(nextPublishActionPublishObjectID);
                            if (main.ShowDialog() == DialogResult.OK)
                                CommonController.SavePublishAction(m_landingpage.PublishID, PubActPubObjIDLPgPublishFilesToProd, MemberID);
                            break;

                        case PubActPubObjIDLPgPublishDBRecordToProd:
                            try
                            {
                                ServiceEnvironmentTypeEnum oldEnv = landingpageController.CurrentServiceEnvironmentTypeEnum;
                                landingpageController.CurrentServiceEnvironmentTypeEnum = ServiceEnvironmentTypeEnum.Prod;
                                UpdateLandingPage();
                                landingpageController.CurrentServiceEnvironmentTypeEnum = oldEnv;
                                //LandingPage lp = proxy.GetLandingPage(m_landingpage.LandingPageID, true, landingpageController.CurrentServiceEnvironmentTypeEnum);
                                //InitializeNextStep();
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                            CommonController.SavePublishAction(m_landingpage.PublishID, PubActPubObjIDLPgPublishFilesToContentStg, MemberID);

                            break;
                        // All Verifies are handled here.
                        default:
                            CommonController.SavePublishAction(m_landingpage.PublishID, nextPublishActionPublishObjectID, MemberID);

                            break;
                    }

                    DialogResult = DialogResult.OK;
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(String.Format(Constants.MsgContactAdministrator, ex.Message), Constants.MsgBoxCaptionSystemError);
                    return;
                }
            }
        }

        private void EditLandingPage_Load(object sender, EventArgs e)
        {

        }

         private void button1_Click(object sender, EventArgs e)
        {
            if (txtURL.Text.Length > 0)
            {
                Process.Start(new ProcessStartInfo("iexplore", "-new " + txtURL.Text));
            }
            else
                MessageBox.Show("Please enter a URL.");
        }

        private void cmdUpdate_Click(object sender, EventArgs e)
        {
            if (m_landingpage != null)
            {
                UpdateLandingPage();
                this.DialogResult = DialogResult.OK;
            }
            else
            { 
                //Adding
                try
                {
                    if (txtName.Text.Trim().Length == 0)
                    {
                        lblError.Text = "Please enter a landing page name.";
                        return;
                    }
                    else if(txtURL.Text.Trim().Length == 0)
                    {
                        lblError.Text = "Please enter a URL.";
                        return;
                    }
                    if (proxy.AddLandingPage(txtName.Text, txtURL.Text, txtControlName.Text, txtDescription.Text, chkActive.Checked, chkStatic.Checked, Convert.ToInt32(this.MemberID), landingpageController.CurrentServiceEnvironmentTypeEnum)>0)
                        this.Close();
                    else
                        lblError.Text = "An error occurred.";
                }
                catch { }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region private methods
        private void InitFields()
        {
            lblLandingPageID.Text = Convert.ToString(m_landingpage.LandingPageID);
            txtName.Text = m_landingpage.LandingPageName;
            txtControlName.Text = m_landingpage.ControlName;
            txtDescription.Text = m_landingpage.Description;
            txtURL.Text = m_landingpage.StaticURL;
            chkActive.Checked = m_landingpage.IsActive;
            chkStatic.Checked = m_landingpage.IsStaticLandingPage;
            if (m_landingpage.PublishID > -1)
                txtPublishID.Text = m_landingpage.PublishID.ToString();
        }

        private void SetLabels()
        {
            if (landingpageController.CurrentServiceEnvironmentTypeEnum == nextStepServiceEnvironmentTypeEnum)
            {
                switch (nextPublishActionPublishObjectID)
                {
                    case PubActPubObjIDLPgVerifyDev:
                        lblCurrentStep.Text = "Verify in Dev";
                        NextStepButton.Text = "Verify in Dev";
                        NextStepButton.Tag = "Verify";
                        break;
                    case PubActPubObjIDLPgPublishFilesToContentStg:
                        lblCurrentStep.Text = "Publish Files to Content Stage";
                        NextStepButton.Text = "Publish Files to Content Stage";
                        NextStepButton.Tag = "Publish";
                        break;
                    case PubActPubObjIDLPgPublishDBRecordToContentStg:
                        lblCurrentStep.Text = "Publish Database Record to Content Stage";
                        NextStepButton.Text = "Publish Record to Content Stage";
                        NextStepButton.Tag = "Publish";
                        break;
                    case PubActPubObjIDLPgVerifyInContentStg:
                        lblCurrentStep.Text = "Verify in Content Stage";
                        NextStepButton.Text = "Verify in Content Stage";
                        NextStepButton.Tag = "Verify";
                        break;
                    case PubActPubObjIDLPgPublishFilesToProd:
                        lblCurrentStep.Text = "Publish Files to Prod";
                        NextStepButton.Text = "Publish Files to Prod";
                        NextStepButton.Tag = "Publish";
                        break;
                    case PubActPubObjIDLPgPublishDBRecordToProd:
                        lblCurrentStep.Text = "Publish Record to Prod";
                        NextStepButton.Text = "Publish Record to Prod";
                        NextStepButton.Tag = "Publish";
                        break;
                    case PubActPubObjIDLPgVerifyInProd:
                        lblCurrentStep.Text = "Verify in Prod";
                        NextStepButton.Text = "Verify in Prod";
                        NextStepButton.Tag = "Verify";
                        break;
                }
            }
            else
                lblNextStep.Visible = false;

            switch (landingpageController.CurrentServiceEnvironmentTypeEnum)
            { 
                case ServiceEnvironmentTypeEnum.Dev:
                    lblCurrentEnvironment.Text = "DEV";
                    break;
                case ServiceEnvironmentTypeEnum.ContentStg:
                    lblCurrentEnvironment.Text = "Content Stage";
                    break;
                case ServiceEnvironmentTypeEnum.Prod:
                    lblCurrentEnvironment.Text = "Production";
                    break;
            }
        }

        private void UpdateLandingPage()
        {
            try
            {
                int publishid = 0;
                if (txtPublishID.Text.Length > 0)
                    publishid = Convert.ToInt32(txtPublishID.Text);
                try
                {
                    if (nextPublishActionPublishObjectID == PubActPubObjIDLPgPublishDBRecordToContentStg || nextPublishActionPublishObjectID == PubActPubObjIDLPgPublishDBRecordToProd)
                        proxy.SaveLandingPage(m_landingpage.LandingPageID, txtName.Text, txtURL.Text, txtControlName.Text, txtDescription.Text, chkActive.Checked, chkStatic.Checked, Convert.ToInt32(this.MemberID), true, publishid, landingpageController.CurrentServiceEnvironmentTypeEnum);
                    else
                        proxy.SaveLandingPage(m_landingpage.LandingPageID, txtName.Text, txtURL.Text, txtControlName.Text, txtDescription.Text, chkActive.Checked, chkStatic.Checked, Convert.ToInt32(this.MemberID), false, publishid, landingpageController.CurrentServiceEnvironmentTypeEnum);
                    this.Close();
                }
                catch(Exception ex)
                {
                    String msg = ex.Message;
                    lblError.Text = "An error occurred.";
                    lblError.Visible = false;
                }

            }
            catch { }
        }

        private void InitializeNextStep()
        {
            NextStepButton.Visible = false;

            if (m_landingpage.LandingPageID > 0)
            {
                ServiceEnvironmentTypeEnum serviceEnvrionmentTypeEnum;
                // Get information about the next Publish step.
                nextPublishActionPublishObjectID = CommonController.GetNextPublishActionPublishObjectStep(m_landingpage.LandingPageID, PublishObjectType.LandingPage, out serviceEnvrionmentTypeEnum);

                //If we're not in the right environment for the next step to occur, hide the next step button and bail.
                if (landingpageController.CurrentServiceEnvironmentTypeEnum != serviceEnvrionmentTypeEnum)
                {
                    NextStepButton.Visible = false;
                    SetLabels();
                    return;
                }

                // Display/Hide NextStepButton.
                if (m_landingpage.LandingPageID > 0 && nextPublishActionPublishObjectID != Matchnet.Constants.NULL_INT && (landingpageController.CurrentServiceEnvironmentTypeEnum == serviceEnvrionmentTypeEnum))
                {
                    SetLabels();
                    switch (nextPublishActionPublishObjectID)
                    {
                        case PubActPubObjIDLPgVerifyDev:
                            NextStepButton.Visible = true;
                            break;

                        case PubActPubObjIDLPgPublishFilesToContentStg:
                        case PubActPubObjIDLPgPublishDBRecordToContentStg:
                            if (landingpageController.CurrentServiceEnvironmentTypeEnum == ServiceEnvironmentTypeEnum.ContentStg)
                            {
                                cmdUpdate.Enabled = false;
                                NextStepButton.Visible = false;
                                return;
                            }
                            NextStepButton.Visible = true;
                            break;

                        case PubActPubObjIDLPgVerifyInContentStg:
                            if (landingpageController.CurrentServiceEnvironmentTypeEnum == ServiceEnvironmentTypeEnum.Dev)
                                cmdUpdate.Enabled = false;
                            if (HasPrivilege(ModuleWorkItem.PrivilegeIDLPApprover))
                                NextStepButton.Visible = true;

                            break;
                        case PubActPubObjIDLPgPublishFilesToProd:
                        case PubActPubObjIDLPgPublishDBRecordToProd:
                            if (HasPrivilege(ModuleWorkItem.PrivilegeIDLPPublisher))
                                NextStepButton.Visible = true;

                            break;

                        case PubActPubObjIDLPgVerifyInProd:
                            NextStepButton.Visible = true;
                            break;

                        default:
                            NextStepButton.Visible = false;
                            break;
                    }
                }
            }
        }
        #endregion

        #region properties
        public enum EditMode : int
        {
            Edit = 1,
            Add = 2
        }
        #endregion

    }
}