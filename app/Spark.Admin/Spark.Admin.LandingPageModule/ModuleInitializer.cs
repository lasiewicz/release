using System;
using System.Collections.Generic;
using System.Text;

using Spark.Admin.Common;
using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.Services;
using Microsoft.Practices.CompositeUI.WinForms;

using Matchnet.Member.ValueObjects.Privilege;

using Spark.Admin;

namespace Spark.Admin.LandingPageModule
{
    public class ModuleInitializer : ModuleInit
    {
        private IWorkItemTypeCatalogService workItemCatalogService;
        private WorkItem parentWorkItem;

        #region Public Properties
        public override void Load()
        {
            base.Load();

            workItemCatalogService.RegisterWorkItem<ModuleWorkItem>();

            ModuleWorkItem moduleWorkItem = parentWorkItem.WorkItems.AddNew<ModuleWorkItem>();
            // You must manually assign GlobalState to WorkItems.
            moduleWorkItem.GlobalState = parentWorkItem.State;

            // Check to see if the user has privileges to use this Module.
            if (moduleWorkItem.HasPrivilege(ModuleWorkItem.PrivilegeIDLPApprover)
                || moduleWorkItem.HasPrivilege(ModuleWorkItem.PrivilegeIDLPEditor)
                || moduleWorkItem.HasPrivilege(ModuleWorkItem.PrivilegeIDLPPublisher))
            {
                moduleWorkItem.Run(parentWorkItem.Workspaces[Constants.ShellWorkspaceName]);
            }
        }
        #endregion

        #region Properties
        [ServiceDependency]
        public IWorkItemTypeCatalogService WorkItemCatalogService
        {
            set
            {
                workItemCatalogService = value;
            }
        }

        [ServiceDependency]
        public WorkItem ParentWorkItem
        {
            set
            {
                parentWorkItem = value;
            }
        }
        #endregion
    }

   }

