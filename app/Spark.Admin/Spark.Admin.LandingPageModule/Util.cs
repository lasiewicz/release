using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Spark.Admin.LandingPageModule
{
    class Util
    {
        /// <summary>
        /// Creates the table for the resource history.
        /// </summary>
        /// <returns>The datatable with columns defined for resource histoy.</returns>
        static public DataTable CreateTable()
        {
            DataTable historyTable = new DataTable("File History");

            DataColumn VersionColumn = new DataColumn();
            VersionColumn.DataType = System.Type.GetType("System.Int32");
            VersionColumn.ColumnName = "Version";
            historyTable.Columns.Add(VersionColumn);

            DataColumn UserColumn = new DataColumn();
            UserColumn.DataType = System.Type.GetType("System.String");
            UserColumn.ColumnName = "User";
            historyTable.Columns.Add(UserColumn);

            DataColumn DateColumn = new DataColumn();
            DateColumn.DataType = System.Type.GetType("System.DateTime");
            DateColumn.ColumnName = "Date";
            historyTable.Columns.Add(DateColumn);

            DataColumn CommentColumn = new DataColumn();
            CommentColumn.DataType = System.Type.GetType("System.String");
            CommentColumn.ColumnName = "Comment";
            historyTable.Columns.Add(CommentColumn);

            return historyTable;
        }

        /// <summary>
        /// Creates the table for the deployment results.
        /// </summary>
        /// <returns>The datatable with columns defined for deployment results.</returns>
        static public DataTable CreateResultsTable()
        {
            DataTable resultsTable = new DataTable("Results");

            DataColumn Filename = new DataColumn();
            Filename.DataType = System.Type.GetType("System.String");
            Filename.ColumnName = "Filename";
            resultsTable.Columns.Add(Filename);

            DataColumn Server = new DataColumn();
            Server.DataType = System.Type.GetType("System.String");
            Server.ColumnName = "Server";
            resultsTable.Columns.Add(Server);

            DataColumn Results = new DataColumn();
            Results.DataType = System.Type.GetType("System.String");
            Results.ColumnName = "Results";
            resultsTable.Columns.Add(Results);

            DataColumn ErrorMessage = new DataColumn();
            ErrorMessage.DataType = System.Type.GetType("System.String");
            ErrorMessage.ColumnName = "Error Message";
            resultsTable.Columns.Add(ErrorMessage);

            return resultsTable;
        }
    }
}
