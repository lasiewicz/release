using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Security.Principal;

using Matchnet.Content.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.ResourcePush.ServiceAdapters;
using Matchnet.ResourcePush.ValueObjects;
using Matchnet.FileDeploy.ValueObjects;
using Matchnet.Configuration.ValueObjects.Servers;

using Matchnet.P4Wrapper.ValueObjects;

namespace Spark.Admin.LandingPageModule
{
    public partial class Main : Form
    {
        #region Publish Constants
        private const Int32 PubActPubObjIDLPgPublishFilesToContentStg = 404;
        private const Int32 PubActPubObjIDLPgPublishFilesToProd = 407;
        #endregion

        #region Global variables
        /// <summary>
        /// The AuthInfo to be used for all VSS interaction.  THis is based off of 
        /// the current windows user.  
        /// </summary>
        Matchnet.VSSWrapper.ValueObjects.AuthInfo authInfo;

        /// <summary>
        /// The datatable to be used for VSS resource history.  This is cleared and 
        /// added to frequently.
        /// </summary>
        private DataTable FileHistoryTable = new DataTable();
        
        /// <summary>
        /// The datatable to be used for the list of files to deploy.  This is used 
        /// for both images and resources.
        /// </summary>
        Matchnet.ResourcePush.ValueObjects.DeployFileCollection FilesToDeploy = new DeployFileCollection();

        /// <summary>
        /// The server type that the files should be deployed too.  The default is 
        /// staging as a safety measure.  
        /// </summary>
        ServerType serverType = ServerType.Staging;

        /// <summary>
        /// The image path that is replaced.  This is done because the path on 
        /// production is different, and does not need the fully qualified dev path.
        /// </summary>
        string IMAGE_PATH = "\\\\devweb01";
        string DEFAULT_IMAGE_PATH = "\\\\devweb01\\img";
        Int32 deploymentTarget = 0;
        #endregion

        #region Constructor
        public Main(Int32 theStage)
        {
            // Initialize the winforms
            deploymentTarget = theStage;
            InitializeComponent();
        }

        public Main()
        {
            // Initialize the winforms
            InitializeComponent();
        }
        #endregion

        #region Resource Selection Event Handlers
        /// <summary>
        /// When the site list changes, clear the existing resource list and resource history, and rebind the resource list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbSites_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.lbResources.Items.Clear();
            this.lbResources.ClearSelected();
            this.FileHistoryTable.Clear();
            this.dgResourceHistory.ClearSelection();
            this.dgResourceHistory.Visible = false;

            BindResourceList();
        }

        /// <summary>
        /// When the selected item in the resource list changes, refresh the resource history with that resource's VSS history.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbResources_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbResources.SelectedItem != null)
            {
                P4File p4File = (P4File)lbResources.SelectedItem;

                List<P4FileRevision> p4FileRevisions = ResourcePushSA.Instance.GetResourceHistory(p4File);

                FileHistoryTable = Util.CreateTable();

                foreach (P4FileRevision p4FileRevision in p4FileRevisions)
                {
                    DataRow row = FileHistoryTable.NewRow();

                    row["Version"] = p4FileRevision.Revision;
                    row["Comment"] = p4FileRevision.Description;

                    FileHistoryTable.Rows.Add(row);
                }

                FileHistoryTable.DefaultView.Sort = "version desc";
                this.dgResourceHistory.DataSource = FileHistoryTable;
                this.dgResourceHistory.Visible = true;
            }
        }

        /// <summary>
        /// Add a resource file and version to the list of resources to deploy  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgResourceHistory_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string ResourcePath = this.lbResources.SelectedItem.ToString();
            int Version = Matchnet.Conversion.CInt(this.dgResourceHistory.Rows[e.RowIndex].Cells[0].Value);

            Matchnet.ResourcePush.ValueObjects.DeployFile deployFile = new DeployFile(ResourcePath, Version.ToString(), FileType.Resource);
            bool AlreadyExists = false;
            foreach (Matchnet.ResourcePush.ValueObjects.DeployFile deployFileIterator in FilesToDeploy)
            {
                if (deployFile.FileName == deployFileIterator.FileName)
                {
                    AlreadyExists = true;
                }
            }

            if (!AlreadyExists)
            {
                FilesToDeploy.Add(deployFile);
                BindResourcesToDeploy();

                this.Tabs.SelectedTab = this.Deploy;
                if (deploymentTarget == PubActPubObjIDLPgPublishFilesToContentStg)
                {
                    btnDeployStage.Visible = false;
                    btnDeployProduction.Visible = false;
                }
                else if (deploymentTarget == PubActPubObjIDLPgPublishFilesToProd)
                {
                    btnDeployStage.Visible = false;
                    btnDeployContentStage.Visible = false;
                }

            }
            else
            {
                MessageBox.Show("Another version of this file is already selected for deployment.");
            }
        }

        #endregion
        
        #region Resource Selection Methods
        /// <summary>
        /// Binds the list of sites to the dropdown for sites.  Initially binds the resource list as well for first load
        /// </summary>
        private void BindSites()
        {
            Matchnet.Content.ValueObjects.BrandConfig.Sites sites = new Matchnet.Content.ValueObjects.BrandConfig.Sites();
            sites = BrandConfigSA.Instance.GetSites();
            
            this.cbSites.DataSource = sites;
            this.cbSites.DisplayMember = "Name";
            this.cbSites.ValueMember = "SiteID";

            BindResourceList();
        }

        /// <summary>
        /// Bind the resource list with the provided site id.  
        /// </summary>
        private void BindResourceList()
        {
            Matchnet.Content.ValueObjects.BrandConfig.Sites sites = BrandConfigSA.Instance.GetSites();
            foreach (Matchnet.Content.ValueObjects.BrandConfig.Site siteIterator in sites)
            {
                if (siteIterator.SiteID.ToString() == cbSites.SelectedValue.ToString())
                {
                    List<P4File> p4Files = ResourcePushSA.Instance.ResourceList(siteIterator.SiteID.ToString());

                    this.lbResources.Sorted = true;

                    foreach (P4File p4File in p4Files)
                    {
                        this.lbResources.Items.Add(p4File);
                    }
                    break;
                }
            }
        }

        /// <summary>
        /// Bind a resource to the listbox of resource to deploy.
        /// </summary>
        private void BindResourcesToDeploy()
        {
            lbResourcesToDeploy.Items.Clear();

            foreach (Matchnet.ResourcePush.ValueObjects.DeployFile deployFile in FilesToDeploy)
            {
                if (deployFile.FileType == FileType.Resource)
                {
                    lbResourcesToDeploy.Items.Add("Version " + deployFile.Version + " of " + deployFile.FileName);
                }
            }
        }
        #endregion

        #region Image Selection Event Handlers
        /// <summary>
        /// Adds the selected image to the list of files to be deployed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImageSelect_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog ofDlg = new FolderBrowserDialog();
            //ofDlg.Filter = ".|*.";
            ofDlg.SelectedPath = DEFAULT_IMAGE_PATH;

            if (DialogResult.OK == ofDlg.ShowDialog() && ofDlg.SelectedPath != null)
            {
                DirectoryInfo di = new DirectoryInfo(ofDlg.SelectedPath);
                //bool AlreadySelected = false;
                DeployFile imageFile = null;
                FilesToDeploy = new DeployFileCollection();

                foreach (FileInfo file in di.GetFiles())
                {
                    imageFile = new DeployFile(file.FullName.Replace(IMAGE_PATH, ""), "0", FileType.Image);
                    FilesToDeploy.Add(imageFile);
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    foreach (FileInfo subfile in dir.GetFiles())
                    {
                        imageFile = new DeployFile(subfile.FullName.Replace(IMAGE_PATH, ""), "0", FileType.Image);
                        FilesToDeploy.Add(imageFile);
                    }
                }

                BindImagesToDeploy();
                //No longer necessary as we clear the collection every time
                /*foreach(DeployFile imageToDeploy in FilesToDeploy)
                {
                    if (imageFile.FileName == imageToDeploy.FileName)
                    {
                        AlreadySelected = true;
                    }
                }

                if (!AlreadySelected)
                {
                    FilesToDeploy.Add(imageFile);
                    BindImagesToDeploy();
                }
                else
                {
                    MessageBox.Show("This file has already selected for deployment.");
                }*/
            }
        }
        #endregion

        #region Image Selection Methods
        /// <summary>
        /// Bind an Image to the listbox of images to deploy.
        /// </summary>
        private void BindImagesToDeploy()
        {
            lbImagesToDeploy.Items.Clear();

            foreach (Matchnet.ResourcePush.ValueObjects.DeployFile deployFile in FilesToDeploy)
            {
                if (deployFile.FileType == FileType.Image)
                {
                    //lbImagesToDeploy.Items.Add(IMAGE_PATH + deployFile.FileName);
                    lbImagesToDeploy.Items.Add(deployFile.FileName);
                }
            }
        }
        #endregion

        #region Deployment Methods
        /// <summary>
        /// Deploys the files to their destined servers.
        /// Results are then bound to the result grid and sorted accordingly.
        /// </summary>
        /// <param name="FilesToDeploy"></param>
        private void DeployFiles(DeployFileCollection FilesToDeploy)
        {
            DataTable ResultsTable = Util.CreateResultsTable();
            lblDeployTime.Text = "Start Time: " + DateTime.Now.ToString() + " " + DateTime.Now.Second.ToString() + "." + DateTime.Now.Millisecond.ToString();
            try
            {
                FileDeploymentResultCollection FileDeploymentResults = ResourcePushSA.Instance.DeployFiles(FilesToDeploy, ServerType.Web | serverType);
                MessageBox.Show("Files deployed.");
            }
            catch(Exception ex)
            {
                this.DialogResult = DialogResult.Abort;
                throw ex;
            }
            lblResultsTime.Text = "End Time: " + DateTime.Now.ToString() + " " + DateTime.Now.Second.ToString() + "." + DateTime.Now.Millisecond.ToString();
            
            /*foreach (FileDeploymentResult fileResult in FileDeploymentResults)
            {
                foreach (DeploymentResult deploymentResult in fileResult)
                {
                    DataRow row = ResultsTable.NewRow();

                    row["Filename"] = fileResult.FileName;
                    row["Server"] = deploymentResult.ServerName;
                    row["Results"] = deploymentResult.Status.ToString();
                    row["Error Message"] = deploymentResult.ErrorText;

                    ResultsTable.Rows.Add(row);
                }
            }

            ResultsTable.DefaultView.Sort = "filename desc, server desc";
            this.dgResults.DataSource = ResultsTable;
            this.dgResults.Visible = true;*/
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region Deployment Event Handlers

        /// <summary>
        /// When attempting to deploy to stage, we hide the Deployment buttons and active the confirmation panel.  
        /// ServerType must be set to staging.  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeployStage_Click(object sender, EventArgs e)
        {
            if (FilesToDeploy.Count > 0)
            {
                btnDeployStage.Enabled = false;
                btnDeployProduction.Enabled = false;
                btnDeployContentStage.Enabled = false;
                btnImageSelect.Enabled = false;

                pnlConfirmation.Visible = true;
                serverType = ServerType.Staging;
            }
            else
            {
                MessageBox.Show("No files to deploy, please add files.");
            }
        }

        /// <summary>
        /// When attempting to deploy to production, we hide the Deployment buttons and active the confirmation panel.  
        /// ServerType must be set to production.  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeployProduction_Click(object sender, EventArgs e)
        {
            if (FilesToDeploy.Count > 0)
            {
                btnDeployProduction.Enabled = false;
                btnDeployStage.Enabled = false;
                btnDeployContentStage.Enabled = false;
                btnImageSelect.Enabled = false;

                pnlConfirmation.Visible = true;
                serverType = ServerType.Production;
            }
            else
            {
                MessageBox.Show("No files to deploy, please add files.");
            }
        }

        /// <summary>
        /// Deploys selected items to the servers specified.  
        /// Removes the list of tabs and creates new tab Results.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnYes_Click(object sender, EventArgs e)
        {
            this.Tabs.Controls.Remove(this.Resources);
            this.Tabs.Controls.Remove(this.Deploy);

            this.Tabs.Controls.Add(this.Results);
            this.Tabs.Controls.Add(this.DeploymentStatus);
            this.Tabs.SelectedTab = Results;

            try
            {
                DeployFiles(FilesToDeploy);
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                String msg = ex.Message;
            }
        }

        /// <summary>
        /// If the user clicks No on the confirmation panel, 
        /// we reeenable the deployment buttons and hide the confirmation panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNo_Click(object sender, EventArgs e)
        {
            btnDeployProduction.Enabled = true;
            btnDeployStage.Enabled = true;
            btnDeployContentStage.Enabled = true;
            btnImageSelect.Enabled = true;

            pnlConfirmation.Visible = false;
        }

        /// <summary>
        /// Removes a resource file from teh list of files to be deployed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbResourcesToDeploy_DoubleClick(object sender, System.EventArgs e)
        {
            foreach (DeployFile deployFile in FilesToDeploy)
            {
                if (lbResourcesToDeploy.SelectedItem.ToString().Contains(deployFile.FileName))
                {
                    FilesToDeploy.Remove(deployFile);
                    lbResourcesToDeploy.Items.Remove(lbResourcesToDeploy.SelectedItem.ToString());
                    lbResourcesToDeploy.SelectedItem = "";
                    MessageBox.Show("File removed.");
                    break;
                }
            }
        }

        /// <summary>
        /// Removes an image file from teh list of files to be deployed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lbImagesToDeploy_DoubleClick(object sender, EventArgs e)
        {
            foreach (DeployFile deployFile in FilesToDeploy)
            {
                if (lbImagesToDeploy.SelectedItem.ToString().Contains(deployFile.FileName))
                {
                    FilesToDeploy.Remove(deployFile);
                    lbImagesToDeploy.Items.Remove(lbImagesToDeploy.SelectedItem.ToString());
                    lbImagesToDeploy.SelectedItem = "";
                    MessageBox.Show("File removed.");
                    break;
                }
            }
        }

        #endregion      

        private void btnRestart_Click(object sender, EventArgs e)
        {
            btnDeployProduction.Enabled = true;
            btnDeployStage.Enabled = true;
            btnDeployContentStage.Enabled = true;
            btnImageSelect.Enabled = true;
            
            pnlConfirmation.Visible = false;

            this.Tabs.Controls.Add(this.Resources);
            this.Tabs.Controls.Add(this.Deploy);
            this.Tabs.SelectedTab = Resources;

            this.Tabs.Controls.Remove(this.DeploymentStatus);
            this.Tabs.Controls.Remove(this.Results);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (FilesToDeploy.Count > 0)
            {
                btnDeployProduction.Enabled = false;
                btnDeployStage.Enabled = false;
                btnDeployContentStage.Enabled = false;
                btnImageSelect.Enabled = false;

                pnlConfirmation.Visible = true;
                serverType = ServerType.ContentStaging;
            }
            else
            {
                MessageBox.Show("No files to deploy, please add files.");
            }
        }

        private void btnSavePassword_Click(object sender, EventArgs e)
        {
            // Get the current window user and set the authInfo username to the current username
            WindowsIdentity windowsIdentity = WindowsIdentity.GetCurrent();
            authInfo = new Matchnet.VSSWrapper.ValueObjects.AuthInfo(windowsIdentity.Name.Substring(windowsIdentity.Name.LastIndexOf("\\") + 1), txtPassword.Text);
            //authInfo = new Matchnet.VSSWrapper.ValueObjects.AuthInfo("wlybrand", txtPassword.Text);

            //this.Tabs.Controls.Add(Resources);
            this.Tabs.Controls.Add(Deploy);

            this.Tabs.Controls.Remove(tbPassword);
            if (deploymentTarget == PubActPubObjIDLPgPublishFilesToContentStg)
            {
                btnDeployStage.Visible = false;
                btnDeployProduction.Visible = false;
            }
            else if (deploymentTarget == PubActPubObjIDLPgPublishFilesToProd)
            {
                btnDeployStage.Visible = false;
                btnDeployContentStage.Visible = false;
            }

            // Bind the sites for the initial load
            //BindSites();

            // Hide the resource history out of user convenience
            this.dgResourceHistory.Visible = false;
        }
    }
}