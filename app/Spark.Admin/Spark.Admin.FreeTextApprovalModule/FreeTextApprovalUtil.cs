﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml;

namespace Spark.Admin.FreeTextApprovalModule
{
    /// <summary>
    /// make sure to include this in the project pre-build event
    /// 
    /// copy "$(ProjectDir)FreeTextApproval.xml"  "$(TargetDir)FreeTextApproval.xml"
    /// </summary>
    internal static class FreeTextApprovalUtil
    {
        #region Static Variables

        /// <summary>
        /// ensures prevention of thread dead lock.
        /// </summary>
        static readonly object locker = new object();
        /// <summary>
        /// to prevent reading ahead of the list while it's still loading.
        /// </summary>
        static bool locked = false;
        static Dictionary<string, Regex> list = new Dictionary<string, Regex>();

        #endregion

        #region Private Constructor

        static FreeTextApprovalUtil()
        {
            loadRegularExpressions();

#if DEBUG
            System.Diagnostics.Debug.WriteLine("Loaded regular expressions");
#endif
        }

        #endregion

        #region Private Methods

        private static void loadRegularExpressions()
        {
            StringReader stringReader = new StringReader(global::Spark.Admin.FreeTextApprovalModule.Properties.Resources.FreeTextApproval);
            XmlDocument doc = new XmlDocument();
            XmlTextReader reader = new XmlTextReader(stringReader);
            reader.WhitespaceHandling = WhitespaceHandling.None;
            doc.Load(reader);

            XmlElement root = doc.DocumentElement;

            foreach (XmlNode node in root.ChildNodes)
            {
                string sectionName = node.Name;
                XmlNode parentNode;

                try
                {
                    XmlNodeList nodeList = root.GetElementsByTagName(sectionName);
                    parentNode = nodeList[0];
                }
                catch (Exception ex)
                {
                    throw new Exception("Section not found", ex);
                }

                string expression = "";

                foreach (XmlNode wordNode in parentNode.ChildNodes)
                {
                    string nodeValue = wordNode.Attributes["value"].Value;
                    expression += "(" + nodeValue + ")|";
                }

                Regex pattern;

                pattern = new Regex(expression.Substring(0, expression.Length - 1), RegexOptions.IgnoreCase);


                lock (locker)
                {
                    locked = true;

                    if (list.ContainsKey(sectionName))
                    {
                        list.Remove(sectionName);
                    }

                    list.Add(sectionName, pattern);

                    locked = false;
                }
            }

            reader.Close();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns Regex pattern
        /// </summary>
        /// <param name="sectionName"></param>
        /// <returns></returns>
        public static Regex GetRegex(string sectionName)
        {
            while (locked)
            {
            }

            Regex pattern = list[sectionName];

            return pattern;
        }

        /// <summary>
        /// Test to see if given text contains any patterns that match
        /// </summary>
        /// <param name="sectionName">"bannedWords", "suspectWords"</param>
        /// <param name="text"></param>
        /// <returns>True if there are any matching patterns</returns>
        public static bool Contains(string sectionName, string text)
        {
            while (locked)
            {
            }

            Regex pattern = list[sectionName];

            MatchCollection matchList = pattern.Matches(text);

            if (matchList.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion
    }
}
