﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Spark.Admin.FreeTextApprovalModule
{
    public partial class PleaseWait : Form
    {

        public PleaseWait()
        {
            InitializeComponent();
        }

        private void PleaseWait_Load(object sender, EventArgs e)
        { 
        }

        protected override void OnShown(EventArgs e)
        {

            Application.DoEvents();

            this.Refresh();

            base.OnShown(e);
        }
    }
}