﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;

namespace Spark.Admin.FreeTextApprovalModule
{
    /// <summary>
    /// rich text box capable of highlighting reg ex matches for free text approval.
    /// </summary>
    public class FreeTextApprovalRichTextBox : System.Windows.Forms.RichTextBox
    {
       
        private bool paint = true;

        /// <summary>
        /// WndProc
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            if (m.Msg == 0x00f)
            {
                if (paint)
                    base.WndProc(ref m);
                else
                    m.Result = IntPtr.Zero;
            }
            else
                base.WndProc(ref m);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="suspectWords"></param>
        /// <param name="bannedWords"></param>
        public void Highlight(bool suspectWords, bool bannedWords)
        {
            paint = false;


            Regex suspectWordsPattern = FreeTextApprovalUtil.GetRegex("suspectWords");
            Regex bannedWordsPattern = FreeTextApprovalUtil.GetRegex("bannedWords");

            MatchCollection suspectWordsMatches = suspectWordsPattern.Matches(Text);

            foreach (Match match in suspectWordsMatches)
            {
                SelectionStart = match.Index;
                SelectionLength = match.Length;
                SelectionBackColor = Color.Yellow;

                SelectionStart = match.Index + match.Length;
                SelectionLength = 0;
                SelectionBackColor = Color.White;
            }

            MatchCollection bannedWordsMatches = bannedWordsPattern.Matches(Text);

            foreach (Match match in bannedWordsMatches)
            {
                SelectionStart = match.Index;
                SelectionLength = match.Length;
                SelectionBackColor = Color.Red;

                SelectionStart = TextLength;
                SelectionLength = 0;
                SelectionBackColor = Color.White;
            }

            paint = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="suspectedWords"></param>
        /// <param name="bannedWords"></param>
        public void Cleanse(bool suspectedWords, bool bannedWords)
        {
            paint = false;

            Regex suspectWordsPattern = FreeTextApprovalUtil.GetRegex("suspectWords");
            Regex bannedWordsPattern = FreeTextApprovalUtil.GetRegex("bannedWords");

            Text = suspectWordsPattern.Replace(Text, String.Empty);
            Text = bannedWordsPattern.Replace(Text, String.Empty);

            paint = true;
        }
    }
}
