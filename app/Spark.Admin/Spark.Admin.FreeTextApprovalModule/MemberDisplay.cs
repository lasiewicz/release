﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Spark.Admin.FreeTextApprovalModule
{
    public partial class MemberDisplay : UserControl
    {

        public MemberDisplay()
        {
            InitializeComponent();

#if DEBUG

            AdminEmailAddress.Visible = true;
#endif
        }

        public void PopulateDisplay(String memberID, String viewProfileURL, String community, String age, String gender, String lastUpdated, String adminEmailAddress, String queueDate)
        {
            Clear();

            MemberID.Text = memberID;
            MemberID.Links.Add(0, MemberID.Text.Length, viewProfileURL);
            Community.Text = community;
            Age.Text = age + "years old";
            Gender.Text = gender;
            LastUpdated.Text = "Updated " + lastUpdated;
            AdminEmailAddress.Text = adminEmailAddress;
            QueueDate.Text = "Queued " + queueDate;
        }

        private void MemberID_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(e.Link.LinkData.ToString());
        }

        private void Clear()
        {
            MemberID.Text = "";
            MemberID.Links.Clear();
            Community.Text = "";
            Age.Text = "";
            Gender.Text = "";
            AdminEmailAddress.Text = "";
            LastUpdated.Text = "";
            QueueDate.Text = "";
        }
    }
}
