﻿namespace Spark.Admin.FreeTextApprovalModule
{
    partial class MemberDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MemberID = new System.Windows.Forms.LinkLabel();
            this.Gender = new System.Windows.Forms.Label();
            this.Age = new System.Windows.Forms.Label();
            this.Community = new System.Windows.Forms.Label();
            this.AdminEmailAddress = new System.Windows.Forms.Label();
            this.LastUpdated = new System.Windows.Forms.Label();
            this.QueueDate = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // MemberID
            // 
            this.MemberID.AutoSize = true;
            this.MemberID.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MemberID.Location = new System.Drawing.Point(3, 9);
            this.MemberID.Name = "MemberID";
            this.MemberID.Size = new System.Drawing.Size(85, 18);
            this.MemberID.TabIndex = 1;
            this.MemberID.TabStop = true;
            this.MemberID.Text = "MemberID";
            this.MemberID.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.MemberID_LinkClicked);
            // 
            // Gender
            // 
            this.Gender.AutoSize = true;
            this.Gender.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Gender.ForeColor = System.Drawing.Color.DimGray;
            this.Gender.Location = new System.Drawing.Point(12, 67);
            this.Gender.Name = "Gender";
            this.Gender.Size = new System.Drawing.Size(53, 14);
            this.Gender.TabIndex = 7;
            this.Gender.Text = "Gender";
            // 
            // Age
            // 
            this.Age.AutoSize = true;
            this.Age.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Age.ForeColor = System.Drawing.Color.DimGray;
            this.Age.Location = new System.Drawing.Point(12, 53);
            this.Age.Name = "Age";
            this.Age.Size = new System.Drawing.Size(31, 14);
            this.Age.TabIndex = 6;
            this.Age.Text = "Age";
            // 
            // Community
            // 
            this.Community.AutoSize = true;
            this.Community.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Community.ForeColor = System.Drawing.Color.DimGray;
            this.Community.Location = new System.Drawing.Point(12, 39);
            this.Community.Name = "Community";
            this.Community.Size = new System.Drawing.Size(77, 14);
            this.Community.TabIndex = 5;
            this.Community.Text = "Community";
            // 
            // AdminEmailAddress
            // 
            this.AdminEmailAddress.AutoSize = true;
            this.AdminEmailAddress.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdminEmailAddress.ForeColor = System.Drawing.Color.DarkGreen;
            this.AdminEmailAddress.Location = new System.Drawing.Point(3, 116);
            this.AdminEmailAddress.Name = "AdminEmailAddress";
            this.AdminEmailAddress.Size = new System.Drawing.Size(45, 14);
            this.AdminEmailAddress.TabIndex = 9;
            this.AdminEmailAddress.Text = "Admin";
            this.AdminEmailAddress.Visible = false;
            // 
            // LastUpdated
            // 
            this.LastUpdated.AutoSize = true;
            this.LastUpdated.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LastUpdated.ForeColor = System.Drawing.Color.DimGray;
            this.LastUpdated.Location = new System.Drawing.Point(12, 81);
            this.LastUpdated.Name = "LastUpdated";
            this.LastUpdated.Size = new System.Drawing.Size(92, 14);
            this.LastUpdated.TabIndex = 10;
            this.LastUpdated.Text = "Last Updated";
            // 
            // QueueDate
            // 
            this.QueueDate.AutoSize = true;
            this.QueueDate.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QueueDate.ForeColor = System.Drawing.Color.DimGray;
            this.QueueDate.Location = new System.Drawing.Point(12, 95);
            this.QueueDate.Name = "QueueDate";
            this.QueueDate.Size = new System.Drawing.Size(83, 14);
            this.QueueDate.TabIndex = 11;
            this.QueueDate.Text = "Queue Date";
            // 
            // MemberDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.QueueDate);
            this.Controls.Add(this.LastUpdated);
            this.Controls.Add(this.AdminEmailAddress);
            this.Controls.Add(this.Gender);
            this.Controls.Add(this.Age);
            this.Controls.Add(this.Community);
            this.Controls.Add(this.MemberID);
            this.Name = "MemberDisplay";
            this.Size = new System.Drawing.Size(164, 155);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel MemberID;
        private System.Windows.Forms.Label Gender;
        private System.Windows.Forms.Label Age;
        private System.Windows.Forms.Label Community;
        private System.Windows.Forms.Label AdminEmailAddress;
        private System.Windows.Forms.Label LastUpdated;
        private System.Windows.Forms.Label QueueDate;
    }
}
