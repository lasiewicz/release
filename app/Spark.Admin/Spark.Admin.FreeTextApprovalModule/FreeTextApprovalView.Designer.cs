﻿namespace Spark.Admin.FreeTextApprovalModule
{
    partial class FreeTextApprovalView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SplitContainer = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.MemberDisplay = new Spark.Admin.FreeTextApprovalModule.MemberDisplay();
            this.ApproveButton = new System.Windows.Forms.Button();
            this.SuspendButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.Selector = new System.Windows.Forms.ComboBox();
            this.ServiceEnvironmentComboBox = new System.Windows.Forms.ComboBox();
            this.HistoryDataGridView = new System.Windows.Forms.DataGridView();
            this.ActionColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.MemberViewProfileColumn = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Order = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.MemberLookupSiteComboBox = new System.Windows.Forms.ComboBox();
            this.MemberLookupMemberIDTextBox = new System.Windows.Forms.TextBox();
            this.MemberLookupButton = new System.Windows.Forms.Button();
            this.RowsLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.SplitContainer.Panel1.SuspendLayout();
            this.SplitContainer.Panel2.SuspendLayout();
            this.SplitContainer.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryDataGridView)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // SplitContainer
            // 
            this.SplitContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer.Location = new System.Drawing.Point(0, 0);
            this.SplitContainer.Name = "SplitContainer";
            // 
            // SplitContainer.Panel1
            // 
            this.SplitContainer.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.SplitContainer.Panel1.Controls.Add(this.flowLayoutPanel1);
            // 
            // SplitContainer.Panel2
            // 
            this.SplitContainer.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.SplitContainer.Panel2.Controls.Add(this.RowsLayoutPanel);
            this.SplitContainer.Size = new System.Drawing.Size(800, 600);
            this.SplitContainer.SplitterDistance = 175;
            this.SplitContainer.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.MemberDisplay);
            this.flowLayoutPanel1.Controls.Add(this.ApproveButton);
            this.flowLayoutPanel1.Controls.Add(this.SuspendButton);
            this.flowLayoutPanel1.Controls.Add(this.panel1);
            this.flowLayoutPanel1.Controls.Add(this.HistoryDataGridView);
            this.flowLayoutPanel1.Controls.Add(this.panel2);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(173, 591);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // MemberDisplay
            // 
            this.MemberDisplay.Enabled = false;
            this.MemberDisplay.Location = new System.Drawing.Point(3, 3);
            this.MemberDisplay.Name = "MemberDisplay";
            this.MemberDisplay.Size = new System.Drawing.Size(156, 135);
            this.MemberDisplay.TabIndex = 11;
            // 
            // ApproveButton
            // 
            this.ApproveButton.BackColor = System.Drawing.SystemColors.Control;
            this.ApproveButton.Enabled = false;
            this.ApproveButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ApproveButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ApproveButton.Location = new System.Drawing.Point(3, 144);
            this.ApproveButton.Name = "ApproveButton";
            this.ApproveButton.Size = new System.Drawing.Size(156, 40);
            this.ApproveButton.TabIndex = 9;
            this.ApproveButton.Text = "Approve";
            this.ApproveButton.UseVisualStyleBackColor = false;
            this.ApproveButton.Click += new System.EventHandler(this.ApproveButton_Click);
            // 
            // SuspendButton
            // 
            this.SuspendButton.BackColor = System.Drawing.SystemColors.Control;
            this.SuspendButton.Enabled = false;
            this.SuspendButton.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SuspendButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SuspendButton.Location = new System.Drawing.Point(3, 190);
            this.SuspendButton.Name = "SuspendButton";
            this.SuspendButton.Size = new System.Drawing.Size(156, 40);
            this.SuspendButton.TabIndex = 10;
            this.SuspendButton.Text = "Suspend";
            this.SuspendButton.UseVisualStyleBackColor = false;
            this.SuspendButton.Click += new System.EventHandler(this.SuspendButton_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.Selector);
            this.panel1.Controls.Add(this.ServiceEnvironmentComboBox);
            this.panel1.Location = new System.Drawing.Point(3, 236);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(156, 96);
            this.panel1.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(4, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 14);
            this.label1.TabIndex = 14;
            this.label1.Text = "Language:";
            // 
            // Selector
            // 
            this.Selector.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Selector.FormattingEnabled = true;
            this.Selector.Items.AddRange(new object[] {
            "English",
            "Hebrew"});
            this.Selector.Location = new System.Drawing.Point(3, 37);
            this.Selector.Name = "Selector";
            this.Selector.Size = new System.Drawing.Size(150, 22);
            this.Selector.TabIndex = 13;
            this.Selector.SelectedIndexChanged += new System.EventHandler(this.Selector_SelectedIndexChanged);
            // 
            // ServiceEnvironmentComboBox
            // 
            this.ServiceEnvironmentComboBox.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ServiceEnvironmentComboBox.FormattingEnabled = true;
            this.ServiceEnvironmentComboBox.Location = new System.Drawing.Point(3, 65);
            this.ServiceEnvironmentComboBox.Name = "ServiceEnvironmentComboBox";
            this.ServiceEnvironmentComboBox.Size = new System.Drawing.Size(151, 22);
            this.ServiceEnvironmentComboBox.TabIndex = 14;
            this.ServiceEnvironmentComboBox.SelectedIndexChanged += new System.EventHandler(this.ServiceEnvironmentComboBox_SelectedIndexChanged);
            // 
            // HistoryDataGridView
            // 
            this.HistoryDataGridView.AllowUserToAddRows = false;
            this.HistoryDataGridView.AllowUserToDeleteRows = false;
            this.HistoryDataGridView.AllowUserToResizeColumns = false;
            this.HistoryDataGridView.AllowUserToResizeRows = false;
            this.HistoryDataGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.HistoryDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HistoryDataGridView.ColumnHeadersVisible = false;
            this.HistoryDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ActionColumn,
            this.MemberViewProfileColumn,
            this.Order});
            this.HistoryDataGridView.Location = new System.Drawing.Point(3, 338);
            this.HistoryDataGridView.MultiSelect = false;
            this.HistoryDataGridView.Name = "HistoryDataGridView";
            this.HistoryDataGridView.ReadOnly = true;
            this.HistoryDataGridView.RowHeadersVisible = false;
            this.HistoryDataGridView.RowTemplate.ReadOnly = true;
            this.HistoryDataGridView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.HistoryDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.HistoryDataGridView.Size = new System.Drawing.Size(156, 113);
            this.HistoryDataGridView.TabIndex = 16;
            this.HistoryDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HistoryDataGridView_CellContentClick);
            // 
            // ActionColumn
            // 
            this.ActionColumn.HeaderText = "Action";
            this.ActionColumn.Name = "ActionColumn";
            this.ActionColumn.ReadOnly = true;
            this.ActionColumn.Width = 50;
            // 
            // MemberViewProfileColumn
            // 
            this.MemberViewProfileColumn.HeaderText = "Member";
            this.MemberViewProfileColumn.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.MemberViewProfileColumn.Name = "MemberViewProfileColumn";
            this.MemberViewProfileColumn.ReadOnly = true;
            this.MemberViewProfileColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.MemberViewProfileColumn.Text = "";
            this.MemberViewProfileColumn.ToolTipText = "View Member Profile";
            this.MemberViewProfileColumn.UseColumnTextForLinkValue = true;
            this.MemberViewProfileColumn.Width = 105;
            // 
            // Order
            // 
            this.Order.HeaderText = "Order";
            this.Order.Name = "Order";
            this.Order.ReadOnly = true;
            this.Order.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.MemberLookupSiteComboBox);
            this.panel2.Controls.Add(this.MemberLookupMemberIDTextBox);
            this.panel2.Controls.Add(this.MemberLookupButton);
            this.panel2.Location = new System.Drawing.Point(3, 457);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(156, 100);
            this.panel2.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Site:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "ID:";
            // 
            // MemberLookupSiteComboBox
            // 
            this.MemberLookupSiteComboBox.FormattingEnabled = true;
            this.MemberLookupSiteComboBox.Items.AddRange(new object[] {
            "Americansingles.com",
            "Jdate.com",
            "Jdate.co.il",
            "Cupid.co.il",
            "Date.co.uk",
            "Date.ca",
            "Collegeluv.com"});
            this.MemberLookupSiteComboBox.Location = new System.Drawing.Point(35, 31);
            this.MemberLookupSiteComboBox.Name = "MemberLookupSiteComboBox";
            this.MemberLookupSiteComboBox.Size = new System.Drawing.Size(116, 21);
            this.MemberLookupSiteComboBox.TabIndex = 19;
            // 
            // MemberLookupMemberIDTextBox
            // 
            this.MemberLookupMemberIDTextBox.Location = new System.Drawing.Point(35, 4);
            this.MemberLookupMemberIDTextBox.Name = "MemberLookupMemberIDTextBox";
            this.MemberLookupMemberIDTextBox.Size = new System.Drawing.Size(116, 20);
            this.MemberLookupMemberIDTextBox.TabIndex = 18;
            // 
            // MemberLookupButton
            // 
            this.MemberLookupButton.BackColor = System.Drawing.SystemColors.Control;
            this.MemberLookupButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MemberLookupButton.Location = new System.Drawing.Point(6, 58);
            this.MemberLookupButton.Name = "MemberLookupButton";
            this.MemberLookupButton.Size = new System.Drawing.Size(145, 27);
            this.MemberLookupButton.TabIndex = 17;
            this.MemberLookupButton.Text = "Member Lookup";
            this.MemberLookupButton.UseVisualStyleBackColor = false;
            this.MemberLookupButton.Click += new System.EventHandler(this.MemberLookupButton_Click);
            // 
            // RowsLayoutPanel
            // 
            this.RowsLayoutPanel.AutoScroll = true;
            this.RowsLayoutPanel.AutoSize = true;
            this.RowsLayoutPanel.BackColor = System.Drawing.Color.White;
            this.RowsLayoutPanel.ColumnCount = 1;
            this.RowsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.RowsLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RowsLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.RowsLayoutPanel.Name = "RowsLayoutPanel";
            this.RowsLayoutPanel.RowCount = 1;
            this.RowsLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.RowsLayoutPanel.Size = new System.Drawing.Size(617, 596);
            this.RowsLayoutPanel.TabIndex = 0;
            // 
            // FreeTextApprovalView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.SplitContainer);
            this.Name = "FreeTextApprovalView";
            this.Size = new System.Drawing.Size(800, 600);
            this.Load += new System.EventHandler(this.FreeTextApprovalView_Load);
            this.SplitContainer.Panel1.ResumeLayout(false);
            this.SplitContainer.Panel1.PerformLayout();
            this.SplitContainer.Panel2.ResumeLayout(false);
            this.SplitContainer.Panel2.PerformLayout();
            this.SplitContainer.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HistoryDataGridView)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer SplitContainer;
        private System.Windows.Forms.TableLayoutPanel RowsLayoutPanel;
        private MemberDisplay MemberDisplay;
        private System.Windows.Forms.Button ApproveButton;
        private System.Windows.Forms.Button SuspendButton;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.ComboBox Selector;
        private System.Windows.Forms.ComboBox ServiceEnvironmentComboBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView HistoryDataGridView;
        private System.Windows.Forms.DataGridViewImageColumn ActionColumn;
        private System.Windows.Forms.DataGridViewLinkColumn MemberViewProfileColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Order;
        private System.Windows.Forms.Button MemberLookupButton;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox MemberLookupSiteComboBox;
        private System.Windows.Forms.TextBox MemberLookupMemberIDTextBox;
    }
}
