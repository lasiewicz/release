﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.CompositeUI;
using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.CompositeUI.WinForms;

using Spark.Admin.Common;

namespace Spark.Admin.FreeTextApprovalModule
{
    public class ModuleWorkItem : BaseModuleWorkItem
    {
        public const Int32 ApprovalApplication = 25;
        public const Int32 DevApprovalApplication = 26;

        private IWorkspace workspace;

        #region Public Methods
        public void Run(IWorkspace workspace)
        {
            this.workspace = workspace;

            FreeTextApprovalView freeTextApprovalView = this.Items.AddNew<FreeTextApprovalView>();
            // You must manually assign GlobalState to the ModuleView.
            freeTextApprovalView.GlobalState = GlobalState;

            this.Items.Add(freeTextApprovalView);

            // Set the Tab properties.
            TabSmartPartInfo tabSmartPartInfo = new TabSmartPartInfo();
            tabSmartPartInfo.Title = freeTextApprovalView.Title;

            workspace.Show(freeTextApprovalView, tabSmartPartInfo);
        }

        #endregion
    }
}
