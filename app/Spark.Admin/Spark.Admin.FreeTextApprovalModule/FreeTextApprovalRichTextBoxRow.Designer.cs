﻿namespace Spark.Admin.FreeTextApprovalModule
{
    partial class FreeTextApprovalRichTextBoxRow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DeleteCheckBox = new System.Windows.Forms.CheckBox();
            this.AttributeName = new System.Windows.Forms.Label();
            this.StatusIcon = new System.Windows.Forms.PictureBox();
            this.FreeTextApprovalRichTextBox = new Spark.Admin.FreeTextApprovalModule.FreeTextApprovalRichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // DeleteCheckBox
            // 
            this.DeleteCheckBox.AutoSize = true;
            this.DeleteCheckBox.BackColor = System.Drawing.Color.White;
            this.DeleteCheckBox.Location = new System.Drawing.Point(579, 3);
            this.DeleteCheckBox.Name = "DeleteCheckBox";
            this.DeleteCheckBox.Size = new System.Drawing.Size(15, 14);
            this.DeleteCheckBox.TabIndex = 1;
            this.DeleteCheckBox.UseVisualStyleBackColor = false;
            // 
            // AttributeName
            // 
            this.AttributeName.AutoSize = true;
            this.AttributeName.BackColor = System.Drawing.Color.White;
            this.AttributeName.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AttributeName.Location = new System.Drawing.Point(37, 3);
            this.AttributeName.Name = "AttributeName";
            this.AttributeName.Size = new System.Drawing.Size(41, 13);
            this.AttributeName.TabIndex = 3;
            this.AttributeName.Text = "label1";
            // 
            // StatusIcon
            // 
            this.StatusIcon.BackColor = System.Drawing.Color.White;
            this.StatusIcon.Location = new System.Drawing.Point(0, 3);
            this.StatusIcon.Name = "StatusIcon";
            this.StatusIcon.Size = new System.Drawing.Size(31, 22);
            this.StatusIcon.TabIndex = 2;
            this.StatusIcon.TabStop = false;
            // 
            // FreeTextApprovalRichTextBox
            // 
            this.FreeTextApprovalRichTextBox.BackColor = System.Drawing.Color.White;
            this.FreeTextApprovalRichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FreeTextApprovalRichTextBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FreeTextApprovalRichTextBox.Location = new System.Drawing.Point(170, 3);
            this.FreeTextApprovalRichTextBox.MaxLength = 4000;
            this.FreeTextApprovalRichTextBox.Name = "FreeTextApprovalRichTextBox";
            this.FreeTextApprovalRichTextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.FreeTextApprovalRichTextBox.Size = new System.Drawing.Size(403, 31);
            this.FreeTextApprovalRichTextBox.TabIndex = 0;
            this.FreeTextApprovalRichTextBox.Text = "";
            this.FreeTextApprovalRichTextBox.TextChanged += new System.EventHandler(this.FreeTextApprovalRichTextBox_TextChanged);
            // 
            // FreeTextApprovalRichTextBoxRow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.DeleteCheckBox);
            this.Controls.Add(this.StatusIcon);
            this.Controls.Add(this.FreeTextApprovalRichTextBox);
            this.Controls.Add(this.AttributeName);
            this.Name = "FreeTextApprovalRichTextBoxRow";
            this.Size = new System.Drawing.Size(597, 59);
            ((System.ComponentModel.ISupportInitialize)(this.StatusIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.CheckBox DeleteCheckBox;
        private System.Windows.Forms.PictureBox StatusIcon;
        public System.Windows.Forms.Label AttributeName;
        public FreeTextApprovalRichTextBox FreeTextApprovalRichTextBox;

    }
}
