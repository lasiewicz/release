﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeUI.SmartParts;
using Microsoft.Practices.CompositeUI;

using Spark.Admin.Common;

using Matchnet;
using Matchnet.ApproveQueue.ValueObjects;
using Matchnet.Member.ValueObjects;
using AMVO = Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.BrandConfig;

namespace Spark.Admin.FreeTextApprovalModule
{
    public partial class FreeTextApprovalView : BaseModuleView
    {
        private const String TitleFreeTextApproval = "Free Text Approval";

        #region Private Variables

        private FreeTextApprovalController controller;
        private ServiceEnvironmentTypeEnum currentServiceEnvironmentTypeEnum;
        private PleaseWait pleaseWait;

        private Int32 totalAction = 0;

        private CachedMember currentMember;
        private Brand currentBrand;
        private String currentQueueItemKey;
        private Int32 currentSelectedLanguageID;
        private String currentViewProfileLink;

        #endregion

        #region Contructors

        public FreeTextApprovalView()
        {
            InitializeComponent();

            // Set the Tab title.
            this.Title = TitleFreeTextApproval;
        }

        #endregion

        #region Private Methods

        private void PopulateRows()
        {
            PopulateRows(Matchnet.Constants.NULL_INT, Matchnet.Constants.NULL_INT);
        }

        /// <summary>
        /// When null params are passed in: Retrives the next item from the approval queue and populates the controls with data.
        /// Else: looks up a specific member
        /// </summary>
        private void PopulateRows(Int32 memberLookupMemberID, Int32 memberLookupBrandID)
        {
            Int32 memberID = Matchnet.Constants.NULL_INT;
            Int32 languageID = Matchnet.Constants.NULL_INT;
            Int32 communityID = Matchnet.Constants.NULL_INT;

            QueueItemText queueItemText = null;
            Brand brand = null;

            if (memberLookupMemberID == Matchnet.Constants.NULL_INT && memberLookupBrandID == Matchnet.Constants.NULL_INT)
            {
                QueueItemBase[] queueItemBase = controller.DequeueText(1, currentSelectedLanguageID, Matchnet.Constants.NULL_INT, currentServiceEnvironmentTypeEnum);

                if (queueItemBase.Length == 0)
                {
                    MessageBox.Show("No more free text approval queues awaiting.", "FreeText Approval", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                queueItemText = (QueueItemText)queueItemBase[0];
                currentQueueItemKey = queueItemText.GetCacheKey();
                memberID = queueItemText.MemberID;
                languageID = queueItemText.LanguageID;
                communityID = queueItemText.CommunityID;
            }
            else
            {
                memberID = memberLookupMemberID;
            }

            CachedMember cachedMember = controller.GetCachedMember(memberID, currentServiceEnvironmentTypeEnum);
            currentMember = cachedMember;

            if (memberLookupMemberID == Matchnet.Constants.NULL_INT && memberLookupBrandID == Matchnet.Constants.NULL_INT)
            {
                brand = controller.GetLastBrand(queueItemText.CommunityID, cachedMember, currentServiceEnvironmentTypeEnum);
            }
            else
            {
                brand = controller.GetBrandByBrandID(memberLookupBrandID, currentServiceEnvironmentTypeEnum);
            }
            currentBrand = brand;

            DateTime birthDate = cachedMember.GetAttributeDate(510, DateTime.MinValue);
            Int32 age = 0;

            if (birthDate != DateTime.MinValue)
            {
                age = (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
            }

            AMVO.Attribute genderMaskAttribute = controller.GetAttributes(currentServiceEnvironmentTypeEnum).GetAttribute("GenderMask");
            AMVO.AttributeGroup genderMaskAttributeGroup = controller.GetAttributeGroup(brand, genderMaskAttribute, currentServiceEnvironmentTypeEnum);
            int genderMask = cachedMember.GetAttributeInt(genderMaskAttributeGroup.ID, Matchnet.Constants.NULL_INT);
            string genderMaskString = controller.GetGenderString(genderMask) + " " + controller.GetSeekingGenderString(genderMask);

            AMVO.Attribute lastUpdatedAttribute = controller.GetAttributes(currentServiceEnvironmentTypeEnum).GetAttribute("LastUpdated");
            AMVO.AttributeGroup lastUpdatedAttributeGroup = controller.GetAttributeGroup(brand, lastUpdatedAttribute, currentServiceEnvironmentTypeEnum);
            DateTime lastUpdated = cachedMember.GetAttributeDate(lastUpdatedAttributeGroup.ID, DateTime.MinValue);

            currentViewProfileLink = controller.GetMemberViewProfileLink(brand, cachedMember, currentServiceEnvironmentTypeEnum);

            if (memberLookupMemberID == Matchnet.Constants.NULL_INT && memberLookupBrandID == Matchnet.Constants.NULL_INT)
            {
                MemberDisplay.PopulateDisplay(cachedMember.MemberID.ToString(), currentViewProfileLink, brand.Site.Community.Name, age.ToString(), genderMaskString, lastUpdated.ToString("d"), Convert.ToString(GlobalState[Spark.Admin.Common.Constants.StateMemberEmail]), queueItemText.QueueDate.ToString("d"));
            }
            else
            {
                MemberDisplay.PopulateDisplay(cachedMember.MemberID.ToString(), currentViewProfileLink, brand.Site.Community.Name, age.ToString(), genderMaskString, lastUpdated.ToString("d"), Convert.ToString(GlobalState[Spark.Admin.Common.Constants.StateMemberEmail]), DateTime.Now.ToString("d"));
            }

            AMVO.AttributeCollection attributeCollection = controller.GetAttributeCollections(currentServiceEnvironmentTypeEnum).GetCollection("FREETEXT", brand.Site.Community.CommunityID, Matchnet.Constants.NULL_INT, Matchnet.Constants.NULL_INT);

            Byte rowCount = 0;

            foreach (AMVO.AttributeCollectionAttribute aca in attributeCollection)
            {
                AMVO.Attribute attribute = controller.GetAttributes(currentServiceEnvironmentTypeEnum).GetAttribute(aca.AttributeID);

                if (attribute.DataType == AMVO.DataType.Text)
                {
                    TextStatusType textStatusType = TextStatusType.None;
                    String attributeValue = String.Empty;

                    AMVO.AttributeGroup attributeGroup = controller.GetAttributeGroup(brand, attribute, currentServiceEnvironmentTypeEnum);

                    if (memberLookupMemberID == Matchnet.Constants.NULL_INT && memberLookupBrandID == Matchnet.Constants.NULL_INT)
                    {
                        attributeValue = cachedMember.GetAttributeText(attributeGroup.ID, queueItemText.LanguageID, null, out textStatusType);
                    }
                    else
                    {
                        attributeValue = cachedMember.GetAttributeText(attributeGroup.ID, brand.Site.LanguageID, null, out textStatusType);
                    }
                    

                    if (attributeValue != null)
                    {
                        // Prevent exceptions about text length exceeding attributegroup max length.
                        if (attributeValue.Length > attributeGroup.Length)
                        {
                            attributeValue = attributeValue.Substring(0, attributeGroup.Length);
                        }

                        FreeTextApprovalRichTextBoxRow freeTextApprovalRichTextBoxRow = new FreeTextApprovalRichTextBoxRow(attributeValue, attribute, textStatusType);

                        RowsLayoutPanel.RowCount = rowCount++;

                        RowsLayoutPanel.Controls.Add(freeTextApprovalRichTextBoxRow, 0, RowsLayoutPanel.RowCount);
                    }
                }
            }
        }

        /// <summary>
        /// Saves and deletes member attributes.
        /// </summary>
        private void SaveTextAttributes()
        {
            CachedMember cachedMember = currentMember;
            Brand brand = currentBrand;
            System.Collections.Hashtable textAttributes = new System.Collections.Hashtable(RowsLayoutPanel.Controls.Count);

            foreach (Control row in RowsLayoutPanel.Controls)
            {
                FreeTextApprovalRichTextBoxRow freeTextApprovalRichTextBoxRow = (FreeTextApprovalRichTextBoxRow)row;

                string text = freeTextApprovalRichTextBoxRow.FreeTextApprovalRichTextBox.Text;

                if (freeTextApprovalRichTextBoxRow.DeleteCheckBox.Checked)
                {
                    text = null;
                }

                // Username cannot be empty when admin deletes it. Replace it with member ID
                if ((freeTextApprovalRichTextBoxRow.AttributeName.Text.ToLower() == "username") && ((text == null) || (text == "")))
                {
                    text = currentMember.MemberID.ToString();
                }

                textAttributes.Add(freeTextApprovalRichTextBoxRow.AttributeName.Text, new TextValue(text, TextStatusType.Human));
            }

            controller.SaveMemberAttributeTexts(cachedMember.MemberID, brand.BrandID, textAttributes, currentServiceEnvironmentTypeEnum);
        }

        private void SuspendMember()
        {
            CachedMember cachedMember = currentMember;
            Brand brand = currentBrand;
            System.Collections.Hashtable intAttributes = new System.Collections.Hashtable(1);


            //445: attributeGroupID for globalstatusmask
            // if not found return use 0 instead of null int which is the default value.
            int globalStatusMask = cachedMember.GetAttributeInt(445, 0);
            //1: admin_suspend
            globalStatusMask = globalStatusMask | 1;

            intAttributes.Add("GlobalStatusMask", globalStatusMask);

            controller.SaveMemberAttributeInts(cachedMember.MemberID, brand.BrandID, intAttributes, currentServiceEnvironmentTypeEnum);

        }

        /// <summary>
        /// Freezes the UI. Used when a process begins.
        /// </summary>
        private void Freeze()
        {
            Cursor.Current = Cursors.WaitCursor;
            ApproveButton.Enabled = false;
            SuspendButton.Enabled = false;
            MemberDisplay.Enabled = false;

            pleaseWait = new PleaseWait();
            pleaseWait.StartPosition = FormStartPosition.Manual;

            pleaseWait.Location = new Point((ParentForm.Location.X) + (ParentForm.Width / 2), (ParentForm.Location.Y) + (ParentForm.Height / 2));

            pleaseWait.Show();

            Application.DoEvents();
            Refresh();
        }

        private void Melt()
        {
            Cursor.Current = Cursors.Default;
            ApproveButton.Enabled = true;
            SuspendButton.Enabled = true;
            MemberDisplay.Enabled = true;

            pleaseWait.Hide();

            Application.DoEvents();
            Refresh();
        }

        private void ClearFormData()
        {
      
            RowsLayoutPanel.Controls.Clear();
            currentMember = null;
            currentBrand = null;
            currentQueueItemKey = null;
            currentViewProfileLink = null;
        }

        #region Events

        private void FreeTextApprovalView_Load(object sender, EventArgs e)
        {
            try
            {
                controller = new FreeTextApprovalController();
                currentServiceEnvironmentTypeEnum = ServiceEnvironmentTypeEnum.Dev;

                ServiceEnvironmentComboBox.DataSource = Enum.GetValues(typeof(ServiceEnvironmentTypeEnum));

                // Only peepz with dev access should be able to do dev approvals. Don't even show that option if they don't.
                if (!HasPrivilege(ModuleWorkItem.DevApprovalApplication))
                {
                    ServiceEnvironmentComboBox.Enabled = false;
                    ServiceEnvironmentComboBox.Visible = false;

                    currentServiceEnvironmentTypeEnum = ServiceEnvironmentTypeEnum.Prod;
                }

                ApproveButton.Image = Properties.Resources.Approved.ToBitmap();
                ApproveButton.TextImageRelation = TextImageRelation.ImageBeforeText;

                SuspendButton.Image = Properties.Resources.Suspended.ToBitmap();
                SuspendButton.TextImageRelation = TextImageRelation.ImageBeforeText;
            }
            catch (Exception ex)
            {
                BaseController.HandleError(this.Title, ex.ToString());
            }
        }

        private void AddToHistoryDataGridView(Icon icon, String memberID, String link)
        {
            if (HistoryDataGridView.Rows.Count > 1000)
            {
                HistoryDataGridView.Rows.Clear();
            }

            totalAction++;

            DataGridViewRow row = new DataGridViewRow();

            DataGridViewImageCell imageCell = new DataGridViewImageCell(true);
            imageCell.Value = icon;
            row.Cells.Add(imageCell);

            DataGridViewLinkCell linkCell = new DataGridViewLinkCell();
            linkCell.LinkBehavior = LinkBehavior.HoverUnderline;
            linkCell.Value = currentMember.MemberID.ToString();
            linkCell.Tag = currentViewProfileLink;
            row.Cells.Add(linkCell);

            DataGridViewTextBoxCell orderCell = new DataGridViewTextBoxCell();
            orderCell.Value = totalAction; 
            row.Cells.Add(orderCell);

            HistoryDataGridView.Rows.Add(row);

            HistoryDataGridView.Sort(HistoryDataGridView.Columns["Order"], ListSortDirection.Descending);
        }

        private void ApproveButton_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean isMemberLookup = (currentQueueItemKey == null) ? true : false;

                Freeze();

                SaveTextAttributes();
                controller.AdminActionLogInsert(currentMember.MemberID, currentBrand.Site.Community.CommunityID, 1, Convert.ToInt32(GlobalState[Spark.Admin.Common.Constants.StateMemberID]), currentServiceEnvironmentTypeEnum);

                if (!isMemberLookup)
                {
                    controller.CompleteApproval(currentQueueItemKey, currentServiceEnvironmentTypeEnum);
                }

                AddToHistoryDataGridView(Properties.Resources.Approved, currentMember.MemberID.ToString(), currentViewProfileLink);

                ClearFormData();

                if (Selector.SelectedIndex != -1)
                {
                    PopulateRows();
                }

                Melt();
            }
            catch (Exception ex)
            {
                BaseController.HandleError(this.Title, ex.ToString());
            }
        }

        private void SuspendButton_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean isMemberLookup = (currentQueueItemKey == null) ? true : false;

                Freeze();

                SaveTextAttributes();
                SuspendMember();
                controller.AdminActionLogInsert(currentMember.MemberID, currentBrand.Site.Community.CommunityID, 4, Convert.ToInt32(GlobalState[Spark.Admin.Common.Constants.StateMemberID]), currentServiceEnvironmentTypeEnum);

                if (!isMemberLookup)
                {
                    controller.CompleteApproval(currentQueueItemKey, currentServiceEnvironmentTypeEnum);
                }

                AddToHistoryDataGridView(Properties.Resources.Suspended, currentMember.MemberID.ToString(), currentViewProfileLink);

                ClearFormData();

                if (Selector.SelectedIndex != -1)
                {
                    PopulateRows();
                }

                Melt();
            }
            catch (Exception ex)
            {
                BaseController.HandleError(this.Title, ex.ToString());
            }
        }

        private void Selector_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Freeze();

                switch ((String)Selector.SelectedItem)
                {
                    case "English":
                        currentSelectedLanguageID = 2;
                        break;

                    case "Hebrew":
                        currentSelectedLanguageID = 262144;
                        break;

                    case null:
                        Melt();
                        return;
                }
                
                ClearFormData();

                PopulateRows();

                Melt();
            }
            catch (Exception ex)
            {
                BaseController.HandleError(this.Title, ex.ToString());
            }
        }

        private void ServiceEnvironmentComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Freeze();

                if (ServiceEnvironmentComboBox.SelectedItem.ToString() == "Dev")
                {
                    currentServiceEnvironmentTypeEnum = ServiceEnvironmentTypeEnum.Dev;
                }
                else
                {
                    currentServiceEnvironmentTypeEnum = ServiceEnvironmentTypeEnum.Prod;
                }

                switch ((String)Selector.SelectedItem)
                {
                    case "English":
                        currentSelectedLanguageID = 2;
                        break;

                    case "Hebrew":
                        currentSelectedLanguageID = 262144;
                        break;

                    case null:
                        Melt();
                        return;
                }

                ClearFormData();

                PopulateRows();

                Melt();
            }
            catch (Exception ex)
            {
                BaseController.HandleError(this.Title, ex.ToString());
            }
        }

        private void HistoryDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (HistoryDataGridView.Columns[e.ColumnIndex] is DataGridViewLinkColumn && e.RowIndex != -1)
            {
                String link = HistoryDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Tag.ToString();

                System.Diagnostics.Process.Start(link);
            }
        }

        private void MemberLookupButton_Click(object sender, EventArgs e)
        {
            Freeze();

            Int32 brandID = Matchnet.Constants.NULL_INT;

            switch ((String)MemberLookupSiteComboBox.SelectedItem)
            {
                case "Americansingles.com":
                    brandID = 1001;
                    break;

                case "Jdate.com":
                    brandID = 1003;
                    break;

                case "Jdate.co.il":
                    brandID = 1004;
                    break;

                case "Cupid.co.il":
                    brandID = 1015;
                    break;

                case "Date.co.uk":
                    brandID = 1006;
                    break;

                case "Date.ca":
                    brandID = 1013;
                    break;

                case "Collegeluv.com":
                    brandID = 10012;
                    break;

                case null:
                    Melt();
                    return;
            }

            ClearFormData();

            PopulateRows(Convert.ToInt32(MemberLookupMemberIDTextBox.Text), brandID);

            Melt();
        }

        #endregion


        #endregion
    }
}
