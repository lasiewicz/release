﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using AMVO = Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Member.ValueObjects;

namespace Spark.Admin.FreeTextApprovalModule
{
    public partial class FreeTextApprovalRichTextBoxRow : UserControl
    {

        ToolTip toolTip = new ToolTip();

        public FreeTextApprovalRichTextBoxRow(String text, AMVO.Attribute attribute, TextStatusType textStatus)
        {
            InitializeComponent();

            FreeTextApprovalRichTextBox.MouseMove += new MouseEventHandler(FreeTextApprovalRichTextBox_MouseMove);
            AttributeName.Text = attribute.Name;

            switch (textStatus)
            {
                case TextStatusType.Human:
                    StatusIcon.Image = global::Spark.Admin.FreeTextApprovalModule.Properties.Resources.humanapproved;
                    break;

                case TextStatusType.Auto:
                    StatusIcon.Image = global::Spark.Admin.FreeTextApprovalModule.Properties.Resources.autoapproved;
                    break;

                default:
                    StatusIcon.Image = global::Spark.Admin.FreeTextApprovalModule.Properties.Resources.notapproved;
                    break;
            }

            FreeTextApprovalRichTextBox.Text = text;
            FreeTextApprovalRichTextBox.Highlight(true, true);
            FreeTextApprovalRichTextBox.Height = GetStringHeight(text, FreeTextApprovalRichTextBox.Font, FreeTextApprovalRichTextBox.Width);
            FreeTextApprovalRichTextBox.Height += 6;

        }

        void FreeTextApprovalRichTextBox_MouseMove(object sender, MouseEventArgs e)
        {
//#if DEBUG
//            String tipText = String.Format("Length:{0}, Width:{1}, Height:{2}", FreeTextApprovalRichTextBox.Lines.Length, FreeTextApprovalRichTextBox.Width, FreeTextApprovalRichTextBox.Height);
//            toolTip.Show(tipText, this, e.Location, 100);
//#endif
        }

        private void FreeTextApprovalRichTextBox_TextChanged(object sender, EventArgs e)
        {
            //FreeTextApprovalRichTextBox.Height = GetStringHeight(FreeTextApprovalRichTextBox.Text, FreeTextApprovalRichTextBox.Font, FreeTextApprovalRichTextBox.Width);
            //FreeTextApprovalRichTextBox.Height += 5;
        }

        private int GetStringHeight(String strMeasureString, System.Drawing.Font stringFont, Int32 nWidth)
        {
            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromHwnd(this.Handle))
            {

                return (int)g.MeasureString(strMeasureString, stringFont, nWidth).Height;
            }
        }

    }
}
