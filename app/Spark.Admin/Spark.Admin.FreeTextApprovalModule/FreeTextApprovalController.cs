﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

using Microsoft.Practices.CompositeUI;

using Spark.Admin.Common;
using Spark.Admin.WebServices;

using Matchnet.Content.ValueObjects.Admin;
using AMVO = Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ApproveQueue.ValueObjects;
using Matchnet.Member.ValueObjects;


namespace Spark.Admin.FreeTextApprovalModule
{
    class FreeTextApprovalController : BaseController
    {
        private WebServiceProxy webServiceProxy;

        // Prevents extra trips to the web services. These are immutable data.
        private Attributes attributes = null;
        private AttributeCollections attributeCollections = null;
        private Brands brands = null;

        #region Constructors

        public FreeTextApprovalController()
        {
            // Initialize the WebServiceProxy if you are using web services.
            webServiceProxy = new WebServiceProxy();        
        }

        #endregion

        #region Public Methods

        public void CompleteApproval(String queueItemKey, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            webServiceProxy.CompleteApproval(queueItemKey, serviceEnvironmentEnum);
        }

        public QueueItemBase[] DequeueText(Byte maxCount, Int32 languageID, Int32 communityID, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            return webServiceProxy.DequeueText(maxCount, languageID, communityID, serviceEnvironmentEnum);
        }

        public void AdminActionLogInsert(Int32 memberID, Int32 communityID, Int32 adminActionMask, Int32 adminMemberID, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            webServiceProxy.AdminActionLogInsert(memberID, communityID, adminActionMask, adminMemberID, serviceEnvironmentEnum);
        }

        public Attributes GetAttributes(ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            if (attributes == null)
            {
                attributes = webServiceProxy.GetAttributes(serviceEnvironmentEnum);
            }

            return attributes;
        }

        public AttributeCollections GetAttributeCollections(ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            if (attributeCollections == null)
            {
                attributeCollections = webServiceProxy.GetAttributeCollections(serviceEnvironmentEnum);
            }

            return attributeCollections;
        }

        public Brand GetBrandByBrandID(Int32 brandID, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            return webServiceProxy.GetBrandByBrandID(brandID, serviceEnvironmentEnum);
        }

        public Brands GetBrands(ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            if (brands == null)
            {
                brands = webServiceProxy.GetBrands(serviceEnvironmentEnum);
            }

            return brands;
        }

        public MemberSaveResult SaveMemberAttributeTexts(Int32 memberID, Int32 brandID, Hashtable textAttributes, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            return webServiceProxy.SaveMemberTextAttributes(memberID, brandID, textAttributes, serviceEnvironmentEnum);
        }

        public MemberSaveResult SaveMemberAttributeInts(Int32 memberID, Int32 brandID, Hashtable intAttributes, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            return webServiceProxy.SaveMemberIntAttributes(memberID, brandID, intAttributes, serviceEnvironmentEnum);
        }

        public CachedMember GetCachedMember(Int32 memberID, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            return webServiceProxy.GetCachedMember(memberID, serviceEnvironmentEnum);
        }

        // Copied from MemberSA.Member
        public AttributeGroup GetAttributeGroup(Brand brand, AMVO.Attribute attribute, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            Int32 groupID = 0;

            switch (attribute.Scope)
            {
                case ScopeType.Personals:
                    groupID = 8383;
                    break;

                case ScopeType.Community:
                    if (!(brand.Site.Community.CommunityID > 0))
                    {
                        throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + "), communityID not set.");
                    }
                    groupID = brand.Site.Community.CommunityID;
                    break;

                case ScopeType.Site:
                    if (!(brand.Site.SiteID > 0))
                    {
                        throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + "), siteID not set.");
                    }
                    groupID = brand.Site.SiteID;
                    break;

                case ScopeType.Brand:
                    if (!(brand.BrandID > 0))
                    {
                        throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + "), brandID not set.");
                    }
                    groupID = brand.BrandID;
                    break;
            }

            if (groupID == 0)
            {
                throw new Exception("Unable to determine groupID for attribute \"" + attribute.Name + "\" (" + attribute.ID.ToString() + ").");
            }

            return GetAttributes(serviceEnvironmentEnum).GetAttributeGroup(groupID, attribute.ID);
        }

        // Copied from MemberSA.Member
        public Brand GetLastBrand(Int32 communityID, CachedMember cachedMember, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            AMVO.Attributes attributes = GetAttributes(serviceEnvironmentEnum);

			Brands brands = GetBrands(serviceEnvironmentEnum);

            Int32 brandID;

            cachedMember.GetLastLogonDate(communityID, out brandID, attributes, brands);

            Brand brand = GetBrandByBrandID(brandID, serviceEnvironmentEnum);

            return brand;
        }

        public String GetGenderString(int genderMask)
        {
            string genderStr = string.Empty;

            if (genderMask < 0)
                return genderStr;

            if ((genderMask & Constants.GENDERID_MALE) == Constants.GENDERID_MALE)
                return "Male";

            if ((genderMask & Constants.GENDERID_FEMALE) == Constants.GENDERID_FEMALE)
                return "Female";

            if ((genderMask & Constants.GENDERID_MTF) == Constants.GENDERID_MTF)
                return "MTF";

            if ((genderMask & Constants.GENDERID_FTM) == Constants.GENDERID_FTM)
                return "FTM";

            // Else
            return genderStr;
        }

        public String GetSeekingGenderString(int genderMask)
        {
            string seeking = string.Empty;

            if ((genderMask & Constants.GENDERID_SEEKING_FEMALE) == Constants.GENDERID_SEEKING_FEMALE)
                seeking = "seeking Female";

            if ((genderMask & Constants.GENDERID_SEEKING_MALE) == Constants.GENDERID_SEEKING_MALE)
            {
                if (seeking.Length > 0)
                    seeking = seeking + ", ";

                seeking = seeking + "seeking Male";
            }

            if ((genderMask & Constants.GENDERID_SEEKING_MTF) == Constants.GENDERID_SEEKING_MTF)
            {
                if (seeking.Length > 0)
                    seeking = seeking + ", ";

                seeking = seeking + "seeking MTF";
            }

            if ((genderMask & Constants.GENDERID_SEEKING_FTM) == Constants.GENDERID_SEEKING_FTM)
            {
                if (seeking.Length > 0)
                    seeking = seeking + ", ";

                seeking = seeking + "seeking FTM";
            }

            return seeking;
        }

        public String GetMemberViewProfileLink(Brand brand, CachedMember member, ServiceEnvironmentTypeEnum serviceEnvironmentEnum)
        {
            String host = "www";

            if (serviceEnvironmentEnum == ServiceEnvironmentTypeEnum.Dev)
            {
                host = "dev";
            }

            String url = @"http://" + host + "." + brand.Uri + "/Applications/MemberProfile/ViewProfile.aspx" + "?MemberID=" + member.MemberID.ToString() + "&LayoutTemplateID=11";

            return url;
        }

        public class Constants
        {
            public const int GENDERID_MALE = 1;
		    public const int GENDERID_FEMALE = 2;
		    public const int GENDERID_SEEKING_MALE = 4;
		    public const int GENDERID_SEEKING_FEMALE = 8;
		    public const int GENDERID_MTF = 16;
		    public const int GENDERID_FTM = 32;
		    public const int GENDERID_SEEKING_MTF = 64;
		    public const int GENDERID_SEEKING_FTM = 128;
        }
        #endregion
        
        #region Properties
        public new ModuleWorkItem WorkItem
        {
            get
            {
                return base.WorkItem as ModuleWorkItem;
            }
        }
        #endregion
    }
}
