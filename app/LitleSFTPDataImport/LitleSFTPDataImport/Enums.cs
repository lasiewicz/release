﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitleSFTPDataImport.Enums
{
    [Flags]
    public enum LitleReportError
    {
        None = 0,
        CSVProcessingError = 2,
        DatabaseSaveError = 4,
        RollbackFailed = 8
    }
}
