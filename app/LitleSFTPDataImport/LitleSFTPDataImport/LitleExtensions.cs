﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LitleSFTPDataImport.ExtensionMethods
{
    public static class LitleExtensions
    {
        public static string RemoveApostrophe(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return null;

            if (str[0] == '\'')
            {
                return str.Substring(1, str.Length - 1);
            }

            return str;
        }
    }
}
