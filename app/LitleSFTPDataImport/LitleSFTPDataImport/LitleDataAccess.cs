﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using LitleSFTPDataImport.DataObjects;

namespace LitleSFTPDataImport
{
    public sealed class LitleDataAccess
    {
        private static readonly LitleDataAccess instance = new LitleDataAccess();
        private string _connectionString;

        private LitleDataAccess()
        {
        }

        public static LitleDataAccess Instance
        {
            get { return instance; }
        }

        public string ConnectionString
        {
            get
            {
                if (string.IsNullOrEmpty(_connectionString))
                {
                    _connectionString = ConfigurationManager.ConnectionStrings["LitleDB"].ConnectionString;
                }

                return _connectionString;
            }
        }

        public List<LitleReportFile> GetLitleReportFileNames()
        {
            var list = new List<LitleReportFile>();

            try
            {
                using (var conn = new SqlConnection(ConnectionString))
                using (var command = new SqlCommand("dbo.up_LitleReportFile_List", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    conn.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            list.Add(new LitleReportFile {Name = reader["Name"].ToString()});
                        }
                    }

                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(Constants.EventLogSource, ex.Message, EventLogEntryType.Error);
            }

            return list;
        }

        public bool SaveSessionReport(List<LitleSessionReportRow> reportRows)
        {
            bool isSuccess = true;
            int recordCount = 0;

            try
            {
                using (var conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (var truncateCommand = new SqlCommand("dbo.up_LitleSessionReportStaging_Truncate", conn))
                    {
                        truncateCommand.CommandType = CommandType.StoredProcedure;
                        truncateCommand.ExecuteNonQuery();
                    }

                    #region stored proc parameter list
                    var paramDictionary = new Dictionary<string, SqlDbType>();
                    paramDictionary.Add("@SessionID", SqlDbType.BigInt);
                    paramDictionary.Add("@BatchID", SqlDbType.BigInt);
                    paramDictionary.Add("@LitlePaymentID", SqlDbType.BigInt);
                    paramDictionary.Add("@ParentLitlePaymentID", SqlDbType.BigInt);
                    paramDictionary.Add("@BatchPostDay", SqlDbType.DateTime2);
                    paramDictionary.Add("@TransactionProcessingTimestampGMT", SqlDbType.DateTime2);
                    paramDictionary.Add("@BatchCompletionTimestampGMT", SqlDbType.DateTime2);
                    paramDictionary.Add("@PurchaseAmt", SqlDbType.Decimal);
                    paramDictionary.Add("@SalesTax", SqlDbType.Decimal);
                    paramDictionary.Add("@ReportingGroup", SqlDbType.NVarChar);
                    paramDictionary.Add("@Presenter", SqlDbType.NVarChar);
                    paramDictionary.Add("@Merchant", SqlDbType.NVarChar);
                    paramDictionary.Add("@MerchantID", SqlDbType.NVarChar);
                    paramDictionary.Add("@MerchantOrderNumber", SqlDbType.NVarChar);
                    paramDictionary.Add("@CustomerID", SqlDbType.NVarChar);
                    paramDictionary.Add("@TxnType", SqlDbType.NVarChar);
                    paramDictionary.Add("@PurchaseCurrency", SqlDbType.NVarChar);
                    paramDictionary.Add("@PaymentType", SqlDbType.NVarChar);
                    paramDictionary.Add("@Bin", SqlDbType.NVarChar);
                    paramDictionary.Add("@AccountSuffix", SqlDbType.NVarChar);
                    paramDictionary.Add("@ResponseReasonCode", SqlDbType.NVarChar);
                    paramDictionary.Add("@ResponseReasonMessage", SqlDbType.NVarChar);
                    paramDictionary.Add("@Avs", SqlDbType.NVarChar);
                    paramDictionary.Add("@FraudCheckSumResponse", SqlDbType.NVarChar);
                    paramDictionary.Add("@PayerID", SqlDbType.NVarChar);
                    paramDictionary.Add("@MerchantTransactionID", SqlDbType.NVarChar);
                    paramDictionary.Add("@Affiliate", SqlDbType.NVarChar);
                    paramDictionary.Add("@Campaign", SqlDbType.NVarChar);
                    paramDictionary.Add("@MerchantGroupingID", SqlDbType.NVarChar);
                    paramDictionary.Add("@FTPFilename", SqlDbType.NVarChar);
                    #endregion

                    using (var command = new SqlCommand("dbo.up_LitleSessionReportStaging_INSERT", conn))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        bool firstRow = true;

                        foreach (LitleSessionReportRow row in reportRows)
                        {
                            if (firstRow)
                            {
                                foreach (var pair in paramDictionary)
                                {
                                    command.Parameters.Add(pair.Key, pair.Value);
                                }

                                firstRow = false;
                            }
                            else
                            {
                                // clear the previous values
                                foreach (var pair in paramDictionary)
                                {
                                    command.Parameters[pair.Key].Value = DBNull.Value;
                                }
                            }

                            #region object property to db column mapping

                            // Load the values from the row object here
                            command.Parameters["@SessionID"].Value = row.SessionID.HasValue
                                                                         ? row.SessionID.Value
                                                                         : (Int64?) null;
                            command.Parameters["@BatchID"].Value = row.BatchID.HasValue
                                                                       ? row.BatchID.Value
                                                                       : (Int64?) null;
                            command.Parameters["@LitlePaymentID"].Value = row.LitlePaymentID.HasValue
                                                                              ? row.LitlePaymentID.Value
                                                                              : (Int64?) null;
                            command.Parameters["@ParentLitlePaymentID"].Value = row.ParentLitlePaymentID.HasValue
                                                                                    ? row.ParentLitlePaymentID.Value
                                                                                    : (Int64?) null;
                            command.Parameters["@BatchPostDay"].Value = row.BatchPostDay.HasValue
                                                                            ? row.BatchPostDay.Value
                                                                            : (DateTime?) null;
                            command.Parameters["@TransactionProcessingTimestampGMT"].Value =
                                row.TransactionProcessingTimestampGMT.HasValue
                                    ? row.TransactionProcessingTimestampGMT.Value
                                    : (DateTime?) null;
                            command.Parameters["@BatchCompletionTimestampGMT"].Value =
                                row.BatchCompletionTimestampGMT.HasValue
                                    ? row.BatchCompletionTimestampGMT.Value
                                    : (DateTime?) null;
                            command.Parameters["@PurchaseAmt"].Value = row.PurchaseAmt.HasValue
                                                                           ? row.PurchaseAmt.Value
                                                                           : (decimal?) null;
                            command.Parameters["@SalesTax"].Value = row.SalesTax.HasValue
                                                                        ? row.SalesTax.Value
                                                                        : (decimal?) null;
                            command.Parameters["@ReportingGroup"].Value = string.IsNullOrEmpty(row.ReportingGroup)
                                                                              ? null
                                                                              : row.ReportingGroup;
                            command.Parameters["@Presenter"].Value = string.IsNullOrEmpty(row.Presenter)
                                                                         ? null
                                                                         : row.Presenter;
                            command.Parameters["@Merchant"].Value = string.IsNullOrEmpty(row.Merchant)
                                                                        ? null
                                                                        : row.Merchant;
                            command.Parameters["@MerchantID"].Value = string.IsNullOrEmpty(row.MerchantID)
                                                                          ? null
                                                                          : row.MerchantID;
                            command.Parameters["@MerchantOrderNumber"].Value =
                                string.IsNullOrEmpty(row.MerchantOrderNumber) ? null : row.MerchantOrderNumber;
                            command.Parameters["@CustomerID"].Value = string.IsNullOrEmpty(row.CustomerID)
                                                                          ? null
                                                                          : row.CustomerID;
                            command.Parameters["@TxnType"].Value = string.IsNullOrEmpty(row.TxnType)
                                                                       ? null
                                                                       : row.TxnType;
                            command.Parameters["@PurchaseCurrency"].Value = string.IsNullOrEmpty(row.PurchaseCurrency)
                                                                                ? null
                                                                                : row.PurchaseCurrency;
                            command.Parameters["@PaymentType"].Value = string.IsNullOrEmpty(row.PaymentType)
                                                                           ? null
                                                                           : row.PaymentType;
                            command.Parameters["@Bin"].Value = string.IsNullOrEmpty(row.Bin) ? null : row.Bin;
                            command.Parameters["@AccountSuffix"].Value = string.IsNullOrEmpty(row.AccountSuffix)
                                                                             ? null
                                                                             : row.AccountSuffix;
                            command.Parameters["@ResponseReasonCode"].Value =
                                string.IsNullOrEmpty(row.ResponseReasonCode) ? null : row.ResponseReasonCode;
                            command.Parameters["@ResponseReasonMessage"].Value =
                                string.IsNullOrEmpty(row.ResponseReasonMessage) ? null : row.ResponseReasonMessage;
                            command.Parameters["@Avs"].Value = string.IsNullOrEmpty(row.Avs) ? null : row.Avs;
                            command.Parameters["@FraudCheckSumResponse"].Value =
                                string.IsNullOrEmpty(row.FraudCheckSumResponse) ? null : row.FraudCheckSumResponse;
                            command.Parameters["@PayerID"].Value = string.IsNullOrEmpty(row.PayerID)
                                                                       ? null
                                                                       : row.PayerID;
                            command.Parameters["@MerchantTransactionID"].Value =
                                string.IsNullOrEmpty(row.MerchantTransactionID) ? null : row.MerchantTransactionID;
                            command.Parameters["@Affiliate"].Value = string.IsNullOrEmpty(row.Affiliate)
                                                                         ? null
                                                                         : row.Affiliate;
                            command.Parameters["@Campaign"].Value = string.IsNullOrEmpty(row.Campaign)
                                                                        ? null
                                                                        : row.Campaign;
                            command.Parameters["@MerchantGroupingID"].Value =
                                string.IsNullOrEmpty(row.MerchantGroupingID) ? null : row.MerchantGroupingID;
                            command.Parameters["@FTPFilename"].Value = string.IsNullOrEmpty(row.FTPFilename)
                                                                           ? null
                                                                           : row.FTPFilename;

                            #endregion

                            command.ExecuteNonQuery();
                        }
                    }

                    using (
                        var getCountCommand = new SqlCommand("dbo.up_LitleSessionReportStaging_GetCount", conn))
                    {
                        getCountCommand.CommandType = CommandType.StoredProcedure;

                        SqlDataReader reader = getCountCommand.ExecuteReader();

                        if (reader.HasRows)
                        {
                            reader.Read();
                            recordCount = Convert.ToInt32(reader["RecordCount"]);
                        }

                        reader.Close();
                    }

                    isSuccess = reportRows.Count == recordCount;

                    if (isSuccess)
                    {
                        using (var commitCommand = new SqlCommand("dbo.up_LitleSessionReport_Commit", conn))
                        {
                            commitCommand.CommandType = CommandType.StoredProcedure;
                            commitCommand.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(Constants.EventLogSource, ex.Message, EventLogEntryType.Error);
                isSuccess = false;
            }

            return isSuccess;
        }

        public bool RollBackSessionReport()
        {
            bool isSuccess = true;

            try
            {
                using (var conn = new SqlConnection(ConnectionString))
                using (var command = new SqlCommand("dbo.up_LitleSessionReportStaging_Truncate", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(Constants.EventLogSource, ex.Message, EventLogEntryType.Error);
                isSuccess = false;
            }

            return isSuccess;
        }

        public bool SaveDownloadedReportName(string[] fileNames)
        {
            bool isSuccess = true;

            try
            {
                using (var conn = new SqlConnection(ConnectionString))
                using (var command = new SqlCommand("dbo.up_LitleReportFile_INSERT", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    conn.Open();

                    bool firstRow = true;
                    foreach (string fName in fileNames)
                    {
                        if (firstRow)
                        {
                            command.Parameters.Add("@Name", SqlDbType.NVarChar);
                            firstRow = false;
                        }

                        command.Parameters["@Name"].Value = fName;
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(Constants.EventLogSource, ex.Message, EventLogEntryType.Error);
                isSuccess = false;
            }

            return isSuccess;
        }
    }
}