﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using CsvHelper;
using LitleSFTPDataImport.DataObjects;
using LitleSFTPDataImport.Enums;
using LitleSFTPDataImport.ExtensionMethods;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using Spark.Common.Security;

namespace LitleSFTPDataImport
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            // download all the reports
            DownloadReports();

            // import the data ignoring files we already processed
            ImportFileDataToDB();

            // copy the files to a secure shared location
            CopyFilesToSecureShare();

            // clean up actions like deleting the files from the local working directory
            PerformCleanup();
        }
        
        private static void CopyFilesToSecureShare()
        {
            var secureShareDirectory = ConfigurationManager.AppSettings.Get("SecureShareDirectory");
            var localWorkingDir = ConfigurationManager.AppSettings.Get("LocalWorkingDirectory");
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Secure Share Location: " + secureShareDirectory + "\n");
            sb.AppendLine("List of Files Copied: ");

            string[] fileNames = Directory.GetFiles(localWorkingDir, "*.*", SearchOption.TopDirectoryOnly);
            foreach (string fullFilePath in fileNames)
            {
                var fileName = Path.GetFileName(fullFilePath);
                sb.AppendLine(fileName);
                var destPath = secureShareDirectory + fileName;
                File.Copy(fullFilePath, destPath, true);
            }

            SendEmail("Litle Reports Uploaded to Secure Share", sb.ToString());
        }

        private static bool DownloadReports()
        {
            try
            {

                string username = ConfigurationManager.AppSettings.Get("Username");
                string password = Encryption.DecryptString(ConfigurationManager.AppSettings.Get("PasswordCipher"));
                string serverAddress = ConfigurationManager.AppSettings.Get("ServerAddress");
                int port = Convert.ToInt32(ConfigurationManager.AppSettings.Get("PortNumber"));
                string reportsDir = ConfigurationManager.AppSettings.Get("RemoteReportsDirectory");
                string localWorkingDir = ConfigurationManager.AppSettings.Get("LocalWorkingDirectory");

                var passwordConnection = new PasswordAuthenticationMethod(username, password);

                #region Commented out - keyboard interactive mode is not needed

                //var keyboardInteractive = new KeyboardInteractiveAuthenticationMethod(username);
                //keyboardInteractive.AuthenticationPrompt += (sender, eventArgs) =>
                //    {
                //        foreach (var prompt in eventArgs.Prompts)
                //        {
                //            Debug.Print(prompt.Request);
                //            if (prompt.Request.ToLower().Contains("password:"))
                //            {
                //                Debug.Print("sending password");
                //                prompt.Response = password;
                //            }
                //        }
                //    };

                #endregion

                var connectionInfo = new ConnectionInfo(serverAddress, port, username, passwordConnection);
                var sftp = new SftpClient(connectionInfo);

                // we need this so that we don't download files we already downloaded. on the ftp server, it seems to be a sliding window
                // of certain amount of days
                var pastReports = LitleDataAccess.Instance.GetLitleReportFileNames();

                sftp.Connect();

                // get list of files to download
                IEnumerable<SftpFile> fileList = sftp.ListDirectory(reportsDir);

                // pull down all the files and ignore directories which include "." and ".."
                foreach (SftpFile file in fileList)
                {
                    if (!file.IsDirectory)
                    {
                        // make sure we didn't download this before
                        var query = (from LitleReportFile report in pastReports
                                     where report.Name.ToLower() == file.Name.ToLower()
                                     select report).SingleOrDefault();

                        if (query == null)
                        {
                            using (var localFile = new FileStream(localWorkingDir + file.Name, FileMode.Create))
                            {
                                sftp.DownloadFile(reportsDir + "/" + file.Name, localFile);
                            }
                        }
                    }
                }

                sftp.Disconnect();

                // write the downloaded file names to DB
                var fileNames = Directory.GetFiles(localWorkingDir, "*.*", SearchOption.TopDirectoryOnly).Select(Path.GetFileName).ToArray();
                LitleDataAccess.Instance.SaveDownloadedReportName(fileNames);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(Constants.EventLogSource, "Exception during download phase: " + ex.Message, EventLogEntryType.Error);
                return false;
            }

            return true;
        }

        private static bool ImportFileDataToDB()
        {
            var localWorkingDir = ConfigurationManager.AppSettings.Get("LocalWorkingDirectory");

            #region we are doing this filter during download now
            //// delete the report files we already processed
            //List<LitleReportFile> pastReports = LitleDataAccess.Instance.GetLitleReportFileNames();

            //string[] fileNames = Directory.GetFiles(localWorkingDir, "*.*", SearchOption.TopDirectoryOnly);
            //foreach (string fullFilePath in fileNames)
            //{
            //    LitleReportFile dbSearchResult = (from LitleReportFile r in pastReports
            //                                      where r.Name.ToLower() == Path.GetFileName(fullFilePath).ToLower()
            //                                      select r).SingleOrDefault();

            //    // if in db, just delete this file because we do not want to process it again
            //    if (dbSearchResult != null)
            //    {
            //        File.Delete(fullFilePath);
            //    }
            //}
            #endregion

            // all the files that remain in the directory, we must process
            // Transactional_Detail_SessionByActivityDate reports are the ones we want to process to DB
            // the rest we just copy it to the secure share location
            var fileNames = Directory.GetFiles(localWorkingDir, "*.*", SearchOption.TopDirectoryOnly);
            
            foreach (string fullFilePath in fileNames)
            {
                if (fullFilePath.ToLower().Contains("transactional_detail_sessionbyactivitydate"))
                {
 
                    int recordCount;
                    decimal netPurchaseAmount;
                    var reportErrors = ProcessCSVFile(fullFilePath, out recordCount, out netPurchaseAmount);
                    ProcessPostImportActions(Path.GetFileName(fullFilePath), reportErrors, recordCount, netPurchaseAmount);

                }
            }

            return true;
        }

        private static LitleReportError ProcessCSVFile(string filePath, out int recordCount, out decimal netPurchaseAmount)
        {
            var sessionReportRows = new List<LitleSessionReportRow>();
            var errorType = LitleReportError.None;

            try
            {
                using (var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                using (var sr = new StreamReader(fs))
                using (var csv = new CsvReader(sr))
                {
                    while (csv.Read())
                    {
                        var aRow = new LitleSessionReportRow();

                        #region csv filed to data object property mapping
                        aRow.SessionID = Convert.ToInt64(csv.GetField<string>("Session ID").RemoveApostrophe());
                        aRow.BatchID = Convert.ToInt64(csv.GetField<string>("Batch ID").RemoveApostrophe());
                        aRow.BatchPostDay = Convert.ToDateTime(csv.GetField<string>("Batch Post Day"));
                        aRow.TransactionProcessingTimestampGMT =
                            Convert.ToDateTime(csv.GetField<string>("Transaction Processing Timestamp GMT"));
                        aRow.BatchCompletionTimestampGMT =
                            Convert.ToDateTime(csv.GetField<string>("Batch Completion Timestamp GMT"));
                        aRow.ReportingGroup = csv.GetField<string>("Reporting Group");
                        aRow.Presenter = csv.GetField<string>("Presenter");
                        aRow.Merchant = csv.GetField<string>("Merchant");
                        aRow.MerchantID = csv.GetField<string>("Merchant ID");
                        aRow.LitlePaymentID = Convert.ToInt64(csv.GetField<string>("Vantiv Payment ID").RemoveApostrophe());

                        aRow.ParentLitlePaymentID =
                            string.IsNullOrEmpty(csv.GetField<string>("Parent Vantiv Payment ID").RemoveApostrophe())
                                ? (Int64?)null
                                : Convert.ToInt64(csv.GetField<string>("Parent Vantiv Payment ID").RemoveApostrophe());

                        aRow.MerchantOrderNumber = csv.GetField<string>("Merchant Order #");
                        aRow.CustomerID = csv.GetField<string>("Customer ID");
                        aRow.TxnType = csv.GetField<string>("Txn Type");
                        aRow.PurchaseCurrency = csv.GetField<string>("Purchase Currency");
                        aRow.PurchaseAmt = Convert.ToDecimal(csv.GetField<string>("Purchase Amt"));
                        aRow.PaymentType = csv.GetField<string>("Payment Type");
                        aRow.Bin = csv.GetField<string>("BIN");
                        aRow.AccountSuffix = csv.GetField<string>("Account Suffix");
                        aRow.ResponseReasonCode = csv.GetField<string>("Response Reason Code");
                        aRow.ResponseReasonMessage = csv.GetField<string>("Response Reason Message");
                        aRow.Avs = csv.GetField<string>("AVS");
                        aRow.FraudCheckSumResponse = csv.GetField<string>("Fraud Check Sum Response");
                        aRow.PayerID = csv.GetField<string>("Payer ID");
                        aRow.MerchantTransactionID = csv.GetField<string>("Merchant Transaction ID");
                        aRow.Affiliate = csv.GetField<string>("Affiliate");
                        aRow.Campaign = csv.GetField<string>("Campaign");
                        aRow.MerchantGroupingID = csv.GetField<string>("Merchant Grouping ID");

                        aRow.SalesTax = csv.GetField<string>("Sales Tax") == string.Empty
                                            ? (decimal?)null
                                            : Convert.ToDecimal(csv.GetField<string>("Sales Tax"));

                        aRow.FTPFilename = csv.GetField<string>("FTP Filename");
                        #endregion

                        sessionReportRows.Add(aRow);
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(Constants.EventLogSource, "Exception during CSV processing : " + ex.Message, EventLogEntryType.Error);
                errorType |= LitleReportError.CSVProcessingError;
            }

            // if we were able to process the csv file, then send the rows to DB
            if (errorType == LitleReportError.None)
            {
                var dbWriteIsSuccess = LitleDataAccess.Instance.SaveSessionReport(sessionReportRows);
                if (!dbWriteIsSuccess)
                {
                    errorType |= LitleReportError.DatabaseSaveError;
                    // if db write failed, try to roll back and report the rollback status
                    errorType = LitleDataAccess.Instance.RollBackSessionReport()
                                    ? errorType | 0
                                    : errorType | LitleReportError.RollbackFailed;
                }
            }

            // only do the out parameter calculations when everything checks out
            if (errorType == LitleReportError.None)
            {
                recordCount = sessionReportRows.Count;

                decimal? depositSum = (from reportRow in sessionReportRows
                                       where reportRow.TxnType.ToLower() == "deposit"
                                       select reportRow.PurchaseAmt).Sum();

                decimal? refundSum = (from reportRow in sessionReportRows
                                      where
                                          reportRow.TxnType.ToLower() == "refund"
                                      select reportRow.PurchaseAmt).Sum();

                netPurchaseAmount = (depositSum.HasValue ? depositSum.Value : 0) -
                                    (refundSum.HasValue ? refundSum.Value : 0);

            }
            else
            {
                recordCount = 0;
                netPurchaseAmount = 0;
            }

            return errorType;
        }

        private static void ProcessPostImportActions(string fileName, LitleReportError reportErrors, int recordCount,
                                                     decimal netPurchaseAmount)
        {
            StringBuilder sb = new StringBuilder();
            string subject = reportErrors == LitleReportError.None
                                 ? "Litle Report Import Success: " + fileName
                                 : "Litle Report Import Failed: " + fileName;

            if (reportErrors == LitleReportError.None)
            {
                sb.AppendLine("Total Record Count: " + recordCount);
                sb.AppendLine("Net Purchase Amount: " + netPurchaseAmount);
            }
            else
            {
                if ((reportErrors & LitleReportError.CSVProcessingError) == LitleReportError.CSVProcessingError)
                {
                    sb.AppendLine("Report import failed during CSV file processing phase.");
                }

                if ((reportErrors & LitleReportError.DatabaseSaveError) == LitleReportError.DatabaseSaveError)
                {
                    sb.AppendLine("Report import failed during database write phase.");
                    sb.AppendLine("Rollback was " + ((reportErrors & LitleReportError.RollbackFailed) ==
                                  LitleReportError.RollbackFailed
                        ? "unsuccessful."
                        : "successful."));
                }

                sb.AppendLine("For more detailed errors, please check the EventLog.");
            }

            SendEmail(subject, sb.ToString());
        }

        private static void SendEmail(string subject, string messageBody)
        {
            var smtpHost = ConfigurationManager.AppSettings.Get("SMTPHost");
            var distrList = ConfigurationManager.AppSettings.Get("EmailDL");
            var fromAddress = ConfigurationManager.AppSettings.Get("EmailFromAddress");

            var smtpClient = new SmtpClient(smtpHost);
            var fromAddr = new MailAddress(fromAddress);
            var toAddr = new MailAddress(distrList);

            var message = new MailMessage(fromAddr, toAddr);
            message.Body = messageBody;
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.Subject = subject;
            message.SubjectEncoding = System.Text.Encoding.UTF8;

            try
            {
                smtpClient.Send(message);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(Constants.EventLogSource, "Exception in SendEmail(): " + ex.Message, EventLogEntryType.Error);
            }
        }

        private static void PerformCleanup()
        {
            var localWorkingDir = ConfigurationManager.AppSettings.Get("LocalWorkingDirectory");

            string[] fileNames = Directory.GetFiles(localWorkingDir, "*.*", SearchOption.TopDirectoryOnly);
            foreach (string fullFilePath in fileNames)
            {
                File.Delete(fullFilePath);
            }
        }
    }
}