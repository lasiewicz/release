﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CsvHelper.Configuration;
using LitleSFTPDataImport.ExtensionMethods;

namespace LitleSFTPDataImport.DataObjects
{
    public class LitleSessionReportRow
    {
        public Int64? SessionID { get; set; }
        public Int64? BatchID { get; set; }
        public DateTime? BatchPostDay { get; set; }
        public DateTime? TransactionProcessingTimestampGMT { get; set; }
        public DateTime? BatchCompletionTimestampGMT { get; set; }
        public string ReportingGroup { get; set; }
        public string Presenter { get; set; }
        public string Merchant { get; set; }
        public string MerchantID { get; set; }
        public Int64? LitlePaymentID { get; set; }
        public Int64? ParentLitlePaymentID { get; set; }
        public string MerchantOrderNumber { get; set; }
        public string CustomerID { get; set; }
        public string TxnType { get; set; }
        public string PurchaseCurrency { get; set; }
        public decimal? PurchaseAmt { get; set; }
        public string PaymentType { get; set; }
        public string Bin { get; set; }
        public string AccountSuffix { get; set; }
        public string ResponseReasonCode { get; set; }
        public string ResponseReasonMessage { get; set; }
        public string Avs { get; set; }
        public string FraudCheckSumResponse { get; set; }
        public string PayerID { get; set; }
        public string MerchantTransactionID { get; set; }
        public string Affiliate { get; set; }
        public string Campaign { get; set; }
        public string MerchantGroupingID { get; set; }
        public decimal? SalesTax { get; set; }
        public string FTPFilename { get; set; }

    }
    
}
