using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

using Matchnet;
using Matchnet.Purchase.ValueObjects;


namespace Spark.Credit.Client
{
	public class CreditDialog : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox grCredit;
		private System.Windows.Forms.Label lblAmount;
		private System.Windows.Forms.Label lblDuration;
		private System.Windows.Forms.TextBox txtAmount;
		private System.Windows.Forms.Label lblMaxAmount;
		private System.Windows.Forms.Label lblMaxDays;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.TextBox txtDays;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private MemberTran memberTran = null;
		private decimal maxAmount;
		private int maxMinutes;
		private decimal creditAmount;
		private System.Windows.Forms.ComboBox cboDurationType;
		private bool isValid;
		private int creditDuration;
		private DurationType creditInterval;

		public CreditDialog(MemberTran memberTran, bool isVoid)
		{
			InitializeComponent();
			this.memberTran = memberTran;
			this.Text = isVoid ? "Void Transaction" : "Credit Transaction";
			this.txtAmount.Enabled = !isVoid;
			Populate();
		}

		public decimal CreditAmount 
		{
			get 
			{
				return creditAmount;
			}
		}

		public int CreditDuration
		{
			get 
			{
				return creditDuration;
			}
		}

		public DurationType CreditInterval 
		{
			get 
			{
				return creditInterval;
			}
		}

		public bool IsValid 
		{
			get 
			{
				return isValid;
			}
		}

		private void Populate() 
		{
			PopulateDuration();
			PopulateMax();
		}

		private void PopulateDuration() 
		{
			cboDurationType.DataSource = getDurations();
			cboDurationType.SelectedIndexChanged += new EventHandler(cboDurationType_SelectedIndexChanged);
		}

		DurationItem[] getDurations() 
		{
			return new DurationItem[] {
									  new DurationItem(DurationType.Minute),
									  new DurationItem(DurationType.Hour),
									  new DurationItem(DurationType.Day),
									  new DurationItem(DurationType.Week),
									  new DurationItem(DurationType.Month),
									  new DurationItem(DurationType.Year) 
								  };
		}

		private void PopulateMax() 
		{
			try 
			{
				CreditMaximum creditMaximum = CreditServiceWrapper.GetService().GetCreditMaximum(memberTran.MemberTranID);

				if ( creditMaximum != null )
				{
					maxAmount = creditMaximum.MaxAmount;
					maxMinutes = creditMaximum.MaxMinutes;

					txtAmount.Text = System.Convert.ToString(maxAmount);
					txtDays.Text = System.Convert.ToString(maxMinutes);
					creditInterval = DurationType.Minute;

					lblMaxAmount.Text = txtAmount.Text + " max";
					lblMaxDays.Text = txtDays.Text + " max";
				}
				else
				{
					MessageBox.Show("Unable to retrieve credit information", "Oops!");
				}
			} 
			catch (Exception ex) 
			{
				MessageBox.Show("There was an error getting maximum amounts for credits.\r\n" + ex.ToString(), "Oops!");
			}
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(CreditDialog));
			this.grCredit = new System.Windows.Forms.GroupBox();
			this.cboDurationType = new System.Windows.Forms.ComboBox();
			this.txtDays = new System.Windows.Forms.TextBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.lblMaxDays = new System.Windows.Forms.Label();
			this.lblMaxAmount = new System.Windows.Forms.Label();
			this.txtAmount = new System.Windows.Forms.TextBox();
			this.lblDuration = new System.Windows.Forms.Label();
			this.lblAmount = new System.Windows.Forms.Label();
			this.grCredit.SuspendLayout();
			this.SuspendLayout();
			// 
			// grCredit
			// 
			this.grCredit.Controls.Add(this.cboDurationType);
			this.grCredit.Controls.Add(this.txtDays);
			this.grCredit.Controls.Add(this.btnCancel);
			this.grCredit.Controls.Add(this.btnOK);
			this.grCredit.Controls.Add(this.lblMaxDays);
			this.grCredit.Controls.Add(this.lblMaxAmount);
			this.grCredit.Controls.Add(this.txtAmount);
			this.grCredit.Controls.Add(this.lblDuration);
			this.grCredit.Controls.Add(this.lblAmount);
			this.grCredit.Location = new System.Drawing.Point(8, 8);
			this.grCredit.Name = "grCredit";
			this.grCredit.Size = new System.Drawing.Size(392, 96);
			this.grCredit.TabIndex = 0;
			this.grCredit.TabStop = false;
			// 
			// cboDurationType
			// 
			this.cboDurationType.Location = new System.Drawing.Point(168, 40);
			this.cboDurationType.Name = "cboDurationType";
			this.cboDurationType.Size = new System.Drawing.Size(88, 21);
			this.cboDurationType.TabIndex = 9;
			// 
			// txtDays
			// 
			this.txtDays.Location = new System.Drawing.Point(80, 40);
			this.txtDays.Name = "txtDays";
			this.txtDays.Size = new System.Drawing.Size(80, 20);
			this.txtDays.TabIndex = 8;
			this.txtDays.Text = "";
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(168, 64);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(88, 20);
			this.btnCancel.TabIndex = 7;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnOK
			// 
			this.btnOK.Location = new System.Drawing.Point(72, 64);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(88, 20);
			this.btnOK.TabIndex = 6;
			this.btnOK.Text = "OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// lblMaxDays
			// 
			this.lblMaxDays.Location = new System.Drawing.Point(264, 40);
			this.lblMaxDays.Name = "lblMaxDays";
			this.lblMaxDays.Size = new System.Drawing.Size(120, 16);
			this.lblMaxDays.TabIndex = 5;
			this.lblMaxDays.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			// 
			// lblMaxAmount
			// 
			this.lblMaxAmount.Location = new System.Drawing.Point(176, 16);
			this.lblMaxAmount.Name = "lblMaxAmount";
			this.lblMaxAmount.Size = new System.Drawing.Size(120, 16);
			this.lblMaxAmount.TabIndex = 4;
			this.lblMaxAmount.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			// 
			// txtAmount
			// 
			this.txtAmount.Location = new System.Drawing.Point(80, 16);
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Size = new System.Drawing.Size(88, 20);
			this.txtAmount.TabIndex = 2;
			this.txtAmount.Text = "";
			// 
			// lblDuration
			// 
			this.lblDuration.Location = new System.Drawing.Point(8, 40);
			this.lblDuration.Name = "lblDuration";
			this.lblDuration.Size = new System.Drawing.Size(64, 16);
			this.lblDuration.TabIndex = 1;
			this.lblDuration.Text = "Duration";
			this.lblDuration.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// lblAmount
			// 
			this.lblAmount.Location = new System.Drawing.Point(8, 16);
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Size = new System.Drawing.Size(64, 16);
			this.lblAmount.TabIndex = 0;
			this.lblAmount.Text = "Amount";
			this.lblAmount.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// CreditDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(410, 111);
			this.Controls.Add(this.grCredit);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "CreditDialog";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Credit Dialog";
			this.grCredit.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private bool CreditIsValid() 
		{
			//mCreditAmount = System.Convert.ToDecimal(txtAmount.Text);
			isValid = true;
			//return true;
			string message = "";
			decimal amount = 0;
			int duration = 0;
			int minutes = 0;

			if (txtAmount.Text.Trim().Length == 0) 
			{
				txtAmount.Text = "0";
			}
			if (txtDays.Text.Trim().Length == 0) 
			{
				txtDays.Text = "0";
			}

			try 
			{
				amount = System.Convert.ToDecimal(txtAmount.Text);
				duration = System.Convert.ToInt32(txtDays.Text);
			} 
			catch 
			{
				// number format exception
				isValid = false;
				message = "Amount and Days must be numeric values";
			}
			if (isValid) 
			{
				// check the maximums
				if (amount > maxAmount || amount < 0) 
				{
					isValid = false;
					message = "Amount cannot exceed the maximum and cannot be less than zero";
				}

				if (isValid) 
				{
					minutes = DurationUtil.GetMinutes(((DurationItem)cboDurationType.SelectedItem).DurationType, 
						duration);
					if ( minutes > maxMinutes || minutes < 0) 
					{
						isValid = false;
						message = "Duration cannot exceed the maximum and cannot be less than zero";
					}
				}
			}

			if (!isValid) 
			{
				MessageBox.Show(message, "Oops!");
			} 
			else 
			{
				creditDuration = duration;
				creditInterval = ((DurationItem)cboDurationType.SelectedItem).DurationType;
				creditAmount = amount;
			}
			return isValid;
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			if (CreditIsValid()) 
			{
				this.Close();				
			}
		}

		private void cboDurationType_SelectedIndexChanged(object sender, EventArgs e)
		{
			try 
			{
				DurationType selectedInterval = ((DurationItem)cboDurationType.SelectedItem).DurationType;
				int duration = DurationUtil.GetMinutes(selectedInterval, maxMinutes);
				txtDays.Text = System.Convert.ToString(duration);
				lblMaxDays.Text = duration + " max";
				creditInterval = selectedInterval;
			} 
			catch 
			{
				MessageBox.Show("Please ensure that only valid values are entered", "Oops!");
			}
		}
	}
}
