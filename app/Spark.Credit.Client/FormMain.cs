using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using Matchnet;

namespace Spark.Credit.Client
{
	public class FormMain : System.Windows.Forms.Form
	{
		private System.ComponentModel.Container components = null;
		private bool hasAccess = false;
		
		public static Int32 AdminMemberID = Constants.NULL_INT;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox FormMainMemberIDTextBox;
		private System.Windows.Forms.Button FormMainMemberLookupButton;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox FormMainSiteComboBox;
		private System.Windows.Forms.Button FormMainTransactionsButton;
		private System.Windows.Forms.Button FormMainPaymentAdjustButton;
		public System.Windows.Forms.Panel panelMain;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button LoginButton;
		private System.Windows.Forms.TextBox LoginPasswordTextBox;
		private System.Windows.Forms.TextBox LoginEmailAddressTextBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.TextBox MemberIDLookupInputText;
		private System.Windows.Forms.Label lblEmailAddress;
		private System.Windows.Forms.GroupBox grCredit;
		private System.Windows.Forms.ComboBox cboDurationType;
		private System.Windows.Forms.TextBox txtDays;
		private System.Windows.Forms.Label lblMaxDays;
		private System.Windows.Forms.Label lblMaxAmount;
		private System.Windows.Forms.TextBox txtAmount;
		private System.Windows.Forms.Label lblDuration;
		private System.Windows.Forms.Label lblAmount;
		private System.Windows.Forms.Button btnCreditCancel;
		private System.Windows.Forms.Button btnCreditOK;
		private Spark.Credit.Client.Login loginControl;
		private System.Windows.Forms.TabControl FormMainTransactionsTab;
		private Spark.Credit.Client.Main mainControl;
		private Spark.Credit.Client.MemberLookup memberLookupControl;
		private Spark.Credit.Client.MemberIDList memberIDListControl;
		private Spark.Credit.Client.Credit creditControl;
		public static string selectedPrivateLabel = Constants.NULL_STRING;
		private System.Timers.Timer _timer;

		public FormMain()
		{
			object launchURL = AppDomain.CurrentDomain.GetData("APP_LAUNCH_URL");
			if (launchURL != null)
			{
				string queryString = launchURL.ToString();
				Uri uri = new Uri(launchURL.ToString());
				Int32 i = queryString.IndexOf("?");
				if (i > -1)
				{
					queryString = queryString.Substring(i + 1);
					string[] nameVal = queryString.Split('=');
					CreditServiceWrapper.GetService().CookieContainer.Add(new Uri(uri.Scheme + "://" + uri.Host), new System.Net.Cookie(nameVal[0], "sid=" + nameVal[1]));
				}
			}

			InitializeComponent();

			this.Width = mainControl.Width + 5;
			this.Height = mainControl.Height + 27;

			mainControl.Top = 0;
			mainControl.Left = 0;

			//center controls on form
			loginControl.Top = (this.Height / 2) - (loginControl.Height / 2);
			loginControl.Left = (this.Width / 2) - (loginControl.Width / 2);

			memberLookupControl.Top = (this.Height / 2) - (memberLookupControl.Height / 2);
			memberLookupControl.Left = (this.Width / 2) - (memberLookupControl.Width / 2);

			memberIDListControl.Top = (this.Height / 2) - (memberIDListControl.Height / 2);
			memberIDListControl.Left = (this.Width / 2) - (memberIDListControl.Width / 2);

			creditControl.Top = (this.Height / 2) - (creditControl.Height / 2);
			creditControl.Left = (this.Width / 2) - (creditControl.Width / 2);

			loginControl.Authenticated += new Spark.Credit.Client.Login.AuthenticationEventHandler(loginControl_Authenticated);

			mainControl.MemberLookupRequested += new Spark.Credit.Client.Main.MemberLookupEventHandler(mainControl_MemberLookupRequested);
			mainControl.CreditRequested += new Spark.Credit.Client.Main.CreditRequestEventHandler(mainControl_CreditRequested);

			memberLookupControl.MemberLookupCompleted += new Spark.Credit.Client.MemberLookup.MemberLookupCompleteEventHandler(memberLookupControl_MemberLookupCompleted);
			memberLookupControl.MemberLookupCancelled += new Spark.Credit.Client.MemberLookup.MemberLookupCancelEventHandler(memberLookupControl_MemberLookupCancelled);

			memberIDListControl.MemberIDChosen += new Spark.Credit.Client.MemberIDList.MemberIDChoiceEventHandler(memberIDListControl_MemberIDChosen);
			memberIDListControl.MemberIDChoiceCancelled += new Spark.Credit.Client.MemberIDList.MemberIDChoiceCancelEventHandler(memberIDListControl_MemberIDChoiceCancelled);

			creditControl.CreditCompleted += new Spark.Credit.Client.Credit.CreditCompleteEventHandler(creditControl_CreditCompleted);
			creditControl.CreditCancelled += new Spark.Credit.Client.Credit.CreditCancelEventHandler(creditControl_CreditCancelled);

			//keep user session alive
			_timer = new System.Timers.Timer();
			_timer.Interval = 60000;
			_timer.Enabled = false;
			_timer.AutoReset = true;
			_timer.Elapsed += new System.Timers.ElapsedEventHandler(_timer_Elapsed);
			_timer.Start();
		}


		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		[STAThread]
		static void Main() 
		{
			System.Windows.Forms.Application.Run(new FormMain());
		}


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(FormMain));
			this.panelMain = new System.Windows.Forms.Panel();
			this.FormMainPaymentAdjustButton = new System.Windows.Forms.Button();
			this.FormMainTransactionsButton = new System.Windows.Forms.Button();
			this.FormMainSiteComboBox = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.FormMainMemberLookupButton = new System.Windows.Forms.Button();
			this.FormMainMemberIDTextBox = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.FormMainTransactionsTab = new System.Windows.Forms.TabControl();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.LoginButton = new System.Windows.Forms.Button();
			this.LoginPasswordTextBox = new System.Windows.Forms.TextBox();
			this.LoginEmailAddressTextBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.MemberIDLookupInputText = new System.Windows.Forms.TextBox();
			this.lblEmailAddress = new System.Windows.Forms.Label();
			this.grCredit = new System.Windows.Forms.GroupBox();
			this.cboDurationType = new System.Windows.Forms.ComboBox();
			this.txtDays = new System.Windows.Forms.TextBox();
			this.btnCreditCancel = new System.Windows.Forms.Button();
			this.btnCreditOK = new System.Windows.Forms.Button();
			this.lblMaxDays = new System.Windows.Forms.Label();
			this.lblMaxAmount = new System.Windows.Forms.Label();
			this.txtAmount = new System.Windows.Forms.TextBox();
			this.lblDuration = new System.Windows.Forms.Label();
			this.lblAmount = new System.Windows.Forms.Label();
			this.loginControl = new Spark.Credit.Client.Login();
			this.mainControl = new Spark.Credit.Client.Main();
			this.memberLookupControl = new Spark.Credit.Client.MemberLookup();
			this.memberIDListControl = new Spark.Credit.Client.MemberIDList();
			this.creditControl = new Spark.Credit.Client.Credit();
			this.panelMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelMain
			// 
			this.panelMain.Controls.Add(this.FormMainPaymentAdjustButton);
			this.panelMain.Controls.Add(this.FormMainTransactionsButton);
			this.panelMain.Controls.Add(this.FormMainSiteComboBox);
			this.panelMain.Controls.Add(this.label2);
			this.panelMain.Controls.Add(this.FormMainMemberLookupButton);
			this.panelMain.Controls.Add(this.FormMainMemberIDTextBox);
			this.panelMain.Controls.Add(this.label1);
			this.panelMain.Controls.Add(this.FormMainTransactionsTab);
			this.panelMain.Location = new System.Drawing.Point(880, 704);
			this.panelMain.Name = "panelMain";
			this.panelMain.Size = new System.Drawing.Size(216, 88);
			this.panelMain.TabIndex = 9;
			this.panelMain.Visible = false;
			// 
			// FormMainPaymentAdjustButton
			// 
			this.FormMainPaymentAdjustButton.Location = new System.Drawing.Point(0, 0);
			this.FormMainPaymentAdjustButton.Name = "FormMainPaymentAdjustButton";
			this.FormMainPaymentAdjustButton.TabIndex = 0;
			// 
			// FormMainTransactionsButton
			// 
			this.FormMainTransactionsButton.Location = new System.Drawing.Point(0, 0);
			this.FormMainTransactionsButton.Name = "FormMainTransactionsButton";
			this.FormMainTransactionsButton.TabIndex = 1;
			// 
			// FormMainSiteComboBox
			// 
			this.FormMainSiteComboBox.Location = new System.Drawing.Point(0, 0);
			this.FormMainSiteComboBox.Name = "FormMainSiteComboBox";
			this.FormMainSiteComboBox.Size = new System.Drawing.Size(121, 21);
			this.FormMainSiteComboBox.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(0, 0);
			this.label2.Name = "label2";
			this.label2.TabIndex = 3;
			// 
			// FormMainMemberLookupButton
			// 
			this.FormMainMemberLookupButton.Location = new System.Drawing.Point(0, 0);
			this.FormMainMemberLookupButton.Name = "FormMainMemberLookupButton";
			this.FormMainMemberLookupButton.TabIndex = 4;
			// 
			// FormMainMemberIDTextBox
			// 
			this.FormMainMemberIDTextBox.Location = new System.Drawing.Point(0, 0);
			this.FormMainMemberIDTextBox.Name = "FormMainMemberIDTextBox";
			this.FormMainMemberIDTextBox.TabIndex = 5;
			this.FormMainMemberIDTextBox.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.TabIndex = 6;
			// 
			// FormMainTransactionsTab
			// 
			this.FormMainTransactionsTab.Location = new System.Drawing.Point(0, 0);
			this.FormMainTransactionsTab.Name = "FormMainTransactionsTab";
			this.FormMainTransactionsTab.SelectedIndex = 0;
			this.FormMainTransactionsTab.TabIndex = 7;
			// 
			// groupBox1
			// 
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			// 
			// LoginButton
			// 
			this.LoginButton.Location = new System.Drawing.Point(0, 0);
			this.LoginButton.Name = "LoginButton";
			this.LoginButton.TabIndex = 0;
			// 
			// LoginPasswordTextBox
			// 
			this.LoginPasswordTextBox.Location = new System.Drawing.Point(0, 0);
			this.LoginPasswordTextBox.Name = "LoginPasswordTextBox";
			this.LoginPasswordTextBox.TabIndex = 0;
			this.LoginPasswordTextBox.Text = "";
			// 
			// LoginEmailAddressTextBox
			// 
			this.LoginEmailAddressTextBox.Location = new System.Drawing.Point(0, 0);
			this.LoginEmailAddressTextBox.Name = "LoginEmailAddressTextBox";
			this.LoginEmailAddressTextBox.TabIndex = 0;
			this.LoginEmailAddressTextBox.Text = "";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(0, 0);
			this.label3.Name = "label3";
			this.label3.TabIndex = 0;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(0, 0);
			this.label4.Name = "label4";
			this.label4.TabIndex = 0;
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(0, 0);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 0;
			// 
			// btnOK
			// 
			this.btnOK.Location = new System.Drawing.Point(0, 0);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 0;
			// 
			// MemberIDLookupInputText
			// 
			this.MemberIDLookupInputText.Location = new System.Drawing.Point(0, 0);
			this.MemberIDLookupInputText.Name = "MemberIDLookupInputText";
			this.MemberIDLookupInputText.TabIndex = 0;
			this.MemberIDLookupInputText.Text = "";
			// 
			// lblEmailAddress
			// 
			this.lblEmailAddress.Location = new System.Drawing.Point(0, 0);
			this.lblEmailAddress.Name = "lblEmailAddress";
			this.lblEmailAddress.TabIndex = 0;
			// 
			// grCredit
			// 
			this.grCredit.Location = new System.Drawing.Point(0, 0);
			this.grCredit.Name = "grCredit";
			this.grCredit.TabIndex = 0;
			this.grCredit.TabStop = false;
			// 
			// cboDurationType
			// 
			this.cboDurationType.Location = new System.Drawing.Point(0, 0);
			this.cboDurationType.Name = "cboDurationType";
			this.cboDurationType.TabIndex = 0;
			// 
			// txtDays
			// 
			this.txtDays.Location = new System.Drawing.Point(0, 0);
			this.txtDays.Name = "txtDays";
			this.txtDays.TabIndex = 0;
			this.txtDays.Text = "";
			// 
			// btnCreditCancel
			// 
			this.btnCreditCancel.Location = new System.Drawing.Point(0, 0);
			this.btnCreditCancel.Name = "btnCreditCancel";
			this.btnCreditCancel.TabIndex = 0;
			// 
			// btnCreditOK
			// 
			this.btnCreditOK.Location = new System.Drawing.Point(0, 0);
			this.btnCreditOK.Name = "btnCreditOK";
			this.btnCreditOK.TabIndex = 0;
			// 
			// lblMaxDays
			// 
			this.lblMaxDays.Location = new System.Drawing.Point(0, 0);
			this.lblMaxDays.Name = "lblMaxDays";
			this.lblMaxDays.TabIndex = 0;
			// 
			// lblMaxAmount
			// 
			this.lblMaxAmount.Location = new System.Drawing.Point(0, 0);
			this.lblMaxAmount.Name = "lblMaxAmount";
			this.lblMaxAmount.TabIndex = 0;
			// 
			// txtAmount
			// 
			this.txtAmount.Location = new System.Drawing.Point(0, 0);
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.TabIndex = 0;
			this.txtAmount.Text = "";
			// 
			// lblDuration
			// 
			this.lblDuration.Location = new System.Drawing.Point(0, 0);
			this.lblDuration.Name = "lblDuration";
			this.lblDuration.TabIndex = 0;
			// 
			// lblAmount
			// 
			this.lblAmount.Location = new System.Drawing.Point(0, 0);
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.TabIndex = 0;
			// 
			// loginControl
			// 
			this.loginControl.Location = new System.Drawing.Point(736, 384);
			this.loginControl.Name = "loginControl";
			this.loginControl.Size = new System.Drawing.Size(336, 104);
			this.loginControl.TabIndex = 3;
			// 
			// mainControl
			// 
			this.mainControl.Location = new System.Drawing.Point(0, 0);
			this.mainControl.MemberID = -2147483647;
			this.mainControl.Name = "mainControl";
			this.mainControl.Size = new System.Drawing.Size(736, 536);
			this.mainControl.TabIndex = 11;
			this.mainControl.Visible = false;
			// 
			// memberLookupControl
			// 
			this.memberLookupControl.Location = new System.Drawing.Point(736, 288);
			this.memberLookupControl.Name = "memberLookupControl";
			this.memberLookupControl.Size = new System.Drawing.Size(434, 82);
			this.memberLookupControl.TabIndex = 2;
			this.memberLookupControl.Visible = false;
			// 
			// memberIDListControl
			// 
			this.memberIDListControl.Location = new System.Drawing.Point(744, 120);
			this.memberIDListControl.Name = "memberIDListControl";
			this.memberIDListControl.Size = new System.Drawing.Size(368, 160);
			this.memberIDListControl.TabIndex = 1;
			this.memberIDListControl.Visible = false;
			// 
			// creditControl
			// 
			this.creditControl.Location = new System.Drawing.Point(736, 0);
			this.creditControl.Name = "creditControl";
			this.creditControl.Size = new System.Drawing.Size(408, 112);
			this.creditControl.TabIndex = 0;
			this.creditControl.Visible = false;
			// 
			// FormMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(1170, 759);
			this.Controls.Add(this.creditControl);
			this.Controls.Add(this.memberIDListControl);
			this.Controls.Add(this.memberLookupControl);
			this.Controls.Add(this.loginControl);
			this.Controls.Add(this.panelMain);
			this.Controls.Add(this.mainControl);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "FormMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Spark.Credit.Client";
			this.panelMain.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private bool IsValidInput() 
		{
			if ( FormMainMemberIDTextBox.Text.Trim().Length == 0 ) 
			{
				MessageBox.Show("MemberID Required");
				return false;				
			}

			if ( FormMainSiteComboBox.SelectedItem == null ) 
			{
				MessageBox.Show("PrivateLabel Required");
				return false;
			}

			try  
			{
				int memberID = System.Convert.ToInt32( FormMainMemberIDTextBox.Text );
			} 
			catch 
			{
				MessageBox.Show("Member ID must be a number");
				return false;
			}

			return true;
		}


		private void FormMain_Load(object sender, System.EventArgs e)
		{
			if ( ! hasAccess ) 
			{
				System.Windows.Forms.Application.Exit();
			}
		}


		private void loginControl_Authenticated()
		{
			this.Cursor = Cursors.WaitCursor;
			mainControl.PopulateSites();
			loginControl.Visible = false;
			AdminMemberID = loginControl.AdminMemberID;
			mainControl.Visible = true;
			this.Cursor = Cursors.Default;
		}


		private void mainControl_MemberLookupRequested()
		{
			mainControl.Visible = false;
			memberLookupControl.Visible = true;
		}


		private void memberLookupControl_MemberLookupCompleted()
		{
			if (memberLookupControl.MemberIDs.Length == 0)
			{
				MessageBox.Show("Member not found.", "Sorry folks, the park is closed.");
			}
			else if (memberLookupControl.MemberIDs.Length == 1)
			{
				mainControl.MemberID = memberLookupControl.MemberIDs[0];
				memberLookupControl.Visible = false;
				mainControl.Visible = true;
			}
			else if (memberLookupControl.MemberIDs.Length > 1)
			{
				memberIDListControl.MemberIDs = memberLookupControl.MemberIDs;
				memberLookupControl.Visible = false;
				memberIDListControl.Visible = true;
			}
		}


		private void memberLookupControl_MemberLookupCancelled()
		{
			memberLookupControl.Visible = false;
			mainControl.MemberID = Constants.NULL_INT;
			mainControl.Visible = true;
		}


		private void memberIDListControl_MemberIDChosen()
		{
			memberIDListControl.Visible = false;
			mainControl.MemberID = memberIDListControl.SelectedMemberID;
			mainControl.Visible = true;
		}

		
		private void memberIDListControl_MemberIDChoiceCancelled()
		{
			memberIDListControl.Visible = false;
			memberLookupControl.Visible = true;
		}


		private void mainControl_CreditRequested(MemberTran memberTran,
			Int32 merchantID,
			bool isVoid)
		{
			this.Cursor = Cursors.WaitCursor;
			mainControl.Visible = false;
			creditControl.Initialize(memberTran, merchantID,
				isVoid);
			creditControl.Visible = true;
			this.Cursor = Cursors.Default;
		}


		private void creditControl_CreditCompleted()
		{
			creditControl.Visible = false;
			mainControl.Visible = true;
			mainControl.LoadTransactions();
		}


		private void creditControl_CreditCancelled()
		{
			creditControl.Visible = false;
			mainControl.Visible = true;
		}


		private void _timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			if (AdminMemberID > 0)
			{
				System.Diagnostics.Trace.WriteLine("___timer_Elapsed");
				CreditServiceWrapper.GetService().IsAuthenticated();
			}
		}
	}
}
