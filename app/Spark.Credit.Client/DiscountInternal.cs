using System;
using System.Data;

using Matchnet;
/*
using Matchnet.Credit.ServiceAdapters;
using Matchnet.Credit.ValueObjects;
*/
namespace Spark.Credit.Client
{
	public class DiscountInternal
	{
		public const int DURATION_TYPE_MINUTE = 1;
		public const int DURATION_TYPE_HOUR = 2;
		public const int DURATION_TYPE_DAY = 3;
		public const int DURATION_TYPE_WEEK = 4;
		public const int DURATION_TYPE_MONTH = 5;
		public const int DURATION_TYPE_YEAR = 6;

		public const int DISCOUNT_TYPE_PERCENTAGE = 1;
		public const int DISCOUNT_TYPE_MONEY = 2;

		private int discountID;
		private int discountTypeID;
		private int durationTypeID;
		private int duration;
		private decimal amount;
		private string description;

		public int DiscountID 
		{
			get 
			{
				return discountID;
			}
		}

		public int DiscountTypeID 
		{
			get 
			{
				return discountTypeID;
			}
		}	

		public int DurationTypeID 
		{
			get 
			{
				return durationTypeID;
			}
		}

		public int Duration 
		{
			get 
			{
				return duration;
			}
		}

		public decimal Amount 
		{
			get 
			{
				return amount;
			}
		}

		public string Description 
		{
			get 
			{
				return description;
			}
		}

		public void Populate(int discountID) 
		{
			this.discountID = discountID;
			
			try 
			{
				Discount discount = CreditServiceWrapper.GetService().GetDiscount(discountID);

				if (discount != null) 
				{
					discountTypeID = (Int32)discount.DiscountType;
					durationTypeID = (Int32)discount.DurationType;
					duration = discount.Duration;
					amount = discount.Amount;
					PopulateDescription();
				} 
				else 
				{
					description = "Unknown Discount";
				}
			} 
			catch 
			{
				description = "Unknown Discount";
			}
		}

		private void PopulateDescription() 
		{
			if (amount == 0 && duration == 0) 
			{
				description = "No Discount";
			} 
			else 
			{
				if (discountTypeID == DISCOUNT_TYPE_MONEY) 
				{
					description = string.Format("{0:c}", amount) + " off for " + duration + " " + FormatDuration();
				} 
				else 
				{
					description = amount + " percent off for " + duration + " " + FormatDuration();					
				}
			}
		}

		private string FormatDuration() 
		{
			string retVal = "Unknown";
			switch (durationTypeID) 
			{
				case DURATION_TYPE_MINUTE:
					retVal = "minute";
					break;
				case DURATION_TYPE_HOUR:
					retVal = "hour";
					break;
				case DURATION_TYPE_DAY:
					retVal = "day";
					break;
				case DURATION_TYPE_WEEK:
					retVal = "week";
					break;
				case DURATION_TYPE_MONTH:
					retVal = "month";
					break;
				case DURATION_TYPE_YEAR:
					retVal = "year";
					break;
			}
			if (duration > 1 || duration < 1) 
			{
				retVal = retVal + "s";
			}
			return retVal;
		}

		public override string ToString()
		{
			return description;
		}
	}
}
