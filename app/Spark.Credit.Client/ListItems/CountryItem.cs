using System;

namespace Spark.Credit.Client
{
	public class CountryItem : Country
	{
		public CountryItem(Country country)
		{
			base.Description = country.Description;
			base.RegionID = country.RegionID;
		}

		public override string ToString()
		{
			return base.Description;
		}

	}
}
