using System;

namespace Spark.Credit.Client
{
	public class DurationItem
	{
		private DurationType _durationType;

		public DurationItem(DurationType durationType)
		{
			_durationType = durationType;
		}

		public DurationType DurationType
		{
			get
			{
				return _durationType;
			}
		}

		public override string ToString()
		{
			return _durationType.ToString();
		}
	}
}
