using System;

namespace Spark.Credit.Client
{
	public class SiteItem : Site
	{
		public SiteItem(Site site)
		{
			base.SiteID = site.SiteID;
			base.SiteName = site.SiteName;
		}

		public override string ToString()
		{
			return base.SiteName;
		}
	}
}
