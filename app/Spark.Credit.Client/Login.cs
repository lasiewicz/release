using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

using Matchnet;

namespace Spark.Credit.Client
{
	public class Login : System.Windows.Forms.UserControl
	{
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button LoginButton;
		private System.Windows.Forms.TextBox LoginPasswordTextBox;
		private System.Windows.Forms.TextBox LoginEmailAddressTextBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;

		private Int32 _adminMemberID = Constants.NULL_INT;

		public delegate void AuthenticationEventHandler();
		public event AuthenticationEventHandler Authenticated;

		public Login()
		{
			InitializeComponent();
			this.VisibleChanged += new EventHandler(Login_VisibleChanged);
		}


		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.LoginButton = new System.Windows.Forms.Button();
			this.LoginPasswordTextBox = new System.Windows.Forms.TextBox();
			this.LoginEmailAddressTextBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.LoginButton);
			this.groupBox1.Controls.Add(this.LoginPasswordTextBox);
			this.groupBox1.Controls.Add(this.LoginEmailAddressTextBox);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Location = new System.Drawing.Point(8, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(320, 104);
			this.groupBox1.TabIndex = 7;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Login";
			// 
			// LoginButton
			// 
			this.LoginButton.Location = new System.Drawing.Point(96, 72);
			this.LoginButton.Name = "LoginButton";
			this.LoginButton.Size = new System.Drawing.Size(88, 24);
			this.LoginButton.TabIndex = 4;
			this.LoginButton.Text = "Login";
			this.LoginButton.Click += new System.EventHandler(this.LoginButton_Click);
			// 
			// LoginPasswordTextBox
			// 
			this.LoginPasswordTextBox.Location = new System.Drawing.Point(96, 48);
			this.LoginPasswordTextBox.Name = "LoginPasswordTextBox";
			this.LoginPasswordTextBox.PasswordChar = '*';
			this.LoginPasswordTextBox.Size = new System.Drawing.Size(216, 20);
			this.LoginPasswordTextBox.TabIndex = 3;
			//this.LoginPasswordTextBox.Text = "password";
			// 
			// LoginEmailAddressTextBox
			// 
			this.LoginEmailAddressTextBox.Location = new System.Drawing.Point(96, 24);
			this.LoginEmailAddressTextBox.Name = "LoginEmailAddressTextBox";
			this.LoginEmailAddressTextBox.Size = new System.Drawing.Size(216, 20);
			this.LoginEmailAddressTextBox.TabIndex = 2;
			//this.LoginEmailAddressTextBox.Text = "gpeterson@matchnet.com";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 48);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(80, 16);
			this.label3.TabIndex = 1;
			this.label3.Text = "Password";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 24);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(80, 16);
			this.label4.TabIndex = 0;
			this.label4.Text = "EmailAddress";
			// 
			// Login
			// 
			this.Controls.Add(this.groupBox1);
			this.Name = "Login";
			this.Size = new System.Drawing.Size(336, 104);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void LoginButton_Click(object sender, System.EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;

			if ( LoginEmailAddressTextBox.Text == "" || LoginPasswordTextBox.Text == "" ) 
			{
				MessageBox.Show("Fill in both Email Address and Password", "Oops!");
				return;
			}

			AuthenticationResult authenticationResult = CreditServiceWrapper.GetService().Authenticate(LoginEmailAddressTextBox.Text, LoginPasswordTextBox.Text);

			if (authenticationResult.MemberID == Constants.NULL_INT)
			{
				MessageBox.Show("Unknown Username or Password", "Oops!");
			}
			else if (!authenticationResult.HasCreditPrivilege)
			{
				MessageBox.Show("You currently do not have permission to use this tool.", "Oops!");
			}
			else
			{
				_adminMemberID = authenticationResult.MemberID;
				Authenticated();
			}

			this.Cursor = Cursors.Default;
		}


		public Int32 AdminMemberID
		{
			get
			{
				return _adminMemberID;
			}
		}

		private void Login_VisibleChanged(object sender, EventArgs e)
		{
			if (this.Visible)
			{
				Int32 memberID = CreditServiceWrapper.GetService().IsAuthenticated();

				if (memberID > 0)
				{
					_adminMemberID = memberID;
					Authenticated();
				}
			}
		}
	}
}
