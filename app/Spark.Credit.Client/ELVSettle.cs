using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Spark.Credit.Application
{
	public class ELVSettle : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label lblInfo;
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Button btnApproved;
		private System.Windows.Forms.Button btnFailed;

		private bool approved;

		public bool Approved 
		{
			get 
			{
				return approved;
			}
		}

		public ELVSettle()
		{
			InitializeComponent();
			InitializeMessage();
		}

		private void InitializeMessage() 
		{
			lblInfo.Text = "If the ELV Transaction was approved, click on Approved. Otherwise, click on Failed. If you accidentally hit the wrong button, please contact App Dev so that we can reverse the action.";
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ELVSettle));
			this.lblInfo = new System.Windows.Forms.Label();
			this.btnApproved = new System.Windows.Forms.Button();
			this.btnFailed = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lblInfo
			// 
			this.lblInfo.Location = new System.Drawing.Point(8, 8);
			this.lblInfo.Name = "lblInfo";
			this.lblInfo.Size = new System.Drawing.Size(312, 56);
			this.lblInfo.TabIndex = 0;
			this.lblInfo.Text = "lblInfo";
			// 
			// btnApproved
			// 
			this.btnApproved.Location = new System.Drawing.Point(32, 72);
			this.btnApproved.Name = "btnApproved";
			this.btnApproved.Size = new System.Drawing.Size(128, 24);
			this.btnApproved.TabIndex = 1;
			this.btnApproved.Text = "Approved";
			this.btnApproved.Click += new System.EventHandler(this.btnApproved_Click);
			// 
			// btnFailed
			// 
			this.btnFailed.Location = new System.Drawing.Point(168, 72);
			this.btnFailed.Name = "btnFailed";
			this.btnFailed.Size = new System.Drawing.Size(128, 24);
			this.btnFailed.TabIndex = 2;
			this.btnFailed.Text = "Failed";
			this.btnFailed.Click += new System.EventHandler(this.btnFailed_Click);
			// 
			// ELVSettle
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(322, 111);
			this.Controls.Add(this.btnFailed);
			this.Controls.Add(this.btnApproved);
			this.Controls.Add(this.lblInfo);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ELVSettle";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "ELV Settle";
			this.ResumeLayout(false);

		}
		#endregion

		private void btnFailed_Click(object sender, System.EventArgs e)
		{
			approved = false;
			this.Close();
		}

		private void btnApproved_Click(object sender, System.EventArgs e)
		{
			approved = true;
			this.Close();
		}
	}
}
