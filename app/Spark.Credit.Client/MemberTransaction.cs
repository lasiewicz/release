using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

using Matchnet;

namespace Spark.Credit.Client
{
	public class MemberTransaction : UserControl
	{
		private GroupBox grpTransactionDetail;
		private TabPage tpProviderLog;
		private TabPage tpPaymentInformation;
		private TabControl tcDetails;
		private Label lblMemberTranID;
		private Label lblReferenceID;
		private Label lblTransactionType;
		private Label lblAdminMemberID;
		private Label lblAmount;
		private Label lblCurrency;
		private Label lblPlan;
		private Label lblDiscount;
		private Label lblDuration;
		private Label lblResponse;
		private TextBox txtTransactionType;
		private TextBox txtAmount;
		private TextBox txtCurrency;
		private TextBox txtPlan;
		private TextBox txtDuration;
		private TextBox txtResponse;
		private TextBox txtMemberTranID;
		private TextBox txtReferenceID;
		private TextBox txtAdminMemberID;
		private TextBox txtDiscount;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private ListView lvProviderLog;
		private ColumnHeader chName;
		private ColumnHeader chValue;
		private ListView lvPaymentInformation;
		private ColumnHeader columnHeader1;
		private ColumnHeader columnHeader2;
		private Button btnCredit;
		private TabPage tpErrorLog;
		private Label lblAttempts;
		private TextBox txtAttempts;
		private Label lblDetail;
        private TextBox txtDetail;
		private Button btnChargeback;

		private MemberTran memberTran = null;
		private int merchantID = Constants.NULL_INT;
        
        // Added for new void/credit resolution. 20071024- RB.
        private const string BATCH_DATE_FORMAT = "{0}/{1}/{2} 20:00:00";
        private const int BATCH_PROCESS_HOUR = 20; // 24hr format 20= 8:00 PM

		public delegate void CreditRequestEventHandler(MemberTran memberTran,
			Int32 merchantID,
			bool isVoid);
		public event  CreditRequestEventHandler CreditRequested;

		public MemberTransaction()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		public void SizeToHost(Control host) 
		{
			this.Height = host.Height - 20;
			this.Width = host.Width;
			grpTransactionDetail.Width = this.Width - 20;
			tcDetails.Width = this.Width - 20;
			tcDetails.Height = this.Height - tcDetails.Top;
			lvProviderLog.Width = tcDetails.Width - 20;
			lvProviderLog.Height = tcDetails.Height - 40;
			lvPaymentInformation.Width = lvProviderLog.Width;
			lvPaymentInformation.Height = lvProviderLog.Height;
		}


		public void Populate(MemberTran mt) 
		{
			try 
			{
				memberTran = mt;
				
				txtMemberTranID.Text = mt.MemberTranID.ToString();
				
				if (mt.ReferenceMemberTranID != Constants.NULL_INT) 
				{
					txtReferenceID.Text = Convert.ToString(mt.ReferenceMemberTranID);
				}
				if (mt.AdminMemberID != Constants.NULL_INT) 
				{
					txtAdminMemberID.Text = Convert.ToString(mt.AdminMemberID);
				}

				DiscountInternal discountInternal = new DiscountInternal();
				discountInternal.Populate(mt.DiscountID);
				txtDiscount.Text = discountInternal.Description;

				txtTransactionType.Text = mt.TranType.ToString();
				txtAmount.Text = Convert.ToString(mt.Amount);
				txtCurrency.Text = mt.CurrencyType.ToString();
				txtDuration.Text = FormatDuration(DurationUtil.GetMinutes(mt.DurationType, mt.Duration));
				txtPlan.Text = CreditServiceWrapper.GetService().GetPlanDescription(mt.PlanID);
	
				if ( mt.MemberTranStatus == MemberTranStatus.Success ) 
				{
					txtResponse.Text = "Successful Transaction";
				} 
				else 
				{
					txtResponse.Text = mt.ResourceConstant;
				}

				PopulateProviderLog();
				if (mt.Amount != 0)
				{
					PopulatePaymentInformation();
				}

				ShowCredit();
			} 
			catch (Exception ex) 
			{
				MessageBox.Show("There was an error populating the MemberTran.\r\n" + ex.ToString(), "Oops!");
			}
		}


		private void ShowCredit() 
		{
			if ( merchantID == Constants.NULL_INT ) 
			{
				int siteID = memberTran.SiteID == 1934 ? 1 : memberTran.SiteID;
				merchantID = 6000 + siteID;

				if (merchantID == 6004) {
					merchantID = 6003;
				}
			}

			if (
					memberTran.MemberTranStatus == MemberTranStatus.Success  &&
					(
						memberTran.TranType == TranType.InitialBuy ||
						memberTran.TranType == TranType.Renewal 
					) 
				) 
			{
				btnCredit.Visible = true;
			}
		}

		private void PopulatePaymentInformation()
		{
			if (memberTran.MemberPaymentID != Constants.NULL_INT)
			{
				NameValue[] paymentAttributes = CreditServiceWrapper.GetService().GetMemberPayment(memberTran.MemberID,
					memberTran.SiteID,
					memberTran.MemberPaymentID);
			
				foreach (NameValue paymentAttribute in paymentAttributes)
				{
					ListViewItem item = lvPaymentInformation.Items.Add(paymentAttribute.Name);
					item.SubItems.Add(paymentAttribute.Value);
				}

				if (memberTran.Amount != 0)
				{
					btnChargeback.Visible = true;
				}
			}
		}

		private void PopulateProviderLog()
		{
			NameValue[] nameValues = CreditServiceWrapper.GetService().GetChargeLog(memberTran.MemberID, memberTran.MemberTranID);
			
			if (nameValues != null)
			{
				foreach (NameValue nameValue in nameValues)
				{
					ListViewItem item = lvProviderLog.Items.Add(nameValue.Name);
					item.SubItems.Add(nameValue.Value);
				}
			}
		}
	
		private string FormatDuration(int minutes) 
		{
			if (minutes == 0) 
			{
				return "0";
			}
			
			try 
			{
				return Convert.ToString((minutes / 60) / 24) + " days";
			} 
			catch 
			{
				return "Unknown";				
			}
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.grpTransactionDetail = new System.Windows.Forms.GroupBox();
            this.btnChargeback = new System.Windows.Forms.Button();
            this.btnCredit = new System.Windows.Forms.Button();
            this.txtDiscount = new System.Windows.Forms.TextBox();
            this.txtAdminMemberID = new System.Windows.Forms.TextBox();
            this.txtReferenceID = new System.Windows.Forms.TextBox();
            this.txtMemberTranID = new System.Windows.Forms.TextBox();
            this.txtResponse = new System.Windows.Forms.TextBox();
            this.txtDuration = new System.Windows.Forms.TextBox();
            this.txtPlan = new System.Windows.Forms.TextBox();
            this.txtCurrency = new System.Windows.Forms.TextBox();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.txtTransactionType = new System.Windows.Forms.TextBox();
            this.lblResponse = new System.Windows.Forms.Label();
            this.lblDuration = new System.Windows.Forms.Label();
            this.lblDiscount = new System.Windows.Forms.Label();
            this.lblPlan = new System.Windows.Forms.Label();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.lblAdminMemberID = new System.Windows.Forms.Label();
            this.lblTransactionType = new System.Windows.Forms.Label();
            this.lblReferenceID = new System.Windows.Forms.Label();
            this.lblMemberTranID = new System.Windows.Forms.Label();
            this.tcDetails = new System.Windows.Forms.TabControl();
            this.tpProviderLog = new System.Windows.Forms.TabPage();
            this.lvProviderLog = new System.Windows.Forms.ListView();
            this.chName = new System.Windows.Forms.ColumnHeader();
            this.chValue = new System.Windows.Forms.ColumnHeader();
            this.tpPaymentInformation = new System.Windows.Forms.TabPage();
            this.lvPaymentInformation = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.tpErrorLog = new System.Windows.Forms.TabPage();
            this.txtDetail = new System.Windows.Forms.TextBox();
            this.lblDetail = new System.Windows.Forms.Label();
            this.txtAttempts = new System.Windows.Forms.TextBox();
            this.lblAttempts = new System.Windows.Forms.Label();
            this.grpTransactionDetail.SuspendLayout();
            this.tcDetails.SuspendLayout();
            this.tpProviderLog.SuspendLayout();
            this.tpPaymentInformation.SuspendLayout();
            this.tpErrorLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpTransactionDetail
            // 
            this.grpTransactionDetail.Controls.Add(this.btnChargeback);
            this.grpTransactionDetail.Controls.Add(this.btnCredit);
            this.grpTransactionDetail.Controls.Add(this.txtDiscount);
            this.grpTransactionDetail.Controls.Add(this.txtAdminMemberID);
            this.grpTransactionDetail.Controls.Add(this.txtReferenceID);
            this.grpTransactionDetail.Controls.Add(this.txtMemberTranID);
            this.grpTransactionDetail.Controls.Add(this.txtResponse);
            this.grpTransactionDetail.Controls.Add(this.txtDuration);
            this.grpTransactionDetail.Controls.Add(this.txtPlan);
            this.grpTransactionDetail.Controls.Add(this.txtCurrency);
            this.grpTransactionDetail.Controls.Add(this.txtAmount);
            this.grpTransactionDetail.Controls.Add(this.txtTransactionType);
            this.grpTransactionDetail.Controls.Add(this.lblResponse);
            this.grpTransactionDetail.Controls.Add(this.lblDuration);
            this.grpTransactionDetail.Controls.Add(this.lblDiscount);
            this.grpTransactionDetail.Controls.Add(this.lblPlan);
            this.grpTransactionDetail.Controls.Add(this.lblCurrency);
            this.grpTransactionDetail.Controls.Add(this.lblAmount);
            this.grpTransactionDetail.Controls.Add(this.lblAdminMemberID);
            this.grpTransactionDetail.Controls.Add(this.lblTransactionType);
            this.grpTransactionDetail.Controls.Add(this.lblReferenceID);
            this.grpTransactionDetail.Controls.Add(this.lblMemberTranID);
            this.grpTransactionDetail.Location = new System.Drawing.Point(8, 8);
            this.grpTransactionDetail.Name = "grpTransactionDetail";
            this.grpTransactionDetail.Size = new System.Drawing.Size(696, 208);
            this.grpTransactionDetail.TabIndex = 0;
            this.grpTransactionDetail.TabStop = false;
            this.grpTransactionDetail.Text = "Transaction Detail";
            // 
            // btnChargeback
            // 
            this.btnChargeback.Location = new System.Drawing.Point(544, 176);
            this.btnChargeback.Name = "btnChargeback";
            this.btnChargeback.Size = new System.Drawing.Size(80, 20);
            this.btnChargeback.TabIndex = 22;
            this.btnChargeback.Text = "Chargeback";
            this.btnChargeback.Visible = false;
            this.btnChargeback.Click += new System.EventHandler(this.btnChargeback_Click);
            // 
            // btnCredit
            // 
            this.btnCredit.Location = new System.Drawing.Point(544, 144);
            this.btnCredit.Name = "btnCredit";
            this.btnCredit.Size = new System.Drawing.Size(72, 20);
            this.btnCredit.TabIndex = 20;
            this.btnCredit.Text = "Credit";
            this.btnCredit.Visible = false;
            this.btnCredit.Click += new System.EventHandler(this.btnCredit_Click);
            // 
            // txtDiscount
            // 
            this.txtDiscount.Enabled = false;
            this.txtDiscount.Location = new System.Drawing.Point(528, 88);
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Size = new System.Drawing.Size(160, 20);
            this.txtDiscount.TabIndex = 19;
            // 
            // txtAdminMemberID
            // 
            this.txtAdminMemberID.Enabled = false;
            this.txtAdminMemberID.Location = new System.Drawing.Point(528, 64);
            this.txtAdminMemberID.Name = "txtAdminMemberID";
            this.txtAdminMemberID.Size = new System.Drawing.Size(160, 20);
            this.txtAdminMemberID.TabIndex = 18;
            // 
            // txtReferenceID
            // 
            this.txtReferenceID.Enabled = false;
            this.txtReferenceID.Location = new System.Drawing.Point(528, 40);
            this.txtReferenceID.Name = "txtReferenceID";
            this.txtReferenceID.Size = new System.Drawing.Size(160, 20);
            this.txtReferenceID.TabIndex = 17;
            // 
            // txtMemberTranID
            // 
            this.txtMemberTranID.Enabled = false;
            this.txtMemberTranID.Location = new System.Drawing.Point(528, 16);
            this.txtMemberTranID.Name = "txtMemberTranID";
            this.txtMemberTranID.Size = new System.Drawing.Size(160, 20);
            this.txtMemberTranID.TabIndex = 16;
            // 
            // txtResponse
            // 
            this.txtResponse.Enabled = false;
            this.txtResponse.Location = new System.Drawing.Point(112, 136);
            this.txtResponse.Multiline = true;
            this.txtResponse.Name = "txtResponse";
            this.txtResponse.Size = new System.Drawing.Size(424, 32);
            this.txtResponse.TabIndex = 15;
            // 
            // txtDuration
            // 
            this.txtDuration.Enabled = false;
            this.txtDuration.Location = new System.Drawing.Point(112, 88);
            this.txtDuration.Name = "txtDuration";
            this.txtDuration.Size = new System.Drawing.Size(152, 20);
            this.txtDuration.TabIndex = 14;
            // 
            // txtPlan
            // 
            this.txtPlan.Enabled = false;
            this.txtPlan.Location = new System.Drawing.Point(112, 112);
            this.txtPlan.Name = "txtPlan";
            this.txtPlan.Size = new System.Drawing.Size(576, 20);
            this.txtPlan.TabIndex = 13;
            // 
            // txtCurrency
            // 
            this.txtCurrency.Enabled = false;
            this.txtCurrency.Location = new System.Drawing.Point(112, 64);
            this.txtCurrency.Name = "txtCurrency";
            this.txtCurrency.Size = new System.Drawing.Size(64, 20);
            this.txtCurrency.TabIndex = 12;
            // 
            // txtAmount
            // 
            this.txtAmount.Enabled = false;
            this.txtAmount.Location = new System.Drawing.Point(112, 40);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(104, 20);
            this.txtAmount.TabIndex = 11;
            // 
            // txtTransactionType
            // 
            this.txtTransactionType.Enabled = false;
            this.txtTransactionType.Location = new System.Drawing.Point(112, 16);
            this.txtTransactionType.Name = "txtTransactionType";
            this.txtTransactionType.Size = new System.Drawing.Size(152, 20);
            this.txtTransactionType.TabIndex = 10;
            // 
            // lblResponse
            // 
            this.lblResponse.Location = new System.Drawing.Point(8, 136);
            this.lblResponse.Name = "lblResponse";
            this.lblResponse.Size = new System.Drawing.Size(104, 16);
            this.lblResponse.TabIndex = 9;
            this.lblResponse.Text = "Response";
            this.lblResponse.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // lblDuration
            // 
            this.lblDuration.Location = new System.Drawing.Point(8, 88);
            this.lblDuration.Name = "lblDuration";
            this.lblDuration.Size = new System.Drawing.Size(104, 16);
            this.lblDuration.TabIndex = 8;
            this.lblDuration.Text = "Duration";
            this.lblDuration.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // lblDiscount
            // 
            this.lblDiscount.Location = new System.Drawing.Point(424, 88);
            this.lblDiscount.Name = "lblDiscount";
            this.lblDiscount.Size = new System.Drawing.Size(104, 16);
            this.lblDiscount.TabIndex = 7;
            this.lblDiscount.Text = "Discount";
            this.lblDiscount.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // lblPlan
            // 
            this.lblPlan.Location = new System.Drawing.Point(8, 112);
            this.lblPlan.Name = "lblPlan";
            this.lblPlan.Size = new System.Drawing.Size(104, 16);
            this.lblPlan.TabIndex = 6;
            this.lblPlan.Text = "Plan";
            this.lblPlan.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // lblCurrency
            // 
            this.lblCurrency.Location = new System.Drawing.Point(8, 64);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(104, 16);
            this.lblCurrency.TabIndex = 5;
            this.lblCurrency.Text = "Currency";
            this.lblCurrency.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // lblAmount
            // 
            this.lblAmount.Location = new System.Drawing.Point(8, 40);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(104, 16);
            this.lblAmount.TabIndex = 4;
            this.lblAmount.Text = "Amount";
            this.lblAmount.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // lblAdminMemberID
            // 
            this.lblAdminMemberID.Location = new System.Drawing.Point(424, 64);
            this.lblAdminMemberID.Name = "lblAdminMemberID";
            this.lblAdminMemberID.Size = new System.Drawing.Size(104, 16);
            this.lblAdminMemberID.TabIndex = 3;
            this.lblAdminMemberID.Text = "Admin Member ID";
            this.lblAdminMemberID.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // lblTransactionType
            // 
            this.lblTransactionType.Location = new System.Drawing.Point(8, 16);
            this.lblTransactionType.Name = "lblTransactionType";
            this.lblTransactionType.Size = new System.Drawing.Size(104, 16);
            this.lblTransactionType.TabIndex = 2;
            this.lblTransactionType.Text = "Transaction Type";
            this.lblTransactionType.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // lblReferenceID
            // 
            this.lblReferenceID.Location = new System.Drawing.Point(424, 40);
            this.lblReferenceID.Name = "lblReferenceID";
            this.lblReferenceID.Size = new System.Drawing.Size(104, 16);
            this.lblReferenceID.TabIndex = 1;
            this.lblReferenceID.Text = "Reference ID";
            this.lblReferenceID.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // lblMemberTranID
            // 
            this.lblMemberTranID.Location = new System.Drawing.Point(424, 16);
            this.lblMemberTranID.Name = "lblMemberTranID";
            this.lblMemberTranID.Size = new System.Drawing.Size(104, 16);
            this.lblMemberTranID.TabIndex = 0;
            this.lblMemberTranID.Text = "Member Tran ID";
            this.lblMemberTranID.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // tcDetails
            // 
            this.tcDetails.Controls.Add(this.tpProviderLog);
            this.tcDetails.Controls.Add(this.tpPaymentInformation);
            this.tcDetails.Controls.Add(this.tpErrorLog);
            this.tcDetails.Location = new System.Drawing.Point(8, 232);
            this.tcDetails.Name = "tcDetails";
            this.tcDetails.SelectedIndex = 0;
            this.tcDetails.Size = new System.Drawing.Size(696, 240);
            this.tcDetails.TabIndex = 1;
            // 
            // tpProviderLog
            // 
            this.tpProviderLog.Controls.Add(this.lvProviderLog);
            this.tpProviderLog.Location = new System.Drawing.Point(4, 22);
            this.tpProviderLog.Name = "tpProviderLog";
            this.tpProviderLog.Size = new System.Drawing.Size(688, 214);
            this.tpProviderLog.TabIndex = 0;
            this.tpProviderLog.Text = "Provider Log";
            // 
            // lvProviderLog
            // 
            this.lvProviderLog.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chName,
            this.chValue});
            this.lvProviderLog.Location = new System.Drawing.Point(8, 8);
            this.lvProviderLog.Name = "lvProviderLog";
            this.lvProviderLog.Size = new System.Drawing.Size(672, 240);
            this.lvProviderLog.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvProviderLog.TabIndex = 0;
            this.lvProviderLog.UseCompatibleStateImageBehavior = false;
            this.lvProviderLog.View = System.Windows.Forms.View.Details;
            // 
            // chName
            // 
            this.chName.Text = "Name";
            this.chName.Width = 150;
            // 
            // chValue
            // 
            this.chValue.Text = "Value";
            this.chValue.Width = 150;
            // 
            // tpPaymentInformation
            // 
            this.tpPaymentInformation.Controls.Add(this.lvPaymentInformation);
            this.tpPaymentInformation.Location = new System.Drawing.Point(4, 22);
            this.tpPaymentInformation.Name = "tpPaymentInformation";
            this.tpPaymentInformation.Size = new System.Drawing.Size(688, 214);
            this.tpPaymentInformation.TabIndex = 1;
            this.tpPaymentInformation.Text = "Payment Information";
            // 
            // lvPaymentInformation
            // 
            this.lvPaymentInformation.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lvPaymentInformation.Location = new System.Drawing.Point(8, 8);
            this.lvPaymentInformation.Name = "lvPaymentInformation";
            this.lvPaymentInformation.Size = new System.Drawing.Size(672, 241);
            this.lvPaymentInformation.TabIndex = 1;
            this.lvPaymentInformation.UseCompatibleStateImageBehavior = false;
            this.lvPaymentInformation.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 150;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Value";
            this.columnHeader2.Width = 250;
            // 
            // tpErrorLog
            // 
            this.tpErrorLog.Controls.Add(this.txtDetail);
            this.tpErrorLog.Controls.Add(this.lblDetail);
            this.tpErrorLog.Controls.Add(this.txtAttempts);
            this.tpErrorLog.Controls.Add(this.lblAttempts);
            this.tpErrorLog.Location = new System.Drawing.Point(4, 22);
            this.tpErrorLog.Name = "tpErrorLog";
            this.tpErrorLog.Size = new System.Drawing.Size(688, 214);
            this.tpErrorLog.TabIndex = 2;
            this.tpErrorLog.Text = "Error Log";
            // 
            // txtDetail
            // 
            this.txtDetail.Enabled = false;
            this.txtDetail.Location = new System.Drawing.Point(72, 40);
            this.txtDetail.Multiline = true;
            this.txtDetail.Name = "txtDetail";
            this.txtDetail.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtDetail.Size = new System.Drawing.Size(608, 112);
            this.txtDetail.TabIndex = 3;
            // 
            // lblDetail
            // 
            this.lblDetail.Location = new System.Drawing.Point(8, 40);
            this.lblDetail.Name = "lblDetail";
            this.lblDetail.Size = new System.Drawing.Size(56, 16);
            this.lblDetail.TabIndex = 2;
            this.lblDetail.Text = "Detail";
            this.lblDetail.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // txtAttempts
            // 
            this.txtAttempts.Enabled = false;
            this.txtAttempts.Location = new System.Drawing.Point(72, 8);
            this.txtAttempts.Name = "txtAttempts";
            this.txtAttempts.Size = new System.Drawing.Size(88, 20);
            this.txtAttempts.TabIndex = 1;
            // 
            // lblAttempts
            // 
            this.lblAttempts.Location = new System.Drawing.Point(8, 8);
            this.lblAttempts.Name = "lblAttempts";
            this.lblAttempts.Size = new System.Drawing.Size(56, 16);
            this.lblAttempts.TabIndex = 0;
            this.lblAttempts.Text = "Attempts";
            this.lblAttempts.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // MemberTransaction
            // 
            this.Controls.Add(this.tcDetails);
            this.Controls.Add(this.grpTransactionDetail);
            this.Name = "MemberTransaction";
            this.Size = new System.Drawing.Size(712, 544);
            this.grpTransactionDetail.ResumeLayout(false);
            this.grpTransactionDetail.PerformLayout();
            this.tcDetails.ResumeLayout(false);
            this.tpProviderLog.ResumeLayout(false);
            this.tpPaymentInformation.ResumeLayout(false);
            this.tpErrorLog.ResumeLayout(false);
            this.tpErrorLog.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion
		private void btnCredit_Click(object sender, System.EventArgs e)
		{

            DateTime PreviousBatchDate;
            // Calculating the time for last batch process
            if (DateTime.Now.TimeOfDay.Hours > BATCH_PROCESS_HOUR)
            {
                PreviousBatchDate = DateTime.Parse(string.Format(BATCH_DATE_FORMAT, DateTime.Today.Month.ToString(), DateTime.Today.Day.ToString(), DateTime.Today.Year.ToString()));
            }
            else
            {
                DateTime dtYesterday = DateTime.Today.AddDays(-1).Date;
                PreviousBatchDate = DateTime.Parse(string.Format(BATCH_DATE_FORMAT, dtYesterday.Month.ToString(), dtYesterday.Day.ToString(), dtYesterday.Year.ToString()));
            }

            if (memberTran.InsertDate.CompareTo(PreviousBatchDate) > 0 && !(memberTran.SiteID == 4 || memberTran.SiteID == 15)) // Aded the siteID logic to avoid voiding Tranzila transactions since they don't have a void process.
            {
                // Void
                CreditRequested(memberTran, merchantID, true);
            }
            else
            {
                // Credit
                CreditRequested(memberTran, merchantID, false);
            }
		}
		
        private void btnChargeback_Click(object sender, System.EventArgs e)
		{
			string x = System.Environment.GetEnvironmentVariable( "TEMP" ) + "\\chargeback.htm";

			string pv = FormMain.selectedPrivateLabel;

			using (StreamWriter sw = new StreamWriter( x ) )
			{
				// Private Label and address
				sw.WriteLine("<html><head>" );

				// use this so the UI is more simple
				sw.WriteLine( "<script language=\"JavaScript\"><!--" );
				sw.WriteLine( "function chTxt(t) {" );
				sw.WriteLine( "\tif (document.getElementById) {" );
				sw.WriteLine( "\t\tdocument.getElementById(\"bankTxt\").innerHTML = t;" );
				sw.WriteLine( "\t}" );
				sw.WriteLine( "}" );
				sw.WriteLine( "//--></script>" );

				sw.WriteLine( "<body><b>" + pv + "</b><p>");
				sw.WriteLine("<b>8383 Wilshire blvd Suite 800<br>");
				sw.WriteLine("Beverly Hills, CA  90211<p>");


				// order information
				sw.Write("</b>Date: <b>");
				sw.WriteLine( DateTime.Now );
				sw.Write( "</b><br>Order Number: <b>" );
				sw.WriteLine( txtMemberTranID.Text );

				// bank info
				sw.Write( "<p>" );
				sw.Write( "</b>To: " );
				sw.WriteLine( "<select name =\"\" id=\"sbank\" onchange=\"chTxt(this.options[this.selectedIndex].text);\">" );
				sw.WriteLine( "<option value=\"Bank of America\">Bank of America" );
				sw.WriteLine( "<option vlaue=\"Chase Merchant Services\">Chase Merchant Services" );
				sw.WriteLine( "<option vlaue=\"American Express\">American Express" );
				sw.WriteLine( "<option vlaue=\"Discover\">Discover" );
				sw.WriteLine( "</select>" );

				sw.Write( "<p>" );

				sw.Write( "Merchant Number: " );
				sw.WriteLine( "<input name=\"merchant\" size=17 value=\"\"><p>" );

				// this hack is due to the nature of contains in the ListViewItemCollection
				// for contains to work you need a listviewitem who is == to one in the
				// collection.  this makes everything messy.  so, iterating is a bad
				// but effective solution.
				string fname = "";
				string lname = "";
				string ccard = "";
				string expyear = "";
				string expmon = "";
				string addrline1 = ""; 
				string addrline2 = "";
				string city = "";
				string state = "";
				string country = "", postacode = "";

				foreach ( ListViewItem i in lvPaymentInformation.Items )
				{
					if ( i.Text == "FirstName" ) fname = i.SubItems[1].Text;
					if ( i.Text == "LastName" ) lname = i.SubItems[1].Text;
					if ( i.Text == "CreditCardNumber" ) ccard = i.SubItems[1].Text;
					if ( i.Text == "CreditCardExpirationYear" ) expyear = i.SubItems[1].Text;
					if ( i.Text == "CreditCardExpirationMonth" ) expmon = i.SubItems[1].Text;
					if ( i.Text == "AddressLine1" ) addrline1 = i.SubItems[1].Text;
					if ( i.Text == "AddressLine2" ) addrline2 = i.SubItems[1].Text;
					if ( i.Text == "City" ) city = i.SubItems[1].Text;
					if ( i.Text == "State" ) state = i.SubItems[1].Text;
					if ( i.Text == "Country" ) country = i.SubItems[1].Text;
					if ( i.Text == "PostalCode" ) postacode = i.SubItems[1].Text;
				}
			
				int clen = ccard.Length;
				if (clen > 4)
				{
					ccard = ccard.Substring( clen - 4, 4 );
					ccard = ccard.PadLeft( clen - 4, 'X' );
				}
				else
				{
					EventLog.WriteEntry("Spark.Credit.Client", "Empty credit card for member # ", EventLogEntryType.Warning);

					ccard = @"************<input type=""text"">";
					MessageBox.Show(
						"The credit card number is missing for this card. Please take a note that you will need to enter the LAST 4 digits manualy on the printed letter.");
					
					
				}
				
				string approval = "";
				string avs1 = "", avs2 = "";

				foreach( ListViewItem i in lvProviderLog.Items ) 
				{
					if ( i.Text == "Approval" ) approval = i.SubItems[1].Text;
					if ( i.Text == "AddressMatch" ) avs1 = i.SubItems[1].Text;
					if ( i.Text == "ZipMatch" ) avs2 = i.SubItems[1].Text;
				}

				bool avs = ( avs1 == "Y" && avs2 == "Y" ) ? true : false;

				sw.Write( "Customer Credit Card Number: <b>" );
				sw.WriteLine( ccard + "</b><br>" );
				sw.Write( "Date of Charge: <b>" );
				sw.Write( memberTran.InsertDate );
				sw.WriteLine( "</b><br>" );
				sw.Write( "Amount of Original Charge: <b>$" );
				sw.WriteLine( txtAmount.Text + "</b><br>" );

				sw.WriteLine( "Retrieval Reference/Control Number: <input name=\"ref\" size=15><p>" );
	
				sw.WriteLine( pv + " processes internet orders.<p>" );
				sw.WriteLine( "Our Customers place orders over the internet using <span id=\"bankTxt\">Bank Of America</span>.<br>" );
				sw.WriteLine( pv + " obtains an authorization and address verification<br>" );
				sw.WriteLine( "when the order is placed.<p>" );

				sw.WriteLine( "<ul>" );
				sw.WriteLine( "<li>The authorization code for this order is: <b>" + approval + "</b>" );
				sw.WriteLine( "<li>The AVS result obtained is address and zip: <b>" + ( avs ? "Yes" : "No" ) + "</b>" );
				sw.WriteLine( "<li>The credit card expiration date is: <b>" + expmon + "-" + expyear + "</b>" );
				sw.WriteLine( "<li>The billing address on the order was: <p>" );

				sw.WriteLine( "<b>" + fname + " " + lname + "<br>" );
				sw.WriteLine( addrline1 + "<br>" );
				if ( addrline2 != "" ) sw.WriteLine( addrline2 + "<br>" );
				sw.WriteLine( city + ", " + state + "<br>" );
				sw.WriteLine( country + "&nbsp;&nbsp;" + postacode + "<br>" );
				sw.WriteLine( "</ul>" );
			}

			Process.Start( x );
		}
	}
}