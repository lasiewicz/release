using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

using Matchnet;


namespace Spark.Credit.Client
{
	public class MemberLookup : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.TextBox MemberIDLookupInputText;
		private System.Windows.Forms.Label lblEmailAddress;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.ComponentModel.Container components = null;

		public Int32[] _memberIDs = new Int32[0];

		public delegate void MemberLookupCompleteEventHandler();
		public event MemberLookupCompleteEventHandler MemberLookupCompleted;

		public delegate void MemberLookupCancelEventHandler();
		public event MemberLookupCancelEventHandler MemberLookupCancelled;

		public MemberLookup()
		{
			InitializeComponent();

		}


		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.MemberIDLookupInputText = new System.Windows.Forms.TextBox();
			this.lblEmailAddress = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(328, 48);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(96, 20);
			this.btnCancel.TabIndex = 35;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnOK
			// 
			this.btnOK.Location = new System.Drawing.Point(216, 48);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(96, 20);
			this.btnOK.TabIndex = 34;
			this.btnOK.Text = "OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// MemberIDLookupInputText
			// 
			this.MemberIDLookupInputText.Location = new System.Drawing.Point(216, 24);
			this.MemberIDLookupInputText.Name = "MemberIDLookupInputText";
			this.MemberIDLookupInputText.Size = new System.Drawing.Size(208, 20);
			this.MemberIDLookupInputText.TabIndex = 33;
			this.MemberIDLookupInputText.Text = "";
			// 
			// lblEmailAddress
			// 
			this.lblEmailAddress.AutoSize = true;
			this.lblEmailAddress.Location = new System.Drawing.Point(14, 28);
			this.lblEmailAddress.Name = "lblEmailAddress";
			this.lblEmailAddress.Size = new System.Drawing.Size(211, 16);
			this.lblEmailAddress.TabIndex = 36;
			this.lblEmailAddress.Text = "Email Address / CCard# / Transaction ID:";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.btnCancel);
			this.groupBox1.Controls.Add(this.btnOK);
			this.groupBox1.Controls.Add(this.MemberIDLookupInputText);
			this.groupBox1.Controls.Add(this.lblEmailAddress);
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(432, 80);
			this.groupBox1.TabIndex = 37;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Member Lookup";
			// 
			// MemberLookup
			// 
			this.Controls.Add(this.groupBox1);
			this.Name = "MemberLookup";
			this.Size = new System.Drawing.Size(434, 82);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;
			Int32 memberID = Constants.NULL_INT;

			try
			{
				MemberIDLookupInputText.Text = MemberIDLookupInputText.Text.Trim();
				if ( MemberIDLookupInputText.Text.Length == 0) 
				{
					MessageBox.Show("Please enter search criteria.");
					return;
				}

				if ( MemberIDLookupInputText.Text.IndexOf( "@" ) == -1 )
				{
					_memberIDs = CreditServiceWrapper.GetService().GetMemberIDsByAccountNumber(MemberIDLookupInputText.Text);

					if (_memberIDs.Length == 0)
					{
						if (MemberIDLookupInputText.Text.Length <= 10)
						{
							memberID = CreditServiceWrapper.GetService().GetMemberIDByMemberTranID(Convert.ToInt32(MemberIDLookupInputText.Text));
							if (memberID != Constants.NULL_INT)
							{
								_memberIDs = new Int32[]{memberID};
							}
						}
					}
				}
				else
				{
					memberID = CreditServiceWrapper.GetService().GetMemberIDByEmailAddress(MemberIDLookupInputText.Text);
					if (memberID != Constants.NULL_INT)
					{
						_memberIDs = new Int32[]{memberID};
					}
				}

				if (_memberIDs.Length == 0)
				{
					MessageBox.Show("Unable to find Member ID [" + MemberIDLookupInputText.Text + "]");
				}
				else
				{

					MemberLookupCompleted();
				}
			}
			catch (Exception ex) 
			{
				MessageBox.Show(ex.Message);
			}

			this.Cursor = Cursors.Default;
		}


		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			MemberLookupCancelled();		
		}


		public Int32[] MemberIDs
		{
			get
			{
				return _memberIDs;
			}
		}
	}
}
