using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

using Matchnet;


namespace Spark.Credit.Client
{
	public class MemberIDList : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.ListBox listBoxIDs;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;
		private System.ComponentModel.Container components = null;

		public delegate void MemberIDChoiceEventHandler();
		public event MemberIDChoiceEventHandler MemberIDChosen;

		public delegate void MemberIDChoiceCancelEventHandler();
		public event MemberIDChoiceCancelEventHandler MemberIDChoiceCancelled;

		public MemberIDList()
		{
			InitializeComponent();
		}


		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.listBoxIDs = new System.Windows.Forms.ListBox();
			this.label1 = new System.Windows.Forms.Label();
			this.btnOK = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// listBoxIDs
			// 
			this.listBoxIDs.Location = new System.Drawing.Point(7, 25);
			this.listBoxIDs.Name = "listBoxIDs";
			this.listBoxIDs.Size = new System.Drawing.Size(344, 95);
			this.listBoxIDs.TabIndex = 9;
			this.listBoxIDs.SelectedIndexChanged += new System.EventHandler(this.listBoxIDs_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(7, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(354, 16);
			this.label1.TabIndex = 8;
			this.label1.Text = "Multiple MemberIDs were returned, please select one of the following:";
			// 
			// btnOK
			// 
			this.btnOK.Enabled = false;
			this.btnOK.Location = new System.Drawing.Point(192, 128);
			this.btnOK.Name = "btnOK";
			this.btnOK.TabIndex = 10;
			this.btnOK.Text = "OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(272, 128);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 11;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// MemberIDList
			// 
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.listBoxIDs);
			this.Controls.Add(this.label1);
			this.Name = "MemberIDList";
			this.Size = new System.Drawing.Size(368, 160);
			this.ResumeLayout(false);

		}
		#endregion


		private void btnOK_Click(object sender, System.EventArgs e)
		{
			MemberIDChosen();
		}


		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			MemberIDChoiceCancelled();
		}

		private void listBoxIDs_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (listBoxIDs.SelectedIndex > -1)
			{
				btnOK.Enabled = true;
			}
			else
			{
				btnOK.Enabled = false;
			}
		}

		
		public Int32 SelectedMemberID
		{
			get
			{
				if (listBoxIDs.SelectedIndex > -1)
				{
					return (Int32)listBoxIDs.Items[listBoxIDs.SelectedIndex];
				}
				else
				{
					return Constants.NULL_INT;
				}
			}
		}


		public Int32[] MemberIDs
		{
			set
			{
				btnOK.Enabled = false;
				listBoxIDs.Items.Clear();
				for (Int32 i = 0; i < value.Length; i++)
				{
					listBoxIDs.Items.Add(value[i]);
				}
			}
		}
	}
}
