using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

using Matchnet;
/*
using Matchnet.Credit.ServiceAdapters;
using Matchnet.Security;
*/
namespace Spark.Credit.Client
{
	public class MemberIDLookup : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label lblEmailAddress;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Button btnCancel;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.TextBox MemberIDLookupInputText;

		private int memberID = Constants.NULL_INT;

		public int MemberID 
		{
			get 
			{
				return memberID;
			}
		}

		public MemberIDLookup()
		{
			InitializeComponent();
			MemberIDLookupInputText.KeyDown += new KeyEventHandler(MemberIDLookupInputText_KeyDown);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(MemberIDLookup));
			this.lblEmailAddress = new System.Windows.Forms.Label();
			this.MemberIDLookupInputText = new System.Windows.Forms.TextBox();
			this.btnOK = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lblEmailAddress
			// 
			this.lblEmailAddress.Location = new System.Drawing.Point(8, 8);
			this.lblEmailAddress.Name = "lblEmailAddress";
			this.lblEmailAddress.Size = new System.Drawing.Size(144, 32);
			this.lblEmailAddress.TabIndex = 0;
			this.lblEmailAddress.Text = "Email Address / CCard# / Transaction ID";
			// 
			// MemberIDLookupInputText
			// 
			this.MemberIDLookupInputText.Location = new System.Drawing.Point(160, 8);
			this.MemberIDLookupInputText.Name = "MemberIDLookupInputText";
			this.MemberIDLookupInputText.Size = new System.Drawing.Size(208, 20);
			this.MemberIDLookupInputText.TabIndex = 1;
			this.MemberIDLookupInputText.Text = "";
			// 
			// btnOK
			// 
			this.btnOK.Location = new System.Drawing.Point(168, 32);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(96, 20);
			this.btnOK.TabIndex = 2;
			this.btnOK.Text = "OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(272, 32);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(96, 20);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// MemberIDLookup
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(378, 63);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.MemberIDLookupInputText);
			this.Controls.Add(this.lblEmailAddress);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "MemberIDLookup";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Member ID Lookup";
			this.ResumeLayout(false);

		}
		#endregion

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			MemberLookup();
		}

		private void MemberLookup() 
		{
			//try
			//{
				MemberIDLookupInputText.Text = MemberIDLookupInputText.Text.Trim();
				if ( MemberIDLookupInputText.Text.Length == 0) 
				{
					MessageBox.Show("Please enter search criteria.");
					return;
				}

				if ( MemberIDLookupInputText.Text.IndexOf( "@" ) == -1 )
				{
					Int32[] memberIDs = CreditServiceWrapper.GetService().GetMemberIDsByAccountNumber(MemberIDLookupInputText.Text);

					if (memberIDs.Length == 1)
					{
						memberID = memberIDs[0];
					}
					else if (memberIDs.Length > 1)
					{
						MemberIDList memberIDList = new MemberIDList();
						memberIDList.MemberIDs = memberIDs;
						memberIDList.ShowDialog();
						memberID = memberIDList.SelectedMemberID;

						if (memberID == Constants.NULL_INT)
						{
							return;
						}
					}

					if ( memberID == Constants.NULL_INT )
					{
						if ( MemberIDLookupInputText.Text.Length <= 9 )
						{
							memberID = CreditServiceWrapper.GetService().GetMemberIDByMemberTranID(Convert.ToInt32(MemberIDLookupInputText.Text));
						}
					}
				}
				else
				{
					memberID = CreditServiceWrapper.GetService().GetMemberIDByEmailAddress(MemberIDLookupInputText.Text);
				}

				if ( memberID == Constants.NULL_INT )
				{
					MessageBox.Show("Unable to find Member ID for Email Address [" + MemberIDLookupInputText.Text + "]");
					return;
				}
				else
				{
					this.Close();
					return;
				}
			/*}
			catch (Exception ex) 
			{
				MessageBox.Show( ex.Message );
			}
			*/
		}

		private void MemberIDLookupInputText_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter) 
			{
				MemberLookup();
			}
		}
	}
}
