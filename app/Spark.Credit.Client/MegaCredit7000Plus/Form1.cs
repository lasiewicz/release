using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Threading;

using Spark.Credit.Client;


namespace MegaCredit7000Plus
{
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox textBox1;
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(8, 8);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			this.button1.Text = "button1";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(8, 40);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.textBox1.Size = new System.Drawing.Size(824, 520);
			this.textBox1.TabIndex = 1;
			this.textBox1.Text = "";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(840, 565);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.button1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			StreamReader sr = new StreamReader(@"c:\credits.txt");
			string[] lines = sr.ReadToEnd().Replace("\r","").Split('\n');

			CreditServiceWrapper.GetService().Authenticate("gpeterson@matchnet.com", "urine666");

			for (Int32 lineNum = 0; lineNum < lines.Length; lineNum++)
			{
				string[] fields = lines[lineNum].Split(',');
				bool hadCredit = false;
				Int32 memberID = Convert.ToInt32(fields[0]);
				Int32 adminMemberID = 8578217;
				Int32 siteID = Convert.ToInt32(fields[1]);
				Int32 referenceMemberTranID = Convert.ToInt32(fields[2]);
				Int32 memberPaymentID = Convert.ToInt32(fields[3]);
				Decimal amount = Convert.ToDecimal(fields[4]);
				Int32 merchantID = Convert.ToInt32(fields[5]);
				Int32 planID = 0;

				try
				{

					MemberTran[] transactions = CreditServiceWrapper.GetService().GetTransactions(memberID,
						siteID);

					for (Int32 tNum = 0; tNum < transactions.Length; tNum++)
					{
						MemberTran transaction = transactions[tNum];
						if (transaction.MemberTranID == referenceMemberTranID)
						{
							planID = transaction.PlanID;
						}
						if (transaction.ReferenceMemberTranID == referenceMemberTranID && transaction.TranType == TranType.AdministrativeAdjustment)
						{
							hadCredit = true;
						}
					}

					if (!hadCredit && planID > 0)
					{
						textBox1.AppendText(DateTime.Now.ToString() + "\t processing " + memberID.ToString() + "\r\n");
						this.Refresh();

						CurrencyType currencyType = CurrencyType.USDollar;

						Console.WriteLine(merchantID);

						if (merchantID >= 8000 && merchantID < 9000)
						{
							currencyType = CurrencyType.Shekels;
						}
						else if (merchantID == 12013)
						{
							currencyType = CurrencyType.CanadianDollar;
						}


						Int32 tranID = CreditServiceWrapper.GetService().BeginCredit(TranType.AdministrativeAdjustment,
							memberID,
							adminMemberID,
							siteID,
							merchantID,
							referenceMemberTranID,
							memberPaymentID,
							planID,
							amount,
							0,
							0,
							DurationType.Minute,
							currencyType);

						SubscriptionResult result;
						Int32 timeOut = 0;
						do
						{
							Thread.Sleep(1000);
							result = CreditServiceWrapper.GetService().GetMemberTranStatus(memberID,
								tranID);
							if (++timeOut >= 60) 
							{
								MessageBox.Show("Transaction timed out");
								break;
							}
						}
						while (result.MemberTranStatus == MemberTranStatus.Pending || result.MemberTranStatus == MemberTranStatus.None);
						textBox1.AppendText(DateTime.Now.ToString() + "\t\tcompleted\r\n");
						this.Refresh();
					}
					else
					{
						textBox1.AppendText(DateTime.Now.ToString() + "\t skipped " + memberID.ToString() + "\r\n");
						this.Refresh();
					}

				}
				catch (Exception ex)
				{
					textBox1.AppendText(DateTime.Now.ToString() + "\t\terror\r\n");
					Console.WriteLine(memberID + "," + siteID);
				}

				if ((lineNum % 50) == 0)
				{
					textBox1.Clear();
					this.Refresh();
				}
				
			}			
		}
	}
}
