using System;
using System.Net;

namespace Spark.Credit.Client
{
	public class CreditServiceWrapper
	{
		private static CreditService _creditService = null;

		private CreditServiceWrapper()
		{
		}

		public static CreditService GetService()
		{
			if (_creditService == null)
			{
				_creditService = new CreditService();
				_creditService.CookieContainer = new CookieContainer();
				string url = "";

				if (AppDomain.CurrentDomain.BaseDirectory.IndexOf("http://") != 0)
				{
					url = System.Configuration.ConfigurationSettings.AppSettings["url"];
				}
				else
				{
					url = AppDomain.CurrentDomain.BaseDirectory + "../CreditService.asmx";
				}

				_creditService.Url = url;
			}

			return _creditService;
		}
	}
}
