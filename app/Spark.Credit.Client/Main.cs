using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

using Matchnet;


namespace Spark.Credit.Client
{
	public class Main : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Button FormMainPaymentAdjustButton;
		private System.Windows.Forms.Button FormMainTransactionsButton;
		private System.Windows.Forms.ComboBox FormMainSiteComboBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button FormMainMemberLookupButton;
		private System.Windows.Forms.TextBox FormMainMemberIDTextBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TabControl FormMainTransactionsTab;
		private System.ComponentModel.Container components = null;

		public delegate void MemberLookupEventHandler();
		public event MemberLookupEventHandler MemberLookupRequested;

		public delegate void CreditRequestEventHandler(MemberTran memberTran,
			Int32 merchantID,
			bool isVoid);
		public event  CreditRequestEventHandler CreditRequested;

		public Main()
		{
			InitializeComponent();
		}


		public void PopulateSites()
		{
			try 
			{
				Site[] sites = CreditServiceWrapper.GetService().GetSites();
				foreach (Site s in sites)
				{
					FormMainSiteComboBox.Items.Add(new SiteItem(s));
				}

				FormMainSiteComboBox.SelectedIndex = 0;
			} 
			catch (Exception ex) 
			{
				MessageBox.Show( ex.Message );
			}
		}


		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.FormMainPaymentAdjustButton = new System.Windows.Forms.Button();
			this.FormMainTransactionsButton = new System.Windows.Forms.Button();
			this.FormMainSiteComboBox = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.FormMainMemberLookupButton = new System.Windows.Forms.Button();
			this.FormMainMemberIDTextBox = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.FormMainTransactionsTab = new System.Windows.Forms.TabControl();
			this.SuspendLayout();
			// 
			// FormMainPaymentAdjustButton
			// 
			this.FormMainPaymentAdjustButton.Location = new System.Drawing.Point(624, 4);
			this.FormMainPaymentAdjustButton.Name = "FormMainPaymentAdjustButton";
			this.FormMainPaymentAdjustButton.Size = new System.Drawing.Size(104, 23);
			this.FormMainPaymentAdjustButton.TabIndex = 25;
			this.FormMainPaymentAdjustButton.Text = "Payment Adjust";
			this.FormMainPaymentAdjustButton.Click += new System.EventHandler(this.FormMainPaymentAdjustButton_Click);
			// 
			// FormMainTransactionsButton
			// 
			this.FormMainTransactionsButton.Location = new System.Drawing.Point(512, 4);
			this.FormMainTransactionsButton.Name = "FormMainTransactionsButton";
			this.FormMainTransactionsButton.Size = new System.Drawing.Size(104, 23);
			this.FormMainTransactionsButton.TabIndex = 24;
			this.FormMainTransactionsButton.Text = "Transactions";
			this.FormMainTransactionsButton.Click += new System.EventHandler(this.FormMainTransactionsButton_Click);
			// 
			// FormMainSiteComboBox
			// 
			this.FormMainSiteComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.FormMainSiteComboBox.Location = new System.Drawing.Point(328, 4);
			this.FormMainSiteComboBox.Name = "FormMainSiteComboBox";
			this.FormMainSiteComboBox.Size = new System.Drawing.Size(152, 21);
			this.FormMainSiteComboBox.Sorted = true;
			this.FormMainSiteComboBox.TabIndex = 23;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(296, 8);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(24, 16);
			this.label2.TabIndex = 22;
			this.label2.Text = "Site:";
			// 
			// FormMainMemberLookupButton
			// 
			this.FormMainMemberLookupButton.Location = new System.Drawing.Point(208, 4);
			this.FormMainMemberLookupButton.Name = "FormMainMemberLookupButton";
			this.FormMainMemberLookupButton.Size = new System.Drawing.Size(24, 23);
			this.FormMainMemberLookupButton.TabIndex = 21;
			this.FormMainMemberLookupButton.Text = "...";
			this.FormMainMemberLookupButton.Click += new System.EventHandler(this.FormMainMemberLookupButton_Click);
			// 
			// FormMainMemberIDTextBox
			// 
			this.FormMainMemberIDTextBox.Location = new System.Drawing.Point(64, 4);
			this.FormMainMemberIDTextBox.Name = "FormMainMemberIDTextBox";
			this.FormMainMemberIDTextBox.Size = new System.Drawing.Size(136, 20);
			this.FormMainMemberIDTextBox.TabIndex = 20;
			this.FormMainMemberIDTextBox.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 16);
			this.label1.TabIndex = 19;
			this.label1.Text = "MemberID";
			// 
			// FormMainTransactionsTab
			// 
			this.FormMainTransactionsTab.ItemSize = new System.Drawing.Size(0, 18);
			this.FormMainTransactionsTab.Location = new System.Drawing.Point(8, 36);
			this.FormMainTransactionsTab.Name = "FormMainTransactionsTab";
			this.FormMainTransactionsTab.SelectedIndex = 0;
			this.FormMainTransactionsTab.Size = new System.Drawing.Size(720, 496);
			this.FormMainTransactionsTab.TabIndex = 26;
			// 
			// Main
			// 
			this.Controls.Add(this.FormMainPaymentAdjustButton);
			this.Controls.Add(this.FormMainTransactionsButton);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.FormMainMemberLookupButton);
			this.Controls.Add(this.FormMainMemberIDTextBox);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.FormMainTransactionsTab);
			this.Controls.Add(this.FormMainSiteComboBox);
			this.Name = "Main";
			this.Size = new System.Drawing.Size(736, 536);
			this.ResumeLayout(false);

		}
		#endregion

		private void FormMainMemberLookupButton_Click(object sender, System.EventArgs e)
		{
			MemberLookupRequested();
		}


		private void FormMainTransactionsButton_Click(object sender, System.EventArgs e)
		{
			LoadTransactions();
		}


		public void LoadTransactions()
		{
			try
			{
				this.Cursor = Cursors.WaitCursor;

				if (!IsValidInput()) 
				{
					this.Cursor = Cursors.Default;
					return;
				}

				int memberID = System.Convert.ToInt32( FormMainMemberIDTextBox.Text );

				Site site = (Site)FormMainSiteComboBox.SelectedItem;
				Int32 siteID = site.SiteID;
				//selectedPrivateLabel = site.SiteName;

				MemberTran[] mtc = CreditServiceWrapper.GetService().GetTransactions(memberID, siteID);

				if ( mtc != null )
				{
					FormMainTransactionsTab.TabPages.Clear();
					
					foreach(MemberTran mt in mtc)
					{
						if (!((mt.TranType == TranType.AdministrativeAdjustment || mt.TranType == TranType.Void || mt.TranType == TranType.Termination) && mt.MemberTranStatus == MemberTranStatus.Failure))
						{
							MemberTransaction memberTransaction = new MemberTransaction();
							memberTransaction.Populate(mt);
							memberTransaction.CreditRequested += new Spark.Credit.Client.MemberTransaction.CreditRequestEventHandler(memberTransaction_CreditRequested);
							AddTransactionTab(mt.InsertDate, memberTransaction);
						}
					}
					this.Cursor = Cursors.Default;
				}
				else
				{
					this.Cursor = Cursors.Default;
					MessageBox.Show("No transactions found for member");
				}
			}
			catch (Exception ex) 
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				this.Cursor = Cursors.Default;
			}
		}


		private bool IsValidInput() 
		{
			if (FormMainMemberIDTextBox.Text.Trim().Length == 0) 
			{
				MessageBox.Show("MemberID Required");
				return false;				
			}

			if (FormMainSiteComboBox.SelectedItem == null) 
			{
				MessageBox.Show("PrivateLabel Required");
				return false;
			}

			try  
			{
				int memberID = System.Convert.ToInt32(FormMainMemberIDTextBox.Text);
			} 
			catch 
			{
				MessageBox.Show("Member ID must be a number");
				return false;
			}

			return true;
		}

		
		private void AddTransactionTab(DateTime insertDate, 
			MemberTransaction memberTransaction) 
		{
			TabPage tranPage = new TabPage(insertDate.ToShortDateString());
			tranPage.Height = FormMainTransactionsTab.Height;
			tranPage.Width = FormMainTransactionsTab.Width;
			FormMainTransactionsTab.TabPages.Add(tranPage);
			tranPage.Controls.Add(memberTransaction);
			memberTransaction.SizeToHost(tranPage);
		}


		private void FormMainPaymentAdjustButton_Click(object sender, System.EventArgs e)
		{
			if (!IsValidInput()) 
			{
				return;
			}

			FormMainTransactionsTab.TabPages.Clear();
			int memberID = System.Convert.ToInt32(FormMainMemberIDTextBox.Text);

			Site site = (Site)FormMainSiteComboBox.SelectedItem;
			int siteID = site.SiteID;

			TabPage adjustPage = new TabPage("Update Member Payment");
			adjustPage.Height = FormMainTransactionsTab.Height;
			adjustPage.Width = FormMainTransactionsTab.Width;
			FormMainTransactionsTab.TabPages.Add(adjustPage);

			CreditCardPayment creditCardPayment = new CreditCardPayment(memberID, siteID);

			adjustPage.Controls.Add(creditCardPayment);
		}


		public Int32 MemberID
		{
			get
			{
				double memberIDD;
				if (Double.TryParse(FormMainMemberIDTextBox.Text, System.Globalization.NumberStyles.Integer, null, out memberIDD))
				{
					return (Int32)memberIDD;
				}

				return Constants.NULL_INT;
			}
			set
			{
				if (value != Constants.NULL_INT)
				{
					FormMainTransactionsTab.TabPages.Clear();
					FormMainMemberIDTextBox.Text = value.ToString();
				}
			}
		}


		private void memberTransaction_CreditRequested(MemberTran memberTran,
			Int32 merchantID,
			bool isVoid)
		{
			CreditRequested(memberTran,
				merchantID,
				isVoid);
		}
	}
}
