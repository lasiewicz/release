using System;
using System.Net;
using System.Configuration;

namespace Spark.Credit.Client
{
	public class CreditServiceWrapper
	{
		private static CreditService _creditService = null;

		private CreditServiceWrapper()
		{
		}

		public static CreditService GetService()
		{
			if (_creditService == null)
			{
				_creditService = new CreditService();
				_creditService.CookieContainer = new CookieContainer();
				string url = "";

				if (AppDomain.CurrentDomain.BaseDirectory.IndexOf("http://") != 0)
				{
                   
					url = System.Configuration.ConfigurationManager.AppSettings["url"];

                    if (url == "")
                    {
                        throw new Exception("Could not read app setting, url from app settings.");
                    }

				}
				else
				{
					url = AppDomain.CurrentDomain.BaseDirectory + "../CreditService.asmx";

                    if (url == "")
                    {
                        throw new Exception("Could not get base directory info."); 
                    }
				}

                if (url == "")
                {
                    throw new Exception("url cannot be null. both appsettings and basedirectory returned empty url.");
                }

                // with 2.0 app.config is not being read properly.
                _creditService.Url = @"http://admin.jdate.com/applications/admin/credit/creditservice.asmx";

#if DEBUG
                _creditService.Url = @"http://dev.jdate.com/applications/admin/credit/creditservice.asmx";
#endif
			}

			return _creditService;
		}
	}
}
