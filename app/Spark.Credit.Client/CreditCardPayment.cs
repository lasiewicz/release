using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using Matchnet;


namespace Spark.Credit.Client
{
	public class CreditCardPayment : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.GroupBox grpPaymentDetails;
		private System.Windows.Forms.Label lblFirstName;
		private System.Windows.Forms.Label lblLastName;
		private System.Windows.Forms.Label lblPhone;
		private System.Windows.Forms.Label lblAddressLine1;
		private System.Windows.Forms.Label lblCity;
		private System.Windows.Forms.Label lblState;
		private System.Windows.Forms.Label lblCountry;
		private System.Windows.Forms.Label lblPostalCode;
		private System.Windows.Forms.Label lblCardType;
		private System.Windows.Forms.Label lblCardNumber;
		private System.Windows.Forms.Label lblCardExpiration;
		private System.Windows.Forms.TextBox txtFirstName;
		private System.Windows.Forms.TextBox txtLastName;
		private System.Windows.Forms.TextBox txtPhone;
		private System.Windows.Forms.TextBox txtAddressLine1;
		private System.Windows.Forms.ComboBox cboCountry;
		private System.Windows.Forms.TextBox txtCity;
		private System.Windows.Forms.ComboBox cboCardType;
		private System.Windows.Forms.ComboBox cboMonth;
		private System.Windows.Forms.ComboBox cboYear;
		private System.Windows.Forms.Button btnUpdate;
		private System.Windows.Forms.TextBox txtState;
		private System.Windows.Forms.TextBox txtPostalCode;
		private System.Windows.Forms.TextBox txtCardNumber;
		
		private System.ComponentModel.Container components = null;

		private int memberID = Constants.NULL_INT;
		private int siteID = Constants.NULL_INT;

		public CreditCardPayment(int memberID, int siteID)
		{
			InitializeComponent();
			this.memberID = memberID;
			this.siteID = siteID;
			Populate();
		}

		private void Populate() 
		{
			PopulateCardType();
			PopulateMonth();
			PopulateYear();
			PopulateCountries();
		}

		private void PopulateCardType() 
		{
			foreach (string s in Enum.GetNames(typeof(CreditCardType)))
			{
				cboCardType.Items.Add(s);
			}
		}

		private void PopulateMonth() 
		{
			for (int x = 1; x < 13; x++) 
			{
				cboMonth.Items.Add(x);
			}
		}

		private void PopulateYear() 
		{
			DateTime now = DateTime.Now;
			int year = now.Year;

			for (int x = year; x < year + 11; x++) 
			{
				cboYear.Items.Add(x);
			}	
		}

		private void PopulateCountries() 
		{
			Country[] countries = CreditServiceWrapper.GetService().GetCountries();

			if (countries != null)
			{
				foreach (Country country in countries)
				{
					cboCountry.Items.Add(new CountryItem(country));
				}
			}
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.grpPaymentDetails = new System.Windows.Forms.GroupBox();
			this.btnUpdate = new System.Windows.Forms.Button();
			this.cboYear = new System.Windows.Forms.ComboBox();
			this.cboMonth = new System.Windows.Forms.ComboBox();
			this.cboCardType = new System.Windows.Forms.ComboBox();
			this.txtCardNumber = new System.Windows.Forms.TextBox();
			this.txtPostalCode = new System.Windows.Forms.TextBox();
			this.txtState = new System.Windows.Forms.TextBox();
			this.txtCity = new System.Windows.Forms.TextBox();
			this.cboCountry = new System.Windows.Forms.ComboBox();
			this.txtAddressLine1 = new System.Windows.Forms.TextBox();
			this.txtPhone = new System.Windows.Forms.TextBox();
			this.txtLastName = new System.Windows.Forms.TextBox();
			this.txtFirstName = new System.Windows.Forms.TextBox();
			this.lblCardExpiration = new System.Windows.Forms.Label();
			this.lblCardNumber = new System.Windows.Forms.Label();
			this.lblCardType = new System.Windows.Forms.Label();
			this.lblPostalCode = new System.Windows.Forms.Label();
			this.lblCountry = new System.Windows.Forms.Label();
			this.lblState = new System.Windows.Forms.Label();
			this.lblCity = new System.Windows.Forms.Label();
			this.lblAddressLine1 = new System.Windows.Forms.Label();
			this.lblPhone = new System.Windows.Forms.Label();
			this.lblLastName = new System.Windows.Forms.Label();
			this.lblFirstName = new System.Windows.Forms.Label();
			this.grpPaymentDetails.SuspendLayout();
			this.SuspendLayout();
			// 
			// grpPaymentDetails
			// 
			this.grpPaymentDetails.Controls.Add(this.btnUpdate);
			this.grpPaymentDetails.Controls.Add(this.cboYear);
			this.grpPaymentDetails.Controls.Add(this.cboMonth);
			this.grpPaymentDetails.Controls.Add(this.cboCardType);
			this.grpPaymentDetails.Controls.Add(this.txtCardNumber);
			this.grpPaymentDetails.Controls.Add(this.txtPostalCode);
			this.grpPaymentDetails.Controls.Add(this.txtState);
			this.grpPaymentDetails.Controls.Add(this.txtCity);
			this.grpPaymentDetails.Controls.Add(this.cboCountry);
			this.grpPaymentDetails.Controls.Add(this.txtAddressLine1);
			this.grpPaymentDetails.Controls.Add(this.txtPhone);
			this.grpPaymentDetails.Controls.Add(this.txtLastName);
			this.grpPaymentDetails.Controls.Add(this.txtFirstName);
			this.grpPaymentDetails.Controls.Add(this.lblCardExpiration);
			this.grpPaymentDetails.Controls.Add(this.lblCardNumber);
			this.grpPaymentDetails.Controls.Add(this.lblCardType);
			this.grpPaymentDetails.Controls.Add(this.lblPostalCode);
			this.grpPaymentDetails.Controls.Add(this.lblCountry);
			this.grpPaymentDetails.Controls.Add(this.lblState);
			this.grpPaymentDetails.Controls.Add(this.lblCity);
			this.grpPaymentDetails.Controls.Add(this.lblAddressLine1);
			this.grpPaymentDetails.Controls.Add(this.lblPhone);
			this.grpPaymentDetails.Controls.Add(this.lblLastName);
			this.grpPaymentDetails.Controls.Add(this.lblFirstName);
			this.grpPaymentDetails.Location = new System.Drawing.Point(8, 8);
			this.grpPaymentDetails.Name = "grpPaymentDetails";
			this.grpPaymentDetails.Size = new System.Drawing.Size(560, 216);
			this.grpPaymentDetails.TabIndex = 0;
			this.grpPaymentDetails.TabStop = false;
			// 
			// btnUpdate
			// 
			this.btnUpdate.Location = new System.Drawing.Point(464, 184);
			this.btnUpdate.Name = "btnUpdate";
			this.btnUpdate.Size = new System.Drawing.Size(88, 20);
			this.btnUpdate.TabIndex = 23;
			this.btnUpdate.Text = "Update";
			this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
			// 
			// cboYear
			// 
			this.cboYear.Location = new System.Drawing.Point(464, 64);
			this.cboYear.Name = "cboYear";
			this.cboYear.Size = new System.Drawing.Size(88, 21);
			this.cboYear.TabIndex = 22;
			this.cboYear.Text = "Year";
			// 
			// cboMonth
			// 
			this.cboMonth.Location = new System.Drawing.Point(368, 64);
			this.cboMonth.Name = "cboMonth";
			this.cboMonth.Size = new System.Drawing.Size(88, 21);
			this.cboMonth.TabIndex = 21;
			this.cboMonth.Text = "Month";
			// 
			// cboCardType
			// 
			this.cboCardType.Location = new System.Drawing.Point(368, 16);
			this.cboCardType.Name = "cboCardType";
			this.cboCardType.Size = new System.Drawing.Size(184, 21);
			this.cboCardType.TabIndex = 20;
			// 
			// txtCardNumber
			// 
			this.txtCardNumber.Location = new System.Drawing.Point(368, 40);
			this.txtCardNumber.Name = "txtCardNumber";
			this.txtCardNumber.Size = new System.Drawing.Size(184, 20);
			this.txtCardNumber.TabIndex = 19;
			this.txtCardNumber.Text = "";
			// 
			// txtPostalCode
			// 
			this.txtPostalCode.Location = new System.Drawing.Point(104, 184);
			this.txtPostalCode.Name = "txtPostalCode";
			this.txtPostalCode.Size = new System.Drawing.Size(144, 20);
			this.txtPostalCode.TabIndex = 18;
			this.txtPostalCode.Text = "";
			// 
			// txtState
			// 
			this.txtState.Location = new System.Drawing.Point(104, 160);
			this.txtState.Name = "txtState";
			this.txtState.Size = new System.Drawing.Size(48, 20);
			this.txtState.TabIndex = 17;
			this.txtState.Text = "";
			// 
			// txtCity
			// 
			this.txtCity.Location = new System.Drawing.Point(104, 136);
			this.txtCity.Name = "txtCity";
			this.txtCity.Size = new System.Drawing.Size(264, 20);
			this.txtCity.TabIndex = 16;
			this.txtCity.Text = "";
			// 
			// cboCountry
			// 
			this.cboCountry.Location = new System.Drawing.Point(104, 112);
			this.cboCountry.Name = "cboCountry";
			this.cboCountry.Size = new System.Drawing.Size(160, 21);
			this.cboCountry.TabIndex = 15;
			// 
			// txtAddressLine1
			// 
			this.txtAddressLine1.Location = new System.Drawing.Point(104, 88);
			this.txtAddressLine1.Name = "txtAddressLine1";
			this.txtAddressLine1.Size = new System.Drawing.Size(448, 20);
			this.txtAddressLine1.TabIndex = 14;
			this.txtAddressLine1.Text = "";
			// 
			// txtPhone
			// 
			this.txtPhone.Location = new System.Drawing.Point(104, 64);
			this.txtPhone.Name = "txtPhone";
			this.txtPhone.Size = new System.Drawing.Size(144, 20);
			this.txtPhone.TabIndex = 13;
			this.txtPhone.Text = "";
			// 
			// txtLastName
			// 
			this.txtLastName.Location = new System.Drawing.Point(104, 40);
			this.txtLastName.Name = "txtLastName";
			this.txtLastName.Size = new System.Drawing.Size(144, 20);
			this.txtLastName.TabIndex = 12;
			this.txtLastName.Text = "";
			// 
			// txtFirstName
			// 
			this.txtFirstName.Location = new System.Drawing.Point(104, 16);
			this.txtFirstName.Name = "txtFirstName";
			this.txtFirstName.Size = new System.Drawing.Size(144, 20);
			this.txtFirstName.TabIndex = 11;
			this.txtFirstName.Text = "";
			// 
			// lblCardExpiration
			// 
			this.lblCardExpiration.Location = new System.Drawing.Point(272, 64);
			this.lblCardExpiration.Name = "lblCardExpiration";
			this.lblCardExpiration.Size = new System.Drawing.Size(96, 16);
			this.lblCardExpiration.TabIndex = 10;
			this.lblCardExpiration.Text = "Card Expiration";
			this.lblCardExpiration.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// lblCardNumber
			// 
			this.lblCardNumber.Location = new System.Drawing.Point(272, 40);
			this.lblCardNumber.Name = "lblCardNumber";
			this.lblCardNumber.Size = new System.Drawing.Size(96, 16);
			this.lblCardNumber.TabIndex = 9;
			this.lblCardNumber.Text = "Card Number";
			this.lblCardNumber.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// lblCardType
			// 
			this.lblCardType.Location = new System.Drawing.Point(272, 16);
			this.lblCardType.Name = "lblCardType";
			this.lblCardType.Size = new System.Drawing.Size(96, 16);
			this.lblCardType.TabIndex = 8;
			this.lblCardType.Text = "Card Type";
			this.lblCardType.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// lblPostalCode
			// 
			this.lblPostalCode.Location = new System.Drawing.Point(8, 184);
			this.lblPostalCode.Name = "lblPostalCode";
			this.lblPostalCode.Size = new System.Drawing.Size(96, 16);
			this.lblPostalCode.TabIndex = 7;
			this.lblPostalCode.Text = "Postal Code";
			this.lblPostalCode.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// lblCountry
			// 
			this.lblCountry.Location = new System.Drawing.Point(8, 112);
			this.lblCountry.Name = "lblCountry";
			this.lblCountry.Size = new System.Drawing.Size(96, 16);
			this.lblCountry.TabIndex = 6;
			this.lblCountry.Text = "Country";
			this.lblCountry.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// lblState
			// 
			this.lblState.Location = new System.Drawing.Point(8, 160);
			this.lblState.Name = "lblState";
			this.lblState.Size = new System.Drawing.Size(96, 16);
			this.lblState.TabIndex = 5;
			this.lblState.Text = "State";
			this.lblState.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// lblCity
			// 
			this.lblCity.Location = new System.Drawing.Point(8, 136);
			this.lblCity.Name = "lblCity";
			this.lblCity.Size = new System.Drawing.Size(96, 16);
			this.lblCity.TabIndex = 4;
			this.lblCity.Text = "City";
			this.lblCity.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// lblAddressLine1
			// 
			this.lblAddressLine1.Location = new System.Drawing.Point(8, 88);
			this.lblAddressLine1.Name = "lblAddressLine1";
			this.lblAddressLine1.Size = new System.Drawing.Size(96, 16);
			this.lblAddressLine1.TabIndex = 3;
			this.lblAddressLine1.Text = "Address 1";
			this.lblAddressLine1.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// lblPhone
			// 
			this.lblPhone.Location = new System.Drawing.Point(8, 64);
			this.lblPhone.Name = "lblPhone";
			this.lblPhone.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.lblPhone.Size = new System.Drawing.Size(96, 16);
			this.lblPhone.TabIndex = 2;
			this.lblPhone.Text = "Phone";
			this.lblPhone.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// lblLastName
			// 
			this.lblLastName.Location = new System.Drawing.Point(8, 40);
			this.lblLastName.Name = "lblLastName";
			this.lblLastName.Size = new System.Drawing.Size(96, 16);
			this.lblLastName.TabIndex = 1;
			this.lblLastName.Text = "Last Name";
			this.lblLastName.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// lblFirstName
			// 
			this.lblFirstName.Location = new System.Drawing.Point(8, 16);
			this.lblFirstName.Name = "lblFirstName";
			this.lblFirstName.Size = new System.Drawing.Size(96, 16);
			this.lblFirstName.TabIndex = 0;
			this.lblFirstName.Text = "First Name";
			this.lblFirstName.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// CreditCardPayment
			// 
			this.Controls.Add(this.grpPaymentDetails);
			this.Name = "CreditCardPayment";
			this.Size = new System.Drawing.Size(576, 232);
			this.grpPaymentDetails.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private bool IsValid() 
		{
			bool isValid = true;
			StringBuilder buff = new StringBuilder();

			TextField[] requiredTextFields = RequiredTextFields();

			for (int x = 0; x < requiredTextFields.Length; x++) 
			{
				TextField requiredText = requiredTextFields[x];
				isValid = ValidateText(ref buff, requiredText.TextBox, requiredText.Name);
			}

			ComboField[] requiredComboFields = RequiredComboFields();

			for (int x = 0; x < requiredComboFields.Length; x++) 
			{
				ComboField requiredCombo = requiredComboFields[x];
				isValid = isValid && ValidateCombo(ref buff, requiredCombo.ComboBox, requiredCombo.Name);
			}

			if (!ValidateCreditCard()) 
			{
				isValid = false;
				buff.Append("The credit card number is invalid");
			}

			if (!isValid) 
			{
				MessageBox.Show(buff.ToString(), "Please correct the following");
			}
			return isValid;
		}

		private TextField[] RequiredTextFields() 
		{
			TextField[] required = new TextField[] {
				new TextField(txtFirstName, "First Name"),
				new TextField(txtLastName, "Last Name"),
				new TextField(txtPhone, "Phone Number"),
				new TextField(txtAddressLine1, "Address Line 1"),
				new TextField(txtCity, "City"),
				new TextField(txtState, "State"),
				new TextField(txtPostalCode, "Postal Code"),
				new TextField(txtCardNumber, "Credit Card Number")
			};
			return required;
		}

		private ComboField[] RequiredComboFields() 
		{
			ComboField[] required = new ComboField[] {
				new ComboField(cboMonth, "Month"),
				new ComboField(cboYear, "Year"),
				new ComboField(cboCountry, "Country"),
				new ComboField(cboCardType, "Card Type")
			 };
			return required;
		}

		private bool ValidateText(ref StringBuilder buff, TextBox textBox, string name) 
		{
			bool isValid = true;
			if (textBox.Text.Trim().Length == 0) 
			{
				buff.Append("Enter the " + name + "\r\n");
				isValid = false;
			}
			return isValid;
		}

		private bool ValidateCombo(ref StringBuilder buff, ComboBox comboBox, string name) 
		{
			bool isValid = true;
			if (comboBox.SelectedIndex == -1) 
			{
				buff.Append("Choose the " + name + "\r\n");
				isValid = false;
			}
			return isValid;
		}

		private bool ValidateCreditCard() 
		{
			string valueToValidate = txtCardNumber.Text;
			int indicator = 1;
			int firstNumToAdd = 0;
			int secondNumToAdd = 0;
			string num1;
			string num2;
			char[] ccArr = valueToValidate.ToCharArray();

			for (int i = ccArr.Length - 1; i >= 0; i--)
			{
				char ccNoAdd = ccArr[i];
				int ccAdd = Int32.Parse(ccNoAdd.ToString());
				if (indicator == 1)
				{
					firstNumToAdd += ccAdd;
					indicator = 0;
				} 
				else 
				{
					if ((ccAdd + ccAdd) >= 10)
					{
						int temporary = (ccAdd + ccAdd);
						num1 = temporary.ToString().Substring(0,1);
						num2 = temporary.ToString().Substring(1,1);
						secondNumToAdd += (Convert.ToInt32(num1) + Convert.ToInt32(num2));
					} 
					else
					{
						secondNumToAdd += ccAdd + ccAdd;
					}
					indicator = 1;
				}
			}
			return (firstNumToAdd + secondNumToAdd) % 10 == 0;
		}

		private void btnUpdate_Click(object sender, System.EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;

			if (IsValid()) 
			{
				try 
				{
					if (siteID == 100 || 
						siteID == 101 ||
						siteID == 102 ||
						siteID == 103 ||
						siteID == 112 )
					{
						Int32 memberTranID = CreditServiceWrapper.GetService().BeginCreditCardVerification(memberID,
							siteID,
							txtFirstName.Text,
							txtLastName.Text,
							txtPhone.Text,
							txtAddressLine1.Text,
							txtCity.Text,
							txtState.Text,
							((Country)cboCountry.SelectedItem).RegionID,
							txtPostalCode.Text,
							(CreditCardType)Enum.Parse(typeof(CreditCardType), cboCardType.SelectedItem.ToString()),
							txtCardNumber.Text,
							(int)cboMonth.SelectedItem,
							(int)cboYear.SelectedItem);

						MemberTranStatus status = MemberTranStatus.None;

						int timeOut = 0;
						do
						{
							Thread.Sleep(2000);
							status = CreditServiceWrapper.GetService().GetChargeStatus(memberID,
								memberTranID);
							if (++timeOut >= 30)
							{
								MessageBox.Show("Tranaction timed out");
								break;
							}
						}
						while (status == MemberTranStatus.Pending || status == MemberTranStatus.None);

						if (status == MemberTranStatus.Success) 
						{
							MessageBox.Show( "Payment saved.  Card authorized." );
						} 
						else 
						{
							MessageBox.Show( "Payment saved.  Unable to Authorize Credit Card" );
						}
					}
					else
					{
						CreditServiceWrapper.GetService().UpdateMemberPayment(memberID,
							siteID,
							txtFirstName.Text,
							txtLastName.Text,
							txtPhone.Text,
							txtAddressLine1.Text,
							txtCity.Text,
							txtState.Text,
							((Country)cboCountry.SelectedItem).RegionID,
							txtPostalCode.Text,
							txtCardNumber.Text,
							(int)cboMonth.SelectedItem,
							(int)cboYear.SelectedItem,
							(CreditCardType)Enum.Parse(typeof(CreditCardType), cboCardType.SelectedItem.ToString()));

						MessageBox.Show("Payment saved.  Card not authorized");
					}
				} 
				catch (Exception ex) 
				{
					MessageBox.Show( ex.Message );
				}
			}

			this.Cursor = Cursors.Default;
		}

		struct TextField 
		{
			public TextField(TextBox textBox, string name) 
			{
				TextBox = textBox;
				Name = name;
			}

			public TextBox TextBox;
			public string Name;
		}

		struct ComboField 
		{
			public ComboField(ComboBox comboBox, string name) 
			{
				ComboBox = comboBox;
				Name = name;
			}

			public ComboBox ComboBox;
			public string Name;
		}
	}
}
