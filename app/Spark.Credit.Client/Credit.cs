using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.Threading;

using Matchnet;


namespace Spark.Credit.Client
{
	public class Credit : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.GroupBox grCredit;
		private System.Windows.Forms.ComboBox cboDurationType;
		private System.Windows.Forms.TextBox txtDays;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Label lblMaxDays;
		private System.Windows.Forms.Label lblMaxAmount;
		private System.Windows.Forms.TextBox txtAmount;
		private System.Windows.Forms.Label lblDuration;
		private System.Windows.Forms.Label lblAmount;
		private System.ComponentModel.Container components = null;

		private MemberTran _memberTran = null;
		private Int32 _merchantID;
		private bool _isVoid;
		private decimal _maxAmount;
		private int _maxMinutes;
		private decimal _creditAmount;
		private int _creditDuration;
		private DurationType _creditInterval;

		public delegate void CreditCompleteEventHandler();
		public event CreditCompleteEventHandler CreditCompleted;

		public delegate void CreditCancelEventHandler();
		public event CreditCancelEventHandler CreditCancelled;


		public Credit()
		{
			InitializeComponent();
		}

		
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.grCredit = new System.Windows.Forms.GroupBox();
			this.cboDurationType = new System.Windows.Forms.ComboBox();
			this.txtDays = new System.Windows.Forms.TextBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.lblMaxDays = new System.Windows.Forms.Label();
			this.lblMaxAmount = new System.Windows.Forms.Label();
			this.txtAmount = new System.Windows.Forms.TextBox();
			this.lblDuration = new System.Windows.Forms.Label();
			this.lblAmount = new System.Windows.Forms.Label();
			this.grCredit.SuspendLayout();
			this.SuspendLayout();
			// 
			// grCredit
			// 
			this.grCredit.Controls.Add(this.cboDurationType);
			this.grCredit.Controls.Add(this.txtDays);
			this.grCredit.Controls.Add(this.btnCancel);
			this.grCredit.Controls.Add(this.btnOK);
			this.grCredit.Controls.Add(this.lblMaxDays);
			this.grCredit.Controls.Add(this.lblMaxAmount);
			this.grCredit.Controls.Add(this.txtAmount);
			this.grCredit.Controls.Add(this.lblDuration);
			this.grCredit.Controls.Add(this.lblAmount);
			this.grCredit.Location = new System.Drawing.Point(8, 8);
			this.grCredit.Name = "grCredit";
			this.grCredit.Size = new System.Drawing.Size(392, 96);
			this.grCredit.TabIndex = 1;
			this.grCredit.TabStop = false;
			// 
			// cboDurationType
			// 
			this.cboDurationType.Location = new System.Drawing.Point(168, 40);
			this.cboDurationType.Name = "cboDurationType";
			this.cboDurationType.Size = new System.Drawing.Size(88, 21);
			this.cboDurationType.TabIndex = 9;
			// 
			// txtDays
			// 
			this.txtDays.Location = new System.Drawing.Point(80, 40);
			this.txtDays.Name = "txtDays";
			this.txtDays.Size = new System.Drawing.Size(80, 20);
			this.txtDays.TabIndex = 8;
			this.txtDays.Text = "";
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(168, 64);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(88, 20);
			this.btnCancel.TabIndex = 7;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// btnOK
			// 
			this.btnOK.Location = new System.Drawing.Point(80, 64);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(80, 20);
			this.btnOK.TabIndex = 6;
			this.btnOK.Text = "OK";
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// lblMaxDays
			// 
			this.lblMaxDays.Location = new System.Drawing.Point(264, 40);
			this.lblMaxDays.Name = "lblMaxDays";
			this.lblMaxDays.Size = new System.Drawing.Size(120, 16);
			this.lblMaxDays.TabIndex = 5;
			this.lblMaxDays.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			// 
			// lblMaxAmount
			// 
			this.lblMaxAmount.Location = new System.Drawing.Point(176, 16);
			this.lblMaxAmount.Name = "lblMaxAmount";
			this.lblMaxAmount.Size = new System.Drawing.Size(120, 16);
			this.lblMaxAmount.TabIndex = 4;
			this.lblMaxAmount.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			// 
			// txtAmount
			// 
			this.txtAmount.Location = new System.Drawing.Point(80, 16);
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Size = new System.Drawing.Size(80, 20);
			this.txtAmount.TabIndex = 2;
			this.txtAmount.Text = "";
			// 
			// lblDuration
			// 
			this.lblDuration.Location = new System.Drawing.Point(8, 40);
			this.lblDuration.Name = "lblDuration";
			this.lblDuration.Size = new System.Drawing.Size(64, 16);
			this.lblDuration.TabIndex = 1;
			this.lblDuration.Text = "Duration";
			this.lblDuration.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// lblAmount
			// 
			this.lblAmount.Location = new System.Drawing.Point(8, 16);
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Size = new System.Drawing.Size(64, 16);
			this.lblAmount.TabIndex = 0;
			this.lblAmount.Text = "Amount";
			this.lblAmount.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// Credit
			// 
			this.Controls.Add(this.grCredit);
			this.Name = "Credit";
			this.Size = new System.Drawing.Size(408, 112);
			this.grCredit.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion


		public void Initialize(MemberTran memberTran,
			Int32 merchantID,
			bool isVoid)
		{
			_memberTran = memberTran;
			_merchantID = merchantID;
			grCredit.Text = isVoid ? "Void Transaction" : "Credit Transaction";
			_isVoid = isVoid;
			txtAmount.Enabled = !isVoid;

			cboDurationType.DataSource = new DurationItem[] {
																new DurationItem(DurationType.Minute),
																new DurationItem(DurationType.Hour),
																new DurationItem(DurationType.Day),
																new DurationItem(DurationType.Week),
																new DurationItem(DurationType.Month),
																new DurationItem(DurationType.Year) 
															};
			cboDurationType.SelectedIndexChanged += new EventHandler(cboDurationType_SelectedIndexChanged);

			try 
			{
				CreditMaximum creditMaximum = CreditServiceWrapper.GetService().GetCreditMaximum(memberTran.MemberTranID);

				if ( creditMaximum != null )
				{
					_maxAmount = creditMaximum.MaxAmount;
					_maxMinutes = creditMaximum.MaxMinutes;

					txtAmount.Text = System.Convert.ToString(_maxAmount);
					txtDays.Text = System.Convert.ToString(_maxMinutes);
					_creditInterval = DurationType.Minute;

					lblMaxAmount.Text = txtAmount.Text + " max";
					lblMaxDays.Text = txtDays.Text + " max";
				}
				else
				{
					MessageBox.Show("Unable to retrieve credit information", "Oops!");
				}
			} 
			catch (Exception ex) 
			{
				MessageBox.Show("There was an error getting maximum amounts for credits.\r\n" + ex.ToString(), "Oops!");
			}
		}


		private void cboDurationType_SelectedIndexChanged(object sender, EventArgs e)
		{
			try 
			{
				DurationType selectedInterval = ((DurationItem)cboDurationType.SelectedItem).DurationType;
				int duration = DurationUtil.GetMinutes(selectedInterval, _maxMinutes);
				txtDays.Text = System.Convert.ToString(duration);
				lblMaxDays.Text = duration + " max";
				_creditInterval = selectedInterval;
			} 
			catch 
			{
				MessageBox.Show("Please ensure that only valid values are entered", "Oops!");
			}
		}


		private void btnOK_Click(object sender, System.EventArgs e)
		{
			bool isValid = true;
			string message = "";
			decimal amount = 0;
			int duration = 0;
			int minutes = 0;

			if (txtAmount.Text.Trim().Length == 0) 
			{
				txtAmount.Text = "0";
			}
			if (txtDays.Text.Trim().Length == 0) 
			{
				txtDays.Text = "0";
			}

			try 
			{
				amount = System.Convert.ToDecimal(txtAmount.Text);
				duration = System.Convert.ToInt32(txtDays.Text);
			} 
			catch 
			{
				// number format exception
				isValid = false;
				message = "Amount and Days must be numeric values";
			}
			if (isValid) 
			{
				// check the maximums
				if (amount > _maxAmount || amount < 0) 
				{
					isValid = false;
					message = "Amount cannot exceed the maximum and cannot be less than zero";
				}

				if (isValid) 
				{
					minutes = DurationUtil.GetMinutes(((DurationItem)cboDurationType.SelectedItem).DurationType, 
						duration);
					if (minutes > _maxMinutes || minutes < 0) 
					{
						isValid = false;
						message = "Duration cannot exceed the maximum and cannot be less than zero";
					}
				}
			}

			if (!isValid) 
			{
				MessageBox.Show(message, "Oops!");
			} 
			else 
			{
				_creditDuration = -duration;
				_creditInterval = ((DurationItem)cboDurationType.SelectedItem).DurationType;
				_creditAmount = amount;

				performCredit(amount,
					((DurationItem)cboDurationType.SelectedItem).DurationType,
					-duration,
					_isVoid);
			}
		
			CreditCompleted();
		}


		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			CreditCancelled();
		}


		private void performCredit(decimal amount,
			DurationType creditInterval,
			int creditDuration,
			bool isVoid) 
		{
			this.Cursor = Cursors.WaitCursor;

			try 
			{
				DurationType durationType = ((DurationItem)cboDurationType.SelectedItem).DurationType;
				SubscriptionResult sr = null;
				MemberTranStatus memberTranStatus = MemberTranStatus.Success;
				Int32 memberTranID = Constants.NULL_INT;
				//TranType tranType;

				if (amount > 0) 
				{
					amount = -System.Math.Abs(amount);

					if (!isVoid) 
					{
						memberTranID = CreditServiceWrapper.GetService().BeginCredit(TranType.AdministrativeAdjustment,
							_memberTran.MemberID,
							FormMain.AdminMemberID,
							_memberTran.SiteID,
							_merchantID,
							_memberTran.MemberTranID,
							_memberTran.MemberPaymentID,
							_memberTran.PlanID,
							amount,
							DurationUtil.GetMinutes(creditInterval, creditDuration),
							creditDuration,
							durationType,
							_memberTran.CurrencyType);
						    //tranType = TranType.AdministrativeAdjustment;
					} 
					else 
					{
						memberTranID = CreditServiceWrapper.GetService().BeginCredit(TranType.Void,
							_memberTran.MemberID,
							FormMain.AdminMemberID,
							_memberTran.SiteID,
							_merchantID,
							_memberTran.MemberTranID,
							_memberTran.MemberPaymentID,
							_memberTran.PlanID,
							amount,
							DurationUtil.GetMinutes(creditInterval, creditDuration),
							creditDuration,
							durationType,
							_memberTran.CurrencyType);
						//tranType = TranType.Void;
					}

					Int32 timeOut = 0;
					do
					{
						Thread.Sleep(1000);
						sr = CreditServiceWrapper.GetService().GetMemberTranStatus(_memberTran.MemberID,memberTranID);
						memberTranStatus = sr.MemberTranStatus;
                        #region Try to perform a credit after a failed void attempt. 20071023-RB.

                        if (sr.MemberTranStatus != MemberTranStatus.Success && sr.MemberTranStatus != MemberTranStatus.Pending && sr.MemberTranStatus != MemberTranStatus.None)
                        {
                            if (isVoid)
                            {
                                memberTranID = CreditServiceWrapper.GetService().BeginCredit(TranType.AdministrativeAdjustment,
                                _memberTran.MemberID,
                                FormMain.AdminMemberID,
                                _memberTran.SiteID,
                                _merchantID,
                                _memberTran.MemberTranID,
                                _memberTran.MemberPaymentID,
                                _memberTran.PlanID,
                                amount,
                                DurationUtil.GetMinutes(creditInterval, creditDuration),
                                creditDuration,
                                durationType,
                                _memberTran.CurrencyType);
                                // set isVoid to false in order to avoid multiple attempts
                                isVoid = false;
                                //tranType = TranType.AdministrativeAdjustment;

                                //MessageBox.Show("Void attempt failed. Trying to Credit.");
                                Thread.Sleep(1000);
                                sr = CreditServiceWrapper.GetService().GetMemberTranStatus(_memberTran.MemberID, memberTranID);
                                memberTranStatus = sr.MemberTranStatus;
                            }
                        }
                        #endregion
                        if (++timeOut >= 600) 
						{
							MessageBox.Show("Transaction timed out");
							break;
						}
					}
					while (sr.MemberTranStatus == MemberTranStatus.Pending || sr.MemberTranStatus == MemberTranStatus.None);
				}
				else
				{
					//todo
					/*
					MemberTranSA.Instance.SaveCredit(memberTran.MemberID,
						FormMain.AdminMemberID,
						Constants.NULL_INT,
						memberTran.MemberTranID,
						memberTran.SiteID,
						creditDuration,
						durationType,
						amount,
						24);
					*/
				}

				if (memberTranStatus == MemberTranStatus.Success) 
				{
					MessageBox.Show("Credit Successful", "Success");
				} 
				else 
				{
					MessageBox.Show("Unable to complete credit.");
				}
			} 
			catch (Exception ex) 
			{
				MessageBox.Show(ex.Message);
			}
			finally
			{
				this.Cursor = Cursors.Default;
			}
		}
	}
}
