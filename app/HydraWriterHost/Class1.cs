using System;

using Matchnet.Data;
using Matchnet.Data.Hydra;

namespace HydraWriterHost
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class CommandLineHydraWriter
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			HydraWriter hydraWriter = new HydraWriter(args[0]);
			hydraWriter.Start();

			Console.WriteLine("Started HydraWriter for " + args[0] + "\nAny key + Enter to stop\n");
			Console.ReadLine();
			Console.WriteLine("Stopping HydraWriter ...\n ");
			hydraWriter.Stop();
			Console.WriteLine("Stopped.\n ");
		}
	}
}
