using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

namespace Spark.Monitoring.Agent.Service
{
	public class AgentService : System.ServiceProcess.ServiceBase
	{
		private System.ComponentModel.Container components = null;
		private Agent _agent;

		public AgentService()
		{
			InitializeComponent();
		}


		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new AgentService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = "Spark.Monitoring.Agent.AgentService";
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		protected override void OnStart(string[] args)
		{
			_agent = new Agent();
			_agent.Start();
		}
 
		protected override void OnStop()
		{
			_agent.Stop();
		}
	}
}
