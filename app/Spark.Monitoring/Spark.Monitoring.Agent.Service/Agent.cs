using System;
using System.Collections;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Xml;
using System.Messaging;

using Spark.Monitoring.ValueObjects;
using Spark.Monitoring.ValueObjects.ServiceDefinitions;


namespace Spark.Monitoring.Agent.Service
{
    public class Agent
    {
        private bool _runnable = false;
        private ArrayList _list;
        private ReaderWriterLock _listLock;
        private Int32 _queueEventLogsInterval;
        private Int32 _queuePerformanceCountersIntervalFloor;
        private Int32 _queuePerformanceCountersInterval;
        private string _collectorHost;
        //private PerformanceCounter[] _performanceCounters;

        const string PERFORMANCECOUNTERSCOLLECTOR_QUEUEPATH = @".\private$\PerformanceCountersCollector";
        private MessageQueue _performanceCountersCollectorQueue;
        private Thread _threadQueuePerformanceCounters;
        private Thread _threadPerformanceCountersCollector;
        private Int32 _sendPerformanceCountersToCollectorInterval;
        private long _performanceCountersCollector_maximumQueueSize = 0;

        const string EVENTLOGSCOLLECTOR_QUEUEPATH = @".\private$\EventLogsCollector";
        private MessageQueue _eventLogsCollectorQueue;
        private Thread _threadQueueEventLogs;
        private Thread _threadEventLogsCollector;
        private Int32 _sendEventLogsToCollectorInterval;
        private string _counterCollectionGroupName = String.Empty;
        private long _eventLogsCollector_maximumQueueSize = 0;

        private ArrayList _eventLogToSendToCollectorList;
        private ReaderWriterLock _eventLogToSendToCollectorListLock;

        // REMOVE
        private Int64 _logEntryCounter = 0;

        public Agent()
        {
            _list = new ArrayList();
            _listLock = new ReaderWriterLock();
            this._queueEventLogsInterval = Convert.ToInt32(ConfigurationSettings.AppSettings["QueueEventLogsInterval"]);
            this._queuePerformanceCountersIntervalFloor = Convert.ToInt32(ConfigurationSettings.AppSettings["QueuePerformanceCountersIntervalFloor"]);
            this._queuePerformanceCountersInterval = this._queuePerformanceCountersIntervalFloor;
            _collectorHost = ConfigurationSettings.AppSettings["CollectorHost"];

            this._sendEventLogsToCollectorInterval = Convert.ToInt32(ConfigurationSettings.AppSettings["SendEventLogsToCollectorInterval"]);
            this._eventLogToSendToCollectorList = new ArrayList();
            this._eventLogToSendToCollectorListLock = new ReaderWriterLock();
            this._counterCollectionGroupName = ConfigurationSettings.AppSettings["CounterCollectionGroupName"];

            this._sendPerformanceCountersToCollectorInterval = Convert.ToInt32(ConfigurationSettings.AppSettings["SendPerformanceCountersToCollectorInterval"]);

            this._performanceCountersCollector_maximumQueueSize = Convert.ToInt64(ConfigurationSettings.AppSettings["PerformanceCountersCollectorMaximumQueueSize"]);
            this._eventLogsCollector_maximumQueueSize = Convert.ToInt64(ConfigurationSettings.AppSettings["EventLogsCollectorMaximumQueueSize"]);
        }


        public void Start()
        {
            if (!MessageQueue.Exists(EVENTLOGSCOLLECTOR_QUEUEPATH))
            {
                this._eventLogsCollectorQueue = MessageQueue.Create(EVENTLOGSCOLLECTOR_QUEUEPATH, true);
            }
            else
            {
                this._eventLogsCollectorQueue = new MessageQueue(EVENTLOGSCOLLECTOR_QUEUEPATH);
            }
            this._eventLogsCollectorQueue.MaximumQueueSize = this._eventLogsCollector_maximumQueueSize;
            this._eventLogsCollectorQueue.Formatter = new BinaryMessageFormatter();
            //this._eventLogsCollectorQueue.ReceiveCompleted += new ReceiveCompletedEventHandler(this.eventLogsDBQueue_ReceiveCompleted);
            //this._eventLogsCollectorQueue.BeginReceive();

            if (!MessageQueue.Exists(PERFORMANCECOUNTERSCOLLECTOR_QUEUEPATH))
            {
                this._performanceCountersCollectorQueue = MessageQueue.Create(PERFORMANCECOUNTERSCOLLECTOR_QUEUEPATH, true);
            }
            else
            {
                this._performanceCountersCollectorQueue = new MessageQueue(PERFORMANCECOUNTERSCOLLECTOR_QUEUEPATH);
            }
            this._performanceCountersCollectorQueue.MaximumQueueSize = this._performanceCountersCollector_maximumQueueSize;
            this._performanceCountersCollectorQueue.Formatter = new BinaryMessageFormatter();

            //_performanceCounters = (PerformanceCounter[])System.Configuration.ConfigurationSettings.GetConfig("performanceCounters");

            Trace.WriteLine("Agent service started");

            this._runnable = true;

            this._threadQueuePerformanceCounters = new Thread(new ThreadStart(queuePerformanceCountersCycle));
            this._threadQueuePerformanceCounters.Start();
            this._threadQueueEventLogs = new Thread(new ThreadStart(queueEventLogsCycle));
            this._threadQueueEventLogs.Start();

            foreach (EventLog eventLog in EventLog.GetEventLogs())
            {
                Trace.WriteLine("GetEventLogs(), Source: " + eventLog.Source + ", Log: " + eventLog.Log + ", LogDisplayName: " + eventLog.LogDisplayName + ", MachineName: " + eventLog.MachineName);
                eventLog.EntryWritten += new EntryWrittenEventHandler(eventLog_EntryWritten);
                eventLog.EnableRaisingEvents = true;
            }

            this._threadEventLogsCollector = new Thread(new ThreadStart(sendEventLogsCollectorCycle));
            this._threadEventLogsCollector.Start();

            this._threadPerformanceCountersCollector = new Thread(new ThreadStart(sendPerformanceCountersCollectorCycle));
            this._threadPerformanceCountersCollector.Start();
        }


        public void Stop()
        {
            this._runnable = false;
            this._threadQueuePerformanceCounters.Join(10000);
            this._threadQueueEventLogs.Join(10000);

            this._threadEventLogsCollector.Join(10000);
            this._threadPerformanceCountersCollector.Join(10000);
        }

        private PerformanceCounter[] CreatePerformanceCounters(PerformanceCounterConfiguration[] arrPerformanceCounterConfigurations)
        {
            PerformanceCounter[] arrPerformanceCountersCreated = new PerformanceCounter[arrPerformanceCounterConfigurations.Length];
            int minimumGroupPollingInterval = -1;

            try
            {
                System.Diagnostics.Trace.WriteLine("__In Agent.CreatePerformanceCounters(), Before creating [" + arrPerformanceCounterConfigurations.Length.ToString() + "] performance counters");

                this._queuePerformanceCountersIntervalFloor = Convert.ToInt32(ConfigurationSettings.AppSettings["QueuePerformanceCountersIntervalFloor"]);

                for (int i = 0; i < arrPerformanceCounterConfigurations.Length; i++)
                {
                    PerformanceCounterConfiguration item = arrPerformanceCounterConfigurations[i];

                    if (item.Instance == String.Empty)
                    {
                        arrPerformanceCountersCreated[i] = new PerformanceCounter(item.Category,
                            item.Name,
                            true);
                    }
                    else
                    {
                        arrPerformanceCountersCreated[i] = new PerformanceCounter(item.Category,
                            item.Name,
                            item.Instance,
                            true);
                    }

                    if (minimumGroupPollingInterval == -1)
                    {
                        minimumGroupPollingInterval = item.PollingInterval;
                    }

                    if (item.PollingInterval < minimumGroupPollingInterval)
                    {
                        minimumGroupPollingInterval = item.PollingInterval;
                    }
                }

                if (minimumGroupPollingInterval > this._queuePerformanceCountersIntervalFloor)
                {
                    this._queuePerformanceCountersInterval = minimumGroupPollingInterval;
                }
                else
                {
                    this._queuePerformanceCountersInterval = this._queuePerformanceCountersIntervalFloor;
                }

                System.Diagnostics.Trace.WriteLine("__In Agent.CreatePerformanceCounters(), Created [" + arrPerformanceCounterConfigurations.Length.ToString() + "] performance counters");
                System.Diagnostics.Trace.WriteLine("__In Agent.CreatePerformanceCounters(), Performance counters polling interval: " + this._queuePerformanceCountersInterval.ToString());
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("__Error in Agent.CreatePerformanceCounters(), Error message: " + ex.Message);
                throw ex;
            }

            return arrPerformanceCountersCreated;
        }

        private void queuePerformanceCountersCycle()
        {
            PerformanceCounter[] performanceCounters = null;

            //PerformanceCounter perfCPU = new PerformanceCounter("Processor", "% Processor Time", "_Total", true);

            while (this._runnable)
            {
                // Specifies how often 2 things happen
                // 1) How often the selected performance counters are checked
                // 2) How often these performance counters are pushed into the queue
                // More performance counters can be collected by shortening this interval 
                // There is separate interval that specifies how often the performance counter items are picked up
                // from the queue and sent to the collector service    
                Thread.Sleep(this._queuePerformanceCountersInterval);

                try
                {
                    this._counterCollectionGroupName = ConfigurationSettings.AppSettings["CounterCollectionGroupName"];

                    try
                    {
                        System.Diagnostics.Trace.WriteLine("__In Agent.queuePerformanceCountersCycle(), Calling Collector.GetPerformanceCounterConfigurations(" + System.Environment.MachineName + ", " + this._counterCollectionGroupName + ")");
                        ICollectorService collectorService = (ICollectorService)Activator.GetObject(typeof(ICollectorService), "tcp://" + this._collectorHost + "/Collector.rem");
                        performanceCounters = CreatePerformanceCounters(collectorService.GetPerformanceCounterConfigurations(System.Environment.MachineName, this._counterCollectionGroupName));
                        System.Diagnostics.Trace.WriteLine("__In Agent.queuePerformanceCountersCycle(), Collector.GetPerformanceCounterConfigurations returned [" + performanceCounters.Length.ToString() + "] counters for " + System.Environment.MachineName);
                        if (performanceCounters.Length < 1)
                        {
                            System.Diagnostics.Trace.WriteLine("__In Agent.queuePerformanceCountersCycle(), No counters found so exit");
                            return;
                        }
                    }
                    catch (Exception e)
                    {
                        performanceCounters = (PerformanceCounter[])System.Configuration.ConfigurationSettings.GetConfig("performanceCounters");
                        System.Diagnostics.Trace.WriteLine("__Error in Agent.queuePerformanceCountersCycle(), Use the [" + performanceCounters.Length.ToString() + "] counters from configuration");
                    }

                    if (performanceCounters != null)
                    {
                        if (performanceCounters.Length < 1)
                        {
                            return;
                        }
                    }
                    else
                    {
                        return;
                    }

                    CounterValue[] counterValues = new CounterValue[performanceCounters.Length];

                    // Send the performance counter items to a MSMQ queue instead of directly pushing it
                    // through the collector service  
                    System.Diagnostics.Trace.WriteLine("__In Agent.queuePerformanceCountersCycle(), Before pushing the [" + performanceCounters.Length.ToString() + "] performance counter items to the PerformanceCountersCollector queue");

                    for (Int32 counterNum = 0; counterNum < performanceCounters.Length; counterNum++)
                    {
                        // The performance counters are created once
                        // At every interval check the values of these performance counters  
                        PerformanceCounter counter = performanceCounters[counterNum];
                        counterValues[counterNum] = new CounterValue(counter.CategoryName,
                            counter.CounterName,
                            counter.InstanceName,
                            counter.NextValue(),
                            DateTime.Now);
                    }

                    MessageQueue queue = Matchnet.Queuing.Util.GetQueue(Agent.PERFORMANCECOUNTERSCOLLECTOR_QUEUEPATH, true, true);
                    queue.Formatter = new BinaryMessageFormatter();
                    foreach (object item in counterValues)
                    {
                        CounterValue counterValueItem = (CounterValue)item;
                        queue.Send(counterValueItem, MessageQueueTransactionType.Single);
                    }

                    System.Diagnostics.Trace.WriteLine("__In Agent.queuePerformanceCountersCycle(), After pushing the [" + performanceCounters.Length.ToString() + "] performance counter items to the PerformanceCountersCollector queue");
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine("__" + ex.ToString());
                    Thread.Sleep(1000);
                }
                finally
                {
                    performanceCounters = null;
                }
            }
        }

        private void queueEventLogsCycle()
        {
            ArrayList list = new ArrayList();

            while (this._runnable)
            {
                // Specifies how often the event logs that are collected into memory is moved into the queue
                // Shortening the interval will make it less likely that the event logs are lost if the
                // server memory is cleared  
                // The event logs are collected into memory whenever they are created so there is no 
                // separate interval for collecting the new event logs into memory.
                // There is separate interval that specifies how often the event log items are picked up
                // from the queue and sent to the collector service    
                Thread.Sleep(this._queueEventLogsInterval);
                try
                {
                    // Event logs that are collected in memory need to be moved into the queue
                    // But first these logs need to be copied into a local array so that this._list
                    // is not locked out.  If this._list is locked out while trying to move logs 
                    // into the queue, then the other thread cannot insert logs into this._list. 
                    // There could be a chance that the logs never make it into the queue and
                    // will be lost since the logs do not build up in this._list and immediately 
                    // moved into a local array.  If the log is kept into this._list and the queue
                    // has problems such as permission restrictions, than the logs may consume too
                    // much server memory and cause server problems.  
                    lock (_list.SyncRoot)
                    {
                        if (_list.Count > 0)
                        {
                            System.Diagnostics.Trace.WriteLine("__In Agent.queueEventLogsCycle(), Copy event logs from global to local array, count: " + _list.Count.ToString());
                            // At every interval copy the event log items from this._list array
                            // to a local array before clearing out the this_list array  
                            list.AddRange(_list);
                        }

                        // In any case clear out this._list array once all event log items have
                        // been copied over to the local list array  
                        _list.Clear();
                    }

                    if (list.Count > 0)
                    {
                        try
                        {
                            // Send the event log items to a MSMQ queue instead of directly pushing it
                            // through the collector service  
                            System.Diagnostics.Trace.WriteLine("__In Agent.queueEventLogsCycle(), Before pushing the event log items to the EventLogsCollector queue, Total to queue up: " + list.Count.ToString());

                            MessageQueue queue = Matchnet.Queuing.Util.GetQueue(Agent.EVENTLOGSCOLLECTOR_QUEUEPATH, true, true);
                            queue.Formatter = new BinaryMessageFormatter();
                            foreach (object item in list)
                            {
                                LogEntry logEntryItem = (LogEntry)item;
                                queue.Send(logEntryItem, MessageQueueTransactionType.Single);

                                //LogEntry logEntryItem = item as LogEntry;
                                //if (logEntryItem != null)
                                //{
                                //    queue.Send(logEntryItem, MessageQueueTransactionType.Single);
                                //}
                            }

                            System.Diagnostics.Trace.WriteLine("__In Agent.queueEventLogsCycle(), After pushing the event log items to the EventLogsCollector queue, Total queued up: " + list.Count.ToString());

                            /*
                            System.Diagnostics.Trace.WriteLine("Before pushing the events to the collector service");

							ICollectorService collectorService = (ICollectorService)Activator.GetObject(typeof(ICollectorService), "tcp://" + _collectorHost + "/Collector.rem");
                            
                            collectorService.PublishLogValues((LogEntry[])list.ToArray(typeof(LogEntry)));

                            System.Diagnostics.Trace.WriteLine("After pushing the events to the collector service");
                            */

                            //System.Diagnostics.Trace.WriteLine("__agent PublishLogValues");
                            list.Clear();
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Trace.WriteLine("__" + ex.ToString());
                            while (list.Count > 1000)
                            {
                                list.RemoveAt(0);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine("__" + ex.ToString());
                    Thread.Sleep(1000);
                }
            }
        }


        private void eventLog_EntryWritten(object sender, EntryWrittenEventArgs e)
        {
            // Whenever a new event log is created, add this event log to the this._list 
            // array
            lock (_list.SyncRoot)
            {
                //System.Diagnostics.Trace.WriteLine("__agent eventLog_EntryWritten");
                EventLogEntry eventLogEntry = e.Entry;

                _list.Add(new LogEntry(System.Environment.MachineName,
                    eventLogEntry.Source,
                    eventLogEntry.Category,
                    (EntryType)Enum.Parse(typeof(EntryType), eventLogEntry.EntryType.ToString()),
                    eventLogEntry.TimeGenerated,
                    eventLogEntry.Message));
            }
        }

        private void sendPerformanceCountersCollectorCycle()
        {
            ArrayList counterValues = new ArrayList();
            //CounterValue[] counterValues;

            while (this._runnable)
            {
                try
                {
                    // Every 5 seconds get as many of the event log items from the queue  
                    Thread.Sleep(this._sendPerformanceCountersToCollectorInterval);

                    Message message = null;

                    System.Diagnostics.Trace.WriteLine("__In Agent.sendPerformanceCountersCollectorCycle(), Performance counters queue count: " + this._performanceCountersCollectorQueue.GetAllMessages().Length.ToString());

                    if (!IsQueueEmpty(this._performanceCountersCollectorQueue))
                    {
                        using (MessageQueueTransaction mqt = new MessageQueueTransaction())
                        {
                            try
                            {
                                mqt.Begin();

                                //counterValues = new CounterValue[this._performanceCountersCollectorQueue.GetAllMessages().Length];

                                // Collect at most 50 performance counter items from the queue at a time
                                for (int i = 0; (i < 1000 && (!IsQueueEmpty(this._performanceCountersCollectorQueue))); i++)
                                {
                                    message = this._performanceCountersCollectorQueue.Receive(mqt);
                                    if (message != null)
                                    {
                                        counterValues.Add((CounterValue)message.Body);
                                        //counterValues[i] = (CounterValue)message.Body;
                                    }
                                }

                                bool performanceCountersSuccessfullyProcessedInCollectorService = false;

                                if (counterValues.Count > 0)
                                {
                                    // Once the performance counter are retrieved from the queue, push them through the collector service
                                    // At most only 50 performance counter items are collected and pushed through the collector service  
                                    System.Diagnostics.Trace.WriteLine("__In Agent.sendPerformanceCountersCollectorCycle() within the MSMQ transaction, Before pushing the collection of the [" + counterValues.Count.ToString() + "] queued event logs to the collector service");
                                    ICollectorService collectorService = (ICollectorService)Activator.GetObject(typeof(ICollectorService), "tcp://" + _collectorHost + "/Collector.rem");
                                    // A connection error to the collector service will immediately generate an error when calling Collector.PublishLogValues()
                                    performanceCountersSuccessfullyProcessedInCollectorService = collectorService.PublishCounterValuesWithCompletionStatus(System.Environment.MachineName, (CounterValue[])counterValues.ToArray(typeof(CounterValue)));
                                    System.Diagnostics.Trace.WriteLine("__In Agent.sendPerformanceCountersCollectorCycle() within the MSMQ transaction, Return value in Collector.PublishCounterValuesWithCompletionStatus(): " + (performanceCountersSuccessfullyProcessedInCollectorService ? "true" : "false"));
                                    if (performanceCountersSuccessfullyProcessedInCollectorService)
                                    {
                                        // Error in Collector.PublishCounterValuesWithCompletionStatus()
                                        System.Diagnostics.Trace.WriteLine("__In Agent.sendPerformanceCountersCollectorCycle() within the MSMQ transaction, After successfully processing the collection of the performance counters to the collector service");
                                    }
                                    else
                                    {
                                        // No errors in Collector.PublishCounterValuesWithCompletionStatus()
                                        System.Diagnostics.Trace.WriteLine("__In Agent.sendPerformanceCountersCollectorCycle() within the MSMQ transaction, Error in processsing the performance counters in the collector service");
                                        throw new Exception("Error in processing the event logs in Collector.PublishLogValues()");
                                    }
                                }
                                else
                                {
                                    System.Diagnostics.Trace.WriteLine("__In Agent.sendPerformanceCountersCollectorCycle() within the MSMQ transaction, No performance counters retrieved from the queue to send to the collector service.");
                                }

                                mqt.Commit();
                            }
                            catch (Exception ex)
                            {
                                // A connection error to the collector service will be captured here when calling Collector.PublishCounterValuesWithCompletionStatus()
                                mqt.Abort();
                                System.Diagnostics.Trace.WriteLine("__Error in Agent.sendPerformanceCountersCollectorCycle(), Transaction for receiving performance counters from the queue for sending to collector is aborted, Error message: " + ex.Message);
                            }
                            finally
                            {
                                if (counterValues.Count > 0)
                                {
                                    counterValues.Clear();
                                }
                                //counterValues = null;
                            }
                        }
                    }
                    else
                    {
                        System.Diagnostics.Trace.WriteLine("__In Agent.sendPerformanceCountersCollectorCycle(), No performance counters retrieved from the queue to send to the collector service.");
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine("__Error in Agent.sendPerformanceCountersCollectorCycle(), Error message: " + ex.Message);
                }
            }
        }

        private void sendEventLogsCollectorCycle()
        {
            ArrayList eventLogToSendToCollectorListCopy = new ArrayList();

            while (this._runnable)
            {
                try
                {
                    // Every 5 seconds get as many of the event log items from the queue  
                    Thread.Sleep(this._sendEventLogsToCollectorInterval);

                    Message message = null;

                    System.Diagnostics.Trace.WriteLine("__In Agent.sendEventLogsCollectorCycle(), Event logs queue count: " + this._eventLogsCollectorQueue.GetAllMessages().Length.ToString());

                    if (!IsQueueEmpty(this._eventLogsCollectorQueue))
                    {
                        using (MessageQueueTransaction mqt = new MessageQueueTransaction())
                        {
                            try
                            {
                                mqt.Begin();

                                // Collect at most 50 event log items from the queue at a time
                                for (int i = 0; (i < 1000 && (!IsQueueEmpty(this._eventLogsCollectorQueue))); i++)
                                {
                                    message = this._eventLogsCollectorQueue.Receive(mqt);
                                    if (message != null)
                                    {
                                        eventLogToSendToCollectorListCopy.Add((LogEntry)message.Body);
                                    }
                                }

                                bool eventLogsSuccessfullyProcessedInCollectorService = false;

                                if (eventLogToSendToCollectorListCopy.Count > 0)
                                {
                                    // Once the event logs are retrieved from the queue, push them through the collector service
                                    // At most only 50 event log items are collected and pushed through the collector service  
                                    System.Diagnostics.Trace.WriteLine("__In Agent.sendEventLogsCollectorCycle() within the MSMQ transaction, Before pushing the collection of the queued event logs to the collector service, Total count to send: " + eventLogToSendToCollectorListCopy.Count.ToString());
                                    ICollectorService collectorService = (ICollectorService)Activator.GetObject(typeof(ICollectorService), "tcp://" + _collectorHost + "/Collector.rem");
                                    // A connection error to the collector service will immediately generate an error when calling Collector.PublishLogValues()
                                    eventLogsSuccessfullyProcessedInCollectorService = collectorService.PublishLogValuesWithCompletionStatus((LogEntry[])eventLogToSendToCollectorListCopy.ToArray(typeof(LogEntry)));
                                    System.Diagnostics.Trace.WriteLine("__In Agent.sendEventLogsCollectorCycle() within the MSMQ transaction, Return value in Collector.PublishLogValues(): " + (eventLogsSuccessfullyProcessedInCollectorService ? "true" : "false"));
                                    if (eventLogsSuccessfullyProcessedInCollectorService)
                                    {
                                        // Error in Collector.PublishLogValues()
                                        System.Diagnostics.Trace.WriteLine("__In Agent.sendEventLogsCollectorCycle() within the MSMQ transaction, After successfully processing the collection of the queued event logs to the collector service");
                                    }
                                    else
                                    {
                                        // No errors in Collector.PublishLogValues()
                                        System.Diagnostics.Trace.WriteLine("__In Agent.sendEventLogsCollectorCycle() within the MSMQ transaction, Error in processsing the event logs in the collector service");
                                        throw new Exception("Error in processing the event logs in Collector.PublishLogValues()");
                                    }
                                }
                                else
                                {
                                    System.Diagnostics.Trace.WriteLine("__In Agent.sendEventLogsCollectorCycle() within the MSMQ transaction, No event logs retrieved from the queue to send to the collector service.");
                                }

                                mqt.Commit();
                            }
                            catch (Exception ex)
                            {
                                // A connection error to the collector service will be captured here when calling Collector.PublishLogValues()
                                mqt.Abort();
                                System.Diagnostics.Trace.WriteLine("__Error in Agent.sendEventLogsCollectorCycle(), Transaction for receiving error logs from the queue for sending to collector is aborted, Error message: " + ex.Message);
                            }
                            finally
                            {
                                if (eventLogToSendToCollectorListCopy.Count > 0)
                                {
                                    eventLogToSendToCollectorListCopy.Clear();
                                }
                            }
                        }
                    }
                    else
                    {
                        System.Diagnostics.Trace.WriteLine("__In Agent.sendEventLogsCollectorCycle(), No event logs retrieved from the queue to send to the collector service.");
                    }

                    // REMOVE
                    //for (int addEntryCounter = 0; addEntryCounter < 500; addEntryCounter++)
                    //{
                    //    this._logEntryCounter++;

                    //    if (!System.Diagnostics.EventLog.SourceExists("AgentService"))
                    //    {
                    //        System.Diagnostics.EventLog.CreateEventSource(
                    //           "AgentService", "Microsoft Office Diagnostics");
                    //    }
                    //    EventLog EventLog1 = new EventLog();
                    //    EventLog1.Source = "AgentService";
                    //    EventLog1.WriteEntry("[" + this._logEntryCounter.ToString() + "] entry");
                    //}



                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine("__Error in Agent.sendEventLogsCollectorCycle(), Error message: " + ex.Message);
                }
            }
        }

        private void eventLogsDBQueue_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                using (MessageQueueTransaction tran = new MessageQueueTransaction())
                {
                    tran.Begin();

                    MessageQueue mq = (MessageQueue)sender;

                    Message message = mq.EndReceive(e.AsyncResult);

                    lock (this._eventLogToSendToCollectorList.SyncRoot)
                    {
                        this._eventLogToSendToCollectorList.Add((LogEntry)message.Body);
                    }

                    mq.Receive();
                    return;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("__Error in eventLogsDBQueue_ReceiveCompleted, " + ex.ToString().Replace("/r", "").Replace("/n", ""));
            }
        }

        private bool IsQueueEmpty(MessageQueue msmq)
        {
            bool isQueueEmpty = false;

            try
            {
                // Set Peek to return immediately.
                msmq.Peek(new TimeSpan(0));

                // If an IOTimeout was not thrown, there is a message 
                // in the queue.
                isQueueEmpty = false;
            }

            catch (MessageQueueException e)
            {
                if (e.MessageQueueErrorCode ==
                    MessageQueueErrorCode.IOTimeout)
                {
                    // No message was in the queue.
                    isQueueEmpty = true;
                }
            }

            // Return true if there are no messages in the queue.
            return isQueueEmpty;
        }
    }
}
