using System;
using System.Configuration;
using System.Diagnostics;
using System.Xml;


namespace Spark.Monitoring.Agent.Service
{
	public class PerfConfigHandler : IConfigurationSectionHandler
	{
		public object Create(object parent,
			object configContext,
			XmlNode section)
		{
			PerformanceCounter[] performanceCounters = new PerformanceCounter[section.ChildNodes.Count];
			
			for (Int32 nodeNum = 0; nodeNum < section.ChildNodes.Count; nodeNum++)
			{
				XmlNode counterNode = section.ChildNodes[nodeNum];

				string categoryName = counterNode.Attributes["categoryName"].Value;
				string counterName = counterNode.Attributes["counterName"].Value;

				if (counterNode.Attributes["instanceName"] != null)
				{
					performanceCounters[nodeNum] = new PerformanceCounter(categoryName,
						counterName,
						counterNode.Attributes["instanceName"].Value,
						true);
				}
				else
				{
					performanceCounters[nodeNum] = new PerformanceCounter(categoryName,
						counterName,
						true);
				}

			}

			System.Diagnostics.Trace.WriteLine("__Create() " + performanceCounters.Length.ToString());

			return performanceCounters;
		}
	}
}
