using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using System.Threading;
using System.IO;


namespace Spark.Monitoring.Client
{
	public class MonitoringClient : System.Windows.Forms.Form
	{
		private System.ComponentModel.Container components = null;

		private static MonitoringClient _monitoringClient;
        private static EventTextWriter eventTextWriter = null;
		private static DataSet _ds;
		private static DataTable _dtBg;
		private static DataView _dv;
		private static Guid _currentGuid;
		private static MethodInvoker CallDataBindToDataGrid = new MethodInvoker(DataBindToDataGrid);

		private Thread _threadRefresh;
		private bool _runnable = false;
		private ClientAPI _clientAPI;
		private System.Windows.Forms.DataGrid gridLogEntry;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.StatusBar statusBar;
		private System.Windows.Forms.StatusBarPanel statusBarPanel1;
		private System.Windows.Forms.TabPage tabPageEvents;
		private System.Windows.Forms.TabPage tabPageCounters;
		private System.Windows.Forms.TabPage tabPageHydra;
		private System.Windows.Forms.TextBox txtLogDetail;
		private Guid _clientID = Guid.NewGuid();
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel panelGraph;
		private System.Windows.Forms.GroupBox groupBoxAddCounter;
		private System.Windows.Forms.ComboBox dropDownMachine;
		private System.Windows.Forms.ComboBox dropDownCounter;
		private System.Windows.Forms.Button buttonAddCounter;
		private System.Windows.Forms.DataGrid gridCounters;
		private DataGridTextBoxColumn _colMessage;
		private System.Windows.Forms.Label lblFilter;
		private System.Windows.Forms.ComboBox comboBoxFilter;
		private System.Windows.Forms.Panel panelHydra;
        private CheckBox checkBoxLogToFile;
        private SaveFileDialog saveFileDialogEventLog;
		private DataSet _dsCounters;

		public MonitoringClient()
		{
			InitializeComponent();

			try
			{
				comboBoxFilter.Items.Add(new Filter("<none>", ""));
				comboBoxFilter.Items.Add(new Filter("web/middle tier errors", "(Source = 'www' or Source like 'Matchnet.*' or Source like 'FAST*') and EntryType = " + ((Int32)EntryType.Error).ToString()));
				comboBoxFilter.Items.Add(new Filter("web/middle tier errors (without IM 'INVALID')", "(Source = 'www' or Source like 'Matchnet.*' or Source like 'FAST*') and Message not like '%''INVALID''%' and EntryType = " + ((Int32)EntryType.Error).ToString()));
				comboBoxFilter.SelectedIndex = 1;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}

			this.comboBoxFilter.SelectedIndexChanged += new System.EventHandler(this.comboBoxFilter_SelectedIndexChanged);

			//event log data
			_ds = new DataSet();
			DataTable dt = new DataTable("foobar");
			dt.PrimaryKey = new DataColumn[]{dt.Columns.Add("Guid", typeof(Guid))};
			dt.Columns.Add("Machine", typeof(string));
			dt.Columns.Add("Source", typeof(string));
			dt.Columns.Add("Category", typeof(string));
			dt.Columns.Add("EntryType", typeof(EntryType));
			dt.Columns.Add("DateTime", typeof(DateTime));
			dt.Columns.Add("Message", typeof(string));
			dt.DefaultView.Sort = "DateTime desc";
			_ds.Tables.Add(dt);

			_dtBg = dt.Clone();
			_dtBg.DefaultView.Sort = "DateTime desc";

			_dv = new DataView(_ds.Tables[0], ((Filter)comboBoxFilter.Items[comboBoxFilter.SelectedIndex]).Text, "DateTime desc", DataViewRowState.CurrentRows);
			//_dv = new DataView(_ds.Tables[0], "", "DateTime desc", DataViewRowState.CurrentRows);
			gridLogEntry.SetDataBinding(_dv, "");
			setEventGridStyle();

			//counter data
			_dsCounters = new DataSet();
			_dsCounters.Tables.Add(new DataTable("counters"));
			_dsCounters.Tables[0].Columns.Add("MachineName", typeof(string));
			_dsCounters.Tables[0].Columns.Add("Counter", typeof(string));
			_dsCounters.Tables[0].Columns.Add("Scale", typeof(Int32));
			_dsCounters.Tables[0].Columns.Add("Color", typeof(Color));
			_dsCounters.Tables[0].Columns.Add("Values", typeof(ArrayList));
			gridCounters.SetDataBinding(_dsCounters, "counters");
			setCounterGridStyle();


			gridLogEntry.CurrentCellChanged += new EventHandler(gridLogEntry_CurrentCellChanged);

			this.Resize += new EventHandler(MonitoringClient_Resize);

			_clientAPI = new ClientAPI();
			string url = "";

			if (AppDomain.CurrentDomain.BaseDirectory.IndexOf("http://") != 0)
			{
				url = System.Configuration.ConfigurationSettings.AppSettings["url"];
			}
			else
			{
				url = AppDomain.CurrentDomain.BaseDirectory + "../ClientAPI.asmx";
			}
			_clientAPI.Url = url;
			_clientAPI.Timeout = 2000;

			_threadRefresh = new Thread(new ThreadStart(workCycle));
			_runnable = true;
			_threadRefresh.Start();

			tabControl1.SelectedIndexChanged += new EventHandler(tabControl1_SelectedIndexChanged);
			dropDownMachine.SelectedIndexChanged += new EventHandler(dropDownMachine_SelectedIndexChanged);
			panelGraph.Paint += new PaintEventHandler(panelGraph_Paint);
			panelHydra.Paint += new PaintEventHandler(panelHydra_Paint);
		}


		private void setEventGridStyle()
		{
			DataGridTableStyle ts = new DataGridTableStyle(); 
			ts.ReadOnly = true;
			ts.RowHeadersVisible = false;
  
			DataGridTextBoxColumn colMachine = new DataGridTextBoxColumn(); 
			colMachine.MappingName = "Machine";		
			colMachine.HeaderText = "Machine"; 
			colMachine.Width = 120;
			colMachine.ReadOnly = true;
			ts.GridColumnStyles.Add(colMachine); 
			
			DataGridTextBoxColumn colSource = new DataGridTextBoxColumn(); 
			colSource.MappingName = "Source";		
			colSource.HeaderText = "Source"; 
			colSource.Width = 160;
			colSource.ReadOnly = true;
			ts.GridColumnStyles.Add(colSource); 
			
			DataGridTextBoxColumn colCategory = new DataGridTextBoxColumn(); 
			colCategory.MappingName = "Category";		
			colCategory.HeaderText = "Category"; 
			colCategory.Width = 80;
			colCategory.ReadOnly = true;
			ts.GridColumnStyles.Add(colCategory); 

			DataGridEntryTypeColumn colEntryType = new DataGridEntryTypeColumn(); 
			colEntryType.MappingName = "EntryType";		
			colEntryType.HeaderText = "Entry Type"; 
			colEntryType.Width = 95;
			colEntryType.ReadOnly = true;
			ts.GridColumnStyles.Add(colEntryType);

			DataGridTextBoxColumn colDateTime = new DataGridTextBoxColumn(); 
			colDateTime.MappingName = "DateTime";		
			colDateTime.HeaderText = "DateTime"; 
			colDateTime.Format = "MM/dd/yyyy hh:mm:sstt";
			colDateTime.Width = 128;
			colDateTime.ReadOnly = true;
			ts.GridColumnStyles.Add(colDateTime); 

			_colMessage = new DataGridTextBoxColumn(); 
			_colMessage.MappingName = "Message";		
			_colMessage.HeaderText = "Message"; 
			//_colMessage.Width = 460;
			_colMessage.ReadOnly = true;
			ts.GridColumnStyles.Add(_colMessage); 

			ts.MappingName = "foobar";
			
			gridLogEntry.TableStyles.Clear();
			gridLogEntry.TableStyles.Add(ts);
		}


		private void setCounterGridStyle()
		{
			DataGridTableStyle ts = new DataGridTableStyle(); 
			ts.ReadOnly = true;
			ts.RowHeadersVisible = false;
  
			DataGridTextBoxColumn colMachine = new DataGridTextBoxColumn(); 
			colMachine.MappingName = "MachineName";
			colMachine.HeaderText = "Machine"; 
			colMachine.Width = 120;
			colMachine.ReadOnly = true;
			ts.GridColumnStyles.Add(colMachine); 
			
			DataGridTextBoxColumn colCounter = new DataGridTextBoxColumn(); 
			colCounter.MappingName = "Counter";		
			colCounter.HeaderText = "Counter"; 
			colCounter.Width = 350;
			colCounter.ReadOnly = true;
			ts.GridColumnStyles.Add(colCounter); 
			
			DataGridTextBoxColumn colScale = new DataGridTextBoxColumn(); 
			colScale.MappingName = "Scale";		
			colScale.HeaderText = "Scale"; 
			colScale.Width = 80;
			colScale.ReadOnly = true;
			ts.GridColumnStyles.Add(colScale); 


			DataTable dtColor = new DataTable();
			dtColor.Columns.Add("Color", typeof(Color));
			dtColor.Columns.Add("ColorName", typeof(string));
			dtColor.Rows.Add(new object[]{Color.Black, Color.Black.ToString()});
			dtColor.Rows.Add(new object[]{Color.Green, Color.Green.ToString()});
			_dsCounters.Tables.Add(dtColor);

			DataGridComboBoxColumn colColor = new DataGridComboBoxColumn("Color", dtColor, "ColorName", "Color", gridCounters); 
			colColor.MappingName = "Color";		
			colColor.HeaderText = "Color"; 
			colColor.Width = 80;
			colColor.ReadOnly = true;
			ts.GridColumnStyles.Add(colColor);

			ts.MappingName = "counters";

			gridCounters.TableStyles.Clear();
			gridCounters.TableStyles.Add(ts);
		}


		protected override void Dispose( bool disposing )
		{
			_runnable = false;
			_threadRefresh.Join(5000);

			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MonitoringClient));
            this.gridLogEntry = new System.Windows.Forms.DataGrid();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageEvents = new System.Windows.Forms.TabPage();
            this.checkBoxLogToFile = new System.Windows.Forms.CheckBox();
            this.comboBoxFilter = new System.Windows.Forms.ComboBox();
            this.lblFilter = new System.Windows.Forms.Label();
            this.txtLogDetail = new System.Windows.Forms.TextBox();
            this.tabPageCounters = new System.Windows.Forms.TabPage();
            this.groupBoxAddCounter = new System.Windows.Forms.GroupBox();
            this.buttonAddCounter = new System.Windows.Forms.Button();
            this.dropDownCounter = new System.Windows.Forms.ComboBox();
            this.dropDownMachine = new System.Windows.Forms.ComboBox();
            this.gridCounters = new System.Windows.Forms.DataGrid();
            this.panelGraph = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPageHydra = new System.Windows.Forms.TabPage();
            this.panelHydra = new System.Windows.Forms.Panel();
            this.statusBar = new System.Windows.Forms.StatusBar();
            this.statusBarPanel1 = new System.Windows.Forms.StatusBarPanel();
            this.saveFileDialogEventLog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.gridLogEntry)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPageEvents.SuspendLayout();
            this.tabPageCounters.SuspendLayout();
            this.groupBoxAddCounter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCounters)).BeginInit();
            this.tabPageHydra.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridLogEntry
            // 
            this.gridLogEntry.AllowNavigation = false;
            this.gridLogEntry.AllowSorting = false;
            this.gridLogEntry.CaptionVisible = false;
            this.gridLogEntry.CausesValidation = false;
            this.gridLogEntry.DataMember = "";
            this.gridLogEntry.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.gridLogEntry.Location = new System.Drawing.Point(0, 32);
            this.gridLogEntry.Name = "gridLogEntry";
            this.gridLogEntry.ReadOnly = true;
            this.gridLogEntry.RowHeadersVisible = false;
            this.gridLogEntry.Size = new System.Drawing.Size(1104, 328);
            this.gridLogEntry.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageEvents);
            this.tabControl1.Controls.Add(this.tabPageCounters);
            this.tabControl1.Controls.Add(this.tabPageHydra);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1141, 653);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPageEvents
            // 
            this.tabPageEvents.Controls.Add(this.checkBoxLogToFile);
            this.tabPageEvents.Controls.Add(this.comboBoxFilter);
            this.tabPageEvents.Controls.Add(this.lblFilter);
            this.tabPageEvents.Controls.Add(this.txtLogDetail);
            this.tabPageEvents.Controls.Add(this.gridLogEntry);
            this.tabPageEvents.Location = new System.Drawing.Point(4, 22);
            this.tabPageEvents.Name = "tabPageEvents";
            this.tabPageEvents.Size = new System.Drawing.Size(1133, 627);
            this.tabPageEvents.TabIndex = 0;
            this.tabPageEvents.Text = "Event Logs";
            // 
            // checkBoxLogToFile
            // 
            this.checkBoxLogToFile.AutoSize = true;
            this.checkBoxLogToFile.Location = new System.Drawing.Point(527, 9);
            this.checkBoxLogToFile.Name = "checkBoxLogToFile";
            this.checkBoxLogToFile.Size = new System.Drawing.Size(72, 17);
            this.checkBoxLogToFile.TabIndex = 4;
            this.checkBoxLogToFile.Text = "Log to file";
            this.checkBoxLogToFile.UseVisualStyleBackColor = true;
            this.checkBoxLogToFile.CheckedChanged += new System.EventHandler(this.checkBoxLogToFile_CheckedChanged);
            // 
            // comboBoxFilter
            // 
            this.comboBoxFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFilter.Location = new System.Drawing.Point(40, 5);
            this.comboBoxFilter.Name = "comboBoxFilter";
            this.comboBoxFilter.Size = new System.Drawing.Size(256, 21);
            this.comboBoxFilter.TabIndex = 3;
            // 
            // lblFilter
            // 
            this.lblFilter.AutoSize = true;
            this.lblFilter.Location = new System.Drawing.Point(8, 8);
            this.lblFilter.Name = "lblFilter";
            this.lblFilter.Size = new System.Drawing.Size(32, 13);
            this.lblFilter.TabIndex = 2;
            this.lblFilter.Text = "Filter:";
            // 
            // txtLogDetail
            // 
            this.txtLogDetail.Location = new System.Drawing.Point(0, 360);
            this.txtLogDetail.Multiline = true;
            this.txtLogDetail.Name = "txtLogDetail";
            this.txtLogDetail.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLogDetail.Size = new System.Drawing.Size(288, 150);
            this.txtLogDetail.TabIndex = 1;
            // 
            // tabPageCounters
            // 
            this.tabPageCounters.Controls.Add(this.groupBoxAddCounter);
            this.tabPageCounters.Controls.Add(this.gridCounters);
            this.tabPageCounters.Controls.Add(this.panelGraph);
            this.tabPageCounters.Controls.Add(this.label1);
            this.tabPageCounters.Location = new System.Drawing.Point(4, 22);
            this.tabPageCounters.Name = "tabPageCounters";
            this.tabPageCounters.Size = new System.Drawing.Size(1133, 627);
            this.tabPageCounters.TabIndex = 1;
            this.tabPageCounters.Text = "Perf Counters";
            // 
            // groupBoxAddCounter
            // 
            this.groupBoxAddCounter.Controls.Add(this.buttonAddCounter);
            this.groupBoxAddCounter.Controls.Add(this.dropDownCounter);
            this.groupBoxAddCounter.Controls.Add(this.dropDownMachine);
            this.groupBoxAddCounter.Location = new System.Drawing.Point(8, 8);
            this.groupBoxAddCounter.Name = "groupBoxAddCounter";
            this.groupBoxAddCounter.Size = new System.Drawing.Size(584, 48);
            this.groupBoxAddCounter.TabIndex = 8;
            this.groupBoxAddCounter.TabStop = false;
            this.groupBoxAddCounter.Text = "Counters";
            // 
            // buttonAddCounter
            // 
            this.buttonAddCounter.Enabled = false;
            this.buttonAddCounter.Location = new System.Drawing.Point(528, 16);
            this.buttonAddCounter.Name = "buttonAddCounter";
            this.buttonAddCounter.Size = new System.Drawing.Size(48, 23);
            this.buttonAddCounter.TabIndex = 8;
            this.buttonAddCounter.Text = "add";
            this.buttonAddCounter.Click += new System.EventHandler(this.buttonAddCounter_Click);
            // 
            // dropDownCounter
            // 
            this.dropDownCounter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropDownCounter.Location = new System.Drawing.Point(168, 16);
            this.dropDownCounter.Name = "dropDownCounter";
            this.dropDownCounter.Size = new System.Drawing.Size(352, 21);
            this.dropDownCounter.Sorted = true;
            this.dropDownCounter.TabIndex = 5;
            // 
            // dropDownMachine
            // 
            this.dropDownMachine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropDownMachine.Location = new System.Drawing.Point(8, 16);
            this.dropDownMachine.Name = "dropDownMachine";
            this.dropDownMachine.Size = new System.Drawing.Size(152, 21);
            this.dropDownMachine.Sorted = true;
            this.dropDownMachine.TabIndex = 4;
            // 
            // gridCounters
            // 
            this.gridCounters.AllowSorting = false;
            this.gridCounters.CaptionVisible = false;
            this.gridCounters.DataMember = "";
            this.gridCounters.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.gridCounters.Location = new System.Drawing.Point(8, 64);
            this.gridCounters.Name = "gridCounters";
            this.gridCounters.Size = new System.Drawing.Size(1120, 104);
            this.gridCounters.TabIndex = 6;
            // 
            // panelGraph
            // 
            this.panelGraph.AutoScroll = true;
            this.panelGraph.BackColor = System.Drawing.Color.White;
            this.panelGraph.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelGraph.Location = new System.Drawing.Point(8, 176);
            this.panelGraph.Name = "panelGraph";
            this.panelGraph.Size = new System.Drawing.Size(1120, 448);
            this.panelGraph.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 1;
            // 
            // tabPageHydra
            // 
            this.tabPageHydra.Controls.Add(this.panelHydra);
            this.tabPageHydra.Location = new System.Drawing.Point(4, 22);
            this.tabPageHydra.Name = "tabPageHydra";
            this.tabPageHydra.Size = new System.Drawing.Size(1133, 627);
            this.tabPageHydra.TabIndex = 2;
            this.tabPageHydra.Text = "Hydra Status";
            // 
            // panelHydra
            // 
            this.panelHydra.BackColor = System.Drawing.Color.White;
            this.panelHydra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelHydra.Location = new System.Drawing.Point(8, 8);
            this.panelHydra.Name = "panelHydra";
            this.panelHydra.Size = new System.Drawing.Size(688, 432);
            this.panelHydra.TabIndex = 0;
            // 
            // statusBar
            // 
            this.statusBar.Location = new System.Drawing.Point(0, 671);
            this.statusBar.Name = "statusBar";
            this.statusBar.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
            this.statusBarPanel1});
            this.statusBar.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.statusBar.ShowPanels = true;
            this.statusBar.Size = new System.Drawing.Size(1152, 22);
            this.statusBar.TabIndex = 2;
            // 
            // statusBarPanel1
            // 
            this.statusBarPanel1.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            this.statusBarPanel1.Name = "statusBarPanel1";
            this.statusBarPanel1.Text = "Status: connecting  ";
            this.statusBarPanel1.Width = 1135;
            // 
            // MonitoringClient
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(1152, 693);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MonitoringClient";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Spark.Monitoring.Client 2009";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MonitoringClient_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.gridLogEntry)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPageEvents.ResumeLayout(false);
            this.tabPageEvents.PerformLayout();
            this.tabPageCounters.ResumeLayout(false);
            this.groupBoxAddCounter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCounters)).EndInit();
            this.tabPageHydra.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		[STAThread]
		static void Main() 
		{
			_monitoringClient = new MonitoringClient();
			Application.Run(_monitoringClient);
		}


		private void workCycle()
		{
			MonitoringClient_Resize(null, null);
			/*
			try
			{
				MessageBox.Show(_clientAPI.Url);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			*/
			

			while (_runnable)
			{
				try
				{
					switch (tabControl1.SelectedIndex)
					{
						case 0:
							refreshEvents();
							break;

						case 1:
							refreshCounters();
							break;

						case 2:
							refreshHydra();
							break;
					}
					statusBarPanel1.Text = "Status: connected  ";
				}
				catch (Exception ex)
				{
					statusBarPanel1.Text = "Status: error  ";
					System.Diagnostics.Trace.WriteLine("__" + ex.ToString().Replace("\r", "").Replace("\n", ""));
				}

				Thread.Sleep(1000);
			}
		}


		private void refreshEvents()
		{
			LogEntry[] logEntries = _clientAPI.GetCurrentLogEntries(_clientID);

			if (logEntries.Length > 0)
			{
				bool isDirty = false;

				foreach (LogEntry logEntry in logEntries)
				{
					if (_dtBg.Rows.Find(logEntry.Guid) == null)
					{
						isDirty = true;
						_dtBg.Rows.Add(new object[]{logEntry.Guid,
														logEntry.MachineName,
														logEntry.Source,
														logEntry.Category,
														logEntry.EntryType,
														logEntry.DateTime,
														logEntry.Message});

                        //we check whether we have to login the event
                        if (eventTextWriter != null)
                        {
                            if (eventTextWriter.Running)
                            {
                                try
                                {
                                    eventTextWriter.WriteLogEntry(logEntry);
                                }
                                catch
                                {
                                    MessageBox.Show("The writing service has failed and has been disabled.","Event Writing Service Failed");
                                }
                            }
                        }

						if (_dtBg.Rows.Count > 1000)
						{
							_dtBg.Rows.RemoveAt(0);
						}
					}
				}

				if (isDirty)
				{
					_monitoringClient.BeginInvoke(CallDataBindToDataGrid);
				}
			}
		}


		private void refreshCounters()
		{
			ClientCounterValues clientCounterValues = _clientAPI.GetCurrentCounterValues(_clientID);

			for (Int32 counterNum = 0; counterNum < clientCounterValues.Names.Length; counterNum++)
			{
				ArrayList list = null;

				foreach (DataRow row in _dsCounters.Tables[0].Rows)
				{
					if (row["MachineName"].ToString() + "|" + row["Counter"].ToString().Replace(" > ", "|") == clientCounterValues.Names[counterNum])
					{
						list = (ArrayList)row["Values"];
					}
				}

				CounterValue[] counterValues = null;

				switch (counterNum)
				{
					case 0:
						counterValues = clientCounterValues.Values0;
						break;
					case 1:
						counterValues = clientCounterValues.Values1;
						break;
					case 2:
						counterValues = clientCounterValues.Values2;
						break;
					case 3:
						counterValues = clientCounterValues.Values3;
						break;
					case 4:
						counterValues = clientCounterValues.Values4;
						break;
				}

				for (Int32 valNum = 0; valNum < counterValues.Length; valNum++)
				{
					list.Add(counterValues[valNum].Val);
				}

				while (list.Count > 5000)
				{
					list.RemoveAt(0);
				}
				

				//System.Diagnostics.Trace.WriteLine("__" + clientCounterValues.Names[counterNum] + ": " + currentValue.ToString());
			}


			panelGraph.Refresh();

			/*
			_counterValues = _clientAPI.GetCurrentCounterValues(_clientID);
			if (_counterValues != null)
			{
				label1.Text = (_counterValues[_counterValues.Length - 1].Val / 100 * panelGraph.Height).ToString();
				panelGraph.Refresh();
			}
			*/
		}


		private void refreshHydra()
		{
			panelHydra.Refresh();
		}


		private void panelHydra_Paint(object sender, PaintEventArgs e)
		{
			Graphics g = e.Graphics;			

			Int32 x = 160;
			Int32 y = 4;
			Font font = new Font("Arial", 10);
			Brush brush = new SolidBrush(Color.Black);
			Pen pen = new Pen(Color.Gainsboro);

			ArrayList dbList = new ArrayList("mnLogon,mnRegion,mnMember,mnFile,mnListMember,mnAlert,mnAlertSaveMM,mnAdmin,mnAdminWrite,mnLookup,mnList,mnMailLog,mnAnalysis,mnODS,mnSystem,mnHSStagingMP,mnChargeStage,mnSubscription,mnSubscriptionRenew,mnKey,mnIMail,mnIMailNew,mnContentStage,mnContent,mnSearchStore".Split(','));
			dbList.Sort();
			string[] dbs = (string[])dbList.ToArray(typeof(string));

			StringFormat stringFormatVertical = new StringFormat(StringFormatFlags.DirectionVertical);

			g.DrawLine(pen, x - 1, y, x - 1, y + panelHydra.Height - 12);
			foreach (string db in dbs)
			{
				g.DrawString(db,
					font,
					brush,
					x,
					y,
					stringFormatVertical);

				g.DrawLine(pen, x + 16, y, x + 16, y + panelHydra.Height - 12);

				x = x + 17;
			}

			ArrayList serversList = new ArrayList("LASVCCONTENT01:50000,LASVCCONTENT02:50000,LASVCCONTENT01:40000,LASVCCONTENT02:40000,LASVCCONTENT01:41000,LASVCCONTENT02:41000,svccontentstg01:41000,lasvcsloader01:59000,lasvcsloader02:59000,LASVCSITEMAIL01:46000,LASVCSITEMAIL02:46000,LASVCSITEMAIL03:46000,LASVCSITEMAIL04:46000,LASVCSITEMAIL05:46000,LASVCSITEMAIL06:46000,lasvcsengine01:54000,lasvcsengine02:54000,svcsearch01:54000,LASVCXMAIL01:51000,LASVCXMAIL02:51000,LASVCSITEMAIL01:49000,LASVCSITEMAIL02:49000,LASVCLIST01:43000,LASVCLIST02:43000,LASVCLIST03:43000,LASVCLIST04:43000,LASVCTEMP01:43000,LASVCTEMP02:43000,LASVCTEMP03:43000,LASVCTEMP04:43000,svcmatchmail01:51100,svcmatchmail02:51100,svcmatchmail03:51100,LASVCMEMBER01:42000,LASVCMEMBER02:42000,LASVCMEMBER03:42000,LASVCMEMBER04:42000,LASVCPRESENCE01:47000,LASVCPRESENCE02:47000,ccapp01:45400,ccapp02:45400,svcsub01:45300,svcsub02:45300,query01:48000,query02:48000,query03:48000,query04:48000,LASVCPRESENCE03:44000,LASVCPRESENCE04:44000,svcviralmail01:51200".ToLower().Split(','));
			serversList.Sort();
			string[] servers = (string[])serversList.ToArray(typeof(string));

			x = 4;
			y = 150;

			g.DrawLine(pen, x, y - 1, x + panelHydra.Width - 12, y - 1);
			foreach (string server in servers)
			{
				g.DrawString(server,
					font,
					brush,
					x,
					y);

				g.DrawLine(pen, x, y + 16, x + panelHydra.Width - 12, y + 16);
 
				y = y + 17;
			}

		}


		private void panelGraph_Paint(object sender, PaintEventArgs e)
		{
			Graphics g = e.Graphics;

			Int32 x = 4;
			Int32 y = 4;

			foreach (DataRow row in _dsCounters.Tables[0].Rows)
			{
				Single[] values = (Single[])((ArrayList)row["Values"]).ToArray(typeof(Single));

				drawGraph(g,
					x,
					y,
					panelGraph.Width - 6 - x,
					100,
					(Color)row["Color"],
					100,
					values);

				y = y + 110;
				/*
				System.Diagnostics.Trace.WriteLine("__values: " + values.Length.ToString());

				Pen pn = new Pen((Color)row["Color"]); 

				Point pointLast = new System.Drawing.Point(0,0);

				for (Int32 i = values.Length - 1; i > 1; i--)
				{
					Point p1 = new Point(x, panelGraph.Height - (Int32)((values[i] / 100) * panelGraph.Height));
					Point p2 = new Point(x - 1, panelGraph.Height - (Int32)((values[i - 1] / 100) * panelGraph.Height));

					if (x == panelGraph.Width)
					{
						//System.Diagnostics.Trace.WriteLine("__i: " + i.ToString() +" p1: " + p1.X.ToString() + "x" + p1.Y.ToString() + " p2: " + p2.X.ToString() + "x" + p2.Y.ToString());
					}
					g.DrawLine(pn, p1, p2);

					x--;

					if (x == 1)
					{
						break;
					}
				}
				*/
			}	
		}


		private void drawGraph(Graphics g,
			Int32 x,
			Int32 y,
			Int32 width,
			Int32 height,
			Color color,
			Single scale,
			Single[] values)
		{
			Pen pen = new Pen(Color.DimGray);

			//g.DrawLine(pen, x, y, x + width, y);
			//g.DrawLine(pen, x, y + height, x + width, y + height);

			pen  = new Pen(Color.WhiteSmoke);

			//horiz lines
			g.DrawLine(pen, x, y + ((height / 4) * 1), x + width, y + ((height / 4) * 1));
			g.DrawLine(pen, x, y + ((height / 4) * 3), x + width, y + ((height / 4) * 3));

			g.DrawLine(pen, x, y + ((height / 2) * 1), x + width, y + ((height / 2) * 1));
			
			g.DrawLine(pen, x, y, x, y + height);
			g.DrawLine(pen, x + width, y, x + width, y + height);
			
			//vert lines
			Int32 step = 15;
			Int32 xCurrent = width + x - step;
			while (xCurrent > x)
			{
				g.DrawLine(pen, xCurrent, y + 1, xCurrent, y + height - 1);
				xCurrent = xCurrent - step;
			}

			pen = new Pen(color);
			xCurrent = width - 1;

			GraphicsPath path = new GraphicsPath();

			for (Int32 i = values.Length - 1; i > 1; i--)
			{
				/*
				Point p1 = new Point(xCurrent + x + 1, height + y - 1 - (Int32)((values[i] / scale) * (height - 2)));
				Point p2 = new Point(xCurrent + x, height + y - 1 - (Int32)((values[i - 1] / scale) * (height - 2)));
				g.DrawLine(pen, p1, p2);
				*/

				path.AddLine(xCurrent + x + 1,
					height + y - 1 - (Int32)((values[i] / scale) * (height - 2)),
					xCurrent + x,
					height + y - 1 - (Int32)((values[i - 1] / scale) * (height - 2)));


				xCurrent--;

				if (xCurrent == 1)
				{
					break;
				}
			}

			g.DrawPath(pen, path);
		}


		static void DataBindToDataGrid()
		{
			Console.WriteLine("DataBindToDataGrid");
			_ds.Merge(_dtBg);
			while (_ds.Tables[0].Rows.Count > 1000)
			{
				_ds.Tables[0].Rows.RemoveAt(0);
			}
		}


		private void gridLogEntry_CurrentCellChanged(object sender, EventArgs e)
		{
			txtLogDetail.Text = _dv[gridLogEntry.CurrentCell.RowNumber]["Message"].ToString().Replace("\n", "\r\n");
			_currentGuid = new Guid(_dv[gridLogEntry.CurrentCell.RowNumber]["Guid"].ToString());
			gridLogEntry.Select(gridLogEntry.CurrentCell.RowNumber);
		}


		private void MonitoringClient_Resize(object sender, EventArgs e)
		{
			tabControl1.Width = this.Width - 13;
			tabControl1.Height = this.Height - 56;

			gridLogEntry.Width = tabPageEvents.Width;
			gridLogEntry.Height = tabControl1.Height - 210;
			_colMessage.Width = tabPageEvents.Width - 603;

			txtLogDetail.Width = gridLogEntry.Width;
			txtLogDetail.Top = tabControl1.Height - 176;

			gridCounters.Width = tabPageCounters.Width - 16;
			panelGraph.Width = tabPageCounters.Width - 16;
			panelGraph.Height = tabPageCounters.Height - 185;

			panelHydra.Width = tabPageHydra.Width - 16;
			panelHydra.Height = tabPageHydra.Height - 15;
		}


		private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
		{
			switch (tabControl1.SelectedIndex)
			{
				case 1:
					dropDownMachine.Items.Clear();
					dropDownMachine.Items.Add("<select machine>");
					foreach (string machine in _clientAPI.GetCounterMachines())
					{
						dropDownMachine.Items.Add(machine);
					}

					dropDownMachine.SelectedIndex = 0;
					break;
			}

			MonitoringClient_Resize(null, null);
		}


		private void dropDownMachine_SelectedIndexChanged(object sender, EventArgs e)
		{
			dropDownCounter.Items.Clear();

			if (dropDownMachine.SelectedIndex > 0)
			{
				foreach (string counter in _clientAPI.GetCounters(dropDownMachine.Items[dropDownMachine.SelectedIndex].ToString()))
				{
					dropDownCounter.Items.Add(counter.Replace("|", " > "));
				}

				if (dropDownCounter.Items.Count > 0)
				{
					dropDownCounter.SelectedIndex = 0;
				}

				buttonAddCounter.Enabled = true;
			}
			else
			{
				buttonAddCounter.Enabled = false;
			}
		}


		private void buttonAddCounter_Click(object sender, System.EventArgs e)
		{
			string counter = dropDownCounter.Items[dropDownCounter.SelectedIndex].ToString();

			for (Int32 rowNum = 0; rowNum < _dsCounters.Tables[0].Rows.Count; rowNum++)
			{
				DataRow row = _dsCounters.Tables[0].Rows[rowNum];
				if (row[0].ToString() == dropDownMachine.Items[dropDownMachine.SelectedIndex].ToString() && row[1].ToString() == counter)
				{
					MessageBox.Show("Counter has already been added.", "WTF?");
					return;
				}
			}
			
			string machineName = dropDownMachine.Items[dropDownMachine.SelectedIndex].ToString();

			_clientAPI.RegisterCounter(_clientID,
				machineName,
				counter.Replace(" > ", "|"));

			Color color = Color.Green;

			switch (_dsCounters.Tables[0].Rows.Count)
			{
				case 1:
					color = Color.Blue;
					break;
				case 2:
					color = Color.Red;
					break;
				case 3:
					color = Color.Yellow;
					break;
			}

			_dsCounters.Tables[0].Rows.Add(new object[]{machineName, counter, 1, color, new ArrayList()});
		}


		private void comboBoxFilter_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			_dv.RowFilter = ((Filter)comboBoxFilter.Items[comboBoxFilter.SelectedIndex]).Text;
		}

        private void checkBoxLogToFile_CheckedChanged(object sender, EventArgs e)
        {
            //check whether the box was checked or unchecked
            if (checkBoxLogToFile.Checked == true)
            {
                if (eventTextWriter != null)
                {
                    eventTextWriter.Dispose();
                }
                string path;

                //sets the dialog
                saveFileDialogEventLog.AddExtension = true;
                saveFileDialogEventLog.DefaultExt = "log";
                saveFileDialogEventLog.CheckPathExists = true;
                saveFileDialogEventLog.OverwritePrompt = true;
                saveFileDialogEventLog.Title = "Save log file as...";

                //gets the path
                if (saveFileDialogEventLog.ShowDialog() == DialogResult.OK)
                {
                    path = saveFileDialogEventLog.FileName;

                }
                else
                {
                    //if the user does not supply a path
                    checkBoxLogToFile.Checked = false;
                    return;
                }


                try
                {
                    eventTextWriter = new EventTextWriter(path);
                }
                catch 
                {
                    eventTextWriter = null;
                    checkBoxLogToFile.Checked = false;
                }
                finally
                {
                    if (eventTextWriter != null)
                    {
                        checkBoxLogToFile.Text = string.Format("Log to file {0}", path);
                    }
                }
            }
            else
            {
                if (eventTextWriter != null)
                {
                    eventTextWriter.Dispose();
                }

                checkBoxLogToFile.Text = "Log to file";
            }
            
        }

        private void MonitoringClient_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (eventTextWriter != null)
            {
                eventTextWriter.Dispose();
            }
        }
	}
}
