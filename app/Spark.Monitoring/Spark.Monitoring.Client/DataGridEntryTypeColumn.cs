using System;
using System.Data;
using System.Drawing;

using Spark.Monitoring.ValueObjects;


namespace Spark.Monitoring.Client
{
	public class DataGridEntryTypeColumn : System.Windows.Forms.DataGridColumnStyle
	{
		protected override void Abort(int rowNum)
		{

		}


		protected override bool Commit(System.Windows.Forms.CurrencyManager dataSource, int rowNum)
		{
			return false;
		}


		protected override void Edit(System.Windows.Forms.CurrencyManager source, int rowNum, System.Drawing.Rectangle bounds, bool readOnly, string instantText, bool cellIsVisible)
		{

		}


		protected override int GetMinimumHeight()
		{
			return 16;
		}


		protected  override int GetPreferredHeight(System.Drawing.Graphics g, object value)
		{
			return 16;
		}


		protected override System.Drawing.Size GetPreferredSize(System.Drawing.Graphics g, object value)
		{
			return new System.Drawing.Size(95, 16);
		}


		protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle bounds, System.Windows.Forms.CurrencyManager source, int rowNum)
		{
		}


		protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle bounds, System.Windows.Forms.CurrencyManager source, int rowNum, bool alignToRight)
		{
		}


		protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle bounds, System.Windows.Forms.CurrencyManager source, int rowNum, System.Drawing.Brush backBrush, System.Drawing.Brush foreBrush, bool alignToRight)
		{
			EntryType entryType = (EntryType)Enum.Parse(typeof(EntryType), ((DataRowView)source.List[rowNum])[this.MappingName].ToString());
			try
			{
				g.FillRectangle(backBrush, bounds);
				g.DrawImage(Image.FromStream(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Spark.Monitoring.Client.Images." + entryType.ToString() + ".png")), new Rectangle(bounds.Left, bounds.Top, 16, 16));
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine("__" + ex.ToString().Replace("\r", "").Replace("\n", ""));
			}
			g.DrawString(entryType.ToString(), this.DataGridTableStyle.DataGrid.Font, foreBrush, bounds.Left + 18, bounds.Top);
		}

	}
}
