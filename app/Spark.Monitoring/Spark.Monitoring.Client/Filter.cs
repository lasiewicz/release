using System;

namespace Spark.Monitoring.Client
{
	public class Filter
	{
		private string _name;
		private string _text;
		
		public Filter(string name,
			string text)
		{
			_name = name;
			_text = text;
		}


		public string Text
		{
			get
			{
				return _text;
			}
		}


		public override string ToString()
		{
			return _name;
		}

	}
}
