﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Spark.Monitoring.Client
{
    /// <summary>
    /// this class is used to log events to a specific file.
    /// 
    /// once opened you should execute the WriteEvent method
    /// 
    /// once its work is done, the Dispose() method should be
    /// called to release the lock on the file
    /// </summary>
    public class EventTextWriter:IDisposable
    {

        private string path;

        /// <summary>
        /// whther the writer is running
        /// </summary>
        private bool running = false;


        /// <summary>
        /// the writer used to write to the file
        /// </summary>
        private StreamWriter writer = null;

        /// <summary>
        /// creates a new event text writer
        /// </summary>
        /// <param name="path">the path to the file</param>
        public EventTextWriter(string path)
        {
            
            //checks whether the file exists
            if (File.Exists(path))
            {
                try
                {
                    File.Delete(path);
                }
                catch (Exception ex)
                {
                    throw new System.Exception("The system cannot write to that file. It seems it is already in use", ex);
                }
            }

            try
            {
                writer = File.CreateText(path);
                writer.AutoFlush = true;
                writer.WriteLine("DATETIME /// CATEGORY /// ENTRY TYPE /// MACHINE NAME /// MESSAGE /// SOURCE");
            }
            catch (Exception ex)
            {
                throw new System.Exception("The file cannot be created for writing. Probably out of space?", ex);
            }

            this.path = path;
            running = true;
        }

        public string Path
        {
            get 
            {
                if (running == true)
                {
                    return path;
                }
                else
                {
                    throw new System.Exception("The object is not running.");
                }
            }
        }

        /// <summary>
        /// writer a log entry to the system
        /// </summary>
        /// <param name="logEntry">the entry to log to the file</param>
        public void WriteLogEntry(LogEntry logEntry)
        {
            if (running == true)
            {
                lock (writer)
                {
                    try
                    {
                        writer.WriteLine("{0} /// {1} /// {2} /// {3} /// {4} /// {5}", 
                            (logEntry.DateTime == null ? "": logEntry.DateTime.ToString()),
                            (logEntry.Category == null ? "": logEntry.Category),
                            (logEntry.EntryType == null ? "": logEntry.EntryType.ToString()),
                            (logEntry.MachineName==null ? "":logEntry.MachineName),
                            (logEntry.Message == null ? "":logEntry.Message), 
                            (logEntry.Source == null) ? "":logEntry.Source);
                    }
                    catch (Exception ex)
                    {
                        Dispose();
                    }
                

                }
            }
        
        }

        /// <summary>
        /// whether a file has been opened for writing
        /// </summary>
        public bool Running
        {
            get { return running; }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (writer != null)
            {
                lock (writer)
                {
                    writer.Close();
                    writer = null;
                    running = false;
                }
            }
        }

        #endregion
    }
}
