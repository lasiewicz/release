using System;
using System.Collections;
using System.Diagnostics;

namespace Spark.Monitoring.ValueObjects.ServiceDefinitions
{
    public interface ICollectorService
    {
        bool PublishLogValuesWithCompletionStatus(LogEntry[] logEntries);

        [System.Runtime.Remoting.Messaging.OneWay()]
        void PublishLogValues(LogEntry[] logEntries);

        bool PublishCounterValuesWithCompletionStatus(string machineName,
            CounterValue[] counterValues);

        [System.Runtime.Remoting.Messaging.OneWay()]
        void PublishCounterValues(string machineName,
            CounterValue[] counterValues);

        LogEntry[] GetCurrentLogEntries(Guid clientID);

        PerformanceCounterConfiguration[] GetPerformanceCounterConfigurations(string machineName, string counterCollectionGroupName);

        string[] GetCounterMachines();

        string[] GetCounters(string machineName);

        void RegisterCounter(Guid clientID,
            string machineName,
            string counter);

        void UnregisterCounter(Guid clientID,
            string machineName,
            string counter);

        ClientCounterValues GetCurrentCounterValues(Guid clientID);

        PhysicalDBStatus[] GetHydraStatus();
    }
}
