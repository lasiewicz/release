using System;

namespace Spark.Monitoring.ValueObjects
{
	[Serializable]
	public struct CounterValue
	{
		public string Category;
		public string Name;
		public string Instance;
		public float Val;
		public DateTime DateTime;


		public CounterValue(string category,
			string name,
			string instance,
			float val,
			DateTime dateTime)
		{
			Category = category;
			Name = name;
			Instance = instance;
			Val = val;
			DateTime = dateTime;
		}

	}
}
