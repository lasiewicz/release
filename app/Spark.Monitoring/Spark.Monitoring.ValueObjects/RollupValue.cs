using System;

namespace Spark.Monitoring.ValueObjects
{
	[Serializable]
	public class RollupValue
	{
		private string _machineName;
		private string _counterName;
		private DateTime _dateTime;
		private float _val;

		public RollupValue(string machineName,
			string counterName,
			DateTime dateTime,
			float val)
		{
			_machineName = machineName;
			_counterName = counterName;
			_dateTime = dateTime;
			_val = val;
		}


		public string MachineName
		{
			get
			{
				return _machineName;
			}
		}


		public string CounterName
		{
			get
			{
				return _counterName;
			}
		}


		public DateTime DateTime
		{
			get
			{
				return _dateTime;
			}
		}


		public float Val
		{
			get
			{
				return _val;
			}
		}
	}
}
