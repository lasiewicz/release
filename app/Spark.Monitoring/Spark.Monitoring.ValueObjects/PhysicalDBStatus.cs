using System;
using System.Collections.Generic;
using System.Text;

namespace Spark.Monitoring.ValueObjects
{
    [Serializable]
    public struct PhysicalDBStatus
    {
        public string ServiceName;
        public string ServerName;
        public Int32 Port;
        public string DBServerName;
        public string DBName;
        public bool IsActive;
        public bool IsRecovering;
        public bool IsFailed;
        public bool HasErrors;
    }
}
