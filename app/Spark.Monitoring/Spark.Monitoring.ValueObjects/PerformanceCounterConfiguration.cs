using System;
using System.Collections.Generic;
using System.Text;

namespace Spark.Monitoring.ValueObjects
{
    [Serializable]
    public struct PerformanceCounterConfiguration
    {
        public string Category;
        public string Name;
        public string Instance;
        public int PollingInterval;

        public PerformanceCounterConfiguration(string category,
            string name,
            string instance,
            int pollingInterval)
        {
            this.Category = category;
            this.Name = name;
            this.Instance = instance;
            this.PollingInterval = pollingInterval;
        }
    }
}
