using System;

namespace Spark.Monitoring.ValueObjects
{
	[Serializable]
	public struct ClientCounterValues
	{
		public string[] Names;
		public CounterValue[] Values0;
		public CounterValue[] Values1;
		public CounterValue[] Values2;
		public CounterValue[] Values3;
		public CounterValue[] Values4;
		public CounterValue[] Values5;
		public CounterValue[] Values6;
		public CounterValue[] Values7;
		public CounterValue[] Values8;
		public CounterValue[] Values9;
		public CounterValue[] Values10;
		public CounterValue[] Values11;
		public CounterValue[] Values12;
		public CounterValue[] Values13;
		public CounterValue[] Values14;
		public CounterValue[] Values15;
		public CounterValue[] Values16;
		public CounterValue[] Values17;
		public CounterValue[] Values18;
		public CounterValue[] Values19;
	}
}
