using System;

namespace Spark.Monitoring.ValueObjects
{
	[Serializable]
	public enum EntryType
	{
		Error,
		FailureAudit,
		Information,
		SuccessAudit,
		Warning
	};


	[Serializable]
	public struct LogEntry
	{
		public LogEntry(string machineName,
			string source,
			string category,
			EntryType entryType,
			DateTime dateTime,
			string message)
		{
			Guid = Guid.NewGuid();
			MachineName = machineName;
			Source = source;
			Category = category;
			EntryType = entryType;
			DateTime = dateTime;
			Message = message;
		}

		public Guid Guid;
		public string MachineName;
		public string Source;
		public string Category;
		public EntryType EntryType;
		public DateTime DateTime;
		public string Message;
	}

	/*
	[Serializable]
	[XmlSchemaProvider("XmlSchema")] 
	public class LogEntry
	{
		private Guid _guid;
		private string _machineName;
		private string _source;
		private string _category;
		private EntryType _entryType;
		private DateTime _dateTime;
		private string _message;

		public LogEntry()
		{
		}

			
		public LogEntry(string machineName,
			string source,
			string category,
			EntryType entryType,
			DateTime dateTime,
			string message)
		{
			_guid = Guid.NewGuid();
			_machineName = machineName;
			_source = source;
			_category = category;
			_entryType = entryType;
			_dateTime = dateTime;
			_message = message;
		}


		public Guid Guid
		{
			get
			{
				return _guid;
			}
		}


		public string MachineName
		{
			get
			{
				return _machineName;
			}
		}


		public string Source
		{
			get
			{
				return _source;
			}
		}


		public string Category
		{
			get
			{
				return _category;
			}
		}


		public EntryType EntryType
		{
			get
			{
				return _entryType;
			}
		}


		public DateTime DateTime
		{
			get
			{
				return _dateTime;
			}
		}


		public string Message
		{
			get
			{
				return _message;
			}
		}
	}
	*/
}
