using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;

using Spark.Monitoring.ValueObjects;
using Spark.Monitoring.ValueObjects.ServiceDefinitions;


namespace Spark.Monitoring.Web
{
	[WebService(Namespace="http://spark.net/Monitoring/")]
	public class ClientAPI : System.Web.Services.WebService
	{
		public ClientAPI()
		{
			InitializeComponent();
		}


		#region Component Designer generated code
		
		//Required by the Web Services Designer 
		private IContainer components = null;
				
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if(disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);		
		}
		
		#endregion

		[WebMethod]
		public LogEntry[] GetCurrentLogEntries(Guid clientID)
		{
			return getService().GetCurrentLogEntries(clientID);
		}


		[WebMethod]
		public string[] GetCounterMachines()
		{
			return getService().GetCounterMachines();
		}


		[WebMethod]
		public string[] GetCounters(string machineName)
		{
			return getService().GetCounters(machineName);
		}


		[WebMethod]
		public void RegisterCounter(Guid clientID,
			string machineName,
			string counter)
		{
			getService().RegisterCounter(clientID,
				machineName,
				counter);
		}


		[WebMethod]
		public ClientCounterValues GetCurrentCounterValues(Guid clientID)
		{
			return getService().GetCurrentCounterValues(clientID);
		}


        [WebMethod]
        public PhysicalDBStatus[] GetHydraStatus()
        {
            return getService().GetHydraStatus();
        }


		private ICollectorService getService()
		{
			return (ICollectorService)Activator.GetObject(typeof(ICollectorService), "tcp://" + ConfigurationSettings.AppSettings["CollectorHost"] + "/Collector.rem");
		}

	}
}
