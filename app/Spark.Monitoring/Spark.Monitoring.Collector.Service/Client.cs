using System;
using System.Collections;
using System.Threading;

using Spark.Monitoring.ValueObjects;


namespace Spark.Monitoring.Collector.Service
{
	public class Client
	{
		private DateTime _lastRequest = DateTime.Now;
		private ArrayList _logEntries;
		private ReaderWriterLock _logEntriesLock;
		private Hashtable _counterValues;
		private ReaderWriterLock _counterValuesLock;
		private Hashtable _registeredPerf;
		private ReaderWriterLock _registeredPerfLock;

		public Client()
		{
			_logEntries = new ArrayList();
			_logEntriesLock = new ReaderWriterLock();
			_counterValues = new Hashtable();
			_counterValuesLock = new ReaderWriterLock();
			_registeredPerf = new Hashtable();
			_registeredPerfLock = new ReaderWriterLock();
		}


		public DateTime LastRequest
		{
			get
			{
				return _lastRequest;
			}
			set
			{
				_lastRequest = value;
			}
		}


		public void AddLogEntries(ICollection logEntries)
		{
			_logEntriesLock.AcquireWriterLock(-1);
			try
			{
				_logEntries.AddRange(logEntries);
			}
			finally
			{
				_logEntriesLock.ReleaseLock();
			}
		}


		public LogEntry[] GetLogEntries()
		{
			_logEntriesLock.AcquireWriterLock(-1);
			try
			{
				return (LogEntry[])_logEntries.ToArray(typeof(LogEntry));
			}
			finally
			{
				_logEntries.Clear();
				_logEntriesLock.ReleaseLock();
			}
		}


		public ClientCounterValues GetCounterValues()
		{
			ClientCounterValues clientCounterValues = new ClientCounterValues();

			_registeredPerfLock.AcquireReaderLock(-1);
			try
			{
				clientCounterValues.Names = (string[])(new ArrayList(_registeredPerf.Keys)).ToArray(typeof(string));
				for (Int32 num = 0; num < clientCounterValues.Names.Length; num++)
				{
					ArrayList list = _registeredPerf[clientCounterValues.Names[num]] as ArrayList;
					//System.Diagnostics.Trace.WriteLine("__GetCounterValues " + list.Count.ToString());
					CounterValue[] counterValues;
					lock (list.SyncRoot)
					{
						counterValues = (CounterValue[])list.ToArray(typeof(CounterValue));
						list.Clear();
					}
					//System.Diagnostics.Trace.WriteLine("__counterValues " + counterValues.Length.ToString());

					switch (num)
					{
						case 0:
							clientCounterValues.Values0 = counterValues;
							break;

						case 1:
							clientCounterValues.Values1 = counterValues;
							break;

						case 2:
							clientCounterValues.Values2 = counterValues;
							break;

						case 3:
							clientCounterValues.Values3 = counterValues;
							break;

						case 4:
							clientCounterValues.Values4 = counterValues;
							break;

						case 5:
							clientCounterValues.Values5 = counterValues;
							break;

						case 6:
							clientCounterValues.Values6 = counterValues;
							break;

						case 7:
							clientCounterValues.Values7 = counterValues;
							break;

						case 8:
							clientCounterValues.Values8 = counterValues;
							break;

						case 9:
							clientCounterValues.Values9 = counterValues;
							break;

						case 10:
							clientCounterValues.Values10 = counterValues;
							break;

						case 11:
							clientCounterValues.Values11 = counterValues;
							break;

						case 12:
							clientCounterValues.Values12 = counterValues;
							break;

						case 13:
							clientCounterValues.Values13 = counterValues;
							break;

						case 14:
							clientCounterValues.Values14 = counterValues;
							break;

						case 15:
							clientCounterValues.Values15 = counterValues;
							break;

						case 16:
							clientCounterValues.Values16 = counterValues;
							break;

						case 17:
							clientCounterValues.Values17 = counterValues;
							break;

						case 18:
							clientCounterValues.Values18 = counterValues;
							break;

						case 19:
							clientCounterValues.Values19 = counterValues;
							break;

					}
				}
			}
			finally
			{
				_registeredPerfLock.ReleaseLock();
			}

			return clientCounterValues;
		}


		public void AddCounterValue(string machineName,
			CounterValue counterValue)
		{
			_registeredPerfLock.AcquireReaderLock(-1);
			try
			{
				ArrayList list = _registeredPerf[getPerfKey(machineName, counterValue)] as ArrayList;
				if (list != null)
				{
					lock (list.SyncRoot)
					{
						list.Add(counterValue);
					}
				}
			}
			finally
			{
				_registeredPerfLock.ReleaseLock();
			}
		}


		public ArrayList RegisterCounter(string machineName,
			string counter,
			ArrayList list)
		{
			//System.Diagnostics.Trace.WriteLine("__RegisterCounter " + counter + ", " + list.Count.ToString());

			string key = getPerfKey(machineName,
				counter);

			_registeredPerfLock.AcquireWriterLock(-1);
			try
			{
				_registeredPerf.Add(key, list);
				return list;
			}
			finally
			{
				_registeredPerfLock.ReleaseLock();
			}
		}


		public void UnregisterCounter(string machineName,
			string counter)
		{
			//System.Diagnostics.Trace.WriteLine("__UnregisterCounter");

			string key = getPerfKey(machineName,
				counter);

			_registeredPerfLock.AcquireWriterLock(-1);
			try
			{
				if (_registeredPerf.ContainsKey(key))
				{
					_registeredPerf.Remove(key);
				}
			}
			finally
			{
				_registeredPerfLock.ReleaseLock();
			}
		}

		
		private string getPerfKey(string machineName,
			CounterValue counterValue)
		{
			return machineName + "|" + counterValue.Category + "|" + counterValue.Name + "|" + counterValue.Instance;
;
		}

		
		private string getPerfKey(string machineName,
			string counter)
		{
			return machineName + "|" + counter;
		}
	}
}
