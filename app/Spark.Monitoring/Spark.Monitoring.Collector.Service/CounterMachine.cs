using System;
using System.Collections;
using System.Threading;

using Spark.Monitoring.ValueObjects;


namespace Spark.Monitoring.Collector.Service
{
	public class CounterMachine
	{
		private Hashtable _counters;
		private ReaderWriterLock _countersLock;

		public CounterMachine()
		{
			_counters = new Hashtable();
			_countersLock = new ReaderWriterLock();
		}

		
		public string[] GetCounters()
		{
			return (string[])new ArrayList(_counters.Keys).ToArray(typeof(string));
		}


		public void AddCounterValue(CounterValue counterValue)
		{
			_countersLock.AcquireReaderLock(-1);
			try
			{
				string key = counterValue.Category + "|" + counterValue.Name + "|" + counterValue.Instance;
				ArrayList counterValues = _counters[key] as ArrayList;
				if (counterValues == null)
				{
					LockCookie lockCookie = _countersLock.UpgradeToWriterLock(-1);
					counterValues = _counters[key] as ArrayList;
					if (counterValues == null)
					{
						counterValues = new ArrayList();
						_counters.Add(key, counterValues);
					}
					_countersLock.DowngradeFromWriterLock(ref lockCookie);
				}

				lock (counterValues.SyncRoot)
				{
					//System.Diagnostics.Trace.WriteLine("__" + counterValue.Category + "|" + counterValue.Name + "|" + counterValue.Instance + "|" + counterValue.Val.ToString());
					counterValues.Add(counterValue);
					while (counterValues.Count > 5000)
					{
						counterValues.RemoveAt(0);
					}
				}
			}
			finally
			{
				_countersLock.ReleaseLock();
			}
		}


		public ArrayList GetCounterValues(string counterKey)
		{
			_countersLock.AcquireReaderLock(-1);
			try
			{
				ArrayList counterValues = _counters[counterKey] as ArrayList;
				if (counterValues == null)
				{
					return null;
				}

				lock (counterValues.SyncRoot)
				{
					return (ArrayList)counterValues.Clone();
				}
			}
			finally
			{
				_countersLock.ReleaseLock();
			}
		}
	}
}
