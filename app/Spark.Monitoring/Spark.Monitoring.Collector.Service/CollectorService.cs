using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;


namespace Spark.Monitoring.Collector.Service
{
	public class CollectorService : System.ServiceProcess.ServiceBase
	{
		private System.ComponentModel.Container components = null;
		private Collector _collector;
		private Int32 _port;

		public CollectorService()
		{
			InitializeComponent();
			_port = Convert.ToInt32(ConfigurationSettings.AppSettings["Port"]);
		}


		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new CollectorService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}


		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = "Spark.Monitoring.Collector.Service";
		}

		
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		protected override void OnStart(string[] args)
		{
			_collector = new Collector();
			_collector.Start();
			ChannelServices.RegisterChannel(new TcpServerChannel("", _port));
			System.Runtime.Remoting.RemotingServices.Marshal(_collector, "Collector.rem");
			//System.Diagnostics.Trace.WriteLine("__Spark.Monitoring.Collector.Service listening on " + _port.ToString());
		}
 

		protected override void OnStop()
		{
			_collector.Stop();
		}
	}
}
