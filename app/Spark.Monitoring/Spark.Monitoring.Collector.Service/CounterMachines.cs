using System;
using System.Collections;
using System.Threading;

using Spark.Monitoring.ValueObjects;


namespace Spark.Monitoring.Collector.Service
{
	public class CounterMachines
	{
		private Hashtable _machineList;
		private ReaderWriterLock _machineListLock;


		public CounterMachines()
		{
			_machineList = new Hashtable();
			_machineListLock = new ReaderWriterLock();
		}


		public string[] GetMachines()
		{
			_machineListLock.AcquireReaderLock(-1);
			try
			{
				return (string[])new ArrayList(_machineList.Keys).ToArray(typeof(string));
			}
			finally
			{
				_machineListLock.ReleaseLock();
			}
		}


		public string[] GetCounters(string machineName)
		{
			_machineListLock.AcquireReaderLock(-1);
			try
			{
				CounterMachine counterMachine = _machineList[machineName] as CounterMachine;
				if (counterMachine == null)
				{
					return null;
				}

				return counterMachine.GetCounters();
			}
			finally
			{
				_machineListLock.ReleaseLock();
			}
		}


		public void Publish(string machineName,
			CounterValue[] counterValues)
		{
			_machineListLock.AcquireReaderLock(-1);
			try
			{
				CounterMachine counterMachine = _machineList[machineName] as CounterMachine;
				if (counterMachine == null)
				{
					LockCookie lockCookie =_machineListLock.UpgradeToWriterLock(-1);
					counterMachine = _machineList[machineName] as CounterMachine;
					if (counterMachine == null)
					{
						counterMachine = new CounterMachine();
						_machineList.Add(machineName, counterMachine);
					}
					_machineListLock.DowngradeFromWriterLock(ref lockCookie);
				}

				for (Int32 counterNum = 0; counterNum < counterValues.Length; counterNum++)
				{
					counterMachine.AddCounterValue(counterValues[counterNum]);
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine("__" + ex.ToString().Replace("\r", "").Replace("\n", ""));
			}
			finally
			{
				_machineListLock.ReleaseLock();
			}
		}


		public ArrayList GetCounterValues(string machineName,
			string counterKey)
		{
			_machineListLock.AcquireReaderLock(-1);
			try
			{
				CounterMachine counterMachine = _machineList[machineName] as CounterMachine;
				if (counterMachine == null)
				{
					return null;
				}

				return counterMachine.GetCounterValues(counterKey);
			}
			finally
			{
				_machineListLock.ReleaseLock();
			}
		}
	}
}
