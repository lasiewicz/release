using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;
using System.Messaging;

using Spark.Monitoring.Collector.Service.Lookups;
using Spark.Monitoring.ValueObjects;
using Spark.Monitoring.ValueObjects.ServiceDefinitions;

//using Matchnet.Data;
//using Matchnet.Data.Hydra;
using Matchnet.RemotingServices.ValueObjects;
using Matchnet.RemotingServices.ValueObjects.ServiceDefinitions;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;


namespace Spark.Monitoring.Collector.Service
{
    public class Collector : MarshalByRefObject, ICollectorService
    {
        const string QUEUEPATH = @".\private$\MonitoringDB";

        private ArrayList _currentLogEntries;
        private ReaderWriterLock _currentLogEntriesLock;
        private Thread _threadClientTracker;
        private bool _runnable = false;
        private Hashtable _clients;
        private ReaderWriterLock _clientsLock;
        private CounterMachines _counterMachines;
        private Thread _threadRollup;
        private MessageQueue _queue;
        private Thread[] _threadDBWriters;
        private Thread _threadHydraWatch;
        private PhysicalDBStatus[] _hydraStatus = null;
        private ReaderWriterLock _hydraStatusLock = new ReaderWriterLock();

        Matchnet.Data.Hydra.HydraWriter _hydraWriter;

        public Collector()
        {
            _currentLogEntries = new ArrayList();
            _currentLogEntriesLock = new ReaderWriterLock();
            _clients = new Hashtable();
            _clientsLock = new ReaderWriterLock();
            _counterMachines = new CounterMachines();
            this._hydraWriter = new Matchnet.Data.Hydra.HydraWriter(new string[] { "mnMonitoring" });
        }


        public override object InitializeLifetimeService()
        {
            return null;
        }


        public void Start()
        {
            if (!MessageQueue.Exists(QUEUEPATH))
            {
                _queue = MessageQueue.Create(QUEUEPATH, true);
            }
            else
            {
                _queue = new MessageQueue(QUEUEPATH);
            }
            _queue.Formatter = new BinaryMessageFormatter();

            _runnable = true;
            _threadClientTracker = new Thread(new ThreadStart(clientTrackerCycle));
            _threadClientTracker.Start();
            _threadRollup = new Thread(new ThreadStart(rollupCycle));
            //_threadRollup.Start();

            _threadDBWriters = new Thread[4];
            for (Int32 threadNum = 0; threadNum < _threadDBWriters.Length; threadNum++)
            {
                _threadDBWriters[threadNum] = new Thread(new ThreadStart(dbWriteCycle));
                //_threadDBWriters[threadNum].Start();
            }

            _threadHydraWatch = new Thread(new ThreadStart(hydraWatchCycle));
            _threadHydraWatch.Start();

            this._hydraWriter.Start();
        }


        public void Stop()
        {
            _runnable = false;
            _threadHydraWatch.Join(5000);
            _threadClientTracker.Join(5000);
            _threadRollup.Join(5000);
            for (Int32 threadNum = 0; threadNum < _threadDBWriters.Length; threadNum++)
            {
                _threadDBWriters[threadNum].Join(5000);
            }

            this._hydraWriter.Stop();
        }

        public PhysicalDBStatus[] GetHydraStatus()
        {
            _hydraStatusLock.AcquireReaderLock(-1);
            try
            {
                return _hydraStatus;
            }
            finally
            {
                _hydraStatusLock.ReleaseLock();
            }
        }

        private void hydraWatchCycle()
        {
            while (_runnable)
            {
                try
                {
                    ArrayList list = new ArrayList();
                    ServicePartitions partitions = AdapterConfigurationSA.GetPartitions();
                    for (Int32 partitionNum = 0; partitionNum < partitions.Count; partitionNum++)
                    {
                        ServicePartition partition = partitions[partitionNum];
                        for (Int32 instanceNum = 0; instanceNum < partition.Count; instanceNum++)
                        {
                            ServiceInstance instance = partition[instanceNum];
                            if (partition.Constant != "JOESCLUB_SVC" && partition.Constant != "PAGEVIEW_SVC")
                            {
                                IHydraManagerService svc = null;
                                PhysicalDatabaseStatus[] physicalDatabaseStatusList = null;
                                string uri = "tcp://" + instance.HostName.ToLower() + ":" + partition.Port.ToString() + "/HydraManagerSM.rem";

                                try
                                {
                                    svc = ((IHydraManagerService)Activator.GetObject(typeof(IHydraManagerService), uri));
                                    physicalDatabaseStatusList = svc.GetPhysicalDatabaseStatus();
                                }
                                catch (Exception ex)
                                {
                                    //todo
                                }

                                if (physicalDatabaseStatusList != null)
                                {
                                    for (Int32 n = 0; n < physicalDatabaseStatusList.Length; n++)
                                    {
                                        PhysicalDatabaseStatus status = physicalDatabaseStatusList[n];
                                        PhysicalDBStatus status2 = new PhysicalDBStatus();
                                        status2.ServiceName = partition.Constant;
                                        status2.ServerName = instance.HostName.ToLower();
                                        status2.Port = partition.Port;
                                        status2.DBServerName = status.ServerName;
                                        status2.DBName = status.PhysicalDatabaseName;
                                        status2.IsActive = status.IsActive;
                                        status2.IsRecovering = status.IsRecovering;
                                        status2.IsFailed = status.IsFailed;
                                        status2.HasErrors = status.Errors.Length > 0;
                                        list.Add(status2);
                                    }
                                }
                            }
                        }
                    }

                    _hydraStatusLock.AcquireWriterLock(-1);
                    try
                    {
                        _hydraStatus = (PhysicalDBStatus[])list.ToArray(typeof(PhysicalDBStatus));
                    }
                    finally
                    {
                        _hydraStatusLock.ReleaseLock();
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine("__" + ex.ToString());
                }

                System.Diagnostics.Trace.WriteLine("__s: " + _hydraStatus.Length.ToString());

                Thread.Sleep(500);
            }
        }


        private void clientTrackerCycle()
        {
            while (_runnable)
            {
                try
                {
                    ArrayList removeList = new ArrayList();

                    _clientsLock.AcquireReaderLock(-1);
                    IDictionaryEnumerator de = _clients.GetEnumerator();
                    while (de.MoveNext())
                    {
                        Client client = ((Client)de.Value);
                        if (DateTime.Now.Subtract(client.LastRequest).TotalSeconds > 5)
                        {
                            removeList.Add(de.Key);
                        }
                    }

                    if (removeList.Count > 0)
                    {
                        _clientsLock.UpgradeToWriterLock(-1);
                        foreach (Guid clientID in removeList)
                        {
                            _clients.Remove(clientID);
                        }
                        removeList.Clear();
                    }

                    System.Diagnostics.Trace.WriteLine("__client count: " + _clients.Count.ToString());
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine("__" + ex.ToString().Replace("\r", "").Replace("\n", ""));
                }
                finally
                {
                    _clientsLock.ReleaseLock();
                }
                Thread.Sleep(10000);
            }
        }


        private void rollupCycle()
        {
            DateTime lastRollup = DateTime.Now.AddSeconds(-20);
            DateTime dateBegin;
            DateTime dateEnd;

            while (_runnable)
            {
                try
                {
                    if (DateTime.Now.Subtract(lastRollup).TotalSeconds > 60 && DateTime.Now.Second > 10)
                    {
                        lastRollup = dateEnd = new DateTime(DateTime.Now.Year,
                            DateTime.Now.Month,
                            DateTime.Now.Day,
                            DateTime.Now.Hour,
                            DateTime.Now.Minute,
                            0);

                        dateBegin = dateEnd.AddMinutes(-1);

                        System.Diagnostics.Trace.WriteLine("__begin rollup " + dateBegin.ToString() + " - " + dateEnd.ToString());

                        MessageQueueTransaction tran = new MessageQueueTransaction();
                        tran.Begin();

                        string[] machines = GetCounterMachines();
                        for (Int32 machineNum = 0; machineNum < machines.Length; machineNum++)
                        {
                            string machineName = machines[machineNum];
                            string[] counters = GetCounters(machineName);
                            for (Int32 counterNum = 0; counterNum < counters.Length; counterNum++)
                            {
                                string counterName = counters[counterNum];
                                ArrayList values = _counterMachines.GetCounterValues(machineName, counterName);
                                Int32 rollupCount = 0;
                                float rollupSum = 0;

                                for (Int32 valueNum = values.Count - 1; valueNum > -1; valueNum--)
                                {
                                    CounterValue counterValue = (CounterValue)values[valueNum];
                                    if (counterValue.DateTime < dateBegin)
                                    {
                                        break;
                                    }

                                    if (counterValue.DateTime < dateEnd)
                                    {
                                        rollupSum = rollupSum + counterValue.Val;
                                        rollupCount++;
                                    }
                                }

                                RollupValue rollupValue = new RollupValue(machineName,
                                    counterName,
                                    dateBegin,
                                    rollupSum / rollupCount);

                                this._queue.Send(rollupValue, tran);
                                this._queue.Dispose();

                                //MessageQueue queue = new MessageQueue();
                                //queue.Formatter = new BinaryMessageFormatter();
                                //queue.Send(rollupValue, tran);
                                //queue.Dispose();
                            }
                        }
                        tran.Commit();
                        System.Diagnostics.Trace.WriteLine("__end rollup");
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine("__" + ex.ToString());
                    EventLog.WriteEntry("Spark.Monitoring.Client",
                        ex.ToString(),
                        EventLogEntryType.Error);
                }

                Thread.Sleep(5000);
            }
        }


        private void dbWriteCycle()
        {
            TimeSpan timeout = new TimeSpan(0, 0, 5);

            while (_runnable)
            {
                Message message = null;
                MessageQueueTransaction tran = new MessageQueueTransaction();
                tran.Begin();

                try
                {
                    try
                    {
                        message = _queue.Receive(timeout, tran);
                    }
                    catch (MessageQueueException mEx)
                    {
                        if (mEx.Message != "Timeout for the requested operation has expired.")
                        {
                            throw mEx;
                        }
                    }

                    if (message != null)
                    {
                        LogEntry logEntry = (LogEntry)message.Body;
                        //RollupValue rollupValue = (RollupValue)message.Body;
                        //System.Diagnostics.Trace.WriteLine("__" + rollupValue.MachineName + ", " + rollupValue.CounterName + " - " + rollupValue.Val);

                        SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["ConnectionString"]);
                        SqlCommand cmd = new SqlCommand();

                        cmd.Connection = conn;


                        cmd.CommandText = "dbo.sp_inserteventlog";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@GUID", SqlDbType.VarChar).Value = logEntry.Guid.ToString();
                        cmd.Parameters.Add("@MachineName", SqlDbType.VarChar).Value = logEntry.MachineName;
                        cmd.Parameters.Add("@Source", SqlDbType.VarChar).Value = logEntry.Source;
                        cmd.Parameters.Add("@Category", SqlDbType.VarChar).Value = logEntry.Category;
                        cmd.Parameters.Add("@EntryType", SqlDbType.VarChar).Value = logEntry.EntryType.ToString();
                        cmd.Parameters.Add("@InsertDate", SqlDbType.DateTime).Value = logEntry.DateTime;
                        cmd.Parameters.Add("@Message", SqlDbType.Text).Value = logEntry.Message;

                        conn.Open();
                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        finally
                        {
                            conn.Close();
                            conn.Dispose();
                        }

                    }

                    tran.Commit();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine("__" + ex.ToString().Replace("/r", "").Replace("/n", ""));
                    Thread.Sleep(5000);
                    tran.Abort();
                }


            }
        }

        public void ProcessLogValues(LogEntry[] logEntries)
        {
            // The agent service will sleep for 5 seconds until starting the next cycle
            // Within this new cycle, the collector service will sleep for 7 seconds
            // So at mininum, there will be at least a 12 second interval  
            //System.Threading.Thread.Sleep(7000);

            System.Diagnostics.Trace.WriteLine("__In Collector.PublishLogValues(), Machine: " + logEntries[0].MachineName + ", Total count: " + logEntries.Length.ToString());

            for (Int32 i = 0; i < logEntries.Length; i++)
            {
                logEntries[i].Message = logEntries[i].Message.Replace("\0x14", "");
                logEntries[i].Category = logEntries[i].Category.Replace("\0x14", "");
                logEntries[i].Source = logEntries[i].Source.Replace("\0x14", "");
                logEntries[i].MachineName = logEntries[i].MachineName.Replace("\0x14", "");

                System.Diagnostics.Trace.WriteLine("__In Collector.PublishLogValues() to add log events: " + logEntries[i].Message.ToString());

                try
                {
                    Matchnet.Data.Command command = new Matchnet.Data.Command("mnMonitoring", "dbo.sp_inserteventlog", 0);
                    command.AddParameter("@GUID", SqlDbType.VarChar, ParameterDirection.Input, logEntries[i].Guid.ToString());
                    command.AddParameter("@MachineName", SqlDbType.VarChar, ParameterDirection.Input, logEntries[i].MachineName);
                    command.AddParameter("@Source", SqlDbType.VarChar, ParameterDirection.Input, logEntries[i].Source);
                    command.AddParameter("@Category", SqlDbType.VarChar, ParameterDirection.Input, logEntries[i].Category);
                    command.AddParameter("@EntryType", SqlDbType.VarChar, ParameterDirection.Input, logEntries[i].EntryType.ToString());
                    command.AddParameter("@InsertDate", SqlDbType.DateTime, ParameterDirection.Input, logEntries[i].DateTime);
                    command.AddParameter("@Message", SqlDbType.VarChar, ParameterDirection.Input, logEntries[i].Message);

                    Matchnet.Data.Client.Instance.ExecuteAsyncWrite(command);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine("__Error in Collector.PublishLogValues(), Error in PublishLogValues(): " + ex.Message.ToString());
                }
            }

            _currentLogEntriesLock.AcquireWriterLock(-1);
            try
            {
                _currentLogEntries.AddRange(logEntries);
                while (_currentLogEntries.Count > 100)
                {
                    _currentLogEntries.RemoveAt(0);
                }

                _clientsLock.AcquireReaderLock(-1);
                IDictionaryEnumerator de = _clients.GetEnumerator();
                while (de.MoveNext())
                {
                    ((Client)de.Value).AddLogEntries(logEntries);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("__" + ex.ToString().Replace("\r", "").Replace("\n", ""));
            }
            finally
            {
                _currentLogEntriesLock.ReleaseLock();
                _clientsLock.ReleaseLock();
            }
        }

        public bool PublishLogValuesWithCompletionStatus(LogEntry[] logEntries)
        {
            bool eventLogsCompleted = true;

            ProcessLogValues(logEntries);

            // Only return true value signifying that this function was called
            // Even if there is an error is this function, for example if the database suddenly
            // became offline, the inserts will be put into an inactive queue.  But as far as
            // the agent service is concerned, this function call will have been completed to 
            // prevent rolling back the transaction and putting the event logs back into the
            // collector service queue.  
            return eventLogsCompleted;
        }

        // Another version without a return value 
        // This is to allow some agent services to make one way calls  
        [System.Runtime.Remoting.Messaging.OneWay()]
        public void PublishLogValues(LogEntry[] logEntries)
        {
            ProcessLogValues(logEntries);
        }

        public void ProcessCounterValues(string machineName,
            CounterValue[] counterValues, bool writeToDB)
        {
            try
            {
                System.Diagnostics.Trace.WriteLine("__In Collector.PublishCounterValues(), Processing [" + counterValues.Length.ToString() + "] performance counters for machine [" + machineName + "]");

                if (writeToDB)
                {
                    for (Int32 valNum = 0; valNum < counterValues.Length; valNum++)
                    {
                        try
                        {
                            if (counterValues[valNum].Category != null && counterValues[valNum].Name != null)
                            {
                                Matchnet.Data.Command command = new Matchnet.Data.Command("mnMonitoring", "dbo.[sp_insertPerformanceCounterLog]", 0);
                                command.AddParameter("@MachineName", SqlDbType.VarChar, ParameterDirection.Input, machineName);
                                command.AddParameter("@Category", SqlDbType.VarChar, ParameterDirection.Input, counterValues[valNum].Category);
                                command.AddParameter("@Counter", SqlDbType.VarChar, ParameterDirection.Input, counterValues[valNum].Name);
                                command.AddParameter("@Instance", SqlDbType.VarChar, ParameterDirection.Input, counterValues[valNum].Instance);
                                command.AddParameter("@Value", SqlDbType.Float, ParameterDirection.Input, counterValues[valNum].Val);
                                command.AddParameter("@PerformanceCounterLoggedDateTime", SqlDbType.DateTime, ParameterDirection.Input, counterValues[valNum].DateTime);

                                Matchnet.Data.Client.Instance.ExecuteAsyncWrite(command);
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Trace.WriteLine("__Error in Collector.PublishCounterValues(), Error message: " + ex.Message.ToString());
                        }


                        System.Diagnostics.Trace.WriteLine("_____In Collector.PublishCounterValues(), Counter[" + valNum.ToString() + "]:  Category=" + counterValues[valNum].Category + ", Name=" + counterValues[valNum].Name + ", Instance=" + counterValues[valNum].Instance + ", Value=" + counterValues[valNum].Val.ToString() + ", DateTime=" + counterValues[valNum].DateTime.ToString());
                    }
                }

                _counterMachines.Publish(machineName,
                    counterValues);

                _clientsLock.AcquireReaderLock(-1);
                try
                {
                    IDictionaryEnumerator de = _clients.GetEnumerator();
                    while (de.MoveNext())
                    {
                        Client client = (Client)de.Value;

                        for (Int32 valNum = 0; valNum < counterValues.Length; valNum++)
                        {
                            client.AddCounterValue(machineName,
                                counterValues[valNum]);
                        }
                    }
                }
                finally
                {
                    _clientsLock.ReleaseLock();
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("__" + ex.ToString().Replace("\r", "").Replace("\n", ""));
            }
        }

        public bool PublishCounterValuesWithCompletionStatus(string machineName,
            CounterValue[] counterValues)
        {
            bool performanceCountersCompleted = true;

            // The agent service will sleep for 5 seconds until starting the next cycle
            // Within this new cycle, the collector service will sleep for 7 seconds
            // So at mininum, there will be at least a 12 second interval  
            //System.Threading.Thread.Sleep(7000);

            ProcessCounterValues(machineName, counterValues, true);

            // Only return true value signifying that this function was called
            // Even if there is an error is this function, for example if the database suddenly
            // became offline, the inserts will be put into an inactive queue.  But as far as
            // the agent service is concerned, this function call will have been completed to 
            // prevent rolling back the transaction and putting the event logs back into the
            // collector service queue.  
            return performanceCountersCompleted;
        }

        // Another version without a return value 
        // This is to allow some agent services to make one way calls  
        [System.Runtime.Remoting.Messaging.OneWay()]
        public void PublishCounterValues(string machineName,
            CounterValue[] counterValues)
        {
            ProcessCounterValues(machineName, counterValues, false);
        }

        public LogEntry[] GetCurrentLogEntries(Guid clientID)
        {
            Client client = getClient(clientID);

            _currentLogEntriesLock.AcquireReaderLock(-1);
            try
            {
                client.AddLogEntries(_currentLogEntries);
                return client.GetLogEntries();
            }
            finally
            {
                _currentLogEntriesLock.ReleaseLock();
            }
        }

        public PerformanceCounterConfiguration[] GetPerformanceCounterConfigurations(string machineName, string counterCollectionGroupName)
        {
            SqlDataReader dataReader = null;
            ArrayList arrPerformanceCounterConfigurationRetrieved = new ArrayList();

            try
            {
                Matchnet.Data.Command command = new Matchnet.Data.Command("mnMonitoring", "dbo.sp_getPerformanceCounters", 0);
                if (machineName != String.Empty)
                {
                    command.AddParameter("@MachineName", SqlDbType.VarChar, ParameterDirection.Input, machineName);
                }
                else
                {
                    command.AddParameter("@MachineName", SqlDbType.VarChar, ParameterDirection.Input, DBNull.Value);
                }
                if (counterCollectionGroupName != String.Empty)
                {
                    command.AddParameter("@GroupName", SqlDbType.VarChar, ParameterDirection.Input, counterCollectionGroupName);
                }
                else
                {
                    command.AddParameter("@GroupName", SqlDbType.VarChar, ParameterDirection.Input, DBNull.Value);
                }
                dataReader = Matchnet.Data.Client.Instance.ExecuteReader(command);

                //PerformanceCounter[] arrPerformanceCounterConfigurationRetrieved = new PerformanceCounter[]();

                while (dataReader.Read())
                {
                    if (dataReader["Instance"] == DBNull.Value)
                    {
                        arrPerformanceCounterConfigurationRetrieved.Add(new PerformanceCounterConfiguration(Convert.ToString(dataReader["Category"]),
                            Convert.ToString(dataReader["Counter"]),
                            String.Empty,
                            Convert.ToInt16(dataReader["PollingInterval"])));
                    }
                    else
                    {
                        arrPerformanceCounterConfigurationRetrieved.Add(new PerformanceCounterConfiguration(Convert.ToString(dataReader["Category"]),
                            Convert.ToString(dataReader["Counter"]),
                            Convert.ToString(dataReader["Instance"]),
                            Convert.ToInt16(dataReader["PollingInterval"])));
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("__Error in Collector.GetPerformanceCounters(), Error message: " + ex.Message.ToString());
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }

            return (PerformanceCounterConfiguration[])arrPerformanceCounterConfigurationRetrieved.ToArray(typeof(PerformanceCounterConfiguration));
        }


        public string[] GetCounterMachines()
        {
            return _counterMachines.GetMachines();
        }


        public string[] GetCounters(string machineName)
        {
            return _counterMachines.GetCounters(machineName);
        }


        public void RegisterCounter(Guid clientID,
            string machineName,
            string counter)
        {
            ArrayList listNew = getClient(clientID).RegisterCounter(machineName,
                counter,
                _counterMachines.GetCounterValues(machineName, counter));
        }


        public void UnregisterCounter(Guid clientID,
            string machineName,
            string counter)
        {
            //System.Diagnostics.Trace.WriteLine("__UnregisterCounter");

            getClient(clientID).UnregisterCounter(machineName,
                            counter);
        }


        public ClientCounterValues GetCurrentCounterValues(Guid clientID)
        {
            return getClient(clientID).GetCounterValues();
        }


        private Client getClient(Guid clientID)
        {
            _clientsLock.AcquireReaderLock(-1);
            try
            {
                Client client = _clients[clientID] as Client;

                if (client == null)
                {
                    _clientsLock.UpgradeToWriterLock(-1);
                    client = _clients[clientID] as Client;
                    if (client == null)
                    {
                        client = new Client();
                        _clients.Add(clientID, client);
                    }
                }

                client.LastRequest = DateTime.Now;

                return client;
            }
            finally
            {
                _clientsLock.ReleaseLock();
            }
        }
    }
}
