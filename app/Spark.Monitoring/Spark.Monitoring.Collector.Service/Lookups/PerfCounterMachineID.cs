using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading;


namespace Spark.Monitoring.Collector.Service.Lookups
{
	public class PerfCounterMachineID
	{
		private static readonly Hashtable _table = new Hashtable();
		private static readonly ReaderWriterLock _lock = new ReaderWriterLock();

		private PerfCounterMachineID()
		{
		}


		public static Int32 Lookup(string machineName,
			string counterName)
		{
			object o = null;
			Int32 id = 0;
			string key = machineName + "|" + counterName;

			_lock.AcquireReaderLock(-1);
			try
			{
				o = _table[key];

				if (o != null)
				{
					id = (Int32)o; 
				}
				else
				{
					_lock.UpgradeToWriterLock(-1);
					o = _table[key];

					if (o != null)
					{
						id = (Int32)o; 
					}
					else
					{
						id = lookup(machineName,
							counterName);

						_table.Add(key, id);
					}
				}
			}
			finally
			{
				_lock.ReleaseLock();
			}

			return id;
		}


		private static Int32 lookup(string machineName,
			string counterName)
		{
			Int32 id;
			string categoryName;
			string counterNameExtracted;
			string instanceName = "";

			string[] parts = counterName.Split('|');
			categoryName = parts[0].Trim();
			counterNameExtracted = parts[1].Trim();
			if (parts.Length > 2)
			{
				instanceName = parts[2].Trim();
			}

			SqlConnection conn = new SqlConnection(ConfigurationSettings.AppSettings["ConnectionString"]);
			SqlCommand cmd = new SqlCommand();

			cmd.Connection = conn;
			cmd.CommandText = "dbo.up_PerfCounterMachine_List";
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.Parameters.Add("@MachineName", SqlDbType.VarChar, 50).Value = machineName;
			cmd.Parameters.Add("@PerfCounterCategoryName", SqlDbType.VarChar, 50).Value = categoryName;
			cmd.Parameters.Add("@PerfCounterName", SqlDbType.VarChar, 50).Value = counterNameExtracted;
			cmd.Parameters.Add("@PerfCounterInstanceName", SqlDbType.VarChar, 50).Value = instanceName;
			SqlParameter outParameter = cmd.Parameters.Add("@PerfCounterMachineID", SqlDbType.Int);
			outParameter.Direction = ParameterDirection.Output;

			conn.Open();
			try
			{
				cmd.ExecuteNonQuery();
				id = (Int32)outParameter.Value;
			}
			finally
			{
				conn.Close();
			}

			return id;
		}
	}
}
