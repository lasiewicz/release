﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MemberAccess.AccessService;
using Matchnet.Configuration.ServiceAdapters;

namespace MemberAccess
{
    /// <summary>
    /// Adapter for interfacing with Access Service Proxy
    /// For now it'll just manage the proxy instance, but we can later expand it to map interfaces as an adapter if needed.
    /// 
    /// Type: instance; which means the Service Manager needs to instantiate this adapter and call its closeProxyInstance()
    /// at the end of the SM process.
    /// </summary>
    public class AccessServiceAdapter
    {
        private AccessServiceClient _Client = null;

        public AccessServiceClient GetProxyInstance()
        {
            try
            {
                if (_Client != null)
                {
                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new AccessServiceClient();
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new AccessServiceClient();
                    }
                }
                else
                {
                    _Client = new AccessServiceClient();
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public AccessServiceClient GetProxyInstance(string URL, string endpointConfigurationName)
        {
            try
            {
                if (_Client != null)
                {
                    if (_Client.State == System.ServiceModel.CommunicationState.Closed)
                    {
                        _Client = new AccessServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    }
                    else if (_Client.State == System.ServiceModel.CommunicationState.Faulted)
                    {
                        _Client.Abort();
                        _Client = new AccessServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                    }
                }
                else
                {
                    _Client = new AccessServiceClient(endpointConfigurationName, new System.ServiceModel.EndpointAddress(URL));
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        /// <summary>
        /// This gets proxy using URL stored in DB Setting via configuration service
        /// Client still needs to have the binding and client endpoint (url there will not be used) in their configuration file
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="endpointConfigurationName"></param>
        /// <returns></returns>
        public AccessServiceClient GetProxyInstanceForBedrock()
        {
            try
            {
                string AccessServiceURL = RuntimeSettings.GetSetting("UPS_ACCESS_SERVICE_URL");
                _Client = GetProxyInstance(AccessServiceURL, "BasicHttpBinding_IAccessService");

            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }

            return _Client;

        }

        public void CloseProxyInstance()
        {
            if (_Client != null)
            {
                try
                {
                    _Client.Close();
                }
                catch (Exception ex)
                {
                    _Client.Abort();
                }
            }
        }
    }
}
