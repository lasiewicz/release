namespace HydraMonitor
{
    partial class Lpd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            if (hydraMonitors != null)
            {
                for (int i=0; i < hydraMonitors.Count; i++)
                {
                    hydraMonitors[i].Abort();
                    hydraMonitors[i].Join();
                    

                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgHydra = new System.Windows.Forms.DataGrid();
            this.drStatus = new System.Windows.Forms.DataGridView();
            this.lblLastUpdate = new System.Windows.Forms.Label();
            this.lblStatusOK = new System.Windows.Forms.Label();
            this.lblStatusInactive = new System.Windows.Forms.Label();
            this.lblStatusMP = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblURI = new System.Windows.Forms.Label();
            this.lblPartOff = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgHydra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.drStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // dgHydra
            // 
            this.dgHydra.AllowNavigation = false;
            this.dgHydra.AllowSorting = false;
            this.dgHydra.CaptionVisible = false;
            this.dgHydra.CausesValidation = false;
            this.dgHydra.DataMember = "";
            this.dgHydra.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dgHydra.Location = new System.Drawing.Point(5, 78);
            this.dgHydra.Name = "dgHydra";
            this.dgHydra.ReadOnly = true;
            this.dgHydra.RowHeadersVisible = false;
            this.dgHydra.Size = new System.Drawing.Size(652, 862);
            this.dgHydra.TabIndex = 2;
            this.dgHydra.DoubleClick += new System.EventHandler(this.dgLpd_DoubleClick);
            this.dgHydra.Navigate += new System.Windows.Forms.NavigateEventHandler(this.dgHydra_Navigate);
            // 
            // drStatus
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.drStatus.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.drStatus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.drStatus.DefaultCellStyle = dataGridViewCellStyle5;
            this.drStatus.Location = new System.Drawing.Point(663, 78);
            this.drStatus.Name = "drStatus";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.drStatus.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.drStatus.Size = new System.Drawing.Size(481, 862);
            this.drStatus.TabIndex = 4;
            this.drStatus.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.drStatus_CellContentClick);
            // 
            // lblLastUpdate
            // 
            this.lblLastUpdate.AutoSize = true;
            this.lblLastUpdate.Location = new System.Drawing.Point(23, 9);
            this.lblLastUpdate.Name = "lblLastUpdate";
            this.lblLastUpdate.Size = new System.Drawing.Size(0, 13);
            this.lblLastUpdate.TabIndex = 5;
            // 
            // lblStatusOK
            // 
            this.lblStatusOK.AutoSize = true;
            this.lblStatusOK.BackColor = System.Drawing.Color.LightGreen;
            this.lblStatusOK.Location = new System.Drawing.Point(366, 9);
            this.lblStatusOK.Name = "lblStatusOK";
            this.lblStatusOK.Size = new System.Drawing.Size(55, 13);
            this.lblStatusOK.TabIndex = 6;
            this.lblStatusOK.Text = "Status:OK";
            // 
            // lblStatusInactive
            // 
            this.lblStatusInactive.AutoSize = true;
            this.lblStatusInactive.BackColor = System.Drawing.Color.MintCream;
            this.lblStatusInactive.Location = new System.Drawing.Point(366, 30);
            this.lblStatusInactive.Name = "lblStatusInactive";
            this.lblStatusInactive.Size = new System.Drawing.Size(78, 13);
            this.lblStatusInactive.TabIndex = 7;
            this.lblStatusInactive.Text = "Status:Inactive";
            // 
            // lblStatusMP
            // 
            this.lblStatusMP.AutoSize = true;
            this.lblStatusMP.BackColor = System.Drawing.Color.LightPink;
            this.lblStatusMP.Location = new System.Drawing.Point(533, 9);
            this.lblStatusMP.Name = "lblStatusMP";
            this.lblStatusMP.Size = new System.Drawing.Size(167, 13);
            this.lblStatusMP.TabIndex = 8;
            this.lblStatusMP.Text = "Status:Minor Problems(Has Errors)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(533, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Status:Critical";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Magenta;
            this.label2.Location = new System.Drawing.Point(706, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Status:All Failed";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(934, 9);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 15);
            this.lblStatus.TabIndex = 11;
            // 
            // lblURI
            // 
            this.lblURI.AutoSize = true;
            this.lblURI.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblURI.Location = new System.Drawing.Point(23, 30);
            this.lblURI.Name = "lblURI";
            this.lblURI.Size = new System.Drawing.Size(0, 15);
            this.lblURI.TabIndex = 12;
            // 
            // lblPartOff
            // 
            this.lblPartOff.AutoSize = true;
            this.lblPartOff.BackColor = System.Drawing.Color.MistyRose;
            this.lblPartOff.Location = new System.Drawing.Point(706, 32);
            this.lblPartOff.Name = "lblPartOff";
            this.lblPartOff.Size = new System.Drawing.Size(117, 13);
            this.lblPartOff.TabIndex = 13;
            this.lblPartOff.Text = "Status:Partially Inactive";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.LightGreen;
            this.label3.Location = new System.Drawing.Point(427, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Status:R(Recovery)";
            // 
            // Lpd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1156, 979);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblPartOff);
            this.Controls.Add(this.lblURI);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblStatusMP);
            this.Controls.Add(this.lblStatusInactive);
            this.Controls.Add(this.lblStatusOK);
            this.Controls.Add(this.lblLastUpdate);
            this.Controls.Add(this.drStatus);
            this.Controls.Add(this.dgHydra);
            this.Name = "Lpd";
            this.Text = "Lpd";
            this.Load += new System.EventHandler(this.Lpd_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgHydra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.drStatus)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGrid dgHydra;
        private System.Windows.Forms.DataGridView drStatus;
        private System.Windows.Forms.Label lblLastUpdate;
        private System.Windows.Forms.Label lblStatusOK;
        private System.Windows.Forms.Label lblStatusInactive;
        private System.Windows.Forms.Label lblStatusMP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblURI;
        private System.Windows.Forms.Label lblPartOff;
        private System.Windows.Forms.Label label3;

    }
}