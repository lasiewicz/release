using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Matchnet.Configuration.ValueObjects;
using Matchnet.RemotingServices.ValueObjects;
using Matchnet.RemotingServices.ValueObjects.ServiceDefinitions;
using Matchnet.Data.Hydra;

namespace HydraMonitor
{
    public partial class Monitor : Form
    {
        public Monitor()
        {
            InitializeComponent();
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            try
            {
               // loadEnv();
                drHydra.Rows.Clear();
                loadGrid();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }

        private void loadEnv()
        {
            try
            {
                cboEnv.Items.Clear();
                for (int i = 0; i < EnvironmentBL.Instance.Environments.Count; i++)
                {
                    cboEnv.Items.Add(EnvironmentBL.Instance.Environments[i].ConfigServiceURI);
                    cboEnv.DisplayMember = EnvironmentBL.Instance.Environments[i].Code;
                    
                }
           
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

        }
        private void Monitor_Load(object sender, EventArgs e)
        {
            try
            {
                loadEnv();
                cboEnv.SelectedIndex = 0;
                loadGrid();
                txtHydraURI.Text = "tcp://servername:port/HydraManagerSM.rem";
                txtInstHost.Text = "devapp01";
                txtInstName.Text = "member_svc";
                txtInstPort.Text = "42000";

            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
          
        }
        private void loadGrid()
        {
            ServicePartitions partitions = ServicesBL.GetPartitions(cboEnv.Text);
            string settingURI=cboEnv.Text;
            settingURI=settingURI.Replace("AdapterConfigurationSM", "SettingsSM");
            DataGridViewCell cellTemplate = new DataGridViewTextBoxCell();
            DataGridViewColumn cellCheck= new DataGridViewCheckBoxColumn();
            //drHydra.Columns.Add(new DataGridViewColumn(cellTemplate));
            drHydra.ColumnCount = 7;
            drHydra.Columns[0].HeaderText = "Service";
            drHydra.Columns[0].Width = 100;
            drHydra.Columns[1].HeaderText = "Host";
            drHydra.Columns[2].HeaderText = "OverrideHost";
            drHydra.Columns[2].Width = 50;
            drHydra.Columns[3].HeaderText = "Port";
            drHydra.Columns[3].Width = 60;
            drHydra.Columns[4].HeaderText = "Offset";
            drHydra.Columns[4].Width = 60;
            drHydra.Columns[5].HeaderText = "Instance";
            drHydra.Columns[5].Width = 100;
            drHydra.Columns[6].HeaderText = "IsEnabled";
            drHydra.Columns[6].Width=50;
            drHydra.Columns.Insert(7, cellCheck);
            drHydra.Columns[7].HeaderText = "Select";
            drHydra.Columns[7].Width = 60;
            //drHydra.Columns[6].HeaderText = "Select";
            //drHydra.Columns[6].CellTemplate = cellTemplateCheck;
            for (int i = 0; i < partitions.Count; i++)
            {
                for (int j = 0; j < partitions[i].Count; j++)
                {
                    //ArrayList rows = new ArrayList();
                    //rows.Add(partitions[i].Constant + " | " + partitions[i][j].HostName);

                    //ServicePartition servicepartition = ServicesBL.partitions[i];
                    // servicepartition.ser
                    int envInd = cboEnv.SelectedIndex;
                    string overrideMachine = EnvironmentBL.Instance.Environments[envInd ].overrideMachine;
                    string overrideSetting="";
                    if(!String.IsNullOrEmpty(overrideMachine))
                    {
                        string overrideConst = System.Configuration.ConfigurationSettings.AppSettings[partitions[i].Constant];
                         overrideSetting=ServicesBL.GetOverrideSetting(settingURI,overrideConst ,overrideMachine);
                        if(String.IsNullOrEmpty(overrideSetting)) overrideSetting="";
                    }
                    
                    drHydra.Rows.Add(partitions[i].Constant, partitions[i].HostName, overrideSetting, partitions[i].Port, partitions[i].PartitionOffset, partitions[i][j].HostName, partitions[i][j].IsEnabled);
                }
            }
        }
        private void loadStatusGrid(PhysicalDatabaseStatus[] statusList)
        {
        
            DataGridViewCell cellTemplate = new DataGridViewTextBoxCell();
            drHydraStatus.Rows.Clear();
            //drHydra.Columns.Add(new DataGridViewColumn(cellTemplate));
            drHydraStatus.ColumnCount = 6;
            drHydraStatus.Columns[0].HeaderText = "Ph. dbname";
            drHydraStatus.Columns[1].HeaderText = "Host";
            drHydraStatus.Columns[2].HeaderText = "IsActive";
            drHydraStatus.Columns[2].Width = 40;
            drHydraStatus.Columns[3].HeaderText = "IsFailed";
            drHydraStatus.Columns[3].Width = 40;
            drHydraStatus.Columns[4].HeaderText = "Errors";
            drHydraStatus.Columns[4].Width = 40;
            drHydraStatus.Columns[5].HeaderText = "ErrorsDetails";
           // drStatus.Columns[5].HeaderText = "IsEnabled";

            for (int i = 0; i < statusList.Length; i++)
            {
                string err = "";
                int errLen = statusList[i].Errors.Length;
                int actualErrCount=0;
                for (int j = 0; j < errLen; j++)
                {
                    if (statusList[i].Errors[j] != null)
                    {
                        err += statusList[i].Errors[j].DateTime + "\r\n" + statusList[i].Errors[j].QueuePath + "\r\n" + statusList[i].Errors[j].Description;
                        actualErrCount += 1;
                    }
                }
                string errCount=errLen.ToString();
                if(actualErrCount != errLen)
                  errCount= actualErrCount.ToString() + "/" + errLen;
                drHydraStatus.Rows.Add(statusList[i].PhysicalDatabaseName, statusList[i].ServerName, statusList[i].IsActive, statusList[i].IsFailed, errCount,err);
            }
        }
        private void drHydra_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try{
                if (e.ColumnIndex == 0)
                {
                    DataGridViewRow r = drHydra.Rows[e.RowIndex];
                    string host = r.Cells[5].Value.ToString();
                    string port = r.Cells[3].Value.ToString();
                    string overrhost = r.Cells[2].Value.ToString();
                    if (!String.IsNullOrEmpty(overrhost))
                        host = overrhost;
                    string uri = "tcp://" + host + ":" + port + "/HydraManagerSM.rem";
                    PhysicalDatabaseStatus[] statusList = ServicesBL.GetHydraStatus(uri);
                    if (statusList.Length > 0)
                        loadStatusGrid(statusList);
                }
        }
        catch (Exception ex)
        { MessageBox.Show(ex.Message); }
        }
        private void drStatus_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 5)
                {
                    DataGridViewRow r = drHydraStatus.Rows[e.RowIndex];
                    string err = r.Cells[5].Value.ToString();
                    if(!String.IsNullOrEmpty(err))
                    {
                      ErrorDetail frmErr=new ErrorDetail();
                       frmErr.ErrText=err;
                      frmErr.Show(this);
                    }
                }
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }
        private void btnLpd_Click(object sender, EventArgs e)
        {
            try
            {  
                Lpd f=new Lpd();
                string uri = cboEnv.Text;
                uri=uri.Replace("AdapterConfigurationSM", "LpdSM");
                f.configURI=uri;
                f.Show();

            }
             catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        
        }
        private void button1_Click(object sender, EventArgs e)
        {
            ArrayList svc = new ArrayList();

            for (int i = 1; i < drHydra.Rows.Count; i++)
            {
                if (drHydra.Rows[i].Cells[7].Value!= null && drHydra.Rows[i].Cells[7].Value.ToString().ToLower() == "true")
                {
                    Instance inst=new Instance();
                    inst.host = drHydra.Rows[i].Cells[5].Value.ToString();
                    string overrhost = drHydra.Rows[i].Cells[2].Value.ToString();
                    if (!String.IsNullOrEmpty(overrhost))
                        inst.host = overrhost;
                    inst.name = drHydra.Rows[i].Cells[0].Value.ToString();
                    inst.port = drHydra.Rows[i].Cells[3].Value.ToString();
                    svc.Add(inst);
                }
            }
            Lpd f = new Lpd();
            f.svc = svc;
            string uri = cboEnv.Text;
            uri = uri.Replace("AdapterConfigurationSM", "LpdSM");
            f.configURI = uri;
   
            f.Show();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

            for (int i = 0; i < drHydra.Rows.Count; i++)
            {
                drHydra.Rows[i].Cells[7].Value = false;
            }
        }

        private void cboEnv_SelectedIndexChanged(object sender, EventArgs e)
        {
            int ind=cboEnv.SelectedIndex;
            lblEnv.Text = "Env:" + EnvironmentBL.Instance.Environments[ind].Code + "/ Override Machine:" + EnvironmentBL.Instance.Environments[ind].overrideMachine;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Search s = new Search();
            s.Show();
        }

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            try
            {
               // string uri = "tcp://" + host + ":" + port + "/HydraManagerSM.rem";
                string uri = txtHydraURI.Text;
                PhysicalDatabaseStatus[] statusList = ServicesBL.GetHydraStatus(uri);
                if (statusList.Length > 0)
                    loadStatusGrid(statusList);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + ", " + ex.Message);
            }
        }

        private void btnDisplyNew_Click(object sender, EventArgs e)
        {
            ArrayList svc = new ArrayList();

          
            Instance inst = new Instance();
            inst.host = txtInstHost.Text;
            inst.name = txtInstName.Text;
            inst.port = txtInstPort.Text;
            svc.Add(inst);
          
            Lpd f = new Lpd();
            f.svc = svc;
            string uri = cboEnv.Text;
            uri = uri.Replace("AdapterConfigurationSM", "LpdSM");
            f.configURI = uri;

            f.Show();
        }




    }
}