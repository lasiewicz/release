namespace HydraMonitor
{
    partial class Monitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cboEnv = new System.Windows.Forms.ComboBox();
            this.btnReload = new System.Windows.Forms.Button();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.drHydra = new System.Windows.Forms.DataGridView();
            this.drHydraStatus = new System.Windows.Forms.DataGridView();
            this.btnSelectMonitor = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lblEnv = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtHydraURI = new System.Windows.Forms.TextBox();
            this.btnDisplay = new System.Windows.Forms.Button();
            this.btnDisplyNew = new System.Windows.Forms.Button();
            this.txtInstName = new System.Windows.Forms.TextBox();
            this.txtInstPort = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtInstHost = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.drHydra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.drHydraStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // cboEnv
            // 
            this.cboEnv.FormattingEnabled = true;
            this.cboEnv.Location = new System.Drawing.Point(224, 14);
            this.cboEnv.Name = "cboEnv";
            this.cboEnv.Size = new System.Drawing.Size(404, 21);
            this.cboEnv.TabIndex = 0;
            this.cboEnv.SelectedIndexChanged += new System.EventHandler(this.cboEnv_SelectedIndexChanged);
            // 
            // btnReload
            // 
            this.btnReload.Location = new System.Drawing.Point(682, 12);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(75, 23);
            this.btnReload.TabIndex = 1;
            this.btnReload.Text = "Reload";
            this.btnReload.UseVisualStyleBackColor = true;
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // drHydra
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.drHydra.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.drHydra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.drHydra.DefaultCellStyle = dataGridViewCellStyle2;
            this.drHydra.Location = new System.Drawing.Point(12, 112);
            this.drHydra.Name = "drHydra";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.drHydra.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.drHydra.Size = new System.Drawing.Size(476, 326);
            this.drHydra.TabIndex = 2;
            this.drHydra.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.drHydra_CellContentClick);
            // 
            // drHydraStatus
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.drHydraStatus.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.drHydraStatus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.drHydraStatus.DefaultCellStyle = dataGridViewCellStyle5;
            this.drHydraStatus.Location = new System.Drawing.Point(494, 112);
            this.drHydraStatus.Name = "drHydraStatus";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.drHydraStatus.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.drHydraStatus.Size = new System.Drawing.Size(454, 326);
            this.drHydraStatus.TabIndex = 3;
            this.drHydraStatus.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.drStatus_CellContentClick);
            // 
            // btnSelectMonitor
            // 
            this.btnSelectMonitor.Location = new System.Drawing.Point(783, 12);
            this.btnSelectMonitor.Name = "btnSelectMonitor";
            this.btnSelectMonitor.Size = new System.Drawing.Size(152, 23);
            this.btnSelectMonitor.TabIndex = 5;
            this.btnSelectMonitor.Text = "Select for Hydra Status Monitor";
            this.btnSelectMonitor.UseVisualStyleBackColor = true;
            this.btnSelectMonitor.Click += new System.EventHandler(this.button1_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1059, 11);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(85, 24);
            this.button1.TabIndex = 6;
            this.button1.Text = "UnSelect All";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // lblEnv
            // 
            this.lblEnv.AutoSize = true;
            this.lblEnv.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnv.Location = new System.Drawing.Point(24, 20);
            this.lblEnv.Name = "lblEnv";
            this.lblEnv.Size = new System.Drawing.Size(0, 15);
            this.lblEnv.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(637, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "label1";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtHydraURI
            // 
            this.txtHydraURI.Location = new System.Drawing.Point(536, 42);
            this.txtHydraURI.Name = "txtHydraURI";
            this.txtHydraURI.Size = new System.Drawing.Size(399, 20);
            this.txtHydraURI.TabIndex = 9;
            // 
            // btnDisplay
            // 
            this.btnDisplay.Location = new System.Drawing.Point(1053, 41);
            this.btnDisplay.Name = "btnDisplay";
            this.btnDisplay.Size = new System.Drawing.Size(91, 28);
            this.btnDisplay.TabIndex = 10;
            this.btnDisplay.Text = "Display";
            this.btnDisplay.UseVisualStyleBackColor = true;
            this.btnDisplay.Click += new System.EventHandler(this.btnDisplay_Click);
            // 
            // btnDisplyNew
            // 
            this.btnDisplyNew.Location = new System.Drawing.Point(1029, 77);
            this.btnDisplyNew.Name = "btnDisplyNew";
            this.btnDisplyNew.Size = new System.Drawing.Size(115, 38);
            this.btnDisplyNew.TabIndex = 11;
            this.btnDisplyNew.Text = "Open in new Window";
            this.btnDisplyNew.UseVisualStyleBackColor = true;
            this.btnDisplyNew.Click += new System.EventHandler(this.btnDisplyNew_Click);
            // 
            // txtInstName
            // 
            this.txtInstName.Location = new System.Drawing.Point(700, 87);
            this.txtInstName.Name = "txtInstName";
            this.txtInstName.Size = new System.Drawing.Size(113, 20);
            this.txtInstName.TabIndex = 12;
            // 
            // txtInstPort
            // 
            this.txtInstPort.Location = new System.Drawing.Point(851, 86);
            this.txtInstPort.Name = "txtInstPort";
            this.txtInstPort.Size = new System.Drawing.Size(60, 20);
            this.txtInstPort.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(636, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Inst. Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(819, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Port";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(504, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "URI";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(504, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Inst. Host";
            // 
            // txtInstHost
            // 
            this.txtInstHost.Location = new System.Drawing.Point(562, 85);
            this.txtInstHost.Name = "txtInstHost";
            this.txtInstHost.Size = new System.Drawing.Size(68, 20);
            this.txtInstHost.TabIndex = 17;
            // 
            // Monitor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 457);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtInstHost);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtInstPort);
            this.Controls.Add(this.txtInstName);
            this.Controls.Add(this.btnDisplyNew);
            this.Controls.Add(this.btnDisplay);
            this.Controls.Add(this.txtHydraURI);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblEnv);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSelectMonitor);
            this.Controls.Add(this.drHydraStatus);
            this.Controls.Add(this.drHydra);
            this.Controls.Add(this.btnReload);
            this.Controls.Add(this.cboEnv);
            this.Name = "Monitor";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Monitor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.drHydra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.drHydraStatus)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboEnv;
        private System.Windows.Forms.Button btnReload;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.DataGridView drHydra;
        private System.Windows.Forms.DataGridView drHydraStatus;
        private System.Windows.Forms.Button btnSelectMonitor;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblEnv;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtHydraURI;
        private System.Windows.Forms.Button btnDisplay;
        private System.Windows.Forms.Button btnDisplyNew;
        private System.Windows.Forms.TextBox txtInstName;
        private System.Windows.Forms.TextBox txtInstPort;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtInstHost;
    }
}

