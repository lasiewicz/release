using System;
using System.Collections.Generic;
using System.Text;

namespace HydraMonitor
{
    class HydraStatus
    {
    }

    public struct HydraGridStatus
    {
        public const string statusNone = "N";
        public const string statusOK = "OK";
        public const string statusRecovery = "R";
        public const string statusInActive = "NA";
        public const string statusPartiallyOff = "PI";
        public const string statusMinorProblems = "MP";
        public const string statusCritical = "C";
        public const string statusFatal = "F";
        public const string statusAllFailed = "FF";
        public static int severityMinor;
        public static int severityCritical;
        public static int severityMajor;
       
    }

    public struct Instance
    {
        public string name;
        public string host;
        public string port;
    }

}
