using System;
using System.Collections.Generic;
using System.Text;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;
using Matchnet.RemotingServices.ValueObjects;
using Matchnet.RemotingServices.ValueObjects.ServiceDefinitions;
using Matchnet.Data.Hydra;
using System.Diagnostics;

namespace HydraMonitor
{
    public class ServicesBL
    {
        //public  ServicePartitions partitions = null;
        public static ServicesBL Instance = new ServicesBL();

        private ServicesBL() { }

        public static ServicePartitions GetPartitions(string configURI)
        {
            ServicePartitions partitions = null;
            if (partitions == null) { partitions = new ServicePartitions(); }
            partitions.Clear();

            partitions = getService(configURI).GetAll();
            return partitions;
           }

        public static string GetOverrideSetting(string uri, string settingConst, string machine)
        {
            try
            {
                string overrideSetting = "OVERRIDE/*/*/*/" + settingConst;
                return getSettingService(uri).GetSettings(machine)[overrideSetting];
            }
            catch (Exception e)
            { return ""; }
        }

        public static  LogicalDatabases GetLPD(string configURI)
        {
            LogicalDatabases dbs = null;

            dbs = getLpdService(configURI).GetLogicalDatabases();
            return dbs;
        }


        public static PhysicalDatabaseStatus[] GetHydraStatus(string uri)
        {
            IHydraManagerService svc = null;
            PhysicalDatabaseStatus[] physicalDatabaseStatusList = null;

            try
            {
                svc = ((IHydraManagerService)Activator.GetObject(typeof(IHydraManagerService), uri));
                physicalDatabaseStatusList = svc.GetPhysicalDatabaseStatus();
                return physicalDatabaseStatusList;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public static void WriteEventLog(EventLogEntryType type,  string msg,Exception ex)
        {
            try
            {
                if (!EventLog.SourceExists("HydraMonitor"))
                        EventLog.CreateEventSource("Hydramonitor", "Application");

                    EventLog.WriteEntry("HydraMonitor", msg + "\r\n" + ex.Source + "\r\n" + ex.Message,type);
            }
            catch (Exception e) { }
        }
        private static  IAdapterConfigurationService getService(string uri)
        {
            try
            {
                return (IAdapterConfigurationService)Activator.GetObject(typeof(IAdapterConfigurationService), uri);
            }
            catch (Exception ex)
            {
                throw (new Exception("Cannot activate remote service manager. (uri: " + uri + ")", ex));
            }
        }

        private static ISettingsService getSettingService(string uri)
        {
            try
            {
                return (ISettingsService)Activator.GetObject(typeof(ISettingsService), uri);
            }
            catch (Exception ex)
            {
                throw (new Exception("Cannot activate remote service manager. (uri: " + uri + ")", ex));
            }
        }

        private static ILpdService getLpdService(string uri)
        {
            try
            {
                return (ILpdService)Activator.GetObject(typeof(ILpdService), uri);
            }
            catch (Exception ex)
            {
                throw (new Exception("Cannot activate remote service manager. (uri: " + uri + ")", ex));
            }
        }

   
    }
}
