using System;
using System.Collections.Generic;
using System.Text;
using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.RemotingServices.ValueObjects;
using Matchnet.RemotingServices.ValueObjects.ServiceDefinitions;
using Matchnet.Data.Hydra;
using HydraMonitor.UI;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;

namespace HydraMonitor
{
    public class HydraWorker
    {
        private ArrayList svc;
        private string Uri="";

        private ContainerControl sender;
        public RefreshGridDelegate refreshDelegate;
        private bool stop;
        
        public HydraWorker(  ContainerControl s, ArrayList svcList, string hydraURI, RefreshGridDelegate refreshGrid)
        {
            svc = svcList;
            Uri = hydraURI;
            refreshDelegate = refreshGrid;
            sender = s;
        }

        public void workCycle()
        {
            try
            {
                while (!stop)
                {
                    prepareHydraStatus();
                    Thread.Sleep(30000);
                }
            }
            catch (Exception ex)
            {
                ServicesBL.WriteEventLog(EventLogEntryType.Warning, "Worker thread error",ex);
            }
        }
        private void prepareHydraStatus()
        {
            
            DataTable t = new DataTable();
            LogicalDatabases ldb = ServicesBL.GetLPD(Uri);
            List<PhysicalDatabaseStatus> statusList = null;
          
             AddDataColumn(t, "service");
            
            for (int i = 0; i < ldb.Count; i++)
            {   AddDataColumn(t, ldb[i].LogicalDatabaseName); }

            List<PhysicalDatabaseStatus>[,] arr = new List<PhysicalDatabaseStatus>[svc.Count, t.Columns.Count];
            AddHeaderDataRow(t);

            for (int i = 0; i < svc.Count; i++)
            {
          
                string svcname=((Instance)svc[i]).name;
                string svchost=((Instance)svc[i]).host;
                string svcport=((Instance)svc[i]).port;
                getServiceDBs(svcname, svchost, svcport.ToString(), t, ldb, arr, i);

            }

            sender.BeginInvoke(refreshDelegate, t, arr);
         }
        private void getServiceDBs(DataTable t,   List<PhysicalDatabaseStatus>[,] arr,int i) 
        {
            try{
                List<PhysicalDatabaseStatus> statusList = getStatus(((Instance)svc[i]).host, ((Instance)svc[i]).port);
                    DataRow r = AddDataRow(t);
                    r[t.Columns[0]] = ((Instance)svc[i]).name + "\r\n" + ((Instance)svc[i]).host;
                    for (int j = 1; j < t.Columns.Count; j++)
                    {
                        string colname = t.Columns[j].ColumnName.ToLower();
                        colname = mapDBName(colname);
                        List<PhysicalDatabaseStatus> l = statusList.FindAll(delegate(PhysicalDatabaseStatus s) { return Regex.Replace(s.PhysicalDatabaseName, "[0-9]", "", RegexOptions.IgnoreCase).ToLower() == colname; });
                        string status = calculateGridStatus(l);
                        r[t.Columns[j].ColumnName] = status;
                        arr[i, j] = l;
                    }
            }   
            catch(Exception  ex)
            {}

            
        }


        private void getServiceDBs(string svcName, string svcHost, string svcPort,DataTable t, LogicalDatabases ldb, List<PhysicalDatabaseStatus>[,] arr, int i)
        {
            try
            {
                List<PhysicalDatabaseStatus> statusList = getStatus(((Instance)svc[i]).host, ((Instance)svc[i]).port);
                DataRow r = AddDataRow(t);
               // r[t.Columns[0]] = ((Instance)svc[i]).name + "\r\n" + ((Instance)svc[i]).host;
                r[t.Columns[0]] = svcName + "\r\n" + svcHost;
                for (int j = 0; j <ldb.Count; j++)
                {
                    string colname = ldb[j].LogicalDatabaseName.ToLower();

                    colname = mapDBName(colname);
                    List<PhysicalDatabaseStatus> l = new List<PhysicalDatabaseStatus>();
                    
                    for (int h = 0; h < ldb[j].PartitionCount; h++)
                    {
                        Partition p = ldb[j].GetPartition(h);
                        for(int k=0;k < p.PhysicalDatabases.Count;k++)
                        {
                            PhysicalDatabase db=p.PhysicalDatabases[k];
                            string serverName=db.ServerName;
                            List<PhysicalDatabaseStatus> l1 = statusList.FindAll(delegate(PhysicalDatabaseStatus s) { return (Regex.Replace(s.PhysicalDatabaseName, "[0-9]", "", RegexOptions.IgnoreCase).ToLower() == colname  &&  s.ServerName.ToLower()==serverName.ToLower()); });
                            if (l1 != null && l1.Count > 0)
                            {
                                for (int g = 0; g < l1.Count; g++)
                                {
                                    if (l.FindAll(delegate(PhysicalDatabaseStatus s) { return s == l1[g]; }).Count == 0)
                                        l.Add(l1[g]);
                                }
                            }
                        }
                    }
                   // List<PhysicalDatabaseStatus> l = statusList.FindAll(delegate(PhysicalDatabaseStatus s) { return Regex.Replace(s.PhysicalDatabaseName, "[0-9]", "", RegexOptions.IgnoreCase).ToLower() == colname; });
                    string status = calculateGridStatus(l);
                    r[t.Columns[j+1].ColumnName] = status;
                    arr[i, j+1] = l;
                }
            }
            catch (Exception ex)
            { }


        }
        private string mapDBName(string logicalName)
        {
            string mappedName = logicalName;
            switch (logicalName.ToLower())
            {
                case "mnimailnew":
                    mappedName = "mnimail";
                    break;

                case "mnlistmember":
                    mappedName = "mnmember";
                    break;
                case "mnadminwrite":
                    mappedName = "mnadmin";
                    break;
                case "mnalertsavemm":
                    mappedName = "mnalert";
                    break;
                case "mnalertsavemmstage":
                    mappedName = "mnalert";
                    break;
                case "mnalertwrite":
                    mappedName = "mnalert";
                    break;
                case "mnalertwritestage":
                    mappedName = "mnalert";
                    break;
                case "mnsubscriptionrenew":
                    mappedName = "mnsubscription";
                    break;

                default:
                    mappedName = logicalName;
                    break;
            }
            return mappedName;
        }
         private void AddDataColumn(DataTable dt, string column)
         {
             try
             {
                 DataColumn column2 = new DataColumn(column);
                 dt.Columns.Add(column2);
             }
             catch (Exception)
             {
             }
         }

         private DataRow AddDataRow(DataTable dt)
         {
             try
             {
                 DataRow row = dt.NewRow();
                 dt.Rows.Add(row);
                 return row;
             }
             catch (Exception)
             {
                 return null;
             }
         }

         private DataRow AddHeaderDataRow(DataTable dt)
         {
             try
             {
                 DataRow row = dt.NewRow();
                 foreach (DataColumn column in dt.Columns)
                 {
                     row[column] = column.ColumnName;
                 }
                 dt.Rows.Add(row);
                 return row;
             }
             catch (Exception)
             {
                 return null;
             }
         }

         private string calculateGridStatus(List<PhysicalDatabaseStatus> statusList)
         {
             try
             {
                 string text = "";
                 if ((statusList != null) && (statusList.Count != 0))
                 {
                     int count = statusList.Count;
                     int num = count * 3;

                     int recoveryCount = statusList.FindAll(delegate(PhysicalDatabaseStatus s)
                     {
                         return s.IsRecovering;
                     }).Count;

                     int activeCount = statusList.FindAll(delegate(PhysicalDatabaseStatus s)
                     {
                         return !s.IsActive;
                     }).Count;
                     if (activeCount == count)
                     {
                         return "NA";
                     }
                     int errCount = statusList.FindAll(delegate(PhysicalDatabaseStatus s)
                     {
                         return s.Errors.Length > 0;
                     }).Count;
                     int failedCount = statusList.FindAll(delegate(PhysicalDatabaseStatus s)
                     {
                         return s.IsFailed;
                     }).Count;
                     int score = num - ((activeCount + errCount) + (failedCount * HydraGridStatus.severityMajor));
                     if (score <= 0)
                     {
                         text = "FF";
                     }
                     else if (score == num)
                     {
                         text = "OK";
                     }
                     else if ((score < num) && (score > (num / 2)))
                     {
                         if (failedCount == 0 && errCount == 0  && activeCount < count)
                         { text = "PI"; }
                         else
                         { text = "MP"; }
                     }
                     else if ((score < (num / 2)) && (score > (num / 3)))
                     {
                         text = "C";
                     }
                     else
                     {
                         text = "F";
                     }

                     if (recoveryCount > 0)
                     { text = "R"; }

                 }
              
                 return text;
             }
             catch (Exception)
             {
                 return "";
             }
         }

        private List<PhysicalDatabaseStatus> getStatus(string host, string port)
        {

            string uri = "tcp://" + host + ":" + port + "/HydraManagerSM.rem";
            PhysicalDatabaseStatus[] statusList = ServicesBL.GetHydraStatus(uri);
            List<PhysicalDatabaseStatus> l = new List<PhysicalDatabaseStatus>();
            for (int i = 0; i < statusList.Length; i++)
            {
                l.Add(statusList[i]);
            }
            return l;
        }
    }
}
