using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace HydraMonitor
{
    public class Environment
    {
        public  string Code = "";
        public string ConfigServiceURI = "";
        public string overrideMachine = "";
     }

    public class EnvironmentCollection:KeyedCollection<string,Environment>
    {
        public EnvironmentCollection():base(){}

        protected override string GetKeyForItem(Environment item)
        {
            return (item.Code);
        }
    }

}
