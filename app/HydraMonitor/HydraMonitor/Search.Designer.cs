namespace HydraMonitor
{
    partial class Search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtOverride = new System.Windows.Forms.TextBox();
            this.txtComm = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtOut = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // txtOverride
            // 
            this.txtOverride.Location = new System.Drawing.Point(98, 12);
            this.txtOverride.Name = "txtOverride";
            this.txtOverride.Size = new System.Drawing.Size(328, 20);
            this.txtOverride.TabIndex = 0;
            // 
            // txtComm
            // 
            this.txtComm.Location = new System.Drawing.Point(98, 90);
            this.txtComm.Name = "txtComm";
            this.txtComm.Size = new System.Drawing.Size(538, 96);
            this.txtComm.TabIndex = 1;
            this.txtComm.Text = "";
            this.txtComm.TextChanged += new System.EventHandler(this.txtComm_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(504, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtOut
            // 
            this.txtOut.Location = new System.Drawing.Point(28, 220);
            this.txtOut.Name = "txtOut";
            this.txtOut.Size = new System.Drawing.Size(723, 374);
            this.txtOut.TabIndex = 3;
            this.txtOut.Text = "";
            // 
            // Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(791, 621);
            this.Controls.Add(this.txtOut);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtComm);
            this.Controls.Add(this.txtOverride);
            this.Name = "Search";
            this.Text = "Search";
            this.Load += new System.EventHandler(this.Search_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtOverride;
        private System.Windows.Forms.RichTextBox txtComm;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox txtOut;
    }
}