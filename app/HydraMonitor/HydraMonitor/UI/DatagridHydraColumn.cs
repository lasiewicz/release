using System;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace HydraMonitor.UI
{
    public class DataGridHydraColumn : System.Windows.Forms.DataGridColumnStyle
    {
        int _rowNum = 0;
        bool _resizeHeight = true;
        bool _useColor = false;

        public bool ResizeHeight
        {
            set { _resizeHeight = value; }

        }

        public bool UseColor
        {
            set { _useColor = value; }

        }
        protected override void Abort(int rowNum)
        {
            return;
        }


        protected override bool Commit(System.Windows.Forms.CurrencyManager dataSource, int rowNum)
        {
            return true;
        }


        protected override void Edit(System.Windows.Forms.CurrencyManager source, int rowNum, System.Drawing.Rectangle bounds, bool readOnly, string instantText, bool cellIsVisible)
        {
            try
            {
                return;
            }
            catch (Exception e) { }
        }


        protected override int GetMinimumHeight()
        {

            System.Windows.Forms.CurrencyManager cur = (System.Windows.Forms.CurrencyManager)this.DataGridTableStyle.DataGrid.BindingContext[this.DataGridTableStyle.DataGrid.DataSource, this.DataGridTableStyle.DataGrid.DataMember];
            int clientSize = this.DataGridTableStyle.DataGrid.ClientSize.Height;

            int rowHeight = 0;
            if (cur.Count > 0 && _resizeHeight)
                rowHeight = (int)clientSize / (cur.Count + 2);
            else
                rowHeight = 20;

            if (_rowNum == cur.Count)
                _rowNum = 0;
            else
                _rowNum += 1;
            if (_rowNum == 0)
               return rowHeight + 80;
           else
                return rowHeight;
        }


        protected override int GetPreferredHeight(System.Drawing.Graphics g, object value)
        {
            return 90;
        }


        protected override System.Drawing.Size GetPreferredSize(System.Drawing.Graphics g, object value)
        {
            return new System.Drawing.Size(35, 90);
        }


        protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle bounds, System.Windows.Forms.CurrencyManager source, int rowNum)
        {
        }


        protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle bounds, System.Windows.Forms.CurrencyManager source, int rowNum, bool alignToRight)
        {
        }


        protected override void Paint(System.Drawing.Graphics g, System.Drawing.Rectangle bounds, System.Windows.Forms.CurrencyManager source, int rowNum, System.Drawing.Brush backBrush, System.Drawing.Brush foreBrush, bool alignToRight)
        {
            string entryType = ((DataRowView)source.List[rowNum])[this.MappingName].ToString();
            StringFormat stringFormatVertical = new StringFormat(StringFormatFlags.DirectionVertical);

            try
            {

                if (rowNum == 0)
                {
                   // bounds.Height = 120;
                    g.DrawRectangle(new Pen(backBrush), bounds);
                    g.FillRectangle(backBrush, bounds);
                    g.DrawString(entryType.ToString(),
                        this.DataGridTableStyle.DataGrid.Font,
                        foreBrush,
                        bounds.Left + 1,
                        bounds.Top,
                        stringFormatVertical);
                }
                else
                {
                    bounds.Width = bounds.Width + 10;
                    //bounds.Left = bounds.Left + 10;

                    SolidBrush brush = getStatusBrush(entryType);
                    g.FillRectangle(brush, bounds);
                    g.DrawString(entryType.ToString(),
                      this.DataGridTableStyle.DataGrid.Font,
                      foreBrush,
                      bounds.Left + 1,
                      bounds.Top);
                }
                //g.DrawImage(Image.FromStream(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Spark.Monitoring.Client.Images." + entryType.ToString() + ".png")), new Rectangle(bounds.Left, bounds.Top, 16, 16));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("__" + ex.ToString().Replace("\r", "").Replace("\n", ""));
            }
            //g.DrawString(entryType.ToString(), this.DataGridTableStyle.DataGrid.Font, foreBrush, bounds.Left + 18, bounds.Top);

        }

        private SolidBrush getStatusBrush(string status)
        {
            SolidBrush brush = null;
            if (status.Length >= 2) status = status.Substring(0, 2);
            switch (status)
            {
                case HydraGridStatus.statusOK:
                    {
                        brush = new SolidBrush(Color.LightGreen);
                        break;
                    }
                case HydraGridStatus.statusRecovery:
                    {
                        brush = new SolidBrush(Color.LightGreen);
                        break;
                    }
                case HydraGridStatus.statusInActive:
                    {
                        brush = new SolidBrush(Color.MintCream);
                        break;

                    }
                case HydraGridStatus.statusMinorProblems:
                    {
                        brush = new SolidBrush(Color.LightPink);
                        break;

                    }
                case HydraGridStatus.statusCritical:
                    {
                        brush = new SolidBrush(Color.Red);
                        break;

                    }
                case HydraGridStatus.statusAllFailed:
                    {
                        brush = new SolidBrush(Color.Magenta);
                        break;

                    }
                case HydraGridStatus.statusPartiallyOff:
                    {
                        brush = new SolidBrush(Color.MistyRose);
                        break;

                    }

                default:
                    {
                        brush = new SolidBrush(Color.White);
                        break;
                    }
            }
            return brush;


        }

    }
}
