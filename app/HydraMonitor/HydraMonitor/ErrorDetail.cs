using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace HydraMonitor
{
    public partial class ErrorDetail : Form
    {
        public string ErrText = "";
        public ErrorDetail()
        {
            InitializeComponent();
        }

        private void ErrorDetail_Load(object sender, EventArgs e)
        {
            txtErr.Text = ErrText;
        }
    }
}