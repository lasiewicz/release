using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace HydraMonitor
{
    public class EnvironmentBL
    {
        private static EnvironmentCollection envColl = null;
        public static EnvironmentBL Instance=new EnvironmentBL();
        private EnvironmentBL()
        {
            try
            {   Load();}
            catch (Exception ex)
            { }

        }

        public EnvironmentCollection Environments
        {
            get { return envColl; }
        }
        public static void Load()
        {
            if (envColl == null) { envColl = new EnvironmentCollection(); }

            envColl.Clear();

            string xmlFile = System.Configuration.ConfigurationSettings.AppSettings["envfile"];
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);
            XmlNodeList list = doc.DocumentElement.GetElementsByTagName("env");
            for (int i = 0; i < list.Count; i++)
            {
                XmlNode env = list[i];
                string name = env.SelectSingleNode("name").InnerText;
                string uri = env.SelectSingleNode("configservice").InnerText;
                 string machine="";
                if (env.SelectSingleNode("override") !=null)
                     machine= env.SelectSingleNode("override").InnerText;
                Environment environment = new Environment();
                environment.Code = name;
                environment.ConfigServiceURI = uri;
                environment.overrideMachine = machine;
                envColl.Add(environment);

            }
        }
    }
}
