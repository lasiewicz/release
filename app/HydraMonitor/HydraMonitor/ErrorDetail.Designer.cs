namespace HydraMonitor
{
    partial class ErrorDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtErr = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // txtErr
            // 
            this.txtErr.Location = new System.Drawing.Point(12, 57);
            this.txtErr.Name = "txtErr";
            this.txtErr.Size = new System.Drawing.Size(967, 593);
            this.txtErr.TabIndex = 0;
            this.txtErr.Text = "";
            // 
            // ErrorDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 693);
            this.Controls.Add(this.txtErr);
            this.Name = "ErrorDetail";
            this.Text = "ErrorDetail";
            this.Load += new System.EventHandler(this.ErrorDetail_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox txtErr;
    }
}