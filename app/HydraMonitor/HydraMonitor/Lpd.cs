using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.RemotingServices.ValueObjects;
using Matchnet.RemotingServices.ValueObjects.ServiceDefinitions;
using Matchnet.Data.Hydra;
using HydraMonitor.UI;
using System.Text.RegularExpressions;
using System.Threading;


namespace HydraMonitor
{
    public  delegate void RefreshGridDelegate(DataTable t, List<PhysicalDatabaseStatus>[,]statusList);
    public partial class Lpd : Form
    {
        private LogicalDatabases ldb;
        public ArrayList svc;
        public string configURI = "";
     //   DataTable t = new DataTable();
        List<PhysicalDatabaseStatus>[,] statusList;
        private bool stop;
        //Thread _threadRefresh;
        List<Thread> hydraMonitors=new List<Thread>();
       // HydraWorker worker = null;
        public Lpd()
        {

            try
            {
                InitializeComponent();
   
            }
            catch (Exception ex)
            { }
        }

        private void Lpd_Load(object sender, EventArgs e)
        {
            try
            {
                
               // dgLpd.DoubleClick+=new EventHandler(dgLpd_DoubleClick);
               RefreshGridDelegate del = new RefreshGridDelegate(refreshGrid);
               lblURI.Text = configURI;
               HydraWorker worker = new HydraWorker(this, svc, configURI, del);

              Thread _threadRefresh = new Thread(new ThreadStart(worker.workCycle));
                _threadRefresh.IsBackground = true;
                _threadRefresh.Start();
                hydraMonitors.Add(_threadRefresh);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

  
        private  List<PhysicalDatabaseStatus> getStatus(string host, string port)
        {
           
            string uri = "tcp://" + host + ":" + port + "/HydraManagerSM.rem";
            PhysicalDatabaseStatus[] statusList = ServicesBL.GetHydraStatus(uri);
            List<PhysicalDatabaseStatus> l = new List<PhysicalDatabaseStatus>();
            for (int i = 0; i < statusList.Length; i++)
            {
                l.Add(statusList[i]);
            }
            return l;
        }

        public void refreshGrid(DataTable t, List<PhysicalDatabaseStatus>[,] arr)
        {
            try
            {
                dgHydra.RowHeadersVisible = false;
                dgHydra.ColumnHeadersVisible = false;

         
                DataGridTableStyle ts = new DataGridTableStyle();
                ts.ReadOnly = true;
                ts.RowHeadersVisible = false;
                ts.ColumnHeadersVisible = false;

                ArrayList aHeaderRow = new ArrayList();
                DataGridColumnStyle col1 = new DataGridHydraColumn();
                AddColumnStyle(ts, col1, "service", "service", 80);
              
                for (int i = 1; i < t.Columns.Count; i++)
                {
                    DataGridColumnStyle col = new DataGridHydraColumn();
                    AddColumnStyle(ts, col, t.Rows[0][i].ToString(), t.Rows[0][i].ToString(), 23);
        
                }
          
                dgHydra.TableStyles.Clear();
                dgHydra.TableStyles.Add(ts);
                dgHydra.DataSource = t;
                statusList = arr;
                lblLastUpdate.Text = "Last Update: " + DateTime.Now.ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + "\r\n " + ex.Message);
            }
        }


        private void dgLpd_DoubleClick(object sender, EventArgs e)
        {
            int col = dgHydra.CurrentCell.ColumnNumber;
            int row = dgHydra.CurrentCell.RowNumber;
            string status = dgHydra[row, 0] + "/" + dgHydra[0, col];
            if(row!=0 && col!=0 )
                loadStatusGrid(statusList[row-1, col]);
            lblStatus.Text = status;
        }
        
        private void AddColumnStyleHydra(DataGridTableStyle ts, string mappingName, string headerText, int width)
        {
            DataGridHydraColumn column = new DataGridHydraColumn();
            column.MappingName = mappingName;
            column.HeaderText = headerText;
            column.Width = width;
            column.ResizeHeight = false;
            ts.GridColumnStyles.Add(column);
        }
        private void AddColumnStyle(DataGridTableStyle ts, DataGridColumnStyle colstyle, string mappingName, string headerText, int width)
        {
            colstyle.MappingName = mappingName;
            colstyle.HeaderText = headerText;
            colstyle.Width = width;
            ts.GridColumnStyles.Add(colstyle);
        }

        private void AddColumnStyle(DataGridTableStyle ts, string mappingName, string headerText, int width)
        {
            DataGridColumnStyle column = new DataGridTextBoxColumn();
            column.MappingName = mappingName;
            column.HeaderText = headerText;
            column.Width = width;
            ts.GridColumnStyles.Add(column);
        }
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                //if (!String.IsNullOrEmpty(configURI))
                //   // prepareGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        
        private void loadStatusGrid(List <PhysicalDatabaseStatus> statusList)
        {
            if (statusList == null)
                return;
            DataGridViewCell cellTemplate = new DataGridViewTextBoxCell();
            drStatus.RowHeadersVisible = false;
            drStatus.Rows.Clear();
            //drHydra.Columns.Add(new DataGridViewColumn(cellTemplate));
            drStatus.ColumnCount = 7;
            drStatus.Columns[0].HeaderText = "Ph. dbname";
            drStatus.Columns[1].HeaderText = "Host";
            drStatus.Columns[2].HeaderText = "IsActive";
            drStatus.Columns[2].Width = 45;
            drStatus.Columns[3].HeaderText = "IsFailed";
            drStatus.Columns[3].Width = 45;
            drStatus.Columns[4].HeaderText = "IsRecovery";
            drStatus.Columns[4].Width = 45;
            drStatus.Columns[5].HeaderText = "Errors";
            drStatus.Columns[5].Width= 40;
            drStatus.Columns[6].HeaderText = "ErrorsDetails";
            drStatus.Columns[6].Width = 90;
           // drStatus.Columns[5].HeaderText = "IsEnabled";
             
            for (int i = 0; i < statusList.Count; i++)
            {
                string err = "";
                int errLen = statusList[i].Errors.Length;
                int actualErrCount=0;
                for (int j = 0; j < errLen; j++)
                {
                    if (statusList[i].Errors[j] != null)
                    {
                        err += statusList[i].Errors[j].DateTime + "\r\n" + statusList[i].Errors[j].QueuePath + "\r\n" + statusList[i].Errors[j].Description;
                        actualErrCount += 1;
                    }
                }
                string errCount=errLen.ToString();
                if(actualErrCount != errLen)
                  errCount= actualErrCount.ToString() + "/" + errLen;
                
                drStatus.Rows.Add(statusList[i].PhysicalDatabaseName, statusList[i].ServerName, statusList[i].IsActive, statusList[i].IsFailed, statusList[i].IsRecovering, errCount,err);
            }
     
        }

        private void drStatus_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
              
            try
            {
                if (e.ColumnIndex == 6)
                {
                    DataGridViewRow r = drStatus.Rows[e.RowIndex];
                    string err = r.Cells[6].Value.ToString();
                    if(!String.IsNullOrEmpty(err))
                    {
                      ErrorDetail frmErr=new ErrorDetail();
                       frmErr.ErrText=err;
                      frmErr.Show(this);
                    }
                }
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }

        private void dgHydra_Navigate(object sender, NavigateEventArgs ne)
        {

        }

       
        

        

       

 

 

 


       

      
    }
}