using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace HydraMonitor
{
    public partial class Search : Form
    {
        public Search()
        {
            InitializeComponent();
        }

        private void Search_Load(object sender, EventArgs e)
        {
          
        }

        private void txtComm_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Matchnet.e1.ValueObjects.CommandResult xml = Matchnet.e1.ServiceAdapters.EngineSA.Instance.EvaluateCommand(txtComm.Text, txtOverride.Text);
                txtOut.Text = xml.Xml;
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }
    }
}