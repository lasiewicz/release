﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.ApplicationServer.Caching;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Configuration;
using System.Diagnostics;
using System.Collections;
using Membase;

namespace AppFabricBackerUpper
{
    class Program
    {
        static void Main(string[] args)
        {
            int serverCount = int.Parse(ConfigurationManager.AppSettings["ServerCount"]);
            int cachePort = int.Parse(ConfigurationManager.AppSettings["CachePort"]);
            DataCacheServerEndpoint[] servers = new DataCacheServerEndpoint[serverCount];

            for (int i = 1; i <= serverCount; i++)
            {
                string serverName = String.Format("{0}{1:0#}", ConfigurationManager.AppSettings["ServerNameRoot"], i);
                servers[i - 1] = new DataCacheServerEndpoint(serverName, cachePort);
            }

            Stopwatch sw = new Stopwatch();

            Console.WriteLine("Connecting to the Appfabric cache");

            DataCacheFactoryConfiguration factoryConfig = new DataCacheFactoryConfiguration();
            factoryConfig.Servers = servers;

            sw.Start();
            DataCacheFactory myCacheFactory = new DataCacheFactory(factoryConfig);
            sw.Stop();
            Console.WriteLine("Connection to the Appfabric cache cluster took {0} milliseconds", sw.Elapsed.Milliseconds);
            sw.Reset();
            sw.Start();
            DataCache myDefaultCache = myCacheFactory.GetCache(ConfigurationManager.AppSettings["CacheName"]);
            sw.Stop();
            Console.WriteLine("Retrieving the Appfabric cache took {0} milliseconds", sw.Elapsed.Milliseconds);

            Console.WriteLine("Connected");

            Console.WriteLine("Connecting to the Membase cache");
            Dictionary<string,MembaseClient> _membaseClients = new Dictionary<string,MembaseClient>();
            MembaseClient membaseClient = GetMembaseClient(_membaseClients, ConfigurationManager.AppSettings["CacheName"]);
            Console.WriteLine("Connected");

            if (args.Length > 0)
            {
                if (args[0] == "backup")
                {
                    Console.WriteLine("Backing up data");
                    List<KeyValuePair<string, Object>> cachedObjects = new List<KeyValuePair<string, Object>>();

                    FileStream stream = new FileStream(GetFileName(args), FileMode.Create);
                    BinaryFormatter formatter = new BinaryFormatter();

                    List<string> regions = new List<string>();
                    regions.AddRange(myDefaultCache.GetSystemRegions());
                    int totalObjects = 0;


                    for (int j = int.Parse(args[2]); j <= int.Parse(args[3]); j++ )
                    {
                        try
                        {
                            Console.WriteLine("Backing up Region:{0}", regions[j]);
                            cachedObjects.AddRange(myDefaultCache.GetObjectsInRegion(regions[j]));
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                    formatter.Serialize(stream, cachedObjects);
                    totalObjects += cachedObjects.Count;
                    stream.Close();
                    Console.WriteLine("Backed up {0} objects", totalObjects);
                }
                else if (args[0] == "restore")
                {
                    FileStream stream = null;
                    try
                    {
                        stream = new FileStream(GetFileName(args), FileMode.Open);
                    }
                    catch (System.IO.FileNotFoundException)
                    {
                        Console.WriteLine("The backup file was not found or it was not provided.");
                        return;
                    }
                    BinaryFormatter formatter = new BinaryFormatter();
                    List<KeyValuePair<string, Object>> cachedObjects;
                    cachedObjects = (List<KeyValuePair<string, Object>>)formatter.Deserialize(stream);

                    foreach (KeyValuePair<string, Object> entry in cachedObjects)
                    {
                        myDefaultCache.Put(entry.Key, entry.Value);
                    }
                    Console.WriteLine("Restored {0} objects", cachedObjects.Count);
                }
                else if (args[0] == "restore2membase")
                {
                    FileStream stream = null;
                    try
                    {
                        stream = new FileStream(GetFileName(args), FileMode.Open);
                    }
                    catch (System.IO.FileNotFoundException)
                    {
                        Console.WriteLine("The backup file was not found or it was not provided.");
                        return;
                    }
                    BinaryFormatter formatter = new BinaryFormatter();
                    List<KeyValuePair<string, Object>> cachedObjects;
                    cachedObjects = (List<KeyValuePair<string, Object>>)formatter.Deserialize(stream);

                    foreach (KeyValuePair<string, Object> entry in cachedObjects)
                    {
                        membaseClient.Store(Enyim.Caching.Memcached.StoreMode.Add, entry.Key, entry.Value);
                    }
                    Console.WriteLine("Restored {0} objects to membase", cachedObjects.Count);
                }

            }
            Console.WriteLine("Done");
        }

        private static MembaseClient GetMembaseClient(Dictionary<string,MembaseClient> _membaseClients, string cacheName)
        {
            MembaseClient _membaseClient = null;
            if (_membaseClients.ContainsKey(cacheName))
            {
                _membaseClient = _membaseClients[cacheName];
            }
            else
            {
                _membaseClient = new MembaseClient(cacheName, string.Empty);
                _membaseClients.Add(cacheName, _membaseClient);
            }
            return _membaseClient;
        }

        private static string GetFileName(string[] args)
        {
            if (args.Length > 1)
            {
                return args[1] + ".bin";
            }
            else
            {
                return ConfigurationManager.AppSettings["DefaultFileName"];
            }
        }
    }
}
