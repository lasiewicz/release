using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Collections;
using System.Threading;
using System.Xml;

using Matchnet;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.ApproveQueue.ServiceAdapters;

namespace MemberPhotoDataScrubber
{
    public partial class MainForm : Form
    {
        /// <summary>
        /// NOTE:  THE SCRUB PORTION OF THIS APP DOES NOT WORK WITH OUR .NET 1.1 DLLS.  THUS, USE
        /// MEMBERPHOTODATASCRUBBER1.1 TO DO THE ACTUAL SCRUBBING.  USE THIS APP FOR SCANNING ONLY.
        /// 
        /// Run the following query against all Member partitions.  Output to file and then transfer the files locally to use with this app:
        /// SELECT [MemberPhotoID], '|', [MemberID], '|', [GroupID], '|', [FileID], '|', [FileWebPath], '|', [ThumbFileID], '|', [ThumbFileWebPath], '|', [AdminMemberID], '|',[AlbumID], '|', [ListOrder], '|', [ApprovedFlag], '|',
        /// [Caption], '|', [InsertDate], '|', [PrivateFlag] FROM [MemberPhoto] (nolock)
        /// </summary>
        private const Char Delimiter = '|';
        private const String InputFilePrefix = "MemberPhotoPartition";
        private const String OutputFilePrefix = "MemberPhotoDataScrubberOutput";
        private const String ErrorLogFilePrefix = "MemberPhotoDataScrubberErrorLog";
        private const String ErrorRetryFilePrefix = "MemberPhotoDataScrubberErrorRetry";
        private const String ErrorMsgMemberDoesNotExist = "Member does not exist.";
        private const String FullSize = "Full Size";
        private const String Thumbnail = "Thumbnail";
        private const String MsgNoFiles = "No files to process.";
        private const String Fail = "FAIL";
        private const String Success = "SUCCESS";
        
        private const String RootTag = "ROOT";
        private const String PartitionTag = "PARTITION";
        private const String LineTag = "LINE";
        private const String DataTag = "DATA";
        private const String FileTypeTag = "FILE_TYPE";
        private const String ProblemTag = "PROBLEM";
        private const String ScrubActionTag = "SCRUB_ACTION";

        private const String NoPhotoImagesPath = @"\\File01\c$\Matchnet\bedrock\web\Spark.NoPhoto\NoPhotoImgs";

        private StreamWriter streamWriter;
        private StreamWriter streamWriterErrorLog;
        private StreamWriter streamWriterErrorRetry;

        private ArrayList noPhotoImages = new ArrayList();

        private Thread thread;

        private enum FileType
        {
            FullSize,
            Thumbnail
        }

        private enum ScrubAction
        { 
            DoNothing,
            Delete,
            Requeue
        }

        private delegate void SetTotalProcessedNumLabelCallback(Int64 totalProcessed);
        private delegate void SetTotalRequeuedNumLabelCallback(Int64 totalRequeued);
        private delegate void SetTotalDeletedNumLabelCallback(Int64 totalDeleted);
        private delegate void SetTotalErrorsNumLabelCallback(Int64 totalErrors);
        private delegate void EnableStartButtonCallback();

        public MainForm()
        {
            InitializeComponent();

            // Defaults.
            InputDirTextBox.Text = @"\\dv-wleetest\c$\Matchnet\Bedrock\App\MemberPhotoDataScrubber\1to4\MemberPhotoDataScrubberOutput20051207045155.txt";
            OutputDirTextBox.Text = @"\\dv-wleetest\c$\Matchnet\Bedrock\App\MemberPhotoDataScrubber\1to4";

            InitializeNoPhotoImages();
        }

        /// <summary>
        /// For every NoPhoto image, add a bitmap to noPhotoImages.  There are the images against which the Member photos will be compared.
        /// </summary>
        private void InitializeNoPhotoImages()
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(NoPhotoImagesPath);

            FileInfo[] fileInfos = directoryInfo.GetFiles();

            for (Int32 i = 0; i < fileInfos.Length; i++)
            {
                noPhotoImages.Add(CreateBitmap(fileInfos[i].FullName));
            }
        }

        private void OutputDirButton_Click(object sender, EventArgs e)
        {
            if (OutputFolderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                OutputDirTextBox.Text = OutputFolderBrowserDialog.SelectedPath;
            }
        }

        private void InputDirButton_Click(object sender, EventArgs e)
        {
            if (InputFolderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                InputDirTextBox.Text = InputFolderBrowserDialog.SelectedPath;
            }
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            TotalProcessedNumLabel.Text = "0";
            TotalDeletedNumLabel.Text = "0";
            TotalRequeuedNumLabel.Text = "0";

            StartButton.Enabled = false;
            StopButton.Enabled = true;

            if (ScrubDataCheckBox.Checked)
            {
                thread = new Thread(new ThreadStart(Scrub));
                thread.Start();
            }
            else
            {
                thread = new Thread(new ThreadStart(Scan));
                thread.Start();
            }
        }

        private void StopButton_Click(object sender, EventArgs e)
        {
            StartButton.Enabled = true;
            StopButton.Enabled = false;

            if (thread != null)
            {
                thread.Abort();
                thread.Join();
            }
        }

        private void ParseLineNode(XmlNode xmlNode, ref Int32 memberID, ref Int32 memberPhotoID, ref Int32 communityID, ref ScrubAction scrubAction)
        {
            foreach (XmlNode xmlChildNode in xmlNode.ChildNodes)
            {
                switch (xmlChildNode.Name)
                { 
                    case DataTag:
                        ParseLine(xmlChildNode.InnerText, ref memberPhotoID, ref memberID, ref communityID);
                        break;

                    case ScrubActionTag:
                        scrubAction = ((ScrubAction)Enum.Parse(typeof(ScrubAction), (xmlChildNode.InnerText)));
                        break;

                    default:
                        // Ignore the rest.
                        break;
                }
            
            }
        }

        private void Scrub()
        {
            if (MessageBox.Show(@"YOU ARE ABOUT TO CHANGE / DELETE DATA ON THE PRODUCTION DATABASES.  ARE YOU SURE YOU WANT TO DO THIS?!", "Please Confirm", MessageBoxButtons.YesNoCancel) != DialogResult.Yes)
            {
                thread.Abort();
                thread.Join();
                return;
            }

            // Load the data into an XmlDoc.
            XmlDocument xmlDocument = new XmlDocument();

            try
            {
                xmlDocument.Load(InputDirTextBox.Text.Trim());
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"You must enter a valid file into the 'input file' textbox.  (" + ex.Message + ")");

                thread.Abort();
                thread.Join();
                return;
            }

            Int64 totalProcessed = 0;
            Int64 totalRequeued = 0;
            Int64 totalDeleted = 0;

            String dateString = DateTime.Now.ToString("yyyyMMddhhmmss");
            streamWriter = new StreamWriter(OutputDirTextBox.Text + "\\" + OutputFilePrefix + dateString + "_SCRUB.txt");
            streamWriter.AutoFlush = true;

            InitializeErrorHandling(dateString, true);

            streamWriter.WriteLine("<" + RootTag + ">");

            // Find all the LINE nodes.
            XmlNodeList xmlNodeList = xmlDocument.GetElementsByTagName(LineTag);

            foreach (XmlNode xmlNode in xmlNodeList)
            {
                Int32 memberID = Constants.NULL_INT;
                Int32 memberPhotoID = Constants.NULL_INT;
                Int32 communityID = Constants.NULL_INT;
                ScrubAction scrubAction = ScrubAction.DoNothing;

                ParseLineNode(xmlNode, ref memberID, ref memberPhotoID, ref communityID, ref scrubAction);
                if (communityID == 9)
                    continue;
                String returnVal = "NO ACTION";
                switch (scrubAction)
                {
                    case ScrubAction.Delete:
                        returnVal = Delete(memberID, communityID, memberPhotoID);

                        totalDeleted++;
                        SetTotalDeleted(totalDeleted);
                        break;

                    case ScrubAction.Requeue:
                        returnVal = Requeue(memberID, communityID, memberPhotoID);

                        totalRequeued++;
                        SetTotalRequeued(totalRequeued);
                        break;

                    default:
                        // Do nothing.
                        break;
                }

                streamWriter.WriteLine("<" + LineTag + " MemberPhotoID=\"" + memberPhotoID+ "\" Scrub_Action=\"" + scrubAction.ToString() + "\" ReturnVal=\"" + returnVal + "\">");
                streamWriter.WriteLine("</" + LineTag + ">");

                totalProcessed++;
                SetTotalProcessed(totalProcessed);
            }

            streamWriter.WriteLine("</" + RootTag + ">");

            streamWriter.Close();

            CleanUpErrorHandling();

            EnableStartButton();

            thread.Abort();
            thread.Join();
        }

        private FileInfo[] GetFiles()
        {
            // Iterate through all the MemberPhotoXX.txt files in the InputDir.
            DirectoryInfo directoryInfo = new DirectoryInfo(InputDirTextBox.Text.Trim());

            FileInfo[] fileInfos = directoryInfo.GetFiles(InputFilePrefix + "*");

            return fileInfos;
        }

        private String GetPartition(FileInfo fileInfo)
        {
            return fileInfo.Name.Substring(InputFilePrefix.Length, fileInfo.Name.IndexOf('.') - InputFilePrefix.Length);
        }

        private void ParseLine(String line, ref Int32 memberPhotoID, ref Int32 memberID, ref String fileWebPath,
            ref String thumbFileWebPath, ref Boolean isApproved, ref Int32 communityID)
        {
            String[] items = line.Split(new char[] { Delimiter });

            memberPhotoID = Convert.ToInt32(items[0].ToString().Trim());
            memberID = Convert.ToInt32(items[1].ToString().Trim());
            fileWebPath = items[4].ToString().Trim();
            thumbFileWebPath = items[6].ToString().Trim();
            isApproved = Convert.ToBoolean(Convert.ToByte(items[10].ToString().Trim()));
            communityID = Convert.ToInt32(items[2].ToString().Trim());
        }

        private void ParseLine(String line, ref Int32 memberPhotoID, ref Int32 memberID, ref Int32 communityID)
        {
            String[] items = line.Split(new char[] { Delimiter });

            memberPhotoID = Convert.ToInt32(items[0].ToString().Trim());
            memberID = Convert.ToInt32(items[1].ToString().Trim());
            communityID = Convert.ToInt32(items[2].ToString().Trim());
        }

        private void InitializeErrorHandling(String dateString)
        {
            InitializeErrorHandling(dateString, false);
        }

        private void InitializeErrorHandling(String dateString, Boolean isScrub)
        {
            String extension = ".txt";
            if (isScrub)
            {
                extension = "_SCRUB.txt";
            }

            streamWriterErrorLog = new StreamWriter(OutputDirTextBox.Text + "\\" + ErrorLogFilePrefix + dateString + extension);
            streamWriterErrorLog.AutoFlush = true;

            streamWriterErrorRetry = new StreamWriter(OutputDirTextBox.Text + "\\" + ErrorRetryFilePrefix + dateString + extension);
            streamWriterErrorRetry.AutoFlush = true;
        }

        private void CleanUpErrorHandling()
        {
            streamWriterErrorLog.Close();

            streamWriterErrorRetry.Close();
        }

        private void ProcessError(String errorMsg, ref Int64 totalErrors, String line)
        {
            streamWriterErrorLog.WriteLine(errorMsg);

            if (line != null && line != String.Empty)
            {
                streamWriterErrorRetry.WriteLine(line);
            }

            SetTotalErrors(totalErrors);
        }

        private void Scan()
        {
            FileInfo[] fileInfos = GetFiles();

            if (fileInfos.Length > 0)
            {
                Int64 totalProcessed = 0;
                Int64 totalRequeued = 0;
                Int64 totalDeleted = 0;
                Int64 totalErrors = 0;

                String dateString = DateTime.Now.ToString("yyyyMMddhhmmss");
                streamWriter = new StreamWriter(OutputDirTextBox.Text + "\\" + OutputFilePrefix + dateString + ".txt");
                streamWriter.AutoFlush = true;

                InitializeErrorHandling(dateString);
                
                streamWriter.WriteLine("<" + RootTag  + ">");

                for (Int32 i = 0; i < fileInfos.Length; i++)
                {
                    String partition = GetPartition(fileInfos[i]);

                    // Add a header to the Output file.
                    streamWriter.WriteLine("<" + PartitionTag + " Partition=\"" + partition + "\">");

                    try
                    {
                        StreamReader streamReader = new StreamReader(new FileStream(fileInfos[i].FullName, FileMode.Open, FileAccess.Read));

                        String line;
                        Int64 lineNum = 1;
                        while ((line = streamReader.ReadLine()) != null)
                        {
                            try
                            {
                                ScrubAction scrubAction = ScrubAction.DoNothing;
                
                                // Skip the first 2 lines.
                                if (line.IndexOf("MemberPhotoID") == 0 || line.IndexOf("-------------") == 0 || line.IndexOf("(") == 0)
                                    continue;

                                String[] items = line.Split(new char[] { Delimiter });

                                Int32 memberPhotoID = Constants.NULL_INT;
                                Int32 memberID = Constants.NULL_INT;
                                String fileWebPath = Constants.NULL_STRING;
                                String thumbFileWebPath = Constants.NULL_STRING;
                                Boolean isApproved = false;
                                Int32 communityID = Constants.NULL_INT;

                                ParseLine(line, ref memberPhotoID, ref memberID, ref fileWebPath, ref thumbFileWebPath, ref isApproved, ref communityID);

                                if (fileWebPath.IndexOf("http:") == 0)
                                {
                                    try
                                    {
                                        if (!IsValidPhoto(fileWebPath))
                                        {
                                            AddToOutput(ref streamWriter, line, FileType.FullSize, "FileWebPath incorrect (404)", scrubAction = ScrubAction.Delete);
                                        }
                                    }
                                    catch
                                    {
                                        AddToOutput(ref streamWriter, line, FileType.FullSize, "Trying to load and compare the File thew an error.", scrubAction = ScrubAction.Delete);
                                    }
                                }
                                else
                                {
                                    AddToOutput(ref streamWriter, line, FileType.FullSize, "FileWebPath is null", scrubAction = ScrubAction.Delete);
                                }

                                if (thumbFileWebPath.IndexOf("http:") == 0)
                                {
                                    try
                                    {
                                        if (!IsValidPhoto(thumbFileWebPath))
                                        {
                                            AddToOutput(ref streamWriter, line, FileType.Thumbnail, "ThumbFileWebPath is incorrect (404)", scrubAction = ScrubAction.Requeue);
                                        }
                                    }
                                    catch
                                    {
                                        AddToOutput(ref streamWriter, line, FileType.Thumbnail, "Trying to load and compare the Thumb thew an error.", scrubAction = ScrubAction.Requeue);
                                    }
                                }
                                else if (isApproved)
                                {
                                    AddToOutput(ref streamWriter, line, FileType.Thumbnail, "ThumbFileWebPath is null", scrubAction = ScrubAction.Requeue);
                                }

                                totalProcessed++;
                                SetTotalProcessed(totalProcessed);

                                switch (scrubAction)
                                {
                                    case ScrubAction.Delete:
                                        totalDeleted++;
                                        SetTotalDeleted(totalDeleted);
                                        break;

                                    case ScrubAction.Requeue:
                                        totalRequeued++;
                                        SetTotalRequeued(totalRequeued);
                                        break;
                                }

                                lineNum++;
                            }
                            catch (Exception ex)
                            {
                                ProcessError("General exception while parsing line number " + lineNum.ToString() + " of file " + fileInfos[i].Name + ".  (" + ex.Message + ")", ref totalErrors, line);

                                continue;
                            }
                        }

                        streamWriter.WriteLine("</" + PartitionTag + ">");

                        streamReader.Close();
                    }
                    catch (Exception ex)
                    {
                        ProcessError("General exception while parsing file " + fileInfos[i].Name + ".  (" + ex.Message + ")", ref totalErrors, null);
                    }
                }

                streamWriter.WriteLine("</" + RootTag + ">");

                streamWriter.Close();

                CleanUpErrorHandling();
            }
            else
            {
                MessageBox.Show(MsgNoFiles);
            }

            EnableStartButton();

            thread.Abort();
            thread.Join();
        }

        private void AddToOutput(ref StreamWriter streamWriter, String line, FileType fileType, String problem, ScrubAction scrubAction)
        {
            streamWriter.WriteLine("<" + LineTag + "><" + DataTag + ">" + line + "</" + DataTag + "><" + FileTypeTag + ">" + fileType.ToString() + "</" + FileTypeTag + "><" + ProblemTag + ">" + problem + "</" + ProblemTag + "><" + ScrubActionTag + ">" + scrubAction.ToString() + "</" + ScrubActionTag + "></" + LineTag + ">");
        }

        /// <summary>
        /// Compares the photo against all the NoPhotos.
        /// </summary>
        /// <param name="fileWebPath"></param>
        /// <returns></returns>
        private Boolean IsValidPhoto(String fileWebPath)
        {
            Bitmap photo = CreateBitmap(fileWebPath);
            foreach (Object obj in noPhotoImages)
            {
               Bitmap noPhoto = ((Bitmap)obj);

                // If the images are the same, we are viewing a NoPhoto image.
               if (ImageCompare.Compare(noPhoto, photo) == ImageCompare.CompareResult.ciCompareOk)
                    return false;
            }

            return true;
        }

        private Bitmap CreateBitmap(String path)
        {
            WebClient webClient = new WebClient();
            MemoryStream stream = new MemoryStream(webClient.DownloadData(path));

            // stream.Close(); //http://support.microsoft.com/?id=814675

            return new Bitmap(stream, false);
        }

        private void ScrubDataCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ScrubDataCheckBox.Checked)
            {
                StartButton.Text = "Scrub";
            }
            else
            {
                StartButton.Text = "Scan";
            }
        }

        #region Callbacks
        // Use these for making threadsafe calls on a Window Form control. (ms-help://MS.VSCC.v80/MS.MSDN.v80/MS.VisualStudio.v80.en/dv_fxmclictl/html/138f38b6-1099-4fd5-910c-390b41cbad35.htm).
        private void SetTotalProcessed(Int64 totalProcessed)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (TotalProcessedNumLabel.InvokeRequired)
            {
                SetTotalProcessedNumLabelCallback setTotalProcessedNumLabelCallback = new SetTotalProcessedNumLabelCallback(SetTotalProcessed);
                Invoke(setTotalProcessedNumLabelCallback, new object[] { totalProcessed });
            }
            else
            {
                TotalProcessedNumLabel.Text = totalProcessed.ToString();
            }
        }

        private void SetTotalRequeued(Int64 totalRequeued)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (TotalRequeuedNumLabel.InvokeRequired)
            {
                SetTotalRequeuedNumLabelCallback setTotalRequeuedNumLabelCallback = new SetTotalRequeuedNumLabelCallback(SetTotalRequeued);
                Invoke(setTotalRequeuedNumLabelCallback, new object[] { totalRequeued });
            }
            else
            {
                TotalRequeuedNumLabel.Text = totalRequeued.ToString();
            }
        }

        private void SetTotalDeleted(Int64 totalDeleted)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (TotalDeletedNumLabel.InvokeRequired)
            {
                SetTotalDeletedNumLabelCallback setTotalDeletedNumLabelCallback = new SetTotalDeletedNumLabelCallback(SetTotalDeleted);
                Invoke(setTotalDeletedNumLabelCallback, new object[] { totalDeleted });
            }
            else
            {
                TotalDeletedNumLabel.Text = totalDeleted.ToString();
            }
        }

        private void SetTotalErrors(Int64 totalErrors)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (TotalErrorsNumLabel.InvokeRequired)
            {
                SetTotalErrorsNumLabelCallback setTotalErrorsNumLabelCallback = new SetTotalErrorsNumLabelCallback(SetTotalErrors);
                Invoke(setTotalErrorsNumLabelCallback, new object[] { totalErrors });
            }
            else
            {
                TotalErrorsNumLabel.Text = totalErrors.ToString();
            }
        }

        private void EnableStartButton()
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (StartButton.InvokeRequired)
            {
                EnableStartButtonCallback enableStartButtonCallback = new EnableStartButtonCallback(EnableStartButton);
                Invoke(enableStartButtonCallback, new object[] { });
            }
            else
            {
                StartButton.Enabled = true;
            }
        }
        #endregion

        #region Scrub
        private String Delete(Int32 memberID, Int32 communityID, Int32 memberPhotoID)
        {
            Member member;

            try
            {
                member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
            }
            catch (Exception ex)
            {
                return Fail + " [" + ErrorMsgMemberDoesNotExist + "](" + ex.Message + ")";
            }

            try
            {
                PhotoUpdate deletePhotoUpdate = new PhotoUpdate(null,
                    true,
                    memberPhotoID,
                    Constants.NULL_INT,
                    Constants.NULL_STRING,
                    Constants.NULL_INT,
                    Constants.NULL_STRING,
                    0,
                    false,
                    false,
                    Constants.NULL_INT,
                    Constants.NULL_INT);

                MemberSA.Instance.SavePhotos(communityID,
                    memberID,
                     new PhotoUpdate[] { deletePhotoUpdate });

                return Success;
            }
            catch (Exception ex)
            {
                return Fail + " (" + ex.Message + ")";
            }
        }

        private String Requeue(Int32 memberID, Int32 communityID, Int32 memberPhotoID)
        {
            try
            {
                Member member;

                try
                {
                    member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
                }
                catch (Exception ex)
                {
                    return Fail + " [" + ErrorMsgMemberDoesNotExist + "](" + ex.Message + ")";
                }

                if (member == null)
                {
                    return Fail + " [" + ErrorMsgMemberDoesNotExist + "]";
                }

                Photo photo = member.GetPhotos(communityID).Find(memberPhotoID);

                Int32 fileID = photo.FileID;
                String fileWebPath = photo.FileWebPath;
                Int32 thumbFileID = photo.ThumbFileID;
                String thumbFileWebPath = photo.ThumbFileWebPath;
                Byte listOrder = photo.ListOrder;
                Int32 albumID = photo.AlbumID;
                Int32 adminMemberID = photo.AdminMemberID;
                Boolean isApproved = false;
                Boolean isPrivate = photo.IsPrivate;

                PhotoUpdate photoUpdate = new PhotoUpdate(null,
                    false,
                    memberPhotoID,
                    fileID,
                    fileWebPath,
                    thumbFileID,
                    thumbFileWebPath,
                    listOrder,
                    isApproved,
                    isPrivate,
                    albumID,
                    adminMemberID);

                MemberSA.Instance.SavePhotos(communityID,
                    memberID,
                    new PhotoUpdate[] { photoUpdate });

                ApproveQueueSA.Instance.QueuePhoto(communityID,
                    memberID,
                    memberPhotoID);

                return Success;
            }
            catch (Exception ex)
            {
                return Fail + " (" + ex.Message + ")";
            }
        }
        #endregion

        /// <summary>
        /// This will retrieve a given line from one of our .rpt files.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetButton_Click(object sender, EventArgs e)
        {
            FileInfo[] fileInfos = GetFiles();

            for (Int32 i = 0; i < fileInfos.Length; i++)
            {
                if (PartitionTextBox.Text.Trim() == GetPartition(fileInfos[i]))
                {
                    StreamReader streamReader = new StreamReader(new FileStream(fileInfos[i].FullName, FileMode.Open, FileAccess.Read));

                    String line;
                    Int32 lineNum = 1;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        if (lineNum == Convert.ToInt32(LineNumTextBox.Text.Trim()))
                        {
                            OutputTextBox.Text = line;

                            return;
                        }

                        lineNum++;
                    }
                }
            }
        }

        /// <summary>
        /// Counts the number of occurances of a particular word.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CountButton_Click(object sender, EventArgs e)
        {
            StreamReader streamReader = new StreamReader(new FileStream(InputDirTextBox.Text, FileMode.Open, FileAccess.Read));

            String line;
            Int32 total = 0;
            while ((line = streamReader.ReadLine()) != null)
            {
                line = line.ToLower();
                if (line.IndexOf(SearchWordTextBox.Text.Trim().ToLower()) > -1)
                {
                    total++;
                }
            }

            MessageBox.Show(total.ToString());
        }
    }
}