namespace MemberPhotoDataScrubber
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);

            // Abort child thread.
            if (thread != null)
            {
                thread.Abort();
                thread.Join();
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OutputFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.OutputDirButton = new System.Windows.Forms.Button();
            this.OutputDirTextBox = new System.Windows.Forms.TextBox();
            this.OutputDirLabel = new System.Windows.Forms.Label();
            this.InputDirLabel = new System.Windows.Forms.Label();
            this.InputDirTextBox = new System.Windows.Forms.TextBox();
            this.InputDirButton = new System.Windows.Forms.Button();
            this.InputFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.StartButton = new System.Windows.Forms.Button();
            this.ScrubDataCheckBox = new System.Windows.Forms.CheckBox();
            this.TotalProcessedLabel = new System.Windows.Forms.Label();
            this.TotalProcessedNumLabel = new System.Windows.Forms.Label();
            this.TotalRequeuedNumLabel = new System.Windows.Forms.Label();
            this.TotalRequeuedLabel = new System.Windows.Forms.Label();
            this.TotalDeletedNumLabel = new System.Windows.Forms.Label();
            this.TotalDeletedLabel = new System.Windows.Forms.Label();
            this.StopButton = new System.Windows.Forms.Button();
            this.TotalErrorsNumLabel = new System.Windows.Forms.Label();
            this.TotalErrorsLabel = new System.Windows.Forms.Label();
            this.OutputTextBox = new System.Windows.Forms.TextBox();
            this.LineNumTextBox = new System.Windows.Forms.TextBox();
            this.PartitionTextBox = new System.Windows.Forms.TextBox();
            this.GetButton = new System.Windows.Forms.Button();
            this.SearchWordTextBox = new System.Windows.Forms.TextBox();
            this.CountButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // OutputDirButton
            // 
            this.OutputDirButton.Location = new System.Drawing.Point(285, 120);
            this.OutputDirButton.Name = "OutputDirButton";
            this.OutputDirButton.Size = new System.Drawing.Size(60, 23);
            this.OutputDirButton.TabIndex = 0;
            this.OutputDirButton.Text = "Browse";
            this.OutputDirButton.UseVisualStyleBackColor = true;
            this.OutputDirButton.Click += new System.EventHandler(this.OutputDirButton_Click);
            // 
            // OutputDirTextBox
            // 
            this.OutputDirTextBox.Location = new System.Drawing.Point(12, 120);
            this.OutputDirTextBox.Name = "OutputDirTextBox";
            this.OutputDirTextBox.Size = new System.Drawing.Size(267, 20);
            this.OutputDirTextBox.TabIndex = 1;
            // 
            // OutputDirLabel
            // 
            this.OutputDirLabel.AutoSize = true;
            this.OutputDirLabel.Location = new System.Drawing.Point(12, 104);
            this.OutputDirLabel.Name = "OutputDirLabel";
            this.OutputDirLabel.Size = new System.Drawing.Size(130, 13);
            this.OutputDirLabel.TabIndex = 2;
            this.OutputDirLabel.Text = "Location of the output file:";
            // 
            // InputDirLabel
            // 
            this.InputDirLabel.AutoSize = true;
            this.InputDirLabel.Location = new System.Drawing.Point(12, 18);
            this.InputDirLabel.Name = "InputDirLabel";
            this.InputDirLabel.Size = new System.Drawing.Size(123, 13);
            this.InputDirLabel.TabIndex = 5;
            this.InputDirLabel.Text = "Location of the input file:";
            // 
            // InputDirTextBox
            // 
            this.InputDirTextBox.Location = new System.Drawing.Point(12, 34);
            this.InputDirTextBox.Name = "InputDirTextBox";
            this.InputDirTextBox.Size = new System.Drawing.Size(267, 20);
            this.InputDirTextBox.TabIndex = 4;
            // 
            // InputDirButton
            // 
            this.InputDirButton.Location = new System.Drawing.Point(285, 34);
            this.InputDirButton.Name = "InputDirButton";
            this.InputDirButton.Size = new System.Drawing.Size(60, 23);
            this.InputDirButton.TabIndex = 3;
            this.InputDirButton.Text = "Browse";
            this.InputDirButton.UseVisualStyleBackColor = true;
            this.InputDirButton.Click += new System.EventHandler(this.InputDirButton_Click);
            // 
            // StartButton
            // 
            this.StartButton.Location = new System.Drawing.Point(12, 226);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(267, 23);
            this.StartButton.TabIndex = 6;
            this.StartButton.Text = "Scan";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // ScrubDataCheckBox
            // 
            this.ScrubDataCheckBox.AutoSize = true;
            this.ScrubDataCheckBox.Location = new System.Drawing.Point(12, 193);
            this.ScrubDataCheckBox.Name = "ScrubDataCheckBox";
            this.ScrubDataCheckBox.Size = new System.Drawing.Size(209, 17);
            this.ScrubDataCheckBox.TabIndex = 7;
            this.ScrubDataCheckBox.Text = "I WANT TO SCRUB (CHANGE) DATA";
            this.ScrubDataCheckBox.UseVisualStyleBackColor = true;
            this.ScrubDataCheckBox.CheckedChanged += new System.EventHandler(this.ScrubDataCheckBox_CheckedChanged);
            // 
            // TotalProcessedLabel
            // 
            this.TotalProcessedLabel.AutoSize = true;
            this.TotalProcessedLabel.Location = new System.Drawing.Point(9, 298);
            this.TotalProcessedLabel.Name = "TotalProcessedLabel";
            this.TotalProcessedLabel.Size = new System.Drawing.Size(87, 13);
            this.TotalProcessedLabel.TabIndex = 8;
            this.TotalProcessedLabel.Text = "Total Processed:";
            // 
            // TotalProcessedNumLabel
            // 
            this.TotalProcessedNumLabel.AutoSize = true;
            this.TotalProcessedNumLabel.Location = new System.Drawing.Point(102, 298);
            this.TotalProcessedNumLabel.Name = "TotalProcessedNumLabel";
            this.TotalProcessedNumLabel.Size = new System.Drawing.Size(13, 13);
            this.TotalProcessedNumLabel.TabIndex = 9;
            this.TotalProcessedNumLabel.Text = "0";
            // 
            // TotalRequeuedNumLabel
            // 
            this.TotalRequeuedNumLabel.AutoSize = true;
            this.TotalRequeuedNumLabel.Location = new System.Drawing.Point(102, 311);
            this.TotalRequeuedNumLabel.Name = "TotalRequeuedNumLabel";
            this.TotalRequeuedNumLabel.Size = new System.Drawing.Size(13, 13);
            this.TotalRequeuedNumLabel.TabIndex = 11;
            this.TotalRequeuedNumLabel.Text = "0";
            // 
            // TotalRequeuedLabel
            // 
            this.TotalRequeuedLabel.AutoSize = true;
            this.TotalRequeuedLabel.Location = new System.Drawing.Point(9, 311);
            this.TotalRequeuedLabel.Name = "TotalRequeuedLabel";
            this.TotalRequeuedLabel.Size = new System.Drawing.Size(87, 13);
            this.TotalRequeuedLabel.TabIndex = 10;
            this.TotalRequeuedLabel.Text = "Total Requeued:";
            // 
            // TotalDeletedNumLabel
            // 
            this.TotalDeletedNumLabel.AutoSize = true;
            this.TotalDeletedNumLabel.Location = new System.Drawing.Point(102, 324);
            this.TotalDeletedNumLabel.Name = "TotalDeletedNumLabel";
            this.TotalDeletedNumLabel.Size = new System.Drawing.Size(13, 13);
            this.TotalDeletedNumLabel.TabIndex = 13;
            this.TotalDeletedNumLabel.Text = "0";
            // 
            // TotalDeletedLabel
            // 
            this.TotalDeletedLabel.AutoSize = true;
            this.TotalDeletedLabel.Location = new System.Drawing.Point(9, 324);
            this.TotalDeletedLabel.Name = "TotalDeletedLabel";
            this.TotalDeletedLabel.Size = new System.Drawing.Size(74, 13);
            this.TotalDeletedLabel.TabIndex = 12;
            this.TotalDeletedLabel.Text = "Total Deleted:";
            // 
            // StopButton
            // 
            this.StopButton.Enabled = false;
            this.StopButton.Location = new System.Drawing.Point(12, 255);
            this.StopButton.Name = "StopButton";
            this.StopButton.Size = new System.Drawing.Size(267, 23);
            this.StopButton.TabIndex = 14;
            this.StopButton.Text = "Stop";
            this.StopButton.UseVisualStyleBackColor = true;
            this.StopButton.Click += new System.EventHandler(this.StopButton_Click);
            // 
            // TotalErrorsNumLabel
            // 
            this.TotalErrorsNumLabel.AutoSize = true;
            this.TotalErrorsNumLabel.Location = new System.Drawing.Point(102, 337);
            this.TotalErrorsNumLabel.Name = "TotalErrorsNumLabel";
            this.TotalErrorsNumLabel.Size = new System.Drawing.Size(13, 13);
            this.TotalErrorsNumLabel.TabIndex = 16;
            this.TotalErrorsNumLabel.Text = "0";
            // 
            // TotalErrorsLabel
            // 
            this.TotalErrorsLabel.AutoSize = true;
            this.TotalErrorsLabel.Location = new System.Drawing.Point(9, 337);
            this.TotalErrorsLabel.Name = "TotalErrorsLabel";
            this.TotalErrorsLabel.Size = new System.Drawing.Size(64, 13);
            this.TotalErrorsLabel.TabIndex = 15;
            this.TotalErrorsLabel.Text = "Total Errors:";
            // 
            // OutputTextBox
            // 
            this.OutputTextBox.Location = new System.Drawing.Point(12, 389);
            this.OutputTextBox.Multiline = true;
            this.OutputTextBox.Name = "OutputTextBox";
            this.OutputTextBox.Size = new System.Drawing.Size(333, 54);
            this.OutputTextBox.TabIndex = 17;
            // 
            // LineNumTextBox
            // 
            this.LineNumTextBox.Location = new System.Drawing.Point(55, 363);
            this.LineNumTextBox.MaxLength = 12;
            this.LineNumTextBox.Name = "LineNumTextBox";
            this.LineNumTextBox.Size = new System.Drawing.Size(100, 20);
            this.LineNumTextBox.TabIndex = 18;
            // 
            // PartitionTextBox
            // 
            this.PartitionTextBox.Location = new System.Drawing.Point(12, 363);
            this.PartitionTextBox.MaxLength = 2;
            this.PartitionTextBox.Name = "PartitionTextBox";
            this.PartitionTextBox.Size = new System.Drawing.Size(37, 20);
            this.PartitionTextBox.TabIndex = 19;
            // 
            // GetButton
            // 
            this.GetButton.Location = new System.Drawing.Point(161, 360);
            this.GetButton.Name = "GetButton";
            this.GetButton.Size = new System.Drawing.Size(75, 23);
            this.GetButton.TabIndex = 20;
            this.GetButton.Text = "Get";
            this.GetButton.UseVisualStyleBackColor = true;
            this.GetButton.Click += new System.EventHandler(this.GetButton_Click);
            // 
            // SearchWordTextBox
            // 
            this.SearchWordTextBox.Location = new System.Drawing.Point(125, 60);
            this.SearchWordTextBox.Name = "SearchWordTextBox";
            this.SearchWordTextBox.Size = new System.Drawing.Size(154, 20);
            this.SearchWordTextBox.TabIndex = 21;
            // 
            // CountButton
            // 
            this.CountButton.Location = new System.Drawing.Point(285, 60);
            this.CountButton.Name = "CountButton";
            this.CountButton.Size = new System.Drawing.Size(60, 23);
            this.CountButton.TabIndex = 22;
            this.CountButton.Text = "Count";
            this.CountButton.UseVisualStyleBackColor = true;
            this.CountButton.Click += new System.EventHandler(this.CountButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(357, 463);
            this.Controls.Add(this.CountButton);
            this.Controls.Add(this.SearchWordTextBox);
            this.Controls.Add(this.GetButton);
            this.Controls.Add(this.PartitionTextBox);
            this.Controls.Add(this.LineNumTextBox);
            this.Controls.Add(this.OutputTextBox);
            this.Controls.Add(this.TotalErrorsNumLabel);
            this.Controls.Add(this.TotalErrorsLabel);
            this.Controls.Add(this.StopButton);
            this.Controls.Add(this.TotalDeletedNumLabel);
            this.Controls.Add(this.TotalDeletedLabel);
            this.Controls.Add(this.TotalRequeuedNumLabel);
            this.Controls.Add(this.TotalRequeuedLabel);
            this.Controls.Add(this.TotalProcessedNumLabel);
            this.Controls.Add(this.TotalProcessedLabel);
            this.Controls.Add(this.ScrubDataCheckBox);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.InputDirLabel);
            this.Controls.Add(this.InputDirTextBox);
            this.Controls.Add(this.InputDirButton);
            this.Controls.Add(this.OutputDirLabel);
            this.Controls.Add(this.OutputDirTextBox);
            this.Controls.Add(this.OutputDirButton);
            this.Name = "MainForm";
            this.Text = "MemberPhoto Data Scrubber";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog OutputFolderBrowserDialog;
        private System.Windows.Forms.Button OutputDirButton;
        private System.Windows.Forms.TextBox OutputDirTextBox;
        private System.Windows.Forms.Label OutputDirLabel;
        private System.Windows.Forms.Label InputDirLabel;
        private System.Windows.Forms.TextBox InputDirTextBox;
        private System.Windows.Forms.Button InputDirButton;
        private System.Windows.Forms.FolderBrowserDialog InputFolderBrowserDialog;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.CheckBox ScrubDataCheckBox;
        private System.Windows.Forms.Label TotalProcessedLabel;
        private System.Windows.Forms.Label TotalProcessedNumLabel;
        private System.Windows.Forms.Label TotalRequeuedNumLabel;
        private System.Windows.Forms.Label TotalRequeuedLabel;
        private System.Windows.Forms.Label TotalDeletedNumLabel;
        private System.Windows.Forms.Label TotalDeletedLabel;
        private System.Windows.Forms.Button StopButton;
        private System.Windows.Forms.Label TotalErrorsNumLabel;
        private System.Windows.Forms.Label TotalErrorsLabel;
        private System.Windows.Forms.TextBox OutputTextBox;
        private System.Windows.Forms.TextBox LineNumTextBox;
        private System.Windows.Forms.TextBox PartitionTextBox;
        private System.Windows.Forms.Button GetButton;
        private System.Windows.Forms.TextBox SearchWordTextBox;
        private System.Windows.Forms.Button CountButton;
    }
}

