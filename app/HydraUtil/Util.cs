using System;
using System.Collections;
using System.Threading;

using Matchnet;
using Matchnet.RemotingServices.ValueObjects;
using Matchnet.RemotingServices.ValueObjects.ServiceDefinitions;
using Matchnet.Data.Hydra;
using Matchnet.Configuration.ValueObjects.Lpd;

namespace HydraUtil
{
	class Util
	{
		[STAThread]
		static void Main(string[] args)
		{
			ArrayList list = new ArrayList();
			string [] hosts;

			if ( args.Length == 0)
			{
				hosts = new string[]{
										"lavcpurchase01:45300",
                                        "lavcpurchase02:45300"
							};
				//};
				/*
				hosts = new string[]{
							"172.16.1.81:46000",
							"172.16.1.82:46000",
							"172.16.1.131:46000",
							"172.16.1.132:46000"
									};
				*/
			}
			else
			{
				hosts = args;
			}

			//hosts = new string[]{"172.16.1.121:48000"};


			while (true)
			{
				bool foundError = false;

				foreach (string host in hosts)
				{
					string uri = "tcp://" + host + "/HydraManagerSM.rem";
					IHydraManagerService svc = null;
					PhysicalDatabaseStatus[] physicalDatabaseStatusList = null;

					try
					{
						svc = ((IHydraManagerService)Activator.GetObject(typeof(IHydraManagerService), uri));
						physicalDatabaseStatusList = svc.GetPhysicalDatabaseStatus();
					}
					catch (Exception ex)
					{
						Console.WriteLine("unable to get status for " + uri + " (" + ex.Message + ")");
					}

					if (physicalDatabaseStatusList != null)
					{
						foreach (PhysicalDatabaseStatus physicalDatabaseStatus in physicalDatabaseStatusList)
						{
							//Console.WriteLine(physicalDatabaseStatus.ServerName + ", " + physicalDatabaseStatus.PhysicalDatabaseName);
							foreach (HydraError error in physicalDatabaseStatus.Errors)
							{
								if (error != null)
								{
									foundError = true;

									Console.WriteLine("MSMQ Host:" + host.ToString() + System.Environment.NewLine);
									Console.WriteLine(error.QueuePath + "\t" + error.MessageID + System.Environment.NewLine);
									Console.WriteLine(error.Description+ System.Environment.NewLine);
									
									if (error.Description.ToLower().IndexOf("up_logonmember_save") > -1)
									{
										if (!list.Contains(error.MessageID))
										{
											try
											{
												svc.GetMessage(error.QueuePath,
													error.MessageID);
												Console.WriteLine("message deleted");
												list.Add(error.MessageID);
											}
											catch (Exception ex)
											{
												Console.WriteLine(ex.ToString());
											}
										}
									}
									else if (error.Description.ToLower().IndexOf("up_searchpreference_savemember") > -1 || error.Description.ToLower().IndexOf("cannot insert duplicate key in object 'maillog'") > -1)
									{
										if (!list.Contains(error.MessageID))
										{
											try
											{
												svc.GetMessage(error.QueuePath,
													error.MessageID);
												Console.WriteLine("message deleted");
												list.Add(error.MessageID);
											}
											catch (Exception ex)
											{
												Console.WriteLine(ex.ToString());
											}
										}
									}
									else if (error.Description.ToLower().IndexOf("timeout expired") > -1)
									{
										Console.WriteLine("skip - timeout");
									}
									else if (error.Description.ToLower().IndexOf("deadlock") > -1)
									{
										Console.WriteLine("skip - deadlock");
									}
									else if (error.Description.ToLower().IndexOf("duplicate key") > -1
										&& (error.Description.ToLower().IndexOf("up_ynmlist_save") > -1 
										|| error.Description.ToLower().IndexOf("up_list") > -1))
									{
										Console.WriteLine("skip - list PK");
									}
									else if (error.Description.ToLower().IndexOf("duplicate key") > -1
										&& (error.Description.ToLower().IndexOf("up_messagelist_save") > -1)
										|| (error.Description.ToLower().IndexOf("up_message_save") > -1))
									{
										Console.WriteLine("skip - list PK");
									}
										/*
									else if (error.Description.ToLower().IndexOf("cannot insert duplicate key in object 'messagelist'") > -1)
									{
										if (!list.Contains(error.MessageID))
										{
											try
											{
												svc.GetMessage(error.QueuePath,
													error.MessageID);
												Console.WriteLine("message deleted");
												list.Add(error.MessageID);
											}
											catch (Exception ex)
											{
												Console.WriteLine(ex.ToString());
											}
										}
									}
									else if (error.Description.ToLower().IndexOf("cannot insert duplicate key in object 'message'") > -1)
									{
										if (!list.Contains(error.MessageID))
										{
											try
											{
												svc.GetMessage(error.QueuePath,
													error.MessageID);
												Console.WriteLine("message deleted");
												list.Add(error.MessageID);
											}
											catch (Exception ex)
											{
												Console.WriteLine(ex.ToString());
											}
										}
									}
									*/
									else
									{
										Console.WriteLine("press d <enter> to delete, <enter> to skip:");
										string s = Console.ReadLine();
										if (s == "d")
										{
											try
											{
												svc.GetMessage(error.QueuePath,
													error.MessageID);
												Console.WriteLine("message deleted");
												list.Add(error.MessageID);
												Thread.Sleep(1000);
											}
											catch (Exception ex)
											{
												Console.WriteLine(ex.ToString());
											}
										}
									}

								}
							}
						}
					}
				}

				if (!foundError)
				{
					Console.WriteLine(DateTime.Now.ToString() + "\tno errors found");
					Thread.Sleep(1000);
				}
			}
		}
	}
}
