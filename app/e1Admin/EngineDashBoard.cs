using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using Matchnet.e1.ServiceAdapters;
using Matchnet.e1.ValueObjects;
using Matchnet.e1Loader.ValueObjects;

namespace e1Admin
{
    public partial class EngineDashBoard : UserControl
    {
        private delegate void UpdateProgressCallback();

        private Engine _engine;
        private EngineStatus _engineStatus;
        private Thread _pollingThread;

        public EngineDashBoard(Engine engine)
        {
            _engine = engine;

            InitializeComponent();

            startPolling();
        }

        private void startPolling()
        {
            _pollingThread = new Thread(new ThreadStart(pollCycle));
            _pollingThread.Start();
        }


        void updateProgress()
        {
            if (this.pbBulkLoad.InvokeRequired)
            {
                UpdateProgressCallback callback = new UpdateProgressCallback(updateProgress);
                this.Invoke(callback);
            }
            else
            {
                if (_engineStatus == null)
                {
                    pbBulkLoad.Visible = false;
                }
                else
                {
                    pbBulkLoad.Visible = true;
                    pbBulkLoad.Maximum = _engineStatus.PartitionsTotal;
                    pbBulkLoad.Value = _engineStatus.PartitionsBulkLoaded;
                }
            }
        }

        private void pollCycle()
        {
            while (true)
            {
                try
                {
                    _engineStatus = EngineSA.Instance.GetEngineStatus(_engine.ServerName);
                }
                catch
                {
                    _engineStatus = null;
                }

                updateProgress();

                Thread.Sleep(2000);
            }
        }

        #region Event Handlers

        private void btnRunCommand_Click(object sender, EventArgs e)
        {
            wbCommandResult.Navigate("http://" + _engine.ServerName + ":6969/command/" + tbCommand.Text);
        }

        private void btnBulkLoadRestart_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not yet implemented.");
        }

        private void btnBulkLoadStop_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not yet implemented.");
        }

        #endregion
    }
}
