namespace e1Admin
{
    partial class EngineDashBoard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBulkLoadStatus = new System.Windows.Forms.Label();
            this.pbBulkLoad = new System.Windows.Forms.ProgressBar();
            this.btnBulkLoadRestart = new System.Windows.Forms.Button();
            this.btnBulkLoadStop = new System.Windows.Forms.Button();
            this.tbCommand = new System.Windows.Forms.TextBox();
            this.wbCommandResult = new System.Windows.Forms.WebBrowser();
            this.lblCommand = new System.Windows.Forms.Label();
            this.btnRunCommand = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblBulkLoadStatus
            // 
            this.lblBulkLoadStatus.AutoSize = true;
            this.lblBulkLoadStatus.Location = new System.Drawing.Point(31, 22);
            this.lblBulkLoadStatus.Name = "lblBulkLoadStatus";
            this.lblBulkLoadStatus.Size = new System.Drawing.Size(91, 13);
            this.lblBulkLoadStatus.TabIndex = 0;
            this.lblBulkLoadStatus.Text = "Bulk Load Status:";
            // 
            // pbBulkLoad
            // 
            this.pbBulkLoad.Location = new System.Drawing.Point(138, 22);
            this.pbBulkLoad.Name = "pbBulkLoad";
            this.pbBulkLoad.Size = new System.Drawing.Size(107, 13);
            this.pbBulkLoad.TabIndex = 1;
            // 
            // btnBulkLoadRestart
            // 
            this.btnBulkLoadRestart.Location = new System.Drawing.Point(251, 17);
            this.btnBulkLoadRestart.Name = "btnBulkLoadRestart";
            this.btnBulkLoadRestart.Size = new System.Drawing.Size(56, 22);
            this.btnBulkLoadRestart.TabIndex = 2;
            this.btnBulkLoadRestart.Text = "Restart";
            this.btnBulkLoadRestart.UseVisualStyleBackColor = true;
            this.btnBulkLoadRestart.Click += new System.EventHandler(this.btnBulkLoadRestart_Click);
            // 
            // btnBulkLoadStop
            // 
            this.btnBulkLoadStop.Location = new System.Drawing.Point(314, 17);
            this.btnBulkLoadStop.Name = "btnBulkLoadStop";
            this.btnBulkLoadStop.Size = new System.Drawing.Size(58, 22);
            this.btnBulkLoadStop.TabIndex = 3;
            this.btnBulkLoadStop.Text = "Stop";
            this.btnBulkLoadStop.UseVisualStyleBackColor = true;
            this.btnBulkLoadStop.Click += new System.EventHandler(this.btnBulkLoadStop_Click);
            // 
            // tbCommand
            // 
            this.tbCommand.Location = new System.Drawing.Point(115, 55);
            this.tbCommand.Name = "tbCommand";
            this.tbCommand.Size = new System.Drawing.Size(257, 20);
            this.tbCommand.TabIndex = 4;
            // 
            // wbCommandResult
            // 
            this.wbCommandResult.Location = new System.Drawing.Point(34, 97);
            this.wbCommandResult.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbCommandResult.Name = "wbCommandResult";
            this.wbCommandResult.Size = new System.Drawing.Size(434, 382);
            this.wbCommandResult.TabIndex = 5;
            // 
            // lblCommand
            // 
            this.lblCommand.AutoSize = true;
            this.lblCommand.Location = new System.Drawing.Point(32, 55);
            this.lblCommand.Name = "lblCommand";
            this.lblCommand.Size = new System.Drawing.Size(77, 13);
            this.lblCommand.TabIndex = 6;
            this.lblCommand.Text = "Run Command";
            // 
            // btnRunCommand
            // 
            this.btnRunCommand.Location = new System.Drawing.Point(388, 55);
            this.btnRunCommand.Name = "btnRunCommand";
            this.btnRunCommand.Size = new System.Drawing.Size(46, 19);
            this.btnRunCommand.TabIndex = 7;
            this.btnRunCommand.Text = "Go";
            this.btnRunCommand.UseVisualStyleBackColor = true;
            this.btnRunCommand.Click += new System.EventHandler(this.btnRunCommand_Click);
            // 
            // EngineDashBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnRunCommand);
            this.Controls.Add(this.lblCommand);
            this.Controls.Add(this.wbCommandResult);
            this.Controls.Add(this.tbCommand);
            this.Controls.Add(this.btnBulkLoadStop);
            this.Controls.Add(this.btnBulkLoadRestart);
            this.Controls.Add(this.pbBulkLoad);
            this.Controls.Add(this.lblBulkLoadStatus);
            this.Name = "EngineDashBoard";
            this.Size = new System.Drawing.Size(500, 500);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBulkLoadStatus;
        private System.Windows.Forms.ProgressBar pbBulkLoad;
        private System.Windows.Forms.Button btnBulkLoadRestart;
        private System.Windows.Forms.Button btnBulkLoadStop;
        private System.Windows.Forms.TextBox tbCommand;
        private System.Windows.Forms.WebBrowser wbCommandResult;
        private System.Windows.Forms.Label lblCommand;
        private System.Windows.Forms.Button btnRunCommand;

    }
}
