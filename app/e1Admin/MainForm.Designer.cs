namespace e1Admin
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbEngines = new System.Windows.Forms.TabControl();
            this.SuspendLayout();
            // 
            // tbEngines
            // 
            this.tbEngines.Location = new System.Drawing.Point(20, 21);
            this.tbEngines.Name = "tbEngines";
            this.tbEngines.SelectedIndex = 0;
            this.tbEngines.Size = new System.Drawing.Size(525, 525);
            this.tbEngines.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 570);
            this.Controls.Add(this.tbEngines);
            this.Name = "MainForm";
            this.Text = "e1Admin";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tbEngines;
    }
}

