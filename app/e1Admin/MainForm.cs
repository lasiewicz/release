using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Matchnet.e1Loader.ServiceAdapters;
using Matchnet.e1Loader.ValueObjects;

namespace e1Admin
{
    public partial class MainForm : Form
    {
        private EngineCollection _engines;

        public MainForm()
        {
            InitializeComponent();

            _engines = LoaderSA.Instance.GetEngineStatus();
            setupTabs();
        }

        private void setupTabs()
        {
            foreach (DictionaryEntry de in _engines)
            {
                Engine engine = de.Value as Engine;
                tbEngines.TabPages.Add(engine.ServerName, engine.ServerName);
                tbEngines.TabPages[engine.ServerName].Controls.Add(new EngineDashBoard(engine));
            }
        }
    }
}