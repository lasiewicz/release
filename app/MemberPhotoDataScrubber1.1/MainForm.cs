using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Xml;

using Matchnet;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.ApproveQueue.ServiceAdapters;

namespace MemberPhotoDataScrubber1._1
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		private const Char Delimiter = '|';
		private const String InputFilePrefix = "MemberPhotoPartition";
		private const String OutputFilePrefix = "MemberPhotoDataScrubberOutput";
		private const String ErrorLogFilePrefix = "MemberPhotoDataScrubberErrorLog";
		private const String ErrorRetryFilePrefix = "MemberPhotoDataScrubberErrorRetry";
		private const String ErrorMsgMemberDoesNotExist = "Member does not exist.";
		private const String FullSize = "Full Size";
		private const String Thumbnail = "Thumbnail";
		private const String MsgNoFiles = "No files to process.";
		private const String Fail = "FAIL";
		private const String Success = "SUCCESS";
        
		private const String RootTag = "ROOT";
		private const String PartitionTag = "PARTITION";
		private const String LineTag = "LINE";
		private const String DataTag = "DATA";
		private const String FileTypeTag = "FILE_TYPE";
		private const String ProblemTag = "PROBLEM";
		private const String ScrubActionTag = "SCRUB_ACTION";

		private const String NoPhotoImagesPath = @"\\File01\c$\Matchnet\bedrock\web\Spark.NoPhoto\NoPhotoImgs";

		private StreamWriter streamWriter;
		private StreamWriter streamWriterErrorLog;
		private StreamWriter streamWriterErrorRetry;
		private System.Windows.Forms.TextBox InputDirTextBox;
		private System.Windows.Forms.TextBox OutputDirTextBox;
		private System.Windows.Forms.Button button1;

		private enum FileType
		{
			FullSize,
			Thumbnail
		}

		private enum ScrubAction
		{ 
			DoNothing,
			Delete,
			Requeue
		}

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public MainForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			// Defaults.
			InputDirTextBox.Text = @"\\dv-wleetest\c$\Matchnet\Bedrock\App\MemberPhotoDataScrubber\21to24\MemberPhotoDataScrubberOutput20051207044448.txt";
			OutputDirTextBox.Text = @"\\dv-wleetest\c$\Matchnet\Bedrock\App\MemberPhotoDataScrubber\21to24";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.InputDirTextBox = new System.Windows.Forms.TextBox();
			this.OutputDirTextBox = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// InputDirTextBox
			// 
			this.InputDirTextBox.Location = new System.Drawing.Point(8, 8);
			this.InputDirTextBox.Name = "InputDirTextBox";
			this.InputDirTextBox.Size = new System.Drawing.Size(312, 20);
			this.InputDirTextBox.TabIndex = 0;
			this.InputDirTextBox.Text = "";
			// 
			// OutputDirTextBox
			// 
			this.OutputDirTextBox.Location = new System.Drawing.Point(8, 56);
			this.OutputDirTextBox.Name = "OutputDirTextBox";
			this.OutputDirTextBox.Size = new System.Drawing.Size(312, 20);
			this.OutputDirTextBox.TabIndex = 1;
			this.OutputDirTextBox.Text = "";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(328, 8);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 72);
			this.button1.TabIndex = 2;
			this.button1.Text = "SCRUB";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(416, 85);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.OutputDirTextBox);
			this.Controls.Add(this.InputDirTextBox);
			this.Name = "MainForm";
			this.Text = "Scrubber";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new MainForm());
		}

		private String Delete(Int32 memberID, Int32 communityID, Int32 memberPhotoID)
		{
			Member member;

			try
			{
				member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
			}
			catch (Exception ex)
			{
				return Fail + " [" + ErrorMsgMemberDoesNotExist + "](" + ex.Message + ")";
			}

			try
			{
				PhotoUpdate deletePhotoUpdate = new PhotoUpdate(null,
					true,
					memberPhotoID,
					Constants.NULL_INT,
					Constants.NULL_STRING,
					Constants.NULL_INT,
					Constants.NULL_STRING,
					0,
					false,
					false,
					Constants.NULL_INT,
					Constants.NULL_INT);

				MemberSA.Instance.SavePhotos(communityID,
					memberID,
					new PhotoUpdate[] { deletePhotoUpdate });

				return Success;
			}
			catch (Exception ex)
			{
				return Fail + " (" + ex.Message + ")";
			}
		}

		private String Requeue(Int32 memberID, Int32 communityID, Int32 memberPhotoID)
		{
			try
			{
				Member member;

				try
				{
					member = MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);
				}
				catch (Exception ex)
				{
					return Fail + " [" + ErrorMsgMemberDoesNotExist + "](" + ex.Message + ")";
				}

				if (member == null)
				{
					return Fail + " [" + ErrorMsgMemberDoesNotExist + "]";
				}

				Photo photo = member.GetPhotos(communityID).Find(memberPhotoID);

				Int32 fileID = photo.FileID;
				String fileWebPath = photo.FileWebPath;
				Int32 thumbFileID = photo.ThumbFileID;
				String thumbFileWebPath = photo.ThumbFileWebPath;
				Byte listOrder = photo.ListOrder;
				Int32 albumID = photo.AlbumID;
				Int32 adminMemberID = photo.AdminMemberID;
				Boolean isApproved = false;
				Boolean isPrivate = photo.IsPrivate;

				PhotoUpdate photoUpdate = new PhotoUpdate(null,
					false,
					memberPhotoID,
					fileID,
					fileWebPath,
					thumbFileID,
					thumbFileWebPath,
					listOrder,
					isApproved,
					isPrivate,
					albumID,
					adminMemberID);

				MemberSA.Instance.SavePhotos(communityID,
					memberID,
					new PhotoUpdate[] { photoUpdate });

				ApproveQueueSA.Instance.QueuePhoto(communityID,
					memberID,
					memberPhotoID);

				return Success;
			}
			catch (Exception ex)
			{
				return Fail + " (" + ex.Message + ")";
			}
		}

		private void Scrub()
		{
			if (MessageBox.Show(@"YOU ARE ABOUT TO CHANGE / DELETE DATA ON THE PRODUCTION DATABASES.  ARE YOU SURE YOU WANT TO DO THIS?!", "Please Confirm", MessageBoxButtons.YesNoCancel) != DialogResult.Yes)
			{
				return;
			}

			// Load the data into an XmlDoc.
			XmlDocument xmlDocument = new XmlDocument();

			try
			{
				xmlDocument.Load(InputDirTextBox.Text.Trim());
			}
			catch (Exception ex)
			{
				MessageBox.Show(@"You must enter a valid file into the 'input file' textbox.  (" + ex.Message + ")");

				return;
			}

			Int64 totalProcessed = 0;
			Int64 totalRequeued = 0;
			Int64 totalDeleted = 0;

			String dateString = DateTime.Now.ToString("yyyyMMddhhmmss");
			streamWriter = new StreamWriter(OutputDirTextBox.Text + "\\" + OutputFilePrefix + dateString + "_SCRUB.txt");
			streamWriter.AutoFlush = true;

			InitializeErrorHandling(dateString, true);

			streamWriter.WriteLine("<" + RootTag + ">");

			// Find all the LINE nodes.
			XmlNodeList xmlNodeList = xmlDocument.GetElementsByTagName(LineTag);

			foreach (XmlNode xmlNode in xmlNodeList)
			{
				Int32 memberID = Constants.NULL_INT;
				Int32 memberPhotoID = Constants.NULL_INT;
				Int32 communityID = Constants.NULL_INT;
				ScrubAction scrubAction = ScrubAction.DoNothing;

				ParseLineNode(xmlNode, ref memberID, ref memberPhotoID, ref communityID, ref scrubAction);

				String returnVal = "NO ACTION";
				switch (scrubAction)
				{
					case ScrubAction.Delete:
						returnVal = Delete(memberID, communityID, memberPhotoID);

						totalDeleted++;
						break;

					case ScrubAction.Requeue:
						returnVal = Requeue(memberID, communityID, memberPhotoID);

						totalRequeued++;
						break;

					default:
						// Do nothing.
						break;
				}

				streamWriter.WriteLine("<" + LineTag + " MemberPhotoID=\"" + memberPhotoID+ "\" Scrub_Action=\"" + scrubAction.ToString() + "\" ReturnVal=\"" + returnVal + "\">");
				streamWriter.WriteLine("</" + LineTag + ">");

				totalProcessed++;
			}

			streamWriter.WriteLine("</" + RootTag + ">");

			streamWriter.Close();

			CleanUpErrorHandling();

			MessageBox.Show("[TotalDeleted: " + totalDeleted.ToString() 
				+ "][TotalRequeued: " + totalRequeued.ToString() 
				+ "][TotalProcessed: " + totalProcessed.ToString() + "]");
		}

		private void InitializeErrorHandling(String dateString)
		{
			InitializeErrorHandling(dateString, false);
		}

		private void InitializeErrorHandling(String dateString, Boolean isScrub)
		{
			String extension = ".txt";
			if (isScrub)
			{
				extension = "_SCRUB.txt";
			}

			streamWriterErrorLog = new StreamWriter(OutputDirTextBox.Text + "\\" + ErrorLogFilePrefix + dateString + extension);
			streamWriterErrorLog.AutoFlush = true;

			streamWriterErrorRetry = new StreamWriter(OutputDirTextBox.Text + "\\" + ErrorRetryFilePrefix + dateString + extension);
			streamWriterErrorRetry.AutoFlush = true;
		}

		private void CleanUpErrorHandling()
		{
			streamWriterErrorLog.Close();

			streamWriterErrorRetry.Close();
		}

		private void ProcessError(String errorMsg, ref Int64 totalErrors, String line)
		{
			streamWriterErrorLog.WriteLine(errorMsg);

			if (line != null && line != String.Empty)
			{
				streamWriterErrorRetry.WriteLine(line);
			}
		}

		private void ParseLineNode(XmlNode xmlNode, ref Int32 memberID, ref Int32 memberPhotoID, ref Int32 communityID, ref ScrubAction scrubAction)
		{
			foreach (XmlNode xmlChildNode in xmlNode.ChildNodes)
			{
				switch (xmlChildNode.Name)
				{ 
					case DataTag:
						ParseLine(xmlChildNode.InnerText, ref memberPhotoID, ref memberID, ref communityID);
						break;

					case ScrubActionTag:
						scrubAction = ((ScrubAction)Enum.Parse(typeof(ScrubAction), (xmlChildNode.InnerText)));
						break;

					default:
						// Ignore the rest.
						break;
				}
            
			}
		}

		private void ParseLine(String line, ref Int32 memberPhotoID, ref Int32 memberID, ref String fileWebPath,
			ref String thumbFileWebPath, ref Boolean isApproved, ref Int32 communityID)
		{
			String[] items = line.Split(new char[] { Delimiter });

			memberPhotoID = Convert.ToInt32(items[0].ToString().Trim());
			memberID = Convert.ToInt32(items[1].ToString().Trim());
			fileWebPath = items[4].ToString().Trim();
			thumbFileWebPath = items[6].ToString().Trim();
			isApproved = Convert.ToBoolean(Convert.ToByte(items[10].ToString().Trim()));
			communityID = Convert.ToInt32(items[2].ToString().Trim());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			Scrub();
		}

		private void ParseLine(String line, ref Int32 memberPhotoID, ref Int32 memberID, ref Int32 communityID)
		{
			String[] items = line.Split(new char[] { Delimiter });

			memberPhotoID = Convert.ToInt32(items[0].ToString().Trim());
			memberID = Convert.ToInt32(items[1].ToString().Trim());
			communityID = Convert.ToInt32(items[2].ToString().Trim());
		}
	}
}
