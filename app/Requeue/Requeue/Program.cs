using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects;

namespace Requeue
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = null;

            if (args.Length > 0)
            {
                fileName = args[0];
            }

            if (fileName == null || fileName == string.Empty)
            {
                fileName = @"C:\QueueInput.txt";
                Console.WriteLine("No filename specified.  Using " + fileName + ".");
            }

            List<QueueItemText> queueItems = new List<QueueItemText>();
            StreamReader sr = new StreamReader(fileName);

            while (!sr.EndOfStream)
            {
                string[] parts = sr.ReadLine().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                Int32 memberID = Matchnet.Conversion.CInt(parts[0]);
                Int32 communityID = Matchnet.Conversion.CInt(parts[1]);
                Int32 languageID = Matchnet.Conversion.CInt(parts[2]);

                queueItems.Add(new QueueItemText(communityID, memberID, languageID));
            }

            ApproveQueueSA.Instance.QueueText(queueItems.ToArray());
        }
    }
}
