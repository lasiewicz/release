using System;

namespace ResxManipulator
{
	/// <summary>
	/// A site object, relevant subset of site properties from mnSystem.dbo.Site table.
	/// </summary>
	public class Site {
		public int	SiteID;
		public int CommunityID;
		public string SiteName;
		public string CultureName;

		public Site(int	SiteID, int CommunityID, string SiteName, string CultureName) {
			this.SiteID = SiteID;
			this.CommunityID = CommunityID;
			this.SiteName = SiteName;
			this.CultureName = CultureName;
		}
	}
}
