using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Resources;

namespace ResxManipulator 
{
	/// <summary>
	/// Summary description for ResxFile.
	/// </summary>
	public class ResxFile : IEnumerable 
	{
		private Site		_site;
		private string		_fullFileName;
		private SortedList	_Resources = new SortedList();

		private ResxFile.FileStatus _Status = FileStatus.Unknown;
		
		public ResxFile(string baseFileName, Site site) 
		{
			_site = site;
			_fullFileName = GetFullResourceFilePath(baseFileName, site);

			Debug.WriteLine(string.Format("{0} - {1} uses {2}", site.SiteID , site.SiteName ,_fullFileName));
			if (File.Exists(_fullFileName))
			{
				if (!ChecFileOut())
				{
					throw new Exception("Unable to check out " + _fullFileName);
				}
			}
			else 
			{
				throw new Exception(_fullFileName + " does not exist");
			}

			LoadResources();
		}


		/// <summary>
		/// Sets or gets the value associated with the given key in this resx file
		/// </summary>
		public string this[string key]
		{
			get { return (string)_Resources[key.ToUpper()]; }
			set
			{
				if (_Resources.ContainsKey(key))
				{
					_Resources[key.ToUpper()] = value;
				}
				else
				{
					_Resources.Add(key,value);
				}
			}
		}
		public Site Site
		{
			get { return _site; }
		}


		public void RemoveResource(string key)
		{
			_Resources.Remove(key);
		}
		
		public IEnumerator GetEnumerator()
		{
			return _Resources.GetEnumerator();
		}



		private void LoadResources()
		{
			using (ResXResourceReader reader = new ResXResourceReader(_fullFileName))
			{
				foreach (DictionaryEntry entry in reader)
				{
					_Resources.Add(entry.Key.ToString(), entry.Value);					   
				}
				reader.Close();
			}
		}


		public void Save()
		{
			ResXResourceWriter writer = new ResXResourceWriter(_fullFileName);
			foreach (string key in _Resources.Keys)
			{
				writer.AddResource(key, _Resources[key]);
				Trace.WriteLine(key + " => " + _Resources[key]);
			}
			writer.Close();
			
		}


		private bool ChecFileOut()
		{
			_Status = FileStatus.CheckedOut;
			return true;
			//			try {
			//				if (_DTE.SourceControl.IsItemUnderSCC(_FileName)){
			//					if (!_DTE.SourceControl.IsItemCheckedOut(_FileName)){
			//						_DTE.SourceControl.CheckOutItem(_FileName);
			//					}
			//					_Status = FileStatus.CheckedOut;
			//					return true;
			//				}
			//				else {
			//					return false;
			//				}
			//			}
			//			catch {
			//				_Status = FileStatus.NotAvailable;
			//				return false;
			//			}
		}


		public bool CheckFileIn()
		{
			_Status = FileStatus.CheckedIn;
			return true;
		}
		
		public string FullFileName
		{
			get 
			{
				return _fullFileName;
			}
		}

		public static string GetFullResourceFilePath(string baseFileName, Site site)
		{
			return string.Format(@"{0}.{1}.{2}.Resx", baseFileName, site.SiteID, site.CultureName);
		}

		public FileStatus Status 
		{
			get 
			{
				return _Status;
			}
		}

		/// <summary>
		/// Status enumeration
		/// </summary>
		public enum FileStatus 
		{
			/// <summary>
			/// File status is unknown
			/// </summary>
			Unknown,
			/// <summary>
			/// Fiel is checked in
			/// </summary>
			CheckedIn,
			/// <summary>
			/// File is checked out
			/// </summary>
			CheckedOut,
			/// <summary>
			/// File is checked out by another user or otherwise not available
			/// </summary>
			NotAvailable
		}
	}
}
