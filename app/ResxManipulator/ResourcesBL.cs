using System;
using System.Diagnostics;
using System.IO;

namespace ResxManipulator {
	public class ResourcesBL {

		private Sites _Sites = new Sites();
		private ResxFiles _ResxFiles = new ResxFiles();

		public ResourcesBL(string baseFileName)
		{
			foreach (Site site in _Sites.SiteList)
			{
				try
				{
					ResxFile resx = new ResxFile(baseFileName, site);
					_ResxFiles.Add(resx, site);
				}
				catch (Exception ex)
				{
					Debug.WriteLine("Can't load resx for " + site.SiteName );
					Debug.WriteLine(ex.ToString());
				}
			}
		}


		public void SetValue(string Key, string Value, int [] SiteIDList){
			foreach( int siteid in SiteIDList){
				_ResxFiles[siteid][Key] = Value;
			}
		}

		public void RemoveResource(string key, int [] SiteIDList){
			foreach (int siteid in SiteIDList){
				_ResxFiles[siteid].RemoveResource(key);
			}
		}

		public Sites Sites{
			get{
				return _Sites;
			}
		}

		public ResxFiles ResxFiles{
			get {
				return _ResxFiles;
			}
		}

		public static bool AreReadOnlyFilesPresent(string baseResourceFileName)
		{
			Sites sites = new Sites();
			foreach (Site site in sites.SiteList)
			{
				string resourceFileName = ResxFile.GetFullResourceFilePath(baseResourceFileName, site);
				FileInfo fileInfo = new FileInfo(resourceFileName);

				if ((fileInfo.Attributes & FileAttributes.ReadOnly) > 0)
				{
					return true;
				}
			}

			return false;
		}
	}
}
