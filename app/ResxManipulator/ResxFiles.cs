using System;
using System.Collections;
using System.Collections.Specialized;

namespace ResxManipulator {
	/// <summary>
	/// Summary description for ResxFiles.
	/// </summary>
	public class ResxFiles : IEnumerable
	{
		private Hashtable _ResxBySiteID = new Hashtable();
		private Hashtable _ResxBySiteName = new Hashtable();
		private StringCollection _MasterKeyList = new StringCollection();

		public ResxFiles() {
		}

		public ResxFile this[int SiteID]{
			get {
				return (_ResxBySiteID[SiteID] as ResxFile);
			}
		}


		public ResxFile this[string SiteName]{
			get {
				return (_ResxBySiteName[SiteName] as ResxFile);
			}
		}

		public void Add(ResxFile resx, Site site){
			_ResxBySiteID[site.SiteID] = resx;
			_ResxBySiteName[site.SiteName] = resx;
			UpdateMasterKeyList(resx);
		}

		public IEnumerator GetEnumerator(){
			return _ResxBySiteID.Values.GetEnumerator();
		}

		public void Save(int [] SiteIDList){
			foreach (int i in SiteIDList) {
				(_ResxBySiteID[i] as ResxFile).Save();
			}
		}

		private void UpdateMasterKeyList(ResxFile resx){
			foreach (DictionaryEntry resource in resx){
				if (!_MasterKeyList.Contains(resource.Key.ToString())){
					_MasterKeyList.Add(resource.Key.ToString());
				}
			}			
		}


		public StringCollection MasterKeyList{
			get{
				return _MasterKeyList;
			}
		}

		public int Count{
			get{
				return _ResxBySiteID.Count;
			}
		}

		
	}
}
