using System;
using System.Drawing;
using System.Windows.Forms;

namespace ResxManipulator
{
	/// <summary>
	/// Summary description for ResxValueBox.
	/// </summary>
	public class ResxValueBox : System.Windows.Forms.UserControl
	{
		private string _Key;
		private ResxFile _Resx;

		private System.Windows.Forms.TextBox txtValue;
		private System.Windows.Forms.CheckBox chkIncludeInUpdate;
		private System.Windows.Forms.Label lblSiteID;
		private System.Windows.Forms.Label lblCulture;
		private System.Windows.Forms.Label lblSiteName;
		private System.Windows.Forms.Label lblKey;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		///		Is this site selected.
		/// </summary>
		public bool IsSelected
		{
			get { return chkIncludeInUpdate.Checked; }
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="Key"></param>
		/// <param name="Resx"></param>
		/// <param name="OrdinalPosition">The ordinal position of this control in a container panel. This helps offset the Y position if non zero, so that controls list one afer the other instead of one on top of the other.</param>
		public ResxValueBox(string Key, ResxFile Resx,int OrdinalPosition)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			_Key = Key;
			_Resx = Resx;
			this.Location = new Point(this.Location.X, OrdinalPosition * this.Height);
			PopulateControl();
		}

		private void PopulateControl()
		{
			txtValue.Text = _Resx[_Key];
			lblCulture.Text = _Resx.Site.CultureName;
			lblSiteID.Text = _Resx.Site.SiteID.ToString();
			lblSiteName.Text = _Resx.Site.SiteName;
			lblKey.Text  = _Key;
		}

		/// <summary>
		/// Saves the underlying resx if the checkbox is selected.
		/// </summary>
		public void Save()
		{
			if (chkIncludeInUpdate.Checked) 
			{
				_Resx.Save();
			}
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtValue = new System.Windows.Forms.TextBox();
			this.chkIncludeInUpdate = new System.Windows.Forms.CheckBox();
			this.lblSiteID = new System.Windows.Forms.Label();
			this.lblCulture = new System.Windows.Forms.Label();
			this.lblSiteName = new System.Windows.Forms.Label();
			this.lblKey = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// txtValue
			// 
			this.txtValue.AllowDrop = true;
			this.txtValue.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtValue.Location = new System.Drawing.Point(0, 24);
			this.txtValue.Multiline = true;
			this.txtValue.Name = "txtValue";
			this.txtValue.Size = new System.Drawing.Size(816, 72);
			this.txtValue.TabIndex = 0;
			this.txtValue.Text = "";
			this.txtValue.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtValue_DragDrop);
			this.txtValue.TextChanged += new System.EventHandler(this.txtValue_TextChanged);
			// 
			// chkIncludeInUpdate
			// 
			this.chkIncludeInUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.chkIncludeInUpdate.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.chkIncludeInUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.chkIncludeInUpdate.Location = new System.Drawing.Point(728, 8);
			this.chkIncludeInUpdate.Name = "chkIncludeInUpdate";
			this.chkIncludeInUpdate.Size = new System.Drawing.Size(80, 16);
			this.chkIncludeInUpdate.TabIndex = 1;
			this.chkIncludeInUpdate.Text = "Selected";
			// 
			// lblSiteID
			// 
			this.lblSiteID.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblSiteID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblSiteID.Location = new System.Drawing.Point(504, 8);
			this.lblSiteID.Name = "lblSiteID";
			this.lblSiteID.Size = new System.Drawing.Size(72, 16);
			this.lblSiteID.TabIndex = 2;
			this.lblSiteID.Text = "Site ID";
			// 
			// lblCulture
			// 
			this.lblCulture.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblCulture.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblCulture.Location = new System.Drawing.Point(576, 8);
			this.lblCulture.Name = "lblCulture";
			this.lblCulture.Size = new System.Drawing.Size(72, 16);
			this.lblCulture.TabIndex = 3;
			this.lblCulture.Text = "Culture";
			// 
			// lblSiteName
			// 
			this.lblSiteName.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblSiteName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblSiteName.Location = new System.Drawing.Point(0, 8);
			this.lblSiteName.Name = "lblSiteName";
			this.lblSiteName.Size = new System.Drawing.Size(112, 16);
			this.lblSiteName.TabIndex = 4;
			this.lblSiteName.Text = "Site Name";
			// 
			// lblKey
			// 
			this.lblKey.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lblKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblKey.Location = new System.Drawing.Point(160, 8);
			this.lblKey.Name = "lblKey";
			this.lblKey.Size = new System.Drawing.Size(344, 16);
			this.lblKey.TabIndex = 5;
			this.lblKey.Text = "Key";
			// 
			// ResxValueBox
			// 
			this.Controls.Add(this.lblKey);
			this.Controls.Add(this.lblSiteName);
			this.Controls.Add(this.lblCulture);
			this.Controls.Add(this.lblSiteID);
			this.Controls.Add(this.chkIncludeInUpdate);
			this.Controls.Add(this.txtValue);
			this.Name = "ResxValueBox";
			this.Size = new System.Drawing.Size(816, 96);
			this.Load += new System.EventHandler(this.ResxValueBox_Load);
			this.ResumeLayout(false);

		}
		#endregion


		public string Key
		{
			get 
			{
				return _Key;
			}
			set 
			{
				_Key = value;
				PopulateControl();
			}
		}

		public string Value
		{
			get 
			{
				return txtValue.Text;
			}
			set 
			{
				txtValue.Text = value;
				_Resx[_Key] = value;
			}
		}

		public string Culture
		{
			get 
			{
				return lblCulture.Text;
			}
		}

		public int SiteID
		{
			get 
			{
				return Int32.Parse(lblSiteID.Text);
			}
		}

		public bool IncludeInUpdate
		{
			get
			{
				return chkIncludeInUpdate.Checked;
			}
			set 
			{
				chkIncludeInUpdate.Checked = value;
			}
		}

		private void ResxValueBox_Load(object sender, EventArgs e) 
		{

		}

		private void txtValue_TextChanged(object sender, EventArgs e)
		{
			_Resx[_Key] = txtValue.Text;
		}


		private void txtValue_DragDrop(object sender, DragEventArgs e)
		{
			this.IncludeInUpdate = true;
		}
	}
}
