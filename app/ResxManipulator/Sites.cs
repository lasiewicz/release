using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace ResxManipulator 
{
	/// <summary>
	/// List of sites.
	/// Hard coded list of sites this addin will address .
	/// Not accessing DB because this is an internal tool and some non developers might not have DB access rights.
	/// </summary>
	public class Sites 
	{

		private static Site [] _Sites;
		private Hashtable _SiteByName = new Hashtable();
		private Hashtable _SiteByID = new Hashtable();
	
		/// <summary>
		/// Constructor.
		/// Builds list of sites
		/// </summary>
		public Sites()
		{
			if (_Sites == null)
			{
				_Sites = new Site [] {
										 new Site(101,	1,	"AmericanSingles.com",	"en-US"),	
										 new Site(13,	1,	"Date.ca",				"en-CA"),		
										 new Site(6,	1,	"MatchNet.co.uk",		"en-GB"),		
										 new Site(7,	1,	"MatchNet.com.au",		"en-AU"),		
										 new Site(110,	1,	"MatchnetAsia.com",		"en-US"),	
										 new Site(113,	1,	"SilverSingles.com",	"en-US"),	
										 new Site(1934,	1,	"DatingResults.com",	"en-US"),
										 new Site(102,	2,	"Glimpse.com",			"en-US"),	
										 new Site(112,	12,	"CollegeLuv.com",		"en-US"),	
										 new Site(100,	17,	"Spark.com",			"en-US"),	
										 new Site(103,	3,	"JDate.com",			"en-US"),	
										 new Site(15,	10,	"Cupid.co.il",			"he-IL"),		
										 new Site(4,	3,	"JDate.co.il",			"he-IL"),		
										 new Site(5,	1,	"MatchNet.de",			"de-DE"),
										 new Site(109,	9,	"FaceLink.com",			"en-US")	
										 //new Site(1840,	3,	"JDate.msn.co.il",		"he-IL"),	
										 //new Site(108,	8,	"MatchNet.com",			"en-US"),	
						

									 };
				for (int i = 0; i < _Sites.Length; i++)
				{
					_SiteByID.Add(_Sites[i].SiteID, _Sites[i]);
					_SiteByName.Add(_Sites[i].SiteName, _Sites[i]);
				}
			}
			
		}

		/// <summary>
		/// Returns full list of sites
		/// </summary>
		public Site[] SiteList
		{
			get 
			{
				return _Sites;
			}
		}

		public Site this[int Index] 
		{
			get 
			{
				return _Sites[Index];
			}
		}

		public Site GetSite(int SiteID)
		{
			return _SiteByID[SiteID] as Site ;
		}

		public Site GetSite(string SiteName)
		{
			return _SiteByName[SiteName] as Site;
		}
	}
}
