using System;
using System.Windows.Forms;
using System.Text.RegularExpressions;


namespace ResxManipulator
{
	/// <summary>
	/// Summary description for ResxManager.
	/// </summary>
	public class ResxManager : System.Windows.Forms.Form
	{
		private string _resourceFileBaseName = "";
		private ResourcesBL _resourceBL = null;

		private System.Windows.Forms.ComboBox cmbMasterKeyList;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnChooseResx;
		private System.Windows.Forms.OpenFileDialog ofdOpenFileDialog;
		private System.Windows.Forms.Button btnSaveSelected;
		private System.Windows.Forms.StatusBar statusBar1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.CheckBox chkSelectAll;
		private System.Windows.Forms.Button btnAddKey;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.EnableVisualStyles();
			Application.Run(new ResxManager());
		}

		public ResxManager()
		{
			InitializeComponent();
		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.cmbMasterKeyList = new System.Windows.Forms.ComboBox();
			this.btnChooseResx = new System.Windows.Forms.Button();
			this.ofdOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.btnSaveSelected = new System.Windows.Forms.Button();
			this.statusBar1 = new System.Windows.Forms.StatusBar();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.chkSelectAll = new System.Windows.Forms.CheckBox();
			this.btnAddKey = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.AllowDrop = true;
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.panel1.AutoScroll = true;
			this.panel1.BackColor = System.Drawing.SystemColors.Control;
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel1.Location = new System.Drawing.Point(8, 40);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(816, 384);
			this.panel1.TabIndex = 2;
			// 
			// cmbMasterKeyList
			// 
			this.cmbMasterKeyList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.cmbMasterKeyList.DisplayMember = "data";
			this.cmbMasterKeyList.Location = new System.Drawing.Point(8, 16);
			this.cmbMasterKeyList.MaxDropDownItems = 16;
			this.cmbMasterKeyList.Name = "cmbMasterKeyList";
			this.cmbMasterKeyList.Size = new System.Drawing.Size(712, 21);
			this.cmbMasterKeyList.Sorted = true;
			this.cmbMasterKeyList.TabIndex = 1;
			this.cmbMasterKeyList.ValueMember = "data";
			this.cmbMasterKeyList.SelectedIndexChanged += new System.EventHandler(this.cmbMasterKeyList_SelectedIndexChanged);
			// 
			// btnChooseResx
			// 
			this.btnChooseResx.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnChooseResx.Location = new System.Drawing.Point(8, 8);
			this.btnChooseResx.Name = "btnChooseResx";
			this.btnChooseResx.Size = new System.Drawing.Size(88, 24);
			this.btnChooseResx.TabIndex = 2;
			this.btnChooseResx.Text = "Choose Resx";
			this.btnChooseResx.Click += new System.EventHandler(this.btnChooseResx_Click);
			// 
			// ofdOpenFileDialog
			// 
			this.ofdOpenFileDialog.DefaultExt = "resx";
			this.ofdOpenFileDialog.Filter = "Resx Files|*.resx";
			this.ofdOpenFileDialog.InitialDirectory = "C:\\Matchnet\\Bedrock\\Web\\bedrock.matchnet.com";
			this.ofdOpenFileDialog.Title = "Chose a .Resx file to work on.";
			// 
			// btnSaveSelected
			// 
			this.btnSaveSelected.Enabled = false;
			this.btnSaveSelected.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnSaveSelected.Location = new System.Drawing.Point(104, 8);
			this.btnSaveSelected.Name = "btnSaveSelected";
			this.btnSaveSelected.Size = new System.Drawing.Size(104, 24);
			this.btnSaveSelected.TabIndex = 3;
			this.btnSaveSelected.Text = "Save Selected";
			this.btnSaveSelected.Click += new System.EventHandler(this.btnSaveSelected_Click);
			// 
			// statusBar1
			// 
			this.statusBar1.Location = new System.Drawing.Point(0, 487);
			this.statusBar1.Name = "statusBar1";
			this.statusBar1.Size = new System.Drawing.Size(848, 22);
			this.statusBar1.TabIndex = 4;
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.panel1);
			this.groupBox1.Controls.Add(this.cmbMasterKeyList);
			this.groupBox1.Controls.Add(this.chkSelectAll);
			this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.groupBox1.Location = new System.Drawing.Point(4, 48);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(836, 432);
			this.groupBox1.TabIndex = 5;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Resources For Files";
			// 
			// chkSelectAll
			// 
			this.chkSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.chkSelectAll.Enabled = false;
			this.chkSelectAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.chkSelectAll.Location = new System.Drawing.Point(736, 16);
			this.chkSelectAll.Name = "chkSelectAll";
			this.chkSelectAll.Size = new System.Drawing.Size(72, 16);
			this.chkSelectAll.TabIndex = 6;
			this.chkSelectAll.Text = "Select All";
			this.chkSelectAll.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
			// 
			// btnAddKey
			// 
			this.btnAddKey.Enabled = false;
			this.btnAddKey.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnAddKey.Location = new System.Drawing.Point(216, 8);
			this.btnAddKey.Name = "btnAddKey";
			this.btnAddKey.Size = new System.Drawing.Size(80, 24);
			this.btnAddKey.TabIndex = 7;
			this.btnAddKey.Text = "Add Key";
			this.btnAddKey.Click += new System.EventHandler(this.btnAddKey_Click);
			// 
			// ResxManager
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(848, 509);
			this.Controls.Add(this.btnAddKey);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.statusBar1);
			this.Controls.Add(this.btnSaveSelected);
			this.Controls.Add(this.btnChooseResx);
			this.Name = "ResxManager";
			this.Text = "Resx Manager";
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void PopulateEditBoxes(string Key)
		{
			// clear out Resource Edit controls
			if (panel1.Controls.Count > 0)
			{
				panel1.Controls.Clear();
			}

			int i = 0;
			foreach (ResxFile resx in _resourceBL.ResxFiles)
			{
				ResxValueBox rvl = new ResxValueBox(Key, resx, i++);
				rvl.Width = panel1.ClientSize.Width;
				rvl.IncludeInUpdate = chkSelectAll.Checked;
				rvl.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Top;
				panel1.Controls.Add(rvl);
			}		
		}

		private void cmbMasterKeyList_SelectedIndexChanged(object sender, EventArgs e) 
		{
			// It is critical that this says "as string" rather than ".ToString()".
			string resourceKey = ((ComboBox) sender).SelectedItem as string;
			if (_resourceBL != null && resourceKey != null && resourceKey.Length > 0)
			{
				PopulateEditBoxes(resourceKey);
			}
		}

		private void btnChooseResx_Click(object sender, System.EventArgs e) 
		{
			ofdOpenFileDialog.ShowDialog();
			string fileName = ofdOpenFileDialog.FileName;

			_resourceFileBaseName = Regex.Replace(fileName, @"^(.*)\.\d{1,3}\.\w\w-\w\w\.resx$", "$1");
			LoadData();
		}

		private void LoadData()
		{
			// set up wair cursor and status
			Cursor lastCursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;
			statusBar1.Text = "Loading " + _resourceFileBaseName + " variants";

			// disable controls
			this.btnSaveSelected.Enabled = this.btnAddKey.Enabled = this.chkSelectAll.Enabled = false;

			// perform work
			if (_resourceFileBaseName.Length > 0)
			{
				// check for read-only files
				if (ResourcesBL.AreReadOnlyFilesPresent(_resourceFileBaseName))
				{
					string message = "One or more resource files are read-only (possibly under source control and not checked out)." + Environment.NewLine + "Are you sure you want to continue?";
					if (DialogResult.No == MessageBox.Show(this, message, "Read-only File(s) Encountered", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
					{
						// update status, clear wait cursor
						statusBar1.Text = "Ready";
						Cursor.Current = lastCursor;
						return;
					}
				}

				// reload resource data
				_resourceBL = new ResourcesBL(_resourceFileBaseName);

				// rebind list
				RebindKeyListComboBox();
			}

			// enabled controls
			this.btnSaveSelected.Enabled = this.btnAddKey.Enabled = this.chkSelectAll.Enabled = true;
			
			// update status, clear wait cursor
			statusBar1.Text = "Ready";
			Cursor.Current = lastCursor;
		}

		private void RebindKeyListComboBox()
		{
			// clear out data source
			cmbMasterKeyList.DataSource = null;
	
			// reconfigure binding
			cmbMasterKeyList.DisplayMember = "data";
			cmbMasterKeyList.DataSource = _resourceBL.ResxFiles.MasterKeyList;
	
			// bind
			cmbMasterKeyList.Refresh();
		}

		private void btnSaveSelected_Click(object sender, System.EventArgs e) 
		{
			SaveSelectedResourceFiles();
		}

		private void SaveSelectedResourceFiles()
		{
			// set up wair cursor and status
			Cursor lastCursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;
			statusBar1.Text = "Saving resource files for checked sites...";
	
			bool somethingWasSaved = false;
			foreach (ResxValueBox rvb in panel1.Controls)
			{
				if (rvb.IsSelected)
				{
					rvb.Save();
					Application.DoEvents();
					somethingWasSaved = true;
					Application.DoEvents();
				}
			}		
	
			if (somethingWasSaved)
			{
				LoadData();
			}

			statusBar1.Text = "Ready";
			Cursor.Current = lastCursor;
		}

		private void chkSelectAll_CheckedChanged(object sender, System.EventArgs e)
		{
			foreach( ResxValueBox rvb in panel1.Controls)
			{
				rvb.IncludeInUpdate = chkSelectAll.Checked;
			}		
		}

		private void btnAddKey_Click(object sender, System.EventArgs e)
		{
			string newKey = cmbMasterKeyList.Text.Trim().ToUpper();

			if (null == newKey || newKey.Length == 0)
			{
				return;
			}

			if (DialogResult.Yes == MessageBox.Show(this, "You are about to add key \"" + newKey + "\". Continue?", "Add Key", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
			{
				// add key to master list
				_resourceBL.ResxFiles.MasterKeyList.Add(newKey);

				// rebind control and select new key
				RebindKeyListComboBox();
				cmbMasterKeyList.SelectedItem = newKey;

				// propagate key to edit controls
				foreach( ResxValueBox rvb in panel1.Controls)
				{
					rvb.Key = newKey;
					rvb.Value = "New Value " + DateTime.Now.ToLongDateString();					
				}

				// save checked
				SaveSelectedResourceFiles();
			}
		}
	}
}


#region

		//		private void btnTest1_Click(object sender, System.EventArgs e) {

		//			string FileName = @"C:\ScratchArea\ResxManipulator\Default.aspx";
		//			_ResourceBL = new ResourcesBL(FileName);
		//
		//			_ResourceBL.SetValue("FOO" + DateTime.Now.Ticks.ToString(),DateTime.UtcNow.ToString(),new int [] {4});
		//
		//			_ResourceBL.ResxFiles.Save();
		//
		//			cmbMasterKeyList.DataSource = _ResourceBL.ResxFiles.MasterKeyList;
		//			cmbMasterKeyList.Refresh();
		//			
		//		}




#endregion
