using System;
using System.Messaging;

namespace MoveQueues
{
	public delegate void QueueMovedEventHandler();

	public class Executor
	{
		public event QueueMovedEventHandler QueueMoved;
		private static bool _isRunnable;

		private string _fromQueuePath = String.Empty;
		private string _toQueuePath = String.Empty;
		private int _howMany = 0;

		private bool _isTransational = false;

		public string FromQueuePath 
		{
			get
			{
				return _fromQueuePath;
			}
		}
		public string ToQueuePath 
		{
			get
			{
				return _toQueuePath;
			}
		}

		public int HowMany 
		{
			get
			{
				return _howMany;
			}
		}
		public bool IsTransational 
		{
			get
			{
				return _isTransational;
			}
		}
		

		public Executor()
		{
		}

		public void Run(string fromQueuePath, string toQueuePath, int howMany, bool isTransactional)
		{
			_fromQueuePath = fromQueuePath;
			_toQueuePath = toQueuePath;
			_howMany = howMany;
			_isTransational = isTransactional;
			_isRunnable = true;

			for (int i = 0; i < howMany; i++)
			{
				if (_isRunnable)
				{
					if (moveQueue(_fromQueuePath, _toQueuePath, _isTransational))
					{
						if (QueueMoved != null)
						{
							QueueMoved();
						}
					}
					else
					{
						throw new Exception("No more queues to move.");
					}
				}
			}

			Stop();
		}

		public void Stop()
		{
			_isRunnable = false;
		}


		private bool moveQueue(string fromQueuePath, string toQueuePath, bool isTransactional)
		{
			System.Messaging.Message message = getNext(fromQueuePath, isTransactional);
			
			if (message != null)
			{
				move(toQueuePath, message, isTransactional);
				return true;
			}

			return false;
		}

		private void move(string toQueuePath, System.Messaging.Message message, bool isTransactional)
		{
			MessageQueue queue = new MessageQueue(toQueuePath);
			queue.Formatter = new BinaryMessageFormatter();

			try
			{
				if (isTransactional)
				{
					MessageQueueTransaction tran = new MessageQueueTransaction();
					tran.Begin();
					queue.Send(message, tran);
					tran.Commit();
				}
				else
				{
					queue.Send(message);
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
			}

		}

		private Message getNext(string fromQueuePath, bool isTransactional)
		{
			isTransactional = false;
			/*
			 * MSMQ does not support remote transactional receives.  Also, you can only perform a transactional receive from a transactional queue.
			*/

			TimeSpan timeout = new TimeSpan(0, 0, 3);

			MessageQueue queue = new MessageQueue(fromQueuePath);

			//queue.Formatter = new BinaryMessageFormatter();
			System.Messaging.Message message;
			MessageQueueTransaction tran = new MessageQueueTransaction();
		
			try
			{
				if (isTransactional)
				{
					tran.Begin();
					message = queue.Receive(timeout, tran);
					tran.Commit();
				}
				else
				{
					message = queue.Receive(timeout);
				}
			}
			catch(MessageQueueException mqEx)
			{
				if (!mqEx.Message.Equals("Timeout for the requested operation has expired."))
				{
					throw new Exception("Error receiving from " + queue.Path, mqEx);
				}

				return null;
			}
			finally
			{
				if (tran != null)
				{
					if (tran.Status == MessageQueueTransactionStatus.Pending)
					{
						tran.Abort();
					}

					tran.Dispose();
				}
			}

			if (message == null)
			{
				return null;
			}

			return message;
		}


	}
}
