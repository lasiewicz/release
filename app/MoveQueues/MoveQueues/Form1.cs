using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Messaging;

namespace MoveQueues
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox FromQueue;
		private System.Windows.Forms.TextBox ToQueue;
		private System.Windows.Forms.Label Status;

		int _totalMoved = 0;
		Executor _executor = new Executor();

		private System.Windows.Forms.TextBox HowMany;
		private System.Windows.Forms.CheckBox Transactional;
		private System.Windows.Forms.Button Stop;
		private System.Windows.Forms.Button Start;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;


		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			InitializeComponent();

			_executor.QueueMoved +=new QueueMovedEventHandler(_executor_QueueMoved);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.FromQueue = new System.Windows.Forms.TextBox();
			this.ToQueue = new System.Windows.Forms.TextBox();
			this.Status = new System.Windows.Forms.Label();
			this.HowMany = new System.Windows.Forms.TextBox();
			this.Transactional = new System.Windows.Forms.CheckBox();
			this.Stop = new System.Windows.Forms.Button();
			this.Start = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// FromQueue
			// 
			this.FromQueue.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.FromQueue.Location = new System.Drawing.Point(56, 24);
			this.FromQueue.Name = "FromQueue";
			this.FromQueue.Size = new System.Drawing.Size(472, 24);
			this.FromQueue.TabIndex = 0;
			this.FromQueue.Text = "FormatName:DIRECT=OS:devapp01\\private$\\approval_text_2";
			// 
			// ToQueue
			// 
			this.ToQueue.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.ToQueue.Location = new System.Drawing.Point(56, 64);
			this.ToQueue.Name = "ToQueue";
			this.ToQueue.Size = new System.Drawing.Size(472, 24);
			this.ToQueue.TabIndex = 1;
			this.ToQueue.Text = "FormatName:DIRECT=OS:devapp02\\private$\\approval_text_2";
			// 
			// Status
			// 
			this.Status.BackColor = System.Drawing.Color.Black;
			this.Status.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Status.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Status.ForeColor = System.Drawing.Color.Yellow;
			this.Status.Location = new System.Drawing.Point(256, 200);
			this.Status.Name = "Status";
			this.Status.Size = new System.Drawing.Size(88, 23);
			this.Status.TabIndex = 3;
			this.Status.Text = "0";
			this.Status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// HowMany
			// 
			this.HowMany.BackColor = System.Drawing.Color.White;
			this.HowMany.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.HowMany.ForeColor = System.Drawing.Color.Black;
			this.HowMany.Location = new System.Drawing.Point(224, 112);
			this.HowMany.Name = "HowMany";
			this.HowMany.Size = new System.Drawing.Size(80, 24);
			this.HowMany.TabIndex = 4;
			this.HowMany.Text = "5";
			this.HowMany.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// Transactional
			// 
			this.Transactional.Location = new System.Drawing.Point(328, 112);
			this.Transactional.Name = "Transactional";
			this.Transactional.TabIndex = 5;
			this.Transactional.Text = "Transactional";
			// 
			// Stop
			// 
			this.Stop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Stop.Location = new System.Drawing.Point(320, 152);
			this.Stop.Name = "Stop";
			this.Stop.Size = new System.Drawing.Size(75, 24);
			this.Stop.TabIndex = 6;
			this.Stop.Text = "Stop";
			this.Stop.Click += new System.EventHandler(this.Stop_Click_1);
			// 
			// Start
			// 
			this.Start.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Start.Location = new System.Drawing.Point(208, 152);
			this.Start.Name = "Start";
			this.Start.Size = new System.Drawing.Size(75, 24);
			this.Start.TabIndex = 7;
			this.Start.Text = "Start";
			this.Start.Click += new System.EventHandler(this.Start_Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(144, 112);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(64, 23);
			this.label1.TabIndex = 8;
			this.label1.Text = "How many:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(176, 200);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(72, 23);
			this.label2.TabIndex = 9;
			this.label2.Text = "Moved Total:";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 28);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(40, 23);
			this.label3.TabIndex = 10;
			this.label3.Text = "From:";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(7, 68);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(24, 23);
			this.label4.TabIndex = 11;
			this.label4.Text = "To:";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(560, 246);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.Start);
			this.Controls.Add(this.Stop);
			this.Controls.Add(this.Transactional);
			this.Controls.Add(this.HowMany);
			this.Controls.Add(this.Status);
			this.Controls.Add(this.ToQueue);
			this.Controls.Add(this.FromQueue);
			this.Name = "Form1";
			this.Text = "Spark Networks - Move Queues";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void Start_Click(object sender, System.EventArgs e)
		{
			Start.Enabled = false;

			_totalMoved = 0;

			try
			{
				_executor.Run(FromQueue.Text, ToQueue.Text, 
					Convert.ToInt32(HowMany.Text),
					(Transactional.Checked? true:false));
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				stop();
			}

		}

		private void stop()
		{
			_executor.Stop();

			Start.Enabled = true;
		
		}
		private void Stop_Click_1(object sender, System.EventArgs e)
		{
			stop();
		}

		private void _executor_QueueMoved()
		{
			_totalMoved++;
			Status.Text = _totalMoved.ToString();
			Status.Refresh();
			Application.DoEvents();
		}
	}
}
