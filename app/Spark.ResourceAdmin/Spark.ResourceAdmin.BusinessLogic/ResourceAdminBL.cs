using System;
using System.Diagnostics;
using System.IO;
using SourceSafeTypeLib;

using Spark.ResourceAdmin.ValueObjects;
using Spark.ResourceAdmin.ValueObjects.ServiceDefinitions;


namespace Spark.ResourceAdmin.BusinessLogic 
{
	public class ResourceAdminBL : MarshalByRefObject, IResourceAdminService 
	{
		private string _serviceName;
		private string _vssPath;

		public ResourceAdminBL(string serviceName, string vssPath) 
		{
			_serviceName = serviceName;
			_vssPath = vssPath;
		}


		public override object InitializeLifetimeService() 
		{
			return null;
		}


		#region IResourceAdminService Members

		public OperationResult Get(AuthInfo authInfo, string vssFilePath, string localFilePath) 
		{
			OperationResult result = new OperationResult(false);

			try 
			{
				VSSDatabase database = getVSSDatabase(authInfo);
				System.Diagnostics.Trace.WriteLine("__" + database.CurrentProject);

				VSSItem item = database.get_VSSItem(vssFilePath, false);
				item.Get(ref localFilePath, 0);

				result.Success = true;
			}
			catch (Exception ex) 
			{
				logError("Get error (username: " + authInfo.Username + ", filePath: " + vssFilePath + ").", ex);
				result.ErrorMessage = ex.Message;
			}

			return result;
		}


		// get the file version
		public int GetVersion(AuthInfo authInfo, string vssFilePath, string localFilePath) 
		{
			int versionNumber = int.MinValue;
			OperationResult result = new OperationResult(false);

			try 
			{
				VSSDatabase database = getVSSDatabase(authInfo);
				System.Diagnostics.Trace.WriteLine("__" + database.CurrentProject);

				VSSItem item = database.get_VSSItem(vssFilePath, false);
				item.Get(ref localFilePath, 0);
				versionNumber = item.VersionNumber;

				result.Success = true;
			}
			catch (Exception ex) 
			{
				logError("Get error (username: " + authInfo.Username + ", filePath: " + vssFilePath + ").", ex);
				result.ErrorMessage = ex.Message;
			}

			return versionNumber;
		}


		public OperationResult Checkout(AuthInfo authInfo, string vssFilePath, string localFilePath) 
		{
			OperationResult result = new OperationResult(false);

			try 
			{
				VSSDatabase database = getVSSDatabase(authInfo);
				System.Diagnostics.Trace.WriteLine("__" + database.CurrentProject);

				VSSItem item = database.get_VSSItem(vssFilePath, false);

				if (0 != item.IsCheckedOut) 
				{
					throw new Exception("Item cannot be checked out because it is already checked out by another user.");
				}

				item.Checkout("", localFilePath, 0);

				result.Success = true;
			}
			catch (Exception ex) 
			{
				logError("Checkout error (username: " + authInfo.Username + ", filePath: " + vssFilePath + ").", ex);
				result.ErrorMessage = ex.Message;
			}

			return result;
		}

		public OperationResult Checkin(AuthInfo authInfo, string vssFilePath, string localFilePath) 
		{
			OperationResult result = new OperationResult(false);

			try 
			{
				VSSDatabase database = getVSSDatabase(authInfo);
				VSSItem item = database.get_VSSItem(vssFilePath, false);
			
				if (0 == item.IsCheckedOut) 
				{
					throw new Exception("Item cannot be checked in because it is not checked out.");
				}

				item.Checkin("", localFilePath, 0);

				result.Success = true;
			}
			catch (Exception ex) 
			{
				logError("Checkin error (username: " + authInfo.Username + ", filePath: " + vssFilePath + ").", ex);
				result.ErrorMessage = ex.Message;
			}

			return result;
		}

		public OperationResult Add(AuthInfo authInfo, string vssFilePath, string localFilePath) 
		{
			OperationResult result = new OperationResult(false);

			try 
			{
				VSSDatabase database = getVSSDatabase(authInfo);

				// first get the project in which the file will reside
				string vssProjectPath = Path.GetDirectoryName(vssFilePath);
				VSSItem projectItem = database.get_VSSItem(vssProjectPath, false);

				// add the file to the project
				projectItem.Add(localFilePath, "", 0);
			
				result.Success = true;
			}
			catch (Exception ex) 
			{
				logError("Checkin error (username: " + authInfo.Username + ", filePath: " + vssFilePath + ").", ex);
				result.ErrorMessage = ex.Message;
			}

			return result;
		}

		public OperationResult Login(AuthInfo authInfo) 
		{
			OperationResult result = new OperationResult(false);

			try 
			{
				getVSSDatabase(authInfo);
				result.Success = true;
			}
			catch (Exception ex) 
			{
				logError("Login error (username: " + authInfo.Username + ").", ex);
				result.ErrorMessage = ex.Message;
			}

			return result;
		}

		#endregion

		private VSSDatabase getVSSDatabase(AuthInfo authInfo) 
		{
			VSSDatabase	database = new VSSDatabase();

			try 
			{
				database.Open(_vssPath,	authInfo.Username, authInfo.Password);
			}
			catch (Exception ex)
			{
				throw new Exception("Error opening database (vssPath: " + _vssPath + ", username: " + authInfo.Username  + ").", ex);
			}

			return database;
		}

		private void logError(string message,
			Exception ex) 
		{
			EventLog.WriteEntry(_serviceName, message + "\r\n\r\n" + ex.ToString(), EventLogEntryType.Error);
		}
	}
}
