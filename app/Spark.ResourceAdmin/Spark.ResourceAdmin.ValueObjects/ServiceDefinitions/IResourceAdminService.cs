using Spark.ResourceAdmin.ValueObjects;

namespace Spark.ResourceAdmin.ValueObjects.ServiceDefinitions
{
	public interface IResourceAdminService
	{
		OperationResult Get(AuthInfo authInfo, string vssFilePath, string localFilePath);

		int GetVersion(AuthInfo authInfo, string vssFilePath, string localFilePath);

		OperationResult Checkout(AuthInfo authInfo, string vssFilePath, string localFilePath);

		OperationResult Checkin(AuthInfo authInfo, string vssFilePath, string localFilePath);

		OperationResult Add(AuthInfo authInfo, string vssFilePath, string localFilePath);

		OperationResult Login(AuthInfo authInfo);
	}
}
