using System;

namespace Spark.ResourceAdmin.ValueObjects
{
	/// <summary>
	/// Summary description for LoginResult.
	/// </summary>
	[Serializable]
	public class OperationResult
	{
		private bool	_success;
		private string	_errorMessage;

		public bool Success
		{
			get { return _success; }
			set { _success = value; }
		}

		public string ErrorMessage
		{
			get { return _errorMessage; }
			set { _errorMessage = value; }
		}

		public OperationResult(bool success)
		{
			_success = success;
		}
	}
}
