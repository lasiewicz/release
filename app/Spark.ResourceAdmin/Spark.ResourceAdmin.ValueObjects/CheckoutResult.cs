using System;

namespace Spark.ResourceAdmin.ValueObjects
{
	/// <summary>
	/// represents the status of a checkout and maybe contains the content of the resx file if checkout was successful
	/// </summary>
	[Serializable]
	public class CheckoutResult
	{
		public CheckoutResult()
		{
		}
	}
}
