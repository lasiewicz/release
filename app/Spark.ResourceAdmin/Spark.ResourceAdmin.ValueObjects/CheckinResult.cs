using System;

namespace Spark.ResourceAdmin.ValueObjects
{
	/// <summary>
	/// represents the status of a checkin and maybe contains the content of the resx file that the client just modified
	/// </summary>
	[Serializable]
	public class CheckinResult
	{
		public CheckinResult()
		{
		}
	}
}
