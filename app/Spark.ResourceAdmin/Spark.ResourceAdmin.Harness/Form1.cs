using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using Spark.ResourceAdmin.ValueObjects;
using Spark.ResourceAdmin.ValueObjects.ServiceDefinitions;
using Spark.ResourceAdmin.BusinessLogic;

namespace Spark.ResourceAdmin.Harness
{
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnVSSDirectTest;
		private System.Windows.Forms.RichTextBox txtOutput;
		private System.Windows.Forms.Button btnVSSRemoteTest;
		private System.Windows.Forms.TextBox txtUsername;
		private System.Windows.Forms.TextBox txtRemoteService;
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			InitializeComponent();
		}


		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnVSSDirectTest = new System.Windows.Forms.Button();
			this.txtOutput = new System.Windows.Forms.RichTextBox();
			this.btnVSSRemoteTest = new System.Windows.Forms.Button();
			this.txtUsername = new System.Windows.Forms.TextBox();
			this.txtRemoteService = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// btnVSSDirectTest
			// 
			this.btnVSSDirectTest.Location = new System.Drawing.Point(8, 40);
			this.btnVSSDirectTest.Name = "btnVSSDirectTest";
			this.btnVSSDirectTest.Size = new System.Drawing.Size(136, 23);
			this.btnVSSDirectTest.TabIndex = 1;
			this.btnVSSDirectTest.Text = "VSS Direct Test";
			this.btnVSSDirectTest.Click += new System.EventHandler(this.btnVSSDirectTest_Click);
			// 
			// txtOutput
			// 
			this.txtOutput.Location = new System.Drawing.Point(8, 72);
			this.txtOutput.Name = "txtOutput";
			this.txtOutput.Size = new System.Drawing.Size(616, 296);
			this.txtOutput.TabIndex = 2;
			this.txtOutput.Text = "";
			// 
			// btnVSSRemoteTest
			// 
			this.btnVSSRemoteTest.Location = new System.Drawing.Point(160, 40);
			this.btnVSSRemoteTest.Name = "btnVSSRemoteTest";
			this.btnVSSRemoteTest.Size = new System.Drawing.Size(136, 23);
			this.btnVSSRemoteTest.TabIndex = 3;
			this.btnVSSRemoteTest.Text = "VSS Remote Test";
			this.btnVSSRemoteTest.Click += new System.EventHandler(this.btnVSSRemoteTest_Click);
			// 
			// txtUsername
			// 
			this.txtUsername.Location = new System.Drawing.Point(8, 8);
			this.txtUsername.Name = "txtUsername";
			this.txtUsername.Size = new System.Drawing.Size(136, 20);
			this.txtUsername.TabIndex = 4;
			this.txtUsername.Text = "{username}";
			// 
			// txtRemoteService
			// 
			this.txtRemoteService.Location = new System.Drawing.Point(304, 8);
			this.txtRemoteService.Name = "txtRemoteService";
			this.txtRemoteService.Size = new System.Drawing.Size(328, 20);
			this.txtRemoteService.TabIndex = 5;
			this.txtRemoteService.Text = "tcp://devweb01:12345/ResourceAdminService.rem";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(640, 381);
			this.Controls.Add(this.txtRemoteService);
			this.Controls.Add(this.txtUsername);
			this.Controls.Add(this.btnVSSRemoteTest);
			this.Controls.Add(this.txtOutput);
			this.Controls.Add(this.btnVSSDirectTest);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void btnVSSDirectTest_Click(object sender, System.EventArgs e) {
			OperationResult or;
			AuthInfo ai = new AuthInfo(GetUserName(),"");

			txtOutput.AppendText("\n==============================================\n");
			
			ResourceAdminBL rabl = new ResourceAdminBL("Spark.ResourceAdmin.Service", @"\\devvss01\VSS\srcsafe.ini");
			txtOutput.AppendText("Created new resource admin BL obj\n");

			or = rabl.Login(ai);
			txtOutput.AppendText(String.Format("Logged in result [{0}] for [{1}]: {2}\n",or.Success,ai.Username, or.ErrorMessage) );

		}



		private void btnVSSRemoteTest_Click(object sender, System.EventArgs e) {
			OperationResult or;
			AuthInfo ai = new AuthInfo(GetUserName(),"");

			IResourceAdminService rabl = (IResourceAdminService)Activator.GetObject(typeof(IResourceAdminService),txtRemoteService.Text);
			
			txtOutput.AppendText("\n==============================================\n");
			
			txtOutput.AppendText("Created remote resource admin BL obj\n");

			or = rabl.Login(ai);
			txtOutput.AppendText(String.Format("Logged in result [{0}] for [{1}]: {2}\n",or.Success,ai.Username, or.ErrorMessage) );

		}

		private string GetUserName(){
			return txtUsername.Text;
		}

	}
}
