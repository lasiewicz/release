using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.ServiceProcess;

using Spark.ResourceAdmin.BusinessLogic;


namespace Spark.ResourceAdmin.Service
{
	public class ResourceAdminService : System.ServiceProcess.ServiceBase
	{
		private System.ComponentModel.Container components = null;

		public ResourceAdminService()
		{
			InitializeComponent();
		}


		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new ResourceAdminService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}


		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
			this.ServiceName = "Spark.ResourceAdmin.Service";
		}


		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		protected override void OnStart(string[] args)
		{
			ResourceAdminBL resourceAdminBL = new ResourceAdminBL("Spark.ResourceAdmin.Service", @"\\devvss01\VSS\srcsafe.ini");
			ChannelServices.RegisterChannel(new TcpServerChannel("", 12345));
			System.Runtime.Remoting.RemotingServices.Marshal(resourceAdminBL, "ResourceAdminService.rem");
		}
	}
}
