﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Spark.SettingManager.WindowsClient
{
    public partial class Preview : Form
    {
        public string Text
        {
            get { return rtbPreview.Text; }
            set { rtbPreview.Text = value; }
        }
        public Preview()
        {
            InitializeComponent();
        }

        private void Preview_Load(object sender, EventArgs e)
        {

        }
    }
}
