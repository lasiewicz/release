﻿namespace Spark.SettingManager.WindowsClient
{
    partial class Compare
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.chkIncludeScopeSettings = new System.Windows.Forms.TabPage();
            this.btnGetVerifySQL1 = new System.Windows.Forms.Button();
            this.chkIncludeSiteSettings1 = new System.Windows.Forms.CheckBox();
            this.chkIncludeSiteSettings2 = new System.Windows.Forms.CheckBox();
            this.btnSelect1 = new System.Windows.Forms.Button();
            this.btnClearFilter1 = new System.Windows.Forms.Button();
            this.btnSelect2 = new System.Windows.Forms.Button();
            this.btnClearFilter2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btnSearch1 = new System.Windows.Forms.Button();
            this.txtFilter1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSearch2 = new System.Windows.Forms.Button();
            this.txtFilter2 = new System.Windows.Forms.TextBox();
            this.btnGetSQL2 = new System.Windows.Forms.Button();
            this.lblMissedCount2 = new System.Windows.Forms.Label();
            this.lblMissedCount1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvMissing2 = new System.Windows.Forms.DataGridView();
            this.dgvMissing1 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnDiffValueSQL2 = new System.Windows.Forms.Button();
            this.btnDiffValueSQL1 = new System.Windows.Forms.Button();
            this.btnMissingSQL2 = new System.Windows.Forms.Button();
            this.btnMissingSQL1 = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.btnCompareSites = new System.Windows.Forms.Button();
            this.lblDiffSiteSett2 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lblDiffSiteSett1 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lblMissingSiteSett2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblMissingSiteSett1 = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lblTotalSiteSettings2 = new System.Windows.Forms.Label();
            this.lblTotalSiteSettings1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblTotalS = new System.Windows.Forms.Label();
            this.btnClearSiteFilter = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.btnSiteSearch = new System.Windows.Forms.Button();
            this.txtSiteFilter = new System.Windows.Forms.TextBox();
            this.cboSite2 = new System.Windows.Forms.ComboBox();
            this.cboSite1 = new System.Windows.Forms.ComboBox();
            this.dgvSiteSetting2 = new System.Windows.Forms.DataGridView();
            this.dgvSiteSetting1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.cboEnv1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboEnv2 = new System.Windows.Forms.ComboBox();
            this.btnCompare = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabControl1.SuspendLayout();
            this.chkIncludeScopeSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMissing2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMissing1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSiteSetting2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSiteSetting1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.chkIncludeScopeSettings);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(12, 56);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1354, 837);
            this.tabControl1.TabIndex = 0;
            // 
            // chkIncludeScopeSettings
            // 
            this.chkIncludeScopeSettings.Controls.Add(this.btnGetVerifySQL1);
            this.chkIncludeScopeSettings.Controls.Add(this.chkIncludeSiteSettings1);
            this.chkIncludeScopeSettings.Controls.Add(this.chkIncludeSiteSettings2);
            this.chkIncludeScopeSettings.Controls.Add(this.btnSelect1);
            this.chkIncludeScopeSettings.Controls.Add(this.btnClearFilter1);
            this.chkIncludeScopeSettings.Controls.Add(this.btnSelect2);
            this.chkIncludeScopeSettings.Controls.Add(this.btnClearFilter2);
            this.chkIncludeScopeSettings.Controls.Add(this.button1);
            this.chkIncludeScopeSettings.Controls.Add(this.label6);
            this.chkIncludeScopeSettings.Controls.Add(this.btnSearch1);
            this.chkIncludeScopeSettings.Controls.Add(this.txtFilter1);
            this.chkIncludeScopeSettings.Controls.Add(this.label5);
            this.chkIncludeScopeSettings.Controls.Add(this.btnSearch2);
            this.chkIncludeScopeSettings.Controls.Add(this.txtFilter2);
            this.chkIncludeScopeSettings.Controls.Add(this.btnGetSQL2);
            this.chkIncludeScopeSettings.Controls.Add(this.lblMissedCount2);
            this.chkIncludeScopeSettings.Controls.Add(this.lblMissedCount1);
            this.chkIncludeScopeSettings.Controls.Add(this.label4);
            this.chkIncludeScopeSettings.Controls.Add(this.label3);
            this.chkIncludeScopeSettings.Controls.Add(this.dgvMissing2);
            this.chkIncludeScopeSettings.Controls.Add(this.dgvMissing1);
            this.chkIncludeScopeSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIncludeScopeSettings.Location = new System.Drawing.Point(4, 24);
            this.chkIncludeScopeSettings.Name = "chkIncludeScopeSettings";
            this.chkIncludeScopeSettings.Padding = new System.Windows.Forms.Padding(3);
            this.chkIncludeScopeSettings.Size = new System.Drawing.Size(1346, 809);
            this.chkIncludeScopeSettings.TabIndex = 0;
            this.chkIncludeScopeSettings.Text = "Settings";
            this.chkIncludeScopeSettings.UseVisualStyleBackColor = true;
            this.chkIncludeScopeSettings.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // btnGetVerifySQL1
            // 
            this.btnGetVerifySQL1.Location = new System.Drawing.Point(524, 55);
            this.btnGetVerifySQL1.Name = "btnGetVerifySQL1";
            this.btnGetVerifySQL1.Size = new System.Drawing.Size(127, 21);
            this.btnGetVerifySQL1.TabIndex = 20;
            this.btnGetVerifySQL1.Text = "Get Verify SQL";
            this.btnGetVerifySQL1.UseVisualStyleBackColor = true;
            this.btnGetVerifySQL1.Click += new System.EventHandler(this.btnGetVerifySQL1_Click);
            // 
            // chkIncludeSiteSettings1
            // 
            this.chkIncludeSiteSettings1.AutoSize = true;
            this.chkIncludeSiteSettings1.Location = new System.Drawing.Point(268, 91);
            this.chkIncludeSiteSettings1.Name = "chkIncludeSiteSettings1";
            this.chkIncludeSiteSettings1.Size = new System.Drawing.Size(258, 19);
            this.chkIncludeSiteSettings1.TabIndex = 19;
            this.chkIncludeSiteSettings1.Text = "Include Community/Site etc. settings";
            this.chkIncludeSiteSettings1.UseVisualStyleBackColor = true;
            // 
            // chkIncludeSiteSettings2
            // 
            this.chkIncludeSiteSettings2.AutoSize = true;
            this.chkIncludeSiteSettings2.Location = new System.Drawing.Point(923, 91);
            this.chkIncludeSiteSettings2.Name = "chkIncludeSiteSettings2";
            this.chkIncludeSiteSettings2.Size = new System.Drawing.Size(258, 19);
            this.chkIncludeSiteSettings2.TabIndex = 18;
            this.chkIncludeSiteSettings2.Text = "Include Community/Site etc. settings";
            this.chkIncludeSiteSettings2.UseVisualStyleBackColor = true;
            // 
            // btnSelect1
            // 
            this.btnSelect1.Location = new System.Drawing.Point(28, 89);
            this.btnSelect1.Name = "btnSelect1";
            this.btnSelect1.Size = new System.Drawing.Size(129, 24);
            this.btnSelect1.TabIndex = 17;
            this.btnSelect1.Text = "Select/UnSelect";
            this.btnSelect1.UseVisualStyleBackColor = true;
            this.btnSelect1.Click += new System.EventHandler(this.btnSelect1_Click);
            // 
            // btnClearFilter1
            // 
            this.btnClearFilter1.Location = new System.Drawing.Point(402, 55);
            this.btnClearFilter1.Name = "btnClearFilter1";
            this.btnClearFilter1.Size = new System.Drawing.Size(116, 24);
            this.btnClearFilter1.TabIndex = 16;
            this.btnClearFilter1.Text = "Clear Filter";
            this.btnClearFilter1.UseVisualStyleBackColor = true;
            this.btnClearFilter1.Click += new System.EventHandler(this.btnClearFilter1_Click);
            // 
            // btnSelect2
            // 
            this.btnSelect2.Location = new System.Drawing.Point(688, 87);
            this.btnSelect2.Name = "btnSelect2";
            this.btnSelect2.Size = new System.Drawing.Size(124, 24);
            this.btnSelect2.TabIndex = 15;
            this.btnSelect2.Text = "Select/UnSelect";
            this.btnSelect2.UseVisualStyleBackColor = true;
            this.btnSelect2.Click += new System.EventHandler(this.btnSelect2_Click);
            // 
            // btnClearFilter2
            // 
            this.btnClearFilter2.Location = new System.Drawing.Point(1114, 48);
            this.btnClearFilter2.Name = "btnClearFilter2";
            this.btnClearFilter2.Size = new System.Drawing.Size(116, 24);
            this.btnClearFilter2.TabIndex = 14;
            this.btnClearFilter2.Text = "Clear Filter";
            this.btnClearFilter2.UseVisualStyleBackColor = true;
            this.btnClearFilter2.Click += new System.EventHandler(this.btnClearFilter_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(163, 91);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 21);
            this.button1.TabIndex = 13;
            this.button1.Text = "Get SQL";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 15);
            this.label6.TabIndex = 12;
            this.label6.Text = "Filter:";
            // 
            // btnSearch1
            // 
            this.btnSearch1.Location = new System.Drawing.Point(327, 53);
            this.btnSearch1.Name = "btnSearch1";
            this.btnSearch1.Size = new System.Drawing.Size(69, 24);
            this.btnSearch1.TabIndex = 11;
            this.btnSearch1.Text = "Search";
            this.btnSearch1.UseVisualStyleBackColor = true;
            this.btnSearch1.Click += new System.EventHandler(this.btnSearch1_Click);
            // 
            // txtFilter1
            // 
            this.txtFilter1.Location = new System.Drawing.Point(74, 55);
            this.txtFilter1.Name = "txtFilter1";
            this.txtFilter1.Size = new System.Drawing.Size(242, 21);
            this.txtFilter1.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(713, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 15);
            this.label5.TabIndex = 9;
            this.label5.Text = "Filter:";
            // 
            // btnSearch2
            // 
            this.btnSearch2.Location = new System.Drawing.Point(1033, 47);
            this.btnSearch2.Name = "btnSearch2";
            this.btnSearch2.Size = new System.Drawing.Size(75, 24);
            this.btnSearch2.TabIndex = 8;
            this.btnSearch2.Text = "Search";
            this.btnSearch2.UseVisualStyleBackColor = true;
            this.btnSearch2.Click += new System.EventHandler(this.btnSearch2_Click);
            // 
            // txtFilter2
            // 
            this.txtFilter2.Location = new System.Drawing.Point(775, 50);
            this.txtFilter2.Name = "txtFilter2";
            this.txtFilter2.Size = new System.Drawing.Size(242, 21);
            this.txtFilter2.TabIndex = 7;
            // 
            // btnGetSQL2
            // 
            this.btnGetSQL2.Location = new System.Drawing.Point(818, 89);
            this.btnGetSQL2.Name = "btnGetSQL2";
            this.btnGetSQL2.Size = new System.Drawing.Size(99, 21);
            this.btnGetSQL2.TabIndex = 6;
            this.btnGetSQL2.Text = "Get SQL";
            this.btnGetSQL2.UseVisualStyleBackColor = true;
            this.btnGetSQL2.Click += new System.EventHandler(this.btnGetSQL2_Click);
            // 
            // lblMissedCount2
            // 
            this.lblMissedCount2.AutoSize = true;
            this.lblMissedCount2.Location = new System.Drawing.Point(864, 24);
            this.lblMissedCount2.Name = "lblMissedCount2";
            this.lblMissedCount2.Size = new System.Drawing.Size(15, 15);
            this.lblMissedCount2.TabIndex = 5;
            this.lblMissedCount2.Text = "0";
            // 
            // lblMissedCount1
            // 
            this.lblMissedCount1.AutoSize = true;
            this.lblMissedCount1.Location = new System.Drawing.Point(184, 25);
            this.lblMissedCount1.Name = "lblMissedCount1";
            this.lblMissedCount1.Size = new System.Drawing.Size(15, 15);
            this.lblMissedCount1.TabIndex = 4;
            this.lblMissedCount1.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(705, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(153, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Total Missing Settings:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(153, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Total Missing Settings:";
            // 
            // dgvMissing2
            // 
            this.dgvMissing2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMissing2.Location = new System.Drawing.Point(688, 117);
            this.dgvMissing2.Name = "dgvMissing2";
            this.dgvMissing2.Size = new System.Drawing.Size(620, 532);
            this.dgvMissing2.TabIndex = 1;
            this.dgvMissing2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMissing2_CellContentClick);
            // 
            // dgvMissing1
            // 
            this.dgvMissing1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMissing1.Location = new System.Drawing.Point(28, 117);
            this.dgvMissing1.Name = "dgvMissing1";
            this.dgvMissing1.Size = new System.Drawing.Size(595, 532);
            this.dgvMissing1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnDiffValueSQL2);
            this.tabPage2.Controls.Add(this.btnDiffValueSQL1);
            this.tabPage2.Controls.Add(this.btnMissingSQL2);
            this.tabPage2.Controls.Add(this.btnMissingSQL1);
            this.tabPage2.Controls.Add(this.label23);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.btnCompareSites);
            this.tabPage2.Controls.Add(this.lblDiffSiteSett2);
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Controls.Add(this.lblDiffSiteSett1);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.lblMissingSiteSett2);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.lblMissingSiteSett1);
            this.tabPage2.Controls.Add(this.lbl1);
            this.tabPage2.Controls.Add(this.lblTotalSiteSettings2);
            this.tabPage2.Controls.Add(this.lblTotalSiteSettings1);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.lblTotalS);
            this.tabPage2.Controls.Add(this.btnClearSiteFilter);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.btnSiteSearch);
            this.tabPage2.Controls.Add(this.txtSiteFilter);
            this.tabPage2.Controls.Add(this.cboSite2);
            this.tabPage2.Controls.Add(this.cboSite1);
            this.tabPage2.Controls.Add(this.dgvSiteSetting2);
            this.tabPage2.Controls.Add(this.dgvSiteSetting1);
            this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1346, 809);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Site Settings";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnDiffValueSQL2
            // 
            this.btnDiffValueSQL2.Location = new System.Drawing.Point(909, 165);
            this.btnDiffValueSQL2.Name = "btnDiffValueSQL2";
            this.btnDiffValueSQL2.Size = new System.Drawing.Size(243, 23);
            this.btnDiffValueSQL2.TabIndex = 38;
            this.btnDiffValueSQL2.Text = "Get Different Values Update SQL";
            this.btnDiffValueSQL2.UseVisualStyleBackColor = true;
            this.btnDiffValueSQL2.Click += new System.EventHandler(this.btnDiffValueSQL2_Click);
            // 
            // btnDiffValueSQL1
            // 
            this.btnDiffValueSQL1.Location = new System.Drawing.Point(244, 165);
            this.btnDiffValueSQL1.Name = "btnDiffValueSQL1";
            this.btnDiffValueSQL1.Size = new System.Drawing.Size(243, 23);
            this.btnDiffValueSQL1.TabIndex = 37;
            this.btnDiffValueSQL1.Text = "Get Different Values Update SQL";
            this.btnDiffValueSQL1.UseVisualStyleBackColor = true;
            this.btnDiffValueSQL1.Click += new System.EventHandler(this.btnDiffValueSQL1_Click);
            // 
            // btnMissingSQL2
            // 
            this.btnMissingSQL2.Location = new System.Drawing.Point(679, 165);
            this.btnMissingSQL2.Name = "btnMissingSQL2";
            this.btnMissingSQL2.Size = new System.Drawing.Size(202, 23);
            this.btnMissingSQL2.TabIndex = 36;
            this.btnMissingSQL2.Text = "Get Missing Settings SQL";
            this.btnMissingSQL2.UseVisualStyleBackColor = true;
            this.btnMissingSQL2.Click += new System.EventHandler(this.btnMissingSQL2_Click);
            // 
            // btnMissingSQL1
            // 
            this.btnMissingSQL1.Location = new System.Drawing.Point(16, 165);
            this.btnMissingSQL1.Name = "btnMissingSQL1";
            this.btnMissingSQL1.Size = new System.Drawing.Size(202, 23);
            this.btnMissingSQL1.TabIndex = 35;
            this.btnMissingSQL1.Text = "Get Missing Settings SQL";
            this.btnMissingSQL1.UseVisualStyleBackColor = true;
            this.btnMissingSQL1.Click += new System.EventHandler(this.btnMissingSQL1_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(678, 6);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(39, 16);
            this.label23.TabIndex = 34;
            this.label23.Text = "Site:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(13, 6);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(39, 16);
            this.label21.TabIndex = 33;
            this.label21.Text = "Site:";
            // 
            // btnCompareSites
            // 
            this.btnCompareSites.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompareSites.Location = new System.Drawing.Point(480, 6);
            this.btnCompareSites.Name = "btnCompareSites";
            this.btnCompareSites.Size = new System.Drawing.Size(174, 36);
            this.btnCompareSites.TabIndex = 8;
            this.btnCompareSites.Text = "Compare Site Settings";
            this.btnCompareSites.UseVisualStyleBackColor = true;
            this.btnCompareSites.Click += new System.EventHandler(this.btnCompareSites_Click);
            // 
            // lblDiffSiteSett2
            // 
            this.lblDiffSiteSett2.AutoSize = true;
            this.lblDiffSiteSett2.BackColor = System.Drawing.Color.Yellow;
            this.lblDiffSiteSett2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiffSiteSett2.ForeColor = System.Drawing.Color.Black;
            this.lblDiffSiteSett2.Location = new System.Drawing.Point(1057, 98);
            this.lblDiffSiteSett2.Name = "lblDiffSiteSett2";
            this.lblDiffSiteSett2.Size = new System.Drawing.Size(16, 16);
            this.lblDiffSiteSett2.TabIndex = 32;
            this.lblDiffSiteSett2.Text = "0";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Yellow;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(682, 96);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(235, 16);
            this.label22.TabIndex = 31;
            this.label22.Text = "Site Settings with different values";
            // 
            // lblDiffSiteSett1
            // 
            this.lblDiffSiteSett1.AutoSize = true;
            this.lblDiffSiteSett1.BackColor = System.Drawing.Color.Yellow;
            this.lblDiffSiteSett1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiffSiteSett1.ForeColor = System.Drawing.Color.Black;
            this.lblDiffSiteSett1.Location = new System.Drawing.Point(342, 98);
            this.lblDiffSiteSett1.Name = "lblDiffSiteSett1";
            this.lblDiffSiteSett1.Size = new System.Drawing.Size(16, 16);
            this.lblDiffSiteSett1.TabIndex = 30;
            this.lblDiffSiteSett1.Text = "0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Yellow;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(13, 98);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(235, 16);
            this.label20.TabIndex = 29;
            this.label20.Text = "Site Settings with different values";
            // 
            // lblMissingSiteSett2
            // 
            this.lblMissingSiteSett2.AutoSize = true;
            this.lblMissingSiteSett2.BackColor = System.Drawing.Color.Red;
            this.lblMissingSiteSett2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMissingSiteSett2.ForeColor = System.Drawing.Color.Black;
            this.lblMissingSiteSett2.Location = new System.Drawing.Point(1057, 70);
            this.lblMissingSiteSett2.Name = "lblMissingSiteSett2";
            this.lblMissingSiteSett2.Size = new System.Drawing.Size(16, 16);
            this.lblMissingSiteSett2.TabIndex = 28;
            this.lblMissingSiteSett2.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Red;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(678, 71);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(323, 16);
            this.label11.TabIndex = 27;
            this.label11.Text = "Site Settings not present in other environment:";
            // 
            // lblMissingSiteSett1
            // 
            this.lblMissingSiteSett1.AutoSize = true;
            this.lblMissingSiteSett1.BackColor = System.Drawing.Color.Red;
            this.lblMissingSiteSett1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMissingSiteSett1.ForeColor = System.Drawing.Color.Black;
            this.lblMissingSiteSett1.Location = new System.Drawing.Point(342, 71);
            this.lblMissingSiteSett1.Name = "lblMissingSiteSett1";
            this.lblMissingSiteSett1.Size = new System.Drawing.Size(16, 16);
            this.lblMissingSiteSett1.TabIndex = 26;
            this.lblMissingSiteSett1.Text = "0";
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.BackColor = System.Drawing.Color.Red;
            this.lbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1.ForeColor = System.Drawing.Color.Black;
            this.lbl1.Location = new System.Drawing.Point(13, 71);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(323, 16);
            this.lbl1.TabIndex = 25;
            this.lbl1.Text = "Site Settings not present in other environment:";
            // 
            // lblTotalSiteSettings2
            // 
            this.lblTotalSiteSettings2.AutoSize = true;
            this.lblTotalSiteSettings2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalSiteSettings2.Location = new System.Drawing.Point(1057, 43);
            this.lblTotalSiteSettings2.Name = "lblTotalSiteSettings2";
            this.lblTotalSiteSettings2.Size = new System.Drawing.Size(16, 16);
            this.lblTotalSiteSettings2.TabIndex = 24;
            this.lblTotalSiteSettings2.Text = "0";
            // 
            // lblTotalSiteSettings1
            // 
            this.lblTotalSiteSettings1.AutoSize = true;
            this.lblTotalSiteSettings1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalSiteSettings1.Location = new System.Drawing.Point(342, 43);
            this.lblTotalSiteSettings1.Name = "lblTotalSiteSettings1";
            this.lblTotalSiteSettings1.Size = new System.Drawing.Size(16, 16);
            this.lblTotalSiteSettings1.TabIndex = 23;
            this.lblTotalSiteSettings1.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(678, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(139, 16);
            this.label8.TabIndex = 22;
            this.label8.Text = "Total Site Settings:";
            // 
            // lblTotalS
            // 
            this.lblTotalS.AutoSize = true;
            this.lblTotalS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalS.Location = new System.Drawing.Point(13, 43);
            this.lblTotalS.Name = "lblTotalS";
            this.lblTotalS.Size = new System.Drawing.Size(139, 16);
            this.lblTotalS.TabIndex = 21;
            this.lblTotalS.Text = "Total Site Settings:";
            // 
            // btnClearSiteFilter
            // 
            this.btnClearSiteFilter.Location = new System.Drawing.Point(801, 124);
            this.btnClearSiteFilter.Name = "btnClearSiteFilter";
            this.btnClearSiteFilter.Size = new System.Drawing.Size(116, 24);
            this.btnClearSiteFilter.TabIndex = 20;
            this.btnClearSiteFilter.Text = "Clear Filter";
            this.btnClearSiteFilter.UseVisualStyleBackColor = true;
            this.btnClearSiteFilter.Click += new System.EventHandler(this.btnClearSiteFilter_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(390, 133);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 16);
            this.label7.TabIndex = 19;
            this.label7.Text = "Filter:";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // btnSiteSearch
            // 
            this.btnSiteSearch.Location = new System.Drawing.Point(705, 124);
            this.btnSiteSearch.Name = "btnSiteSearch";
            this.btnSiteSearch.Size = new System.Drawing.Size(75, 24);
            this.btnSiteSearch.TabIndex = 18;
            this.btnSiteSearch.Text = "Search";
            this.btnSiteSearch.UseVisualStyleBackColor = true;
            this.btnSiteSearch.Click += new System.EventHandler(this.btnSiteSearch_Click);
            // 
            // txtSiteFilter
            // 
            this.txtSiteFilter.Location = new System.Drawing.Point(440, 128);
            this.txtSiteFilter.Name = "txtSiteFilter";
            this.txtSiteFilter.Size = new System.Drawing.Size(242, 22);
            this.txtSiteFilter.TabIndex = 17;
            this.txtSiteFilter.TextChanged += new System.EventHandler(this.txtSiteFilter_TextChanged);
            // 
            // cboSite2
            // 
            this.cboSite2.FormattingEnabled = true;
            this.cboSite2.Location = new System.Drawing.Point(821, 3);
            this.cboSite2.Name = "cboSite2";
            this.cboSite2.Size = new System.Drawing.Size(242, 24);
            this.cboSite2.TabIndex = 7;
            // 
            // cboSite1
            // 
            this.cboSite1.FormattingEnabled = true;
            this.cboSite1.Location = new System.Drawing.Point(116, 3);
            this.cboSite1.Name = "cboSite1";
            this.cboSite1.Size = new System.Drawing.Size(242, 24);
            this.cboSite1.TabIndex = 6;
            // 
            // dgvSiteSetting2
            // 
            this.dgvSiteSetting2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSiteSetting2.Location = new System.Drawing.Point(681, 204);
            this.dgvSiteSetting2.Name = "dgvSiteSetting2";
            this.dgvSiteSetting2.Size = new System.Drawing.Size(662, 526);
            this.dgvSiteSetting2.TabIndex = 3;
            this.dgvSiteSetting2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMissingSiteSetting2_CellContentClick);
            // 
            // dgvSiteSetting1
            // 
            this.dgvSiteSetting1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSiteSetting1.Location = new System.Drawing.Point(16, 204);
            this.dgvSiteSetting1.Name = "dgvSiteSetting1";
            this.dgvSiteSetting1.Size = new System.Drawing.Size(609, 526);
            this.dgvSiteSetting1.TabIndex = 2;
            this.dgvSiteSetting1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSiteSetting1_CellContentClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "Environment 1:";
            // 
            // cboEnv1
            // 
            this.cboEnv1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboEnv1.FormattingEnabled = true;
            this.cboEnv1.Location = new System.Drawing.Point(133, 12);
            this.cboEnv1.Name = "cboEnv1";
            this.cboEnv1.Size = new System.Drawing.Size(242, 23);
            this.cboEnv1.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(788, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "Environment 2:";
            // 
            // cboEnv2
            // 
            this.cboEnv2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboEnv2.FormattingEnabled = true;
            this.cboEnv2.Location = new System.Drawing.Point(903, 11);
            this.cboEnv2.Name = "cboEnv2";
            this.cboEnv2.Size = new System.Drawing.Size(251, 23);
            this.cboEnv2.TabIndex = 7;
            // 
            // btnCompare
            // 
            this.btnCompare.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompare.Location = new System.Drawing.Point(519, 14);
            this.btnCompare.Name = "btnCompare";
            this.btnCompare.Size = new System.Drawing.Size(122, 36);
            this.btnCompare.TabIndex = 9;
            this.btnCompare.Text = "Compare";
            this.btnCompare.UseVisualStyleBackColor = true;
            this.btnCompare.Click += new System.EventHandler(this.btnCompare_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(13, 54);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(271, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "Site Settings not present in other environment:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(974, 54);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(14, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(678, 54);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(271, 13);
            this.label13.TabIndex = 27;
            this.label13.Text = "Site Settings not present in other environment:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(300, 54);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(800, 30);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(14, 13);
            this.label15.TabIndex = 24;
            this.label15.Text = "0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(135, 30);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(14, 13);
            this.label16.TabIndex = 23;
            this.label16.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(678, 30);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(116, 13);
            this.label17.TabIndex = 22;
            this.label17.Text = "Total Site Settings:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(13, 30);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(116, 13);
            this.label18.TabIndex = 21;
            this.label18.Text = "Total Site Settings:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(745, 107);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(116, 24);
            this.button2.TabIndex = 20;
            this.button2.Text = "Clear Filter";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(382, 113);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(32, 13);
            this.label19.TabIndex = 19;
            this.label19.Text = "Filter:";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(685, 106);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(54, 24);
            this.button3.TabIndex = 18;
            this.button3.Text = "Search";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(420, 110);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(242, 20);
            this.textBox1.TabIndex = 17;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(681, 6);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(242, 21);
            this.comboBox1.TabIndex = 7;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(16, 6);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(242, 21);
            this.comboBox2.TabIndex = 6;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(681, 137);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(662, 566);
            this.dataGridView1.TabIndex = 3;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(16, 137);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(645, 566);
            this.dataGridView2.TabIndex = 2;
            // 
            // Compare
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1387, 905);
            this.Controls.Add(this.btnCompare);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboEnv2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboEnv1);
            this.Controls.Add(this.tabControl1);
            this.Name = "Compare";
            this.Text = "Compare";
            this.Load += new System.EventHandler(this.Compare_Load);
            this.tabControl1.ResumeLayout(false);
            this.chkIncludeScopeSettings.ResumeLayout(false);
            this.chkIncludeScopeSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMissing2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMissing1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSiteSetting2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSiteSetting1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage chkIncludeScopeSettings;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboEnv1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboEnv2;
        private System.Windows.Forms.DataGridView dgvMissing1;
        private System.Windows.Forms.DataGridView dgvMissing2;
        private System.Windows.Forms.Button btnCompare;
        private System.Windows.Forms.Label lblMissedCount2;
        private System.Windows.Forms.Label lblMissedCount1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnGetSQL2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnSearch1;
        private System.Windows.Forms.TextBox txtFilter1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSearch2;
        private System.Windows.Forms.TextBox txtFilter2;
        private System.Windows.Forms.Button btnClearFilter2;
        private System.Windows.Forms.Button btnSelect2;
        private System.Windows.Forms.Button btnSelect1;
        private System.Windows.Forms.Button btnClearFilter1;
        private System.Windows.Forms.DataGridView dgvSiteSetting2;
        private System.Windows.Forms.DataGridView dgvSiteSetting1;
        private System.Windows.Forms.CheckBox chkIncludeSiteSettings2;
        private System.Windows.Forms.CheckBox chkIncludeSiteSettings1;
        private System.Windows.Forms.Button btnGetVerifySQL1;
        private System.Windows.Forms.ComboBox cboSite2;
        private System.Windows.Forms.ComboBox cboSite1;
        private System.Windows.Forms.Button btnCompareSites;
        private System.Windows.Forms.Button btnClearSiteFilter;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnSiteSearch;
        private System.Windows.Forms.TextBox txtSiteFilter;
        private System.Windows.Forms.Label lblTotalS;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblTotalSiteSettings2;
        private System.Windows.Forms.Label lblTotalSiteSettings1;
        private System.Windows.Forms.Label lblMissingSiteSett1;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lblDiffSiteSett2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblDiffSiteSett1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lblMissingSiteSett2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button btnMissingSQL2;
        private System.Windows.Forms.Button btnMissingSQL1;
        private System.Windows.Forms.Button btnDiffValueSQL1;
        private System.Windows.Forms.Button btnDiffValueSQL2;
    }
}