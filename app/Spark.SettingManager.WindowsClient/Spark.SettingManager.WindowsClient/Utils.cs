﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using Matchnet.Configuration.ValueObjects.Management;

namespace Spark.SettingManager.WindowsClient
{
   public class Utils
    {

       public enum SettingsColumns : int
       {
           CheckBox,
           SettingID,
           SettingCategoryID,
           SettingConstant,
           GlobalDefaultValue,
           SettingDescription,
           IsRequiredForCommunity,
           IsRequiredForSite,
           CreateDate
       }

       public enum SiteSettingsColumns : int
       {
           SiteSettingID=0,
           SiteID,
           SettingID,
           SiteSettingValue
           
       }

       public enum ServerSettingsColumns : int
       {
           ServerSettingID ,
           ServerID,
           SettingID,
           CommunityID,
           SiteID,
           BrandID,
           ServerSettingValue

       }

       public enum CommunitySettingsColumns : int
       {
           CommunitySettingID,
           CommunityID,
           SettingID,
           CommunitySettingValue
           

       }
       public enum Scope : int
       {
           Global,
           Community,
           Site,
           Brand,
           Server
           
       }
       public static void populateGrid<T>(DataGridView dgv, bool addSelect, List<T> list)
       {
           if (list == null || list.Count == 0)
               return;

           if (addSelect)
           {
               DataGridViewColumn cellCheck0 = new DataGridViewCheckBoxColumn();
               cellCheck0.HeaderText = "Select";

               dgv.Columns.Insert(0, cellCheck0);
           }
           Type type = list[0].GetType();
           PropertyInfo[] props = type.GetProperties();
           foreach (PropertyInfo p in props)
           {
               dgv.Columns.Add(p.Name, p.Name);

           }


           foreach (T item in list)
           {
               List<string> values = new List<string>();
               if (addSelect)
                   values.Add("false");
               foreach (PropertyInfo p in props)
               {
                   object val=p.GetValue(item, null);
                   if(val==null)
                       values.Add("null");
                   else
                        values.Add(p.GetValue(item, null).ToString());
               }

               dgv.Rows.Add(values.ToArray());
           }

       }

       public static void populateGrid<T>(DataGridView dgv, bool addSelect, List<T> list, List<string>excludedProps, Hashtable rowColors)
       {
           try
           {
               if (list == null || list.Count == 0)
                   return;

               if (addSelect)
               {
                   DataGridViewColumn cellCheck0 = new DataGridViewCheckBoxColumn();
                   cellCheck0.HeaderText = "Select";

                   dgv.Columns.Insert(0, cellCheck0);
               }
               Type type = list[0].GetType();
               PropertyInfo[] props = type.GetProperties();
               foreach (PropertyInfo p in props)
               {
                   bool addColumn = true;
                   if (excludedProps != null)
                   {
                       string pname = excludedProps.Find(delegate(string name) { return name.ToLower() == p.Name.ToLower(); });
                       addColumn = String.IsNullOrEmpty(pname);

                   }
                   if (addColumn)
                       dgv.Columns.Add(p.Name, p.Name);

               }
               int rowid = 0;

               foreach (T item in list)
               {

                   List<string> values = new List<string>();
                   if (addSelect)
                       values.Add("false");
                   System.Drawing.Color color = System.Drawing.Color.AntiqueWhite;
                   foreach (PropertyInfo p in props)
                   {
                       bool addColumn = true;
                       if (rowColors != null)
                       {
                           if (rowColors.ContainsKey(p.Name.ToLower()))
                           {
                               if (GetBool(p, item))
                               {
                                   color = (System.Drawing.Color)rowColors[p.Name.ToLower()];
                               }
                           }

                       }
                       if (excludedProps != null)
                       {
                           string pname = excludedProps.Find(delegate(string name) { return name.ToLower() == p.Name.ToLower(); });
                           addColumn = String.IsNullOrEmpty(pname);

                       }
                       if (addColumn)
                       {
                           values.Add(p.GetValue(item, null).ToString());



                       }

                   }
                   dgv.Rows.Add(values.ToArray());
                   dgv.Rows[rowid].DefaultCellStyle.BackColor = color;
                   rowid += 1;

               }
           }
           catch (Exception ex) { }
       }

       public static bool GetBool(PropertyInfo p, Object o)
       {
           try
           {
               return Boolean.Parse(p.GetValue(o,null).ToString());
           }
           catch (Exception ex)
           { return false; }
       }
       public static void applyColor(DataGridView dgv, int rowid)
       {

           dgv.Rows[rowid].DefaultCellStyle.BackColor = System.Drawing.Color.Aqua;
       }

       public static string ToSQL<T>(T obj, string tablename, Hashtable customValues )
       {
           string SQL = "\r\ninsert into " + tablename + "\r\n ({0})\r\n values\r\n ({1})";
           try
           {
               Type type = obj.GetType();
               PropertyInfo[] props = type.GetProperties();
               string columns = "";
               string values = "";
               int count = props.Count<PropertyInfo>();
               int i = 0;
               foreach (PropertyInfo p in props)
               {
                   i += 1;
                   columns += p.Name;
                   if (customValues != null)
                   {
                       if (customValues.ContainsKey(p.Name.ToLower()))
                       {
                           values +=  customValues[p.Name.ToLower()].ToString() ;
                       }
                       else
                       {
                           values += "'" + p.GetValue(obj, null).ToString() + "'";
                       }
                   }
                   else
                   {
                       values += "'" + p.GetValue(obj, null).ToString() + "'";
                   }
                   if (i < count)
                   {
                       columns += ",";
                       values += ",";
                   }
               }
               return String.Format(SQL, columns, values);

           }
           catch (Exception ex)
           { throw ex; }

       }


       public static string ToSQL<T>(T obj, string tablename, Hashtable customValues,List<string> excludeProps)
       {
           string SQL = "\r\ninsert into " + tablename + "\r\n ({0})\r\n values\r\n ({1})";
           try
           {
               Type type = obj.GetType();
               PropertyInfo[] props = type.GetProperties();
               string columns = "";
               string values = "";
               int count = props.Count<PropertyInfo>();
               int i = 0;
               foreach (PropertyInfo p in props)
               {
                   i += 1;
                   if (excludeProps != null)
                   {
                       string exclude = excludeProps.Find(delegate(string name) { return name.ToLower() == p.Name.ToLower(); });
                       if (!string.IsNullOrEmpty(exclude))
                           continue;
                   }
                   columns += p.Name;
                   if (customValues != null)
                   {
                       if (customValues.ContainsKey(p.Name.ToLower()))
                       {
                           values += customValues[p.Name.ToLower()].ToString();
                       }
                       else
                       {
                           values += "'" + p.GetValue(obj, null).ToString() + "'";
                       }
                   }
                   else
                   {
                       values += "'" + p.GetValue(obj, null).ToString() + "'";
                   }
                   if (i < count)
                   {
                       columns += ",";
                       values += ",";
                   }
               }
               return String.Format(SQL, columns, values);

           }
           catch (Exception ex)
           { throw ex; }

       }

       public static void ToggleRows(DataGridView dgv, int index)
       {
           for (int i = 0; i < dgv.Rows.Count; i++)
           {
               if (dgv.Rows[i].Cells[index].Value != null)
               {
                   bool val = Boolean.Parse(dgv.Rows[i].Cells[index].Value.ToString());
                   dgv.Rows[i].Cells[index].Value = !val;
               }
           }

       }


       public static string GetMissingCommunitySQL(Setting setting, List<CommunitySetting> settingslist)
       {
           string sql = "";
           try
           {
               string prefix = "";
               string prefix2 = "\r\nselect @communitysettingid=max(communitysettingid)+1 from communitysetting";
               prefix2 += "\r\nselect @SettingID=SettingID from Setting where settingconstant='" + setting.SettingConstant + "'";
               List<CommunitySetting> list1 = SettingsBL.Instance.GetCommunitySettings(setting, settingslist);
               sql += "\r\n--Community Setting for  " + setting.SettingConstant;
               if (list1 == null || list1.Count <= 0) { return sql; }
               sql += prefix;

               Hashtable custom = new Hashtable();
               custom["settingid"] = "@settingid";
               custom["communitysettingid"] = "@communitysettingid";
               custom["createdate"] = "getdate()";
               for (int i = 0; i < list1.Count; i++)
               {
                   sql += prefix2;

                   sql += "\r\n" + Utils.ToSQL<CommunitySetting>(list1[i], "CommunitySetting", custom);

               }
               return sql;

           }
           catch (Exception ex)
           { return sql; throw ex; }
       }


       public static string GetMissingSiteSQL(Setting setting, List<SiteSetting> settingslist)
       {
           string sql = "";
           try
           {
               string prefix = "";
               string prefix2 = "\r\nselect @SiteSettingID=max(SiteSettingID)+1 from SiteSetting";
               prefix2 += "\r\nselect @SettingID=SettingID from Setting where settingconstant='" + setting.SettingConstant + "'";
               List<SiteSetting> list1 = SettingsBL.Instance.GetSiteSettings(setting, settingslist);
               sql += "\r\n--Site Setting for  " + setting.SettingConstant + "\r\n";
               if (list1 == null || list1.Count <= 0) { return sql; }
               sql += prefix;
               Hashtable custom = new Hashtable();
               custom["settingid"] = "@settingid";
               custom["createdate"] = "getdate()";
               custom["sitesettingid"] = "@SiteSettingID";
               for (int i = 0; i < list1.Count; i++)
               {
                   sql += prefix2;

                   sql += "\r\n" + Utils.ToSQL<SiteSetting>(list1[i], "SiteSetting", custom);

               }
               return sql;

           }
           catch (Exception ex)
           { return sql; throw ex; }
       }
       public static Settings GetEnvironmentSettings(ComboBox cboEnv1)
       {
           Settings _settings1 = null;
           Matchnet.Configuration.ServiceAdapters.ManagementSA.Instance.URI = EnvironmentBL.Instance.Environments[cboEnv1.SelectedIndex].ManagementServiceURI;
           _settings1 =Matchnet.Configuration.ServiceAdapters.ManagementSA.Instance.GetSettings();
           return _settings1;

       }
       public static string GetMissingBrandSQL(Setting setting, List<BrandSetting> settingslist)
       {
           string sql = "";
           try
           {
               string prefix = "";
               string prefix2 = "\r\nselect @BrandSettingID=max(BrandSettingID)+1 from BrandSetting";
               prefix2 += "\r\nselect @SettingID=SettingID from Setting where settingconstant='" + setting.SettingConstant + "'";
               List<BrandSetting> list1 = SettingsBL.Instance.GetBrandSettings(setting, settingslist);
               sql += "\r\n--Brand Setting for  " + setting.SettingConstant;
               if (list1 == null || list1.Count <= 0) { return sql; }
               sql += prefix;
               Hashtable custom = new Hashtable();
               custom["settingid"] = "@settingid";
               custom["createdate"] = "getdate()";
               custom["brandsettingid"] = "@BrandSettingID";
               for (int i = 0; i < list1.Count; i++)
               {
                   sql += prefix2;

                   sql += "\r\n" + Utils.ToSQL<BrandSetting>(list1[i], "BrandSetting", custom);

               }
               return sql;

           }
           catch (Exception ex)
           { return sql; throw ex; }
       }

       public static List<SiteSetting> FiterSiteSetting(List<SiteSetting> list, int siteid)
       {

           List<SiteSetting> slist=list.FindAll(delegate(SiteSetting ss){return ss.SiteID==siteid;});
           return slist;

       }
       public static string GetInnerExcepion(Exception ex)
       {
           string inner="\r\nInner Exception: {0} \r\n {1}";
           if (ex.InnerException != null)
           { return String.Format(inner, ex.InnerException.Source, ex.InnerException.Message); }

               return "";

       }
    }
}
