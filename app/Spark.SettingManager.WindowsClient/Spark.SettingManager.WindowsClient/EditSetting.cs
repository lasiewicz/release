﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.Management;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects;
namespace Spark.SettingManager.WindowsClient
{
    public partial class EditSetting : Form
    {
        public EditSetting()
        {
            InitializeComponent();
        }

        public ServerSetting ServerSettings { get; set; }
        int _settingid;
        int _settingcategoryid;
        string _settingconstant = null;

        public string URI { get; set; }

        public string Action
        { get; set; }
 
        public string ServerName{get;set;}
        public string Scope
        {
            get { return lblSettingScope.Text; }
            set { lblSettingScope.Text = value; }
        }

        public string ScopeID
        {
            get { return lblSettingScopeID.Text; }
            set { lblSettingScopeID.Text = value; }
        }

        public string ScopeName
        {
            get { return lblSettingScope.Text; }
            set { lblSettingScope.Text = value; }
        }

        public int SettingID
        {
            get { return _settingid; }
            set { _settingid = value; }
        }

    
        public string SettingConstant
        {
            get { return lblSettingConstant.Text; }
            set { lblSettingConstant.Text = value; }
        }


        public string GlobalDefaultValue
        {
            get { return lblSettingGlobalValue.Text; }
            set { lblSettingGlobalValue.Text = value; }
        }


        public string SettingDescription
        {
            get { return lblSettingDescription.Text; }
            set { lblSettingDescription.Text = value; }
        }

        public string SettingValue
        {
            get { return txtSettingValue.Text; }
            set { txtSettingValue.Text = value; }
        }

       

        private void EditSetting_Load(object sender, EventArgs e)
        {
            pnlScopes.Visible = false;
            if (ScopeName.ToLower() == "site")
                LoadSites(cblSites);
            else if (ScopeName.ToLower() == "server")
            {
                LoadServers();
                LoadCommunities(lbxCommunityID);
                LoadSites(lbxSiteID);
                LoadBrands(lbxBrandID);
                pnlScopes.Visible = true;
            }
            else if (ScopeName.ToLower() == "community")
                LoadCommunities(cblSites);
            else
                cblSites.Visible = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            try
            {
                if (Action == "add")
                {
                    btnClone_Click(sender, e);
                    return;
                }
                DialogResult result = MessageBox.Show("You are going to update setting value. You sure know what you are doing?", "Think again", MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                    return;

                SettingsSave save = new SettingsSave();
                save.Scope = (SettingsScope)Enum.Parse(typeof(SettingsScope), ScopeName, true);
                save.SettingConstant = SettingConstant;
                save.SettingValue = SettingValue;
                save.ScopeIDList = new List<int>();
                save.ScopeIDList.Add(Int32.Parse(ScopeID));
                save.ServerName = ServerName;
                if (save.Scope == SettingsScope.server)
                {
                    save.ServerName = ServerName;
                    Matchnet.Content.ValueObjects.BrandConfig.Community c = (Matchnet.Content.ValueObjects.BrandConfig.Community)lbxCommunityID.SelectedItem;
                    if (c != null)
                        save.CommunityID = c.CommunityID;

                    Matchnet.Content.ValueObjects.BrandConfig.Site s = (Matchnet.Content.ValueObjects.BrandConfig.Site)lbxSiteID.SelectedItem;
                    if (s != null)
                        save.SiteID = s.SiteID;


                    Matchnet.Content.ValueObjects.BrandConfig.Brand b = (Matchnet.Content.ValueObjects.BrandConfig.Brand)lbxBrandID.SelectedItem;
                    if (b != null)
                        save.BrandID = b.BrandID;


                }
                
                List<SettingsSave> list = new List<SettingsSave>();
                list.Add(save);
                ManagementSA.Instance.URI = URI;
                ManagementSA.Instance.SaveSettings(list, chkAsync.Checked);
                if(Action!=null && Action.ToLower()=="serversetting")
                      ((SettingsForm)this.Owner).ReloadServerSetting();
                else
                ((SettingsForm)this.Owner).Reload();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " " + ex.Message);
            }
        }

        private void LoadSites(ListBox lbx)
        {
            Matchnet.Content.ValueObjects.BrandConfig.Sites sites= Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetSites();
            foreach (Matchnet.Content.ValueObjects.BrandConfig.Site s in sites)
            {
                lbx.Items.Add(s);
            }            
            lbx.DisplayMember = "Name";
            
            


        }

        private void LoadCommunities(ListBox lbx)
        {
            Matchnet.Content.ValueObjects.BrandConfig.Sites sites = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetSites();
            List<Matchnet.Content.ValueObjects.BrandConfig.Community> communities = new List<Matchnet.Content.ValueObjects.BrandConfig.Community>();
            lbx.Items.Add(null);
            foreach (Matchnet.Content.ValueObjects.BrandConfig.Site s in sites)
            {
                Matchnet.Content.ValueObjects.BrandConfig.Community c = communities.Find(delegate(Matchnet.Content.ValueObjects.BrandConfig.Community c1) { return c1.CommunityID == s.Community.CommunityID; });
                if (c == null)
                {
                    communities.Add(s.Community);
                    int idx=lbx.Items.Add(s.Community);
                    if (ServerSettings != null)
                    {

                        if (ServerSettings.CommunityID > 0)
                        {
                            if (s.Community.CommunityID == ServerSettings.CommunityID)
                                lbx.SelectedIndex = idx;
                        }
                    }
                   
                }
            }
            lbx.DisplayMember = "Name";
            



        }


        private void LoadBrands(ListBox lbx)
        {
            Matchnet.Content.ValueObjects.BrandConfig.Brands brands = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrands();
           
            foreach (Matchnet.Content.ValueObjects.BrandConfig.Brand b in brands)
            {
               
                    lbx.Items.Add(b);

                 
            }
            lbx.DisplayMember = "Uri";
           


        }

        private void LoadServers()
        {
            List<Server> serverlist = ManagementSA.Instance.GetServers();
            foreach (Server s in serverlist)
            {
                cblSites.Items.Add(s);
            }
            cblSites.DisplayMember = "Name";
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = MessageBox.Show("You are going to delete setting value. You sure know what you are doing?", "Think again", MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                    return;

                SettingsSave save = new SettingsSave();
                save.Scope = (SettingsScope)Enum.Parse(typeof(SettingsScope), ScopeName, true);
                save.SettingConstant = SettingConstant;
                save.SettingValue = SettingValue;
                save.ScopeIDList = new List<int>();
                save.ScopeIDList.Add(Int32.Parse(ScopeID));
                save.DeleteFlag = true;
                List<SettingsSave> list = new List<SettingsSave>();
                list.Add(save);
                ManagementSA.Instance.URI = URI;
                ManagementSA.Instance.SaveSettings(list,chkAsync.Checked);
                ((SettingsForm)this.Owner).Reload();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " " + ex.Message);
            }
        }

        private void btnClone_Click(object sender, EventArgs e)
        {
            try
            {
                string sites = "";
                List<int> selectedScopes = new List<int>();
                for (int i = 0; i < cblSites.CheckedItems.Count; i++)
                {
                  //  Matchnet.Content.ValueObjects.BrandConfig.Site s = (Matchnet.Content.ValueObjects.BrandConfig.Site)cblSites.CheckedItems[i];

                    sites += getCheckedScopeName(i);
                    if(i < cblSites.CheckedItems.Count -1)
                        sites+=", ";

                    selectedScopes.Add(getCheckedScopeID(i));
                }
                if (selectedScopes.Count == 0)
                {
                    MessageBox.Show("You didn't select target to clone settings");
                    return;

                }
                DialogResult result = MessageBox.Show("You are going to update setting value for " + sites + ". Are You Sure? Do you know what you are doing?", "Think again", MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                    return;

                SettingsSave save = new SettingsSave();
                save.Scope = (SettingsScope)Enum.Parse(typeof(SettingsScope), ScopeName, true);
                save.SettingConstant = SettingConstant;
                save.SettingValue = SettingValue;
                save.ScopeIDList = selectedScopes;
                

                List<SettingsSave> list = new List<SettingsSave>();
                list.Add(save);
                ManagementSA.Instance.URI = URI;
                ManagementSA.Instance.SaveSettings(list,chkAsync.Checked);
                ((SettingsForm)this.Owner).Reload();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " " + ex.Message);
            }
        }

        private void btnGetSQL_Click(object sender, EventArgs e)
        {
            try
            {
                string prefix = "declare @settingid int\r\ndeclare @sitesettingid int";

                prefix+= String.Format("\r\ndelete from sitesetting where settingid in (select settingid from setting where settingconstant='{0}')",SettingConstant);
                prefix+= String.Format("\r\nselect @settingid=settingid from setting where settingconstant='{0}')", SettingConstant);
               // prefix+= "\r\nselect @sitesettingid=max(sitesettingid) + 1 from sitesetting";


                string sql = "";


                Hashtable custom = new Hashtable();
                custom["settingid"] = "@settingid";
                custom["sitesettingid"] = "@sitesettingid";
                custom["createdate"] = "getdate()";
                SiteSetting ss = new SiteSetting();
                List<int> selectedScopes = new List<int>();
                for (int i = 0; i < cblSites.CheckedItems.Count; i++)
                {

                    Matchnet.Content.ValueObjects.BrandConfig.Site s = (Matchnet.Content.ValueObjects.BrandConfig.Site)cblSites.CheckedItems[i];


                    selectedScopes.Add(s.SiteID);
                }
                if (selectedScopes.Count == 0)
                {
                    MessageBox.Show("You didn't select any Site to clone settings");
                    return;

                }

                for (int i = 0; i < selectedScopes.Count; i++)
                {
                    SiteSetting sitesetting = new SiteSetting();
                    sitesetting.SiteSettingValue = txtSettingValue.Text;
                    sitesetting.SiteID = selectedScopes[i];
                    sql += "\r\nselect @sitesettingid=max(sitesettingid) + 1 from sitesetting";
                    sql += "\r\n" + Utils.ToSQL<SiteSetting>(sitesetting, "SiteSetting", custom);
                 


                }
                Preview frmPreview = new Preview();
                frmPreview.Text = prefix + sql;
                frmPreview.Show();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message + Utils.GetInnerExcepion(ex)); }
        }


        private int getCheckedScopeID(int idx)
        {
            if (ScopeName.ToLower() == "site")
            {

                Matchnet.Content.ValueObjects.BrandConfig.Site s = (Matchnet.Content.ValueObjects.BrandConfig.Site)cblSites.CheckedItems[idx];
                return s.SiteID;

            }
            else if (ScopeName.ToLower() == "community")
            {

                Matchnet.Content.ValueObjects.BrandConfig.Community s = (Matchnet.Content.ValueObjects.BrandConfig.Community)cblSites.CheckedItems[idx];
                return s.CommunityID;

            }
            else if (ScopeName.ToLower() == "brand")
            {

                Matchnet.Content.ValueObjects.BrandConfig.Brand s = (Matchnet.Content.ValueObjects.BrandConfig.Brand)cblSites.CheckedItems[idx];
                return s.BrandID;

            }
            else if (ScopeName.ToLower() == "server")
            {
                Server s = (Server)cblSites.CheckedItems[idx];
                return s.ServerID;
            }
            return 0;

        }

        private string getCheckedScopeName(int idx)
        {
            if (ScopeName.ToLower() == "site")
            {

                Matchnet.Content.ValueObjects.BrandConfig.Site s = (Matchnet.Content.ValueObjects.BrandConfig.Site)cblSites.CheckedItems[idx];
                return s.Name;

            }
            else if (ScopeName.ToLower() == "community")
            {

                Matchnet.Content.ValueObjects.BrandConfig.Community s = (Matchnet.Content.ValueObjects.BrandConfig.Community)cblSites.CheckedItems[idx];
                return s.Name;

            }
            else if (ScopeName.ToLower() == "brand")
            {

                Matchnet.Content.ValueObjects.BrandConfig.Brand s = (Matchnet.Content.ValueObjects.BrandConfig.Brand)cblSites.CheckedItems[idx];
                return s.Uri;

            }
            else if (ScopeName.ToLower() == "server")
            {
                Server s = (Server)cblSites.CheckedItems[idx];
                return s.Name;
            }
            return null;

        }
    }
}
