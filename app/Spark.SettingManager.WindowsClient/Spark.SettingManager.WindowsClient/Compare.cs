﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.Management;

using Spark.SettingManager.BusinellLogic;
using Spark.SettingManager.ValueObjects;

namespace Spark.SettingManager.WindowsClient
{
    public partial class Compare : Form
    {
        Settings _settings1 = null;
        Settings _settings2 = null;
        List<Setting> _missingSetting1 = null;
        List<Setting> _missingSetting2 = null;

        List<Setting> _filtered1 = null;
        List<Setting> _filtered2 = null;

        List<SiteSettingExpanded> _sitesetting1 = null;
        List<SiteSettingExpanded> _sitesetting2 = null;

        List<SiteSettingExpanded> _sitesettingfiltered1 = null;
        List<SiteSettingExpanded> _sitesettingfiltered2 = null;
        public Compare()
        {
            InitializeComponent();
            loadEnv(cboEnv1);
            loadEnv(cboEnv2);
            LoadSites(cboSite1);
            LoadSites(cboSite2);
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void loadEnv(ComboBox cboEnv)
        {
            try
            {

                cboEnv.Items.Clear();
                for (int i = 0; i < EnvironmentBL.Instance.Environments.Count; i++)
                {
                    cboEnv.Items.Add(EnvironmentBL.Instance.Environments[i].Code);
                    cboEnv.DisplayMember = EnvironmentBL.Instance.Environments[i].Code;

                }
                cboEnv.SelectedIndex = 0;


            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

        }

        private void LoadSites(ComboBox cblSites)
        {
            Matchnet.Content.ValueObjects.BrandConfig.Sites sites = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetSites();
            foreach (Matchnet.Content.ValueObjects.BrandConfig.Site s in sites)
            {
                cblSites.Items.Add(s);
                cblSites.DisplayMember = "Name";
            }
          



        }
        private void btnCompare_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                dgvMissing1.Columns.Clear();
                dgvMissing2.Columns.Clear();
                lblMissedCount1.Text = "0";
                lblMissedCount2.Text = "0";
                _settings1 = null;
                ManagementSA.Instance.URI = EnvironmentBL.Instance.Environments[cboEnv1.SelectedIndex].ManagementServiceURI;
                _settings1 = ManagementSA.Instance.GetSettings();

                _settings2 = null;
                ManagementSA.Instance.URI = EnvironmentBL.Instance.Environments[cboEnv2.SelectedIndex].ManagementServiceURI;
                _settings2 = ManagementSA.Instance.GetSettings();
                if (_missingSetting1 != null)
                {
                    _missingSetting1.Clear();
                    _missingSetting1 = null;
                }
                if (_missingSetting2 != null)
                {
                    _missingSetting2.Clear();
                    _missingSetting2 = null;
                }
               // dgvMissing1.DataSource = SettingsBL.Instance.GetMissingSettings(_settings2.SettingList, _settings1.SettingList);
                _missingSetting1=SettingsBL.Instance.GetMissingSettings(_settings2.SettingList, _settings1.SettingList);
                _missingSetting2=SettingsBL.Instance.GetMissingSettings(_settings1.SettingList, _settings2.SettingList);
                Utils.populateGrid<Setting>(dgvMissing1, true,_missingSetting1 );
                Utils.populateGrid<Setting>(dgvMissing2, true, _missingSetting2);

                lblMissedCount2.Text = dgvMissing2.Rows.Count.ToString();
                lblMissedCount1.Text = dgvMissing1.Rows.Count.ToString();
                this.Cursor = Cursors.Default;

            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;

                MessageBox.Show(ex.Message); }
        }

        private void btnGetSQL2_Click(object sender, EventArgs e)
        {
            try
            {
                string prefix = "declare @settingid int\r\n";
                string prefix2 = "\r\n\r\nselect @settingid=max(settingid)+1 from setting";
                string prefix3 = "\r\ndelete from sitesetting where settingid in (select settingid from setting where settingconstant='{0}')";
                prefix3 += "\r\ndelete from communitysetting where settingid in (select settingid from setting where settingconstant='{0}')";
                prefix3 += "\r\ndelete from brandsetting where settingid in (select settingid from setting where settingconstant='{0}')";
                string prefix4 = "\r\ndelete from setting where settingconstant='{0}'";
               
                string sql = "";

                List<Setting> missing =( List<Setting>)dgvMissing2.DataSource; ;
                Hashtable custom = new Hashtable();
                custom["settingid"]="@settingid";
                custom["createdate"]="getdate()";
                List<Setting> list = _filtered2 == null ? _missingSetting2 : _filtered2;
                for (int i = 0; i < list.Count;i++ )
                {
                    if(dgvMissing2.Rows[i].Cells[0].Value != null && Boolean.Parse(dgvMissing2.Rows[i].Cells[0].Value.ToString()))
                    {
                     Setting  s = list[i];
                    sql += prefix2;
                    sql += string.Format(prefix3, s.SettingConstant);
                    sql += string.Format(prefix4, s.SettingConstant);
                    sql += "\r\n" + Utils.ToSQL<Setting>(s, "Setting", custom);
                    if (chkIncludeSiteSettings2.Checked)
                    {
                        sql += Utils.GetMissingCommunitySQL(s, _settings1.CommunitySettingList);
                        sql += Utils.GetMissingSiteSQL(s, _settings1.SiteSettingList);
                        sql += Utils.GetMissingBrandSQL(s, _settings1.BrandSettingList);

                    }
                    }
                  

                }
                Preview frmPreview = new Preview();
                frmPreview.Text = prefix + sql;
                frmPreview.Show();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }

        private void btnSearch2_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtFilter2.Text))
            {
                dgvMissing2.Columns.Clear();
                _filtered2 = SettingsBL.Instance.Search(txtFilter2.Text, _missingSetting2);
                Utils.populateGrid(dgvMissing2, true, _filtered2);
            }
            else
            {
                dgvMissing2.Columns.Clear();
                Utils.populateGrid(dgvMissing2, true, _missingSetting2);
                _filtered2 = null;
              
            }
        }

        private void btnSearch1_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtFilter1.Text))
            {
                dgvMissing1.Columns.Clear();
                _filtered1 = SettingsBL.Instance.Search(txtFilter1.Text, _missingSetting1);
                Utils.populateGrid(dgvMissing1, true, _filtered1);
            }
            else
            {
                dgvMissing1.Columns.Clear();
                Utils.populateGrid(dgvMissing1, true, _missingSetting1);
                _filtered1 = null;

            }
        }

        private void btnClearFilter_Click(object sender, EventArgs e)
        {
            txtFilter2.Text = "";
            btnSearch2_Click(sender, e);
        }

        private void dgvMissing2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
               

            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }

      

        private void btnSelect2_Click(object sender, EventArgs e)
        {
            Utils.ToggleRows(dgvMissing2, 0);
            return;
        }

       
        private void btnClearFilter1_Click(object sender, EventArgs e)
        {
            txtFilter1.Text = "";
            btnSearch1_Click(sender, e);
        }

        private void btnSelect1_Click(object sender, EventArgs e)
        {
            Utils.ToggleRows(dgvMissing1, 0);
            return;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string prefix = "declare @settingid int\r\n";
                if (chkIncludeSiteSettings1.Checked)
                {
                    prefix += "declare @sitesettingid int\r\ndeclare @communitysettingid int\r\ndeclare @brandsettingid int\r\n";
                }
                string prefix2 = "\r\n\r\nselect @settingid=max(settingid)+1 from setting";
                string prefix3 = "\r\ndelete from sitesetting where settingid in (select settingid from setting where settingconstant='{0}')";
                prefix3 += "\r\ndelete from communitysetting where settingid in (select settingid from setting where settingconstant='{0}')";
                prefix3 += "\r\ndelete from brandsetting where settingid in (select settingid from setting where settingconstant='{0}')";
                string prefix4 = "\r\ndelete from setting where settingconstant='{0}'";

                string sql = "";

                List<Setting> missing = (List<Setting>)dgvMissing1.DataSource; ;
                Hashtable custom = new Hashtable();
                custom["settingid"] = "@settingid";
                custom["createdate"] = "getdate()";
                List<Setting> list = _filtered1 == null ? _missingSetting1 : _filtered1;
                for (int i = 0; i < list.Count; i++)
                {
                    if (dgvMissing1.Rows[i].Cells[0].Value != null && Boolean.Parse(dgvMissing1.Rows[i].Cells[0].Value.ToString()))
                    {
                        Setting s = list[i];
                        sql += prefix2;
                        sql += string.Format(prefix3, s.SettingConstant);
                        sql += string.Format(prefix4, s.SettingConstant);
                        sql += "\r\n" + Utils.ToSQL<Setting>(s, "Setting", custom);
                        if (chkIncludeSiteSettings1.Checked)
                        {
                            sql +=Utils.GetMissingCommunitySQL(s, _settings2.CommunitySettingList);
                            sql += Utils.GetMissingSiteSQL(s, _settings2.SiteSettingList);
                            sql += Utils.GetMissingBrandSQL(s, _settings2.BrandSettingList);

                        }
                    }


                }
                Preview frmPreview = new Preview();
                frmPreview.Text = prefix + sql;
                frmPreview.Show();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }

        private void btnGetVerifySQL1_Click(object sender, EventArgs e)
        {
            try
            {
              
                string sql = "";

                List<Setting> missing = (List<Setting>)dgvMissing1.DataSource; ;
              
                List<Setting> list = _filtered1 == null ? _missingSetting1 : _filtered1;
                for (int i = 0; i < list.Count; i++)
                {
                    if (dgvMissing1.Rows[i].Cells[0].Value != null && Boolean.Parse(dgvMissing1.Rows[i].Cells[0].Value.ToString()))
                    {
                        Setting s = list[i];
                        sql +="'" +  s.SettingConstant + "'";
                        if (i < list.Count - 1)
                        {
                            sql += ",";
                        }
                    }


                }
                Preview frmPreview = new Preview();
                frmPreview.Text = "select * from Setting where SettingConstant in (" + sql + ")";
                frmPreview.Show();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }

        private void dgvMissingSiteSetting2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Compare_Load(object sender, EventArgs e)
        {

        }

        private void btnCompareSites_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (cboSite1.SelectedItem == null || cboSite2.SelectedItem == null)
                {
                    MessageBox.Show("Please select sites to compare.","User Error");
                    this.Cursor = Cursors.Default;
                    return;
                }
                compareSites();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.ToString());
            }

            

        }

        private List<SiteSettingExpanded> populateSiteSetting(ref Settings settings1, ComboBox cboSite, DataGridView dgv, ComboBox cboEnv)
        {
            List<SiteSettingExpanded> explist = null;
            try
            {
             if (cboSite.SelectedItem == null)
                    return null;
             dgv.Columns.Clear();
            Matchnet.Content.ValueObjects.BrandConfig.Site site1 = (Matchnet.Content.ValueObjects.BrandConfig.Site)cboSite.SelectedItem;

            settings1 = Utils.GetEnvironmentSettings(cboEnv);
            List<SiteSetting> list = Utils.FiterSiteSetting(settings1.SiteSettingList, site1.SiteID);
             explist = SettingsBL.Instance.GetSiteSettingsExt(settings1.SettingList, list);
            List<string> excludeCol = new List<string>();
          
            return explist;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return explist;
            }
        }

        private void btnSiteSearch_Click(object sender, EventArgs e)
        {
            SearchSiteSetting(txtSiteFilter.Text, dgvSiteSetting1, _sitesetting1, _sitesettingfiltered1);
            SearchSiteSetting(txtSiteFilter.Text, dgvSiteSetting2, _sitesetting2, _sitesettingfiltered2);
           
        }

        private void SearchSiteSetting(string filter, DataGridView dgv, List<SiteSettingExpanded> list, List<SiteSettingExpanded> filteredlist)
        {
            if (!String.IsNullOrEmpty(filter))
            {
                dgv.Columns.Clear();
                filteredlist = SettingsBL.Instance.Search(filter, list);
                List<string> excludeCol = new List<string>();
                excludeCol.Add("sitesettingid");
                excludeCol.Add("siteid");
                excludeCol.Add("missingincompare");
                excludeCol.Add("valuedifferincompare");
                Hashtable colors = new Hashtable();
                colors["missingincompare"] = System.Drawing.Color.Red;
                colors["valuedifferincompare"] = System.Drawing.Color.Yellow;
                Utils.populateGrid(dgv, true, filteredlist,excludeCol,colors);
            }
            else
            {
                dgv.Columns.Clear();
                Utils.populateGrid(dgv, true, list);
                filteredlist = null;

            }

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void txtSiteFilter_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnClearSiteFilter_Click(object sender, EventArgs e)
        {

        }

        private void dgvSiteSetting1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnMissingSQL1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (_sitesetting1 == null || _sitesetting2 == null || _settings1==null || _settings2==null)
                    compareSites();

                string sql = getMissingSQL(_sitesetting2, _sitesetting1);
                this.Cursor = Cursors.Default;
                Preview frmPreview = new Preview();
                frmPreview.Text = sql;
                frmPreview.Show();

               
            }
            catch (Exception ex)
            { this.Cursor = Cursors.Default;
            MessageBox.Show(ex.ToString());
            }
        }

        private void compareSites()
        {
            _settings1 = null;
            _settings2 = null;
            _sitesetting1 = populateSiteSetting(ref _settings1, cboSite1, dgvSiteSetting1, cboEnv1);
            _sitesetting2 = populateSiteSetting(ref _settings2, cboSite2, dgvSiteSetting2, cboEnv2);
            SettingsBL.Instance.Compare(_sitesetting1, _sitesetting2);
            SettingsBL.Instance.Compare(_sitesetting2, _sitesetting1);
            List<string> excludeCol = new List<string>();
            excludeCol.Add("sitesettingid");
            excludeCol.Add("siteid");
            excludeCol.Add("missingincompare");
            excludeCol.Add("valuedifferincompare");
            Hashtable colors = new Hashtable();
            colors["missingincompare"] = System.Drawing.Color.Red;
            colors["valuedifferincompare"] = System.Drawing.Color.Yellow;

            Utils.populateGrid<SiteSettingExpanded>(dgvSiteSetting1, true, _sitesetting1, excludeCol, colors);
            dgvSiteSetting1.Columns[0].Width = 60;
            dgvSiteSetting1.Columns[1].Width = 270;
            Utils.populateGrid<SiteSettingExpanded>(dgvSiteSetting2, true, _sitesetting2, excludeCol, colors);
            dgvSiteSetting2.Columns[0].Width = 60;
            dgvSiteSetting2.Columns[1].Width = 270;
            lblTotalSiteSettings1.Text = _sitesetting1.Count.ToString();
            lblTotalSiteSettings2.Text = _sitesetting2.Count.ToString();

            var miss1 = _sitesetting1.FindAll(delegate(SiteSettingExpanded s) { return s.MissingInCompare == true; });
            if (miss1 != null)
            {
                lblMissingSiteSett1.Text = miss1.Count.ToString();
            }

            var miss2 = _sitesetting2.FindAll(delegate(SiteSettingExpanded s) { return s.MissingInCompare == true; });
            if (miss2 != null)
            {
                lblMissingSiteSett2.Text = miss2.Count.ToString();
            }
            miss1.Clear();
            miss1 = _sitesetting1.FindAll(delegate(SiteSettingExpanded s) { return s.ValueDifferInCompare == true; });
            if (miss1 != null)
            {
                lblDiffSiteSett1.Text = miss1.Count.ToString();
            }
            miss2.Clear();
            miss2 = _sitesetting2.FindAll(delegate(SiteSettingExpanded s) { return s.ValueDifferInCompare == true; });
            if (miss2 != null)
            {
                lblDiffSiteSett2.Text = miss2.Count.ToString();
            }
        }

        private string getMissingSQL(List<SiteSettingExpanded> setting1,List<SiteSettingExpanded> setting2)
        {
            string sql = "\r\ndeclare @settingid int\r\ndeclare @sitesettingid int";
            var missing = setting1.FindAll(delegate(SiteSettingExpanded s) { return s.MissingInCompare == true; });
            if (missing == null)
                return sql;


            List<SiteSettingExpanded> misslist = missing.ToList<SiteSettingExpanded>();
            Hashtable customValues = new Hashtable();
            customValues["settingid"] = "@settingid";
            customValues["sitesettingid"] = "@sitesettingid";

            List<string> excludeProps = new List<string>();
            excludeProps.Add("settingconstant");
            excludeProps.Add("missingincompare");
            excludeProps.Add("valuedifferincompare");

            
            foreach (SiteSettingExpanded s in misslist)
            {
                Setting sett=_settings1.SettingList.Find(delegate(Setting s1){return s1.SettingConstant==s.SettingConstant;});
                if(sett==null)
                    continue;
                sql += "\r\n\r\nselect @settingid=settingid from setting where settingconstant='" + sett.SettingConstant + "'";
                sql += "\r\nselect @sitesettingid=max(sitesettingid) + 1 from sitesetting ";
                sql += "\r\n" + Utils.ToSQL<SiteSettingExpanded>(s, "SiteSetting", customValues,excludeProps);
            }
            return sql;
        }

        private string getDifferentValuesSQL(List<SiteSettingExpanded> setting1, int siteid)
        {
            string sql = "\r\ndeclare @settingid int\r\ndeclare @sitesettingid int";
            var missing = setting1.FindAll(delegate(SiteSettingExpanded s) { return s.ValueDifferInCompare == true; });
            if (missing == null)
                return sql;


            List<SiteSettingExpanded> misslist = missing.ToList<SiteSettingExpanded>();
            Hashtable customValues = new Hashtable();
            customValues["settingid"] = "@settingid";
            customValues["sitesettingid"] = "@sitesettingid";

            List<string> excludeProps = new List<string>();
            excludeProps.Add("settingconstant");
            excludeProps.Add("missingincompare");
            excludeProps.Add("valuedifferincompare");


            foreach (SiteSettingExpanded s in misslist)
            {
                Setting sett = _settings1.SettingList.Find(delegate(Setting s1) { return s1.SettingConstant == s.SettingConstant; });
                if (sett == null)
                    continue;
                sql += "\r\n\r\nselect @settingid=settingid from setting where settingconstant='" + sett.SettingConstant + "'";
                sql += String.Format("\r\nupdate SiteSetting set SiteSettingValue='{0}' where siteid={1} and settingid=@settingid",s.SiteSettingValue,siteid);
                
            }
            return sql;
        }

        private void btnMissingSQL2_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (_sitesetting1 == null || _sitesetting2 == null || _settings1 == null || _settings2 == null)
                    compareSites();

                string sql = getMissingSQL(_sitesetting1, _sitesetting2);
                this.Cursor = Cursors.Default;
                Preview frmPreview = new Preview();
                frmPreview.Text = sql;
                frmPreview.Show();


            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnDiffValueSQL1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (_sitesetting1 == null || _sitesetting2 == null || _settings1 == null || _settings2 == null)
                    compareSites();

                Matchnet.Content.ValueObjects.BrandConfig.Site site1 = (Matchnet.Content.ValueObjects.BrandConfig.Site)cboSite1.SelectedItem;
                string sql = getDifferentValuesSQL(_sitesetting2, site1.SiteID);
                this.Cursor = Cursors.Default;
                Preview frmPreview = new Preview();
                frmPreview.Text = sql;
                frmPreview.Show();


            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.ToString());
            }

        }

        private void btnDiffValueSQL2_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (_sitesetting1 == null || _sitesetting2 == null || _settings1 == null || _settings2 == null)
                    compareSites();

                Matchnet.Content.ValueObjects.BrandConfig.Site site1 = (Matchnet.Content.ValueObjects.BrandConfig.Site)cboSite2.SelectedItem;
                string sql = getDifferentValuesSQL(_sitesetting1, site1.SiteID);
                this.Cursor = Cursors.Default;
                Preview frmPreview = new Preview();
                frmPreview.Text = sql;
                frmPreview.Show();


            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
