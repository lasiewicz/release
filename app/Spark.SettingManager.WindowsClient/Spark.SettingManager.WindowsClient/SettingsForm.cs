﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.Management;
using System.Reflection;
using Spark.SettingManager.BusinellLogic;
using Spark.SettingManager.ValueObjects;
namespace Spark.SettingManager.WindowsClient
{
    public partial class SettingsForm : Form
    {

        Settings _settings = null;
        List<Setting> _filteredSettings = null;
        int _settingid;

        string _settingconst="";
        string _desc = "";
        int _serverid;
        
        public SettingsForm()
        {
            InitializeComponent();
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            loadEnv();
        }


        private void loadEnv()
        {
            try
            {

                cboEnv.Items.Clear();
                for (int i = 0; i < EnvironmentBL.Instance.Environments.Count; i++)
                {
                    cboEnv.Items.Add(EnvironmentBL.Instance.Environments[i].Code);
                    cboEnv.DisplayMember = EnvironmentBL.Instance.Environments[i].Code;

                }
                cboEnv.SelectedIndex = 0;


            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message + Utils.GetInnerExcepion(ex)); }

        }

        private void cboEnv_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblURI.Text = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ManagementServiceURI;
        }

        private void btnGetGlobalSettings_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                _settings = null;
                ManagementSA.Instance.URI = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ManagementServiceURI;
                _settings = ManagementSA.Instance.GetSettings();

                dgvGlobalSetting.Columns.Clear();

                Utils.populateGrid<Setting>(dgvGlobalSetting, true, _settings.SettingList);

                ResizeGrid(dgvGlobalSetting, ((int)Utils.SettingsColumns.SettingConstant), 250);
                ResizeGrid(dgvGlobalSetting, ((int)Utils.SettingsColumns.SettingDescription), 350);
                this.Cursor = Cursors.Default;

            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message + Utils.GetInnerExcepion(ex)); }
        }

        private void dgvGlobalSetting_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex == -1 || e.ColumnIndex !=1)
                return;

            try
            {

                string settingid = dgvGlobalSetting.Rows[e.RowIndex].Cells[Utils.SettingsColumns.SettingID.ToString()].Value.ToString();

                _settingid = Int32.Parse(settingid);

                _settingconst = dgvGlobalSetting.Rows[e.RowIndex].Cells[Utils.SettingsColumns.SettingConstant.ToString()].Value.ToString();
                _desc = dgvGlobalSetting.Rows[e.RowIndex].Cells[Utils.SettingsColumns.SettingDescription.ToString()].Value.ToString();

                showScopeSettings(_settingid, _settingconst, _desc);

                //  MessageBox.Show(settingconstant);
            }
            catch (Exception ex) { }

        }

        private void dgvGlobalSetting_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                foreach (DataGridViewRow r in dgvGlobalSetting.Rows)
                {
                    r.Cells[0].Value = !Boolean.Parse(r.Cells[0].Value.ToString());

                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (_settings == null)
                {
                    ManagementSA.Instance.URI = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ManagementServiceURI;
                    _settings = ManagementSA.Instance.GetSettings();
                }
                
                if (!String.IsNullOrEmpty(txtFilter.Text))
                {
                    _filteredSettings = SettingsBL.Instance.Search(txtFilter.Text, _settings.SettingList);
                }
                
                dgvGlobalSetting.Columns.Clear();
                Utils.populateGrid<Setting>(dgvGlobalSetting,true, _filteredSettings);
                ResizeGrid(dgvGlobalSetting, ((int)Utils.SettingsColumns.SettingConstant), 250);
                ResizeGrid(dgvGlobalSetting, ((int)Utils.SettingsColumns.SettingDescription), 350);

            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message + Utils.GetInnerExcepion(ex)); }
        }

        private void dgvSiteSetting_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
           try{
               if (e.RowIndex == -1 && e.ColumnIndex == 0)
                   return;
               string settingid = dgvSiteSetting.Rows[e.RowIndex].Cells[Utils.SiteSettingsColumns.SettingID.ToString()].Value.ToString();
               string siteid= dgvSiteSetting.Rows[e.RowIndex].Cells[Utils.SiteSettingsColumns.SiteID.ToString()].Value.ToString();
               string val= dgvSiteSetting.Rows[e.RowIndex].Cells[Utils.SiteSettingsColumns.SiteSettingValue.ToString()].Value.ToString();

               Setting s = SettingsBL.Instance.Find(Int32.Parse(settingid), _settings.SettingList);
               


               EditSetting editform = new EditSetting();
               editform.URI = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ManagementServiceURI;
               editform.SettingID = Int32.Parse(settingid);
               editform.Scope = Utils.Scope.Site.ToString();
               editform.ScopeID = siteid;
               editform.ScopeName = Utils.Scope.Site.ToString();
               editform.SettingConstant = s.SettingConstant;
               editform.SettingDescription = s.SettingDescription;
               editform.GlobalDefaultValue = s.GlobalDefaultValue;
               editform.SettingValue = val;
               editform.Show(this);

           
           }catch(Exception ex)
           {
               MessageBox.Show(ex.Message + Utils.GetInnerExcepion(ex));
           }
        }

        private void ResizeGrid(DataGridView dgv, int colid, int width)
        {
            dgv.Columns[colid].Width = width;

        }

     

   
        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnGetServers_Click(object sender, EventArgs e)
        {
            try
            {
                ManagementSA.Instance.URI = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ManagementServiceURI;
                List<Server> serverlist = ManagementSA.Instance.GetServers();
                Utils.populateGrid<Server>(dgvServers, false, serverlist);
                ResizeGrid(dgvServers, 1, 180);
                tabControl1.SelectedIndex = 3;



            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message + Utils.GetInnerExcepion(ex)); }
        }

        private void dgvServers_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == -1 || e.ColumnIndex == 0)
                    return;
                
                string serverid = dgvServers.Rows[e.RowIndex].Cells[0].Value.ToString();
               
                List<Server> serverlist = ManagementSA.Instance.GetServers();
                Server server = serverlist.Find(delegate(Server s) { return s.ServerID == Int32.Parse(serverid); });
                _serverid = server.ServerID;
                populateServerSettings(_serverid);
                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + Utils.GetInnerExcepion(ex));
            }

        }

        private void dgvServerSetting_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == -1 && e.ColumnIndex == 0)
                    return;
                string settingid = dgvServerSetting.Rows[e.RowIndex].Cells[Utils.ServerSettingsColumns.SettingID.ToString()].Value.ToString();
                string serverid = dgvServerSetting.Rows[e.RowIndex].Cells[Utils.ServerSettingsColumns.ServerID.ToString()].Value.ToString();
                string val = dgvServerSetting.Rows[e.RowIndex].Cells[Utils.ServerSettingsColumns.ServerSettingValue.ToString()].Value.ToString();
              
                 ManagementSA.Instance.URI = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ManagementServiceURI;
                List<Server> serverlist = ManagementSA.Instance.GetServers();
                Server server = serverlist.Find(delegate(Server s) { return s.ServerID == Int32.Parse(serverid); });
                Setting setting = SettingsBL.Instance.Find(Int32.Parse(settingid), _settings.SettingList);



                EditSetting editform = new EditSetting();

                editform.SettingID = Int32.Parse(settingid);
                editform.Scope = Utils.Scope.Server.ToString();
                editform.ScopeID = serverid;
                editform.ScopeName = Utils.Scope.Server.ToString(); ;
                editform.SettingConstant = setting.SettingConstant;
                editform.SettingDescription = setting.SettingDescription;
                editform.GlobalDefaultValue = setting.GlobalDefaultValue;
                editform.ServerName = server.Name;
                editform.SettingValue = val;
                editform.URI = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ManagementServiceURI;
                editform.Show(this);
                editform.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + Utils.GetInnerExcepion(ex));
            }
        }

        private void btnCompare_Click(object sender, EventArgs e)
        {
            Compare frmCompare = new Compare();
            frmCompare.Show();
        }

        private void txtFilter_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dgvGlobalSetting_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnSelect1_Click(object sender, EventArgs e)
        {
            Utils.ToggleRows(dgvGlobalSetting, 0);
            return;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                string prefix = "declare @settingid int\r\n";
                string prefix2 = "\r\n\r\nselect @settingid=max(settingid)+1 from setting";
                string prefix3 = "\r\ndelete from sitesetting where settingid in (select settingid from setting where settingconstant='{0}')";
                prefix3 += "\r\ndelete from communitysetting where settingid in (select settingid from setting where settingconstant='{0}')";
                prefix3 += "\r\ndelete from brandsetting where settingid in (select settingid from setting where settingconstant='{0}')";
                string prefix4 = "\r\ndelete from setting where settingconstant='{0}'";

                string sql = "";
                if (dgvGlobalSetting.DataSource == null)
                {
                    MessageBox.Show("You should retrieve settings first. Press \"Get Global Settings\"  big button.");
                    return;
                }
                List<Setting> missing = (List<Setting>)dgvGlobalSetting.DataSource; ;
                Hashtable custom = new Hashtable();
                custom["settingid"] = "@settingid";
                custom["createdate"] = "getdate()";
                List<Setting> list = _filteredSettings == null ? _settings.SettingList : _filteredSettings;
                for (int i = 0; i < list.Count; i++)
                {
                    if (dgvGlobalSetting.Rows[i].Cells[0].Value != null && Boolean.Parse(dgvGlobalSetting.Rows[i].Cells[0].Value.ToString()))
                    {
                        Setting s = list[i];
                        sql += prefix2;
                        sql += string.Format(prefix3, s.SettingConstant);
                        sql += string.Format(prefix4, s.SettingConstant);
                        sql += "\r\n" + Utils.ToSQL<Setting>(s, "Setting", custom);
                        if (chkIncludeSiteSettings.Checked)
                        {
                            sql += Utils.GetMissingCommunitySQL(s, _settings.CommunitySettingList);
                            sql += Utils.GetMissingSiteSQL(s, _settings.SiteSettingList);
                            sql += Utils.GetMissingBrandSQL(s, _settings.BrandSettingList);

                        }
                    }


                }
                Preview frmPreview = new Preview();
                frmPreview.Text = prefix + sql;
                frmPreview.Show();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message + Utils.GetInnerExcepion(ex)); }
        }

        private void btnAddSiteSetting_Click(object sender, EventArgs e)
        {

            EditSetting editform = new EditSetting();
            editform.URI = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ManagementServiceURI;
            string settingconst = lblConstant.Text;
            if (String.IsNullOrEmpty(settingconst))
            {
                MessageBox.Show("Please select setting first by clicking on SettingID column in the globalsetting grid on the first tab");
                return;
            }
            Setting setting = SettingsBL.Instance.GetSetting(_settings.SettingList, settingconst);
            editform.SettingID =setting.SettingID;
            editform.Scope = Utils.Scope.Site.ToString();
           // editform.ScopeID = siteid;
            editform.ScopeName = Utils.Scope.Site.ToString();
            editform.SettingConstant = setting.SettingConstant;
            editform.SettingDescription = setting.SettingDescription;
            editform.GlobalDefaultValue = setting.GlobalDefaultValue;
            editform.SettingValue = setting.GlobalDefaultValue; ;
            editform.Action = "add";
            editform.Show(this);
          
        }

        private void showScopeSettings(int settingid, string settingconst,string desc)
        {
            Settings s = SettingsBL.Instance.Search(_settings, settingid);
            tabControl1.SelectedIndex = 1;

            lblConstant.Text = settingconst;
            lblDesc.Text = desc;
            dgvSiteSetting.Columns.Clear();
            Utils.populateGrid<SiteSetting>(dgvSiteSetting, false, s.SiteSettingList);


            dgvCommunitySetting.Columns.Clear();
            dgvCommunitySetting.DataSource = s.CommunitySettingList;

            lblSettingConstant.Columns.Clear();
            lblSettingConstant.DataSource = s.BrandSettingList;

            dgvServerSetting.Columns.Clear();
            dgvServerSetting.DataSource = s.ServerSettingList;
        }

        public void Reload()
        {
            btnGetGlobalSettings_Click(null, null);
            showScopeSettings(_settingid, _settingconst, _desc);
        }

        public void ReloadServerSetting()
        {
            btnGetGlobalSettings_Click(null, null);
            showScopeSettings(_settingid, _settingconst, _desc);
            populateServerSettings(_serverid);
            tabControl1.SelectedIndex = 2;
        }

        private void btnAddServerSetting_Click(object sender, EventArgs e)
        {

            EditSetting editform = new EditSetting();
            editform.URI = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ManagementServiceURI;
            string settingconst = lblConstant.Text;
            if (String.IsNullOrEmpty(settingconst))
            {
                MessageBox.Show("Please select setting first by clicking on SettingID column in the globalsetting grid on the first tab");
                return;
            }
            Setting setting = SettingsBL.Instance.GetSetting(_settings.SettingList, settingconst);
            editform.SettingID = setting.SettingID;
            editform.Scope = Utils.Scope.Server.ToString();
            // editform.ScopeID = siteid;
            editform.ScopeName = Utils.Scope.Server.ToString();
            editform.SettingConstant = setting.SettingConstant;
            editform.SettingDescription = setting.SettingDescription;
            editform.GlobalDefaultValue = setting.GlobalDefaultValue;
            editform.SettingValue = setting.GlobalDefaultValue;
            editform.Action = "add";
            editform.Show(this);
        }

        private void dgvCommunitySetting_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == -1 && e.ColumnIndex == 0)
                    return;
                string settingid = dgvCommunitySetting.Rows[e.RowIndex].Cells[Utils.CommunitySettingsColumns.SettingID.ToString()].Value.ToString();
                string communityid = dgvCommunitySetting.Rows[e.RowIndex].Cells[Utils.CommunitySettingsColumns.CommunityID.ToString()].Value.ToString();
                string val = dgvCommunitySetting.Rows[e.RowIndex].Cells[Utils.CommunitySettingsColumns.CommunitySettingValue.ToString()].Value.ToString();

                ManagementSA.Instance.URI = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ManagementServiceURI;
               
                Setting setting = SettingsBL.Instance.Find(Int32.Parse(settingid), _settings.SettingList);


                EditSetting editform = new EditSetting();

                editform.SettingID = Int32.Parse(settingid);
                editform.Scope = Utils.Scope.Community.ToString();
                editform.ScopeID = communityid;
                editform.ScopeName = Utils.Scope.Community.ToString(); ;
                editform.SettingConstant = setting.SettingConstant;
                editform.SettingDescription = setting.SettingDescription;
                editform.GlobalDefaultValue = setting.GlobalDefaultValue;
              
                editform.SettingValue = val;
                editform.URI = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ManagementServiceURI;
                editform.Show(this);
                editform.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + Utils.GetInnerExcepion(ex));
            }
        }

        private void dgvServerSetting1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == -1 && e.ColumnIndex == 0)
                    return;
                string settingid = dgvServerSetting1.Rows[e.RowIndex].Cells[1].Value.ToString();

                string val = dgvServerSetting1.Rows[e.RowIndex].Cells[5].Value.ToString();

                ManagementSA.Instance.URI = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ManagementServiceURI;

                Setting setting = SettingsBL.Instance.Find(Int32.Parse(settingid), _settings.SettingList);
                ServerSetting ss = SettingsBL.Instance.GetServerSettings(setting, _settings.ServerSettingList).Find(delegate(ServerSetting s) { return s.ServerID == _serverid; });
                List<Server> serverlist = ManagementSA.Instance.GetServers();
                Server server = serverlist.Find(delegate(Server s) { return s.ServerID == ss.ServerID; });
                EditSetting editform = new EditSetting();
                editform.ServerName = server.Name;
                editform.SettingID = Int32.Parse(settingid);
                editform.Scope = Utils.Scope.Server.ToString();
                editform.ScopeID = _serverid.ToString();
                editform.ScopeName = Utils.Scope.Server.ToString(); ;
                editform.SettingConstant = setting.SettingConstant;
                editform.SettingDescription = setting.SettingDescription;
                editform.GlobalDefaultValue = setting.GlobalDefaultValue;
                editform.ServerName=
                editform.SettingValue = val;
                editform.URI = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ManagementServiceURI;
                editform.Action = "serversetting";
                editform.ServerSettings = ss;
                editform.Show(this);
                editform.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + Utils.GetInnerExcepion(ex));
            }
        }

        private void populateServerSettings(int serverid)
        {

            //List<Server> serverlist = ManagementSA.Instance.GetServers();
            //Server server = serverlist.Find(delegate(Server s) { return s.ServerID == Int32.Parse(serverid); });
            //_serverid = server.ServerID;
            List<ServerSetting> serversetting = _settings.ServerSettingList.FindAll(delegate(ServerSetting s) { return s.ServerID == serverid; });
            List<ServerSettingExpanded> list = SettingsBL.Instance.GetServerSettingsExt(_settings.SettingList, serversetting);
            List<string> exclude = new List<string>();
            exclude.Add("servername");
            exclude.Add("serverid");
            exclude.Add("serversettingid");
            dgvServerSetting1.Columns.Clear();
            Utils.populateGrid<ServerSettingExpanded>(dgvServerSetting1, false, list, exclude, null);
            ResizeGrid(dgvServerSetting1, 0, 230);
            ResizeGrid(dgvServerSetting1, 1, 50);
            ResizeGrid(dgvServerSetting1, 2, 50);
            ResizeGrid(dgvServerSetting1, 3, 50);
            ResizeGrid(dgvServerSetting1, 4, 50);
            ResizeGrid(dgvServerSetting1, 5, 250);
        }
    }
}
