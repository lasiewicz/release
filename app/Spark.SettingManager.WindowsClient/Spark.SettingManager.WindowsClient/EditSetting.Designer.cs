﻿namespace Spark.SettingManager.WindowsClient
{
    partial class EditSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSettingConstant = new System.Windows.Forms.Label();
            this.lblSettingDescription = new System.Windows.Forms.Label();
            this.lblSettingScope = new System.Windows.Forms.Label();
            this.lblSettingGlobalValue = new System.Windows.Forms.Label();
            this.txtSettingValue = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblSettingScopeID = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cblSites = new System.Windows.Forms.CheckedListBox();
            this.btnClone = new System.Windows.Forms.Button();
            this.btnGetSQL = new System.Windows.Forms.Button();
            this.chkAsync = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlScopes = new System.Windows.Forms.Panel();
            this.lbxCommunityID = new System.Windows.Forms.ListBox();
            this.lbxSiteID = new System.Windows.Forms.ListBox();
            this.lbxBrandID = new System.Windows.Forms.ListBox();
            this.panel1.SuspendLayout();
            this.pnlScopes.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSettingConstant
            // 
            this.lblSettingConstant.AutoSize = true;
            this.lblSettingConstant.Location = new System.Drawing.Point(4, 4);
            this.lblSettingConstant.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSettingConstant.Name = "lblSettingConstant";
            this.lblSettingConstant.Size = new System.Drawing.Size(112, 15);
            this.lblSettingConstant.TabIndex = 0;
            this.lblSettingConstant.Text = "Setting Constant";
            // 
            // lblSettingDescription
            // 
            this.lblSettingDescription.AutoSize = true;
            this.lblSettingDescription.Location = new System.Drawing.Point(6, 37);
            this.lblSettingDescription.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSettingDescription.Name = "lblSettingDescription";
            this.lblSettingDescription.Size = new System.Drawing.Size(129, 15);
            this.lblSettingDescription.TabIndex = 1;
            this.lblSettingDescription.Text = "Setting Description";
            // 
            // lblSettingScope
            // 
            this.lblSettingScope.AutoSize = true;
            this.lblSettingScope.Location = new System.Drawing.Point(156, 115);
            this.lblSettingScope.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSettingScope.Name = "lblSettingScope";
            this.lblSettingScope.Size = new System.Drawing.Size(96, 15);
            this.lblSettingScope.TabIndex = 2;
            this.lblSettingScope.Text = "Setting Scope";
            // 
            // lblSettingGlobalValue
            // 
            this.lblSettingGlobalValue.AutoSize = true;
            this.lblSettingGlobalValue.Location = new System.Drawing.Point(156, 79);
            this.lblSettingGlobalValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSettingGlobalValue.Name = "lblSettingGlobalValue";
            this.lblSettingGlobalValue.Size = new System.Drawing.Size(139, 15);
            this.lblSettingGlobalValue.TabIndex = 3;
            this.lblSettingGlobalValue.Text = "Global Default Value";
            // 
            // txtSettingValue
            // 
            this.txtSettingValue.Location = new System.Drawing.Point(22, 387);
            this.txtSettingValue.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtSettingValue.Name = "txtSettingValue";
            this.txtSettingValue.Size = new System.Drawing.Size(865, 21);
            this.txtSettingValue.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 358);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "Value:";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(22, 459);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(135, 39);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(201, 459);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(135, 39);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(754, 459);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(135, 39);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblSettingScopeID
            // 
            this.lblSettingScopeID.AutoSize = true;
            this.lblSettingScopeID.Location = new System.Drawing.Point(156, 145);
            this.lblSettingScopeID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSettingScopeID.Name = "lblSettingScopeID";
            this.lblSettingScopeID.Size = new System.Drawing.Size(114, 15);
            this.lblSettingScopeID.TabIndex = 9;
            this.lblSettingScopeID.Text = "Setting Scope ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 145);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 15);
            this.label2.TabIndex = 10;
            this.label2.Text = "Setting Scope ID:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 79);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 15);
            this.label3.TabIndex = 11;
            this.label3.Text = "Global Default Value:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 115);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 15);
            this.label4.TabIndex = 12;
            this.label4.Text = " Scope:";
            // 
            // cblSites
            // 
            this.cblSites.FormattingEnabled = true;
            this.cblSites.Location = new System.Drawing.Point(564, 18);
            this.cblSites.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cblSites.Name = "cblSites";
            this.cblSites.Size = new System.Drawing.Size(342, 324);
            this.cblSites.TabIndex = 15;
            // 
            // btnClone
            // 
            this.btnClone.Location = new System.Drawing.Point(356, 459);
            this.btnClone.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnClone.Name = "btnClone";
            this.btnClone.Size = new System.Drawing.Size(167, 39);
            this.btnClone.TabIndex = 16;
            this.btnClone.Text = "Clone to all checked";
            this.btnClone.UseVisualStyleBackColor = true;
            this.btnClone.Click += new System.EventHandler(this.btnClone_Click);
            // 
            // btnGetSQL
            // 
            this.btnGetSQL.Location = new System.Drawing.Point(552, 459);
            this.btnGetSQL.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnGetSQL.Name = "btnGetSQL";
            this.btnGetSQL.Size = new System.Drawing.Size(167, 39);
            this.btnGetSQL.TabIndex = 17;
            this.btnGetSQL.Text = "Get SQL";
            this.btnGetSQL.UseVisualStyleBackColor = true;
            this.btnGetSQL.Click += new System.EventHandler(this.btnGetSQL_Click);
            // 
            // chkAsync
            // 
            this.chkAsync.AutoSize = true;
            this.chkAsync.Location = new System.Drawing.Point(22, 417);
            this.chkAsync.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chkAsync.Name = "chkAsync";
            this.chkAsync.Size = new System.Drawing.Size(227, 19);
            this.chkAsync.TabIndex = 18;
            this.chkAsync.Text = "Execute Asynchronous DB write";
            this.chkAsync.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 514);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(723, 15);
            this.label5.TabIndex = 19;
            this.label5.Text = "*To save setting for multiple Sites/Servers etc , select targets from CheckBoxLis" +
                "t and click \"Clone to all checked\"";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblSettingGlobalValue);
            this.panel1.Controls.Add(this.lblSettingDescription);
            this.panel1.Controls.Add(this.lblSettingConstant);
            this.panel1.Controls.Add(this.lblSettingScope);
            this.panel1.Controls.Add(this.lblSettingScopeID);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(5, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(552, 183);
            this.panel1.TabIndex = 23;
            // 
            // pnlScopes
            // 
            this.pnlScopes.Controls.Add(this.lbxBrandID);
            this.pnlScopes.Controls.Add(this.lbxSiteID);
            this.pnlScopes.Controls.Add(this.lbxCommunityID);
            this.pnlScopes.Location = new System.Drawing.Point(5, 201);
            this.pnlScopes.Name = "pnlScopes";
            this.pnlScopes.Size = new System.Drawing.Size(552, 154);
            this.pnlScopes.TabIndex = 24;
            // 
            // lbxCommunityID
            // 
            this.lbxCommunityID.FormattingEnabled = true;
            this.lbxCommunityID.ItemHeight = 15;
            this.lbxCommunityID.Location = new System.Drawing.Point(11, 3);
            this.lbxCommunityID.Name = "lbxCommunityID";
            this.lbxCommunityID.Size = new System.Drawing.Size(161, 139);
            this.lbxCommunityID.TabIndex = 23;
            // 
            // lbxSiteID
            // 
            this.lbxSiteID.FormattingEnabled = true;
            this.lbxSiteID.ItemHeight = 15;
            this.lbxSiteID.Location = new System.Drawing.Point(196, 3);
            this.lbxSiteID.Name = "lbxSiteID";
            this.lbxSiteID.Size = new System.Drawing.Size(163, 139);
            this.lbxSiteID.TabIndex = 24;
            // 
            // lbxBrandID
            // 
            this.lbxBrandID.FormattingEnabled = true;
            this.lbxBrandID.ItemHeight = 15;
            this.lbxBrandID.Location = new System.Drawing.Point(371, 3);
            this.lbxBrandID.Name = "lbxBrandID";
            this.lbxBrandID.Size = new System.Drawing.Size(178, 139);
            this.lbxBrandID.TabIndex = 25;
            // 
            // EditSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 571);
            this.Controls.Add(this.pnlScopes);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.chkAsync);
            this.Controls.Add(this.btnGetSQL);
            this.Controls.Add(this.btnClone);
            this.Controls.Add(this.cblSites);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSettingValue);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "EditSetting";
            this.Text = "EditSetting";
            this.Load += new System.EventHandler(this.EditSetting_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlScopes.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSettingConstant;
        private System.Windows.Forms.Label lblSettingDescription;
        private System.Windows.Forms.Label lblSettingScope;
        private System.Windows.Forms.Label lblSettingGlobalValue;
        private System.Windows.Forms.TextBox txtSettingValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblSettingScopeID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckedListBox cblSites;
        private System.Windows.Forms.Button btnClone;
        private System.Windows.Forms.Button btnGetSQL;
        private System.Windows.Forms.CheckBox chkAsync;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlScopes;
        private System.Windows.Forms.ListBox lbxBrandID;
        private System.Windows.Forms.ListBox lbxSiteID;
        private System.Windows.Forms.ListBox lbxCommunityID;
    }
}