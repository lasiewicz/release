﻿namespace Spark.SettingManager.WindowsClient
{
    partial class Preview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtbPreview = new System.Windows.Forms.RichTextBox();
            this.lbllTitle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // rtbPreview
            // 
            this.rtbPreview.Location = new System.Drawing.Point(12, 100);
            this.rtbPreview.Name = "rtbPreview";
            this.rtbPreview.Size = new System.Drawing.Size(1166, 724);
            this.rtbPreview.TabIndex = 0;
            this.rtbPreview.Text = "";
            // 
            // lbllTitle
            // 
            this.lbllTitle.AutoSize = true;
            this.lbllTitle.Location = new System.Drawing.Point(28, 23);
            this.lbllTitle.Name = "lbllTitle";
            this.lbllTitle.Size = new System.Drawing.Size(0, 13);
            this.lbllTitle.TabIndex = 1;
            // 
            // Preview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1190, 873);
            this.Controls.Add(this.lbllTitle);
            this.Controls.Add(this.rtbPreview);
            this.Name = "Preview";
            this.Text = "Preview";
            this.Load += new System.EventHandler(this.Preview_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbPreview;
        private System.Windows.Forms.Label lbllTitle;
    }
}