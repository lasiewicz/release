﻿namespace Spark.SettingManager.WindowsClient
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboEnv = new System.Windows.Forms.ComboBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabGlobal = new System.Windows.Forms.TabPage();
            this.chkIncludeSiteSettings = new System.Windows.Forms.CheckBox();
            this.btnSelect1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtFilter = new System.Windows.Forms.TextBox();
            this.dgvGlobalSetting = new System.Windows.Forms.DataGridView();
            this.tabScopes = new System.Windows.Forms.TabPage();
            this.btnAddServerSetting = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnAddSiteSetting = new System.Windows.Forms.Button();
            this.lblDesc = new System.Windows.Forms.Label();
            this.lblConstant = new System.Windows.Forms.Label();
            this.dgvServerSetting = new System.Windows.Forms.DataGridView();
            this.lblSettingConstant = new System.Windows.Forms.DataGridView();
            this.dgvCommunitySetting = new System.Windows.Forms.DataGridView();
            this.dgvSiteSetting = new System.Windows.Forms.DataGridView();
            this.tabServer = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dgvServers = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.btnGetGlobalSettings = new System.Windows.Forms.Button();
            this.lblURI = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGetServers = new System.Windows.Forms.Button();
            this.btnCompare = new System.Windows.Forms.Button();
            this.dgvServerSetting1 = new System.Windows.Forms.DataGridView();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabGlobal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGlobalSetting)).BeginInit();
            this.tabScopes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServerSetting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSettingConstant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommunitySetting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSiteSetting)).BeginInit();
            this.tabServer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServerSetting1)).BeginInit();
            this.SuspendLayout();
            // 
            // cboEnv
            // 
            this.cboEnv.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboEnv.FormattingEnabled = true;
            this.cboEnv.Location = new System.Drawing.Point(133, 14);
            this.cboEnv.Name = "cboEnv";
            this.cboEnv.Size = new System.Drawing.Size(242, 23);
            this.cboEnv.TabIndex = 0;
            this.cboEnv.SelectedIndexChanged += new System.EventHandler(this.cboEnv_SelectedIndexChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabGlobal);
            this.tabControl1.Controls.Add(this.tabScopes);
            this.tabControl1.Controls.Add(this.tabServer);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(27, 109);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1196, 826);
            this.tabControl1.TabIndex = 1;
            // 
            // tabGlobal
            // 
            this.tabGlobal.Controls.Add(this.label10);
            this.tabGlobal.Controls.Add(this.chkIncludeSiteSettings);
            this.tabGlobal.Controls.Add(this.btnSelect1);
            this.tabGlobal.Controls.Add(this.button2);
            this.tabGlobal.Controls.Add(this.label2);
            this.tabGlobal.Controls.Add(this.btnSearch);
            this.tabGlobal.Controls.Add(this.txtFilter);
            this.tabGlobal.Controls.Add(this.dgvGlobalSetting);
            this.tabGlobal.Location = new System.Drawing.Point(4, 25);
            this.tabGlobal.Name = "tabGlobal";
            this.tabGlobal.Padding = new System.Windows.Forms.Padding(3);
            this.tabGlobal.Size = new System.Drawing.Size(1188, 797);
            this.tabGlobal.TabIndex = 0;
            this.tabGlobal.Text = "Global";
            this.tabGlobal.UseVisualStyleBackColor = true;
            // 
            // chkIncludeSiteSettings
            // 
            this.chkIncludeSiteSettings.AutoSize = true;
            this.chkIncludeSiteSettings.Location = new System.Drawing.Point(314, 69);
            this.chkIncludeSiteSettings.Name = "chkIncludeSiteSettings";
            this.chkIncludeSiteSettings.Size = new System.Drawing.Size(275, 20);
            this.chkIncludeSiteSettings.TabIndex = 22;
            this.chkIncludeSiteSettings.Text = "Include Community/Site etc. settings";
            this.chkIncludeSiteSettings.UseVisualStyleBackColor = true;
            // 
            // btnSelect1
            // 
            this.btnSelect1.Location = new System.Drawing.Point(24, 65);
            this.btnSelect1.Name = "btnSelect1";
            this.btnSelect1.Size = new System.Drawing.Size(153, 24);
            this.btnSelect1.TabIndex = 21;
            this.btnSelect1.Text = "Select/UnSelect";
            this.btnSelect1.UseVisualStyleBackColor = true;
            this.btnSelect1.Click += new System.EventHandler(this.btnSelect1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(195, 67);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(108, 22);
            this.button2.TabIndex = 20;
            this.button2.Text = "Get SQL";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Filter:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(327, 22);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(92, 24);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtFilter
            // 
            this.txtFilter.Location = new System.Drawing.Point(61, 26);
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.Size = new System.Drawing.Size(242, 22);
            this.txtFilter.TabIndex = 1;
            this.txtFilter.TextChanged += new System.EventHandler(this.txtFilter_TextChanged);
            // 
            // dgvGlobalSetting
            // 
            this.dgvGlobalSetting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGlobalSetting.Location = new System.Drawing.Point(21, 103);
            this.dgvGlobalSetting.Name = "dgvGlobalSetting";
            this.dgvGlobalSetting.Size = new System.Drawing.Size(1151, 569);
            this.dgvGlobalSetting.TabIndex = 0;
            this.dgvGlobalSetting.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvGlobalSetting_CellMouseClick);
            this.dgvGlobalSetting.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvGlobalSetting_CellContentClick);
            // 
            // tabScopes
            // 
            this.tabScopes.Controls.Add(this.label11);
            this.tabScopes.Controls.Add(this.btnAddServerSetting);
            this.tabScopes.Controls.Add(this.label8);
            this.tabScopes.Controls.Add(this.label7);
            this.tabScopes.Controls.Add(this.label6);
            this.tabScopes.Controls.Add(this.label5);
            this.tabScopes.Controls.Add(this.btnAddSiteSetting);
            this.tabScopes.Controls.Add(this.lblDesc);
            this.tabScopes.Controls.Add(this.lblConstant);
            this.tabScopes.Controls.Add(this.dgvServerSetting);
            this.tabScopes.Controls.Add(this.lblSettingConstant);
            this.tabScopes.Controls.Add(this.dgvCommunitySetting);
            this.tabScopes.Controls.Add(this.dgvSiteSetting);
            this.tabScopes.Location = new System.Drawing.Point(4, 25);
            this.tabScopes.Name = "tabScopes";
            this.tabScopes.Padding = new System.Windows.Forms.Padding(3);
            this.tabScopes.Size = new System.Drawing.Size(1188, 797);
            this.tabScopes.TabIndex = 1;
            this.tabScopes.Text = "Scopes";
            this.tabScopes.UseVisualStyleBackColor = true;
            // 
            // btnAddServerSetting
            // 
            this.btnAddServerSetting.Location = new System.Drawing.Point(997, 63);
            this.btnAddServerSetting.Name = "btnAddServerSetting";
            this.btnAddServerSetting.Size = new System.Drawing.Size(175, 37);
            this.btnAddServerSetting.TabIndex = 12;
            this.btnAddServerSetting.Text = "Add Server Settings";
            this.btnAddServerSetting.UseVisualStyleBackColor = true;
            this.btnAddServerSetting.Click += new System.EventHandler(this.btnAddServerSetting_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(502, 597);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 16);
            this.label8.TabIndex = 11;
            this.label8.Text = "Brand Settings";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(502, 398);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(143, 16);
            this.label7.TabIndex = 10;
            this.label7.Text = "Community Settings";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(497, 90);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 16);
            this.label6.TabIndex = 9;
            this.label6.Text = "Server Settings";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "Site Settings";
            // 
            // btnAddSiteSetting
            // 
            this.btnAddSiteSetting.Location = new System.Drawing.Point(289, 69);
            this.btnAddSiteSetting.Name = "btnAddSiteSetting";
            this.btnAddSiteSetting.Size = new System.Drawing.Size(175, 37);
            this.btnAddSiteSetting.TabIndex = 7;
            this.btnAddSiteSetting.Text = "Add Site Settings";
            this.btnAddSiteSetting.UseVisualStyleBackColor = true;
            this.btnAddSiteSetting.Click += new System.EventHandler(this.btnAddSiteSetting_Click);
            // 
            // lblDesc
            // 
            this.lblDesc.AutoSize = true;
            this.lblDesc.Location = new System.Drawing.Point(6, 25);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(0, 16);
            this.lblDesc.TabIndex = 6;
            // 
            // lblConstant
            // 
            this.lblConstant.AutoSize = true;
            this.lblConstant.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConstant.Location = new System.Drawing.Point(13, 12);
            this.lblConstant.Name = "lblConstant";
            this.lblConstant.Size = new System.Drawing.Size(0, 13);
            this.lblConstant.TabIndex = 5;
            // 
            // dgvServerSetting
            // 
            this.dgvServerSetting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvServerSetting.Location = new System.Drawing.Point(500, 115);
            this.dgvServerSetting.Name = "dgvServerSetting";
            this.dgvServerSetting.Size = new System.Drawing.Size(672, 274);
            this.dgvServerSetting.TabIndex = 4;
            this.dgvServerSetting.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvServerSetting_CellContentClick);
            // 
            // lblSettingConstant
            // 
            this.lblSettingConstant.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lblSettingConstant.Location = new System.Drawing.Point(500, 626);
            this.lblSettingConstant.Name = "lblSettingConstant";
            this.lblSettingConstant.Size = new System.Drawing.Size(672, 140);
            this.lblSettingConstant.TabIndex = 3;
            // 
            // dgvCommunitySetting
            // 
            this.dgvCommunitySetting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCommunitySetting.Location = new System.Drawing.Point(500, 426);
            this.dgvCommunitySetting.Name = "dgvCommunitySetting";
            this.dgvCommunitySetting.Size = new System.Drawing.Size(672, 168);
            this.dgvCommunitySetting.TabIndex = 2;
            this.dgvCommunitySetting.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCommunitySetting_CellContentClick);
            // 
            // dgvSiteSetting
            // 
            this.dgvSiteSetting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSiteSetting.Location = new System.Drawing.Point(16, 115);
            this.dgvSiteSetting.Name = "dgvSiteSetting";
            this.dgvSiteSetting.Size = new System.Drawing.Size(448, 651);
            this.dgvSiteSetting.TabIndex = 1;
            this.dgvSiteSetting.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvSiteSetting_CellMouseClick);
            // 
            // tabServer
            // 
            this.tabServer.Controls.Add(this.label9);
            this.tabServer.Controls.Add(this.btnGetServers);
            this.tabServer.Controls.Add(this.dgvServerSetting1);
            this.tabServer.Controls.Add(this.label4);
            this.tabServer.Controls.Add(this.button1);
            this.tabServer.Controls.Add(this.textBox1);
            this.tabServer.Controls.Add(this.dgvServers);
            this.tabServer.Controls.Add(this.label3);
            this.tabServer.Location = new System.Drawing.Point(4, 25);
            this.tabServer.Name = "tabServer";
            this.tabServer.Size = new System.Drawing.Size(1188, 797);
            this.tabServer.TabIndex = 3;
            this.tabServer.Text = "Server";
            this.tabServer.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Filter:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(319, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 24);
            this.button1.TabIndex = 7;
            this.button1.Text = "Search";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(88, 16);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(211, 22);
            this.textBox1.TabIndex = 6;
            // 
            // dgvServers
            // 
            this.dgvServers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvServers.Location = new System.Drawing.Point(16, 65);
            this.dgvServers.Name = "dgvServers";
            this.dgvServers.Size = new System.Drawing.Size(456, 657);
            this.dgvServers.TabIndex = 2;
            this.dgvServers.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvServers_CellContentClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Servers";
            // 
            // btnGetGlobalSettings
            // 
            this.btnGetGlobalSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetGlobalSettings.Location = new System.Drawing.Point(31, 54);
            this.btnGetGlobalSettings.Name = "btnGetGlobalSettings";
            this.btnGetGlobalSettings.Size = new System.Drawing.Size(175, 49);
            this.btnGetGlobalSettings.TabIndex = 2;
            this.btnGetGlobalSettings.Text = "Get Global Settings";
            this.btnGetGlobalSettings.UseVisualStyleBackColor = true;
            this.btnGetGlobalSettings.Click += new System.EventHandler(this.btnGetGlobalSettings_Click);
            // 
            // lblURI
            // 
            this.lblURI.AutoSize = true;
            this.lblURI.Location = new System.Drawing.Point(375, 14);
            this.lblURI.Name = "lblURI";
            this.lblURI.Size = new System.Drawing.Size(0, 13);
            this.lblURI.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Environment:";
            // 
            // btnGetServers
            // 
            this.btnGetServers.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetServers.Location = new System.Drawing.Point(439, 12);
            this.btnGetServers.Name = "btnGetServers";
            this.btnGetServers.Size = new System.Drawing.Size(175, 30);
            this.btnGetServers.TabIndex = 5;
            this.btnGetServers.Text = "Get Servers";
            this.btnGetServers.UseVisualStyleBackColor = true;
            this.btnGetServers.Click += new System.EventHandler(this.btnGetServers_Click);
            // 
            // btnCompare
            // 
            this.btnCompare.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompare.Location = new System.Drawing.Point(240, 54);
            this.btnCompare.Name = "btnCompare";
            this.btnCompare.Size = new System.Drawing.Size(175, 49);
            this.btnCompare.TabIndex = 6;
            this.btnCompare.Text = "Compare Settings by Environment";
            this.btnCompare.UseVisualStyleBackColor = true;
            this.btnCompare.Click += new System.EventHandler(this.btnCompare_Click);
            // 
            // dgvServerSetting1
            // 
            this.dgvServerSetting1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvServerSetting1.Location = new System.Drawing.Point(490, 65);
            this.dgvServerSetting1.Name = "dgvServerSetting1";
            this.dgvServerSetting1.Size = new System.Drawing.Size(685, 657);
            this.dgvServerSetting1.TabIndex = 9;
            this.dgvServerSetting1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvServerSetting1_CellContentClick);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(487, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 16);
            this.label9.TabIndex = 10;
            this.label9.Text = "Settings";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(21, 689);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(558, 16);
            this.label10.TabIndex = 23;
            this.label10.Text = "* To see site/community etc for particular setting click on the \"SettingID\" colum" +
                "n ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(418, 769);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(260, 16);
            this.label11.TabIndex = 24;
            this.label11.Text = "* To edit settings  click within the row";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1246, 947);
            this.Controls.Add(this.btnCompare);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblURI);
            this.Controls.Add(this.btnGetGlobalSettings);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.cboEnv);
            this.Name = "SettingsForm";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.Settings_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabGlobal.ResumeLayout(false);
            this.tabGlobal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGlobalSetting)).EndInit();
            this.tabScopes.ResumeLayout(false);
            this.tabScopes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServerSetting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSettingConstant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommunitySetting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSiteSetting)).EndInit();
            this.tabServer.ResumeLayout(false);
            this.tabServer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServerSetting1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboEnv;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabGlobal;
        private System.Windows.Forms.TabPage tabScopes;
        private System.Windows.Forms.Button btnGetGlobalSettings;
        private System.Windows.Forms.DataGridView dgvGlobalSetting;
        private System.Windows.Forms.Label lblURI;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtFilter;
        private System.Windows.Forms.DataGridView dgvCommunitySetting;
        private System.Windows.Forms.DataGridView dgvSiteSetting;
        private System.Windows.Forms.DataGridView lblSettingConstant;
        private System.Windows.Forms.DataGridView dgvServerSetting;
        private System.Windows.Forms.Label lblConstant;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.TabPage tabServer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridView dgvServers;
        private System.Windows.Forms.Button btnGetServers;
        private System.Windows.Forms.Button btnCompare;
        private System.Windows.Forms.Button btnAddSiteSetting;
        private System.Windows.Forms.CheckBox chkIncludeSiteSettings;
        private System.Windows.Forms.Button btnSelect1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnAddServerSetting;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dgvServerSetting1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
    }
}