﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.Management;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Spark.SettingManager.BusinellLogic;
using Spark.SettingManager.ValueObjects;
namespace Spark.SettingManager
{
    public class SettingsBL
    {

          public static SettingsBL Instance = new SettingsBL();

          public Setting Find(int settingid, List<Setting> settings)
          {
              try
              {
                  if (settingid <=0 || settings == null)
                      return null;

                  List<Setting> list = settings.FindAll(delegate(Setting s) { return s.SettingID == settingid; });
                  if (list == null || list.Count == 0)
                      return null;

                  return list[0];

              }
              catch (Exception ex)
              { throw ex; }


          }

           
          public List<Setting> Search(string filter, List<Setting> settings)
          {
              try
              {
                  if (String.IsNullOrEmpty(filter) || settings==null)
                      return settings;

                List<Setting> list= settings.FindAll(delegate(Setting s) {return  s.SettingConstant.ToLower().IndexOf(filter.ToLower()) >=0; });
                return list;

              }
              catch (Exception ex)
              { throw ex; }


          }

          public List<SiteSettingExpanded> Search(string filter, List<SiteSettingExpanded> settings)
          {
              try
              {
                  if (String.IsNullOrEmpty(filter) || settings == null)
                      return settings;

                  List<SiteSettingExpanded> list = settings.FindAll(delegate(SiteSettingExpanded s) { return s.SettingConstant.ToLower().IndexOf(filter.ToLower()) >= 0; });
                  return list;

              }
              catch (Exception ex)
              { throw ex; }


          }


          public Settings Search( Settings settings, int settingid)
          {
              try
              {
                  if ( settings == null)
                      return settings;

                  Settings settingslist = new Settings();
                  List<SiteSetting> sitelist = settings.SiteSettingList.FindAll(delegate(SiteSetting s) { return s.SettingID== settingid; });
                  settingslist.SiteSettingList = sitelist;

                  List<CommunitySetting> communitylist = settings.CommunitySettingList.FindAll(delegate(CommunitySetting s) { return s.SettingID == settingid; });
                  settingslist.CommunitySettingList = communitylist;

                  List<BrandSetting> brandlist = settings.BrandSettingList.FindAll(delegate(BrandSetting s) { return s.SettingID == settingid; });
                  settingslist.BrandSettingList = brandlist;

                  List<ServerSetting> serverlist = settings.ServerSettingList.FindAll(delegate(ServerSetting s) { return s.SettingID == settingid; });
                  settingslist.ServerSettingList = serverlist;

                  return settingslist;



              }
              catch (Exception ex)
              { throw ex; }


          }


          public List<Site> GetSitesList(int communityid, string uri)
          {
              try
              {
                  List<Site> list = new List<Site>();
                  Sites sites = ServicesBL.Instance.GetBrandConfigService(uri).GetSites();

                  if (communityid > 0)
                  {
                      foreach (Site site in sites)
                      {
                          if (site.Community.CommunityID == communityid)
                              list.Add(site);

                      }
                  }
                  return list;
              }
              catch (Exception ex)
              { return null; }

          }


          public List<Setting> GetMissingSettings(List<Setting> setting1,List<Setting> setting2)
          {
               
                var consts1 = from c in setting1 select c.SettingConstant;
                var consts2 = from c in setting2 select c.SettingConstant;
                var missing2 = consts1.Except(consts2);

                if (missing2 != null)
                {
                    var sett1 = from c in setting1
                                from m in missing2.ToList<string>()
                                where c.SettingConstant == m
                                select c;
                  

                return sett1.ToList<Setting>();

                }
                return null;
          }

          public void Compare(List<SiteSettingExpanded> setting1, List<SiteSettingExpanded> setting2)
          {

              var consts1 = from c in setting1 select c.SettingConstant;
              var consts2 = from c in setting2 select c.SettingConstant;
              List<string> missing2 = consts1.Except(consts2).ToList<string>();

              
             var diff1 = from c1 in setting1
                          from c2 in setting2
                          where c1.SettingConstant == c2.SettingConstant && c1.SiteSettingValue != c2.SiteSettingValue
                          select c1;


              foreach (SiteSettingExpanded s in setting1)
              {
                  string ss = missing2.Find(delegate(string c) { return c.ToLower() == s.SettingConstant.ToLower(); });
                  if (!String.IsNullOrEmpty(ss))
                      s.MissingInCompare = true;

                  SiteSettingExpanded s1 = diff1.ToList<SiteSettingExpanded>().Find(delegate(SiteSettingExpanded c) { return c.SettingConstant.ToLower() == s.SettingConstant.ToLower(); });
                  if (s1 != null)
                      s.ValueDifferInCompare = true;
              }


          }


          public List<SiteSettingExpanded> GetSiteSettingsExt(List<Setting> setting1, List<SiteSetting> sitesetting)
          {


              var sett1 = from c in setting1
                          from ss in sitesetting
                          where c.SettingID == ss.SettingID
                          select new SiteSettingExpanded(ss, c.SettingConstant) { };

                  return sett1.ToList<SiteSettingExpanded>();

            
          }

          public List<ServerSettingExpanded> GetServerSettingsExt(List<Setting> setting1, List<ServerSetting> serversetting)
          {


              var sett1 = from c in setting1
                          from ss in serversetting
                          where c.SettingID == ss.SettingID
                          select new ServerSettingExpanded(ss, c.SettingConstant) { };

              return sett1.ToList<ServerSettingExpanded>();


          }
          public Setting GetSetting( List<Setting> list, string settingconst)
          {
              Setting s = list.Find(delegate(Setting ss) { return ss.SettingConstant.ToLower() == settingconst.ToLower(); });
              return s;

          }

          public List<SiteSetting> GetSiteSettings(Setting s, List<SiteSetting> sitesettings)
          {
              List<SiteSetting> sett1 = sitesettings.FindAll(delegate(SiteSetting ss){return ss.SettingID==s.SettingID;});
              return sett1;

          }

          public List<CommunitySetting> GetCommunitySettings(Setting s, List<CommunitySetting> settings)
          {
              List<CommunitySetting> sett1 = settings.FindAll(delegate(CommunitySetting ss) { return ss.SettingID == s.SettingID; });
              return sett1;

          }

          public List<BrandSetting> GetBrandSettings(Setting s, List<BrandSetting> settings)
          {
              List<BrandSetting> sett1 = settings.FindAll(delegate(BrandSetting ss) { return ss.SettingID == s.SettingID; });
              return sett1;

          }

          public List<ServerSetting> GetServerSettings(Setting s, List<ServerSetting> settings)
          {
              List<ServerSetting> sett1 = settings.FindAll(delegate(ServerSetting ss) { return ss.SettingID == s.SettingID; });
              return sett1;

          }


    }
}
