﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ValueObjects.Lpd;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.RemotingServices.ValueObjects;
using Matchnet.RemotingServices.ValueObjects.ServiceDefinitions;
namespace Spark.SettingManager.BusinellLogic
{
    public class ServicesBL
    {
        public static ServicesBL Instance = new ServicesBL();

        private ServicesBL() { }


        public ISettingsService GetSettingService(string uri)
        {
            try
            {
                return (ISettingsService)Activator.GetObject(typeof(ISettingsService), uri);
            }
            catch (Exception ex)
            {
                throw (new Exception("Cannot activate remote service manager. (uri: " + uri + ")", ex));
            }
        }




        public IBrandConfigService GetBrandConfigService(string uri)
        {
            try
            {
                return (IBrandConfigService)Activator.GetObject(typeof(IBrandConfigService), uri);
            }
            catch (Exception ex)
            {
                throw (new Exception("Cannot activate remote service manager. (uri: " + uri + ")", ex));
            }
        }


        public IAttributeMetadataService GetAttributeService(string uri)
        {
            try
            {
                return (IAttributeMetadataService)Activator.GetObject(typeof(IAttributeMetadataService), uri);
            }
            catch (Exception ex)
            {
                throw (new Exception("Cannot activate remote service manager. (uri: " + uri + ")", ex));
            }
        }



        public IAttributeOptionService GetAttributeOptionService(string uri)
        {
            try
            {
                return (IAttributeOptionService)Activator.GetObject(typeof(IAttributeOptionService), uri);
            }
            catch (Exception ex)
            {
                throw (new Exception("Cannot activate remote service manager. (uri: " + uri + ")", ex));
            }
        }



        public IPageConfigService GetPageConfigService(string uri)
        {
            try
            {
                return (IPageConfigService)Activator.GetObject(typeof(IPageConfigService), uri);
            }
            catch (Exception ex)
            {
                throw (new Exception("Cannot activate remote service manager. (uri: " + uri + ")", ex));
            }
        }


        public IPromotionService GetPromotionService(string uri)
        {
            try
            {
                return (IPromotionService)Activator.GetObject(typeof(IPromotionService), uri);
            }
            catch (Exception ex)
            {
                throw (new Exception("Cannot activate remote service manager. (uri: " + uri + ")", ex));
            }
        }


        public IProfileSectionService GetProfileSectionService(string uri)
        {
            try
            {
                return (IProfileSectionService)Activator.GetObject(typeof(IProfileSectionService), uri);
            }
            catch (Exception ex)
            {
                throw (new Exception("Cannot activate remote service manager. (uri: " + uri + ")", ex));
            }
        }

        //public IPurchaseService GetPurchaseService(string uri)
        //{
        //    try
        //    {
        //        return (IPurchaseService)Activator.GetObject(typeof(IPurchaseService), uri);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw (new Exception("Cannot activate remote service manager. (uri: " + uri + ")", ex));
        //    }

        //}

        public IArticleService GetContentArticleService(string uri)
        {
            try
            {
                return (IArticleService)Activator.GetObject(typeof(IArticleService), uri);
            }
            catch (Exception ex)
            {
                throw (new Exception("Cannot activate remote service manager. (uri: " + uri + ")", ex));
            }

        }
        //public IResourceTemplateService GeResourceTemplateService(string uri)
        //{
        //    try
        //    {
        //        return (IResourceTemplateService)Activator.GetObject(typeof(IResourceTemplateService), uri);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw (new Exception("Cannot activate remote service manager. (uri: " + uri + ")", ex));
        //    }

        //}
    }
}
