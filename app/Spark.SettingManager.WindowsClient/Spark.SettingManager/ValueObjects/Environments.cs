﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Spark.SettingManager
{
    public class Environment
    {
        public string Code = "";
        public string ManagementServiceURI = "";
        public string ConfigServiceURI = "";
        public string ContentServiceURI = "";
        public string PurchaseServiceURI = "";
        public string ResourceTemplateServiceURI = "";
        public string overrideMachine = "";
    }
   
}
