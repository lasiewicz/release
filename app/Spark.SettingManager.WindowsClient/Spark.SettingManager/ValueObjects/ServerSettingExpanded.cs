﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ValueObjects.Management;
namespace Spark.SettingManager.ValueObjects
{
    public class ServerSettingExpanded:ServerSetting
    {
        public ServerSettingExpanded(ServerSetting s, string settingconstant)
        {
            SettingID = s.SettingID;
            ServerID = s.ServerID;
            ServerSettingValue = s.ServerSettingValue;
            SettingConstant = settingconstant;
            ServerSettingID = s.ServerSettingID;
            SiteID = s.SiteID;
            CommunityID = s.CommunityID;
            BrandID = s.BrandID;
          

        }
        public string SettingConstant { get; set; }
    }
}
