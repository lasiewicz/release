﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Matchnet.Configuration.ValueObjects.Management;
namespace Spark.SettingManager.ValueObjects
{
    
    public class SiteSettingExpanded:SiteSetting
    {

        const string TABLE_NAME="SiteSetting";
        public SiteSettingExpanded(SiteSetting s, string settingconstant)
        {
            SettingID = s.SettingID;
            SiteID = s.SiteID;
            SiteSettingValue = s.SiteSettingValue;
            SettingConstant = settingconstant;
          

        }
        public string SettingConstant { get; set; }

        public bool MissingInCompare { get; set; }
        public bool ValueDifferInCompare { get; set; }
    }
}
