﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Matchnet.Content.ValueObjects.AttributeOption;
namespace SparkSiteManagement.VO
{
    public class AttributeOption :IComparable
    {

        private int _attributeOptionID;
        private int _attributeID;
        private int _value;
        private int _listOrder;
        private string _description;
        private string _resourceKey;
        private string _name;
        private int _groupid;
        public AttributeOption(Matchnet.Content.ValueObjects.AttributeOption.AttributeOption option, string attrname)
        {
            _attributeOptionID = option.AttributeOptionID;
            _attributeID = option.AttributeID;
            _value = option.Value;
            _listOrder = option.ListOrder;
            _description = option.Description;
            _resourceKey = option.ResourceKey;
            _name = attrname;
        }

        /// <summary>
        /// 
        /// </summary>
        public int GroupID
        {
            get
            {
                return _groupid;
            }
            set
            {
                _groupid = value;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public int AttributeOptionID
        {
            get
            {
                return _attributeOptionID;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int AttributeID
        {
            get
            {
                return _attributeID;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Value
        {
            get
            {
                return _value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int ListOrder
        {
            get
            {
                return _listOrder;
            }
            set
            {
                _listOrder = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Description
        {
            get
            {
                return _description;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string ResourceKey
        {

            get
            {
                return _resourceKey;
            }
        }
        public string AttributeName
        {

            get
            {
                return _name;
            }
        }

        public int CompareTo(object obj)
        {
            if (obj is Setting)
            {
                AttributeOption m2 = (AttributeOption)obj;
                return _attributeID.CompareTo(m2.AttributeID);
            }
            else
                throw new ArgumentException("Object is not a AttributeOption.");
        }

    }
}
