﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace SparkSiteManagement.VO
{

    #region class RegistrationPromotion
    public class RegistrationPromotion
    {
        int _Registrationpromotionid;
        int _Promotionid;
        int _Domainid;
        int _Privatelabelid;
        string _Resourcekey = null;
        string _Resourceconstant = null;
        int _Listorder;
        object _Insertdate = null;


        public void LoadRegistrationPromotion(DataRow rs)
        {

            _Registrationpromotionid = (int)(rs["registrationpromotionid"] != Convert.DBNull ? rs["registrationpromotionid"] : 0);
            _Promotionid = (int)(rs["promotionid"] != Convert.DBNull ? rs["promotionid"] : 0);
            _Domainid = (int)(rs["domainid"] != Convert.DBNull ? rs["domainid"] : 0);
            _Privatelabelid = (int)(rs["privatelabelid"] != Convert.DBNull ? rs["privatelabelid"] : 0);
            _Resourcekey = (string)(rs["resourcekey"] != Convert.DBNull ? rs["resourcekey"] : "");
            _Resourceconstant = (string)(rs["resourceconstant"] != Convert.DBNull ? rs["resourceconstant"] : "");
            _Listorder = (int)(rs["listorder"] != Convert.DBNull ? rs["listorder"] : 0);
            _Insertdate = (DateTime)(rs["insertdate"] != Convert.DBNull ? rs["insertdate"] : DateTime.MinValue);
        }
        public int Registrationpromotionid
        {
            get
            {
                return _Registrationpromotionid;
            }
            set
            {
                _Registrationpromotionid = value;
            }

        }


        public int Promotionid
        {
            get
            {
                return _Promotionid;
            }
            set
            {
                _Promotionid = value;
            }

        }


        public int Domainid
        {
            get
            {
                return _Domainid;
            }
            set
            {
                _Domainid = value;
            }

        }


        public int Privatelabelid
        {
            get
            {
                return _Privatelabelid;
            }
            set
            {
                _Privatelabelid = value;
            }

        }


        public string Resourcekey
        {
            get
            {
                return _Resourcekey;
            }
            set
            {
                _Resourcekey = value;
            }

        }


        public string Resourceconstant
        {
            get
            {
                return _Resourceconstant;
            }
            set
            {
                _Resourceconstant = value;
            }

        }


        public int Listorder
        {
            get
            {
                return _Listorder;
            }
            set
            {
                _Listorder = value;
            }

        }


        public object Insertdate
        {
            get
            {
                return _Insertdate;
            }
            set
            {
                _Insertdate = value;
            }

        }


    }
    #endregion

}
