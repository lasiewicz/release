﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SparkSiteManagement.VO
{
    public class ListCategory
    {
        int _Listcategoryid;
        int _Siteid;
        int _Listorder;
        string _resourceKey = "";


        
        public string ResourceKey
        {
            get
            {
                return _resourceKey;
            }
            set
            {
                _resourceKey = value;
            }


        }
        public int Listcategoryid
        {
            get
            {
                return _Listcategoryid;
            }
            set
            {
                _Listcategoryid = value;
            }

        }


        public int Siteid
        {
            get
            {
                return _Siteid;
            }
            set
            {
                _Siteid = value;
            }

        }


        public int Listorder
        {
            get
            {
                return _Listorder;
            }
            set
            {
                _Listorder = value;
            }

        }

    }
}
