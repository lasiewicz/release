﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SparkSiteManagement.VO
{
    public class AttributeGroup
    {

        //attr

        private int _ScopeID;
        private string _AttributeName;
        private string _DataType;
        private int _AttributeIDOldValueContainer;
        //group

        private int _AttributeGroupID;
        private int _AttributeID;
        private int _GroupID;
        private int _AttributeTypeID;
        private int _AttributeStatusMask;
        private int _Length;
        private bool _EncryptFlag;
        private string _DefaultValue;
        private int _AttributeGroupIDOldValueContainer;
        //attr

        public int ScopeID
        {
            get { return _ScopeID; }
            set { _ScopeID = value; }

        }
        public string AttributeName
        {
            get { return _AttributeName; }
            set { _AttributeName = value; }

        }
        public string DataType
        {
            get { return _DataType; }
            set { _DataType = value; }

        }
        public int AttributeIDOldValueContainer
        {
            get { return _AttributeIDOldValueContainer; }
            set { _AttributeIDOldValueContainer = value; }

        }
        //group

        public int AttributeGroupID
        {
            get { return _AttributeGroupID; }
            set { _AttributeGroupID = value; }

        }
        public int AttributeID
        {
            get { return _AttributeID; }
            set {_AttributeID = value; }

        }
        public int GroupID
        {
            get { return _GroupID; }
            set { _GroupID = value; }

        }
        public int AttributeTypeID
        {
            get { return _AttributeTypeID; }
            set { _AttributeTypeID = value; }

        }
        public int AttributeStatusMask
        {
            get { return _AttributeStatusMask; }
            set { _AttributeStatusMask = value; }

        }
        public int Length
        {
            get { return _Length; }
            set { _Length = value; }

        }
        public bool EncryptFlag
        {
            get { return _EncryptFlag; }
            set { _EncryptFlag = value; }

        }
        public string DefaultValue
        {
            get { return _DefaultValue; }
            set { _DefaultValue = value; }

        }
        public int AttributeGroupIDOldValueContainer
        {
            get { return _AttributeGroupIDOldValueContainer; }
            set { _AttributeGroupIDOldValueContainer = value; }

        }
        
    }
}
