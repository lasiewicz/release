﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SparkSiteManagement.VO
{
    public class SitePage
    {
        private Int32 _appID;
        private string _appName;
        private string _appPath;
        private Int32 _pageID;
        private Int32 _siteID;
        private string _controlName;
        private int _securityMask;
        private int _layoutTemplateID;
        private string _pageName;

        public SitePage(Int32 appid, string appName, string appPath,Int32 pageid,Int32 siteID,
            string controlName,
            int securityMask,
            int layoutTemplateName, string pagename)
        {
            _appID = appid;
            _appName = appName;
            _appPath = appPath;
            _pageID = pageid;
            _siteID = siteID;
            _controlName = controlName;
            _securityMask = securityMask;
            _layoutTemplateID = layoutTemplateName;
            _pageName = pagename;
        }

        /// <summary>
        /// Site identifier
        /// </summary>
        public Int32 AppID
        {
            get
            {
                return _appID;
            }
        }

        public string AppName
        {
            get
            {
                return _appName;
            }
        }

        public String AppPath
        {
            get
            {
                return _appPath;
            }
        }
        /// <summary>
        /// Site identifier
        /// </summary>
        public Int32 PageID
        {
            get
            {
                return _pageID;
            }
        }


        /// <summary>
        /// Site identifier
        /// </summary>
        public Int32 SiteID
        {
            get
            {
                return _siteID;
            }

        }


        /// <summary>
        /// Filename of WebUserControl that renders page. Does not include path or file extension.
        /// </summary>
        public string ControlName
        {
            get
            {
                return _controlName;
            }
        }


        /// <summary>
        /// Represents security permissions required to access page 
        /// </summary>
        public Int32 SecurityMask
        {
            get
            {
                return _securityMask;
            }
        }


        /// <summary>
        /// Page layout template control name.
        /// </summary>
        public int LayoutTemplateID
        {
            get
            {
                return _layoutTemplateID;
            }
        }

        public string PageName
        {
            get
            {
                return _pageName;
            }
        }

    }
}
