﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace SparkSiteManagement.VO
{

    #region class PhotoSearchOption
    public class PhotoSearchOption
    {
        int _Optiontypeid;
        int _Optionvalue;
        int _Siteid;
        int _Listorder;
        bool _Defaultflag;
        string _Description = null;

        public PhotoSearchOption(DataRow rs)
        {
            _Optiontypeid = (int)(rs["optiontypeid"] != Convert.DBNull ? rs["optiontypeid"] : 0);
            _Optionvalue = (int)(rs["optionvalue"] != Convert.DBNull ? rs["optionvalue"] : 0);
            _Siteid = (int)(rs["siteid"] != Convert.DBNull ? rs["siteid"] : 0);
            _Listorder = (int)(rs["listorder"] != Convert.DBNull ? rs["listorder"] : 0);
            _Defaultflag = (bool)(rs["defaultflag"] != Convert.DBNull ? rs["defaultflag"] : false);
            _Description = (string)(rs["description"] != Convert.DBNull ? rs["description"] : "");

        }

        public int Optiontypeid
        {
            get
            {
                return _Optiontypeid;
            }
            set
            {
                _Optiontypeid = value;
            }

        }


        public int Optionvalue
        {
            get
            {
                return _Optionvalue;
            }
            set
            {
                _Optionvalue = value;
            }

        }


        public int Siteid
        {
            get
            {
                return _Siteid;
            }
            set
            {
                _Siteid = value;
            }

        }


        public int Listorder
        {
            get
            {
                return _Listorder;
            }
            set
            {
                _Listorder = value;
            }

        }


        public bool Defaultflag
        {
            get
            {
                return _Defaultflag;
            }
            set
            {
                _Defaultflag = value;
            }

        }

        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                _Description = value;
            }

        }
    #endregion
    }
}
