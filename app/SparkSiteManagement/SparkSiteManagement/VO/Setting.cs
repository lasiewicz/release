﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SparkSiteManagement.VO
{
    public class Setting:IComparable
    {
        public string settingConst;
        public string settingValue;
        public int settingID;

        public int CompareTo(object obj)
        {
            if (obj is Setting)
            {
                Setting m2 = (Setting)obj;
                return settingConst.CompareTo(m2.settingConst);
            }
            else
                throw new ArgumentException("Object is not a Setting.");
        }
    }


}
