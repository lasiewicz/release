﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace SparkSiteManagement.VO
{
   
    #region class ObjectGroup
    public class ObjectGroup
    {
        int _Objectgroupid;
        int _Objectid;
        int _Groupid;
        int _Objecttypeid;
        object _Updatedate = null;
        string _Description = null;

        public ObjectGroup(DataRow rs)
        {
            _Objectgroupid = (int) (rs["objectgroupid"]!=Convert.DBNull ? rs["objectgroupid"] : 0);
            _Objectid = (int) (rs["objectid"]!=Convert.DBNull ? rs["objectid"] : 0);
            _Groupid = (int) (rs["groupid"]!=Convert.DBNull ? rs["groupid"] : 0);
            _Objecttypeid = (int) (rs["objecttypeid"]!=Convert.DBNull ? rs["objecttypeid"] : 0);
            _Description = (string)(rs["description"] != Convert.DBNull ? rs["description"] : "");

        }

        public int Objectgroupid
        {
            get
            {
                return _Objectgroupid;
            }
            set
            {
                _Objectgroupid = value;
            }

        }


        public int Objectid
        {
            get
            {
                return _Objectid;
            }
            set
            {
                _Objectid = value;
            }

        }


        public int Groupid
        {
            get
            {
                return _Groupid;
            }
            set
            {
                _Groupid = value;
            }

        }


        public int Objecttypeid
        {
            get
            {
                return _Objecttypeid;
            }
            set
            {
                _Objecttypeid = value;
            }

        }


        public object Updatedate
        {
            get
            {
                return _Updatedate;
            }
            set
            {
                _Updatedate = value;
            }

        }

        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                _Description = value;
            }

        }

    }
    #endregion

}
