﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SparkSiteManagement.VO
{


   public class ProfileSectionAttribute
    {
       int _profilesectionid;
       string _desc;
       int _attributeid;
       int _datatype;
       int _communityid;
       int _siteid;
       int _brandid;
       int _languageid;
       bool _enable;

       public ProfileSectionAttribute(int profilesectionid, string desc, int attributeid, int datatype, int communityid, int siteid, int brandid, int languageid)
       {

           _profilesectionid = profilesectionid;
           _desc = desc;
           _attributeid = attributeid;
           _datatype = datatype;
           _communityid = communityid;
           _siteid = siteid;
           _brandid = brandid;
           _languageid = languageid;

       }

       public ProfileSectionAttribute(System.Data.DataRow dr)
       {

           _profilesectionid = Int32.Parse( dr["profilesectiondefinitionid"].ToString());
           _desc = dr["Description"].ToString();
           _attributeid = Int32.Parse(dr["AttributeID"].ToString());
           _datatype = Int32.Parse(dr["DataType"].ToString());
           _communityid = Int32.Parse(dr["CommunityID"].ToString());
           _siteid = Int32.Parse(dr["SiteID"].ToString());
           _brandid = Int32.Parse(dr["BrandID"].ToString());
           _languageid = Int32.Parse(dr["LanguageID"].ToString());

       }

       public ProfileSectionAttribute(Hashtable dr)
       {

           _profilesectionid = Int32.Parse(dr["profilesectiondefinitionid"].ToString());
           _desc = dr["description"].ToString();
           _attributeid = Int32.Parse(dr["attributeid"].ToString());
           _datatype = Int32.Parse(dr["datatype"].ToString());
           _communityid = Int32.Parse(dr["communityid"].ToString());
           _siteid = Int32.Parse(dr["siteid"].ToString());
           _brandid = Int32.Parse(dr["brandid"].ToString());
           _languageid = Int32.Parse(dr["languageid"].ToString());
           

       }

       public int ProfileSectionDefinitionID
       {
           get { return _profilesectionid; }
           set { _profilesectionid = value; }
       }


       public string Description
       {
           get { return _desc; }
           set { _desc = value; }
       }

       public int AttributeID
       {
           get { return _attributeid; }
           set { _attributeid = value; }
       }

       public int DataType
       {
           get { return _datatype; }
           set { _datatype = value; }
       }

       public int CommunityID
       {
           get { return _communityid; }
           set { _communityid = value; }
       }

       public int SiteID
       {
           get { return _siteid; }
           set { _siteid = value; }
       }

       public int BrandID
       {
           get { return _brandid; }
           set { _brandid = value; }
       }

       public int LanguageID
       {
           get { return _languageid; }
           set { _languageid = value; }
       }
    }


   public class ProfileSectionGroup
   {
       int _profilesectionid;
       string _desc;
       int _groupid;


       public ProfileSectionGroup(int profilesectionid, string desc, int groupid)
       {

           _profilesectionid = profilesectionid;
           _desc = desc;
           _groupid = groupid;
           
       }

       public ProfileSectionGroup(System.Data.DataRow dr)
       {

           _profilesectionid = Int32.Parse(dr["profilesectiondefinitionid"].ToString());
           _desc = dr["Description"].ToString();
           _groupid = Int32.Parse(dr["GroupID"].ToString());
           

       }

       public int ProfileSectionDefinitionID
       {
           get { return _profilesectionid; }
           set { _profilesectionid = value; }
       }


       public string Description
       {
           get { return _desc; }
           set { _desc = value; }
       }

       public int GroupID
       {
           get { return _groupid; }
           set { _groupid = value; }
       }

   }
}
