﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
namespace SparkSiteManagement
{
    public partial class Preview : Form
    {
        public string Script;
        public string ScriptCode;
        public Preview()
        {
            InitializeComponent();
        }

        private void Preview_Load(object sender, EventArgs e)
        {
            rtfPreview.Text = Script;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dlgSaveFile.FileName = ScriptCode;
            dlgSaveFile.ShowDialog();
            
            string filename = dlgSaveFile.FileName;
            lblFileName.Text = filename;

            FileInfo t = new FileInfo(filename);
            StreamWriter Tex =t.CreateText();
            Tex.Write(rtfPreview.Text);
            Tex.Close();
            SiteBL.Instance.SavedFiles.Add(filename);
            
            
        }
    }
}
