﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Promotion;
using Matchnet.Purchase;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
using Matchnet.PromoEngine.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ValueObjects.Article;
namespace SparkSiteManagement
{
    public partial class SiteConfig : Form
    {
        List<Community> communities = new List<Community>();
        DateTime _timeStamp = DateTime.Now;
        public SiteConfig()
        {
            InitializeComponent();
            dgvCommunity.CellEndEdit += new DataGridViewCellEventHandler(dgvCommunity_CellEndEdit);
            dataGridView1.CellEndEdit += new DataGridViewCellEventHandler(dataGridView1_CellEndEdit);
            dataGridView3.CellEndEdit += new DataGridViewCellEventHandler(dataGridView3_CellEndEdit);
        }

        private DateTime TimeStamp
        {
            get
            {
                try
                {
                    return DateTime.Parse(txtTimeStamp.Text);
                }
                catch (Exception ex)
                {
                    return _timeStamp;

                }

            }
        }
        private void tabConfig_Click(object sender, EventArgs e)
        {

        }

        private bool IsNewCommunity
        {
            get
            {
                Community comm = (Community)cboCommunity.SelectedItem;
                int commid = comm.CommunityID;

                int newcomm = Int32.Parse(txtNewCommunityID.Text);
                return commid != newcomm;
            }

        }

        private string ContentURI
        {
            get
            {
                string uri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ContentServiceURI;
                return uri;
            }
        }

        private string ConfigSettingURI
        {
            get
            {
                string uri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ConfigServiceURI;
                return uri;
            }
        }

        private string PurchaseURI
        {
            get
            {
                string uri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].PurchaseServiceURI;
                return uri;
            }
        }
        private void loadEnv()
        {
            try
            {

                cboEnv.Items.Clear();
                for (int i = 0; i < EnvironmentBL.Instance.Environments.Count; i++)
                {
                    cboEnv.Items.Add(EnvironmentBL.Instance.Environments[i].ConfigServiceURI);
                    cboEnv.DisplayMember = EnvironmentBL.Instance.Environments[i].Code;

                }
                cboEnv.SelectedIndex = 0;

                populateCommunities();
                cboCommunity.SelectedIndex = 0;
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            GetSite();
        }

        private void SiteConfig_Load(object sender, EventArgs e)
        {
            loadEnv();
            txtTimeStamp.Text = _timeStamp.ToString();
        }


        //private void BL.WFormBL.Instance.Instance.populateGridFromProperties(Object obj,DataGridView grid)
        //{

        //    Type t = obj.GetType();

        //    PropertyInfo[] pi = t.GetProperties();
        //    ArrayList vals = new ArrayList();
        //    grid.ColumnCount = pi.Length;

        //    int i = 0;
        //    foreach (PropertyInfo p in pi)
        //    {
        //        grid.Columns[i].HeaderText = p.Name;
        //        string val=p.GetValue(obj, null).ToString();
        //        if (val == "Matchnet.Content.ValueObjects.BrandConfig.Community")
        //        {
        //            Community comm = (Community)p.GetValue(obj, null);
        //            val = comm.CommunityID.ToString();

        //        }
        //        else if (val == "Matchnet.Content.ValueObjects.BrandConfig.Site")
        //        {
        //            Site site = (Site)p.GetValue(obj, null);
        //            val = site.SiteID.ToString();

        //        }
        //        vals.Add(val);
        //        i++;
        //    }
        //    grid.Rows.Add(vals.ToArray());


        //}

        //private void  BL.WFormBL.Instance.populateGridFromPropertiesList<T>(List<T> objList, DataGridView grid, int startColumn, bool select)
        //{
        //    if (objList == null || objList.Count==0)
        //        return;
        //    Type t = objList[0].GetType();

        //    PropertyInfo[] pi = t.GetProperties();

        //    grid.ColumnCount = pi.Length + startColumn;

        //    int i = startColumn;

        //    foreach (PropertyInfo p in pi)
        //    {
        //        grid.Columns[i].HeaderText = p.Name;
        //        i++;
        //    }

        //    for (int l = 0; l < objList.Count; l++)
        //    {
        //        ArrayList vals = new ArrayList();
        //        for (int k = 0; k < startColumn; k++)
        //        {
        //            vals.Add(select);
        //        }
        //        foreach (PropertyInfo p in pi)
        //        {   
        //            var vval=p.GetValue(objList[l], null);
        //            string val = "";
        //            if (vval != null)
        //                val = vval.ToString();

        //            if (val == "Matchnet.Content.ValueObjects.BrandConfig.Community")
        //            {
        //                Community comm = (Community)p.GetValue(objList[l], null);
        //                val = comm.CommunityID.ToString();
        //            }
        //            else if (val == "Matchnet.Content.ValueObjects.BrandConfig.Site")
        //            {
        //                Site site = (Site)p.GetValue(objList[l], null);
        //                val = site.SiteID.ToString();
        //            }
        //            vals.Add(val);

        //        }
        //        grid.Rows.Add(vals.ToArray());
        //    }

        //}

        private void populateBrands(ComboBox cbo, List<Brand> brands)
        {
            cbo.Items.Clear();
            foreach (Brand b in brands)
            {
                cbo.Items.Add(b.BrandID.ToString());
                cbo.DisplayMember = b.BrandID.ToString();
            }


        }


        private void populateCommunities()
        {
            AddCommunity(1, "Spark");
            AddCommunity(2, "Glimpse");
            AddCommunity(3, "JDate");
            AddCommunity(8, "Corp");
            AddCommunity(10, "Cupid");
            AddCommunity(12, "College");
            AddCommunity(20, "Mingle");
            AddCommunity(21, "Italian");
            AddCommunity(22, "InterRacial");
            AddCommunity(26, "SingleSeniorsMeet");



            cboCommunity.DataSource = communities;
            cboCommunity.DisplayMember = "Name";

        }

        private void AddCommunity(int id, string name)
        {
            Community c = new Community(id, name);
            communities.Add(c);

        }
        private void btnGetBrandAttributes_Click(object sender, EventArgs e)
        {
            GetBrandAttributes();
        }

        private void btnGetSiteScripts_Click(object sender, EventArgs e)
        {
            string sql = "";
            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";

            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            string sqldeclare = DBScripting.SiteDB.Instance.GetSQLTemplate("attribute_dynamic_declare");
            sql += sqldeclare;
            sql += sqlbegintran;
            if (IsNewCommunity)
            {
                Hashtable commgroup = new Hashtable();
                commgroup["groupid"] = txtNewCommunityID.Text;
                commgroup["groupname"] = txtNewCommunityName.Text;
                commgroup["scopeid"] = "100";
                sql += "--community group \r\n";
                sql += DBScripting.SiteDB.Instance.GetSQL("group", commgroup, chkTimeStamp.Checked, TimeStamp) + "\r\n";
                sql += sqlrollbacktran;

                Hashtable comm = GetObject(dgvCommunity, 0);
                sql += "\r\n--community record";
                sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("community", comm, chkTimeStamp.Checked, TimeStamp);
                sql += sqlrollbacktran;

            }

            Hashtable sitegroup = new Hashtable();
            sitegroup["groupid"] = txtNewSiteID.Text;
            sitegroup["groupname"] = txtNewSiteGroupName.Text;
            sitegroup["scopeid"] = "1000";
            sql += "\r\n--site group \r\n";
            sql += DBScripting.SiteDB.Instance.GetSQL("group", sitegroup, chkTimeStamp.Checked, TimeStamp) + "\r\n";
            sql += sqlrollbacktran;

            Hashtable brandgroup = new Hashtable();
            brandgroup["groupid"] = txtNewBrandID.Text;
            brandgroup["groupname"] = txtNewBrandGroupName.Text;
            brandgroup["scopeid"] = "10000";

            sql += "\r\n--brand group\r\n";
            sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("group", brandgroup, chkTimeStamp.Checked, TimeStamp);
            sql += sqlrollbacktran;

            Hashtable site = GetObject(dataGridView1, 0);
            sql += "\r\n--site record ";
            sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("site", site, chkTimeStamp.Checked, TimeStamp);
            sql += sqlrollbacktran;

            int row = 0; ;
            for (int i = 0; i < dataGridView3.Rows.Count; i++)
            {
                row = i;
                if (Boolean.Parse(dataGridView3.Rows[i].Cells[0].Value.ToString()))
                {
                    break;
                }
            }
            Hashtable brand = GetObject(dataGridView3, row);
            sql += "\r\n--brand record ";
            sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("brand", brand, chkTimeStamp.Checked, TimeStamp);
            sql += sqlrollbacktran;
            sql += sqlcommittran;
            ShowPreviewForm(sql, "Community_Site_Brand.sql");


        }


        private Hashtable GetObject(DataGridView rgd, int row)
        {
            Hashtable site = new Hashtable();
            for (int i = 0; i < rgd.Columns.Count; i++)
            {
                string key = rgd.Columns[i].HeaderText.ToLower();
                if (!string.IsNullOrEmpty(key))
                {
                    if (rgd.Rows[row].Cells[i].Value != null)
                    {
                        string val = rgd.Rows[row].Cells[i].Value.ToString();
                        site[key] = val;
                    }
                }

            }
            return site;
        }

        private void btnGetSiteSettings_Click(object sender, EventArgs e)
        {
            int row = 0;
            int maxsett = 0;

            string sql = "";
            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";

            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            sql += sqlbegintran;
            for (int i = 0; i < dataGridView2.Rows.Count - 1; i++)
            {
                Hashtable setting = GetObject(dataGridView2, i);
                if (Boolean.Parse(dataGridView2.Rows[i].Cells[0].Value.ToString()))
                {



                    setting["siteid"] = Int32.Parse(txtNewSiteID.Text);
                    if (chkGetDynamic.Checked)
                    {
                        setting["sitesettingid"] = "@sitesettingid";
                        sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("sitesettings_dynamic", setting, chkTimeStamp.Checked, TimeStamp);
                        sql += sqlrollbacktran;

                    }
                    else
                    {

                        // setting["sitesettingid"] = maxsett + i + 1;
                        maxsett = Int32.Parse(txtMaxSiteSettingID.Text);
                        setting["sitesettingid"] = maxsett + i + 1;
                        sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("sitesettings", setting, chkTimeStamp.Checked, TimeStamp);
                        sql += sqlrollbacktran;

                    }
                }
            }
            sql += sqlcommittran;
            if (chkGetDynamic.Checked)
            {

                string sqldeclare = DBScripting.SiteDB.Instance.GetSQLTemplate("sitesettings_dynamic_declare");
                sql = sqldeclare + "\r\n" + sql;


            }

            ShowPreviewForm(sql, "SiteSetting.sql");
        }

        private void btnGetAttrScript_Click(object sender, EventArgs e)
        {
            int row = 0;
            int maxattr = Int32.Parse(txtMaxAttributeGroupID.Text);

            string sql = "";

            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";
            
            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            string sqldeclare = DBScripting.SiteDB.Instance.GetSQLTemplate("attribute_dynamic_declare");
            sql += sqldeclare;
            sql += sqlbegintran;
            for (int i = 0; i < dataGridView4.Rows.Count - 1; i++)
            {
                Hashtable attrGroup = GetObject(dataGridView4, i);
                if (!chkNoNewPKS.Checked)
                {
                    maxattr = maxattr + 1;
                    attrGroup["attributegroupid"] = maxattr;
                }
                int oldValContainer = -1;
                Int32.TryParse(attrGroup["attributegroupidoldvaluecontainer"].ToString(), out oldValContainer);
                if (oldValContainer < 0)
                {
                    attrGroup["attributegroupidoldvaluecontainer"] = "null";
                }

                int defaultValue = -1;
                Int32.TryParse(attrGroup["defaultvalue"].ToString(), out defaultValue);
                if (defaultValue < 0)
                {
                    attrGroup["defaultvalue"] = "null";
                }
                else
                {
                    if (String.IsNullOrEmpty(attrGroup["defaultvalue"].ToString()))
                    {
                        attrGroup["defaultvalue"] = "null";
                    }

                }

                attrGroup["groupid"] = Int32.Parse(txtNewSiteID.Text);
                sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("attribute", attrGroup, chkTimeStamp.Checked, TimeStamp);
                sql += sqlrollbacktran;
            }


            for (int i = 0; i < dataGridView5.Rows.Count - 1; i++)
            {
                Hashtable attrGroup = GetObject(dataGridView5, i);
                maxattr = maxattr + 1;
                attrGroup["attributegroupid"] = maxattr;
                attrGroup["groupid"] = Int32.Parse(txtNewBrandID.Text);

                int oldValContainer = -1;
                Int32.TryParse(attrGroup["attributegroupidoldvaluecontainer"].ToString(), out oldValContainer);
                if (oldValContainer < 0)
                {
                    attrGroup["attributegroupidoldvaluecontainer"] = "null";
                }

                int defaultValue = -1;
                Int32.TryParse(attrGroup["defaultvalue"].ToString(), out defaultValue);
                if (defaultValue < 0)
                {
                    attrGroup["defaultvalue"] = "null";
                }
                else
                {
                    if (String.IsNullOrEmpty(attrGroup["defaultvalue"].ToString()))
                    {
                        attrGroup["defaultvalue"] = "null";
                    }

                }
                sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("attribute", attrGroup, chkTimeStamp.Checked, TimeStamp);
                sql += sqlrollbacktran;
            }
            txtMaxAttributeGroupID.Text = maxattr.ToString();

            sql += sqlcommittran;
            ShowPreviewForm(sql, "AttributeGroup.sql");
        }

        private void btnBrandAttrOption_Click(object sender, EventArgs e)
        {
            GetBrandAttrOptions();
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void btnGetAttrOptionScript_Click(object sender, EventArgs e)
        {
            GetAttributeOptions();
        }

        private void btnGetSitePages_Click(object sender, EventArgs e)
        {
            string sql = "";
            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";

            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            sql += sqlbegintran;
            string sqldeclare = DBScripting.SiteDB.Instance.GetSQLTemplate("sitepages_dynamic_declare");

            sql += sqldeclare;
            for (int i = 0; i < dataGridView8.Rows.Count - 1; i++)
            {
                Hashtable pages = GetObject(dataGridView8, i);

                pages["siteid"] = Int32.Parse(txtNewSiteID.Text);
                sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("sitepages", pages, chkTimeStamp.Checked, TimeStamp);
                sql += sqlrollbacktran;
            }

            sql += sqlcommittran;
            ShowPreviewForm(sql, "SitePages.sql");
        }

        private void btnGetListCatScript_Click(object sender, EventArgs e)
        {
            int row = 0;
            string sql = "";

            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";

            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            sql += sqlbegintran;
            for (int i = 0; i < dataGridView9.Rows.Count - 1; i++)
            {
                row = i;
                if (Boolean.Parse(dataGridView9.Rows[i].Cells[0].Value.ToString()))
                {
                    Hashtable cat = GetObject(dataGridView9, row);
                    cat["siteid"] = Int32.Parse(txtNewSiteID.Text);

                    sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("listcategory", cat, chkTimeStamp.Checked, TimeStamp);
                    sql += sqlrollbacktran;
                }
            }

            sql += sqlcommittran;
            ShowPreviewForm(sql, "ListCategory.sql");
        }

        private void btnGetRegPromos_Click(object sender, EventArgs e)
        {
            string uri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ContentServiceURI;
            int siteId = Int32.Parse(cboSites.Text);

            Matchnet.Content.ValueObjects.BrandConfig.Site site = SiteBL.Instance.GetSite(siteId, uri);

            List<VO.RegistrationPromotion> promos = SiteBL.Instance.GetRegistrationPromotion(site.Community.CommunityID);

            DataGridViewColumn cellCheck10 = new DataGridViewCheckBoxColumn();
            dataGridView10.Columns.Insert(0, cellCheck10);
            dataGridView10.Columns[0].HeaderText = "Select";
            BL.WFormBL.Instance.populateGridFromPropertiesList<VO.RegistrationPromotion>(promos, dataGridView10, 1, true);
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnSiteSettingSelect_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dataGridView2.Rows.Count; i++)
            {
                if (dataGridView2.Rows[i].Cells[0].Value != null && Boolean.Parse(dataGridView2.Rows[i].Cells[0].Value.ToString()))
                    dataGridView2.Rows[i].Cells[0].Value = false;
                else
                    dataGridView2.Rows[i].Cells[0].Value = true;

            }
        }

        private void cmdCommunitySites_Click(object sender, EventArgs e)
        {
            GetCommunitySites();

        }

        private void dgvCommunity_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            txtNewCommunityID.Text = dgvCommunity.Rows[0].Cells[0].Value.ToString();
            txtNewCommunityName.Text = dgvCommunity.Rows[0].Cells[1].Value.ToString();
            if (dataGridView1.Rows.Count > 1)
                dataGridView1.Rows[0].Cells[1].Value = txtNewCommunityID.Text;
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void cboBrands_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetBrandAttributes();
            GetBrandAttrOptions();
            GetSparkWSProfileSections();
            int brandID = Int32.Parse(cboBrands.Items[cboBrands.SelectedIndex].ToString());
            for (int i = 0; i < dataGridView3.Rows.Count; i++)
            {
                int rowBrandID = Int32.Parse(dataGridView3.Rows[i].Cells[1].Value.ToString());
                if (rowBrandID == brandID)
                {
                    dataGridView3.Rows[i].Cells[0].Value = "true";
                    dataGridView3.Rows[i].Selected = true;
                    txtNewBrandID.Text = brandID.ToString();
                    txtNewBrandGroupName.Text = dataGridView3.Rows[i].Cells[3].Value.ToString();

                    break;

                }

            }
        }

        private void dgvCommunity_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            string id = dataGridView1.Rows[0].Cells[0].Value.ToString();
            //bool valid = SiteBL.Instance.ValidateSiteID(id, ContentURI);
            //if (valid)
            //{
            txtNewSiteID.Text = dataGridView1.Rows[0].Cells[0].Value.ToString();
            txtNewSiteGroupName.Text = dataGridView1.Rows[0].Cells[2].Value.ToString();
            for (int i = 0; i < dataGridView3.Rows.Count; i++)
            {
                if (dataGridView3.Rows[i].Cells[0] != null)
                {
                    if (dataGridView3.Rows[i].Cells[0].Value != null && Boolean.Parse(dataGridView3.Rows[i].Cells[0].Value.ToString()))
                    {
                        dataGridView3.Rows[i].Cells[2].Value = txtNewSiteID.Text;
                        break;
                    }
                }
            }
            //}
            //else
            //{
            //    MessageBox.Show("Please enter valid siteid. It should integer and unique.");
            //}
        }
        private void dataGridView3_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            string id = dataGridView3.Rows[e.RowIndex].Cells[1].Value.ToString();
            // bool valid = SiteBL.Instance.ValidateBrandID(id, ContentURI);

            if (dataGridView3.Rows[e.RowIndex].Cells[0].Value != null && Boolean.Parse(dataGridView3.Rows[e.RowIndex].Cells[0].Value.ToString()))
            {
                txtNewBrandID.Text = dataGridView3.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtNewBrandGroupName.Text = dataGridView3.Rows[e.RowIndex].Cells[3].Value.ToString();
            }

        }

        private void btnGetCommAttrScripts_Click(object sender, EventArgs e)
        {
            int row = 0;
            int maxattr = Int32.Parse(txtMaxAttributeGroupID.Text);

            string sql = "";

            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";
            string sqldeclare = DBScripting.SiteDB.Instance.GetSQLTemplate("attribute_dynamic_declare");
            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            sql += sqlbegintran;
            sql += sqldeclare;
            List<Hashtable> attributes = new List<Hashtable>();
            Hashtable attrContainers = new Hashtable();
            for (int i = 0; i < dgvCommAttr.Rows.Count - 1; i++)
            {
                Hashtable attrGroup = GetObject(dgvCommAttr, i);
                maxattr += 1;
                string oldattrgroupid = attrGroup["attributegroupid"].ToString();
                if (!chkNoNewPKS.Checked)
                {
                    attrGroup["attributegroupid"] = maxattr;

                }
                int oldValContainer = -1;
                Int32.TryParse(attrGroup["attributegroupidoldvaluecontainer"].ToString(), out oldValContainer);
                if (oldValContainer < 0)
                {
                    attrGroup["attributegroupidoldvaluecontainer"] = "null";
                }


                attrContainers.Add(oldattrgroupid, attrGroup["attributegroupid"]);


                int defaultValue = -1;
                Int32.TryParse(attrGroup["defaultvalue"].ToString(), out defaultValue);
                if (defaultValue < 0)
                {
                    attrGroup["defaultvalue"] = "null";
                }
                else
                {
                    if (String.IsNullOrEmpty(attrGroup["defaultvalue"].ToString()))
                    {
                        attrGroup["defaultvalue"] = "null";
                    }

                }
                attrGroup["groupid"] = Int32.Parse(txtNewCommunityID.Text);
                attributes.Add(attrGroup);

            }
            foreach (Hashtable attrGroup in attributes)
            {
                int oldValContainer = -1;
                Int32.TryParse(attrGroup["attributegroupidoldvaluecontainer"].ToString(), out oldValContainer);
                if (oldValContainer > 0)
                {
                    attrGroup["attributegroupidoldvaluecontainer"] = attrContainers[attrGroup["attributegroupidoldvaluecontainer"]];
                }
                sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("attribute", attrGroup, chkTimeStamp.Checked, TimeStamp);
                sql += sqlrollbacktran;
            }

            sql += "\r\n" + "insert into AttributeCollectionAttribute \r\n" +
                            "select attributecollectionid,attributeid," + txtNewCommunityID.Text + ",optionalflag\r\n" +
                            "from AttributeCollectionAttribute\r\n" +
                            "where GroupID=" + txtNewCommunityID.Text + "\r\n";

            sql += sqlrollbacktran;
            sql += sqlcommittran;
            txtMaxAttributeGroupID.Text = maxattr.ToString();
            ShowPreviewForm(sql, "CommunityAttributes.sql");
        }

        private void GetRegistrationPromos()
        {
            dataGridView10.Columns.Clear();
            string uri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ContentServiceURI;

            List<VO.RegistrationPromotion> promos = SiteBL.Instance.GetRegistrationPromotion(((Community)cboCommunity.SelectedItem).CommunityID);

            DataGridViewColumn cellCheck10 = new DataGridViewCheckBoxColumn();
            dataGridView10.Columns.Insert(0, cellCheck10);
            dataGridView10.Columns[0].HeaderText = "Select";
            BL.WFormBL.Instance.populateGridFromPropertiesList<VO.RegistrationPromotion>(promos, dataGridView10, 1, true);
        }

        private void button2_Click(object sender, EventArgs e)
        {

            string sql = "";
            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";

            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            sql += sqlbegintran;
            int promoid = Int32.Parse(txtMaxPromoID.Text);
            for (int i = 0; i < dataGridView10.Rows.Count - 1; i++)
            {
                if (Boolean.Parse(dataGridView10.Rows[i].Cells[0].Value.ToString()))
                {
                    Hashtable promos = GetObject(dataGridView10, i);
                    promoid += 1;
                    promos["registrationpromotionid"] = promoid;
                    promos["domainid"] = txtNewCommunityID.Text;
                    sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("registrationpromo", promos, chkTimeStamp.Checked, TimeStamp);
                    sql += sqlrollbacktran;
                }
            }

            sql += sqlcommittran;
            ShowPreviewForm(sql, "RegistrationPromos.sql");
        }

        private void GetSparkWSProfileSections()
        {

            dgvPSPhoto.Columns.Clear();
            dgvPSAttr.Columns.Clear();
            dgvPSCOmm.Columns.Clear();
            dgvPSSite.Columns.Clear();
            Community comm = (Community)cboCommunity.SelectedItem;
            int commid = comm.CommunityID;
            int siteId = Int32.Parse(cboSites.Text);
            int brandID = Int32.Parse(cboBrands.Items[cboBrands.SelectedIndex].ToString());


            DataSet ds = SiteBL.Instance.GetProfileSections(commid, siteId, brandID);

            List<VO.ProfileSectionAttribute> psattributes = SiteBL.Instance.GetProfileSectionsAttr(ds.Tables[0]);

            List<VO.ProfileSectionGroup> pscommunity = SiteBL.Instance.GetProfileSectionsGroup(ds.Tables[1]);
            List<VO.ProfileSectionGroup> pssite = SiteBL.Instance.GetProfileSectionsGroup(ds.Tables[2]);
            List<VO.ProfileSectionGroup> psphoto = SiteBL.Instance.GetProfileSectionsGroup(ds.Tables[3]);

            DataGridViewColumn cellCheck1 = new DataGridViewCheckBoxColumn();
            dgvPSAttr.Columns.Insert(0, cellCheck1);
            dgvPSAttr.Columns[0].HeaderText = "Select";
            BL.WFormBL.Instance.populateGridFromPropertiesList<VO.ProfileSectionAttribute>(psattributes, dgvPSAttr, 1, true);

            DataGridViewColumn cellCheck2 = new DataGridViewCheckBoxColumn();
            dgvPSCOmm.Columns.Insert(0, cellCheck2);
            dgvPSCOmm.Columns[0].HeaderText = "Select";
            BL.WFormBL.Instance.populateGridFromPropertiesList<VO.ProfileSectionGroup>(pscommunity, dgvPSCOmm, 1, true);

            DataGridViewColumn cellCheck3 = new DataGridViewCheckBoxColumn();
            dgvPSSite.Columns.Insert(0, cellCheck3);
            dgvPSSite.Columns[0].HeaderText = "Select";
            BL.WFormBL.Instance.populateGridFromPropertiesList<VO.ProfileSectionGroup>(pssite, dgvPSSite, 1, true);

            DataGridViewColumn cellCheck4 = new DataGridViewCheckBoxColumn();
            dgvPSPhoto.Columns.Insert(0, cellCheck4);
            dgvPSPhoto.Columns[0].HeaderText = "Select";
            BL.WFormBL.Instance.populateGridFromPropertiesList<VO.ProfileSectionGroup>(psphoto, dgvPSPhoto, 1, true);
        }


        private void GetCommunitySites()
        {

            cboSites.Items.Clear();
            Community comm = (Community)cboCommunity.SelectedItem;
            int commid = comm.CommunityID;
            string uri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ContentServiceURI;

            List<Site> sites = SiteBL.Instance.GetCommunitySites(commid, uri);

            foreach (Site s in sites)
            {
                cboSites.Items.Add(s.SiteID);
            }

            dgvCommunity.Rows.Clear();

            BL.WFormBL.Instance.populateGridFromProperties(comm, dgvCommunity);
            dgvCommAttr.Columns.Clear();
            string attruri = uri.ToLower().Replace("brandconfigsm", "AttributeMetadataSM");
            List<VO.AttributeGroup> attrComm = BL.AttributeBL.Instance.GetAttributes(commid, attruri, true);
            int maxGroupID = BL.AttributeBL.Instance.GetMaxAttributeGroupID(attruri, false);
            txtMaxAttributeGroupID.Text = maxGroupID.ToString();
            BL.WFormBL.Instance.populateGridFromPropertiesList<VO.AttributeGroup>(attrComm, dgvCommAttr, 0, false);


            attruri = uri.ToLower().Replace("brandconfigsm", "AttributeOptionSM");
            dgvCommAttrOptions.Columns.Clear();
            List<VO.AttributeOption> options = BL.AttributeOptionBL.Instance.GetAttributeOptions(attruri, commid);
            BL.WFormBL.Instance.populateGridFromPropertiesList<VO.AttributeOption>(options, dgvCommAttrOptions, 0, false);

            GetRegistrationPromos();
            txtNewCommunityID.Text = comm.CommunityID.ToString();
            txtNewCommunityName.Text = comm.Name;
        }


        private void GetSite()
        {
            dataGridView1.Columns.Clear();
            dataGridView2.Columns.Clear();
            dataGridView2.Rows.Clear();
            dataGridView3.Columns.Clear();
            dataGridView4.Columns.Clear();
            dataGridView5.Columns.Clear();
            dataGridView6.Columns.Clear();
            dataGridView7.Columns.Clear();
            dataGridView8.Columns.Clear();
            dataGridView9.Columns.Clear();
            dataGridView11.Columns.Clear();
            dgvPhotoSearch.Columns.Clear();
            dgvCCTypes.Columns.Clear();

            int siteId = Int32.Parse(cboSites.Text);



            string uri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ContentServiceURI;
            Matchnet.Content.ValueObjects.BrandConfig.Site site = SiteBL.Instance.GetSite(siteId, uri);
            string purchaseUri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].PurchaseServiceURI;
            txtNewSiteID.Text = siteId.ToString();
            txtNewSiteGroupName.Text = site.Name;

            BL.WFormBL.Instance.populateGridFromProperties(site, dataGridView1);
            DataGridViewColumn cellCheck = new DataGridViewCheckBoxColumn();

            List<Matchnet.Content.ValueObjects.BrandConfig.Brand> brands = SiteBL.Instance.GetSiteBrands(siteId, uri);
            dataGridView3.Columns.Insert(0, cellCheck);
            dataGridView3.Columns[0].HeaderText = "Select";
            BL.WFormBL.Instance.populateGridFromPropertiesList<Matchnet.Content.ValueObjects.BrandConfig.Brand>(brands, dataGridView3, 1, false);

            string configuri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ConfigServiceURI;
            List<VO.Setting> settings = SiteBL.Instance.GetSettings(siteId, configuri);


            dataGridView2.ColumnCount = 4;

            DataGridViewColumn cellCheck0 = new DataGridViewCheckBoxColumn();
            dataGridView2.Columns.Insert(0, cellCheck0);
            dataGridView2.Columns[0].HeaderText = "Select";
            dataGridView2.Columns[1].HeaderText = "SettingID";
            dataGridView2.Columns[1].Width = dataGridView2.Width / 3;
            dataGridView2.Columns[2].HeaderText = "SettingConstant";
            dataGridView2.Columns[2].Width = dataGridView2.Width / 3;
            dataGridView2.Columns[3].HeaderText = "SiteSettingValue";
            dataGridView2.Columns[3].Width = dataGridView2.Width / 3;

            foreach (VO.Setting s in settings)
            {
                dataGridView2.Rows.Add(false, s.settingID, s.settingConst, s.settingValue);

            }

            string attruri = uri.ToLower().Replace("brandconfigsm", "AttributeMetadataSM");
            List<VO.AttributeGroup> attrSite = BL.AttributeBL.Instance.GetAttributes(siteId, attruri, true);

            BL.WFormBL.Instance.populateGridFromPropertiesList<VO.AttributeGroup>(attrSite, dataGridView4, 0, false);
            populateBrands(cboBrands, brands);


            attruri = uri.ToLower().Replace("brandconfigsm", "AttributeOptionSM");

            List<VO.AttributeOption> options = BL.AttributeOptionBL.Instance.GetAttributeOptions(attruri, siteId);
            BL.WFormBL.Instance.populateGridFromPropertiesList<VO.AttributeOption>(options, dataGridView6, 0, false);


            List<VO.SitePage> listPages = SiteBL.Instance.GetSitePages(siteId);
            DataGridViewColumn cellCheck8 = new DataGridViewCheckBoxColumn();
            dataGridView8.Columns.Insert(0, cellCheck8);
            dataGridView8.Columns[0].HeaderText = "Select";
            BL.WFormBL.Instance.populateGridFromPropertiesList<VO.SitePage>(listPages, dataGridView8, 1, true);

            List<VO.ListCategory> listCateg = SiteBL.Instance.GetSiteListCategory(siteId);
            DataGridViewColumn cellCheck1 = new DataGridViewCheckBoxColumn();
            dataGridView9.Columns.Insert(0, cellCheck1);
            dataGridView9.Columns[0].HeaderText = "Select";
            BL.WFormBL.Instance.populateGridFromPropertiesList<VO.ListCategory>(listCateg, dataGridView9, 1, true);


            int maxobjectgroupid = 0;
            List<VO.ObjectGroup> obj = SiteBL.Instance.GetObjGroups(siteId, ref maxobjectgroupid);
            txtMaxObjGroupID.Text = maxobjectgroupid.ToString();
            BL.WFormBL.Instance.populateGridFromPropertiesList<VO.ObjectGroup>(obj, dataGridView11, 1, true);

            List<Matchnet.Purchase.ValueObjects.CreditCard> cc = SiteBL.Instance.GetCCTypes(siteId, purchaseUri);
            BL.WFormBL.Instance.populateGridFromPropertiesList<Matchnet.Purchase.ValueObjects.CreditCard>(cc, dgvCCTypes, 0, false);

            List<VO.PhotoSearchOption> opt = SiteBL.Instance.GetPhotoSearchOptions(siteId);
            DataGridViewColumn cellCheck12 = new DataGridViewCheckBoxColumn();
            dgvPhotoSearch.Columns.Insert(0, cellCheck12);
            dgvPhotoSearch.Columns[0].HeaderText = "Select";

            BL.WFormBL.Instance.populateGridFromPropertiesList<VO.PhotoSearchOption>(opt, dgvPhotoSearch, 1, true);
            
            string artcleuri = uri.ToLower().Replace("brandconfigsm", "ArticleSM");
            List<SiteCategory> catlist=null;
            if (String.IsNullOrEmpty(txtArticleCategories.Text))
                catlist = SiteBL.Instance.GetArticleCategories(siteId, artcleuri);
            else
                catlist = BL.ProjectBL.Instance.GetArticleCategories(txtArticleCategories.Text, siteId, artcleuri);
            BL.WFormBL.Instance.populateGridFromPropertiesList<SiteCategory>(catlist, dgvSiteCategories, 0, false);


            dgvArticles.Columns.Clear();
            List<SiteArticle> articles = new List<SiteArticle>();
            List<string> propsDisplay = new List<string>();

            propsDisplay.Add("articleid");
            propsDisplay.Add("title");
            propsDisplay.Add("content");
            propsDisplay.Add("fileid");
            propsDisplay.Add("publishedflag");
            propsDisplay.Add("featuredflag");
            propsDisplay.Add("ordinal");
            for (int i = 0; i < dgvSiteCategories.Rows.Count - 1; i++)
            {

                string id = dgvSiteCategories.Rows[i].Cells[1].Value.ToString();
                List<SiteArticle> sa = BL.ProjectBL.Instance.GetArticle(Int32.Parse(id), siteId, artcleuri);
                articles.AddRange(sa);

            }

            BL.WFormBL.Instance.populateGridFromPropertiesList<SiteArticle>(articles, dgvArticles, 0, true, propsDisplay);
            lblArticlesCount.Text = articles.Count.ToString();

        }
        private void cboCommunity_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetCommunitySites();
        }

        private void cboSites_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetSite();
        }

        private void ShowArticlesAndCategories()
        {
            try
            {
                string uri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ContentServiceURI;
                int siteId = Int32.Parse(cboSites.Text);
                string artcleuri = uri.ToLower().Replace("brandconfigsm", "ArticleSM");
                dgvSiteCategories.Columns.Clear();
                List<SiteCategory> catlist = null;
                if (String.IsNullOrEmpty(txtArticleCategories.Text))
                    catlist = SiteBL.Instance.GetArticleCategories(siteId, artcleuri);
                else
                    catlist = BL.ProjectBL.Instance.GetArticleCategories(txtArticleCategories.Text, siteId, artcleuri);
                BL.WFormBL.Instance.populateGridFromPropertiesList<SiteCategory>(catlist, dgvSiteCategories, 0, false);


                dgvArticles.Columns.Clear();
                List<SiteArticle> articles = new List<SiteArticle>();
                List<string> propsDisplay = new List<string>();

                propsDisplay.Add("articleid");
                propsDisplay.Add("title");
                propsDisplay.Add("content");
                propsDisplay.Add("fileid");
                propsDisplay.Add("publishedflag");
                propsDisplay.Add("featuredflag");
                propsDisplay.Add("ordinal");




                for (int i = 0; i < dgvSiteCategories.Rows.Count - 1; i++)
                {

                    string id = dgvSiteCategories.Rows[i].Cells[1].Value.ToString();
                    List<SiteArticle> sa = BL.ProjectBL.Instance.GetArticle(Int32.Parse(id), siteId, artcleuri);
                    articles.AddRange(sa);

                }

                BL.WFormBL.Instance.populateGridFromPropertiesList<SiteArticle>(articles, dgvArticles, 0, true, propsDisplay);
                lblArticlesCount.Text = articles.Count.ToString();
            }catch(Exception ex)
            { MessageBox.Show(ex.ToString()); }
        }
        private void GetBrandAttributes()
        {

            dataGridView5.Columns.Clear();
            dataGridView5.Rows.Clear();
            int brandID = Int32.Parse(cboBrands.Items[cboBrands.SelectedIndex].ToString());
            string uri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ContentServiceURI;
            string attruri = uri.ToLower().Replace("brandconfigsm", "AttributeMetadataSM");
            List<VO.AttributeGroup> attrBrand = BL.AttributeBL.Instance.GetAttributes(brandID, attruri, true);

            BL.WFormBL.Instance.populateGridFromPropertiesList<VO.AttributeGroup>(attrBrand, dataGridView5, 0, false);
        }


        private void GetBrandAttrOptions()
        {
            dataGridView7.Columns.Clear();
            dataGridView7.Rows.Clear();
            int brandID = Int32.Parse(cboBrands.Items[cboBrands.SelectedIndex].ToString());
            string uri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ContentServiceURI;
            string attruri = uri.ToLower().Replace("brandconfigsm", "AttributeOptionSM");
            List<VO.AttributeOption> options = BL.AttributeOptionBL.Instance.GetAttributeOptions(attruri, brandID);
            BL.WFormBL.Instance.populateGridFromPropertiesList<VO.AttributeOption>(options, dataGridView7, 0, false);
        }

        private void btnGetProfileSectionScripts_Click(object sender, EventArgs e)
        {

            string sql = "";

            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";

            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            sql += sqlbegintran;
            sql += GetProfileSectionAttrScript();

            sql += GetProfileSectionCommunityScript();

            sql += GetProfileSectionSiteScript();
            sql += GetProfileSectionPhotoScript();
            sql += sqlcommittran;
            ShowPreviewForm(sql, "SparkWS.sql");

        }


        private string GetProfileSectionAttrScript()
        {
            string sql = "";
            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";

            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }

            Community comm = (Community)cboCommunity.SelectedItem;
            int commid = comm.CommunityID;
            int siteId = Int32.Parse(cboSites.Text);
            int brandID = Int32.Parse(cboBrands.Items[cboBrands.SelectedIndex].ToString());

            int newcommid = Int32.Parse(txtNewCommunityID.Text); ;
            int newsiteId = Int32.Parse(txtNewSiteID.Text);
            int newbrandid = Int32.Parse(txtNewBrandID.Text);

            for (int i = 0; i < dgvPSAttr.Rows.Count - 1; i++)
            {
                Hashtable ps = GetObject(dgvPSAttr, i);
                VO.ProfileSectionAttribute attr = new VO.ProfileSectionAttribute(ps);
                if (IsNewCommunity)
                {
                    if (attr.CommunityID > 0)
                        ps["communityid"] = newcommid.ToString();

                    if (attr.SiteID > 0)
                        ps["siteid"] = newsiteId.ToString();


                    if (attr.BrandID > 0)
                        ps["brandid"] = newbrandid.ToString();

                }
                else
                {
                    if (attr.SiteID <= 0)
                        continue;
                    ps["siteid"] = newsiteId.ToString();

                    if (attr.BrandID > 0)
                        ps["brandid"] = newbrandid.ToString();


                }

                sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("profilesectionattr", ps, chkTimeStamp.Checked, TimeStamp);
                sql += sqlrollbacktran;
            }


            return sql;
        }



        private string GetProfileSectionCommunityScript()
        {
            string sql = "";
            Community comm = (Community)cboCommunity.SelectedItem;

            if (!IsNewCommunity)
                return "";

            int newcommid = Int32.Parse(txtNewCommunityID.Text); ;

            for (int i = 0; i < dgvPSCOmm.Rows.Count - 1; i++)
            {
                Hashtable ps = GetObject(dgvPSCOmm, i);
                ps["communityid"] = newcommid.ToString();
                sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("profilesectioncommunity", ps, chkTimeStamp.Checked, TimeStamp);
            }


            return sql;
        }


        private string GetProfileSectionSiteScript()
        {
            string sql = "";
            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";

            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            sql += sqlbegintran;
            int newsiteId = Int32.Parse(txtNewSiteID.Text);

            for (int i = 0; i < dgvPSSite.Rows.Count - 1; i++)
            {
                Hashtable ps = GetObject(dgvPSCOmm, i);
                ps["siteid"] = newsiteId.ToString();

                sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("profilesectionsite", ps, chkTimeStamp.Checked, TimeStamp);
                sql += sqlrollbacktran;
            }


            return sql;
        }

        private string GetProfileSectionPhotoScript()
        {
            string sql = "";
            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";

            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            sql += sqlbegintran;
            int newcommid = Int32.Parse(txtNewCommunityID.Text); ;
            if (!IsNewCommunity)
                return "";

            for (int i = 0; i < dgvPSPhoto.Rows.Count - 1; i++)
            {
                Hashtable ps = GetObject(dgvPSPhoto, i);
                ps["communityid"] = newcommid.ToString();

                sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("profilesectionphoto", ps, chkTimeStamp.Checked, TimeStamp);
                sql += sqlrollbacktran;
            }


            return sql;
        }

        private void ShowPreviewForm(string sql, string sqlcode)
        {

            Preview frmPrev = new Preview();
            frmPrev.Script = sql;
            frmPrev.ScriptCode = sqlcode;
            frmPrev.Show();

        }

        private void chkUseTran_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnRollback_Click(object sender, EventArgs e)
        {
            Hashtable ids = new Hashtable();

            ids["communityid"] = txtNewCommunityID.Text;
            ids["siteid"] = txtNewSiteID.Text;
            ids["brandid"] = txtNewBrandID.Text;

            string sql = "";
            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";

            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            sql += sqlbegintran;

            sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("site_rollback", ids, chkTimeStamp.Checked, TimeStamp);
            if (IsNewCommunity)
            {
                sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("community_rollback", ids, chkTimeStamp.Checked, TimeStamp);
            }

            sql += sqlcommittran;
            ShowPreviewForm(sql, "rollback.sql");
        }

        private void btnGetObjScript_Click(object sender, EventArgs e)
        {


            string sql = "";
            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";

            int maxObjGroupID = Int32.Parse(txtMaxObjGroupID.Text);
            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            sql += sqlbegintran;

            for (int i = 0; i < dataGridView11.Rows.Count - 1; i++)
            {
                Hashtable ps = GetObject(dataGridView11, i);
                ps["groupid"] = txtNewSiteID.Text;
                maxObjGroupID += 1;
                ps["objectgroupid"] = maxObjGroupID.ToString();
                sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("objectgroup", ps, chkTimeStamp.Checked, TimeStamp);
                sql += sqlrollbacktran;
            }
            sql += sqlcommittran;
            ShowPreviewForm(sql, "objectgroup.sql");

        }

        private void tabCCTypes_Click(object sender, EventArgs e)
        {

        }

        private void btnGetCCTypesScripts_Click(object sender, EventArgs e)
        {
            try
            {

                string sql = "";
                string sqlbegintran = "";
                string sqlcommittran = "";
                string sqlrollbacktran = "";

                int maxObjGroupID = Int32.Parse(txtMaxCCTypeGroupID.Text);
                if (chkUseTran.Checked)
                {
                    sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                    sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                    sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

                }
                sql += sqlbegintran;
                sql += "  SET IDENTITY_INSERT CreditCardGroup ON \r\n";
                for (int i = 0; i < dgvCCTypes.Rows.Count - 1; i++)
                {
                    Hashtable ps = GetObject(dgvCCTypes, i);
                    ps["groupid"] = txtNewSiteID.Text;
                    maxObjGroupID += 1;
                    ps["creditcardgroupid"] = maxObjGroupID.ToString();

                    sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("cctypes", ps, chkTimeStamp.Checked, TimeStamp);
                    sql += sqlrollbacktran;
                }
                sql += sqlcommittran;
                ShowPreviewForm(sql, "cctypes.sql");

            }
            catch (Exception ex)
            { }
        }

        private void dataGridView8_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnMaxIds_Click(object sender, EventArgs e)
        {
            Hashtable maxids = SiteBL.Instance.GetMaxID();
            txtAttrOptionMaxId.Text = maxids["maxattroptgroupid"].ToString();
            txtMaxAttributeGroupID.Text = maxids["maxattrgroupid"].ToString();
            txtMaxCCTypeGroupID.Text = maxids["ccgroupid"].ToString();
            txtMaxObjGroupID.Text = maxids["objgroupid"].ToString();
            txtMaxPromoID.Text = maxids["regpromoid"].ToString();
            txtMaxSiteSettingID.Text = maxids["maxsitesettingid"].ToString();
            txtMaxPropertyID.Text = maxids["maxpropertyid"].ToString();
            txtMaxSiteArticleID.Text = maxids["maxsitearticleid"].ToString();
            txtMaxSiteCategoryID.Text = maxids["maxsitecategoryid"].ToString();
            txtCurrResTemplID.Text = maxids["maxresourcetemplateid"].ToString();
        }

        private void btnPhotoScripts_Click(object sender, EventArgs e)
        {
            try
            {

                string sql = "";
                string sqlbegintran = "";
                string sqlcommittran = "";
                string sqlrollbacktran = "";


                if (chkUseTran.Checked)
                {
                    sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                    sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                    sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

                }
                sql += sqlbegintran;

                for (int i = 0; i < dgvPhotoSearch.Rows.Count - 1; i++)
                {
                    if (Boolean.Parse(dgvPhotoSearch.Rows[i].Cells[0].Value.ToString()))
                    {
                        Hashtable ps = GetObject(dgvPhotoSearch, i);
                        ps["siteid"] = txtNewSiteID.Text;

                        sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("photostore", ps, chkTimeStamp.Checked, TimeStamp);
                        sql += sqlrollbacktran;
                    }
                }
                sql += sqlcommittran;
                ShowPreviewForm(sql, "photostore.sql");

            }
            catch (Exception ex)
            { }
        }

        private void btnGetResTemplates_Click(object sender, EventArgs e)
        {
            dgvResTemplates.Columns.Clear();
            int brandID = Int32.Parse(cboBrands.Items[cboBrands.SelectedIndex].ToString());
            string uri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ResourceTemplateServiceURI;
            List<ResourceTemplate> templates = SiteBL.Instance.GetResourceTemplates(brandID, uri);
            BL.WFormBL.Instance.populateGridFromPropertiesList<ResourceTemplate>(templates, dgvResTemplates, 0, true);
        }

        private void cmdGetResTemplScripts_Click(object sender, EventArgs e)
        {
            int id = -1;
            Int32.TryParse(txtCurrResTemplID.Text, out id);
            if (id <= 0)
            {
                MessageBox.Show("Please enter current Max Resource template id");
                return;
            }

            string sql = "";
            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";


            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            sql += "use mnSubscription \r\ngo\r\n";
            sql = sql + sqlbegintran;

            for (int i = 0; i < dgvResTemplates.Rows.Count - 1; i++)
            {
                id += 3;
                Hashtable ps = GetObject(dgvResTemplates, i);
                ps["groupid"] = txtNewBrandID.Text;
                ps["resourcetemplateid"] = id;

                string restemplatetypeid = dgvResTemplates.Rows[i].Cells[5].Value.ToString();

                ResourceTemplateType templtype = (ResourceTemplateType)Enum.Parse(typeof(ResourceTemplateType), restemplatetypeid);
                int templtypeid = (int)templtype;

                ps["resourcetemplatetypeid"] = templtypeid.ToString();

                string groupdef = dgvResTemplates.Rows[i].Cells[4].Value.ToString();
                ps["groupdefault"] = groupdef.ToLower() == "false" ? "0" : "1";

                string content = dgvResTemplates.Rows[i].Cells[1].Value.ToString();
                ps["content"] = content.Replace("'", "''");
                ps["pkid"] = id.ToString();
                sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("resourcetemplates", ps, chkTimeStamp.Checked, TimeStamp);
                sql += sqlrollbacktran;

                if (i == dgvResTemplates.Rows.Count - 2)
                {
                    string pksql = DBScripting.SiteDB.Instance.GetSQL("res_template_key", ps, chkTimeStamp.Checked, TimeStamp, true, true);
                    rtfTemplatesKey.Text += pksql;
                }

            }
            sql += sqlcommittran;
            ShowPreviewForm(sql, "ResourceTemplates.sql");



        }

        private void button3_Click(object sender, EventArgs e)
        {
            int row = 0;
            int maxsett = 0;
            int siteId = Int32.Parse(cboSites.Text);
            string sql = "";
            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";

            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            sql += sqlbegintran;
            for (int i = 0; i < dataGridView2.Rows.Count - 1; i++)
            {
                Hashtable setting = GetObject(dataGridView2, i);
                if (Boolean.Parse(dataGridView2.Rows[i].Cells[0].Value.ToString()))
                {

                    setting["siteid"] = siteId.ToString();



                    sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("sitesettings_update", setting, chkTimeStamp.Checked, TimeStamp);
                    sql += sqlrollbacktran;


                }
            }
            sql += sqlcommittran;


            ShowPreviewForm(sql, "SiteSetting_update.sql");
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void btnYNMPartitions_Click(object sender, EventArgs e)
        {
            int row = 0;
            int maxsett = 0;
            int communityid = 0;
            Int32.TryParse(txtNewCommunityID.Text, out communityid);
            if (communityid <= 0)
            {
                MessageBox.Show("Please enter New CommunityID");
                return;
            }
            int partitions = 0;
            Int32.TryParse(txtPartitions.Text, out partitions);
            if (partitions <= 0)
            {
                MessageBox.Show("Please enter number of YNM partitions to create");
                return;
            }

            int maxpropertyid = 0;
            Int32.TryParse(txtMaxPropertyID.Text, out maxpropertyid);
            if (maxpropertyid <= 0)
            {
                MessageBox.Show("Please enter current max PropertyID, or press button \"Get Max PK ids from current database\"");
                return;
            }
            maxpropertyid += 1;
            string sql = "";
            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";

            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            sql += sqlbegintran;

            Hashtable props = new Hashtable();
            props.Add("communityid", communityid);
            props.Add("propertyname", "AffinityID");
            props.Add("propertyid", maxpropertyid);
            props.Add("propertyowner", String.Format("YNMDomainID{0}", communityid));
            props.Add("propertyvalue", partitions.ToString());
            sql += DBScripting.SiteDB.Instance.GetSQL("list_ynm_prop", props, chkTimeStamp.Checked, TimeStamp);
            sql += sqlrollbacktran;

            maxpropertyid += 1;
            props["propertyname"] = "PartitionProcFlag";
            props["propertyvalue"] = "1";
            props["propertyid"] = maxpropertyid.ToString();
            sql += DBScripting.SiteDB.Instance.GetSQL("list_ynm_prop", props, chkTimeStamp.Checked, TimeStamp);
            sql += sqlrollbacktran;
            sql += sqlcommittran;


            ShowPreviewForm(sql, "YNM_Properties_Insert.sql");
        }

        private void btnGetYNMTables_Click(object sender, EventArgs e)
        {
            int row = 0;
            int maxsett = 0;
            int communityid = 0;
            Int32.TryParse(txtNewCommunityID.Text, out communityid);
            if (communityid <= 0)
            {
                MessageBox.Show("Please enter New CommunityID");
                return;
            }
            int partitions = 0;
            Int32.TryParse(txtPartitions.Text, out partitions);
            if (partitions <= 0)
            {
                MessageBox.Show("Please enter number of YNM partitions to create");
                return;
            }


            string sql = "";
            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";

            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            sql += sqlbegintran;
            for (int i = 0; i < partitions; i++)
            {

                Hashtable tbls = new Hashtable();
                tbls.Add("communityid", communityid.ToString());
                tbls.Add("partitionid", i.ToString());
                sql += DBScripting.SiteDB.Instance.GetSQL("list_ynm_tables", tbls, chkTimeStamp.Checked, TimeStamp, true, false);
                sql += sqlrollbacktran;
            }

            sql += sqlcommittran;

            ShowPreviewForm(sql, "YNM_Tables_Create.sql");
        }

        private void btnGetYNMSP_Click(object sender, EventArgs e)
        {
            int row = 0;
            int maxsett = 0;
            int communityid = 0;
            Int32.TryParse(txtNewCommunityID.Text, out communityid);
            if (communityid <= 0)
            {
                MessageBox.Show("Please enter New CommunityID");
                return;
            }
            int partitions = 0;
            Int32.TryParse(txtPartitions.Text, out partitions);
            if (partitions <= 0)
            {
                MessageBox.Show("Please enter number of YNM partitions to create");
                return;
            }


            string sql = "";
            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";

            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            sql += sqlbegintran;

            for (int i = 0; i < partitions; i++)
            {

                Hashtable tbls = new Hashtable();
                tbls.Add("communityid", communityid.ToString());
                tbls.Add("partitionid", i.ToString());


                sql += DBScripting.SiteDB.Instance.GetSQL("list_ynm_sp_list", tbls, chkTimeStamp.Checked, TimeStamp, true, false);
                sql += sqlrollbacktran;

                sql += DBScripting.SiteDB.Instance.GetSQL("list_ynm_sp_mult_list", tbls, chkTimeStamp.Checked, TimeStamp, true, false);
                sql += sqlrollbacktran;

                sql += DBScripting.SiteDB.Instance.GetSQL("list_ynm_sp_save", tbls, chkTimeStamp.Checked, TimeStamp, true, false);
                sql += sqlrollbacktran;


                sql += DBScripting.SiteDB.Instance.GetSQL("list_ynm_sp_status_save", tbls, chkTimeStamp.Checked, TimeStamp, true, false);
                sql += sqlrollbacktran;

                sql += DBScripting.SiteDB.Instance.GetSQL("list_ynm_sp_vm_list", tbls, chkTimeStamp.Checked, TimeStamp, true, false);
                sql += sqlrollbacktran;

            }

            sql += sqlcommittran;

            ShowPreviewForm(sql, "YNM_SPs_Create.sql");
        }

        private void label33_Click(object sender, EventArgs e)
        {

        }

        private void label32_Click(object sender, EventArgs e)
        {

        }

        private void txtMaxSiteCategoryID_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMaxSiteArticleID_TextChanged(object sender, EventArgs e)
        {

        }

        private void dgvArticles_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnGetSiteCategoryScrits_Click(object sender, EventArgs e)
        {
            int row = 0;
            int maxsett = 0;
            int siteId = 0;



            Int32.TryParse(txtNewSiteID.Text, out siteId);
            if (siteId <= 0)
            {
                MessageBox.Show("Please enter New SiteID");
                return;
            }


            Int32.TryParse(txtMaxSiteCategoryID.Text, out maxsett);
            if (maxsett <= 0)
            {
                MessageBox.Show("Please enter current max Current Category ID, or press button \"Get Max PK ids from current database\"");
                return;
            }

            string sql = "";
            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";

            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            sql += sqlbegintran;
            for (int i = 0; i < dgvSiteCategories.Rows.Count - 1; i++)
            {
                maxsett += 3;
                Hashtable categories = GetObject(dgvSiteCategories, i);

                categories["siteid"] = siteId.ToString();
                categories["sitecategoryid"] = maxsett.ToString();
                categories["publishedflag"] = "1";
                categories["publishid"] = "null";
                categories["pkid"] = maxsett.ToString();
                sql += DBScripting.SiteDB.Instance.GetSQL("site_category", categories, chkTimeStamp.Checked, TimeStamp, true, true);
                sql += sqlrollbacktran;

                if (i == dgvSiteCategories.Rows.Count - 2)
                {
                    string pksql = DBScripting.SiteDB.Instance.GetSQL("site_category_key", categories, chkTimeStamp.Checked, TimeStamp, true, true);
                    rtfKeyCategory.Text = pksql;
                }

            }
            sql += sqlcommittran;


            ShowPreviewForm(sql, "SiteCategory.sql");
        }

        private void btnGetSiteArtScripts_Click(object sender, EventArgs e)
        {
            int row = 0;
            int maxsett = 0;
            int siteId = 0;



            Int32.TryParse(txtNewSiteID.Text, out siteId);
            if (siteId <= 0)
            {
                MessageBox.Show("Please enter New SiteID");
                return;
            }


            Int32.TryParse(txtMaxSiteArticleID.Text, out maxsett);
            if (maxsett <= 0)
            {
                MessageBox.Show("Please enter current max Current Site Article ID, or press button \"Get Max PK ids from current database\"");
                return;
            }

            string sql = "";
            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";

            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            sql += sqlbegintran;
            for (int i = 0; i < dgvArticles.Rows.Count - 1; i++)
            {
                maxsett += 3;
                Hashtable categories = GetObject(dgvArticles, i);

                categories["siteid"] = siteId.ToString();
                categories["sitearticleid"] = maxsett.ToString();
                categories["publishedflag"] = "1";
                categories["fileid"] = "null";
                categories["publishid"] = "null";
                categories["memberid"] = "0";
                categories["pkid"] = maxsett.ToString();

                sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("site_article", categories, chkTimeStamp.Checked, TimeStamp, true, true);
                sql += sqlrollbacktran;

                if (i == dgvArticles.Rows.Count - 2)
                {
                    string pksql = DBScripting.SiteDB.Instance.GetSQL("site_article_key", categories, chkTimeStamp.Checked, TimeStamp, true, true);
                    rtfKeyCategory.Text += pksql;
                }

            }
            sql += sqlcommittran;


            ShowPreviewForm(sql, "SiteArticles.sql");
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void btnGetDeltaAttr_Click(object sender, EventArgs e)
        {
            try
            {
                int siteId = Int32.Parse(cboSites.Text);
                int newSiteId = Int32.Parse(txtNewSiteID.Text);

                int commid = ((Community)cboCommunity.SelectedValue).CommunityID;
                int newcommid = Int32.Parse(txtNewCommunityID.Text);

                int brandid = Int32.Parse(cboBrands.Text);
                int newbrandid = Int32.Parse(txtNewBrandID.Text);

                dgvDeltaAttrBrand.Rows.Clear();
                dgvDeltaAttrSite.Rows.Clear();
                dgvDeltaAttrComm.Rows.Clear();

                string uri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ContentServiceURI;

                string attruri = uri.ToLower().Replace("brandconfigsm", "AttributeMetadataSM");
                List<VO.AttributeGroup> attrComm = BL.AttributeBL.Instance.GetAttributes(commid, attruri, true);

                List<VO.AttributeGroup> attrNewComm = BL.AttributeBL.Instance.GetAttributes(newcommid, attruri, true);

                var deltacomm = (from a in attrComm select a.AttributeID).Except(from n in attrNewComm join a1 in attrComm on n.AttributeID equals a1.AttributeID select n.AttributeID);

                List<VO.AttributeGroup> attrCommDelta = new List<VO.AttributeGroup>();
                foreach (int i in deltacomm)
                {
                    int attrid = i;
                    VO.AttributeGroup attrgroup = (from a in attrComm where a.AttributeID == attrid select a).ToList<VO.AttributeGroup>()[0];
                    attrCommDelta.Add(attrgroup);

                }


                BL.WFormBL.Instance.populateGridFromPropertiesList<VO.AttributeGroup>(attrCommDelta, dgvDeltaAttrComm, 0, false);


             
                List<VO.AttributeGroup> attrSite = BL.AttributeBL.Instance.GetAttributes(siteId, attruri, true);

                List<VO.AttributeGroup> attrNewSite = BL.AttributeBL.Instance.GetAttributes(newSiteId, attruri, true);

                var delta = (from a in attrSite select a.AttributeID).Except(from n in attrNewSite join a1 in attrSite on n.AttributeID equals a1.AttributeID select n.AttributeID);
               
                 List<VO.AttributeGroup> attrDelta=new List<VO.AttributeGroup>();
                foreach (int i in delta)
                {
                    int attrid = i;
                    VO.AttributeGroup attrgroup = (from a in attrSite where a.AttributeID == attrid select a).ToList <VO.AttributeGroup>()[0];
                    attrDelta.Add(attrgroup);

                }

                BL.WFormBL.Instance.populateGridFromPropertiesList<VO.AttributeGroup>(attrDelta, dgvDeltaAttrSite, 0, false);


                List<VO.AttributeGroup> attrBrand = BL.AttributeBL.Instance.GetAttributes(brandid, attruri, true);

                List<VO.AttributeGroup> attrNewBrand = BL.AttributeBL.Instance.GetAttributes(newbrandid, attruri, true);

                var deltabrand = (from a in attrBrand select a.AttributeID).Except(from n in attrNewBrand join a1 in attrBrand on n.AttributeID equals a1.AttributeID select n.AttributeID);

                List<VO.AttributeGroup> attrDeltaBrand = new List<VO.AttributeGroup>();

                foreach (int i in deltabrand)
                {
                    int attrid = i;
                    VO.AttributeGroup attrgroup = (from a in attrBrand where a.AttributeID == attrid select a).ToList<VO.AttributeGroup>()[0];
                    attrDeltaBrand.Add(attrgroup);

                }

                BL.WFormBL.Instance.populateGridFromPropertiesList<VO.AttributeGroup>(attrDeltaBrand, dgvDeltaAttrBrand, 0, false);
            }
            catch (Exception ex)
            { MessageBox.Show(ex.ToString()); }
        }


        private string GetAttributeScripts(DataGridView dataGridView4, int groupid)
        {
            int row = 0;
            int maxattr = Int32.Parse(txtMaxAttributeGroupID.Text);

            string sql = "";

            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";

            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            sql += sqlbegintran;
            for (int i = 0; i < dataGridView4.Rows.Count - 1; i++)
            {
                Hashtable attrGroup = GetObject(dataGridView4, i);
                if (!chkNoNewPKS.Checked)
                {
                    maxattr = maxattr + 1;
                    attrGroup["attributegroupid"] = maxattr;
                }
                int oldValContainer = -1;
                Int32.TryParse(attrGroup["attributegroupidoldvaluecontainer"].ToString(), out oldValContainer);
                if (oldValContainer < 0)
                {
                    attrGroup["attributegroupidoldvaluecontainer"] = "null";
                }

                int defaultValue = -1;
                Int32.TryParse(attrGroup["defaultvalue"].ToString(), out defaultValue);
                if (defaultValue < 0)
                {
                    attrGroup["defaultvalue"] = "null";
                }
                else
                {
                    if (String.IsNullOrEmpty(attrGroup["defaultvalue"].ToString()))
                    {
                        attrGroup["defaultvalue"] = "null";
                    }

                }

                attrGroup["groupid"] =groupid;
                sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("attribute", attrGroup, chkTimeStamp.Checked, TimeStamp);
                sql += sqlrollbacktran;
            }


            txtMaxAttributeGroupID.Text = maxattr.ToString();

            sql += sqlcommittran;
            return sql;
        }

        private void btnGetDeltaScripts_Click(object sender, EventArgs e)
        {
            try
            {
             
                int newSiteId = Int32.Parse(txtNewSiteID.Text);
                            
                int newcommid = Int32.Parse(txtNewCommunityID.Text);

                int newbrandid = Int32.Parse(txtNewBrandID.Text);

                string sql="";
                sql += "--Community Attributes";
                sql += GetAttributeScripts(dgvDeltaAttrComm,newcommid);
                sql += "-- End Community Attributes\r\n";
                sql += "--Site Attributes\r\n";
                sql += GetAttributeScripts(dgvDeltaAttrSite,newSiteId);
                sql += "--End Site Attributes\r\n";
                sql += "--Brand Attributes\r\n";
                sql += GetAttributeScripts(dgvDeltaAttrBrand,newbrandid);
                sql += "--End Brand Attributes\r\n";

                ShowPreviewForm(sql, "AttributeGroupDelta.sql");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string sql = "";
            string sqlbegintran = "";
            string sqlcommittran = "";
            string sqlrollbacktran = "";
            int newSiteId = Int32.Parse(txtNewSiteID.Text);
            if (chkUseTran.Checked)
            {
                sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

            }
            sql += sqlbegintran;
            for (int i = 0; i < dgvDeltaSitePages.Rows.Count - 1; i++)
            {
                Hashtable pages = GetObject(dgvDeltaSitePages, i);

                pages["siteid"] = newSiteId;
                sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("sitepages", pages, chkTimeStamp.Checked, TimeStamp);
                sql += sqlrollbacktran;
            }

            sql += sqlcommittran;
            ShowPreviewForm(sql, "SitePagesDelta.sql");
        }

        private void btnGetDeltaSitePages_Click(object sender, EventArgs e)
        {
            try
            {
                int siteId = Int32.Parse(cboSites.Text);
                int newSiteId = Int32.Parse(txtNewSiteID.Text);


                List<VO.SitePage> listPagesOld = SiteBL.Instance.GetSitePages(siteId);
                List<VO.SitePage> listPagesNew = SiteBL.Instance.GetSitePages(newSiteId);

                var delta = (from o in listPagesOld select o.PageID).Except(from n in listPagesNew join o1 in listPagesOld on n.PageID equals o1.PageID select o1.PageID);

                List<VO.SitePage> listDelta = new List<VO.SitePage>();

                foreach (int i in delta)
                {

                    VO.SitePage sitepage = (from p in listPagesOld where p.PageID == i select p).ToList<VO.SitePage>()[0];
                    listDelta.Add(sitepage);

                }

                listDelta.Sort(delegate(VO.SitePage p1, VO.SitePage p2) { return p1.AppID.CompareTo(p2.AppID); });
                DataGridViewColumn cellCheck8 = new DataGridViewCheckBoxColumn();
                dgvDeltaSitePages.Columns.Insert(0, cellCheck8);
                dgvDeltaSitePages.Columns[0].HeaderText = "Select";
                BL.WFormBL.Instance.populateGridFromPropertiesList<VO.SitePage>(listDelta, dgvDeltaSitePages, 1, true);


            }
            catch (Exception ex)
            { MessageBox.Show(ex.ToString()); }

        }

        private void dgvDeltaSiteSetting_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                int siteId = Int32.Parse(cboSites.Text);
                int newSiteId = Int32.Parse(txtNewSiteID.Text);
               
                string configuri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ConfigServiceURI;
                List<VO.Setting> settingsOld = SiteBL.Instance.GetSettings(siteId, configuri);
                List<VO.Setting> settingsNew = SiteBL.Instance.GetSettings(newSiteId, configuri);

                var delta = (from o in settingsOld select o.settingID).Except(from n in settingsNew join o1 in settingsOld on n.settingID equals o1.settingID select o1.settingID);

                List<VO.Setting> listDelta = new List<VO.Setting>();

                foreach (int i in delta)
                {

                    VO.Setting sett = (from p in settingsOld where p.settingID == i select p).ToList<VO.Setting>()[0];
                    listDelta.Add(sett);

                }
                listDelta.Sort(delegate(VO.Setting s1, VO.Setting s2) { return s1.CompareTo(s2); });

                dgvDeltaSiteSetting.ColumnCount = 4;

                DataGridViewColumn cellCheck0 = new DataGridViewCheckBoxColumn();
                dgvDeltaSiteSetting.Columns.Insert(0, cellCheck0);
                dgvDeltaSiteSetting.Columns[0].HeaderText = "Select";
                dgvDeltaSiteSetting.Columns[1].HeaderText = "SettingID";
                dgvDeltaSiteSetting.Columns[1].Width = dgvDeltaSiteSetting.Width / 3;
                dgvDeltaSiteSetting.Columns[2].HeaderText = "SettingConstant";
                dgvDeltaSiteSetting.Columns[2].Width = dgvDeltaSiteSetting.Width / 3;
                dgvDeltaSiteSetting.Columns[3].HeaderText = "SiteSettingValue";
                dgvDeltaSiteSetting.Columns[3].Width = dgvDeltaSiteSetting.Width / 3;

                foreach (VO.Setting s in listDelta)
                {
                    dgvDeltaSiteSetting.Rows.Add(false, s.settingID, s.settingConst, s.settingValue);

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnGetDeltaSettingScripts_Click(object sender, EventArgs e)
        {
            try
            {
                int row = 0;
                int maxsett = 0;

                string sql = "";
                string sqlbegintran = "";
                string sqlcommittran = "";
                string sqlrollbacktran = "";

                if (chkUseTran.Checked)
                {
                    sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                    sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                    sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

                }
                sql += sqlbegintran;
                for (int i = 0; i < dgvDeltaSiteSetting.Rows.Count - 1; i++)
                {
                    Hashtable setting = GetObject(dgvDeltaSiteSetting, i);
                    if (Boolean.Parse(dgvDeltaSiteSetting.Rows[i].Cells[0].Value.ToString()))
                    {



                        setting["siteid"] = Int32.Parse(txtNewSiteID.Text);
                        if (chkGetDynamic.Checked)
                        {
                            setting["sitesettingid"] = "@sitesettingid";
                            sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("sitesettings_dynamic", setting, chkTimeStamp.Checked, TimeStamp);
                            sql += sqlrollbacktran;

                        }
                        else
                        {

                            // setting["sitesettingid"] = maxsett + i + 1;
                            maxsett = Int32.Parse(txtMaxSiteSettingID.Text);
                            setting["sitesettingid"] = maxsett + i + 1;
                            sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("sitesettings", setting, chkTimeStamp.Checked, TimeStamp);
                            sql += sqlrollbacktran;

                        }
                    }
                }
                sql += sqlcommittran;
                if (chkGetDynamic.Checked)
                {

                    string sqldeclare = DBScripting.SiteDB.Instance.GetSQLTemplate("sitesettings_dynamic_declare");
                    sql = sqldeclare + "\r\n" + sql;


                }

                ShowPreviewForm(sql, "SiteSettingDelta.sql");

            }
            catch (Exception ex)
            { MessageBox.Show(ex.ToString()); }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            ShowArticlesAndCategories();
        }

        private void btnBrowseCategoriesFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtArticleCategories.Text = openFileDialog1.FileName;
        }
        private void GetAttributeOptions()
        {
            try
            {
                int row = 0;
                int maxattr = Int32.Parse(txtAttrOptionMaxId.Text);

                string sql = "";
                string sqlbegintran = "";
                string sqlcommittran = "";
                string sqlrollbacktran = "";

                if (chkUseTran.Checked)
                {
                    sqlbegintran = DBScripting.SiteDB.Instance.GetSQLTemplate("begin_tran");
                    sqlcommittran = DBScripting.SiteDB.Instance.GetSQLTemplate("commit_tran");
                    sqlrollbacktran = DBScripting.SiteDB.Instance.GetSQLTemplate("single_sql_rollback");

                }
                string sqldeclare = DBScripting.SiteDB.Instance.GetSQLTemplate("attributeoption_dynamic_declare");
                sql += sqldeclare;
                sql += sqlbegintran;

                for (int i = 0; i < dgvCommAttrOptions.Rows.Count - 1; i++)
                {
                    Hashtable attrGroup = GetObject(dgvCommAttrOptions, i);
                    maxattr += 1;
                    attrGroup["attributeoptiongroupid"] = maxattr;
                    attrGroup["groupid"] = Int32.Parse(txtNewCommunityID.Text);
                    sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("attributeoption", attrGroup, chkTimeStamp.Checked, TimeStamp);
                    sql += sqlrollbacktran;
                }
                for (int i = 0; i < dataGridView6.Rows.Count - 1; i++)
                {
                    Hashtable attrGroup = GetObject(dataGridView6, i);
                    maxattr += 1;
                    attrGroup["attributeoptiongroupid"] = maxattr;
                    attrGroup["groupid"] = Int32.Parse(txtNewSiteID.Text);
                    sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("attributeoption", attrGroup, chkTimeStamp.Checked, TimeStamp);
                    sql += sqlrollbacktran;
                }


                for (int i = 0; i < dataGridView7.Rows.Count - 1; i++)
                {
                    Hashtable attrGroup = GetObject(dataGridView7, i);
                    maxattr += 1;
                    attrGroup["attributeoptiongroupid"] = maxattr;
                    attrGroup["groupid"] = Int32.Parse(txtNewBrandID.Text);
                    sql += "\r\n" + DBScripting.SiteDB.Instance.GetSQL("attributeoption", attrGroup, chkTimeStamp.Checked, TimeStamp);
                    sql += sqlrollbacktran;
                }
                sql += sqlcommittran;
                txtAttrOptionMaxId.Text = maxattr.ToString();
                ShowPreviewForm(sql, "AttributeOptionGroup.sql");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

     
    }
}
