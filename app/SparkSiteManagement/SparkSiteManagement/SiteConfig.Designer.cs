﻿namespace SparkSiteManagement
{
    partial class SiteConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabSettings = new System.Windows.Forms.TabControl();
            this.tabConfig = new System.Windows.Forms.TabPage();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.dgvCommunity = new System.Windows.Forms.DataGridView();
            this.btnGetSiteScripts = new System.Windows.Forms.Button();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabSitePages = new System.Windows.Forms.TabPage();
            this.btnGetSitePages = new System.Windows.Forms.Button();
            this.dataGridView8 = new System.Windows.Forms.DataGridView();
            this.tabSiteSettings = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.chkGetDynamic = new System.Windows.Forms.CheckBox();
            this.btnSiteSettingSelect = new System.Windows.Forms.Button();
            this.btnGetSiteSettings = new System.Windows.Forms.Button();
            this.txtMaxSiteSettingID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabCommunityAttr = new System.Windows.Forms.TabPage();
            this.dgvCommAttrOptions = new System.Windows.Forms.DataGridView();
            this.btnGetCommAttrScripts = new System.Windows.Forms.Button();
            this.dgvCommAttr = new System.Windows.Forms.DataGridView();
            this.tabAttributes = new System.Windows.Forms.TabPage();
            this.btnGetAttrScript = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnGetBrandAttributes = new System.Windows.Forms.Button();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.tabAttrOptions = new System.Windows.Forms.TabPage();
            this.btnGetAttrOptionScript = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.btnBrandAttrOption = new System.Windows.Forms.Button();
            this.dataGridView7 = new System.Windows.Forms.DataGridView();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.tabRegPromos = new System.Windows.Forms.TabPage();
            this.txtMaxPromoID = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.dataGridView10 = new System.Windows.Forms.DataGridView();
            this.tabList = new System.Windows.Forms.TabPage();
            this.btnGetListCatScript = new System.Windows.Forms.Button();
            this.dataGridView9 = new System.Windows.Forms.DataGridView();
            this.tabTease = new System.Windows.Forms.TabPage();
            this.btnGetObjScript = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.txtMaxObjGroupID = new System.Windows.Forms.TextBox();
            this.dataGridView11 = new System.Windows.Forms.DataGridView();
            this.tabSparkWS = new System.Windows.Forms.TabPage();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.btnGetProfileSectionScripts = new System.Windows.Forms.Button();
            this.dgvPSSite = new System.Windows.Forms.DataGridView();
            this.dgvPSCOmm = new System.Windows.Forms.DataGridView();
            this.dgvPSPhoto = new System.Windows.Forms.DataGridView();
            this.dgvPSAttr = new System.Windows.Forms.DataGridView();
            this.tabCCTypes = new System.Windows.Forms.TabPage();
            this.btnGetCCTypesScripts = new System.Windows.Forms.Button();
            this.txtMaxCCTypeGroupID = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.dgvCCTypes = new System.Windows.Forms.DataGridView();
            this.tabPhotoSearch = new System.Windows.Forms.TabPage();
            this.btnPhotoScripts = new System.Windows.Forms.Button();
            this.dgvPhotoSearch = new System.Windows.Forms.DataGridView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.rtfTemplatesKey = new System.Windows.Forms.RichTextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.cmdGetResTemplScripts = new System.Windows.Forms.Button();
            this.txtCurrResTemplID = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.btnGetResTemplates = new System.Windows.Forms.Button();
            this.dgvResTemplates = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtMaxPropertyID = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.btnYNMPartitions = new System.Windows.Forms.Button();
            this.btnGetYNMSP = new System.Windows.Forms.Button();
            this.btnGetYNMTables = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.lboProcedures = new System.Windows.Forms.ListBox();
            this.label29 = new System.Windows.Forms.Label();
            this.lboTables = new System.Windows.Forms.ListBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtPartitions = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button6 = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.txtArticleCategories = new System.Windows.Forms.TextBox();
            this.rtfKeyCategory = new System.Windows.Forms.RichTextBox();
            this.btnGetSiteArtScripts = new System.Windows.Forms.Button();
            this.btnGetSiteCategoryScrits = new System.Windows.Forms.Button();
            this.lblArticlesCount = new System.Windows.Forms.Label();
            this.dgvArticles = new System.Windows.Forms.DataGridView();
            this.dgvSiteCategories = new System.Windows.Forms.DataGridView();
            this.label34 = new System.Windows.Forms.Label();
            this.txtMaxSiteCategoryID = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtMaxSiteArticleID = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.btnGetDeltaScripts = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.btnGetDeltaAttr = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.dgvDeltaAttrComm = new System.Windows.Forms.DataGridView();
            this.label36 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.dgvDeltaAttrBrand = new System.Windows.Forms.DataGridView();
            this.dgvDeltaAttrSite = new System.Windows.Forms.DataGridView();
            this.btnGetDeltaSettings = new System.Windows.Forms.TabPage();
            this.btnGetDeltaSettingScripts = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.dgvDeltaSiteSetting = new System.Windows.Forms.DataGridView();
            this.button5 = new System.Windows.Forms.Button();
            this.btnGetDeltaSitePages = new System.Windows.Forms.Button();
            this.dgvDeltaSitePages = new System.Windows.Forms.DataGridView();
            this.txtAttrOptionMaxId = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cboBrands = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboEnv = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNewSiteID = new System.Windows.Forms.TextBox();
            this.txtNewBrandID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNewSiteGroupName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNewBrandGroupName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.cboCommunity = new System.Windows.Forms.ComboBox();
            this.cmdCommunitySites = new System.Windows.Forms.Button();
            this.cboSites = new System.Windows.Forms.ComboBox();
            this.txtNewCommunityName = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtNewCommunityID = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.chkTimeStamp = new System.Windows.Forms.CheckBox();
            this.txtMaxAttributeGroupID = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chkUseTran = new System.Windows.Forms.CheckBox();
            this.txtTimeStamp = new System.Windows.Forms.TextBox();
            this.btnRollback = new System.Windows.Forms.Button();
            this.btnMaxIds = new System.Windows.Forms.Button();
            this.chkNoNewPKS = new System.Windows.Forms.CheckBox();
            this.btnBrowseCategoriesFile = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.tabSettings.SuspendLayout();
            this.tabConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommunity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabSitePages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).BeginInit();
            this.tabSiteSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabCommunityAttr.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommAttrOptions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommAttr)).BeginInit();
            this.tabAttributes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.tabAttrOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            this.tabRegPromos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView10)).BeginInit();
            this.tabList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView9)).BeginInit();
            this.tabTease.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView11)).BeginInit();
            this.tabSparkWS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPSSite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPSCOmm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPSPhoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPSAttr)).BeginInit();
            this.tabCCTypes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCCTypes)).BeginInit();
            this.tabPhotoSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPhotoSearch)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResTemplates)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSiteCategories)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeltaAttrComm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeltaAttrBrand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeltaAttrSite)).BeginInit();
            this.btnGetDeltaSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeltaSiteSetting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeltaSitePages)).BeginInit();
            this.SuspendLayout();
            // 
            // tabSettings
            // 
            this.tabSettings.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabSettings.Controls.Add(this.tabConfig);
            this.tabSettings.Controls.Add(this.tabSitePages);
            this.tabSettings.Controls.Add(this.tabSiteSettings);
            this.tabSettings.Controls.Add(this.tabCommunityAttr);
            this.tabSettings.Controls.Add(this.tabAttributes);
            this.tabSettings.Controls.Add(this.tabAttrOptions);
            this.tabSettings.Controls.Add(this.tabRegPromos);
            this.tabSettings.Controls.Add(this.tabList);
            this.tabSettings.Controls.Add(this.tabTease);
            this.tabSettings.Controls.Add(this.tabSparkWS);
            this.tabSettings.Controls.Add(this.tabCCTypes);
            this.tabSettings.Controls.Add(this.tabPhotoSearch);
            this.tabSettings.Controls.Add(this.tabPage1);
            this.tabSettings.Controls.Add(this.tabPage2);
            this.tabSettings.Controls.Add(this.tabPage3);
            this.tabSettings.Controls.Add(this.tabPage4);
            this.tabSettings.Controls.Add(this.btnGetDeltaSettings);
            this.tabSettings.Location = new System.Drawing.Point(16, 179);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.SelectedIndex = 0;
            this.tabSettings.Size = new System.Drawing.Size(1180, 793);
            this.tabSettings.TabIndex = 0;
            // 
            // tabConfig
            // 
            this.tabConfig.Controls.Add(this.label18);
            this.tabConfig.Controls.Add(this.label17);
            this.tabConfig.Controls.Add(this.label16);
            this.tabConfig.Controls.Add(this.dgvCommunity);
            this.tabConfig.Controls.Add(this.btnGetSiteScripts);
            this.tabConfig.Controls.Add(this.dataGridView3);
            this.tabConfig.Controls.Add(this.dataGridView1);
            this.tabConfig.Location = new System.Drawing.Point(4, 25);
            this.tabConfig.Name = "tabConfig";
            this.tabConfig.Padding = new System.Windows.Forms.Padding(3);
            this.tabConfig.Size = new System.Drawing.Size(1172, 764);
            this.tabConfig.TabIndex = 0;
            this.tabConfig.Text = "Config";
            this.tabConfig.UseVisualStyleBackColor = true;
            this.tabConfig.Click += new System.EventHandler(this.tabConfig_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(16, 282);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(38, 13);
            this.label18.TabIndex = 11;
            this.label18.Text = "Brand:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(21, 168);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(28, 13);
            this.label17.TabIndex = 10;
            this.label17.Text = "Site:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(21, 65);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 13);
            this.label16.TabIndex = 9;
            this.label16.Text = "Community:";
            // 
            // dgvCommunity
            // 
            this.dgvCommunity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCommunity.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvCommunity.Location = new System.Drawing.Point(16, 94);
            this.dgvCommunity.Name = "dgvCommunity";
            this.dgvCommunity.Size = new System.Drawing.Size(546, 62);
            this.dgvCommunity.TabIndex = 8;
            this.dgvCommunity.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCommunity_CellContentClick);
            // 
            // btnGetSiteScripts
            // 
            this.btnGetSiteScripts.Location = new System.Drawing.Point(16, 29);
            this.btnGetSiteScripts.Name = "btnGetSiteScripts";
            this.btnGetSiteScripts.Size = new System.Drawing.Size(295, 26);
            this.btnGetSiteScripts.TabIndex = 7;
            this.btnGetSiteScripts.Text = "Get Community/Site/Brand Script";
            this.btnGetSiteScripts.UseVisualStyleBackColor = true;
            this.btnGetSiteScripts.Click += new System.EventHandler(this.btnGetSiteScripts_Click);
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView3.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridView3.Location = new System.Drawing.Point(16, 309);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(1137, 445);
            this.dataGridView3.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridView1.Location = new System.Drawing.Point(16, 196);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1137, 68);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // tabSitePages
            // 
            this.tabSitePages.Controls.Add(this.btnGetSitePages);
            this.tabSitePages.Controls.Add(this.dataGridView8);
            this.tabSitePages.Location = new System.Drawing.Point(4, 25);
            this.tabSitePages.Name = "tabSitePages";
            this.tabSitePages.Size = new System.Drawing.Size(1172, 764);
            this.tabSitePages.TabIndex = 7;
            this.tabSitePages.Text = "Site Pages";
            this.tabSitePages.UseVisualStyleBackColor = true;
            // 
            // btnGetSitePages
            // 
            this.btnGetSitePages.Location = new System.Drawing.Point(28, 17);
            this.btnGetSitePages.Name = "btnGetSitePages";
            this.btnGetSitePages.Size = new System.Drawing.Size(137, 26);
            this.btnGetSitePages.TabIndex = 8;
            this.btnGetSitePages.Text = "Get Site Pages Script";
            this.btnGetSitePages.UseVisualStyleBackColor = true;
            this.btnGetSitePages.Click += new System.EventHandler(this.btnGetSitePages_Click);
            // 
            // dataGridView8
            // 
            this.dataGridView8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView8.Location = new System.Drawing.Point(26, 65);
            this.dataGridView8.Name = "dataGridView8";
            this.dataGridView8.Size = new System.Drawing.Size(1016, 528);
            this.dataGridView8.TabIndex = 0;
            this.dataGridView8.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView8_CellContentClick);
            // 
            // tabSiteSettings
            // 
            this.tabSiteSettings.Controls.Add(this.button3);
            this.tabSiteSettings.Controls.Add(this.chkGetDynamic);
            this.tabSiteSettings.Controls.Add(this.btnSiteSettingSelect);
            this.tabSiteSettings.Controls.Add(this.btnGetSiteSettings);
            this.tabSiteSettings.Controls.Add(this.txtMaxSiteSettingID);
            this.tabSiteSettings.Controls.Add(this.label8);
            this.tabSiteSettings.Controls.Add(this.dataGridView2);
            this.tabSiteSettings.Location = new System.Drawing.Point(4, 25);
            this.tabSiteSettings.Name = "tabSiteSettings";
            this.tabSiteSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabSiteSettings.Size = new System.Drawing.Size(1172, 764);
            this.tabSiteSettings.TabIndex = 1;
            this.tabSiteSettings.Text = "Site Settings";
            this.tabSiteSettings.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(867, 13);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(191, 26);
            this.button3.TabIndex = 14;
            this.button3.Text = "Get  Update Site Setting Scripts";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // chkGetDynamic
            // 
            this.chkGetDynamic.AutoSize = true;
            this.chkGetDynamic.Location = new System.Drawing.Point(350, 15);
            this.chkGetDynamic.Name = "chkGetDynamic";
            this.chkGetDynamic.Size = new System.Drawing.Size(152, 17);
            this.chkGetDynamic.TabIndex = 13;
            this.chkGetDynamic.Text = "Dynamically get SettingsID";
            this.chkGetDynamic.UseVisualStyleBackColor = true;
            // 
            // btnSiteSettingSelect
            // 
            this.btnSiteSettingSelect.Location = new System.Drawing.Point(517, 16);
            this.btnSiteSettingSelect.Name = "btnSiteSettingSelect";
            this.btnSiteSettingSelect.Size = new System.Drawing.Size(111, 20);
            this.btnSiteSettingSelect.TabIndex = 12;
            this.btnSiteSettingSelect.Text = "Select All";
            this.btnSiteSettingSelect.UseVisualStyleBackColor = true;
            this.btnSiteSettingSelect.Click += new System.EventHandler(this.btnSiteSettingSelect_Click);
            // 
            // btnGetSiteSettings
            // 
            this.btnGetSiteSettings.Location = new System.Drawing.Point(646, 13);
            this.btnGetSiteSettings.Name = "btnGetSiteSettings";
            this.btnGetSiteSettings.Size = new System.Drawing.Size(191, 26);
            this.btnGetSiteSettings.TabIndex = 11;
            this.btnGetSiteSettings.Text = "Get Site Settings Scripts";
            this.btnGetSiteSettings.UseVisualStyleBackColor = true;
            this.btnGetSiteSettings.Click += new System.EventHandler(this.btnGetSiteSettings_Click);
            // 
            // txtMaxSiteSettingID
            // 
            this.txtMaxSiteSettingID.Location = new System.Drawing.Point(190, 13);
            this.txtMaxSiteSettingID.Name = "txtMaxSiteSettingID";
            this.txtMaxSiteSettingID.Size = new System.Drawing.Size(130, 20);
            this.txtMaxSiteSettingID.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(34, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(129, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Current Max SiteSettingID";
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(37, 50);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(1100, 719);
            this.dataGridView2.TabIndex = 0;
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            // 
            // tabCommunityAttr
            // 
            this.tabCommunityAttr.Controls.Add(this.dgvCommAttrOptions);
            this.tabCommunityAttr.Controls.Add(this.btnGetCommAttrScripts);
            this.tabCommunityAttr.Controls.Add(this.dgvCommAttr);
            this.tabCommunityAttr.Location = new System.Drawing.Point(4, 25);
            this.tabCommunityAttr.Name = "tabCommunityAttr";
            this.tabCommunityAttr.Size = new System.Drawing.Size(1172, 764);
            this.tabCommunityAttr.TabIndex = 10;
            this.tabCommunityAttr.Text = "Community Attributes";
            this.tabCommunityAttr.UseVisualStyleBackColor = true;
            // 
            // dgvCommAttrOptions
            // 
            this.dgvCommAttrOptions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCommAttrOptions.Location = new System.Drawing.Point(24, 410);
            this.dgvCommAttrOptions.Name = "dgvCommAttrOptions";
            this.dgvCommAttrOptions.Size = new System.Drawing.Size(1131, 352);
            this.dgvCommAttrOptions.TabIndex = 2;
            // 
            // btnGetCommAttrScripts
            // 
            this.btnGetCommAttrScripts.Location = new System.Drawing.Point(33, 13);
            this.btnGetCommAttrScripts.Name = "btnGetCommAttrScripts";
            this.btnGetCommAttrScripts.Size = new System.Drawing.Size(141, 35);
            this.btnGetCommAttrScripts.TabIndex = 1;
            this.btnGetCommAttrScripts.Text = "Get Community Attributes Scripts";
            this.btnGetCommAttrScripts.UseVisualStyleBackColor = true;
            this.btnGetCommAttrScripts.Click += new System.EventHandler(this.btnGetCommAttrScripts_Click);
            // 
            // dgvCommAttr
            // 
            this.dgvCommAttr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCommAttr.Location = new System.Drawing.Point(24, 63);
            this.dgvCommAttr.Name = "dgvCommAttr";
            this.dgvCommAttr.Size = new System.Drawing.Size(1131, 316);
            this.dgvCommAttr.TabIndex = 0;
            // 
            // tabAttributes
            // 
            this.tabAttributes.Controls.Add(this.btnGetAttrScript);
            this.tabAttributes.Controls.Add(this.label3);
            this.tabAttributes.Controls.Add(this.btnGetBrandAttributes);
            this.tabAttributes.Controls.Add(this.dataGridView5);
            this.tabAttributes.Controls.Add(this.dataGridView4);
            this.tabAttributes.Location = new System.Drawing.Point(4, 25);
            this.tabAttributes.Name = "tabAttributes";
            this.tabAttributes.Size = new System.Drawing.Size(1172, 764);
            this.tabAttributes.TabIndex = 3;
            this.tabAttributes.Text = "Attributes";
            this.tabAttributes.UseVisualStyleBackColor = true;
            // 
            // btnGetAttrScript
            // 
            this.btnGetAttrScript.Location = new System.Drawing.Point(906, 18);
            this.btnGetAttrScript.Name = "btnGetAttrScript";
            this.btnGetAttrScript.Size = new System.Drawing.Size(173, 26);
            this.btnGetAttrScript.TabIndex = 13;
            this.btnGetAttrScript.Text = "Get Attr Group Script";
            this.btnGetAttrScript.UseVisualStyleBackColor = true;
            this.btnGetAttrScript.Click += new System.EventHandler(this.btnGetAttrScript_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Site Scope Attributes";
            // 
            // btnGetBrandAttributes
            // 
            this.btnGetBrandAttributes.Location = new System.Drawing.Point(21, 378);
            this.btnGetBrandAttributes.Name = "btnGetBrandAttributes";
            this.btnGetBrandAttributes.Size = new System.Drawing.Size(119, 33);
            this.btnGetBrandAttributes.TabIndex = 2;
            this.btnGetBrandAttributes.Text = "Get Brand Attributes";
            this.btnGetBrandAttributes.UseVisualStyleBackColor = true;
            this.btnGetBrandAttributes.Click += new System.EventHandler(this.btnGetBrandAttributes_Click);
            // 
            // dataGridView5
            // 
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Location = new System.Drawing.Point(20, 417);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.Size = new System.Drawing.Size(1108, 364);
            this.dataGridView5.TabIndex = 1;
            // 
            // dataGridView4
            // 
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(20, 69);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(1108, 296);
            this.dataGridView4.TabIndex = 0;
            // 
            // tabAttrOptions
            // 
            this.tabAttrOptions.Controls.Add(this.btnGetAttrOptionScript);
            this.tabAttrOptions.Controls.Add(this.label12);
            this.tabAttrOptions.Controls.Add(this.btnBrandAttrOption);
            this.tabAttrOptions.Controls.Add(this.dataGridView7);
            this.tabAttrOptions.Controls.Add(this.dataGridView6);
            this.tabAttrOptions.Location = new System.Drawing.Point(4, 25);
            this.tabAttrOptions.Name = "tabAttrOptions";
            this.tabAttrOptions.Size = new System.Drawing.Size(1172, 764);
            this.tabAttrOptions.TabIndex = 6;
            this.tabAttrOptions.Text = "AttributeOptions";
            this.tabAttrOptions.UseVisualStyleBackColor = true;
            // 
            // btnGetAttrOptionScript
            // 
            this.btnGetAttrOptionScript.Location = new System.Drawing.Point(891, 27);
            this.btnGetAttrOptionScript.Name = "btnGetAttrOptionScript";
            this.btnGetAttrOptionScript.Size = new System.Drawing.Size(173, 26);
            this.btnGetAttrOptionScript.TabIndex = 17;
            this.btnGetAttrOptionScript.Text = "Get Attr Option Group Script";
            this.btnGetAttrOptionScript.UseVisualStyleBackColor = true;
            this.btnGetAttrOptionScript.Click += new System.EventHandler(this.btnGetAttrOptionScript_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(25, 49);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(137, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "Site Scope AttributeOptions";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // btnBrandAttrOption
            // 
            this.btnBrandAttrOption.Location = new System.Drawing.Point(32, 349);
            this.btnBrandAttrOption.Name = "btnBrandAttrOption";
            this.btnBrandAttrOption.Size = new System.Drawing.Size(186, 33);
            this.btnBrandAttrOption.TabIndex = 3;
            this.btnBrandAttrOption.Text = "Get Brand Attribute Options";
            this.btnBrandAttrOption.UseVisualStyleBackColor = true;
            this.btnBrandAttrOption.Click += new System.EventHandler(this.btnBrandAttrOption_Click);
            // 
            // dataGridView7
            // 
            this.dataGridView7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView7.Location = new System.Drawing.Point(32, 383);
            this.dataGridView7.Name = "dataGridView7";
            this.dataGridView7.Size = new System.Drawing.Size(1032, 318);
            this.dataGridView7.TabIndex = 1;
            // 
            // dataGridView6
            // 
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView6.Location = new System.Drawing.Point(32, 74);
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.Size = new System.Drawing.Size(1032, 269);
            this.dataGridView6.TabIndex = 0;
            // 
            // tabRegPromos
            // 
            this.tabRegPromos.Controls.Add(this.txtMaxPromoID);
            this.tabRegPromos.Controls.Add(this.label19);
            this.tabRegPromos.Controls.Add(this.button2);
            this.tabRegPromos.Controls.Add(this.dataGridView10);
            this.tabRegPromos.Location = new System.Drawing.Point(4, 25);
            this.tabRegPromos.Name = "tabRegPromos";
            this.tabRegPromos.Size = new System.Drawing.Size(1172, 764);
            this.tabRegPromos.TabIndex = 9;
            this.tabRegPromos.Text = "Registration Promos";
            this.tabRegPromos.UseVisualStyleBackColor = true;
            // 
            // txtMaxPromoID
            // 
            this.txtMaxPromoID.Location = new System.Drawing.Point(223, 11);
            this.txtMaxPromoID.Name = "txtMaxPromoID";
            this.txtMaxPromoID.Size = new System.Drawing.Size(130, 20);
            this.txtMaxPromoID.TabIndex = 25;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(29, 14);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(167, 13);
            this.label19.TabIndex = 26;
            this.label19.Text = "Current Max Registration PromoID";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(479, 14);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(141, 24);
            this.button2.TabIndex = 2;
            this.button2.Text = "Get Scripts";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dataGridView10
            // 
            this.dataGridView10.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView10.Location = new System.Drawing.Point(32, 47);
            this.dataGridView10.Name = "dataGridView10";
            this.dataGridView10.Size = new System.Drawing.Size(1018, 508);
            this.dataGridView10.TabIndex = 0;
            // 
            // tabList
            // 
            this.tabList.Controls.Add(this.btnGetListCatScript);
            this.tabList.Controls.Add(this.dataGridView9);
            this.tabList.Location = new System.Drawing.Point(4, 25);
            this.tabList.Name = "tabList";
            this.tabList.Size = new System.Drawing.Size(1172, 764);
            this.tabList.TabIndex = 2;
            this.tabList.Text = "List Categories";
            this.tabList.UseVisualStyleBackColor = true;
            // 
            // btnGetListCatScript
            // 
            this.btnGetListCatScript.Location = new System.Drawing.Point(45, 16);
            this.btnGetListCatScript.Name = "btnGetListCatScript";
            this.btnGetListCatScript.Size = new System.Drawing.Size(139, 24);
            this.btnGetListCatScript.TabIndex = 1;
            this.btnGetListCatScript.Text = "Get Scripts";
            this.btnGetListCatScript.UseVisualStyleBackColor = true;
            this.btnGetListCatScript.Click += new System.EventHandler(this.btnGetListCatScript_Click);
            // 
            // dataGridView9
            // 
            this.dataGridView9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView9.Location = new System.Drawing.Point(45, 58);
            this.dataGridView9.Name = "dataGridView9";
            this.dataGridView9.Size = new System.Drawing.Size(1095, 458);
            this.dataGridView9.TabIndex = 0;
            // 
            // tabTease
            // 
            this.tabTease.Controls.Add(this.btnGetObjScript);
            this.tabTease.Controls.Add(this.label24);
            this.tabTease.Controls.Add(this.txtMaxObjGroupID);
            this.tabTease.Controls.Add(this.dataGridView11);
            this.tabTease.Location = new System.Drawing.Point(4, 25);
            this.tabTease.Name = "tabTease";
            this.tabTease.Size = new System.Drawing.Size(1172, 764);
            this.tabTease.TabIndex = 5;
            this.tabTease.Text = "Tease/Obj. Groups";
            this.tabTease.UseVisualStyleBackColor = true;
            // 
            // btnGetObjScript
            // 
            this.btnGetObjScript.Location = new System.Drawing.Point(345, 16);
            this.btnGetObjScript.Name = "btnGetObjScript";
            this.btnGetObjScript.Size = new System.Drawing.Size(154, 33);
            this.btnGetObjScript.TabIndex = 3;
            this.btnGetObjScript.Text = "Get Script";
            this.btnGetObjScript.UseVisualStyleBackColor = true;
            this.btnGetObjScript.Click += new System.EventHandler(this.btnGetObjScript_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(39, 24);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(107, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Max Object Group ID";
            // 
            // txtMaxObjGroupID
            // 
            this.txtMaxObjGroupID.Location = new System.Drawing.Point(159, 21);
            this.txtMaxObjGroupID.Name = "txtMaxObjGroupID";
            this.txtMaxObjGroupID.Size = new System.Drawing.Size(102, 20);
            this.txtMaxObjGroupID.TabIndex = 1;
            // 
            // dataGridView11
            // 
            this.dataGridView11.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView11.Location = new System.Drawing.Point(35, 72);
            this.dataGridView11.Name = "dataGridView11";
            this.dataGridView11.Size = new System.Drawing.Size(1113, 460);
            this.dataGridView11.TabIndex = 0;
            // 
            // tabSparkWS
            // 
            this.tabSparkWS.Controls.Add(this.label23);
            this.tabSparkWS.Controls.Add(this.label22);
            this.tabSparkWS.Controls.Add(this.label21);
            this.tabSparkWS.Controls.Add(this.label20);
            this.tabSparkWS.Controls.Add(this.btnGetProfileSectionScripts);
            this.tabSparkWS.Controls.Add(this.dgvPSSite);
            this.tabSparkWS.Controls.Add(this.dgvPSCOmm);
            this.tabSparkWS.Controls.Add(this.dgvPSPhoto);
            this.tabSparkWS.Controls.Add(this.dgvPSAttr);
            this.tabSparkWS.Location = new System.Drawing.Point(4, 25);
            this.tabSparkWS.Name = "tabSparkWS";
            this.tabSparkWS.Size = new System.Drawing.Size(1172, 764);
            this.tabSparkWS.TabIndex = 8;
            this.tabSparkWS.Text = "SparkWS Profile Sections";
            this.tabSparkWS.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(794, 404);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(106, 13);
            this.label23.TabIndex = 8;
            this.label23.Text = "Profile Section Photo";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(401, 404);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(96, 13);
            this.label22.TabIndex = 7;
            this.label22.Text = "Profile Section Site";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(22, 404);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(129, 13);
            this.label21.TabIndex = 6;
            this.label21.Text = "Profile Section Community";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(39, 17);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(117, 13);
            this.label20.TabIndex = 5;
            this.label20.Text = "Profile Section Attribute";
            // 
            // btnGetProfileSectionScripts
            // 
            this.btnGetProfileSectionScripts.Location = new System.Drawing.Point(486, 12);
            this.btnGetProfileSectionScripts.Name = "btnGetProfileSectionScripts";
            this.btnGetProfileSectionScripts.Size = new System.Drawing.Size(210, 23);
            this.btnGetProfileSectionScripts.TabIndex = 4;
            this.btnGetProfileSectionScripts.Text = "Get Scripts";
            this.btnGetProfileSectionScripts.UseVisualStyleBackColor = true;
            this.btnGetProfileSectionScripts.Click += new System.EventHandler(this.btnGetProfileSectionScripts_Click);
            // 
            // dgvPSSite
            // 
            this.dgvPSSite.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPSSite.Location = new System.Drawing.Point(788, 430);
            this.dgvPSSite.Name = "dgvPSSite";
            this.dgvPSSite.Size = new System.Drawing.Size(371, 345);
            this.dgvPSSite.TabIndex = 3;
            // 
            // dgvPSCOmm
            // 
            this.dgvPSCOmm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPSCOmm.Location = new System.Drawing.Point(404, 430);
            this.dgvPSCOmm.Name = "dgvPSCOmm";
            this.dgvPSCOmm.Size = new System.Drawing.Size(365, 345);
            this.dgvPSCOmm.TabIndex = 2;
            // 
            // dgvPSPhoto
            // 
            this.dgvPSPhoto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPSPhoto.Location = new System.Drawing.Point(11, 429);
            this.dgvPSPhoto.Name = "dgvPSPhoto";
            this.dgvPSPhoto.Size = new System.Drawing.Size(371, 346);
            this.dgvPSPhoto.TabIndex = 1;
            // 
            // dgvPSAttr
            // 
            this.dgvPSAttr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPSAttr.Location = new System.Drawing.Point(25, 41);
            this.dgvPSAttr.Name = "dgvPSAttr";
            this.dgvPSAttr.Size = new System.Drawing.Size(1134, 351);
            this.dgvPSAttr.TabIndex = 0;
            // 
            // tabCCTypes
            // 
            this.tabCCTypes.Controls.Add(this.btnGetCCTypesScripts);
            this.tabCCTypes.Controls.Add(this.txtMaxCCTypeGroupID);
            this.tabCCTypes.Controls.Add(this.label25);
            this.tabCCTypes.Controls.Add(this.dgvCCTypes);
            this.tabCCTypes.Location = new System.Drawing.Point(4, 25);
            this.tabCCTypes.Name = "tabCCTypes";
            this.tabCCTypes.Size = new System.Drawing.Size(1172, 764);
            this.tabCCTypes.TabIndex = 11;
            this.tabCCTypes.Text = "CreditCards";
            this.tabCCTypes.UseVisualStyleBackColor = true;
            this.tabCCTypes.Click += new System.EventHandler(this.tabCCTypes_Click);
            // 
            // btnGetCCTypesScripts
            // 
            this.btnGetCCTypesScripts.Location = new System.Drawing.Point(481, 37);
            this.btnGetCCTypesScripts.Name = "btnGetCCTypesScripts";
            this.btnGetCCTypesScripts.Size = new System.Drawing.Size(127, 36);
            this.btnGetCCTypesScripts.TabIndex = 19;
            this.btnGetCCTypesScripts.Text = "Get Scripts";
            this.btnGetCCTypesScripts.UseVisualStyleBackColor = true;
            this.btnGetCCTypesScripts.Click += new System.EventHandler(this.btnGetCCTypesScripts_Click);
            // 
            // txtMaxCCTypeGroupID
            // 
            this.txtMaxCCTypeGroupID.Location = new System.Drawing.Point(226, 44);
            this.txtMaxCCTypeGroupID.Name = "txtMaxCCTypeGroupID";
            this.txtMaxCCTypeGroupID.Size = new System.Drawing.Size(130, 20);
            this.txtMaxCCTypeGroupID.TabIndex = 17;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(34, 47);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(156, 13);
            this.label25.TabIndex = 18;
            this.label25.Text = "Current Max CreditCardGroupID";
            // 
            // dgvCCTypes
            // 
            this.dgvCCTypes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCCTypes.Location = new System.Drawing.Point(30, 91);
            this.dgvCCTypes.Name = "dgvCCTypes";
            this.dgvCCTypes.Size = new System.Drawing.Size(594, 432);
            this.dgvCCTypes.TabIndex = 0;
            // 
            // tabPhotoSearch
            // 
            this.tabPhotoSearch.Controls.Add(this.btnPhotoScripts);
            this.tabPhotoSearch.Controls.Add(this.dgvPhotoSearch);
            this.tabPhotoSearch.Location = new System.Drawing.Point(4, 25);
            this.tabPhotoSearch.Name = "tabPhotoSearch";
            this.tabPhotoSearch.Size = new System.Drawing.Size(1172, 764);
            this.tabPhotoSearch.TabIndex = 12;
            this.tabPhotoSearch.Text = "PhotoSearch";
            this.tabPhotoSearch.UseVisualStyleBackColor = true;
            // 
            // btnPhotoScripts
            // 
            this.btnPhotoScripts.Location = new System.Drawing.Point(34, 17);
            this.btnPhotoScripts.Name = "btnPhotoScripts";
            this.btnPhotoScripts.Size = new System.Drawing.Size(115, 38);
            this.btnPhotoScripts.TabIndex = 1;
            this.btnPhotoScripts.Text = "Get Scripts";
            this.btnPhotoScripts.UseVisualStyleBackColor = true;
            this.btnPhotoScripts.Click += new System.EventHandler(this.btnPhotoScripts_Click);
            // 
            // dgvPhotoSearch
            // 
            this.dgvPhotoSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPhotoSearch.Location = new System.Drawing.Point(34, 76);
            this.dgvPhotoSearch.Name = "dgvPhotoSearch";
            this.dgvPhotoSearch.Size = new System.Drawing.Size(1067, 292);
            this.dgvPhotoSearch.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.rtfTemplatesKey);
            this.tabPage1.Controls.Add(this.label35);
            this.tabPage1.Controls.Add(this.cmdGetResTemplScripts);
            this.tabPage1.Controls.Add(this.txtCurrResTemplID);
            this.tabPage1.Controls.Add(this.label26);
            this.tabPage1.Controls.Add(this.btnGetResTemplates);
            this.tabPage1.Controls.Add(this.dgvResTemplates);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1172, 764);
            this.tabPage1.TabIndex = 13;
            this.tabPage1.Text = "Resource Templates";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // rtfTemplatesKey
            // 
            this.rtfTemplatesKey.Location = new System.Drawing.Point(35, 34);
            this.rtfTemplatesKey.Name = "rtfTemplatesKey";
            this.rtfTemplatesKey.Size = new System.Drawing.Size(774, 84);
            this.rtfTemplatesKey.TabIndex = 37;
            this.rtfTemplatesKey.Text = "";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(34, 12);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(589, 13);
            this.label35.TabIndex = 36;
            this.label35.Text = "In order to keep IDs consistent update statements for mnKey1 will be generated, m" +
                "ake sure to run them on mnKey database";
            // 
            // cmdGetResTemplScripts
            // 
            this.cmdGetResTemplScripts.Location = new System.Drawing.Point(1017, 26);
            this.cmdGetResTemplScripts.Name = "cmdGetResTemplScripts";
            this.cmdGetResTemplScripts.Size = new System.Drawing.Size(145, 35);
            this.cmdGetResTemplScripts.TabIndex = 27;
            this.cmdGetResTemplScripts.Text = "Get  Res. Templates Scripts";
            this.cmdGetResTemplScripts.UseVisualStyleBackColor = true;
            this.cmdGetResTemplScripts.Click += new System.EventHandler(this.cmdGetResTemplScripts_Click);
            // 
            // txtCurrResTemplID
            // 
            this.txtCurrResTemplID.Location = new System.Drawing.Point(227, 133);
            this.txtCurrResTemplID.Name = "txtCurrResTemplID";
            this.txtCurrResTemplID.Size = new System.Drawing.Size(130, 20);
            this.txtCurrResTemplID.TabIndex = 25;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(40, 136);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(171, 13);
            this.label26.TabIndex = 26;
            this.label26.Text = "Current Max Resource TemplateID";
            // 
            // btnGetResTemplates
            // 
            this.btnGetResTemplates.Location = new System.Drawing.Point(838, 26);
            this.btnGetResTemplates.Name = "btnGetResTemplates";
            this.btnGetResTemplates.Size = new System.Drawing.Size(145, 35);
            this.btnGetResTemplates.TabIndex = 1;
            this.btnGetResTemplates.Text = "Get Brand Res. Templates";
            this.btnGetResTemplates.UseVisualStyleBackColor = true;
            this.btnGetResTemplates.Click += new System.EventHandler(this.btnGetResTemplates_Click);
            // 
            // dgvResTemplates
            // 
            this.dgvResTemplates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvResTemplates.Location = new System.Drawing.Point(35, 171);
            this.dgvResTemplates.Name = "dgvResTemplates";
            this.dgvResTemplates.Size = new System.Drawing.Size(1099, 562);
            this.dgvResTemplates.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtMaxPropertyID);
            this.tabPage2.Controls.Add(this.label31);
            this.tabPage2.Controls.Add(this.btnYNMPartitions);
            this.tabPage2.Controls.Add(this.btnGetYNMSP);
            this.tabPage2.Controls.Add(this.btnGetYNMTables);
            this.tabPage2.Controls.Add(this.label30);
            this.tabPage2.Controls.Add(this.lboProcedures);
            this.tabPage2.Controls.Add(this.label29);
            this.tabPage2.Controls.Add(this.lboTables);
            this.tabPage2.Controls.Add(this.label28);
            this.tabPage2.Controls.Add(this.label27);
            this.tabPage2.Controls.Add(this.txtPartitions);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1172, 764);
            this.tabPage2.TabIndex = 14;
            this.tabPage2.Text = "YNM Lists";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // txtMaxPropertyID
            // 
            this.txtMaxPropertyID.Location = new System.Drawing.Point(160, 45);
            this.txtMaxPropertyID.Name = "txtMaxPropertyID";
            this.txtMaxPropertyID.Size = new System.Drawing.Size(130, 20);
            this.txtMaxPropertyID.TabIndex = 34;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(22, 48);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(117, 13);
            this.label31.TabIndex = 35;
            this.label31.Text = "Current Max PropertyID";
            // 
            // btnYNMPartitions
            // 
            this.btnYNMPartitions.Location = new System.Drawing.Point(266, 80);
            this.btnYNMPartitions.Name = "btnYNMPartitions";
            this.btnYNMPartitions.Size = new System.Drawing.Size(157, 26);
            this.btnYNMPartitions.TabIndex = 33;
            this.btnYNMPartitions.Text = "Get YNM Property Script";
            this.btnYNMPartitions.UseVisualStyleBackColor = true;
            this.btnYNMPartitions.Click += new System.EventHandler(this.btnYNMPartitions_Click);
            // 
            // btnGetYNMSP
            // 
            this.btnGetYNMSP.Location = new System.Drawing.Point(497, 127);
            this.btnGetYNMSP.Name = "btnGetYNMSP";
            this.btnGetYNMSP.Size = new System.Drawing.Size(129, 21);
            this.btnGetYNMSP.TabIndex = 32;
            this.btnGetYNMSP.Text = "Get YNM SP Script";
            this.btnGetYNMSP.UseVisualStyleBackColor = true;
            this.btnGetYNMSP.Click += new System.EventHandler(this.btnGetYNMSP_Click);
            // 
            // btnGetYNMTables
            // 
            this.btnGetYNMTables.Location = new System.Drawing.Point(130, 123);
            this.btnGetYNMTables.Name = "btnGetYNMTables";
            this.btnGetYNMTables.Size = new System.Drawing.Size(129, 21);
            this.btnGetYNMTables.TabIndex = 31;
            this.btnGetYNMTables.Text = "Get YNM Tables Script";
            this.btnGetYNMTables.UseVisualStyleBackColor = true;
            this.btnGetYNMTables.Click += new System.EventHandler(this.btnGetYNMTables_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(394, 127);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(88, 13);
            this.label30.TabIndex = 30;
            this.label30.Text = "YNM Procedures";
            // 
            // lboProcedures
            // 
            this.lboProcedures.FormattingEnabled = true;
            this.lboProcedures.Location = new System.Drawing.Point(397, 160);
            this.lboProcedures.Name = "lboProcedures";
            this.lboProcedures.Size = new System.Drawing.Size(285, 472);
            this.lboProcedures.TabIndex = 29;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(35, 131);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(66, 13);
            this.label29.TabIndex = 28;
            this.label29.Text = "YNM Tables";
            // 
            // lboTables
            // 
            this.lboTables.FormattingEnabled = true;
            this.lboTables.Location = new System.Drawing.Point(28, 160);
            this.lboTables.Name = "lboTables";
            this.lboTables.Size = new System.Drawing.Size(285, 472);
            this.lboTables.TabIndex = 27;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(22, 16);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(234, 13);
            this.label28.TabIndex = 26;
            this.label28.Text = "This needs to be done for new communities only";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(34, 83);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(57, 13);
            this.label27.TabIndex = 25;
            this.label27.Text = "Partitions#";
            // 
            // txtPartitions
            // 
            this.txtPartitions.Location = new System.Drawing.Point(113, 80);
            this.txtPartitions.Name = "txtPartitions";
            this.txtPartitions.Size = new System.Drawing.Size(130, 20);
            this.txtPartitions.TabIndex = 24;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnBrowseCategoriesFile);
            this.tabPage3.Controls.Add(this.button6);
            this.tabPage3.Controls.Add(this.label39);
            this.tabPage3.Controls.Add(this.txtArticleCategories);
            this.tabPage3.Controls.Add(this.rtfKeyCategory);
            this.tabPage3.Controls.Add(this.btnGetSiteArtScripts);
            this.tabPage3.Controls.Add(this.btnGetSiteCategoryScrits);
            this.tabPage3.Controls.Add(this.lblArticlesCount);
            this.tabPage3.Controls.Add(this.dgvArticles);
            this.tabPage3.Controls.Add(this.dgvSiteCategories);
            this.tabPage3.Controls.Add(this.label34);
            this.tabPage3.Controls.Add(this.txtMaxSiteCategoryID);
            this.tabPage3.Controls.Add(this.label33);
            this.tabPage3.Controls.Add(this.txtMaxSiteArticleID);
            this.tabPage3.Controls.Add(this.label32);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1172, 764);
            this.tabPage3.TabIndex = 15;
            this.tabPage3.Text = "Articles";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(673, 75);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(90, 42);
            this.button6.TabIndex = 38;
            this.button6.Text = "Refresh";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(20, 70);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(91, 13);
            this.label39.TabIndex = 37;
            this.label39.Text = "Categories xml file";
            // 
            // txtArticleCategories
            // 
            this.txtArticleCategories.Location = new System.Drawing.Point(138, 75);
            this.txtArticleCategories.Name = "txtArticleCategories";
            this.txtArticleCategories.Size = new System.Drawing.Size(392, 20);
            this.txtArticleCategories.TabIndex = 36;
            // 
            // rtfKeyCategory
            // 
            this.rtfKeyCategory.Location = new System.Drawing.Point(20, 103);
            this.rtfKeyCategory.Name = "rtfKeyCategory";
            this.rtfKeyCategory.Size = new System.Drawing.Size(510, 109);
            this.rtfKeyCategory.TabIndex = 35;
            this.rtfKeyCategory.Text = "";
            // 
            // btnGetSiteArtScripts
            // 
            this.btnGetSiteArtScripts.Location = new System.Drawing.Point(584, 218);
            this.btnGetSiteArtScripts.Name = "btnGetSiteArtScripts";
            this.btnGetSiteArtScripts.Size = new System.Drawing.Size(145, 21);
            this.btnGetSiteArtScripts.TabIndex = 34;
            this.btnGetSiteArtScripts.Text = "Get Site Artcls Scripts";
            this.btnGetSiteArtScripts.UseVisualStyleBackColor = true;
            this.btnGetSiteArtScripts.Click += new System.EventHandler(this.btnGetSiteArtScripts_Click);
            // 
            // btnGetSiteCategoryScrits
            // 
            this.btnGetSiteCategoryScrits.Location = new System.Drawing.Point(21, 218);
            this.btnGetSiteCategoryScrits.Name = "btnGetSiteCategoryScrits";
            this.btnGetSiteCategoryScrits.Size = new System.Drawing.Size(145, 21);
            this.btnGetSiteCategoryScrits.TabIndex = 33;
            this.btnGetSiteCategoryScrits.Text = "Get Site Category Scripts";
            this.btnGetSiteCategoryScrits.UseVisualStyleBackColor = true;
            this.btnGetSiteCategoryScrits.Click += new System.EventHandler(this.btnGetSiteCategoryScrits_Click);
            // 
            // lblArticlesCount
            // 
            this.lblArticlesCount.AutoSize = true;
            this.lblArticlesCount.Location = new System.Drawing.Point(568, 541);
            this.lblArticlesCount.Name = "lblArticlesCount";
            this.lblArticlesCount.Size = new System.Drawing.Size(0, 13);
            this.lblArticlesCount.TabIndex = 32;
            // 
            // dgvArticles
            // 
            this.dgvArticles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArticles.Location = new System.Drawing.Point(571, 245);
            this.dgvArticles.Name = "dgvArticles";
            this.dgvArticles.Size = new System.Drawing.Size(582, 519);
            this.dgvArticles.TabIndex = 31;
            this.dgvArticles.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvArticles_CellContentClick);
            // 
            // dgvSiteCategories
            // 
            this.dgvSiteCategories.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSiteCategories.Location = new System.Drawing.Point(13, 249);
            this.dgvSiteCategories.Name = "dgvSiteCategories";
            this.dgvSiteCategories.Size = new System.Drawing.Size(518, 515);
            this.dgvSiteCategories.TabIndex = 30;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(20, 23);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(381, 13);
            this.label34.TabIndex = 29;
            this.label34.Text = "In order to keep IDs consistent update statements for mnKey1 will be generated";
            // 
            // txtMaxSiteCategoryID
            // 
            this.txtMaxSiteCategoryID.Location = new System.Drawing.Point(177, 39);
            this.txtMaxSiteCategoryID.Name = "txtMaxSiteCategoryID";
            this.txtMaxSiteCategoryID.Size = new System.Drawing.Size(130, 20);
            this.txtMaxSiteCategoryID.TabIndex = 27;
            this.txtMaxSiteCategoryID.TextChanged += new System.EventHandler(this.txtMaxSiteCategoryID_TextChanged);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(21, 42);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(141, 13);
            this.label33.TabIndex = 28;
            this.label33.Text = "Current Max Site CategoryID";
            this.label33.Click += new System.EventHandler(this.label33_Click);
            // 
            // txtMaxSiteArticleID
            // 
            this.txtMaxSiteArticleID.Location = new System.Drawing.Point(480, 42);
            this.txtMaxSiteArticleID.Name = "txtMaxSiteArticleID";
            this.txtMaxSiteArticleID.Size = new System.Drawing.Size(130, 20);
            this.txtMaxSiteArticleID.TabIndex = 25;
            this.txtMaxSiteArticleID.TextChanged += new System.EventHandler(this.txtMaxSiteArticleID_TextChanged);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(324, 45);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(128, 13);
            this.label32.TabIndex = 26;
            this.label32.Text = "Current Max Site ArticleID";
            this.label32.Click += new System.EventHandler(this.label32_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.btnGetDeltaScripts);
            this.tabPage4.Controls.Add(this.label38);
            this.tabPage4.Controls.Add(this.btnGetDeltaAttr);
            this.tabPage4.Controls.Add(this.label37);
            this.tabPage4.Controls.Add(this.dgvDeltaAttrComm);
            this.tabPage4.Controls.Add(this.label36);
            this.tabPage4.Controls.Add(this.button4);
            this.tabPage4.Controls.Add(this.dgvDeltaAttrBrand);
            this.tabPage4.Controls.Add(this.dgvDeltaAttrSite);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1172, 764);
            this.tabPage4.TabIndex = 16;
            this.tabPage4.Text = "Delta Attributes";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // btnGetDeltaScripts
            // 
            this.btnGetDeltaScripts.Location = new System.Drawing.Point(422, 23);
            this.btnGetDeltaScripts.Name = "btnGetDeltaScripts";
            this.btnGetDeltaScripts.Size = new System.Drawing.Size(163, 26);
            this.btnGetDeltaScripts.TabIndex = 12;
            this.btnGetDeltaScripts.Text = "Get Delta Scripts";
            this.btnGetDeltaScripts.UseVisualStyleBackColor = true;
            this.btnGetDeltaScripts.Click += new System.EventHandler(this.btnGetDeltaScripts_Click);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(32, 516);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(116, 13);
            this.label38.TabIndex = 11;
            this.label38.Text = "Brand Scope Attributes";
            // 
            // btnGetDeltaAttr
            // 
            this.btnGetDeltaAttr.Location = new System.Drawing.Point(35, 6);
            this.btnGetDeltaAttr.Name = "btnGetDeltaAttr";
            this.btnGetDeltaAttr.Size = new System.Drawing.Size(85, 26);
            this.btnGetDeltaAttr.TabIndex = 10;
            this.btnGetDeltaAttr.Text = "Get Delta";
            this.btnGetDeltaAttr.UseVisualStyleBackColor = true;
            this.btnGetDeltaAttr.Click += new System.EventHandler(this.btnGetDeltaAttr_Click);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(31, 49);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(139, 13);
            this.label37.TabIndex = 9;
            this.label37.Text = "Community Scope Attributes";
            // 
            // dgvDeltaAttrComm
            // 
            this.dgvDeltaAttrComm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeltaAttrComm.Location = new System.Drawing.Point(34, 65);
            this.dgvDeltaAttrComm.Name = "dgvDeltaAttrComm";
            this.dgvDeltaAttrComm.Size = new System.Drawing.Size(1108, 156);
            this.dgvDeltaAttrComm.TabIndex = 8;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(32, 235);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(106, 13);
            this.label36.TabIndex = 7;
            this.label36.Text = "Site Scope Attributes";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(1023, 23);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(119, 36);
            this.button4.TabIndex = 6;
            this.button4.Text = "Get Brand Attributes";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // dgvDeltaAttrBrand
            // 
            this.dgvDeltaAttrBrand.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeltaAttrBrand.Location = new System.Drawing.Point(34, 532);
            this.dgvDeltaAttrBrand.Name = "dgvDeltaAttrBrand";
            this.dgvDeltaAttrBrand.Size = new System.Drawing.Size(1108, 198);
            this.dgvDeltaAttrBrand.TabIndex = 5;
            // 
            // dgvDeltaAttrSite
            // 
            this.dgvDeltaAttrSite.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeltaAttrSite.Location = new System.Drawing.Point(34, 251);
            this.dgvDeltaAttrSite.Name = "dgvDeltaAttrSite";
            this.dgvDeltaAttrSite.Size = new System.Drawing.Size(1108, 262);
            this.dgvDeltaAttrSite.TabIndex = 4;
            // 
            // btnGetDeltaSettings
            // 
            this.btnGetDeltaSettings.Controls.Add(this.btnGetDeltaSettingScripts);
            this.btnGetDeltaSettings.Controls.Add(this.button7);
            this.btnGetDeltaSettings.Controls.Add(this.dgvDeltaSiteSetting);
            this.btnGetDeltaSettings.Controls.Add(this.button5);
            this.btnGetDeltaSettings.Controls.Add(this.btnGetDeltaSitePages);
            this.btnGetDeltaSettings.Controls.Add(this.dgvDeltaSitePages);
            this.btnGetDeltaSettings.Location = new System.Drawing.Point(4, 25);
            this.btnGetDeltaSettings.Name = "btnGetDeltaSettings";
            this.btnGetDeltaSettings.Padding = new System.Windows.Forms.Padding(3);
            this.btnGetDeltaSettings.Size = new System.Drawing.Size(1172, 764);
            this.btnGetDeltaSettings.TabIndex = 17;
            this.btnGetDeltaSettings.Text = "Delta Site Pages/Settings";
            this.btnGetDeltaSettings.UseVisualStyleBackColor = true;
            // 
            // btnGetDeltaSettingScripts
            // 
            this.btnGetDeltaSettingScripts.Location = new System.Drawing.Point(287, 379);
            this.btnGetDeltaSettingScripts.Name = "btnGetDeltaSettingScripts";
            this.btnGetDeltaSettingScripts.Size = new System.Drawing.Size(251, 30);
            this.btnGetDeltaSettingScripts.TabIndex = 14;
            this.btnGetDeltaSettingScripts.Text = "Get Delta Site Settings Script";
            this.btnGetDeltaSettingScripts.UseVisualStyleBackColor = true;
            this.btnGetDeltaSettingScripts.Click += new System.EventHandler(this.btnGetDeltaSettingScripts_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(85, 379);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(176, 30);
            this.button7.TabIndex = 13;
            this.button7.Text = "Get Delta Site Settings";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // dgvDeltaSiteSetting
            // 
            this.dgvDeltaSiteSetting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeltaSiteSetting.Location = new System.Drawing.Point(77, 415);
            this.dgvDeltaSiteSetting.Name = "dgvDeltaSiteSetting";
            this.dgvDeltaSiteSetting.Size = new System.Drawing.Size(1016, 343);
            this.dgvDeltaSiteSetting.TabIndex = 12;
            this.dgvDeltaSiteSetting.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDeltaSiteSetting_CellContentClick);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(287, 6);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(251, 30);
            this.button5.TabIndex = 11;
            this.button5.Text = "Get Delta Site Pages Script";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // btnGetDeltaSitePages
            // 
            this.btnGetDeltaSitePages.Location = new System.Drawing.Point(85, 6);
            this.btnGetDeltaSitePages.Name = "btnGetDeltaSitePages";
            this.btnGetDeltaSitePages.Size = new System.Drawing.Size(176, 30);
            this.btnGetDeltaSitePages.TabIndex = 10;
            this.btnGetDeltaSitePages.Text = "Get Delta Site Pages";
            this.btnGetDeltaSitePages.UseVisualStyleBackColor = true;
            this.btnGetDeltaSitePages.Click += new System.EventHandler(this.btnGetDeltaSitePages_Click);
            // 
            // dgvDeltaSitePages
            // 
            this.dgvDeltaSitePages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeltaSitePages.Location = new System.Drawing.Point(77, 42);
            this.dgvDeltaSitePages.Name = "dgvDeltaSitePages";
            this.dgvDeltaSitePages.Size = new System.Drawing.Size(1016, 322);
            this.dgvDeltaSitePages.TabIndex = 9;
            // 
            // txtAttrOptionMaxId
            // 
            this.txtAttrOptionMaxId.Location = new System.Drawing.Point(537, 146);
            this.txtAttrOptionMaxId.Name = "txtAttrOptionMaxId";
            this.txtAttrOptionMaxId.Size = new System.Drawing.Size(130, 20);
            this.txtAttrOptionMaxId.TabIndex = 15;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(345, 149);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(177, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "Current Max AttributeOptionGroupID";
            // 
            // cboBrands
            // 
            this.cboBrands.FormattingEnabled = true;
            this.cboBrands.Location = new System.Drawing.Point(97, 92);
            this.cboBrands.Name = "cboBrands";
            this.cboBrands.Size = new System.Drawing.Size(79, 21);
            this.cboBrands.TabIndex = 4;
            this.cboBrands.SelectedIndexChanged += new System.EventHandler(this.cboBrands_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "SiteID";
            // 
            // cboEnv
            // 
            this.cboEnv.FormattingEnabled = true;
            this.cboEnv.Location = new System.Drawing.Point(97, 2);
            this.cboEnv.Name = "cboEnv";
            this.cboEnv.Size = new System.Drawing.Size(346, 21);
            this.cboEnv.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Environment";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(216, 65);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(85, 26);
            this.button1.TabIndex = 5;
            this.button1.Text = "Get Site";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(544, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "New SiteID";
            // 
            // txtNewSiteID
            // 
            this.txtNewSiteID.Location = new System.Drawing.Point(616, 38);
            this.txtNewSiteID.Name = "txtNewSiteID";
            this.txtNewSiteID.Size = new System.Drawing.Size(100, 20);
            this.txtNewSiteID.TabIndex = 5;
            // 
            // txtNewBrandID
            // 
            this.txtNewBrandID.Location = new System.Drawing.Point(616, 70);
            this.txtNewBrandID.Name = "txtNewBrandID";
            this.txtNewBrandID.Size = new System.Drawing.Size(100, 20);
            this.txtNewBrandID.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(534, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "New BrandID";
            // 
            // txtNewSiteGroupName
            // 
            this.txtNewSiteGroupName.Location = new System.Drawing.Point(874, 36);
            this.txtNewSiteGroupName.Name = "txtNewSiteGroupName";
            this.txtNewSiteGroupName.Size = new System.Drawing.Size(220, 20);
            this.txtNewSiteGroupName.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(736, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "New Site Group Name";
            // 
            // txtNewBrandGroupName
            // 
            this.txtNewBrandGroupName.Location = new System.Drawing.Point(874, 67);
            this.txtNewBrandGroupName.Name = "txtNewBrandGroupName";
            this.txtNewBrandGroupName.Size = new System.Drawing.Size(220, 20);
            this.txtNewBrandGroupName.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(736, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "New Brand Group Name";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(28, 94);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Brand";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(23, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 13);
            this.label13.TabIndex = 15;
            this.label13.Text = "Community";
            // 
            // cboCommunity
            // 
            this.cboCommunity.FormattingEnabled = true;
            this.cboCommunity.Location = new System.Drawing.Point(97, 38);
            this.cboCommunity.Name = "cboCommunity";
            this.cboCommunity.Size = new System.Drawing.Size(204, 21);
            this.cboCommunity.TabIndex = 14;
            this.cboCommunity.SelectedIndexChanged += new System.EventHandler(this.cboCommunity_SelectedIndexChanged);
            // 
            // cmdCommunitySites
            // 
            this.cmdCommunitySites.Location = new System.Drawing.Point(322, 38);
            this.cmdCommunitySites.Name = "cmdCommunitySites";
            this.cmdCommunitySites.Size = new System.Drawing.Size(170, 26);
            this.cmdCommunitySites.TabIndex = 16;
            this.cmdCommunitySites.Text = "Get Community Sites";
            this.cmdCommunitySites.UseVisualStyleBackColor = true;
            this.cmdCommunitySites.Click += new System.EventHandler(this.cmdCommunitySites_Click);
            // 
            // cboSites
            // 
            this.cboSites.FormattingEnabled = true;
            this.cboSites.Location = new System.Drawing.Point(97, 65);
            this.cboSites.Name = "cboSites";
            this.cboSites.Size = new System.Drawing.Size(79, 21);
            this.cboSites.TabIndex = 17;
            this.cboSites.SelectedIndexChanged += new System.EventHandler(this.cboSites_SelectedIndexChanged);
            // 
            // txtNewCommunityName
            // 
            this.txtNewCommunityName.Location = new System.Drawing.Point(874, 3);
            this.txtNewCommunityName.Name = "txtNewCommunityName";
            this.txtNewCommunityName.Size = new System.Drawing.Size(220, 20);
            this.txtNewCommunityName.TabIndex = 20;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(722, 10);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(146, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "New Community Group Name";
            // 
            // txtNewCommunityID
            // 
            this.txtNewCommunityID.Location = new System.Drawing.Point(616, 5);
            this.txtNewCommunityID.Name = "txtNewCommunityID";
            this.txtNewCommunityID.Size = new System.Drawing.Size(100, 20);
            this.txtNewCommunityID.TabIndex = 18;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(516, 8);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 13);
            this.label15.TabIndex = 19;
            this.label15.Text = "New CommunityID";
            // 
            // chkTimeStamp
            // 
            this.chkTimeStamp.AutoSize = true;
            this.chkTimeStamp.Location = new System.Drawing.Point(180, 123);
            this.chkTimeStamp.Name = "chkTimeStamp";
            this.chkTimeStamp.Size = new System.Drawing.Size(101, 17);
            this.chkTimeStamp.TabIndex = 22;
            this.chkTimeStamp.Text = "Use TimeStamp";
            this.chkTimeStamp.UseVisualStyleBackColor = true;
            // 
            // txtMaxAttributeGroupID
            // 
            this.txtMaxAttributeGroupID.Location = new System.Drawing.Point(537, 120);
            this.txtMaxAttributeGroupID.Name = "txtMaxAttributeGroupID";
            this.txtMaxAttributeGroupID.Size = new System.Drawing.Size(130, 20);
            this.txtMaxAttributeGroupID.TabIndex = 23;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(381, 123);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(146, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Current Max AttributeGroupID";
            // 
            // chkUseTran
            // 
            this.chkUseTran.AutoSize = true;
            this.chkUseTran.Location = new System.Drawing.Point(180, 146);
            this.chkUseTran.Name = "chkUseTran";
            this.chkUseTran.Size = new System.Drawing.Size(128, 17);
            this.chkUseTran.TabIndex = 25;
            this.chkUseTran.Text = "Use SQL Transaction";
            this.chkUseTran.UseVisualStyleBackColor = true;
            this.chkUseTran.CheckedChanged += new System.EventHandler(this.chkUseTran_CheckedChanged);
            // 
            // txtTimeStamp
            // 
            this.txtTimeStamp.Location = new System.Drawing.Point(25, 121);
            this.txtTimeStamp.Name = "txtTimeStamp";
            this.txtTimeStamp.Size = new System.Drawing.Size(149, 20);
            this.txtTimeStamp.TabIndex = 26;
            // 
            // btnRollback
            // 
            this.btnRollback.Location = new System.Drawing.Point(1037, 120);
            this.btnRollback.Name = "btnRollback";
            this.btnRollback.Size = new System.Drawing.Size(159, 30);
            this.btnRollback.TabIndex = 27;
            this.btnRollback.Text = "Get Rollback Scripts";
            this.btnRollback.UseVisualStyleBackColor = true;
            this.btnRollback.Click += new System.EventHandler(this.btnRollback_Click);
            // 
            // btnMaxIds
            // 
            this.btnMaxIds.Location = new System.Drawing.Point(711, 121);
            this.btnMaxIds.Name = "btnMaxIds";
            this.btnMaxIds.Size = new System.Drawing.Size(185, 30);
            this.btnMaxIds.TabIndex = 28;
            this.btnMaxIds.Text = "Get Max PK ids from current database";
            this.btnMaxIds.UseVisualStyleBackColor = true;
            this.btnMaxIds.Click += new System.EventHandler(this.btnMaxIds_Click);
            // 
            // chkNoNewPKS
            // 
            this.chkNoNewPKS.AutoSize = true;
            this.chkNoNewPKS.Location = new System.Drawing.Point(20, 156);
            this.chkNoNewPKS.Name = "chkNoNewPKS";
            this.chkNoNewPKS.Size = new System.Drawing.Size(148, 17);
            this.chkNoNewPKS.TabIndex = 29;
            this.chkNoNewPKS.Text = "Do not generate new PKs";
            this.chkNoNewPKS.UseVisualStyleBackColor = true;
            // 
            // btnBrowseCategoriesFile
            // 
            this.btnBrowseCategoriesFile.Location = new System.Drawing.Point(546, 77);
            this.btnBrowseCategoriesFile.Name = "btnBrowseCategoriesFile";
            this.btnBrowseCategoriesFile.Size = new System.Drawing.Size(89, 40);
            this.btnBrowseCategoriesFile.TabIndex = 39;
            this.btnBrowseCategoriesFile.Text = "Browse";
            this.btnBrowseCategoriesFile.UseVisualStyleBackColor = true;
            this.btnBrowseCategoriesFile.Click += new System.EventHandler(this.btnBrowseCategoriesFile_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // SiteConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1208, 984);
            this.Controls.Add(this.chkNoNewPKS);
            this.Controls.Add(this.btnMaxIds);
            this.Controls.Add(this.btnRollback);
            this.Controls.Add(this.txtTimeStamp);
            this.Controls.Add(this.chkUseTran);
            this.Controls.Add(this.txtMaxAttributeGroupID);
            this.Controls.Add(this.txtAttrOptionMaxId);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.chkTimeStamp);
            this.Controls.Add(this.txtNewCommunityName);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtNewCommunityID);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.cboSites);
            this.Controls.Add(this.cmdCommunitySites);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.cboCommunity);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtNewBrandGroupName);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtNewSiteGroupName);
            this.Controls.Add(this.cboBrands);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtNewBrandID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtNewSiteID);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboEnv);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabSettings);
            this.Name = "SiteConfig";
            this.Text = "Site";
            this.Load += new System.EventHandler(this.SiteConfig_Load);
            this.tabSettings.ResumeLayout(false);
            this.tabConfig.ResumeLayout(false);
            this.tabConfig.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommunity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabSitePages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).EndInit();
            this.tabSiteSettings.ResumeLayout(false);
            this.tabSiteSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabCommunityAttr.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommAttrOptions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommAttr)).EndInit();
            this.tabAttributes.ResumeLayout(false);
            this.tabAttributes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.tabAttrOptions.ResumeLayout(false);
            this.tabAttrOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            this.tabRegPromos.ResumeLayout(false);
            this.tabRegPromos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView10)).EndInit();
            this.tabList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView9)).EndInit();
            this.tabTease.ResumeLayout(false);
            this.tabTease.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView11)).EndInit();
            this.tabSparkWS.ResumeLayout(false);
            this.tabSparkWS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPSSite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPSCOmm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPSPhoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPSAttr)).EndInit();
            this.tabCCTypes.ResumeLayout(false);
            this.tabCCTypes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCCTypes)).EndInit();
            this.tabPhotoSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPhotoSearch)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResTemplates)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSiteCategories)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeltaAttrComm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeltaAttrBrand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeltaAttrSite)).EndInit();
            this.btnGetDeltaSettings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeltaSiteSetting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeltaSitePages)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabSettings;
        private System.Windows.Forms.TabPage tabConfig;
        private System.Windows.Forms.TabPage tabSiteSettings;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboEnv;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.TabPage tabList;
        private System.Windows.Forms.TabPage tabAttributes;
        private System.Windows.Forms.TabPage tabTease;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnGetBrandAttributes;
        private System.Windows.Forms.ComboBox cboBrands;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNewSiteID;
        private System.Windows.Forms.TextBox txtNewBrandID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnGetSiteScripts;
        private System.Windows.Forms.TextBox txtNewSiteGroupName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNewBrandGroupName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnGetSiteSettings;
        private System.Windows.Forms.TextBox txtMaxSiteSettingID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnGetAttrScript;
        private System.Windows.Forms.TabPage tabAttrOptions;
        private System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnBrandAttrOption;
        private System.Windows.Forms.DataGridView dataGridView7;
        private System.Windows.Forms.Button btnGetAttrOptionScript;
        private System.Windows.Forms.TextBox txtAttrOptionMaxId;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabPage tabSitePages;
        private System.Windows.Forms.DataGridView dataGridView8;
        private System.Windows.Forms.Button btnGetSitePages;
        private System.Windows.Forms.DataGridView dataGridView9;
        private System.Windows.Forms.Button btnGetListCatScript;
        private System.Windows.Forms.TabPage tabRegPromos;
        private System.Windows.Forms.TabPage tabSparkWS;
        private System.Windows.Forms.DataGridView dataGridView10;
        private System.Windows.Forms.DataGridView dataGridView11;
        private System.Windows.Forms.Button btnSiteSettingSelect;
        private System.Windows.Forms.CheckBox chkGetDynamic;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cboCommunity;
        private System.Windows.Forms.Button cmdCommunitySites;
        private System.Windows.Forms.ComboBox cboSites;
        private System.Windows.Forms.DataGridView dgvCommunity;
        private System.Windows.Forms.TextBox txtNewCommunityName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtNewCommunityID;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox chkTimeStamp;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TabPage tabCommunityAttr;
        private System.Windows.Forms.DataGridView dgvCommAttr;
        private System.Windows.Forms.TextBox txtMaxAttributeGroupID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnGetCommAttrScripts;
        private System.Windows.Forms.DataGridView dgvCommAttrOptions;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtMaxPromoID;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DataGridView dgvPSPhoto;
        private System.Windows.Forms.DataGridView dgvPSAttr;
        private System.Windows.Forms.DataGridView dgvPSSite;
        private System.Windows.Forms.DataGridView dgvPSCOmm;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnGetProfileSectionScripts;
        private System.Windows.Forms.CheckBox chkUseTran;
        private System.Windows.Forms.TextBox txtTimeStamp;
        private System.Windows.Forms.Button btnRollback;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtMaxObjGroupID;
        private System.Windows.Forms.Button btnGetObjScript;
        private System.Windows.Forms.TabPage tabCCTypes;
        private System.Windows.Forms.TextBox txtMaxCCTypeGroupID;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.DataGridView dgvCCTypes;
        private System.Windows.Forms.Button btnGetCCTypesScripts;
        private System.Windows.Forms.Button btnMaxIds;
        private System.Windows.Forms.TabPage tabPhotoSearch;
        private System.Windows.Forms.DataGridView dgvPhotoSearch;
        private System.Windows.Forms.Button btnPhotoScripts;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnGetResTemplates;
        private System.Windows.Forms.DataGridView dgvResTemplates;
        private System.Windows.Forms.Button cmdGetResTemplScripts;
        private System.Windows.Forms.TextBox txtCurrResTemplID;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox chkNoNewPKS;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtPartitions;
        private System.Windows.Forms.Button btnGetYNMSP;
        private System.Windows.Forms.Button btnGetYNMTables;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ListBox lboProcedures;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ListBox lboTables;
        private System.Windows.Forms.Button btnYNMPartitions;
        private System.Windows.Forms.TextBox txtMaxPropertyID;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox txtMaxSiteCategoryID;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtMaxSiteArticleID;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.DataGridView dgvSiteCategories;
        private System.Windows.Forms.DataGridView dgvArticles;
        private System.Windows.Forms.Label lblArticlesCount;
        private System.Windows.Forms.Button btnGetSiteArtScripts;
        private System.Windows.Forms.Button btnGetSiteCategoryScrits;
        private System.Windows.Forms.RichTextBox rtfKeyCategory;
        private System.Windows.Forms.RichTextBox rtfTemplatesKey;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.DataGridView dgvDeltaAttrComm;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridView dgvDeltaAttrBrand;
        private System.Windows.Forms.DataGridView dgvDeltaAttrSite;
        private System.Windows.Forms.TabPage btnGetDeltaSettings;
        private System.Windows.Forms.Button btnGetDeltaAttr;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Button btnGetDeltaScripts;
        private System.Windows.Forms.Button btnGetDeltaSitePages;
        private System.Windows.Forms.DataGridView dgvDeltaSitePages;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btnGetDeltaSettingScripts;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.DataGridView dgvDeltaSiteSetting;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtArticleCategories;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button btnBrowseCategoriesFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}