﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Matchnet.Content.ValueObjects.Article;
namespace SparkSiteManagement.DBScripting
{
    public class SiteDB
    {
        XmlDocument _templates = null;
         public static SiteDB Instance = new SiteDB();



         private SiteDB()
        {
            LoadXML();  
        }

        public void LoadXML()
        {
            string xmlFile = System.Configuration.ConfigurationSettings.AppSettings["sqltemplate"];
            _templates = new XmlDocument();
           
            _templates.Load(xmlFile);
           

        }


        public string GetSQLTemplate(string script)
        {
           
            string name = _templates.DocumentElement.SelectSingleNode(script).InnerText;
            return name;
        }

        public string GetSQL(string template, System.Reflection.PropertyInfo[] p, Object o)
        {
            string sqltempl = GetSQLTemplate(template);
            string sql=sqltempl.ToLower();
            for (int i = 0; i < p.Length; i++)
            {
                string propName=p[i].Name.ToLower();
                sql = sql.Replace("[" + propName + "]", "'" + p[i].GetValue(o, null).ToString());

            }
            return sql;
        }


        public string GetSQL(string template, Hashtable p, bool UseTimestamp, DateTime timestamp)
        {
            string sqltempl = GetSQLTemplate(template);
            string sql = sqltempl.ToLower();
            string quot = "'";
            foreach(string key in p.Keys)
            {
                string propName = key.ToLower();
                if (p[key].ToString() == "null")
                    quot = "";
                else
                    quot="'";
                sql = sql.Replace("{" + propName + "}", quot + p[key].ToString() + quot);
                
            }

            if (UseTimestamp)
            {
                sql = sql.Replace("getdate()", "'" + timestamp.ToString() + "'");
            }
            sql = sql.Replace("{n}", "N");
            return sql;
        }

        public string GetSQL(string template, Hashtable p, bool UseTimestamp, DateTime timestamp, bool preserveCase, bool useQuots)
        {
            string sqltempl = GetSQLTemplate(template);
            string sql = "";
            if (!preserveCase)
                sql = sqltempl.ToLower();
            else
                sql = sqltempl;

            string quot = "'";
            if (!useQuots)
                quot = "";
            foreach (string key in p.Keys)
            {
                string propName = key.ToLower();
                string quot1 = quot;
                if (p[key].ToString() == "null")
                    quot1 = "";
                
                sql = sql.Replace("{" + propName + "}", quot + p[key].ToString().Replace("'","''") + quot);

            }

            if (UseTimestamp)
            {
                sql = sql.Replace("getdate()", "'" + timestamp.ToString() + "'");
                sql = sql.Replace("GETDATE()", "'" + timestamp.ToString() + "'");
            }
            sql = sql.Replace("{n}", "N");
            return sql;
        }

        public List<string> GetYNMTablesList(int communityid, int partitions)
        {
            string YNM_TABLE_FORMAT = "D{0}YNMList{1}";
            List<string> ynmTables = new List<string>();
            for (int i = 0; i <= partitions; i++)
            {
                ynmTables.Add(String.Format(YNM_TABLE_FORMAT,communityid.ToString(),partitions.ToString()));

            }
            return ynmTables;
        }

         public List<string> GetYNMSPFormatList()
        {
            
            List<string> ynmSPFormat = new List<string>();

            ynmSPFormat.Add("dbo.up_D{0}YNMList_List{1}");
            ynmSPFormat.Add("dbo.up_D{0}YNMList_MutualMail_List{1}");
            ynmSPFormat.Add("[dbo].[up_D{0}YNMList_Save{1}]");
            ynmSPFormat.Add("[dbo].[up_D{0}YNMList_Status_Save{1}]");
            ynmSPFormat.Add("[dbo].[up_D{0}YNMList_ViralMail_List{1}]");

            return ynmSPFormat;
        }

         public List<string> GetYNMSPList(int communityid, int partitions)
         {

             List<string> ynmSP = new List<string>();
             List<string> spFormats = GetYNMSPFormatList();
             for (int i = 0; i <= partitions; i++)
             {
                 for (int k = 0; k < spFormats.Count; k++)
                 {
                     ynmSP.Add(String.Format(spFormats[k], communityid.ToString(), partitions.ToString()));
                 }

             }
             return ynmSP;
         }

         public string GetDropScripts(List<string> dbObjects, string tbl_sp_prefix)
         {
             
             string dropFormat = "drop {0} {1}\r\n";
             string sqlbegintran = GetSQLTemplate("begin_tran");
             string ssqlcommittran = GetSQLTemplate("commit_tran");
             string sqlrollbacktran = GetSQLTemplate("single_sql_rollback");
             string sql=sqlbegintran;

             foreach (string dbObj in dbObjects)
             {
                 sql += String.Format(dropFormat, tbl_sp_prefix, dbObj);
                 sql += sqlrollbacktran;
             }

             sql += ssqlcommittran;
             return sql;
         }


       

    }
}
