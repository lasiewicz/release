﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;

namespace SparkSiteManagement.BL
{
    

    public class AttributeBL
    {
        public static AttributeBL Instance = new AttributeBL();

        Attributes attributes = null;

        private AttributeBL()
        {

        }

        public List<VO.AttributeGroup> GetAttributes(int siteid, string uri, bool clearAtrtibutesObj)
        {

            if (attributes == null || clearAtrtibutesObj)
                getAttributes(uri);

            Hashtable attrGr = (Hashtable)attributes._attributeGroupByGroupIDAttributeID[siteid];

            List<VO.AttributeGroup> listAttr = new List<VO.AttributeGroup>();
            foreach (int key in attrGr.Keys)
            {
                AttributeGroup group = (AttributeGroup)attrGr[key];
                VO.AttributeGroup g = new VO.AttributeGroup();
                g.AttributeID = group.AttributeID;
                g.AttributeGroupID = group.ID;
                g.AttributeGroupIDOldValueContainer = group.OldValueContainerID;
                g.AttributeStatusMask =(int) group.Status;
                g.DefaultValue = group.DefaultValue;
                g.EncryptFlag = group.EncryptFlag;
                g.Length = group.Length;
                Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = (Matchnet.Content.ValueObjects.AttributeMetadata.Attribute)attributes._attributeByID[g.AttributeID];
                if (attribute != null)
                {
                    g.AttributeName = attribute.Name;
                    g.ScopeID = (int)attribute.Scope;
                    g.DataType = attribute.DataType.ToString();
                    g.AttributeTypeID = (int)attribute.DataType;
                    g.AttributeIDOldValueContainer = attribute.OldValueContainerID;
                    g.GroupID = siteid;
                    listAttr.Add(g);
                }
                else
                {
                    System.Diagnostics.Trace.Write("Didnt find attribute for " + group.AttributeID.ToString());
                }


            }
            return listAttr;
            

        }

        private void getAttributes(string uri)
        {
            attributes = null;
            IAttributeMetadataService attr = ServicesBL.Instance.GetAttributeService(uri);
            attributes = attr.GetAttributes();
        }

        public int GetMaxAttributeGroupID(string uri,bool clearAtrtibutesObj)
        {
            int maxId = 0;

            Hashtable attrGroupIDs = (Hashtable)attributes._attributeGroupByID;
            foreach (int key in attrGroupIDs.Keys)
            {
                if (key > maxId)
                    maxId = key;

            }
            return maxId;

            return maxId;


        }
        public AttributeCollections GetAttributeCollections(int siteid, string uri)
        {

            IAttributeMetadataService attr = ServicesBL.Instance.GetAttributeService(uri);
          
            AttributeCollections c = attr.GetAttributeCollections();
            
            return c;


        }
    }


}
