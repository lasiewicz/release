﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.AccessControl;
using System.Xml;
using Matchnet.Content.ValueObjects.Article;
using P4API;
namespace SparkSiteManagement.BL
{
  
       
     
    public class ProjectBL
    {  
        string resx_file_format=".{0}.{1}.{2}";

        string prj_file_format = "<Content Include=\"{0}\"/>\r\n";
        public static ProjectBL Instance = new ProjectBL();

        public string GAM_XML_ATTR = "<value>((NewSite)).com</value>\r\n<gamvalue>((NewSite))</gamvalue>";
        private ProjectBL()
        {
          
        }


        public List<String> ProcessSiteFiles(string projdir, string dir, string ext, int sourcesiteid, string sourceculture, int targetsiteid, string targetculture, bool overwrite)
        {
            try
            {
                List<String> processed_files = new List<string>();
                
                string  source_filemask=String.Format(resx_file_format,sourcesiteid,sourceculture,ext);
                string target_filemask = String.Format(resx_file_format, targetsiteid, targetculture, ext);
                if(!overwrite)
                    ProcessDir(projdir, dir, source_filemask, target_filemask, processed_files);
                else
                    ProcessDirWithOverwrite(projdir, dir, source_filemask, target_filemask, processed_files);
                return processed_files;
               
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

       

        public List<String> ProcessSiteFiles(string projdir, string dir, string ext, string fileformat, int sourcesiteid, int targetsiteid, bool overwrite)
        {
            try
            {
                List<String> processed_files = new List<string>();

                string source_filemask = String.Format(fileformat, sourcesiteid, ext);
                string target_filemask = String.Format(fileformat, targetsiteid, ext);
                if(!overwrite)
                    ProcessDir(projdir, dir, source_filemask, target_filemask, processed_files);
                else
                    ProcessDirWithOverwrite(projdir, dir, source_filemask, target_filemask, processed_files);
                return processed_files;

            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

        public List<String> ListSite(string projdir, string dir, string ext, string fileformat, int sourcesiteid)
        {
            try
            {
                List<String> processed_files = new List<string>();

                string source_filemask = String.Format(fileformat, sourcesiteid, ext);
                 ListSiteFilesDir(projdir, dir, source_filemask,  processed_files);
              
                return processed_files;

            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }
        public List<String> ListSite(string projdir, string dir, string ext, int sourcesiteid, string sourceculture)
        {
            try
            {
                List<String> processed_files = new List<string>();

                string source_filemask = String.Format(resx_file_format, sourcesiteid, sourceculture, ext);
              
                    ListSiteFilesDir(projdir, dir, source_filemask,  processed_files);
              
              
                return processed_files;

            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

      
        public List<String> ProcessSiteFiles(string projdir, string dir, string ext, string fileformat, int sourcesiteid, int targetsiteid, string sourceculture,string targetculture)
        {
            try
            {
                List<String> processed_files = new List<string>();

                string source_filemask = String.Format(fileformat, sourcesiteid, sourceculture, ext);
                string target_filemask = String.Format(fileformat, targetsiteid,targetculture, ext);
                ProcessDir(projdir, dir, source_filemask, target_filemask, processed_files);
                return processed_files;

            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }


        public List<String> ProcessSiteDeltaFiles(string projdir, string dir, string ext, int sourcesiteid, string sourceculture, int targetsiteid, string targetculture, bool simulateonly)
        {
            try
            {
                List<String> processed_files = new List<string>();

                string source_filemask = String.Format(resx_file_format, sourcesiteid, sourceculture, ext);
                string target_filemask = String.Format(resx_file_format, targetsiteid, targetculture, ext);
                ProcessDir(projdir, dir, source_filemask, target_filemask, processed_files,simulateonly);
                return processed_files;

            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }
        public List<String> ProcessSiteDeltaFiles(string projdir, string dir, string ext, string fileformat, int sourcesiteid, int targetsiteid, bool simulateonly)
        {
            try
            {
                List<String> processed_files = new List<string>();

                string source_filemask = String.Format(fileformat, sourcesiteid, ext);
                string target_filemask = String.Format(fileformat, targetsiteid, ext);
                ProcessDir(projdir, dir, source_filemask, target_filemask, processed_files,simulateonly);
                return processed_files;

            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }
        public List<String> CreateSiteFilesCopy(string projdir, string newprojdir, string dir, string ext, string fileformat, int sourcesiteid, string culture)
        {
            try
            {
             
                List<String> processed_files = new List<string>();
                DirectoryInfo newdir= new DirectoryInfo(newprojdir);
       
                if (!newdir.Exists)
                     newdir=Directory.CreateDirectory(newprojdir);

                string source_filemask = String.Format(fileformat, sourcesiteid,culture, ext);
            
                CreateDirCopy(projdir, newdir, dir,source_filemask, processed_files);
                return processed_files;

            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

        public void DeleteSiteFiles(string projdir, string dir, string ext, int sourcesiteid, string sourceculture)
        {
            try
            {
                string source_filemask = String.Format(resx_file_format, sourcesiteid, sourceculture, ext);
                DeleteDirFiles(projdir, dir,source_filemask);

            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

        public void DeleteSiteFiles(string projdir, string file_format, string ext, int sourcesiteid)
        {
            try
            {
                string source_filemask = String.Format(file_format, sourcesiteid, ext);
                DeleteDirFiles(projdir, "", source_filemask);

            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }
        public void ProcessDirWithOverwrite(string dir, string dir_patt, string source_ext, string target_ext, List<String> files)
        {

            string[] dirs = Directory.GetDirectories(dir);
            foreach (string d in dirs)
            {
                DirectoryInfo dinfo = new DirectoryInfo(d);
                if (!String.IsNullOrEmpty(dir_patt))
                {
                    if (dinfo.Name.ToLower() == dir_patt.ToLower())
                    {
                        CopyFilesWithOverwrite(d, source_ext, target_ext, files);
                    }
                    else
                    { ProcessDirWithOverwrite(d, dir_patt, source_ext, target_ext, files); }
                }
                else
                {
                    CopyFilesWithOverwrite(d, source_ext, target_ext, files);
                    ProcessDirWithOverwrite(d, dir_patt, source_ext, target_ext, files);
                }

            }

        }



        public void ProcessDir(string dir, string dir_patt,string source_ext, string target_ext, List<String> files)
        {
        
            string[] dirs = Directory.GetDirectories(dir);
            foreach (string d in dirs)
            {
                DirectoryInfo dinfo = new DirectoryInfo(d);
                if(!String.IsNullOrEmpty(dir_patt))
                {
                    if (dinfo.Name.ToLower() == dir_patt.ToLower())
                    {
                        CopyFiles(d, source_ext, target_ext,files);
                    }
                    else
                    {   ProcessDir(d, dir_patt, source_ext, target_ext, files); }
                }
                else
                {
                    CopyFiles(d, source_ext, target_ext,files);
                    ProcessDir(d, dir_patt, source_ext, target_ext, files); 
                }

            }

        }


        public void ProcessDir(string dir, string dir_patt, string source_ext, string target_ext, List<String> files, bool simulateOnly)
        {

            string[] dirs = Directory.GetDirectories(dir);
            foreach (string d in dirs)
            {
                DirectoryInfo dinfo = new DirectoryInfo(d);
                if (!String.IsNullOrEmpty(dir_patt))
                {
                    if (dinfo.Name.ToLower() == dir_patt.ToLower())
                    {
                        CopyIfNotExistsFiles(d, source_ext, target_ext, files,simulateOnly);
                    }
                    else
                    { ProcessDir(d, dir_patt, source_ext, target_ext, files,simulateOnly); }
                }
                else
                {
                    CopyIfNotExistsFiles(d, source_ext, target_ext, files, simulateOnly);
                    ProcessDir(d, dir_patt, source_ext, target_ext, files,simulateOnly);
                }

            }

        }

        public void ListSiteFilesDir(string dir, string dir_patt, string source_ext,List<String> files)
        {

            string[] dirs = Directory.GetDirectories(dir);
            foreach (string d in dirs)
            {
                DirectoryInfo dinfo = new DirectoryInfo(d);
                if (!String.IsNullOrEmpty(dir_patt))
                {
                    if (dinfo.Name.ToLower() == dir_patt.ToLower())
                    {
                        ListSiteFiles(d, source_ext,  files);
                    }
                    else
                    { ListSiteFilesDir(d, dir_patt, source_ext, files); }
                }
                else
                {
                    ListSiteFiles(d, source_ext,  files);
                    ListSiteFilesDir(d, dir_patt, source_ext,  files);
                }

            }

        }

        public void CreateDirCopy(string dir, DirectoryInfo newdir, string dir_patt, string source_ext, List<String> files)
        {
            string[] dirs = Directory.GetDirectories(dir);
          
         
            foreach (string d in dirs)
            {
                DirectoryInfo dinfo = new DirectoryInfo(d);

                string path = newdir.FullName + "\\" + dinfo.Name;
                DirectoryInfo newdinfo = new DirectoryInfo(path);

                if (!newdinfo.Exists)
                    newdinfo = newdir.CreateSubdirectory(dinfo.Name);

                if (!String.IsNullOrEmpty(dir_patt))
                {
                    if (dinfo.Name.ToLower() == dir_patt.ToLower())
                    {
                        CopyDirFiles(d, newdinfo.FullName, source_ext, files);
                    }
                    else
                    { CreateDirCopy(d, newdinfo, dir_patt, source_ext,  files); }
                }
                else
                {
                    CopyDirFiles(d, newdir.FullName, source_ext, files);
                    CreateDirCopy(d, newdinfo, dir_patt, source_ext, files);
                }

            }

        }


        public void DeleteDirFiles(string dir, string dir_patt , string source_ext)
        {
            string[] dirs = Directory.GetDirectories(dir);
            foreach (string d in dirs)
            {
                DirectoryInfo dinfo = new DirectoryInfo(d);
                if (!String.IsNullOrEmpty(dir_patt))
                {
                    if (dinfo.Name.ToLower() == dir_patt.ToLower())
                    {
                        DeleteFiles(d, source_ext);
                    }
                    else
                    {
                        DeleteDirFiles(d, dir_patt, source_ext);
                    }
                }
                else
                {
                    DeleteFiles(d, source_ext);
                    DeleteDirFiles(d, dir_patt, source_ext);

                }
            }

        }
        public void CopyFiles(string dir, string source_ext, string target_ext, List<String> processedfiles)
        {
            string[] files=Directory.GetFiles(dir, "*" + source_ext);
            foreach (string file in files)
            {
                string filename = GetFileName(file, source_ext);
                string newfilename = filename +  target_ext;
                try
                {
                    
                    File.Copy(file, newfilename);
                    SetFileAccess(newfilename);
                    processedfiles.Add(newfilename);
                }
                catch (Exception ex)
                { //ignore}
                }
            }
            
        }

        public void CopyFilesWithOverwrite(string dir, string source_ext, string target_ext, List<String> processedfiles)
        {
            string[] files = Directory.GetFiles(dir, "*" + source_ext);
            foreach (string file in files)
            {
                string filename = GetFileName(file, source_ext);
                string newfilename = filename + target_ext;
                try
                {
                    if (File.Exists(newfilename))
                    {
                        SetFileAccess(newfilename);
                    }
                    File.Copy(file, newfilename,true);
                    SetFileAccess(newfilename);
                    processedfiles.Add(newfilename);
                }
                catch (Exception ex)
                { //ignore}
                }
            }

        }
        public void ListSiteFiles(string dir, string source_ext, List<String> processedfiles)
        {
            string[] files = Directory.GetFiles(dir, "*" + source_ext);
            foreach (string file in files)
            {
               
             
                try
                {
                    //string filename = GetFileName(file, source_ext);

                    processedfiles.Add(file);
                  
                }
                catch (Exception ex)
                { //ignore}
                }
            }

        }

        public void CopyIfNotExistsFiles(string dir, string source_ext, string target_ext, List<String> processedfiles, bool simulateOnly)
        {
            string[] files = Directory.GetFiles(dir, "*" + source_ext);
            foreach (string file in files)
            {
                string filename = GetFileName(file, source_ext);
                string newfilename = filename + target_ext;
                try
                {
                    if (!File.Exists(newfilename))
                    {
                        if (!simulateOnly)
                        {
                            File.Copy(file, newfilename);
                            SetFileAccess(newfilename);
                        }
                        processedfiles.Add(newfilename);
                    }
                }
                catch (Exception ex)
                { //ignore}
                }
            }

        }

        public void CopyDirFiles(string dir, string newdir,string source_ext, List<String> processedfiles)
        {
            string[] files = Directory.GetFiles(dir, "*" + source_ext);
            foreach (string file in files)
            {
               
                string newfilename = file.Replace(dir,newdir);

                File.Copy(file, newfilename,true);
                SetFileAccess(newfilename);
                processedfiles.Add(newfilename);

            }

        }

        public string GetFileName(string file, string ext)
        {
            string name = "";
            int ind = file.ToLower().IndexOf(ext.ToLower());
            if (ind >= 0)
            {

                name = file.Substring(0, ind);

            }
            return name;
        }

        public void DeleteFiles(string dir, string source_ext)
        {
            string[] files = Directory.GetFiles(dir, "*" + source_ext);
            foreach (string file in files)
            {
                SetFileAccess(file);
                File.Delete( file);


            }

        }

        public string ShowFileProcessStatus(string status, string filename)
        {
            return filename + " status";
        }

        public string GetProjectXMLFile(string relpath, string bldaction)
        {

            string xml = String.Format(prj_file_format, relpath, bldaction);
            return xml;

        }

        public void SetFileAccess(string file)
        {

         
                File.SetAttributes(file, FileAttributes.Normal);
        }


        public void AddToXmlFile(string xmlfile, string parentnodename, string nodename, string nodevalue)
        {
            XmlDocument docParent = new XmlDocument();
            docParent.Load(xmlfile);

            XmlNode node = docParent.SelectSingleNode(parentnodename);
            if (node != null)
            {
                XmlNode nodechild = docParent.CreateElement(nodename);
                nodechild.InnerXml = nodevalue;
                node.AppendChild(nodechild);

            }
            SetFileAccess(xmlfile);
            docParent.Save(xmlfile);


        }

        public XmlDocument LoadProjectXml(string xmlfile)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlfile);
            return doc;
        }

        public void RemoveFiles(string project, List<string> files)
        {
             XmlDocument doc=LoadProjectXml(project);
             XmlNamespaceManager mgr = new XmlNamespaceManager(doc.NameTable);
             mgr.AddNamespace("x", "http://schemas.microsoft.com/developer/msbuild/2003");
            foreach(string file in files)
            {
                string file1 = file.Substring(1);
                XmlNode node = doc.SelectSingleNode("/x:Project/x:ItemGroup/x:Content[@Include=\"" + file1 + "\"]", mgr);
                if (node == null)
                {
                    node = doc.SelectSingleNode("/x:Project/x:ItemGroup/x:EmbeddedResource[@Include=\"" + file1 + "\"]", mgr);
                }
                if (node != null)
                {
                    XmlNode parent = node.ParentNode;
                    parent.RemoveChild(node);
                }
            }
            doc.Save(project + ".xml");

        }

        public List<SiteCategory> GetArticleCategories(string xmlfile, int siteid, string uri)
        {
            XmlDocument cat = new XmlDocument();
            cat.Load(xmlfile);
            List<SiteCategory> categoryList = new List<SiteCategory>();
            SiteCategoryCollection categories= ServicesBL.Instance.GetContentArticleService(uri).RetrieveSiteCategories(siteid, false);
            XmlNodeList list = cat.SelectNodes("Categories/CategoryID");
            foreach (XmlNode node in list)
            {

                int id = Int32.Parse(node.InnerText);
                SiteCategory c = (SiteCategory)categories.GetSiteCategory(id);
                
                if(c.PublishedFlag)
                    categoryList.Add(c);
            }

            return categoryList;

        }


        public List<SiteArticle> GetArticle(int categoryid, int siteid, string uri)
        {

            List<SiteArticle> siteArticle = new List<SiteArticle>();
            SiteArticleCollection articles = ServicesBL.Instance.GetContentArticleService(uri).RetrieveCategorySiteArticles(categoryid, siteid);
            foreach (SiteArticle a in articles)
            {
                    a.FileID = categoryid;
                    siteArticle.Add(a);

            }

            return siteArticle;
        }
        public int CreateChangeList(string description)
        {
            try
            {
                P4Connection conn = new P4Connection();
                conn.Connect();
                P4PendingChangelist cl = conn.CreatePendingChangelist(description);
                conn.Disconnect();
                return cl.Number;
            }
            catch (Exception ex)
            { return 0; }
            
           
        }

        public void Checkout(int changelist, List<string> depotfiles)
        {
            try
            {
                P4Connection conn = new P4Connection();
                conn.Connect();
                foreach (string file in depotfiles)
                    conn.Run("edit", "-c", changelist.ToString(), file);
                conn.Disconnect();
            }
            catch (Exception ex)
            { }
            //    P4PendingChangelist cl = conn.CreatePendingChangelist("Testing new CL");
            //    conn.Run("edit", "-c" ,cl.Number.ToString(), "//depot/SiteClone/web/bedrock.matchnet.com/_Resources/Default.aspx.100.en-US.resx");
            //    MessageBox.Show("CL# " + cl.Number.ToString());
            //    clist = cl.Number;
            //    conn.Disconnect();
        }
        public void OpenForDelete(int changelist, List<string> depotfiles)
        {
            try
            {
                P4Connection conn = new P4Connection();
                conn.Connect();
                foreach (string file in depotfiles)
                    conn.Run("delete", "-c", changelist.ToString(), file);
                conn.Disconnect();
            }
            catch (Exception ex)
            { }
            //    P4PendingChangelist cl = conn.CreatePendingChangelist("Testing new CL");
            //    conn.Run("edit", "-c" ,cl.Number.ToString(), "//depot/SiteClone/web/bedrock.matchnet.com/_Resources/Default.aspx.100.en-US.resx");
            //    MessageBox.Show("CL# " + cl.Number.ToString());
            //    clist = cl.Number;
            //    conn.Disconnect();
        }

        public void Submit(int changelist)
        {
            try
            {
                P4Connection conn = new P4Connection();
                conn.Connect();

                conn.Run("submit", "-c", changelist.ToString());

                conn.Disconnect();
            }
            catch (Exception ex)
            { }
            //    P4PendingChangelist cl = conn.CreatePendingChangelist("Testing new CL");
            //    conn.Run("edit", "-c" ,cl.Number.ToString(), "//depot/SiteClone/web/bedrock.matchnet.com/_Resources/Default.aspx.100.en-US.resx");
            //    MessageBox.Show("CL# " + cl.Number.ToString());
            //    clist = cl.Number;
            //    conn.Disconnect();
        }
    }
}
