﻿using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.ComponentModel;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
namespace SparkSiteManagement.BL
{
    public class WFormBL
    {

          public static WFormBL Instance = new WFormBL();



          private WFormBL()
          { }
            
        public void populateGridFromPropertiesList<T>(List<T> objList, DataGridView grid, int startColumn, bool select)
        {
            if (objList == null || objList.Count == 0)
                return;
            Type t = objList[0].GetType();

            PropertyInfo[] pi = t.GetProperties();

            grid.ColumnCount = pi.Length + startColumn;

            int i = startColumn;

            foreach (PropertyInfo p in pi)
            {
                
                grid.Columns[i].HeaderText = p.Name;
                i++;
            }

            for (int l = 0; l < objList.Count; l++)
            {
                ArrayList vals = new ArrayList();
                for (int k = 0; k < startColumn; k++)
                {
                    vals.Add(select);
                }
                foreach (PropertyInfo p in pi)
                {
                    
                    try
                    {
                        var  vval = p.GetValue(objList[l], null);
                        string val = "";
                        if (vval != null)
                            val = vval.ToString();

                        if (val == "Matchnet.Content.ValueObjects.BrandConfig.Community")
                        {
                            Community comm = (Community)p.GetValue(objList[l], null);
                            val = comm.CommunityID.ToString();
                        }
                        else if (val == "Matchnet.Content.ValueObjects.BrandConfig.Site")
                        {
                            Site site = (Site)p.GetValue(objList[l], null);
                            val = site.SiteID.ToString();
                        }
                        vals.Add(val);
                    }
                    catch (Exception ex) { continue; }

                 

                }
                grid.Rows.Add(vals.ToArray());
            }

        }


        public void populateGridFromProperties(Object obj, DataGridView grid)
        {

            Type t = obj.GetType();

            PropertyInfo[] pi = t.GetProperties();
            ArrayList vals = new ArrayList();
            grid.ColumnCount = pi.Length;

            int i = 0;
            foreach (PropertyInfo p in pi)
            {
                grid.Columns[i].HeaderText = p.Name;
                string val = p.GetValue(obj, null).ToString();
                if (val == "Matchnet.Content.ValueObjects.BrandConfig.Community")
                {
                    Community comm = (Community)p.GetValue(obj, null);
                    val = comm.CommunityID.ToString();

                }
                else if (val == "Matchnet.Content.ValueObjects.BrandConfig.Site")
                {
                    Site site = (Site)p.GetValue(obj, null);
                    val = site.SiteID.ToString();

                }
                vals.Add(val);
                i++;
            }
            grid.Rows.Add(vals.ToArray());


        }

        public void populateGridFromPropertiesList<T>(List<T> objList, DataGridView grid, int startColumn, bool select, List<string>names)
        {
            if (objList == null || objList.Count == 0)
                return;
            Type t = objList[0].GetType();

            PropertyInfo[] pi = t.GetProperties();
            List<PropertyInfo>filteredPi=new List<PropertyInfo>();
            foreach (PropertyInfo p in pi)
            {
                if (!names.Contains(p.Name.ToLower()))
                    continue;
                filteredPi.Add(p);
            }
            grid.ColumnCount = filteredPi.Count + startColumn;

            int i = startColumn;

            foreach (PropertyInfo p in filteredPi)
            {
             
                grid.Columns[i].HeaderText = p.Name;
                i++;
            }

            for (int l = 0; l < objList.Count; l++)
            {
                ArrayList vals = new ArrayList();
                for (int k = 0; k < startColumn; k++)
                {
                    vals.Add(select);
                }
                foreach (PropertyInfo p in filteredPi)
                {

                    try
                    {
                       
                        var vval = p.GetValue(objList[l], null);
                        string val = "";
                        if (vval != null)
                            val = vval.ToString();

                        if (val == "Matchnet.Content.ValueObjects.BrandConfig.Community")
                        {
                            Community comm = (Community)p.GetValue(objList[l], null);
                            val = comm.CommunityID.ToString();
                        }
                        else if (val == "Matchnet.Content.ValueObjects.BrandConfig.Site")
                        {
                            Site site = (Site)p.GetValue(objList[l], null);
                            val = site.SiteID.ToString();
                        }
                        vals.Add(val);
                    }
                    catch (Exception ex) { continue; }



                }
                grid.Rows.Add(vals.ToArray());
            }

        }
    }
}
