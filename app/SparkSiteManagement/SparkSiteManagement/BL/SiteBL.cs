﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects.SparkWS;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Content.ValueObjects.Promotion;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Data.Hydra;
using System.Data;
using System.Data.SqlClient;
using Matchnet.Purchase;
using Matchnet.PromoEngine.ValueObjects.ServiceDefinitions;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
using Matchnet.Content.ValueObjects.Article;
namespace SparkSiteManagement
{
    public class SiteBL
    {
       
        public static SiteBL Instance = new SiteBL();

        Hashtable _settings = null;
        public List<string> SavedFiles = new List<string>();

        private SiteBL()
        {
            _settings = GetSettingID();
        }

        public Matchnet.Content.ValueObjects.BrandConfig.Site GetSite(int siteID, string uri)
        {
            try
            {
                Sites sites = ServicesBL.Instance.GetBrandConfigService(uri).GetSites();

                foreach (Site site in sites)
                {
                    if (site.SiteID == siteID)
                        return site;

                }
                return null;
            }
            catch (Exception ex)
            { return null; }

        }

        public List<Site> GetCommunitySites(int communityid, string uri)
        {
            try
            {
                List<Site> list = new List<Site>();
                Sites sites = ServicesBL.Instance.GetBrandConfigService(uri).GetSites();

                foreach (Site site in sites)
                {
                    if (site.Community.CommunityID == communityid)
                        list.Add(site);

                }
                return list;
            }
            catch (Exception ex)
            { return null; }

        }

        public bool ValidateSiteID(string siteid, string uri)
        {
            try
            {  
                Sites sites = ServicesBL.Instance.GetBrandConfigService(uri).GetSites();
                int id = Int32.Parse(siteid);
                foreach (Site site in sites)
                {
                    if (site.SiteID == id)
                        return false;

                }
                return true;
            }
            catch (Exception ex)
            { return false; }

        }

        public List<Site> GetSites(string uri)
        {
            try
            {   List<Site> list = new List<Site>();
                Sites sites = ServicesBL.Instance.GetBrandConfigService(uri).GetSites();
               
                foreach (Site site in sites)
                {
                    list.Add(site);

                }
                return list;
            }
            catch (Exception ex)
            { return null; }

        }

        public bool ValidateBrandID(string brand, string uri)
        {
            try
            {
                Brands brands = ServicesBL.Instance.GetBrandConfigService(uri).GetBrands();
                int id = Int32.Parse(brand);
                foreach (Brand b in brands)
                {
                    if (b.BrandID == id)
                        return false;

                }
                return true;
            }
            catch (Exception ex)
            { return false; }

        }

        public List<Brand> GetSiteBrands(int siteID, string uri)
        {
            try
            {
                List<Brand> sitebrands = new List<Brand>();
                Brands brands = ServicesBL.Instance.GetBrandConfigService(uri).GetBrands();

                foreach (Brand brand in brands)
                {
                    if (brand.Site.SiteID == siteID)
                        sitebrands.Add(brand);

                }
                return sitebrands;
            }
            catch (Exception ex)
            { return null; }

        }



        public List<VO.Setting> GetSettings(int siteid, string uri)
        {
            NameValueCollection settingsCollection;
            Hashtable settings = new Hashtable();

            try
            {
                string settingURI = uri.ToLower().Replace("adapterconfigurationsm", "SettingsSM");
                settingsCollection = ServicesBL.Instance.GetSettingService(settingURI).GetSettings("");
                if (settingsCollection != null && settingsCollection.Count > 0)
                {
                    for (Int32 i = 0; i < settingsCollection.Count; i++)
                    {
                        string[] nameParts = settingsCollection.Keys[i].Split('/');
                        bool isMachine = false;
                        Int32 key = 0;
                        string name;

                        if (nameParts[0] == "OVERRIDE")
                        {
                            isMachine = true;
                        }

                        if (nameParts[3] != "*")
                        {
                            key = Convert.ToInt32(nameParts[3]);
                        }
                        else if (nameParts[2] != "*")
                        {
                            key = Convert.ToInt32(nameParts[2]);
                        }
                        else if (nameParts[1] != "*")
                        {
                            key = Convert.ToInt32(nameParts[1]);
                        }

                        name = nameParts[4];

                        Hashtable tableCurrent = settings[key] as Hashtable;
                        if (tableCurrent == null)
                        {
                            tableCurrent = new Hashtable();
                            settings[key] = tableCurrent;
                        }

                        if (isMachine)
                        {
                            tableCurrent[name] = settingsCollection[i];
                        }
                        else if (!tableCurrent.ContainsKey(name))
                        {
                            tableCurrent[name] = settingsCollection[i];
                        }
                    }

                   
                }
                List<VO.Setting> listSettings = new List<VO.Setting>();
                Hashtable sitesett = (Hashtable)settings[siteid];
                foreach (string skey in sitesett.Keys)
                {
                    VO.Setting sett = new VO.Setting();
                    sett.settingConst = skey;
                    if(sitesett[skey] != null)
                        sett.settingValue = sitesett[skey].ToString();

                    if (_settings != null && _settings[skey] != null)
                        sett.settingID = Int32.Parse(_settings[skey].ToString());
                    else
                        sett.settingID = 0;
                    listSettings.Add(sett);
                }
                listSettings.Sort();
                return listSettings;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public List<SitePage> GetSitePage(int siteid, string uri)
        {
            SitePages sitepages;
            Hashtable pages = new Hashtable();
            List<SitePage> list = new List<SitePage>();
            try
            {

                sitepages = ServicesBL.Instance.GetPageConfigService(uri).GetSitePages();

                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Hashtable GetSettingID()
        {
            try
            {
                Matchnet.Data.Command comm = new Matchnet.Data.Command("mnSystem", "up_GetSettingID", 0);
                Hashtable settings = new Hashtable();

                DataTable dt = Matchnet.Data.Client.Instance.ExecuteDataTable(comm);
                foreach (DataRow dr in dt.Rows)
                {
                    int id = Int32.Parse(dr["SettingID"].ToString());
                    string settconst = dr["SettingConstant"].ToString();
                    settings[settconst] = id;
                }

                return settings;
            }
            catch (Exception ex)
            { return null; }
        }


        public List<VO.SitePage> GetSitePages(int siteid)
        {
            Matchnet.Data.Command comm = new Matchnet.Data.Command("mnSystem", "up_SitePages_List", 0);
            comm.AddParameter("siteid", SqlDbType.Int, ParameterDirection.Input, siteid);
            List<VO.SitePage> pages = new List<VO.SitePage>();

            DataTable dt = Matchnet.Data.Client.Instance.ExecuteDataTable(comm);
            foreach (DataRow dr in dt.Rows)
            {
                VO.SitePage p = new VO.SitePage(Int32.Parse(dr["appid"].ToString()), dr["name"].ToString(), dr["path"].ToString(), Int32.Parse(dr["pageid"].ToString()), Int32.Parse(dr["siteid"].ToString()), dr["controlname"].ToString(), Int32.Parse(dr["securitymask"].ToString()), Int32.Parse(dr["LayoutTemplateID"].ToString()), dr["pagename"].ToString());
                pages.Add(p);
            }

            return pages;
        }

        public List<VO.ListCategory> GetSiteListCategory(int siteid)
        {
            List<VO.ListCategory> categories = new List<VO.ListCategory>();
            try
            {
                Matchnet.Data.Command comm = new Matchnet.Data.Command("mnSystem", "up_ListCategorySite_ListAll", 0);
                comm.AddParameter("siteid", SqlDbType.Int, ParameterDirection.Input, siteid);

                DataTable dt = Matchnet.Data.Client.Instance.ExecuteDataTable(comm);
                foreach (DataRow dr in dt.Rows)
                {
                    VO.ListCategory l = new VO.ListCategory();
                    l.Listcategoryid = Int32.Parse(dr["ListCategoryID"].ToString());
                    l.Siteid = siteid;
                    l.ResourceKey = dr["ResourceKey"].ToString();
                    l.Listorder = Int32.Parse(dr["ListOrder"].ToString());

                    categories.Add(l);
                }

                return categories;
            }
            catch (Exception ex)
            {
                return categories;
            }
        }


        public List<VO.RegistrationPromotion> GetRegistrationPromotion(int domainid)
        {
            List<VO.RegistrationPromotion> list = new List<VO.RegistrationPromotion>();
            try
            {
                Matchnet.Data.Command comm = new Matchnet.Data.Command("mnSystem", "up_RegistrationPromotionDomain_List", 0);
                comm.AddParameter("domainid", SqlDbType.Int, ParameterDirection.Input, domainid);

                DataTable dt = Matchnet.Data.Client.Instance.ExecuteDataTable(comm);
                foreach (DataRow dr in dt.Rows)
                {
                    VO.RegistrationPromotion p = new VO.RegistrationPromotion();
                    p.LoadRegistrationPromotion(dr);

                    list.Add(p);
                }
                return list;
            }
            catch (Exception ex)
            {
                return list;
            }
            
        }

        public List<VO.ObjectGroup> GetObjGroups(int siteid, ref int maxobjgroupid)
        {
            List<VO.ObjectGroup> list = new List<VO.ObjectGroup>();
            try
            {
                Matchnet.Data.Command comm = new Matchnet.Data.Command("mnSystem", "up_ObjectTypeGroup_List", 0);
                comm.AddParameter("siteid", SqlDbType.Int, ParameterDirection.Input, siteid);

                DataSet ds = Matchnet.Data.Client.Instance.ExecuteDataSet(comm);
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    VO.ObjectGroup p = new VO.ObjectGroup(dr);
                    list.Add(p);
                }
                DataTable dt1 = ds.Tables[1];
                DataRow dr1 = dt1.Rows[0];
                maxobjgroupid = Int32.Parse(dr1["maxobjectgroupid"].ToString());
                return list;
            }
            catch (Exception ex)
            { return list; }
        }

        public List<Community> GetCommunities(List<Brand> brands)
        {
            List<Community> comm = new List<Community>();

            Hashtable retVal = new Hashtable();

            for(int i=0;i < brands.Count;i++)
            {
                if (!comm.Contains(brands[i].Site.Community))
                {
                    comm.Add(brands[i].Site.Community);
                }
            }


            return comm;

        }

        public DataSet GetProfileSections(int commid, int siteid, int brandid)
        {
            try
            {
                Matchnet.Data.Command comm = new Matchnet.Data.Command("mnSystem", "up_ProfileSectionsDefinition_list", 0);
                comm.AddParameter("communityid", SqlDbType.Int, ParameterDirection.Input, commid);
                comm.AddParameter("siteid", SqlDbType.Int, ParameterDirection.Input, siteid);
                comm.AddParameter("brandid", SqlDbType.Int, ParameterDirection.Input, brandid);


                DataSet ds = Matchnet.Data.Client.Instance.ExecuteDataSet(comm);

                return ds;
            }
            catch (Exception ex)
            { return null; }


        }


        public List<VO.ProfileSectionAttribute> GetProfileSectionsAttr(DataTable dt)
        {

            List<VO.ProfileSectionAttribute> list = new List<VO.ProfileSectionAttribute>();
            foreach (DataRow dr in dt.Rows)
            {
                VO.ProfileSectionAttribute psa = new VO.ProfileSectionAttribute(dr);
                list.Add(psa);

            }

            return list;
        }

        public List<VO.ProfileSectionGroup> GetProfileSectionsGroup(DataTable dt)
        {

            List<VO.ProfileSectionGroup> list = new List<VO.ProfileSectionGroup>();
            foreach (DataRow dr in dt.Rows)
            {
                VO.ProfileSectionGroup psa = new VO.ProfileSectionGroup(dr);
                list.Add(psa);

            }

            return list;
        }


        public List<Matchnet.Purchase.ValueObjects.CreditCard> GetCCTypes(int siteid, string uri)
        {

            List<Matchnet.Purchase.ValueObjects.CreditCard> list = new List<Matchnet.Purchase.ValueObjects.CreditCard>();

            Matchnet.Purchase.ValueObjects.CreditCardCollection coll = ServicesBL.Instance.GetPurchaseService(uri).GetCreditCardTypes(siteid);
            foreach (Matchnet.Purchase.ValueObjects.CreditCard cc in coll)
            {
                list.Add(cc);
            }

            return list;
        }

        public Hashtable GetMaxID()
        {
            Matchnet.Data.Command comm = new Matchnet.Data.Command("mnSystem", "up_ListMax_IDs", 0);
            Hashtable maxids = new Hashtable();

            SqlDataReader rs = Matchnet.Data.Client.Instance.ExecuteReader(comm);

            if (rs.Read())
            {
                maxids["maxattrgroupid"] = rs["maxattrgroupid"];
                maxids["maxattroptgroupid"] = rs["maxattroptgroupid"];
                maxids["maxsitesettingid"] = rs["maxsitesettingid"];
                maxids["regpromoid"] = rs["regpromoid"];
                maxids["objgroupid"] = rs["objgroupid"];
                maxids["ccgroupid"] = rs["ccgroupid"];
                maxids["maxpropertyid"] = rs["maxpropertyid"];
                maxids["maxsitearticleid"] = rs["maxsitearticleid"];
                maxids["maxsitecategoryid"] = rs["maxsitecategoryid"];
                maxids["maxresourcetemplateid"] = rs["maxresourcetemplateid"];
            }
            return maxids;



        }


        public List<VO.PhotoSearchOption> GetPhotoSearchOptions(int siteid)
        {
            List<VO.PhotoSearchOption> list = new List<VO.PhotoSearchOption>();

            Matchnet.Data.Command comm = new Matchnet.Data.Command("mnPhotoStore", "up_PhotoSearchOptions_List", 0);
            comm.AddParameter("siteid", SqlDbType.Int, ParameterDirection.Input, siteid);

            DataSet ds = Matchnet.Data.Client.Instance.ExecuteDataSet(comm);
            DataTable dt = ds.Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                VO.PhotoSearchOption p = new VO.PhotoSearchOption(dr);
                list.Add(p);
            }
      
            return list;
        }

        public List<ResourceTemplate> GetResourceTemplates(int brandid, string uri)
        {
            List<ResourceTemplate> templates = new List<ResourceTemplate>();
            ResourceTemplateCollection coll = ServicesBL.Instance.GeResourceTemplateService(uri).GetResourceTemplateCollection("", null);

            foreach (ResourceTemplate t in coll)
            {
                if (t.GroupID == brandid)
                    templates.Add(t);

            }
            return templates;

        }

        public List<SiteCategory> GetArticleCategories(int siteid, string uri)
        {
           
            List<SiteCategory> categoryList = new List<SiteCategory>();
            try
            {
                SiteCategoryCollection categories = ServicesBL.Instance.GetContentArticleService(uri).RetrieveSiteCategories(siteid, false);

                foreach (SiteCategory c in categories)
                {
                    if (c.PublishedFlag)
                        categoryList.Add(c);
                }
                return categoryList;
            }
            catch (Exception ex) { return categoryList; }
            

        }

        
    }
}
