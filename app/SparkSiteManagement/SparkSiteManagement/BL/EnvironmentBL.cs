﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace SparkSiteManagement
{
    public class EnvironmentBL
    {

        private static List<Environment> environments = null;
        public static EnvironmentBL Instance = new EnvironmentBL();

        public List<Environment> Environments
        {
            get { return environments; }
        }
        private EnvironmentBL()
        {
            try
            { Load(); }
            catch (Exception ex)
            { }

        }


        public static void Load()
        {
            if (environments == null) { environments = new List<Environment>(); }

            environments.Clear();

            string xmlFile = System.Configuration.ConfigurationSettings.AppSettings["envfile"];
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);
            XmlNodeList list = doc.DocumentElement.GetElementsByTagName("env");
            for (int i = 0; i < list.Count; i++)
            {
                XmlNode env = list[i];
                string name = env.SelectSingleNode("name").InnerText;
                string uri = env.SelectSingleNode("configservice").InnerText;
                string contenturi = env.SelectSingleNode("contentservice").InnerText;
                string purchaseuri = env.SelectSingleNode("purchaseservice").InnerText;
                string resourcetempluri = env.SelectSingleNode("resourcetemplateservice").InnerText;
                string machine = "";
                if (env.SelectSingleNode("override") != null)
                    machine = env.SelectSingleNode("override").InnerText;
                Environment environment = new Environment();
                environment.Code = name;
                environment.ConfigServiceURI = uri;
                environment.ContentServiceURI = contenturi;
                environment.PurchaseServiceURI = purchaseuri;
                environment.ResourceTemplateServiceURI = resourcetempluri;
                environment.overrideMachine = machine;
                environments.Add(environment);

            }
        }
    }
}
