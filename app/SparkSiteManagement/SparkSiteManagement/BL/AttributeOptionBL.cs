﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;

namespace SparkSiteManagement.BL
{
    
    public class AttributeOptionBL
    {
       public static AttributeOptionBL Instance = new AttributeOptionBL();

        Matchnet.Content.ValueObjects.AttributeOption.Attributes attributes = null;

        private AttributeOptionBL()
        {

        }


        public Matchnet.Content.ValueObjects.AttributeOption.Attributes GetAttributeOptions(string uri)
        {
            attributes = null;
            IAttributeOptionService attr = ServicesBL.Instance.GetAttributeOptionService(uri);
            attributes = attr.GetAttributes();
            return attributes;
        }

        public List<VO.AttributeOption> GetAttributeOptions(string uri, int groupid)
        {
            List<VO.AttributeOption> list = new List<VO.AttributeOption>();
            if (attributes == null)
                GetAttributeOptions(uri);
            ICollection attrnames = attributes.GetAttributeNames();
            IEnumerator enumerator = attrnames.GetEnumerator();
            while( enumerator.MoveNext())
            {
                if (attributes.ContainsGroup((string)enumerator.Current, groupid))
                {
                AttributeOptionCollection coll = attributes.Get((string)enumerator.Current, groupid);

                    foreach (AttributeOption o in coll)
                    {
                       
                            VO.AttributeOption opt = new VO.AttributeOption(o, (string)enumerator.Current);
                            opt.GroupID = groupid;
                            list.Add(opt);
                     }
                }
            }

            return list;
        }
    }
}
