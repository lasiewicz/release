﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PageConfig;
using Matchnet.Content.ValueObjects.Article;
using SparkSiteManagement.BL;
//using P4API;
namespace SparkSiteManagement
{
    public partial class ProjectConfig : Form
    {
        int clist = 0;
        List<String> files = new List<string>();
        List<string> perforcefiles = new List<string>();
        public ProjectConfig()
        {
            InitializeComponent();
        }

        private int SourceSiteID
        {
            get
            {
                int s = 0;
                if(cboSites.SelectedItem!=null)
                    Int32.TryParse(cboSites.SelectedItem.ToString(), out s);
                return s;
            }

        }

        private int TargetSiteID
        {
            get
            {

                int sid = 0;
                
                Int32.TryParse(txtNewSiteID.Text, out sid);
                return sid;
            }

        }

        private string SourceCulture
        {
            get
            {
              

                return cboCulture.Text;
            }



        }

        private string TargetCulture
        {
            get
            {
                string c = "";
                if (!String.IsNullOrEmpty(txtNewCulture.Text))
                {
                    c = txtNewCulture.Text;
                    return c;
                }

                return cboNewCulture.Text;
            }



        }
        private void btnOpenProject_Click(object sender, EventArgs e)
        {
            dlgProject.ShowDialog();
            txtProject.Text = dlgProject.FileName;
            int ind = txtProject.Text.LastIndexOf("\\");
            string prjpath = txtProject.Text.Substring(0, ind);

            prjpath+="\\Framework\\Ui\\Advertising\\GAM.xml";
            txtGAMFile.Text = prjpath;
            webGAMXML.Navigate(txtGAMFile.Text);
            rtfGAMAddXML.Text = ProjectBL.Instance.GAM_XML_ATTR;
            
        }

        private void cboCulture_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ProjectConfig_Load(object sender, EventArgs e)
        {
            loadEnv();
            GetSites();
            cboCSS.Checked = true;
            cboXML.Checked = true;
            cboResx.Checked = true;
            txtDepotBranch.Text = "//depot/Sopranos/web/bedrock.matchnet.com";
            txtCategoryFile.Text = System.Configuration.ConfigurationSettings.AppSettings["articlecategories"];
        }


        private void loadEnv()
        {
            try
            {

                cboEnv.Items.Clear();
                for (int i = 0; i < EnvironmentBL.Instance.Environments.Count; i++)
                {
                    cboEnv.Items.Add(EnvironmentBL.Instance.Environments[i].ConfigServiceURI);
                    cboEnv.DisplayMember = EnvironmentBL.Instance.Environments[i].Code;

                }
                cboEnv.SelectedIndex = 1;

             
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

        }

        private void GetSites()
        {

            cboSites.Items.Clear();
         
            string uri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ContentServiceURI;

            List<Site> sites = SiteBL.Instance.GetSites(uri);

            foreach (Site s in sites)
            {
                cboSites.Items.Add(s.SiteID);
            }

          
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtProject.Text))
            {
                MessageBox.Show("Please select project");
                return;
            }

            string prj = txtProject.Text;

            int ind = prj.LastIndexOf("\\");
            string prjpath = prj.Substring(0, ind);
            files.Clear();
            if(cboResx.Checked)
                files =ProjectBL.Instance.ProcessSiteFiles(prjpath, "_Resources", "resx", SourceSiteID, SourceCulture, TargetSiteID, TargetCulture,cboOverwrite.Checked);

            if (cboXML.Checked)
            {
                List<String> xmlList1 = ProjectBL.Instance.ProcessSiteFiles(prjpath, "", "xml", "_{0}.xml", SourceSiteID, TargetSiteID, cboOverwrite.Checked);
                List<String> xmlList2 = ProjectBL.Instance.ProcessSiteFiles(prjpath, "", "xml", ".{0}.xml", SourceSiteID, TargetSiteID, cboOverwrite.Checked);
                 files.AddRange(xmlList1);
                 files.AddRange(xmlList2);
            }
            if (cboCSS.Checked)
            {
                List<String> xmlList3 = ProjectBL.Instance.ProcessSiteFiles(prjpath, "", "css", ".{0}.css", SourceSiteID, TargetSiteID, cboOverwrite.Checked);
                files.AddRange(xmlList3);
            }
            string txt = "";
            StringBuilder bld=new StringBuilder();
            perforcefiles.Clear();
            foreach(string s in files)
            {

                bld.Append(s.Replace(prjpath,"") + "\r\n");
                string depotversion = s.Replace(prjpath, "");
                depotversion = depotversion.Replace("\\", "/");
                perforcefiles.Add(txtDepotBranch.Text + depotversion);
            }
            rtfFiles.Clear();
            rtfFiles.Text = bld.ToString();

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnGetPrjXML_Click(object sender, EventArgs e)
        {
            string t = rtfFiles.Text;
            rtfPrjXML.Text = "";
            StringBuilder bld=new StringBuilder();
            foreach(string s in files)
            {
                bld.Append(ProjectBL.Instance.GetProjectXMLFile(s,"Content"));

            }
            rtfPrjXML.Text = bld.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtProject.Text))
            {
                MessageBox.Show("Please select project");
                return;
            }

            string prj = txtProject.Text;

            int ind = prj.LastIndexOf("\\");
            string prjpath = prj.Substring(0, ind);
            ProjectBL.Instance.DeleteSiteFiles(prjpath, "_Resources", "resx", TargetSiteID, TargetCulture);
            ProjectBL.Instance.DeleteSiteFiles(prjpath, "{0}.{1}", "xml", TargetSiteID);
            ProjectBL.Instance.DeleteSiteFiles(prjpath, "{0}.{1}", "css", TargetSiteID);
            string txt = "";
            MessageBox.Show("Finish deleting css, xml and resx files for " + TargetSiteID.ToString());
        }

        private void btnAddToGamXML_Click(object sender, EventArgs e)
        {
            ProjectBL.Instance.AddToXmlFile(txtGAMFile.Text, "/attributes/attribute[name='Site']/attributeoptions", "attributeoption", rtfGAMAddXML.Text);
        }

        private void btnBrowseNewProj_Click(object sender, EventArgs e)
        {
            dlgFolder.ShowDialog();
            txtNewProj.Text =dlgFolder.SelectedPath;

          
        }

        private void btnCopy1_Click(object sender, EventArgs e)
        {
            string newprjpath = txtNewProj.Text;
            if (String.IsNullOrEmpty(newprjpath))
            {
                MessageBox.Show("Please select directory to copy resource files.");
                return;
            }
            string prj = txtProject.Text;

            if (String.IsNullOrEmpty(prj))
            {
                MessageBox.Show("Please select source project directory to copy resource files.");
                return;
            }
            if (String.IsNullOrEmpty(txtNewSiteID.Text) || String.IsNullOrEmpty(TargetCulture))
            {
                MessageBox.Show("Please enter new siteid and new culture");
                return;
            }
            int ind = prj.LastIndexOf("\\");
            string prjpath = prj.Substring(0, ind);
            DirectoryInfo dinfo = new DirectoryInfo(newprjpath);
            ProjectBL.Instance.CreateSiteFilesCopy(prjpath, newprjpath, "_Resources", "resx", ".{0}.{1}.{2}", TargetSiteID, TargetCulture);
        }

        private void btnCopyToPrj_Click(object sender, EventArgs e)
        {
            string newprjpath = txtNewProj.Text;
            if (String.IsNullOrEmpty(newprjpath))
            {
                MessageBox.Show("Please select directory from which to copy resource files.");
                return;
            }
            string prj = txtProject.Text;

            if (String.IsNullOrEmpty(prj))
            {
                MessageBox.Show("Please select target project directory to copy resource files.");
                return;
            }
            if (String.IsNullOrEmpty(txtNewSiteID.Text) || String.IsNullOrEmpty(TargetCulture))
            {
                MessageBox.Show("Please enter new siteid and new culture");
                return;
            }
            int ind = prj.LastIndexOf("\\");
            string prjpath = prj.Substring(0, ind);
            DirectoryInfo dinfo = new DirectoryInfo(newprjpath);
            ProjectBL.Instance.CreateSiteFilesCopy(newprjpath, prjpath, "_Resources", "resx", ".{0}.{1}.{2}", TargetSiteID, cboNewCulture.Text);

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnGetArticleCat_Click(object sender, EventArgs e)
        {
            if (SourceSiteID <=0 )
            {
                MessageBox.Show("Please select siteid");
                return;
            }
            
            dgvCategories.Columns.Clear();
            string xmlFile = System.Configuration.ConfigurationSettings.AppSettings["articlecategories"];
            string uri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ContentServiceURI;
            string artcleuri = uri.ToLower().Replace("brandconfigsm", "ArticleSM");

            List<SiteCategory> list = ProjectBL.Instance.GetArticleCategories(xmlFile, SourceSiteID, artcleuri);
            List<string> propsDisplay = new List<string>();
            propsDisplay.Add("categoryid");
            propsDisplay.Add("constant");

            DataGridViewColumn cellCheck = new DataGridViewCheckBoxColumn();
            dgvCategories.Columns.Insert(0, cellCheck);
            dgvCategories.Columns[0].HeaderText = "Select";
            WFormBL.Instance.populateGridFromPropertiesList<SiteCategory>(list, dgvCategories, 0, true,propsDisplay);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            dgvCategories.Columns.Clear();

        }

        private void btnGetFromFile_Click(object sender, EventArgs e)
        {
            if (SourceSiteID <= 0)
            {
                MessageBox.Show("Please select source siteid");
                return;
            }
            dgvCategories.Columns.Clear();
            string xmlFile = txtCategoryFile.Text;
            string uri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ContentServiceURI;
            string artcleuri = uri.ToLower().Replace("brandconfigsm", "ArticleSM");

            List<SiteCategory> list = ProjectBL.Instance.GetArticleCategories(xmlFile, SourceSiteID, artcleuri);
            List<string> propsDisplay = new List<string>();
            DataGridViewColumn cellCheck = new DataGridViewCheckBoxColumn();
            dgvCategories.Columns.Insert(0, cellCheck);
            propsDisplay.Add("categoryid");
            propsDisplay.Add("constant");
            propsDisplay.Add("content");
            WFormBL.Instance.populateGridFromPropertiesList<SiteCategory>(list, dgvCategories, 1, true, propsDisplay);
        }

        private void btnSaveCategories_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlgFile = new SaveFileDialog();
            dlgFile.Filter = "XML file|*.xml";
            dlgFile.FileName = "category_" + SourceSiteID.ToString();
            dlgFile.ShowDialog();
            txtCategoryFile.Text = dlgFile.FileName;


            TextWriter cats = new StreamWriter(txtCategoryFile.Text);
            cats.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            cats.WriteLine("<Categories>");
            for (int i = 0; i < dgvCategories.Rows.Count - 1; i++)
            {
                if (Boolean.Parse(dgvCategories.Rows[i].Cells[0].Value.ToString()))
                {

                    string id = dgvCategories.Rows[i].Cells[1].Value.ToString();
                    cats.WriteLine("<Category>" + id + "</Category>");

                }

            }
            cats.WriteLine("</Categories>");
            cats.Close();
        }

        private void btnGetCategory_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlgFile=new SaveFileDialog();
            dlgFile.Filter = "XML file|*.xml";
            dlgFile.FileName = "category_" + SourceSiteID.ToString();
            dlgFile.ShowDialog();
            txtCategoryFile.Text = dlgSaveFile.FileName;
          
            
        }

        private void btnGetArticles_Click(object sender, EventArgs e)
        {

            if (SourceSiteID <= 0)
            {
                MessageBox.Show("Please select source siteid");
                return;
            }
            string uri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ContentServiceURI;
            string artcleuri = uri.ToLower().Replace("brandconfigsm", "ArticleSM");
            dgvArticles.Columns.Clear();
            List<SiteArticle> articles = new List<SiteArticle>();
            List<string> propsDisplay = new List<string>();
           
            propsDisplay.Add("articleid");
            propsDisplay.Add("title");
            propsDisplay.Add("content");
            propsDisplay.Add("fileid");
            for (int i = 0; i < dgvCategories.Rows.Count - 1; i++)
            {
                if (Boolean.Parse(dgvCategories.Rows[i].Cells[0].Value.ToString()))
                {

                    string id = dgvCategories.Rows[i].Cells[1].Value.ToString();
                    List<SiteArticle> sa= ProjectBL.Instance.GetArticle(Int32.Parse( id), SourceSiteID, artcleuri);
                    articles.AddRange(sa);
                }

            }

            WFormBL.Instance.populateGridFromPropertiesList<SiteArticle>(articles, dgvArticles, 0, true, propsDisplay);
           
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string template="";
            string uri = EnvironmentBL.Instance.Environments[cboEnv.SelectedIndex].ContentServiceURI;
            string artcleuri = uri.ToLower().Replace("brandconfigsm", "ArticleSM");
            dlgFolder.ShowDialog();
            string path = dlgFolder.SelectedPath;
            string articlenameformat="article_{0}.{1}.{2}.htm";
            DialogResult result = MessageBox.Show("Do you want to save Article as html files to " + path, "Save files", MessageBoxButtons.YesNo);
            if (result == DialogResult.No)
                return;
            template = DBScripting.SiteDB.Instance.GetSQLTemplate("article_no_sql");
            SiteCategoryCollection categories = ServicesBL.Instance.GetContentArticleService(artcleuri).RetrieveSiteCategories(SourceSiteID, false);
            for (int i = 0; i < dgvArticles.Rows.Count - 1; i++)
            {    string id = dgvArticles.Rows[i].Cells[0].Value.ToString();
                string content = dgvArticles.Rows[i].Cells[3].Value.ToString();
                string category = dgvArticles.Rows[i].Cells[1].Value.ToString();
                string categorytitle=categories.GetSiteCategory(Int32.Parse(category)).Content;
                string title= dgvArticles.Rows[i].Cells[2].Value.ToString();
                TextWriter w = new StreamWriter(path + "\\" + String.Format(articlenameformat,SourceSiteID,category,id));
                string fileText = String.Format(template, category, categorytitle, id.ToString(), title, content);
                w.Write(fileText);
                w.Close();
            }

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
        //    P4Connection conn = new P4Connection();
        //    conn.Connect();
        //    P4PendingChangelist cl = conn.CreatePendingChangelist("Testing new CL");
        //    conn.Run("edit", "-c" ,cl.Number.ToString(), "//depot/SiteClone/web/bedrock.matchnet.com/_Resources/Default.aspx.100.en-US.resx");
        //    MessageBox.Show("CL# " + cl.Number.ToString());
        //    clist = cl.Number;
        //    conn.Disconnect();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            //P4Connection conn = new P4Connection();
            //conn.Connect();
            
            //conn.Run("submit",  "-c" ,clist.ToString());
           
            //conn.Disconnect();
        }

        private void btnCopy20Resx_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtProject.Text))
            {
                MessageBox.Show("Please select project");
                return;
            }

            string prj = txtProject.Text;

            int ind = prj.LastIndexOf("\\");
            string prjpath = prj.Substring(0, ind);
            files.Clear();
          //  files = ProjectBL.Instance.ProcessSiteFiles(prjpath, "_Resources", "resx", SourceSiteID, SourceCulture, TargetSiteID, TargetCulture);
            List<String> xmlList1 = ProjectBL.Instance.ProcessSiteFiles(prjpath, "", "resx", "20.ascx.{0}.{1}.resx", SourceSiteID, TargetSiteID, SourceCulture,TargetCulture);
          
            files.AddRange(xmlList1);
          
            string txt = "";
            StringBuilder bld = new StringBuilder();
            foreach (string s in files)
            {

                bld.Append(s.Replace(prjpath, "") + "\r\n");
            }
            rtfFiles.Clear();
            rtfFiles.Text = bld.ToString();
        }

        private void btnCopyDelta_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtProject.Text))
                {
                    MessageBox.Show("Please select project");
                    return;
                }

                string prj = txtProject.Text;

                int ind = prj.LastIndexOf("\\");
                string prjpath = prj.Substring(0, ind);
                files.Clear();
                files = ProjectBL.Instance.ProcessSiteDeltaFiles(prjpath, "_Resources", "resx", SourceSiteID, SourceCulture, TargetSiteID, TargetCulture,false);
                List<String> xmlList1 = ProjectBL.Instance.ProcessSiteDeltaFiles(prjpath, "", "xml", "_{0}.xml", SourceSiteID, TargetSiteID, false);
                List<String> xmlList2 = ProjectBL.Instance.ProcessSiteDeltaFiles(prjpath, "", "xml", ".{0}.xml", SourceSiteID, TargetSiteID, false);
                List<String> xmlList3 = ProjectBL.Instance.ProcessSiteDeltaFiles(prjpath, "", "css", ".{0}.css", SourceSiteID, TargetSiteID, false);
                files.AddRange(xmlList1);
                files.AddRange(xmlList2);
                files.AddRange(xmlList3);
                string txt = "";
                StringBuilder bld = new StringBuilder();
                foreach (string s in files)
                {

                    bld.Append(s.Replace(prjpath, "") + "\r\n");
                }
                rtfFiles.Clear();
                rtfFiles.Text = bld.ToString();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnShowDelta_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtProject.Text))
                {
                    MessageBox.Show("Please select project");
                    return;
                }

                string prj = txtProject.Text;

                int ind = prj.LastIndexOf("\\");
                string prjpath = prj.Substring(0, ind);
                files.Clear();
                files = ProjectBL.Instance.ProcessSiteDeltaFiles(prjpath, "_Resources", "resx", SourceSiteID, SourceCulture, TargetSiteID, TargetCulture, true);
                List<String> xmlList1 = ProjectBL.Instance.ProcessSiteDeltaFiles(prjpath, "", "xml", "_{0}.xml", SourceSiteID, TargetSiteID, true);
                List<String> xmlList2 = ProjectBL.Instance.ProcessSiteDeltaFiles(prjpath, "", "xml", ".{0}.xml", SourceSiteID, TargetSiteID, true);
                List<String> xmlList3 = ProjectBL.Instance.ProcessSiteDeltaFiles(prjpath, "", "css", ".{0}.css", SourceSiteID, TargetSiteID, true);
                files.AddRange(xmlList1);
                files.AddRange(xmlList2);
                files.AddRange(xmlList3);
                string txt = "";
                StringBuilder bld = new StringBuilder();
                foreach (string s in files)
                {

                    bld.Append(s.Replace(prjpath, "") + "\r\n");
                }
                rtfFiles.Clear();
                rtfFiles.Text = bld.ToString();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnCreateCL_Click(object sender, EventArgs e)
        {
           txtChangeList.Text= ProjectBL.Instance.CreateChangeList(txtChangeListDescription.Text).ToString();
        }

        private void btnCheckout_Click(object sender, EventArgs e)
        {
            try{
                int changelist=Int32.Parse(txtChangeList.Text);
                ProjectBL.Instance.Checkout(changelist, perforcefiles);
            }catch(Exception ex)
            {
                MessageBox.Show("Error :" +  ex.Message + "\r\n" + "Did you create Change List?\r\nReally?");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ListFiles();
        }

        private List<string> ListFiles()
        {
            List<string> filelist = new List<string>();
            if (String.IsNullOrEmpty(txtProject.Text))
            {
                MessageBox.Show("Please select project");
                return null;
            }

            string prj = txtProject.Text;

            int ind = prj.LastIndexOf("\\");
            string prjpath = prj.Substring(0, ind);
            files.Clear();

            files = ProjectBL.Instance.ListSite(prjpath, "_Resources", "resx", SourceSiteID, SourceCulture);

            List<String> xmlList1 = ProjectBL.Instance.ListSite(prjpath, "", "xml", "_{0}.xml", SourceSiteID);
            List<String> xmlList2 = ProjectBL.Instance.ListSite(prjpath, "", "xml", ".{0}.xml", SourceSiteID);
            files.AddRange(xmlList1);
            files.AddRange(xmlList2);
            List<String> xmlList3 = ProjectBL.Instance.ListSite(prjpath, "", "css", ".{0}.css", SourceSiteID);
            files.AddRange(xmlList3);

            string txt = "";
            StringBuilder bld = new StringBuilder();
            perforcefiles.Clear();
            foreach (string s in files)
            {

                bld.Append(s.Replace(prjpath, "") + "\r\n");
                filelist.Add(s.Replace(prjpath, ""));
                string depotversion = s.Replace(prjpath, "");
                depotversion = depotversion.Replace("\\", "/");
                perforcefiles.Add(txtDepotBranch.Text + depotversion);
            }
            rtfFiles.Clear();
            rtfFiles.Text = bld.ToString();
            return filelist;
        }
        private void button4_Click(object sender, EventArgs e)
        {
            List<string> list= ListFiles();
            ProjectBL.Instance.RemoveFiles(txtProject.Text,list);

        }

        private void tabResx_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                int changelist = Int32.Parse(txtChangeList.Text);
                ProjectBL.Instance.OpenForDelete(changelist, perforcefiles);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error :" + ex.Message + "\r\n" + "Did you create Change List?\r\nReally?");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            List<string> filelist = new List<string>();
            if (String.IsNullOrEmpty(txtProject.Text))
            {
                MessageBox.Show("Please select project");
               
            }

            string prj = txtProject.Text;

            int ind = prj.LastIndexOf("\\");
            string prjpath = prj.Substring(0, ind);
            files.Clear();

            string fileformat = String.Format("20.ascx.{0}.{1}.resx", SourceSiteID, SourceCulture);
            files = ProjectBL.Instance.ListSite(prjpath, "_Resources", "resx", fileformat ,SourceSiteID);

         
            string txt = "";
            StringBuilder bld = new StringBuilder();
            perforcefiles.Clear();
            foreach (string s in files)
            {

                bld.Append(s.Replace(prjpath, "") + "\r\n");
                filelist.Add(s.Replace(prjpath, ""));
                string depotversion = s.Replace(prjpath, "");
                depotversion = depotversion.Replace("\\", "/");
                perforcefiles.Add(txtDepotBranch.Text + depotversion);
            }
            rtfFiles.Clear();
            rtfFiles.Text = bld.ToString();
            
        }

        private void button7_Click(object sender, EventArgs e)
        {
            List<string> filelist = new List<string>();
            if (String.IsNullOrEmpty(txtProject.Text))
            {
                MessageBox.Show("Please select project");

            }

            string prj = txtProject.Text;

            int ind = prj.LastIndexOf("\\");
            string prjpath = prj.Substring(0, ind);
            files.Clear();

            string fileformat = String.Format("20.ascx.{0}.{1}.resx", SourceSiteID, SourceCulture);
            files = ProjectBL.Instance.ListSite(prjpath, "_Resources", "resx", fileformat, SourceSiteID);


            string txt = "";
            StringBuilder bld = new StringBuilder();
            perforcefiles.Clear();
            foreach (string s in files)
            {
                string no20file = s.Replace("20", "");
                bld.Append(no20file.Replace(prjpath, "") + "\r\n");
                filelist.Add(no20file.Replace(prjpath, ""));
                string depotversion = s.Replace(prjpath, "");
                depotversion = depotversion.Replace("\\", "/");
                depotversion = depotversion.Replace("20", "");
                perforcefiles.Add(txtDepotBranch.Text + depotversion);
            }

            int changelist = ProjectBL.Instance.CreateChangeList("Removing non 20 resx files for site:" + SourceSiteID);
            if (changelist > 0)
            {
                ProjectBL.Instance.OpenForDelete(changelist, perforcefiles);
                ProjectBL.Instance.RemoveFiles(prj, filelist);
            }
            rtfFiles.Clear();
            rtfFiles.Text = bld.ToString();

        }

        

    }
}
