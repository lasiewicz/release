﻿namespace SparkSiteManagement
{
    partial class ProjectConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabProject = new System.Windows.Forms.TabControl();
            this.tabResx = new System.Windows.Forms.TabPage();
            this.btnShowDelta = new System.Windows.Forms.Button();
            this.btnCopyDelta = new System.Windows.Forms.Button();
            this.btnCopy20Resx = new System.Windows.Forms.Button();
            this.btnGetPrjXML = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.rtfFiles = new System.Windows.Forms.RichTextBox();
            this.btnCopyResx = new System.Windows.Forms.Button();
            this.tabPrjXML = new System.Windows.Forms.TabPage();
            this.rtfPrjXML = new System.Windows.Forms.RichTextBox();
            this.tabGAM = new System.Windows.Forms.TabPage();
            this.btnAddToGamXML = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.rtfGAMAddXML = new System.Windows.Forms.RichTextBox();
            this.webGAMXML = new System.Windows.Forms.WebBrowser();
            this.txtGAMFile = new System.Windows.Forms.TextBox();
            this.tabTranslation = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.btnCopyToPrj = new System.Windows.Forms.Button();
            this.btnCopy1 = new System.Windows.Forms.Button();
            this.txtNewProj = new System.Windows.Forms.TextBox();
            this.btnBrowseNewProj = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnGetArticles = new System.Windows.Forms.Button();
            this.dgvArticles = new System.Windows.Forms.DataGridView();
            this.label11 = new System.Windows.Forms.Label();
            this.txtCategoryFile = new System.Windows.Forms.TextBox();
            this.btnGetCategory = new System.Windows.Forms.Button();
            this.btnSaveCategories = new System.Windows.Forms.Button();
            this.btnGetFromFile = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.dgvCategories = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dlgProject = new System.Windows.Forms.OpenFileDialog();
            this.btnOpenProject = new System.Windows.Forms.Button();
            this.txtProject = new System.Windows.Forms.TextBox();
            this.cboSites = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNewSiteID = new System.Windows.Forms.TextBox();
            this.cboCulture = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNewCulture = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cboEnv = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cboNewCulture = new System.Windows.Forms.ComboBox();
            this.dlgFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.label9 = new System.Windows.Forms.Label();
            this.dlgSaveFile = new System.Windows.Forms.SaveFileDialog();
            this.cboXML = new System.Windows.Forms.CheckBox();
            this.cboResx = new System.Windows.Forms.CheckBox();
            this.cboCSS = new System.Windows.Forms.CheckBox();
            this.cboOverwrite = new System.Windows.Forms.CheckBox();
            this.btnCheckout = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.txtChangeList = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnCreateCL = new System.Windows.Forms.Button();
            this.txtChangeListDescription = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDepotBranch = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.tabProject.SuspendLayout();
            this.tabResx.SuspendLayout();
            this.tabPrjXML.SuspendLayout();
            this.tabGAM.SuspendLayout();
            this.tabTranslation.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategories)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabProject
            // 
            this.tabProject.Controls.Add(this.tabResx);
            this.tabProject.Controls.Add(this.tabPrjXML);
            this.tabProject.Controls.Add(this.tabGAM);
            this.tabProject.Controls.Add(this.tabTranslation);
            this.tabProject.Controls.Add(this.tabPage1);
            this.tabProject.Controls.Add(this.tabPage2);
            this.tabProject.Location = new System.Drawing.Point(21, 155);
            this.tabProject.Name = "tabProject";
            this.tabProject.SelectedIndex = 0;
            this.tabProject.Size = new System.Drawing.Size(1142, 615);
            this.tabProject.TabIndex = 0;
            // 
            // tabResx
            // 
            this.tabResx.Controls.Add(this.button7);
            this.tabResx.Controls.Add(this.button6);
            this.tabResx.Controls.Add(this.button5);
            this.tabResx.Controls.Add(this.button4);
            this.tabResx.Controls.Add(this.button3);
            this.tabResx.Controls.Add(this.label13);
            this.tabResx.Controls.Add(this.txtChangeListDescription);
            this.tabResx.Controls.Add(this.btnCreateCL);
            this.tabResx.Controls.Add(this.label12);
            this.tabResx.Controls.Add(this.txtChangeList);
            this.tabResx.Controls.Add(this.btnSubmit);
            this.tabResx.Controls.Add(this.btnCheckout);
            this.tabResx.Controls.Add(this.cboOverwrite);
            this.tabResx.Controls.Add(this.cboCSS);
            this.tabResx.Controls.Add(this.cboResx);
            this.tabResx.Controls.Add(this.cboXML);
            this.tabResx.Controls.Add(this.btnShowDelta);
            this.tabResx.Controls.Add(this.btnCopyDelta);
            this.tabResx.Controls.Add(this.btnCopy20Resx);
            this.tabResx.Controls.Add(this.btnGetPrjXML);
            this.tabResx.Controls.Add(this.btnDelete);
            this.tabResx.Controls.Add(this.rtfFiles);
            this.tabResx.Controls.Add(this.btnCopyResx);
            this.tabResx.Location = new System.Drawing.Point(4, 22);
            this.tabResx.Name = "tabResx";
            this.tabResx.Padding = new System.Windows.Forms.Padding(3);
            this.tabResx.Size = new System.Drawing.Size(1134, 589);
            this.tabResx.TabIndex = 0;
            this.tabResx.Text = "Site Files(resx/xml/css)";
            this.tabResx.UseVisualStyleBackColor = true;
            this.tabResx.Click += new System.EventHandler(this.tabResx_Click);
            // 
            // btnShowDelta
            // 
            this.btnShowDelta.Location = new System.Drawing.Point(781, 111);
            this.btnShowDelta.Name = "btnShowDelta";
            this.btnShowDelta.Size = new System.Drawing.Size(159, 27);
            this.btnShowDelta.TabIndex = 35;
            this.btnShowDelta.Text = "Show Delta Site Files";
            this.btnShowDelta.UseVisualStyleBackColor = true;
            this.btnShowDelta.Click += new System.EventHandler(this.btnShowDelta_Click);
            // 
            // btnCopyDelta
            // 
            this.btnCopyDelta.Location = new System.Drawing.Point(616, 111);
            this.btnCopyDelta.Name = "btnCopyDelta";
            this.btnCopyDelta.Size = new System.Drawing.Size(159, 27);
            this.btnCopyDelta.TabIndex = 34;
            this.btnCopyDelta.Text = "Copy Delta Site Files";
            this.btnCopyDelta.UseVisualStyleBackColor = true;
            this.btnCopyDelta.Click += new System.EventHandler(this.btnCopyDelta_Click);
            // 
            // btnCopy20Resx
            // 
            this.btnCopy20Resx.Location = new System.Drawing.Point(451, 111);
            this.btnCopy20Resx.Name = "btnCopy20Resx";
            this.btnCopy20Resx.Size = new System.Drawing.Size(159, 27);
            this.btnCopy20Resx.TabIndex = 33;
            this.btnCopy20Resx.Text = "Copy 20 Resx files";
            this.btnCopy20Resx.UseVisualStyleBackColor = true;
            this.btnCopy20Resx.Click += new System.EventHandler(this.btnCopy20Resx_Click);
            // 
            // btnGetPrjXML
            // 
            this.btnGetPrjXML.Location = new System.Drawing.Point(343, 111);
            this.btnGetPrjXML.Name = "btnGetPrjXML";
            this.btnGetPrjXML.Size = new System.Drawing.Size(102, 27);
            this.btnGetPrjXML.TabIndex = 1;
            this.btnGetPrjXML.Text = "Get Project XML";
            this.btnGetPrjXML.UseVisualStyleBackColor = true;
            this.btnGetPrjXML.Click += new System.EventHandler(this.btnGetPrjXML_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(178, 111);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(159, 27);
            this.btnDelete.TabIndex = 32;
            this.btnDelete.Text = "Delete New Files";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // rtfFiles
            // 
            this.rtfFiles.Location = new System.Drawing.Point(19, 144);
            this.rtfFiles.Name = "rtfFiles";
            this.rtfFiles.Size = new System.Drawing.Size(921, 430);
            this.rtfFiles.TabIndex = 0;
            this.rtfFiles.Text = "";
            this.rtfFiles.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // btnCopyResx
            // 
            this.btnCopyResx.Location = new System.Drawing.Point(13, 111);
            this.btnCopyResx.Name = "btnCopyResx";
            this.btnCopyResx.Size = new System.Drawing.Size(159, 27);
            this.btnCopyResx.TabIndex = 31;
            this.btnCopyResx.Text = "Copy Site Files";
            this.btnCopyResx.UseVisualStyleBackColor = true;
            this.btnCopyResx.Click += new System.EventHandler(this.button2_Click);
            // 
            // tabPrjXML
            // 
            this.tabPrjXML.Controls.Add(this.rtfPrjXML);
            this.tabPrjXML.Location = new System.Drawing.Point(4, 22);
            this.tabPrjXML.Name = "tabPrjXML";
            this.tabPrjXML.Padding = new System.Windows.Forms.Padding(3);
            this.tabPrjXML.Size = new System.Drawing.Size(952, 503);
            this.tabPrjXML.TabIndex = 1;
            this.tabPrjXML.Text = "Project XML";
            this.tabPrjXML.UseVisualStyleBackColor = true;
            // 
            // rtfPrjXML
            // 
            this.rtfPrjXML.Location = new System.Drawing.Point(16, 29);
            this.rtfPrjXML.Name = "rtfPrjXML";
            this.rtfPrjXML.Size = new System.Drawing.Size(921, 445);
            this.rtfPrjXML.TabIndex = 1;
            this.rtfPrjXML.Text = "";
            // 
            // tabGAM
            // 
            this.tabGAM.Controls.Add(this.btnAddToGamXML);
            this.tabGAM.Controls.Add(this.label7);
            this.tabGAM.Controls.Add(this.rtfGAMAddXML);
            this.tabGAM.Controls.Add(this.webGAMXML);
            this.tabGAM.Controls.Add(this.txtGAMFile);
            this.tabGAM.Location = new System.Drawing.Point(4, 22);
            this.tabGAM.Name = "tabGAM";
            this.tabGAM.Size = new System.Drawing.Size(952, 503);
            this.tabGAM.TabIndex = 2;
            this.tabGAM.Text = "GAM";
            this.tabGAM.UseVisualStyleBackColor = true;
            // 
            // btnAddToGamXML
            // 
            this.btnAddToGamXML.Location = new System.Drawing.Point(548, 41);
            this.btnAddToGamXML.Name = "btnAddToGamXML";
            this.btnAddToGamXML.Size = new System.Drawing.Size(124, 28);
            this.btnAddToGamXML.TabIndex = 4;
            this.btnAddToGamXML.Text = "Add to GAM.xml";
            this.btnAddToGamXML.UseVisualStyleBackColor = true;
            this.btnAddToGamXML.Click += new System.EventHandler(this.btnAddToGamXML_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(493, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(442, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Following XML should be added to GAM.xml, replace ((SiteName.com)) with New Site " +
                "Name.";
            // 
            // rtfGAMAddXML
            // 
            this.rtfGAMAddXML.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtfGAMAddXML.Location = new System.Drawing.Point(548, 75);
            this.rtfGAMAddXML.Name = "rtfGAMAddXML";
            this.rtfGAMAddXML.Size = new System.Drawing.Size(351, 401);
            this.rtfGAMAddXML.TabIndex = 2;
            this.rtfGAMAddXML.Text = "";
            // 
            // webGAMXML
            // 
            this.webGAMXML.Location = new System.Drawing.Point(28, 75);
            this.webGAMXML.MinimumSize = new System.Drawing.Size(20, 20);
            this.webGAMXML.Name = "webGAMXML";
            this.webGAMXML.Size = new System.Drawing.Size(485, 412);
            this.webGAMXML.TabIndex = 1;
            // 
            // txtGAMFile
            // 
            this.txtGAMFile.Location = new System.Drawing.Point(28, 22);
            this.txtGAMFile.Name = "txtGAMFile";
            this.txtGAMFile.Size = new System.Drawing.Size(384, 20);
            this.txtGAMFile.TabIndex = 0;
            // 
            // tabTranslation
            // 
            this.tabTranslation.Controls.Add(this.label10);
            this.tabTranslation.Controls.Add(this.btnCopyToPrj);
            this.tabTranslation.Controls.Add(this.btnCopy1);
            this.tabTranslation.Controls.Add(this.txtNewProj);
            this.tabTranslation.Controls.Add(this.btnBrowseNewProj);
            this.tabTranslation.Controls.Add(this.label8);
            this.tabTranslation.Location = new System.Drawing.Point(4, 22);
            this.tabTranslation.Name = "tabTranslation";
            this.tabTranslation.Size = new System.Drawing.Size(952, 503);
            this.tabTranslation.TabIndex = 3;
            this.tabTranslation.Text = "Translation Kit(Resx)";
            this.tabTranslation.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(46, 49);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 13);
            this.label10.TabIndex = 34;
            this.label10.Text = "Path";
            // 
            // btnCopyToPrj
            // 
            this.btnCopyToPrj.Location = new System.Drawing.Point(262, 82);
            this.btnCopyToPrj.Name = "btnCopyToPrj";
            this.btnCopyToPrj.Size = new System.Drawing.Size(200, 48);
            this.btnCopyToPrj.TabIndex = 33;
            this.btnCopyToPrj.Text = "CopyTranslated Files from Path above to Project folders";
            this.btnCopyToPrj.UseVisualStyleBackColor = true;
            this.btnCopyToPrj.Click += new System.EventHandler(this.btnCopyToPrj_Click);
            // 
            // btnCopy1
            // 
            this.btnCopy1.Location = new System.Drawing.Point(43, 82);
            this.btnCopy1.Name = "btnCopy1";
            this.btnCopy1.Size = new System.Drawing.Size(200, 48);
            this.btnCopy1.TabIndex = 32;
            this.btnCopy1.Text = "Copy Site Resx Files for Translation from Proj. Folder to Path above";
            this.btnCopy1.UseVisualStyleBackColor = true;
            this.btnCopy1.Click += new System.EventHandler(this.btnCopy1_Click);
            // 
            // txtNewProj
            // 
            this.txtNewProj.Location = new System.Drawing.Point(113, 46);
            this.txtNewProj.Name = "txtNewProj";
            this.txtNewProj.Size = new System.Drawing.Size(583, 20);
            this.txtNewProj.TabIndex = 4;
            // 
            // btnBrowseNewProj
            // 
            this.btnBrowseNewProj.Location = new System.Drawing.Point(731, 41);
            this.btnBrowseNewProj.Name = "btnBrowseNewProj";
            this.btnBrowseNewProj.Size = new System.Drawing.Size(91, 29);
            this.btnBrowseNewProj.TabIndex = 3;
            this.btnBrowseNewProj.Text = "Browse";
            this.btnBrowseNewProj.UseVisualStyleBackColor = true;
            this.btnBrowseNewProj.Click += new System.EventHandler(this.btnBrowseNewProj_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(40, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(547, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "When resx files for a new site created (Tab 1) you can copy them preserving direc" +
                "tory structure to another directory";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnSave);
            this.tabPage1.Controls.Add(this.btnGetArticles);
            this.tabPage1.Controls.Add(this.dgvArticles);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.txtCategoryFile);
            this.tabPage1.Controls.Add(this.btnGetCategory);
            this.tabPage1.Controls.Add(this.btnSaveCategories);
            this.tabPage1.Controls.Add(this.btnGetFromFile);
            this.tabPage1.Controls.Add(this.btnClear);
            this.tabPage1.Controls.Add(this.dgvCategories);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(952, 503);
            this.tabPage1.TabIndex = 4;
            this.tabPage1.Text = "Translation Kit(Articles)";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(661, 59);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(131, 32);
            this.btnSave.TabIndex = 25;
            this.btnSave.Text = "Save Articles";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnGetArticles
            // 
            this.btnGetArticles.Location = new System.Drawing.Point(480, 59);
            this.btnGetArticles.Name = "btnGetArticles";
            this.btnGetArticles.Size = new System.Drawing.Size(131, 32);
            this.btnGetArticles.TabIndex = 24;
            this.btnGetArticles.Text = "Get Articles";
            this.btnGetArticles.UseVisualStyleBackColor = true;
            this.btnGetArticles.Click += new System.EventHandler(this.btnGetArticles_Click);
            // 
            // dgvArticles
            // 
            this.dgvArticles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvArticles.Location = new System.Drawing.Point(480, 97);
            this.dgvArticles.Name = "dgvArticles";
            this.dgvArticles.Size = new System.Drawing.Size(439, 352);
            this.dgvArticles.TabIndex = 23;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Xml file";
            // 
            // txtCategoryFile
            // 
            this.txtCategoryFile.Location = new System.Drawing.Point(95, 16);
            this.txtCategoryFile.Name = "txtCategoryFile";
            this.txtCategoryFile.Size = new System.Drawing.Size(409, 20);
            this.txtCategoryFile.TabIndex = 6;
            // 
            // btnGetCategory
            // 
            this.btnGetCategory.Location = new System.Drawing.Point(519, 16);
            this.btnGetCategory.Name = "btnGetCategory";
            this.btnGetCategory.Size = new System.Drawing.Size(91, 29);
            this.btnGetCategory.TabIndex = 5;
            this.btnGetCategory.Text = "Browse";
            this.btnGetCategory.UseVisualStyleBackColor = true;
            this.btnGetCategory.Click += new System.EventHandler(this.btnGetCategory_Click);
            // 
            // btnSaveCategories
            // 
            this.btnSaveCategories.Location = new System.Drawing.Point(170, 59);
            this.btnSaveCategories.Name = "btnSaveCategories";
            this.btnSaveCategories.Size = new System.Drawing.Size(131, 32);
            this.btnSaveCategories.TabIndex = 4;
            this.btnSaveCategories.Text = "Save Categories to File";
            this.btnSaveCategories.UseVisualStyleBackColor = true;
            this.btnSaveCategories.Click += new System.EventHandler(this.btnSaveCategories_Click);
            // 
            // btnGetFromFile
            // 
            this.btnGetFromFile.Location = new System.Drawing.Point(21, 59);
            this.btnGetFromFile.Name = "btnGetFromFile";
            this.btnGetFromFile.Size = new System.Drawing.Size(131, 32);
            this.btnGetFromFile.TabIndex = 3;
            this.btnGetFromFile.Text = "Get Categories from File";
            this.btnGetFromFile.UseVisualStyleBackColor = true;
            this.btnGetFromFile.Click += new System.EventHandler(this.btnGetFromFile_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(329, 59);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(131, 32);
            this.btnClear.TabIndex = 2;
            this.btnClear.Text = "Clear Categories";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // dgvCategories
            // 
            this.dgvCategories.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCategories.Location = new System.Drawing.Point(21, 97);
            this.dgvCategories.Name = "dgvCategories";
            this.dgvCategories.Size = new System.Drawing.Size(439, 352);
            this.dgvCategories.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(952, 589);
            this.tabPage2.TabIndex = 5;
            this.tabPage2.Text = "p4";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(513, 35);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(155, 40);
            this.button2.TabIndex = 1;
            this.button2.Text = "Submit";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(309, 35);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(155, 40);
            this.button1.TabIndex = 0;
            this.button1.Text = "Checkout";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // dlgProject
            // 
            this.dlgProject.FileName = "openFileDialog1";
            this.dlgProject.InitialDirectory = "C:\\\\matchnet\\";
            // 
            // btnOpenProject
            // 
            this.btnOpenProject.Location = new System.Drawing.Point(695, 27);
            this.btnOpenProject.Name = "btnOpenProject";
            this.btnOpenProject.Size = new System.Drawing.Size(91, 29);
            this.btnOpenProject.TabIndex = 1;
            this.btnOpenProject.Text = "Browse";
            this.btnOpenProject.UseVisualStyleBackColor = true;
            this.btnOpenProject.Click += new System.EventHandler(this.btnOpenProject_Click);
            // 
            // txtProject
            // 
            this.txtProject.Location = new System.Drawing.Point(138, 36);
            this.txtProject.Name = "txtProject";
            this.txtProject.Size = new System.Drawing.Size(551, 20);
            this.txtProject.TabIndex = 2;
            // 
            // cboSites
            // 
            this.cboSites.FormattingEnabled = true;
            this.cboSites.Location = new System.Drawing.Point(141, 94);
            this.cboSites.Name = "cboSites";
            this.cboSites.Size = new System.Drawing.Size(140, 21);
            this.cboSites.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Source Site";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(319, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Target New Site ID";
            // 
            // txtNewSiteID
            // 
            this.txtNewSiteID.Location = new System.Drawing.Point(446, 94);
            this.txtNewSiteID.Name = "txtNewSiteID";
            this.txtNewSiteID.Size = new System.Drawing.Size(86, 20);
            this.txtNewSiteID.TabIndex = 22;
            // 
            // cboCulture
            // 
            this.cboCulture.FormattingEnabled = true;
            this.cboCulture.Items.AddRange(new object[] {
            "en-US",
            "en-CA",
            "en-GB",
            "he-IL",
            "fr-FR"});
            this.cboCulture.Location = new System.Drawing.Point(141, 121);
            this.cboCulture.Name = "cboCulture";
            this.cboCulture.Size = new System.Drawing.Size(140, 21);
            this.cboCulture.TabIndex = 23;
            this.cboCulture.SelectedIndexChanged += new System.EventHandler(this.cboCulture_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Source Culture";
            // 
            // txtNewCulture
            // 
            this.txtNewCulture.Location = new System.Drawing.Point(733, 117);
            this.txtNewCulture.Name = "txtNewCulture";
            this.txtNewCulture.Size = new System.Drawing.Size(128, 20);
            this.txtNewCulture.TabIndex = 25;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(603, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "New Culture (if not listed)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "Environment";
            // 
            // cboEnv
            // 
            this.cboEnv.FormattingEnabled = true;
            this.cboEnv.Location = new System.Drawing.Point(138, 9);
            this.cboEnv.Name = "cboEnv";
            this.cboEnv.Size = new System.Drawing.Size(276, 21);
            this.cboEnv.TabIndex = 27;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(319, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "New Culture";
            // 
            // cboNewCulture
            // 
            this.cboNewCulture.FormattingEnabled = true;
            this.cboNewCulture.Items.AddRange(new object[] {
            "en-US",
            "en-CA",
            "en-GB",
            "he-IL",
            "fr-FR"});
            this.cboNewCulture.Location = new System.Drawing.Point(446, 121);
            this.cboNewCulture.Name = "cboNewCulture";
            this.cboNewCulture.Size = new System.Drawing.Size(140, 21);
            this.cboNewCulture.TabIndex = 29;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(23, 43);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Project File";
            // 
            // cboXML
            // 
            this.cboXML.AutoSize = true;
            this.cboXML.Location = new System.Drawing.Point(11, 88);
            this.cboXML.Name = "cboXML";
            this.cboXML.Size = new System.Drawing.Size(48, 17);
            this.cboXML.TabIndex = 36;
            this.cboXML.Text = "XML";
            this.cboXML.UseVisualStyleBackColor = true;
            // 
            // cboResx
            // 
            this.cboResx.AutoSize = true;
            this.cboResx.Location = new System.Drawing.Point(81, 88);
            this.cboResx.Name = "cboResx";
            this.cboResx.Size = new System.Drawing.Size(77, 17);
            this.cboResx.TabIndex = 37;
            this.cboResx.Text = "Resources";
            this.cboResx.UseVisualStyleBackColor = true;
            // 
            // cboCSS
            // 
            this.cboCSS.AutoSize = true;
            this.cboCSS.Location = new System.Drawing.Point(165, 88);
            this.cboCSS.Name = "cboCSS";
            this.cboCSS.Size = new System.Drawing.Size(47, 17);
            this.cboCSS.TabIndex = 38;
            this.cboCSS.Text = "CSS";
            this.cboCSS.UseVisualStyleBackColor = true;
            // 
            // cboOverwrite
            // 
            this.cboOverwrite.AutoSize = true;
            this.cboOverwrite.Location = new System.Drawing.Point(245, 88);
            this.cboOverwrite.Name = "cboOverwrite";
            this.cboOverwrite.Size = new System.Drawing.Size(92, 17);
            this.cboOverwrite.TabIndex = 39;
            this.cboOverwrite.Text = "Overwrite files";
            this.cboOverwrite.UseVisualStyleBackColor = true;
            // 
            // btnCheckout
            // 
            this.btnCheckout.Location = new System.Drawing.Point(693, 10);
            this.btnCheckout.Name = "btnCheckout";
            this.btnCheckout.Size = new System.Drawing.Size(117, 27);
            this.btnCheckout.TabIndex = 40;
            this.btnCheckout.Text = "Checkout Files";
            this.btnCheckout.UseVisualStyleBackColor = true;
            this.btnCheckout.Click += new System.EventHandler(this.btnCheckout_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(921, 10);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(95, 27);
            this.btnSubmit.TabIndex = 41;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            // 
            // txtChangeList
            // 
            this.txtChangeList.Location = new System.Drawing.Point(633, 17);
            this.txtChangeList.Name = "txtChangeList";
            this.txtChangeList.Size = new System.Drawing.Size(54, 20);
            this.txtChangeList.TabIndex = 42;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(557, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 13);
            this.label12.TabIndex = 43;
            this.label12.Text = "Change List#";
            // 
            // btnCreateCL
            // 
            this.btnCreateCL.Location = new System.Drawing.Point(425, 17);
            this.btnCreateCL.Name = "btnCreateCL";
            this.btnCreateCL.Size = new System.Drawing.Size(117, 27);
            this.btnCreateCL.TabIndex = 44;
            this.btnCreateCL.Text = "Create ChangeList";
            this.btnCreateCL.UseVisualStyleBackColor = true;
            this.btnCreateCL.Click += new System.EventHandler(this.btnCreateCL_Click);
            // 
            // txtChangeListDescription
            // 
            this.txtChangeListDescription.Location = new System.Drawing.Point(11, 24);
            this.txtChangeListDescription.Name = "txtChangeListDescription";
            this.txtChangeListDescription.Size = new System.Drawing.Size(408, 20);
            this.txtChangeListDescription.TabIndex = 45;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 3);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(119, 13);
            this.label13.TabIndex = 46;
            this.label13.Text = "Change List Description";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(26, 69);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(98, 13);
            this.label14.TabIndex = 35;
            this.label14.Text = "Depot Branch Path";
            // 
            // txtDepotBranch
            // 
            this.txtDepotBranch.Location = new System.Drawing.Point(141, 62);
            this.txtDepotBranch.Name = "txtDepotBranch";
            this.txtDepotBranch.Size = new System.Drawing.Size(445, 20);
            this.txtDepotBranch.TabIndex = 34;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(383, 69);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(159, 27);
            this.button3.TabIndex = 47;
            this.button3.Text = "List Site Files";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(946, 69);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(159, 27);
            this.button4.TabIndex = 48;
            this.button4.Text = "Delete Site Files from Project";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(816, 10);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(99, 27);
            this.button5.TabIndex = 49;
            this.button5.Text = "Open for Delete";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(543, 69);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(159, 27);
            this.button6.TabIndex = 50;
            this.button6.Text = "List Site Resx20 Files";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(756, 69);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(159, 27);
            this.button7.TabIndex = 51;
            this.button7.Text = "Remove Site  Resx  Not 20 Files";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // ProjectConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1175, 798);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtDepotBranch);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cboNewCulture);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cboEnv);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtNewCulture);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cboCulture);
            this.Controls.Add(this.txtNewSiteID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboSites);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtProject);
            this.Controls.Add(this.btnOpenProject);
            this.Controls.Add(this.tabProject);
            this.Name = "ProjectConfig";
            this.Text = "ProjectConfig";
            this.Load += new System.EventHandler(this.ProjectConfig_Load);
            this.tabProject.ResumeLayout(false);
            this.tabResx.ResumeLayout(false);
            this.tabResx.PerformLayout();
            this.tabPrjXML.ResumeLayout(false);
            this.tabGAM.ResumeLayout(false);
            this.tabGAM.PerformLayout();
            this.tabTranslation.ResumeLayout(false);
            this.tabTranslation.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvArticles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategories)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabProject;
        private System.Windows.Forms.TabPage tabResx;
        private System.Windows.Forms.TabPage tabPrjXML;
        private System.Windows.Forms.OpenFileDialog dlgProject;
        private System.Windows.Forms.Button btnOpenProject;
        private System.Windows.Forms.TextBox txtProject;
        private System.Windows.Forms.ComboBox cboSites;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNewSiteID;
        private System.Windows.Forms.ComboBox cboCulture;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNewCulture;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboEnv;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboNewCulture;
        private System.Windows.Forms.Button btnCopyResx;
        private System.Windows.Forms.RichTextBox rtfFiles;
        private System.Windows.Forms.Button btnGetPrjXML;
        private System.Windows.Forms.RichTextBox rtfPrjXML;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.TabPage tabGAM;
        private System.Windows.Forms.TextBox txtGAMFile;
        private System.Windows.Forms.WebBrowser webGAMXML;
        private System.Windows.Forms.RichTextBox rtfGAMAddXML;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnAddToGamXML;
        private System.Windows.Forms.TabPage tabTranslation;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNewProj;
        private System.Windows.Forms.Button btnBrowseNewProj;
        private System.Windows.Forms.FolderBrowserDialog dlgFolder;
        private System.Windows.Forms.Button btnCopy1;
        private System.Windows.Forms.Button btnCopyToPrj;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dgvCategories;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSaveCategories;
        private System.Windows.Forms.Button btnGetFromFile;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtCategoryFile;
        private System.Windows.Forms.Button btnGetCategory;
        private System.Windows.Forms.SaveFileDialog dlgSaveFile;
        private System.Windows.Forms.Button btnGetArticles;
        private System.Windows.Forms.DataGridView dgvArticles;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnCopy20Resx;
        private System.Windows.Forms.Button btnShowDelta;
        private System.Windows.Forms.Button btnCopyDelta;
        private System.Windows.Forms.CheckBox cboResx;
        private System.Windows.Forms.CheckBox cboXML;
        private System.Windows.Forms.CheckBox cboCSS;
        private System.Windows.Forms.CheckBox cboOverwrite;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtChangeList;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnCheckout;
        private System.Windows.Forms.Button btnCreateCL;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtChangeListDescription;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtDepotBranch;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
    }
}