﻿namespace SparkSiteManagement
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSite = new System.Windows.Forms.Button();
            this.btnProject = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSite
            // 
            this.btnSite.Location = new System.Drawing.Point(55, 23);
            this.btnSite.Name = "btnSite";
            this.btnSite.Size = new System.Drawing.Size(190, 35);
            this.btnSite.TabIndex = 0;
            this.btnSite.Text = "Site";
            this.btnSite.UseVisualStyleBackColor = true;
            this.btnSite.Click += new System.EventHandler(this.btnSite_Click);
            // 
            // btnProject
            // 
            this.btnProject.Location = new System.Drawing.Point(55, 96);
            this.btnProject.Name = "btnProject";
            this.btnProject.Size = new System.Drawing.Size(190, 35);
            this.btnProject.TabIndex = 1;
            this.btnProject.Text = "Project";
            this.btnProject.UseVisualStyleBackColor = true;
            this.btnProject.Click += new System.EventHandler(this.btnProject_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(295, 226);
            this.Controls.Add(this.btnProject);
            this.Controls.Add(this.btnSite);
            this.Name = "Main";
            this.Text = "Main";
            this.Load += new System.EventHandler(this.Main_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSite;
        private System.Windows.Forms.Button btnProject;
    }
}

