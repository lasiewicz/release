﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SparkSiteManagement
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void btnSite_Click(object sender, EventArgs e)
        {
            SiteConfig frmSite = new SiteConfig();
            frmSite.Show();
        }

        private void btnProject_Click(object sender, EventArgs e)
        {
            ProjectConfig frmProject = new ProjectConfig();
            frmProject.Show();
        }

        private void Main_Load(object sender, EventArgs e)
        {

        }
    }
}
