﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace FreeTextSyncAssem
{
    /// <summary>
    /// Database Access Layer
    /// </summary>
    public class FreeTextSyncDAL
    {
        /// <summary>
        /// Execute a non queury against the database
        /// </summary>
        /// <param name="sqlCommandStr">command to execute</param>
        /// <param name="DBConnStr">connection string</param>
        /// <returns></returns>
        public static int ExecuteNonQueryCmd(string sqlCommandStr, string DBConnStr)
        {
            SqlCommand objSqlCommand = null;
            SqlConnection objSqlConnection = null;

            try
            {
                
                objSqlConnection = new SqlConnection(DBConnStr);

                if (objSqlConnection.State == ConnectionState.Closed)
                    objSqlConnection.Open();

                objSqlCommand = new SqlCommand(sqlCommandStr, objSqlConnection);
                objSqlCommand.CommandTimeout = 300;
                return objSqlCommand.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                string errMsg = "FreeTextSyncDAL.ExecuteNonQueryCmd() - " + ex.Message;
                throw new Exception(errMsg);
            }
            #region cleanup
            finally
            {
                if (null != objSqlCommand)
                {
                    objSqlCommand.Dispose();
                    objSqlCommand = null;
                }

                if (null != objSqlConnection)
                {
                    // utilize connection pooling, always close connectin on exit
                    if (objSqlConnection.State != ConnectionState.Closed)
                    {
                        try
                        {
                            objSqlConnection.Close();
                        }
                        catch //cant do anything at this point
                        {}
                    }
                    objSqlConnection.Dispose();
                    objSqlConnection = null;
                }
            }
            #endregion

        }

        /// <summary>
        /// Execute a query to retrieve data
        /// </summary>
        /// <param name="sqlCommandStr">query string</param>
        /// <param name="DBConnStr">connection string</param>
        /// <returns></returns>
        public static DataSet ExecuteQueryCmd(string sqlCommandStr, string DBConnStr)
        {
            DataSet dsDataReport = null;
            SqlConnection objSqlConnection = null;
            SqlDataAdapter objSqlDataAdapter  = null;
            
            try
            {
                objSqlConnection = new SqlConnection(DBConnStr);
                
                dsDataReport = new DataSet();

                objSqlDataAdapter = new SqlDataAdapter(sqlCommandStr, objSqlConnection);

                objSqlDataAdapter.Fill(dsDataReport);

                return dsDataReport;
            }
            catch (Exception ex)
            {
                string errMsg = "FreeTextSyncDAL.ExecuteQueryCmd() - " + ex.Message;
                throw new Exception(errMsg);
            }
            #region cleanup
            finally
            {
                if(null != dsDataReport)
                {
                    dsDataReport.Dispose();
                    dsDataReport = null;
                }

                if(null != objSqlConnection)
                {
                    objSqlConnection.Dispose();
                    objSqlConnection = null;
                }

                if(null != objSqlDataAdapter)
                {
                    objSqlDataAdapter.Dispose();
                    objSqlDataAdapter = null;
                }
            }
            #endregion
        }
    }
}
