﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FreeTextSyncAssem
{
    /// <summary>
    /// class that contains the value object for configuration
    /// </summary>
    public class FreeTextSyncConfig
    {
        private string _UnApprovedMemberDBConnStr;
        private string _UnApprovedMemberCmdStr;
        private string _RequeueOnlyLanguageID;
        private string _RequeueLogDBConnStr;
        private string _RequeueLogCmdStr;
        private int _RequeueCountLimit;
        private string _DedupeLogCmdStr;

        public string UnApprovedMemberDBConnStr
        {
            get
            {
                return _UnApprovedMemberDBConnStr;
            }
            set
            {
                _UnApprovedMemberDBConnStr = value;
            }
        }

        public string UnApprovedMemberCmdStr
        {
            get
            {
                return _UnApprovedMemberCmdStr;
            }
            set
            {
                _UnApprovedMemberCmdStr = value;
            }
        }

        public string RequeueOnlyLanguageID
        {
            get
            {
                return _RequeueOnlyLanguageID;
            }
            set
            {
                _RequeueOnlyLanguageID = value;
            }
        }

        public string RequeueLogDBConnStr
        {
            get
            {
                return _RequeueLogDBConnStr;
            }
            set
            {
                _RequeueLogDBConnStr = value;
            }
        }
        
        public string RequeueLogCmdStr
        {
            get
            {
                return _RequeueLogCmdStr;
            }
            set
            {
                _RequeueLogCmdStr = value;
            }
        }

        public int RequeueCountLimit
        {
            get
            {
                return _RequeueCountLimit;
            }
            set
            {
                _RequeueCountLimit = value;
            }
        }

        public string DedupeLogCmdStr
        {
            get
            {
                return _DedupeLogCmdStr;
            }
            set
            {
                _DedupeLogCmdStr = value;
            }
        }

    }
}
