﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Diagnostics;

namespace FreeTextSyncAssem
{
    public class EventLogger
    {
        public static void LogEvent(string source, string logStr, EventLogEntryType type)
        {
            // Create the source, if it does not already exist.
            if (!EventLog.SourceExists(source))
            {
                EventLog.CreateEventSource(source, "FreeTextRequeue");
            }

            // Create an EventLog instance and assign its source.
            EventLog myLog = new EventLog();
            myLog.Source = source;

            // Write an informational entry to the event log.    
            myLog.WriteEntry(logStr, type);
        }
    }
}
