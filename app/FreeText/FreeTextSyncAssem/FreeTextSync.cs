﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Threading;

using Matchnet;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Member.ValueObjects;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects;


namespace FreeTextSyncAssem
{
    public class FreeTextSync
    {
        FreeTextSyncConfig _FreeTextSyncConfig = new FreeTextSyncConfig();

        /// <summary>
        /// Load configuration files
        /// </summary>
        /// <param name="objFreeTextSyncConfig"></param>
        public FreeTextSync(FreeTextSyncConfig objFreeTextSyncConfig)
        {
            _FreeTextSyncConfig = objFreeTextSyncConfig;
        }

        /// <summary>
        /// entry point into Re-queue in dedupe process
        /// </summary>
        /// <returns></returns>
        public bool Process()
        {
            bool bCompletedProcess = true;
            DataSet dsUnApprovedMembers = null;

            int englishCount;
            int hebrewCount;
            try
            {
                //get the text count at this point so don't have to dedupe for new queue
                GetTextCount(out englishCount,out hebrewCount);

                #region retrieve the list of member who as need to be requeued and queue them
                dsUnApprovedMembers = FreeTextSyncDAL.ExecuteQueryCmd(_FreeTextSyncConfig.UnApprovedMemberCmdStr, _FreeTextSyncConfig.UnApprovedMemberDBConnStr);

                int count = 0;
                foreach (DataRow dr in dsUnApprovedMembers.Tables[0].Rows)
                {
                    // limit number of requeue base on configuration
                    if (count >= this._FreeTextSyncConfig.RequeueCountLimit)
                        break;

                    if (this.RequeueMember(dr["MemberGroupLangStr"].ToString(), dr["UpdateDate"].ToString()))
                    {
                        count++;
                    }
                }
                #endregion

                // dedupe the number base on the number in the queue before the requeued
                ProcessDedupe(englishCount, hebrewCount);
            }
            catch(Exception ex)
            {
                EventLogger.LogEvent("FreeTextSync",this.ToString() + ".Process() - " + ex.Message,System.Diagnostics.EventLogEntryType.Error);
                throw new Exception(ex.Message);
                //bCompletedProcess = false;
            }
            #region cleanup
            finally
            {
                if(null != dsUnApprovedMembers)
                {
                    dsUnApprovedMembers.Dispose();
                    dsUnApprovedMembers = null;
                }
            }
            #endregion
            return bCompletedProcess;
        }

        /// <summary>
        /// Insert the entry back into the queue
        /// </summary>
        /// <param name="MemGroupLangStr"></param>
        /// <param name="UpdateDateStr"></param>
        private bool RequeueMember(string MemGroupLangStr, string UpdateDateStr)
        {
            int MemberID;
            int GroupID;
            int LanguageID;

            bool bIsMemberApproved = true;

            #region split the MemGroupLamg into each parts
            try
            {
                string[] splitArr = MemGroupLangStr.Split(',');
                MemberID = int.Parse(splitArr[0]);
                GroupID = int.Parse(splitArr[1]);
                LanguageID = int.Parse(splitArr[2]);
            }
            catch(Exception ex)
            {
                throw new Exception(this.ToString() + ".RequeueMember() - error parsing MemGroupLangStr '" + MemGroupLangStr + "'. - " + ex.Message);
            }
            #endregion

            //To requeue hebrew or other
            if (
                (_FreeTextSyncConfig.RequeueOnlyLanguageID == LanguageID.ToString())
                || (_FreeTextSyncConfig.RequeueOnlyLanguageID == "ALL" )
                )
            {

                try
                {
                    // determine if the Member has any free text
                    bIsMemberApproved = IsMemberApproved(MemberID, GroupID, LanguageID);

                    //Queue the text
                    if (!bIsMemberApproved)
                    {
                        ApproveQueueSA.Instance.QueueText(GroupID, // CommunityID
                            MemberID, // MemberID
                            LanguageID); // LanguageID  
                    }

                    #region Format and Log the Queue
                    string insertRecordStr = this._FreeTextSyncConfig.RequeueLogCmdStr;

                    if (bIsMemberApproved)
                    {
                        insertRecordStr = insertRecordStr.Replace("[Requeued]", "0");
                    }
                    else
                    {
                        insertRecordStr = insertRecordStr.Replace("[Requeued]", "1");
                    }
                    insertRecordStr = insertRecordStr.Replace("[MemberID]", MemberID.ToString());
                    insertRecordStr = insertRecordStr.Replace("[GroupID]", GroupID.ToString());
                    insertRecordStr = insertRecordStr.Replace("[LanguageID]", LanguageID.ToString());
                    insertRecordStr = insertRecordStr.Replace("[UpdateDate]", "'" + UpdateDateStr + "'" );

                    FreeTextSyncDAL.ExecuteNonQueryCmd(insertRecordStr, _FreeTextSyncConfig.RequeueLogDBConnStr);
                    #endregion
                }
                catch (Exception exc)
                {
                    throw new Exception(this.ToString() + ".RequeueMember() - " + exc.Message);
                }
            }
            
            return !bIsMemberApproved;
        }

        public bool IsMemberApproved(int memberID, int communityID, int languageID)
        {
            bool existsNewText = false;

            try
            {    
                int brandID = Constants.NULL_INT;

                Member member = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(memberID, MemberLoadFlags.None);

                member.GetLastLogonDate(communityID, out brandID);

                if (brandID == Constants.NULL_INT || brandID == 0)
                {
                     EventLogger.LogEvent("FreeTextSync"
                        ,"Null BrandID\t" + memberID.ToString() + "\t" + communityID.ToString() + "\t" + languageID.ToString() + "\t"
                        ,System.Diagnostics.EventLogEntryType.Warning);
                    return true;
                }

                Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);


                if (brand == null)
                {
                    EventLogger.LogEvent("FreeTextSync"
                        ,"Null Brand\t" + memberID.ToString() + "\t" + communityID.ToString() + "\t" + languageID.ToString() + "\t"
                        ,System.Diagnostics.EventLogEntryType.Warning);

                    return true;
                }

                Matchnet.Content.ValueObjects.AttributeMetadata.AttributeCollection attributeCollection = AttributeMetadataSA.Instance.GetAttributeCollections().GetCollection
                    ("FREETEXT", communityID, Constants.NULL_INT, Constants.NULL_INT);

                foreach (AttributeCollectionAttribute aca in attributeCollection)
                {
                    Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = AttributeMetadataSA.Instance.GetAttributes().GetAttribute(aca.AttributeID);

                    if (attribute.DataType == DataType.Text)
                    {
                        TextStatusType textStatusType = TextStatusType.None;

                        member.GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, languageID,
                            attribute.Name, String.Empty, out textStatusType);

                        if (textStatusType == TextStatusType.None || textStatusType == TextStatusType.Pending)
                        {
                            existsNewText = true;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                throw new Exception(this.ToString() + ".IsMemberApproved() - " + ex.Message);
            }

            if (existsNewText)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Retrieve the number of items in the Queue
        /// </summary>
        /// <param name="english"></param>
        /// <param name="hebrew"></param>
        public void GetTextCount(out int english, out int hebrew)
        {
            english = 0;
            hebrew = 0;

            try
            {
                QueueCountCollection queueCountCollection = ApproveQueueSA.Instance.GetQueueCounts(QueueItemType.Text);

                foreach (QueueCount queueCount in queueCountCollection)
                {
                    if (queueCount.Language != null)
                    {
                        if (queueCount.Language.Description == "English")
                        {
                            english = queueCount.Count;
                        }
                        if (queueCount.Language.Description == "Hebrew")
                        {
                            hebrew = queueCount.Count;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(this.ToString() + ".GetTextCount() - " + ex.Message);
            }
        }

        /// <summary>
        /// Dedupe base on the number of counts in the Queue
        /// </summary>
        /// <param name="englishCount"></param>
        /// <param name="hebrewCount"></param>
        private void ProcessDedupe(int englishCount, int hebrewCount)
        {
            // Only want to do 75% at this point due to multiple queues and process is random on the queue
            // short-term solution
            int dedupeEnglishCount = (int)((double)englishCount * .75);
            int dedupeHebrewCount = (int)((double)hebrewCount * .75);

            #region dedupe english
            for (int i = 0; i <= dedupeEnglishCount; i++)
            {
                if (!Dedupe(2))
                {
                    break;
                }
                Thread.Sleep(100);
            }
            #endregion

            #region dedupe hebrew
            for (int j = 0; j <= dedupeHebrewCount; j++)
            {
                if (!Dedupe(262144))
                {
                    break;
                }
                Thread.Sleep(100);
            }
            #endregion
        }

        /// <summary>
        /// dedupe a single item base on the language id
        /// </summary>
        /// <param name="languageID"></param>
        /// <returns></returns>
        private bool Dedupe(int languageID)
        {
            bool queueRemaining = true;

            //get the database log command from configuration
            string DedupeLogCmdStr = this._FreeTextSyncConfig.DedupeLogCmdStr;

            try
            {
                // get 1 item from the queue
                QueueItemText queueItemText = getNextQueueItemText(languageID);

                if (null != queueItemText)
                {
                    //if the member has any free text to get approve
                    bool bIsMemberApproved = this.IsMemberApproved(queueItemText.MemberID, queueItemText.CommunityID, queueItemText.LanguageID);

                    // if the member is already approved, tehn need to make a call to complete the removal of the queue
                    if (bIsMemberApproved)
                    {
                        ApproveQueueSA.Instance.CompleteApproval(queueItemText.GetCacheKey());
                        DedupeLogCmdStr = DedupeLogCmdStr.Replace("[QueueType]", "'Dequeued'");
                    }
                    else
                    {
                        DedupeLogCmdStr = DedupeLogCmdStr.Replace("[QueueType]", "'Requeued'");
                    }

                    #region format the log string
                    DedupeLogCmdStr = DedupeLogCmdStr.Replace("[MemberID]", queueItemText.MemberID.ToString());
                    DedupeLogCmdStr = DedupeLogCmdStr.Replace("[CommunityID]", queueItemText.CommunityID.ToString());
                    DedupeLogCmdStr = DedupeLogCmdStr.Replace("[LanguageID]", queueItemText.LanguageID.ToString());
                    #endregion

                    FreeTextSyncDAL.ExecuteNonQueryCmd(DedupeLogCmdStr, this._FreeTextSyncConfig.RequeueLogDBConnStr);
                }
                else
                {
                    queueRemaining = false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(this.ToString() + ".Dedupe() - " + ex.Message);
            }

            return queueRemaining;
        }

        /// <summary>
        /// Get the next item in the queue base on the language id
        /// </summary>
        /// <param name="languageID"></param>
        /// <returns></returns>
        public QueueItemText getNextQueueItemText(int languageID)
        {
            QueueItemBase[] queueItems = ApproveQueueSA.Instance.Dequeue(1, QueueItemType.Text, Constants.NULL_INT, languageID);
            QueueItemText queueItemText = null;

            if (queueItems.Length != 0)
            {
                queueItemText = (QueueItemText)queueItems[0];
            }

            return queueItemText;
        }


    }
}
