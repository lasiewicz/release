﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;



using FreeTextSyncAssem;

namespace FreeTextSyncAssemDriver
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void process()
        {
            FreeTextSyncAssem.FreeTextSyncConfig objFreeTextSyncConfig = null;
            FreeTextSyncAssem.FreeTextSync objFreeTextSync = null;
            try
            {
                objFreeTextSyncConfig = new FreeTextSyncConfig();

                objFreeTextSyncConfig.UnApprovedMemberDBConnStr = ConfigurationManager.AppSettings["UnApprovedMemberDBConnStr"];
                objFreeTextSyncConfig.UnApprovedMemberCmdStr = ConfigurationManager.AppSettings["UnApprovedMemberCmdStr"];

                objFreeTextSyncConfig.RequeueOnlyLanguageID = ConfigurationManager.AppSettings["RequeueOnlyLanguageID"];

                objFreeTextSyncConfig.RequeueLogDBConnStr = ConfigurationManager.AppSettings["RequeueLogDBConnStr"];
                objFreeTextSyncConfig.RequeueLogCmdStr = ConfigurationManager.AppSettings["RequeueLogCmdStr"];

                objFreeTextSyncConfig.RequeueCountLimit = int.Parse(ConfigurationManager.AppSettings["RequeueCountLimit"]);

                objFreeTextSyncConfig.DedupeLogCmdStr = ConfigurationManager.AppSettings["DedupeLogCmdStr"];

                objFreeTextSync = new FreeTextSync(objFreeTextSyncConfig);

                objFreeTextSync.Process();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                //error is already log in event log
            }
            finally
            {
                if (null == objFreeTextSyncConfig)
                {
                    objFreeTextSyncConfig = null;
                }
                if (null == objFreeTextSync)
                {
                    objFreeTextSync = null;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.timer1.Enabled = false;
            this.process();
            this.Close();
        }
    }
}