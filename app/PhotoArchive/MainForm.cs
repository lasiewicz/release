using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Data.SqlClient;

using Matchnet.Data;

namespace PhotoArchive
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		private enum Scope
		{
			SelectedDirectory,
			SelectedDirectory_Recursive,
			AllFileServers
		}

		private const String ScopeSelectedDirectoryRecursive = "Selected directory, recursive";
		private const String ScopeAllFileServers = "All FileXX servers";
		private const Int32 ArchiveIntervalInDays = 7;
		private const String AcceptableFileTypes = ".jpg;.jpeg;.gif;.bmp";
		private String[] AllFileServers = new String[]{@"\\file01\c$\filerootc",
																@"\\file01\c$\filerootc",
																@"\\file02\c$\filerootc",
																@"\\file02\d$\filerootd",
																@"\\file03\c$\filerootc",
																@"\\file03\d$\filerootd",
																@"\\file04\c$\filerootc",
																@"\\file04\d$\filerootd",
																@"\\file05\c$\filerootc",
																@"\\file05\d$\filerootd",
																@"\\file06\c$\filerootc",
																@"\\file06\d$\filerootd",
																@"\\file07\c$\filerootc",
																@"\\file07\d$\filerootd",
																@"\\file08\c$\filerootc",
																@"\\file08\d$\filerootd",
																@"\\file09\c$\filerootc",
																@"\\file09\d$\filerootd",
																@"\\file10\c$\filerootc",
																@"\\file10\d$\filerootd",
																@"\\file11\c$\filerootc",
																@"\\file11\d$\filerootd",
																@"\\file12\c$\filerootc",
																@"\\file12\d$\filerootd"};

		private DateTime lastFullArchiveDate = DateTime.MinValue;

		private System.Windows.Forms.Button StartButton;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
		private System.Windows.Forms.TextBox SelectedDirectoryTextBox;
		private System.Windows.Forms.Button BrowseButton;
		private System.Windows.Forms.CheckBox UpdatesOnlyCheckBox;
		private System.Windows.Forms.ComboBox ScopeComboBox;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public MainForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			// Initialize components.
			folderBrowserDialog.ShowNewFolderButton = false;
			folderBrowserDialog.Description = "Select the root directory of the Photo file tree that you would like to archive.";

			ScopeComboBox.Items.Add(Scope.SelectedDirectory);
			ScopeComboBox.Items.Add(Scope.SelectedDirectory_Recursive);
			ScopeComboBox.Items.Add(Scope.AllFileServers);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.StartButton = new System.Windows.Forms.Button();
			this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.SelectedDirectoryTextBox = new System.Windows.Forms.TextBox();
			this.BrowseButton = new System.Windows.Forms.Button();
			this.UpdatesOnlyCheckBox = new System.Windows.Forms.CheckBox();
			this.ScopeComboBox = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// StartButton
			// 
			this.StartButton.Location = new System.Drawing.Point(16, 176);
			this.StartButton.Name = "StartButton";
			this.StartButton.TabIndex = 0;
			this.StartButton.Text = "Start";
			this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
			// 
			// SelectedDirectoryTextBox
			// 
			this.SelectedDirectoryTextBox.Location = new System.Drawing.Point(16, 32);
			this.SelectedDirectoryTextBox.Name = "SelectedDirectoryTextBox";
			this.SelectedDirectoryTextBox.Size = new System.Drawing.Size(272, 20);
			this.SelectedDirectoryTextBox.TabIndex = 1;
			this.SelectedDirectoryTextBox.Text = "";
			// 
			// BrowseButton
			// 
			this.BrowseButton.Location = new System.Drawing.Point(296, 32);
			this.BrowseButton.Name = "BrowseButton";
			this.BrowseButton.TabIndex = 2;
			this.BrowseButton.Text = "Browse";
			this.BrowseButton.Click += new System.EventHandler(this.BrowseButton_Click);
			// 
			// UpdatesOnlyCheckBox
			// 
			this.UpdatesOnlyCheckBox.Checked = true;
			this.UpdatesOnlyCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.UpdatesOnlyCheckBox.Location = new System.Drawing.Point(16, 152);
			this.UpdatesOnlyCheckBox.Name = "UpdatesOnlyCheckBox";
			this.UpdatesOnlyCheckBox.Size = new System.Drawing.Size(192, 24);
			this.UpdatesOnlyCheckBox.TabIndex = 4;
			this.UpdatesOnlyCheckBox.Text = "Updates and New Records Only";
			// 
			// ScopeComboBox
			// 
			this.ScopeComboBox.Location = new System.Drawing.Point(16, 88);
			this.ScopeComboBox.Name = "ScopeComboBox";
			this.ScopeComboBox.Size = new System.Drawing.Size(121, 21);
			this.ScopeComboBox.TabIndex = 5;
			// 
			// MainForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(392, 349);
			this.Controls.Add(this.ScopeComboBox);
			this.Controls.Add(this.UpdatesOnlyCheckBox);
			this.Controls.Add(this.BrowseButton);
			this.Controls.Add(this.SelectedDirectoryTextBox);
			this.Controls.Add(this.StartButton);
			this.Name = "MainForm";
			this.Text = "Photo Archive Tool";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new MainForm());
		}

		private void StartButton_Click(object sender, System.EventArgs e)
		{
			GetLastFullArchiveDate();

			String directoryPath = SelectedDirectoryTextBox.Text.Trim();

			if ((Scope)ScopeComboBox.SelectedItem != Scope.AllFileServers && (directoryPath == String.Empty || !Directory.Exists(directoryPath)))
			{
				MessageBox.Show("Please select a valid directory.");
				return;
			}

			if ((Scope)ScopeComboBox.SelectedItem == Scope.AllFileServers)
			{
				for (int i = 0; i < AllFileServers.Length; i++)
				{
					ParseDirectories(AllFileServers[i]);
				}

				SaveFullArchiveDate();
			}
			else
			{
				ParseDirectories(directoryPath);
			}
		}

		private void BrowseButton_Click(object sender, System.EventArgs e)
		{
			folderBrowserDialog.SelectedPath = SelectedDirectoryTextBox.Text.Trim();

			if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
				SelectedDirectoryTextBox.Text = folderBrowserDialog.SelectedPath;
		}

		private void ParseDirectories(String directoryPath)
		{
			String[] filePaths = Directory.GetFiles(directoryPath);

			foreach (String filePath in filePaths)
			{
				// Only archive if the modified date of the file is later than the last time we archived.
				// (NOTE:  Assumes we will not be archiving more than the ArchiveIntervalInDays and assumes that archiving can
				// be completed in less than ArchiveIntervalInDays.  If neither of these is true then we must rework this.
				FileInfo fileInfo = new FileInfo(filePath);
				DateTime modifiedDate = fileInfo.LastWriteTime;	

				if (modifiedDate.Date.Subtract(new TimeSpan(ArchiveIntervalInDays, 0, 0, 0)) >= lastFullArchiveDate.Date)
				{	
					// Get properties.
					try
					{
						DateTime createDate = fileInfo.CreationTime;
						String fileExtension = fileInfo.Extension;

						if (AcceptableFileTypes.IndexOf(fileExtension) != -1)
						{
							String fileName = fileInfo.Name;
							fileName = fileName.Substring(0, fileName.Length - fileExtension.Length);
							Int64 fileSize = fileInfo.Length;

							Image image = Image.FromFile(filePath);
							Int32 width = image.Width;
							Int32 height = image.Height;
							Single horResolution = image.HorizontalResolution;
							Single verResolution = image.VerticalResolution;

							SavePhotoArchive(fileName, fileExtension, filePath,
								fileSize, width, height, horResolution, verResolution,
								createDate, modifiedDate);
						}
					}
					catch (Exception ex)
					{
						Console.WriteLine(ex.Message);
					}
				}
			}

			if ((Scope)ScopeComboBox.SelectedItem == Scope.AllFileServers || (Scope)ScopeComboBox.SelectedItem == Scope.SelectedDirectory_Recursive)
			{
				String[] subDirectoryPaths = Directory.GetDirectories(directoryPath);
				foreach (String subDirectoryPath in subDirectoryPaths)
				{
					// Do not iterate through reparse points.
					// It is the responsibility of any application that scans the directory hierarchy 
					// and especially the responsibility of applications that make destructive changes 
					// recursively through the directory hierarchy to recognize directory junctions and 
					// avoid traversing through them, Because directory junctions are implementer using 
					// reparse points, applications should see if a directory has the FILE_REPARSE_POINT 
					// attribute set before processing that directory. Your code is safe if you do not 
					// process any directory with FILE_REPARSE_POINT set which you can verify with 
					// functions such as GetFileAttributes.
					if ((File.GetAttributes(subDirectoryPath) & FileAttributes.ReparsePoint) != FileAttributes.ReparsePoint)
						ParseDirectories(subDirectoryPath);
				}
			}
		}

		private void SavePhotoArchive(String fileID, String fileExtension,
			String filePath, Int64 fileSize, Int32 width, Int32 height, Single horResolution,
			Single verResolution, DateTime createDate, DateTime modifiedDate)
		{
			Command command = new Command("mnPhotoArchive", "dbo.up_Photo_Save", 0);
			
			command.AddParameter("@FileID", SqlDbType.VarChar, ParameterDirection.Input, fileID);
			command.AddParameter("@FileExtension", SqlDbType.VarChar, ParameterDirection.Input, fileExtension);
			command.AddParameter("@FilePath", SqlDbType.VarChar, ParameterDirection.Input, filePath);
			command.AddParameter("@FileSize", SqlDbType.BigInt, ParameterDirection.Input, fileSize);
			command.AddParameter("@Width", SqlDbType.Int, ParameterDirection.Input, width);
			command.AddParameter("@Height", SqlDbType.Int, ParameterDirection.Input, height);
			command.AddParameter("@HorResolution", SqlDbType.Float, ParameterDirection.Input, horResolution);
			command.AddParameter("@VerResolution", SqlDbType.Float, ParameterDirection.Input, verResolution);
			command.AddParameter("@CreateDate", SqlDbType.SmallDateTime, ParameterDirection.Input, createDate);
			command.AddParameter("@ModifiedDate", SqlDbType.SmallDateTime, ParameterDirection.Input, modifiedDate);
			
			Client.Instance.ExecuteNonQuery(command);
		}

		private void GetLastFullArchiveDate()
		{
			Command command = new Command("mnPhotoArchive", "dbo.up_FullArchiveDate_Get", 0);
						
			SqlDataReader sqlDataReader = Client.Instance.ExecuteReader(command);

			sqlDataReader.Read();
			lastFullArchiveDate = Convert.ToDateTime(sqlDataReader.GetValue(0));
			sqlDataReader.Close();
		}

		private void SaveFullArchiveDate()
		{
			Command command = new Command("mnPhotoArchive", "dbo.up_FullArchiveDate_Save", 0);
						
			Client.Instance.ExecuteNonQuery(command);
		}
	}
}
