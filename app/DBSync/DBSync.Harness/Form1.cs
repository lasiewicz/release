using System;
using System.Drawing;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using System.Threading;

using DBSync.BusinessLogic;
namespace DBSync.Harness {
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form {
		private DBSync.Harness.dsSlaveDatabases dsSlaveDatabases1;
		private System.Data.SqlClient.SqlDataAdapter sqlDASlaveDB;
		private System.Windows.Forms.RichTextBox txtOutput;
		private System.Windows.Forms.Button btnCompare;
		private System.Windows.Forms.Button btnSyncAllSlaves;
		private System.Windows.Forms.TabControl tabMain;
		private System.Windows.Forms.TabPage tpSlaves;
		private System.Windows.Forms.TabPage tpTables;
		private System.Windows.Forms.TreeView tvTables;
		private System.Data.SqlClient.SqlDataAdapter sqlDATableNames;
		private DBSync.Harness.dsTableNames dsTableNames1;
		private System.Data.SqlClient.SqlConnection sqlConnMasterDB;
		private System.Windows.Forms.TreeView tvSlaves;

		private string _MasterDB;
		private string _MasterHost;
		private string [] _SlaveHosts;
		private string [] _TablesToCompare;
		private Thread _BLThread;
		private DBSyncBL _DBSyncBL;
		private System.Windows.Forms.Button btnPopulate;
		private System.Windows.Forms.CheckBox chkSelectAll;
		private System.Windows.Forms.TextBox txtMasterHost;
		private System.Windows.Forms.TextBox txtMasterDB;
		private System.Windows.Forms.StatusBar statusBar1;
		private System.Data.SqlClient.SqlCommand sqlSelectCommand2;
		private System.Data.SqlClient.SqlCommand sqlSelectCommand1;
		private System.Windows.Forms.Panel panel1;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1() {
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();			
			
			dsSlaveDatabases1 = new dsSlaveDatabases();
			dsTableNames1 = new dsTableNames();

			_MasterDB = ConfigurationSettings.AppSettings["MasterDB"];
			_MasterHost = ConfigurationSettings.AppSettings["MasterHost"];

			txtMasterDB.Text = _MasterDB;
			txtMasterHost.Text = _MasterHost;


			RefreshDBSettings();


		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing ) {
			if( disposing ) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.sqlDASlaveDB = new System.Data.SqlClient.SqlDataAdapter();
			this.sqlSelectCommand1 = new System.Data.SqlClient.SqlCommand();
			this.sqlConnMasterDB = new System.Data.SqlClient.SqlConnection();
			this.btnPopulate = new System.Windows.Forms.Button();
			this.txtOutput = new System.Windows.Forms.RichTextBox();
			this.btnCompare = new System.Windows.Forms.Button();
			this.btnSyncAllSlaves = new System.Windows.Forms.Button();
			this.tabMain = new System.Windows.Forms.TabControl();
			this.tpSlaves = new System.Windows.Forms.TabPage();
			this.tvSlaves = new System.Windows.Forms.TreeView();
			this.tpTables = new System.Windows.Forms.TabPage();
			this.tvTables = new System.Windows.Forms.TreeView();
			this.sqlDATableNames = new System.Data.SqlClient.SqlDataAdapter();
			this.sqlSelectCommand2 = new System.Data.SqlClient.SqlCommand();
			this.chkSelectAll = new System.Windows.Forms.CheckBox();
			this.txtMasterHost = new System.Windows.Forms.TextBox();
			this.txtMasterDB = new System.Windows.Forms.TextBox();
			this.statusBar1 = new System.Windows.Forms.StatusBar();
			this.panel1 = new System.Windows.Forms.Panel();
			this.tabMain.SuspendLayout();
			this.tpSlaves.SuspendLayout();
			this.tpTables.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// sqlDASlaveDB
			// 
			this.sqlDASlaveDB.SelectCommand = this.sqlSelectCommand1;
			this.sqlDASlaveDB.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
																								   new System.Data.Common.DataTableMapping("Table", "PhysicalDatabase", new System.Data.Common.DataColumnMapping[] {
																																																					   new System.Data.Common.DataColumnMapping("ServerName", "ServerName"),
																																																					   new System.Data.Common.DataColumnMapping("Checked", "Checked")})});
			// 
			// sqlSelectCommand1
			// 
			this.sqlSelectCommand1.CommandText = "SELECT LOWER(ServerName) AS ServerName, ActiveFlag AS Checked FROM mnSystem.dbo.P" +
				"hysicalDatabase WHERE (LOWER(PhysicalDatabaseName) = LOWER(@MasterDB)) AND (LOWE" +
				"R(ServerName) <> LOWER(@MasterHostName)) ORDER BY ActiveFlag DESC";
			this.sqlSelectCommand1.Connection = this.sqlConnMasterDB;
			this.sqlSelectCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@MasterDB", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "", System.Data.DataRowVersion.Current, "mnSystem"));
			this.sqlSelectCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@MasterHostName", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "", System.Data.DataRowVersion.Current, "devsql01"));
			// 
			// sqlConnMasterDB
			// 
			this.sqlConnMasterDB.ConnectionString = "packet size=4096;integrated security=SSPI;data source=devsql01;persist security info=True;initial catalog=mnSystem";
			// 
			// btnPopulate
			// 
			this.btnPopulate.Location = new System.Drawing.Point(0, 0);
			this.btnPopulate.Name = "btnPopulate";
			this.btnPopulate.Size = new System.Drawing.Size(96, 23);
			this.btnPopulate.TabIndex = 1;
			this.btnPopulate.Text = "Populate";
			this.btnPopulate.Click += new System.EventHandler(this.btnPopulate_Click);
			// 
			// txtOutput
			// 
			this.txtOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtOutput.AutoSize = true;
			this.txtOutput.Location = new System.Drawing.Point(136, 8);
			this.txtOutput.Name = "txtOutput";
			this.txtOutput.Size = new System.Drawing.Size(568, 528);
			this.txtOutput.TabIndex = 2;
			this.txtOutput.Text = "";
			// 
			// btnCompare
			// 
			this.btnCompare.Enabled = false;
			this.btnCompare.Location = new System.Drawing.Point(0, 32);
			this.btnCompare.Name = "btnCompare";
			this.btnCompare.Size = new System.Drawing.Size(120, 23);
			this.btnCompare.TabIndex = 3;
			this.btnCompare.Text = "Compare Selected";
			this.btnCompare.Click += new System.EventHandler(this.btnCompare_Click);
			// 
			// btnSyncAllSlaves
			// 
			this.btnSyncAllSlaves.Enabled = false;
			this.btnSyncAllSlaves.Location = new System.Drawing.Point(0, 56);
			this.btnSyncAllSlaves.Name = "btnSyncAllSlaves";
			this.btnSyncAllSlaves.Size = new System.Drawing.Size(120, 23);
			this.btnSyncAllSlaves.TabIndex = 4;
			this.btnSyncAllSlaves.Text = "Sync All Slave(s)";
			this.btnSyncAllSlaves.Click += new System.EventHandler(this.btnSyncAllSlaves_Click);
			// 
			// tabMain
			// 
			this.tabMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left)));
			this.tabMain.Controls.Add(this.tpSlaves);
			this.tabMain.Controls.Add(this.tpTables);
			this.tabMain.Location = new System.Drawing.Point(0, 64);
			this.tabMain.Name = "tabMain";
			this.tabMain.SelectedIndex = 0;
			this.tabMain.Size = new System.Drawing.Size(136, 376);
			this.tabMain.TabIndex = 5;
			// 
			// tpSlaves
			// 
			this.tpSlaves.Controls.Add(this.tvSlaves);
			this.tpSlaves.Location = new System.Drawing.Point(4, 22);
			this.tpSlaves.Name = "tpSlaves";
			this.tpSlaves.Size = new System.Drawing.Size(128, 326);
			this.tpSlaves.TabIndex = 0;
			this.tpSlaves.Text = "Servers";
			// 
			// tvSlaves
			// 
			this.tvSlaves.CheckBoxes = true;
			this.tvSlaves.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tvSlaves.ImageIndex = -1;
			this.tvSlaves.Location = new System.Drawing.Point(0, 0);
			this.tvSlaves.Name = "tvSlaves";
			this.tvSlaves.SelectedImageIndex = -1;
			this.tvSlaves.ShowLines = false;
			this.tvSlaves.ShowPlusMinus = false;
			this.tvSlaves.ShowRootLines = false;
			this.tvSlaves.Size = new System.Drawing.Size(128, 326);
			this.tvSlaves.TabIndex = 0;
			// 
			// tpTables
			// 
			this.tpTables.Controls.Add(this.tvTables);
			this.tpTables.Location = new System.Drawing.Point(4, 22);
			this.tpTables.Name = "tpTables";
			this.tpTables.Size = new System.Drawing.Size(128, 350);
			this.tpTables.TabIndex = 1;
			this.tpTables.Text = "Tables";
			// 
			// tvTables
			// 
			this.tvTables.CheckBoxes = true;
			this.tvTables.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tvTables.ImageIndex = -1;
			this.tvTables.Location = new System.Drawing.Point(0, 0);
			this.tvTables.Name = "tvTables";
			this.tvTables.SelectedImageIndex = -1;
			this.tvTables.ShowLines = false;
			this.tvTables.ShowPlusMinus = false;
			this.tvTables.ShowRootLines = false;
			this.tvTables.Size = new System.Drawing.Size(128, 350);
			this.tvTables.TabIndex = 0;
			// 
			// sqlDATableNames
			// 
			this.sqlDATableNames.SelectCommand = this.sqlSelectCommand2;
			this.sqlDATableNames.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
																									  new System.Data.Common.DataTableMapping("Table", "sysobjects", new System.Data.Common.DataColumnMapping[] {
																																																					new System.Data.Common.DataColumnMapping("name", "name")})});
			// 
			// sqlSelectCommand2
			// 
			this.sqlSelectCommand2.CommandText = "SELECT name FROM sysobjects WHERE (type = \'u\') ORDER BY name";
			this.sqlSelectCommand2.Connection = this.sqlConnMasterDB;
			// 
			// chkSelectAll
			// 
			this.chkSelectAll.Checked = true;
			this.chkSelectAll.CheckState = System.Windows.Forms.CheckState.Indeterminate;
			this.chkSelectAll.Location = new System.Drawing.Point(112, 0);
			this.chkSelectAll.Name = "chkSelectAll";
			this.chkSelectAll.Size = new System.Drawing.Size(24, 24);
			this.chkSelectAll.TabIndex = 6;
			this.chkSelectAll.ThreeState = true;
			this.chkSelectAll.CheckStateChanged += new System.EventHandler(this.chkSelectAll_CheckedStateChanged);
			// 
			// txtMasterHost
			// 
			this.txtMasterHost.Location = new System.Drawing.Point(8, 16);
			this.txtMasterHost.Name = "txtMasterHost";
			this.txtMasterHost.Size = new System.Drawing.Size(120, 20);
			this.txtMasterHost.TabIndex = 7;
			this.txtMasterHost.Text = "[Master Host]";
			this.txtMasterHost.TextChanged += new System.EventHandler(this.txtMasterHost_TextChanged);
			// 
			// txtMasterDB
			// 
			this.txtMasterDB.Location = new System.Drawing.Point(8, 40);
			this.txtMasterDB.Name = "txtMasterDB";
			this.txtMasterDB.Size = new System.Drawing.Size(120, 20);
			this.txtMasterDB.TabIndex = 8;
			this.txtMasterDB.Text = "[MasterDB]";
			this.txtMasterDB.TextChanged += new System.EventHandler(this.txtMasterDB_TextChanged);
			// 
			// statusBar1
			// 
			this.statusBar1.Location = new System.Drawing.Point(0, 535);
			this.statusBar1.Name = "statusBar1";
			this.statusBar1.Size = new System.Drawing.Size(712, 22);
			this.statusBar1.TabIndex = 9;
			this.statusBar1.Text = "Click populate to begin..";
			// 
			// panel1
			// 
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.panel1.Controls.Add(this.btnPopulate);
			this.panel1.Controls.Add(this.btnCompare);
			this.panel1.Controls.Add(this.btnSyncAllSlaves);
			this.panel1.Controls.Add(this.chkSelectAll);
			this.panel1.Location = new System.Drawing.Point(0, 448);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(136, 80);
			this.panel1.TabIndex = 10;
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(712, 557);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.statusBar1);
			this.Controls.Add(this.txtMasterDB);
			this.Controls.Add(this.txtMasterHost);
			this.Controls.Add(this.tabMain);
			this.Controls.Add(this.txtOutput);
			this.Name = "Form1";
			this.Text = "DB Sync Utility";
			this.tabMain.ResumeLayout(false);
			this.tpSlaves.ResumeLayout(false);
			this.tpTables.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() {
			Application.Run(new Form1());
		}

		private void RefreshDBSettings(){
			sqlConnMasterDB = new SqlConnection(String.Format("Persist Security Info=False;Integrated Security=SSPI;server={0};database={1}",
																_MasterHost,
																_MasterDB));

			sqlDASlaveDB.SelectCommand.Connection = sqlConnMasterDB;
			sqlDATableNames.SelectCommand.Connection = sqlConnMasterDB;			

			sqlSelectCommand1.Parameters["@MasterHostName"].Value = _MasterHost;
			sqlSelectCommand1.Parameters["@MasterDB"].Value = _MasterDB;

		}


		private void btnPopulate_Click(object sender, System.EventArgs e) {
			btnCompare.Enabled = false;
			btnSyncAllSlaves.Enabled = false;
		
			dsSlaveDatabases1.Clear();
			try {
				sqlDASlaveDB.Fill(dsSlaveDatabases1);
			}
			catch {
				MessageBox.Show(string.Format("Can't populate list of slave databases.\n"
									+"Make sure  [{0}].[{1}] exists and that you have permissions there",
									_MasterHost,_MasterDB),
								"Error!",
								MessageBoxButtons.OK);
				return;

			}

			StringCollection Preselected;
			Preselected = GetPreselected("SlaveList");

			tvSlaves.Nodes.Clear();
			foreach( dsSlaveDatabases.PhysicalDatabaseRow r in dsSlaveDatabases1.PhysicalDatabase){
				TreeNode tn = new TreeNode(r.ServerName);
				if (Preselected.Contains(r.ServerName) || r.Checked == true){
					tn.Checked = true;
				}
				tvSlaves.Nodes.Add(tn);
			}


			dsTableNames1.Clear();
			try {
				sqlDATableNames.Fill(dsTableNames1);
			}
			catch {
				MessageBox.Show(string.Format("Can't populate list of tables.\n"
											+"Make sure [{0}].[{1}] exists and that you have permissions there",
											_MasterHost,_MasterDB),
								"Error!",
								MessageBoxButtons.OK);
				return;

			}

			Preselected = GetPreselected("TableList");

			tvTables.Nodes.Clear();
			foreach( dsTableNames.sysobjectsRow  row in dsTableNames1.sysobjects){
				TreeNode tn = new TreeNode(row.name);
				if (Preselected.Contains(row.name)){
					tn.Checked = true;
				}
				tvTables.Nodes.Add(tn);
			}

			if (tvSlaves.Nodes.Count > 0){
				btnCompare.Enabled = true;
			}
		}	

		private void btnCompare_Click(object sender, System.EventArgs e) {
			btnSyncAllSlaves.Enabled = false;
			txtOutput.Clear();

			_SlaveHosts = GetSelectedTreeNodes(tvSlaves);
			_TablesToCompare = GetSelectedTreeNodes(tvTables);
			_DBSyncBL = new DBSyncBL(_MasterHost,_MasterDB,_SlaveHosts,_TablesToCompare);
			_DBSyncBL.StatusNotification +=new DBSynceNotification(_DBSyncBL_StatusNotification);

			/// switch to background thread..
	///			_DBSyncBL.CompareAll();
			_BLThread = new Thread(new ThreadStart(_DBSyncBL.CompareAll));
			_BLThread.Start();
			_BLThread.Join();


			ShowCompareResults();

			btnSyncAllSlaves.Enabled = true;
		}


		private void ShowCompareResults(){
			for (int i = 0; i < _DBSyncBL.Comparators.Length; i++){
				Comparator cmp = _DBSyncBL.Comparators[i];

				txtOutput.AppendText(cmp.ComparisonSummary);
				txtOutput.AppendText("\n-----------------------------\n");
				txtOutput.AppendText(cmp.ComparisonErrors);

				txtOutput.AppendText(cmp.ComparisonDetail);
				txtOutput.AppendText("\n/***************************/\n");
				txtOutput.AppendText(cmp.MigrationSQL);
							
			}
		}


		private void btnSyncAllSlaves_Click(object sender, System.EventArgs e) {
			_DBSyncBL.SyncAllSlaves();
		}


		private string [] GetSelectedTreeNodes(TreeView tv){
			int SelectedCount = 0;
			foreach (TreeNode tn in tv.Nodes){
				if (tn.Checked) {
					SelectedCount++;
				}
			}
			string [] result = new string[SelectedCount];
			int i = 0;
			foreach (TreeNode tn in tv.Nodes){
				if (tn.Checked) {
					result[i++] = tn.Text;
				}
			}
			return result;
		}


		private void chkSelectAll_CheckedStateChanged(object sender, System.EventArgs e) {
			btnSyncAllSlaves.Enabled = false;
			if (tabMain.SelectedTab == tpSlaves){
				ToggleSelectionForAllNodes(ref tvSlaves, "SlaveList");
			}
			if (tabMain.SelectedTab == tpTables){
				ToggleSelectionForAllNodes(ref tvTables, "TableList");

			}
		}

		private void ToggleSelectionForAllNodes(ref TreeView tv, string AppSettingKey){
			if (chkSelectAll.CheckState == CheckState.Indeterminate){
				StringCollection Preselected = GetPreselected(AppSettingKey);
				foreach (TreeNode tn in tv.Nodes){
					tn.Checked = Preselected.Contains(tn.Text);
				}
			}
			else { 
				foreach (TreeNode tn in tv.Nodes){
					tn.Checked = chkSelectAll.Checked;
				}
			}
		}

		private StringCollection GetPreselected(string AppSettingKey){
			StringCollection result = new StringCollection();
			result.AddRange(ConfigurationSettings.AppSettings[AppSettingKey].ToLower().Split(','));
			return result;
		}

		private void txtMasterHost_TextChanged(object sender, System.EventArgs e) {
			_MasterHost = txtMasterHost.Text;
			ClearSelectionLists();
		}

		private void txtMasterDB_TextChanged(object sender, System.EventArgs e) {
			_MasterDB = txtMasterDB.Text;
			ClearSelectionLists();
		}

		private void ClearSelectionLists(){
			tvSlaves.Nodes.Clear();
			tvTables.Nodes.Clear();		
			btnCompare.Enabled = false;
			btnSyncAllSlaves.Enabled = false;
		
			RefreshDBSettings();

		}

	
		private void UpdateStatus(string message){
			statusBar1.Text = message;
		}

		private void _DBSyncBL_StatusNotification(object sender, DBSynceEventArgs  e)
		{
			UpdateStatus(e.Information);
		}

	}
}
