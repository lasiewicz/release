using System;
using System.Collections;

namespace DBSync.BusinessLogic
{


	public class DBSynceEventArgs: EventArgs
	{
		public string Information;
		public DBSynceEventArgs(string Text)
		{ 
			Information = Text; 
		}
	}

	public delegate void DBSynceNotification(object sender, DBSynceEventArgs e);

	/// <summary>
	/// Summary description for DBSyncBL.
	/// </summary>
	public class DBSyncBL
	{
		private Comparator [] _Comparators;
		private string _MasterHost;
		private string _DBName;
		
		public event DBSynceNotification StatusNotification;

		public DBSyncBL(string MasterHost, string DBName, string [] SlaveHosts, string [] TablesToCompare){
			_MasterHost = MasterHost;
			_DBName = DBName;

			_Comparators = new Comparator[SlaveHosts.Length];

			for (int i = 0; i < SlaveHosts.Length; i++){
				_Comparators[i] = new Comparator(_MasterHost,DBName,SlaveHosts[i],DBName);
				if (TablesToCompare != null){
					_Comparators[i].TablesToInclude = TablesToCompare;
				}
			}
		}

		public void CompareAll(){
			for (int i = 0; i < _Comparators.Length; i++){
				StatusNotification(this ,new DBSynceEventArgs("Comparing " +  _Comparators[i].SlaveHost));
				_Comparators[i].Compare();
			}
		}

		public Comparator [] Comparators{
			get {
				return _Comparators;
			}
		}

		public string MasterHost{
			get {
				return _MasterHost;
			}
		}

		public string DatabaseName{
			get {
				return _DBName;
			}
		}

		public void SyncAllSlaves(){
			foreach (Comparator cmp in _Comparators){
				
				if (cmp.DifferencesExist)
				{
					StatusNotification(this ,new DBSynceEventArgs("Sync " + cmp.SlaveHost));
					cmp.SynchronizeSlave();
				}
				else {
					StatusNotification(this ,new DBSynceEventArgs("No Differences found in " + cmp.SlaveHost));
				}
			}
		}
	}

}
