using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Text;

using RedGate.SQL.Shared;
using RedGate.SQLDataCompare.Engine;
using RedGate.SQLDataCompare;
using System.Text.RegularExpressions;


namespace DBSync.BusinessLogic 
{
	/// <summary>
	/// Comparator is the object that does the work on comparison of a single master to a single slave.
	/// </summary>
	public class Comparator 
	{

		private SqlProvider _Provider = new SqlProvider();
		private Database _DBMaster = null;
		private Database _DBSlave = null;
		private string _MasterHost;
		private string _MasterDB;
		private string _SlaveHost;
		private string _SlaveDB;
		private StringBuilder _CompareErrorReport = new StringBuilder();
		private bool _DifferencesExist = false;
		private bool _CompareFailed = false;
		private ComparisonSession _Session;

		private StringCollection _TablesToInclude = new StringCollection();

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="MasterHost">Master database server / host name</param>
		/// <param name="MasterDB">Master database DB </param>
		/// <param name="SlaveHost">Slave database server /host name</param>
		/// <param name="SlaveDB">Slave database DB</param>
		public Comparator(string MasterHost,string MasterDB, string SlaveHost, string SlaveDB)	
		{
			_MasterHost = MasterHost;
			_MasterDB = MasterDB;
			_SlaveHost = SlaveHost;
			_SlaveDB = SlaveDB;
		}

		/// <summary>
		/// Resets and initializes all data. Previous comparison session and data will be lost.
		/// </summary>
		public void Clear()
		{
			_DBMaster = _Provider.GetDatabase(new SqlConnectionProperties(_MasterHost, _MasterDB));
			_DBSlave = _Provider.GetDatabase(new SqlConnectionProperties(_SlaveHost, _SlaveDB));
			_Session = new ComparisonSession();
			_Provider = new SqlProvider();
			_DifferencesExist = false;
		}

		/// <summary>
		/// Returns a name like [dbo].[SomeName]
		/// RedGate requires a name to be owner qualified square bracketed.
		/// This function wraps a bare name with [dbo].[BareName] if necessary.
		/// </summary>
		/// <param name="BareName">The potentially bare table name</param>
		/// <returns>[dbo].[BareName]</returns>
		private string FormatTableName(string BareName)
		{
			if (Regex.IsMatch(BareName,"\\[.+\\]\\.\\[.+\\]"))
			{
				return BareName;
			}
			else 
			{
				return string.Format("[dbo].[{0}]",BareName);
				
			}
		}
		
		/// <summary>
		/// Gets the name of the slave database host / server
		/// </summary>
		public string SlaveHost
		{
			get
			{
				return _SlaveHost;
			}
		}

		public bool CompareFailed
		{
			get 
			{
				return _CompareFailed;
			}
		}

		/// <summary>
		/// Gets a table difference object
		/// </summary>
		/// <paramref name="index"> The 0 based index of the table difference to retreive</paramref>
		public TableDifference this[int index]
		{
			get
			{
				return _Session.TableDifferences[GetIndex(index)];
			}
		}

		/// <summary>
		/// Gets the collection of table differences
		/// </summary>
		public TableDifferences TableDifferences
		{
			get 
			{
				return _Session.TableDifferences;
			}
		}

		/// <summary>
		/// Performs a comparison on the 2 tables on 2 servers specified in the constructor.
		/// Comparison results are expressed in terms of changing Server2.DB2.{Table} to match Server1.DB1.{Table}
		/// </summary>
		public void Compare()
		{
			Clear();

			TableComparisonSettings Settings = new TableComparisonSettings();
			TableComparisonSetting Setting;
			Table cmpTbl;
			
			// Load up the comparison settings into this session.
			foreach (string TableName in TablesToCompare(_DBMaster))
			{
				cmpTbl = _DBMaster.Tables[TableName];

				if (_DBSlave.Tables[cmpTbl.FullyQualifiedName] == null)
				{
					Trace.WriteLine(String.Format("  --> {0} not found in slave DB.", TableName ));
					_CompareFailed = true;
					_DifferencesExist = true;
					_CompareErrorReport.Append(String.Format("  --> {0} not found in slave DB.\n", TableName ));

				} 
				else 
				{
					try 
					{
						Setting = new TableComparisonSetting(cmpTbl.FullyQualifiedName,cmpTbl.Fields,cmpTbl.PrimaryKey.Fields);
						Settings.Add(Setting);		
					} 
					catch (Exception ex)
					{
						_CompareFailed = true;
						_DifferencesExist = true;
						_CompareErrorReport.Append(String.Format("  --> Can't compare {0}. (PK?) ({1})\n", cmpTbl.FullyQualifiedName, ex.Message));
					}
				}
			}

			try 
			{
				if (!_CompareFailed)
				{
					// perform comparison for all these tables
					_Session.CompareDatabases(_DBMaster, _DBSlave, Settings);

					// populate results on a per table basis
					foreach ( TableDifference td in _Session.TableDifferences)
					{
						td.Selected = true;
						if (td.DifferentCount + td.In1Count + td.In2Count > 0 ) 
						{ 
							_DifferencesExist = true;
						}
					}
				}
			}
			catch (Exception ex) 
			{
				Trace.WriteLine("!!! Session.CompareDatabases exception\n" + ex.ToString());
				_DifferencesExist = true;
				_CompareErrorReport.Append("!!! Session.CompareDatabases exception\n" + ex.ToString());
			}
		}


		public void SynchronizeSlave()
		{
			if (!_DifferencesExist || _CompareFailed) 
			{
				return;
			}
			RedGate.SQL.Shared.Utils utils = new Utils();		
			ExecutionBlock block = _Provider.GetMigrationSQL(_Session, true);
			utils.ExecuteBlock( block,_SlaveHost,_SlaveDB);
		}

		
		public string MigrationSQL
		{
			get 
			{
				if (!_DifferencesExist) 
				{
					return "/* NOTHING TO DO. */";
				}
				if (_CompareFailed){
					return "/* Comparison failed. Unsafe to make changes based on failed comparisons.*/";
				}
				ExecutionBlock block = _Provider.GetMigrationSQL(_Session, true);
				if (block != null) 
				{
					return block.ToString();
				}
				else 
				{
					return "/* Migration SQL is empty */";
				}
			}
		}

		
		public string ComparisonSummary
		{
			get 
			{
				return String.Format("[{0}] from [{1}] to [{2}]: ({3}) Tables compared.", _MasterDB , _MasterHost , _SlaveHost, _Session.TableDifferences.Count);
			}
		}

		public string ComparisonErrors
		{
			get 
			{
				return _CompareErrorReport.ToString();
			}
		}		

		public string ComparisonDetail
		{
			get 
			{
				StringBuilder sb = new StringBuilder();
				foreach ( TableDifference td in _Session.TableDifferences)
				{
					sb.Append(String.Format("/*** DIFF: {0} -->> {1} identical rec, {2} diff rec, {3} rec only in Master, {4} rec only in Slave\n", td.Name, td.SameCount, td.DifferentCount, td.In1Count, td.In2Count));
				}
				return sb.ToString();
			}
		}

		public bool DifferencesExist
		{
			get 
			{
				return _DifferencesExist;
			}
		}

		private int GetIndex(int index)
		{
			if (index >= 0 && _Session.TableDifferences != null && index < _Session.TableDifferences.Count)
			{
				return index;
			}
			else 
			{
				throw new Exception("Index out of range. No such table difference entry " + index.ToString());
			}
		}

		/// <summary>
		/// Assign list of tables, which limits comparison to this list alone.
		/// </summary>
		public string [] TablesToInclude
		{
			set 
			{
				_TablesToInclude.AddRange(value);
			}
		}

		/// <summary>
		/// Packs a list of table names, fully qualified ([dbo].[tablename]) from a list of tables in the 
		/// master DB passed to it, srubbing the full list of tables in the master DB against the list of 
		/// tables to include.
		/// If list of tables to include was not set, then all tables in the master DB will be compared.
		/// </summary>
		/// <param name="MasterDB">The database object representing the Master database.</param>
		/// <returns>A list of tables the comparer should compare</returns>
		private StringCollection TablesToCompare( Database MasterDB)
		{
			StringCollection result = new StringCollection();
			foreach (Table tbl in MasterDB.Tables)
			{
				if ( _TablesToInclude.Count == 0 || _TablesToInclude.Contains(tbl.Name))
				{
					result.Add(tbl.FullyQualifiedName);
				}
			}
			return result;
		}
	}
}
