using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using Matchnet.Content.ServiceAdapters;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for Category.
	/// </summary>
	public class Category : System.Windows.Forms.Form
	{
		private System.ComponentModel.Container Components = null;
		private Settings _settings;
		private Matchnet.Content.ValueObjects.Article.Category _voCategory;
		private System.Windows.Forms.GroupBox gbxButtons;
		private System.Windows.Forms.Button cmdCancel;
		private System.Windows.Forms.Button cmdSave;
		private System.Windows.Forms.Label lblCategoryID;
		private System.Windows.Forms.Label lblConstant;
		private System.Windows.Forms.Label lblParentCategoryID;
		private System.Windows.Forms.TextBox tbxCategoryID;
		private System.Windows.Forms.TextBox tbxParentCategoryID;
		private System.Windows.Forms.TextBox tbxConstant;
		private System.Windows.Forms.TextBox tbxLastUpdated;
		private System.Windows.Forms.Label lblLastUpdated;
		private System.Windows.Forms.Label lblListOrder;
		private System.Windows.Forms.TextBox tbxListOrder;

		public Category()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		public Category(int CategoryID) : this()
		{
			if (CategoryID != int.MinValue)
				_voCategory = ArticleSA.Instance.RetrieveCategory(CategoryID);
			else
			{
				_voCategory = new Matchnet.Content.ValueObjects.Article.Category();
				_voCategory.CategoryID = CategoryID;
			}
		}

		public Category(int CategoryID, int ParentCategoryID) : this(CategoryID)
		{
			_voCategory.ParentCategoryID = ParentCategoryID;
		}

		public Settings settings
		{
			set
			{
				_settings = value;
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(Components != null)
				{
					Components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblConstant = new System.Windows.Forms.Label();
			this.gbxButtons = new System.Windows.Forms.GroupBox();
			this.cmdCancel = new System.Windows.Forms.Button();
			this.cmdSave = new System.Windows.Forms.Button();
			this.lblParentCategoryID = new System.Windows.Forms.Label();
			this.lblCategoryID = new System.Windows.Forms.Label();
			this.tbxCategoryID = new System.Windows.Forms.TextBox();
			this.tbxParentCategoryID = new System.Windows.Forms.TextBox();
			this.tbxConstant = new System.Windows.Forms.TextBox();
			this.tbxLastUpdated = new System.Windows.Forms.TextBox();
			this.lblLastUpdated = new System.Windows.Forms.Label();
			this.tbxListOrder = new System.Windows.Forms.TextBox();
			this.lblListOrder = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lblConstant
			// 
			this.lblConstant.Location = new System.Drawing.Point(120, 88);
			this.lblConstant.Name = "lblConstant";
			this.lblConstant.Size = new System.Drawing.Size(89, 16);
			this.lblConstant.TabIndex = 28;
			this.lblConstant.Text = "Constant:";
			this.lblConstant.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// gbxButtons
			// 
			this.gbxButtons.Location = new System.Drawing.Point(-16, 144);
			this.gbxButtons.Name = "gbxButtons";
			this.gbxButtons.Size = new System.Drawing.Size(716, 4);
			this.gbxButtons.TabIndex = 30;
			this.gbxButtons.TabStop = false;
			this.gbxButtons.Text = "gbxButtons";
			// 
			// cmdCancel
			// 
			this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cmdCancel.Location = new System.Drawing.Point(560, 160);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(81, 28);
			this.cmdCancel.TabIndex = 4;
			this.cmdCancel.Text = "&Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.Location = new System.Drawing.Point(472, 160);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Size = new System.Drawing.Size(81, 28);
			this.cmdSave.TabIndex = 3;
			this.cmdSave.Text = "&Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// lblParentCategoryID
			// 
			this.lblParentCategoryID.Location = new System.Drawing.Point(96, 40);
			this.lblParentCategoryID.Name = "lblParentCategoryID";
			this.lblParentCategoryID.Size = new System.Drawing.Size(112, 16);
			this.lblParentCategoryID.TabIndex = 27;
			this.lblParentCategoryID.Text = "ParentCategoryID:";
			this.lblParentCategoryID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblCategoryID
			// 
			this.lblCategoryID.Location = new System.Drawing.Point(120, 16);
			this.lblCategoryID.Name = "lblCategoryID";
			this.lblCategoryID.Size = new System.Drawing.Size(89, 16);
			this.lblCategoryID.TabIndex = 25;
			this.lblCategoryID.Text = "CategoryID:";
			this.lblCategoryID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxCategoryID
			// 
			this.tbxCategoryID.Location = new System.Drawing.Point(208, 16);
			this.tbxCategoryID.Name = "tbxCategoryID";
			this.tbxCategoryID.ReadOnly = true;
			this.tbxCategoryID.Size = new System.Drawing.Size(144, 20);
			this.tbxCategoryID.TabIndex = 36;
			this.tbxCategoryID.Text = "";
			// 
			// tbxParentCategoryID
			// 
			this.tbxParentCategoryID.Location = new System.Drawing.Point(208, 40);
			this.tbxParentCategoryID.Name = "tbxParentCategoryID";
			this.tbxParentCategoryID.ReadOnly = true;
			this.tbxParentCategoryID.Size = new System.Drawing.Size(144, 20);
			this.tbxParentCategoryID.TabIndex = 37;
			this.tbxParentCategoryID.Text = "";
			// 
			// tbxConstant
			// 
			this.tbxConstant.Location = new System.Drawing.Point(208, 88);
			this.tbxConstant.Name = "tbxConstant";
			this.tbxConstant.Size = new System.Drawing.Size(320, 20);
			this.tbxConstant.TabIndex = 46;
			this.tbxConstant.Text = "";
			this.tbxConstant.TextChanged += new System.EventHandler(this.tbxConstant_TextChanged);
			// 
			// tbxLastUpdated
			// 
			this.tbxLastUpdated.Location = new System.Drawing.Point(208, 112);
			this.tbxLastUpdated.Name = "tbxLastUpdated";
			this.tbxLastUpdated.ReadOnly = true;
			this.tbxLastUpdated.Size = new System.Drawing.Size(320, 20);
			this.tbxLastUpdated.TabIndex = 39;
			this.tbxLastUpdated.Text = "";
			// 
			// lblLastUpdated
			// 
			this.lblLastUpdated.Location = new System.Drawing.Point(120, 112);
			this.lblLastUpdated.Name = "lblLastUpdated";
			this.lblLastUpdated.Size = new System.Drawing.Size(89, 16);
			this.lblLastUpdated.TabIndex = 41;
			this.lblLastUpdated.Text = "Last Updated:";
			this.lblLastUpdated.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxListOrder
			// 
			this.tbxListOrder.Location = new System.Drawing.Point(208, 64);
			this.tbxListOrder.Name = "tbxListOrder";
			this.tbxListOrder.Size = new System.Drawing.Size(144, 20);
			this.tbxListOrder.TabIndex = 45;
			this.tbxListOrder.Text = "";
			// 
			// lblListOrder
			// 
			this.lblListOrder.Location = new System.Drawing.Point(96, 64);
			this.lblListOrder.Name = "lblListOrder";
			this.lblListOrder.Size = new System.Drawing.Size(112, 16);
			this.lblListOrder.TabIndex = 46;
			this.lblListOrder.Text = "List Order:";
			this.lblListOrder.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// Category
			// 
			this.AcceptButton = this.cmdSave;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.cmdCancel;
			this.ClientSize = new System.Drawing.Size(654, 199);
			this.Controls.Add(this.lblListOrder);
			this.Controls.Add(this.tbxListOrder);
			this.Controls.Add(this.lblLastUpdated);
			this.Controls.Add(this.tbxLastUpdated);
			this.Controls.Add(this.tbxConstant);
			this.Controls.Add(this.tbxParentCategoryID);
			this.Controls.Add(this.tbxCategoryID);
			this.Controls.Add(this.lblConstant);
			this.Controls.Add(this.gbxButtons);
			this.Controls.Add(this.cmdCancel);
			this.Controls.Add(this.cmdSave);
			this.Controls.Add(this.lblParentCategoryID);
			this.Controls.Add(this.lblCategoryID);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Category";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Edit Category";
			this.Load += new System.EventHandler(this.Category_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private bool SaveCategory()
		{
			try
			{
				int ret = ArticleSA.Instance.SaveCategory(BuildVOCategory());

				if (ret > 0)
					return true;
				else
					return false;
			}
			catch(Exception ex)
			{
				// TODO:  Move this error handler to the middle tier and make sure it propagates up so it can be handled here.
//				if (ex.Number == 2601)
//					MessageBox.Show("This constant is already in use. Please create a new constant and save again.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
//				else
					MessageBox.Show("Error: " + ex.Message,  "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Information);

				return false;
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			if (tbxConstant.Text == string.Empty)
			{
				MessageBox.Show("Error: Please enter English constant",  "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				if (SaveCategory())
				{
					DialogResult = DialogResult.OK;
					this.Close();
				}
			}
		}

		private void Category_Load(object sender, System.EventArgs e)
		{
			DisplayCategory();
		}

		/// <summary>
		/// Make the Constant upper-case.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tbxConstant_TextChanged(object sender, System.EventArgs e)
		{
			Helper.BuildConstant(ref tbxConstant);
		}

		private void DisplayCategory()
		{
			tbxCategoryID.Text = _voCategory.CategoryID.ToString();
			tbxParentCategoryID.Text = _voCategory.ParentCategoryID.ToString();
			tbxConstant.Text = _voCategory.Constant;
			tbxListOrder.Text = _voCategory.ListOrder.ToString();
			tbxLastUpdated.Text = _voCategory.LastUpdated.ToString();
		}

		private Matchnet.Content.ValueObjects.Article.Category BuildVOCategory()
		{
			int categoryID = Convert.ToInt32(tbxCategoryID.Text.Trim());
			int parentCategoryID = Convert.ToInt32(tbxParentCategoryID.Text.Trim());
			int listOrder = Convert.ToInt32(tbxListOrder.Text.Trim());
			string constant = tbxConstant.Text.Trim();

			return new Matchnet.Content.ValueObjects.Article.Category(categoryID, parentCategoryID, listOrder, constant); 
		}
	}
}
