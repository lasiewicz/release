using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for GroupList.
	/// </summary>
	public class GroupList : BaseList
	{
		
		private Boolean _allowNew;
		private System.Windows.Forms.ComboBox cbGroups;

		/// Required designer variable.
		private System.ComponentModel.Container components = null;

		#region Properties
		public override object SelectedItem
		{
			get
			{
				return base.SelectedItem;
			}
			set
			{
				if (base.Data != null && value != null)
				{
					Web.SecurityGroup newGroup = (Web.SecurityGroup) value;

					foreach (Web.SecurityGroup group in base.Data)
					{
						if (group.SecurityGroupID == newGroup.SecurityGroupID)
						{
							base.SelectedItem = group;
						}
					}
				}
			}
		}

		/// <summary>
		/// If true, the user can enter a new group description and a new SecurityGroup will be created and added to the list.
		/// Otherwise, the user can select from existing SecurityGroups and add these to the list.
		/// Cannot be changed after the control loads.
		/// </summary>
		public Boolean AllowNew
		{
			get
			{
				return _allowNew;
			}
			set
			{
				_allowNew = value;
			}
		}
		#endregion

		public GroupList()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cbGroups = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// cbGroups
			// 
			this.cbGroups.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbGroups.Location = new System.Drawing.Point(400, 40);
			this.cbGroups.Name = "cbGroups";
			this.cbGroups.Size = new System.Drawing.Size(160, 21);
			this.cbGroups.TabIndex = 16;
			// 
			// GroupList
			// 
			this.Controls.Add(this.cbGroups);
			this.Name = "GroupList";
			this.Load += new System.EventHandler(this.GroupList_Load);
			this.Controls.SetChildIndex(this.cbGroups, 0);
			this.ResumeLayout(false);

		}
		#endregion

		private void GroupList_Load(object sender, System.EventArgs e)
		{
			//Per Garry, cannot modify mnSystem from code, so for now not allowing modifications to groups
			base.ShowAdd = false;
			base.ShowDelete = false;
			this.tbNewItem.Visible = false;
			this.cbGroups.Visible = false;
			this.AddText = String.Empty;

			/*if (!AllowNew)
			{
				this.tbNewItem.Visible = false;
				this.cbGroups.Visible = true;
				cbGroups.DataSource = MemberPrivilegeSA.Instance.GetSecurityGroups();
				cbGroups.DisplayMember = "Description";
			}
			else
			{
				this.tbNewItem.Visible = true;
				this.cbGroups.Visible = false;
			}*/
		}

		#region Overrides
		protected override void onAdd()
		{
			this.AddItem(this.cbGroups.SelectedItem);
			base.onAdd(this.cbGroups.SelectedItem);
		}

		/*public override void AddItem(object newItem)
		{
			Member newMember = (Member) newItem;

			foreach (Member member in Data)
			{
				if (member.MemberID == newMember.MemberID)
				{
					MessageBox.Show("Cannot add this member.  Member is already in the list.");
					return;
				}
			}

			base.AddItem(newItem);
		}*/
		#endregion
	}
}
