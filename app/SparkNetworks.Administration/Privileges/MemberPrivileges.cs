using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for MemberPrivileges.
	/// </summary>
	public class MemberPrivileges : System.Windows.Forms.Form
	{
		private Settings _settings;
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Label lblMemberID;
		private System.Windows.Forms.Label lblEmail;
		private System.Windows.Forms.Label lblUsername;
		private System.Windows.Forms.TextBox tbMemberID;
		private System.Windows.Forms.TextBox tbEmail;
		private System.Windows.Forms.TextBox tbUsername;
		private System.Windows.Forms.Button btnLookup;
		private System.Windows.Forms.CheckedListBox clbPrivileges;

		private Hashtable memberPrivilegeItems = new Hashtable();
		private System.Windows.Forms.Button btnSave;
		private Web.Member _member;

		public Web.Member Member
		{
			get
			{
				return _member;
			}
			set
			{
				_member = value;
			}
		}

		public MemberPrivileges()
		{
			// Required for Windows Form Designer support
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblMemberID = new System.Windows.Forms.Label();
			this.lblEmail = new System.Windows.Forms.Label();
			this.lblUsername = new System.Windows.Forms.Label();
			this.tbMemberID = new System.Windows.Forms.TextBox();
			this.tbEmail = new System.Windows.Forms.TextBox();
			this.tbUsername = new System.Windows.Forms.TextBox();
			this.btnLookup = new System.Windows.Forms.Button();
			this.clbPrivileges = new System.Windows.Forms.CheckedListBox();
			this.btnSave = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lblMemberID
			// 
			this.lblMemberID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblMemberID.Location = new System.Drawing.Point(16, 8);
			this.lblMemberID.Name = "lblMemberID";
			this.lblMemberID.Size = new System.Drawing.Size(64, 16);
			this.lblMemberID.TabIndex = 0;
			this.lblMemberID.Text = "MemberID";
			// 
			// lblEmail
			// 
			this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblEmail.Location = new System.Drawing.Point(96, 8);
			this.lblEmail.Name = "lblEmail";
			this.lblEmail.Size = new System.Drawing.Size(160, 16);
			this.lblEmail.TabIndex = 1;
			this.lblEmail.Text = "Email";
			// 
			// lblUsername
			// 
			this.lblUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblUsername.Location = new System.Drawing.Point(272, 8);
			this.lblUsername.Name = "lblUsername";
			this.lblUsername.Size = new System.Drawing.Size(96, 16);
			this.lblUsername.TabIndex = 2;
			this.lblUsername.Text = "Username";
			// 
			// tbMemberID
			// 
			this.tbMemberID.Location = new System.Drawing.Point(16, 32);
			this.tbMemberID.Name = "tbMemberID";
			this.tbMemberID.Size = new System.Drawing.Size(64, 20);
			this.tbMemberID.TabIndex = 7;
			this.tbMemberID.Text = "";
			// 
			// tbEmail
			// 
			this.tbEmail.Location = new System.Drawing.Point(96, 32);
			this.tbEmail.Name = "tbEmail";
			this.tbEmail.Size = new System.Drawing.Size(160, 20);
			this.tbEmail.TabIndex = 8;
			this.tbEmail.Text = "";
			// 
			// tbUsername
			// 
			this.tbUsername.Location = new System.Drawing.Point(272, 32);
			this.tbUsername.Name = "tbUsername";
			this.tbUsername.Size = new System.Drawing.Size(96, 20);
			this.tbUsername.TabIndex = 9;
			this.tbUsername.Text = "";
			// 
			// btnLookup
			// 
			this.btnLookup.Location = new System.Drawing.Point(384, 16);
			this.btnLookup.Name = "btnLookup";
			this.btnLookup.Size = new System.Drawing.Size(56, 24);
			this.btnLookup.TabIndex = 10;
			this.btnLookup.Text = "Lookup";
			this.btnLookup.Click += new System.EventHandler(this.btnLookup_Click);
			// 
			// clbPrivileges
			// 
			this.clbPrivileges.Location = new System.Drawing.Point(16, 72);
			this.clbPrivileges.Name = "clbPrivileges";
			this.clbPrivileges.Size = new System.Drawing.Size(352, 214);
			this.clbPrivileges.TabIndex = 11;
			this.clbPrivileges.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.clbPrivileges_ItemCheck);
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(384, 80);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(56, 24);
			this.btnSave.TabIndex = 12;
			this.btnSave.Text = "Save";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// MemberPrivileges
			// 
			this.AcceptButton = this.btnLookup;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(456, 317);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.clbPrivileges);
			this.Controls.Add(this.btnLookup);
			this.Controls.Add(this.tbUsername);
			this.Controls.Add(this.tbEmail);
			this.Controls.Add(this.tbMemberID);
			this.Controls.Add(this.lblUsername);
			this.Controls.Add(this.lblEmail);
			this.Controls.Add(this.lblMemberID);
			this.Name = "MemberPrivileges";
			this.Text = "MemberPrivileges";
			this.Load += new System.EventHandler(this.MemberPrivileges_Load);
			this.ResumeLayout(false);

		}
		#endregion

		#region Properties
		public Settings settings
		{
			set
			{
				_settings = value;
			}
		}
		#endregion

		#region Private Methods
		private void bindData()
		{
			this.tbMemberID.Text = Member.MemberID.ToString();
			this.tbEmail.Text = Member.EmailAddress;
			this.tbUsername.Text = Member.Username;

			clbPrivileges.Items.Clear();
			memberPrivilegeItems.Clear();
			Web.MemberPrivilege[] memberPrivileges = _settings.ClientAPI.GetMemberPrivileges(Member.MemberID);
			for (Int32 i = 0; i < memberPrivileges.Length; i++)
			{
				Web.MemberPrivilege memberPrivilege = memberPrivileges[i];

				CheckState checkState;
				switch (memberPrivilege.PrivilegeState)
				{
					case Web.PrivilegeState.Denied:
						checkState = CheckState.Unchecked;
						break;
					case Web.PrivilegeState.Individual:
						checkState = CheckState.Checked;
						break;
					case Web.PrivilegeState.SecurityGroup:
					default:
						checkState = CheckState.Indeterminate;
						break;
				}
				memberPrivilegeItems.Add(clbPrivileges.Items.Add(memberPrivilege.Privilege.Description, checkState), memberPrivilege);
			}
		}

		private void resetForm()
		{
			this.tbMemberID.Clear();
			this.tbEmail.Clear();
			this.tbUsername.Clear();

			clbPrivileges.Items.Clear();
		}

		private void doMemberLookup()
		{
			//First try lookup by MemberID
			Int32 memberID = Matchnet.Conversion.CInt(this.tbMemberID.Text);

			//If no MemberID, try Email
			if (memberID == Matchnet.Constants.NULL_INT && this.tbEmail.Text != string.Empty)
			{
				Web.Member[] members = _settings.ClientAPI.GetMembersByEmail(this.tbEmail.Text,	true);

				if (members.Length > 0)
					memberID = members[0].MemberID;
			}

			//If no Email, try Username
			if (memberID == Matchnet.Constants.NULL_INT && this.tbUsername.Text != string.Empty)
			{
				memberID = _settings.ClientAPI.GetMembersByUsername(this.tbUsername.Text, true)[0].MemberID;
			}

			if (memberID != Matchnet.Constants.NULL_INT)
			{
				this.Member = _settings.ClientAPI.GetMember(memberID);
			}
			else
			{
				MessageBox.Show("Member not found.");
				return;
			}

			bindData();
		}

		private void enableLookup(Boolean enabled)
		{
			this.tbMemberID.Enabled = enabled;
			this.tbEmail.Enabled = enabled;
			this.tbUsername.Enabled = enabled;

			btnSave.Enabled = !enabled;

			btnLookup.Text = enabled ? "Lookup" : "Reset";
		}

		#endregion

		#region Event Handlers
		private void MemberPrivileges_Load(object sender, System.EventArgs e)
		{
			if (Member != null)
			{
				bindData();
				this.enableLookup(false);
			}
			else
			{
				this.enableLookup(true);
			}
		}

		private void btnLookup_Click(object sender, System.EventArgs e)
		{
			if (btnLookup.Text != "Lookup")
			{
				resetForm();
				enableLookup(true);
			}
			else
			{
				doMemberLookup();

				if (this.Member != null)
				{
					enableLookup(false);
				}
			}
		}

		private void clbPrivileges_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
		{
			Web.MemberPrivilege memberPrivilege = memberPrivilegeItems[e.Index] as Web.MemberPrivilege;
			if (memberPrivilege != null && memberPrivilege.PrivilegeState == Web.PrivilegeState.SecurityGroup)
			{
				e.NewValue = e.CurrentValue;
				MessageBox.Show("This privilege was inherited as part of a group membership.  To remove it, please remove the user from the relevant security group(s).");
			}
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			for (Int32 i = 0; i < this.clbPrivileges.Items.Count; i++)
			{
				Web.MemberPrivilege memberPrivilege = (Web.MemberPrivilege) this.memberPrivilegeItems[i];

				switch (this.clbPrivileges.GetItemCheckState(i))
				{
					case CheckState.Checked:
						//if (!MemberPrivilegeSA.Instance.HasPrivilege(Member.MemberID, memberPrivilege.Privilege.PrivilegeID))
						//{
							_settings.ClientAPI.SaveMemberPrivilege(Member.MemberID, memberPrivilege.Privilege.PrivilegeID);
						//}
						break;
					case CheckState.Unchecked:
						//if (MemberPrivilegeSA.Instance.HasPrivilege(Member.MemberID, memberPrivilege.Privilege.PrivilegeID))
						//{
							_settings.ClientAPI.DeleteMemberPrivilege(Member.MemberID, memberPrivilege.Privilege.PrivilegeID);
						//}
						break;
				}
			}
		}
		#endregion
	}
}
