using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for MemberList.
	/// </summary>
	public class MemberList : BaseList
	{
		private Settings _settings;
		private System.ComponentModel.Container components = null;

		public MemberList()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			this.DisplayMember = "EmailAddress";
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.SuspendLayout();
			// 
			// MemberList
			// 
			this.Controls.Add(this.tbNewItem);
			this.Name = "MemberList";
			this.Size = new System.Drawing.Size(584, 136);
			this.Controls.SetChildIndex(this.tbNewItem, 0);
			this.ResumeLayout(false);

		}
		#endregion

		#region Properties
		public Settings settings
		{
			set
			{
				_settings = value;
			}
		}
		#endregion

		#region Overrides
		protected override void onAdd()
		{
			int newMemberID = Matchnet.Conversion.CInt(this.tbNewItem.Text);
			Web.Member newMember;

			if (newMemberID > 0)
			{
				newMember = _settings.ClientAPI.GetMember(newMemberID);
			}
			else
			{
				Web.Member[] members = _settings.ClientAPI.GetMembersByEmail(this.tbNewItem.Text, false);

				//TOFIX: make GetMembersByEmail correctly populate totalRows
				if (members == null || members.Length == 0)
				{
					MessageBox.Show("No member found with that e-mail address.");
					return;
				}
				else if (members[0].MemberID == -1)	// If -1 is returned then more than 1 user was returned.
				{
					MessageBox.Show("Multiple members found with that e-mail address.  Use MemberID instead.");
					return;
				}
				else //totalRows == 1
				{
					newMember = _settings.ClientAPI.GetMember(members[0].MemberID);
				}
			}

			this.AddItem(newMember);
			base.onAdd(newMember);
		}

		public override void AddItem(object newItem)
		{
			Web.Member newMember = (Web.Member) newItem;

			foreach (Web.Member member in Data)
			{
				if (member != null && member.MemberID == newMember.MemberID)
				{
					MessageBox.Show("Cannot add this member.  Member is already in the list.");
					return;
				}
			}

			base.AddItem(newItem);
		}

		#endregion
	}
}
