using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for Groups.
	/// </summary>
	public class Groups : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Settings _settings;
		private System.ComponentModel.Container components = null;
		private SparkNetworks.Administration.GroupList GroupList;
		private SparkNetworks.Administration.MemberList MemberList;
		private System.Windows.Forms.Label label1;

		[Browsable(false)]
		public Web.SecurityGroup SelectedGroup
		{
			get
			{
				return (Web.SecurityGroup) this.GroupList.SelectedItem;
			}
			set
			{
				this.GroupList.SelectedItem = value;
			}
		}

		public Groups()
		{
			// Required for Windows Form Designer support
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.GroupList = new SparkNetworks.Administration.GroupList();
			this.MemberList = new SparkNetworks.Administration.MemberList();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// GroupList
			// 
			this.GroupList.AllowNew = true;
			this.GroupList.Data = null;
			this.GroupList.DisplayMember = "Description";
			this.GroupList.Location = new System.Drawing.Point(16, 16);
			this.GroupList.Name = "GroupList";
			this.GroupList.SelectedItem = null;
			this.GroupList.ShowAdd = false;
			this.GroupList.ShowDelete = false;
			this.GroupList.Size = new System.Drawing.Size(584, 144);
			this.GroupList.TabIndex = 0;
			this.GroupList.SelectedIndexChanged += new SparkNetworks.Administration.BaseList.SelectedIndexChangedEventHandler(this.GroupList_SelectedIndexChanged);
			// 
			// MemberList
			// 
			this.MemberList.Data = null;
			this.MemberList.DisplayMember = null;
			this.MemberList.Location = new System.Drawing.Point(16, 192);
			this.MemberList.Name = "MemberList";
			this.MemberList.SelectedItem = null;
			this.MemberList.ShowAdd = true;
			this.MemberList.ShowDelete = true;
			this.MemberList.Size = new System.Drawing.Size(584, 136);
			this.MemberList.TabIndex = 1;
			this.MemberList.Delete += new SparkNetworks.Administration.BaseList.DeleteEventHandler(this.MemberList_Delete);
			this.MemberList.ItemDoubleClick += new SparkNetworks.Administration.BaseList.ItemDoubleClickEventHandler(this.MemberList_ItemDoubleClick);
			this.MemberList.Add += new SparkNetworks.Administration.BaseList.AddEventHandler(this.MemberList_Add);
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.label1.Location = new System.Drawing.Point(16, 176);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(208, 16);
			this.label1.TabIndex = 2;
			this.label1.Text = "Members Who Belong To This Group";
			// 
			// Groups
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(616, 461);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.MemberList);
			this.Controls.Add(this.GroupList);
			this.Name = "Groups";
			this.Text = "Groups";
			this.Load += new System.EventHandler(this.Groups_Load);
			this.ResumeLayout(false);
		}
		#endregion

		#region Properties
		public Settings settings
		{
			set
			{
				_settings = value;
				MemberList.settings = _settings;
			}
		}
		#endregion

		#region Event Handlers
		private void GroupList_SelectedIndexChanged(System.EventArgs args)
		{
			Web.SecurityGroupMember[] groupMembers = _settings.ClientAPI.GetSecurityGroupMembers(this.SelectedGroup.SecurityGroupID);

			ArrayList members = new ArrayList();
			for (Int32 i = 0; i < groupMembers.Length; i++)
			{
				try
				{
					members.Add(_settings.ClientAPI.GetMember(groupMembers[i].MemberID));
				}
				catch
				{
					// Sometimes faulty data causes the GetMember call to fail.  Do not blow up because of data.
				}
			}

			this.MemberList.Data = members;
			this.MemberList.DisplayMember = "EmailAddressVal";
		}

		private void Groups_Load(object sender, System.EventArgs e)
		{
			this.GroupList.Data = _settings.ClientAPI.GetSecurityGroups();
			this.GroupList.DisplayMember = "DescriptionVal";
			this.MemberList.AddText = "E-mail Address or MemberID";
		}

		private void MemberList_ItemDoubleClick(System.EventArgs args)
		{
			MemberPrivileges frmMemberPrivileges = new MemberPrivileges();
			frmMemberPrivileges.settings = _settings;
			frmMemberPrivileges.MdiParent = this.MdiParent;
			frmMemberPrivileges.Member = (Web.Member) MemberList.SelectedItem;
			frmMemberPrivileges.Show();
		}

		private void MemberList_Add(SparkNetworks.Administration.AddEventArgs args)
		{
			_settings.ClientAPI.SaveSecurityGroupMember(((Web.Member) args.Added).MemberID, this.SelectedGroup.SecurityGroupID);
		}

		private void MemberList_Delete(SparkNetworks.Administration.DeleteEventArgs args)
		{
			_settings.ClientAPI.DeleteSecurityGroupMember(((Web.Member) args.Deleted).MemberID, this.SelectedGroup.SecurityGroupID);
		}
		#endregion
	}
}
