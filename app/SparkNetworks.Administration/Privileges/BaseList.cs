using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for BaseList.
	/// </summary>
	public class BaseList : System.Windows.Forms.UserControl
	{
		#region Form Elements
		private System.Windows.Forms.Label lblAdd;
		private System.Windows.Forms.ListBox lbData;
		private System.Windows.Forms.Button btnDelete;
		private System.Windows.Forms.Button btnAdd;
		protected System.Windows.Forms.TextBox tbNewItem;
		#endregion
		/// Required designer variable.
		private System.ComponentModel.Container components = null;

		private IList _data;
		private String _displayMember;
		private Boolean _showAdd = true;
		private Boolean _showDelete = true;

		#region Properties
		[Browsable(false)]
		public IList Data
		{
			set
			{
				_data = value;
				bindData();
			}
			get
			{
				return _data;
			}
		}

		[Browsable(false)]
		public String DisplayMember
		{
			get
			{
				return _displayMember;
			}
			set
			{
				_displayMember = value;
				this.lbData.DisplayMember = _displayMember;
			}
		}

		[Browsable(false)]
		public virtual object SelectedItem
		{
			get
			{
				return this.lbData.SelectedItem;
			}
			set
			{
				if (_data != null && _data.Contains(value))
				{
					this.lbData.SelectedItem = value;
				}
			}
		}

		[Browsable(false)]
		public string AddText
		{
			set
			{
				this.lblAdd.Text = value;
			}
		}

		[Browsable(false)]
		public Boolean ShowAdd
		{
			get
			{
				return _showAdd;
			}
			set
			{
				_showAdd = value;
				this.tbNewItem.Visible = _showAdd;
				this.btnAdd.Visible = _showAdd;
			}
		}

		[Browsable(false)]
		public Boolean ShowDelete
		{
			get
			{
				return _showDelete;
			}
			set
			{
				_showDelete = value;
				this.btnDelete.Visible = _showDelete;
			}
		}
		#endregion

		#region Events
		public delegate void AddEventHandler(AddEventArgs args);
		public event AddEventHandler Add;

		public delegate void DeleteEventHandler(DeleteEventArgs args);
		public event DeleteEventHandler Delete;

		public delegate void SelectedIndexChangedEventHandler(EventArgs args);
		public event SelectedIndexChangedEventHandler SelectedIndexChanged;

		public delegate void ItemDoubleClickEventHandler(EventArgs args);
		public event ItemDoubleClickEventHandler ItemDoubleClick;
		#endregion

		public BaseList()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblAdd = new System.Windows.Forms.Label();
			this.lbData = new System.Windows.Forms.ListBox();
			this.btnDelete = new System.Windows.Forms.Button();
			this.btnAdd = new System.Windows.Forms.Button();
			this.tbNewItem = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// lblAdd
			// 
			this.lblAdd.Location = new System.Drawing.Point(400, 8);
			this.lblAdd.Name = "lblAdd";
			this.lblAdd.Size = new System.Drawing.Size(160, 24);
			this.lblAdd.TabIndex = 15;
			// 
			// lbData
			// 
			this.lbData.Location = new System.Drawing.Point(0, 0);
			this.lbData.Name = "lbData";
			this.lbData.Size = new System.Drawing.Size(392, 134);
			this.lbData.TabIndex = 13;
			this.lbData.DoubleClick += new System.EventHandler(this.lbData_DoubleClick);
			this.lbData.SelectedIndexChanged += new System.EventHandler(this.lbData_SelectedIndexChanged);
			// 
			// btnDelete
			// 
			this.btnDelete.Location = new System.Drawing.Point(512, 104);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(56, 24);
			this.btnDelete.TabIndex = 12;
			this.btnDelete.Text = "Delete";
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(512, 72);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(56, 24);
			this.btnAdd.TabIndex = 11;
			this.btnAdd.Text = "Add";
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// tbNewItem
			// 
			this.tbNewItem.Location = new System.Drawing.Point(400, 40);
			this.tbNewItem.Name = "tbNewItem";
			this.tbNewItem.Size = new System.Drawing.Size(160, 20);
			this.tbNewItem.TabIndex = 14;
			this.tbNewItem.Text = "";
			// 
			// BaseList
			// 
			this.Controls.Add(this.lblAdd);
			this.Controls.Add(this.tbNewItem);
			this.Controls.Add(this.lbData);
			this.Controls.Add(this.btnDelete);
			this.Controls.Add(this.btnAdd);
			this.Name = "BaseList";
			this.Size = new System.Drawing.Size(576, 136);
			this.Load += new System.EventHandler(this.BaseList_Load);
			this.ResumeLayout(false);

		}
		#endregion

		#region Event Handlers
		private void BaseList_Load(object sender, System.EventArgs e)
		{
			this.btnAdd.Visible = ShowAdd;
			this.btnDelete.Visible = ShowDelete;
		}

		private void btnAdd_Click(object sender, System.EventArgs e)
		{
			onAdd();
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			if (this.SelectedItem == null)
			{
				MessageBox.Show("Please select an item to delete.");
				return;
			}
			
			Object deleteItem = this.SelectedItem;
			_data.Remove(deleteItem);
			bindData();

			if (this.Delete != null)
			{
				this.Delete(new DeleteEventArgs(deleteItem));
			}
		}

		private void lbData_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (this.SelectedIndexChanged != null)
			{
				SelectedIndexChanged(e);
			}
		}

		private void lbData_DoubleClick(object sender, System.EventArgs e)
		{
			if (this.ItemDoubleClick != null)
			{
				ItemDoubleClick(e);
			}
		}
		#endregion

		#region Private/Protected Methods
		protected void bindData()
		{
			this.lbData.DataSource = null;
			this.lbData.DataSource = _data;
			this.lbData.DisplayMember = this.DisplayMember;
		}

		protected virtual void onAdd()
		{
		}

		protected void onAdd(Object added)
		{
			if (this.Add != null)
			{
				Add(new AddEventArgs(added));
			}
		}
		#endregion

		#region Public Methods
		public virtual void AddItem(object newItem)
		{
			Data.Add(newItem);
			bindData();
		}

		#endregion
	}

	public class AddEventArgs : EventArgs
	{
		private object _added;

		public AddEventArgs(object added)
		{
			_added = added;
		}

		public object Added
		{
			get
			{
				return _added;
			}
		}
	}

	public class DeleteEventArgs : EventArgs
	{
		private object _deleted;

		public DeleteEventArgs(object deleted)
		{
			_deleted = deleted;
		}

		public object Deleted
		{
			get
			{
				return _deleted;
			}
		}
	}
}
