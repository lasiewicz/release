using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for Privileges.
	/// </summary>
	public class Privileges : System.Windows.Forms.Form
	{
		private Settings _settings;
		private SparkNetworks.Administration.MemberList MemberList;
		private System.Windows.Forms.Label lblMembers;
		private System.Windows.Forms.Label lblGroups;
		private SparkNetworks.Administration.GroupList GroupList;
		private System.Windows.Forms.Label lblPrivileges;
		private SparkNetworks.Administration.BaseList PrivilegeList;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Privileges()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.MemberList = new SparkNetworks.Administration.MemberList();
			this.lblMembers = new System.Windows.Forms.Label();
			this.lblGroups = new System.Windows.Forms.Label();
			this.GroupList = new SparkNetworks.Administration.GroupList();
			this.lblPrivileges = new System.Windows.Forms.Label();
			this.PrivilegeList = new SparkNetworks.Administration.BaseList();
			this.SuspendLayout();
			// 
			// MemberList
			// 
			this.MemberList.Data = null;
			this.MemberList.DisplayMember = null;
			this.MemberList.Location = new System.Drawing.Point(16, 192);
			this.MemberList.Name = "MemberList";
			this.MemberList.SelectedItem = null;
			this.MemberList.ShowAdd = true;
			this.MemberList.ShowDelete = true;
			this.MemberList.Size = new System.Drawing.Size(600, 144);
			this.MemberList.TabIndex = 7;
			this.MemberList.Delete += new SparkNetworks.Administration.BaseList.DeleteEventHandler(this.MemberList_Delete);
			this.MemberList.ItemDoubleClick += new SparkNetworks.Administration.BaseList.ItemDoubleClickEventHandler(this.MemberList_ItemDoubleClick);
			this.MemberList.Add += new SparkNetworks.Administration.BaseList.AddEventHandler(this.MemberList_Add);
			// 
			// lblMembers
			// 
			this.lblMembers.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblMembers.Location = new System.Drawing.Point(16, 176);
			this.lblMembers.Name = "lblMembers";
			this.lblMembers.Size = new System.Drawing.Size(392, 16);
			this.lblMembers.TabIndex = 8;
			this.lblMembers.Text = "Members Who Have This Privilege";
			// 
			// lblGroups
			// 
			this.lblGroups.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblGroups.Location = new System.Drawing.Point(16, 352);
			this.lblGroups.Name = "lblGroups";
			this.lblGroups.Size = new System.Drawing.Size(392, 16);
			this.lblGroups.TabIndex = 9;
			this.lblGroups.Text = "Groups That Have This Privilege";
			// 
			// GroupList
			// 
			this.GroupList.AllowNew = false;
			this.GroupList.Data = null;
			this.GroupList.DisplayMember = "";
			this.GroupList.Location = new System.Drawing.Point(16, 368);
			this.GroupList.Name = "GroupList";
			this.GroupList.SelectedItem = null;
			this.GroupList.ShowAdd = false;
			this.GroupList.ShowDelete = false;
			this.GroupList.Size = new System.Drawing.Size(600, 144);
			this.GroupList.TabIndex = 10;
			this.GroupList.ItemDoubleClick += new SparkNetworks.Administration.BaseList.ItemDoubleClickEventHandler(this.GroupList_ItemDoubleClick);
			// 
			// lblPrivileges
			// 
			this.lblPrivileges.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblPrivileges.Location = new System.Drawing.Point(16, 8);
			this.lblPrivileges.Name = "lblPrivileges";
			this.lblPrivileges.Size = new System.Drawing.Size(416, 16);
			this.lblPrivileges.TabIndex = 11;
			this.lblPrivileges.Text = "Select a Privilege to see Members and Groups that have the selected Privilege";
			// 
			// PrivilegeList
			// 
			this.PrivilegeList.Data = null;
			this.PrivilegeList.DisplayMember = "";
			this.PrivilegeList.Location = new System.Drawing.Point(16, 24);
			this.PrivilegeList.Name = "PrivilegeList";
			this.PrivilegeList.SelectedItem = null;
			this.PrivilegeList.ShowAdd = false;
			this.PrivilegeList.ShowDelete = false;
			this.PrivilegeList.Size = new System.Drawing.Size(584, 136);
			this.PrivilegeList.TabIndex = 12;
			this.PrivilegeList.Delete += new SparkNetworks.Administration.BaseList.DeleteEventHandler(this.PrivilegeList_Delete);
			this.PrivilegeList.SelectedIndexChanged += new SparkNetworks.Administration.BaseList.SelectedIndexChangedEventHandler(this.PrivilegeList_SelectedIndexChanged);
			// 
			// Privileges
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(632, 525);
			this.Controls.Add(this.PrivilegeList);
			this.Controls.Add(this.lblPrivileges);
			this.Controls.Add(this.GroupList);
			this.Controls.Add(this.lblGroups);
			this.Controls.Add(this.lblMembers);
			this.Controls.Add(this.MemberList);
			this.Name = "Privileges";
			this.Text = "Privileges";
			this.Load += new System.EventHandler(this.Privileges_Load);
			this.ResumeLayout(false);

		}
		#endregion

		#region Properties
		public Settings settings
		{
			set
			{
				_settings = value;
			}
		}
		#endregion

		#region Event Handlers
		private void Privileges_Load(object sender, System.EventArgs e)
		{
			this.PrivilegeList.DisplayMember = "DescriptionVal";
			this.PrivilegeList.Data = _settings.ClientAPI.GetPrivileges();
			this.MemberList.AddText = "E-mail Address or MemberID";
			this.MemberList.settings = _settings;
		}

		private void PrivilegeList_SelectedIndexChanged(System.EventArgs e)
		{
			this.MemberList.DisplayMember = "EmailAddressVal";
			this.MemberList.Data = ConvertArrayToArrayList(_settings.ClientAPI.GetMembersByPrivilege(SelectedPrivilege.PrivilegeID));
			
			this.GroupList.DisplayMember = "DescriptionVal";
			this.GroupList.Data = _settings.ClientAPI.GetGroupsByPrivilege(SelectedPrivilege.PrivilegeID);
		}

		private void PrivilegeList_Delete(SparkNetworks.Administration.DeleteEventArgs args)
		{
			MessageBox.Show("Deleted.");
		}

		private void MemberList_Delete(DeleteEventArgs args)
		{
			_settings.ClientAPI.DeleteMemberPrivilege(((Web.Member) args.Deleted).MemberID, SelectedPrivilege.PrivilegeID);
		}

		private void MemberList_Add(SparkNetworks.Administration.AddEventArgs args)
		{
			_settings.ClientAPI.SaveMemberPrivilege(((Web.Member) args.Added).MemberID, SelectedPrivilege.PrivilegeID);
		}

		/*private void GroupList_Add(SparkNetworks.Administration.AddEventArgs args)
		{
			int securityGroupID = ((SecurityGroup) args.Added).SecurityGroupID;
			MemberPrivilegeSA.Instance.SaveGroupPrivilege(securityGroupID, this.SelectedPrivilege.PrivilegeID);
		}

		private void GroupList_Delete(SparkNetworks.Administration.DeleteEventArgs args)
		{
			int securityGroupID = ((SecurityGroup) args.Deleted).SecurityGroupID;
			MemberPrivilegeSA.Instance.DeleteGroupPrivilege(securityGroupID, this.SelectedPrivilege.PrivilegeID);
		}*/

		private void GroupList_ItemDoubleClick(EventArgs e)
		{
			Groups frmGroups = new Groups();
			frmGroups.MdiParent = this.MdiParent;
			frmGroups.settings = _settings;
			frmGroups.SelectedGroup = (Web.SecurityGroup) GroupList.SelectedItem;
			frmGroups.Show();
		}

		private void MemberList_ItemDoubleClick(System.EventArgs args)
		{
			MemberPrivileges frmMemberPrivileges = new MemberPrivileges();
			frmMemberPrivileges.MdiParent = this.MdiParent;
			frmMemberPrivileges.settings = _settings;
			frmMemberPrivileges.Member = (Web.Member) MemberList.SelectedItem;
			frmMemberPrivileges.Show();
		}
		#endregion

		protected Web.Privilege SelectedPrivilege
		{
			get
			{
				if(this.PrivilegeList.SelectedItem.GetType().Equals(typeof(Web.Privilege)))
				{
					return (Web.Privilege) this.PrivilegeList.SelectedItem;
				}
				else
				{
					return GetPrivilegeByID(_settings.ClientAPI.GetPrivileges(), (Int32)this.PrivilegeList.SelectedItem);
				}
			}
		}

		private Web.Privilege GetPrivilegeByID(Web.Privilege[] privileges, Int32 privilegeID)
		{
			for (Int32 i = 0; i < privileges.Length; i++)
			{
				if (privileges[i].PrivilegeID == privilegeID)
					return privileges[i];
			}

			return null;
		}

		private ArrayList ConvertArrayToArrayList(Object[] array)
		{
			ArrayList arrayList = new ArrayList(array.Length);
			for (Int32 i = 0; i < array.Length; i++)
			{
				arrayList.Add(array[i]);
			}

			return arrayList;
		}
	}
}
