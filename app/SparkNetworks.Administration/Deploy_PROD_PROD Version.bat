echo "Copying Client..."

robocopy C:\Matchnet\bedrock\App\SparkNetworks.Administration\bin\Release_PROD_PROD\Client\ \\spwebadmin01\c$\matchnet\bedrock\App\SparkNetworks.Administration\Client\ * /s /xo /r:999 /w:0
robocopy C:\Matchnet\bedrock\App\SparkNetworks.Administration\bin\Release_PROD_PROD\Client\ \\spwebadmin02\c$\matchnet\bedrock\App\SparkNetworks.Administration\Client\ * /s /xo /r:999 /w:0

echo "Copying Web Service..."

robocopy C:\Matchnet\bedrock\App\SparkNetworks.Administration\SparkNetworks.Administration.Web\ \\spwebadmin01\c$\matchnet\bedrock\App\SparkNetworks.Administration\ * /xo /r:999 /w:0
robocopy C:\Matchnet\bedrock\App\SparkNetworks.Administration\SparkNetworks.Administration.Web\ \\spwebadmin02\c$\matchnet\bedrock\App\SparkNetworks.Administration\ * /xo /r:999 /w:0
robocopy C:\Matchnet\bedrock\App\SparkNetworks.Administration\SparkNetworks.Administration.Web\bin\Release_PROD_PROD\ \\spwebadmin01\c$\matchnet\bedrock\App\SparkNetworks.Administration\bin\ * /xo /r:999 /w:0
robocopy C:\Matchnet\bedrock\App\SparkNetworks.Administration\SparkNetworks.Administration.Web\bin\Release_PROD_PROD\ \\spwebadmin02\c$\matchnet\bedrock\App\SparkNetworks.Administration\bin\ * /xo /r:999 /w:0

pause

