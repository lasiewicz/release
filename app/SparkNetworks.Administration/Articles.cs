using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

using Matchnet.Content.ValueObjects.Article;
using Matchnet.Content.ServiceAdapters;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// TODO:  The tvCateogies in Articles.cs and the tvCategories in Categories.cs should be consolidated so we don't
	/// have duplication of code.  Due to time constraints I can't do it at this time.
	/// </summary>
	public class Articles : System.Windows.Forms.Form
	{
		private System.ComponentModel.Container Components = null;
		private Settings _settings;
		private System.Windows.Forms.TreeView tvCategories;
		private System.Windows.Forms.ListView lvArticles;
		private System.Windows.Forms.MenuItem mnuAdd;
		private System.Windows.Forms.MenuItem mnuEdit;
		private System.Windows.Forms.MenuItem mnuDelete;
		private System.Windows.Forms.MenuItem mnuRefresh;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.ColumnHeader columnHeader6;
		private System.Windows.Forms.Label lblSite;
		private System.Windows.Forms.ComboBox cboSite;
		private System.Windows.Forms.ColumnHeader columnHeader7;
		private System.Windows.Forms.ColumnHeader columnHeader8;
		private System.Windows.Forms.ColumnHeader columnHeader9;
		private System.Windows.Forms.ColumnHeader columnHeader10;
		private System.Windows.Forms.ColumnHeader columnHeader11;
		private System.Windows.Forms.LinkLabel lnkRefreshCategories;
		private System.Windows.Forms.LinkLabel lnkEditSiteArtice;
		private System.Windows.Forms.Label lblCategories;
		private System.Windows.Forms.Label lblArticles;
		private System.Windows.Forms.MainMenu mnuMain;
		private System.Windows.Forms.MenuItem mnuArticles;
		private System.Windows.Forms.ColumnHeader columnHeader12;
		private System.Windows.Forms.LinkLabel lnkAddArticle;
		private System.Windows.Forms.MenuItem mnuDiv1;

		public Articles()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(Components != null)
				{
					Components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tvCategories = new System.Windows.Forms.TreeView();
			this.lblCategories = new System.Windows.Forms.Label();
			this.lnkAddArticle = new System.Windows.Forms.LinkLabel();
			this.lnkEditSiteArtice = new System.Windows.Forms.LinkLabel();
			this.lnkRefreshCategories = new System.Windows.Forms.LinkLabel();
			this.lvArticles = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader10 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader12 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader8 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader9 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader11 = new System.Windows.Forms.ColumnHeader();
			this.lblArticles = new System.Windows.Forms.Label();
			this.lblSite = new System.Windows.Forms.Label();
			this.mnuMain = new System.Windows.Forms.MainMenu();
			this.mnuArticles = new System.Windows.Forms.MenuItem();
			this.mnuAdd = new System.Windows.Forms.MenuItem();
			this.mnuEdit = new System.Windows.Forms.MenuItem();
			this.mnuDelete = new System.Windows.Forms.MenuItem();
			this.mnuDiv1 = new System.Windows.Forms.MenuItem();
			this.mnuRefresh = new System.Windows.Forms.MenuItem();
			this.cboSite = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// tvCategories
			// 
			this.tvCategories.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left)));
			this.tvCategories.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.tvCategories.HideSelection = false;
			this.tvCategories.ImageIndex = -1;
			this.tvCategories.Location = new System.Drawing.Point(10, 90);
			this.tvCategories.Name = "tvCategories";
			this.tvCategories.SelectedImageIndex = -1;
			this.tvCategories.Size = new System.Drawing.Size(282, 470);
			this.tvCategories.TabIndex = 27;
			this.tvCategories.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvCategories_AfterSelect);
			// 
			// lblCategories
			// 
			this.lblCategories.Location = new System.Drawing.Point(10, 70);
			this.lblCategories.Name = "lblCategories";
			this.lblCategories.Size = new System.Drawing.Size(96, 18);
			this.lblCategories.TabIndex = 33;
			this.lblCategories.Text = "Categories:";
			// 
			// lnkAddArticle
			// 
			this.lnkAddArticle.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
			this.lnkAddArticle.Location = new System.Drawing.Point(150, 43);
			this.lnkAddArticle.Name = "lnkAddArticle";
			this.lnkAddArticle.Size = new System.Drawing.Size(82, 20);
			this.lnkAddArticle.TabIndex = 24;
			this.lnkAddArticle.TabStop = true;
			this.lnkAddArticle.Text = "Add Article";
			this.lnkAddArticle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lnkAddArticle.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkAddArticle_LinkClicked);
			// 
			// lnkEditSiteArtice
			// 
			this.lnkEditSiteArtice.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
			this.lnkEditSiteArtice.Location = new System.Drawing.Point(250, 43);
			this.lnkEditSiteArtice.Name = "lnkEditSiteArtice";
			this.lnkEditSiteArtice.Size = new System.Drawing.Size(86, 20);
			this.lnkEditSiteArtice.TabIndex = 25;
			this.lnkEditSiteArtice.TabStop = true;
			this.lnkEditSiteArtice.Text = "Edit SiteArticle";
			this.lnkEditSiteArtice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lnkEditSiteArtice.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkEditArt_LinkClicked);
			// 
			// lnkRefreshCategories
			// 
			this.lnkRefreshCategories.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
			this.lnkRefreshCategories.Location = new System.Drawing.Point(30, 43);
			this.lnkRefreshCategories.Name = "lnkRefreshCategories";
			this.lnkRefreshCategories.Size = new System.Drawing.Size(106, 20);
			this.lnkRefreshCategories.TabIndex = 23;
			this.lnkRefreshCategories.TabStop = true;
			this.lnkRefreshCategories.Text = "Refresh Categories";
			this.lnkRefreshCategories.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lnkRefreshCategories.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkRefreshArt_LinkClicked);
			// 
			// lvArticles
			// 
			this.lvArticles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lvArticles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						 this.columnHeader1,
																						 this.columnHeader2,
																						 this.columnHeader3,
																						 this.columnHeader10,
																						 this.columnHeader12,
																						 this.columnHeader4,
																						 this.columnHeader5,
																						 this.columnHeader6,
																						 this.columnHeader7,
																						 this.columnHeader8,
																						 this.columnHeader9,
																						 this.columnHeader11});
			this.lvArticles.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lvArticles.FullRowSelect = true;
			this.lvArticles.GridLines = true;
			this.lvArticles.HideSelection = false;
			this.lvArticles.Location = new System.Drawing.Point(305, 90);
			this.lvArticles.MultiSelect = false;
			this.lvArticles.Name = "lvArticles";
			this.lvArticles.Size = new System.Drawing.Size(476, 470);
			this.lvArticles.TabIndex = 28;
			this.lvArticles.View = System.Windows.Forms.View.Details;
			this.lvArticles.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lvArticles_KeyPress);
			this.lvArticles.DoubleClick += new System.EventHandler(this.lvArticles_DoubleClick);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "SiteArticleID";
			this.columnHeader1.Width = 70;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "ArticleID";
			this.columnHeader2.Width = 50;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "SiteID";
			// 
			// columnHeader10
			// 
			this.columnHeader10.Text = "PublishedFlag";
			// 
			// columnHeader12
			// 
			this.columnHeader12.Text = "Constant";
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "MemberID";
			// 
			// columnHeader5
			// 
			this.columnHeader5.Text = "Title";
			this.columnHeader5.Width = 120;
			// 
			// columnHeader6
			// 
			this.columnHeader6.Text = "Content";
			this.columnHeader6.Width = 300;
			// 
			// columnHeader7
			// 
			this.columnHeader7.Text = "FileID";
			// 
			// columnHeader8
			// 
			this.columnHeader8.Text = "Ordinal";
			// 
			// columnHeader9
			// 
			this.columnHeader9.Text = "LastUpdated";
			// 
			// columnHeader11
			// 
			this.columnHeader11.Text = "FeaturedFlag";
			// 
			// lblArticles
			// 
			this.lblArticles.Location = new System.Drawing.Point(305, 70);
			this.lblArticles.Name = "lblArticles";
			this.lblArticles.Size = new System.Drawing.Size(96, 18);
			this.lblArticles.TabIndex = 34;
			this.lblArticles.Text = "Articles:";
			// 
			// lblSite
			// 
			this.lblSite.Location = new System.Drawing.Point(32, 8);
			this.lblSite.Name = "lblSite";
			this.lblSite.Size = new System.Drawing.Size(45, 21);
			this.lblSite.TabIndex = 35;
			this.lblSite.Text = "&Site:";
			this.lblSite.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// mnuArticles
			// 
			this.mnuArticles.Index = -1;
			this.mnuArticles.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						this.mnuAdd,
																						this.mnuEdit,
																						this.mnuDelete,
																						this.mnuDiv1,
																						this.mnuRefresh});
			this.mnuArticles.Text = "&Articles";
			// 
			// mnuAdd
			// 
			this.mnuAdd.Index = 0;
			this.mnuAdd.Text = "A&dd Article";
			this.mnuAdd.Click += new System.EventHandler(this.mnuAdd_Click);
			// 
			// mnuEdit
			// 
			this.mnuEdit.Index = 1;
			this.mnuEdit.Text = "&Edit Article";
			this.mnuEdit.Click += new System.EventHandler(this.mnuEdit_Click);
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = 2;
			this.mnuDelete.Text = "&Delete Article";
			// 
			// mnuDiv1
			// 
			this.mnuDiv1.Index = 3;
			this.mnuDiv1.Text = "-";
			// 
			// mnuRefresh
			// 
			this.mnuRefresh.Index = 4;
			this.mnuRefresh.Shortcut = System.Windows.Forms.Shortcut.F5;
			this.mnuRefresh.Text = "&Refresh Articles";
			this.mnuRefresh.Click += new System.EventHandler(this.mnuRefresh_Click);
			// 
			// cboSite
			// 
			this.cboSite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboSite.Location = new System.Drawing.Point(76, 10);
			this.cboSite.Name = "cboSite";
			this.cboSite.Size = new System.Drawing.Size(240, 21);
			this.cboSite.TabIndex = 36;
			this.cboSite.SelectedIndexChanged += new System.EventHandler(this.cboSite_SelectedIndexChanged);
			// 
			// Articles
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(792, 573);
			this.Controls.Add(this.cboSite);
			this.Controls.Add(this.lblSite);
			this.Controls.Add(this.tvCategories);
			this.Controls.Add(this.lblCategories);
			this.Controls.Add(this.lnkAddArticle);
			this.Controls.Add(this.lnkEditSiteArtice);
			this.Controls.Add(this.lnkRefreshCategories);
			this.Controls.Add(this.lvArticles);
			this.Controls.Add(this.lblArticles);
			this.Menu = this.mnuMain;
			this.Name = "Articles";
			this.Text = "Articles";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.Articles_Load);
			this.ResumeLayout(false);

		}
		#endregion

		public Settings settings
		{
			set
			{
				_settings = value;
				ItemData.SetComboBoxValues(cboSite, _settings.Sites, 2);
			}
			get
			{
				return _settings;
			}
		}

		private void Articles_Load(object sender, System.EventArgs e)
		{
			this.Visible = true;
			this.Refresh();

			Reload();
		}

		public void Reload()
		{
			lvArticles.Items.Clear();

			tvCategories.Nodes.Clear();
			SiteCategoryNode rootNode = new SiteCategoryNode("[Categories]", int.MinValue);
			tvCategories.Nodes.Add(rootNode);

			ItemData item = (ItemData)cboSite.SelectedItem;

			if (item == null)
				return;

	//		bool blnHebrew = _settings.IsHebrewTranslation(item.Value);	// TODO: ADD HEBREW SUPPORT.  Or is is already supported as is?

			try
			{
				SiteCategoryCollection siteCategories = ArticleSA.Instance.RetrieveSiteCategories(item.Value, true);

				foreach (Matchnet.Content.ValueObjects.Article.SiteCategory siteCategory in siteCategories)
				{
					string strCaption;

					//if (blnHebrew)
						//strCaption = _settings.HebrewToUTF(siteCategory.Content + "[" + siteCategory.CategoryID + "]");
					//else
						strCaption = siteCategory.Content + "[" + siteCategory.CategoryID + "]";

					Categories.AddCategoryNode(rootNode, siteCategory);
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show("Error: " + ex.Message, "ArticleSA");
			}
			
			tvCategories.Nodes[0].Expand();
		}
	
		public void RefreshArticles()
		{
			lvArticles.Items.Clear();

			ItemData item = (ItemData)cboSite.SelectedItem;
			//			bool blnHebrew = _settings.IsHebrewTranslation(item.Value);	// TODO: ADD HEBREW SUPPORT.  Or is is already supported as is?

			SiteCategoryNode node = (SiteCategoryNode)tvCategories.SelectedNode;
			if (node != null && node.CategoryID != int.MinValue)
			{
				try
				{
					SiteArticleCollection siteArticles = ArticleSA.Instance.RetrieveCategorySiteArticlesAndArticles(node.CategoryID, item.Value, true);

					ListViewItem lvItem;
					foreach (Matchnet.Content.ValueObjects.Article.SiteArticle siteArticle in siteArticles)
					{
						lvItem = new ListViewItem();

						// If a value doesn not exist for the SiteArticleID then make it regular.
						// KNOWN ISSUE:  Clipping occurs if we make the default font for the tree as regular and then try
						// to bold individual nodes.  Doing this in reverse avoids this problem.
						if (siteArticle.SiteArticleID == int.MinValue)
							lvItem.Font = new Font("Microsoft Sans Serif", (float)8.25, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
						// If the Article is not published, gray it out.
						if (siteArticle.PublishedFlag == false)
							lvItem.ForeColor = System.Drawing.Color.Gray;

						lvItem.Text = siteArticle.SiteArticleID.ToString();
						lvItem.SubItems.Add(siteArticle.ArticleID.ToString());
						lvItem.SubItems.Add(siteArticle.SiteID.ToString());
						lvItem.SubItems.Add(siteArticle.PublishedFlag.ToString());
						lvItem.SubItems.Add(siteArticle.ArticleData.Constant);
						lvItem.SubItems.Add(siteArticle.MemberID != int.MinValue ? siteArticle.MemberID.ToString() : _settings.MemberID.ToString());
						lvItem.SubItems.Add(siteArticle.Title);
						lvItem.SubItems.Add(siteArticle.Content);
						lvItem.SubItems.Add(siteArticle.FileID.ToString());
						lvItem.SubItems.Add(siteArticle.Ordinal != int.MinValue ? siteArticle.Ordinal.ToString() : "0");
						lvItem.SubItems.Add(siteArticle.LastUpdated.ToString());
						lvItem.SubItems.Add(siteArticle.FeaturedFlag.ToString());

//							if (blnHebrew)
//							{
//								lvItem.SubItems.Add(_settings.HebrewToUTF(dr.GetValue(4).ToString()));
//								lvItem.SubItems.Add(_settings.HebrewToUTF(dr.GetValue(5).ToString()));
//							}
//							else
//							{
//								lvItem.SubItems.Add(dr.GetValue(4).ToString());
//								lvItem.SubItems.Add(dr.GetValue(5).ToString());
//							}
						lvArticles.Items.Add(lvItem);
					}
				}
				catch(Exception ex)
				{
					MessageBox.Show("Error: " + ex.Message, "ArticleSA");
				}
			
				tvCategories.Nodes[0].Expand();
			}
		}

		public void Add()
		{
			SiteCategoryNode node = (SiteCategoryNode)tvCategories.SelectedNode;
			if (node != null && node.CategoryID != int.MinValue)
			{
				Article frm = new Article(node.CategoryID, true);
				frm.settings = _settings;

				if (frm.ShowDialog() == DialogResult.OK)
				{
					Reload();
				}
			}
			else
			{
				MessageBox.Show("Please select the Category from the Categories Tree View to which you would like to add an Article.", "Category Not Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
	
		public void Edit()
		{
			try
			{
				ListViewItem lvItem = lvArticles.SelectedItems[0];

				if (lvItem != null)
				{
					SiteArticle frm = new SiteArticle(new Matchnet.Content.ValueObjects.Article.SiteArticle(
						Convert.ToInt32(lvItem.Text),
						Convert.ToInt32(lvItem.SubItems[1].Text),
						Convert.ToInt32(lvItem.SubItems[2].Text),
						lvItem.SubItems[4].Text,
						Convert.ToInt32(lvItem.SubItems[5].Text),
						Convert.ToInt32(lvItem.SubItems[9].Text),
						Convert.ToDateTime(lvItem.SubItems[10].Text),
						Convert.ToBoolean(lvItem.SubItems[3].Text),
						Convert.ToBoolean(lvItem.SubItems[11].Text),
						Convert.ToInt32(lvItem.SubItems[8].Text),
						lvItem.SubItems[6].Text,
						lvItem.SubItems[7].Text));

					frm.settings = _settings;
					if (frm.ShowDialog() == DialogResult.OK)
					{
						RefreshArticles();
					}
				}
				else
				{
					MessageBox.Show("Please select the SiteArticle from the SiteArticles list that you would like to edit.", "SiteArticle Not Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception ex)
			{
				string error = ex.Message;
				MessageBox.Show("Please select the SiteArticle from the SiteArticles list that you would like to edit.", "SiteArticle Not Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void lnkRefreshArt_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			Reload();
		}

		private void lnkAddArticle_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			Add();
		}

		private void lnkEditArt_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			Edit();
		}

		private void tvCategories_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			RefreshArticles();
		}

		private void lvArticles_DoubleClick(object sender, System.EventArgs e)
		{
			Edit();
		}

		private void lvArticles_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)13 || e.KeyChar == (char)32)
			{
				Edit();
			}
		}

		private void cboSite_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Reload();
		}

		private void mnuAdd_Click(object sender, System.EventArgs e)
		{
			Add();
		}

		private void mnuEdit_Click(object sender, System.EventArgs e)
		{
			Edit();
		}

		private void mnuRefresh_Click(object sender, System.EventArgs e)
		{
			Reload();
		}
	}
}
