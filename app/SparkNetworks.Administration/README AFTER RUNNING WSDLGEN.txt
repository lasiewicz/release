You MUST replace the Member, Privilege, and SecurityGroup class definitions in the generated ClientAPI.cs 
file with the following class definitions.  The WSDL.exe generator does not appear to be smart enough to generate
properties (instead, it relies on public fields) but the ListBox.DisplayMember property (which is used heavily
by the Privilege Editor) requires Properties.  Simply open ClientAPI.cs and cut and replace the class definitions.

	[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://spark.net/SparkNetworks.Administration/")]
    public class Member {
        
        /// <remarks/>
        public int MemberID;
        
        /// <remarks/>
        public string Username;
        
        /// <remarks/>
        public string EmailAddress;

		public Int32 MemberIDVal
		{
			get
			{
				return MemberID;
			}
		}

		public String UsernameVal
		{
			get
			{
				return Username;
			}
		}

		public String EmailAddressVal
		{
			get
			{
				return EmailAddress;
			}
		}
    }
    
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://spark.net/SparkNetworks.Administration/")]
    public class Privilege {
        
        /// <remarks/>
        public int PrivilegeID;
        
        /// <remarks/>
        public string Description;

		public Int32 PrivilegeIDVal
		{
			get
			{
				return PrivilegeID;
			}
		}

		public String DescriptionVal
		{
			get
			{
				return Description;
			}
		}
    }
    
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://spark.net/SparkNetworks.Administration/")]
    public class SecurityGroup {
        
        /// <remarks/>
        public int SecurityGroupID;
        
        /// <remarks/>
        public string Description;

		public Int32 SecurityGroupIDVal
		{
			get
			{
				return SecurityGroupID;
			}
		}

		public String DescriptionVal
		{
			get
			{
				return Description;
			}
		}
    }