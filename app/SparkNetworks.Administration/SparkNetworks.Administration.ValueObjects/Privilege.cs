using System;

namespace SparkNetworks.Administration.ValueObjects
{
	/// <summary>
	/// This is necessary because Matchnet.Content.ValueObjects.BrandConfig.Privilege has a property that is of type
	/// System.Globalization.CultureInfo and that type does not contain a default contructor.  Default constructors
	/// are necessary for wsdl.exe to work.
	/// </summary>
	[Serializable]
	public class Privilege
	{
		public Int32 PrivilegeID;
		public String Description;
		
		public Privilege()
		{
		}

		public Privilege(Int32 privilegeID, String description)
		{
			PrivilegeID = privilegeID;
			Description = description;
		}

		public Int32 PrivilegeIDVal
		{
			get
			{
				return PrivilegeID;
			}
		}

		public String DescriptionVal
		{
			get
			{
				return Description;
			}
		}
	}
}
