using System;

namespace SparkNetworks.Administration.ValueObjects
{
	/// <summary>
	/// This is necessary because Matchnet.Content.ValueObjects.BrandConfig.Site has a property that is of type
	/// System.Globalization.CultureInfo and that type does not contain a default contructor.  Default constructors
	/// are necessary for wsdl.exe to work.
	/// </summary>
	[Serializable]
	public class Site
	{
		public Int32 SiteID;
		public String Name;

		public Site()
		{
		}

		public Site(Int32 siteID,
			String name)
		{
			SiteID = siteID;
			Name = name;
		}
	}
}
