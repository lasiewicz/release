using System;

namespace SparkNetworks.Administration.ValueObjects
{
	[Serializable]
	public enum PrivilegeState
	{
		/// <summary>
		/// 
		/// </summary>
		Denied = 0,		//Member does not have this privilege
		/// <summary>
		/// 
		/// </summary>
		SecurityGroup = -1,		//Member has this privilege through their membership in a security group
		/// <summary>
		/// 
		/// </summary>
		Individual = 1	//Member has this privilege assigned to them individually
	}
}
