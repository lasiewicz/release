using System;

namespace SparkNetworks.Administration.ValueObjects
{
	/// <summary>
	/// This is necessary because Matchnet.Content.ValueObjects.BrandConfig.PagePixel has a property that is of type
	/// System.Globalization.CultureInfo and that type does not contain a default contructor.  Default constructors
	/// are necessary for wsdl.exe to work.
	/// </summary>
	[Serializable]
	public class PagePixel
	{
		public Int32 PagePixelID;
		public Int32 PageID;
		public Int32 SiteID;
		public String Content;
		public String ContentCondition;
		public String ContentConditionArgs;
		public DateTime UpdateDate;
		public Boolean ImgFlag = true;
		public String Description;
		public DateTime AddDate;
		public DateTime EndDate;
		public Object ActiveFlag;
		public Boolean PublishFlag;
		public Object ApprovalFlag;
		public Int32 ApproverMemberID;
		public DateTime ApprovedDate;
		public Int32 PublishID;

		public PagePixel()
		{
		}

		public PagePixel(Int32 pagePixelID, Int32 pageID, Int32 siteID, String content, String contentCondition, String contentConditionArgs, DateTime updateDate,
			Boolean imgFlag, String description, DateTime addDate, DateTime endDate, Boolean activeFlag, Boolean publishFlag, Boolean approvalFlag, Int32 approverMemberID,
			DateTime approvedDate, Int32 publishID)
		{
			PagePixelID = pagePixelID;
			PageID = pageID;
			SiteID = siteID;
			Content = content;
			ContentCondition = contentCondition;
			ContentConditionArgs = contentConditionArgs;
			UpdateDate = updateDate;
			ImgFlag = imgFlag;
			Description = description;
			AddDate = addDate;
			EndDate = endDate;
			ActiveFlag = activeFlag;
			PublishFlag = publishFlag;
			ApprovalFlag = approvalFlag;
			ApproverMemberID = approverMemberID;
			ApprovedDate = approvedDate;
			PublishID = publishID;
		}

		public PagePixel(Int32 pagePixelID, Int32 pageID, Int32 siteID, String content, String contentCondition, String contentConditionArgs, DateTime updateDate,
			Boolean imgFlag, String description, DateTime addDate, DateTime endDate, Boolean activeFlag, Boolean publishFlag, Boolean approvalFlag, Int32 approverMemberID,
			DateTime approvedDate)
		{
			PagePixelID = pagePixelID;
			PageID = pageID;
			SiteID = siteID;
			Content = content;
			ContentCondition = contentCondition;
			ContentConditionArgs = contentConditionArgs;
			UpdateDate = updateDate;
			ImgFlag = imgFlag;
			Description = description;
			AddDate = addDate;
			EndDate = endDate;
			ActiveFlag = activeFlag;
			PublishFlag = publishFlag;
			ApprovalFlag = approvalFlag;
			ApproverMemberID = approverMemberID;
			ApprovedDate = approvedDate;
		}
	}
}
