using System;

namespace SparkNetworks.Administration.ValueObjects
{
	/// <summary>
	/// This is necessary because Matchnet.Content.ValueObjects.BrandConfig.SiteArticle has a property that is of type
	/// System.Globalization.CultureInfo and that type does not contain a default contructor.  Default constructors
	/// are necessary for wsdl.exe to work.
	/// </summary>
	[Serializable]
	public class SiteArticle
	{
		public Int32 SiteArticleID = Int32.MinValue;
		public Int32 SiteID;
		public Int32 MemberID;
		public Int32 Ordinal;
		public DateTime LastUpdated;
		public Boolean PublishedFlag;
		public Boolean FeaturedFlag;
		public Int32 FileID;
		public String Title;
		public String Content;
		public Int32 ArticleID;
		public Article Article = new Article();

		public SiteArticle()
		{
		}

		public SiteArticle(Int32 siteArticleID, Int32 articleID, Int32 siteID, Int32 memberID, Int32 ordinal, Boolean publishedFlag, Boolean featuredFlag, Int32 fileID, String title, String content)
		{
			SiteArticleID = siteArticleID;
			ArticleID = articleID;
			SiteID = siteID;
			MemberID = memberID;
			Ordinal = ordinal;
			PublishedFlag = publishedFlag;
			FeaturedFlag = featuredFlag;
			FileID = fileID;
			Title = title;
			Content = content;
		}

		public SiteArticle(Int32 siteArticleID, Int32 articleID, Int32 siteID, String constant, Int32 memberID, Int32 ordinal, DateTime lastUpdated, Boolean publishedFlag, Boolean featuredFlag, Int32 fileID, String title, String content)
		{
			SiteArticleID = siteArticleID;
			ArticleID = articleID;
			SiteID = siteID;
			Article.Constant = constant;
			MemberID = memberID;
			Ordinal = ordinal;
			LastUpdated = lastUpdated;
			PublishedFlag = publishedFlag;
			FeaturedFlag = featuredFlag;
			FileID = fileID;
			Title = title;
			Content = content;
		}
	}
}
