using System;

namespace SparkNetworks.Administration.ValueObjects
{
	/// <summary>
	/// This is necessary because Matchnet.Content.ValueObjects.BrandConfig.MemberPrivilege has a property that is of type
	/// System.Globalization.CultureInfo and that type does not contain a default contructor.  Default constructors
	/// are necessary for wsdl.exe to work.
	/// </summary>
	[Serializable]
	public class MemberPrivilege
	{
		public Privilege Privilege;
		public PrivilegeState PrivilegeState;
		
		public MemberPrivilege()
		{
		}

		public MemberPrivilege(Privilege privilege, PrivilegeState privilegeState)
		{
			Privilege = privilege;
			PrivilegeState = privilegeState;
		}
	}
}
