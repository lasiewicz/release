using System;

namespace SparkNetworks.Administration.ValueObjects
{
	/// <summary>
	/// This is necessary because Matchnet.Content.ValueObjects.BrandConfig.Article has a property that is of type
	/// System.Globalization.CultureInfo and that type does not contain a default contructor.  Default constructors
	/// are necessary for wsdl.exe to work.
	/// </summary>
	[Serializable]
	public class Article
	{
		public Int32 ArticleID = Int32.MinValue; 
		public Int32 CategoryID;
		public String Constant;
		public String DefaultContent;
		public DateTime LastUpdated;
		
		public Article()
		{
		}

		public Article(Int32 articleID, Int32 categoryID, String defaultContent, String constant, DateTime lastUpdated)
		{
			ArticleID = articleID;
			CategoryID = categoryID;
			DefaultContent = defaultContent;
			Constant = constant;
			LastUpdated = lastUpdated;
		}
	}
}
