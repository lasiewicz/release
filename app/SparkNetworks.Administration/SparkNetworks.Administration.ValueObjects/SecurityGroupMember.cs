using System;

namespace SparkNetworks.Administration.ValueObjects
{
	/// <summary>
	/// This is necessary because Matchnet.Content.ValueObjects.BrandConfig.SecurityGroupMember has a property that is of type
	/// System.Globalization.CultureInfo and that type does not contain a default contructor.  Default constructors
	/// are necessary for wsdl.exe to work.
	/// </summary>
	[Serializable]
	public class SecurityGroupMember
	{
		public Int32 MemberID;
		public Int32 SecurityGroupID;
		
		public SecurityGroupMember()
		{
		}

		public SecurityGroupMember(Int32 memberID, Int32 securityGroupID)
		{
			MemberID = memberID;
			SecurityGroupID = securityGroupID;
		}
	}
}
