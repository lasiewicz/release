using System;

namespace SparkNetworks.Administration.ValueObjects
{
	/// <summary>
	/// This is necessary because Matchnet.Content.ValueObjects.BrandConfig.SecurityGroup has a property that is of type
	/// System.Globalization.CultureInfo and that type does not contain a default contructor.  Default constructors
	/// are necessary for wsdl.exe to work.
	/// </summary>
	[Serializable]
	public class SecurityGroup
	{
		public Int32 SecurityGroupID;
		public String Description;
		
		public SecurityGroup()
		{
		}

		public SecurityGroup(Int32 securityGroupID, String description)
		{
			SecurityGroupID = securityGroupID;
			Description = description;
		}

		public Int32 SecurityGroupIDVal
		{
			get
			{
				return SecurityGroupID;
			}
		}

		public String DescriptionVal
		{
			get
			{
				return Description;
			}
		}
	}
}
