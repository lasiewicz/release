using System;

namespace SparkNetworks.Administration.ValueObjects
{
	[Serializable]
	public enum AdminDisplayTypeMaskEnum
	{
		/// <summary>
		/// 
		/// </summary>
		NormalUser = 0,
		/// <summary>
		/// 
		/// </summary>
		Administrator = 1,
		/// <summary>
		/// 
		/// </summary>
		Approver = 2,
		/// <summary>
		/// 
		/// </summary>
		AdministratorAndApprover = 3
	}
}
