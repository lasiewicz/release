using System;

namespace SparkNetworks.Administration.ValueObjects
{
	[Serializable]
	public enum PublicActionTypeEnum
	{
		/// <summary>
		/// 
		/// </summary>
		AddInDev = 1,
		/// <summary>
		/// 
		/// </summary>
		EditInDev = 2,
		/// <summary>
		/// 
		/// </summary>
		DeleteInDev = 3,
		/// <summary>
		/// 
		/// </summary>
		VerifyInDev = 4,
		ApproveInDev = 5,
		/// <summary>
		/// 
		/// </summary>
		VerifyInContentStg = 6,
		/// <summary>
		/// 
		/// </summary>
		PublishToContentStg = 7,
		/// <summary>
		/// 
		/// </summary>
		VerifyWithPartnerInContentStg = 8,
		/// <summary>
		/// 
		/// </summary>
		PublishToProd = 9,
		/// <summary>
		/// 
		/// </summary>
		VerifyInProd = 10,
		/// <summary>
		/// 
		/// </summary>
		VerifyWithPartnerInProd = 11
	}
}
