using System;

namespace SparkNetworks.Administration.ValueObjects
{
	/// <summary>
	/// This is necessary because Matchnet.Content.ValueObjects.BrandConfig.Member has a property that is of type
	/// System.Globalization.CultureInfo and that type does not contain a default contructor.  Default constructors
	/// are necessary for wsdl.exe to work.
	/// </summary>
	[Serializable]
	public class Member
	{
		public Int32 MemberID = Int32.MinValue;
		public String Username = String.Empty;
		public String EmailAddress = String.Empty;
		
		public Member()
		{
		}

		public Member(Int32 memberID, String username, String emailAddress)
		{
			MemberID = memberID;
			Username = username;
			EmailAddress = emailAddress;
		}

		public Int32 MemberIDVal
		{
			get
			{
				return MemberID;
			}
		}

		public String UsernameVal
		{
			get
			{
				return Username;
			}
		}

		public String EmailAddressVal
		{
			get
			{
				return EmailAddress;
			}
		}
	}
}
