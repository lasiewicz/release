using System;

namespace SparkNetworks.Administration.ValueObjects
{
	/// <summary>
	/// This is necessary because Matchnet.Content.ValueObjects.BrandConfig.SiteCategory has a property that is of type
	/// System.Globalization.CultureInfo and that type does not contain a default contructor.  Default constructors
	/// are necessary for wsdl.exe to work.
	/// </summary>
	[Serializable]
	public class SiteCategory
	{
		public Int32 SiteCategoryID = Int32.MinValue;
		public Int32 SiteID = Int32.MinValue;
		public Int32 CategoryID;
		public String Content;
		public DateTime LastUpdated;
		public Boolean PublishedFlag;
		public Category Category = new Category();

		public SiteCategory()
		{
		}

		public SiteCategory(Int32 siteCategoryID, Int32 categoryID, Int32 siteID, Int32 parentCategoryID, String content, String constant, Boolean publishedFlag, DateTime lastUpdated)
		{
			SiteCategoryID = siteCategoryID;
			CategoryID = categoryID;
			SiteID = siteID;
			Content = content;
			LastUpdated = lastUpdated;
			Category.ParentCategoryID = parentCategoryID;
			Category.Constant = constant;
			PublishedFlag = publishedFlag;
		}
	}
}
