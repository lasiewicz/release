using System;

namespace SparkNetworks.Administration.ValueObjects
{
	/// <summary>
	/// This is necessary because Matchnet.Content.ValueObjects.BrandConfig.PriviligeType has a property that is of type
	/// System.Globalization.CultureInfo and that type does not contain a default contructor.  Default constructors
	/// are necessary for wsdl.exe to work.
	/// </summary>
	[Serializable]
	public enum PrivilegeType 
	{
		/// <summary></summary>
		PRIVILEGE_ADD_EDIT_DELETE_PRIVILEGES = 1,
		/// <summary></summary>
		PRIVILEGE_GRANT_REVOKE_PRIVILEGES_TO_MEMBERS = 2,
		/// <summary></summary>
		PRIVILEGE_GRANT_REVOKE_PRIVILEGES_TO_GROUPS = 6,
		/// <summary></summary>
		PRIVILEGE_CHANGE_EMAIL = 10,
		/// <summary></summary>
		PRIVILEGE_VIEW_PASSWORD = 12,
		/// <summary></summary>
		PRIVILEGE_ADD_EDIT_DELETE_GROUPS = 13,
		/// <summary></summary>
		PRIVILEGE_VIEW_LIST_OF_GROUPS = 14,
		/// <summary></summary>
		PRIVILEGE_VIEW_CREDITCARD = 15,
		/// <summary></summary>
		PRIVILEGE_MEMBER_PHOTO_UPLOAD_ANOTHER_MEMBER = 24,
		/// <summary></summary>
		PRIVILEGE_APPROVAL_APPLICATION = 25,
		/// <summary></summary>
		PRIVILEGE_MEMBER_SEARCH = 32,
		/// <summary></summary>
		PRIVILEGE_CACHE_ADMIN = 34,
		/// <summary></summary>
		PRIVILEGE_ARTICLE_ADMIN = 36,
		/// <summary></summary>
		PRIVILEGE_PURCHASE_PACKAGE_FOR_ANOTHER_MEMBER = 40,
		/// <summary></summary>
		PRIVILEGE_DISCOUNTED_PACKAGES = 42,
		/// <summary></summary>
		PRIVILEGE_CLOSED_PACKAGES = 44,
		/// <summary></summary>
		PRIVILEGE_DOMAIN_LIST_ADMINISTRATION = 46,
		/// <summary></summary>
		PRIVILEGE_ADD_EDIT_DELETE_ATTRIBUTES = 48,
		/// <summary></summary>
		PRIVILEGE_ADD_EDIT_DELETE_RESOURCES = 50,
		/// <summary></summary>
		PRIVILEGE_PROMOTION_ADMIN = 52,
		/// <summary></summary>
		PRIVILEGE_EDIT_MEMBER_ATTRIBUTES = 54,
		/// <summary></summary>
		PRIVILEGE_ADD_EDIT_DELETE_MEMBERS_FROM_A_GROUP = 58,
		/// <summary></summary>
		PRIVILEGE_REPORTS = 62,
		/// <summary></summary>
		PRIVILEGE_VIEW_SITE_STATUS_PAGE = 74,
		/// <summary></summary>
		PRIVILEGE_EDIT_ADMIN_MEMBER_ATTRIBUTES = 76,
		/// <summary></summary>
		PRIVILEGE_DOMAIN_ALIASES_PARTIAL_ADMIN = 78,
		/// <summary></summary>
		PRIVILEGE_DOMAIN_ALIASES_DATA_ENTRY = 80,
		/// <summary></summary>
		PRIVILEGE_ADMINISTER_POLLS = 82,
		/// <summary></summary>
		PRIVILEGE_CATEGORIES = 84,
		/// <summary></summary>
		PRIVILEGE_VERIFY_EMAIL_ADDRESSES = 86,
		/// <summary></summary>
		PRIVILEGE_DOMAIN_ALIASES_SUPER_ADMIN = 90,
		/// <summary></summary>
		PRIVILEGE_EVENTS_ADMIN = 92,
		/// <summary></summary>
		PRIVILEGE_ADMINISTER_PAGETEXT = 96,
		/// <summary></summary>
		PRIVILEGE_CREDIT_CARD_UPDATE = 98,
		/// <summary></summary>
		PRIVILEGE_DEPLOYMENT_IMAGE_VIEW = 104,
		/// <summary></summary>
		PRIVILEGE_DEPLOYMENT_IMAGE_PUBLISH = 106,
		/// <summary></summary>
		PRIVILEGE_DEPLOYMENT_CONTENT_VIEW = 108,
		/// <summary></summary>
		PRIVILEGE_DEPLOYMENT_CONTENT_PUBLISH = 110,
		/// <summary></summary>
		PRIVILEGE_DEPLOYMENT_CACHE_CLEAR = 112,
		/// <summary></summary>
		PRIVILEGE_DEPLOYMENT_WEB_VIEW = 114,
		/// <summary></summary>
		PRIVILEGE_DEPLOYMENT_WEB_PUBLISH = 116,
		/// <summary></summary>
		PRIVILEGE_CHARGE_LOG_PERFORM_CREDIT = 118,
		/// <summary></summary>
		PRIVILEGE_DEPLOYMENT_DEPLOY_IMAGES = 120,
		/// <summary></summary>
		PRIVILEGE_DEPLOYMENT_DEPLOY_WEB = 122,
		/// <summary></summary>
		PRIVILEGE_CSR_BBS_POST_EDIT = 123,
		/// <summary></summary>
		PRIVILEGE_OVERRIDE_PASSWORD = 135
	}
}
