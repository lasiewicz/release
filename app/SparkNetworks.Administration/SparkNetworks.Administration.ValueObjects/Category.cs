using System;

namespace SparkNetworks.Administration.ValueObjects
{
	/// <summary>
	/// This is necessary because Matchnet.Content.ValueObjects.BrandConfig.Category has a property that is of type
	/// System.Globalization.CultureInfo and that type does not contain a default contructor.  Default constructors
	/// are necessary for wsdl.exe to work.
	/// </summary>
	[Serializable]
	public class Category
	{
		public Int32 CategoryID = Int32.MinValue;
		public Int32 ParentCategoryID = Int32.MinValue;
		public Int32 ListOrder;
		public String Constant;
		public DateTime LastUpdated;
		
		public Category()
		{
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="categoryID"></param>
		/// <param name="parentCategoryID"></param>
		/// <param name="listOrder"></param>
		/// <param name="constant"></param>
		/// <param name="lastUpdated"></param>
		public Category(Int32 categoryID, Int32 parentCategoryID, Int32 listOrder, String constant, DateTime lastUpdated)
		{
			CategoryID = categoryID;
			ParentCategoryID = parentCategoryID;
			ListOrder = listOrder;
			Constant = constant;
			LastUpdated = lastUpdated;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="categoryID"></param>
		/// <param name="parentCategoryID"></param>
		/// <param name="listOrder"></param>
		/// <param name="constant"></param>
		public Category(Int32 categoryID, Int32 parentCategoryID, Int32 listOrder, String constant)
		{
			CategoryID = categoryID;
			ParentCategoryID = parentCategoryID;
			ListOrder = listOrder;
			Constant = constant;
		}
	}
}
