using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Xml;
using System.Web;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for Pixel.
	/// </summary>
	public class Pixel : System.Windows.Forms.Form
	{
		private System.ComponentModel.Container components = null;
		private Settings settings;
		private Web.PagePixel pixel;
		private Int32 nextPublishActionPublishObjectID = Matchnet.Constants.NULL_INT;
		private String nextEnvironmentType = Matchnet.Constants.NULL_STRING;
		private String nextStepText = String.Empty;
		private DateTime addDate;
		private Boolean activeFlag;
		private Boolean approvalFlag;
		private DateTime updateDate;

		private System.Windows.Forms.GroupBox gbxButtons;
		private System.Windows.Forms.Label lblPixelID;
		private System.Windows.Forms.TextBox tbxPixelID;
		private System.Windows.Forms.RadioButton rbLive;
		private System.Windows.Forms.RadioButton rbOnHold;
		private System.Windows.Forms.TextBox tbxPageID;
		private System.Windows.Forms.Label lblPageID;
		private System.Windows.Forms.ComboBox cboSite;
		private System.Windows.Forms.Label lblEndDate;
		private System.Windows.Forms.RadioButton rbText;
		private System.Windows.Forms.RadioButton rbImage;
		private System.Windows.Forms.TextBox tbxDescription;
		private System.Windows.Forms.Label lblDescription;
		private System.Windows.Forms.Label lblContent;
		private System.Windows.Forms.ListBox lbxFilters;
		private System.Windows.Forms.Button btnAddFilter;
		private System.Windows.Forms.Button btnDeleteFilter;
		private System.Windows.Forms.Button btnDeleteParameter;
		private System.Windows.Forms.Button btnAddParameter;
		private System.Windows.Forms.Label lblParameters;
		private System.Windows.Forms.ListBox lbxParameters;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Label lblFilters;
		private System.Windows.Forms.GroupBox gbxStatus;
		private System.Windows.Forms.DateTimePicker dtpEndDate;
		private System.Windows.Forms.RichTextBox rtbxContent;
		private System.Windows.Forms.Button btnEditFilter;
		private System.Windows.Forms.Button btnEditParameter;
		private System.Windows.Forms.Label lblNoEndDate;
		private System.Windows.Forms.Button btnDelete;
		private System.Windows.Forms.Button btnPreview;
		private System.Windows.Forms.Button btnTest;
		private System.Windows.Forms.GroupBox gbxType;
		private System.Windows.Forms.TextBox PublishIDTextBox;
		private System.Windows.Forms.Label PublishIDLabel;
		private System.Windows.Forms.Button btnNextStep;
		private System.Windows.Forms.Label lblSite;

		public Pixel(Settings settings, Web.PagePixel pixel)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			// Settings.
			this.settings = settings;
			ItemData.SetComboBoxValues(cboSite, settings.Sites);

			lbxFilters.DisplayMember = "DisplayName";
			lbxParameters.DisplayMember = "DisplayName";

			this.pixel = pixel;

			// Display/Hide btnSave and btnDelete.
			if (settings.User.IsPixelEditor)
			{
				btnSave.Visible = true;
				btnDelete.Visible = true;
			}
			else
			{
				btnSave.Visible = false;
				btnDelete.Visible = false;
			}

			// If this is a new pixel, disable btnDelete.
			if (pixel.PagePixelID <= 0)
				btnDelete.Enabled = false;
			
			// Get information about the next Publish step.
			settings.ClientAPI.GetNextPublishActionPublishObjectStep(pixel.PublishID, out nextPublishActionPublishObjectID, out nextEnvironmentType);

			// Display/Hide btnNextStep.
			btnNextStep.Visible = false;
			if (pixel.PagePixelID > 0 && nextPublishActionPublishObjectID != Matchnet.Constants.NULL_INT)
			{
				switch (nextPublishActionPublishObjectID)
				{
					case Constants.PUBACTPUBOBJID_PIXEL_APPROVEINCONTENTSTG:
						if (nextEnvironmentType == settings.EnvironmentType
							&& settings.User.IsPixelApprover
							&& pixel.ApprovalFlag != null
							&& !(Boolean)pixel.ApprovalFlag)
						{
							btnNextStep.Visible = true;
							nextStepText = Constants.PUBACTPUBOBJID_PIXEL_APPROVEINCONTENTSTG_TEXT;
						}

						break;

					case Constants.PUBACTPUBOBJID_PIXEL_PUBLISHTOCONTENTSTG:
						if (nextEnvironmentType == settings.EnvironmentType)	// Even "NON-LIVE" Pixels can be published to ContentStg.  Anyone with any type of Pixel authorization can publish to ContentStg.
						{
							btnNextStep.Visible = true;
							nextStepText = Constants.PUBACTPUBOBJID_PIXEL_PUBLISHTOCONTENTSTG_TEXT;
						}

						break;

					case Constants.PUBACTPUBOBJID_PIXEL_PUBLISHTOPROD:
						if (nextEnvironmentType == settings.EnvironmentType
							&& pixel.PublishFlag == true
							&& settings.User.IsPixelPublisher)
						{
							btnNextStep.Visible = true;
							nextStepText = Constants.PUBACTPUBOBJID_PIXEL_PUBLISHTOPROD_TEXT;
						}

						break;

					case Constants.PUBACTPUBOBJID_PIXEL_VERIFYINCONTENTSTG:
						if (nextEnvironmentType == settings.EnvironmentType)
						{
							btnNextStep.Visible = true;
							nextStepText = Constants.PUBACTPUBOBJID_PIXEL_VERIFYINCONTENTSTG_TEXT;
						}

						break;

					default:
						btnNextStep.Visible = false;
						
						break;
				}

				btnNextStep.Text = nextStepText;
			}

			rbText_CheckedChanged(null, null);
		}

		public Pixel(Settings settings, Web.PagePixel pixel, Int32 siteID, Int32 pageID) : this(settings, pixel)
		{
			// siteID and pageID should be the same as from Pixels page.
			ItemData.SelectItem(cboSite, siteID);
			tbxPageID.Text = pageID.ToString();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(Boolean disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.gbxButtons = new System.Windows.Forms.GroupBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.rtbxContent = new System.Windows.Forms.RichTextBox();
			this.tbxPixelID = new System.Windows.Forms.TextBox();
			this.lblPixelID = new System.Windows.Forms.Label();
			this.rbLive = new System.Windows.Forms.RadioButton();
			this.rbOnHold = new System.Windows.Forms.RadioButton();
			this.tbxPageID = new System.Windows.Forms.TextBox();
			this.lblPageID = new System.Windows.Forms.Label();
			this.cboSite = new System.Windows.Forms.ComboBox();
			this.lblSite = new System.Windows.Forms.Label();
			this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
			this.lblEndDate = new System.Windows.Forms.Label();
			this.rbText = new System.Windows.Forms.RadioButton();
			this.rbImage = new System.Windows.Forms.RadioButton();
			this.tbxDescription = new System.Windows.Forms.TextBox();
			this.lblDescription = new System.Windows.Forms.Label();
			this.lblContent = new System.Windows.Forms.Label();
			this.lbxFilters = new System.Windows.Forms.ListBox();
			this.lblFilters = new System.Windows.Forms.Label();
			this.btnAddFilter = new System.Windows.Forms.Button();
			this.btnDeleteFilter = new System.Windows.Forms.Button();
			this.btnDeleteParameter = new System.Windows.Forms.Button();
			this.btnAddParameter = new System.Windows.Forms.Button();
			this.lblParameters = new System.Windows.Forms.Label();
			this.lbxParameters = new System.Windows.Forms.ListBox();
			this.gbxStatus = new System.Windows.Forms.GroupBox();
			this.gbxType = new System.Windows.Forms.GroupBox();
			this.btnEditFilter = new System.Windows.Forms.Button();
			this.btnEditParameter = new System.Windows.Forms.Button();
			this.lblNoEndDate = new System.Windows.Forms.Label();
			this.btnNextStep = new System.Windows.Forms.Button();
			this.btnDelete = new System.Windows.Forms.Button();
			this.btnPreview = new System.Windows.Forms.Button();
			this.btnTest = new System.Windows.Forms.Button();
			this.PublishIDTextBox = new System.Windows.Forms.TextBox();
			this.PublishIDLabel = new System.Windows.Forms.Label();
			this.gbxStatus.SuspendLayout();
			this.gbxType.SuspendLayout();
			this.SuspendLayout();
			// 
			// gbxButtons
			// 
			this.gbxButtons.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.gbxButtons.Location = new System.Drawing.Point(-84, 464);
			this.gbxButtons.Name = "gbxButtons";
			this.gbxButtons.Size = new System.Drawing.Size(832, 4);
			this.gbxButtons.TabIndex = 30;
			this.gbxButtons.TabStop = false;
			this.gbxButtons.Text = "gbxButtons";
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(560, 480);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(81, 28);
			this.btnCancel.TabIndex = 21;
			this.btnCancel.Text = "&Cancel";
			this.btnCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.Location = new System.Drawing.Point(384, 480);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(81, 28);
			this.btnSave.TabIndex = 19;
			this.btnSave.Text = "&Save";
			this.btnSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// rtbxContent
			// 
			this.rtbxContent.Location = new System.Drawing.Point(96, 160);
			this.rtbxContent.MaxLength = 2000;
			this.rtbxContent.Name = "rtbxContent";
			this.rtbxContent.Size = new System.Drawing.Size(544, 136);
			this.rtbxContent.TabIndex = 9;
			this.rtbxContent.Text = "";
			// 
			// tbxPixelID
			// 
			this.tbxPixelID.Location = new System.Drawing.Point(96, 8);
			this.tbxPixelID.Name = "tbxPixelID";
			this.tbxPixelID.ReadOnly = true;
			this.tbxPixelID.Size = new System.Drawing.Size(144, 20);
			this.tbxPixelID.TabIndex = 49;
			this.tbxPixelID.Text = "";
			// 
			// lblPixelID
			// 
			this.lblPixelID.Location = new System.Drawing.Point(32, 8);
			this.lblPixelID.Name = "lblPixelID";
			this.lblPixelID.Size = new System.Drawing.Size(64, 24);
			this.lblPixelID.TabIndex = 48;
			this.lblPixelID.Text = "PixelID:";
			this.lblPixelID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// rbLive
			// 
			this.rbLive.Location = new System.Drawing.Point(24, 16);
			this.rbLive.Name = "rbLive";
			this.rbLive.Size = new System.Drawing.Size(56, 24);
			this.rbLive.TabIndex = 3;
			this.rbLive.Text = "LIVE";
			// 
			// rbOnHold
			// 
			this.rbOnHold.Location = new System.Drawing.Point(88, 16);
			this.rbOnHold.Name = "rbOnHold";
			this.rbOnHold.Size = new System.Drawing.Size(80, 24);
			this.rbOnHold.TabIndex = 4;
			this.rbOnHold.Text = "ON-HOLD";
			// 
			// tbxPageID
			// 
			this.tbxPageID.Location = new System.Drawing.Point(384, 32);
			this.tbxPageID.MaxLength = 6;
			this.tbxPageID.Name = "tbxPageID";
			this.tbxPageID.Size = new System.Drawing.Size(56, 20);
			this.tbxPageID.TabIndex = 2;
			this.tbxPageID.Text = "";
			// 
			// lblPageID
			// 
			this.lblPageID.Location = new System.Drawing.Point(336, 32);
			this.lblPageID.Name = "lblPageID";
			this.lblPageID.Size = new System.Drawing.Size(48, 21);
			this.lblPageID.TabIndex = 66;
			this.lblPageID.Text = "&PageID:";
			this.lblPageID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// cboSite
			// 
			this.cboSite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboSite.Location = new System.Drawing.Point(96, 32);
			this.cboSite.Name = "cboSite";
			this.cboSite.Size = new System.Drawing.Size(240, 21);
			this.cboSite.TabIndex = 1;
			// 
			// lblSite
			// 
			this.lblSite.Location = new System.Drawing.Point(64, 32);
			this.lblSite.Name = "lblSite";
			this.lblSite.Size = new System.Drawing.Size(32, 21);
			this.lblSite.TabIndex = 64;
			this.lblSite.Text = "&Site:";
			this.lblSite.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// dtpEndDate
			// 
			this.dtpEndDate.Location = new System.Drawing.Point(96, 112);
			this.dtpEndDate.MinDate = new System.DateTime(1999, 1, 1, 0, 0, 0, 0);
			this.dtpEndDate.Name = "dtpEndDate";
			this.dtpEndDate.TabIndex = 7;
			this.dtpEndDate.Value = new System.DateTime(1999, 1, 1, 0, 0, 0, 0);
			// 
			// lblEndDate
			// 
			this.lblEndDate.Location = new System.Drawing.Point(32, 112);
			this.lblEndDate.Name = "lblEndDate";
			this.lblEndDate.Size = new System.Drawing.Size(64, 24);
			this.lblEndDate.TabIndex = 69;
			this.lblEndDate.Text = "&End Date";
			this.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// rbText
			// 
			this.rbText.Location = new System.Drawing.Point(104, 16);
			this.rbText.Name = "rbText";
			this.rbText.Size = new System.Drawing.Size(56, 24);
			this.rbText.TabIndex = 6;
			this.rbText.Text = "TEXT";
			this.rbText.CheckedChanged += new System.EventHandler(this.rbText_CheckedChanged);
			// 
			// rbImage
			// 
			this.rbImage.Location = new System.Drawing.Point(24, 16);
			this.rbImage.Name = "rbImage";
			this.rbImage.Size = new System.Drawing.Size(64, 24);
			this.rbImage.TabIndex = 5;
			this.rbImage.Text = "IMAGE";
			// 
			// tbxDescription
			// 
			this.tbxDescription.Location = new System.Drawing.Point(96, 136);
			this.tbxDescription.MaxLength = 200;
			this.tbxDescription.Name = "tbxDescription";
			this.tbxDescription.Size = new System.Drawing.Size(544, 20);
			this.tbxDescription.TabIndex = 8;
			this.tbxDescription.Text = "";
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(24, 136);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(72, 21);
			this.lblDescription.TabIndex = 73;
			this.lblDescription.Text = "&Description:";
			this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblContent
			// 
			this.lblContent.Location = new System.Drawing.Point(24, 160);
			this.lblContent.Name = "lblContent";
			this.lblContent.Size = new System.Drawing.Size(72, 21);
			this.lblContent.TabIndex = 75;
			this.lblContent.Text = "&Content:";
			this.lblContent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lbxFilters
			// 
			this.lbxFilters.Location = new System.Drawing.Point(96, 304);
			this.lbxFilters.Name = "lbxFilters";
			this.lbxFilters.Size = new System.Drawing.Size(448, 69);
			this.lbxFilters.TabIndex = 10;
			// 
			// lblFilters
			// 
			this.lblFilters.Location = new System.Drawing.Point(24, 304);
			this.lblFilters.Name = "lblFilters";
			this.lblFilters.Size = new System.Drawing.Size(72, 21);
			this.lblFilters.TabIndex = 77;
			this.lblFilters.Text = "&Filters:";
			this.lblFilters.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// btnAddFilter
			// 
			this.btnAddFilter.Location = new System.Drawing.Point(552, 328);
			this.btnAddFilter.Name = "btnAddFilter";
			this.btnAddFilter.Size = new System.Drawing.Size(24, 24);
			this.btnAddFilter.TabIndex = 11;
			this.btnAddFilter.Text = "&A";
			this.btnAddFilter.Click += new System.EventHandler(this.btnAddFilter_Click);
			// 
			// btnDeleteFilter
			// 
			this.btnDeleteFilter.Location = new System.Drawing.Point(616, 328);
			this.btnDeleteFilter.Name = "btnDeleteFilter";
			this.btnDeleteFilter.Size = new System.Drawing.Size(24, 24);
			this.btnDeleteFilter.TabIndex = 13;
			this.btnDeleteFilter.Text = "&D";
			this.btnDeleteFilter.Click += new System.EventHandler(this.btnDeleteFilter_Click);
			// 
			// btnDeleteParameter
			// 
			this.btnDeleteParameter.Location = new System.Drawing.Point(616, 400);
			this.btnDeleteParameter.Name = "btnDeleteParameter";
			this.btnDeleteParameter.Size = new System.Drawing.Size(24, 24);
			this.btnDeleteParameter.TabIndex = 17;
			this.btnDeleteParameter.Text = "D";
			this.btnDeleteParameter.Click += new System.EventHandler(this.btnDeleteParameter_Click);
			// 
			// btnAddParameter
			// 
			this.btnAddParameter.Location = new System.Drawing.Point(552, 400);
			this.btnAddParameter.Name = "btnAddParameter";
			this.btnAddParameter.Size = new System.Drawing.Size(24, 24);
			this.btnAddParameter.TabIndex = 15;
			this.btnAddParameter.Text = "A";
			this.btnAddParameter.Click += new System.EventHandler(this.btnAddParameter_Click);
			// 
			// lblParameters
			// 
			this.lblParameters.Location = new System.Drawing.Point(24, 376);
			this.lblParameters.Name = "lblParameters";
			this.lblParameters.Size = new System.Drawing.Size(72, 21);
			this.lblParameters.TabIndex = 81;
			this.lblParameters.Text = "&Parameters:";
			this.lblParameters.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lbxParameters
			// 
			this.lbxParameters.Location = new System.Drawing.Point(96, 376);
			this.lbxParameters.Name = "lbxParameters";
			this.lbxParameters.Size = new System.Drawing.Size(448, 69);
			this.lbxParameters.TabIndex = 14;
			// 
			// gbxStatus
			// 
			this.gbxStatus.Controls.Add(this.rbLive);
			this.gbxStatus.Controls.Add(this.rbOnHold);
			this.gbxStatus.Location = new System.Drawing.Point(96, 56);
			this.gbxStatus.Name = "gbxStatus";
			this.gbxStatus.Size = new System.Drawing.Size(176, 48);
			this.gbxStatus.TabIndex = 84;
			this.gbxStatus.TabStop = false;
			this.gbxStatus.Text = "&Status";
			// 
			// gbxType
			// 
			this.gbxType.Controls.Add(this.rbText);
			this.gbxType.Controls.Add(this.rbImage);
			this.gbxType.Location = new System.Drawing.Point(288, 56);
			this.gbxType.Name = "gbxType";
			this.gbxType.Size = new System.Drawing.Size(176, 48);
			this.gbxType.TabIndex = 85;
			this.gbxType.TabStop = false;
			this.gbxType.Text = "&Type";
			// 
			// btnEditFilter
			// 
			this.btnEditFilter.Location = new System.Drawing.Point(584, 328);
			this.btnEditFilter.Name = "btnEditFilter";
			this.btnEditFilter.Size = new System.Drawing.Size(24, 24);
			this.btnEditFilter.TabIndex = 12;
			this.btnEditFilter.Text = "&E";
			this.btnEditFilter.Click += new System.EventHandler(this.btnEditFilter_Click);
			// 
			// btnEditParameter
			// 
			this.btnEditParameter.Location = new System.Drawing.Point(584, 400);
			this.btnEditParameter.Name = "btnEditParameter";
			this.btnEditParameter.Size = new System.Drawing.Size(24, 24);
			this.btnEditParameter.TabIndex = 16;
			this.btnEditParameter.Text = "E";
			this.btnEditParameter.Click += new System.EventHandler(this.btnEditParameter_Click);
			// 
			// lblNoEndDate
			// 
			this.lblNoEndDate.Location = new System.Drawing.Point(304, 112);
			this.lblNoEndDate.Name = "lblNoEndDate";
			this.lblNoEndDate.Size = new System.Drawing.Size(224, 21);
			this.lblNoEndDate.TabIndex = 88;
			this.lblNoEndDate.Text = "(Select \"January 01, 1999\" for no End Date)";
			this.lblNoEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnNextStep
			// 
			this.btnNextStep.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnNextStep.Location = new System.Drawing.Point(8, 480);
			this.btnNextStep.Name = "btnNextStep";
			this.btnNextStep.Size = new System.Drawing.Size(192, 28);
			this.btnNextStep.TabIndex = 18;
			this.btnNextStep.Text = "&Approve";
			this.btnNextStep.Click += new System.EventHandler(this.btnNextStep_Click);
			// 
			// btnDelete
			// 
			this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnDelete.Location = new System.Drawing.Point(472, 480);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(81, 28);
			this.btnDelete.TabIndex = 20;
			this.btnDelete.Text = "&Delete";
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// btnPreview
			// 
			this.btnPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnPreview.Location = new System.Drawing.Point(296, 480);
			this.btnPreview.Name = "btnPreview";
			this.btnPreview.Size = new System.Drawing.Size(81, 28);
			this.btnPreview.TabIndex = 89;
			this.btnPreview.Text = "&Preview";
			this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
			// 
			// btnTest
			// 
			this.btnTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnTest.Location = new System.Drawing.Point(208, 480);
			this.btnTest.Name = "btnTest";
			this.btnTest.Size = new System.Drawing.Size(81, 28);
			this.btnTest.TabIndex = 90;
			this.btnTest.Text = "&Test JS";
			this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
			// 
			// PublishIDTextBox
			// 
			this.PublishIDTextBox.Location = new System.Drawing.Point(552, 8);
			this.PublishIDTextBox.MaxLength = 6;
			this.PublishIDTextBox.Name = "PublishIDTextBox";
			this.PublishIDTextBox.ReadOnly = true;
			this.PublishIDTextBox.Size = new System.Drawing.Size(88, 20);
			this.PublishIDTextBox.TabIndex = 91;
			this.PublishIDTextBox.Text = "";
			// 
			// PublishIDLabel
			// 
			this.PublishIDLabel.Location = new System.Drawing.Point(496, 8);
			this.PublishIDLabel.Name = "PublishIDLabel";
			this.PublishIDLabel.Size = new System.Drawing.Size(56, 21);
			this.PublishIDLabel.TabIndex = 92;
			this.PublishIDLabel.Text = "&PublishID:";
			this.PublishIDLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// Pixel
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(650, 519);
			this.Controls.Add(this.PublishIDTextBox);
			this.Controls.Add(this.PublishIDLabel);
			this.Controls.Add(this.btnTest);
			this.Controls.Add(this.btnPreview);
			this.Controls.Add(this.btnDelete);
			this.Controls.Add(this.btnNextStep);
			this.Controls.Add(this.lblNoEndDate);
			this.Controls.Add(this.btnEditParameter);
			this.Controls.Add(this.btnEditFilter);
			this.Controls.Add(this.gbxStatus);
			this.Controls.Add(this.btnDeleteParameter);
			this.Controls.Add(this.btnAddParameter);
			this.Controls.Add(this.lblParameters);
			this.Controls.Add(this.lbxParameters);
			this.Controls.Add(this.btnDeleteFilter);
			this.Controls.Add(this.btnAddFilter);
			this.Controls.Add(this.lblFilters);
			this.Controls.Add(this.lbxFilters);
			this.Controls.Add(this.lblContent);
			this.Controls.Add(this.tbxDescription);
			this.Controls.Add(this.tbxPageID);
			this.Controls.Add(this.tbxPixelID);
			this.Controls.Add(this.lblDescription);
			this.Controls.Add(this.lblEndDate);
			this.Controls.Add(this.dtpEndDate);
			this.Controls.Add(this.lblPageID);
			this.Controls.Add(this.cboSite);
			this.Controls.Add(this.lblSite);
			this.Controls.Add(this.rtbxContent);
			this.Controls.Add(this.lblPixelID);
			this.Controls.Add(this.gbxButtons);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.gbxType);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Pixel";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Pixel";
			this.Load += new System.EventHandler(this.Pixel_Load);
			this.gbxStatus.ResumeLayout(false);
			this.gbxType.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			pixel = BuildPagePixel();

			if (ValidatePixel(pixel))
			{
				// Are you sure?
				if (MessageBox.Show(Constants.MSG_SAVE_PIXEL_CONFIRM, Constants.MSGBOXCAPTION_CONFIRM, MessageBoxButtons.YesNo) == DialogResult.No)
					return;
				else
				{
					try
					{
						// Save.
						settings.ClientAPI.SavePagePixel(pixel, settings.MemberID, Matchnet.Constants.NULL_INT);
					}
					catch(Exception ex)
					{
						MessageBox.Show(String.Format(Constants.MSG_CONTACT_ADMINISTRATOR, ex.Message), Constants.MSGBOXCAPTION_SYSTEMERROR);
						return;
					}

					DialogResult = DialogResult.OK;
					this.Close();
				}
			}
		}

		private Boolean ValidatePixel(Web.PagePixel pixel)
		{
			if (pixel.SiteID == Matchnet.Constants.NULL_INT || pixel.PageID == Matchnet.Constants.NULL_INT)
			{
				MessageBox.Show(Constants.MSG_SITE_AND_PAGEID_REQUIRED_ERROR, Constants.MSGBOXCAPTION_WARNING);
				return false;
			}

			if (pixel.EndDate < DateTime.Now && pixel.EndDate != Constants.NO_ENDDATE && pixel.EndDate != DateTime.MinValue)
			{
				MessageBox.Show(Constants.MSG_ENDDATE_IS_PAST_ERROR, Constants.MSGBOXCAPTION_WARNING);
				return false;
			}
			
			if (pixel.Description == String.Empty)
			{
				MessageBox.Show(Constants.MSG_DESCRIPTION_REQUIRED_ERROR, Constants.MSGBOXCAPTION_WARNING);
				return false;
			}

			if (pixel.Content == String.Empty)
			{
				MessageBox.Show(Constants.MSG_CONTENT_REQUIRED_ERROR, Constants.MSGBOXCAPTION_WARNING);
				return false;
			}

			// If this is a 3000 page (Subscription), make sure that it contains no "http:" (only https:).
			if (pixel.PageID >= 3000 && pixel.PageID <= 3999 && pixel.Content.ToLower().IndexOf("http:") != -1)
			{
				MessageBox.Show(Constants.MSG_SUBSCRIPTION_SECURE_LINKS_ONLY_ERROR, Constants.MSGBOXCAPTION_WARNING);
				return false;
			}

			// If this is an IMAGE type, make sure there is no "<img " in the Content.
			if (pixel.ImgFlag && pixel.Content.ToLower().IndexOf("<img ") != -1)
			{
				MessageBox.Show(Constants.MSG_IMAGETYPE_NO_IMGTAG_ERROR, Constants.MSGBOXCAPTION_WARNING);
				return false;
			}

			// If this is an IMAGE type, make sure there is no "<script " in the Content.
			if (pixel.ImgFlag && pixel.Content.ToLower().IndexOf("<script") != -1)
			{
				MessageBox.Show(Constants.MSG_IMAGETYPE_NO_SCRIPTTAG_ERROR, Constants.MSGBOXCAPTION_WARNING);
				return false;
			}

			// If this is a TEXT type, make sure that it begins with "<img " or "<script" or "<iframe".
			if (!pixel.ImgFlag && (pixel.Content.Length <= 7 || pixel.Content.ToLower().Substring(0, 5) != "<img " && pixel.Content.ToLower().Substring(0, 7) != "<script" && pixel.Content.ToLower().Substring(0, 7) != "<iframe"))
			{
				MessageBox.Show(Constants.MSG_TEXTTYPE_IMGTAG_OR_SCRIPTTAG_ERROR, Constants.MSGBOXCAPTION_WARNING);
				return false;
			}

			if (!pixel.ImgFlag && pixel.Content.ToLower().Substring(0, 7) == "<script" && pixel.Content.ToLower().IndexOf("</script>") == -1)
			{
				MessageBox.Show(Constants.MSG_TEXTTYPE_CLOSING_SCRIPTTAG_ERROR, Constants.MSGBOXCAPTION_WARNING);
				return false;
			}

			// If this is a TEXT type, make sure that there is "width="1" height="1" if it contains a "<img " tag.
			if (!pixel.ImgFlag && pixel.Content.ToLower().IndexOf("<img ") != -1)
			{
				if (pixel.Content.ToLower().IndexOf("width=\"1\"") == -1 && pixel.Content.ToLower().IndexOf("width=1") == -1 && pixel.Content.ToLower().IndexOf("vspace=\"0\"") == -1
					|| pixel.Content.ToLower().IndexOf("height=\"1\"") == -1 && pixel.Content.ToLower().IndexOf("height=1") == -1 && pixel.Content.ToLower().IndexOf("hspace=\"0\"") == -1
					|| pixel.Content.ToLower().IndexOf("border=\"0\"") == -1 && pixel.Content.ToLower().IndexOf("border=0") == -1)
				{
					MessageBox.Show(Constants.MSG_TEXTTYPE_WIDTH_HEIGHT_BORDER_ERROR, Constants.MSGBOXCAPTION_WARNING);
					return false;
				}
			}

			// If the pixel contains PARAMETERS, make sure there is a PARAMETER_STRING_START somewhere in the Content.
			if (lbxParameters.Items.Count > 0 && pixel.Content.IndexOf(Constants.PARAMETER_STRING_START) == -1)
			{
				MessageBox.Show(Constants.MSG_PARAMETER_STRING_START_MISSING_ERROR, Constants.MSGBOXCAPTION_WARNING);
				return false;
			}

			if (pixel.ContentCondition != null && pixel.ContentCondition.Length > Constants.MAX_LENGTH_CONTENTCONDITION)
			{
				MessageBox.Show(Constants.MSG_CONTENTCONDITION_MAX_LENGTH_ERROR, Constants.MSGBOXCAPTION_WARNING);
				return false;
			}

			if (pixel.ContentConditionArgs != null && pixel.ContentConditionArgs.Length > Constants.MAX_LENGTH_CONTENTCONDITIONARGS)
			{
				MessageBox.Show(Constants.MSG_CONTENTCONDITIONARGS_MAX_LENGTH_ERROR, Constants.MSGBOXCAPTION_WARNING);
				return false;
			}
			
			return true;
		}

		private Web.PagePixel BuildPagePixel()
		{
			Int32 siteID;
			Int32 pixelID;
			Int32 pageID;

			RetrieveSitePixelPageIDsFromControls(out siteID, out pixelID, out pageID);

			pixel.PagePixelID = pixelID;
			pixel.PageID = pageID;
			pixel.SiteID = siteID;
			pixel.Content = rtbxContent.Text.Trim();
			pixel.ContentCondition = FilterObject.BuildContentConditionXml(lbxFilters, lbxParameters);
			pixel.ContentConditionArgs = ArgumentObject.BuildContentConditionArgsXml(lbxFilters, lbxParameters);
			pixel.ImgFlag = rbImage.Checked;
			pixel.Description = tbxDescription.Text.Trim();
			if (dtpEndDate.Value != Constants.NO_ENDDATE)
				pixel.EndDate = dtpEndDate.Value;
			if (pixel.ActiveFlag == null)
				pixel.ActiveFlag = true;
			pixel.PublishFlag = rbLive.Checked;
			if (pixel.ApprovalFlag == null)
				pixel.ApprovalFlag = false;

			return pixel;
		}

		private void RetrieveSitePixelPageIDsFromControls(out Int32 siteID, out Int32 pixelID, out Int32 pageID)
		{
			ItemData siteItem = (ItemData)cboSite.SelectedItem;
			if (siteItem != null)
				siteID = Convert.ToInt32(((ItemData)(siteItem)).Value);
			else
				siteID = Matchnet.Constants.NULL_INT;

			String pixelIDStr = tbxPixelID.Text.Trim();
			if (pixelIDStr != String.Empty)
				pixelID = Convert.ToInt32(pixelIDStr);
			else
				pixelID = Matchnet.Constants.NULL_INT;

			String pageIDStr = tbxPageID.Text.Trim();
			if (pageIDStr != String.Empty)
				pageID = Convert.ToInt32(pageIDStr);
			else
				pageID = Matchnet.Constants.NULL_INT;
		}

		private void Pixel_Load(object sender, System.EventArgs e)
		{
			DisplayPixel(true);
		}

		private void DisplayPixel(Boolean isFirstLoad)
		{
			if (isFirstLoad)
			{
				if (pixel.PagePixelID != Matchnet.Constants.NULL_INT && pixel.PagePixelID != 0)
					tbxPixelID.Text = pixel.PagePixelID.ToString();

				for (int i = 0; i < cboSite.Items.Count; i++)
				{
					if (Convert.ToInt32(((ItemData)(cboSite.Items[i])).Value) == pixel.SiteID)
					{
						cboSite.SelectedIndex = i;
						break;
					}
				}
				if (pixel.PageID != Matchnet.Constants.NULL_INT && pixel.PageID != 0)
					tbxPageID.Text = pixel.PageID.ToString();
				if (pixel.PublishFlag)
					rbLive.Checked = true;
				else
					rbOnHold.Checked = true;

				if (pixel.ImgFlag)
					rbImage.Checked = true;
				else
					rbText.Checked = true;

				if (pixel.EndDate < dtpEndDate.MinDate)
					dtpEndDate.Value = Constants.NO_ENDDATE;
				tbxDescription.Text = pixel.Description;
				rtbxContent.Text = pixel.Content;
				PublishIDTextBox.Text = pixel.PublishID.ToString();
			}

			// Build filters and parameters.
			if (isFirstLoad && pixel.ContentCondition != Matchnet.Constants.NULL_STRING)
			{
				// Store the contentCondition info in a collection.
				PopulateFilters(pixel.ContentCondition);
				// Store the Paramter info in a collection.
				PopulateParameters(pixel.ContentCondition);
			}
		}

		/// <summary>
		/// This method converts the string xml into an XMLDocument, finds the nodes with name <code>xsl:if</code>
		/// and then parses out all the conditions (filters) from them.
		/// NOTE:  This Pixel Administrator tool assumes that all <code>xsl:if</code> conditions are nested (they do not use "and"
		/// in the "test" attribute).  This tool will also create all contentConditions in this nested manner.
		/// </summary>
		/// <param name="contentCondition"></param>
		private void PopulateFilters(String contentCondition)
		{
			String valueHolder;
			XmlNodeList ifNodeList;
			XmlDocument ccXmlDoc = ArgumentObject.StringToXml(contentCondition);

			ifNodeList = ccXmlDoc.GetElementsByTagName("xsl:if");

			try
			{
				// Add a FilterObject object for each contentCondition.
				foreach (XmlNode ifNode in ifNodeList)
				{
					String testText;
					String testTextHolder;
					Int32 afterLastBrackedIndex;
					String comparisonOperator;
					valueHolder = String.Empty;

					testText = ifNode.Attributes["test"].InnerText;

					// The index value of the last bracket +2 will be useful in parsing.  It will be the position of the Comparison operator.
					afterLastBrackedIndex = testText.LastIndexOf(']') + 2;
	
					// Parse out the comparisonOperator.
					// If this fails it is a "contains."
					try
					{
						comparisonOperator = testText.Substring(afterLastBrackedIndex, testText.IndexOf(' ', afterLastBrackedIndex) - afterLastBrackedIndex);
					}
					catch
					{
						comparisonOperator = "contains";
					}

					// Parse out each of the Values.  There can be more than one (or).
					testTextHolder = testText;
					if (comparisonOperator != "contains")
					{
						do
						{
							if (testTextHolder.IndexOf(" or ") != -1)
							{
								String CurrentTest = String.Empty;

								CurrentTest = testTextHolder.Substring(0, testTextHolder.IndexOf(" or "));

								// The index value of the last bracket +2 will be useful in parsing.  It will be the position of the Comparison operator.
								afterLastBrackedIndex = CurrentTest.LastIndexOf(']') + 2;

								valueHolder += ArgumentObject.ClearQuotes(CurrentTest.Substring(CurrentTest.IndexOf(comparisonOperator, afterLastBrackedIndex) + 2));
					
								valueHolder += "|";

								// Update the testText value.
								testTextHolder = testTextHolder.Substring(testTextHolder.IndexOf(" or ") + 4);
							}
							else
							{
								valueHolder += ArgumentObject.ClearQuotes(testTextHolder.Substring(testTextHolder.IndexOf(comparisonOperator, afterLastBrackedIndex) + comparisonOperator.Length + 1));

								testTextHolder = String.Empty;
							}
						}
						while (testTextHolder.Length > 0);
					}
					else	// Contains.
					{
						do
						{
							if (testTextHolder.IndexOf(" or ") != -1)
							{
								String CurrentTest = String.Empty;

								CurrentTest = testTextHolder.Substring(0, testTextHolder.IndexOf(" or "));

								// The index value of the last bracket +2 will be useful in parsing.  It will be the position of the Comparison operator.
								afterLastBrackedIndex = CurrentTest.LastIndexOf(']') + 2;

								valueHolder += ArgumentObject.ClearQuotes(CurrentTest.Substring(afterLastBrackedIndex, CurrentTest.LastIndexOf(')') - afterLastBrackedIndex));

								valueHolder += "|";

								// Update the testText value.
								testTextHolder = testTextHolder.Substring(testTextHolder.IndexOf(" or ") + 4);
							}
							else
							{
								valueHolder += ArgumentObject.ClearQuotes(testTextHolder.Substring(afterLastBrackedIndex, testTextHolder.LastIndexOf(')') - afterLastBrackedIndex));

								testTextHolder = String.Empty;
							}
						}
						while (testTextHolder.Length > 0);
					}

					if (testText.IndexOf(ArgumentObject.ARG_XML_INFO_COUNTRYREGIONID) != -1)
					{
						AddFilterToFilters(new FilterObject(ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID, comparisonOperator, valueHolder));
					}
					else if (testText.IndexOf(ArgumentObject.ARG_XML_INFO_GENDERMASK) != -1)
					{
						AddFilterToFilters(new FilterObject(ArgumentObject.ArgumentTypeMaskEnum.GenderMask, comparisonOperator, valueHolder));
					}
					else if (testText.IndexOf(ArgumentObject.ARG_XML_INFO_BIRTHDATE) != -1)
					{
						AddFilterToFilters(new FilterObject(ArgumentObject.ArgumentTypeMaskEnum.BirthDate, comparisonOperator, valueHolder));
					}
					else if (testText.IndexOf(ArgumentObject.ARG_XML_INFO_PROMOTIONID) != -1)
					{
						AddFilterToFilters(new FilterObject(ArgumentObject.ArgumentTypeMaskEnum.PromotionID, comparisonOperator, valueHolder));
					}
					else if (testText.IndexOf(ArgumentObject.ARG_XML_INFO_MEMBERID) != -1)
					{
						AddFilterToFilters(new FilterObject(ArgumentObject.ArgumentTypeMaskEnum.MemberID, comparisonOperator, valueHolder));
					}
					else if (testText.IndexOf(ArgumentObject.ARG_XML_INFO_ISPAYINGMEMBERFLAG) != -1)
					{
						AddFilterToFilters(new FilterObject(ArgumentObject.ArgumentTypeMaskEnum.IsPayingMemberFlag, comparisonOperator, valueHolder));
					}
					else if (testText.IndexOf(ArgumentObject.ARG_XML_INFO_LANDINGPAGEURL) != -1)
					{
						AddFilterToFilters(new FilterObject(ArgumentObject.ArgumentTypeMaskEnum.LandingPageURL, comparisonOperator, valueHolder));
					}
					else if (testText.IndexOf(ArgumentObject.ARG_XML_INFO_GENDERMASK_SEARCHPREF) != -1)
					{
						AddFilterToFilters(new FilterObject(ArgumentObject.ArgumentTypeMaskEnum.GenderMask_SearchPref, comparisonOperator, valueHolder));
					}
				}
			}
			catch
			{
				// If an error was produced while trying to create the pixels then most likely this pixel was created manually (before the
				// creation of the PixelAdminTool.  Notify the user that this pixel should be thoroughly checked and updated.
				pixel.ContentCondition = String.Empty;
				pixel.ContentConditionArgs = String.Empty;
				lbxFilters.Items.Clear();

				MessageBox.Show(Constants.MSG_FILTER_PARAMETER_ERROR, Constants.MSGBOXCAPTION_WARNING);
			}
		}

		private void AddFilterToFilters(FilterObject filter)
		{
			lbxFilters.Items.Add(filter);
		}

		/// <summary>
		/// This method converts the string xml into an XMLDocument, finds the node with name <code>urlArgs</code>
		/// and then parses it (Paramters).  There should be only one such node per pixel.
		/// </summary>
		/// <param name="contentCondition"></param>
		private void PopulateParameters(string contentCondition)
		{
			String keyHolder;
			XmlNode urlNode;
			XmlDocument ccXmlDoc = ArgumentObject.StringToXml(contentCondition);
			ArrayList parameterNamesList = new ArrayList();
			String parameterNames;
			Int32 counter = 0;

			try
			{
				// Get the first node named urlArgs (there should be only one).
				try
				{
					urlNode = ((XmlNodeList)ccXmlDoc.GetElementsByTagName("urlArgs")).Item(0);
				}
				catch
				{
					urlNode = null;
				}

				if (urlNode != null)
				{
					// Extract the parameterNamesList from the InnerXml.
					parameterNames = urlNode.InnerXml;

					// Each parameter name will end with a "<xsl:".
					// An HtmlDecode is necessary because the parameters are HtmlEncoded when they are added to the collection.  If we
					// do not decode them we will encode twice.
					do 
					{
						parameterNamesList.Add(HttpUtility.HtmlDecode(parameterNames.Substring(0, parameterNames.IndexOf("<xsl:"))));

						parameterNames = parameterNames.Substring(parameterNames.IndexOf("/>") + 2);	// +2 to account for the length of "/>".
					}
					while (parameterNames.Length > 0);

					// Scroll through each child node and parse.  We assume that all its children will be "value-of" nodes.
					foreach (XmlNode ValueOfNode in urlNode.ChildNodes)
					{
						if (ValueOfNode.NodeType.ToString() == "Element")
						{
							// Retrive the Key.
							keyHolder = parameterNamesList[counter].ToString();
							counter += 1;

							switch (ValueOfNode.Attributes.GetNamedItem("select").Value)
							{
								case ArgumentObject.ARG_XML_INFO_BIRTHDATE:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.BirthDate, keyHolder));
									break;
								case ArgumentObject.ARG_XML_INFO_GENDERMASK:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.GenderMask, keyHolder));
									break;
								case ArgumentObject.ARG_XML_INFO_SESSIONID:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.SessionID, keyHolder));
									break;
								case ArgumentObject.ARG_XML_INFO_MEMBERID:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.MemberID, keyHolder));
									break;
								case ArgumentObject.ARG_XML_INFO_COUNTRYREGIONID:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID, keyHolder));
									break;
								case ArgumentObject.ARG_XML_INFO_REFERRINGURL:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.ReferringURL, keyHolder));
									break;
								case ArgumentObject.ARG_XML_INFO_SUBSCRIPTIONDURATION:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.SubscriptionDuration, keyHolder));
									break;
								case ArgumentObject.ARG_XML_INFO_SUBSCRIPTIONAMOUNT:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.SubscriptionAmount, keyHolder));
									break;
								case ArgumentObject.ARG_XML_INFO_STATEABBREVIATION:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.StateAbbreviation, keyHolder));
									break;
								case ArgumentObject.ARG_XML_INFO_AGEGROUP:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.AgeGroup, keyHolder));
									break;
								case ArgumentObject.ARG_XML_INFO_AGE:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.Age, keyHolder));
									break;
								case ArgumentObject.ARG_XML_INFO_GENDERMASK_SEARCHPREF:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.GenderMask_SearchPref, keyHolder));
									break;
								case ArgumentObject.ARG_XML_INFO_COUNTRYREGIONID_SEARCHPREF:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID_SearchPref, keyHolder));
									break;
								case ArgumentObject.ARG_XML_INFO_STATEABBREVIATION_SEARCHPREF:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.StateAbbreviation_SearchPref, keyHolder));
									break;
								case ArgumentObject.ARG_XML_INFO_MINAGE_SEARCHPREF:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.MinAge_SearchPref, keyHolder));
									break;
								case ArgumentObject.ARG_XML_INFO_MAXAGE_SEARCHPREF:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.MaxAge_SearchPref, keyHolder));
									break;
								case ArgumentObject.ARG_XML_INFO_MINAGEGROUP_SEARCHPREF:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.MinAgeGroup_SearchPref, keyHolder));
									break;
								case ArgumentObject.ARG_XML_INFO_MAXAGEGROUP_SEARCHPREF:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.MaxAgeGroup_SearchPref, keyHolder));
									break;
								case ArgumentObject.ARG_XML_INFO_PRM:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.PRM, keyHolder));
									break;
								case ArgumentObject.ARG_XML_INFO_TIMESTAMP:
									AddParameterToParameters(new ParameterObject(ArgumentObject.ArgumentTypeMaskEnum.Timestamp, keyHolder));
									break;
							}
						}
					}
				}
			}
			catch
			{
				// If an error was produced while trying to create the pixels then most likely this pixel was created manually (before the
				// creation of the PixelAdminTool.  Notify the user that this pixel should be thoroughly checked and updated.
				pixel.ContentCondition = String.Empty;
				pixel.ContentConditionArgs = String.Empty;
				lbxFilters.Items.Clear();

				MessageBox.Show(Constants.MSG_FILTER_PARAMETER_ERROR, Constants.MSGBOXCAPTION_WARNING);
			}
		}

		private void AddParameterToParameters(ParameterObject parameter)
		{
			lbxParameters.Items.Add(parameter);
		}

		private void btnAddFilter_Click(object sender, System.EventArgs e)
		{
			AddFilter();
		}

		private void AddFilter()
		{
			Filter frm = new Filter(lbxFilters);
			frm.settings = settings;

			if (frm.ShowDialog() == DialogResult.OK)
			{
				DisplayPixel(false);
			}
		}

		private void btnEditFilter_Click(object sender, System.EventArgs e)
		{
			if (lbxFilters.Items.Count <= 0)
				MessageBox.Show(Constants.MSG_FILTER_NONE_ERROR, Constants.MSGBOXCAPTION_WARNING);
			else if (lbxFilters.SelectedItem == null)
				MessageBox.Show(Constants.MSG_FILTER_SELECT_ERROR, Constants.MSGBOXCAPTION_WARNING);
			else
			{
				Filter frm = new Filter(lbxFilters, true);
				frm.settings = settings;

				if (frm.ShowDialog() == DialogResult.OK)
				{
					DisplayPixel(false);
				}
			}
		}

		private void btnDeleteFilter_Click(object sender, System.EventArgs e)
		{
			if (lbxFilters.Items.Count <= 0)
				MessageBox.Show(Constants.MSG_FILTER_NONE_ERROR, Constants.MSGBOXCAPTION_WARNING);
			else if (lbxFilters.SelectedItem == null)
				MessageBox.Show(Constants.MSG_FILTER_SELECT_ERROR, Constants.MSGBOXCAPTION_WARNING);
			else
				lbxFilters.Items.Remove(lbxFilters.SelectedItem);
		}

		private void btnAddParameter_Click(object sender, System.EventArgs e)
		{
			AddParameter();
		}

		private void AddParameter()
		{
			Parameter frm = new Parameter(lbxParameters);
			frm.settings = settings;

			if (frm.ShowDialog() == DialogResult.OK)
			{
				DisplayPixel(false);
			}
		}

		private void btnDeleteParameter_Click(object sender, System.EventArgs e)
		{
			if (lbxParameters.Items.Count <= 0)
				MessageBox.Show(Constants.MSG_PARAMETER_NONE_ERROR, Constants.MSGBOXCAPTION_WARNING);
			else if (lbxParameters.SelectedItem == null)
				MessageBox.Show(Constants.MSG_PARAMETER_SELECT_ERROR, Constants.MSGBOXCAPTION_WARNING);
			else
				lbxParameters.Items.Remove(lbxParameters.SelectedItem);
		}

		private void btnEditParameter_Click(object sender, System.EventArgs e)
		{
			if (lbxParameters.Items.Count <= 0)
				MessageBox.Show(Constants.MSG_PARAMETER_NONE_ERROR, Constants.MSGBOXCAPTION_WARNING);
			else if (lbxParameters.SelectedItem == null)
				MessageBox.Show(Constants.MSG_PARAMETER_SELECT_ERROR, Constants.MSGBOXCAPTION_WARNING);
			else
			{
				Parameter frm = new Parameter(lbxParameters, true);
				frm.settings = settings;

				if (frm.ShowDialog() == DialogResult.OK)
				{
					DisplayPixel(false);
				}
			}
		}

		/// <summary>
		/// NOTE:  If changes were made to the Pixel currently open in the window and that pixel was not yet saved,
		/// the pixel that will be approved is the pixel as it was before the unsaved changes.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnNextStep_Click(object sender, System.EventArgs e)
		{
			// Are you sure?
			if (MessageBox.Show(String.Format(Constants.MSG_NEXTSTEP_PIXEL_CONFIRM, btnNextStep.Text), Constants.MSGBOXCAPTION_CONFIRM, MessageBoxButtons.YesNo) == DialogResult.No)
				return;
			else
			{
				try
				{
					// Do the correct action.
					switch (nextPublishActionPublishObjectID)
					{
						case Constants.PUBACTPUBOBJID_PIXEL_APPROVEINCONTENTSTG:
							settings.ClientAPI.ApprovePagePixel(pixel.PagePixelID, settings.MemberID, pixel.PublishID);
							break;

						case Constants.PUBACTPUBOBJID_PIXEL_PUBLISHTOCONTENTSTG:
							settings.ClientAPI.PublishPagePixel(pixel, settings.MemberID, Constants.PUBACTPUBOBJID_PIXEL_PUBLISHTOCONTENTSTG);
							break;

						default:
							settings.ClientAPI.SavePublishAction(pixel.PublishID, nextPublishActionPublishObjectID, settings.MemberID);
							break;
					}

					DialogResult = DialogResult.OK;
					this.Close();
				}
				catch(Exception ex)
				{
					MessageBox.Show(String.Format(Constants.MSG_CONTACT_ADMINISTRATOR, ex.Message), Constants.MSGBOXCAPTION_SYSTEMERROR);
					return;
				}
			}
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			// Are you sure?
			if (MessageBox.Show(Constants.MSG_DELETE_PIXEL_CONFIRM, Constants.MSGBOXCAPTION_CONFIRM, MessageBoxButtons.YesNo) == DialogResult.No)
				return;
			else
			{
				try
				{
					// Delete.
					settings.ClientAPI.DeletePagePixel(pixel.PagePixelID, settings.MemberID);
				}
				catch(Exception ex)
				{
					MessageBox.Show(String.Format(Constants.MSG_CONTACT_ADMINISTRATOR, ex.Message), Constants.MSGBOXCAPTION_SYSTEMERROR);
					return;
				}

				DialogResult = DialogResult.OK;
				this.Close();
			}
		}

		private String BuildPreview()
		{
			String previewText = String.Empty;

			String parametersText = BuildParametersForPreview(lbxParameters);

			// Build the Preview.  Add the parameters with placeholder values, where necessary.
			if (rbImage.Checked)
				previewText += "http://";
		
			if (parametersText != String.Empty)
				previewText += rtbxContent.Text.Replace(Constants.PARAMETER_STRING_START, parametersText);
			else
				previewText += rtbxContent.Text;

			return previewText;
		}

		private string BuildParametersForPreview(ListBox parameters)
		{
			string dummyData = String.Empty;
			string stringHolder = String.Empty;

			for (Int16 i = 0; i < parameters.Items.Count; i++)
			{
				switch (((ParameterObject)(parameters.Items[i])).ArgumentTypeMask)
				{
					case ArgumentObject.ArgumentTypeMaskEnum.BirthDate:
						dummyData = "19730630";
						break;
					case ArgumentObject.ArgumentTypeMaskEnum.GenderMask:
						dummyData = "9";
						break;
					case ArgumentObject.ArgumentTypeMaskEnum.SessionID:
						dummyData = "EDC898CE-8126-465B-9F6F-F2C7C37D51C7";
						break;
					case ArgumentObject.ArgumentTypeMaskEnum.MemberID:
						dummyData = "988264912";
						break;
					case ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID:
						dummyData = "223";
						break;
					case ArgumentObject.ArgumentTypeMaskEnum.ReferringURL:
						dummyData = "http%3a%2f%2fwww.americansingles.com%2fdefault.asp%3fp%3d18000";
						break;
					case ArgumentObject.ArgumentTypeMaskEnum.SubscriptionDuration:
						dummyData = "3";
						break;
					case ArgumentObject.ArgumentTypeMaskEnum.SubscriptionAmount:
						dummyData = "29.95";
						break;
					case ArgumentObject.ArgumentTypeMaskEnum.StateAbbreviation:
						dummyData = "CA";
						break;
					case ArgumentObject.ArgumentTypeMaskEnum.Age:
						dummyData = "27";
						break;
					case ArgumentObject.ArgumentTypeMaskEnum.AgeGroup:
						dummyData = "2";
						break;
					case ArgumentObject.ArgumentTypeMaskEnum.GenderMask_SearchPref:
						dummyData = "9";
						break;
					case ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID_SearchPref:
						dummyData = "223";
						break;
					case ArgumentObject.ArgumentTypeMaskEnum.StateAbbreviation_SearchPref:
						dummyData = "CA";
						break;
					case ArgumentObject.ArgumentTypeMaskEnum.MinAge_SearchPref:
						dummyData = "23";
						break;
					case ArgumentObject.ArgumentTypeMaskEnum.MaxAge_SearchPref:
						dummyData = "32";
						break;
					case ArgumentObject.ArgumentTypeMaskEnum.MinAgeGroup_SearchPref:
						dummyData = "1";
						break;
					case ArgumentObject.ArgumentTypeMaskEnum.MaxAgeGroup_SearchPref:
						dummyData = "2";
						break;
					case ArgumentObject.ArgumentTypeMaskEnum.PRM:
						dummyData = "4938273";
						break;
					case ArgumentObject.ArgumentTypeMaskEnum.Timestamp:
						dummyData = DateTime.Now.ToString("yyyyMMddHHmmss");
						break;
					default:
						break;
				}

				stringHolder += ((ParameterObject)(parameters.Items[i])).Key + dummyData;
			}

			return stringHolder;
		}

		private void btnPreview_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show(BuildPreview());
		}

		private void btnTest_Click(object sender, System.EventArgs e)
		{
			Browser frm = new Browser(BuildPreview());

			if (frm.ShowDialog() == DialogResult.OK)
			{
				DisplayPixel(false);
			}
		}

		private void rbText_CheckedChanged(object sender, System.EventArgs e)
		{
			if (rbText.Checked)
				btnTest.Visible = true;
			else
				btnTest.Visible = false;
		}
	}
}
