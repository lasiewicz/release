using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Xml;
using System.Web;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for Filter.
	/// </summary>
	public class Filter : System.Windows.Forms.Form
	{
		public readonly DateTime NO_ENDDATE = new DateTime(1999, 1, 1);

		private System.ComponentModel.Container components = null;
		private Settings settingsObj;
		private FilterObject filterObj;
		private ListBox filtersObj;
		private System.Windows.Forms.GroupBox gbxButtons;
		private System.Windows.Forms.Button btnAdd;
		private System.Windows.Forms.TextBox tbxValue;
		private System.Windows.Forms.Label lblValue;
		private System.Windows.Forms.ComboBox cboType;
		private System.Windows.Forms.Label lblType;
		private System.Windows.Forms.ComboBox cboComparison;
		private System.Windows.Forms.Label lblComparison;
		private System.Windows.Forms.Label lblHint;
		private System.Windows.Forms.Button btnCancel;

		public Filter()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			InitializeFilterTypeList();
			InitializeComparisonOperators();
		}

		public Filter(ListBox filters, Boolean isEdit) : this()
		{
			filtersObj = filters;
			
			if (isEdit)
				filterObj = (FilterObject)(filters.SelectedItem);

			DisplayFilter();
		}

		public Filter(ListBox filters) : this(filters, false)
		{
		}

		public Settings settings
		{
			set
			{
				settingsObj = value;
			}
		}

		/// <summary>	
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(Boolean disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.gbxButtons = new System.Windows.Forms.GroupBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnAdd = new System.Windows.Forms.Button();
			this.tbxValue = new System.Windows.Forms.TextBox();
			this.lblValue = new System.Windows.Forms.Label();
			this.cboType = new System.Windows.Forms.ComboBox();
			this.lblType = new System.Windows.Forms.Label();
			this.cboComparison = new System.Windows.Forms.ComboBox();
			this.lblComparison = new System.Windows.Forms.Label();
			this.lblHint = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// gbxButtons
			// 
			this.gbxButtons.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.gbxButtons.Location = new System.Drawing.Point(-160, 152);
			this.gbxButtons.Name = "gbxButtons";
			this.gbxButtons.Size = new System.Drawing.Size(832, 4);
			this.gbxButtons.TabIndex = 30;
			this.gbxButtons.TabStop = false;
			this.gbxButtons.Text = "gbxButtons";
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(408, 168);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(81, 28);
			this.btnCancel.TabIndex = 4;
			this.btnCancel.Text = "&Cancel";
			this.btnCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// btnAdd
			// 
			this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnAdd.Location = new System.Drawing.Point(320, 168);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(81, 28);
			this.btnAdd.TabIndex = 3;
			this.btnAdd.Text = "&Add";
			this.btnAdd.Click += new System.EventHandler(this.cmdAdd_Click);
			// 
			// tbxValue
			// 
			this.tbxValue.Location = new System.Drawing.Point(96, 56);
			this.tbxValue.MaxLength = 1024;
			this.tbxValue.Name = "tbxValue";
			this.tbxValue.Size = new System.Drawing.Size(392, 20);
			this.tbxValue.TabIndex = 67;
			this.tbxValue.Text = "";
			// 
			// lblValue
			// 
			this.lblValue.Location = new System.Drawing.Point(48, 56);
			this.lblValue.Name = "lblValue";
			this.lblValue.Size = new System.Drawing.Size(48, 21);
			this.lblValue.TabIndex = 66;
			this.lblValue.Text = "&Value:";
			this.lblValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// cboType
			// 
			this.cboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboType.Location = new System.Drawing.Point(96, 8);
			this.cboType.Name = "cboType";
			this.cboType.Size = new System.Drawing.Size(232, 21);
			this.cboType.TabIndex = 65;
			this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
			// 
			// lblType
			// 
			this.lblType.Location = new System.Drawing.Point(64, 8);
			this.lblType.Name = "lblType";
			this.lblType.Size = new System.Drawing.Size(32, 21);
			this.lblType.TabIndex = 64;
			this.lblType.Text = "&Type:";
			this.lblType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// cboComparison
			// 
			this.cboComparison.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboComparison.Location = new System.Drawing.Point(96, 32);
			this.cboComparison.Name = "cboComparison";
			this.cboComparison.Size = new System.Drawing.Size(176, 21);
			this.cboComparison.TabIndex = 76;
			// 
			// lblComparison
			// 
			this.lblComparison.Location = new System.Drawing.Point(16, 32);
			this.lblComparison.Name = "lblComparison";
			this.lblComparison.Size = new System.Drawing.Size(80, 21);
			this.lblComparison.TabIndex = 75;
			this.lblComparison.Text = "&Comparison:";
			this.lblComparison.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblHint
			// 
			this.lblHint.Location = new System.Drawing.Point(104, 80);
			this.lblHint.Name = "lblHint";
			this.lblHint.Size = new System.Drawing.Size(384, 64);
			this.lblHint.TabIndex = 77;
			// 
			// Filter
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(498, 207);
			this.Controls.Add(this.lblHint);
			this.Controls.Add(this.cboComparison);
			this.Controls.Add(this.lblComparison);
			this.Controls.Add(this.tbxValue);
			this.Controls.Add(this.lblValue);
			this.Controls.Add(this.cboType);
			this.Controls.Add(this.lblType);
			this.Controls.Add(this.gbxButtons);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnAdd);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Filter";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Filter";
			this.Load += new System.EventHandler(this.Filter_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void InitializeFilterTypeList()
		{
			ArrayList filterTypeList = new ArrayList();

			// Add a blank value.
			filterTypeList.Add(new ItemData(String.Empty, Matchnet.Constants.NULL_INT));

			for (Int16 i = 0; i <= Constants.FILTER_TYPE_LIST.GetUpperBound(0); i++)
			{
				filterTypeList.Add(new ItemData(Constants.FILTER_TYPE_LIST[i, 0], Convert.ToInt32(Constants.FILTER_TYPE_LIST[i, 1])));
			}

			cboType.DataSource = filterTypeList;
		}

		private void InitializeComparisonOperators()
		{
			ArrayList comparisonOpList = new ArrayList();

			// Add a blank value.
			comparisonOpList.Add(new ItemData(String.Empty, String.Empty));

			for (Int16 i = 0; i < Constants.COMPARISON_OPERATOR_LIST.GetUpperBound(0); i++)
			{
				comparisonOpList.Add(new ItemData(Constants.COMPARISON_OPERATOR_LIST[i, 0], Constants.COMPARISON_OPERATOR_LIST[i, 1]));
			}

			cboComparison.DataSource = comparisonOpList;
		}

		private Boolean ValidateFilter(FilterObject filter)
		{
			if ((Int32)filter.ArgumentTypeMask == Matchnet.Constants.NULL_INT)
			{
				MessageBox.Show(Constants.MSG_ARGUMENTTYPEMASK_REQUIRED_ERROR, Constants.MSGBOXCAPTION_WARNING);
				return false;
			}

			if (filter.ComparisonOperator == String.Empty)
			{
				MessageBox.Show(Constants.MSG_COMPARISONOPERATOR_REQUIRED_ERROR, Constants.MSGBOXCAPTION_WARNING);
				return false;
			}

			if (filter.Value == String.Empty)
			{
				MessageBox.Show(Constants.MSG_VALUE_REQUIRED_ERROR, Constants.MSGBOXCAPTION_WARNING);
				return false;
			}
			
			return true;
		}

		private void AddFilter()
		{
			FilterObject filter = BuildFilter();

			if (ValidateFilter(filter))
			{
				// Remove the existing filterObj, if one exists.
				if (filterObj != null)
					filtersObj.Items.Remove(filterObj);

				// Add the new filter.
				filtersObj.Items.Add(filter);

				DialogResult = DialogResult.OK;
				Close();
			}
		}

		private FilterObject BuildFilter()
		{
			return new FilterObject((ArgumentObject.ArgumentTypeMaskEnum)(((ItemData)(cboType.SelectedValue)).Value), ((ItemData)(cboComparison.SelectedValue)).Value.ToString(), tbxValue.Text);
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			AddFilter();
		}

		private void Filter_Load(object sender, System.EventArgs e)
		{
			DisplayFilter();
		}

		private void DisplayFilter()
		{
			if (filterObj != null)
			{
				ItemData.SelectItem(cboType, (Int32)filterObj.ArgumentTypeMask);
				ItemData.SelectItem(cboComparison, filterObj.ComparisonOperator);
				tbxValue.Text = filterObj.Value;
			}
		}

		private void cboType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cboType.SelectedIndex > 0)
				lblHint.Text = Constants.FILTER_TYPE_LIST[cboType.SelectedIndex - 1, 2];
		}
	}
}
