using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using mshtml;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for Browser.
	/// </summary>
	public class Browser : System.Windows.Forms.Form
	{
		private AxSHDocVw.AxWebBrowser wbBrowser;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Browser(String pixelString)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			Object notUsed = null;
			wbBrowser.Navigate("about:blank", ref notUsed, ref notUsed, ref notUsed, ref notUsed);

			IHTMLDocument2 htmlDoc = (IHTMLDocument2)wbBrowser.Document;

			htmlDoc.write(String.Format(Constants.HTML_TEST_TEMPLATE, pixelString));
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Browser));
			this.wbBrowser = new AxSHDocVw.AxWebBrowser();
			((System.ComponentModel.ISupportInitialize)(this.wbBrowser)).BeginInit();
			this.SuspendLayout();
			// 
			// wbBrowser
			// 
			this.wbBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.wbBrowser.Enabled = true;
			this.wbBrowser.Location = new System.Drawing.Point(8, 8);
			this.wbBrowser.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("wbBrowser.OcxState")));
			this.wbBrowser.Size = new System.Drawing.Size(632, 496);
			this.wbBrowser.TabIndex = 0;
			// 
			// Browser
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(648, 517);
			this.Controls.Add(this.wbBrowser);
			this.Name = "Browser";
			this.Text = "Browser";
			((System.ComponentModel.ISupportInitialize)(this.wbBrowser)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}
