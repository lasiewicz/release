using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// </summary>
	public class Pixels : System.Windows.Forms.Form
	{
		private System.ComponentModel.Container components = null;
		private Settings settingsObj;
		private Web.PagePixel[] pagePixels;
		private System.Windows.Forms.ListView lvPixels;
		private System.Windows.Forms.MenuItem mnuAdd;
		private System.Windows.Forms.MenuItem mnuEdit;
		private System.Windows.Forms.MenuItem mnuDelete;
		private System.Windows.Forms.MenuItem mnuRefresh;
		private System.Windows.Forms.Label lblSite;
		private System.Windows.Forms.ComboBox cboSite;
		private System.Windows.Forms.Label lblPixels;
		private System.Windows.Forms.MainMenu mnuMain;
		private System.Windows.Forms.MenuItem mnuPixels;
		private System.Windows.Forms.Label lblPageID;
		private System.Windows.Forms.TextBox tbxPageID;
		private System.Windows.Forms.Button btnLoad;
		private System.Windows.Forms.Button btnAddNew;
		private System.Windows.Forms.ColumnHeader PixelID;
		private System.Windows.Forms.ColumnHeader PageID;
		private System.Windows.Forms.ColumnHeader SiteID;
		private System.Windows.Forms.ColumnHeader PixelType;
		private System.Windows.Forms.ColumnHeader EndDate;
		private System.Windows.Forms.ColumnHeader Description;
		private System.Windows.Forms.ColumnHeader Status;
		private System.Windows.Forms.Label lblKey1;
		private System.Windows.Forms.Label lblKey2;
		private System.Windows.Forms.Label lblKey3;
		private System.Windows.Forms.MenuItem mnuDiv1;

		public Pixels()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lvPixels = new System.Windows.Forms.ListView();
			this.PixelID = new System.Windows.Forms.ColumnHeader();
			this.PageID = new System.Windows.Forms.ColumnHeader();
			this.SiteID = new System.Windows.Forms.ColumnHeader();
			this.PixelType = new System.Windows.Forms.ColumnHeader();
			this.Status = new System.Windows.Forms.ColumnHeader();
			this.EndDate = new System.Windows.Forms.ColumnHeader();
			this.Description = new System.Windows.Forms.ColumnHeader();
			this.lblPixels = new System.Windows.Forms.Label();
			this.lblSite = new System.Windows.Forms.Label();
			this.mnuMain = new System.Windows.Forms.MainMenu();
			this.mnuPixels = new System.Windows.Forms.MenuItem();
			this.mnuAdd = new System.Windows.Forms.MenuItem();
			this.mnuEdit = new System.Windows.Forms.MenuItem();
			this.mnuDelete = new System.Windows.Forms.MenuItem();
			this.mnuDiv1 = new System.Windows.Forms.MenuItem();
			this.mnuRefresh = new System.Windows.Forms.MenuItem();
			this.cboSite = new System.Windows.Forms.ComboBox();
			this.lblPageID = new System.Windows.Forms.Label();
			this.tbxPageID = new System.Windows.Forms.TextBox();
			this.btnLoad = new System.Windows.Forms.Button();
			this.btnAddNew = new System.Windows.Forms.Button();
			this.lblKey1 = new System.Windows.Forms.Label();
			this.lblKey2 = new System.Windows.Forms.Label();
			this.lblKey3 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lvPixels
			// 
			this.lvPixels.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lvPixels.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																					   this.PixelID,
																					   this.PageID,
																					   this.SiteID,
																					   this.PixelType,
																					   this.Status,
																					   this.EndDate,
																					   this.Description});
			this.lvPixels.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lvPixels.FullRowSelect = true;
			this.lvPixels.GridLines = true;
			this.lvPixels.HideSelection = false;
			this.lvPixels.Location = new System.Drawing.Point(16, 56);
			this.lvPixels.MultiSelect = false;
			this.lvPixels.Name = "lvPixels";
			this.lvPixels.Size = new System.Drawing.Size(760, 468);
			this.lvPixels.TabIndex = 28;
			this.lvPixels.View = System.Windows.Forms.View.Details;
			this.lvPixels.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lvPixels_KeyPress);
			this.lvPixels.DoubleClick += new System.EventHandler(this.lvPixels_DoubleClick);
			this.lvPixels.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.ColumnClick);
			// 
			// PixelID
			// 
			this.PixelID.Text = "PixelID";
			this.PixelID.Width = 52;
			// 
			// PageID
			// 
			this.PageID.Text = "PageID";
			this.PageID.Width = 57;
			// 
			// SiteID
			// 
			this.SiteID.Text = "SiteID";
			this.SiteID.Width = 47;
			// 
			// PixelType
			// 
			this.PixelType.Text = "PixelType";
			this.PixelType.Width = 70;
			// 
			// Status
			// 
			this.Status.Text = "Status";
			// 
			// EndDate
			// 
			this.EndDate.Text = "EndDate";
			this.EndDate.Width = 105;
			// 
			// Description
			// 
			this.Description.Text = "Description";
			this.Description.Width = 424;
			// 
			// lblPixels
			// 
			this.lblPixels.Location = new System.Drawing.Point(16, 40);
			this.lblPixels.Name = "lblPixels";
			this.lblPixels.Size = new System.Drawing.Size(96, 18);
			this.lblPixels.TabIndex = 34;
			this.lblPixels.Text = "Pixels:";
			// 
			// lblSite
			// 
			this.lblSite.Location = new System.Drawing.Point(8, 8);
			this.lblSite.Name = "lblSite";
			this.lblSite.Size = new System.Drawing.Size(32, 21);
			this.lblSite.TabIndex = 35;
			this.lblSite.Text = "&Site:";
			this.lblSite.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// mnuPixels
			// 
			this.mnuPixels.Index = -1;
			this.mnuPixels.Text = "";
			// 
			// mnuAdd
			// 
			this.mnuAdd.Index = -1;
			this.mnuAdd.Text = "";
			// 
			// mnuEdit
			// 
			this.mnuEdit.Index = -1;
			this.mnuEdit.Text = "";
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = -1;
			this.mnuDelete.Text = "";
			// 
			// mnuDiv1
			// 
			this.mnuDiv1.Index = -1;
			this.mnuDiv1.Text = "";
			// 
			// mnuRefresh
			// 
			this.mnuRefresh.Index = -1;
			this.mnuRefresh.Text = "";
			// 
			// cboSite
			// 
			this.cboSite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboSite.Location = new System.Drawing.Point(40, 8);
			this.cboSite.Name = "cboSite";
			this.cboSite.Size = new System.Drawing.Size(240, 21);
			this.cboSite.TabIndex = 36;
			// 
			// lblPageID
			// 
			this.lblPageID.Location = new System.Drawing.Point(280, 8);
			this.lblPageID.Name = "lblPageID";
			this.lblPageID.Size = new System.Drawing.Size(48, 21);
			this.lblPageID.TabIndex = 37;
			this.lblPageID.Text = "&PageID:";
			this.lblPageID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxPageID
			// 
			this.tbxPageID.Location = new System.Drawing.Point(328, 8);
			this.tbxPageID.MaxLength = 6;
			this.tbxPageID.Name = "tbxPageID";
			this.tbxPageID.Size = new System.Drawing.Size(56, 20);
			this.tbxPageID.TabIndex = 38;
			this.tbxPageID.Text = "";
			// 
			// btnLoad
			// 
			this.btnLoad.Location = new System.Drawing.Point(392, 8);
			this.btnLoad.Name = "btnLoad";
			this.btnLoad.Size = new System.Drawing.Size(96, 24);
			this.btnLoad.TabIndex = 39;
			this.btnLoad.Text = "Load";
			this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
			// 
			// btnAddNew
			// 
			this.btnAddNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnAddNew.Location = new System.Drawing.Point(680, 8);
			this.btnAddNew.Name = "btnAddNew";
			this.btnAddNew.Size = new System.Drawing.Size(96, 24);
			this.btnAddNew.TabIndex = 40;
			this.btnAddNew.Text = "Add a New Pixel";
			this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
			// 
			// lblKey1
			// 
			this.lblKey1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.lblKey1.Location = new System.Drawing.Point(16, 528);
			this.lblKey1.Name = "lblKey1";
			this.lblKey1.Size = new System.Drawing.Size(176, 16);
			this.lblKey1.TabIndex = 41;
			this.lblKey1.Text = "Black indicates a LIVE pixel.";
			// 
			// lblKey2
			// 
			this.lblKey2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.lblKey2.ForeColor = System.Drawing.Color.Gray;
			this.lblKey2.Location = new System.Drawing.Point(200, 528);
			this.lblKey2.Name = "lblKey2";
			this.lblKey2.Size = new System.Drawing.Size(200, 16);
			this.lblKey2.TabIndex = 42;
			this.lblKey2.Text = "Gray indicates an ON-HOLD pixel.";
			// 
			// lblKey3
			// 
			this.lblKey3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.lblKey3.ForeColor = System.Drawing.Color.Red;
			this.lblKey3.Location = new System.Drawing.Point(416, 528);
			this.lblKey3.Name = "lblKey3";
			this.lblKey3.Size = new System.Drawing.Size(208, 16);
			this.lblKey3.TabIndex = 43;
			this.lblKey3.Text = "Red indicates a NOT APPROVED pixel.";
			// 
			// Pixels
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(792, 553);
			this.Controls.Add(this.lblKey3);
			this.Controls.Add(this.lblKey2);
			this.Controls.Add(this.lblKey1);
			this.Controls.Add(this.btnAddNew);
			this.Controls.Add(this.btnLoad);
			this.Controls.Add(this.tbxPageID);
			this.Controls.Add(this.lblPageID);
			this.Controls.Add(this.cboSite);
			this.Controls.Add(this.lblSite);
			this.Controls.Add(this.lvPixels);
			this.Controls.Add(this.lblPixels);
			this.Menu = this.mnuMain;
			this.Name = "Pixels";
			this.Text = "Pixels";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.Pixels_Load);
			this.ResumeLayout(false);

		}
		#endregion

		public Settings settings
		{
			set
			{
				settingsObj = value;
				ItemData.SetComboBoxValues(cboSite, settingsObj.Sites);
			}
			get
			{
				return settingsObj;
			}
		}

		private void Pixels_Load(object sender, System.EventArgs e)
		{
			// Display/Hide btnAdd.
			if (settingsObj.User.IsPixelEditor)
				btnAddNew.Visible = true;
			else
				btnAddNew.Visible = false;

			this.Visible = true;
			this.Refresh();
		}

		public void Reload()
		{
			lvPixels.Items.Clear();

			ItemData siteItem = (ItemData)cboSite.SelectedItem;
			String pageIDStr = tbxPageID.Text.Trim();
			Int32 pageID = Matchnet.Constants.NULL_INT;

			if (pageIDStr != String.Empty)
				pageID = Convert.ToInt32(pageIDStr);

			if (siteItem == null || pageID == Matchnet.Constants.NULL_INT)
			{
				MessageBox.Show(Constants.MSG_SITE_AND_PAGEID_REQUIRED_ERROR, Constants.MSGBOXCAPTION_WARNING);
				return;
			}

			try
			{
				pagePixels = settingsObj.ClientAPI.RetrievePagePixels(pageID, Convert.ToInt32(siteItem.Value), settingsObj.User.PixelAdminDisplayTypeMask);

				ListViewItem lvItem;

				for (Int32 i = 0; i < pagePixels.Length; i++)
				{
					Web.PagePixel pagePixel = pagePixels[i];
					lvItem = new ListViewItem();

					// If a pixel is ON HOLD then make it gray.
					if (pagePixel.PublishFlag)
						lvItem.ForeColor = System.Drawing.Color.Gray;
					// If a pixel is NOT APPROVED then make it red.
					if (pagePixel.ApprovalFlag != null && !(Boolean)pagePixel.ApprovalFlag)
						lvItem.ForeColor = System.Drawing.Color.Red;

					lvItem.Text = pagePixel.PagePixelID.ToString();
					lvItem.SubItems.Add(pagePixel.PageID.ToString());
					lvItem.SubItems.Add(pagePixel.SiteID.ToString());
					lvItem.SubItems.Add(DisplayPixelType(pagePixel.ImgFlag));
//DELETE IF ALL WORKS WELL
//Boolean approvalFlag = pagePixel.ApprovalFlag != null ? (Boolean)pagePixel.ApprovalFlag : false;
					lvItem.SubItems.Add(DisplayPixelPublishedStatus(pagePixel.PublishFlag, (Boolean)pagePixel.ApprovalFlag));
					lvItem.SubItems.Add(pagePixel.EndDate.ToString("g"));
					lvItem.SubItems.Add(DisplayFirstXChars(pagePixel.Description, Constants.DESCRIPTION_SUMMARY_LENGTH));

					lvPixels.Items.Add(lvItem);
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(String.Format(Constants.MSG_CONTACT_ADMINISTRATOR, ex.Message), Constants.MSGBOXCAPTION_SYSTEMERROR);
			}
		}

		protected static String DisplayPixelType(Boolean imgFlag)
		{
			if (imgFlag)
				return Constants.PIXELTYPE_IMAGE;
			else
				return Constants.PIXELTYPE_TEXT;
		}

		protected static String DisplayPixelPublishedStatus(Boolean isPublishedFlag, Boolean approvalFlag)
		{
			string status = String.Empty;

			if (isPublishedFlag)
				status += Constants.PIXELSTATUS_LIVE + ", ";
			else
				status += Constants.PIXELSTATUS_ONHOLD + ", ";

			if (approvalFlag)
				status += Constants.PIXELSTATUS_APPROVED;
			else
				status += Constants.PIXELSTATUS_NOTAPPROVED;

			return status;
		}

		protected static string DisplayFirstXChars(string text, int length)
		{
			string returnText;

			if (text.Length <= length)
			{
				returnText = text;
			}
			else
			{
				returnText = text.Substring(0, length) + " ...";
			}

			return returnText;
		}
	
		public void Edit()
		{
			try
			{
				ListViewItem lvItem = lvPixels.SelectedItems[0];

				if (lvItem != null)
				{
					Pixel frm = new Pixel(settingsObj, GetPagePixelByID(pagePixels, Convert.ToInt32(lvItem.SubItems[0].Text)));

					if (frm.ShowDialog() == DialogResult.OK)
					{
						Reload();
					}
				}
				else
				{
					MessageBox.Show(Constants.MSG_SELECT_PIXEL_TO_EDIT, Constants.MSGBOXCAPTION_WARNING, MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(String.Format(Constants.MSG_CONTACT_ADMINISTRATOR, ex.Message), Constants.MSGBOXCAPTION_SYSTEMERROR, MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private Web.PagePixel GetPagePixelByID(Web.PagePixel[] pixels, Int32 pagePixelID)
		{
			for (Int32 i = 0; i < pixels.Length; i++)
			{
				if (pixels[i].PagePixelID == pagePixelID)
					return pixels[i];
			}

			return null;
		}

		private void lvPixels_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)13 || e.KeyChar == (char)32)
			{
				Edit();
			}
		}

		private void lvPixels_DoubleClick(object sender, System.EventArgs e)
		{
			Edit();
		}

		private void btnLoad_Click(object sender, System.EventArgs e)
		{
			Reload();
		}

		private void btnAddNew_Click(object sender, System.EventArgs e)
		{
			Pixel frm;

			if (cboSite.SelectedIndex > 0 && tbxPageID.Text != String.Empty)
				frm = new Pixel(settingsObj, new Web.PagePixel(), Convert.ToInt32(((ItemData)(cboSite.SelectedItem)).Value), Convert.ToInt32(tbxPageID.Text));
			else
				frm = new Pixel(settingsObj, new Web.PagePixel());

			if (frm.ShowDialog() == DialogResult.OK)
			{
				Reload();
			}
		}

		#region Column sorting
		// ColumnClick event handler.
		private void ColumnClick(object o, ColumnClickEventArgs e)
		{
			if (lvPixels.Sorting != SortOrder.Ascending)
				lvPixels.Sorting = SortOrder.Ascending;
			else
				lvPixels.Sorting = SortOrder.Descending;

			lvPixels.ListViewItemSorter = new ListViewItemComparer(e.Column, lvPixels.Sorting != SortOrder.Ascending);
		}
		#endregion
	}
}
