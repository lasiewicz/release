using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Xml;
using System.Web;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for Parameter.
	/// </summary>
	public class Parameter : System.Windows.Forms.Form
	{
		public readonly DateTime NO_ENDDATE = new DateTime(1999, 1, 1);

		private System.ComponentModel.Container components = null;
		private Settings settingsObj;
		private ParameterObject parameterObj;
		private ListBox parametersObj;
		private System.Windows.Forms.GroupBox gbxButtons;
		private System.Windows.Forms.Button btnAdd;
		private System.Windows.Forms.ComboBox cboType;
		private System.Windows.Forms.Label lblType;
		private System.Windows.Forms.TextBox tbxKey;
		private System.Windows.Forms.Label lblKey;
		private System.Windows.Forms.Button btnCancel;

		public Parameter()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			InitializeParameterTypeList();
		}

		public Parameter(ListBox parameters, Boolean isEdit) : this()
		{
			parametersObj = parameters;
			
			if (isEdit)
				parameterObj = (ParameterObject)(parameters.SelectedItem);

			DisplayParameter();
		}

		public Parameter(ListBox parameters) : this(parameters, false)
		{
		}

		public Settings settings
		{
			set
			{
				settingsObj = value;
			}
		}

		/// <summary>	
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(Boolean disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.gbxButtons = new System.Windows.Forms.GroupBox();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnAdd = new System.Windows.Forms.Button();
			this.tbxKey = new System.Windows.Forms.TextBox();
			this.lblKey = new System.Windows.Forms.Label();
			this.cboType = new System.Windows.Forms.ComboBox();
			this.lblType = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// gbxButtons
			// 
			this.gbxButtons.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.gbxButtons.Location = new System.Drawing.Point(-160, 64);
			this.gbxButtons.Name = "gbxButtons";
			this.gbxButtons.Size = new System.Drawing.Size(832, 4);
			this.gbxButtons.TabIndex = 30;
			this.gbxButtons.TabStop = false;
			this.gbxButtons.Text = "gbxButtons";
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(408, 80);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(81, 28);
			this.btnCancel.TabIndex = 4;
			this.btnCancel.Text = "&Cancel";
			this.btnCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// btnAdd
			// 
			this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnAdd.Location = new System.Drawing.Point(320, 80);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(81, 28);
			this.btnAdd.TabIndex = 3;
			this.btnAdd.Text = "&Add";
			this.btnAdd.Click += new System.EventHandler(this.cmdAdd_Click);
			// 
			// tbxKey
			// 
			this.tbxKey.Location = new System.Drawing.Point(96, 32);
			this.tbxKey.MaxLength = 1024;
			this.tbxKey.Name = "tbxKey";
			this.tbxKey.Size = new System.Drawing.Size(392, 20);
			this.tbxKey.TabIndex = 67;
			this.tbxKey.Text = "";
			// 
			// lblKey
			// 
			this.lblKey.Location = new System.Drawing.Point(48, 32);
			this.lblKey.Name = "lblKey";
			this.lblKey.Size = new System.Drawing.Size(48, 21);
			this.lblKey.TabIndex = 66;
			this.lblKey.Text = "&Key:";
			this.lblKey.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// cboType
			// 
			this.cboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboType.Location = new System.Drawing.Point(96, 8);
			this.cboType.Name = "cboType";
			this.cboType.Size = new System.Drawing.Size(232, 21);
			this.cboType.TabIndex = 65;
			// 
			// lblType
			// 
			this.lblType.Location = new System.Drawing.Point(64, 8);
			this.lblType.Name = "lblType";
			this.lblType.Size = new System.Drawing.Size(32, 21);
			this.lblType.TabIndex = 64;
			this.lblType.Text = "&Type:";
			this.lblType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// Parameter
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(498, 119);
			this.Controls.Add(this.tbxKey);
			this.Controls.Add(this.lblKey);
			this.Controls.Add(this.cboType);
			this.Controls.Add(this.lblType);
			this.Controls.Add(this.gbxButtons);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnAdd);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Parameter";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Parameter";
			this.ResumeLayout(false);

		}
		#endregion

		private void InitializeParameterTypeList()
		{
			ArrayList parameterTypeList = new ArrayList();

			// Add a blank value.
			parameterTypeList.Add(new ItemData(String.Empty, Matchnet.Constants.NULL_INT));

			for (Int16 i = 0; i <= Constants.PARAMETER_TYPE_LIST.GetUpperBound(0); i++)
			{
				parameterTypeList.Add(new ItemData(Constants.PARAMETER_TYPE_LIST[i, 0], Convert.ToInt32(Constants.PARAMETER_TYPE_LIST[i, 1])));
			}

			cboType.DataSource = parameterTypeList;
		}

		private void AddParameter()
		{
			ParameterObject parameter = BuildParameter();

			if (ValidateParameter(parameter))
			{
				// Remove the existing parameterObj, if one exists.
				if (parameterObj != null)
					parametersObj.Items.Remove(parameterObj);

				// Add the new parameter.
				parametersObj.Items.Add(parameter);

				DialogResult = DialogResult.OK;
				Close();
			}
		}

		private ParameterObject BuildParameter()
		{
			return new ParameterObject((ArgumentObject.ArgumentTypeMaskEnum)(((ItemData)(cboType.SelectedValue)).Value), tbxKey.Text);
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			AddParameter();
		}

		private void Parameter_Load(object sender, System.EventArgs e)
		{
			DisplayParameter();
		}

		private Boolean ValidateParameter(ParameterObject parameter)
		{
			if ((Int32)parameter.ArgumentTypeMask == Matchnet.Constants.NULL_INT)
			{
				MessageBox.Show(Constants.MSG_ARGUMENTTYPEMASK_REQUIRED_ERROR, Constants.MSGBOXCAPTION_WARNING);
				return false;
			}

			if (parameter.Key == String.Empty)
			{
				MessageBox.Show(Constants.MSG_KEY_REQUIRED_ERROR, Constants.MSGBOXCAPTION_WARNING);
				return false;
			}
			
			return true;
		}

		private void DisplayParameter()
		{
			if (parameterObj != null)
			{
				ItemData.SelectItem(cboType, (Int32)parameterObj.ArgumentTypeMask);
				tbxKey.Text = parameterObj.Key;
			}
		}
	}
}
