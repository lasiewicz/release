using System;
using System.Collections;
using System.Text;
using System.Windows.Forms;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for ParameterObject.
	/// </summary>
	public class ParameterObject
	{
		#region Private Variables
		private ArgumentObject.ArgumentTypeMaskEnum argumentTypeMaskVal;
		private String parameterTypeName;
		private String key;
		#endregion

		#region Constructors
		public ParameterObject(ArgumentObject.ArgumentTypeMaskEnum ArgumentTypeMask, String keyObj)
		{
			argumentTypeMaskVal = ArgumentTypeMask;
			key = keyObj;

			SetReadOnlyProperties(argumentTypeMaskVal);
		}
		#endregion

		#region Public Methods
		public static String BuildParameterXml(ListBox parameters)
		{
			if (parameters.Items.Count == 0)
			{
				return Matchnet.Constants.NULL_STRING;
			}
			else
			{
				StringBuilder xmlString = new StringBuilder();

				// Add main opening tags.
				xmlString.Append("<urlArgs>");

				for (Int16 i = 0; i < parameters.Items.Count; i++)
				{
					xmlString.Append(System.Web.HttpUtility.HtmlEncode(((ParameterObject)(parameters.Items[i])).Key));
					xmlString.Append("<xsl:value-of select=\"");
					xmlString.Append(ArgumentObject.GetArgumentInfo(((ParameterObject)(parameters.Items[i])).ArgumentTypeMask));
					xmlString.Append("\" />");
				}

				// Add main closing tags.
				xmlString.Append("</urlArgs>");

				return xmlString.ToString();
			}
		}
		#endregion

		#region Private Methods
		private void SetReadOnlyProperties(ArgumentObject.ArgumentTypeMaskEnum argmentTypeMask)
		{
			switch (argmentTypeMask)
			{
				case ArgumentObject.ArgumentTypeMaskEnum.BirthDate:
					parameterTypeName = "Birth Date Parameter";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.GenderMask:
					parameterTypeName = "GenderMask Parameter";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.SessionID:
					parameterTypeName = "Session ID Parameter";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.MemberID:
					parameterTypeName = "Member ID Parameter";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID:
					parameterTypeName = "Country Parameter";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.ReferringURL:
					parameterTypeName = "Referring URL Parameter";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.SubscriptionDuration:
					parameterTypeName = "Subscription Duration Parameter";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.SubscriptionAmount:
					parameterTypeName = "Subscription Amount Parameter";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.StateAbbreviation:
					parameterTypeName = "State Abbreviation Parameter";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.Age:
					parameterTypeName = "Age Parameter";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.AgeGroup:
					parameterTypeName = "Age Group Parameter";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.GenderMask_SearchPref:
					parameterTypeName = "Search Preference - GenderMask Parameter";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID_SearchPref:
					parameterTypeName = "Search Preference - Country ID Parameter";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.StateAbbreviation_SearchPref:
					parameterTypeName = "Search Preference - State Abbreviation Parameter";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.MinAge_SearchPref:
					parameterTypeName = "Search Preference - Min Age Parameter";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.MaxAge_SearchPref:
					parameterTypeName = "Search Preference - Max Age Parameter";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.MinAgeGroup_SearchPref:
					parameterTypeName = "Search Preference - Min Age Group Parameter";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.MaxAgeGroup_SearchPref:
					parameterTypeName = "Search Preference - Max Age Group Parameter";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.PRM:
					parameterTypeName = "PRM Parameter";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.Timestamp:
					parameterTypeName = "Timestamp Parameter";
					break;
				default:
					break;
			}
		}
		#endregion

		#region Public Properties
		public string ParameterTypeName
		{
			get
			{
				return parameterTypeName;
			}
		}

		public string Key
		{
			get
			{
				return key;
			}
		}

		public ArgumentObject.ArgumentTypeMaskEnum ArgumentTypeMask
		{
			get
			{
				return argumentTypeMaskVal;
			}
			set
			{
				argumentTypeMaskVal = value;
			}
		}

		public String DisplayName
		{
			get
			{
				return parameterTypeName + ": " + key;
			}
		}
		#endregion
	}
}
