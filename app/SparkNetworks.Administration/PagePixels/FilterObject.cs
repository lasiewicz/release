using System;
using System.Collections;
using System.Text;
using System.Xml;
using System.Windows.Forms;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for FilterObject.
	/// </summary>
	public class FilterObject
	{
		#region Enums and Constants
		public const string OR_DELIMITER = "|";
		#endregion

		#region Private Variables
		private ArgumentObject.ArgumentTypeMaskEnum argumentTypeMaskVal;
		private String filterTypeName;
		private String comparisonOperatorVal;
		private String comparisonOperatorName;
		private String val;
		private String key;
		#endregion

		#region Constructors
		public FilterObject(ArgumentObject.ArgumentTypeMaskEnum argumentTypeMask, String comparisonOperator, String valArg)
		{
			argumentTypeMaskVal = argumentTypeMask;
			comparisonOperatorVal = comparisonOperator;
			val = valArg;

			SetReadOnlyProperties(argumentTypeMaskVal);
		}
		#endregion

		#region Public Methods
		public static string GetComparisonOperatorName(string ComparisonOperator)
		{
			// Note:  < and > are not allowed in XmlDocuments.  Thus, we use their escape character sequence.
			switch (ComparisonOperator)
			{
				case "=":
					return "equal to";
				case "!=":
					return "not equal to";
				case "&lt;":
					return "less than";
				case "&lt;=":
					return "less than or equal to";
				case "&gt;":
					return "greater than";
				case "&gt;=":
					return "greater than or equal to";
				case "contains":
					return "contains the words";
				default:
					return ComparisonOperator;
			}
		}

		/// <summary>
		/// The <code>ContentCondition</code> hold information for both <code>Filters</code> and <code>Parameters</code>.  The code
		/// to create the the ContentCondition is located here, in the <code>FilterObject</code> class but it also makes a call to the
		/// <code>ParameterObject</code> class.
		/// </summary>
		/// <param name="filters"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		public static String BuildContentConditionXml(ListBox filters, ListBox parameters)
		{
			if (filters.Items.Count == 0 && parameters.Items.Count == 0)
			{
				return Matchnet.Constants.NULL_STRING;
			}
			else
			{
				StringBuilder xmlString = new StringBuilder();

				// Add main opening tags.
				xmlString.Append("<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">");
				xmlString.Append("<xsl:template match=\"/\">");
				xmlString.Append("<pagePixel>");

				// Add the ContentCondtions opening tags.
				for (Int16 i = 0; i < filters.Items.Count; i++)
				{
					String valueHolder = ((FilterObject)(filters.Items[i])).Value;

					if (((FilterObject)(filters.Items[i])).ComparisonOperator != "contains")
					{
						xmlString.Append("<xsl:if test=\"");
						// Add a loop for possible ";" ("or" clause).
						do
						{
							xmlString.Append(ArgumentObject.GetArgumentInfo(((FilterObject)(filters.Items[i])).ArgumentTypeMask));
							xmlString.Append(" ");
							xmlString.Append(System.Web.HttpUtility.HtmlEncode(((FilterObject)(filters.Items[i])).ComparisonOperator));

							xmlString.Append(" '");
							// Retrieve the value up to the first OR_DELIMITER.
							// If there is no more delimiters, the current Value goes to the end of the string.
							if (valueHolder.IndexOf(OR_DELIMITER) != -1)
							{
								xmlString.Append(valueHolder.Substring(0, valueHolder.IndexOf(OR_DELIMITER)));
								xmlString.Append("' or ");

								// Update the valueHolder value.
								valueHolder = System.Web.HttpUtility.HtmlEncode(valueHolder.Substring(valueHolder.IndexOf(OR_DELIMITER) + OR_DELIMITER.Length));
							}
							else
							{
								xmlString.Append(valueHolder);
								xmlString.Append("'");

								valueHolder = String.Empty;
							}
						}
						while (valueHolder.Length > 0);

						xmlString.Append("\">");
					}
					else
					{
						xmlString.Append("<xsl:if test=\"");
						// Add a loop for possible ";" ("or" clause).
						do
						{
							xmlString.Append("contains(");
							xmlString.Append(ArgumentObject.GetArgumentInfo(((FilterObject)(filters.Items[i])).ArgumentTypeMask));
							xmlString.Append(",'");
							// Retrieve the value up to the first OR_DELIMITER.
							// If there is no more delimiters, the current Value goes to the end of the string.
							if (valueHolder.IndexOf(OR_DELIMITER) != -1)
							{
								xmlString.Append(valueHolder.Substring(0, valueHolder.IndexOf(OR_DELIMITER)));
								xmlString.Append("') or ");

								// Update the valueHolder value.
								valueHolder = valueHolder.Substring(valueHolder.IndexOf(OR_DELIMITER) + OR_DELIMITER.Length);
							}
							else
							{
								xmlString.Append(valueHolder);
								xmlString.Append("')");

								valueHolder = String.Empty;
							}
						}
						while (valueHolder.Length > 0);

						xmlString.Append("\">");
					}
				}

				// Add the Dislay tag.
				xmlString.Append("<display>true</display>");

				// Add the parameters.
				xmlString.Append(ParameterObject.BuildParameterXml(parameters));

				// Add the ContentCondtions closing tags.
				for (Int16 i = 0; i < filters.Items.Count; i++)
				{
					xmlString.Append("</xsl:if>");
				}

				// Add main closing tags.
				xmlString.Append("</pagePixel>");
				xmlString.Append("</xsl:template>");
				xmlString.Append("</xsl:stylesheet>");

				return xmlString.ToString();
			}
		}
		#endregion

		#region Private Methods
		private void SetReadOnlyProperties(ArgumentObject.ArgumentTypeMaskEnum argumentTypeMask)
		{
			comparisonOperatorName = GetComparisonOperatorName(comparisonOperatorVal);

			switch (argumentTypeMask)
			{
				case ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID:
					filterTypeName = "Country Filter";
					key = "Country ID";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.GenderMask:
					filterTypeName = "GenderMask Filter";
					key = "GenderMask ID";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.PromotionID:
					filterTypeName = "Promotion Filter";
					key = "Promotion ID";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.BirthDate:
					filterTypeName = "Birth Date Filter";
					key = "Birth Date";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.MemberID:
					filterTypeName = "Member ID Filter";
					key = "Member ID";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.IsPayingMemberFlag:
					filterTypeName = "Subscribed Filter";
					key = "Is Subscribed";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.LandingPageURL:
					filterTypeName = "Landing Page URL Filter";
					key = "Landing Page URL";
					break;
				case ArgumentObject.ArgumentTypeMaskEnum.GenderMask_SearchPref:
					filterTypeName = "Search Preferences - GenderMask Filter";
					key = "Search Preferences - GenderMask ID";
					break;
				default:
					break;
			}
		}
		#endregion

		#region Public Properties
		public String FilterTypeName
		{
			get
			{
				return filterTypeName;
			}
		}

		public String Key
		{
			get
			{
				return key;
			}
		}

		public ArgumentObject.ArgumentTypeMaskEnum ArgumentTypeMask
		{
			get
			{
				return argumentTypeMaskVal;
			}
			set
			{
				argumentTypeMaskVal = value;
			}
		}

		public String ComparisonOperator
		{
			get
			{
				return comparisonOperatorVal;
			}
			set
			{
				comparisonOperatorVal = value;
			}
		}

		public String ComparisonOperatorName
		{
			get
			{
				return comparisonOperatorName;
			}
			set
			{
				comparisonOperatorName = value;
			}
		}

		public String Value
		{
			get
			{
				return val;
			}
			set
			{
				val = value;
			}
		}

		public String DisplayName
		{
			get
			{
				return filterTypeName + ": " + key + " " + comparisonOperatorName + " " + val;
			}
		}
		#endregion
	}
}
