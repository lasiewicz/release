using System;
using System.Windows.Forms;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for ItemData.
	/// </summary>
	public class ItemData
	{
		protected String _text;
		protected Object _value;

		public ItemData()
		{
		}

		//================================================================================
		public ItemData(String text, Object val)
		{
			_text = text;
			_value = val;
		}

		//================================================================================
		public override String ToString()
		{
			return _text;
		}

		//================================================================================
		public Object Value
		{
			get
			{
				return _value;
			}
		}

		//================================================================================
		public static void SetComboBoxValues(System.Windows.Forms.ComboBox cbo, ItemData[] items)
		{
			cbo.Items.Clear();
			for (int i = 0; i < items.Length; i++)
			{
				if (items[i] == null)
				{
					break;
				}
				else
				{
					cbo.Items.Add(items[i]);
				}
			}
		}


		public static void SetComboBoxValues(System.Windows.Forms.ComboBox cbo, ItemData[] itemData, Object defaultVal)
		{
			ItemData item;
			for (int i = 0; i < itemData.Length; i++)
			{
				item = itemData[i];
				cbo.Items.Add(item);

				if (item.Value == defaultVal)
					cbo.SelectedIndex = cbo.Items.Count - 1;
			}

			if (cbo.Items.Count > 0 && defaultVal.ToString() == Matchnet.Constants.NULL_INT.ToString() || defaultVal.ToString() == String.Empty)
				cbo.SelectedIndex = 0;
		}

		public static void SelectItem(System.Windows.Forms.ComboBox cbo, Object selectedVal)
		{
			ItemData item;
			for(int i = 0; i < cbo.Items.Count; i++)
			{
				item = (ItemData)cbo.Items[i];

				if (item.Value.ToString() == selectedVal.ToString())
				{
					cbo.SelectedIndex = i;
					return;
				}
			}
		}

		public static int GetItemIndex(ItemData[] itemData, Object val)
		{
			for (int i = 0; i < itemData.Length; i++)
			{
				if (itemData[i].Value == val)
					return i;
			}
			return -1;
		}
	}
}
