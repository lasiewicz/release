using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text;
using System.Threading;
using System.IO;
using System.Reflection;

using SparkNetworks.Administration.Web;
using SparkNetworks.Administration.Resources;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for MDIMain.
	/// </summary>
	public class MDIMain : System.Windows.Forms.Form
	{
		private const Int32 ArticleEditorPrivilegeID = 36;
		private const Int32 PixelEditorPrivilegeID = 126;
		private const Int32 PixelApproverPrivilegeID = 130;
		private const Int32 PrivilegeGroupEditorPrivilegeID = 6;
		private const Int32 PrivilegePrivilegeEditorPrivilegeID = 1;
		private const Int32 PrivilegeMemberPrivilegeEditorPrivilegeID = 2;
		private const Int32 PixelPublisherPrivilegeID = 140;

		private System.Windows.Forms.MainMenu mnuMain;
		private System.Windows.Forms.MenuItem mnuAdminister;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.MenuItem mnuArticles;
		private System.Windows.Forms.MenuItem mnuCategories;
		private System.Windows.Forms.MenuItem mnuPixels;

		private Settings _settings = new Settings();
		private Int32 memberID;
		private System.Windows.Forms.MenuItem mnuHelp;
		private System.Windows.Forms.MenuItem mnuPrivileges;
		private System.Windows.Forms.MenuItem itmPrivileges;
		private System.Windows.Forms.MenuItem itmGroups;
		private System.Windows.Forms.MenuItem itmMemberPrivileges;
		private System.Windows.Forms.MenuItem mnuResources;
		private Constants.RolesMaskEnum userRolesMask = Constants.RolesMaskEnum.None;

		public MDIMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose (bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}

			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mnuMain = new System.Windows.Forms.MainMenu();
			this.mnuAdminister = new System.Windows.Forms.MenuItem();
			this.mnuArticles = new System.Windows.Forms.MenuItem();
			this.mnuCategories = new System.Windows.Forms.MenuItem();
			this.mnuPixels = new System.Windows.Forms.MenuItem();
			this.mnuPrivileges = new System.Windows.Forms.MenuItem();
			this.itmPrivileges = new System.Windows.Forms.MenuItem();
			this.itmGroups = new System.Windows.Forms.MenuItem();
			this.itmMemberPrivileges = new System.Windows.Forms.MenuItem();
			this.mnuHelp = new System.Windows.Forms.MenuItem();
			this.mnuResources = new System.Windows.Forms.MenuItem();
			// 
			// mnuMain
			// 
			this.mnuMain.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					this.mnuAdminister,
																					this.mnuHelp});
			// 
			// mnuAdminister
			// 
			this.mnuAdminister.Index = 0;
			this.mnuAdminister.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						  this.mnuArticles,
																						  this.mnuCategories,
																						  this.mnuPixels,
																						  this.mnuPrivileges,
																						  this.mnuResources});
			this.mnuAdminister.Text = "&Administer";
			// 
			// mnuArticles
			// 
			this.mnuArticles.Index = 0;
			this.mnuArticles.Text = "&Articles";
			this.mnuArticles.Click += new System.EventHandler(this.mnuArticles_Click);
			// 
			// mnuCategories
			// 
			this.mnuCategories.Index = 1;
			this.mnuCategories.Text = "&Categories";
			this.mnuCategories.Click += new System.EventHandler(this.mnuCategories_Click);
			// 
			// mnuPixels
			// 
			this.mnuPixels.Index = 2;
			this.mnuPixels.Text = "&Pixels";
			this.mnuPixels.Click += new System.EventHandler(this.mnuPixels_Click);
			// 
			// mnuPrivileges
			// 
			this.mnuPrivileges.Index = 3;
			this.mnuPrivileges.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						  this.itmPrivileges,
																						  this.itmGroups,
																						  this.itmMemberPrivileges});
			this.mnuPrivileges.Text = "P&rivileges";
			// 
			// itmPrivileges
			// 
			this.itmPrivileges.Index = 0;
			this.itmPrivileges.Text = "&Privileges";
			this.itmPrivileges.Click += new System.EventHandler(this.itmPrivileges_Click);
			// 
			// itmGroups
			// 
			this.itmGroups.Index = 1;
			this.itmGroups.Text = "&Groups";
			this.itmGroups.Click += new System.EventHandler(this.itmGroups_Click);
			// 
			// itmMemberPrivileges
			// 
			this.itmMemberPrivileges.Index = 2;
			this.itmMemberPrivileges.Text = "&Member Privileges";
			this.itmMemberPrivileges.Click += new System.EventHandler(this.itmMemberPrivileges_Click);
			// 
			// mnuHelp
			// 
			this.mnuHelp.Index = 1;
			this.mnuHelp.Text = "Help";
			// 
			// mnuResources
			// 
			this.mnuResources.Index = 4;
			this.mnuResources.Text = "Resources";
			this.mnuResources.Click += new System.EventHandler(this.mnuResources_Click);
			// 
			// MDIMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(792, 553);
			this.IsMdiContainer = true;
			this.Menu = this.mnuMain;
			this.Name = "MDIMain";
			this.Text = "SparkNetworks Administration";
			this.Load += new System.EventHandler(this.MDIMain_Load);

		}
		#endregion

		[STAThread]
		static void Main() 
		{
			// HACK:  For some reason, if the following Trace.WriteLine is not included in the code then the SmartClient
			// takes over a minute to load.  With this line, the load takes only a few seconds.
			System.Diagnostics.Trace.WriteLine("SparkNetworks.Administration loading: " + DateTime.Now.ToString());
			
			Application.Run(new MDIMain());
		}

		private void MDIMain_Load(object sender, System.EventArgs e)
		{
			if (Authenticate())
			{
				this.Visible = true;
				this.Refresh();

				_settings.LoadSettings(memberID, userRolesMask);

				Authorize(userRolesMask);
			}
			else
			{
				this.Close();
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void mnuArticles_Click(object sender, System.EventArgs e)
		{
			Articles frmArticles = new Articles();

			frmArticles.settings = _settings;
			frmArticles.MdiParent = this;
			frmArticles.Show();
		}

		private void mnuCategories_Click(object sender, System.EventArgs e)
		{
			Categories frmCategories = new Categories();

			frmCategories.settings = _settings;
			frmCategories.MdiParent = this;
			frmCategories.Show();
		}

		private void mnuPixels_Click(object sender, System.EventArgs e)
		{
			Pixels frmPixels = new Pixels();

			frmPixels.settings = _settings;
			frmPixels.MdiParent = this;
			frmPixels.Show();
		}

		private void itmPrivileges_Click(object sender, System.EventArgs e)
		{
			Privileges frmPrivileges = new Privileges();

			frmPrivileges.settings = _settings;
			frmPrivileges.MdiParent = this;
			frmPrivileges.Show();
		}

		private void itmGroups_Click(object sender, System.EventArgs e)
		{
			Groups frmGroups = new Groups();

			frmGroups.settings = _settings;
			frmGroups.MdiParent = this;
			frmGroups.Show();
		}

		private void itmMemberPrivileges_Click(object sender, System.EventArgs e)
		{
			MemberPrivileges frmMemberPrivileges = new MemberPrivileges();

			frmMemberPrivileges.settings = _settings;
			frmMemberPrivileges.MdiParent = this;
			frmMemberPrivileges.Show();
		}

		private bool Authenticate()
		{
			Web.Member[] members1 = null;
			Web.Member[] members2 = null;
			AppDomain.CurrentDomain.SetPrincipalPolicy(System.Security.Principal.PrincipalPolicy.WindowsPrincipal);

			// Check to make sure they are authenticated.
			if (!Thread.CurrentPrincipal.Identity.IsAuthenticated)
			{
				MessageBox.Show(Constants.MSG_NOT_AUTHENTICATED, Constants.MSGBOXCAPTION_AUTHENTICATIONERROR);
				return false;
			}

			String username = Thread.CurrentPrincipal.Identity.Name;
			username = username.ToLower().Substring(username.IndexOf('\\') + 1);

			// Check to make sure the user has a valid MemberID.
			Int32 notUsed = new Int32();
			
			// Get privileges from both @spark.net and @matchnet.com.
			members1 = _settings.ClientAPI.GetMembersByEmail(username + "@" + Constants.DOMAIN_EMAIL, true);
			CheckPrivileges(members1);
			members2 = _settings.ClientAPI.GetMembersByEmail(username + "@" + Constants.DOMAIN_EMAIL2, true);
			CheckPrivileges(members2);
				
			if (members1.Length == 0 && members2.Length == 0)
			{
				MessageBox.Show(Constants.MSG_NO_MEMBERID, Constants.MSGBOXCAPTION_AUTHENTICATIONERROR);
				return false;
			}

			if (userRolesMask == 0)
			{
				MessageBox.Show(Constants.MSG_NOT_AUTHORIZED, Constants.MSGBOXCAPTION_AUTHENTICATIONERROR);
				return false;
			}
			
			return true;
		}

		private void CheckPrivileges(Web.Member[] members)
		{
			if (members.Length > 0)
			{
				memberID = members[0].MemberID;
				Web.MemberPrivilege[] memberPrivileges = _settings.ClientAPI.GetMemberPrivileges(memberID);
			
				if (CheckMemberPrivilege(memberPrivileges, ArticleEditorPrivilegeID))
				{
					userRolesMask = userRolesMask | Constants.RolesMaskEnum.ArticleEditor;
				}

				if (CheckMemberPrivilege(memberPrivileges, PixelEditorPrivilegeID))
				{
					userRolesMask = userRolesMask | Constants.RolesMaskEnum.PixelEditor;
				}

				if (CheckMemberPrivilege(memberPrivileges, PixelPublisherPrivilegeID))
				{
					userRolesMask = userRolesMask | Constants.RolesMaskEnum.PixelPublisher;
				}

				if (CheckMemberPrivilege(memberPrivileges, PixelApproverPrivilegeID))
				{
					userRolesMask = userRolesMask | Constants.RolesMaskEnum.PixelApprover;
				}

				// This functionality is not yet available.
				//			if (CheckMemberPrivilege(memberPrivileges, PrivilegeGroupEditorPrivilegeID))
				//			{
				//				userRolesMask = userRolesMask | Constants.RolesMaskEnum.PrivilegeGroupEditor;
				//			}
				//
				//			if (CheckMemberPrivilege(memberPrivileges, PrivilegePrivilegeEditorPrivilegeID))
				//			{
				//				userRolesMask = userRolesMask | Constants.RolesMaskEnum.PrivilegePrivilegeEditor;
				//			}

				if (CheckMemberPrivilege(memberPrivileges, PrivilegeMemberPrivilegeEditorPrivilegeID))
				{
					userRolesMask = userRolesMask | Constants.RolesMaskEnum.PrivilegeMemberPrivilegeEditor;
				}
			}
		}

		public Boolean CheckMemberPrivilege(MemberPrivilege[] memberPrivileges, Int32 privilegeID)
		{
			Boolean result = false;

			for (Int32 i = 0; i < memberPrivileges.Length; i++)
			{
				Web.MemberPrivilege memberPrivilege = memberPrivileges[i];
				if (memberPrivilege.Privilege.PrivilegeID == privilegeID)
				{
					if (memberPrivilege.PrivilegeState == Web.PrivilegeState.Individual || memberPrivilege.PrivilegeState == Web.PrivilegeState.SecurityGroup)
					{
						return true;
					}
				}
			}

			return result;
		}

		private void Authorize(Constants.RolesMaskEnum userRolesMask)
		{
			if (!_settings.IsContentEditingAllowed)
			{
				mnuArticles.Visible = false;
				mnuCategories.Visible = false;
				mnuPixels.Visible = false;
			}

			if (!_settings.User.IsArticleEditor)
			{
				mnuArticles.Visible = false;
				mnuCategories.Visible = false;
			}

			if (!_settings.User.IsPixelEditor && !_settings.User.IsPixelApprover && !_settings.User.IsPixelPublisher)
				mnuPixels.Visible = false;

			if (!_settings.User.IsPrivilegeGroupEditor && !_settings.User.IsPrivilegeMemberPrivilegeEditor && !_settings.User.IsPrivilegePrivilegeEditor)
			{
				mnuPrivileges.Visible = false;
			}
//			else	// This functionality is not yet available.
//			{
//				itmPrivileges.Visible = false;
//				itmGroups.Visible = false;
//				itmMemberPrivileges.Visible = false;
//
//				if (_settings.User.IsPrivilegeGroupEditor)
//					itmGroups.Visible = true;
//
//				if (_settings.User.IsPrivilegeMemberPrivilegeEditor)
//					itmMemberPrivileges.Visible = true;
//
//				if (_settings.User.IsPrivilegePrivilegeEditor)
//					itmPrivileges.Visible = true;
//			}
		}

		private void mnuResources_Click(object sender, System.EventArgs e)
		{
			Resources.Resources frmResources = new Resources.Resources();

			frmResources.settings = _settings;
			frmResources.MdiParent = this;
			frmResources.Show();
		}
	}
}
