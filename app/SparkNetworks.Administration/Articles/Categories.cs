using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for Categories.
	/// </summary>
	public class Categories : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TreeView tvCategories;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private Settings _settings;
		private System.Windows.Forms.Label lblSite;
		private System.Windows.Forms.ComboBox cboSite;
		private System.Windows.Forms.LinkLabel lnkEditSiteCategory;
		private System.Windows.Forms.LinkLabel lnkAddCategory;
		private System.Windows.Forms.MenuItem mnuAddCategory;
		private System.Windows.Forms.Label lblCategories;
		private System.Windows.Forms.MenuItem mnuCategories;
		private System.Windows.Forms.MenuItem mnuRefreshCategories;
		private System.Windows.Forms.MenuItem mnuDiv1;
		private System.Windows.Forms.MainMenu mainMenu;
		private System.Windows.Forms.LinkLabel lnkLoadCategories;
		private System.Windows.Forms.MenuItem mnuEditSiteCategory;

		public Categories()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool Disposing)
		{
			if( Disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(Disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lnkAddCategory = new System.Windows.Forms.LinkLabel();
			this.lnkEditSiteCategory = new System.Windows.Forms.LinkLabel();
			this.lnkLoadCategories = new System.Windows.Forms.LinkLabel();
			this.tvCategories = new System.Windows.Forms.TreeView();
			this.lblCategories = new System.Windows.Forms.Label();
			this.lblSite = new System.Windows.Forms.Label();
			this.mainMenu = new System.Windows.Forms.MainMenu();
			this.mnuCategories = new System.Windows.Forms.MenuItem();
			this.mnuAddCategory = new System.Windows.Forms.MenuItem();
			this.mnuEditSiteCategory = new System.Windows.Forms.MenuItem();
			this.mnuDiv1 = new System.Windows.Forms.MenuItem();
			this.mnuRefreshCategories = new System.Windows.Forms.MenuItem();
			this.cboSite = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// lnkAddCategory
			// 
			this.lnkAddCategory.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
			this.lnkAddCategory.Location = new System.Drawing.Point(154, 43);
			this.lnkAddCategory.Name = "lnkAddCategory";
			this.lnkAddCategory.Size = new System.Drawing.Size(76, 20);
			this.lnkAddCategory.TabIndex = 24;
			this.lnkAddCategory.TabStop = true;
			this.lnkAddCategory.Text = "Add Category";
			this.lnkAddCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lnkAddCategory.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkAddCategory_LinkClicked);
			// 
			// lnkEditSiteCategory
			// 
			this.lnkEditSiteCategory.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
			this.lnkEditSiteCategory.Location = new System.Drawing.Point(266, 43);
			this.lnkEditSiteCategory.Name = "lnkEditSiteCategory";
			this.lnkEditSiteCategory.Size = new System.Drawing.Size(92, 20);
			this.lnkEditSiteCategory.TabIndex = 26;
			this.lnkEditSiteCategory.TabStop = true;
			this.lnkEditSiteCategory.Text = "Edit SiteCategory";
			this.lnkEditSiteCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lnkEditSiteCategory.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkEditSiteCategory_LinkClicked);
			// 
			// lnkLoadCategories
			// 
			this.lnkLoadCategories.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
			this.lnkLoadCategories.Location = new System.Drawing.Point(30, 43);
			this.lnkLoadCategories.Name = "lnkLoadCategories";
			this.lnkLoadCategories.Size = new System.Drawing.Size(108, 20);
			this.lnkLoadCategories.TabIndex = 23;
			this.lnkLoadCategories.TabStop = true;
			this.lnkLoadCategories.Text = "Refresh Categories";
			this.lnkLoadCategories.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lnkLoadCategories.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLoadCategories_LinkClicked);
			// 
			// tvCategories
			// 
			this.tvCategories.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.tvCategories.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.tvCategories.HideSelection = false;
			this.tvCategories.ImageIndex = -1;
			this.tvCategories.Location = new System.Drawing.Point(10, 92);
			this.tvCategories.Name = "tvCategories";
			this.tvCategories.SelectedImageIndex = -1;
			this.tvCategories.Size = new System.Drawing.Size(774, 471);
			this.tvCategories.TabIndex = 29;
			this.tvCategories.DoubleClick += new System.EventHandler(this.tvCategories_DoubleClick);
			// 
			// lblCategories
			// 
			this.lblCategories.Location = new System.Drawing.Point(10, 72);
			this.lblCategories.Name = "lblCategories";
			this.lblCategories.Size = new System.Drawing.Size(96, 18);
			this.lblCategories.TabIndex = 32;
			this.lblCategories.Text = "Categories:";
			// 
			// lblSite
			// 
			this.lblSite.Location = new System.Drawing.Point(-13, 10);
			this.lblSite.Name = "lblSite";
			this.lblSite.Size = new System.Drawing.Size(89, 21);
			this.lblSite.TabIndex = 33;
			this.lblSite.Text = "&Site:";
			this.lblSite.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// mainMenu
			// 
			this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.mnuCategories});
			// 
			// mnuCategories
			// 
			this.mnuCategories.Index = 0;
			this.mnuCategories.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						  this.mnuAddCategory,
																						  this.mnuEditSiteCategory,
																						  this.mnuDiv1,
																						  this.mnuRefreshCategories});
			this.mnuCategories.Text = "&Categories";
			// 
			// mnuAddCategory
			// 
			this.mnuAddCategory.Index = 0;
			this.mnuAddCategory.Text = "&Add Category";
			this.mnuAddCategory.Click += new System.EventHandler(this.mnuAddCategory_Click);
			// 
			// mnuEditSiteCategory
			// 
			this.mnuEditSiteCategory.Index = 1;
			this.mnuEditSiteCategory.Text = "&Edit SiteCategory";
			this.mnuEditSiteCategory.Click += new System.EventHandler(this.mnuEditSiteCategory_Click);
			// 
			// mnuDiv1
			// 
			this.mnuDiv1.Index = 2;
			this.mnuDiv1.Text = "-";
			// 
			// mnuRefreshCategories
			// 
			this.mnuRefreshCategories.Index = 3;
			this.mnuRefreshCategories.Shortcut = System.Windows.Forms.Shortcut.F5;
			this.mnuRefreshCategories.Text = "&Refresh Categories";
			this.mnuRefreshCategories.Click += new System.EventHandler(this.mnuRefresh_Click);
			// 
			// cboSite
			// 
			this.cboSite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboSite.Location = new System.Drawing.Point(76, 10);
			this.cboSite.Name = "cboSite";
			this.cboSite.Size = new System.Drawing.Size(240, 21);
			this.cboSite.TabIndex = 37;
			this.cboSite.SelectedIndexChanged += new System.EventHandler(this.cboSite_SelectedIndexChanged);
			// 
			// Categories
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(792, 573);
			this.Controls.Add(this.cboSite);
			this.Controls.Add(this.lblSite);
			this.Controls.Add(this.lnkAddCategory);
			this.Controls.Add(this.lnkEditSiteCategory);
			this.Controls.Add(this.lnkLoadCategories);
			this.Controls.Add(this.tvCategories);
			this.Controls.Add(this.lblCategories);
			this.Menu = this.mainMenu;
			this.Name = "Categories";
			this.Text = "Categories";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Load += new System.EventHandler(this.Categories_Load);
			this.ResumeLayout(false);

		}
		#endregion

		public Settings settings
		{
			set
			{
				_settings = value;
				ItemData.SetComboBoxValues(cboSite, _settings.Sites, 2);
			}
		}

		private void Categories_Load(object sender, System.EventArgs e)
		{
			this.Visible = true;
			this.Refresh();

			Reload();
		}

		public void Add()
		{
			int parentCategoryID = int.MinValue;

			// Set the parent if available.
			SiteCategoryNode node = (SiteCategoryNode)tvCategories.SelectedNode;
			if (node != null)
			{
				parentCategoryID = node.CategoryID;
			}

			Category frm = new Category(int.MinValue, parentCategoryID);
			frm.settings = _settings;

			if (frm.ShowDialog() == DialogResult.OK)
			{
				Reload();
			}
		}

		public void Reload()
		{
			tvCategories.Nodes.Clear();
			SiteCategoryNode rootNode = new SiteCategoryNode("[Categories]", int.MinValue);
			tvCategories.Nodes.Add(rootNode);

			ItemData item = (ItemData)cboSite.SelectedItem;

			if (item == null)
				return;
			
			try
			{
				Web.SiteCategory[] siteCategories = _settings.ClientAPI.RetrieveSiteCategories(Convert.ToInt32(item.Value), true);

				for (Int32 i = 0; i < siteCategories.Length; i++)
				{
					Web.SiteCategory siteCategory = siteCategories[i];

					String strCaption = siteCategory.Content + "[" + siteCategory.CategoryID + "]";

					AddCategoryNode(rootNode, siteCategory);
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show("Error: " + ex.Message, "ArticleSA");
			}
			
			tvCategories.Nodes[0].Expand();
		}

		public static bool AddCategoryNode(SiteCategoryNode searchNode, Web.SiteCategory VOSiteCategory)
		{
			SiteCategoryNode newNode;

			if (searchNode.CategoryID == VOSiteCategory.Category.ParentCategoryID)
			{
				newNode = new SiteCategoryNode(VOSiteCategory);
				
				searchNode.Nodes.Add(newNode);
				
				return true;
			}

			foreach(SiteCategoryNode node in searchNode.Nodes)
			{
				if (node.CategoryID == VOSiteCategory.Category.ParentCategoryID)
				{
					newNode = new SiteCategoryNode(VOSiteCategory);

					node.Nodes.Add(newNode);

					return true;
				}
				else
				{
					if (AddCategoryNode(node, VOSiteCategory))
						return true;
				}
			}

			return false;
		}

		public void Edit()
		{	
			SiteCategoryNode node = (SiteCategoryNode)tvCategories.SelectedNode;
			if (node != null)
			{
				if (node.CategoryID == int.MinValue)
					return;

				SiteCategory frm = new SiteCategory(node.VOSiteCategory);
				frm.settings = _settings;
				if (frm.ShowDialog() == DialogResult.OK)
				{
					Reload();
				}
			}
			else
			{
				MessageBox.Show("Please select the SiteCategory from the Categories Tree View that you would like to edit.", "Category Not Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void lnkLoadCategories_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			Reload();
		}

		private void lnkAddCategory_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			Add();
		}

		private void lnkEditSiteCategory_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			Edit();
		}

		private void tvCategories_DoubleClick(object sender, System.EventArgs e)
		{
			Edit();
		}

		private void mnuAddCategory_Click(object sender, System.EventArgs e)
		{
			Add();
		}

		private void mnuEditSiteCategory_Click(object sender, System.EventArgs e)
		{
			Edit();
		}

		private void mnuRefresh_Click(object sender, System.EventArgs e)
		{
			Reload();
		}

		private void cboSite_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Reload();
		}
	}

	public class SiteCategoryNode : TreeNode
	{
		private int _categoryID;
		private Web.SiteCategory _voSiteCategory;

		public SiteCategoryNode(string Caption, int CategoryID) : base(Caption)
		{
			_categoryID = CategoryID;
		}

		public SiteCategoryNode(Web.SiteCategory VOSiteCategory) : base(VOSiteCategory.Content + "[" + VOSiteCategory.CategoryID.ToString() + "]")
		{
			_categoryID = VOSiteCategory.CategoryID;
			_voSiteCategory = VOSiteCategory;

			// If a value doesn not existfor the SiteCategoryID then make it regular.
			// KNOWN ISSUE:  Clipping occurs if we make the default font for the tree as regular and then try
			// to bold individual nodes.  Doing this in reverse avoids this problem.
			if (VOSiteCategory.SiteCategoryID == int.MinValue)
				this.NodeFont = new Font("Microsoft Sans Serif", (float)8.25, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			// If the base Category is not published, gray it out.
			if (VOSiteCategory.PublishedFlag == false)
				this.ForeColor = System.Drawing.Color.Gray;
		}

		public int CategoryID
		{
			get { return _categoryID; }
		}

		public Web.SiteCategory VOSiteCategory
		{
			get { return _voSiteCategory; }
		}
	}
}
