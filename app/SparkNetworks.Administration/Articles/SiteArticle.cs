using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for SiteArticle.
	/// </summary>
	public class SiteArticle : System.Windows.Forms.Form
	{
		private System.ComponentModel.Container components = null;
		private Settings _settings;
		private Web.SiteArticle _voSiteArticle;
		private System.Windows.Forms.GroupBox gbxButtons;
		private System.Windows.Forms.Button cmdCancel;
		private System.Windows.Forms.Button cmdSave;
		private System.Windows.Forms.Label lblContent;
		private System.Windows.Forms.CheckBox cbxPublishedFlag;
		private System.Windows.Forms.TextBox tbxLastUpdated;
		private System.Windows.Forms.RichTextBox tbxContent;
		private System.Windows.Forms.Label lblLastUpdated;
		private System.Windows.Forms.TextBox tbxSiteID;
		private System.Windows.Forms.Label lblSiteID;
		private System.Windows.Forms.Label lblSiteArticleID;
		private System.Windows.Forms.TextBox tbxSiteArticleID;
		private System.Windows.Forms.TextBox tbxArticleID;
		private System.Windows.Forms.Label lblArticleID;
		private System.Windows.Forms.TextBox tbxMemberID;
		private System.Windows.Forms.Label lblMemberID;
		private System.Windows.Forms.TextBox tbxTitle;
		private System.Windows.Forms.GroupBox gbxArticle;
		private System.Windows.Forms.Label lblFileID;
		private System.Windows.Forms.TextBox tbxFileID;
		private System.Windows.Forms.TextBox tbxOrdinal;
		private System.Windows.Forms.Label lblOrdinal;
		private System.Windows.Forms.CheckBox cbxFeaturedFlag;
		private System.Windows.Forms.TextBox tbxConstant;
		private System.Windows.Forms.Label lblConstant;
		private System.Windows.Forms.Button btnEditArticle;
		private System.Windows.Forms.Label lblTitle;

		public SiteArticle(Web.SiteArticle VOSiteArticle)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			_voSiteArticle = VOSiteArticle;
		}

		public Settings settings
		{
			set
			{
				_settings = value;
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.gbxButtons = new System.Windows.Forms.GroupBox();
			this.cmdCancel = new System.Windows.Forms.Button();
			this.cmdSave = new System.Windows.Forms.Button();
			this.lblContent = new System.Windows.Forms.Label();
			this.lblFileID = new System.Windows.Forms.Label();
			this.cbxPublishedFlag = new System.Windows.Forms.CheckBox();
			this.tbxFileID = new System.Windows.Forms.TextBox();
			this.tbxLastUpdated = new System.Windows.Forms.TextBox();
			this.tbxContent = new System.Windows.Forms.RichTextBox();
			this.lblLastUpdated = new System.Windows.Forms.Label();
			this.tbxSiteID = new System.Windows.Forms.TextBox();
			this.lblSiteID = new System.Windows.Forms.Label();
			this.gbxArticle = new System.Windows.Forms.GroupBox();
			this.tbxConstant = new System.Windows.Forms.TextBox();
			this.lblConstant = new System.Windows.Forms.Label();
			this.tbxSiteArticleID = new System.Windows.Forms.TextBox();
			this.lblSiteArticleID = new System.Windows.Forms.Label();
			this.tbxArticleID = new System.Windows.Forms.TextBox();
			this.lblArticleID = new System.Windows.Forms.Label();
			this.tbxMemberID = new System.Windows.Forms.TextBox();
			this.lblMemberID = new System.Windows.Forms.Label();
			this.tbxTitle = new System.Windows.Forms.TextBox();
			this.lblTitle = new System.Windows.Forms.Label();
			this.tbxOrdinal = new System.Windows.Forms.TextBox();
			this.lblOrdinal = new System.Windows.Forms.Label();
			this.cbxFeaturedFlag = new System.Windows.Forms.CheckBox();
			this.btnEditArticle = new System.Windows.Forms.Button();
			this.gbxArticle.SuspendLayout();
			this.SuspendLayout();
			// 
			// gbxButtons
			// 
			this.gbxButtons.Location = new System.Drawing.Point(0, 456);
			this.gbxButtons.Name = "gbxButtons";
			this.gbxButtons.Size = new System.Drawing.Size(832, 4);
			this.gbxButtons.TabIndex = 30;
			this.gbxButtons.TabStop = false;
			this.gbxButtons.Text = "gbxButtons";
			// 
			// cmdCancel
			// 
			this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cmdCancel.Location = new System.Drawing.Point(624, 472);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(81, 28);
			this.cmdCancel.TabIndex = 4;
			this.cmdCancel.Text = "&Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.Location = new System.Drawing.Point(536, 472);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Size = new System.Drawing.Size(81, 28);
			this.cmdSave.TabIndex = 3;
			this.cmdSave.Text = "&Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// lblContent
			// 
			this.lblContent.Location = new System.Drawing.Point(24, 328);
			this.lblContent.Name = "lblContent";
			this.lblContent.Size = new System.Drawing.Size(72, 16);
			this.lblContent.TabIndex = 29;
			this.lblContent.Text = "Content:";
			this.lblContent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblFileID
			// 
			this.lblFileID.Location = new System.Drawing.Point(40, 352);
			this.lblFileID.Name = "lblFileID";
			this.lblFileID.Size = new System.Drawing.Size(56, 16);
			this.lblFileID.TabIndex = 27;
			this.lblFileID.Text = "FileID:";
			this.lblFileID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// cbxPublishedFlag
			// 
			this.cbxPublishedFlag.Location = new System.Drawing.Point(96, 400);
			this.cbxPublishedFlag.Name = "cbxPublishedFlag";
			this.cbxPublishedFlag.Size = new System.Drawing.Size(80, 24);
			this.cbxPublishedFlag.TabIndex = 57;
			this.cbxPublishedFlag.Text = "Published";
			// 
			// tbxFileID
			// 
			this.tbxFileID.Location = new System.Drawing.Point(96, 352);
			this.tbxFileID.Name = "tbxFileID";
			this.tbxFileID.Size = new System.Drawing.Size(144, 20);
			this.tbxFileID.TabIndex = 55;
			this.tbxFileID.Text = "";
			// 
			// tbxLastUpdated
			// 
			this.tbxLastUpdated.Location = new System.Drawing.Point(96, 424);
			this.tbxLastUpdated.Name = "tbxLastUpdated";
			this.tbxLastUpdated.ReadOnly = true;
			this.tbxLastUpdated.Size = new System.Drawing.Size(320, 20);
			this.tbxLastUpdated.TabIndex = 39;
			this.tbxLastUpdated.Text = "";
			// 
			// tbxContent
			// 
			this.tbxContent.Location = new System.Drawing.Point(96, 208);
			this.tbxContent.MaxLength = 131068;
			this.tbxContent.Name = "tbxContent";
			this.tbxContent.Size = new System.Drawing.Size(608, 136);
			this.tbxContent.TabIndex = 54;
			this.tbxContent.Text = "";
			// 
			// lblLastUpdated
			// 
			this.lblLastUpdated.Location = new System.Drawing.Point(8, 424);
			this.lblLastUpdated.Name = "lblLastUpdated";
			this.lblLastUpdated.Size = new System.Drawing.Size(89, 16);
			this.lblLastUpdated.TabIndex = 41;
			this.lblLastUpdated.Text = "Last Updated:";
			this.lblLastUpdated.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxSiteID
			// 
			this.tbxSiteID.Location = new System.Drawing.Point(96, 136);
			this.tbxSiteID.Name = "tbxSiteID";
			this.tbxSiteID.ReadOnly = true;
			this.tbxSiteID.Size = new System.Drawing.Size(144, 20);
			this.tbxSiteID.TabIndex = 43;
			this.tbxSiteID.Text = "";
			// 
			// lblSiteID
			// 
			this.lblSiteID.Location = new System.Drawing.Point(8, 136);
			this.lblSiteID.Name = "lblSiteID";
			this.lblSiteID.Size = new System.Drawing.Size(89, 16);
			this.lblSiteID.TabIndex = 42;
			this.lblSiteID.Text = "SiteID:";
			this.lblSiteID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// gbxArticle
			// 
			this.gbxArticle.Controls.Add(this.tbxConstant);
			this.gbxArticle.Controls.Add(this.lblConstant);
			this.gbxArticle.Location = new System.Drawing.Point(8, 32);
			this.gbxArticle.Name = "gbxArticle";
			this.gbxArticle.Size = new System.Drawing.Size(256, 80);
			this.gbxArticle.TabIndex = 45;
			this.gbxArticle.TabStop = false;
			this.gbxArticle.Text = "Article Attributes";
			// 
			// tbxConstant
			// 
			this.tbxConstant.Location = new System.Drawing.Point(88, 48);
			this.tbxConstant.Name = "tbxConstant";
			this.tbxConstant.ReadOnly = true;
			this.tbxConstant.Size = new System.Drawing.Size(144, 20);
			this.tbxConstant.TabIndex = 60;
			this.tbxConstant.Text = "";
			// 
			// lblConstant
			// 
			this.lblConstant.Location = new System.Drawing.Point(24, 48);
			this.lblConstant.Name = "lblConstant";
			this.lblConstant.Size = new System.Drawing.Size(64, 16);
			this.lblConstant.TabIndex = 59;
			this.lblConstant.Text = "Constant:";
			this.lblConstant.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxSiteArticleID
			// 
			this.tbxSiteArticleID.Location = new System.Drawing.Point(96, 8);
			this.tbxSiteArticleID.Name = "tbxSiteArticleID";
			this.tbxSiteArticleID.ReadOnly = true;
			this.tbxSiteArticleID.Size = new System.Drawing.Size(144, 20);
			this.tbxSiteArticleID.TabIndex = 47;
			this.tbxSiteArticleID.Text = "";
			// 
			// lblSiteArticleID
			// 
			this.lblSiteArticleID.Location = new System.Drawing.Point(8, 8);
			this.lblSiteArticleID.Name = "lblSiteArticleID";
			this.lblSiteArticleID.Size = new System.Drawing.Size(89, 16);
			this.lblSiteArticleID.TabIndex = 46;
			this.lblSiteArticleID.Text = "SiteArticleID:";
			this.lblSiteArticleID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxArticleID
			// 
			this.tbxArticleID.Location = new System.Drawing.Point(96, 56);
			this.tbxArticleID.Name = "tbxArticleID";
			this.tbxArticleID.ReadOnly = true;
			this.tbxArticleID.Size = new System.Drawing.Size(144, 20);
			this.tbxArticleID.TabIndex = 49;
			this.tbxArticleID.Text = "";
			// 
			// lblArticleID
			// 
			this.lblArticleID.Location = new System.Drawing.Point(32, 56);
			this.lblArticleID.Name = "lblArticleID";
			this.lblArticleID.Size = new System.Drawing.Size(64, 16);
			this.lblArticleID.TabIndex = 48;
			this.lblArticleID.Text = "ArticleID:";
			this.lblArticleID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxMemberID
			// 
			this.tbxMemberID.Location = new System.Drawing.Point(96, 160);
			this.tbxMemberID.Name = "tbxMemberID";
			this.tbxMemberID.ReadOnly = true;
			this.tbxMemberID.Size = new System.Drawing.Size(144, 20);
			this.tbxMemberID.TabIndex = 51;
			this.tbxMemberID.Text = "";
			// 
			// lblMemberID
			// 
			this.lblMemberID.Location = new System.Drawing.Point(8, 160);
			this.lblMemberID.Name = "lblMemberID";
			this.lblMemberID.Size = new System.Drawing.Size(89, 16);
			this.lblMemberID.TabIndex = 50;
			this.lblMemberID.Text = "MemberID:";
			this.lblMemberID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxTitle
			// 
			this.tbxTitle.Location = new System.Drawing.Point(96, 184);
			this.tbxTitle.Name = "tbxTitle";
			this.tbxTitle.Size = new System.Drawing.Size(608, 20);
			this.tbxTitle.TabIndex = 53;
			this.tbxTitle.Text = "";
			// 
			// lblTitle
			// 
			this.lblTitle.Location = new System.Drawing.Point(8, 184);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(89, 16);
			this.lblTitle.TabIndex = 52;
			this.lblTitle.Text = "Title:";
			this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxOrdinal
			// 
			this.tbxOrdinal.Location = new System.Drawing.Point(96, 376);
			this.tbxOrdinal.Name = "tbxOrdinal";
			this.tbxOrdinal.Size = new System.Drawing.Size(144, 20);
			this.tbxOrdinal.TabIndex = 56;
			this.tbxOrdinal.Text = "";
			// 
			// lblOrdinal
			// 
			this.lblOrdinal.Location = new System.Drawing.Point(24, 376);
			this.lblOrdinal.Name = "lblOrdinal";
			this.lblOrdinal.Size = new System.Drawing.Size(72, 16);
			this.lblOrdinal.TabIndex = 54;
			this.lblOrdinal.Text = "OrdinalID:";
			this.lblOrdinal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// cbxFeaturedFlag
			// 
			this.cbxFeaturedFlag.Location = new System.Drawing.Point(184, 400);
			this.cbxFeaturedFlag.Name = "cbxFeaturedFlag";
			this.cbxFeaturedFlag.Size = new System.Drawing.Size(80, 24);
			this.cbxFeaturedFlag.TabIndex = 58;
			this.cbxFeaturedFlag.Text = "Featured";
			// 
			// btnEditArticle
			// 
			this.btnEditArticle.Location = new System.Drawing.Point(712, 472);
			this.btnEditArticle.Name = "btnEditArticle";
			this.btnEditArticle.Size = new System.Drawing.Size(81, 28);
			this.btnEditArticle.TabIndex = 59;
			this.btnEditArticle.Text = "&Edit Article";
			this.btnEditArticle.Click += new System.EventHandler(this.btnEditArticle_Click);
			// 
			// SiteArticle
			// 
			this.AcceptButton = this.cmdSave;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.cmdCancel;
			this.ClientSize = new System.Drawing.Size(802, 511);
			this.Controls.Add(this.btnEditArticle);
			this.Controls.Add(this.cbxFeaturedFlag);
			this.Controls.Add(this.tbxOrdinal);
			this.Controls.Add(this.tbxTitle);
			this.Controls.Add(this.tbxMemberID);
			this.Controls.Add(this.tbxArticleID);
			this.Controls.Add(this.tbxSiteArticleID);
			this.Controls.Add(this.tbxSiteID);
			this.Controls.Add(this.tbxLastUpdated);
			this.Controls.Add(this.tbxFileID);
			this.Controls.Add(this.tbxContent);
			this.Controls.Add(this.lblOrdinal);
			this.Controls.Add(this.lblTitle);
			this.Controls.Add(this.lblMemberID);
			this.Controls.Add(this.lblArticleID);
			this.Controls.Add(this.lblSiteArticleID);
			this.Controls.Add(this.lblSiteID);
			this.Controls.Add(this.lblLastUpdated);
			this.Controls.Add(this.cbxPublishedFlag);
			this.Controls.Add(this.gbxButtons);
			this.Controls.Add(this.cmdCancel);
			this.Controls.Add(this.cmdSave);
			this.Controls.Add(this.lblContent);
			this.Controls.Add(this.lblFileID);
			this.Controls.Add(this.gbxArticle);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SiteArticle";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Edit SiteArticle";
			this.Load += new System.EventHandler(this.SiteArticle_Load);
			this.gbxArticle.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private bool SaveSiteArticle()
		{
			try
			{
				int ret = _settings.ClientAPI.SaveSiteArticle(BuildVOSiteArticle());

				if (ret > 0)
					return true;
				else
					return false;
			}
			catch(System.Data.SqlClient.SqlException ex)
			{
				MessageBox.Show("Error: " + ex.Message, "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Information);

				return false;
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			if (tbxContent.Text == string.Empty)
			{
				MessageBox.Show("Error: Please enter English content",  "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				if (SaveSiteArticle())
				{
					DialogResult = DialogResult.OK;
					this.Close();
				}
				else
				{
					DialogResult = DialogResult.Cancel;
					this.Close();
				}
			}
		}

		private void SiteArticle_Load(object sender, System.EventArgs e)
		{
			DisplaySiteArticle();
		}

		private void DisplaySiteArticle()
		{
			tbxSiteArticleID.Text = _voSiteArticle.SiteArticleID.ToString();
			tbxArticleID.Text = _voSiteArticle.ArticleID.ToString();
			tbxSiteID.Text = _voSiteArticle.SiteID.ToString();
			tbxConstant.Text = _voSiteArticle.Article.Constant;
			tbxMemberID.Text = _voSiteArticle.MemberID.ToString();
			tbxTitle.Text = _voSiteArticle.Title;
			tbxContent.Text = _voSiteArticle.Content;
			tbxFileID.Text = _voSiteArticle.FileID.ToString();
			tbxOrdinal.Text = _voSiteArticle.Ordinal.ToString();
			tbxLastUpdated.Text = _voSiteArticle.LastUpdated.ToString();
			cbxPublishedFlag.Checked = _voSiteArticle.PublishedFlag;
			cbxFeaturedFlag.Checked = _voSiteArticle.FeaturedFlag;
		}

		private Web.SiteArticle BuildVOSiteArticle()
		{
			Web.SiteArticle siteArticle = new Web.SiteArticle();

			siteArticle.SiteArticleID = Convert.ToInt32(tbxSiteArticleID.Text.Trim());
			siteArticle.ArticleID = Convert.ToInt32(tbxArticleID.Text.Trim());
			siteArticle.SiteID = Convert.ToInt32(tbxSiteID.Text.Trim());
			siteArticle.MemberID = Convert.ToInt32(tbxMemberID.Text.Trim());
			siteArticle.Title = tbxTitle.Text;
			siteArticle.Content = tbxContent.Text;
			siteArticle.FileID = Convert.ToInt32(tbxFileID.Text.Trim());
			siteArticle.Ordinal = Convert.ToInt32(tbxOrdinal.Text.Trim());
			siteArticle.PublishedFlag = cbxPublishedFlag.Checked;
			siteArticle.FeaturedFlag = cbxFeaturedFlag.Checked;

			return siteArticle;
		}

		private void btnEditArticle_Click(object sender, System.EventArgs e)
		{
			Article frm = new Article(_voSiteArticle.ArticleID, _settings);

			if (frm.ShowDialog() == DialogResult.OK)
			{
				this.DialogResult = DialogResult.OK;
			}
		}
	}
}
