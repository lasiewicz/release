using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for Article.
	/// </summary>
	public class Article : System.Windows.Forms.Form
	{
		private Settings _settings;
		private Web.Article _voArticle;
		private System.Windows.Forms.TextBox tbxArticleID;
		private System.Windows.Forms.TextBox tbxCategoryID;
		private System.Windows.Forms.RichTextBox tbxDefaultContent;
		private System.Windows.Forms.TextBox tbxConstant;
		private System.Windows.Forms.TextBox tbxLastUpdated;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Label lblArticleID;
		private System.Windows.Forms.Label lblCategoryID;
		private System.Windows.Forms.Label lblDefaultContent;
		private System.Windows.Forms.Label lblConstant;
		private System.Windows.Forms.Label lblLastUpdated;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Article()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		public Article(int ArticleID, Settings settings) : this()
		{
			_settings = settings;

			if (ArticleID != int.MinValue)
			{
				_voArticle = _settings.ClientAPI.RetrieveArticle(ArticleID);
			}
			else
			{
				_voArticle = new Web.Article();
				_voArticle.ArticleID = ArticleID;
			}
		}

		public Article(int CategoryID, bool CategoryIDFlag) : this()
		{
			_voArticle = new Web.Article();
			_voArticle.CategoryID = CategoryID;
		}

		public Settings settings
		{
			set
			{
				_settings = value;
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private bool SaveArticle()
		{
			try
			{
				int ret = _settings.ClientAPI.SaveArticle(BuildVOArticle());

				if (ret > 0)
					return true;
				else
					return false;
			}
			catch(Exception ex)
			{
				// TODO:  Move this error handler to the middle tier and make sure it propagates up so it can be handled here.
				//				if (ex.Number == 2601)
				//					MessageBox.Show("This constant is already in use. Please create a new constant and save again.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//				else
				MessageBox.Show("Error: " + ex.Message,  "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Information);

				return false;
			}
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			if (tbxConstant.Text == string.Empty)
			{
				MessageBox.Show("Error: Please enter English constant",  "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				if (SaveArticle())
				{
					DialogResult = DialogResult.OK;
					this.Close();
				}
			}
		}

		private void Article_Load(object sender, System.EventArgs e)
		{
			DisplayArticle();
		}

		/// <summary>
		/// Make the Constant upper-case.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tbxConstant_TextChanged(object sender, System.EventArgs e)
		{
			Constants.BuildConstant(ref tbxConstant);
		}

		private void DisplayArticle()
		{
			tbxArticleID.Text = _voArticle.ArticleID.ToString();
			tbxCategoryID.Text = _voArticle.CategoryID.ToString();
			tbxConstant.Text = _voArticle.Constant;
			tbxDefaultContent.Text = _voArticle.DefaultContent;
			tbxLastUpdated.Text = _voArticle.LastUpdated.ToString();
		}

		private Web.Article BuildVOArticle()
		{
			Web.Article article = new Web.Article();

			article.ArticleID = Convert.ToInt32(tbxArticleID.Text.Trim());
			article.CategoryID = Convert.ToInt32(tbxCategoryID.Text.Trim());
			article.DefaultContent = tbxDefaultContent.Text.Trim();
			article.Constant = tbxConstant.Text.Trim();

			return article; 
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblArticleID = new System.Windows.Forms.Label();
			this.lblCategoryID = new System.Windows.Forms.Label();
			this.lblDefaultContent = new System.Windows.Forms.Label();
			this.lblConstant = new System.Windows.Forms.Label();
			this.lblLastUpdated = new System.Windows.Forms.Label();
			this.tbxArticleID = new System.Windows.Forms.TextBox();
			this.tbxCategoryID = new System.Windows.Forms.TextBox();
			this.tbxDefaultContent = new System.Windows.Forms.RichTextBox();
			this.tbxConstant = new System.Windows.Forms.TextBox();
			this.tbxLastUpdated = new System.Windows.Forms.TextBox();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lblArticleID
			// 
			this.lblArticleID.Location = new System.Drawing.Point(8, 8);
			this.lblArticleID.Name = "lblArticleID";
			this.lblArticleID.Size = new System.Drawing.Size(100, 16);
			this.lblArticleID.TabIndex = 0;
			this.lblArticleID.Text = "ArticleID:";
			this.lblArticleID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblCategoryID
			// 
			this.lblCategoryID.Location = new System.Drawing.Point(8, 32);
			this.lblCategoryID.Name = "lblCategoryID";
			this.lblCategoryID.Size = new System.Drawing.Size(100, 16);
			this.lblCategoryID.TabIndex = 1;
			this.lblCategoryID.Text = "CategoryID:";
			this.lblCategoryID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblDefaultContent
			// 
			this.lblDefaultContent.Location = new System.Drawing.Point(8, 56);
			this.lblDefaultContent.Name = "lblDefaultContent";
			this.lblDefaultContent.Size = new System.Drawing.Size(100, 16);
			this.lblDefaultContent.TabIndex = 2;
			this.lblDefaultContent.Text = "Default Content:";
			this.lblDefaultContent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblConstant
			// 
			this.lblConstant.Location = new System.Drawing.Point(8, 208);
			this.lblConstant.Name = "lblConstant";
			this.lblConstant.Size = new System.Drawing.Size(100, 16);
			this.lblConstant.TabIndex = 3;
			this.lblConstant.Text = "Constant:";
			this.lblConstant.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblLastUpdated
			// 
			this.lblLastUpdated.Location = new System.Drawing.Point(8, 232);
			this.lblLastUpdated.Name = "lblLastUpdated";
			this.lblLastUpdated.Size = new System.Drawing.Size(100, 16);
			this.lblLastUpdated.TabIndex = 4;
			this.lblLastUpdated.Text = "Last Updated:";
			this.lblLastUpdated.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxArticleID
			// 
			this.tbxArticleID.Location = new System.Drawing.Point(112, 8);
			this.tbxArticleID.Name = "tbxArticleID";
			this.tbxArticleID.ReadOnly = true;
			this.tbxArticleID.TabIndex = 5;
			this.tbxArticleID.Text = "";
			// 
			// tbxCategoryID
			// 
			this.tbxCategoryID.Location = new System.Drawing.Point(112, 32);
			this.tbxCategoryID.Name = "tbxCategoryID";
			this.tbxCategoryID.ReadOnly = true;
			this.tbxCategoryID.TabIndex = 6;
			this.tbxCategoryID.Text = "";
			// 
			// tbxDefaultContent
			// 
			this.tbxDefaultContent.Location = new System.Drawing.Point(112, 56);
			this.tbxDefaultContent.MaxLength = 131068;
			this.tbxDefaultContent.Name = "tbxDefaultContent";
			this.tbxDefaultContent.Size = new System.Drawing.Size(520, 144);
			this.tbxDefaultContent.TabIndex = 7;
			this.tbxDefaultContent.Text = "";
			// 
			// tbxConstant
			// 
			this.tbxConstant.Location = new System.Drawing.Point(112, 208);
			this.tbxConstant.Name = "tbxConstant";
			this.tbxConstant.Size = new System.Drawing.Size(520, 20);
			this.tbxConstant.TabIndex = 8;
			this.tbxConstant.Text = "";
			this.tbxConstant.TextChanged += new System.EventHandler(this.tbxConstant_TextChanged);
			// 
			// tbxLastUpdated
			// 
			this.tbxLastUpdated.Location = new System.Drawing.Point(112, 232);
			this.tbxLastUpdated.Name = "tbxLastUpdated";
			this.tbxLastUpdated.ReadOnly = true;
			this.tbxLastUpdated.Size = new System.Drawing.Size(176, 20);
			this.tbxLastUpdated.TabIndex = 9;
			this.tbxLastUpdated.Text = "";
			// 
			// btnSave
			// 
			this.btnSave.Location = new System.Drawing.Point(456, 288);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(88, 32);
			this.btnSave.TabIndex = 10;
			this.btnSave.Text = "&Save";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(552, 288);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(88, 32);
			this.btnCancel.TabIndex = 11;
			this.btnCancel.Text = "&Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// Article
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(648, 333);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnSave);
			this.Controls.Add(this.tbxLastUpdated);
			this.Controls.Add(this.tbxConstant);
			this.Controls.Add(this.tbxCategoryID);
			this.Controls.Add(this.tbxArticleID);
			this.Controls.Add(this.tbxDefaultContent);
			this.Controls.Add(this.lblLastUpdated);
			this.Controls.Add(this.lblConstant);
			this.Controls.Add(this.lblDefaultContent);
			this.Controls.Add(this.lblCategoryID);
			this.Controls.Add(this.lblArticleID);
			this.Name = "Article";
			this.Text = "Article";
			this.Load += new System.EventHandler(this.Article_Load);
			this.ResumeLayout(false);

		}
		#endregion
	}
}
