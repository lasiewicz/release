using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for SiteCategory.
	/// </summary>
	public class SiteCategory : System.Windows.Forms.Form
	{
		private System.ComponentModel.Container components = null;
		private Web.SiteCategory _voSiteCategory;
		private Settings _settings;
		private System.Windows.Forms.GroupBox gbxButtons;
		private System.Windows.Forms.Button cmdCancel;
		private System.Windows.Forms.Button cmdSave;
		private System.Windows.Forms.Label lblContent;
		private System.Windows.Forms.Label lblCategoryID;
		private System.Windows.Forms.Label lblSiteCategoryID;
		private System.Windows.Forms.Label lblConstant;
		private System.Windows.Forms.Label lblParentCategoryID;
		private System.Windows.Forms.TextBox tbxSiteCategoryID;
		private System.Windows.Forms.CheckBox cbxPublishedFlag;
		private System.Windows.Forms.TextBox tbxCategoryID;
		private System.Windows.Forms.TextBox tbxParentCategoryID;
		private System.Windows.Forms.TextBox tbxConstant;
		private System.Windows.Forms.TextBox tbxLastUpdated;
		private System.Windows.Forms.TextBox tbxContent;
		private System.Windows.Forms.Label lblLastUpdated;
		private System.Windows.Forms.TextBox tbxSiteID;
		private System.Windows.Forms.Label lblSiteID;
		private System.Windows.Forms.Button btnEditCategory;
		private System.Windows.Forms.GroupBox gbxCategory;

		public SiteCategory(Web.SiteCategory VOSiteCategory)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			_voSiteCategory = VOSiteCategory;
		}

		public Settings settings
		{
			set
			{
				_settings = value;
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblConstant = new System.Windows.Forms.Label();
			this.gbxButtons = new System.Windows.Forms.GroupBox();
			this.cmdCancel = new System.Windows.Forms.Button();
			this.cmdSave = new System.Windows.Forms.Button();
			this.lblContent = new System.Windows.Forms.Label();
			this.lblParentCategoryID = new System.Windows.Forms.Label();
			this.lblCategoryID = new System.Windows.Forms.Label();
			this.lblSiteCategoryID = new System.Windows.Forms.Label();
			this.tbxSiteCategoryID = new System.Windows.Forms.TextBox();
			this.cbxPublishedFlag = new System.Windows.Forms.CheckBox();
			this.tbxCategoryID = new System.Windows.Forms.TextBox();
			this.tbxParentCategoryID = new System.Windows.Forms.TextBox();
			this.tbxConstant = new System.Windows.Forms.TextBox();
			this.tbxLastUpdated = new System.Windows.Forms.TextBox();
			this.tbxContent = new System.Windows.Forms.TextBox();
			this.lblLastUpdated = new System.Windows.Forms.Label();
			this.tbxSiteID = new System.Windows.Forms.TextBox();
			this.lblSiteID = new System.Windows.Forms.Label();
			this.btnEditCategory = new System.Windows.Forms.Button();
			this.gbxCategory = new System.Windows.Forms.GroupBox();
			this.SuspendLayout();
			// 
			// lblConstant
			// 
			this.lblConstant.Location = new System.Drawing.Point(112, 120);
			this.lblConstant.Name = "lblConstant";
			this.lblConstant.Size = new System.Drawing.Size(89, 16);
			this.lblConstant.TabIndex = 28;
			this.lblConstant.Text = "Constant:";
			this.lblConstant.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// gbxButtons
			// 
			this.gbxButtons.Location = new System.Drawing.Point(-16, 264);
			this.gbxButtons.Name = "gbxButtons";
			this.gbxButtons.Size = new System.Drawing.Size(716, 4);
			this.gbxButtons.TabIndex = 30;
			this.gbxButtons.TabStop = false;
			this.gbxButtons.Text = "gbxButtons";
			// 
			// cmdCancel
			// 
			this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cmdCancel.Location = new System.Drawing.Point(424, 280);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(81, 28);
			this.cmdCancel.TabIndex = 4;
			this.cmdCancel.Text = "&Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.Location = new System.Drawing.Point(336, 280);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Size = new System.Drawing.Size(81, 28);
			this.cmdSave.TabIndex = 3;
			this.cmdSave.Text = "&Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// lblContent
			// 
			this.lblContent.Location = new System.Drawing.Point(112, 240);
			this.lblContent.Name = "lblContent";
			this.lblContent.Size = new System.Drawing.Size(89, 16);
			this.lblContent.TabIndex = 29;
			this.lblContent.Text = "Content:";
			this.lblContent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblParentCategoryID
			// 
			this.lblParentCategoryID.Location = new System.Drawing.Point(96, 96);
			this.lblParentCategoryID.Name = "lblParentCategoryID";
			this.lblParentCategoryID.Size = new System.Drawing.Size(104, 16);
			this.lblParentCategoryID.TabIndex = 27;
			this.lblParentCategoryID.Text = "ParentCategoryID:";
			this.lblParentCategoryID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblCategoryID
			// 
			this.lblCategoryID.Location = new System.Drawing.Point(112, 72);
			this.lblCategoryID.Name = "lblCategoryID";
			this.lblCategoryID.Size = new System.Drawing.Size(89, 16);
			this.lblCategoryID.TabIndex = 25;
			this.lblCategoryID.Text = "CategoryID:";
			this.lblCategoryID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lblSiteCategoryID
			// 
			this.lblSiteCategoryID.Location = new System.Drawing.Point(112, 32);
			this.lblSiteCategoryID.Name = "lblSiteCategoryID";
			this.lblSiteCategoryID.Size = new System.Drawing.Size(89, 16);
			this.lblSiteCategoryID.TabIndex = 33;
			this.lblSiteCategoryID.Text = "SiteCategoryID:";
			this.lblSiteCategoryID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxSiteCategoryID
			// 
			this.tbxSiteCategoryID.Location = new System.Drawing.Point(200, 32);
			this.tbxSiteCategoryID.Name = "tbxSiteCategoryID";
			this.tbxSiteCategoryID.ReadOnly = true;
			this.tbxSiteCategoryID.Size = new System.Drawing.Size(144, 20);
			this.tbxSiteCategoryID.TabIndex = 34;
			this.tbxSiteCategoryID.Text = "";
			// 
			// cbxPublishedFlag
			// 
			this.cbxPublishedFlag.Location = new System.Drawing.Point(528, 160);
			this.cbxPublishedFlag.Name = "cbxPublishedFlag";
			this.cbxPublishedFlag.Size = new System.Drawing.Size(80, 24);
			this.cbxPublishedFlag.TabIndex = 35;
			this.cbxPublishedFlag.Text = "Published";
			// 
			// tbxCategoryID
			// 
			this.tbxCategoryID.Location = new System.Drawing.Point(200, 72);
			this.tbxCategoryID.Name = "tbxCategoryID";
			this.tbxCategoryID.ReadOnly = true;
			this.tbxCategoryID.Size = new System.Drawing.Size(144, 20);
			this.tbxCategoryID.TabIndex = 36;
			this.tbxCategoryID.Text = "";
			// 
			// tbxParentCategoryID
			// 
			this.tbxParentCategoryID.Location = new System.Drawing.Point(200, 96);
			this.tbxParentCategoryID.Name = "tbxParentCategoryID";
			this.tbxParentCategoryID.ReadOnly = true;
			this.tbxParentCategoryID.Size = new System.Drawing.Size(144, 20);
			this.tbxParentCategoryID.TabIndex = 37;
			this.tbxParentCategoryID.Text = "";
			// 
			// tbxConstant
			// 
			this.tbxConstant.Location = new System.Drawing.Point(200, 120);
			this.tbxConstant.Name = "tbxConstant";
			this.tbxConstant.ReadOnly = true;
			this.tbxConstant.Size = new System.Drawing.Size(320, 20);
			this.tbxConstant.TabIndex = 38;
			this.tbxConstant.Text = "";
			// 
			// tbxLastUpdated
			// 
			this.tbxLastUpdated.Location = new System.Drawing.Point(200, 160);
			this.tbxLastUpdated.Name = "tbxLastUpdated";
			this.tbxLastUpdated.ReadOnly = true;
			this.tbxLastUpdated.Size = new System.Drawing.Size(320, 20);
			this.tbxLastUpdated.TabIndex = 39;
			this.tbxLastUpdated.Text = "";
			// 
			// tbxContent
			// 
			this.tbxContent.Location = new System.Drawing.Point(200, 184);
			this.tbxContent.Multiline = true;
			this.tbxContent.Name = "tbxContent";
			this.tbxContent.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.tbxContent.Size = new System.Drawing.Size(440, 72);
			this.tbxContent.TabIndex = 40;
			this.tbxContent.Text = "";
			// 
			// lblLastUpdated
			// 
			this.lblLastUpdated.Location = new System.Drawing.Point(112, 160);
			this.lblLastUpdated.Name = "lblLastUpdated";
			this.lblLastUpdated.Size = new System.Drawing.Size(89, 16);
			this.lblLastUpdated.TabIndex = 41;
			this.lblLastUpdated.Text = "Last Updated:";
			this.lblLastUpdated.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbxSiteID
			// 
			this.tbxSiteID.Location = new System.Drawing.Point(200, 8);
			this.tbxSiteID.Name = "tbxSiteID";
			this.tbxSiteID.ReadOnly = true;
			this.tbxSiteID.Size = new System.Drawing.Size(144, 20);
			this.tbxSiteID.TabIndex = 43;
			this.tbxSiteID.Text = "";
			// 
			// lblSiteID
			// 
			this.lblSiteID.Location = new System.Drawing.Point(112, 8);
			this.lblSiteID.Name = "lblSiteID";
			this.lblSiteID.Size = new System.Drawing.Size(89, 16);
			this.lblSiteID.TabIndex = 42;
			this.lblSiteID.Text = "SiteID:";
			this.lblSiteID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// btnEditCategory
			// 
			this.btnEditCategory.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnEditCategory.Location = new System.Drawing.Point(512, 280);
			this.btnEditCategory.Name = "btnEditCategory";
			this.btnEditCategory.Size = new System.Drawing.Size(136, 28);
			this.btnEditCategory.TabIndex = 44;
			this.btnEditCategory.Text = "&Edit Category";
			this.btnEditCategory.Click += new System.EventHandler(this.btnEditCategory_Click);
			// 
			// gbxCategory
			// 
			this.gbxCategory.Location = new System.Drawing.Point(40, 56);
			this.gbxCategory.Name = "gbxCategory";
			this.gbxCategory.Size = new System.Drawing.Size(568, 96);
			this.gbxCategory.TabIndex = 45;
			this.gbxCategory.TabStop = false;
			this.gbxCategory.Text = "Category Attributes";
			// 
			// SiteCategory
			// 
			this.AcceptButton = this.cmdSave;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.cmdCancel;
			this.ClientSize = new System.Drawing.Size(654, 319);
			this.Controls.Add(this.btnEditCategory);
			this.Controls.Add(this.tbxSiteID);
			this.Controls.Add(this.lblSiteID);
			this.Controls.Add(this.lblLastUpdated);
			this.Controls.Add(this.tbxContent);
			this.Controls.Add(this.tbxLastUpdated);
			this.Controls.Add(this.tbxConstant);
			this.Controls.Add(this.tbxParentCategoryID);
			this.Controls.Add(this.tbxCategoryID);
			this.Controls.Add(this.cbxPublishedFlag);
			this.Controls.Add(this.tbxSiteCategoryID);
			this.Controls.Add(this.lblSiteCategoryID);
			this.Controls.Add(this.lblConstant);
			this.Controls.Add(this.gbxButtons);
			this.Controls.Add(this.cmdCancel);
			this.Controls.Add(this.cmdSave);
			this.Controls.Add(this.lblContent);
			this.Controls.Add(this.lblParentCategoryID);
			this.Controls.Add(this.lblCategoryID);
			this.Controls.Add(this.gbxCategory);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SiteCategory";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Edit SiteCategory";
			this.Load += new System.EventHandler(this.SiteCategory_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private bool SaveSiteCategory()
		{
			try
			{
				int ret = _settings.ClientAPI.SaveSiteCategory(BuildVOSiteCategory());

				if (ret > 0)
					return true;
				else
					return false;
			}
			catch(Exception ex)
			{
				MessageBox.Show("Error: " + ex.Message,  "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Information);

				return false;
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void cmdSave_Click(object sender, System.EventArgs e)
		{
			if (tbxContent.Text == string.Empty)
			{
				MessageBox.Show("Error: Please enter English content",  "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				if (SaveSiteCategory())
				{
					DialogResult = DialogResult.OK;
					this.Close();
				}
				else
				{
					DialogResult = DialogResult.Cancel;
					this.Close();
				}
			}
		}

		private void SiteCategory_Load(object sender, System.EventArgs e)
		{
			DisplaySiteCategory();
		}

		private void DisplaySiteCategory()
		{
			tbxSiteID.Text = _voSiteCategory.SiteID.ToString();
			tbxSiteCategoryID.Text = _voSiteCategory.SiteCategoryID.ToString();
			tbxCategoryID.Text = _voSiteCategory.CategoryID.ToString();
			tbxContent.Text = _voSiteCategory.Content;
			tbxParentCategoryID.Text = _voSiteCategory.Category.ParentCategoryID.ToString();
			tbxConstant.Text = _voSiteCategory.Category.Constant;
			tbxLastUpdated.Text = _voSiteCategory.LastUpdated.ToString();
			cbxPublishedFlag.Checked = _voSiteCategory.PublishedFlag;
		}

		private Web.SiteCategory BuildVOSiteCategory()
		{
			Web.SiteCategory siteCategory = new Web.SiteCategory();

			siteCategory.SiteCategoryID = Convert.ToInt32(tbxSiteCategoryID.Text.Trim());
			siteCategory.CategoryID = Convert.ToInt32(tbxCategoryID.Text.Trim());
			siteCategory.SiteID = Convert.ToInt32(tbxSiteID.Text.Trim());
			siteCategory.Content = tbxContent.Text;
			siteCategory.PublishedFlag = cbxPublishedFlag.Checked;

			return siteCategory; 
		}

		private void btnEditCategory_Click(object sender, System.EventArgs e)
		{
			Category frm = new Category(_voSiteCategory.CategoryID, _settings);

			if (frm.ShowDialog() == DialogResult.OK)
			{
				this.DialogResult = DialogResult.OK;
			}
		}
	}
}
