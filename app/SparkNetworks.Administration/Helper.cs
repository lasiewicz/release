using System;
using System.Security.Cryptography; 
using System.Text.RegularExpressions;
using System.IO;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for Helper.
	/// </summary>
	public class Helper
	{
		private static Regex isNumericRegex = new Regex(@"^\d+$");

		public static Boolean IsNumeric(String theValue)
		{
			Match match = isNumericRegex.Match(theValue);

			return match.Success;
		}

		public static Boolean AreFilesIdentical(String source, String target)
		{
			//create the hashing object.
			HashAlgorithm alg = HashAlgorithm.Create();

			//Calculate the hash for the first file
			FileStream fsA = new FileStream(source, FileMode.Open);
			byte[] hashA = alg.ComputeHash(fsA);
			fsA.Close();
 
			//calculate the hash for the second file
			FileStream fsB = new FileStream(target, FileMode.Open);
			byte[] hashB = alg.ComputeHash(fsB);
			fsB.Close();
 
			//compare the hashes
			return BitConverter.ToString(hashA) == BitConverter.ToString(hashB);
		}
	}
}
