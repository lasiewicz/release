using System;
using System.Collections;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Security;
using System.Threading;

using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// HACK HACK HACK.  This will eventually be a SmartClient and security will be handled in a
	/// a very different manner.  For now it is hardcoded.
	/// </summary>
	public class Login : System.Windows.Forms.Form
	{
		private static string[] articleEditors = new string[] {"dlee", "crunge", "jvargas", "tdavison", "nschubert", "afan", "rmisenheimer"};
		private static string[] pixelEditors = new string[] {"dlee", "asandoval", "refron", "cbenish"};
		private static string[] pixelApprovers = new string[] {"dlee"};

		private static string[,] LOGIN_DATA = new string[,] { {"12345", "12345", "100"}, 
			{"ephipps@spark.net", "SP@rk", "101"},
			{"schyun@spark.net", "SP@rk", "102"},
			{"ryoung@spark.net", "SP@rk", "103"},
			{"rmarazsky@spark.net", "SP@rk", "104"},
			{"jjandoc@spark.net", "SP@rk", "105"},
			{"jvargas@spark.net", "SP@rk", "106"},
			{"tdavison@spark.net", "todd", "107"},
			{"nschubert@spark.net", "12345", "108"},
			{"afan@spark.net", "SP@rk", "109"},
			{"crunge@spark.net", "SP@rk", "111"},
			{"rmisenheimer@spark.net", "SP@rk", "110"}};

		private const string UPDATESCRIPT_PATH = @"C:\Matchnet\SparkNetworksAdministration_UPDATE.bat";

		private int _memberID = int.MinValue;

		private System.Windows.Forms.GroupBox grpLogin;
		private System.Windows.Forms.Label lblEmailAddress;
		private System.Windows.Forms.Label lblPassword;
		private System.Windows.Forms.Button btnLogin;
		private System.Windows.Forms.TextBox tbxEmailAddress;
		private System.Windows.Forms.TextBox tbxPassword;
		private System.Windows.Forms.Button btnCreateForceUpdate;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Login()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.grpLogin = new System.Windows.Forms.GroupBox();
			this.btnCreateForceUpdate = new System.Windows.Forms.Button();
			this.btnLogin = new System.Windows.Forms.Button();
			this.tbxPassword = new System.Windows.Forms.TextBox();
			this.tbxEmailAddress = new System.Windows.Forms.TextBox();
			this.lblPassword = new System.Windows.Forms.Label();
			this.lblEmailAddress = new System.Windows.Forms.Label();
			this.grpLogin.SuspendLayout();
			this.SuspendLayout();
			// 
			// grpLogin
			// 
			this.grpLogin.Controls.Add(this.btnCreateForceUpdate);
			this.grpLogin.Controls.Add(this.btnLogin);
			this.grpLogin.Controls.Add(this.tbxPassword);
			this.grpLogin.Controls.Add(this.tbxEmailAddress);
			this.grpLogin.Controls.Add(this.lblPassword);
			this.grpLogin.Controls.Add(this.lblEmailAddress);
			this.grpLogin.Location = new System.Drawing.Point(8, 8);
			this.grpLogin.Name = "grpLogin";
			this.grpLogin.Size = new System.Drawing.Size(280, 112);
			this.grpLogin.TabIndex = 0;
			this.grpLogin.TabStop = false;
			this.grpLogin.Text = "Login";
			// 
			// btnCreateForceUpdate
			// 
			this.btnCreateForceUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnCreateForceUpdate.Location = new System.Drawing.Point(184, 72);
			this.btnCreateForceUpdate.Name = "btnCreateForceUpdate";
			this.btnCreateForceUpdate.Size = new System.Drawing.Size(88, 32);
			this.btnCreateForceUpdate.TabIndex = 5;
			this.btnCreateForceUpdate.Text = "Create Force Update Script";
			this.btnCreateForceUpdate.Click += new System.EventHandler(this.btnCreateForceUpdate_Click);
			// 
			// btnLogin
			// 
			this.btnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.btnLogin.Location = new System.Drawing.Point(96, 72);
			this.btnLogin.Name = "btnLogin";
			this.btnLogin.Size = new System.Drawing.Size(80, 20);
			this.btnLogin.TabIndex = 4;
			this.btnLogin.Text = "Login";
			this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
			// 
			// tbxPassword
			// 
			this.tbxPassword.Location = new System.Drawing.Point(96, 48);
			this.tbxPassword.Name = "tbxPassword";
			this.tbxPassword.PasswordChar = '*';
			this.tbxPassword.Size = new System.Drawing.Size(176, 20);
			this.tbxPassword.TabIndex = 3;
			this.tbxPassword.Text = "";
			// 
			// tbxEmailAddress
			// 
			this.tbxEmailAddress.Location = new System.Drawing.Point(96, 24);
			this.tbxEmailAddress.Name = "tbxEmailAddress";
			this.tbxEmailAddress.Size = new System.Drawing.Size(176, 20);
			this.tbxEmailAddress.TabIndex = 2;
			this.tbxEmailAddress.Text = "";
			// 
			// lblPassword
			// 
			this.lblPassword.Location = new System.Drawing.Point(8, 48);
			this.lblPassword.Name = "lblPassword";
			this.lblPassword.Size = new System.Drawing.Size(88, 16);
			this.lblPassword.TabIndex = 1;
			this.lblPassword.Text = "Password:";
			this.lblPassword.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// lblEmailAddress
			// 
			this.lblEmailAddress.Location = new System.Drawing.Point(8, 24);
			this.lblEmailAddress.Name = "lblEmailAddress";
			this.lblEmailAddress.Size = new System.Drawing.Size(88, 16);
			this.lblEmailAddress.TabIndex = 0;
			this.lblEmailAddress.Text = "Email Address:";
			this.lblEmailAddress.TextAlign = System.Drawing.ContentAlignment.BottomRight;
			// 
			// Login
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(298, 125);
			this.Controls.Add(this.grpLogin);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "Login";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Login";
			this.Load += new System.EventHandler(this.Login_Load);
			this.grpLogin.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		public int MemberID
		{
			get
			{
				return _memberID;
			}
		}

		private void Login_Load(object sender, System.EventArgs e)
		{
		}

		private bool Authenticate()
		{
			ArrayList cachedMembers = null;
			Int32 userRoles = 0;
			Int32 memberID = Matchnet.Constants.NULL_INT;
			AppDomain.CurrentDomain.SetPrincipalPolicy(System.Security.Principal.PrincipalPolicy.WindowsPrincipal);

			// Check to make sure they are authenticated.
			if (!Thread.CurrentPrincipal.Identity.IsAuthenticated)
			{
				MessageBox.Show(Constants.MSG_NOT_AUTHENTICATED, Constants.MSGBOXCAPTION_AUTHENTICATIONERROR);
				return false;
			}

			String username = Thread.CurrentPrincipal.Identity.Name;
			username = username.Substring(username.IndexOf('\\') + 1);

			// Check to make sure the user has a valid MemberID.
			Int32 notUsed = new Int32();
			// First check using @spark.net.
			cachedMembers = MemberSA.Instance.GetMembersByEmail(username + "@" + Constants.DOMAIN_EMAIL, 1, 1, ref notUsed);
			// Try @matchnet.com.
			if (cachedMembers.Count == 0)
				cachedMembers = MemberSA.Instance.GetMembersByEmail(username + "@" + Constants.DOMAIN_EMAIL2, 1, 1, ref notUsed);

			if (cachedMembers.Count == 0)
			{
				MessageBox.Show(Constants.MSG_NO_MEMBERID, Constants.MSGBOXCAPTION_AUTHENTICATIONERROR);
				return false;
			}

			memberID = ((CachedMember)cachedMembers[0]).MemberID;

			// Next, determine which functionality to which the user has access.
			// NOTE:  The can eventually be configured to use Windows Groups (i.e. Create a new Windows Domain group
			// for each piece of functionality).
			// All Creative people have access to Articles.
			if (Thread.CurrentPrincipal.IsInRole(Constants.DOMAIN_ACCOUNT + @"\creative"))
				userRoles += (Int32)Constants.RolesMaskEnum.ArticleEditor;

			// Iterate the articleEditors array.
			for (int i = 0; i <= articleEditors.GetUpperBound(0); i++)
			{
				if (articleEditors[i] == username)
				{
					userRoles += (Int32)Constants.RolesMaskEnum.ArticleEditor;
					break;
				}
			}

			// Iterate the pixelEditors array.
			for (int i = 0; i <= pixelEditors.GetUpperBound(0); i++)
			{
				if (pixelEditors[i] == username)
				{
					userRoles += (Int32)Constants.RolesMaskEnum.PixelEditor;
					break;
				}
			}

			// Iterate the pixelApprovers array.
			for (int i = 0; i <= pixelApprovers.GetUpperBound(0); i++)
			{
				if (pixelApprovers[i] == username)
				{
					userRoles += (Int32)Constants.RolesMaskEnum.PixelApprover;
					break;
				}
			}

			return true;
		}

		private void btnLogin_Click(object sender, System.EventArgs e)
		{
			if (LoginUser())
			{
				// Load the necessary assemblies.
				Assembly.Load(Assembly.GetAssembly(typeof(Matchnet.Content.ServiceAdapters.ArticleSA)).FullName);
				Assembly.Load(Assembly.GetAssembly(typeof(Matchnet.Content.ValueObjects.Article.Article)).FullName);

				this.DialogResult = DialogResult.OK;
			}
			else
			{
				MessageBox.Show("Incorrect Username and/or Password.", "Authentication Error");
			}
		}

		private bool LoginUser() 
		{
			for (int i = 0; i <= LOGIN_DATA.GetUpperBound(0); i++)
			{
				if (tbxEmailAddress.Text.ToLower() == LOGIN_DATA[i, 0] && tbxPassword.Text == LOGIN_DATA[i, 1])
				{
					_memberID = Convert.ToInt32(LOGIN_DATA[i, 2]);

					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// HACK until I can figure out why NTD is not updating dependency files!.
		/// This is only necessary when one of the dependency files changes (i.e. Matchnet.Content.ValueObjects).
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnCreateForceUpdate_Click(object sender, System.EventArgs e)
		{
			// Create an instance of StreamWriter to write text to a file.
			// The using statement also closes the StreamWriter.
			using (StreamWriter sw = new StreamWriter(UPDATESCRIPT_PATH)) 
			{
				sw.Write("del \"");
				sw.Write(Assembly.GetAssembly(typeof(Matchnet.Content.ServiceAdapters.ArticleSA)).Location);
				sw.WriteLine("\"");

				sw.Write("del \"");
				sw.Write(Assembly.GetAssembly(typeof(Matchnet.Content.ValueObjects.Article.Article)).Location);
				sw.WriteLine("\"");
			}

			MessageBox.Show("Please close this application and all your browsers and then run " + UPDATESCRIPT_PATH + ".");

			this.DialogResult = DialogResult.Cancel;
		}
	}
}
