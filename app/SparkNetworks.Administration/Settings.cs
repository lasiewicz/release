using System;
using System.IO;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using System.Net;

using SparkNetworks.Administration.Web;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for Settings.
	/// </summary>
	public class Settings
	{
		private ItemData[] sitesObj = new ItemData[0];
		private Int32 memberIDObj;
		private Boolean isContentEditingAllowed = false;
		private User user;
		private ClientAPI clientAPI;
		private String environmentType;

		public Settings()
		{
			clientAPI = new ClientAPI();
//			clientAPI.PreAuthenticate = true;
//			clientAPI.Credentials = CredentialCache.DefaultCredentials;

			isContentEditingAllowed = Convert.ToBoolean(clientAPI.GetSetting("SPARKADMIN_ISCONTENTEDITINGALLOWED"));
			environmentType = Convert.ToString(clientAPI.GetSetting("ENVIRONMENT_TYPE"));
		}

		public ItemData[] Sites
		{
			get
			{
				return sitesObj;
			}
		}

		public Int32 MemberID
		{
			get
			{
				return memberIDObj;
			}
		}

		public User User
		{
			get
			{
				return user;
			}
		}

		public Boolean IsContentEditingAllowed
		{
			get
			{
				return isContentEditingAllowed;
			}
		}

		public String EnvironmentType
		{
			get
			{
				return environmentType;
			}
		}

		public ClientAPI ClientAPI
		{
			get
			{
				return clientAPI;
			}
		}

		public void LoadSettings(Int32 memberID, Constants.RolesMaskEnum userRolesMask)
		{
			memberIDObj = memberID;
			user = new User(userRolesMask);

			LoadSites();
		}

		private void LoadSites()
		{
			try
			{
				Site[] sites = clientAPI.GetSites();

				if (sites.Length > 0)
					sitesObj = new ItemSite[sites.Length];

				int i = 0;
				foreach(Site site in sites)
				{
					sitesObj[i] = new ItemSite(site.Name, site.SiteID, site.SiteID);

					i++;
				}				
			}
			catch(System.Data.SqlClient.SqlException ex)
			{
				MessageBox.Show("Error: " + ex.Message, "Error Loading Sites");
			}
		}
	}

	public class ItemSite : ItemData
	{
		public int siteID;

		public ItemSite(string Text, int Value, int SiteID)
		{
			_text = Text;
			_value = Value;
			siteID = SiteID;	
		}
	}
}
