using System;
using System.Windows.Forms;
using System.Collections;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for ListViewItemComparer.
	/// </summary>
	class ListViewItemComparer : IComparer
	{
		private int col;
		private Boolean asc;

		public ListViewItemComparer()
		{
			col = 0;
		}
		public ListViewItemComparer(int column, Boolean ascending)
		{
			col = column;
			asc = ascending;
		}
		public int Compare(object x, object y)
		{
			Int32 val = 0;

			if (Helper.IsNumeric(((ListViewItem)x).SubItems[col].Text) && Helper.IsNumeric(((ListViewItem)y).SubItems[col].Text))
				val = Int32.Parse(((ListViewItem)x).SubItems[col].Text) - Int32.Parse(((ListViewItem)y).SubItems[col].Text);
			else
				val = String.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);

			return val * (asc ? -1 : 1);
		}
	}
}
