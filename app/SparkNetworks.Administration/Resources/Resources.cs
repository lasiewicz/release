using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace SparkNetworks.Administration.Resources
{
	/// <summary>
	/// Summary description for Resources.
	/// </summary>
	public class Resources : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.ComboBox cboSite;
		private System.Windows.Forms.Label lblSite;
		private Settings _settings;

		public Resources()
		{
			InitializeComponent();

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Properties
		public Settings settings
		{
			set
			{
				_settings = value;
				ItemData.SetComboBoxValues(cboSite, _settings.Sites, 2);
			}
			get
			{
				return _settings;
			}
		}
		#endregion

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cboSite = new System.Windows.Forms.ComboBox();
			this.lblSite = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// cboSite
			// 
			this.cboSite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboSite.Location = new System.Drawing.Point(56, 16);
			this.cboSite.Name = "cboSite";
			this.cboSite.Size = new System.Drawing.Size(176, 21);
			this.cboSite.TabIndex = 0;
			this.cboSite.SelectedIndexChanged += new System.EventHandler(this.cboSite_SelectedIndexChanged);
			// 
			// lblSite
			// 
			this.lblSite.Location = new System.Drawing.Point(24, 16);
			this.lblSite.Name = "lblSite";
			this.lblSite.Size = new System.Drawing.Size(32, 21);
			this.lblSite.TabIndex = 1;
			this.lblSite.Text = "&Site:";
			this.lblSite.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// Resources
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(792, 573);
			this.Controls.Add(this.lblSite);
			this.Controls.Add(this.cboSite);
			this.Name = "Resources";
			this.Text = "Resources";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.ResumeLayout(false);

		}
		#endregion

		private void cboSite_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//Reload();
		}
	}
}
