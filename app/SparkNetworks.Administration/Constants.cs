using System;
using System.Windows.Forms;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for Constants.
	/// </summary>
	public class Constants
	{
		public const String SYSTEM_CONFIG_PATH = @"C:\Matchnet\System.config";
		public const String SYSTEM_CONFIG_DEV_PATH = @"C:\Matchnet\System.config_DEV";
		public const String SYSTEM_CONFIG_PROD_PATH = @"C:\Matchnet\System.config_PROD";
		public const String MSG_SYSTEM_CONFIG_MISSING_ERROR = @"You are missing files necessary to run this application.  Please copy \\matchnet\infosys\appdev\Matchnet\SparkNetworks.Administration\System.config, \\matchnet\infosys\appdev\Matchnet\SparkNetworks.Administration\System.config_DEV, and \\matchnet\infosys\appdev\Matchnet\SparkNetworks.Administration\System.config_PROD to your C:\Matchnet directory.";
		public const String HTML_TEST_TEMPLATE = "<HTML><BODY>THIS IS A TEST OF PIXELS.  PLEASE LOOK FOR JAVASCRIPT ERRORS.<BR /><BR />If no error dialog box appeared before this window opened then the Javascript test has been passed.<BR /><BR />{0}<BR /><BR /></BODY></HTML>";
		public const String UPDATESCRIPT_PATH = @"C:\Matchnet\SparkNetworksAdministration_UPDATE.bat";
		public const String MSG_UPDATE = "Please close this application and all your browsers and then run \"" + UPDATESCRIPT_PATH + "\".";
		public static readonly DateTime NO_ENDDATE = new DateTime(1999, 1, 1);
		public const String PARAMETER_STRING_START = "[*]";
		public const String MSGBOXCAPTION_WARNING = "WARNING";
		public const String MSGBOXCAPTION_AUTHENTICATIONERROR = "AUTHENTICATION ERROR";
		public const String MSGBOXCAPTION_SYSTEMERROR = "SYSTEM ERROR";
		public const String MSGBOXCAPTION_CONFIRM = "CONFIRM";
		public const String MSG_CONTACT_ADMINISTRATOR = "Please contact a system administrator. ({0})";
		public const String MSG_SITE_AND_PAGEID_REQUIRED_ERROR = "Please select a Site and enter a valid PageID";
		public const String MSG_SELECT_PIXEL_TO_EDIT = "Please select a pixel to edit.";
		public const String MSG_NOT_AUTHENTICATED = "Please log in to Windows before attempting to use this application.";
		public const String MSG_NOT_AUTHORIZED = "You are not authorized to use any of the tools in this application.";
		public const String MSG_FILTER_PARAMETER_ERROR = "The Filters and Parameters for this pixel could not be loaded correctly.  This is most likely because this pixel was entered manually (before the creation of the Pixel Administrator).  Please re-add the Filters and Parameters and re-save this pixel.";
		public const String MSG_FILTER_NONE_ERROR = "There are no filters to modify.";
		public const String MSG_PARAMETER_NONE_ERROR = "There are no parameters to modify.";
		public const String MSG_FILTER_SELECT_ERROR = "Please select a filter to modify.";
		public const String MSG_PARAMETER_SELECT_ERROR = "Please select a parameter to modify.";
		public const String MSG_ENDDATE_IS_PAST_ERROR = "EndDate can not be in the past.";
		public const String MSG_DESCRIPTION_REQUIRED_ERROR = "A Description is required.";
		public const String MSG_CONTENT_REQUIRED_ERROR = "Content is required.";
		public const String MSG_SUBSCRIPTION_SECURE_LINKS_ONLY_ERROR = "All pixels on 3000 pages (Subscription) must not contain unsecure links (http:).";
		public const String MSG_IMAGETYPE_NO_IMGTAG_ERROR = "IMAGE type pixels can not contain an <IMG> tag.";
		public const String MSG_IMAGETYPE_NO_SCRIPTTAG_ERROR = "IMAGE type pixels can not contain a <SCRIPT> tag.";
		public const String MSG_TEXTTYPE_IMGTAG_OR_SCRIPTTAG_ERROR = "TEXT type pixels begin with either an <IMG>, <SCRIPT>, or <IFRAME> tag.";
		public const String MSG_TEXTTYPE_WIDTH_HEIGHT_BORDER_ERROR = "TEXT type pixels must contain width=\"1\" height=\"1\" border=\"0\".";
		public const String MSG_PARAMETER_STRING_START_MISSING_ERROR = "If you include parameters in the pixel you must include a \"" + PARAMETER_STRING_START + "\" in the Content.";
		public const String MSG_CONTENTCONDITION_MAX_LENGTH_ERROR = "The ContentCondition length exceeds the maximum length of 2000.  Please delete some of your filters and/or parameters, or reduce the number of values in the filters.";
		public const String MSG_CONTENTCONDITIONARGS_MAX_LENGTH_ERROR = "The ContentConditionArgs length exceeds the maximum length of 2000.  Please delete some of your filters or parameters.";
		public const String MSG_SAVE_PIXEL_CONFIRM = "Are you sure you want to save this pixel?";
		public const String MSG_NEXTSTEP_PIXEL_CONFIRM = "Are you sure you want to {0} this pixel?";
		public const String MSG_DELETE_PIXEL_CONFIRM = "Are you sure you want to delete this pixel?";
		public const String MSG_COMPARISONOPERATOR_REQUIRED_ERROR = "You must select a Comparison Operator.";
		public const String MSG_ARGUMENTTYPEMASK_REQUIRED_ERROR = "You must select an ArgumentTypeMask.";
		public const String MSG_TEXTTYPE_CLOSING_SCRIPTTAG_ERROR = "Every <SCRIPT> tag must have a closing </SCRIPT> tag.";
		public const String MSG_VALUE_REQUIRED_ERROR = "You must enter a Value.";
		public const String MSG_KEY_REQUIRED_ERROR = "You must enter a Key.";
		public const String MSG_MT_ENVIRONMENT_CHANGE = "The operation you requested requires that the application be restarted.  Please manually restart the application after it closes.";
		public const String DOMAIN_EMAIL = "spark.net";
		public const String DOMAIN_EMAIL2 = "matchnet.com";
		public const String DOMAIN_ACCOUNT = "matchnet";
		public const String MSG_NO_MEMBERID = "You must create an account on one of our DEV sites (i.e. http://dev.collegeluv.com) and one of our PROD sites (i.e. http://www.collegeluv.com) using your XXXXX@" + DOMAIN_EMAIL + " email address before accessing this application.";
		public const String PIXELTYPE_IMAGE = "IMAGE";
		public const String PIXELTYPE_TEXT = "TEXT";
		public const String PIXELSTATUS_LIVE = "LIVE";
		public const String PIXELSTATUS_ONHOLD = "ON HOLD";
		public const String PIXELSTATUS_APPROVED = "APPROVED";
		public const String PIXELSTATUS_NOTAPPROVED = "NOT APPROVED";
		public const Int32 DESCRIPTION_SUMMARY_LENGTH = 100;
		public const Int32 MAX_LENGTH_CONTENTCONDITION = 2000;
		public const Int32 MAX_LENGTH_CONTENTCONDITIONARGS = 2000;

		public static readonly String[,] FILTER_TYPE_LIST = new String[,] {{"Country ID Filter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID)).ToString(), "Enter a country's RegionID value (i.e. 223 for USA, 38 for Canada, 105 for Israel, 83 for Germany, 222 for UK, 13 for Australia, 76 for France, 10 for Argentina).  Multiple values can be entered by separating with " + FilterObject.OR_DELIMITER + " (i.e. 223" + FilterObject.OR_DELIMITER + "224)."},
													{"GenderMask Filter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.GenderMask)).ToString(), "Enter a GenderMask value.  Possibilities include:  M seeking F = 9, F seeking M = 6, F seeking F = 10, M seeking M = 5.  Multiple values can be entered by separating with " + FilterObject.OR_DELIMITER + "."},
													{"Promotion Filter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.PromotionID)).ToString(), "Enter a Promotion ID.  Multiple values can be entered by separating with " + FilterObject.OR_DELIMITER+ " (i.e. 10000" + FilterObject.OR_DELIMITER + "10001)."},
													{"Birth Date Filter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.BirthDate)).ToString(), "Enter a date in the form 'YYYYMMDD'(i.e. 19730630 for June 30th, 1973).  This is good to use with the > or < comparison operators."},
													{"Member ID Filter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.MemberID)).ToString(), "Use this along with a > comparison operator to filter Registered users.  'Key > 0' will filter for MemberIDs greater than 0, thus returning all users with valid MemberIDs."},
													{"Subscribed Filter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.IsPayingMemberFlag)).ToString(), "Enter 1 to filter all Subscribed members.  Enter 0 for all non-Subscribed members."},
													{"Landing Page URL Filter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.LandingPageURL)).ToString(), "Enter a valid URL (i.e. 'http://www.yahoo.com').  Multiple values can be entered by separating with " + FilterObject.OR_DELIMITER + " (i.e. http://www.msn.com" + FilterObject.OR_DELIMITER + "http://www.yahoo.com)"},
													{"Search Preference - GenderMask Filter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.GenderMask_SearchPref)).ToString(), "Enter a Search Preference GenderMask value.  Possibilities include:  M seeking F = 9, F seeking M = 6, F seeking F = 10, M seeking M = 5.  Multiple values can be entered by separating with " + FilterObject.OR_DELIMITER + "."}};

		public static readonly String[,] COMPARISON_OPERATOR_LIST = new String[,] {{"equal to", "="},
													{"not equal to", "!="},
													{"less than", "<"},
													{"less than or equal to", "<="},
													{"greater than", ">"},
													{"greater than or equal to", ">="},
													{"contains the words", "contains"}};

		public static readonly String[,] PARAMETER_TYPE_LIST = new String[,] {{"Birth Date Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.BirthDate)).ToString()},
													{"GenderMask Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.GenderMask)).ToString()},
													{"Session ID Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.SessionID)).ToString()},
													{"Member ID Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.MemberID)).ToString()},
													{"Country ID Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID)).ToString()},
													{"Referring URL Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.ReferringURL)).ToString()},
													{"Subscription Amount Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.SubscriptionAmount)).ToString()},
													{"Subscription Duration Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.SubscriptionDuration)).ToString()},
													{"State Abbreviation Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.StateAbbreviation)).ToString()},
													{"Age Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.Age)).ToString()},
													{"Age Group Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.AgeGroup)).ToString()},
													{"PRM Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.PRM)).ToString()},
													{"Search Preference - GenderMask Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.GenderMask)).ToString()},
													{"Search Preference - Country ID Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.CountryRegionID_SearchPref)).ToString()},
													{"Search Preference - State Abbreviation Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.StateAbbreviation_SearchPref)).ToString()},
													{"Search Preference - Min Age Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.MinAge_SearchPref)).ToString()},
													{"Search Preference - Max Age Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.MaxAge_SearchPref)).ToString()},
													{"Search Preference - Min Age Group Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.MinAgeGroup_SearchPref)).ToString()},
													{"Search Preference - Max Age Group Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.MaxAgeGroup_SearchPref)).ToString()},
													{"Timestamp Parameter", ((Int32)(ArgumentObject.ArgumentTypeMaskEnum.Timestamp)).ToString()}};


		/// <summary>
		/// These bits represent each role in this application.
		/// </summary>
		[Flags]
		public enum RolesMaskEnum
		{
			None = 0,
			ArticleEditor = 1,
			PixelEditor = 2,
			PixelApprover = 4,
			PrivilegeGroupEditor = 8,
			PrivilegePrivilegeEditor = 16,
			PrivilegeMemberPrivilegeEditor = 32,
			PixelPublisher = 64
		}

		public enum EnvironmentEnum
		{
			Default,
			DEV,
			PROD
		}

		public static void BuildConstant(ref TextBox ConstantTextbox)
		{
			ConstantTextbox.Text = ConstantTextbox.Text.ToUpper().Replace(" ", "_");

			// Place cursor at end of Text.
			ConstantTextbox.SelectionStart = ConstantTextbox.Text.Length;
		}

		#region Publish Constants
		public const Int32 PUBACTPUBOBJID_PIXEL_ADD = 100;
		public const Int32 PUBACTPUBOBJID_PIXEL_EDIT = 101;
		public const Int32 PUBACTPUBOBJID_PIXEL_DELETE = 102;
		public const Int32 PUBACTPUBOBJID_PIXEL_PUBLISHTOCONTENTSTG = 103;
		public const Int32 PUBACTPUBOBJID_PIXEL_VERIFYINCONTENTSTG = 104;
		public const Int32 PUBACTPUBOBJID_PIXEL_APPROVEINCONTENTSTG = 105;
		public const Int32 PUBACTPUBOBJID_PIXEL_VERIFYWITHPARTNERINCONTENTSTG = 106;
		public const Int32 PUBACTPUBOBJID_PIXEL_PUBLISHTOPROD = 107;
		public const Int32 PUBACTPUBOBJID_PIXEL_VERIFYINPROD = 108;
		public const Int32 PUBACTPUBOBJID_PIXEL_VERIFYWITHPARTNERINPROD = 109;
		public const String PUBACTPUBOBJID_PIXEL_APPROVEINCONTENTSTG_TEXT = "Approve in ContentStg";
		public const String PUBACTPUBOBJID_PIXEL_VERIFYINCONTENTSTG_TEXT = "Verify in ContentStg";
		public const String PUBACTPUBOBJID_PIXEL_PUBLISHTOCONTENTSTG_TEXT = "Publish to ContentStg";
		public const String PUBACTPUBOBJID_PIXEL_VERIFYWITHPARTNERINCONTENTSTG_TEXT = "Verify with Partner in ContentStg";
		public const String PUBACTPUBOBJID_PIXEL_PUBLISHTOPROD_TEXT = "Publish to Prod";
		public const String PUBACTPUBOBJID_PIXEL_VERIFYINPROD_TEXT = "Verify in Prod";
		public const String PUBACTPUBOBJID_PIXEL_VERIFYWITHPARTNERINPROD_TEXT = "Verify with Partner in Prod";
		#endregion
	}
}
