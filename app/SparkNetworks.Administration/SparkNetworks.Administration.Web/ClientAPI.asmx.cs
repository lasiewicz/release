using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;

using Matchnet.Member.ServiceDefinitions;
using Matchnet.Member.ValueObjects;
using Matchnet.Configuration.ValueObjects.ServiceDefinitions;
using Matchnet.Exceptions;
using Matchnet.Content.ValueObjects;
using Matchnet.Content.ValueObjects.ServiceDefinitions;
using Matchnet.Content.ServiceAdapters;
using SparkNetworks.Administration.ValueObjects;

namespace SparkNetworks.Administration.Web
{
	/// <summary>
	/// Summary description for ClientAPI.
	/// </summary>
	[WebService(Namespace="http://spark.net/SparkNetworks.Administration/")]
	public class ClientAPI : System.Web.Services.WebService
	{
		private const String CUSTOMSETTING_CONFIGURATION_SERVICE_KEY = "configuration/services/SettingsUri";
		private const String SERVICEMANAGERNAME_BRANDCONFIG = "BrandConfigSM";
		private const String SERVICEMANAGERNAME_ARTICLE = "ArticleSM";
		private const String SERVICEMANAGERNAME_PAGEPIXEL = "PagePixelSM";
		private const String SERVICEMANAGERNAME_ADMIN = "AdminSM";

		public ClientAPI()
		{
			//CODEGEN: This call is required by the ASP.NET Web Services Designer
			InitializeComponent();
		}

		#region Component Designer generated code
		
		//Required by the Web Services Designer 
		private IContainer components = null;
				
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if(disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);		
		}
		
		#endregion

		[WebMethod]
		public void DeleteMemberPrivilege(Int32 memberID, Int32 privilegeID)
		{
			((IMemberService)getService(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME,
				typeof(IMemberService))).DeleteMemberPrivilege(memberID, privilegeID);
		}

		/// <summary>
		/// ArrayList will be converted to String[] on the client.
		/// Sometimes the client does not want multiple values returned from this call.  In that case, return a constant
		/// value so that the client can act accordingly.
		/// </summary>
		/// <param name="email"></param>
		/// <returns></returns>
		[WebMethod]
		public Member[] GetMembersByEmail(String email, Boolean multipleOK)
		{
			Int32 totalRows = new Int32();

			ArrayList mtMembers = ((IMemberService)getService(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME,
				typeof(IMemberService))).GetMembersByEmail(email, 1, 1, ref totalRows);

			if (!multipleOK && totalRows > 1)
				return new Member[] {new Member(-1, String.Empty, String.Empty)};	// -1 indicates that more than 1 row was returned.
			else
				return ConvertToMembers(mtMembers);
		}

		[WebMethod]
		public Member[] GetMembersByUsername(String username, Boolean multipleOK)
		{
			Int32 totalRows = new Int32();

			ArrayList mtMembers = ((IMemberService)getService(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME,
				typeof(IMemberService))).GetMembersByUserName(username, 1, 1, ref totalRows);

			if (!multipleOK && totalRows > 1)
				return new Member[] {new Member(-1, String.Empty, String.Empty)};	// -1 indicates that more than 1 row was returned.
			else
				return ConvertToMembers(mtMembers);
		}

		[WebMethod]
		public String GetSetting(String constant)
		{
			NameValueCollection settings = ((ISettingsService)getService(CUSTOMSETTING_CONFIGURATION_SERVICE_KEY, 
				typeof(ISettingsService))).GetSettings(Environment.MachineName);

			// First, look for the Global setting from the machine override
			if(settings.Get("OVERRIDE/*/*/*/" + constant) != null)
			{
				return settings.Get("OVERRIDE/*/*/*/" + constant);
			}

			// Next, look for the default Global setting
			if(settings.Get("/*/*/*/" + constant) != null)
			{
				return settings.Get("/*/*/*/" + constant);
			}

			return String.Empty;
		}

		[WebMethod]
		public Site[] GetSites()
		{
			return ConvertToSites(((IBrandConfigService)getService(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				SERVICEMANAGERNAME_BRANDCONFIG,
				typeof(IBrandConfigService))).GetSites());
		}

		[WebMethod]
		public SiteCategory[] RetrieveSiteCategories(Int32 siteID, Boolean forceLoadFlag)
		{
			return ConvertToSiteCategories(((IArticleService)getService(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				SERVICEMANAGERNAME_ARTICLE,
				typeof(IArticleService))).RetrieveSiteCategories(siteID, forceLoadFlag));
		}

		[WebMethod]
		public Int32 SaveSiteCategory(SiteCategory siteCategory)
		{
			return ((IArticleService)getService(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				SERVICEMANAGERNAME_ARTICLE,
				typeof(IArticleService))).SaveSiteCategory(ConvertToMTSiteCategory(siteCategory));
		}

		[WebMethod]
		public Int32 SaveSiteArticle(SiteArticle siteArticle)
		{
			return ((IArticleService)getService(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				SERVICEMANAGERNAME_ARTICLE,
				typeof(IArticleService))).SaveSiteArticle(ConvertToMTSiteArticle(siteArticle));
		}

		[WebMethod]
		public SiteArticle[] RetrieveCategorySiteArticlesAndArticles(Int32 categoryID, Int32 siteID, Boolean forceLoadFlag)
		{
			return ConvertToSiteArticles(((IArticleService)getService(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				SERVICEMANAGERNAME_ARTICLE,
				typeof(IArticleService))).RetrieveCategorySiteArticlesAndArticles(categoryID, siteID, forceLoadFlag));
		}

		[WebMethod]
		public Category RetrieveCategory(Int32 categoryID)
		{
			return ConvertToCategory(((IArticleService)getService(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				SERVICEMANAGERNAME_ARTICLE,
				typeof(IArticleService))).RetrieveCategory(categoryID));
		}

		[WebMethod]
		public Int32 SaveCategory(Category category)
		{
			return ((IArticleService)getService(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				SERVICEMANAGERNAME_ARTICLE,
				typeof(IArticleService))).SaveCategory(ConvertToMTCategory(category));
		}

		[WebMethod]
		public Article RetrieveArticle(Int32 articleID)
		{
			return ConvertToArticle(((IArticleService)getService(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				SERVICEMANAGERNAME_ARTICLE,
				typeof(IArticleService))).RetrieveArticle(articleID));
		}

		[WebMethod]
		public Int32 SaveArticle(Article article)
		{
			return ((IArticleService)getService(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				SERVICEMANAGERNAME_ARTICLE,
				typeof(IArticleService))).SaveArticle(ConvertToMTArticle(article));
		}

		[WebMethod]
		public PagePixel[] RetrievePagePixels(Int32 pageID, Int32 siteID, AdminDisplayTypeMaskEnum adminDisplayTypeMaskEnum)
		{
			return ConvertToPagePixels(((IPagePixelService)getService(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				SERVICEMANAGERNAME_PAGEPIXEL,
				typeof(IPagePixelService))).RetrievePagePixels(pageID, siteID, (Matchnet.Content.ValueObjects.PagePixel.PagePixelCollection.AdminDisplayTypeMaskEnum)adminDisplayTypeMaskEnum));
		}

		[WebMethod]
		public void SavePagePixel(PagePixel pagePixel, Int32 memberID, Int32 pubActPubObjID)
		{
			((IPagePixelService)getService(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				SERVICEMANAGERNAME_PAGEPIXEL,
				typeof(IPagePixelService))).SavePagePixel(ConvertToMTPagePixel(pagePixel), memberID, pubActPubObjID);
		}

		[WebMethod]
		public void PublishPagePixel(PagePixel pagePixel, Int32 memberID, Int32 pubActPubObjID)
		{
			((IPagePixelService)getService(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				SERVICEMANAGERNAME_PAGEPIXEL,
				typeof(IPagePixelService))).PublishPagePixel(ConvertToMTPagePixel(pagePixel), memberID, pubActPubObjID);
		}

		[WebMethod]
		public void ApprovePagePixel(Int32 pagePixelID, Int32 approverMemberID, Int32 publishID)
		{
			((IPagePixelService)getService(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				SERVICEMANAGERNAME_PAGEPIXEL,
				typeof(IPagePixelService))).ApprovePagePixel(pagePixelID, approverMemberID, publishID);
		}

		[WebMethod]
		public void DeletePagePixel(Int32 pagePixelID, Int32 memberID)
		{
			((IPagePixelService)getService(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				SERVICEMANAGERNAME_PAGEPIXEL,
				typeof(IPagePixelService))).DeletePagePixel(pagePixelID, false, memberID);
		}

		[WebMethod]
		public Int32 CheckPublishActionPublishObjectStep(Int32 publishID, Int32 publishActionPublishObjectID, String environmentType)
		{
			return ((IAdminService)getService(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				SERVICEMANAGERNAME_ADMIN,
				typeof(IAdminService))).CheckPublishActionPublishObjectStep(publishID, publishActionPublishObjectID, environmentType);
		}

		[WebMethod]
		public void GetNextPublishActionPublishObjectStep(Int32 publishID, out Int32 publishActionPublishObjectID, out String environmentType)
		{
			((IAdminService)getService(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				SERVICEMANAGERNAME_ADMIN,
				typeof(IAdminService))).GetNextPublishActionPublishObjectStep(publishID, out publishActionPublishObjectID, out environmentType);
		}

		[WebMethod]
		public Privilege[] GetPrivileges()
		{
			return ConvertToPriviliges(((IMemberService)getService(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME,
				typeof(IMemberService))).GetPrivileges());
		}

		[WebMethod]
		public Member[] GetMembersByPrivilege(Int32 privilegeID)
		{
			return ConvertToMembersByMemberIDs(((IMemberService)getService(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME,
				typeof(IMemberService))).GetMembersByPrivilege(privilegeID));
		}

		[WebMethod]
		public SecurityGroup[] GetGroupsByPrivilege(Int32 privilegeID)
		{
			return ConvertToSecurityGroup(((IMemberService)getService(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME,
				typeof(IMemberService))).GetGroupsByPrivilege(privilegeID));
		}

		[WebMethod]
		public void SaveMemberPrivilege(Int32 memberID, Int32 privilegeID)
		{
			((IMemberService)getService(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME,
				typeof(IMemberService))).SaveMemberPrivilege(memberID, privilegeID);
		}

		[WebMethod]
		public SecurityGroupMember[] GetSecurityGroupMembers(Int32 groupID)
		{
			return ConvertToSecurityGroupMembers(((IMemberService)getService(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME,
				typeof(IMemberService))).GetSecurityGroupMembers(groupID), groupID);
		}

		[WebMethod]
		public Member GetMember(Int32 memberID)
		{
			Int32 notUsed = new Int32();

			return ConvertToMember(((IMemberService)getService(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME,
				typeof(IMemberService))).GetMembersByMemberID(memberID, ref notUsed));
		}

		[WebMethod]
		public SecurityGroup[] GetSecurityGroups()
		{
			return ConvertToSecurityGroup(((IMemberService)getService(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME,
				typeof(IMemberService))).GetSecurityGroups());
		}

		[WebMethod]
		public void SaveSecurityGroupMember(Int32 memberID, Int32 groupID)
		{
			((IMemberService)getService(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME,
				typeof(IMemberService))).SaveSecurityGroupMember(memberID, groupID);
		}

		[WebMethod]
		public void DeleteSecurityGroupMember(Int32 memberID, Int32 groupID)
		{
			((IMemberService)getService(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME,
				typeof(IMemberService))).DeleteSecurityGroupMember(memberID, groupID);
		}

		[WebMethod]
		public MemberPrivilege[] GetMemberPrivileges(Int32 memberID)
		{
			return ConvertToMemberPrivileges(((IMemberService)getService(Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				Matchnet.Member.ValueObjects.ServiceConstants.SERVICE_MANAGER_NAME,
				typeof(IMemberService))).GetMemberPrivileges(memberID));
		}

		[WebMethod]
		public void SavePublishAction(Int32 publishID, Int32 publishActionPublishObjectID, Int32 memberID)
		{
			((IAdminService)getService(Matchnet.Content.ValueObjects.ServiceConstants.SERVICE_CONSTANT, 
				SERVICEMANAGERNAME_ADMIN,
				typeof(IAdminService))).SavePublishAction(publishID, publishActionPublishObjectID, memberID);
		}


		#region Private Methods
		private MemberPrivilege[] ConvertToMemberPrivileges(Matchnet.Member.ValueObjects.Privilege.MemberPrivilegeCollection mtMemberPrivileges)
		{
			MemberPrivilege[] memberPrivileges = new MemberPrivilege[mtMemberPrivileges.Count];

			Int32 i = 0;
			foreach(Matchnet.Member.ValueObjects.Privilege.MemberPrivilege mtMemberPrivilege in mtMemberPrivileges)
			{
				memberPrivileges[i] = new MemberPrivilege(new Privilege(mtMemberPrivilege.Privilege.PrivilegeID, mtMemberPrivilege.Privilege.Description),
					((PrivilegeState)((Int32)mtMemberPrivilege.PrivilegeState)));

				i++;
			}

			return memberPrivileges;
		}

		/// <summary>
		/// Only return the first Member.
		/// </summary>
		/// <param name="mtMembers"></param>
		/// <returns></returns>
		private Member ConvertToMember(ArrayList mtMembers)
		{
			CachedMember mtMember = (CachedMember)mtMembers[0];

			return new Member(mtMember.MemberID, mtMember.Username, mtMember.EmailAddress);
		}

		private SecurityGroupMember[] ConvertToSecurityGroupMembers(Matchnet.Member.ValueObjects.Privilege.SecurityGroupMemberCollection mtSecurityGroupMembers,
			Int32 groupID)
		{
			SecurityGroupMember[] securityGroupMembers = new SecurityGroupMember[mtSecurityGroupMembers.Count];

			Int32 i = 0;
			foreach(Int32 memberID in mtSecurityGroupMembers)
			{
				securityGroupMembers[i] = new SecurityGroupMember(memberID, groupID);

				i++;
			}

			return securityGroupMembers;
		}

		private SecurityGroup[] ConvertToSecurityGroup(Matchnet.Member.ValueObjects.Privilege.SecurityGroupCollection mtSecurityGroups)
		{
			SecurityGroup[] securityGroups = new SecurityGroup[mtSecurityGroups.Count];

			Int32 i = 0;
			foreach(Matchnet.Member.ValueObjects.Privilege.SecurityGroup mtSecurityGroup in mtSecurityGroups)
			{
				securityGroups[i] = new SecurityGroup(mtSecurityGroup.SecurityGroupID, mtSecurityGroup.Description);

				i++;
			}

			return securityGroups;
		}

		private Member[] ConvertToMembers(ArrayList mtMembers)
		{
			Member[] members = new Member[mtMembers.Count];

			for (Int32 i = 0; i < mtMembers.Count; i++)
			{
				CachedMember mtMember = (CachedMember)mtMembers[i];

				members[i] = new Member(mtMember.MemberID, mtMember.Username, mtMember.EmailAddress);
			}

			return members;
		}

		private Member[] ConvertToMembersByMemberIDs(ArrayList mtMemberIDs)
		{
			Member[] members = new Member[mtMemberIDs.Count];

			for (Int32 i = 0; i < mtMemberIDs.Count; i++)
			{
				Matchnet.Member.ServiceAdapters.Member mtMember = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMember(Convert.ToInt32(mtMemberIDs[i]), Matchnet.Member.ServiceAdapters.MemberLoadFlags.None);

				members[i] = new Member(mtMember.MemberID, mtMember.Username, mtMember.EmailAddress);
			}

			return members;
		}

		private Privilege[] ConvertToPriviliges(Matchnet.Member.ValueObjects.Privilege.PrivilegeCollection mtPrivileges)
		{
			Privilege[] privileges = new Privilege[mtPrivileges.Count];

			Int32 i = 0;
			foreach(Matchnet.Member.ValueObjects.Privilege.Privilege mtPrivilege in mtPrivileges)
			{
				privileges[i] = new Privilege(mtPrivilege.PrivilegeID, mtPrivilege.Description);

				i++;
			}

			return privileges;
		}

		private Matchnet.Content.ValueObjects.PagePixel.PagePixel ConvertToMTPagePixel(PagePixel pagePixel)
		{
			Int32 pagePixelID = pagePixel.PagePixelID > 0 ? pagePixel.PagePixelID : Int32.MinValue;

			return new Matchnet.Content.ValueObjects.PagePixel.PagePixel(pagePixelID,
				pagePixel.PageID, pagePixel.SiteID, pagePixel.Content, pagePixel.ContentCondition, pagePixel.ContentConditionArgs,
				pagePixel.UpdateDate, pagePixel.ImgFlag, pagePixel.Description, pagePixel.AddDate, pagePixel.EndDate, (Boolean)pagePixel.ActiveFlag,
				(Boolean)pagePixel.PublishFlag, (Boolean)pagePixel.ApprovalFlag, pagePixel.ApproverMemberID, pagePixel.ApprovedDate, pagePixel.PublishID);
		}

		private PagePixel[] ConvertToPagePixels(Matchnet.Content.ValueObjects.PagePixel.PagePixelCollection mtPagePixels)
		{
			PagePixel[] pagePixels = new PagePixel[mtPagePixels.Count];

			for (Int32 i = 0; i < mtPagePixels.Count; i++)
			{
				Matchnet.Content.ValueObjects.PagePixel.PagePixel mtPagePixel = mtPagePixels[i];

				pagePixels[i] = new PagePixel(mtPagePixel.PagePixelID, mtPagePixel.PageID, mtPagePixel.SiteID, mtPagePixel.Content, mtPagePixel.ContentCondition, 
					mtPagePixel.ContentConditionArgs, mtPagePixel.UpdateDate, mtPagePixel.ImgFlag, mtPagePixel.Description, mtPagePixel.AddDate,
					mtPagePixel.EndDate, mtPagePixel.ActiveFlag, mtPagePixel.IsPublishedFlag, mtPagePixel.ApprovalFlag, mtPagePixel.ApproverMemberID,
					mtPagePixel.ApprovedDate, mtPagePixel.PublishID);
			}

			return pagePixels;
		}

		private Matchnet.Content.ValueObjects.Article.Article ConvertToMTArticle(Article article)
		{
			Int32 articleID = article.ArticleID > 0 ? article.ArticleID : Int32.MinValue;

			return new Matchnet.Content.ValueObjects.Article.Article(articleID, article.CategoryID, article.DefaultContent, article.Constant, article.LastUpdated);
		}

		private Article ConvertToArticle(Matchnet.Content.ValueObjects.Article.Article mtArticle)
		{
			return new Article(mtArticle.ArticleID, mtArticle.CategoryID, mtArticle.DefaultContent, mtArticle.Constant, mtArticle.LastUpdated);
		}

		private Matchnet.Content.ValueObjects.Article.Category ConvertToMTCategory(Category category)
		{
			Int32 categoryID = category.CategoryID > 0 ? category.CategoryID : Int32.MinValue;

			return new Matchnet.Content.ValueObjects.Article.Category(categoryID, category.ParentCategoryID, category.ListOrder, category.Constant, category.LastUpdated);
		}

		private Category ConvertToCategory(Matchnet.Content.ValueObjects.Article.Category mtCategory)
		{
			return new Category(mtCategory.CategoryID, mtCategory.ParentCategoryID, mtCategory.ListOrder, mtCategory.Constant, mtCategory.LastUpdated);
		}

		private SiteArticle[] ConvertToSiteArticles(Matchnet.Content.ValueObjects.Article.SiteArticleCollection mtSiteArticles)
		{
			SiteArticle[] siteArticles = new SiteArticle[mtSiteArticles.Count];

			for (Int32 i = 0; i < mtSiteArticles.Count; i++)
			{
				Matchnet.Content.ValueObjects.Article.SiteArticle mtSiteArticle = mtSiteArticles[i];

				siteArticles[i] = new SiteArticle(mtSiteArticle.SiteArticleID,
					mtSiteArticle.ArticleID,
					mtSiteArticle.SiteID,
					mtSiteArticle.ArticleData.Constant,
					mtSiteArticle.MemberID,
					mtSiteArticle.Ordinal,
					mtSiteArticle.LastUpdated,
					mtSiteArticle.PublishedFlag,
					mtSiteArticle.FeaturedFlag,
					mtSiteArticle.FileID,
					mtSiteArticle.Title,
					mtSiteArticle.Content);
			}

			return siteArticles;
		}

		private Matchnet.Content.ValueObjects.Article.SiteArticle ConvertToMTSiteArticle(SiteArticle siteArticle)
		{
			Int32 siteArticleID = siteArticle.SiteArticleID > 0 ? siteArticle.SiteArticleID : Int32.MinValue;

			return new Matchnet.Content.ValueObjects.Article.SiteArticle(siteArticleID,
				siteArticle.ArticleID,
				siteArticle.SiteID,
				siteArticle.MemberID,
				siteArticle.Ordinal,
				siteArticle.PublishedFlag,
				siteArticle.FeaturedFlag,
				siteArticle.FileID,
				siteArticle.Title,
				siteArticle.Content);
		}

		private Matchnet.Content.ValueObjects.Article.SiteCategory ConvertToMTSiteCategory(SiteCategory siteCategory)
		{
			Int32 siteCategoryID = siteCategory.SiteCategoryID > 0 ? siteCategory.SiteCategoryID : Int32.MinValue;

			return new Matchnet.Content.ValueObjects.Article.SiteCategory(siteCategoryID, siteCategory.CategoryID, siteCategory.SiteID, siteCategory.Content, siteCategory.PublishedFlag);
		}

		private SiteCategory[] ConvertToSiteCategories(Matchnet.Content.ValueObjects.Article.SiteCategoryCollection mtSiteCategories)
		{
			SiteCategory[] siteCategories = new SiteCategory[mtSiteCategories.Count];

			for (Int32 i = 0; i < mtSiteCategories.Count; i++)
			{
				Matchnet.Content.ValueObjects.Article.SiteCategory mtSiteCategory = mtSiteCategories[i];

				siteCategories[i] = new SiteCategory(mtSiteCategory.SiteCategoryID,
					mtSiteCategory.CategoryID,
					mtSiteCategory.SiteID,
					mtSiteCategory.CategoryData.ParentCategoryID,
					mtSiteCategory.Content,
					mtSiteCategory.CategoryData.Constant,
					mtSiteCategory.PublishedFlag,
					mtSiteCategory.LastUpdated);
			}

			return siteCategories;
		}

		private Site[] ConvertToSites(Matchnet.Content.ValueObjects.BrandConfig.Sites mtSites)
		{
			Site[] sites = new Site[mtSites.Count];

			for (Int32 i = 0; i < mtSites.Count; i++)
			{
				Matchnet.Content.ValueObjects.BrandConfig.Site mtSite = mtSites[i];

				sites[i] = new Site(mtSite.SiteID, mtSite.Name);
			}

			return sites;
		}

		#region For Matchnet.Member, Matchnet.Content.
		internal static Object getService(String serviceConstant, String serviceManagerName, Type serviceType)
		{
			String uri = getServiceManagerUri(serviceConstant, serviceManagerName);

			try
			{
				return Activator.GetObject(serviceType, uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		internal static string getServiceManagerUri(String serviceConstant, String serviceManagerName)
		{
			try
			{
				return Matchnet.Configuration.ServiceAdapters.AdapterConfigurationSA.GetServicePartition(serviceConstant, Matchnet.Configuration.ServiceAdapters.PartitionMode.Random).ToUri(serviceManagerName);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot get configuration settings for remote service manager.", ex));
			}
		}
		#endregion

		#region For Matchnet.Configuration.
		internal static Object getService(String serviceKey, Type serviceType)
		{
			String uri = getServiceManagerUri(serviceKey);

			try
			{
				return Activator.GetObject(serviceType, uri);
			}
			catch(Exception ex)
			{
				throw(new SAException("Cannot activate remote service manager at " + uri, ex));
			}
		}

		internal static string getServiceManagerUri(String serviceKey)
		{
			try
			{
				return Matchnet.InitialConfiguration.InitializationSettings.Get(CUSTOMSETTING_CONFIGURATION_SERVICE_KEY);
			}
			catch(Exception ex)
			{
				throw(new Exception("Cannot get AdapterConfiguration URI based off of CustomSettings key of: " + CUSTOMSETTING_CONFIGURATION_SERVICE_KEY + ".",ex));
			}
		}
		#endregion
		#endregion
	}
}
