using System;

namespace SparkNetworks.Administration
{
	/// <summary>
	/// Summary description for User.
	/// </summary>
	public class User
	{
		private Constants.RolesMaskEnum userRolesMask;

		#region Constructors
		public User(Constants.RolesMaskEnum userRolesMask)
		{
			this.userRolesMask = userRolesMask;
		}
		#endregion

		#region Public properties
		/// <summary>
		/// Used to translate our Authorization values into a PagePixelCollection.AdminDisplayTypeMaskEnum value.
		/// </summary>
		public Web.AdminDisplayTypeMaskEnum PixelAdminDisplayTypeMask
		{
			get
			{
				if (IsPixelEditor && IsPixelApprover)
					return Web.AdminDisplayTypeMaskEnum.AdministratorAndApprover;
				else if (IsPixelEditor || IsPixelPublisher)
					return Web.AdminDisplayTypeMaskEnum.Administrator;
				else if (IsPixelApprover)
					return Web.AdminDisplayTypeMaskEnum.Approver;
				else
					return Web.AdminDisplayTypeMaskEnum.NormalUser;
			}
		}
		
		public Boolean IsArticleEditor
		{
			get
			{
				if ((userRolesMask & Constants.RolesMaskEnum.ArticleEditor) == Constants.RolesMaskEnum.ArticleEditor)
					return true;
				else
					return false;
			}
		}

		public Boolean IsPixelEditor
		{
			get
			{
				if ((userRolesMask & Constants.RolesMaskEnum.PixelEditor) == Constants.RolesMaskEnum.PixelEditor)
					return true;
				else
					return false;
			}
		}

		public Boolean IsPixelApprover
		{
			get
			{
				if ((userRolesMask & Constants.RolesMaskEnum.PixelApprover) == Constants.RolesMaskEnum.PixelApprover)
					return true;
				else
					return false;
			}
		}

		public Boolean IsPrivilegePrivilegeEditor
		{
			get
			{
				if ((userRolesMask & Constants.RolesMaskEnum.PrivilegePrivilegeEditor) == Constants.RolesMaskEnum.PrivilegePrivilegeEditor)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		public Boolean IsPrivilegeGroupEditor
		{
			get
			{
				if ((userRolesMask & Constants.RolesMaskEnum.PrivilegeGroupEditor) == Constants.RolesMaskEnum.PrivilegeGroupEditor)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		public Boolean IsPrivilegeMemberPrivilegeEditor
		{
			get
			{
				if ((userRolesMask & Constants.RolesMaskEnum.PrivilegeMemberPrivilegeEditor) == Constants.RolesMaskEnum.PrivilegeMemberPrivilegeEditor)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		public Boolean IsPixelPublisher
		{
			get
			{
				if ((userRolesMask & Constants.RolesMaskEnum.PixelPublisher) == Constants.RolesMaskEnum.PixelPublisher)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		#endregion
	}
}
