echo "Copying Client..."

robocopy C:\Matchnet\bedrock\App\SparkNetworks.Administration\bin\Release_PROD_DEV\Client\ \\devweb01\c$\matchnet\bedrock\App\SparkNetworks.Administration\Client\ * /s /xo /r:999 /w:0

echo "Copying Web Service..."

robocopy C:\Matchnet\bedrock\App\SparkNetworks.Administration\SparkNetworks.Administration.Web\ \\devweb01\c$\matchnet\bedrock\App\SparkNetworks.Administration\ * /xo /r:999 /w:0
robocopy C:\Matchnet\bedrock\App\SparkNetworks.Administration\SparkNetworks.Administration.Web\bin\Release_PROD_DEV\ \\devweb01\c$\matchnet\bedrock\App\SparkNetworks.Administration\bin\ * /xo /r:999 /w:0

rem psexec \\devweb01 iisreset /restart

pause

