using System;
using System.Collections.Specialized;

public sealed class Singleton<T> where T : new()
{
    private readonly static T _instance;

    static Singleton() 
    {
        _instance = new T();
    }

    private Singleton() { }

    public static T Instance
    {
        get
        {
            return _instance;
        }
    }
}