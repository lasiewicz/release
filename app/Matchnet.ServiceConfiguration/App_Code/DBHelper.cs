using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

public class DBHelper
{
	public DBHelper()
	{
		
	}

    private static SqlConnection CreateConnection()
    {
        return new SqlConnection(ConfigurationManager.ConnectionStrings["DataBase"].ConnectionString);
    }

    private static SqlCommand CreateCommand(string procName, params SqlParameter[] parameters)
    {
        SqlCommand cmd = new SqlCommand(procName, CreateConnection());
        cmd.CommandType = CommandType.StoredProcedure;
        foreach (SqlParameter param in parameters)
        {
            cmd.Parameters.AddWithValue(param.ParameterName, param.Value);
        }
        return cmd;
    }

    public static DataTable GetTable(string proceName, params SqlParameter[] parameters)
    {
        SqlCommand cmd = CreateCommand(proceName, parameters);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
    }

    public static object ExcuteScalar(string procName, params SqlParameter[] parameters)
    {
        SqlCommand cmd = CreateCommand(procName, parameters);
        using (cmd.Connection)
        {
            cmd.Connection.Open();
            return cmd.ExecuteScalar();
        }
    }

    public static void ExcuteNonQuery(string procName, params SqlParameter[] parameters)
    {
        SqlCommand cmd = CreateCommand(procName, parameters);
        using (cmd.Connection)
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
        }
    }
}
