using System;
using System.Data;
using System.Data.SqlClient;

public class ServerSetting
{
	public ServerSetting()
	{
		
	}

    public static ServerSetting Instance
    {
        get
        {
            return Singleton<ServerSetting>.Instance;
        }
    }

    public DataTable GetServers()
    {
        return DBHelper.GetTable("dbo.up_config_servers");
    }

    public DataTable GetServerSettings(int serverID)
    {
        return DBHelper.GetTable("dbo.up_config_serverSettings", new SqlParameter("@ServerID", serverID));
    }

    public DataTable GetGlobalSettings()
    {
        return DBHelper.GetTable("dbo.up_config_globalSettings");
    }

    public bool IsSettingAlreadyAdded(int serverID, int settingID)
    {
        return Convert.ToBoolean(DBHelper.ExcuteScalar("dbo.up_config_isSettingInPlace", new SqlParameter("@ServerID", serverID), new SqlParameter("@SettingID", settingID)));
    }

    public int AddServerSetting(int serverID, int settingID)
    {
        return Convert.ToInt32(DBHelper.ExcuteScalar("dbo.up_config_addServerSetting", new SqlParameter("@ServerID", serverID), new SqlParameter("@SettingID", settingID)));
    }

    public void DeleteServerSetting(int serverSettingID)
    {
        DBHelper.ExcuteNonQuery("dbo.up_config_deleteServerSetting", new SqlParameter("@ServerSettingID", serverSettingID));
    }

    public void CreateGlobalSetting(int settingCategoryID, string settingName, string settingValue, string settingDescription)
    {
        DBHelper.ExcuteNonQuery("dbo.up_config_createGlobalSetting", new SqlParameter("@SettingCategoryID", settingCategoryID),
                                                                     new SqlParameter("@SettingConstant", settingName),
                                                                     new SqlParameter("@GlobalDefaultValue", settingValue),
                                                                     new SqlParameter("@SettingDescription", settingDescription));
    }

    public DataTable GetSettingCategories()
    {
        return DBHelper.GetTable("dbo.up_config_settingCategories");
    }

    public void AddHost(string serverName)
    {
        DBHelper.ExcuteNonQuery("dbo.up_config_addServer", new SqlParameter("@ServerName", serverName));
    }

    public bool IsServerInPlace(string serverName)
    {
        return Convert.ToBoolean(DBHelper.ExcuteScalar("dbo.up_config_isServerAdded", new SqlParameter("@ServerName", serverName)));
    }

    public void UpdateServerSetting(int serverSettingID, string serverSettingValue)
    {
        DBHelper.ExcuteNonQuery("dbo.up_config_updateServerSetting", new SqlParameter("@serverSettingID", serverSettingID), new SqlParameter("@serverSettingValue", serverSettingValue));
    }
}
