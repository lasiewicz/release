<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="AddSetting.aspx.cs" Inherits="AddSetting" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:GridView runat="server" ID="GridAllSettings" CellPadding="3" Caption="<b><font color='blue'>Global Setting Values</font></b><br /><br />" DataKeyNames="SettingID" AllowPaging="true" PageSize="20" DataSourceID="DataSourceAllSettings" AutoGenerateColumns="false" OnSelectedIndexChanged="GridAllSettings_SelectedIndexChanged">
        <PagerSettings Mode="numeric" PageButtonCount="10" />
        <PagerStyle HorizontalAlign="center" ForeColor="maroon" Font-Bold="true" />
        <Columns>
            <asp:BoundField HeaderText="Setting Category" ItemStyle-Width="160px" DataField="SettingCategoryName" />
            <asp:BoundField HeaderText="Setting Name" ItemStyle-HorizontalAlign="left" ItemStyle-Width="160px" DataField="SettingConstant" />
            <asp:BoundField HeaderText="Setting value" ItemStyle-HorizontalAlign="left" ItemStyle-Width="160px" DataField="GlobalDefaultValue" />
            <asp:CheckBoxField HeaderText="Community" DataField="IsRequiredForCommunity" />
            <asp:CheckBoxField HeaderText="Site" DataField="IsRequiredForSite" ItemStyle-Width="80px"  />
            <asp:CommandField ButtonType="link" SelectText="Add" ShowSelectButton="true" />
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource runat="server" ID="DataSourceAllSettings" SelectMethod="GetGlobalSettings" TypeName="ServerSetting" OnObjectCreating="DataSourceAllSettings_ObjectCreating"></asp:ObjectDataSource>
    <br />
    <asp:LinkButton runat="server" ID="ButtonServerSettings" Text="Back to my settings" OnClick="ButtonServerSettings_Click"></asp:LinkButton>
</asp:Content>
