<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CreateSetting.aspx.cs" Inherits="LocalService_CreateSetting" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:DetailsView ID="DetailsViewNewSetting" DataSourceID="DataSourceNewSetting" AutoGenerateInsertButton="true" Caption="<b><font color='blue'>Create New Global Setting</font></b><br /><br />" CellPadding="5" AutoGenerateRows="false" runat="server" DefaultMode="insert" BorderColor="black" BorderWidth="1px" BorderStyle="solid" OnItemCommand="DetailsViewNewSetting_ItemCommand">
        <Fields>
            <asp:TemplateField HeaderText="Setting Category">
                <InsertItemTemplate>
                    <asp:DropDownList runat="server" ID="DropDownSettingCategory" SelectedValue='<%# Bind("settingCategoryID") %>' DataSourceID="DataSourceSettingCateogry" DataTextField="SettingCategoryName" DataValueField="SettingCategoryID"></asp:DropDownList>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="Setting Name" DataField="settingName" />
            <asp:BoundField HeaderText="Setting Value" DataField="settingValue" />
            <asp:BoundField HeaderText="Description" DataField="settingDescription" ConvertEmptyStringToNull="false" />
        </Fields>
    </asp:DetailsView>
    <asp:ObjectDataSource runat="server" ID="DataSourceNewSetting" TypeName="ServerSetting" InsertMethod="CreateGlobalSetting" OnObjectCreating="DataSourceNewSetting_ObjectCreating">
        <InsertParameters>
            <asp:Parameter Name="settingCategoryID" Type="int32" />
            <asp:Parameter Name="settingName" Type="string" />
            <asp:Parameter Name="settingValue" Type="string" />
            <asp:Parameter Name="settingDescription" Type="string" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource runat="server" ID="DataSourceSettingCateogry" TypeName="ServerSetting"  EnableCaching="true" CacheDuration="60000" SelectMethod="GetSettingCategories" OnObjectCreating="DataSourceSettingCateogry_ObjectCreating">
    </asp:ObjectDataSource>
</asp:Content>
