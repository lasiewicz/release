using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class LocalService_CreateSetting : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void DetailsViewNewSetting_ItemCommand(object sender, DetailsViewCommandEventArgs e)
    {
        if (e.CommandName == "Cancel")
            Response.Redirect(ResolveUrl("~/LocalService/Settings.aspx?serverID=" + Request.QueryString["serverID"]));
    }
    protected void DataSourceNewSetting_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = ServerSetting.Instance;
    }
    protected void DataSourceSettingCateogry_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = ServerSetting.Instance;
    }
}
