using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Settings : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void ButtonAddSetting_Click(object sender, EventArgs e)
    {
        Response.Redirect(ResolveUrl("~/LocalService/AddSetting.aspx?serverID=" + Request.QueryString["serverID"]));
    }
    protected void DataSourceServerSettings_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = ServerSetting.Instance;
    }
    protected void GridServerSettings_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (!IsPostBack && Request.QueryString["serverSettingID"] != null)
        {
            DataRowView rowView = e.Row.DataItem as DataRowView;
            if (rowView != null)
            {
                if (Request.QueryString["serverSettingID"] == rowView["ServerSettingID"].ToString())
                {                   
                    GridServerSettings.EditIndex = e.Row.RowIndex;
                }
            }
        }  
    }
    protected void ButtonCreateSetting_Click(object sender, EventArgs e)
    {
        Response.Redirect(ResolveUrl("~/LocalService/CreateSetting.aspx?serverID=") + Request.QueryString["serverID"]);
    }
}
