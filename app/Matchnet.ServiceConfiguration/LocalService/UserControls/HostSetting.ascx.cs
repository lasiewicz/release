using System;
using System.Net;
using System.Data;


public partial class UserControls_HostSetting : System.Web.UI.UserControl
{
    private string _clientHost;

    protected void Page_Load(object sender, EventArgs e)
    {
        _clientHost = Dns.GetHostEntry(Request.UserHostAddress).HostName;
        _clientHost = _clientHost.Substring(0, _clientHost.IndexOf("."));
    }
    protected void DataSourceServers_ObjectCreating(object sender, System.Web.UI.WebControls.ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = ServerSetting.Instance;
    }
    protected void GridServers_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        DataRowView rowView = e.Row.DataItem as DataRowView;
        if (rowView != null)
        {
            if (_clientHost != rowView["Name"].ToString())
            {
                e.Row.Cells[1].Enabled = false;
            }
        }
    }
    protected void ButtonCreateHost_Click(object sender, EventArgs e)
    {
        string input = TextBoxNewHost.Text.Trim();
        if (ButtonCreateHost.Text == "Create Host")
        {
            if (input != "")
            {
                if (_clientHost.ToLower() != input.ToLower())
                {
                    TextBoxNewHost.Enabled = false;
                    ButtonCreateHost.Text = "Add Anyway";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "script1", "alert('The host you are adding does not match the current client machine name.  You can still add though.');", true);
                }
                else if (ServerSetting.Instance.IsServerInPlace(input))
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "script1", "alert('The host you are adding already exists in the system.');", true);
                }
                else
                {
                    ServerSetting.Instance.AddHost(input);
                    DataSourceServers.Select();
                    DataBind();
                }
            }
        }
        else if (ButtonCreateHost.Text == "Add Anyway")
        {
            ServerSetting.Instance.AddHost(input);
            DataSourceServers.Select();
            DataBind();
        }
    }
}
