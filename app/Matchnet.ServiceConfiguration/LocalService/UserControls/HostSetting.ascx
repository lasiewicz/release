<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HostSetting.ascx.cs" Inherits="UserControls_HostSetting" %>
<table cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td valign="top">
            <b>Select an exiting host:</b>
        </td>
        <td width="10">
        </td>
        <td align="left">
            <asp:GridView ID="GridServers" runat="server" GridLines="none" DataKeyNames="ServerID"
                BorderColor="black" BorderWidth="1px" BorderStyle="solid" DataSourceID="DataSourceServers"
                AutoGenerateColumns="false" AllowPaging="true" PageSize="15" OnRowDataBound="GridServers_RowDataBound">
                <PagerSettings Mode="nextPrevious" NextPageText="Next" PreviousPageText="Previous" />
                <PagerStyle HorizontalAlign="center" />
                <SelectedRowStyle BackColor="gray" />
                <Columns>
                    <asp:BoundField HeaderText="Hosts" DataField="Name" ItemStyle-BorderColor="gray"
                        ItemStyle-BorderStyle="solid" ItemStyle-BorderWidth="1px" ItemStyle-Width="160px"
                        ItemStyle-HorizontalAlign="center" />
                    <asp:HyperLinkField ItemStyle-BorderColor="gray" ControlStyle-ForeColor="blue" DataNavigateUrlFields="ServerID" Text="Select" DataNavigateUrlFormatString="~/LocalService/Settings.aspx?serverid={0}" ItemStyle-BorderStyle="solid" ItemStyle-BorderWidth="1px" />
                </Columns>
            </asp:GridView>
            <asp:ObjectDataSource ID="DataSourceServers" runat="server" OnObjectCreating="DataSourceServers_ObjectCreating"
                SelectMethod="GetServers" TypeName="ServerSetting"></asp:ObjectDataSource>
        </td>
    </tr>
    <tr>
        <td colspan="3" height="10">
        </td>
    </tr>
    <tr>
        <td valign="top">
            <b>Or create a new host:</b>
        </td>
        <td width="10">
        </td>
        <td align="left">
            <asp:Panel ID="PanelNewHost" runat="server">
                <asp:TextBox ID="TextBoxNewHost" runat="server"></asp:TextBox>
                <asp:Button ID="ButtonCreateHost" runat="server" Text="Create Host" OnClick="ButtonCreateHost_Click" />
            </asp:Panel>
        </td>
    </tr>
</table>
