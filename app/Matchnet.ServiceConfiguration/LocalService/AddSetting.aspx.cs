using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class AddSetting : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    protected void GridAllSettings_SelectedIndexChanged(object sender, EventArgs e)
    {
        int serverID = Convert.ToInt32(Request.QueryString["serverID"]);
        int settingID = Convert.ToInt32(GridAllSettings.DataKeys[GridAllSettings.SelectedIndex].Value);
        if (ServerSetting.Instance.IsSettingAlreadyAdded(serverID, settingID))
        {
            this.ClientScript.RegisterStartupScript(this.GetType(), "script1", "alert('Oops!  This setting is already in your server setting list.');", true);
        }
        else
        {
            int serverSettingID = ServerSetting.Instance.AddServerSetting(serverID, settingID);
            Response.Redirect("~/LocalService/Settings.aspx?serverID=" + serverID.ToString() + "&serverSettingID=" + serverSettingID.ToString());
        }
    }
    protected void DataSourceAllSettings_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = ServerSetting.Instance;
    }
    protected void ButtonServerSettings_Click(object sender, EventArgs e)
    {
        Response.Redirect(ResolveUrl("~/LocalService/Settings.aspx?serverID=" + Request.QueryString["serverID"]));
    }
}
