<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Settings.aspx.cs" Inherits="Settings" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:GridView ID="GridServerSettings" runat="server" CellPadding="3" DataKeyNames="ServerSettingID" Caption="<b><font color='blue'>Your computer settings</font><b><br /><br />"
        GridLines="none" DataSourceID="DataSourceServerSettings" AutoGenerateColumns="false" OnRowDataBound="GridServerSettings_RowDataBound">
        <EmptyDataTemplate>
            <b>No settings has been made to your server.</b>
        </EmptyDataTemplate>
        <EditRowStyle BackColor="maroon" ForeColor="white" />
        <AlternatingRowStyle BackColor="silver" />
        <Columns>
            <asp:BoundField HeaderText="Setting Name" ReadOnly="true" ItemStyle-Width="330px" ItemStyle-HorizontalAlign="left" DataField="SettingConstant" ItemStyle-BorderColor="gray"
                ItemStyle-BorderStyle="solid" ItemStyle-BorderWidth="1px" />
            <asp:BoundField HeaderText="Server Setting Value" ItemStyle-Width="300px" ControlStyle-Width="290px" ItemStyle-HorizontalAlign="left" DataField="ServerSettingValue" ItemStyle-BorderColor="gray"
                ItemStyle-BorderStyle="solid" ItemStyle-BorderWidth="1px" />
            
            <asp:BoundField HeaderText="Global Setting Value" ReadOnly="true" ItemStyle-Width="300px" ItemStyle-HorizontalAlign="left" DataField="GlobalDefaultValue" ItemStyle-BorderColor="gray"
                ItemStyle-BorderStyle="solid" ItemStyle-BorderWidth="1px" />
            <asp:CommandField ItemStyle-HorizontalAlign="left" ItemStyle-BorderColor="gray" ButtonType="link" DeleteText="Delete" ShowDeleteButton="true" ShowEditButton="true" UpdateText="Save" EditText="Edit"
                ItemStyle-BorderStyle="solid" ItemStyle-BorderWidth="1px" />
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="DataSourceServerSettings" runat="server" SelectMethod="GetServerSettings" DeleteMethod="DeleteServerSetting" UpdateMethod="UpdateServerSetting"
        TypeName="ServerSetting" OnObjectCreating="DataSourceServerSettings_ObjectCreating">
        <SelectParameters>
            <asp:QueryStringParameter Name="serverID" QueryStringField="serverID" Type="Int32" />
        </SelectParameters>
        <DeleteParameters>
            <asp:ControlParameter ControlID="GridServerSettings" Type="int32" Name="serverSettingID" PropertyName="SelectedValue" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:ControlParameter ControlID="GridServerSettings" Type="int32" PropertyName="SelectedValue" Name="serverSettingID" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <br />
    <asp:LinkButton ID="ButtonAddSetting" runat="server" OnClick="ButtonAddSetting_Click">Add existing setting</asp:LinkButton>
    &nbsp;|&nbsp;
    <asp:LinkButton ID="ButtonCreateSetting" Enabled="false" runat="server" OnClick="ButtonCreateSetting_Click">Create new setting</asp:LinkButton>
    <br /><br />
</asp:Content>
