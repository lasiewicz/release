use mnSubscription 
go


    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2080',N'<div class="cell-plan classic strongest" dir="rtl" lang="he" xml:lang="he"> <h2>{0} ₪ <span class="per-month">לחודש</span></h2> <p class="fine-print">מתחדש בתום התקופה ב-{2} ₪ לחודש. </p> </div>',N'CU Classic plan (1 month) - strongest','1019','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2081',N'<div class="cell-plan classic" dir="rtl" lang="he" xml:lang="he"> <h2>{0} ₪ <span class="per-month">לחודש</span></h2> <p class="fine-print">מתחדש בתום התקופה ב-{2} ₪ לחודש. </p> </div>',N'CU Classic plan (1 month)','1019','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2082',N'<div class="cell-plan classic shade" dir="rtl" lang="he" xml:lang="he"> <h2>{0} ₪ <span class="per-month">לחודש</span></h2> <p class="fine-print">מתחדש בתום התקופה ב-{2} ₪ לחודש. </p> </div>',N'CU Classic plan (1 mont) - shade','1019','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2083',N'<div class="cell-row-header classic shade"> <h2>חודשיים</h2> </div>',N'CU Classic row header (2 months) - shaded','1019','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2084',N'<div class="cell-row-header classic"> <h2>חודשיים</h2> </div>',N'CU Classic row header (2 months)','1019','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2085',N'<div class="cell-row-header classic"> <h2>חודש</h2> </div>',N'CU Classic row header (1 month)','1019','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2086',N'<div class="cell-row-header classic shade"> <h2>חודש</h2> </div>',N'CU Classic row header - shaded (1 month)','1019','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2087',N'<div class="cell-row-header classic shade"> <h2>{0} חודשים</h2> </div>',N'CU Classic row header - shaded','1019','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2088',N'<div class="cell-row-header classic"> <h2>{0} חודשים</h2> </div>',N'CU Classic row header','1019','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2089',N'<div class="cell-col-header classic premium"> <!--<h2>Premium Plans</h2> <p>Includes</p> <ul class="rowHeaderInfoOutside"> <li>Member Spotlight</li> <li>Highlighted Profile</li> </ul> <div class="rowHeaderHPDetails" onmouseover="TogglePopupDiv(''listOfBenefits_details''); return false;" onmouseout="TogglePopupDiv(''listOfBenefits_details''); return false;"> <img src="/img/Community/JDate/sub_memberSpotlight_mediumThumb.gif"> <a href="#" onclick="return false">Details</a> </div> </div> <div id="listOfBenefits_details" class="helpLayerContainerB"> <div class="helpLayerMiddle"> <div class="helpLayerInner lightest borderDark"> <h2>List of benefits</h2> <img align="absmiddle" src="/img/Community/JDate/sub_listOfBenefitsChart.gif" border="0" /> </div> </div> --></div>',N'CU Classic column header - premium','1019','0','2',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2090',N'<div class="cell-col-header classic">  <h2>תכניות זמינות</h2> </div>  <div id="listOfBenefitsD" class="helpLayerContainerB" style="top:0;">  <div class="helpLayerMiddle">  <div class="helpLayerInner lightest borderDark">  

<img align="absmiddle" src="/img/Site/Cupid-co-il/sub_tablet_d4.gif" border="0" />  </div>  </div> </div>',N'CU Classic column header - standard','1019','0','2',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2091',N'<div class="cell-plan classic" dir="rtl" lang="he" xml:lang="he"> <h2>{0} ₪ <span class="per-month">לחודש</span></h2> <p class="fine-print">בתשלום אחד של {1} ₪<br /> מתחדש בתום התקופה ב-{2} ₪ לחודש. </p> </div>',N'CU Classic plan (Reg)','1019','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2092',N'<div class="cell-plan classic shade" dir="rtl" lang="he" xml:lang="he"> <h2>{0} ₪ <span class="per-month">לחודש</span></h2> <p class="fine-print">בתשלום אחד של {1} ₪<br /> מתחדש בתום התקופה ב-{2} ₪ לחודש. </p> </div>',N'[CU Classic plan (Reg) - shade','1019','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2093',N'<div class="cell-plan classic strong" dir="rtl" lang="he" xml:lang="he"> <h2>{0} ₪ <span class="per-month">לחודש</span></h2> <p class="fine-print">בתשלום אחד של {1} ₪<br /> מתחדש בתום התקופה ב-{2} ₪ לחודש. </p> </div>',N'CU Classic plan (Reg) - strong','1019','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2094',N'<div class="cell-plan classic strongest" dir="rtl" lang="he" xml:lang="he"> <h2>{0} ₪ <span class="per-month">לחודש</span></h2> <p class="fine-print">בתשלום אחד של {1} ₪<br /> מתחדש בתום התקופה ב-{2} ₪ לחודש. </p> </div> ',N'CU Classic plan (Reg) - strongest','1019','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2095',N'',N'[x] unused','1019','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2096',N'<div class="cell-plan classic shade" dir="rtl" lang="he" xml:lang="he">
	<h2>{0} ₪ <span class="per-month">לחודש</span></h2>
	<p class="fine-print">(ב-{3} תשלומים שווים)<br/>
	מתחדש בתום התקופה ב-{2} ₪ לחודש. </p>
</div>',N'CU Classic plan (Ins) - shade','1019','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2097',N'<div class="cell-plan classic" dir="rtl" lang="he" xml:lang="he">
	<h2>{0} ₪ <span class="per-month">לחודש</span></h2>
	<p class="fine-print">(ב-{3} תשלומים שווים)<br/>
	מתחדש בתום התקופה ב-{2} ₪ לחודש. </p>
</div>',N'CU Classic plan (Ins)','1019','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2098',N'<div class="cell-plan classic strongest" dir="rtl" lang="he" xml:lang="he">
	<h2>{0} ₪ <span class="per-month">לחודש</span></h2>
	<p class="fine-print">(ב-{3} תשלומים שווים)<br/>
	מתחדש בתום התקופה ב-{2} ₪ לחודש. </p>
</div>',N'CU Classic plan (Ins) - strongest','1019','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2099',N'<div class="cell-plan classic strong" dir="rtl" lang="he" xml:lang="he">
	<h2>{0} ₪ <span class="per-month">לחודש</span></h2>
	<p class="fine-print">(ב-{3} תשלומים שווים)<br/>
	מתחדש בתום התקופה ב-{2} ₪ לחודש. </p>
</div>',N'CU Classic plan (Ins) - strong','1019','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2100',N'<div class="cell-plan classic strong" dir="rtl" lang="he" xml:lang="he"> <h2>{0} ₪ <span class="per-month">לחודש</span></h2> <p class="fine-print">מתחדש בתום התקופה ב-{2} ₪ לחודש. </p> </div>',N'CU Classic plan (1 month) - strong','1019','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2101',N'<div class="cell-plan classic strongest" dir="rtl" lang="he" xml:lang="he">
<img alt="" src="/img/Site/Cupid-co-il/sub-badge-less-then-50.png" style="margin-left:30px; float:left;"/>	
	<h2>{0} ₪ <span class="per-month">לחודש</span></h2>
	<p class="fine-print">(ב-{3} תשלומים שווים)<br/>
	מתחדש בתום התקופה <br /> ב-{2} ₪ לחודש. </p>
</div>',N'CU Classic plan (Ins less than 50) - strongest','1019','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2102',N'<div class="cell-row-header classic"> <h2>שבוע</h2> </div>',N'CU Classic row header (week)','1019','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2103',N'<div class="cell-plan classic shade" dir="rtl" lang="he" xml:lang="he">
	<h2>{0} ₪ <span class="per-month">לחודש</span><span style="color:Red;font-size:0.66em;"> +שלושה חודשים במתנה!</span></h2>
	<p class="fine-print">(ב-{3} תשלומים שווים)<br/>
	מתחדש בתום התקופה ב-{2} ₪ לחודש. </p>
</div>',N'CU Classic plan (Ins) - shade - 3m - free','1019','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2104',N'<div class="cell-plan classic strongest" dir="rtl" lang="he" xml:lang="he">
	<h2>{0} ₪ <span class="per-month">לחודש</span><span style="color:Red; font-size:0.66em;"> במקום <img alt="" src="/img/Site/Cupid-co-il/sub-price-strike-59.gif" style="vertical-align:middle;"/></span></h2>
	<p class="fine-print">(ב-{3} תשלומים שווים)<br/>
	מתחדש בתום התקופה ב-{2} ₪ לחודש. </p>
</div>',N'CU Classic plan (Ins) - strongest - 6m - strike','1019','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2105',N'<div class="cell-plan classic strong" dir="rtl" lang="he" xml:lang="he">
	<h2>{0} ₪ <span class="per-month">לחודש</span><span style="color:Red; font-size:0.66em;"> במקום <img alt="" src="/img/Site/Cupid-co-il/sub-price-strike-84.gif" style="vertical-align:middle;"/></span></h2>
	<p class="fine-print">(ב-{3} תשלומים שווים)<br/>
	מתחדש בתום התקופה ב-{2} ₪ לחודש. </p>
</div>',N'CU Classic plan (Ins) - strong - 3m - strike','1019','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2106',N'<div class="cell-plan classic" dir="rtl" lang="he" xml:lang="he"> 
	<h2>{0} ₪ <span class="per-month">לחודש</span><span style="color:Red; font-size:0.66em;"> במקום <img alt="" src="/img/Site/Cupid-co-il/sub-price-strike-109.gif" style="vertical-align:middle;"/></span></h2>
	<p class="fine-print">מתחדש בתום התקופה ב-{2} ₪ לחודש. </p>
</div>
',N'CU Classic plan (1 month) - strike','1019','0','3',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2107',N'<div class="cell-row-header classic shade"> <h2>{0} חודשים</h2> <img alt="" src="/img/Site/Cupid-co-il/sub-badge-most-worthwhile.png" style="vertical-align:middle;"/> </div>',N'CU Classic row header - shaded - most-worthwhile','1019','0','1',getdate(),getdate())
    

    insert into [mnsubscription].[dbo].[resourcetemplate]
([resourcetemplateid],[content],[description],[groupid],[groupdefault],[resourcetemplatetypeid],[insertdatetime],[updatedatetime])
 values
('2108',N'<div class="cell-row-header classic shade"> <h2>{0} חודשים</h2> <img alt="" src="/img/Site/Cupid-co-il/btn-3plus3.png" style="margin-top: 10px;"/> </div>',N'CU Classic row header - shaded - 3 + 3','1019','0','1',getdate(),getdate())
    