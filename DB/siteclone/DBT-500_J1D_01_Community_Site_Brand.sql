
     begin tran
    --community group 
insert into [mnsystem].[dbo].[group]([groupid],[groupname],[scopeid],[updatedate])
  values
  ('50','JustDate','100','8/4/2009 10:28:01 AM')
  

     if @@error <> 0
      begin
        goto PROBLEM
      end
    
--community record
 
    insert into [mnsystem].[dbo].[community]
([communityid],[communityname],[insertdate],[updatedate])
values
('50','JustDate','8/4/2009 10:28:01 AM','8/4/2009 10:28:01 AM')
     
     if @@error <> 0
      begin
        goto PROBLEM
      end
    
--site group 
insert into [mnsystem].[dbo].[group]([groupid],[groupname],[scopeid],[updatedate])
  values
  ('501','Just1Date.com','1000','8/4/2009 10:28:01 AM')
  

     if @@error <> 0
      begin
        goto PROBLEM
      end
    
--brand group

insert into [mnsystem].[dbo].[group]([groupid],[groupname],[scopeid],[updatedate])
  values
  ('5001','Just1Date.com','10000','8/4/2009 10:28:01 AM')
  
     if @@error <> 0
      begin
        goto PROBLEM
      end
    
--site record 
insert into [mnsystem].[dbo].[site]
 ([siteid],[communityid],[sitename],[currencyid],[languageid],[culturename],[defaultregionid],[searchtypemask],[defaultsearchtypeid],
 [csspath],[charset],[direction],[gmtoffset],[defaulthost] ,[sslurl],[paymenttypemask],[insertdate],[updatedate])
  values
  ('501', '50', 'Just1Date.com', '1', '2', 'en-US', '223', '7', '1', '/lib/css/as.css', '1252', 'ltr', '-8', 'stg' ,'https://stg.just1date.com', '3',  '8/4/2009 10:28:01 AM', '8/4/2009 10:28:01 AM')
  
     if @@error <> 0
      begin
        goto PROBLEM
      end
    
--brand record 
insert into [mnsystem].[dbo].[brand]
([brandid],[siteid],[uri],[statusmask],[phonenumber],[defaultagemin],[defaultagemax],[defaultsearchradius],[insertdate],[updatedate])
values
 ('5001', '501', 'Just1Date.com', '4617', '(888) 854-3803', '25', '40', '40', '8/4/2009 10:28:01 AM', '8/4/2009 10:28:01 AM')
  
     if @@error <> 0
      begin
        goto PROBLEM
      end
    
     commit tran
     goto FINITA
     
   PROBLEM:
     rollback tran
     
   FINITA:
    