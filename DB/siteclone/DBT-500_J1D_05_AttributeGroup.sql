
--attribute  'SiteMatchNewsletterPeriod'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1753','556','501','3','1','0'
,'False','3',getdate(),null)
   
--attribute  'ForcedPageTimedPromotionVersion'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1754','555','501','3','1','0'
,'False','0',getdate(),null)
   
--attribute  'SubscriptionExpirationDate'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1755','501','501','1','1','0'
,'False',null,getdate(),null)
   
--attribute  'SubscriptionLastInitialPurchaseDate'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1756','547','501','1','1','0'
,'False',null,getdate(),null)
   
--attribute  'LastBrandID'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1757','452','501','3','1','0'
,'False','1001',getdate(),null)
   
--attribute  'SpotlightExpirationDate'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1758','545','501','1','1','0'
,'False',null,getdate(),null)
   
--attribute  'SubscriptionStatus'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1759','544','501','3','1','0'
,'False','1',getdate(),null)
   
--attribute  'HighlightedFlag'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1760','542','501','0','1','0'
,'False','0',getdate(),null)
   
--attribute  'HasNewNotificationMask'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1761','530','501','2','1','0'
,'False',null,getdate(),null)
   
--attribute  'ToolbarFlag'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1762','529','501','0','1','0'
,'False','0',getdate(),null)
   
--attribute  'SMSAlertPreference'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1763','528','501','2','1','0'
,'False','31',getdate(),null)
   
--attribute  'SMSPhoneIsValidated'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1764','527','501','0','1','0'
,'False','0',getdate(),null)
   
--attribute  'SMSConfirmationCode'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1765','526','501','4','1','10'
,'False',null,getdate(),null)
   
--attribute  'SMSCarrier'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1766','525','501','3','1','0'
,'False',null,getdate(),null)
   
--attribute  'SMSPhone'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1767','524','501','4','1','10'
,'False',null,getdate(),null)
   
--attribute  'HighlightedExpirationDate'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1768','523','501','1','1','0'
,'False',null,getdate(),null)
   
--attribute  'PremiumAuthenticatedExpirationDate'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1769','522','501','1','1','0'
,'False',null,getdate(),null)
   
--attribute  'SiteLastName'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1770','521','501','4','1','100'
,'False',null,getdate(),null)
   
--attribute  'SiteFirstName'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1771','520','501','4','1','100'
,'False',null,getdate(),null)
   
--attribute  'BrandLandingPageID'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1772','516','5001','3','3','0'
,'False',null,getdate(),null)
   
--attribute  'BrandPromotionID'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1773','515','5001','3','3','0'
,'False',null,getdate(),null)
   
--attribute  'BrandLogonCount'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1774','496','5001','3','1','0'
,'False','0',getdate(),null)
   
--attribute  'BrandLastLogonDate'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1775','495','5001','1','1','0'
,'False',null,getdate(),null)
   
--attribute  'BrandInsertDate'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1776','497','5001','1','3','0'
,'False',null,getdate(),null)
   
--attribute  'BrandLuggage'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1777','518','5001','4','3','150'
,'False',null,getdate(),null)
   
--attribute  'BrandBannerID'
 insert into [mnsystem].[dbo].[attributegroup]
([attributegroupid],[attributeid],[groupid],[attributetypeid],[attributestatusmask],[length]
,[encryptflag],[defaultvalue],[updatedate],[attributegroupidoldvaluecontainer])
values
  ('1778','517','5001','3','3','0'
,'False',null,getdate(),null)
   