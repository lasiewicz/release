use mnsystem

go

insert into Setting
values
(1521230,2,'SITE_DATE_FORMAT','US','Display date in US or EU format',0,0,GETDATE())

insert into SiteSetting
values
(56080,103,1521230,'US')

insert into SiteSetting
values
(56081,101,1521230,'US')


insert into SiteSetting
values
(56082,111,1521230,'US')





insert into Setting
values
(1521262,2,'COMPOSE_SAVE_DRAFT_BFR_SUB','false','Allow non paying member save msg as draft',0,0,GETDATE())

insert into SiteSetting
values
(56083,101,1521262,'true')



insert into Setting
values
(1521235,2,'SITE_NAME_ALIAS','','Site Name Alias for resx TOKEN',0,0,GETDATE())

insert into SiteSetting
values
(56127,103,1521235,'JDate')

insert into SiteSetting
values
(56223,101,1521235,'AmericanSingles')

insert into SiteSetting
values
(56227,105,1521235,'JDate')

insert into SiteSetting
values
(56228,107,1521235,'JDate')

insert into SiteSetting
values
(56226,4,1521235,'JDate')

insert into Setting
values
(1521236,2,'USER_PLANE_LOCALE_FILE','english-spark','File name of locale file for Userplane',0,0,GETDATE())

insert into SiteSetting
values
(56128,103,1521236,'english-jdate')

insert into SiteSetting
values
(56129,105,1521236,'french-jdate')

insert into SiteSetting
values
(56130,107,1521236,'english-jdatecouk')

insert into SiteSetting
values
(56131,4,1521236,'hebrew-jdate')

insert into SiteSetting
values
(56132,15,1521236,'hebrew-cupid')

insert into SiteSetting
values
(56133,9171,1521236,'english-jewishmingle')

insert into Setting
values
(1521237,2,'USER_PLANE_ALLOW_PHONE_CALLS','false','Call to us from Userplane is disabled by default',0,0,GETDATE())

insert into SiteSetting
values
(56134,103,1521237,'true')


insert into SiteSetting
values
(56135,15,1521237,'true')



insert into Setting
values
(1521240,2,'SITE_ACTIVE_FLAG','true','Setting used to populate some ListBoxes',0,0,GETDATE())

insert into SiteSetting
values
(56140,6,1521240,'false')

insert into SiteSetting
values
(56141,100,1521240,'false')

insert into SiteSetting
values
(56142,102,1521240,'false')

insert into SiteSetting
values
(56143,108,1521240,'false')



insert into Setting
values
(1521241,2,'MAILBOX_ACCESS_MASK','0','0-sub member,1-same country region rule(IL),2-sitelanguage rule(FR)',0,0,GETDATE())

insert into SiteSetting
values
(56144,4,1521241,'1')

insert into SiteSetting
values
(56145,15,1521241,'1')



insert into SiteSetting
values
(56146,105,1521241,'2')

insert into Setting
values
(1521242,2,'VIEWMSG_ACCESS_MASK','0','0-sub member,1-same country region rule(IL),2-sitelanguage rule(FR)',0,0,GETDATE())
insert into SiteSetting
values
(56147,4,1521242,'1')

insert into SiteSetting
values
(56148,15,1521242,'1')



insert into SiteSetting
values
(56149,105,1521242,'2')

insert into Setting
values
(1521243,2,'COMPOSEMSG_ACCESS_MASK','0','0-sub member,1-same country region rule(IL),2-sitelanguage rule(FR)',0,0,GETDATE())

insert into SiteSetting
values
(56150,15,1521243,'1')



insert into Setting
values
(1521253,2,'AMADESA_ACCESS_MASK','0','1-home,2-everify,4-reg, see webconstants for rest',0,0,GETDATE())

insert into SiteSetting
values
(56151,4,1521253,'61')

insert into SiteSetting
values
(56152,103,1521253,'831')

insert into SiteSetting
values
(56153,101,1521253,'63')

insert into SiteSetting
values
(56154,15,1521253,'830')



insert into SiteSetting
values
(56155,105,1521253,'12')

insert into SiteSetting
values
(56156,107,1521253,'12')



insert into Setting
values
(1521254,2,'MOL_DISPLAY_FLAG','true','Display MOL link in activity center',0,0,GETDATE())

insert into SiteSetting
values
(56157,15,1521254,'false')

insert into Setting
values
(1521263,2,'CSS_COMMON','/css/sparkCommonNonHe.css','Common style sheet',0,0,GETDATE())

insert into SiteSetting
values
(56159,15,1521263,'/css/sparkCommonCU.css')

insert into SiteSetting
values
(56160,4,1521263,'/css/sparkCommonHe.css')

insert into Setting
values
(1521256,2,'TELECLAL_ENABLED','false','Use Teleclal as SMSProvider',0,0,GETDATE())

insert into SiteSetting
values
(56161,4,1521256,'true')


insert into Setting
values
(1521261,2,'SESSION_COMMUNITY_LIST','1,2,3,8,9,10,12,17,50','List of communities for perf. MOL counters',0,0,GETDATE())