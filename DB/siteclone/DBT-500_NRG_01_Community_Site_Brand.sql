
     begin tran
    
--site group 
insert into [mnsystem].[dbo].[group]([groupid],[groupname],[scopeid],[updatedate])
  values
  ('19','NRGDating.co.il','1000','7/30/2009 1:11:40 PM')
  

     if @@error <> 0
      begin
        goto PROBLEM
      end
    
--brand group

insert into [mnsystem].[dbo].[group]([groupid],[groupname],[scopeid],[updatedate])
  values
  ('1019','NRGDating.co.il','10000','7/30/2009 1:11:40 PM')
  
     if @@error <> 0
      begin
        goto PROBLEM
      end
    
--site record 
insert into [mnsystem].[dbo].[site]
 ([siteid],[communityid],[sitename],[currencyid],[languageid],[culturename],[defaultregionid],[searchtypemask],[defaultsearchtypeid],
 [csspath],[charset],[direction],[gmtoffset],[defaulthost] ,[sslurl],[paymenttypemask],[insertdate],[updatedate])
  values
  ('19', '10', 'NRGDating.co.il', '6', '262144', 'he-IL', '105', '7', '4', '/lib/css/ci.css', '1255', 'rtl', '2', 'stg' ,'https://www.NRGDating.co.il', '1',  '7/30/2009 1:11:40 PM', '7/30/2009 1:11:40 PM')
  
     if @@error <> 0
      begin
        goto PROBLEM
      end
    
--brand record 
insert into [mnsystem].[dbo].[brand]
([brandid],[siteid],[uri],[statusmask],[phonenumber],[defaultagemin],[defaultagemax],[defaultsearchradius],[insertdate],[updatedate])
values
 ('1019', '19', 'NRGDating.co.il', '1545', '09-9701721', '23', '38', '40', '7/30/2009 1:11:40 PM', '7/30/2009 1:11:40 PM')
  
     if @@error <> 0
      begin
        goto PROBLEM
      end
    
     commit tran
     goto FINITA
     
   PROBLEM:
     rollback tran
     
   FINITA:
    