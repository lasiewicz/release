
     begin tran
    

    --site rollback
use mnphotostore
go
delete from [photostoresearchoption]
where siteid='501'

use mnsubscription
go
delete from resourcetemplate
where groupid='5001'

use mnsystem
go
delete from objectgroup
where groupid='501'

delete from sitepage
where siteid='501'

delete from sitesetting
where siteid='501'

delete from attributegroup
where groupid='501'

delete from attributegroup
where groupid='5001'

delete from attributeoptiongroup
where groupid='501'

delete from attributeoptiongroup
where groupid='5001'

delete from listcategorysite
where siteid='501'

delete from profilesectiondefinitionattribute
where siteid='501'

delete from [creditcardgroup]
where groupid='501'

delete from profilesectiondefinitionattribute
where brandid='5001'

delete from profilesectiondefinitionsite
where siteid='501'

delete from brand
where brandid='5001'

delete from [site]
where siteid='501'

delete from [group]
where groupid='5001'

delete from [group]
where groupid='501'
    

    --community rollback
delete from attributegroup
where groupid='50'

delete from attributeoptiongroup
where groupid='50'

delete from registrationpromotion
where domainid='50'

delete from profilesectiondefinitionattribute
where communityid='50'

delete from community
where communityid='50'

delete from [group]
where groupid='50'
    
     commit tran
     goto FINITA
     
   PROBLEM:
     rollback tran
     
   FINITA:
    