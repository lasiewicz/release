INSERT INTO [mnsystem].[dbo].[Setting] ([SettingID],[SettingCategoryID],[SettingConstant],[GlobalDefaultValue],[SettingDescription],[IsRequiredForCommunity],[IsRequiredForSite],[CreateDate])
     VALUES
           (1521257
           ,2
           ,'MATCH_METER_LANGUAGE_CONST'
           ,'HEB'
           ,'The language const parameter for Zoozamen Jmeter'
           ,0
           ,0
           ,GETDATE())
INSERT INTO [mnsystem].[dbo].[SiteSetting]
           ([SiteSettingID]
           ,[SiteID]
           ,[SettingID]
           ,[SiteSettingValue])
     VALUES
           (56162
           ,4
           ,1521257
           ,'HEB')
		   
INSERT INTO [mnsystem].[dbo].[Setting] ([SettingID],[SettingCategoryID],[SettingConstant],[GlobalDefaultValue],[SettingDescription],[IsRequiredForCommunity],[IsRequiredForSite],[CreateDate])
     VALUES
           (1521258
           ,2
           ,'MATCH_METER_PARTNER_ID_CONST'
           ,'SPARK.JDIL.V2'
           ,'The partner ID const parameter for Zoozamen Jmeter'
           ,0
           ,0
           ,GETDATE())
INSERT INTO [mnsystem].[dbo].[SiteSetting]
           ([SiteSettingID]
           ,[SiteID]
           ,[SettingID]
           ,[SiteSettingValue])
     VALUES
           (56163
           ,4
           ,1521258
           ,'SPARK.JDIL.V2')
		   





