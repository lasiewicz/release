var spark = spark || {};
spark.utilities = spark.utilities || {

    getRootDomain : function() {
        var domainParts = document.domain.split('.');
        var last = domainParts.length -1;
        var rootDomain;
        if (domainParts[last -1] === "co") { // grab 3 parts: jdate co il, jdate co uk
            rootDomain = domainParts[last -2] + '.' + domainParts[last -1] + '.' + domainParts[last];
        }
        else { // grab 2 parts, jdate com, jdate fr, blacksingles com
            rootDomain = domainParts[last -1] + '.' + domainParts[last];
        }
        return rootDomain;
    },
    getFormattedTime: function () {
        var date = new Date();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var minStr = '' + minutes;
        while (minStr.length < 2) {
            minStr = '0' + minStr;
        }
        var amPm = 'am';
        if (hours > 11) {
            amPm = 'pm';
            if (hours > 12) {
                hours -= 12;
            }
        }
        return hours + ':' + minStr + amPm;
    },
	
    getFormattedDateTime: function () {
        var date = new Date();
        var minutes = date.getMinutes();
        var minStr = '' + minutes;
        while (minStr.length < 2) {
            minStr = '0' + minStr;
        }
		var monthStr=date.getMonth()+1;
	    var str = date.getFullYear() + "-" + monthStr + "-" + date.getDate() + "  " +  date.getHours() + ":" + minStr + ":" + date.getSeconds();

	    return str;
	},

    get_special_jid: function(Jid, Token){
        parseInt(this.getQueryVariable(window.location.href, "memberId"), 10);
    },

    log : function(message) {
        if (typeof(console) !== "undefined") {
            console.log(message);
        }
        if ($('#debug')[0]) {
            $('#debug').append(message + '<br/>\n');
        }
	},
	logInfoToApi : function(message) {	
		if(spark.config.logToAPI){
			var ajaxRequest =
	        {
	            url: spark.config.restServerV2 + "/loginfo?client=IMWebClient&message="+ message +"&timestamp=" + this.getFormattedDateTime(),
	            type: "PUT",
				async: true,
	            dataType : 'json',
				cache:false
	        };

	        if (webIM.IsAndroidGingerbread) {
	            ajaxRequest.beforeSend = function (xhr) {
	                // Hack for Android 2.3.X devices (known Google issue).
	                xhr.setRequestHeader("Content-Length", "");
	            };
	        }
	        $.ajax(ajaxRequest);		
		}
    },
	logErrorToApi : function(message) {	
		if(spark.config.logToAPI){
			var ajaxRequest =
	        {
	            url: spark.config.restServerV2 + "/logerror?client=IMWebClient&message="+ message +"&timestamp=" + this.getFormattedDateTime(),
	            type: "PUT",
				async: true,
	            dataType : 'json',
				cache:false
	        };

	        if (webIM.IsAndroidGingerbread) {
	            ajaxRequest.beforeSend = function (xhr) {
	                // Hack for Android 2.3.X devices (known Google issue).
	                xhr.setRequestHeader("Content-Length", "");
	            };
	        }
	        $.ajax(ajaxRequest);		
		}
    }
};
