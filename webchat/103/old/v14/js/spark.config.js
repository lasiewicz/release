var spark = spark || {};
spark.config = spark.config || {
    fwsWebServerSegment : "",
    environmentSubdomain : "www",
    restServer : "https://api.spark.net",
    chatServerName : "chat.spark-networks.com",
    boshServer : "http://chat.spark-networks.com/http-bind"
};
/* must be included after spark.config has been created */
(function(){

    var brandDataMap =
    {
        "jdate.com" : {
            "siteName" : "JDate",
            "brandId" : 1003,
            "communityId" : 3,
            "omnitureKeyword" : "sparkjdatecom",
            "locale" : spark.localization.enUS
        },

        "jdate.co.uk" : {
            "siteName" : "JDate",
            "brandId" : 1003,
            "communityId" : 3,
            "omnitureKeyword" : "sparkjdatecouk",
            "locale" : spark.localization.enGB
        },

        "jdate.co.il" : {
            "siteName" : "JDate",
            "brandId" : 1004,
            "communityId" : 3,
            "omnitureKeyword" : "sparkjdatecoil",
            "locale" : spark.localization.heIL
        },

        "jdate.fr" : {
            "siteName" : "JDate",
            "brandId" : 1003,
            "communityId" : 3,
            "omnitureKeyword" : "sparkjdatefr",
            "locale" : spark.localization.frFR
        },

        "cupid.co.il" : {
            "siteName" : "Cupid",
            "brandId" : 1015,
            "communityId" : 10,
            "omnitureKeyword" : "sparkcupidcoil",
            "locale" : spark.localization.heIL
        },

        "spark.com" : {
            "siteName" : "Spark",
            "brandId" : 1001,
            "communityId" : 1,
            "omnitureKeyword" : "sparkamericansinglescom",
            "locale" : spark.localization.enUS
        },

        "bbwpersonalsplus.com" : {
            "siteName" : "BBW Personals Plus",
            "brandId" : 90410,
            "communityId" : 23,
            "omnitureKeyword" : "sparkbbw",
            "locale" : spark.localization.enUS
        },

        "blacksingles.com" : {
            "siteName" : "Black Singles",
            "brandId" : 90510,
            "communityId" : 24,
            "omnitureKeyword" : "sparkblack",
            "locale" : spark.localization.enUS
        }
    };

    var rootDomain = spark.utilities.getRootDomain();
    spark.config.brandData = brandDataMap[rootDomain];
    if (!spark.config.brandData) {
        throw 'missing brand data for domain: ' + rootDomain;
    }
    spark.config.fwsUrl = '//' + spark.config.fwsWebServerSegment + spark.config.environmentSubdomain + '.' +  rootDomain;
    spark.config.IMInitiatorUrl = spark.config.fwsUrl +  '/applications/instantmessenger/icxml.aspx';
    spark.locale = spark.config.brandData.locale;

})();