var spark = spark || {};
spark.messageconversion = spark.messageconversion || {
	messageAttachmentURLPath : spark.config.messageAttachmentURLPath,
	ownMessageBackgroundClass : '',
	ownMessageFontTypeClass : '',
	ownMessageFontColorClass : '',
	ownMessageFontSizeClass: '',
	ownMessageFontBoldClass: '',
	ownMessageFontUnderlineClass: '',
	ownMessageFontItalicizeClass: '',
	chatterMessageBackgroundClass : '',
	chatterMessageFontTypeClass : '',
	chatterMessageFontColorClass : '',
	chatterMessageFontSizeClass : '',
	chatterMessageFontBoldClass: '',
	chatterMessageFontUnderlineClass: '',
	chatterMessageFontItalicizeClass: '',
	messageMiscChanged : false,
	
	/**Templates**/
	//photo
	sparkTagPhoto : '<sparktag type="photo" alt="{{alt}}" src="{{src}}" id="{{id}}" data-originalExt="{{originalExt}}"/>',
	htmlPhoto : '<a href="{{href}}" target="_blank"><img alt="{{alt}}" src="{{src}}" id="{{id}}"/></a>',
	htmlPhotoPreview : '<img alt="{{alt}}" src="{{src}}" id="{{id}}" data-originalExt="{{originalExt}}"/>&nbsp;',
	
	//emoticon
	sparkTagEmoticon : '<sparktag type="emoticon" class="{{styleClass}}"/>',
	htmlEmoticon : '<span class="{{styleClass}}" data-type="emoticon"></span>',
	htmlEmoticonPreview : '<span class="{{styleClass}}" data-type="emoticon"></span>',
	
	//bg, font
	sparkTagMessageMisc : '<sparktag type="messageMisc" bgclass="{{bgStyleClass}}" fonttypeclass="{{fontTypeStyleClass}}" fontcolorclass="{{fontColorStyleClass}}" fontsizeclass="{{fontSizeStyleClass}}" fontboldclass="{{fontBoldStyleClass}}" fontunderlineclass="{{fontUnderlineStyleClass}}" fontitalicizeclass="{{fontItalicizeStyleClass}}"/>',
	
	/**Conversion functions**/
	convertHtmlToCustomTag : function(message){
		//remove paragraphs, nbsp, and divs that we don't care for
		message = spark.messageconversion.removeHtmlParagraphs(message);
		message = spark.messageconversion.removeHtmlNoise(message);
		message = $.trim(message);
		var rebuiltHtml = '';
		var html = $('<div>').append(message).contents();
		$.each(html, function (i, el) {
			switch (el.nodeName.toLowerCase()) {
				case "#text":
					//text
					rebuiltHtml += spark.messageconversion.htmlEncode($(el).text());
					break;
				case "img":
					//photo
					var dtoPhoto = {
						src: String($(el).attr('src')).replace(spark.messageconversion.messageAttachmentURLPath, ''),
						id: $(el).attr('id'),
						alt: $(el).attr('alt'),
						originalExt: $(el).attr('data-originalExt')
					};
					rebuiltHtml += Mustache.to_html(spark.messageconversion.sparkTagPhoto, dtoPhoto);
					break;
				case "span":
					var type = $(el).attr('data-type');
					//emoticon
					if (type == "emoticon"){
						var dtoEmoticon = {
							styleClass: $(el).attr('class')
						};
						rebuiltHtml += Mustache.to_html(spark.messageconversion.sparkTagEmoticon, dtoEmoticon);
					}
					break;
			}
		});
		
		//bg, font
		if (rebuiltHtml != '' && spark.messageconversion.messageMiscChanged){
			//include message misc tag to update recipient side with message metadata
			var dtoMessageMisc = {
				bgStyleClass: spark.messageconversion.ownMessageBackgroundClass,
				fontTypeStyleClass: spark.messageconversion.ownMessageFontTypeClass,
				fontColorStyleClass: spark.messageconversion.ownMessageFontColorClass,
				fontSizeStyleClass: spark.messageconversion.ownMessageFontSizeClass,
				fontBoldStyleClass: spark.messageconversion.ownMessageFontBoldClass,
				fontUnderlineStyleClass: spark.messageconversion.ownMessageFontUnderlineClass,
				fontItalicizeStyleClass: spark.messageconversion.ownMessageFontItalicizeClass
			};
			rebuiltHtml += Mustache.to_html(spark.messageconversion.sparkTagMessageMisc, dtoMessageMisc);
			spark.messageconversion.messageMiscChanged = false;
		}
		
		return $.trim(rebuiltHtml);
	},
	
	convertCustomTagToHtml : function(message, isOwnMessage){
		var rebuiltHtml = '';
		var html = $('<div>').append(message).contents();
		$.each(html, function (i, el) {
			switch (el.nodeName.toLowerCase()) {
				case "#text":
					//text
					rebuiltHtml += spark.messageconversion.htmlEncode($(el).text());
					break;
				case "sparktag":
					var type = $(el).attr('type');
					if (type == "photo"){
						//photo
						var dtoPhoto = {
							src: spark.messageconversion.messageAttachmentURLPath + $(el).attr('src'),
							id: $(el).attr('id'),
							alt: $(el).attr('alt'),
							href: spark.messageconversion.messageAttachmentURLPath + String($(el).attr('src')).replace("preview", "original").replace('.jpg', '.' + $(el).attr('data-originalExt'))
						};
						rebuiltHtml += Mustache.to_html(spark.messageconversion.htmlPhoto, dtoPhoto);
					}
					else if (type == "emoticon"){
						//emoticon
						var dtoEmoticon = {
							styleClass: $(el).attr('class')
						};
						rebuiltHtml += Mustache.to_html(spark.messageconversion.htmlEmoticon, dtoEmoticon);
					}
					else if (type == "messageMisc"){
						//bg, font
						if (!isOwnMessage){
							spark.messageconversion.chatterMessageBackgroundClass = $(el).attr('bgclass');
							spark.messageconversion.chatterMessageFontTypeClass = $(el).attr('fonttypeclass');
							spark.messageconversion.chatterMessageFontColorClass = $(el).attr('fontcolorclass');
							spark.messageconversion.chatterMessageFontSizeClass = $(el).attr('fontsizeclass');
							spark.messageconversion.chatterMessageFontBoldClass = $(el).attr('fontboldclass');
							spark.messageconversion.chatterMessageFontUnderlineClass = $(el).attr('fontunderlineclass');
							spark.messageconversion.chatterMessageFontItalicizeClass = $(el).attr('fontitalicizeclass');
						}
					}
					break;
			}
		});
		
		return $.trim(rebuiltHtml);
	},
	
	/**Encode/Decode which also strips white spaces **/
	htmlEncode: function(value) {
		//create a in-memory div, set it's inner text(which jQuery automatically encodes)
		//then grab the encoded contents back out.  The div never exists on the page.
		return $('<div/>').text(value).html();
	},

    htmlDecode: function(value) {
		return $('<div/>').html(value).text();
	},
	
	/**Encode/Decode without stripping white spaces **/
	htmlEscape: function (str) {
		return String(str)
		.replace(/&/g, '&amp;')
		.replace(/"/g, '&quot;')
		.replace(/'/g, '&#39;')
		.replace(/</g, '&lt;')
		.replace(/>/g, '&gt;');
	},

    htmlUnescape: function (value) {
		return String(value)
		.replace(/&quot;/g, '"')
		.replace(/&#39;/g, "'")
		.replace(/&lt;/g, '<')
		.replace(/&gt;/g, '>')
		.replace(/&amp;/g, '&');
	},
	
	removeHtmlParagraphs: function(value) {
		return String(value)
			.replace(/<p>&nbsp;<\/p>/gi, "")
			.replace(/<p>/gi, "")
			.replace(/<\/p>/gi, "")
			.replace(/<br\/>/gi, "")
			.replace(/<br>/gi, "")
			.replace(/<\/br>/gi, "");
	},
	removeParagraphs: function(value){
		return String(value)
			.replace(/\n/g, "")
			.replace(/\r/g, "");
	},
	removeHtmlNoise: function(value){
		return String(value)
			.replace(/&nbsp;/g, ' ')
			.replace(/<div>/gi, "")
			.replace(/<\/div>/gi, "");
	},
	
};