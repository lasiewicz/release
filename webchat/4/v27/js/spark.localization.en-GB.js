spark.localization.enGB = Object.create(spark.localization.enUS); // inherit from US english.
(function(){
    var translations = {
        "favoriteMember" : "Favourite Member",
        "unfavoriteMember" : "Unfavourite Member",
		"photoUploadFailed": "Photo upload failed, error code: ",
		"photoUploadFailedMsg": ", message: ",
		"photoUploadFailedNull": "Photo upload failed: null response"
    };
    spark.localization.enGB.addTranslations(translations);
})();
