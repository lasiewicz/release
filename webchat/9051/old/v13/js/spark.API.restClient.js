var spark = spark || {};
spark.API = spark.API || {};
spark.API.restClient = spark.API.restClient || {

    errorCodes : {
        "error_ejab_auth_failed" : "ZA000A",
        "error_missing_token_cookie" : "ZA000T",
        "error_missing_member_id_cookie" : "ZA000C",
        "error_token_expired" : "ZA000E",
        "error_api_call_failed" : "ZA000R"
    },

    isCorsSupported : function () {
        return !!(window.XMLHttpRequest && 'withCredentials' in new XMLHttpRequest());
    },

    getLoginDataCookies : function () {
        var dateStr = localStorage["accessExpiresTime"];
        var loginData = {
            "memberId" : parseInt($.cookie("MOS_MEMBER"), 10),
            "accessToken" : $.cookie("MOS_ACCESS"),
            "isPayingMember" : $.cookie("MOS_SUB"),
            "accessExpiresTime" : new Date($.cookie("MOS_ACCESS_EXPIRES")),
            "refreshToken" : $.cookie("MOS_REFRESH")
        };
        return loginData;
    },

    getLoginData : function () {
        return this.getLoginDataCookies();
    },

    saveLoginData : function (loginData) {
        this.saveLoginDataCookies(loginData);
    },

    tokenResponseToLoginData : function (tokenResponse) {
        var loginData = {
            "memberId" : praseInt(tokenResponse["MemberId"], 10),
            "accessToken":tokenResponse["AccessToken"],
            "expiresIn":tokenResponse["ExpiresIn"],
            "accessExpiresTime":this.convertAspNetDate(tokenResponse["AccessExpiresTime"]),
            "refreshToken":tokenResponse["RefreshToken"],
            "isPayingMember":tokenResponse["IsPayingMember"]
        };
        console.log("expires " + loginData.accessExpiresTime);
        return loginData;
    },

    getSearchPrefs : function () {
        var prefJson = localStorage["searchPrefs"];
        if (!prefJson) {
            return null;
        }
        var prefData = JSON.parse(prefJson);
        return prefData;
    },

    saveSearchPrefs : function (searchPrefs) {
        var prefJson = JSON.stringify(searchPrefs);
        localStorage["searchPrefs"] = prefJson;
    },

    loadSearchPrefsFromPage : function() {
        var searchPrefs = {
            "gender" : $('input:radio[name=gender]:checked').val(),
            "seekingGender" : $('input:radio[name=seekingGender]:checked').val(),
            "minAge" : parseInt($('#search-preferences-minage').val()),
            "maxAge" : parseInt($('#search-preferences-maxage').val()),
            "maxDistance" : parseInt($('#search-preferences-location-range').val()),
            "showOnlyMembersWithPhotos" : $('#search-preferences-photo').val() === "true",
            "showOnlyJewishMembers" : $('#search-preferences-jewish').val() === "true"
        };
        return searchPrefs;
    },

    matchPrefsResponseToDTO : function (machPrefsResponse) {
        var searchPrefs = {
            "gender" : machPrefsResponse["gender"],
            "seekingGender" : machPrefsResponse["seekingGender"],
            "minAge" : parseInt(machPrefsResponse["minAge"]),
            "maxAge" : parseInt(machPrefsResponse["maxAge"]),
            "location" : machPrefsResponse["location"],
            "maxDistance": parseInt(machPrefsResponse["maxDistance"]),
            "showOnlyMembersWithPhotos" : machPrefsResponse["showOnlyMembersWithPhotos"] === "true",
            "showOnlyJewishMembers" : true
        };
        return searchPrefs;
    },

    ensureSearchPrefs : function(callback, state) {
        if (typeof(callback) !== "function") {
            return;
        }
        var searchPrefs = getSearchPrefs();
        if (searchPrefs) {
            callback(searchPrefs, state);
        }
        else {
            ensureGoodToken(function () {
                $.ajax({
                    contentType:"application/json",
                    type:'GET',
                    url: self.baseUrl + '/match/preferences/' + loginData.memberId + '?access_token=' + loginData.accessToken,
                    tryCount : 0,
                    retryLimit : 3,
                    dataType:'json',
                    success:function (data) {
                        searchPrefs = matchPrefsResponseToDTO(data);
                        saveSearchPrefs(searchPrefs);
                        searchPrefs = getSearchPrefs();
                        callback(searchPrefs, state);
                    },
                    error: function(data) {
                        spark.API.restClient.showErrors(data);
                    }
                });
            });
        }
    },

    goToLogin : function (errorCode) {
        if (typeof(this.loggedOutCallback) === 'function') {
            this.loggedOutCallback(errorCode);
        }
    },

    setLoggedOutCallback : function (callback) {
        if (typeof(callback) === 'function') {
            this.loggedOutCallback = callback;
        }
    },

    fetchMiniProfile : function (memberId, callback) {
        this.ensureGoodToken(function () {
        if (memberId) {
            var api = spark.API.restClient;
            var ajaxCall = {
                type:'GET',
                url: api.baseUrl + '/profile/attributeset/miniProfile/' + memberId + "/" + api.loginData.memberId + '?access_token=' + api.loginData.accessToken,
                tryCount : 0,
                retryLimit : 3,
                dataType:'json',
                success:function (data) {
                    if (typeof(callback) === 'function') {
                        data.thumbOrNoPic = api.getThumbOrNoPic(data); // adding convenience property
                        callback(true, data);
                    }
                },
                error: function(data) {
                    if (typeof(callback) === 'function') {
                        callback(false, data);
                    }
                }
            };
            $.ajax(ajaxCall);
        }
        });
    },

    fetchHotlistCounts : function (successCallback, failureCallback) {
        spark.API.restClient.ensureGoodToken(function () {
            var api = spark.API.restClient;
            $.ajax({
                type:'GET',
                url: api.baseUrl + '/hotlist/counts/' + api.loginData.memberId + '?access_token=' + api.loginData.accessToken,
                tryCount : 0,
                retryLimit : 3,
                dataType:'json',
                success:function (data) {
                    if (typeof(successCallback) === "function") {
                        successCallback(data);
                    }
                },
                error : function(data) {
                    if (typeof(failureCallback) === "function") {
                        failureCallback();
                    }
                    spark.API.restClient.showErrors(data);
                }
            });
        });
     },

    fetchHotlist : function (hotlist, memberId, callback) {
        spark.API.restClient.ensureGoodToken(function () {
            var api = spark.API.restClient;
            $.ajax({
                type:'GET',
                url: api.baseUrl + '/hotlist/' + hotlist + '/' + memberId + '/pagesize/500/pagenumber/1'+ '?access_token=' + api.loginData.accessToken,
                tryCount : 0,
                retryLimit : 3,
                dataType:'json',
                success:function (data) {
                    if (typeof(callback) === "function") {
                        callback(true, data);
                    }
                },
                error : function(data) {
                    if (typeof(callback) === "function") {
                        callback(false, data);
                    }
                   spark.API.restClient.showErrors(data);
                }
            });
        });
    },

    checkMemberOnHotlist : function (hotlist, listOwnerMemberId, targetMemberId,  callback) {
        spark.API.restClient.ensureGoodToken(function () {
            var api = spark.API.restClient;
            $.ajax({
                type:'GET',
                url: api.baseUrl + '/memberonhotlist/' + hotlist + '/' + listOwnerMemberId + '/' + targetMemberId + '?access_token=' + api.loginData.accessToken,
                tryCount : 0,
                retryLimit : 3,                dataType:'json',
                success:function (data) {
                    if (typeof(callback) === "function") {
                        callback(true, data);
                    }
                },
                error : function(data) {
                    if (typeof(callback) === "function") {
                        callback(false, data);
                    }
                    spark.API.restClient.showErrors(data);
                }
            });
        });
    },

    changeHotlist : function (hotlist, memberId, isAddAction, callback) {
        spark.API.restClient.ensureGoodToken(function () {
            var api = spark.API.restClient;
            var request = {
                url: api.baseUrl + '/hotlist/' + hotlist + '/' + api.loginData.memberId + '/targetMemberId/' + memberId + '?access_token=' + api.loginData.accessToken,
                tryCount : 0,
                retryLimit : 3,
                dataType : 'json',
                success:function (data) {
                    if (typeof(callback) === "function") {
                        callback(data.success, data); // http call succeeded, now check to see if hotlist change worked
                    }
                },
                error : function(data) {
                    if (typeof(callback) === "function") {
                        callback(false, data);
                    }
                    spark.API.restClient.showErrors(data);
                }
            };

            if (isAddAction) { // add to hotlist
                request.type = "post";
                if (!api.isCorsSupported()) {
                    request.data = {"a" : "b"}; // IE hack :( need something in the body or request fails
                }
            }
            else { // remove from hotlist
                if (api.isCorsSupported()) {
                    request.type = "delete";
                }
                else { // IE hack :(  need to tunnel DELETE request inside a POST request
                    request.type = "post";
                    request.data = { "method" : "delete"};
                }
            }
            $.ajax(request);
        });
    },

    showErrors : function (data) {
        var errorInfo = "";

        function showType(dataType) {
            for (var prop in data) {
                if (data.hasOwnProperty(prop) && (typeof(data[prop]) === dataType)) {
                    errorInfo += prop + " :  " + data[prop] + "\n";
                }
            }
        }
        showType("number");
        showType("string");
        logIt(errorInfo);
    },

    completePendingRestCalls : function () {
        var count = this.restCallbackWithState.length;
        for (var i = 0; i < count; i++) {
            var cbAndState = this.restCallbackWithState[i];
            if (cbAndState) {
                if (cbAndState.callback && typeof(cbAndState.callback) === "function") {
                    cbAndState.callback(cbAndState.state);
                }
            }
        }
        this.restCallbackWithState = [];
    },

    getUTCTime : function(theTime) {
        if (!theTime) {
            theTime = new Date();
        }
        var utc = new Date(theTime.getTime() + theTime.getTimezoneOffset() * 60 * 1000);
        return utc;
    },

    ensureGoodToken : function (callback, state) {
        this.loginData = this.getLoginData();
        var now = new Date();
        if (!this.loginData.accessToken && !this.loginData.refreshToken) {
            this.goToLogin('error_missing_token_cookie');
            return;
        }

        var nowUTC = this.getUTCTime();
        var accessExpiresUTC = this.loginData.accessExpiresTime;
        accessExpiresUTC.setHours(accessExpiresUTC.getHours() + 8); // adjust from pacific time
        if (accessExpiresUTC > nowUTC) {
            callback(state); // good token, ok to make call
        }
        else {
            this.goToLogin('error_token_expired');
        }
    },

    convertAspNetDate : function (dateString) {
        if (!dateString) {
            return null;
        }
        return new Date(parseInt(dateString.substr(6)));
    },

    getNoPic : function(gender) {
        return (gender && gender === "Female") ? '//www.blacksingles.com/img/no-photo-s-f.png' : '//www.blacksingles.com/img/no-photo-s-m.png';
    },

    getThumbOrNoPic : function (member) {
        if (member.primaryPhoto && member.primaryPhoto.thumbPath) {
            member.primaryPhoto.thumbPath = member.primaryPhoto.thumbPath.replace('http://', '//');
            return member.primaryPhoto.thumbPath;
        }
        else if (member.defaultPhoto && member.defaultPhoto.thumbPath) {
            member.defaultPhoto.thumbPath = member.defaultPhoto.thumbPath.replace('http://', '//');
            return member.defaultPhoto.thumbPath;
        }
        return (this.getNoPic(member.gender));
    },

    getFullOrNoPic : function (member) {
        if (member.primaryPhoto && member.defaultPhoto.fullPath) {
            return member.primaryPhoto.fullPath;
        }
        else if (member.defaultPhoto && member.defaultPhoto.fullPath) {
            return member.defaultPhoto.fullPath;
        }
        return (this.getNoPic(member.gender));
    },

    init : function() {
        this.baseUrl = spark.config.restServer + '/brandId/' + spark.config.brandData.brandId;
        this.loginData = this.getLoginData();
        this.restCallbackWithState = [];
    }
};

if (!spark.API.restClient.initialized) {
    spark.API.restClient.init();
    spark.API.restClient.initialized = true;
}
