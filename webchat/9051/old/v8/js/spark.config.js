var spark = spark || {};
spark.config = spark.config || {
    environmentSubdomain : "www",
    restServer : "https://api.spark.net",
    chatServerName : 'chat.spark-networks.com',
    boshServer : 'https://chat.spark-networks.com/http-bind'
};
/* must be included after spark.config has been created */
(function(){

    var brandDataMap =
    {
        "jdate.com" : {
            "siteName" : "JDate",
            "brandId" : 1003,
            "communityId" : 3,
            "omnitureKeyword" : "sparkjdatecom",
            "locale" : spark.localization.enUS
        },

        "jdate.co.uk" : {
            "siteName" : "JDate",
            "brandId" : 1003,
            "communityId" : 3,
            "omnitureKeyword" : "sparkjdatecouk",
            "locale" : spark.localization.enUS
        },

        "jdate.co.il" : {
            "siteName" : "JDate",
            "brandId" : 1004,
            "communityId" : 3,
            "omnitureKeyword" : "sparkjdatecoil",
            "locale" : spark.localization.heIL
        },

        "jdate.fr" : {
            "siteName" : "JDate",
            "brandId" : 1003,
            "communityId" : 3,
            "omnitureKeyword" : "sparkjdatefr",
            "locale" : spark.localization.frFR
        },

        "blacksingles.com" : {
            "siteName" : "Black Singles",
            "brandId" : 90510,
            "communityId" : 24,
            "omnitureKeyword" : "sparkblack",
            "locale" : spark.localization.enUS
        }
    };

    var domainParts = document.domain.split('.');
    var last = domainParts.length -1;
    var rootDomain;
    if (domainParts[last -1] === "co") { // grab 3 parts: jdate co il, jdate co uk
        rootDomain = domainParts[last -2] + '.' + domainParts[last -1] + '.' + domainParts[last];
    }
    else { // grab 2 parts, jdate com, jdate fr, blacksingles com
        rootDomain = domainParts[last -1] + '.' + domainParts[last];
    }
    spark.config.brandData = brandDataMap[rootDomain];
    spark.config.fwsUrl = 'http://' + spark.config.environmentSubdomain + '.' +  rootDomain;
    spark.config.IMInitiatorUrl = spark.config.fwsUrl +  '/applications/instantmessenger/icxml.aspx';
    spark.locale = spark.config.brandData.locale;

})();