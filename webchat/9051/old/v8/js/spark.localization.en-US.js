spark.localization.enUS = Object.create(spark.localization);
(function(){
    var translations = {
        "loadingProfile" : "Loading profile information.",
        "waitingForMember" : "Hang tight! We are waiting for {0} to connect.",
        "youBlockedMember" : "You have blocked member {0}",
        "youMustUnblock" : "You have blocked {0} and will need to unblock this member in order to send a message.",
        "pendingSubscription" : "Hello, there! You will be able to send an Instant Message as soon as your pending subscription is verified!",
        "notAcceptingMessages" : "Sorry, but {0} is currently not accepting messages.",
        "pleaseLogIn" : "Could not log into IM.  Please log in to the main site and try again",
        "yearsOld" : "{0} Years Old",
        "genderAndSeeking" : "{0} seeking {1}",
        "chatConnectionFailed" : "Connection to chat server failed.  Retrying...",
        "connecting" : "Connecting...",
        "disconnectedReconnecting" : "Disconnected from chat server, reconnecting",
        "userNotAccepting" : "Sorry, but {0} is currently not accepting messages.",
        "IMWindowClosed" : "{0}'s IM window is now closed.",
        "IMWindowOpen" : "There we go! {0}'s IM window is now open.",
        "memberIsTyping" : "{0} is typing...",
        "memberDeclinedInvitation" : "Sorry. {0} has declined your instant message invitation at this time.",
        "messagesWillBeEmailed" : "If {0} doesn't receive this message, they will be emailed when you close your window.",
        "refreshPage" : "Not connected - refresh this page",
        "oopsTryLater" : "Oops, we couldn't complete the request. Please try again later",
        "male" : "Man",
        "female" : "Woman"
    };
    spark.localization.enUS.addTranslations(translations);
})();
