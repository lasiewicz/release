function logIt(message) {
    if (typeof(console) !== "undefined") {
        console.log(message);
    }
    if ($('#debug')[0]) {
        $('#debug').append(message + '<br/>\n');
    }
}

var webIM = webIM || {

    chatServerName: spark.config.chatServerName,
    boshServer: spark.config.boshServer,
    IMInitiatorUrl: spark.config.IMInitiatorUrl,
    ownJid: null,
    ownMiniProfile: null,
    chatterMemberId: null,
    chatterJid: null,
    chatterMiniProfile: null,
    chatterState: "unavailable",
    chatterIsBlocked : false,
    chatterIsFavorite : false,
	invitationPending: false,
    composing: false,
    composingContainer: null,
    errorContainer: null,
    connection: null,
    messageQueue: [],
    startChatTime: new Date().getTime(),
    endChatTime: null,
    messageCount: 0,
    stropheStatusText: {
        0: 'Error',
        1: 'Connecting',
        2: 'Connection fail',
        3: 'Authenticating',
        4: 'Authentication fail',
        5: 'Connected',
        6: 'Disconnected',
        7: 'Disconnecting',
        8: 'Attached'
    },
    stropheOmnitureCodes: {
        0: 'strophe_error',
        1: 'strophe_connecting',
        2: 'strophe_connection_fail',
        3: 'strophe_authenticating',
        4: 'strophe_authentication_fail',
        5: 'strophe_connected',
        6: 'strophe_disconnected',
        7: 'strophe_disconnecting',
        8: 'strophe_attached'
    },


    callOmniture : function() {
        s.pageName = "webchat_window";
        s.t();
    },

    getQueryVariable: function (url, variable) {
        var query = url.split("?");
        if (query.length > 1) {
            var vars = query[1].split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    return pair[1];
                }
            }
        }
        return null;
    },

    getFormattedTime: function () {   // TODO: move to a utility class?
        var date = new Date();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var minStr = '' + minutes;
        while (minStr.length < 2) {
            minStr = '0' + minStr;
        }
        var amPm = 'am';
        if (hours > 11) {
            amPm = 'pm';
            if (hours > 12) {
                hours -= 12;
            }
        }
        return hours + ':' + minStr + amPm;
    },

    renderTemplate: function (templateSelector, dto, doAppend) {
        var template = $(templateSelector);
        if (template[0]) {
            var html = Mustache.to_html(template.html(), dto);
            var targetSelector = template.data("target-element");
            var target = $(targetSelector);
            if (target[0]) {
                if (doAppend) {
                    target.append(html);
                }
                else {
                    target.html(html);
                }
                return targetSelector;
            }
        }
    },


    on_roster: function () { //(iq)
        logIt('on roster event');
        // set up presence handler and send initial presence
        webIM.connection.addHandler(webIM.on_presence, null, "presence");
        webIM.connection.send($pres());
        webIM.connection.send($pres({
            to: webIM.chatterJid,
            "type": "online"
        }));

        return true;
    },

    pending_subscriber: null,

    on_presence: function (presence) {
        var ptype = $(presence).attr('type');
        var from = $(presence).attr('from');
        var jid = Strophe.getBareJidFromJid(from);
        var dto;

        logIt("on_presence " + jid + ' ' + (ptype ? ptype : ''));
        if (webIM.chatterJid !== jid) {
            return true;
        }

        if (ptype === "online" || ptype === "declined") {
            webIM.invitationPending = false;
        }

        if (webIM.chatterState === "declined" && ptype === "unavailable") {
            return; // don't change status from declined to unavailable or "waiting for xxx to join would display"
                    // instead of "xxx has declined"
        }

        if (ptype === "declined") {
            dto = { "username": spark.config.brandData.siteName,
                "chatText": spark.locale.getText('memberDeclinedInvitation', webIM.chatterMiniProfile.username),
                "timestamp": webIM.getFormattedTime()
            };
            webIM.renderTemplate('#system_template', dto, true);
            webIM.scroll_chat();
        }

        if (webIM.chatterState === "unavailable" && ptype === "online") {
            // if user is coming online, send them a presence message.  This should not cause infinite looping
            // because the message is only sent when a user goes from unavailable to online status
            webIM.connection.send($pres({
                to: webIM.chatterJid,
                "type": "online"
            }));
        }

        webIM.chatterState = ptype;
        webIM.renderChatState();

        if (webIM.chatterState === "online") {
            for (var i = 0; i < webIM.messageQueue.length; i++) {
                webIM.sendMessage(webIM.messageQueue[i]);
                logIt("sent queue message " + webIM.messageQueue[i]);
            }
            webIM.messageQueue.length = 0;
            webIM.missedIMQueued = false;
        }

        return true;
    },

    renderChatState: function () {
        var dto;
        if (webIM.chatterIsBlocked) {
            dto = { "userState": spark.locale.getText('youBlockedMember', webIM.chatterMiniProfile.username) };
            webIM.composingContainer = $(webIM.renderTemplate('#chatter_status_template', dto, false));
            return;
        }
        if (webIM.chatterState === 'unavailable') {
            var message;
            if (webIM.invitationPending) {
                message = spark.locale.getText('waitingForMember', webIM.chatterMiniProfile.username);
            }
            else if (!webIM.chatterHasJoined && !webIM.invitationPending) {
                message = spark.locale.getText('typeAMessage', webIM.chatterMiniProfile.username);
            }
            else {
                message = spark.locale.getText('IMWindowClosed', webIM.chatterMiniProfile.username);
            }
            dto = { "userState": message };
            webIM.composingContainer = $(webIM.renderTemplate('#chatter_status_template', dto, false));
        } else if (webIM.chatterState === "blocked"){
            dto = { "userState": spark.locale.getText('userNotAccepting', webIM.chatterMiniProfile.username) };
            webIM.composingContainer = $(webIM.renderTemplate('#chatter_status_template', dto, false));
        } else if (webIM.chatterState === "declined"){
            dto = { "userState": spark.locale.getText('memberDeclinedInvitation', webIM.chatterMiniProfile.username) };
            webIM.composingContainer = $(webIM.renderTemplate('#chatter_status_template', dto, false));
        } else {
            webIM.chatterHasJoined = true;
            dto = { "userState": spark.locale.getText('IMWindowOpen', webIM.chatterMiniProfile.username)  };
            webIM.composingContainer = $(webIM.renderTemplate('#chatter_status_template', dto, false));
            clearTimeout(webIM.missedImInfoCall);
            logIt("clearing missedImInfoCall");
        }
    },

    on_roster_changed: function () { // (iq)
        logIt('roster changed');
        return true;
    },

    on_message: function (message) {
        logIt('on message');
        if (!message) {
            logIt('Error! on_message called with no message!');
        }

        var full_jid = $(message).attr('from');
        var jid = Strophe.getBareJidFromJid(full_jid);
        var chatArea = $('.chat_messages');
        if (chatArea.length === 0) {
            chatArea = null;
        }
        var chatterMemberId = parseInt(jid ? jid.split("@")[0] : null, 10);

        if (chatArea[0]) {
            if (chatterMemberId !== webIM.chatterMemberId) {
                return true; // message wasn't meant for this window, ignore it
            }
            if (webIM.chatterState === "blocked" || webIM.chatterIsBlocked) { // if blocked or blocking, don't continue
                return true;
            }
            var composing = $(message).find('composing');
            var dto;
            if (composing.length > 0) { // if this is a "composing" message, add "<user> is typing..."
                dto = { "userState": spark.locale.getText('memberIsTyping', webIM.chatterMiniProfile.username) };
                webIM.composingContainer = $(webIM.renderTemplate('#chatter_status_template', dto, false));
                return true;
            }

            var paused = $(message).find('paused');
            if (paused.length > 0) { // if this is a "paused" message, remove "user is typing..."
                if (webIM.composingContainer) {
                    webIM.renderChatState();
                }
                return true;
            }

            var body = $(message).find("html > body");
            if (body.length === 0) {
                body = $(message).find('body');
                if (body.length > 0) {
                    body = body.text()
                } else {
                    body = null;
                }
            } else {
                body = body.contents();

                var span = $("<span></span>");
                body.each(function () {
                    if (document.importNode) {
                        $(document.importNode(this, true)).appendTo(span);
                    } else {
                        // IE workaround
                        span.append(this.xml);
                    }
                });
                body = span;
            }

            if (body) {
                var div = document.createElement('temp');
                div.innerHTML = body;
                var decoded = div.firstChild.nodeValue;
                dto = { "thumbnail": webIM.chatterMiniProfile.thumbOrNoPic,
                    "username": webIM.chatterMiniProfile.username,
                    "chatText": decoded,
                    "timestamp": webIM.getFormattedTime()
                };
                webIM.renderTemplate('#message_template', dto, true);
                if (webIM.composingContainer) {
                    webIM.renderChatState();
                    //webIM.composingContainer.html('');
                }
                webIM.scroll_chat();
            }
        }
        return true;
    },

    sendMessage: function (messageText) {
        if (!messageText) {
            return false;
        }

        var dto;
        var now;
        if (webIM.chatterState === "blocked") {
            now = webIM.getFormattedTime();
            dto = { "username": spark.config.brandData.siteName,
                "chatText": spark.locale.getText('userNotAccepting', webIM.chatterMiniProfile.username),
                "timestamp": now
            };
            webIM.renderTemplate('#system_template', dto, true);
            webIM.scroll_chat();
            return false;
        }

        if (webIM.chatterState === "declined") {
            webIM.chatterState = "unavailable";  // let the user try to make contact again
            webIM.renderChatState();
        }

        if (webIM.chatterIsBlocked) {
            now = webIM.getFormattedTime();
            dto = { "username": spark.config.brandData.siteName,
                "chatText": spark.locale.getText('youMustUnblock', webIM.chatterMiniProfile.username),
                "timestamp": now
            };
            webIM.renderTemplate('#system_template', dto, true);
            webIM.scroll_chat();
            return false;
        }

        var message = $msg({ to: webIM.chatterJid,
            "type": "chat"
        })
            .c('body').t(messageText).up()
            .c('active', { xmlns: "http://jabber.org/protocol/chatstates" });
        
        $(this).val('');
        webIM.composing = false;
		webIM.messageCount++;
		if (webIM.chatterState === "unavailable") {
            webIM.messageQueue.push(messageText.replace("\n", ""));
            //logIt("message enqueued. total:" + webIM.messageQueue.length + " message:" + messageText);
			if (!webIM.invitationPending) {
                webIM.initBedrockIM(webIM.IMInitiatorUrl, spark.API.restClient.loginData.memberId, webIM.chatterMemberId);
                webIM.queueMissedIMMessages();
                webIM.invitationPending = true;
				s.eVar26 = "IM";
                s.events = "event11,event67";
                webIM.callOmniture();
            }
            webIM.renderChatState(); // change status from "type a message to invite..." to "waiting for ..."
        }
		webIM.connection.send(message);
        return true;
    },

    scroll_chat: function () {
        var div = $('.chat_messages').get(0);
        div.scrollTop = div.scrollHeight;
    },

    displayOwnMessage : function (messageText) {
        var dto = { "username": webIM.ownMiniProfile.username,
            "chatText": messageText,
            "timestamp": webIM.getFormattedTime()
        };
        webIM.renderTemplate('#self_message_template', dto, true);
        webIM.scroll_chat();
    },

    blockUI : function(message) {
        $.unblockUI();
        $.blockUI({
            message: message,
            css: {
                border: '',
                cursor: '',
                color: '',
                backgroundColor: 'white',
                padding:'1em',
                fontSize : '18px'
            },
            overlayCSS: {
                cursor: null
            }
        });
    },

    allowUI : function() {
        $.unblockUI();
    },

    reconnectToServer: function () {
        setTimeout(function () {
            $(document).trigger('connect', {
                jid: webIM.ownJid,
                password: spark.API.restClient.loginData.accessToken
            });
        }, 2000);
    },

    sendSynchDisconnect: function() {
        if (webIM.connection) {
            webIM.connection.send($pres({
                to: webIM.chatterJid,
                "type": "unavailable"
            }));
            webIM.connection.sync = true;
            webIM.connection.flush();
            webIM.connection.disconnect('disconnecting');
        }
    },

    queueMissedIMMessages: function () {
        if (webIM.missedIMQueued) {
            return;
        }

        webIM.missedImInfoCall = setTimeout(function () {
            var dto = { "username": spark.config.brandData.siteName,
                "chatText": spark.locale.getText('messagesWillBeEmailed', webIM.chatterMiniProfile.username),
                "timestamp": webIM.getFormattedTime()
            };
            webIM.renderTemplate('#system_template', dto, true);
            webIM.scroll_chat();
            webIM.missedIMQueued = false;
        }, 300000);
        webIM.missedIMQueued = true;
    },
	
    bindEvents: function () {
        $(document).bind('connect', function (ev, data) {
            var conn = new Strophe.Connection(webIM.boshServer);
            var message = spark.locale.getText('connecting');
            webIM.blockUI(message);
            logIt(message);

            conn.connect(data.jid, data.password, function (status) {
                logIt('status: ' + webIM.stropheStatusText[status]);
                if (webIM.stropheOmnitureCodes[status]) {
                    s.eVar66 = webIM.stropheOmnitureCodes[status];
                    webIM.callOmniture();
                }
                //webIM.blockUI(webIM.stropheStatusText[status]);
                if (status === Strophe.Status.CONNFAIL) {
                    logIt('Connection to chat server at ' + webIM.boshServer + ' failed.  Retrying...');
                    message = spark.locale.getText('chatConnectionFailed');
                    webIM.blockUI(message);
                    webIM.reconnectToServer();
                }
                else if (status === Strophe.Status.CONNECTED) {
                    webIM.allowUI();
                    $(document).trigger('connected');
                }
                else if (status === Strophe.Status.DISCONNECTED) {
                    if (!webIM.closingWindow) {
                        message = spark.locale.getText('disconnectedReconnecting');
                        webIM.blockUI(message);
                        webIM.reconnectToServer();
                    }
                }
                else if (status === Strophe.Status.AUTHFAIL) {
                    logIt('authorization with chat server failed');
                    s.eVar66 = 'error_ejab_auth_failed';
                    webIM.callOmniture();
                    message = spark.locale.getText('pleaseLogIn', spark.API.restClient.errorCodes['error_ejab_auth_failed']);
                    webIM.blockUI(message);
                }
            });
            webIM.connection = conn;
        });


        $(document).bind('connected', function () {
            var iq = $iq({ type: 'get' }).c('query', { xmlns: 'jabber:iq:roster' });
            webIM.connection.sendIQ(iq, webIM.on_roster);

            webIM.connection.addHandler(webIM.on_roster_changed,
                "jabber:iq:roster", "iq", "set");
            webIM.connection.addHandler(webIM.on_message,
                null, "message", "chat");
        });

        $(window).bind('beforeunload', function () {
                webIM.closingWindow = true; // to prevent reconnecting to chat
                webIM.sendSynchDisconnect();
                // IE8 closes before the disconnecting message goes through, delay by prompting the user

                // if receiver never got online, trigger missed IM on bedrock.
                if (webIM.messageQueue.length > 0 && webIM.chatterState === "unavailable") {
                    webIM.missedBedrockIM(webIM.IMInitiatorUrl, webIM.messageQueue, spark.API.restClient.loginData.memberId, webIM.chatterMemberId);
                }
                webIM.endChatTime = new Date().getTime();
                var duration = Math.round((webIM.endChatTime - webIM.startChatTime) / 1000);
                logIt("Total duration in seconds:" + duration  + " Start:" + webIM.startChatTime
                    + " End:" + webIM.endChatTime);
                logIt("Total messages sent:" + webIM.messageCount);

				s.eVar58 = duration;
				s.eVar59 = webIM.messageCount;
				s.events = "";
				webIM.callOmniture();

                if (navigator.appVersion.indexOf('MSIE 8') !== -1) {
                    return 'Are you sure you want to leave this IM session?';
                    // note: browsers normally show their own text, not what's returned
                }
        });

        $(document).bind('disconnected', function () {
            webIM.connection = null;
            webIM.pending_subscriber = null;
            webIM.blockUI(spark.locale.getText('refreshPage'));
            $('.chat_messages ul').empty();
        });

        $('.send_button').live('click', function () {
            var input = $('.chat_input');
            var messageText = input.val();
            if (webIM.sendMessage(messageText)) {
                webIM.displayOwnMessage(messageText);
            }
            input.val('');
        });

        webIM.$favorite_symbol.live('click', function () {
            var isAddAction = !webIM.chatterIsFavorite;
            logIt(isAddAction ? 'Adding' : 'Removing' + ' favorite');
            spark.API.restClient.changeHotlist('Default', webIM.chatterMemberId, isAddAction, function(success) {
                if (success) {
                    var imagePath;
                    var titleText;
                    if (isAddAction) {
                        imagePath = 'images/icon_fave_on.png';
                        titleText = spark.locale.getText('unfavoriteMember');
                    }
                    else {
                        imagePath = 'images/icon_fave_off.png';
                        titleText = spark.locale.getText('favoriteMember');
                    }
                    webIM.$favorite_symbol = $('#favorite_symbol');
                    webIM.$favorite_symbol.attr('src', imagePath);
                    webIM.$favorite_symbol.attr('title', titleText);
                    webIM.chatterIsFavorite = isAddAction;
                    logIt('successfully updated favorite list');
                }
                else {
                    logIt('failed to update favorite list');
                }
            });
        });

        webIM.$block_symbol.live('click', function () {
            var isAddAction = !webIM.chatterIsBlocked;
            logIt(isAddAction ? 'Adding' : 'Removing' + ' to blocked list');
            var status = isAddAction ? 'blocked' : 'online';
            webIM.connection.send($pres({
                to: webIM.chatterJid,
                "type": status
            }));

            spark.API.restClient.changeHotlist('IgnoreList', webIM.chatterMemberId, isAddAction, function(success) {
                if (success) {
                    var imagePath;
                    var titleText;
                    if (isAddAction) {
                        imagePath = 'images/icon_block_on.png';
                        titleText = spark.locale.getText('unblockMember');
                    }
                    else {
                        imagePath = 'images/icon_block_off.png';
                        titleText = spark.locale.getText('blockMember');
                    }
                    webIM.$block_symbol = $('#block_symbol');
                    webIM.$block_symbol.attr('src', imagePath);
                    webIM.$block_symbol.attr('title', titleText);

                    webIM.chatterIsBlocked = isAddAction;
                    webIM.renderChatState();
                    logIt('successfully updated blocked list');
                }
                else {
                    logIt('failed to update blocked list');
                }
            });
        });
        
        $('.chat_input').live('keyup', function (ev) {
            var messageText = $(this).val().replace(/[\n\r]/g, '');
            if (ev.which === 13) { //} && !ev.shiftKey) {
                ev.preventDefault();
                $(this).val('');
                if (!messageText) {
                    return;
                }
                logIt('sending message: ' + messageText);
                if (webIM.sendMessage(messageText)) {
                    webIM.displayOwnMessage(messageText);
                }
                return;
            }

            if (!webIM.isMaxLengthSupported()) { // if textarea maxLength isn't supported by the browser, limit characters here
                var val = webIM.$textarea.val().replace(/\r\n|\r|\n/g, "\r\n"); // Fix OS-specific line-returns to do an accurate count
                if (val.length > webIM.maxLength) {
                    val = val.slice(0, webIM.maxLength);
                    webIM.$textarea.val(val);
                }
            }

            var notify;
            if (!messageText && webIM.composing) {  // remove "user is typing..."
                logIt('sending paused message');
                notify = $msg({ to: webIM.chatterJid, "type": "chat" })
                    .c('paused', { xmlns: "http://jabber.org/protocol/chatstates" });
                webIM.connection.send(notify);
                webIM.composing = false;
            }
            else if (messageText && !webIM.composing && webIM.chatterState === "online") { // add "user is typing..."
                logIt('sending composing message');
                notify = $msg({ to: webIM.chatterJid, "type": "chat" })
                    .c('composing', { xmlns: "http://jabber.org/protocol/chatstates" });
                webIM.connection.send(notify);
                webIM.composing = true;
            }
        });
    },

    directUserToLogin: function (errorCode) {
        s.eVar66 = errorCode;
        webIM.callOmniture();
        webIM.blockUI(spark.locale.getText('pleaseLogIn', spark.API.restClient.errorCodes[errorCode]));
    },

    initBedrockIM: function (initUrl, senderMemberId, targetMemberId) {
        logIt('calling ' + initUrl + '?action=startconversation&memberid=' + senderMemberId + '&targetmemberid=' + targetMemberId);
        $.ajax({
            crossDomain: true,
            url: initUrl + '?action=startconversation&memberid=' + senderMemberId + '&targetmemberid=' + targetMemberId,
            success: function (data) {
                logIt('initBedrockIM successful \n' + data.statusText);
            },
            error: function (data) {
                logIt('initBedrockIM error \n' + data.statusText);
            }
        });
    },

    missedBedrockIM: function (initUrl, messageQueue, senderMemberId, targetMemberId) {
    	if (messageQueue.length == 0)
    		return;
        var xmlData = webIM.buildMissedXml(messageQueue);
        var missedUrl = initUrl + '?action=notifyconnectionclosed&memberid=' + senderMemberId + '&targetmemberid=' + targetMemberId;
        var postData = 'xmlData=' + xmlData;
        logIt('calling ' + missedUrl + ' ' + postData);
        $.ajax({
            crossDomain: true,
            type: "post",
            data: postData,
            url: missedUrl,
            success: function (data) {
                logIt('missedBedrockIM successful \n' + data.statusText);
            },
            error: function (data) {
                logIt('missedBedrockIM error \n' + data.statusText);
            }
        });
    },

    buildMissedXml: function (messageQueue) {
        var doc = document.implementation.createDocument(null, "undeliveredMessages", null);
        for (var i = 0; i < messageQueue.length; i++) {
            var messageElement = doc.createElement("message");
            var messageText = doc.createTextNode(messageQueue[i]);
            messageElement.appendChild(messageText);
            doc.documentElement.appendChild(messageElement);
        }
        return new XMLSerializer().serializeToString(doc);
    },

    useFlXHRIfNeeded : function () {
        if (!spark.API.restClient.corsIsSupported()) {
            $.flXHRproxy.registerOptions(spark.config.restServer, {xmlResponseText:false});
            // set flXHR as the default XHR object used in jQuery AJAX requests
            $.ajaxSetup({transport:'flXHRproxy'});
        }
    },

    isMaxLengthSupported : function () {
        if (typeof(webIM.maxLengthSupported) !== "undefined") {
            return webIM.maxLengthSupported;
        }
        var ta =  document.createElement("textarea");
        webIM.maxLengthSupported = (typeof(ta.maxLength) !== "undefined");
        logIt('maxLength is ' + (webIM.maxLengthSupported ? '' : 'not ') + 'supported');
        return webIM.maxLengthSupported;
    },

    init: function () {
        "use strict";
        webIM.callOmniture();
        if (!spark || !spark.config) {
            var error = 'Missing spark.config, cannot continue';
            logIt(error);
            throw(error);
        }
        $('.send_button').text(spark.locale.getText('send'));
        webIM.$textarea = $(".chat_input");
        webIM.$textarea.focus();
        var max = webIM.$textarea.attr('maxLength');
        webIM.maxLength = parseInt(max, 10);

        webIM.$favorite_symbol = $('#favorite_symbol');
        webIM.$block_symbol = $('#block_symbol');

        var errorCode;
        this.bindEvents();
        if (!spark.API.restClient.loginData.accessToken) {
            logIt('Missing access token cookie, log into main site');
            errorCode = 'error_missing_token_cookie';
            s.eVar66 = errorCode;
            webIM.callOmniture();
            webIM.blockUI(spark.locale.getText('pleaseLogIn', spark.API.restClient.errorCodes[errorCode]));
            return;
        }
        if (!spark.API.restClient.loginData.memberId) {
            logIt('Missing logged in member ID cookie');
            errorCode = 'error_missing_member_id_cookie';
            s.eVar66 = errorCode;
            webIM.callOmniture();
            webIM.blockUI(spark.locale.getText('pleaseLogIn', spark.API.restClient.errorCodes[errorCode]));
            return;
        }
        this.chatterMemberId = parseInt(webIM.getQueryVariable(window.location.href, "memberId"), 10);
        if (!this.chatterMemberId) {
            webIM.blockUI(spark.locale.getText('noMemberId'));
            return;
        }
        this.ownJid = spark.API.restClient.loginData.memberId + '-' + spark.config.brandData.communityId + '@' + this.chatServerName;
        this.chatterJid = this.chatterMemberId + '-' + spark.config.brandData.communityId + '@' + this.chatServerName;
        if (spark.API.restClient.loginData.memberId === this.chatterMemberId) {
            webIM.blockUI(spark.locale.getText('noSelfChat'));
            return;
        }

        this.useFlXHRIfNeeded();
        spark.API.restClient.setLoggedOutCallback(webIM.directUserToLogin);

        webIM.blockUI(spark.locale.getText('loadingProfile'));
        // get chatter's mini profile first, so the username can be displayed in messages like "<username> has blocked you, etc.
        spark.API.restClient.fetchMiniProfile(webIM.chatterMemberId, function (success, data) {
            if (success) {
                webIM.chatterMiniProfile = data;

                var gender = webIM.chatterMiniProfile.gender ? spark.locale.getText(webIM.chatterMiniProfile.gender.toLowerCase()) : '';
                var seekingGender = webIM.chatterMiniProfile.seekingGender ? spark.locale.getText(webIM.chatterMiniProfile.seekingGender.toLowerCase()) : '';
                var dto = {
                    "miniProfileId": webIM.chatterMiniProfile.id,
                    "username": webIM.chatterMiniProfile.username,
                    "thumbnail": webIM.chatterMiniProfile.thumbOrNoPic,
                    "age": spark.locale.getText('yearsOld', webIM.chatterMiniProfile.age),
                    "genderAndSeeking": spark.locale.getText('genderAndSeeking', gender, seekingGender),
                    "location": webIM.chatterMiniProfile.location,
                    "profileLink": spark.config.fwsUrl + "/Applications/MemberProfile/ViewProfile.aspx?PersistLayoutTemplate=1&EntryPoint=99999&Ordinal=1&MemberID=" +  
                    					webIM.chatterMemberId + "&LayoutTemplateID=15&rnd=1348172485420"
                };

                webIM.renderTemplate('#mini_profile_template', dto, false);
                webIM.$favorite_symbol = $('#favorite_symbol');
                webIM.$favorite_symbol.attr('title', spark.locale.getText('favoriteMember'));

                webIM.$block_symbol = $('#block_symbol');
                webIM.$block_symbol.attr('title', spark.locale.getText('blockMember'));

                // todo: bedrock should assign the title instead. launchEjabIMWindow();
                document.title = webIM.chatterMiniProfile.username + ' - Instant Message';
                spark.API.restClient.fetchHotlist('ignoreList', spark.API.restClient.loginData.memberId, function (success, data) {
                    if (!success) {
                        logIt("Unable to retrieve blocked list for logged in member");
                        var message = spark.locale.getText('oopsTryLater', spark.API.restClient.loginData.memberId);
                        webIM.blockUI(message);
                        logIt(message);
                        return;
                    }
                    var len = data.length;
                    for (var i = 0; i < len; i++) {
                        var miniProfile = data[i]['miniProfile'];
                        if (miniProfile && miniProfile.id === webIM.chatterMemberId) {
                            webIM.blockUI(spark.locale.getText('youMustUnblock', webIM.chatterMiniProfile.username));
                            webIM.chatterIsBlocked = true;
                            // todo: display blocked symbol instead of using blockUI?
                            return;
                        }
                    }
                    spark.API.restClient.checkMemberOnHotlist('ignoreList', webIM.chatterMemberId, spark.API.restClient.loginData.memberId, function (success, data) {
                        if (!success) {
                            logIt('Unable to retrieve blocked status for member ' +  spark.API.restClient.loginData.memberId + ' on blocked list of member ' + webIM.chatterMemberId);
                            webIM.blockUI(spark.locale.getText('oopsTryLater'));
                            return;
                        }
                        if (data) {
                            webIM.blockUI(spark.locale.getText('notAcceptingMessages', webIM.chatterMiniProfile.username));
                            return;
                        }
                        spark.API.restClient.fetchMiniProfile(spark.API.restClient.loginData.memberId, function (success, data) {
                            if (success) {
                                webIM.ownMiniProfile = data;
                                if (!webIM.ownMiniProfile['passedFraudCheck']) {
                                    webIM.blockUI(spark.locale.getText('pendingSubscription'));
                                    return;
                                }
                                webIM.renderChatState();
                                $(function () {
                                    $(document).trigger('connect', {
                                        jid: webIM.ownJid,
                                        password:spark.API.restClient.loginData.accessToken
                                    });
                                });
                            }
                            else { // token looked good but got 401, tell user to log in
                                logIt('Error getting own mini Profile');
                                webIM.directUserToLogin('error_api_call_failed');
                            }
                        });
                        spark.API.restClient.fetchHotlist('default', spark.API.restClient.loginData.memberId, function (success, data) {
                            if (!success) {
                                var message = 'Unable to retrieve favorite list for member ' + spark.API.restClient.loginData.memberId;
                                logIt(message);
                                return;
                            }
                            var len = data.length;
                            for (var i = 0; i < len; i++) {
                                var miniProfile = data[i]['miniProfile'];
                                if (miniProfile && miniProfile.id === webIM.chatterMemberId) {
                                    webIM.$favorite_symbol = $('#favorite_symbol');
                                    webIM.$favorite_symbol.attr('src', 'images/icon_fave_on.png');
                                    webIM.$favorite_symbol.attr('title', spark.locale.getText('unfavoriteMember'));
                                    webIM.chatterIsFavorite = true;
                                    return;
                                }
                            }
                        });

                    });
                });

            }
            else { // token looked good but got 401, tell user to log in
                logIt('Error getting chatter mini Profile');
                webIM.directUserToLogin('error_api_call_failed');
            }
        });

    }
};

if (!webIM.initialized) {
    $(function() {
        webIM.init();
        webIM.initialized = true;
    });
}
