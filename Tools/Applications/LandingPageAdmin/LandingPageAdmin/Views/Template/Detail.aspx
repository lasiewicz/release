﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Matchnet.Content.ValueObjects.LandingPage.LandingPageTemplate>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Templates - Detail
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%=Model.Name %></h2>
    
    <table>
        <tr>
            <td class="lpheaders">Description: </td>
            <td><%=Model.Description %></td>
        </tr>
        <tr>
            <td class="lpheaders">Definition: </td>
            <td><%=Html.Encode(Model.Definition) %></td>
        </tr>
        <tr>
            <td class="lpheaders">User Defined Fields:</td>
            <td>
            <%if(Model.UserDefinedFields != null){ 
                foreach(string field in Model.UserDefinedFields) {%>
                    <%=field %><br />    
                <%} %>
            <%} %>
            </td>
        </tr>
    </table>
    <br /><br />
    <div class="button"><%=Html.ActionLink("Edit", "Edit", "Template", new {templateID = Model.LandingPageTemplateID}, null) %></div>
</asp:Content>
