﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<TemplateWithFiles>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Templates - Edit
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="additionalScripts" runat="server">
    <script src="../../Scripts/jquery.tmpl.js" type="text/javascript"></script>   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%Html.BeginForm("Edit", "Template", FormMethod.Post, new { @id = "editTemplate" }); %>
    <table>
        <tr>
            <td class="lpheaders">Name: </td>
            <td><%=Html.TextBox("Name", Model.Template.Name, new { @maxlength = 50 })%>&nbsp;<input type="button" value="Check" onclick="CheckTemplateNameAvailability(<%=Model.Template.LandingPageTemplateID %>);" /></td>
        </tr>
        <tr>
            <td class="lpheaders">Description: </td>
            <td><%=Html.TextBox("Description", Model.Template.Description, new { @maxlength = 200 })%></td>
        </tr>
    </table>
    <br />
    <div id="definitionContainer">
    <span class="lpheaders">Definition:</span><br />
    <%=Html.TextArea("Definition", Model.Template.Definition, new { @class = "resizeable", cols = 60, rows = 45 })%>
            <div id="files-list">
                <%if (Model.DirectoryWithFiles.Files != null){ %>
                   <span class="lpheaders">Files:</span> <br />
                    <%foreach (string file in Model.DirectoryWithFiles.Files) {%>
                        <a href="/Assets/<%=Model.Template.Name %>/<%=file %>"><%=file %></a><br />
                    <%}
                }%>
            </div>            
            <br clear="all" />          
    </div>
    
    <div id="user-defined">
    <br />
    <span class="lpheaders">User Defined Fields:</span><br />
    <ul id="userDefinedFieldsContainer">
    <%if (Model.Template.UserDefinedFields != null)
      { 
        int num = 0;

        foreach (string field in Model.Template.UserDefinedFields)
        {
            num++;
            %>
            <li><%=Html.TextBox("UserDefinedField" + num.ToString(), field, new { @class = "userDefinedFieldInput required" }) %> <a href="#" class="deleteUserDefinedField" data-fieldNum="<%= num.ToString()  %>"></a></li> 
        <%} %>
    <%} %>

       </ul>
           <a id="addField"><div href="#" class="addUserDefinedField"></div>Add Field</a>

    
    <%=Html.Hidden("templateID", Model.Template.LandingPageTemplateID.ToString())%>
    <input type="submit" class="input-edits" value="Save" />
    
    </div>
    
<div class="templateEditButtons">
    
    <%Html.EndForm(); %>
    <%Html.BeginForm("Delete", "Template", FormMethod.Post, new {@id="editForm2"}); %>
    <%=Html.Hidden("templateID", Model.Template.LandingPageTemplateID.ToString())%>
    <input type="submit" class="input-edits" value="Delete" />
    <%Html.EndForm(); %>

</div>
<br /><br />    
<div id="dialog" title="Delete Template"><p>
<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span> 
Are you sure you want to delete this template?</p>
<p>To confirm click Submit Form.</p>
<p>To edit, click Cancel.</p></div>    
    
    
</asp:Content>
