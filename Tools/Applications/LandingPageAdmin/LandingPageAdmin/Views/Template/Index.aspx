﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LandingPageAdmin.Models.ModelViews.TemplateList>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Templates - Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<table>
    <tr class="lpheaders">
        <td>Name</td>
        <td>Used By</td>
    </tr>
    <%foreach (LandingPageTemplate template in Model.TemplatesWithPages.Keys) {%>
        <tr>
            <td>
                <%=Html.ActionLink(template.Name, "Detail", new {templateID = template.LandingPageTemplateID}) %>
            </td>
            <td>
                <%if(Model.TemplatesWithPages[template] != null){
                      foreach (TemplateBasedLandingPage page in Model.TemplatesWithPages[template])
                      { %>
                        <%=Html.ActionLink(page.Name, "Detail", "LandingPage", new { landingPageID = page.LandingPageID }, null)%>
                    <%} %>
                <%} %> 
            </td>    
        </tr>
    <%} %>    
</table>
<br />
<div class="button"><%=Html.ActionLink("Add New Template", "Add") %></div>
</asp:Content>
