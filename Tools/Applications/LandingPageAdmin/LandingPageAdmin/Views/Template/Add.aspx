﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Templates - Add
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%Html.BeginForm("Add", "Template"); %>
    <table>
        <tr>
            <td>Name: </td>
            <td><%=Html.TextBox("Name", null, new { @maxlength = 50 })%>&nbsp;<input type="button" value="Check" onclick="CheckTemplateNameAvailability(0);" /></td>
        </tr>
        <tr>
            <td>Description: </td>
            <td><%=Html.TextBox("Description", null, new { @maxlength = 200 })%></td>
        </tr>
    </table>
    
    <br />
    <div id="definitionContainer">
    Definition:<br />
    <%=Html.TextArea("Definition", string.Empty, new { @class = "resizeable", cols = 60, rows = 45 })%>
    </div>
    
    <div id="user-defined">
    <br />
    User Defined Fields:<br />
    <ul id="userDefinedFieldsContainer">
    </ul>
    <a id="addField"><div href="#" class="addUserDefinedField"></div>Add Field</a>
    
    <input type="submit" value="Save" />
    
    </div>
    
    <%Html.EndForm(); %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="additionalScripts" runat="server">
</asp:Content>
