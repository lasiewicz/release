﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<GroupedDirectories>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<span class="lpheaders">Shared:</span><br />
<%=Html.ActionLink("Shared", "Detail", new { directoryName = "Shared" })%> (<span class="lpCount"><%=Model.SharedDirectory.NumberOfFiles.ToString() %></span>)


<br /><br />
<ul class="fileList">
<span class="lpheaders">Templates:</span><br />
<%foreach (DirectoryInformation templateDirectory in Model.TemplateDirectories) { %>
    <li><%=Html.ActionLink(templateDirectory.Name, "Detail", new {directoryName = templateDirectory.Name }) %> (<span class="lpCount"><%=templateDirectory.NumberOfFiles.ToString() %></span>)</li>
<%} %>
</ul>

<ul class="fileList">
<span class="lpheaders">Landing Pages:</span><br />
<%foreach (DirectoryInformation pageDirectory in Model.LandingPageDirectories) { %>
    <li><%=Html.ActionLink(pageDirectory.Name, "Detail", new { directoryName = pageDirectory.Name })%> (<span class="lpCount"><%=pageDirectory.NumberOfFiles.ToString()%></span>)</li>
<%} %>
</ul>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="additionalScripts" runat="server">
</asp:Content>
