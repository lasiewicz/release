﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<DirectoryWithFiles>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Detail
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h1><%=Model.DirectoryName %></h1><br />
<dl id="deleteFileList">
<%foreach (string file in Model.Files) { %>
<dt><a href="/Assets/<%=Model.DirectoryName %>/<%=file %>" target="_blank"><%=file %></a></dt>
<dd><%Html.BeginForm("DeleteFile", "File"); %><input type="hidden" name="directoryName" value="<%=Model.DirectoryName %>" /><input type="hidden" name="fileName" value="<%=file %>"/><input type="submit" value="Delete" /> <%Html.EndForm(); %></dd>

<%} %>
</dl>
<%Html.BeginForm("Upload", "File", FormMethod.Post, new { enctype = "multipart/form-data" }); %>
<%=Html.Hidden("directoryName", Model.DirectoryName)%>
<input type="file" id="fileUpload" name="fileUpload" size="23"/><br />
<input type="submit" value="Upload file" />
<%Html.EndForm(); %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="additionalScripts" runat="server">
</asp:Content>
