﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LandingPageWithDetails>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Landing Pages - Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<table>
    <tr>
        <td class="lpheaders">ID:</td>
        <td><%=Model.LandingPage.LandingPageID.ToString() %></td>
    </tr>
    <tr>
        <td class="lpheaders">Name:</td>
        <td><%=Model.LandingPage.Name %></td>
    </tr>
    <tr>
        <td class="lpheaders">URL:</td>
        <td><%=Model.LandingPage.URL %></td>
    </tr>
    <tr>
        <td class="lpheaders">Analytics Page Name:</td>
        <td><%=Model.LandingPage.AnalyticsPageName %></td>
    </tr>
    <tr>
        <td class="lpheaders">Description:</td>
        <td><%=Model.LandingPage.Description %></td>
    </tr>
    <tr>
        <td class="lpheaders">Tags:</td>
        <td><%=Model.LandingPage.Tags %></td>
    </tr>
    <tr>
        <td class="lpheaders">Published:</td>
        <td><%=Model.LandingPage.Published.ToString() %></td>
    </tr>
</table>
<br />
<span class="lpheaders">Sites:</span>
<table>
<%foreach(SiteEnumValue siteEnumValue in Model.Sites) {%>
   <%if(Model.LandingPage.Sites.Contains(siteEnumValue.ID)) { %> 
    <tr>
        <td><%=Model.Sites[Model.Sites.IndexOf(siteEnumValue)].Name %></td>
    </tr>
    <%} %>
<%} %>
</table>    
<br />
<%if (Model.AllUserDefinedFields != null){ %>
    <span class="lpheaders">User Defined Values</span><br />
    <table>
        <tr>
            <td class="lpheaders">Name</td>
            <td class="lpheaders">Value</td>
        </tr>
    <%foreach (UserDefinedField field in Model.AllUserDefinedFields) {%>
        <tr>
            <td><%=field.Name %></td>
            <td><%=field.Value %></td>
        </tr>
    <%} %>
    </table>
    <br />
<%} %>
<div>
<span class="button"><%=Html.ActionLink("Edit", "Edit", new { landingPageID = Model.LandingPage.LandingPageID })%></span>
<span class="button"><%=Html.ActionLink("Copy", "Copy", new { landingPageID = Model.LandingPage.LandingPageID })%></span>
<input class="previewBtn" type="button" value="Preview" onclick="Preview(<%=Model.LandingPage.LandingPageID.ToString() %>);" />
</div>

</asp:Content>
