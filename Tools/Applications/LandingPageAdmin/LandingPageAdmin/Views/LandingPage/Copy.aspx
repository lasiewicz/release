﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LandingPageWithDetails>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Landing Pages - Copy
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
    function GetUserDefinedFields() {
        var landingPageTemplateID= $('#landingPageTemplateID').val();
        var landingPageID = $('#landingPageID').val();
        $.ajax({
            type: "GET",
            url: '/API/GetUserDefinedFields?landingPageID=' + landingPageID + '&landingPageTemplateID=' + landingPageTemplateID,
            dataType: 'html',
            success: function(result) {
                $('#userdefinedfields').html(result);
            }
        });

    }
</script>

<%Html.BeginForm("Add", "LandingPage"); %>
<table>
    <tr>
        <td class="lpheaders">Name:</td>
        <td><%=Html.TextBox("Name", Model.LandingPage.Name,  new { @maxlength = 50 }) %>&nbsp;<input type="button" value="Check" onclick="CheckLandingPageNameAvailability(0);" /></td>
    </tr>
    <tr>
        <td class="lpheaders">Template:</td>
        <td>
            <select id="landingPageTemplateID" name="landingPageTemplateID" onchange="GetUserDefinedFields();">
            <%foreach (LandingPageTemplate template in Model.Templates) { %>
                <%if (template.LandingPageTemplateID == Model.LandingPage.Template.LandingPageTemplateID) { %>
                    <option value="<%=template.LandingPageTemplateID.ToString() %>" selected><%=template.Name%></option>
                <%} else{ %>
                    <option value="<%=template.LandingPageTemplateID.ToString() %>"><%=template.Name%></option>
                <%} %>
            <%} %>
            </select>
        </td>
    </tr>
    <tr>
        <td class="lpheaders">Analytics Page Name:</td>
        <td><%=Html.TextBox("AnalyticsPageName", Model.LandingPage.AnalyticsPageName, new { @maxlength = 50 })%></td>
    </tr>
    <tr>
        <td class="lpheaders">Description:</td>
        <td><%=Html.TextBox("Description", Model.LandingPage.Description, new { @maxlength = 500 })%></td>
    </tr>
    <tr>
        <td class="lpheaders">Tags:</td>
        <td><%=Html.TextBox("Tags", Model.LandingPage.Tags, new { @maxlength = 500 })%></td>
    </tr>
    <tr>
        <td class="lpheaders">Url:</td>
        <td><%=Html.TextBox("URL", Model.LandingPage.URL, new { @maxlength = 50 })%>&nbsp;<input type="button" value="Check" onclick="CheckURLAvailability(0);" /></td>
    </tr>
</table>
<br />
<span class="lpheaders">Sites:</span>
<table>
<%foreach(SiteEnumValue site in Model.Sites) {%>
    <tr>
        <td><%=Html.RadioButton("Site", site.ID.ToString(), Model.LandingPage.Sites.Contains(site.ID)) %><%=site.Name%></td>
    </tr>
<%} %>
</table>  
<span class="lpheaders">Published:</span> <br />
No<%=Html.RadioButton("published", "false", !Model.LandingPage.Published) %>&nbsp;&nbsp;Yes<%=Html.RadioButton("published", "true", Model.LandingPage.Published)%>
<br />
<div id="userdefinedfields">
    <%if (Model.AllUserDefinedFields != null) { %>
        <%Html.RenderPartial("Controls/UserDefinedFields", Model.AllUserDefinedFields); %>
    <%} %>
</div>
            
    <br />
    <input type="hidden" name="landingPageID" id="landingPageID" value="0" />
    <input type="submit" value="Save" />
    
    <%Html.EndForm(); %>
</asp:Content>
