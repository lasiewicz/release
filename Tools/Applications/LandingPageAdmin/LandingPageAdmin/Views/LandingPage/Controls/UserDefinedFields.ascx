﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<List<UserDefinedField>>" %>
<%if(Model != null){ %>
    <br /><span class="lpheaders">User Defined Fields:</span><br />
    <table id="userDefinedFields">
    <tr class="lpheaders"><td>Names</td><td>Value</td></tr>
    <%
    int num = 0;
    foreach (UserDefinedField field in Model){
        num++;%>
        <tr>
            <td><%=field.Name %><%=Html.Hidden("UserDefinedField" + num.ToString(), field.Name) %></td>
            <td><%=Html.TextBox("UserDefinedValue" + num.ToString(), field.Value, new { @class = "userDefinedFieldInput required" })%></td>
        </tr>
    <%} %>
    </table>
<%} %>