﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LandingPageWithDetails>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Landing Pages - Edit
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<%Html.BeginForm("Edit", "LandingPage", FormMethod.Post, new {@class="editForm1"}); %>
<table>
    <tr>
        <td>Name:</td>
        <td><%=Html.TextBox("Name", Model.LandingPage.Name, new { @maxlength = 50 })%>&nbsp;<input type="button" value="Check" onclick="CheckLandingPageNameAvailability(<%=Model.LandingPage.LandingPageID.ToString() %>);" /></td>
    </tr>
    <tr>
        <td>Template:</td>
        <td>
            <select id="landingPageTemplateID" name="landingPageTemplateID" onchange="GetUserDefinedFields();">
            <%foreach (LandingPageTemplate template in Model.Templates) { %>
                <%if (template.LandingPageTemplateID == Model.LandingPage.Template.LandingPageTemplateID) { %>
                    <option value="<%=template.LandingPageTemplateID.ToString() %>" selected><%=template.Name%></option>
                <%} else{ %>
                    <option value="<%=template.LandingPageTemplateID.ToString() %>"><%=template.Name%></option>
                <%} %>
            <%} %>
            </select>
        </td>
    </tr>
    <tr>
        <td>Analytics Page Name:</td>
        <td><%=Html.TextBox("AnalyticsPageName", Model.LandingPage.AnalyticsPageName, new { @maxlength = 50 })%></td>
    </tr>
    <tr>
        <td>Description:</td>
        <td><%=Html.TextBox("Description", Model.LandingPage.Description, new { @maxlength = 500 })%></td>
    </tr>
    <tr>
        <td>Tags:</td>
        <td><%=Html.TextBox("Tags", Model.LandingPage.Tags, new { @maxlength = 500 })%></td>
    </tr>
    <tr>
        <td>Url:</td>
        <td><%=Html.TextBox("URL", Model.LandingPage.URL, new { @maxlength = 50 })%>&nbsp;<input type="button" value="Check" onclick="CheckURLAvailability(<%=Model.LandingPage.LandingPageID.ToString() %>);" /></td>
    </tr>
</table>
<br />
Sites:
<ul id="siteList">
<%foreach(SiteEnumValue site in Model.Sites) {%>
    <li>
        <%=Html.RadioButton("Site", site.ID.ToString(), Model.LandingPage.Sites.Contains(site.ID)) %><%=site.Name%>
    </li>
<%} %>
</ul>
Published: <br />
No<%=Html.RadioButton("published", "false", !Model.LandingPage.Published) %>&nbsp;&nbsp;Yes<%=Html.RadioButton("published", "true", Model.LandingPage.Published)%>
<br />

<div id="userdefinedfields">
    <%if (Model.AllUserDefinedFields != null) { %>
        <%Html.RenderPartial("Controls/UserDefinedFields", Model.AllUserDefinedFields); %>
    <%} %>
</div>
            
    <br />
 <div class="ladingPageEditButtons">
    <%=Html.Hidden("landingPageID", Model.LandingPage.LandingPageID.ToString()) %>    
    <input type="submit" value="Save" class="input-edits" />
    <%Html.EndForm(); %>
        
    <%Html.BeginForm("Delete", "LandingPage", FormMethod.Post, new {@id="editForm2"}); %>    
    <%=Html.Hidden("landingPageID", Model.LandingPage.LandingPageID.ToString()) %>
    <input type="submit" value="Delete" class="input-edits" />
    <%Html.EndForm(); %>
    <input type="button" value="Preview" onclick="Preview(<%=Model.LandingPage.LandingPageID.ToString() %>);"  />
 </div>      


<div id="dialog" title="Delete Landing Page"><p>

<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span> 
Are you sure you want to delete this landing page?</p>
<p>To confirm click Submit Form.</p>
<p>To edit, click Cancel.</p></div>



</asp:Content>
