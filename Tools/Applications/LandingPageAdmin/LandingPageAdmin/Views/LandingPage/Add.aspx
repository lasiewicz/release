﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LandingPageWithDetails>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Landing Pages - Add
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<%Html.BeginForm("Add", "LandingPage"); %>
<%=Html.Hidden("landingPageID", "0") %>
<table>
    <tr>
        <td class="lpheaders">Name:</td>
        <td><%=Html.TextBox("Name", null, new { @maxlength = 50 })%>&nbsp;<input type="button" value="Check" onclick="CheckLandingPageNameAvailability(0);" /></td>
    </tr>
    <tr>
        <td class="lpheaders">Template:</td>
        <td>
            <select id="landingPageTemplateID" name="landingPageTemplateID" onchange="GetUserDefinedFields();">
            <option value="0">Select a Template</option>
            <%foreach (LandingPageTemplate template in Model.Templates) { %>
                <option value="<%=template.LandingPageTemplateID.ToString() %>"><%=template.Name%></option>
            <%} %>
            </select>
        </td>
    </tr>
    <tr>
        <td class="lpheaders">Analytics Page Name:</td>
        <td><%=Html.TextBox("AnalyticsPageName", null, new { @maxlength = 50 })%></td>
    </tr>
    <tr>
        <td class="lpheaders">Description:</td>
        <td><%=Html.TextBox("Description", null, new { @maxlength = 500 }) %></td>
    </tr>
    <tr>
        <td class="lpheaders">Tags:</td>
        <td><%=Html.TextBox("Tags", null, new { @maxlength = 500 }) %></td>
    </tr>
    <tr>
        <td class="lpheaders">Url:</td>
        <td><%=Html.TextBox("URL", null, new { @maxlength = 50 })%>&nbsp;<input type="button" value="Check" onclick="CheckURLAvailability(0);" /></td>
    </tr>
</table>
<br />
<span class="lpheaders">Sites:</span>
<ul id="siteList">
<%foreach(SiteEnumValue site in Model.Sites) {%>
    <li>
        <%=Html.RadioButton("Site", site.ID.ToString())%><%=site.Name%>
    </li>
<%} %>
</ul>

<span class="lpheaders">Published:</span> <br />
No<%=Html.RadioButton("published", "false", true) %>&nbsp;&nbsp;Yes<%=Html.RadioButton("published", "true", false) %>

<div id="userdefinedfields">
    
</div>
            
    <br />
    <input type="submit" value="Save" />
    <%Html.EndForm(); %>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="additionalScripts" runat="server">
</asp:Content>
