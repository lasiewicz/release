﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<List<TemplateBasedLandingPage>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Landing Pages - Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <table>
        <tr class="lpheaders">
            <td>Name</td>
            <td>Template</td>
            <td>URL</td>
            <td>Published</td>
        </tr>
        <%foreach (TemplateBasedLandingPage page in Model)
          { %>
            <tr>
                <td><%=Html.ActionLink(page.Name, "Detail", new {landingPageID=page.LandingPageID}) %></td>
                <td><%=Html.ActionLink(page.Template.Name, "Detail", "Template", new {templateID=page.Template.LandingPageTemplateID}, null) %></td>
                <td>http://{site}/LP/<%=page.URL %></td>
                <td><%=page.Published.ToString() %></td>
            </tr>
        <%} %>
    </table>
<br />
<div class="button"><%=Html.ActionLink("Add New Landing Page", "Add") %></div>
</asp:Content>
