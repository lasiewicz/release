﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.CommonLibrary;

namespace LandingPageAdmin.Models
{
    public class GeneralHelper
    {
        public static List<SiteEnumValue> GetSiteEnumValues()
        {
            List<SiteEnumValue> values = new List<SiteEnumValue>();
            Array siteIDValues = System.Enum.GetValues(typeof(SiteIDs));
            foreach (object siteIDValue in siteIDValues)
            {
                if (Convert.ToInt32(siteIDValue) != 0)
                {
                    values.Add(new SiteEnumValue(Convert.ToInt32(siteIDValue), System.Enum.GetName(typeof(SiteIDs), siteIDValue)));
                }
            }

            return values;
        }
    }
}
