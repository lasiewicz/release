﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.LandingPage;
using Matchnet.Content.ServiceAdapters;
using LandingPageAdmin.Models.ModelViews;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Text;
using System.IO;

namespace LandingPageAdmin.Models
{
    public class LandingPageService
    {
        public List<TemplateBasedLandingPage> GetAllLandingPages()
        {
            return LandingPageSA.Instance.GetLandingPages(true);
        }

        public TemplateBasedLandingPage GetLandingPage(int landingPageID)
        {
            return LandingPageSA.Instance.GetTemplateBasedLandingPage(landingPageID);
        }

        public LandingPageWithDetails GetLandingPageWithDetails(int landingPageID)
        {
            LandingPageWithDetails landingPageWithDetails = new LandingPageWithDetails();
            landingPageWithDetails.LandingPage = GetLandingPage(landingPageID);
            landingPageWithDetails.Sites = GeneralHelper.GetSiteEnumValues();
            landingPageWithDetails.Templates = new TemplateService().GetAllTemplates();
            if (landingPageID > 0)
            {
                landingPageWithDetails.AllUserDefinedFields = LandingPageSA.Instance.GetFieldsForLandingPage(landingPageWithDetails.LandingPage);
            }

            return landingPageWithDetails;
        }

        public void SaveLandingPage(TemplateBasedLandingPage page)
        {
            bool uniqueName = CheckNameAvailability(page.Name, page.LandingPageID);
            if (!uniqueName)
            {
                throw new Exception("Landing page name is not unique.");
            }

            TemplateBasedLandingPage existingPage = GetLandingPageByURL(page.URL);
            if (existingPage != null && page.LandingPageID != existingPage.LandingPageID)
            {
                throw new Exception("Landing page name URL is not unique.");
            }

            LandingPageSA.Instance.SaveLandingPage(page);
            if (page.LandingPageID <= 0)
            {
                new FileService().CreateDirectory(page.Name);
            }
        }

        public void DeleteLandingPage(int landingPageID)
        {
            LandingPageSA.Instance.DeleteLandingPage(landingPageID);
        }

        public void SaveLandingPage(TemplateBasedLandingPage page, string oldName)
        {
            SaveLandingPage(page);
            new FileService().RenameDirectory(oldName, page.Name);

        }

        public string GetPageContentFromLandingPage(TemplateBasedLandingPage landingPage)
        {
            if (landingPage.Template.Definition.Contains("xsl:stylesheet"))
            {
                try
                {
                    XPathDocument userDefinedFields = new XPathDocument(new StringReader(UserDefinedFieldsToXML(landingPage.UserDefinedFields)));
                    XslCompiledTransform xslTrans = new XslCompiledTransform();
                    xslTrans.Load(XmlReader.Create(new StringReader(landingPage.Template.Definition)));
                    StringWriter sw = new StringWriter();

                    xslTrans.Transform(userDefinedFields, null, sw);
                    return HttpUtility.HtmlDecode(sw.ToString());
                }
                catch (Exception xmlException)
                {
                    string errorString = "There was an error parsing the XSL template definition";
                    if (xmlException.InnerException != null && xmlException.InnerException is XmlException)
                    {
                        errorString = errorString + ": " + xmlException.InnerException.Message;
                    }
                    return errorString;
                }
            }
            else
            {
                return landingPage.Template.Definition;
            }
        }

        public bool CheckNameAvailability(string name, int existingID)
        {
            return LandingPageSA.Instance.CheckNameAvailability(name, existingID, CheckAvailabilityType.LandingPage);
        }

        public TemplateBasedLandingPage GetLandingPageByURL(string url)
        {
            return LandingPageSA.Instance.GetTemplateBasedLandingPageByURL(url);
        }

        private string UserDefinedFieldsToXML(List<UserDefinedField> fields)
        {
            StringBuilder xml = new StringBuilder();
            if (fields.Count > 0)
            {
                xml.Append("<UserDefinedFields>");
                foreach (UserDefinedField field in fields)
                {
                    xml.Append(string.Format(@"<Field Name=""{0}"" Value=""{1}""></Field>", field.Name, HttpUtility.HtmlEncode(field.Value)));
                }
                xml.Append("</UserDefinedFields>");
            }
            return xml.ToString();
        }
    }
}
