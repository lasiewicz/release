﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LandingPageAdmin.Models.ModelViews
{
    public class GroupedDirectories
    {
        public DirectoryInformation SharedDirectory { get; set; }
        public List<DirectoryInformation> TemplateDirectories { get; set; }
        public List<DirectoryInformation> LandingPageDirectories { get; set; }

        public GroupedDirectories(DirectoryInformation sharedDirectory, List<DirectoryInformation> templateDirectories, List<DirectoryInformation> landingPageDirectories)
        {
            SharedDirectory = sharedDirectory;
            TemplateDirectories = templateDirectories;
            LandingPageDirectories = landingPageDirectories;
        }
    }
}
