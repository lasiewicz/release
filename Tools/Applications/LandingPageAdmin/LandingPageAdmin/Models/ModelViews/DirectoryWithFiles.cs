﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LandingPageAdmin.Models.ModelViews
{
    public class DirectoryWithFiles
    {
        public string DirectoryName { get; set; }
        public List<string> Files { get; set; }

        public DirectoryWithFiles()
        {

        }

        public DirectoryWithFiles(string directoryName, List<string> files)
        {
            DirectoryName = directoryName;
            Files = files;
        }
    }
}
