﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.LandingPage;
using Matchnet.Content.ServiceAdapters;

namespace LandingPageAdmin.Models.ModelViews
{
    public class TemplateWithFiles
    {
        public LandingPageTemplate Template { get; set; }
        public DirectoryWithFiles DirectoryWithFiles { get; set; }

        public TemplateWithFiles(LandingPageTemplate template, DirectoryWithFiles directoryWithFiles)
        {
            Template = template;
            DirectoryWithFiles = directoryWithFiles;
        }

    }
}
