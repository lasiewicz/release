﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LandingPageAdmin.Models.ModelViews
{
    public class Preview
    {
        public string PageOutput { get; set; }

        public Preview(string pageOutput)
        {
            PageOutput = pageOutput;
        }
    }
}
