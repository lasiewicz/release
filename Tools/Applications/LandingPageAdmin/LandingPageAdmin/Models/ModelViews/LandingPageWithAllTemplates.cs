﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.LandingPage;
using Matchnet.Content.ServiceAdapters;

namespace LandingPageAdmin.Models.ModelViews
{
    public class LandingPageWithAllTemplates
    {
        public LandingPage LandingPage { get; set; }
        public List<LandingPageTemplate> Templates { get; set; }
    }
}
