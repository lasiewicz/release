﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.LandingPage;
using Matchnet.Content.ServiceAdapters;

namespace LandingPageAdmin.Models.ModelViews
{
    public class TemplateList
    {
        public Dictionary<LandingPageTemplate, List<TemplateBasedLandingPage>> TemplatesWithPages { get; set; }
    }
}
