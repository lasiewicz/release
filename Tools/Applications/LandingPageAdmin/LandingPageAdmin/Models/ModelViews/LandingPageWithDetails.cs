﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.LandingPage;
using Matchnet.Content.ServiceAdapters;

namespace LandingPageAdmin.Models.ModelViews
{
    public class LandingPageWithDetails
    {
        public TemplateBasedLandingPage LandingPage { get; set; }
        public List<LandingPageTemplate> Templates { get; set; }
        public List<SiteEnumValue> Sites {get; set;}
        public List<UserDefinedField> AllUserDefinedFields {get; set; }

        public LandingPageWithDetails()
        {

        }

        public LandingPageWithDetails(TemplateBasedLandingPage page, List<LandingPageTemplate> templates, List<SiteEnumValue> sites)
        {
            LandingPage = page;
            Templates = templates;
            Sites = sites;
        }

        public LandingPageWithDetails(TemplateBasedLandingPage page, List<SiteEnumValue> sites)
        {
            LandingPage = page;
            Sites = sites;
        }
    }
}
