﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.LandingPage;
using Matchnet.Content.ServiceAdapters;
using LandingPageAdmin.Models.ModelViews;

namespace LandingPageAdmin.Models
{
    public class TemplateService
    {
        public Dictionary<LandingPageTemplate, List<TemplateBasedLandingPage>> GetAllTemplatesWithPages()
        {
            Dictionary<LandingPageTemplate, List<TemplateBasedLandingPage>> templatesWithPages = new Dictionary<LandingPageTemplate, List<TemplateBasedLandingPage>>();

            List<LandingPageTemplate> templates = LandingPageSA.Instance.GetLandingPageTemplates();
            List<TemplateBasedLandingPage> pages = LandingPageSA.Instance.GetLandingPages(true);

            foreach(LandingPageTemplate template in templates)
            {
                List<TemplateBasedLandingPage> pagesForTemplate = (from TemplateBasedLandingPage lp in pages where lp.Template.LandingPageTemplateID == template.LandingPageTemplateID select lp).ToList();
                templatesWithPages.Add(template, pagesForTemplate);
            }
            
            return templatesWithPages;
        }

        public LandingPageTemplate GetTemplate(int templateID)
        {
            List<LandingPageTemplate> templates = LandingPageSA.Instance.GetLandingPageTemplates();
            return (from LandingPageTemplate lpt in templates where lpt.LandingPageTemplateID == templateID select lpt).FirstOrDefault();
        }

        public TemplateWithFiles GetTemplateWithFiles(int templateID)
        {
            List<LandingPageTemplate> templates = LandingPageSA.Instance.GetLandingPageTemplates();
            LandingPageTemplate template = (from LandingPageTemplate lpt in templates where lpt.LandingPageTemplateID == templateID select lpt).FirstOrDefault();
            DirectoryWithFiles directoryWithFiles = new FileService().GetFilesForDirectory(template.Name);
            return new TemplateWithFiles(template, directoryWithFiles);
        }

        public List<LandingPageTemplate> GetAllTemplates()
        {
            return LandingPageSA.Instance.GetLandingPageTemplates();
        }

        public void SaveTemplate(LandingPageTemplate template)
        {
            bool uniqueName = CheckNameAvailability(template.Name, template.LandingPageTemplateID);
            if (!uniqueName)
            {
                throw new Exception("Template name is not unique.");
            }
            
            LandingPageSA.Instance.SaveLandingPageTemplate(template);
            if (template.LandingPageTemplateID <= 0)
            {
                new FileService().CreateDirectory(template.Name);
            }
        }

        public void SaveTemplate(LandingPageTemplate template, string oldName)
        {
            SaveTemplate(template);
            new FileService().RenameDirectory(oldName, template.Name);
        }

        public bool CheckNameAvailability(string name, int existingID)
        {
            return LandingPageSA.Instance.CheckNameAvailability(name, existingID, CheckAvailabilityType.Template);
        }

        public List<UserDefinedField> GetFieldsFromTemplate(LandingPageTemplate template)
        {
            if (template.UserDefinedFields == null)
            {
                return null;
            }
            
            List<UserDefinedField> fields = new List<UserDefinedField>();
            foreach (string field in template.UserDefinedFields)
            {
                fields.Add(new UserDefinedField(field, string.Empty));
            }

            return fields;
        }

        public void DeleteTemplate(int templateID)
        {
            LandingPageSA.Instance.DeleteTemplate(templateID);
        }
    }
}
