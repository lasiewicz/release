﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Matchnet.Content.ValueObjects.LandingPage;
using Matchnet.Configuration.ServiceAdapters;
using LandingPageAdmin.Models.ModelViews;


namespace LandingPageAdmin.Models
{
    public enum DirectoryType
    {
        Shared=0,
        Template=1,
        LandingPage=2
    }
    
    public class FileService
    {
        string _rootDirectory;

        public FileService()
        {
            _rootDirectory = RuntimeSettings.GetSetting("LANDING_PAGE_TOOL_ASSETS_PHYSICAL_DIRECTORY");
        }

        public void CreateDirectory(string directoryName)
        {
            if (!Directory.Exists(_rootDirectory + directoryName))
            {
                Directory.CreateDirectory(_rootDirectory + directoryName);
            }
        }

        public void RenameDirectory(string oldName, string newName)
        {
            if (Directory.Exists(_rootDirectory + oldName))
            {
                Directory.Move(_rootDirectory + oldName, _rootDirectory + newName);
            }
        }

        public DirectoryWithFiles GetFilesForDirectory(string directoryName)
        {
           List<string> files = Directory.GetFiles(_rootDirectory + directoryName).ToList();
           for(int index=0; index<files.Count;index++)
           {
               files[index] = Path.GetFileName(files[index]);
           }

           return new DirectoryWithFiles(directoryName, files);
        }

        public GroupedDirectories GetGroupedDirectories()
        {
            int sharedFileCount = Directory.GetFiles(_rootDirectory + "Shared").Length;
            DirectoryInformation sharedDirectory = new DirectoryInformation("Shared", sharedFileCount);
            List<DirectoryInformation> templateDirectories = new List<DirectoryInformation>();
            List<DirectoryInformation> pageDirectories = new List<DirectoryInformation>();

            foreach (LandingPageTemplate template in new TemplateService().GetAllTemplates())
            {
                int templateFileCount = Directory.GetFiles(_rootDirectory + template.Name).Length;
                templateDirectories.Add(new DirectoryInformation(template.Name, templateFileCount));
            }

            foreach (TemplateBasedLandingPage page in new LandingPageService().GetAllLandingPages())
            {
                int pageFileCount = Directory.GetFiles(_rootDirectory + page.Name).Length;
                pageDirectories.Add(new DirectoryInformation(page.Name, pageFileCount));
            }

            GroupedDirectories directories = new GroupedDirectories(sharedDirectory, templateDirectories, pageDirectories);
            return directories;
        }

        public void DeleteFile(string directoryName, string fileName)
        {
            File.Delete(_rootDirectory + directoryName + @"\" + fileName);
        }

        public void DeleteDirectory(string directoryName)
        {
            string[] files = Directory.GetFiles(_rootDirectory + directoryName);
            foreach(string file in files)
            {
                DeleteFile(directoryName, file);
            }

            Directory.Delete(_rootDirectory + directoryName);
        }

        public void UploadFile(string directoryName, HttpPostedFileBase file)
        {
            if (file.ContentLength > 0)
            {
                string filePath = Path.Combine(HttpContext.Current.Server.MapPath("/Assets/" + directoryName), Path.GetFileName(file.FileName));
                file.SaveAs(filePath);
            }
        }
    }
}
