﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Security.Principal;
using Microsoft.Interop.Security.AzRoles;
using System.Configuration;

namespace LandingPageAdmin.Models
{
    public class ActiveDirectoryHelper
    {
        private const string applicationName = "LandingPageAdminTool";
        Cache _cache = HttpContext.Current.Cache;

        public bool CheckUserAccess(IIdentity User, int applicationOperationID)
        {
            bool result = true;
            if (ConfigurationManager.AppSettings["isADEnabled"] == "true")
            {
                WindowsIdentity user = (WindowsIdentity)User;

                result = false;

                Dictionary<int, bool> cachedPermissions = GetCachedPermissions(user.Name);

                if (cachedPermissions.ContainsKey(applicationOperationID))
                {
                    result = cachedPermissions[applicationOperationID];
                }
                else
                {
                    result = VerifyUserAccess(user, applicationOperationID);
                    CacheResult(cachedPermissions, user.Name, applicationOperationID, result);
                }
            }
            return result;
        }

        private Dictionary<int, bool> GetCachedPermissions(string userName)
        {
            object cachedPermissions = _cache.Get(GetCacheKey(userName));
            Dictionary<int, bool> permissions = null;

            if (cachedPermissions != null)
            {
                permissions = (Dictionary<int, bool>)cachedPermissions;
            }
            else
            {
                permissions = new Dictionary<int, bool>();
            }

            return permissions;
        }

        private void CacheResult(Dictionary<int, bool> cachedPermissions, string userName, int applicationOperationID, bool result)
        {
            cachedPermissions.Add(applicationOperationID, result);
            if (cachedPermissions.Count == 1)
            {
                int cachingPeriod = Convert.ToInt32(ConfigurationManager.AppSettings["PermissionCachePeriodInHours"]);
                _cache.Add(GetCacheKey(userName), cachedPermissions, null, DateTime.Now.AddHours(cachingPeriod), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
            }
        }

        private bool VerifyUserAccess(WindowsIdentity User, int applicationOperationID)
        {
            AzAuthorizationStore store = new AzAuthorizationStoreClass();
            store.Initialize(0, ConfigurationManager.ConnectionStrings["ActiveDirectoryAzMan"].ConnectionString, null);
            IAzApplication app = store.OpenApplication(applicationName, null);

            IntPtr token = User.Token;

            IAzClientContext ctx = app.InitializeClientContextFromToken((ulong)token.ToInt64(), null);

            int result = AccessCheck(ctx, "Check Operation Access", applicationOperationID);
            return result == 0;
        }

        private int AccessCheck(IAzClientContext ctx, string objectName, int operation)
        {
            object[] ops = new object[] { (object)operation };

            object[] scopes = new object[] { (object)"" };

            object[] results = (object[])ctx.AccessCheck(objectName, scopes, ops, null, null, null, null, null);
            return (int)results[0];
        }

        private string GetCacheKey(string userName)
        {
            return "ADMINPERMISSIONS-" + userName;
        }
    }

}
