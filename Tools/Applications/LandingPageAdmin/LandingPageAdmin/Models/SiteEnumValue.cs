﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LandingPageAdmin.Models
{
    public class SiteEnumValue
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public SiteEnumValue(int id, string name)
        {
            ID = id;
            Name = name;
        }
    }
}
