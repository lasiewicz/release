﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LandingPageAdmin.Models
{
    public class DirectoryInformation
    {
        public string Name { get; set; }
        public int NumberOfFiles { get; set; }

        public DirectoryInformation(string name, int numberOfFiles)
        {
            Name = name;
            NumberOfFiles = numberOfFiles;
        }
    }
}
