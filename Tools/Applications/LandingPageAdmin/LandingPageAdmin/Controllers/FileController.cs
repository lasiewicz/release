﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LandingPageAdmin.Models;
using LandingPageAdmin.Models.ModelViews;
using Matchnet.Content.ValueObjects.LandingPage;

namespace LandingPageAdmin.Controllers
{
    public class FileController : BaseController
    {
        //
        // GET: /File/

        public ActionResult Index()
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.Access, true);
            
            GroupedDirectories directories = new FileService().GetGroupedDirectories();
            return View(directories);
        }

        public ActionResult Detail(string directoryName)
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.Access, true);
            
            DirectoryWithFiles files = new FileService().GetFilesForDirectory(directoryName);
            return View(files);
        }

        public ActionResult Upload(string directoryName)
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.Access, true);
            
            new FileService().UploadFile(directoryName, Request.Files[0]);
            return RedirectToAction("Detail", new { directoryName = directoryName });
        }

        public ActionResult DeleteFile(string directoryName, string fileName)
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.Access, true);
            
            new FileService().DeleteFile(directoryName, fileName);
            return RedirectToAction("Detail", new { directoryName = directoryName });
        }

    }
}
