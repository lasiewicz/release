﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LandingPageAdmin.Models;
using LandingPageAdmin.Models.ModelViews;
using Matchnet.Content.ValueObjects.LandingPage;
using Matchnet.Content.ServiceAdapters;

namespace LandingPageAdmin.Controllers
{
    public class APIController : Controller
    {
        //
        // GET: /API/

        public ActionResult GetUserDefinedFields(int landingPageID, int landingPageTemplateID)
        {
            TemplateBasedLandingPage page = new LandingPageService().GetLandingPage(landingPageID);
            List<UserDefinedField> fields = null;

            if (page != null && page.Template.LandingPageTemplateID == landingPageTemplateID)
            {
                fields = LandingPageSA.Instance.GetFieldsForLandingPage(page);
            }
            else
            {
                TemplateService templateService = new TemplateService();
                LandingPageTemplate template = templateService.GetTemplate(landingPageTemplateID);
                fields = templateService.GetFieldsFromTemplate(template);
            }

            return PartialView("~/Views/LandingPage/Controls/UserDefinedFields.ascx", fields);
        }

        public bool IsLandingPageNameAvailable(string name, int existingID)
        {
            return new LandingPageService().CheckNameAvailability(name, existingID);
        }

        public bool IsTemplateNameAvailable(string name, int existingID)
        {
            return new TemplateService().CheckNameAvailability(name, existingID);
        }

        public bool IsURLAvailable(string url, int existingID)
        {
            TemplateBasedLandingPage landingPage = new LandingPageService().GetLandingPageByURL(url);
            if (landingPage != null && landingPage.LandingPageID != existingID)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}
