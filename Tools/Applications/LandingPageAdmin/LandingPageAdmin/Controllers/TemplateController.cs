﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LandingPageAdmin.Models;
using LandingPageAdmin.Models.ModelViews;
using Matchnet.Content.ValueObjects.LandingPage;

namespace LandingPageAdmin.Controllers
{
    public class TemplateController : BaseController
    {
        //
        // GET: /Template/

        public ActionResult Index()
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.Access, true);
            
            TemplateService service = new TemplateService();
            TemplateList templateList = new TemplateList();
            templateList.TemplatesWithPages = service.GetAllTemplatesWithPages();
            return View(templateList);
        }

        public ActionResult Detail(int templateID)
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.Access, true);
            
            LandingPageTemplate template = new TemplateService().GetTemplate(templateID);
            return View(template);
        }

        public ActionResult Edit(int templateID)
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.Access, true);
            
            TemplateWithFiles templateWithFiles = new TemplateService().GetTemplateWithFiles(templateID);
            return View(templateWithFiles);
        }

        public ActionResult Add()
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.Access, true);
            
            return View();
        }

        public ActionResult Delete(int templateID)
        {
            base.CheckActionPermission(Enums.Operations.Access, true);

            new TemplateService().DeleteTemplate(templateID);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add(string name, string description, string definition)
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.Access, true);
            
            List<string> userDefinedFields = null;
            if (Request.Form["UserDefinedField1"] != null)
            {
                userDefinedFields = new List<string>();
                int fieldNumber = 1;
                while (Request.Form["UserDefinedField" + fieldNumber.ToString()] != null)
                {
                    userDefinedFields.Add(Request.Form["UserDefinedField" + fieldNumber.ToString()]);
                    fieldNumber++;
                }
            }

            LandingPageTemplate newTemplate = new LandingPageTemplate(0, name, description, definition, userDefinedFields);
            new TemplateService().SaveTemplate(newTemplate);

            return RedirectToAction("Index");

        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int templateID, string name, string description, string definition)
        {
            //check permission
            base.CheckActionPermission(Enums.Operations.Access, true);

            List<string> userDefinedFields = null;
            if (Request.Form["UserDefinedField1"] != null)
            {
                userDefinedFields = new List<string>();
                int fieldNumber = 1;
                while (Request.Form["UserDefinedField" + fieldNumber.ToString()] != null)
                {
                    userDefinedFields.Add(Request.Form["UserDefinedField" + fieldNumber.ToString()]);
                    fieldNumber++;
                }
            }

            LandingPageTemplate existingTemplate = new TemplateService().GetTemplate(templateID);
            LandingPageTemplate newTemplate = new LandingPageTemplate(templateID, name, description, definition, userDefinedFields);

            if (existingTemplate.Name != name)
            {
                new TemplateService().SaveTemplate(newTemplate, existingTemplate.Name);
            }
            else
            {
                new TemplateService().SaveTemplate(newTemplate);
            }

            return RedirectToAction("Detail", new { templateID = templateID });
        }

    }
}
