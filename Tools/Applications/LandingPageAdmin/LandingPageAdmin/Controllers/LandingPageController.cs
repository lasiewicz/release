﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LandingPageAdmin.Models;
using LandingPageAdmin.Models.ModelViews;
using Matchnet.Content.ValueObjects.LandingPage;
using Matchnet.Content.ServiceAdapters;

namespace LandingPageAdmin.Controllers
{
    public class LandingPageController : BaseController
    {
        //
        // GET: /LandinPage/

        public ActionResult Index()
        {
            List<TemplateBasedLandingPage> pages = new LandingPageService().GetAllLandingPages();
            return View(pages);
        }

        public ActionResult Detail(int landingPageID)
        {
            LandingPageWithDetails landingPageWithDetails = new LandingPageService().GetLandingPageWithDetails(landingPageID);
            return View(landingPageWithDetails);
        }

        public ActionResult Edit(int landingPageID)
        {
            LandingPageWithDetails landingPageWithDetails = new LandingPageService().GetLandingPageWithDetails(landingPageID);
            return View(landingPageWithDetails);
        }

        public ActionResult Add()
        {
            LandingPageWithDetails landingPageWithDetails = new LandingPageService().GetLandingPageWithDetails(0);
            return View(landingPageWithDetails);
        }

        public ActionResult Copy(int landingPageID)
        {
            LandingPageWithDetails landingPageWithDetails = new LandingPageService().GetLandingPageWithDetails(landingPageID);
            return View(landingPageWithDetails);
        }

        public ActionResult Preview(int landingPageID)
        {
            LandingPageService service = new LandingPageService();
            TemplateBasedLandingPage landingPage = service.GetLandingPage(landingPageID);
            string pageOutput = service.GetPageContentFromLandingPage(landingPage);
            return View(new Preview(pageOutput));
        }

        [HttpPost]
        public ActionResult Delete(int landingPageID)
        {
            new LandingPageService().DeleteLandingPage(landingPageID);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add(int landingPageID, int landingPageTemplateID, string analyticsPageName,
            string name, string description, string tags, string url, bool published, int site)
        {
            List<UserDefinedField> userDefinedFields = null;
            if (Request.Form["UserDefinedField1"] != null)
            {
                userDefinedFields = new List<UserDefinedField>();
                int fieldNumber = 1;
                while (Request.Form["UserDefinedField" + fieldNumber.ToString()] != null)
                {
                    UserDefinedField field = new UserDefinedField(Request.Form["UserDefinedField" + fieldNumber.ToString()], Request.Form["UserDefinedValue" + fieldNumber.ToString()]);
                    userDefinedFields.Add(field);
                    fieldNumber++;
                }
            }

            List<int> sites = new List<int>()
            {
                Convert.ToInt32(site)
            };

            LandingPageTemplate template = new LandingPageTemplate(landingPageTemplateID);
            TemplateBasedLandingPage newPage = new TemplateBasedLandingPage(landingPageID, template, name, analyticsPageName,
                description, tags, url, published, userDefinedFields, sites);

            new LandingPageService().SaveLandingPage(newPage);
            
            return RedirectToAction("Index");

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int landingPageID, int landingPageTemplateID, string analyticsPageName,
            string name, string description, string tags, string url, bool published, int site)
        {
           
            List<UserDefinedField> userDefinedFields = null;
            if (Request.Form["UserDefinedField1"] != null)
            {
                userDefinedFields = new List<UserDefinedField>();
                int fieldNumber = 1;
                while (Request.Form["UserDefinedField" + fieldNumber.ToString()] != null)
                {
                    UserDefinedField field = new UserDefinedField(Request.Form["UserDefinedField" + fieldNumber.ToString()], HttpUtility.HtmlEncode(Request.Form["UserDefinedValue" + fieldNumber.ToString()]));
                    userDefinedFields.Add(field);
                    fieldNumber++;
                }
            }

            List<int> sites = new List<int>()
            {
                Convert.ToInt32(site)
            };

            TemplateBasedLandingPage existingPage = new LandingPageService().GetLandingPage(landingPageID);
            LandingPageTemplate template = new LandingPageTemplate(landingPageTemplateID);
            TemplateBasedLandingPage newPage = new TemplateBasedLandingPage(landingPageID, template, name, analyticsPageName, 
                description, tags, url, published, userDefinedFields,sites);

            if (existingPage.Name != name)
            {
                new LandingPageService().SaveLandingPage(newPage);
                new LandingPageService().SaveLandingPage(newPage, existingPage.Name);
            }
            else
            {
                new LandingPageService().SaveLandingPage(newPage);
            }

            return RedirectToAction("Detail", new { landingPageID = landingPageID });
        }

    }
}
