﻿$(document).ready(function() {
    select_nav();
    highlightTextInput();
    $('#addField').click(function(e) {
        ieSafePreventDefault(e);
        userDefinedInput.add(e);
    });
    $('.deleteUserDefinedField').live('click', function(e) {
        ieSafePreventDefault(e);
        userDefinedInput.remove(e);
    });

    $(".resizeable").resizable({
        handles: "se"
    });
    
    // Rules and messages specifies which inputs are being validated and with what messages
    $('form').validate({
        rules: {
            Name: { required: true },
            landingPageTemplateID: { selectNone: true },
            AnalyticsPageName: { required: true },
            Description: { required: true },
            URL: { required: true },
            Site: { required: true },
            published: { required: true },
            Definition: { required: true }
        },
        messages: {
            Name: { required: "<div class='label_wrap'>Please enter a page name</div>" },
            landingPageTemplateID: { required: "<div class='label_wrap'>Please select a template</div>" },
            AnalyticsPageName: { required: "<div class='label_wrap'>Please select an analytics page name</div>" },
            Description: { required: "<div class='label_wrap'>Please enter a description</div>" },
            URL: { required: "<div class='label_wrap'>Please enter a URL</div>" },
            Site: { required: "<div class='label_wrap'>Please select a site </div>" },
            published: { required: "<div class='label_wrap'>Please choose published status</div>" },
            Definition: { required: "<div class='label_wrap'>Please enter a definition</div>" }
        }
    });

});


// To allow IE to drag and drop links into form textarea
document.ondragstart = function() {
    window.event.dataTransfer.effectAllowed = "copyLink";
};




// Image preview of files images
$(document).ready(function() {

	/* CONFIG */
		
		xOffset = 10;
		yOffset = 30;
		
		// these 2 variable determine popup's distance from the cursor
		// you might want to adjust to get the right result
		
	/* END CONFIG */
	$("#files-list a[href$=.jpg], #files-list a[href$=.gif], #files-list a[href$=.png]").hover(function(e){
		this.t = this.title;
		this.title = "";	
		var c = (this.t != "") ? "<br/>" + this.t : "";
		$("body").append("<p id='preview'><img src='"+ this.href +"' width='200' alt='Image preview' />"+ c +"</p>");								 
		$("#preview")
			.css("top",(e.pageY - xOffset) + "px")
			.css("left",(e.pageX + yOffset) + "px")
			.fadeIn("fast");						
    },
	function(){
		this.title = this.t;	
		$("#preview").remove();
    });	
	$("a.preview").mousemove(function(e){
		$("#preview")
			.css("top",(e.pageY - xOffset) + "px")
			.css("left",(e.pageX + yOffset) + "px");
	});			


});

// Dialog for deleting the landing page
$(function() {
    // jQuery UI Dialog    

    $('#dialog').dialog({
        autoOpen: false,
        width: 400,
        modal: true,
        resizable: false,
        buttons: {
            "Submit Form": function() {               
                document.getElementById("editForm2").submit();
            },
            "Cancel": function() {
                $(this).dialog("close");
            }
        }
    });

    $('form#editForm2').submit(function() {
        $('#dialog').dialog('open');
        return false;
    });
});


function dialogModal(opener, target, buttons) {
    // TODO: make a modular dialog Modal
    // This is for messages that are placed on the page at 
    // render and activated by a click.
    $(target).dialog({
        autoOpen: false,
        buttons: buttons,
        modal: true,
        resizable: false
    });
    $(opener).click(function() {
        $(target).dialog('open');
        return false;
    });
}

function popUp(options) {
    var popupMessage = options.message;
    var popupHeight = options.height || 100;
    var popupWidth = options.width || 300;
    var popupModal = options.modal || true;
    var popupResizable = options.resizable || true;
    var popupButtons = options.buttons || {};
    
    if ($('#popup').length == 0) {
        $('<div id="popup"><p></p></div>').appendTo("body");
    }
    $('#popup p').html(popupMessage);
    
    $('#popup').dialog({
            height: popupHeight,
            width: popupWidth,
            modal: popupModal,
            resizable: popupResizable,
            buttons : popupButtons
     });

}



function limitChars(textid, limit) {
    var text = $('#' + textid).val();
    var textlength = text.length;

    if (textlength > limit) {
        $('#' + textid).val(text.substr(0, limit));
        return false;
    }
    else {
        return true;
    }
}

function getDate() {
    var currentTime = new Date()
    var month = currentTime.getMonth() + 1
    var day = currentTime.getDate()
    var year = currentTime.getFullYear()

    return month + "/" + day + "/" + year;
}


function select_nav() {
    var selected = window.location.href.split('/')[3];
    var setMenu = false;


    $.each($("#menu a"), function(i, val) {
        valHref = val.href.split('/')[3];
        if (valHref == selected) {
            $(val).addClass("selected");
            setMenu = true;
        }
    });

    if (setMenu == false) {
       
    }
}

function highlightTextInput() {

    $('input[type="text"]').focus(function() {
        $(this).addClass("focusField");
    });
    $('input[type="text"]').blur(function() {
        $(this).removeClass("focusField");
    });
}

function ieSafePreventDefault(e) {
    if (e.preventDefault) {
        e.preventDefault();
    } else {
        e.returnValue = false;
    }
}

function GetUserDefinedFields() {
    var landingPageTemplateID = $('#landingPageTemplateID').val();
    if (landingPageTemplateID > 0) {
        var landingPageID = $('#landingPageID').val();
        $.ajax({
            type: "GET",
            url: '/API/GetUserDefinedFields?landingPageID=' + landingPageID + '&landingPageTemplateID=' + landingPageTemplateID,
            dataType: 'html',
            success: function(result) {
                $('#userdefinedfields').html(result);
            }
        });
    }
}

function CheckLandingPageNameAvailability(existingID) {
    var name = $('#Name').val();
    $.ajax({
        type: "GET",
        url: '/API/IsLandingPageNameAvailable?name=' + name + "&existingID=" + existingID,
        dataType: 'html',
        success: function(result) {
            if (result == "True") {
                alert("The name '" + name + "' is available.");
            }
            else {
                alert("The name '" + name + "' is not available. Please choose another one.");
            }
        }
    });
}

function CheckTemplateNameAvailability(existingID) {
    var name = $('#Name').val();
    $.ajax({
        type: "GET",
        url: '/API/IsTemplateNameAvailable?name=' + name + "&existingID=" + existingID,
        dataType: 'html',
        success: function(result) {
            if (result == "True") {
                alert("The name '" + name + "' is available.");
            }
            else {
                alert("The name '" + name + "' is not available. Please choose another one.");
            }
        }
    });
}
//

function CheckURLAvailability(existingID) {
    var url = $('#URL').val();
    $.ajax({
        type: "GET",
        url: '/API/IsURLAvailable?url=' + url + "&existingID=" + existingID,
        dataType: 'html',
        success: function(result) {
            if (result == "True") {
                alert("The url '" + url + "' is available.");
            }
            else {
                alert("The url '" + url + "' is not available. Please choose another one.");
            }
        }
    });
}

var userDefinedInput = {
    add: function(e) {
        var self = e.target;
        var newFieldCount = $('.userDefinedFieldInput').length + 1;
        var newField =
            $('<input type="text" />')
                .attr({
                    'id': 'UserDefinedField' + newFieldCount,
                    'name': 'UserDefinedField' + newFieldCount,
                    'class': 'userDefinedFieldInput required',
                    'value': ''
                });
        newField = $("<li />")
         .append(newField)
         .append($('<a data-fieldnum="' + newFieldCount + '" class="deleteUserDefinedField" href="#"></a>'));
        newField.appendTo('#userDefinedFieldsContainer');

    },
    remove: function(e) {
        var self = e.target;
        $(self).parent().remove(); // remove the containing li
        this.renumberFields($(self).attr('data-fieldNum'), $('.userDefinedFieldInput').length);

    },
    renumberFields: function(fieldNumber, numFields) {
        var num = fieldNumber;
        var startElement = fieldNumber - 2;
        $('.userDefinedFieldInput:gt(' + startElement + ')').each(function() {
            $(this)
                .attr('id', 'UserDefinedField' + num)
                .attr('name', 'UserDefinedField' + num)
                .next().attr('data-fieldNum', num);
            num++;
        });
    }
}

function Preview(landingPageID) {
    window.open("/LandingPage/Preview?landingPageID=" + landingPageID);
}


/* Custom form validation methods */
jQuery.validator.addMethod(
  "selectNone",
  function(value, element) {
    if (element.value == 0)
    {
      return false;
    }
    else return true;
  },
  "<br /><span class='label_wrap'>Please select an option.</span>"
);

