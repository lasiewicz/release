﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet;
using Matchnet.Caching;
using Matchnet.Content.ValueObjects.LandingPage;
using Matchnet.Configuration.ServiceAdapters;

namespace LandingPageEngine.Models
{
    public class CachedLandingPages : ICacheable
    {
        private int _CacheTTLSeconds;
        private CacheItemPriorityLevel _CachePriority;
        private string _CacheKey;
        public List<TemplateBasedLandingPage> Pages {get;private set;}


        public CachedLandingPages(List<TemplateBasedLandingPage> pages)
        {
            Pages = pages;
            _CacheTTLSeconds = Conversion.CInt(RuntimeSettings.GetSetting("LANDING_PAGE_ENGINE_CACHE_TTL"));
            _CacheKey = Constants.LANDING_PAGES_CACHE_KEY;
            _CachePriority = CacheItemPriorityLevel.Normal;
        }

        #region ICacheable Members

        /// <summary>
        /// 
        /// </summary>
        public int CacheTTLSeconds
        {
            get
            {
                return _CacheTTLSeconds;
            }
            set
            {
                _CacheTTLSeconds = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemMode CacheMode
        {
            get
            {
                return CacheItemMode.Sliding;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Matchnet.CacheItemPriorityLevel CachePriority
        {
            get
            {
                return _CachePriority;
            }
            set
            {
                _CachePriority = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetCacheKey()
        {
            return _CacheKey;
        }

        /// <summary>
        /// Write-only accessor for setting the value of the ICacheable.GetCacheKey() attribute.
        /// </summary>
        public string SetCacheKey
        {
            set
            {
                _CacheKey = value;
            }
        }

        #endregion
    }
}
