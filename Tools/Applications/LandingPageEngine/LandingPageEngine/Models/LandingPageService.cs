﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.LandingPage;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Text;
using System.IO;
using Matchnet.Caching;
using Spark.Common.Adapter;
using System.Web.Script.Serialization;
using Matchnet.Configuration.ServiceAdapters;

namespace LandingPageEngine.Models
{
    public class LandingPageService
    {
        private Cache _cache;

        public LandingPageService()
        {
            _cache = Matchnet.Caching.Cache.Instance;
        }
        
        public string GetPageContentFromLandingPage(TemplateBasedLandingPage landingPage)
        {
            if (landingPage.Template.Definition.Contains("xsl:stylesheet"))
            {
                try
                {
                    XPathDocument userDefinedFields = new XPathDocument(new StringReader(UserDefinedFieldsToXML(landingPage.UserDefinedFields)));
                    XslCompiledTransform xslTrans = new XslCompiledTransform();
                    xslTrans.Load(XmlReader.Create(new StringReader(landingPage.Template.Definition)));
                    StringWriter sw = new StringWriter();

                    xslTrans.Transform(userDefinedFields, null, sw);
                    return HttpUtility.HtmlDecode(sw.ToString());
                }
                catch (Exception xmlException)
                {
                    string errorString = "There was an error parsing the XSL template definition";
                    if(landingPage != null) errorString = errorString + " ( landingPageID: " + landingPage.LandingPageID.ToString() + " )";
                    if (xmlException.InnerException != null && xmlException.InnerException is XmlException)
                    {
                        errorString = errorString + ": " + xmlException.InnerException.Message;
                    }
                    throw new Exception(errorString);
                }
            }
            else
            {
                return landingPage.Template.Definition;
            }
         }

        public TemplateBasedLandingPage GetLandingPageByURL(string url)
        {
            List<TemplateBasedLandingPage> pages = null;

            object cachedPages = _cache.Get(Constants.LANDING_PAGES_CACHE_KEY);
            if(cachedPages != null)
            {
                pages = ((CachedLandingPages)cachedPages).Pages;
            }
            else
            {
                pages = LandingPageSA.Instance.GetLandingPages(true);
                _cache.Add(new CachedLandingPages(pages));
            }
             
            return (from TemplateBasedLandingPage lp in pages where (lp.URL.ToLower() == url.ToLower() && lp.Published == true) select lp).FirstOrDefault();
        }

        #region GetUnifiedLandingPageID(int siteID, string theURL)
        /// <summary>
        /// returns a landing page id for the siteID and url passed to it http://mmservices.minglematch.com:9000/landing_page/url_to_id.php
        /// </summary>
        /// <param name="siteID">the id of the site e.g jdate 103</param>
        /// <param name="theURL">the url where its coming from e.g http%3A%2F%2Fwww.jdate.com/Applications/ContactUs/ContactUs.aspx</param>
        /// <returns></returns>
        public int? GetUnifiedLandingPageID(int siteID, string theURL)
        {
            int? uLPID = null;
            string webResponse = string.Empty;
            string mmServiceUrl = RuntimeSettings.GetSetting("MM_SERVICE_URL");

            WebPostRequest request = new WebPostRequest(mmServiceUrl, "POST", 2000);
            //the webpostrequest url encodes the value.
            request.Add(Constants.MM_SERVICE_SITEID_KEY, siteID.ToString());
            request.Add(Constants.MM_SERVICE_URL_KEY, theURL);
            
            try
            {
                webResponse = request.GetResponse();

                //we are getting back a JSON string
                var serializer = new JavaScriptSerializer();
                Dictionary<string, string> values = serializer.Deserialize<Dictionary<string, string>>(webResponse);

                bool success = Convert.ToBoolean(values[Constants.MM_SERVICE_RESPONSE_SUCCESS_KEY]);

                if (success)
                {
                    int gottenULPID = -1;
                    
                    bool isInt = int.TryParse(values[Constants.MM_SERVICE_RESPONSE_LPID_KEY], out gottenULPID);

                    if (isInt)
                        uLPID = gottenULPID;
                    else
                    {
                        string errorString = "LPID returned from MM_SERVICE_URL was not an integer";
                        errorString = errorString + ": " + values[Constants.MM_SERVICE_RESPONSE_ERROR_KEY];
                        throw new Exception(errorString);
                    }
                }
                else
                {
                    string errorString = "There was an error while trying to get LPID from MM_SERVICE_URL";
                    errorString = errorString + ": " + values[Constants.MM_SERVICE_RESPONSE_ERROR_KEY];
                    throw new Exception(errorString);
                }
            }
            catch (Exception ex)
            {
                string errorString = "There was an error while trying to get LPID from MM_SERVICE_URL";
                errorString = errorString + ": " + ex.Message;
                throw new Exception(errorString);
            }

            return uLPID;
        }
        #endregion

        private string UserDefinedFieldsToXML(List<UserDefinedField> fields)
        {
            StringBuilder xml = new StringBuilder();
            if (fields.Count > 0)
            {
                xml.Append("<UserDefinedFields>");
                foreach (UserDefinedField field in fields)
                {
                    xml.Append(string.Format(@"<Field Name=""{0}"" Value=""{1}""></Field>", field.Name, HttpUtility.HtmlEncode(field.Value)));
                }
                xml.Append("</UserDefinedFields>");
            }
            return xml.ToString();
        }





    }
}

