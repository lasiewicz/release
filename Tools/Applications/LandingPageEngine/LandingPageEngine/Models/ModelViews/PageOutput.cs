﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.LandingPage;

namespace LandingPageEngine.Models.ModelViews
{
    public class PageOutput
    {
        public string Output { get; set; }
        public TemplateBasedLandingPage Page { get; set; }
        public string OmnitureAccountName { get; set; }
        public string OmnitureInternalFilter { get; set; }
    }
}
