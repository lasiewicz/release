﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<LandingPageEngine.Models.ModelViews.PageOutput>" %>
<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    <%=Model.Page.Description %>
</asp:Content>
<asp:Content ID="indexHead" ContentPlaceHolderID="HeadContent" runat="server">
    <!-- 
	Host: <%= System.Environment.MachineName %>
	-->
    <meta name="description" content="<%=Model.Page.Tags %>" />
    <%Html.RenderPartial("Controls/Omniture", Model); %>
    
</asp:Content>
<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
    <%=Model.Output %>
</asp:Content>
