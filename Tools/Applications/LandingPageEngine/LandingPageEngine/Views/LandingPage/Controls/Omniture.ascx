﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<LandingPageEngine.Models.ModelViews.PageOutput>" %>
<script language="javascript" type="text/javascript">
    var s_account = "<%=Model.OmnitureAccountName %>";
</script>
<script type="text/javascript" src="http://jdate.com/Analytics/Javascript/Omniture.js"></script>
<script type="text/javascript">
function PopulateS(clearValue) {
    s.linkInternalFilters = "<%=Model.OmnitureInternalFilter %>";
    /*Traffic Variables*/
    s.pageName = "<%=Model.Page.AnalyticsPageName %>";
    s.campaign = (clearValue) ? "" : s.getQueryParam('prm');
    s.eVar30 = "<%=Model.Page.LandingPageID.ToString() %>";
    s.eVar38 = (clearValue) ? "" : s.getQueryParam('refcd');
    s.eVar39 = (clearValue) ? "" : s.getQueryParam('tsacr');
    s.eVar47 = "<%=Model.Page.AnalyticsPageName %>";
    s.eVar11 = (clearValue) ? "" : s.getQueryParam('prm');
    s.eVar12 = (clearValue) ? "" : s.getQueryParam('eid');
    s.eVar13 = (clearValue) ? "" : s.getQueryParam('lgid');
}

PopulateS(false);
/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
var s_code = s.t();if (s_code) document.write(s_code);
</script>

