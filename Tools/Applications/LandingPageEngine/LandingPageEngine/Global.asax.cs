﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Spark.CommonLibrary.Logging;

namespace LandingPageEngine
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

             routes.MapRoute(
                "Engine", 
                "LP/{url}",
                new { controller = "LandingPage", action = "Index", url = ""} 
            );
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            Logger.Configure(Server.MapPath("/") + "log4net.config");

            RegisterRoutes(RouteTable.Routes);
        }
    }
}