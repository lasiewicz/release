﻿#region

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using AdminTool.Models.Helpers;
using AdminTool.Models.ModelViews;
using AdminTool.Models.ModelViews.Approval;
using AdminTool.Models.ModelViews.Email;
using AdminTool.Models.ModelViews.Member;
using AdminTool.Models.ModelViews.Migration;
using AdminTool.Models.SparkAPI;
using AdminTool.Models.SparkAPI.EndpointClients;
using Matchnet;
using Matchnet.BulkMail.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Registration;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.MatchTest.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.PremiumServices.ServiceAdapter;
using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Session.ServiceAdapters;
using Spark.Common.AccessService;
using Spark.Common.Adapter;
using Spark.Common.AuthorizationService;
using Spark.Common.DiscountService;
using Spark.Common.FraudService;
using Spark.Common.OrderHistoryService;
using Spark.Common.PaymentProfileService;
using Spark.Common.RenewalService;
using Spark.CommonLibrary;
using Spark.CommonLibrary.Logging;
using Spark.SAL;
using Spark.UnifiedPurchaseSystem.Lib.Common;
using AdminActionLog = AdminTool.Models.ModelViews.Member.AdminActionLog;
using AuthorizationStatus = Spark.Common.AccessService.AuthorizationStatus;
using Constants = Matchnet.Constants;
using GiftType = Spark.Common.OrderHistoryService.GiftType;
using PaymentType = Spark.Common.PurchaseService.PaymentType;
using Purchase = Matchnet.Purchase.ValueObjects;
using ServiceConstants = Matchnet.PremiumServices.ValueObjects.ServiceConstants;

#endregion

namespace AdminTool.Models
{
    public class MemberManager : ManagerBase
    {
        private const string EDIT_SEARCH_PREF_REGION_ID_FORM_FIELD = "editSearchPreferencesRegionPicker";
        private const string EDIT_SEARCH_PREF_COUNTRY_REGION_ID_FORM_FIELD = "editSearchCountryRegionID";
        private const string EDIT_SEARCH_PREF_SEARCH_TYPE_FORM_FIELD = "editSearchSearchType";
        private const string EDIT_PROFILE_REGION_ID_FORM_FIELD = "editProfileRegionPicker";
        private const string EDIT_PROFILE_GENDER_PICKER_FORM_FIELD = "editProfileGenderPicker";
        private const string EDIT_SEARCH_PREF_GENDER_PICKER_FORM_FIELD = "editSearchGenderPicker";
        private static readonly Logger _logger = new Logger(MethodBase.GetCurrentMethod().DeclaringType);

        #region Properties

        public int MemberID { get; set; }
        public SiteIDs SiteID { get; set; }

        public Member Member
        {
            get { return _Member; }
        }

        public MemberSiteInfo MemberSiteInfo
        {
            get { return _MemberSiteInfo; }
        }

        // Temporary use for getting the default payment profile
        public string DefaultPaymentProfileID { get; set; }
        public int OrderIDForDefaultPaymentProfile { get; set; }

        #endregion

        #region Constructor

        public MemberManager(int memberID, int siteID, MemberLoadFlags memberLoadFlags, bool isMigratedMember = false)
        {
            try
            {
                DefaultPaymentProfileID = string.Empty;
                MemberID = memberID;
                SiteID = (SiteIDs) Enum.Parse(typeof (SiteIDs), siteID.ToString());
                _Member = isMigratedMember ?
                    MemberHelper.GetMigratedMember(memberID) : MemberHelper.GetMember(memberID, memberLoadFlags);

                if (_Member == null)
                {
                    throw new Exception("Member not found: " + memberID);
                }
                _MemberSiteInfo = MemberHelper.GetMemberSiteInfo(_Member, siteID);
                timingHelper = new TimingHelper();
            }
            catch (Exception ex)
            {
                throw new Exception("Error in MemberManager.MemberManager().", ex);
            }
        }

        #endregion

        private readonly Member _Member;
        private readonly MemberSiteInfo _MemberSiteInfo;
        private TimingHelper timingHelper;

        public EditProfileData GetEditProfileData(int controlType)
        {
            var data = new EditProfileData();
            data.MemberSiteInfo = _MemberSiteInfo;
            data.MemberID = MemberID;
            data.ControlType = controlType;

            var brand = g.GetBrand((int) SiteID);

            MemberHelper.PopulateEditProfileDataFromMember(_Member, brand, controlType, data, null, EDIT_PROFILE_REGION_ID_FORM_FIELD, EDIT_PROFILE_GENDER_PICKER_FORM_FIELD);

            return data;
        }

        public MemberDataForPrint GetMemberDataForPrint()
        {
            var b = g.GetBrand((int) SiteID);

            var mdp = new MemberDataForPrint();
            mdp.MDPBrandInfo = b.Site.Name;
            mdp.TheSiteID = (int) SiteID;
            mdp.MemberID = _Member.MemberID;
            mdp.MDPEmailAddress = _Member.EmailAddress;
            mdp.FirstName = _Member.GetAttributeText(b, "SiteFirstName");
            mdp.LastName = _Member.GetAttributeText(b, "SiteLastName");
            mdp.UserName = _Member.GetUserName(b);
            mdp.ProfileStatus = MemberHelper.GetProfileStatus(_Member, b);
            mdp.Notes = GetAdminNotesForPrint();
            mdp.ProfileInfo = GetProfileInfo();
            mdp.HasPhoto = _Member.HasAnyApprovedPhoto(b.Site.Community.CommunityID);
            mdp.LastLogons = GroupLastLogins(_Member.GetMemberLastLogons(g.GetBrand((int) SiteID).BrandID, MemberLoadFlags.IngoreSACache));
            MemberHelper.PopulateMemberDataForPrint(_Member, b, mdp);


            // sent emails 
            var mailLog = EmailLogSA.Instance.RetrieveEmailLog(MemberID, b.Site.Community.CommunityID);
            var emails = new List<Email>();
            foreach (EmailLogEntry entry in mailLog.Sent)
            {
                emails.Add(new Email(entry.MailID, (MailType) entry.MailType, entry.InsertDate,
                    MemberHelper.GetMemberUsername(entry.FromMemberID, b),
                    MemberHelper.GetMemberUsername(entry.ToMemberID, b),
                    entry.FromMemberID, entry.ToMemberID, (entry.MailOption & (int) MailOption.VIP) == (int) MailOption.VIP));
            }

            mdp.AllSentEmails = emails;

            return mdp;
        }

        private List<MemberDataForPrint.LoginData> GroupLastLogins(List<LastLogon> lastLogons)
        {
            var loginDatas = new List<MemberDataForPrint.LoginData>();

            if (lastLogons != null)
            {
                var currentDate = DateTime.Now;
                MemberDataForPrint.LoginData loginData = null;

                var ls = lastLogons.GroupBy(r => new {r.LastLogonDate.Date})
                    .Select(r => new {r.Key.Date, RCOUNT = r.Count()}).ToList();

                if (ls.Count > 0)
                {
                    foreach (var a in ls.ToArray())
                    {
                        loginData = new MemberDataForPrint.LoginData();
                        loginData.Login = a.Date;
                        loginData.count = a.RCOUNT;
                        loginDatas.Add(loginData);
                    }
                }
            }


            return loginDatas;
        }

        private List<MemberDataForPrint.AdminActionNotesMiniForPrint> GetAdminNotesForPrint()
        {
            var notes = new List<MemberDataForPrint.AdminActionNotesMiniForPrint>();

            var aacc = AdminSA.Instance.GetAdminActivity(_Member.MemberID, g.GetBrand((int) SiteID).Site.Community.CommunityID);

            MemberDataForPrint.AdminActionNotesMiniForPrint aan = null;

            if (aacc == null)
            {
                _logger.Debug("GetAdminNotesForPrint - aacc is null");
            }
            else
            {
                _logger.Debug("GetAdminNotesForPrint - aacc count: " + aacc.Count);
                foreach (AdminActivityContainer aac in aacc)
                {
                    aan = new MemberDataForPrint.AdminActionNotesMiniForPrint();

                    if (aac == null)
                    {
                        _logger.Debug("GetAdminNotesForPrint - aac is null");
                    }
                    else if (aac.AdminNote == null)
                    {
                        _logger.Debug("GetAdminNotesForPrint - aac AdminNote is null");
                    }
                    else if (aac.Type == ActivityType.Note)
                    {
                        aan.Note = aac.AdminNote.Note;
                        aan.InsertDate = aac.AdminNote.InsertDate;
                        notes.Add(aan);
                    }
                }
            }

            return notes;
        }

        private string GetFormElementName(DataType dataType, int attributeID, string attributeName)
        {
            if (attributeName.ToLower() == "regionid")
                return EDIT_PROFILE_REGION_ID_FORM_FIELD;

            if (attributeName.ToLower() == "gendermask")
                return EDIT_PROFILE_GENDER_PICKER_FORM_FIELD;

            var prefix = string.Empty;
            switch (dataType)
            {
                case DataType.FreeText:
                    prefix = "sft_";
                    break;
                case DataType.NumberText:
                    prefix = "snt_";
                    break;
                case DataType.DateTime:
                    prefix = "sdt_";
                    break;
                case DataType.Boolean:
                    prefix = "sbcb_";
                    break;
                case DataType.SingleSelect:
                    prefix = "sss_";
                    break;
                case DataType.MultiSelect:
                    prefix = "sms_";
                    break;
            }

            return prefix + attributeID;
        }

        public EditProfileData UpdateMemberProfileAttributes(FormCollection formCollection, int controlType)
        {
            var updateUsername = false;
            var data = new EditProfileData();
            data.MemberSiteInfo = _MemberSiteInfo;
            data.MemberID = MemberID;
            data.ControlType = controlType;

            var brand = g.GetBrand((int) SiteID);

            var metaData = new ProfileAttributeMetadataCollection();
            metaData.GetMetadata(controlType, brand);

            // prepare to update the member object
            // if the user submitted blank string for attributes represented by textboxes, wipe the user attribute value
            foreach (var aMeta in metaData.ProfileAttributeMetadataCol)
            {
                var formElementName = GetFormElementName(aMeta.DataType, aMeta.AttributeID, aMeta.AttributeName);
                if (formCollection[formElementName] != null)
                {
                    switch (aMeta.DataType)
                    {
                        case DataType.FreeText:
                            var value = GeneralHelper.RemoveHtmlTags(formCollection[formElementName]);
                            _Member.SetAttributeText(brand, aMeta.AttributeID, value, TextStatusType.Human);
                            if (aMeta.AttributeName.ToLower() == "username")
                            {
                                updateUsername = value != _Member.GetUserName(brand);
                            }
                            break;
                        case DataType.NumberText:
                            int numberTextValue;

                            if (formCollection[formElementName] == string.Empty)
                            {
                                _Member.SetAttributeInt(brand, aMeta.AttributeID, Constants.NULL_INT);
                            }
                            else
                            {
                                var parsedInt = int.TryParse(formCollection[formElementName], out numberTextValue);
                                if (parsedInt)
                                {
                                    _Member.SetAttributeInt(brand, aMeta.AttributeID, numberTextValue);
                                }
                            }
                            break;
                        case DataType.DateTime:
                            DateTime dateValue;

                            if (formCollection[formElementName] == string.Empty)
                            {
                                _Member.SetAttributeDate(brand, aMeta.AttributeID, DateTime.MinValue);
                            }
                            else
                            {
                                var parsedDate = DateTime.TryParse(formCollection[formElementName], out dateValue);
                                if (parsedDate)
                                {
                                    _Member.SetAttributeDate(brand, aMeta.AttributeID, dateValue);
                                }
                            }
                            break;
                        case DataType.Boolean:
                            var boolValue = Convert.ToBoolean(formCollection[formElementName]);
                            _Member.SetAttributeInt(brand, aMeta.AttributeID, boolValue ? 1 : 0);
                            break;
                        case DataType.SingleSelect:
                            if (aMeta.AttributeName.ToLower() == "regionid")
                            {
                                // set the region data
                                int regionIDValue;
                                var regionParsed = int.TryParse(formCollection[formElementName], out regionIDValue);

                                if (regionParsed && regionIDValue > 0)
                                {
                                    _Member.SetAttributeInt(brand, aMeta.AttributeID, regionIDValue);
                                }
                            }
                            else
                            {
                                int selectedValue;
                                var singleSelParsed = int.TryParse(formCollection[formElementName], out selectedValue);

                                if (singleSelParsed)
                                {
                                    _Member.SetAttributeInt(brand, aMeta.AttributeID, selectedValue);
                                }
                            }
                            break;
                        case DataType.MultiSelect:
                            if (aMeta.AttributeName.ToLower() == "gendermask")
                            {
                                int genderMaskValue;

                                if (int.TryParse(formCollection[formElementName], out genderMaskValue))
                                {
                                    _Member.SetAttributeInt(brand, aMeta.AttributeID, genderMaskValue);
                                }
                            }
                            else
                            {
                                var selections = formCollection[formElementName];
                                var splits = selections.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries);

                                var multiSelectedValues = 0;

                                if (splits != null && splits.Count() > 0)
                                {
                                    for (var i = 0; i < splits.Count(); i++)
                                    {
                                        multiSelectedValues |= int.Parse(splits[i]);
                                    }
                                }

                                _Member.SetAttributeInt(brand, aMeta.AttributeID, multiSelectedValues);
                            }
                            break;
                    }
                }
                else
                {
                    // if a form element didn't post when we know it exists on the form means that the value got wiped
                    var testid = 0;
                    switch (aMeta.DataType)
                    {
                        case DataType.MultiSelect:
                            testid = aMeta.AttributeID;
                            _Member.SetAttributeInt(brand, aMeta.AttributeID, 0);
                            break;
                    }
                }
            }

            // save member
            //Use overloaded Save Member method to update username - this avoids storing duplicate username
            if (updateUsername)
                MemberSA.Instance.SaveMember(_Member, brand.Site.Community.CommunityID);
            else
                MemberSA.Instance.SaveMember(_Member);

            // rebind the view with new data
            MemberHelper.PopulateEditProfileDataFromMember(_Member, brand, controlType, data, metaData, EDIT_PROFILE_REGION_ID_FORM_FIELD, EDIT_PROFILE_GENDER_PICKER_FORM_FIELD);

            return data;
        }

        public MemberData GetMemberData()
        {
            var memberData = new MemberData();
            bool hasPremiumServices;
            memberData.MemberID = _Member.MemberID;
            memberData.MemberSiteInfo = _MemberSiteInfo;

            //partial view models
            memberData.TopNav = GetTopNav();
            memberData.MemberLeft = GetMemberLeft();
            memberData.BasicUserInfo = GetBasicUserInfo();
            memberData.GlobalProfile = GetGlobalProfile();
            memberData.DateTimeStampInfo = GetDateTimeStampInfo();
            memberData.AdminActionLog = GetAdminActionLog();
            memberData.SubscriptionStatusInfo = GetSubscriptionStatusInfo();
            memberData.PauseSubscriptionInfo = GetPauseSubscriptionInfo(memberData.SubscriptionStatusInfo.HasPausedSubscription, memberData.SubscriptionStatusInfo.IsPauseAllowed);
            memberData.CommunicationHistory = GetCommunicationHistory();
            memberData.FraudAbuseInfo = GetFraudAbuseInfo();
            memberData.OtherActionInfo = GetOtherActionInfo();
            memberData.ProfileInfo = GetProfileInfo();
            memberData.OffSiteNotifications = GetOffSiteNotifications();
            memberData.PremiumServiceSettings = GetPremiumServiceSettings(out hasPremiumServices);
            memberData.AdminAdjustInfo = GetAdminAdjustInfo();
            memberData.RenewalInfo = GetRenewalInfo();
            memberData.ProfileDisplaySettings = GetProfileDisplaySettings();
            memberData.TopNav.ShowEditPremiumServicesLink = hasPremiumServices;
            memberData.JMeterEditable = GetJMeterEditable();
            memberData.IlAuthenticated = GetILAuthenticated();
            memberData.LastFewClientIPs = GetLastFewClientIPs();

            return memberData;
        }

        private LastFewClientIPs GetLastFewClientIPs()
        {
            var clientIPs = new LastFewClientIPs();
            clientIPs.SessionTrackings = SessionSA.Instance.GetLastFewSessionTrackingEntries(_Member.MemberID, (int) _MemberSiteInfo.SiteID);

            return clientIPs;
        }

        private ILAuthenticated GetILAuthenticated()
        {
            var ilAuthenticated = new ILAuthenticated();
            ilAuthenticated.MemberID = _Member.MemberID;
            ilAuthenticated.MemberEmail = _Member.EmailAddress;
            var brand = g.GetBrand((int) _MemberSiteInfo.SiteID);
            ilAuthenticated.Username = _Member.GetUserName(brand);
            ilAuthenticated.MemberSiteInfo = _MemberSiteInfo;

            var blnAuthenticated = MemberHelper.IsMemberPremiumAuthenticated(_Member, brand);
            var dteExpirationDate = _Member.GetAttributeDate(brand, "PremiumAuthenticatedExpirationDate", DateTime.MinValue);

            if (blnAuthenticated)
            {
                // Member is authenticated and the PremiumAuthenticatedExpirationDate
                // attribute date has not yet expired  
                ilAuthenticated.ExpirationDate = dteExpirationDate;
                ilAuthenticated.Status = "Currently ACTIVE";
            }
            else if (!blnAuthenticated && dteExpirationDate != DateTime.MinValue)
            {
                // Member no longer is authenticated and the PremiumAuthenticatedExpirationDate
                // attribute date has expired                           
                ilAuthenticated.ExpirationDate = dteExpirationDate;
                ilAuthenticated.Status = "Currently INACTIVE";
            }
            else
            {
                ilAuthenticated.Status = "Currently INACTIVE";
            }

            return ilAuthenticated;
        }

        private JMeterEditable GetJMeterEditable()
        {
            var jMeterEditable = new JMeterEditable();
            jMeterEditable.MemberID = _Member.MemberID;
            jMeterEditable.MemberSiteInfo = _MemberSiteInfo;

            return jMeterEditable;
        }

        public BasicUserInfo GetBasicUserInfo()
        {
            var basicUserInfo = new BasicUserInfo();
            basicUserInfo.MemberID = _Member.MemberID;
            basicUserInfo.MemberSiteInfo = _MemberSiteInfo;
            basicUserInfo.Email = _Member.EmailAddress;
            basicUserInfo.FirstName = _Member.GetAttributeText(g.GetBrand((int) SiteID), "SiteFirstName");
            basicUserInfo.LastName = _Member.GetAttributeText(g.GetBrand((int) SiteID), "SiteLastName");
            basicUserInfo.UserName = _Member.GetUserName(g.GetBrand((int) SiteID));
            //basicUserInfo.ProfileStatus = ((_Member.GetAttributeInt(g.GetBrand((int)SiteID), "HideMask") & (Int32)AdminTool.Models.Enums.AttributeOptionHideMask.HideSearch) == (Int32)AdminTool.Models.Enums.AttributeOptionHideMask.HideSearch) ? "Hidden" : "Active";
            basicUserInfo.ProfileStatus = MemberHelper.GetProfileStatus(_Member, g.GetBrand((int) SiteID));
            basicUserInfo.IsAdmin = MemberPrivilegeSA.Instance.IsAdminByPrivilegesOnly(Member.MemberID);
            if (basicUserInfo.ProfileStatus == Enums.ProfileStatus.AdminSuspend)
            {
                //TODO - Talk to Mishkoff about getting the new admin suspend reason
                basicUserInfo.SuspendReason = "";
            }
            basicUserInfo.LastEmails = _Member.GetMemberLastEmail(g.GetBrand((int) SiteID).BrandID, MemberLoadFlags.IngoreSACache);

            //norbert
            basicUserInfo.IsNorbert = (_Member.GetAttributeInt(g.GetBrand((int)SiteID), "REDRedesignBetaParticipatingFlag") == 1);

            return basicUserInfo;
        }

        public GlobalProfile GetGlobalProfile()
        {
            var globalStatusMask = _Member.GetAttributeInt(g.GetBrand((int) SiteID), "GlobalStatusMask");
            var adminSuspended = (globalStatusMask & WebConstants.GLOBALSTATUSMASK_ADMIN_SUSPEND) == WebConstants.GLOBALSTATUSMASK_ADMIN_SUSPEND;
            var lastSuspendedReasonID = MemberHelper.GetLastSuspendedReasonID(_Member);
            var globalProfile = new GlobalProfile(adminSuspended, lastSuspendedReasonID);
            globalProfile.MemberID = _Member.MemberID;
            globalProfile.MemberSiteInfo = _MemberSiteInfo;
            globalProfile.BrandID = _Member.GetAttributeInt(Constants.NULL_INT, (int) _MemberSiteInfo.SiteID, Constants.NULL_INT, "LastBrandID", Constants.NULL_INT);
            globalProfile.MemberEmail = _Member.EmailAddress;

            var dneEntry = DoNotEmailSA.Instance.GetEntryBySiteAndEmailAddress((int) SiteID, _Member.EmailAddress);

            globalProfile.BadEmail = (globalStatusMask & WebConstants.GLOBALSTATUSMASK_BAD_EMAIL_SUSPEND) == WebConstants.GLOBALSTATUSMASK_BAD_EMAIL_SUSPEND;
            globalProfile.EmailVerified = (globalStatusMask & WebConstants.GLOBALSTATUSMASK_EMAIL_VERIFIED) == WebConstants.GLOBALSTATUSMASK_EMAIL_VERIFIED;
            globalProfile.OnDNE = (dneEntry != null);

            return globalProfile;
        }

        public ProfileInfo GetProfileInfo()
        {
            var brand = g.GetBrand((int) _MemberSiteInfo.SiteID);

            var profile = new ProfileInfo();
            profile.MemberID = _Member.MemberID;
            profile.MemberSiteInfo = _MemberSiteInfo;
            profile.FirstName = _Member.GetAttributeText(brand, "SiteFirstName");
            profile.LastName = _Member.GetAttributeText(brand, "SiteLastName");
            profile.BirthDate = _Member.GetAttributeDate(brand, "Birthdate");
            profile.Gender = MemberHelper.GetMemberGender(_Member, brand);
            profile.SeekingGender = MemberHelper.GetSeekingGender(_Member, brand);
            profile.RegionDisplay = MemberHelper.GetRegionDisplayString(_Member, brand);
            return profile;
        }

        public TopNav GetTopNav()
        {
            var topNav = new TopNav();
            topNav.MemberID = _Member.MemberID;
            topNav.MemberSiteInfo = _MemberSiteInfo;
            topNav.BrandID = g.GetBrand((int) SiteID).BrandID;
            topNav.Email = _Member.EmailAddress;
            if (RuntimeSettings.GetSetting("ENABLE_ADMIN_TOOL_PHOTO_UPLOAD").ToLower() == "true")
            {
                topNav.UseAdminToolPhotoUpload = true;
            }

            return topNav;
        }

        public AdminActionLog GetAdminActionLog()
        {
            var actionLog = new AdminActionLog();
            var activityContainerColleciton = AdminSA.Instance.GetAdminActivity(_Member.MemberID, g.GetBrand((int) SiteID).Site.Community.CommunityID);

            actionLog.MemberID = _Member.MemberID;
            actionLog.MemberSiteInfo = _MemberSiteInfo;
            actionLog.ActivityContainerCollection = activityContainerColleciton;

            return actionLog;
        }

        public MemberLeft GetMemberLeft()
        {
            var memberLeft = new MemberLeft();
            var brand = g.GetBrand((int) SiteID);

            memberLeft.MemberID = _Member.MemberID;
            memberLeft.MemberSiteInfo = _MemberSiteInfo;
            memberLeft.PhotoURL = MemberPhotoHelper.GetDefaultPhotoThumbnailDisplayURL(_Member, brand);

            //all member sites
            memberLeft.OtherMemberSites = MemberHelper.GetMemberSiteInfoList(_Member);

            //promotion attributes
            var prm = _Member.GetAttributeInt(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, "PromotionID", Constants.NULL_INT);
            if (prm != Constants.NULL_INT && prm > 0)
            {
                memberLeft.PRM = prm.ToString();
            }

            var landingPageID = _Member.GetAttributeInt(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, "LandingPageID", Constants.NULL_INT);
            if (landingPageID != Constants.NULL_INT && landingPageID > 0)
            {
                memberLeft.LandingPageID = landingPageID.ToString();
            }

            var bannerID = _Member.GetAttributeInt(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, "BannerID", Constants.NULL_INT);
            if (bannerID != Constants.NULL_INT && bannerID > 0)
            {
                memberLeft.BannerID = bannerID.ToString();
            }

            memberLeft.LuggageID = _Member.GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, brand.Site.LanguageID, "Luggage", Constants.NULL_STRING);

            var userAgent = _Member.GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, brand.Site.LanguageID, "UserAgent", Constants.NULL_STRING);
            var userFlashVersion = _Member.GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, brand.Site.LanguageID, "UserFlashVersion", Constants.NULL_STRING);
            var userInformationCaptured = _Member.GetAttributeDate(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, "UserCapabilitiesCaptured", DateTime.MinValue);

            memberLeft.UserCapabilities = new UserCapabilities(userAgent, userFlashVersion, userInformationCaptured);

            return memberLeft;
        }

        public TransactionHistory GetTransactionHistory()
        {
            var transactionHistory = new TransactionHistory();
            transactionHistory.MemberID = _Member.MemberID;
            transactionHistory.MemberSiteInfo = _MemberSiteInfo;

            var colAccountHistory = new List<AccountHistory>();
            // For the non financial account history

            #region OLD - MemberTran

            //timingHelper.AddTask("getmtc");
            //timingHelper.StartTaskTiming("getmtc");
            //Matchnet.Purchase.ValueObjects.MemberTranCollection mtc = PurchaseSA.Instance.GetMemberTransactions(this.MemberID, (int)this.SiteID);
            ////timingHelper.StopTaskTiming("getmtc");
            //if (mtc != null)
            //{
            //    foreach (Matchnet.Purchase.ValueObjects.MemberTran memberTran in mtc)
            //    {
            //        if ((memberTran.TranType == Matchnet.Purchase.ValueObjects.TranType.AdministrativeAdjustment
            //                || memberTran.TranType == Matchnet.Purchase.ValueObjects.TranType.ReopenAutoRenewal
            //                || memberTran.TranType == Matchnet.Purchase.ValueObjects.TranType.Termination
            //                || memberTran.TranType == Matchnet.Purchase.ValueObjects.TranType.TrialPayAdjustment)
            //            && (memberTran.Amount == 0))
            //        {
            //            AccountHistory accountHistory = new AccountHistory(memberTran, g.GetBrand((int)this.SiteID).BrandID, AccountHistoryType.NonFinancial);
            //            colAccountHistory.Add(accountHistory);
            //        }
            //    }
            //}

            #endregion

            // For Non-Financials: Terminations/Reopen-AutoRenewals
            var renewalTrans = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetRenewalTransactionHistory(MemberID, (int) SiteID, 1, 100);

            //if (renewalTrans != null)
            //{
            //    System.Diagnostics.EventLog.WriteEntry("AdminTool", string.Format("Number of renewal transactions: {0}", Convert.ToString(renewalTrans.Length)));
            //}
            //else
            //{
            //    System.Diagnostics.EventLog.WriteEntry("AdminTool", "Number of renewal transactions: 0");
            //}          

            if (renewalTrans != null && renewalTrans.Length > 0)
            {
                foreach (var tran in renewalTrans)
                {
                    var accountHistory = new AccountHistory(tran, g.GetBrand((int) SiteID), AccountHistoryType.NonFinancial, _Member);
                    colAccountHistory.Add(accountHistory);
                }
            }

            // Freezing and unfreezing transaction history 
            var frozenRenewalTrans = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetRenewalFrozenTransactionHistory(MemberID, (int) SiteID, 1, 100);

            //if (frozenRenewalTrans != null)
            //{
            //    System.Diagnostics.EventLog.WriteEntry("AdminTool", string.Format("Number of frozen subscription transactions: {0}", Convert.ToString(frozenRenewalTrans.Length)));
            //}
            //else
            //{
            //    System.Diagnostics.EventLog.WriteEntry("AdminTool", "Number of frozen subscription transactions: 0");
            //}

            if (frozenRenewalTrans != null && frozenRenewalTrans.Length > 0)
            {
                foreach (var tran in frozenRenewalTrans)
                {
                    var accountHistory = new AccountHistory(tran, g.GetBrand((int) SiteID), AccountHistoryType.NonFinancial, _Member);
                    colAccountHistory.Add(accountHistory);
                }
            }

            // For Non-Financials: Admin Adjustments, TrialPay Adjustments
            var accessTrans = AccessServiceWebAdapter.GetProxyInstanceForBedrock().GetAccessTransactionHistory(MemberID, (int) SiteID, 1, 100);
            if (accessTrans != null && accessTrans.Length > 0)
            {
                foreach (var tran in accessTrans)
                {
                    AccountHistory accountHistory = null;

                    if (tran.UnifiedTransactionTypeID == (int) Spark.Common.AccessService.TransactionType.GiftRedeem)
                    {
                        //this is a giftredemption, get the gift record.
                        var redeemedGift = getRedeemedGift(tran.AccessTransactionID);
                        accountHistory = new AccountHistory(tran, g.GetBrand((int) SiteID), _Member, redeemedGift);
                        DefaultPaymentProfileCache.Add(Member.MemberID, (int) _MemberSiteInfo.SiteID, redeemedGift.GiftRedemption.UserPaymentGUID);
                    }
                    else
                    {
                        accountHistory = new AccountHistory(tran, g.GetBrand((int) SiteID), AccountHistoryType.NonFinancial, _Member);

                        if (tran.UnifiedTransactionTypeID == (int) TransactionType.Authorization)
                        {
                            var authorizationSubscription = AuthorizationServiceWebAdapter.GetProxyInstance().GetAuthorizationSubscriptionByAccessTransactionID(tran.AccessTransactionID);
                            if (authorizationSubscription != null)
                            {
                                accountHistory.PromoID = authorizationSubscription.PromoID;
                                accountHistory.PrimaryPackageID = authorizationSubscription.PrimaryPackageID;
                                accountHistory.PackageIDList = Convert.ToString(accountHistory.PrimaryPackageID);
                                accountHistory.AuthorizationPaymentID = authorizationSubscription.AuthorizationChargeID;
                                accountHistory.UserPaymentGUID = authorizationSubscription.PaymentProfileID;
                            }
                        }
                    }

                    colAccountHistory.Add(accountHistory);
                }
            }

            // for the financial account history
            //List<OrderInfo> colOrderInfo = new List<OrderInfo>();
            //timingHelper.AddTask("getorderhistory");
            //timingHelper.StartTaskTiming("getorderhistory");
            var arrOrderInfo = OrderHistoryServiceWebAdapter.GetProxyInstanceForBedrock().GetMemberOrderHistoryByMemberID(MemberID, (int) SiteID, 5000, 1);

            // Temporary way of getting the default payment profile
            var paymentProfileResponse = PaymentProfileServiceWebAdapter.GetProxyInstance().GetMemberDefaultPaymentProfile(MemberID, (int) SiteID);
            //timingHelper.StopTaskTiming("getorderhistory");

            foreach (var orderInfo in arrOrderInfo)
            {
                if (orderInfo.OrderDetail.Length > 0)
                {
                    // Only add orders that have order details  
                    var accountHistory = new AccountHistory(orderInfo, g.GetBrand((int) SiteID), AccountHistoryType.Financial, _Member);
                    accountHistory.FraudDetailedResponse = MemberHelper.GetFraudDetailedResponse(MemberID, (int)SiteID, 1, orderInfo.UserPaymentGuid);
                    
                    var gift = getGiftForOrder(orderInfo);

                    if (gift != null)
                    {
                        accountHistory.AddPuchasedGift(gift);
                    }

                    colAccountHistory.Add(accountHistory);

                    if (paymentProfileResponse != null && orderInfo.UserPaymentGuid == paymentProfileResponse.PaymentProfileID)
                    {
                        DefaultPaymentProfileCache.Add(Member.MemberID, (int) _MemberSiteInfo.SiteID, orderInfo.UserPaymentGuid);
                    }
                }
            }

            var sortedAccountHistoryList = GetSortedAccountHistory(colAccountHistory);
            //System.Diagnostics.EventLog.WriteEntry("AdminTool", string.Format("Number of all transactions: {0}", Convert.ToString(sortedAccountHistoryList.Count)));

            if (colAccountHistory.Count > 0)
            {
                //determine promoID for terminations and auto-renewal reopens
                foreach (var ahistory in sortedAccountHistoryList)
                {
                    if (ahistory.TransactionType == TransactionType.AutoRenewalTerminate
                        || ahistory.TransactionType == TransactionType.AutoRenewalReopen)
                    {
                        //get promo id associated with most recent initial purchase
                        var query = (from AccountHistory ah in sortedAccountHistoryList
                            where ah.TransactionType == TransactionType.InitialSubscriptionPurchase
                                  && ah.OrderStatus == OrderStatus.Successful
                                  && ah.PrimaryPackageID == ahistory.PrimaryPackageID
                            orderby ah.InsertDateInPST descending
                            select ah).Take(1);

                        foreach (var AccountHistory in query)
                        {
                            ahistory.PromoID = AccountHistory.PromoID;
                        }
                    }
                }
            }
            transactionHistory.AllAccountHistory = sortedAccountHistoryList;

            // For the account history with PromoId, let's add the promo description to it
            foreach (var acctH in transactionHistory.AllAccountHistory)
            {
                if (acctH.PromoID > 0)
                {
                    var brand = g.GetBrand(acctH.CallingSystemID);
                    var promo = brand != null
                        ? PromoEngineSA.Instance.GetPromoByID(
                            acctH.PromoID, brand.BrandID, false)
                        : null;


                    acctH.PromoDescription = promo != null ? promo.PageTitle : string.Empty;
                }
            }

            return transactionHistory;
        }

        private Gift getGiftForOrder(OrderInfo order)
        {
            Gift gift = null;

            foreach (var orderDetail in order.OrderDetail)
            {
                if (orderDetail.GiftTypeID != GiftType.None)
                {
                    gift = DiscountServiceWebAdapter.GetProxyInstanceForBedrock().GetGiftByOrder(order.OrderID);
                }
            }

            return gift;
        }

        private Gift getRedeemedGift(int accesstransactionID)
        {
            Gift gift = null;

            try
            {
                gift = DiscountServiceWebAdapter.GetProxyInstanceForBedrock().GetGiftByAccessTransactionID(accesstransactionID);
            }
            finally
            {
            }

            return gift;
        }

        public BillingInfo GetDefaultBillingInfo()
        {
            var billingInfoModel = new BillingInfo();
            billingInfoModel.UsedForDefaultPaymentProfile = true;
            billingInfoModel.MemberSiteInfo = _MemberSiteInfo;


            //billingInfoModel.OrderID = orderID;

            //PaymentProfileInfo paymentProfileResponse = Spark.Common.Adapter.PaymentProfileServiceWebAdapter.GetProxyInstance().GetMemberDefaultPaymentProfile(this.MemberID, (int)this.SiteID);
            var defaultPaymentProfileID = DefaultPaymentProfileCache.GetPaymentProfileID(_MemberSiteInfo.MemberID,
                (int) _MemberSiteInfo.SiteID);

            var paymentProfileResponse = PaymentProfileServiceWebAdapter.GetProxyInstance().GetObsfucatedPaymentProfileByUserPaymentID(defaultPaymentProfileID, (int) SiteID);

            if (paymentProfileResponse != null
                && paymentProfileResponse.Code == "0"
                && paymentProfileResponse.PaymentProfile != null)
            {
                if (paymentProfileResponse.PaymentProfile is ObsfucatedCreditCardPaymentProfile)
                {
                    billingInfoModel.PaymentType = PaymentType.CreditCard;
                    var creditCardPaymentProfile = (ObsfucatedCreditCardPaymentProfile) paymentProfileResponse.PaymentProfile;
                    billingInfoModel.PaymentProfileId = creditCardPaymentProfile.PaymentProfileID.ToString();
                    billingInfoModel.CardType = creditCardPaymentProfile.CardType;
                    billingInfoModel.City = creditCardPaymentProfile.City;
                    billingInfoModel.Country = creditCardPaymentProfile.Country;
                    billingInfoModel.ExpMonth = creditCardPaymentProfile.ExpMonth;
                    billingInfoModel.ExpYear = creditCardPaymentProfile.ExpYear;
                    billingInfoModel.FirstName = creditCardPaymentProfile.FirstName;
                    billingInfoModel.LastFourDigitsCreditCardNumber = creditCardPaymentProfile.LastFourDigitsCreditCardNumber;
                    billingInfoModel.LastName = creditCardPaymentProfile.LastName;
                    billingInfoModel.PhoneNumber = creditCardPaymentProfile.PhoneNumber;
                    billingInfoModel.PostalCode = creditCardPaymentProfile.PostalCode;
                    billingInfoModel.State = creditCardPaymentProfile.State;
                    billingInfoModel.StreetAddress = creditCardPaymentProfile.StreetAddress;
                    billingInfoModel.GovernmentIssuedID = creditCardPaymentProfile.GovernmentIssuedID;
                }
                else if (paymentProfileResponse.PaymentProfile is ObfuscatedCreditCardShortFormMobileSitePaymentProfile)
                {
                    billingInfoModel.PaymentType = PaymentType.CreditCardShortFormMobileSite;
                    var creditCardShortFormMobileSitePaymentProfile = (ObfuscatedCreditCardShortFormMobileSitePaymentProfile) paymentProfileResponse.PaymentProfile;
                    billingInfoModel.PaymentProfileId = creditCardShortFormMobileSitePaymentProfile.PaymentProfileID.ToString();
                    billingInfoModel.CardType = creditCardShortFormMobileSitePaymentProfile.CardType;
                    billingInfoModel.ExpMonth = creditCardShortFormMobileSitePaymentProfile.ExpMonth;
                    billingInfoModel.ExpYear = creditCardShortFormMobileSitePaymentProfile.ExpYear;
                    billingInfoModel.LastFourDigitsCreditCardNumber = creditCardShortFormMobileSitePaymentProfile.LastFourDigitsCreditCardNumber;
                    billingInfoModel.FirstName = creditCardShortFormMobileSitePaymentProfile.FirstName;
                    billingInfoModel.LastName = creditCardShortFormMobileSitePaymentProfile.LastName;
                    billingInfoModel.PostalCode = creditCardShortFormMobileSitePaymentProfile.PostalCode;
                }
                else if (paymentProfileResponse.PaymentProfile is ObfuscatedCreditCardShortFormFullWebSitePaymentProfile)
                {
                    billingInfoModel.PaymentType = PaymentType.CreditCardShortFormFullWebSite;
                    var creditCardShortFormFullWebSitePaymentProfile = (ObfuscatedCreditCardShortFormFullWebSitePaymentProfile) paymentProfileResponse.PaymentProfile;
                    billingInfoModel.PaymentProfileId = creditCardShortFormFullWebSitePaymentProfile.PaymentProfileID.ToString();
                    billingInfoModel.CardType = creditCardShortFormFullWebSitePaymentProfile.CardType;
                    billingInfoModel.ExpMonth = creditCardShortFormFullWebSitePaymentProfile.ExpMonth;
                    billingInfoModel.ExpYear = creditCardShortFormFullWebSitePaymentProfile.ExpYear;
                    billingInfoModel.LastFourDigitsCreditCardNumber = creditCardShortFormFullWebSitePaymentProfile.LastFourDigitsCreditCardNumber;
                    billingInfoModel.FirstName = creditCardShortFormFullWebSitePaymentProfile.FirstName;
                    billingInfoModel.LastName = creditCardShortFormFullWebSitePaymentProfile.LastName;
                    billingInfoModel.PostalCode = creditCardShortFormFullWebSitePaymentProfile.PostalCode;
                }
                else if (paymentProfileResponse.PaymentProfile is ObsfucatedPaypalPaymentProfile)
                {
                    billingInfoModel.PaymentType = PaymentType.PayPalLitle;
                    var paypalPaymentProfile = (ObsfucatedPaypalPaymentProfile) paymentProfileResponse.PaymentProfile;
                    billingInfoModel.PaymentProfileId = paypalPaymentProfile.PaymentProfileID.ToString();
                    billingInfoModel.FirstName = paypalPaymentProfile.FirstName;
                    billingInfoModel.LastName = paypalPaymentProfile.LastName;
                    billingInfoModel.EmailAddress = paypalPaymentProfile.Email;
                    billingInfoModel.Country = paypalPaymentProfile.Country;
                    billingInfoModel.PayerStatus = paypalPaymentProfile.PayerStatus;
                    billingInfoModel.PaypalToken = paypalPaymentProfile.PaypalToken;
                    billingInfoModel.BillingAgreementID = paypalPaymentProfile.BillingAgreementID;
                }
                else if (paymentProfileResponse.PaymentProfile is ObfuscatedDebitCardPaymentProfile)
                {
                    billingInfoModel.PaymentType = PaymentType.DebitCard;
                    var debitCardPaymentProfile = (ObfuscatedDebitCardPaymentProfile) paymentProfileResponse.PaymentProfile;
                    billingInfoModel.CardType = debitCardPaymentProfile.CardType;
                    billingInfoModel.City = debitCardPaymentProfile.City;
                    billingInfoModel.Country = debitCardPaymentProfile.Country;
                    billingInfoModel.ExpMonth = debitCardPaymentProfile.ExpMonth;
                    billingInfoModel.ExpYear = debitCardPaymentProfile.ExpYear;
                    billingInfoModel.FirstName = debitCardPaymentProfile.FirstName;
                    billingInfoModel.LastFourDigitsCreditCardNumber = debitCardPaymentProfile.LastFourDigitsCreditCardNumber;
                    billingInfoModel.LastName = debitCardPaymentProfile.LastName;
                    billingInfoModel.PhoneNumber = debitCardPaymentProfile.PhoneNumber;
                    billingInfoModel.PostalCode = debitCardPaymentProfile.PostalCode;
                    billingInfoModel.State = debitCardPaymentProfile.State;
                    billingInfoModel.StreetAddress = debitCardPaymentProfile.StreetAddress;
                    billingInfoModel.GovernmentIssuedID = debitCardPaymentProfile.GovernmentIssuedID;
                }
                else
                {
                    billingInfoModel.PaymentType = PaymentType.None;
                }

                //lets take care of the display
                if (_MemberSiteInfo.SiteID == SiteIDs.JDateCoIL || _MemberSiteInfo.SiteID == SiteIDs.Cupid)
                    billingInfoModel.ShowGovernmentIssueID = true;
                else
                    billingInfoModel.ShowGovernmentIssueID = false;

                return billingInfoModel;
            }
            billingInfoModel.PaymentType = PaymentType.None;
            return billingInfoModel;
        }

        public BillingInfo ArchivePaymentProfile(string paymentProfileId)
        {
            // archive and do a re-get after where you should get nothing back
            var responseXml = PaymentProfileServiceWebAdapter.GetProxyInstance().ArchivePaymentProfile(paymentProfileId, (int) _MemberSiteInfo.SiteID);

            // archiving payment profile does not mark off IsDefault (DefaultProfile column in DB). we must do this manually
            if (!string.IsNullOrEmpty(responseXml))
            {
                var response = new XmlDocument();
                response.LoadXml(responseXml);

                if (response.DocumentElement.Name == "PaymentResponse" &&
                    response.DocumentElement.GetAttribute("response") == "SUCCESS")
                {
                    PaymentProfileServiceWebAdapter.GetProxyInstance().UpdateMemberPaymentProfileIsDefault(MemberID,
                        (int) SiteID,
                        paymentProfileId,
                        false);

                    // we need to delete this from our cache. i don't know why this cache was added in the first place...
                    DefaultPaymentProfileCache.Remove(MemberID, (int) SiteID);
                }
            }

            return GetDefaultBillingInfo();
        }

        public RenewalInfo GetRenewalInfo()
        {
            var renewalInfo = new RenewalInfo();

            var brand = g.GetBrand((int) _MemberSiteInfo.SiteID);
            var renewalSub = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetCurrentRenewalSubscription(_Member.MemberID, (int) SiteID);
            var memberSubExpDate = _Member.GetAttributeDate(brand, "SubscriptionExpirationDate");
            var memberRenewDate = DateTime.MinValue;

            if (renewalSub != null && renewalSub.RenewalSubscriptionID > 0 && renewalSub.PrimaryPackageID > 0)
            {
                memberRenewDate = renewalSub.RenewalDatePST;
                if (memberSubExpDate != memberRenewDate)
                {
                    renewalInfo.CanAdjustRenewalDate = true;
                }

                var memberPlan = PlanSA.Instance.GetPlan(renewalSub.PrimaryPackageID, g.GetBrand((int) SiteID).BrandID);

                if (memberPlan != null)
                {
                    renewalInfo.CanReopen = !renewalSub.IsRenewalEnabled;

                    #region grab a la carte info

                    if (renewalSub.RenewalSubscriptionDetails != null && renewalSub.RenewalSubscriptionDetails.Length > 1)
                    {
                        Purchase.Plan rsePlan = null;
                        foreach (var rse in renewalSub.RenewalSubscriptionDetails)
                        {
                            // make sure it's a la carte
                            if (rse.IsPrimaryPackage)
                                continue;

                            rsePlan = PlanSA.Instance.GetPlan(rse.PackageID, brand.BrandID);
                            if (rsePlan != null)
                            {
                                if ((rsePlan.PremiumTypeMask & Purchase.PremiumType.HighlightedProfile) == Purchase.PremiumType.HighlightedProfile)
                                {
                                    renewalInfo.HasPremiumHighlight = true;
                                    if (rse.IsRenewalEnabled)
                                    {
                                        renewalInfo.RenewalEnabledPremiumHighlight = true;
                                    }
                                }
                                else if ((rsePlan.PremiumTypeMask & Purchase.PremiumType.SpotlightMember) == Purchase.PremiumType.SpotlightMember)
                                {
                                    renewalInfo.HasPremiumSpotlight = true;
                                    if (rse.IsRenewalEnabled)
                                    {
                                        renewalInfo.RenewalEnabledPremiumSpotlight = true;
                                    }
                                }
                                else if ((rsePlan.PremiumTypeMask & Purchase.PremiumType.JMeter) == Purchase.PremiumType.JMeter)
                                {
                                    renewalInfo.HasPremiumJMeter = true;
                                    if (rse.IsRenewalEnabled)
                                    {
                                        renewalInfo.RenewalEnabledPremiumJMeter = true;
                                    }
                                }
                                else if ((rsePlan.PremiumTypeMask & Purchase.PremiumType.AllAccess) == Purchase.PremiumType.AllAccess)
                                {
                                    renewalInfo.HasPremiumAllAccess = true;
                                    if (rse.IsRenewalEnabled)
                                    {
                                        renewalInfo.RenewalEnabledPremiumAllAccess = true;
                                    }
                                }
                                else if ((rsePlan.PremiumTypeMask & Purchase.PremiumType.ReadReceipt) == Purchase.PremiumType.ReadReceipt)
                                {
                                    renewalInfo.HasPremiumReadReceipts = true;
                                    if (rse.IsRenewalEnabled)
                                    {
                                        renewalInfo.RenewalEnabledReadReceipts = true;
                                    }
                                }
                            }
                        }
                    }

                    #endregion

                    // if renewal is active, load up termination reasons for UI
                    if (renewalSub.IsRenewalEnabled)
                    {
                        renewalInfo.HasValidPlan = true;

                        // load the termination reason here
                        var trsc = PurchaseSA.Instance.GetTransactionReasonsSite((int) SiteID);
                        if (trsc != null)
                        {
                            var resourceBag = GeneralHelper.GetResourceCollection(GeneralHelper.DetermineResourceFileName(brand));
                            foreach (Purchase.TransactionReason tr in trsc)
                            {
                                renewalInfo.TerminationReasons.Add(new SelectListItem {Text = GeneralHelper.GetResource(resourceBag, tr.ResourceConstant), Value = tr.TransactionReasonID.ToString()});
                            }
                        }
                    }
                }
            }

            renewalInfo.MemberID = _Member.MemberID;
            renewalInfo.MemberSiteInfo = _MemberSiteInfo;
            renewalInfo.HasActiveFreeTrialSubscription = HasActiveFreeTrialSubscription();

            return renewalInfo;
        }

        /// <summary>
        ///     note wrap AuthorizationServiceWebAdapter in try catch when checking for a free trial.
        /// </summary>
        /// <returns></returns>
        public bool HasActiveFreeTrialSubscription()
        {
            var hasActiveFreeTrial = false;
            try
            {
                if (AuthorizationServiceWebAdapter.GetProxyInstance().HasActiveFreeTrialSubscription(_Member.MemberID, (int) SiteID))
                    hasActiveFreeTrial = true;
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Error in checking to see if the member has an active free trial subscription, Error message: " + ex.Message);
            }
            finally
            {
                AuthorizationServiceWebAdapter.CloseProxyInstance();
            }

            return hasActiveFreeTrial;
        }

        public AdminAdjustInfo GetAdminAdjustInfo()
        {
            var adminAdjustInfo = new AdminAdjustInfo();

            var renewalSub = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetCurrentRenewalSubscription(_Member.MemberID, (int) SiteID);
            Purchase.Plan basePlan = null;

            if (renewalSub != null && renewalSub.RenewalSubscriptionID > 0 && renewalSub.PrimaryPackageID > 0)
            {
                basePlan = PlanSA.Instance.GetPlan(renewalSub.PrimaryPackageID, g.GetBrand((int) SiteID).BrandID);
            }

            if (basePlan != null)
            {
                if ((basePlan.PlanTypeMask & Purchase.PlanType.PremiumPlan) == Purchase.PlanType.PremiumPlan)
                {
                    if ((basePlan.PremiumTypeMask & Purchase.PremiumType.HighlightedProfile) == Purchase.PremiumType.HighlightedProfile)
                    {
                        adminAdjustInfo.HasPremiumHighlightedProfile = true;
                    }

                    if ((basePlan.PremiumTypeMask & Purchase.PremiumType.SpotlightMember) == Purchase.PremiumType.SpotlightMember)
                    {
                        adminAdjustInfo.HasPremiumMemberSpotlight = true;
                    }

                    if ((basePlan.PremiumTypeMask & Purchase.PremiumType.JMeter) == Purchase.PremiumType.JMeter)
                    {
                        adminAdjustInfo.HasPremiumJMeter = true;
                    }

                    if ((basePlan.PremiumTypeMask & Purchase.PremiumType.AllAccess) == Purchase.PremiumType.AllAccess)
                    {
                        adminAdjustInfo.HasPremiumAllAccess = true;
                    }

                    if ((basePlan.PremiumTypeMask & Purchase.PremiumType.ReadReceipt) == Purchase.PremiumType.ReadReceipt)
                    {
                        adminAdjustInfo.HasPremiumReadReceipts = true;
                    }
                }

                //A La Carte Plans
                if (renewalSub.RenewalSubscriptionDetails != null && renewalSub.RenewalSubscriptionDetails.Length > 1)
                {
                    for (var alc = 0; alc < renewalSub.RenewalSubscriptionDetails.Length; alc++)
                    {
                        if (!renewalSub.RenewalSubscriptionDetails[alc].IsPrimaryPackage && renewalSub.RenewalSubscriptionDetails[alc].IsRenewalEnabled)
                        {
                            var alcPlan = PlanSA.Instance.GetPlan(renewalSub.RenewalSubscriptionDetails[alc].PackageID, g.GetBrand((int) SiteID).BrandID);
                            if (alcPlan != null)
                            {
                                if ((alcPlan.PlanTypeMask & Purchase.PlanType.ALaCarte) == Purchase.PlanType.ALaCarte)
                                {
                                    if ((alcPlan.PremiumTypeMask & Purchase.PremiumType.HighlightedProfile) == Purchase.PremiumType.HighlightedProfile)
                                    {
                                        adminAdjustInfo.HasPremiumHighlightedProfile = true;
                                    }

                                    if ((alcPlan.PremiumTypeMask & Purchase.PremiumType.SpotlightMember) == Purchase.PremiumType.SpotlightMember)
                                    {
                                        adminAdjustInfo.HasPremiumMemberSpotlight = true;
                                    }

                                    if ((alcPlan.PremiumTypeMask & Purchase.PremiumType.JMeter) == Purchase.PremiumType.JMeter)
                                    {
                                        adminAdjustInfo.HasPremiumJMeter = true;
                                    }

                                    if ((alcPlan.PremiumTypeMask & Purchase.PremiumType.AllAccess) == Purchase.PremiumType.AllAccess)
                                    {
                                        adminAdjustInfo.HasPremiumAllAccess = true;
                                    }

                                    if ((alcPlan.PremiumTypeMask & Purchase.PremiumType.ReadReceipt) == Purchase.PremiumType.ReadReceipt)
                                    {
                                        adminAdjustInfo.HasPremiumReadReceipts = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            adminAdjustInfo.MemberID = _Member.MemberID;
            adminAdjustInfo.MemberSiteInfo = _MemberSiteInfo;

            return adminAdjustInfo;
        }

        public PauseSubscriptionInfo GetPauseSubscriptionInfo(bool hasPausedSubscription, bool isPausedAllowed)
        {
            var pauseSubscriptionInfo = new PauseSubscriptionInfo(hasPausedSubscription, isPausedAllowed);
            pauseSubscriptionInfo.MemberID = _Member.MemberID;
            pauseSubscriptionInfo.MemberSiteInfo = _MemberSiteInfo;

            // Set the maximum pause time allowed 
            var brand = g.GetBrand((int) _MemberSiteInfo.SiteID);
            //pauseSubscriptionInfo.MaximumTimeLimitForPauseInMinutes = Int32.Parse(RuntimeSettings.GetSetting("MAXIMUM_SUBSCRIPTION_FREEZE_TIME_IN_MINUTES", brand.Site.Community.CommunityID, brand.Site.SiteID));

            if (!hasPausedSubscription)
            {
                // Subscription is not paused
                // Check to see if pausing a subscription is allowed for this site 
                // Only get the information that will be used in the pause subscription overlay only if pausing a subscription on this site is allowed 
                if (pauseSubscriptionInfo.IsPauseAllowed)
                {
                    // Customer subscription status is not frozen 
                    // Get all the privileges that this customer has for this site 
                    var arrAccessPrivileges = AccessServiceWebAdapter.GetProxyInstanceForBedrock().GetCustomerPrivilegeList(_Member.MemberID, (int) SiteID);
                    var customerPrivilegeDescriptions = new List<string>();
                    var remainingBasicSubscriptionInMinutes = Constants.NULL_INT;
                    //int allAccessEmailsLeft = Constants.NULL_INT;
                    //bool addToPrivilegeDescriptionList = true;
                    //string privilegeDescription = Constants.NULL_STRING;

                    if (arrAccessPrivileges != null)
                    {
                        foreach (var privilege in arrAccessPrivileges)
                        {
                            var allAccessEmailsLeft = Constants.NULL_INT;
                            var addToPrivilegeDescriptionList = true;
                            var privilegeDescription = Constants.NULL_STRING;

                            if (privilege.UnifiedPrivilegeType == PrivilegeType.BasicSubscription)
                            {
                                var span = privilege.EndDateUTC.Subtract(DateTime.UtcNow);

                                remainingBasicSubscriptionInMinutes = Convert.ToInt32(span.TotalMinutes);
                                addToPrivilegeDescriptionList = false;
                            }
                            else if (privilege.UnifiedPrivilegeType == PrivilegeType.AllAccessEmails)
                            {
                                allAccessEmailsLeft = privilege.RemainingCount;
                                privilegeDescription = Enum.GetName(typeof (PrivilegeType), privilege.UnifiedPrivilegeType) + "(" + Convert.ToString(allAccessEmailsLeft) + ")";
                            }
                            else
                            {
                                privilegeDescription = Enum.GetName(typeof (PrivilegeType), privilege.UnifiedPrivilegeType);
                            }

                            if (addToPrivilegeDescriptionList)
                            {
                                customerPrivilegeDescriptions.Add(privilegeDescription);
                            }
                        }
                    }

                    pauseSubscriptionInfo.remainingBasicSubscriptionMinutes = remainingBasicSubscriptionInMinutes;
                    pauseSubscriptionInfo.customerPrivilegeDescriptions = customerPrivilegeDescriptions;

                    // Get the current renewal subscription 
                    var renewalSub = RenewalServiceWebAdapter.GetProxyInstance().GetCurrentRenewalSubscription(_Member.MemberID, (int) SiteID);
                    if (renewalSub != null)
                    {
                        pauseSubscriptionInfo.lastSavedRenewalIsOpen = renewalSub.IsRenewalEnabled;
                        var plan = PlanSA.Instance.GetPlan(renewalSub.PrimaryPackageID, g.GetBrand((int) SiteID).BrandID);

                        var renewalInfo = new RenewalInfo();
                        renewalInfo.RenewalPlanInitialDuration = plan.InitialDuration;
                        renewalInfo.RenewalPlanInitialDurationType = (int) plan.InitialDurationType;
                        renewalInfo.RenewalPlanInitialAmount = plan.InitialCost;
                        renewalInfo.RenewalPlanRenewalDuration = plan.RenewDuration;
                        renewalInfo.RenewalPlanRenewalDurationType = (int) plan.RenewDurationType;
                        renewalInfo.RenewalPlanRenewalAmount = plan.RenewCost;

                        pauseSubscriptionInfo.CurrentRenewalInfo = renewalInfo;
                    }
                }
            }
            else
            {
                // Get all the frozen privileges that this customer has for this site 
                var arrAccessFrozenPrivileges = AccessServiceWebAdapter.GetProxyInstanceForBedrock().GetCustomerFrozenPrivilegeList(_Member.MemberID, (int) SiteID);
                var customerPrivilegeDescriptions = new List<string>();
                var remainingBasicSubscriptionInMinutes = Constants.NULL_INT;

                foreach (var frozenPrivilege in arrAccessFrozenPrivileges)
                {
                    var allAccessEmailsLeft = Constants.NULL_INT;
                    var addToPrivilegeDescriptionList = true;
                    var privilegeDescription = Constants.NULL_STRING;

                    if (frozenPrivilege.UnifiedPrivilegeType == PrivilegeType.BasicSubscription)
                    {
                        remainingBasicSubscriptionInMinutes = frozenPrivilege.Duration;
                        addToPrivilegeDescriptionList = false;
                    }
                    else if (frozenPrivilege.UnifiedPrivilegeType == PrivilegeType.AllAccessEmails)
                    {
                        allAccessEmailsLeft = frozenPrivilege.RemainingCount;
                        privilegeDescription = Enum.GetName(typeof (PrivilegeType), frozenPrivilege.UnifiedPrivilegeType) + "(" + Convert.ToString(allAccessEmailsLeft) + ")";
                    }
                    else
                    {
                        privilegeDescription = Enum.GetName(typeof (PrivilegeType), frozenPrivilege.UnifiedPrivilegeType);
                    }

                    if (addToPrivilegeDescriptionList)
                    {
                        customerPrivilegeDescriptions.Add(privilegeDescription);
                    }
                }

                pauseSubscriptionInfo.remainingBasicSubscriptionMinutes = remainingBasicSubscriptionInMinutes;
                pauseSubscriptionInfo.customerPrivilegeDescriptions = customerPrivilegeDescriptions;

                // Get the current frozen renewal subscription 
                var renewalSub = RenewalServiceWebAdapter.GetProxyInstance().GetCurrentFrozenRenewalSubscription(_Member.MemberID, (int) SiteID);
                if (renewalSub != null)
                {
                    pauseSubscriptionInfo.lastSavedRenewalIsOpen = renewalSub.LastIsRenewalEnabled;
                    var plan = PlanSA.Instance.GetPlan(renewalSub.PrimaryPackageID, g.GetBrand((int) SiteID).BrandID);

                    var renewalInfo = new RenewalInfo();
                    renewalInfo.RenewalPlanInitialDuration = plan.InitialDuration;
                    renewalInfo.RenewalPlanInitialDurationType = (int) plan.InitialDurationType;
                    renewalInfo.RenewalPlanInitialAmount = plan.InitialCost;
                    renewalInfo.RenewalPlanRenewalDuration = plan.RenewDuration;
                    renewalInfo.RenewalPlanRenewalDurationType = (int) plan.RenewDurationType;
                    renewalInfo.RenewalPlanRenewalAmount = plan.RenewCost;

                    pauseSubscriptionInfo.CurrentRenewalInfo = renewalInfo;
                }
            }

            // Get the history of the subscription freezes 
            // Get only the top 5 most recent subscription freezes 
            var renewalTransactions = RenewalServiceWebAdapter.GetProxyInstance().GetRenewalFrozenTransactionHistory(_Member.MemberID, (int) SiteID, 1, 5);
            PauseSubscriptionHistory history = null;
            var allPauseSubscriptionHistory = new List<PauseSubscriptionHistory>();
            var lastExternalTransactionID = Constants.NULL_INT;
            foreach (var renewalTransaction in renewalTransactions)
            {
                if (renewalTransaction.ExternalTransactionID != lastExternalTransactionID)
                {
                    if (history != null)
                    {
                        allPauseSubscriptionHistory.Add(history);
                        history = null;
                    }
                }

                if (history == null)
                {
                    history = new PauseSubscriptionHistory();
                }

                var transactionType = (TransactionType) renewalTransaction.TransactionTypeID;
                if (transactionType == TransactionType.FreezeSubscriptionAccount)
                {
                    history.PauseDate = renewalTransaction.InsertDateUTC.ToLocalTime();

                    // Get the duration of the primary package found only for the frozen renewal transaction 
                    // The primary package for an unfrozen renewal transaction is a regular renewal subscription which can be updated with a different package
                    // after a purchase. 
                    var primaryPackage = CatalogServiceWebAdapter.GetProxyInstance().GetPackageDetails(renewalTransaction.PrimaryPackageID);
                    foreach (var packageItem in primaryPackage.Items)
                    {
                        if (packageItem.PrivilegeType[0] == Spark.Common.CatalogService.PrivilegeType.BasicSubscription)
                        {
                            // Each package item has only one privilege type 
                            history.PlanDuration = packageItem.Duration;
                            history.PlanDurationType = (int) packageItem.DurationType;
                            break;
                        }
                    }

                    //Matchnet.Purchase.ValueObjects.Plan basePlan = PlanSA.Instance.GetPlan(renewalTransaction.PrimaryPackageID, g.GetBrand((int)SiteID).BrandID);
                }
                else if (transactionType == TransactionType.UnfreezeSubscriptionAccount)
                {
                    history.ResumeDate = renewalTransaction.InsertDateUTC.ToLocalTime();
                }

                lastExternalTransactionID = renewalTransaction.ExternalTransactionID;
            }

            if (history != null)
            {
                allPauseSubscriptionHistory.Add(history);
            }
            pauseSubscriptionInfo.FrozenTransactionHistory = allPauseSubscriptionHistory;


            //if (renewalSub != null && renewalSub.RenewalSubscriptionID > 0 && renewalSub.PrimaryPackageID > 0)
            //{
            //    Matchnet.Purchase.ValueObjects.Plan basePlan = PlanSA.Instance.GetPlan(renewalSub.PrimaryPackageID, g.GetBrand((int)SiteID).BrandID);
            //    if ((basePlan.PlanTypeMask & Purchase.PlanType.PremiumPlan) == Purchase.PlanType.PremiumPlan)
            //    {
            //        if ((basePlan.PremiumTypeMask & Purchase.PremiumType.HighlightedProfile) == Purchase.PremiumType.HighlightedProfile)
            //        {
            //            adminAdjustInfo.HasPremiumHighlightedProfile = true;
            //        }

            //        if ((basePlan.PremiumTypeMask & Purchase.PremiumType.SpotlightMember) == Purchase.PremiumType.SpotlightMember)
            //        {
            //            adminAdjustInfo.HasPremiumMemberSpotlight = true;
            //        }

            //        if ((basePlan.PremiumTypeMask & Purchase.PremiumType.JMeter) == Purchase.PremiumType.JMeter)
            //        {
            //            adminAdjustInfo.HasPremiumJMeter = true;
            //        }

            //        if ((basePlan.PremiumTypeMask & Purchase.PremiumType.AllAccess) == Purchase.PremiumType.AllAccess)
            //        {
            //            adminAdjustInfo.HasPremiumAllAccess = true;
            //        }

            //    }

            //    //A La Carte Plans
            //    if (renewalSub.RenewalSubscriptionDetails != null && renewalSub.RenewalSubscriptionDetails.Length > 1)
            //    {
            //        for (int alc = 0; alc < renewalSub.RenewalSubscriptionDetails.Length; alc++)
            //        {
            //            if (!renewalSub.RenewalSubscriptionDetails[alc].IsPrimaryPackage && renewalSub.RenewalSubscriptionDetails[alc].IsRenewalEnabled)
            //            {
            //                Matchnet.Purchase.ValueObjects.Plan alcPlan = PlanSA.Instance.GetPlan(renewalSub.RenewalSubscriptionDetails[alc].PackageID, g.GetBrand((int)SiteID).BrandID);
            //                if ((alcPlan.PlanTypeMask & Purchase.PlanType.ALaCarte) == Purchase.PlanType.ALaCarte)
            //                {
            //                    if ((alcPlan.PremiumTypeMask & Purchase.PremiumType.HighlightedProfile) == Purchase.PremiumType.HighlightedProfile)
            //                    {
            //                        adminAdjustInfo.HasPremiumHighlightedProfile = true;
            //                    }

            //                    if ((alcPlan.PremiumTypeMask & Purchase.PremiumType.SpotlightMember) == Purchase.PremiumType.SpotlightMember)
            //                    {
            //                        adminAdjustInfo.HasPremiumMemberSpotlight = true;
            //                    }

            //                    if ((alcPlan.PremiumTypeMask & Purchase.PremiumType.JMeter) == Purchase.PremiumType.JMeter)
            //                    {
            //                        adminAdjustInfo.HasPremiumJMeter = true;
            //                    }

            //                    if ((alcPlan.PremiumTypeMask & Purchase.PremiumType.AllAccess) == Purchase.PremiumType.AllAccess)
            //                    {
            //                        adminAdjustInfo.HasPremiumMemberSpotlight = true;
            //                    }
            //                }
            //            }
            //        }
            //    }

            //}

            //adminAdjustInfo.MemberID = _Member.MemberID;
            //adminAdjustInfo.MemberSiteInfo = _MemberSiteInfo;

            //return adminAdjustInfo;

            return pauseSubscriptionInfo;
        }

        private List<AccountHistory> GetSortedAccountHistory(List<AccountHistory> colAccountHistory)
        {
            colAccountHistory.Sort((x, y) => (y.InsertDateInPST.CompareTo(x.InsertDateInPST)));
            return colAccountHistory;
        }

        public DateTimeStampInfo GetDateTimeStampInfo()
        {
            var dateTimeStampInfo = new DateTimeStampInfo();
            dateTimeStampInfo.LastLogons = _Member.GetMemberLastLogons(g.GetBrand((int) SiteID).BrandID, MemberLoadFlags.IngoreSACache);

            dateTimeStampInfo.LastLoginDate = _Member.GetAttributeDate(g.GetBrand((int) SiteID), "BrandLastLogonDate", DateTime.MinValue);
            dateTimeStampInfo.LastUpdateDate = _Member.GetAttributeDate(g.GetBrand((int) SiteID), "LastUpdated", DateTime.MinValue);
            dateTimeStampInfo.RegisteredDate = _Member.GetAttributeDate(g.GetBrand((int) SiteID), "BrandInsertDate", DateTime.MinValue);
            var firstEverBrandInsertDate = MemberHelper.GetFirstBrandInsertDate(_Member);

            // check to see if we are on the site where member registered from
            if (firstEverBrandInsertDate == dateTimeStampInfo.RegisteredDate)
            {
                var regSceneID = _Member.GetAttributeInt(g.GetBrand((int) SiteID), "RegistrationScenarioID", Constants.NULL_INT);

                if (regSceneID != Constants.NULL_INT)
                {
                    var regScenarios =
                        RegistrationMetadataSA.Instance.GetScenariosForAdmin();
                    var memberRegScenario = (from r in regScenarios
                        where r.ID == regSceneID
                        select r).SingleOrDefault();
                    dateTimeStampInfo.IsMobileRegistration = (memberRegScenario != null) && (memberRegScenario.DeviceType == DeviceType.Handeheld);
                }
            }

            dateTimeStampInfo.MemberEmail = _Member.EmailAddress;
            dateTimeStampInfo.MemberID = _Member.MemberID;

            switch ((int) SiteID)
            {
                case 4:
                    dateTimeStampInfo.SiteNameForLink = "jdateil";
                    break;
                case 15:
                    dateTimeStampInfo.SiteNameForLink = "cupidil";
                    break;
                case 101:
                    dateTimeStampInfo.SiteNameForLink = "american";
                    break;
                case 103:
                    dateTimeStampInfo.SiteNameForLink = "jdate";
                    break;
                case 105:
                    dateTimeStampInfo.SiteNameForLink = "jdatefr";
                    break;
                case 107:
                    dateTimeStampInfo.SiteNameForLink = "jdateuk";
                    break;
                case 9041:
                    dateTimeStampInfo.SiteNameForLink = "bbwbh";
                    break;
                case 9051:
                    dateTimeStampInfo.SiteNameForLink = "blackbh";
                    break;
                default: // so we dont blow up. 
                    _logger.Debug(String.Format("GetDateTimeStampInfo SiteID {0} not valid", (int) SiteID));
                    dateTimeStampInfo.SiteNameForLink = "jdate";
                    break;
            }

            return dateTimeStampInfo;
        }

        public SubscriptionStatusInfo GetSubscriptionStatusInfo()
        {
            var brand = g.GetBrand((int) SiteID);

            var subInfo = new SubscriptionStatusInfo();
            subInfo.MemberID = _Member.MemberID;
            subInfo.MemberSiteInfo = _MemberSiteInfo;
            subInfo.BrandID = brand.BrandID;
            subInfo.SubscriberStatus = MemberHelper.GetSubscriberStatusString(_Member, brand);
            subInfo.SubscriptionLastInitialPurchaseDate = _Member.GetAttributeDate(brand, "SubscriptionLastInitialPurchaseDate", DateTime.MinValue);
            if (subInfo != null && subInfo.SubscriptionLastInitialPurchaseDate != null && subInfo.SubscriptionLastInitialPurchaseDate.HasValue && subInfo.SubscriptionLastInitialPurchaseDate.Value == DateTime.MinValue)
            {
                subInfo.SubscriptionLastInitialPurchaseDate = null;
            }
            subInfo.ActivePremiumServices = "";

            _logger.Debug("GetSubscriptionStatusInfo: Checkpoint 1");
            var renewalSub = MemberHelper.GetRenewalSubscription(_Member.MemberID, (int) _MemberSiteInfo.SiteID);
            var hasBasicSubscriptionPrivileges = false;
            Purchase.Plan renewalPlan = null;

            if (renewalSub != null && renewalSub.PrimaryPackageID > 0)
            {
                renewalPlan = PlanSA.Instance.GetPlan(renewalSub.PrimaryPackageID, brand.BrandID);
                if (renewalPlan == null)
                {
                    _logger.Debug(String.Format("GetSubscriptionStatusInfo: Member {0}, Plan {1} does not exist", _Member.MemberID, renewalSub.PrimaryPackageID));
                }
            }

            if (renewalPlan != null)
            {
                subInfo.HasValidSubsciption = true;
                subInfo.RenewalAmount = renewalPlan.RenewCost;
                subInfo.BundledPremiumServices = GeneralHelper.GetPlanTypeString(renewalPlan);
                if (renewalSub.RenewalSubscriptionDetails != null && renewalSub.RenewalSubscriptionDetails.Length > 1)
                {
                    subInfo.ALCPremiumServices = GeneralHelper.GetPlanTypeString(renewalSub.RenewalSubscriptionDetails, brand);
                    RenewalSubscriptionDetail mseHighlight = null;
                    subInfo.HasHighlightALCService = (GeneralHelper.GetPlanFromRenewalSubscriptionDetail(renewalSub.RenewalSubscriptionDetails, Purchase.PremiumType.HighlightedProfile, brand, out mseHighlight) > 0);
                    RenewalSubscriptionDetail mseSpotlight = null;
                    subInfo.HasSpotlightALCService = (GeneralHelper.GetPlanFromRenewalSubscriptionDetail(renewalSub.RenewalSubscriptionDetails, Purchase.PremiumType.SpotlightMember, brand, out mseSpotlight) > 0);
                    RenewalSubscriptionDetail mseAllAccess = null;
                    subInfo.HasAllAccessALCService = (GeneralHelper.GetPlanFromRenewalSubscriptionDetail(renewalSub.RenewalSubscriptionDetails, Purchase.PremiumType.AllAccess, brand, out mseAllAccess) > 0);
                    RenewalSubscriptionDetail mseJMeter = null;
                    subInfo.HasJMeterALCService = (GeneralHelper.GetPlanFromRenewalSubscriptionDetail(renewalSub.RenewalSubscriptionDetails, Purchase.PremiumType.JMeter, brand, out mseJMeter) > 0);
                    RenewalSubscriptionDetail mseReadReceipts = null;
                    subInfo.HasReadReceiptALCService = (GeneralHelper.GetPlanFromRenewalSubscriptionDetail(renewalSub.RenewalSubscriptionDetails, Purchase.PremiumType.ReadReceipt, brand, out mseReadReceipts) > 0);
                }
                subInfo.RenewalDate = renewalSub.RenewalDatePST;
                if (renewalSub.IsRenewalEnabled)
                {
                    subInfo.SubscriptionEndDate = null;
                }
                else
                {
                    subInfo.SubscriptionEndDate = null;

                    var terminateTransaction = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetRenewalTransactionMostRecent(_Member.MemberID, (int) _MemberSiteInfo.SiteID, 5);
                    if (terminateTransaction != null && terminateTransaction.RenewalTransactionID > 0)
                        subInfo.SubscriptionEndDate = GeneralHelper.ConvertUTCToPST(terminateTransaction.InsertDateUTC);
                }

                if (renewalSub.RenewalDateUTC > DateTime.UtcNow)
                {
                    hasBasicSubscriptionPrivileges = true;
                }
                _logger.Debug("GetSubscriptionStatusInfo: Checkpoint 2");
            }
            else
            {
                _logger.Debug("GetSubscriptionStatusInfo: Checkpoint 3");
                subInfo.HasValidSubsciption = false;
                subInfo.RenewalAmount = 0;
                subInfo.BundledPremiumServices = "None";
                subInfo.HasHighlightALCService = false;
                subInfo.HasSpotlightALCService = false;
                subInfo.HasAllAccessALCService = false;

                if (renewalSub != null)
                {
                    subInfo.RenewalDate = renewalSub.RenewalDatePST;
                    if (renewalSub.IsRenewalEnabled)
                    {
                        subInfo.SubscriptionEndDate = null;
                    }
                    else
                    {
                        subInfo.SubscriptionEndDate = null;

                        var terminateTransaction = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetRenewalTransactionMostRecent(_Member.MemberID, (int) _MemberSiteInfo.SiteID, 5);
                        if (terminateTransaction != null && terminateTransaction.RenewalTransactionID > 0)
                            subInfo.SubscriptionEndDate = GeneralHelper.ConvertUTCToPST(terminateTransaction.InsertDateUTC);
                    }
                }
                else //Member has never subscribed and got privileges using TrialPay or AdminAdjust. In this case, get subscription enddate from Member's basic subscription access privileges.
                {
                    var privilege = _Member.GetUnifiedAccessPrivilege(PrivilegeType.BasicSubscription, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                    if (privilege != null)
                    {
                        subInfo.RenewalDate = privilege.EndDatePST;
                    }
                    else
                    {
                        EventLog.WriteEntry("AdminTool", string.Format("Could not retrieve Access Privilege for Member: {0}", _Member.MemberID));
                    }
                }

                if (subInfo.RenewalDate > DateTime.Now)
                {
                    hasBasicSubscriptionPrivileges = true;
                }
                _logger.Debug("GetSubscriptionStatusInfo: Checkpoint 4");
            }

            if (hasBasicSubscriptionPrivileges)
            {
                subInfo.ActivePremiumServices = GeneralHelper.GetMemberActivePremiumServicesString(_Member.MemberID, brand);
            }

            var accessFrozenPrivileges = MemberHelper.GetCustomerFrozenPrivilegeList(_Member.MemberID, (int) _MemberSiteInfo.SiteID);
            if (accessFrozenPrivileges.Count > 0)
            {
                subInfo.HasPausedSubscription = true;
            }

            // Check to see if pausing a subscription is allowed for this site 
            _logger.Debug("GetSubscriptionStatusInfo: Checkpoint 5");
            var allowSubscriptionFreeze = Convert.ToBoolean(RuntimeSettings.GetSetting("IS_SUBSCRIPTION_FREEZE_ENABLED", brand.Site.Community.CommunityID, brand.Site.SiteID));
            if (subInfo.HasValidSubsciption
                && hasBasicSubscriptionPrivileges
                && allowSubscriptionFreeze)
            {
                subInfo.IsPauseAllowed = true;
            }
            else
            {
                subInfo.IsPauseAllowed = false;
            }

            _logger.Debug("GetSubscriptionStatusInfo: Checkpoint 6");
            // check to see if was an in app purchase
            //subInfo.IsIAP = HttpContext.Current.Request.QueryString["hasIAP"]!= null ? true : false;
            subInfo.IsIAP = false;
            if (hasBasicSubscriptionPrivileges)
            {
                try
                {
                    if (!string.IsNullOrEmpty(_Member.GetAttributeText(brand, "IOS_IAP_Receipt_Original_TransactionID")))
                    {
                        subInfo.IsIAP = true;
                    }
                }
                catch (Exception ex1)
                {
                    //attribute doesn't exist for site/community
                    _logger.Debug("GetSubscriptionStatusInfo() issue checking IOS_IAP_Receipt_Original_TransactionID attribute. ", ex1);
                }
            }

            return subInfo;
        }

        public CommunicationHistory GetCommunicationHistory()
        {
            var brand = g.GetBrand((int) SiteID);

            var comHistory = new CommunicationHistory();
            comHistory.MemberID = _Member.MemberID;
            comHistory.MemberSiteInfo = _MemberSiteInfo;
            comHistory.BrandID = brand.BrandID;
            comHistory.LastMessageSentDate = MemberHelper.GetLastMessageSentDate(_Member.MemberID, brand.Site.Community.CommunityID);
            comHistory.LastMatchMailSentDate = DateTime.MinValue;
            comHistory.LastMatchMailAttemptDate = DateTime.MinValue;
            comHistory.UnreadMessageCount = EmailMessageSA.Instance.GetMessageCountByStatus(_Member.MemberID, brand, (int) SystemFolders.Inbox, MessageStatus.Read, true, false);
            comHistory.UnreadMessageCount += EmailMessageSA.Instance.GetMessageCountByStatus(_Member.MemberID, brand, (int) SystemFolders.VIPInbox, MessageStatus.Read, true, false);

            var dict = MemberSA.Instance.GetCommunicationWarningCounts(_Member.MemberID, (int) SiteID);
            if (dict != null)
            {
                int countVal;
                var isInDict = dict.TryGetValue((int) WarningReason.ContactInfo, out countVal);
                comHistory.ContactInfoWarnings = isInDict ? countVal : 0;
                isInDict = dict.TryGetValue((int) WarningReason.InappropriateContent, out countVal);
                comHistory.InappropriateContentWarnings = isInDict ? countVal : 0;
                isInDict = dict.TryGetValue((int) WarningReason.GeneralViolation, out countVal);
                comHistory.GeneralViolationWarnings = isInDict ? countVal : 0;
            }

            try
            {
                var bulkMailRecord = BulkMailReportingSA.Instance.GetBulkMailScheduleRecord(_Member.MemberID, brand.Site.Community.CommunityID, BulkMailType.MatchMail);
                if (bulkMailRecord != null)
                {
                    comHistory.LastMatchMailSentDate = bulkMailRecord.LastSentDate;
                    comHistory.LastMatchMailAttemptDate = bulkMailRecord.LastAttemptDate;
                }
            }
            catch (Exception ex)
            {
            }

            var priv = _Member.GetUnifiedAccessPrivilege(PrivilegeType.AllAccessEmails, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
            if (priv != null)
            {
                comHistory.AllAccessRemainingCount = priv.RemainingCount;
            }
            priv = _Member.GetUnifiedAccessPrivilege(PrivilegeType.AllAccess, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
            if (priv != null)
            {
                comHistory.AllAcessExpireDate = priv.EndDatePST;
            }

            return comHistory;
        }

        public FraudAbuseInfo GetFraudAbuseInfo()
        {
            var brand = g.GetBrand((int) SiteID);
            var fraudInfo = new FraudAbuseInfo();
            fraudInfo.MemberID = _Member.MemberID;
            fraudInfo.MemberSiteInfo = _MemberSiteInfo;
            fraudInfo.SubscriptionFraud = !(Conversion.CBool(_Member.GetAttributeInt(brand, "PassedFraudCheckSite", 1)));
            fraudInfo.ChatBanned = Conversion.CBool(_Member.GetAttributeInt(brand.Site.Community.CommunityID, brand.Site.SiteID, Constants.NULL_INT, "ChatBanned", 0));
            fraudInfo.EmailBanned = Conversion.CBool(_Member.GetAttributeInt(brand.Site.Community.CommunityID, brand.Site.SiteID, Constants.NULL_INT, "EmailBan", 0));
            //fraudInfo.FraudResult = MingleFraudHelper.Instance.GetFraudResult((int)SiteID, _Member.MemberID.ToString());
           // fraudInfo.KountFraudResult = KountFraudHelper.Instance.GetFraudResult(_Member, brand);
            fraudInfo.KountFraudResult = MemberHelper.GetFraudDetailedResponse(_Member.MemberID, (int)SiteID, 3);          
            fraudInfo.SignedPledge = Conversion.CBool(_Member.GetAttributeInt(brand.Site.Community.CommunityID, brand.Site.SiteID, Constants.NULL_INT, "PledgeToCounterFroud", 0));
            var regIPAddress = new IPAddress(BitConverter.GetBytes(_Member.GetAttributeInt(brand, "RegistrationIP")));
            if (regIPAddress.ToString() != WebConstants.INVALID_IP_ADDRESS)
            {
                fraudInfo.IPAddress = regIPAddress.ToString();
                //fraudInfo.IPLocation = new IPLocation.LookupService().getLocation(regIPAddress.ToString());
            }

            // do the timespan calculation here
            var arrOrderInfo = OrderHistoryServiceWebAdapter.GetProxyInstanceForBedrock().GetMemberOrderHistoryByMemberID(_Member.MemberID, (int) SiteID, 5000, 1);
            if (arrOrderInfo != null && arrOrderInfo.Count() > 0)
            {
                var firstPurchaseDate = DateTime.MinValue;

                var orders = arrOrderInfo.ToList();
                var queryResults = (from orderInfo in orders
                    where orderInfo.OrderStatusID == 2
                    select orderInfo.InsertDate
                    );
                if (queryResults != null && queryResults.Count() > 0)
                {
                    firstPurchaseDate = queryResults.Min();
                }

                var regDate = _Member.GetAttributeDate(brand, "BrandInsertDate");

                if (firstPurchaseDate != DateTime.MinValue && regDate != DateTime.MinValue)
                {
                    fraudInfo.TimeBetweenRegAndFirstSub = firstPurchaseDate.ToLocalTime() - regDate;
                }

                _logger.Debug(string.Format("RegToSub debug info: MemberID:{0}, SiteID:{1}, FirstPurchaseDate:{2}, BrandID:{3}, RegDate:{4}, FirstPurchaseDateLocalTime:{5}",
                    _Member.MemberID, SiteID, firstPurchaseDate, brand.BrandID, regDate, firstPurchaseDate.ToLocalTime()));
            }

            return fraudInfo;
        }

        public OtherActioninfo GetOtherActionInfo()
        {
            var brand = g.GetBrand((int) SiteID);
            var otherAction = new OtherActioninfo();
            otherAction.MemberID = _Member.MemberID;
            otherAction.MemberSiteInfo = _MemberSiteInfo;
            otherAction.BrandID = brand.BrandID;
            otherAction.CompletedColorQuiz = MemberHelper.HasMemberCompletedColorQuiz(_Member, brand);
            if (otherAction.CompletedColorQuiz)
            {
                var colorPurchase = _Member.GetAttributeInt(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, "ColorAnalysis", Constants.NULL_INT);
                otherAction.ColorAnalysisPurchase = colorPurchase > 0;
            }
            else
            {
                otherAction.ColorAnalysisPurchase = false;
            }
            if (_MemberSiteInfo.SiteID == SiteIDs.JDateCoIL)
            {
                otherAction.Authenticated = MemberHelper.IsMemberPremiumAuthenticated(_Member, brand);
                otherAction.ProfileHighlight = MemberHelper.IsMemberHighlighted(_Member, brand);
                if (GeneralHelper.IsMatchMeterEnabled(brand))
                {
                    otherAction.JMeterEditable = MemberHelper.IsJMeterEditable(_Member, brand);
                }
            }

            var memberID = _Member.MemberID;
            var siteID = (int) SiteID;

            var accessSvc = AccessServiceWebAdapter.GetProxyInstanceForBedrock();
            var lifetimeInfo = accessSvc.GetLifetimeMember(memberID, siteID);
            otherAction.IsLifetimeMember = lifetimeInfo != null;

            AccessServiceWebAdapter.CloseProxyInstance();


            //get ramah info
            otherAction.HasRamahBadge = Convert.ToBoolean(_Member.GetAttributeInt(brand, "RamahAlum"));


            return otherAction;
        }

        public OffSiteNotifications GetOffSiteNotifications()
        {
            var brand = g.GetBrand((int) SiteID);
            var offSiteNotif = new OffSiteNotifications();
            offSiteNotif.MemberID = _Member.MemberID;
            offSiteNotif.MemberSiteInfo = _MemberSiteInfo;
            offSiteNotif.MatchMailFrequency = _Member.GetAttributeInt(brand, "MatchNewsletterPeriod", 0);
            offSiteNotif.ClickMailFrequency = _Member.GetAttributeInt(brand, "GotAClickEmailPeriod", 0);
            var group = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(brand.Site.Community.CommunityID, EmailAlertConstant.ATTRIBUTE_ID);
            offSiteNotif.EmailAlertMaskValues = _Member.GetAttributeInt(brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, int.Parse(group.DefaultValue));
            offSiteNotif.NewsEventOfferMaskValues = _Member.GetAttributeInt(brand, "NewsletterMask", 0);

            return offSiteNotif;
        }

        public PremiumServiceSettings GetPremiumServiceSettings(out bool hasPremiumServices)
        {
            var settings = new PremiumServiceSettings();
            hasPremiumServices = false;
            settings.MemberID = _Member.MemberID;
            settings.MemberSiteInfo = _MemberSiteInfo;
            var brand = g.GetBrand((int) SiteID);

            var resourceBag = GeneralHelper.GetResourceCollection(GeneralHelper.DetermineResourceFileName(brand));
            var premiumServices = ServiceMemberSA.Instance.GetMemberServices(brand.BrandID, _Member.MemberID);

            var highlightMember = premiumServices.GetMemberServiceInstance((int) ServiceConstants.ServiceIDs.HighligtedProfile);

            if (highlightMember != null)
            {
                settings.HasHighlightProfile = true;
                settings.HiglightEnabled = highlightMember.Attributes.Get<bool>((int) ServiceConstants.Attributes.EnableFlag);
                hasPremiumServices = true;
            }

            var spotlightMember = premiumServices.GetMemberServiceInstance((int) ServiceConstants.ServiceIDs.SpotlightMember);

            if (spotlightMember != null)
            {
                settings.HasSpotlightProfile = true;
                settings.SpotlightEnabled = spotlightMember.Attributes.Get<bool>((int) ServiceConstants.Attributes.EnableFlag);
                settings.SpotlightAgeMin = spotlightMember.Attributes.Get<int>((int) ServiceConstants.Attributes.AgeRange, true);
                settings.SpotlightAgeMax = spotlightMember.Attributes.Get<int>((int) ServiceConstants.Attributes.AgeRange, false);

                var distance = spotlightMember.Attributes.Get<int>((int) ServiceConstants.Attributes.Distance);
                var gendermask = spotlightMember.Attributes.Get<int>((int) ServiceConstants.Attributes.GenderMask);
                var regionID = spotlightMember.Attributes.Get<int>((int) ServiceConstants.Attributes.RegionID);

                settings.SpotlightGender = GeneralHelper.GetGenderFromMask(gendermask);
                settings.SpotlightSeekingGender = GeneralHelper.GetSeekingGenderFromMask(gendermask);
                settings.SpotlightDistanceOptions = GeneralHelper.GetAttributeOptionListItems(brand, "Distance", distance, false, resourceBag);
                settings.SpotlightRegionPicker = new SearchRegionPicker
                {
                    SiteID = brand.Site.SiteID,
                    FormFieldName = "PSSRegionPickerRegionID",
                    CountryRegionIDFormFieldName = "PSSRegionPickerCountryRegionID",
                    SearchTypeFormFieldName = "PSSRegionPickerSearchType",
                    RegionID = regionID,
                    SearchType = SearchType.Region,
                    LockSearchType = true
                };
                hasPremiumServices = true;
            }

            return settings;
        }

        public ProfileDisplaySettings GetProfileDisplaySettings()
        {
            var profileDisplaySettings = new ProfileDisplaySettings();
            var brand = g.GetBrand((int) _MemberSiteInfo.SiteID);

            var displayPhotos = _Member.GetAttributeInt(brand, "DisplayPhotosToGuests", 0);
            if (displayPhotos == 1)
            {
                profileDisplaySettings.ShowPhotosToNonMembers = true;
            }

            var hideMask = _Member.GetAttributeInt(brand, "HideMask", 0);

            if ((Convert.ToInt16(Enums.HideMask.ShowOnline) & hideMask) == Convert.ToInt16(Enums.HideMask.ShowOnline))
            {
                profileDisplaySettings.ShowOnline = false;
            }

            if ((Convert.ToInt16(Enums.HideMask.ShowActivityTrail) & hideMask) == Convert.ToInt16(Enums.HideMask.ShowActivityTrail))
            {
                profileDisplaySettings.ShowActivityTrail = false;
            }

            if ((Convert.ToInt16(Enums.HideMask.ShowInSearches) & hideMask) == Convert.ToInt16(Enums.HideMask.ShowInSearches))
            {
                profileDisplaySettings.ShowInSearches = false;
            }

            profileDisplaySettings.MemberID = _Member.MemberID;
            profileDisplaySettings.MemberSiteInfo = _MemberSiteInfo;
            profileDisplaySettings.BrandID = brand.BrandID;

            return profileDisplaySettings;
        }

        public string GetPassword(out string errorMessage)
        {
            errorMessage = string.Empty;
            if (MemberPrivilegeSA.Instance.IsAdminByPrivilegesOnly(_Member.MemberID))
            {
                errorMessage = "Admin account's password cannot be revealed.";
                return "***";
            }
            return MemberSA.Instance.GetPassword(_Member.EmailAddress);
        }

        public bool UpdateEmail(string newEmail, out string errorMessage)
        {
            var succeeded = false;
            errorMessage = string.Empty;

            if (_Member.EmailAddress.ToLower().Contains("@spark.net"))
            {
                errorMessage = "Spark emails cannot be updated.";
                return succeeded;
            }

            var brand = g.GetBrand((int) _MemberSiteInfo.SiteID);

            if (!String.IsNullOrEmpty(newEmail) && (newEmail != _Member.EmailAddress))
            {
                _Member.SetAttributeDate(brand, "LastUpdated", DateTime.Now);
                var emailVerify = new EmailVerifyHelper(brand);
                if (emailVerify.EnableEmailVerification)
                {
                    if (emailVerify.IfMustBlock(Enums.EmailVerificationSteps.AfterEmailChange) ||
                        emailVerify.IfMustHide(Enums.EmailVerificationSteps.AfterEmailChange))
                    {
                        emailVerify.SetNotVerifiedMemberAttributes(_Member,
                            Enums.EmailVerificationSteps
                                .AfterEmailChange);
                    }
                }

                GlobalStatusMask globalStatusMask;
                _Member.EmailAddress = newEmail;

                globalStatusMask =
                    (GlobalStatusMask)
                        _Member.GetAttributeInt(brand, WebConstants.ATTRIBUTE_NAME_GLOBALSTATUSMASK,
                            (Int32) GlobalStatusMask.Default);
                globalStatusMask = globalStatusMask & (~GlobalStatusMask.BouncedEmail) &
                                   (~GlobalStatusMask.VerifiedEmail);
                _Member.SetAttributeInt(brand, WebConstants.ATTRIBUTE_NAME_GLOBALSTATUSMASK,
                    (Int32) globalStatusMask);

                var memberSaveResult = MemberSA.Instance.SaveMember(_Member,
                    brand.Site.Community.CommunityID);

                if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
                {
                    LogAdminAction(Constants.GROUP_PERSONALS, (Int32) AdminAction.ChangedEmail);
                    succeeded = true;
                }
                else
                {
                    errorMessage = GeneralHelper.AddSpaceBeforeEachCapitalLetter(Enum.GetName(typeof (MemberSaveStatusType), memberSaveResult.SaveStatus));
                }
            }
            else
            {
                errorMessage = "Login value cannot be blank and must be different from the current value.";
            }

            return succeeded;
        }

        /// <summary>
        ///     We are no longer sending the actual password. We are sending them an email with instructions on how to reset the
        ///     password.
        /// </summary>
        /// <returns></returns>
        public bool SendPassword()
        {
            var succeeded = false;

            var brand = g.GetBrand((int) _MemberSiteInfo.SiteID);

            var tokenGuid = MemberSA.Instance.ResetPassword(_Member.MemberID);
            var suaURL = RuntimeSettings.GetSetting(SettingConstants.SUA_URL);
            var suaApplicationId = RuntimeSettings.GetSetting(SettingConstants.SUA_APPLICATION_ID);
            var templateId = brand.Site.Name.Replace(".", "").ToLower();

            var resetUrl = string.Format("{0}/password/reset/{1}?resettoken={2}&mid={3}&clientid={4}",
                suaURL, templateId, HttpUtility.UrlEncode(tokenGuid), _Member.MemberID, suaApplicationId);

            succeeded = ExternalMailSA.Instance.ResetPassword(brand.BrandID, _Member.MemberID, resetUrl);

            LogAdminAction(Constants.GROUP_PERSONALS, (Int32) AdminAction.SendPassword);

            return succeeded;
        }

        public bool SendVerificationEmail()
        {
            var success = false;
            try
            {
                success = true;
                var brand = g.GetBrand((int) _MemberSiteInfo.SiteID);
                ExternalMailSA.Instance.SendEmailVerification(_Member.MemberID, brand.BrandID, true);
            }
            catch
            {
                success = false;
            }
            return success;
        }

        public bool UpdatePassword(string newPassword)
        {
            var succeeded = false;

            var brand = g.GetBrand((int) _MemberSiteInfo.SiteID);
            _Member.SetAttributeDate(brand, "LastUpdated", DateTime.Now);
            _Member.Password = newPassword;
            var memberSaveResult = MemberSA.Instance.SaveMember(_Member, brand.Site.Community.CommunityID);

            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                LogAdminAction(Constants.GROUP_PERSONALS, (Int32) AdminAction.ChangedPassword);

                succeeded = true;
            }

            return succeeded;
        }

        public void AddAdminNote(string note)
        {
            var communityID = g.GetBrand((int) _MemberSiteInfo.SiteID).Site.Community.CommunityID;
            AdminSA.Instance.UpdateAdminNote(note, _Member.MemberID, g.AdminID, communityID, 0);
        }

        public bool UpdateDNEStatus(bool onDNE)
        {
            var succeeded = false;

            var brand = g.GetBrand((int) _MemberSiteInfo.SiteID);

            if (onDNE)
            {
                succeeded = DoNotEmailSA.Instance.AddEmailAddress(brand.BrandID, _Member.EmailAddress, _Member.MemberID);
            }
            else
            {
                succeeded = DoNotEmailSA.Instance.RemoveEmailAddress(brand.Site.SiteID, _Member.EmailAddress);
            }

            return succeeded;
        }

        public bool UpdateGlobalStatusMask(bool adminSuspended, bool badEmail, bool emailVerified, AdminActionReasonID adminActionReasonID)
        {
            var succeeded = false;

            //try
            {
                var globalStatusMask = (GlobalStatusMask) Constants.NULL_INT;
                var brand = g.GetBrand((int) _MemberSiteInfo.SiteID);

                globalStatusMask = (GlobalStatusMask) _Member.GetAttributeInt(brand, WebConstants.ATTRIBUTE_NAME_GLOBALSTATUSMASK);
                if (globalStatusMask != (GlobalStatusMask) Constants.NULL_INT)
                {
                    var oldGlobalStatusMask = globalStatusMask;

                    if (adminSuspended)
                    {
                        globalStatusMask = globalStatusMask | GlobalStatusMask.AdminSuspended;
                    }
                    else
                    {
                        globalStatusMask = globalStatusMask & (~GlobalStatusMask.AdminSuspended);
                        //if not actually admin suspending, set reasonid = none
                        adminActionReasonID = AdminActionReasonID.None;
                    }

                    if (badEmail)
                    {
                        globalStatusMask = globalStatusMask | GlobalStatusMask.BouncedEmail;
                    }
                    else
                    {
                        globalStatusMask = globalStatusMask & (~GlobalStatusMask.BouncedEmail);
                    }

                    if (emailVerified)
                    {
                        globalStatusMask = globalStatusMask | GlobalStatusMask.VerifiedEmail;
                        SetVerifiedEmailAttributes(Member, brand);
                    }
                    else
                    {
                        globalStatusMask = globalStatusMask & (~GlobalStatusMask.VerifiedEmail);
                    }

                    //Update the GlobalStatusMask
                    _Member.SetAttributeInt(brand, WebConstants.ATTRIBUTE_NAME_GLOBALSTATUSMASK, (Int32) globalStatusMask);
                    MemberHelper.SetLastUpdateDateForAllSites(_Member);
                    var msr = MemberSA.Instance.SaveMember(_Member);

                    if (msr.SaveStatus == MemberSaveStatusType.Success)
                    {
                        // Log any of the mask flags that changed
                        var adminAction = AdminAction.Default;
                        if ((oldGlobalStatusMask & GlobalStatusMask.AdminSuspended) != (globalStatusMask & GlobalStatusMask.AdminSuspended))
                        {
                            if ((globalStatusMask & GlobalStatusMask.AdminSuspended) == GlobalStatusMask.AdminSuspended)
                                adminAction |= AdminAction.AdminSuspendMember;
                            else
                                adminAction |= AdminAction.AdminUnsuspendMember;
                        }

                        if ((oldGlobalStatusMask & GlobalStatusMask.VerifiedEmail) != (globalStatusMask & GlobalStatusMask.VerifiedEmail))
                        {
                            adminAction |= AdminAction.ChangedEmailVerified;
                        }

                        if ((oldGlobalStatusMask & GlobalStatusMask.BouncedEmail) != (globalStatusMask & GlobalStatusMask.BouncedEmail))
                        {
                            adminAction |= AdminAction.ChangedBadEmail;
                        }

                        AdminSA.Instance.AdminActionLogInsert(
                            _Member.MemberID
                            , Constants.GROUP_PERSONALS
                            , (Int32) adminAction
                            , g.AdminID, AdminMemberIDType.AdminProfileMemberID, adminActionReasonID);
                    }
                    else
                    {
                        //Update to GlobalStatusMask failed
                        succeeded = false;
                    }
                }
                else
                {
                    succeeded = false;
                }
            }

            return succeeded;
        }

        public void HideEmails(Brand brand)
        {
            AdminSA.Instance.AdminActionLogInsert(_Member.MemberID, brand.Site.Community.CommunityID, (int) AdminAction.BannedEmails, g.AdminID);
            EmailMessageSA.Instance.MarkDeletedSentByMember(_Member.MemberID, brand.Site.Community.CommunityID, true);
        }

        public void UpdateProfileInfo(string firstName, string lastName, DateTime birthdate,
            Enums.Gender gender, Enums.SeekingGender seekingGender)
        {
            var brand = g.GetBrand((int) _MemberSiteInfo.SiteID);
            _Member.SetAttributeText(brand, "SiteFirstName", firstName, TextStatusType.Human);
            _Member.SetAttributeText(brand, "SiteLastName", lastName, TextStatusType.Human);
            _Member.SetAttributeDate(brand, "Birthdate", birthdate);

            var currentGenderMask = _Member.GetAttributeInt(brand, "GenderMask");
            var newGenderMask = (int) gender + (int) seekingGender;

            if (currentGenderMask != newGenderMask)
            {
                _Member.SetAttributeInt(brand, "GenderMask", newGenderMask);
                var mmsett = Int32.Parse(RuntimeSettings.GetSetting("MatchTest_App_Settings", brand.Site.Community.CommunityID, brand.Site.SiteID));
                var mmenumval = (Int32) Enums.MatchMeterFlags.EnableApp;
                if ((mmsett & mmenumval) == mmenumval)
                {
                    MatchMeterSA.Instance.UpdateUserGender(_Member.MemberID, newGenderMask, brand);
                }
            }
            MemberSA.Instance.SaveMember(_Member, brand.Site.Community.CommunityID);
        }

        public void UpdateFraudInfo(bool subscriptionFraud, bool chatBanned, bool emailBanned, bool forceUnfraudEmails)
        {
            var brand = g.GetBrand((int) _MemberSiteInfo.SiteID);

            var passedFraud = subscriptionFraud ? 0 : 1;

            var currentFraudStatus = _Member.GetAttributeInt(brand, "PassedFraudCheckSite");
            var currentChatBanned = _Member.GetAttributeInt(brand, "ChatBanned");
            var currentEmailBanned = _Member.GetAttributeInt(brand, "EmailBan");

            var unfraudEmails = false;

            if (currentFraudStatus != passedFraud)
            {
                if (subscriptionFraud)
                {
                    AdminSA.Instance.AdminActionLogInsert(_Member.MemberID, brand.Site.Community.CommunityID, (int) AdminAction.ChangedFraudToYes, g.AdminID);
                }
                else
                {
                    AdminSA.Instance.AdminActionLogInsert(_Member.MemberID, brand.Site.Community.CommunityID, (int) AdminAction.ChangedFraudToNo, g.AdminID);
                    unfraudEmails = true;
                }
            }

            if (currentChatBanned != Convert.ToInt32(chatBanned))
            {
                if (chatBanned)
                {
                    AdminSA.Instance.AdminActionLogInsert(_Member.MemberID, brand.Site.Community.CommunityID, (int) AdminAction.BlockedFromChatRoom, g.AdminID);
                }
                else
                {
                    AdminSA.Instance.AdminActionLogInsert(_Member.MemberID, brand.Site.Community.CommunityID, (int) AdminAction.GrantedAccessToChatRoom, g.AdminID);
                }
            }

            if (currentEmailBanned != Convert.ToInt32(emailBanned))
            {
                if (emailBanned)
                {
                    HideEmails(brand);
                }
                else
                {
                    AdminSA.Instance.AdminActionLogInsert(_Member.MemberID, brand.Site.Community.CommunityID, (int) AdminAction.UnbannedEmails, g.AdminID);
                    EmailMessageSA.Instance.MarkDeletedSentByMember(_Member.MemberID, brand.Site.Community.CommunityID, false);
                }
            }

            _Member.SetAttributeInt(brand, "PassedFraudCheckSite", passedFraud);
            _Member.SetAttributeInt(brand, "ChatBanned", Convert.ToInt32(chatBanned));
            _Member.SetAttributeInt(brand, "EmailBan", Convert.ToInt32(emailBanned));
            MemberSA.Instance.SaveMember(_Member, brand.Site.Community.CommunityID);

            if (forceUnfraudEmails)
            {
                // This version does NOT respect the "EMAIL_FRAUD_HOLD" setting before unfrauding emails.
                EmailMessageSA.Instance.MarkOffFraudHoldSentByMember(_Member.MemberID, brand.Site.Community.CommunityID, false);
                //string traceString = EmailMessageSA.Instance.MarkOffFraudHoldSentByMember(_Member.MemberID, brand.Site.Community.CommunityID, true);
                //_logger.Debug(traceString);
            }
            else
            {
                // after saving the attributes do some post processing like unfrauding emails
                if (unfraudEmails)
                {
                    // This version does respect the "EMAIL_FRAUD_HOLD" setting before unfrauding emails, which means no email will
                    // be unfrauded if the setting is off.
                    EmailMessageSA.Instance.MarkOffFraudHoldSentByMember(_Member.MemberID, brand.Site.Community.CommunityID, brand.Site.SiteID);
                }
            }
        }

        public void UpdateOffSiteNotifications(int matchmailFrequency, int clickmailFrequency, int emailAlertMaskValues, int newsletterMaskValues)
        {
            var brand = g.GetBrand((int) _MemberSiteInfo.SiteID);

            _Member.SetAttributeInt(brand, "MatchNewsletterPeriod", matchmailFrequency);
            _Member.SetAttributeInt(brand, "GotAClickEmailPeriod", clickmailFrequency);
            _Member.SetAttributeInt(brand, EmailAlertConstant.ALERT_ATTRIBUTE_NAME, emailAlertMaskValues);
            _Member.SetAttributeInt(brand, "NewsletterMask", newsletterMaskValues);
            MemberSA.Instance.SaveMember(_Member, brand.Site.Community.CommunityID);
        }

        public void UpdatePremiumServiceSettings(bool highlightEnabled, bool spotlightEnabled, int spotlightGender,
            int spotlightSeekingGender, int spotlightMinAge, int spotlightMaxAge,
            int spotlightDistance, int spotlightRegionID)
        {
            var settings = new PremiumServiceSettings();
            var brand = g.GetBrand((int) SiteID);

            var premiumServices = ServiceMemberSA.Instance.GetMemberServices(brand.BrandID, _Member.MemberID);

            var highlightMember = premiumServices.GetMemberServiceInstance((int) ServiceConstants.ServiceIDs.HighligtedProfile);
            var spotlightMember = premiumServices.GetMemberServiceInstance((int) ServiceConstants.ServiceIDs.SpotlightMember);

            if (highlightMember != null)
            {
                highlightMember.Attributes.Add<bool>((int) ServiceConstants.Attributes.EnableFlag, highlightEnabled);
                ServiceMemberSA.Instance.SaveMemberService(brand, Member.MemberID, highlightMember);
            }

            if (spotlightMember != null)
            {
                spotlightMember.Attributes.Add<bool>((int) ServiceConstants.Attributes.EnableFlag, spotlightEnabled);
                spotlightMember.Attributes.Add<int>((int) ServiceConstants.Attributes.AgeRange, spotlightMinAge, default(int));
                spotlightMember.Attributes.Add<int>((int) ServiceConstants.Attributes.AgeRange, default(int), spotlightMaxAge);
                spotlightMember.Attributes.Add<int>((int) ServiceConstants.Attributes.Distance, spotlightDistance);
                spotlightMember.Attributes.Add<int>((int) ServiceConstants.Attributes.GenderMask, spotlightGender + spotlightSeekingGender);
                spotlightMember.Attributes.Add<int>((int) ServiceConstants.Attributes.RegionID, spotlightRegionID);
                ServiceMemberSA.Instance.SaveMemberService(brand, Member.MemberID, spotlightMember);
            }
        }

        public void UpdateOtherActions(bool colorAnalysisPurchase)
        {
            var brand = g.GetBrand((int) _MemberSiteInfo.SiteID);

            var newColorAnalysisPurchaseValue = colorAnalysisPurchase ? 1 : 0;

            //Save to Unified Access Service
            if (GeneralHelper.IsUPSAccessUpdateEnabled(brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID))
            {
                var accessServiceCient = AccessServiceWebAdapter.GetProxyInstanceForBedrock();
                var accessResponse = accessServiceCient.AdjustBinaryPrivilege(_Member.MemberID, new[] {(int) PrivilegeType.ColorAnalysis}, (newColorAnalysisPurchaseValue == 1) ? true : false, g.AdminID, g.AdminDomainAccount, Constants.NULL_INT, brand.Site.SiteID, 1);
                if (accessResponse.InternalResponse.responsecode != "0")
                    throw new Exception("Failed to update with Unified Access, please try again or contact Software Engineering.");
            }

            //update member attribute
            _Member.SetAttributeInt(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, "ColorAnalysis", newColorAnalysisPurchaseValue);

            MemberSA.Instance.SaveMember(_Member);
        }

        public void UpdateRamahAction(bool ramahDate)
        {
            var brand = g.GetBrand((int) _MemberSiteInfo.SiteID);

            //lets take care of member ramah setting. 
            if (ramahDate)
            {
                Member.SetAttributeInt(brand, "RamahAlum", 1);
            }
            else
            {
                Member.SetAttributeInt(brand, "RamahAlum", 0);
            }


            MemberSA.Instance.SaveMember(_Member);
        }

        public bool EndAutoRenewal()
        {
            //bool success = true;

            //BrandConfig.Brand brand = g.GetBrand((int)_MemberSiteInfo.SiteID);

            //int terminationReasonID = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultTerminationReason"]);

            ////Update legacy
            ////Purchase.SubscriptionResult sr = PurchaseSA.Instance.EndSubscription( _Member.MemberID, g.AdminID, brand.Site.SiteID, terminationReasonID);

            ////if (sr.ReturnValue != Constants.RETURN_OK)
            ////{
            ////    success = false;
            ////}

            ////Update UPS Renewal
            //Spark.Common.RenewalService.RenewalResponse renewalResponse = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().DisableAutoRenewal(_Member.MemberID,
            //        brand.Site.SiteID, 1, g.AdminID, g.AdminDomainAccount, Constants.NULL_INT);

            //if (renewalResponse.InternalResponse.responsecode != "0")
            //{
            //    success = false;
            //    throw new Exception("Failed to update with Unified Renewal to disable auto-renewal for MemberID: " + _Member.MemberID.ToString() + ", please try again or contact Software Engineering.");
            //}

            //return success;

            return EndAutoRenewalWithReason(Constants.NULL_INT);
        }

        public bool EndAutoRenewalWithReason(int terminationReasonID)
        {
            var success = true;
            var brand = g.GetBrand((int) _MemberSiteInfo.SiteID);

            try
            {
                //Update UPS Renewal
                var renewalResponse = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().DisableAutoRenewal(_Member.MemberID,
                    brand.Site.SiteID, 1, g.AdminID, g.AdminDomainAccount, terminationReasonID);

                if (renewalResponse.InternalResponse.responsecode != "0")
                {
                    success = false;
                    throw new Exception("Failed to update with Unified Renewal to disable auto-renewal for MemberID: " + _Member.MemberID + ", please try again or contact Software Engineering.");
                }
            }
            catch (Exception)
            {
                success = false;
                throw new Exception("Failed to update with Unified Renewal to disable auto-renewal for MemberID: " + _Member.MemberID + ", please try again or contact Software Engineering.");
            }

            return success;
        }

        public bool ReopenAutoRenewal(bool reopenMainSubscription, bool reopenHighlightALC, bool reopenSpotlightALC, bool reopenAllAccessALC, bool reopenReadReceiptALC)
        {
            var success = true;
            var brand = g.GetBrand((int) _MemberSiteInfo.SiteID);
            var SecondaryPackageIDs = new List<int>();
            var basicSubscriptionEnabled = false;

            var renewalSub = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetCurrentRenewalSubscription(_Member.MemberID, brand.Site.SiteID);

            if (renewalSub != null && renewalSub.RenewalSubscriptionID > 0)
            {
                if (renewalSub.IsRenewalEnabled)
                {
                    basicSubscriptionEnabled = true;
                }

                RenewalSubscriptionDetail rseHighlight = null;
                RenewalSubscriptionDetail rseSpotlight = null;
                RenewalSubscriptionDetail rseJMeter = null;
                RenewalSubscriptionDetail rseAllAccess = null;
                RenewalSubscriptionDetail rseReadReceipts = null;

                var spotlightPlanID = GeneralHelper.GetPlanFromRenewalSubscriptionDetail(renewalSub.RenewalSubscriptionDetails, Purchase.PremiumType.SpotlightMember, brand, out rseSpotlight);
                var highlightPlanID = GeneralHelper.GetPlanFromRenewalSubscriptionDetail(renewalSub.RenewalSubscriptionDetails, Purchase.PremiumType.HighlightedProfile, brand, out rseHighlight);
                var jMeterPlanID = GeneralHelper.GetPlanFromRenewalSubscriptionDetail(renewalSub.RenewalSubscriptionDetails, Purchase.PremiumType.JMeter, brand, out rseJMeter);
                var allAccessPlanID = GeneralHelper.GetPlanFromRenewalSubscriptionDetail(renewalSub.RenewalSubscriptionDetails, Purchase.PremiumType.AllAccess, brand, out rseAllAccess);
                var readReceiptPlanID = GeneralHelper.GetPlanFromRenewalSubscriptionDetail(renewalSub.RenewalSubscriptionDetails, Purchase.PremiumType.ReadReceipt, brand, out rseReadReceipts);

                //update Spark.Renewal (UPS)
                if (basicSubscriptionEnabled || reopenMainSubscription)
                {
                    if (reopenHighlightALC && highlightPlanID > 0)
                    {
                        SecondaryPackageIDs.Add(highlightPlanID);
                    }

                    if (reopenSpotlightALC && spotlightPlanID > 0)
                    {
                        SecondaryPackageIDs.Add(spotlightPlanID);
                    }

                    if (reopenAllAccessALC && allAccessPlanID > 0)
                    {
                        SecondaryPackageIDs.Add(allAccessPlanID);
                    }

                    if (reopenReadReceiptALC && readReceiptPlanID > 0)
                    {
                        SecondaryPackageIDs.Add(readReceiptPlanID);
                    }

                    RenewalResponse renewalResponse = null;
                    if (!basicSubscriptionEnabled)
                    {
                        //reopen auto-renewal (basic sub + a la carte)
                        renewalResponse = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().EnableAutoRenewal(SecondaryPackageIDs.ToArray(),
                            _Member.MemberID, brand.Site.SiteID,
                            1, g.AdminID, g.AdminDomainAccount, Constants.NULL_INT);
                    }
                    else if (SecondaryPackageIDs.Count > 0)
                    {
                        //reopen auto-renewal (a la carte only)
                        renewalResponse = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().EnableALaCarteAutoRenewal(SecondaryPackageIDs.ToArray(),
                            _Member.MemberID, brand.Site.SiteID,
                            1, g.AdminID, g.AdminDomainAccount, Constants.NULL_INT);
                    }

                    if (renewalResponse != null && renewalResponse.InternalResponse.responsecode != "0")
                    {
                        throw new Exception("Failed to update with Unified Renewal with enabling auto-renewal for MemberID: " + _Member.MemberID + ", please try again or contact Software Engineering.");
                    }
                    success = true;
                }
            }
            else
            {
                success = false;
            }

            return success;
        }

        public void ClearColorCodeResults()
        {
            var brand = g.GetBrand((int) _MemberSiteInfo.SiteID);

            //save quiz answers
            var quizXml = "null";
            _Member.SetAttributeText(brand, WebConstants.ATTRIBUTE_NAME_COLORCODEQUIZANSWERS, quizXml, TextStatusType.Auto);

            //save primary color
            _Member.SetAttributeText(brand, WebConstants.ATTRIBUTE_NAME_COLORCODEPRIMARYCOLOR, Enums.Color.none.ToString(), TextStatusType.Auto);

            //save secondary color
            _Member.SetAttributeText(brand, WebConstants.ATTRIBUTE_NAME_COLORCODESECONDARYCOLOR, Enums.Color.none.ToString(), TextStatusType.Auto);

            //save scores
            _Member.SetAttributeText(brand, WebConstants.ATTRIBUTE_NAME_COLORCODEBLUESCORE, "0", TextStatusType.Auto);
            _Member.SetAttributeText(brand, WebConstants.ATTRIBUTE_NAME_COLORCODEREDSCORE, "0", TextStatusType.Auto);
            _Member.SetAttributeText(brand, WebConstants.ATTRIBUTE_NAME_COLORCODEWHITESCORE, "0", TextStatusType.Auto);
            _Member.SetAttributeText(brand, WebConstants.ATTRIBUTE_NAME_COLORCODEYELLOWSCORE, "0", TextStatusType.Auto);

            MemberSA.Instance.SaveMember(_Member);
            AdminSA.Instance.AdminActionLogInsert(_Member.MemberID, brand.Site.Community.CommunityID, (int) AdminAction.ResetColorCode, g.AdminID);
        }

        public void LogAdminAction(int groupID, int adminAction)
        {
            //adminID should never be less than zero except when testing locally with AD turned off (which then does not require AdminID to exists in mapping)
            if (adminAction > 0)
            {
                AdminSA.Instance.AdminActionLogInsert(
                    _Member.MemberID
                    , groupID
                    , adminAction
                    , g.AdminID);
            }
        }

        private void SetVerifiedEmailAttributes(Member member, Brand brand)
        {
            var blockingmask = Enums.ForcedBlockingMask.NotBlocked;
            ;
            int hideMask;

            blockingmask = (Enums.ForcedBlockingMask) Enum.Parse(typeof (Enums.ForcedBlockingMask), member.GetAttributeInt(brand, "BlockingMask", 0).ToString());

            if ((blockingmask & Enums.ForcedBlockingMask.BlockedAfterEmailChange) == Enums.ForcedBlockingMask.BlockedAfterEmailChange)
            {
                blockingmask = blockingmask & (~Enums.ForcedBlockingMask.BlockedAfterEmailChange);
            }

            if ((blockingmask & Enums.ForcedBlockingMask.BlockedAfterReg) == Enums.ForcedBlockingMask.BlockedAfterReg)
            {
                blockingmask = blockingmask & (~Enums.ForcedBlockingMask.BlockedAfterReg);
            }

            hideMask = member.GetAttributeInt(brand, "HideMask", 0);

            if ((blockingmask & Enums.ForcedBlockingMask.HideFromMOL) == Enums.ForcedBlockingMask.HideFromMOL)
            {
                hideMask = hideMask & ~(int) Enums.AttributeOptionHideMask.HideMembersOnline;
                blockingmask = blockingmask & ~Enums.ForcedBlockingMask.HideFromMOL;
            }
            if ((blockingmask & Enums.ForcedBlockingMask.HideFromSearch) == Enums.ForcedBlockingMask.HideFromSearch)
            {
                hideMask = hideMask & ~(int) Enums.AttributeOptionHideMask.HideSearch;
                blockingmask = blockingmask & ~Enums.ForcedBlockingMask.HideFromSearch;
            }
            member.SetAttributeInt(brand, "HideMask", hideMask);
            member.SetAttributeInt(brand, "BlockingMask", (int) blockingmask);
            member.SetAttributeDate(brand, "EmailVerificationDate", DateTime.Now);
        }

        public void UpdateSubscriptionLastInitialPurchaseDate(DateTime newDate)
        {
            var brand = GeneralHelper.GetBrandForSite((int) SiteID);
            Member.SetAttributeDate(brand, "SubscriptionLastInitialPurchaseDate", newDate);
            MemberSA.Instance.SaveMember(Member);
        }

        internal SearchPreferencesData GetEditSearchPreferences()
        {
            var data = new SearchPreferencesData();
            data.MemberSiteInfo = _MemberSiteInfo;
            data.MemberID = MemberID;

            var brand = g.GetBrand((int) SiteID);
            var memberSearch = MemberSearchCollection.Load(MemberID, brand, true).PrimarySearch;

            MemberHelper.PopulateSearchPreferencesDataFromMemberSearch(data, memberSearch, brand, MemberID, EDIT_SEARCH_PREF_REGION_ID_FORM_FIELD,
                EDIT_SEARCH_PREF_COUNTRY_REGION_ID_FORM_FIELD, EDIT_SEARCH_PREF_SEARCH_TYPE_FORM_FIELD, EDIT_SEARCH_PREF_GENDER_PICKER_FORM_FIELD, false);

            return data;
        }

        internal SearchPreferencesData UpdateMemberSearchPreferences(FormCollection collection)
        {
            var data = new SearchPreferencesData();
            data.MemberSiteInfo = _MemberSiteInfo;
            data.MemberID = MemberID;

            var brand = g.GetBrand((int) SiteID);

            var memberSearch = MemberSearchCollection.Load(MemberID, brand, true).PrimarySearch;

            string valueHolder;
            // Age
            if (GeneralHelper.TryGetFromFormCollection(collection, "MinAge", out valueHolder))
            {
                memberSearch.Age.MinValue = Convert.ToInt32(valueHolder);
            }

            if (GeneralHelper.TryGetFromFormCollection(collection, "MaxAge", out valueHolder))
            {
                memberSearch.Age.MaxValue = Convert.ToInt32(valueHolder);
            }

            // Gender
            if (GeneralHelper.TryGetFromFormCollection(collection, EDIT_SEARCH_PREF_GENDER_PICKER_FORM_FIELD, out valueHolder))
            {
                var genderMask = Convert.ToInt32(valueHolder);
                var gender = GeneralHelper.GetGenderFromMask(genderMask);
                if (gender == Enums.Gender.Male)
                {
                    memberSearch.Gender = Gender.Male;
                }
                else
                {
                    memberSearch.Gender = Gender.Female;
                }

                var seekingGender = GeneralHelper.GetSeekingGenderFromMask(genderMask);
                if (seekingGender == Enums.SeekingGender.Male)
                {
                    memberSearch.SeekingGender = Gender.Male;
                }
                else
                {
                    memberSearch.SeekingGender = Gender.Female;
                }
            }

            // Distance
            if (GeneralHelper.TryGetFromFormCollection(collection, "SearchDistance", out valueHolder))
            {
                memberSearch.Distance = Convert.ToInt32(valueHolder);
            }

            // Region
            if (GeneralHelper.TryGetFromFormCollection(collection, EDIT_SEARCH_PREF_SEARCH_TYPE_FORM_FIELD, out valueHolder))
            {
                memberSearch.SearchType = (SearchType) Enum.Parse(typeof (SearchType), valueHolder);
            }

            // Keyword search pref
            if (GeneralHelper.TryGetFromFormCollection(collection, "KeywordSearch", out valueHolder))
            {
                memberSearch.KeywordSearch = valueHolder;
            }

            if (GeneralHelper.TryGetFromFormCollection(collection, EDIT_SEARCH_PREF_REGION_ID_FORM_FIELD, out valueHolder))
            {
                if (memberSearch.SearchType == SearchType.AreaCode)
                {
                    // for area codes, regionID container actually contains area codes delimited by commas
                    var splits = valueHolder.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries);
                    for (var i = 1; i <= 6; i++)
                    {
                        var areaCodePreference = memberSearch[Preference.GetInstance("AreaCode" + i + 1)] as MemberSearchPreferenceInt;
                        if ((i - 1) < splits.Length)
                        {
                            areaCodePreference.Value = Convert.ToInt32(splits[i - 1]);
                        }
                        else
                        {
                            // if user specified less than 6, fill in the rest with NULL_INT
                            areaCodePreference.Value = Constants.NULL_INT;
                        }
                    }
                }
                else
                {
                    memberSearch.RegionID = Convert.ToInt32(valueHolder);
                }
            }

            // has photos
            if (GeneralHelper.TryGetFromFormCollection(collection, "HasPhotos", out valueHolder))
            {
                memberSearch.HasPhoto = valueHolder.Contains("true");
            }

            // height
            var mspHeight = MemberSearchPreference.CreateInstance(memberSearch.MemberSearchID, Preference.GetInstance("Height")) as MemberSearchPreferenceRange;
            if (GeneralHelper.TryGetFromFormCollection(collection, "MinHeight", out valueHolder))
            {
                mspHeight.MinValue = Convert.ToInt32(valueHolder);
            }
            else
            {
                mspHeight.MinValue = Constants.NULL_INT;
            }

            if (GeneralHelper.TryGetFromFormCollection(collection, "MaxHeight", out valueHolder))
            {
                mspHeight.MaxValue = Convert.ToInt32(valueHolder);
            }
            else
            {
                mspHeight.MaxValue = Constants.NULL_INT;
            }
            memberSearch[Preference.GetInstance("Height")] = mspHeight;

            // search preference fields from the xml file
            MemberHelper.GetDynamicSearchPreferences(data, brand.Site.SiteID);

            MemberHelper.PopulateMemberSearchForDynamicPortion(memberSearch, collection, data.BasicSearchPrefs);
            MemberHelper.PopulateMemberSearchForDynamicPortion(memberSearch, collection, data.AdditionalSearchPrefs);

            // save if valid
            string errorMessage;
            if (ValidateUpdateEditSearchPreferences(collection, out errorMessage))
            {
                memberSearch.Save();
                data.UIMessage = "success";
            }
            else
            {
                data.UIMessage = errorMessage;
            }

            // rebind the form to the new data
            MemberHelper.PopulateSearchPreferencesDataFromMemberSearch(data, memberSearch, brand, MemberID, EDIT_SEARCH_PREF_REGION_ID_FORM_FIELD,
                EDIT_SEARCH_PREF_COUNTRY_REGION_ID_FORM_FIELD, EDIT_SEARCH_PREF_SEARCH_TYPE_FORM_FIELD, EDIT_SEARCH_PREF_GENDER_PICKER_FORM_FIELD, true);

            return data;
        }

        private bool ValidateUpdateEditSearchPreferences(FormCollection collection, out string errorMessage)
        {
            var minAge = Constants.NULL_INT;
            var maxAge = Constants.NULL_INT;
            var minHeight = Constants.NULL_INT;
            var maxHeight = Constants.NULL_INT;
            string valueHolder;
            var isValid = true;
            errorMessage = string.Empty;
            var invalidFields = new ArrayList();

            if (GeneralHelper.TryGetFromFormCollection(collection, "MinAge", out valueHolder))
            {
                minAge = Convert.ToInt32(valueHolder);
            }

            if (GeneralHelper.TryGetFromFormCollection(collection, "MaxAge", out valueHolder))
            {
                maxAge = Convert.ToInt32(valueHolder);
            }

            if (GeneralHelper.TryGetFromFormCollection(collection, "MinHeight", out valueHolder))
            {
                minHeight = Convert.ToInt32(valueHolder);
            }

            if (GeneralHelper.TryGetFromFormCollection(collection, "MaxHeight", out valueHolder))
            {
                maxHeight = Convert.ToInt32(valueHolder);
            }

            if (minAge != Constants.NULL_INT && maxAge != Constants.NULL_INT)
            {
                if (minAge > maxAge)
                {
                    isValid = false;
                    invalidFields.Add("Age range");
                }
            }

            if (minHeight != Constants.NULL_INT && maxHeight != Constants.NULL_INT)
            {
                if (minHeight > maxHeight)
                {
                    isValid = false;
                    invalidFields.Add("Height range");
                }
            }

            if (!isValid)
            {
                errorMessage = string.Join(",", (string[]) invalidFields.ToArray(Type.GetType("System.String")));
                errorMessage += " invalid";
            }

            return isValid;
        }

        internal bool ResetJMeter()
        {
            var mem = MemberHelper.GetMember(MemberID, MemberLoadFlags.None);
            return MatchMeterSA.Instance.ResetPersonalityTest(MemberID,
                mem.IsPayingMember((int) MemberSiteInfo.SiteID),
                g.GetBrand((int) MemberSiteInfo.SiteID));
        }

        internal void SyncWithZoozamen()
        {
            var mem = MemberHelper.GetMember(MemberID, MemberLoadFlags.None);
            MatchMeterSA.Instance.SetMatchTestStatusToDefault(mem, g.GetBrand((int) MemberSiteInfo.SiteID));
        }

        internal Enums.Status UpdateILAuthentication(bool disableILAuth, DateTime iLAuthExpDate, out string ilAuthStatus)
        {
            var expDate = disableILAuth ? DateTime.MinValue : iLAuthExpDate;
            var updateStatus = Enums.Status.Success;


            _Member.SetAttributeDate(g.GetBrand((int) _MemberSiteInfo.SiteID), "PremiumAuthenticatedExpirationDate",
                expDate);

            var memberSaveResult = MemberSA.Instance.SaveMember(_Member);

            if (memberSaveResult.SaveStatus == MemberSaveStatusType.Success)
            {
                ilAuthStatus = expDate == DateTime.MinValue ? "Currently INACTIVE" : "Currently ACTIVE";
            }
            else
            {
                ilAuthStatus = string.Empty;
                updateStatus = Enums.Status.Failed;
            }

            return updateStatus;
        }

        public string GenerateStealthLoginLink(int adminMemberId)
        {
            var brand = g.GetBrand((int) MemberSiteInfo.SiteID);
            var token = MemberSA.Instance.GenerateImpersonateToken(adminMemberId, MemberID, brand.BrandID);
            var env = string.Empty;
            var prefix = string.Empty;

            try
            {
                env = RuntimeSettings.GetSetting("ENVIRONMENT_TYPE");
                prefix = env.Trim().ToLower() == "dev" ? "local" : env.Trim().ToLower();
            }
            catch (Exception ex)
            {
                // this means, setting doesn't exists which means it's prod
                prefix = "www";
            }

            return string.Format("http://{0}.{1}/Applications/Logon/Logon.aspx?imptoken={2}",
                prefix, brand.Uri, HttpUtility.UrlEncode(token));
        }

        public string UpdateALaCarteRenewal(List<int> premiumTypes, bool enable)
        {
            var errorMessage = string.Empty;
            var renewalSub = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetCurrentRenewalSubscription(MemberID, (int) SiteID);
            if (renewalSub == null || renewalSub.RenewalSubscriptionID <= 0)
                return string.Empty;

            if (renewalSub.RenewalSubscriptionDetails == null)
                return string.Empty;

            var brand = g.GetBrand((int) MemberSiteInfo.SiteID);
            var packageIds = new List<int>();

            foreach (var rse in renewalSub.RenewalSubscriptionDetails)
            {
                if (rse.IsPrimaryPackage)
                    continue;

                var rsePlan = PlanSA.Instance.GetPlan(rse.PackageID, brand.BrandID);
                if (rsePlan == null)
                    continue;

                if (premiumTypes.Any(premT => (rsePlan.PremiumTypeMask & (Purchase.PremiumType) premT) == (Purchase.PremiumType) premT))
                {
                    packageIds.Add(rse.PackageID);
                }
            }

            if (packageIds.Count > 0)
            {
                RenewalResponse renewalResponse = null;

                if (enable)
                {
                    renewalResponse = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().EnableALaCarteAutoRenewal(packageIds.ToArray(), MemberID, (int) SiteID,
                        (int) SystemType.System,
                        g.AdminID, g.AdminDomainAccount,
                        Constants.NULL_INT);
                }
                else
                {
                    renewalResponse = RenewalServiceWebAdapter.GetProxyInstanceForBedrock().DisableALaCarteAutoRenewal(packageIds.ToArray(), MemberID, (int) SiteID,
                        (int) SystemType.System,
                        g.AdminID, g.AdminDomainAccount,
                        Constants.NULL_INT);
                }

                if (renewalResponse.InternalResponse.responsecode != "0")
                {
                    errorMessage = string.Format("Renewal Service encounterd an error with the error message --> {0}", renewalResponse.InternalResponse.responsemessage);
                }
            }

            return errorMessage;
        }

        public string CancelFreeTrial()
        {
            var errorMessage = string.Empty;

            // Get the active free trial subscription for this member 
            AuthorizationSubscription freeTrialSubscription = null;
            var hasActiveFreeTrial = false;

            try
            {
                var defaultPaymentProfile = PaymentProfileServiceWebAdapter.GetProxyInstance().GetMemberDefaultPaymentProfile(MemberID, (int) SiteID);

                if (defaultPaymentProfile != null)
                {
                    freeTrialSubscription = AuthorizationServiceWebAdapter.GetProxyInstance().GetActiveAuthorizationSubscription(defaultPaymentProfile.PaymentProfileID, MemberID, (int) SiteID);
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Error in getting the free trial subscription details, Error message: " + ex.Message);
            }
            finally
            {
                PaymentProfileServiceWebAdapter.CloseProxyInstance();
                AuthorizationServiceWebAdapter.CloseProxyInstance();
            }

            if (freeTrialSubscription != null)
            {
                if (freeTrialSubscription.CaptureDateUTC >= DateTime.UtcNow)
                {
                    hasActiveFreeTrial = true;
                }
            }

            if (hasActiveFreeTrial)
            {
                var accessFreeTrial = new AccessFreeTrial();
                accessFreeTrial.AuthorizationSubscriptionID = freeTrialSubscription.AuthorizationSubscriptionID;
                accessFreeTrial.AuthorizationCode = freeTrialSubscription.AuthorizationCode;
                accessFreeTrial.AuthorizationChargeID = freeTrialSubscription.AuthorizationChargeID;
                accessFreeTrial.AuthorizationAmount = freeTrialSubscription.AuthorizationAmount;
                accessFreeTrial.PaymentProfileID = freeTrialSubscription.PaymentProfileID;
                accessFreeTrial.PaymentType = "CreditCard";
                accessFreeTrial.CustomerID = freeTrialSubscription.CustomerID;
                accessFreeTrial.CallingSystemID = freeTrialSubscription.CallingSystemID;
                accessFreeTrial.CallingSystemTypeID = 2;
                accessFreeTrial.CaptureDate = freeTrialSubscription.CaptureDateUTC;
                accessFreeTrial.CaptureAttempt = freeTrialSubscription.CaptureAttempt;
                accessFreeTrial.AuthorizationOnlyPackageID = freeTrialSubscription.PrimaryPackageID;
                accessFreeTrial.SecondaryAuthorizationOnlyPackageID = freeTrialSubscription.AllSecondaryPackageID;
                accessFreeTrial.AdminID = 0;
                accessFreeTrial.AdminUserName = String.Empty;
                accessFreeTrial.TransactionType = Spark.Common.AccessService.TransactionType.CancelFreeTrial;
                accessFreeTrial.AuthorizationStatus = AuthorizationStatus.Successful;
                accessFreeTrial.ConvertedOrderID = 0;
                accessFreeTrial.AccessReasonID = 0;
                accessFreeTrial.UnifiedActionTypeID = 2; // Remove privileges

                var accessResponse = AccessServiceWebAdapter.GetProxyInstance().AdjustAuthorizationOnlyPrivilege(accessFreeTrial);

                if (accessResponse.InternalResponse.responsecode != "0")
                {
                    errorMessage = string.Format("Renewal Service encounterd an error with the error message --> {0}", accessResponse.InternalResponse.responsemessage);
                }
            }

            return errorMessage;
        }

        public QASupport GetQASupport()
        {
            var memberData = new QASupport();
            memberData.MemberID = _Member.MemberID;
            memberData.MemberSiteInfo = _MemberSiteInfo;
            memberData.brand = g.GetBrand((int) SiteID);
            memberData.TopNav = GetTopNav();
            memberData.MemberLeft = GetMemberLeft();
            memberData.Member = _Member;
            try
            {
                memberData.BBE_PROMOTION_EXPIRATION = Convert.ToDateTime(RuntimeSettings.GetSetting("BBE_PROMOTION_EXPIRATION", memberData.brand.Site.Community.CommunityID, memberData.brand.Site.SiteID, memberData.brand.BrandID));
            }
            catch
            {
                //missing setting
                memberData.BBE_PROMOTION_EXPIRATION = DateTime.MinValue;
            }

            try
            {
                memberData.IOS_IAP_Receipt_Original_TransactionID = _Member.GetAttributeText(memberData.brand, "IOS_IAP_Receipt_Original_TransactionID");
                memberData.HasRamahAlumBadge = Convert.ToBoolean(_Member.GetAttributeInt(memberData.brand, "RamahAlum"));
                memberData.RamahStartYear = _Member.GetAttributeInt(memberData.brand, "RamahStartYear");
                memberData.RamahEndYear = _Member.GetAttributeInt(memberData.brand, "RamahEndYear");

                var ramahCampInt = Member.GetAttributeInt(memberData.brand, "RamahCamp");

                if (ramahCampInt != Constants.NULL_INT && ramahCampInt > 0)
                {
                    var sb = new StringBuilder();
                    var reHydratedMask = (QASupport.Camps) ramahCampInt;

                    foreach (int i in Enum.GetValues(typeof (QASupport.Camps)))
                    {
                        if ((reHydratedMask & (QASupport.Camps) i) == (QASupport.Camps) i)
                        {
                            if ((QASupport.Camps) i == QASupport.Camps.None)
                            {
                                //ignore
                            }
                            else
                                sb.Append(((QASupport.Camps) i)).Append("<br/>");
                        }
                    }

                    memberData.RamahCamp = sb.ToString();
                }
            }
            catch
            {
                //missing community/site attribute
                memberData.IOS_IAP_Receipt_Original_TransactionID = "";
            }

            return memberData;
        }

        public MigrationSupport GetMigrationSupportModelView()
        {
            var migrationSupport = new MigrationSupport();

            migrationSupport.MemberID = _Member.MemberID;
            migrationSupport.MemberSiteInfo = _MemberSiteInfo;
            migrationSupport.brand = g.GetBrand((int) SiteID);
            migrationSupport.TopNav = GetTopNav();
            migrationSupport.MemberLeft = GetMemberLeft();
            migrationSupport.Member = _Member;

            var migrationManager = new MigrationManager();
            migrationSupport.IntegerAttributes = migrationManager.GetMigratedIntegerAttributes(_Member, migrationSupport.brand).OrderBy(a => a.BHAttributeName).ToList();
            migrationSupport.TextAttributes = migrationManager.GetMigratedTextAttributes(_Member, migrationSupport.brand).OrderBy(a => a.BHAttributeName).ToList();
            migrationSupport.DateAttributes = migrationManager.GetMigratedDateAttributes(_Member, migrationSupport.brand).OrderBy(a => a.BHAttributeName).ToList();
            migrationSupport.SearchPreferences =
                migrationManager.GetMigratedSearchPreferences(_Member, migrationSupport.brand).OrderBy(sp => sp.BHPreferenceName).ToList();

            return migrationSupport;
        }

        public ListMigrationModelView GetListMigrationSupportModelView()
        {
            var migrationSupport = new ListMigrationModelView
            {
                MemberID = _Member.MemberID,
                MemberSiteInfo = _MemberSiteInfo,
                brand = g.GetBrand((int) SiteID),
                TopNav = GetTopNav(),
                MemberLeft = GetMemberLeft(),
                Member = _Member
            };

            var migrationManager = new MigrationManager();

            migrationSupport.ListData = migrationManager.GetMigratedListItemsByCategory(_Member, migrationSupport.brand, 1, 10000);

            return migrationSupport;
        }

        public PhotoMigrationModelView GetPhotoMigrationSupportModelView()
        {
            var modelView = new PhotoMigrationModelView
            {
                MemberID = _Member.MemberID,
                MemberSiteInfo = _MemberSiteInfo,
                Brand = g.GetBrand((int)SiteID),
                TopNav = GetTopNav(),
                MemberLeft = GetMemberLeft(),
                Member = _Member,
                Photos = new List<Photo>()
            };

            var migrationManager = new MigrationManager();

            var communityPhotos = migrationManager.GetPhotos(_Member, modelView.Brand);

            foreach (var communityPhoto in communityPhotos)
            {
                modelView.Photos.Add(new Photo
                {
                    MemberPhotoId = communityPhoto.MemberPhotoID,
                    FullCloudPath = MemberPhotoHelper.GetPhotoDisplayUrl(PhotoType.Full,
                        communityPhoto, modelView.Brand.Site.Community.CommunityID,
                        modelView.Brand.Site.SiteID),
                    ThumbCloudPath = MemberPhotoHelper.GetPhotoDisplayUrl(PhotoType.Thumbnail,
                        communityPhoto, modelView.Brand.Site.Community.CommunityID,
                        modelView.Brand.Site.SiteID),
                    AdminMemberId = communityPhoto.AdminMemberID,
                    IsApproved = communityPhoto.IsApproved
                });
            }

            return modelView;
        }

        public bool GiveLifetimeMembership(out string ErrorMessage)
        {
            ErrorMessage = string.Empty;
            var isValid = true;

            var currentAccess =
                AccessServiceWebAdapter.GetProxyInstanceForBedrock()
                    .GetCustomerPrivilege(_Member.MemberID, (int) SiteID,
                        (int) PrivilegeType.BasicSubscription);
            // check to see if the end date is 23 hours from now
            if (currentAccess == null || currentAccess.EndDatePST < DateTime.Now.AddHours(23))
            {
                isValid = false;
                ErrorMessage = "Please add 1 day to this member's basic subscription and try again.";
            }

            if (!isValid)
                return false;

            // check to see if this member has a renewal record. this is required for Paul's script to work.
            var renewalSub = RenewalServiceWebAdapter.GetProxyInstanceForBedrock()
                .GetCurrentRenewalSubscription(_Member.MemberID, (int) SiteID);

            if (renewalSub != null && renewalSub.RenewalSubscriptionID > 0 && renewalSub.PrimaryPackageID > 0)
            {
                AccessServiceWebAdapter.GetProxyInstanceForBedrock()
                    .InsertLifetimeMember(_Member.MemberID, (int) SiteID, g.AdminID);
            }
            else
            {
                ErrorMessage = "Renewal subscription record must be created before this feature can be used.";
                isValid = false;
            }

            return isValid;
        }

        public void QAOnlyUpdateIAPTranIDAttribute(bool remove)
        {
            var brand = GeneralHelper.GetBrandForSite((int) SiteID);
            if (remove)
            {
                Member.SetAttributeText(brand, "IOS_IAP_Receipt_Original_TransactionID", null, TextStatusType.Auto);
                Member.SetAttributeText(brand, "Lifetime_IOS_Original_TransactionID", null, TextStatusType.Auto);
            }
            else
            {
                Member.SetAttributeText(brand, "IOS_IAP_Receipt_Original_TransactionID", "QATest_" + Member.MemberID, TextStatusType.Auto);
                Member.SetAttributeText(brand, "Lifetime_IOS_Original_TransactionID", "QATest_" + Member.MemberID, TextStatusType.Auto);
            }

            MemberSA.Instance.SaveMember(Member);
        }

        public void QAOnlyUpdateRamahAlumAttribute(bool remove)
        {
            var brand = GeneralHelper.GetBrandForSite((int) SiteID);
            if (remove)
            {
                Member.SetAttributeInt(brand, "RamahAlum", 0);
            }
            else
            {
                Member.SetAttributeInt(brand, "RamahAlum", 1);
            }

            MemberSA.Instance.SaveMember(Member);
        }
    }
}
