﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models
{
    public class ManagerBase
    {
        public ContextGlobal g
        {
            get { return HttpContext.Current.Items["g"] as ContextGlobal; }
        }
    }
}
