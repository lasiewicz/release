﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models
{
    public class ApprovalQuestion
    {
        public string Question { get; set; }
        public string Answer { get; set; }
        public int QuestionID { get; set; }
        public int AnswerID { get; set; }
        public int TextboxHeight { get; set; }
        public string StatusIconSource { get; set; }
        public int Number { get; set; }
    }
}
