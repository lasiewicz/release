﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using AdminTool.Models.ModelViews.SubAdmin;
using AdminTool.Models.ModelViews.SubAdmin.Validators;
using AdminTool.Models.UPS;
using FluentValidation.Results;
using Matchnet;
using Matchnet.Content.ServiceAdapters;
using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using AT = AdminTool.Models.ModelViews.SubAdmin;
using Promo = Matchnet.PromoEngine.ValueObjects.Promo;
using PromoEngine = Matchnet.PromoEngine.ValueObjects;

namespace AdminTool.Models
{
    public class PromoManager : ManagerBase
    {
        public PromoManager()
        {
        }

        #region Promo priority/ordering

        public void GetPromoList(PromoPriority promoPriority)
        {
            promoPriority.Promos = GetPromoList(promoPriority.BrandId, promoPriority.PaymentType,
                                                promoPriority.PromoType);
            return;
        }

        public List<Promo> GetPromoList(int brandId, int paymentType, int promoType)
        {
            //PromoCollection promoCol = PromoEngineSA.Instance.GetActivePromos(TargetBrandID);
            // Current business rules for which promos should be displayed on this page:
            // 1. No rejected promos
            // 2. Active promos
            // 3. Promos in the future
            // 4. Ignore non-member promos
            PromoCollection promoCol = PromoEngineSA.Instance.GetPromos(brandId);
            var dbOrderedPromoList = (from Promo promo in promoCol
                                      where promo.PaymentType == (PaymentType) paymentType &&
                                            promo.PromoApprovalStatus != PromoApprovalStatus.Rejected &&
                                            promo.EndDate.CompareTo(DateTime.Now) > 0 &&
                                            promo.PromoType == (PromoType) promoType
                                      orderby promo.ListOrder
                                      select promo).ToList<Promo>();

            var promoList = new List<Promo>();
            int listOrder = 1;
            foreach (Promo promo in dbOrderedPromoList)
            {
                promo.ListOrder = listOrder;
                promoList.Add(promo);

                listOrder++;
            }

            return promoList;
        }

        public void UpdatePromoPriority(PromoPriority promoPriority)
        {
            if (promoPriority.Promos == null || promoPriority.Promos.Count == 0)
                return;

            var promosFromDB = GetPromoList(promoPriority.BrandId, promoPriority.PaymentType, promoPriority.PromoType);
            if (promosFromDB.Count == 0)
                return;

            var promoCol = new PromoCollection(promoPriority.BrandId);

            foreach (var p in promoPriority.Promos)
            {
                var tempP = p;

                var query = (from Promo dbPromo in promosFromDB
                             where dbPromo.PromoID == tempP.PromoID && dbPromo.ListOrder != tempP.ListOrder
                             select dbPromo).SingleOrDefault();

                if (query != null)
                {
                    query.ListOrder = tempP.ListOrder;
                    promoCol.Add(query);
                }
            }

            if (promoCol.Count > 0)
            {
                PromoEngineSA.Instance.SavePromotions(promoCol, promoPriority.BrandId);
            }
        }

        #endregion

        #region Promo Search

        public AT.PromoSearch PerformPromoSearch(FormCollection collection)
        {
            var data = new AT.PromoSearch();
            string errorMessage;

            if (!IsPerformPromoSearchValid(collection, out errorMessage))
            {
                BindPerformPromoSearch(collection, data);
                data.ErrorMessage = errorMessage;
                return data;
            }

            // let's get all the input data first
            string brandId, paymentTypeId, promoTypeId, promoId, promoName, promoApprovalStatus;
            GeneralHelper.TryGetFromFormCollection(collection, "BrandId", out brandId);
            GeneralHelper.TryGetFromFormCollection(collection, "PaymentType", out paymentTypeId);
            GeneralHelper.TryGetFromFormCollection(collection, "PromoType", out promoTypeId);
            GeneralHelper.TryGetFromFormCollection(collection, "PromoId", out promoId);
            GeneralHelper.TryGetFromFormCollection(collection, "PromoName", out promoName);
            GeneralHelper.TryGetFromFormCollection(collection, "PromoApprovalStatus", out promoApprovalStatus);

            PromoEngine.PromoCollection promoCol = null;
            // there are 3 types of searches; return all, search by promoId, search by promo name substring
            if (string.IsNullOrEmpty(promoId) && string.IsNullOrEmpty(promoName))
            {
                // no optional search parameters defined, which means we should return all
                promoCol = PromoEngineSA.Instance.GetPromos(int.Parse(brandId));
            }

            // if promoId was specified, this takes precedence over promo name substring
            if (promoCol == null && !string.IsNullOrEmpty(promoId))
            {
                PromoEngine.Promo promo = PromoEngineSA.Instance.GetPromoByID(int.Parse(promoId), int.Parse(brandId),
                                                                              false);
                if (promo != null)
                    promoCol = new PromoEngine.PromoCollection(int.Parse(brandId)) {promo};
            }

            // last type of search; by promo name substring
            if (promoCol == null && !string.IsNullOrEmpty(promoName))
            {
                promoCol = PromoEngineSA.Instance.GetPromosByDescription(promoName, int.Parse(brandId));
            }

            // filter by payment and promo types here
            if (promoCol != null)
            {
                data.MPromoSearchResults.BrandId = int.Parse(brandId);

                if (int.Parse(promoApprovalStatus) == 0)
                {
                    data.MPromoSearchResults.FoundPromos = (from PromoEngine.Promo promo in promoCol
                                                            where
                                                                promo.PaymentType ==
                                                                (PaymentType) int.Parse(paymentTypeId) &&
                                                                promo.PromoType ==
                                                                (PromoEngine.PromoType) int.Parse(promoTypeId)
                                                            orderby promo.PromoID descending
                                                            select promo).ToList();
                }
                else
                {
                    data.MPromoSearchResults.FoundPromos = (from PromoEngine.Promo promo in promoCol
                                                            where
                                                                promo.PaymentType ==
                                                                (PaymentType) int.Parse(paymentTypeId) &&
                                                                promo.PromoType ==
                                                                (PromoEngine.PromoType) int.Parse(promoTypeId) &&
                                                                promo.PromoApprovalStatus ==
                                                                (PromoEngine.PromoApprovalStatus)
                                                                int.Parse(promoApprovalStatus)
                                                            orderby promo.PromoID descending
                                                            select promo).ToList();
                }
            }

            // if we got this far, we know things went according to plan. rebind the user selected values and auto hide the
            // search parameters
            BindPerformPromoSearch(collection, data);
            data.AutoHideSearchParms = true;

            return data;
        }

        private void BindPerformPromoSearch(FormCollection collection, AT.PromoSearch data)
        {
            // let's get all the input data first
            string brandId, paymentTypeId, promoTypeId, promoId, promoName, promoApprovalStatus;
            GeneralHelper.TryGetFromFormCollection(collection, "BrandId", out brandId);
            GeneralHelper.TryGetFromFormCollection(collection, "PaymentType", out paymentTypeId);
            GeneralHelper.TryGetFromFormCollection(collection, "PromoType", out promoTypeId);
            GeneralHelper.TryGetFromFormCollection(collection, "PromoId", out promoId);
            GeneralHelper.TryGetFromFormCollection(collection, "PromoName", out promoName);
            GeneralHelper.TryGetFromFormCollection(collection, "PromoApprovalStatus", out promoApprovalStatus);

            data.MPromoSearchParams.BrandId = int.Parse(brandId);
            data.MPromoSearchParams.PaymentType = int.Parse(paymentTypeId);
            data.MPromoSearchParams.PromoType = int.Parse(promoTypeId);
            data.MPromoSearchParams.PromoId = promoId;
            data.MPromoSearchParams.PromoName = promoName;
            data.MPromoSearchParams.PromoApprovalStatus = int.Parse(promoApprovalStatus);
        }

        private bool IsPerformPromoSearchValid(FormCollection collection, out string errorMessage)
        {
            bool isValid = true;
            // Since everything is a dropdown we only have to make sure that the user typed promoId is a numerical value

            string promoId;
            GeneralHelper.TryGetFromFormCollection(collection, "PromoId", out promoId);
            int intPromoId;
            isValid = promoId == string.Empty || int.TryParse(promoId, out intPromoId);
            if (isValid)
            {
                errorMessage = string.Empty;
            }
            else
            {
                errorMessage = "Promo ID must be valid";
            }

            return isValid;
        }

        public AT.PromoSearch SetupPromoSearch()
        {
            var data = new AT.PromoSearch();


            return data;
        }

        #endregion

        #region Create Promo

        #region Promo step saves

        public string SavePromoStep(AT.Promo promo, int promoStep, out bool hasError)
        {
            var errorString = ValidatePromoStep(promo, promoStep);
            hasError = !string.IsNullOrEmpty(errorString);

            if (!hasError)
            {
                switch (promoStep)
                {
                    case 1:
                        SavePromoStep1(promo);
                        break;
                    case 2:
                        SavePromoStep2(promo);
                        break;
                    case 3:
                        SavePromoStep3(promo);
                        break;
                    case 4:
                        SavePromoStep4(promo);
                        break;
                    case 5:
                        int promoId = InsertPromo(promo, out errorString);

                        if (string.IsNullOrEmpty(errorString))
                        {
                            errorString = promoId.ToString();
                        }
                        else
                        {
                            hasError = true;
                        }

                        break;
                }
            }

            SubAdminHelper.PopulateSelectListItems(promo);
            return errorString;
        }

        private int InsertPromo(AT.Promo promo, out string errorString)
        {
            // let's convert our promo object to PromoEngine.ValueObjects.Promo
            Promo pePromo = MapAdmintoolPromoToPromoEnginePromo(promo, out errorString);
            if(!string.IsNullOrEmpty(errorString))
                return Matchnet.Constants.NULL_INT;

            #region For new inserts, we have some properties to set or overwrite

            pePromo.ListOrder = 1000; // put it at the bottom in terms of priority
            pePromo.BestValuePlanID = Constants.NULL_INT; // no longer used
            pePromo.PromoID = Constants.NULL_INT; // during clones, this promoId could be set so clear it
            pePromo.PromoApprovalStatus = PromoApprovalStatus.Pending;
                // during clones, this status could be set already so clear it
            pePromo.AdminMemberID = g.AdminID;

            #endregion

            // actually insert the promo here
            int promoId = Constants.NULL_INT;
            errorString = string.Empty;
            try
            {
                promoId = PromoEngineSA.Instance.SavePromotion(pePromo, promo.BrandId);
            }
            catch (Exception ex)
            {
                string errorMessage = ex.ToString();
                if (ex.InnerException != null)
                {
                    errorMessage += ex.InnerException.ToString();
                }

                errorString = "Unable to insert the promotion Exception: " + errorMessage;
            }

            return promoId;
        }

        private void SavePromoStep1(AT.Promo promo)
        {
            #region RowHeaders

            int startRow = 1;

            if (promo.RowHeaders == null)
            {
                // new promo
                promo.RowHeaders = new List<AT.RowHeader>();
            }
            else
            {
                // this is an Edit - figure out if we need to add or reduce
                if (promo.RowDimension >= promo.RowHeaders.Count)
                {
                    startRow = promo.RowHeaders.Count + 1;
                }
                else
                {
                    startRow = -1; // to indicate we shouldn't add anything
                    promo.RowHeaders.RemoveRange(promo.RowDimension, promo.RowHeaders.Count - promo.RowDimension);
                }
            }

            if (startRow != -1)
            {
                for (int i = startRow; i <= promo.RowDimension; i++)
                {
                    var rHeader = new AT.RowHeader()
                                      {
                                          HeaderNumber = i,
                                          Duration = Constants.NULL_INT,
                                          DurationType = (int) DurationType.Month,
                                          ResourceTemplateId = Constants.NULL_INT
                                      };

                    promo.RowHeaders.Add(rHeader);
                }
            }

            #endregion

            #region ColHeaders

            int startCol = 1;
            if (promo.ColHeaders == null)
            {
                // new promo
                promo.ColHeaders = new List<AT.ColHeader>();
            }
            else
            {
                // edit
                if (promo.ColDimension >= promo.ColHeaders.Count)
                {
                    startCol = promo.ColHeaders.Count + 1;
                }
                else
                {
                    startCol = -1;
                    promo.ColHeaders.RemoveRange(promo.ColDimension, promo.ColHeaders.Count - promo.ColDimension);
                }
            }

            if (startCol != -1)
            {
                for (int i = startCol; i <= promo.ColDimension; i++)
                {
                    var cHeader = new AT.ColHeader()
                                      {
                                          HeaderNumber = i,
                                          PlanType = Constants.NULL_INT,
                                          PremiumType = Constants.NULL_INT,
                                          ResourceTemplateId = Constants.NULL_INT
                                      };

                    promo.ColHeaders.Add(cHeader);
                }
            }

            // set the IsMixedPlanType flag
            foreach(var columnHeader in promo.ColHeaders)
            {
                columnHeader.IsMixedPlanType = promo.IsSingleColWithMultPlanTypes;
            }

            #endregion
        }

        private void SavePromoStep2(AT.Promo promo)
        {
            SetupPlanCells(promo);
        }

        private void SavePromoStep3(AT.Promo promo)
        {
            // modelstate was cleared, so we have to set a new object
            SetupPlanCells(promo);
        }

        private void SavePromoStep4(AT.Promo promo)
        {
            // let's build the cron expression here
            if (promo.IsRecurring)
            {
                if (promo.RecurringPattern.ToLower() == "weekly")
                {
                    var days = promo.ReccuringDayOfWeek == null || promo.ReccuringDayOfWeek.Length == 7
                                   ? "*"
                                   : string.Join(",", promo.ReccuringDayOfWeek);

                    promo.RecurrenceCronExpression = string.Format("0 0 {0} ? * {1}", promo.RecurringStartHour, days);
                }
                else if (promo.RecurringPattern.ToLower() == "monthly")
                {
                    promo.RecurrenceCronExpression = string.Format("0 0 {0} {1} * ?", promo.RecurringStartHour,
                                                                   promo.RecurringDayOfMonth);
                }
            }

            PopulatePromoDetailLineItems(promo);
        }

        #endregion

        #region Promo validation

        /// <summary>
        /// If we use Fluent Validation for MVC, we don't have to do this manually, but until the upgrade to MVC3 happens
        /// let's do this manually.  Not adding the version dependency right now would make the upgrade easier.
        /// </summary>
        /// <param name="promo"></param>
        /// <param name="promoStep"></param>
        public string ValidatePromoStep(AT.Promo promo, int promoStep)
        {
            switch (promoStep)
            {
                case 1:
                    return ValidatePromoStep1(promo);
                    break;
                case 2:
                    return ValidatePromoStep2(promo);
                    break;
                case 3:
                    return ValidatePromoStep3(promo);
                    break;
                case 4:
                    return ValidatePromoStep4(promo);
                    break;
            }

            return string.Empty;
        }

        private string ValidatePromoStep1(AT.Promo promo)
        {
            var sb = new StringBuilder();
            var promoValidator = new PromoValidator();

            promo.Step = 1;
            var results = promoValidator.Validate(promo);
            sb = AddErrorMessages(results, sb);

            return sb.ToString();
        }

        private string ValidatePromoStep2(AT.Promo promo)
        {
            return string.Empty;
        }

        private string ValidatePromoStep3(AT.Promo promo)
        {
            var sb = new StringBuilder();
            var promoValidator = new PromoValidator();

            promo.Step = 3;
            var results = promoValidator.Validate(promo);
            sb = AddErrorMessages(results, sb);

            return sb.ToString();
        }

        private string ValidatePromoStep4(AT.Promo promo)
        {
            var sb = new StringBuilder();
            promo.Step = 4;

            var multiSelectTextBoxValidator = new PEMultiSelectTextBoxValidator();
            var results = multiSelectTextBoxValidator.Validate(promo.YearOfBirth);
            sb = AddErrorMessages(results, sb);
            results = multiSelectTextBoxValidator.Validate(promo.MemberIdLastDigit);
            sb = AddErrorMessages(results, sb);
            results = multiSelectTextBoxValidator.Validate(promo.PRM);
            sb = AddErrorMessages(results, sb);
            results = multiSelectTextBoxValidator.Validate(promo.PriceTargetID);
            sb = AddErrorMessages(results, sb);

            var rangeSelectorValidator = new PERangeSelectorValidator();
            results = rangeSelectorValidator.Validate(promo.DaysTillTermination);
            sb = AddErrorMessages(results, sb);
            results = rangeSelectorValidator.Validate(promo.DaysSinceRegistration);
            sb = AddErrorMessages(results, sb);
            results = rangeSelectorValidator.Validate(promo.Age);
            sb = AddErrorMessages(results, sb);
            results = rangeSelectorValidator.Validate(promo.DaysSinceBirthday);
            sb = AddErrorMessages(results, sb);

            var promoValidator = new PromoValidator();
            results = promoValidator.Validate(promo);
            sb = AddErrorMessages(results, sb);

            return sb.ToString();
        }

        #endregion

        #endregion

        #region Create Promo helpers

        private Promo MapAdmintoolPromoToPromoEnginePromo(AT.Promo atPromo, out string errorMessage)
        {
            var pePromo = new PromoEngine.Promo();
            errorMessage = string.Empty;

            pePromo.PromoID = atPromo.PromoId;
            pePromo.PromoApprovalStatus = atPromo.PromoApprovalStatus;
            pePromo.Description = atPromo.Description;
            pePromo.PaymentType = atPromo.PaymentType;
            pePromo.PromoType = atPromo.PromoType;

            #region header template collections

            foreach (var t in atPromo.RowHeaders)
            {
                pePromo.RowHeaderResourceTemplateCollection.Add(new RowHeaderResourceTemplate(Constants.NULL_INT,
                                                                                              t.ResourceTemplateId,
                                                                                              (short) t.HeaderNumber,
                                                                                              t.Duration,
                                                                                              (DurationType)
                                                                                              t.DurationType));
            }

            foreach (var c in atPromo.ColHeaders)
            {
                pePromo.ColHeaderResourceTemplateCollection.Add(new ColHeaderResourceTemplate(Constants.NULL_INT,
                                                                                              c.ResourceTemplateId,
                                                                                              (short) c.HeaderNumber,
                                                                                              (PlanType) c.PlanType,
                                                                                              (PremiumType)
                                                                                              c.PremiumType));
            }

            #endregion

            #region Selected Plans

            if (pePromo.PromoPlans == null)
                pePromo.PromoPlans = new PromoPlanCollection(atPromo.BrandId);

            foreach (var p in atPromo.PlanSelectors)
            {
                //  don't even bother if some invalid plan made it here like matchnet.constants.null_int or something
                if(p.PlanId < 1) 
                    continue;

                var purchasePlan = PlanSA.Instance.GetPlan(p.PlanId, atPromo.BrandId);
                if (purchasePlan == null)
                {
                    errorMessage = string.Format("Plan {0} is not a valid plan. BrandId: {1}", p.PlanId, atPromo.BrandId);
                    return null;
                }

                var pPlan = new PromoPlan(purchasePlan);
                pPlan.ResourceTemplateID = p.ResourceTemplateId;
                pPlan.ListOrder = p.RowNumber;
                pPlan.ColumnGroup = p.ColumnNumber;
                pePromo.PromoPlans.Add(pPlan);
            }

            #endregion

            #region Criteria

            // only the following promos support targeting
            if (atPromo.PromoType == PromoType.Member || atPromo.PromoType == PromoType.EmailLinkWithTargeting
                || atPromo.PromoType == PromoType.Mobile)
            {
                if (pePromo.Criteria == null)
                    pePromo.Criteria = new ExpressionCollection();

                SubAdminHelper.AddPromoExpression((PromoExpressionBase) atPromo.Country, pePromo);
                SubAdminHelper.AddPromoExpression((PromoExpressionBase) atPromo.State, pePromo);
                SubAdminHelper.AddPromoExpression((PromoExpressionBase) atPromo.City, pePromo);
                SubAdminHelper.AddPromoExpression((PromoExpressionBase) atPromo.DaysTillTermination, pePromo);
                SubAdminHelper.AddPromoExpression((PromoExpressionBase) atPromo.DaysSinceRegistration, pePromo);
                SubAdminHelper.AddPromoExpression((PromoExpressionBase) atPromo.Age, pePromo);
                SubAdminHelper.AddPromoExpression((PromoExpressionBase) atPromo.DaysSinceBirthday, pePromo);
                SubAdminHelper.AddPromoExpression((PromoExpressionBase) atPromo.MemberIdLastDigit, pePromo);
                SubAdminHelper.AddPromoExpression((PromoExpressionBase) atPromo.YearOfBirth, pePromo);
                SubAdminHelper.AddPromoExpression((PromoExpressionBase)atPromo.PRM, pePromo);
                SubAdminHelper.AddPromoExpression((PromoExpressionBase)atPromo.PriceTargetID, pePromo);
                SubAdminHelper.AddPromoExpression((PromoExpressionBase) atPromo.DMAs, pePromo);
                SubAdminHelper.AddPromoExpression((PromoExpressionBase) atPromo.SubscriptionStatus, pePromo);
                SubAdminHelper.AddPromoExpression((PromoExpressionBase) atPromo.GenderSeekingGender, pePromo);
                SubAdminHelper.AddPromoExpression((PromoExpressionBase) atPromo.MaritalStatus, pePromo);
                SubAdminHelper.AddPromoExpression((PromoExpressionBase) atPromo.Education, pePromo);
                SubAdminHelper.AddPromoExpression((PromoExpressionBase)atPromo.Profession, pePromo);
                SubAdminHelper.AddPromoExpression((PromoExpressionBase)atPromo.DeviceOS, pePromo);
            }

            #endregion

            // other stuff from the criteria screen
            pePromo.StartDate =
                GeneralHelper.ConvertDateTimeString(atPromo.PromoStartDate).AddHours(atPromo.PromoStartTime);
            pePromo.EndDate = GeneralHelper.ConvertDateTimeString(atPromo.PromoEndDate).AddHours(atPromo.PromoEndTime);
            pePromo.Recurring = atPromo.IsRecurring;
            pePromo.RecurrenceDurationHours = atPromo.RecurringDuration == null ? 0 : (int) atPromo.RecurringDuration;
            pePromo.RecurrenceCronExpression = atPromo.RecurrenceCronExpression;
            pePromo.DefaultPlanID = atPromo.DefaultPlanId;
            pePromo.PageTitle = atPromo.PageTitle;
            pePromo.LegalNote = atPromo.LegalNote;
            pePromo.UPSTemplateID = atPromo.PageTemplateId;
            pePromo.AdminMemberID = atPromo.AdminMemberId;

            // For promos that shouldn't have criteria,
            // we need to make sure there is only 1 expression for the attributeID of 1011, which is used to indicate no criterion
            if (pePromo.PromoType == PromoType.AdminOnly || pePromo.PromoType == PromoType.EmailLink ||
                pePromo.PromoType == PromoType.SupervisorOnly)
            {
                if (pePromo.Criteria == null)
                {
                    pePromo.Criteria = new ExpressionCollection();
                }
                else
                {
                    pePromo.Criteria.Clear();
                }

                pePromo.Criteria.Add(
                    new Expression(
                        new PromoAttribute(1011, Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Number, 2),
                        OperatorType.Equal, new ArrayList() {1}));
            }

            return pePromo;
        }

        public string GeneratePromoPUIHtml(AT.Promo promo)
        {
            var connector = new PaymentUIConnector();
            var page = new CreditCardPage(g.AdminID, promo.BrandId,false);
            ((IPaymentUIJSON) page).PaymentType = promo.PaymentType; 
            ((IPaymentUIJSON) page).SaveLegacyData = true;
            page.Context = System.Web.HttpContext.Current;
            page.PromoPreviewOnly = true;

            string error = "";
            Promo previewPromo = MapAdmintoolPromoToPromoEnginePromo(promo, out error);
            previewPromo.ListOrder = 1000; 
            previewPromo.BestValuePlanID = Constants.NULL_INT; 
            previewPromo.PromoID = Constants.NULL_INT; 
            previewPromo.PromoApprovalStatus = PromoApprovalStatus.Pending;
            previewPromo.AdminMemberID = g.AdminID;
            page.PreviewPromo = previewPromo;
            string html = connector.RedirectToPaymentUI(page, g.AdminID, promo.BrandId);
            return html;
        }

        public ModelViews.SubAdmin.Promo MapPromoEnginePromoToAdmintoolPromo(Promo pePromo, int brandId)
        {
            var atPromo = new AT.Promo();

            atPromo.BrandId = brandId;
            atPromo.PromoId = pePromo.PromoID;
            atPromo.PromoApprovalStatus = pePromo.PromoApprovalStatus;
            atPromo.Description = pePromo.Description;
            atPromo.PaymentType = pePromo.PaymentType;
            atPromo.PromoType = pePromo.PromoType;

            #region Row and Column headers

            if (atPromo.RowHeaders == null)
                atPromo.RowHeaders = new List<RowHeader>();

            foreach (RowHeaderResourceTemplate r in pePromo.RowHeaderResourceTemplateCollection)
            {
                atPromo.RowHeaders.Add(new RowHeader()
                                           {
                                               ResourceTemplateId = r.ResourceTemplateID,
                                               HeaderNumber = r.HeaderNumber,
                                               Duration = r.Duration,
                                               DurationType = (int) r.DurationType
                                           });
            }
            
            // make sure it's sorted because it doesn't come sorted from the PromoEngine service
            atPromo.RowHeaders.Sort((rh1, rh2) => rh1.HeaderNumber.CompareTo(rh2.HeaderNumber));

            if (atPromo.ColHeaders == null)
                atPromo.ColHeaders = new List<ColHeader>();

            foreach (ColHeaderResourceTemplate c in pePromo.ColHeaderResourceTemplateCollection)
            {
                atPromo.ColHeaders.Add(new ColHeader()
                                           {
                                               ResourceTemplateId = c.ResourceTemplateID,
                                               HeaderNumber = c.HeaderNumber,
                                               PlanType = (int) c.PlanType,
                                               PremiumType = (int) c.PremiumType
                                           });
            }

            // make sure it's sorted because it doesn't come sorted from the PromoEngine service
            atPromo.ColHeaders.Sort((ch1, ch2) => ch1.HeaderNumber.CompareTo(ch2.HeaderNumber));

            atPromo.RowDimension = atPromo.RowHeaders.Count;
            atPromo.ColDimension = atPromo.ColHeaders.Count;

            #endregion

            #region Selected Plans

            if (atPromo.PlanSelectors == null)
                atPromo.PlanSelectors = new List<PlanSelector>();

            foreach (PromoPlan promoPlan in pePromo.PromoPlans)
            {
                atPromo.PlanSelectors.Add(new PlanSelector()
                                              {
                                                  ResourceTemplateId = promoPlan.ResourceTemplateID,
                                                  RowNumber = promoPlan.ListOrder,
                                                  ColumnNumber = promoPlan.ColumnGroup,
                                                  PlanId = promoPlan.PlanID,
                                                  BrandID = brandId
                                              });
            }

            // In the database, we don't store the "null" cells, but we still need to create a PlanSelector object for each of these "null" cells
            foreach(var colHeader in atPromo.ColHeaders)
            {
                var tempColHeader = colHeader; // to avoid 'Access to modified closure' problem
                foreach(var rowHeader in atPromo.RowHeaders)
                {
                    var tempRowHeader = rowHeader;

                    var pSel = (from PlanSelector pSelector in atPromo.PlanSelectors
                                where
                                    pSelector.ColumnNumber == tempColHeader.HeaderNumber &&
                                    pSelector.RowNumber == tempRowHeader.HeaderNumber
                                select pSelector).SingleOrDefault();

                    if(pSel == null)
                    {
                        // if not found, create an empty PlanSelector object
                        atPromo.PlanSelectors.Add(new PlanSelector()
                                                      {
                                                          ColumnNumber = tempColHeader.HeaderNumber,
                                                          RowNumber = tempRowHeader.HeaderNumber,
                                                          PlanId = Constants.NULL_INT,
                                                          ResourceTemplateId = Constants.NULL_INT,
                                                          BrandID = brandId
                                                      });
                    }

                }
            }

            // make sure it's sorted because it doesn't come sorted from the PromoEngine service
            var sortedPlanSelectors =
                atPromo.PlanSelectors.OrderBy(x => x.ColumnNumber).ThenBy(x => x.RowNumber).ToList();

            atPromo.PlanSelectors = sortedPlanSelectors;
            #endregion

            #region Criteria

            SetPromoExpression(atPromo.Country, pePromo.Criteria);
            SetPromoExpression(atPromo.State, pePromo.Criteria);
            SetPromoExpression(atPromo.City, pePromo.Criteria);
            SetPromoExpression(atPromo.DaysTillTermination, pePromo.Criteria);
            SetPromoExpression(atPromo.DaysSinceRegistration, pePromo.Criteria);
            SetPromoExpression(atPromo.Age, pePromo.Criteria);
            SetPromoExpression(atPromo.DaysSinceBirthday, pePromo.Criteria);
            SetPromoExpression(atPromo.MemberIdLastDigit, pePromo.Criteria);
            SetPromoExpression(atPromo.YearOfBirth, pePromo.Criteria);
            SetPromoExpression(atPromo.PRM, pePromo.Criteria);
            SetPromoExpression(atPromo.PriceTargetID, pePromo.Criteria);
            SetPromoExpression(atPromo.DMAs, pePromo.Criteria);
            SetPromoExpression(atPromo.SubscriptionStatus, pePromo.Criteria);
            SetPromoExpression(atPromo.GenderSeekingGender, pePromo.Criteria);
            SetPromoExpression(atPromo.MaritalStatus, pePromo.Criteria);
            SetPromoExpression(atPromo.Education, pePromo.Criteria);
            SetPromoExpression(atPromo.Profession, pePromo.Criteria);
            SetPromoExpression(atPromo.DeviceOS, pePromo.Criteria);

            // set the parent regions for state and city
            atPromo.State.ParentRegionId = atPromo.Country.TheValue;
            atPromo.City.ParentRegionId = atPromo.State.TheValue;

            #endregion

            // If a promo is not a mixed promo (std and premium plans), you can't have PlanType=0 for column header
            atPromo.IsSingleColWithMultPlanTypes =
                (from cHeader in atPromo.ColHeaders where cHeader.PlanType == 0 select cHeader).Any();

            atPromo.PromoStartDate = GeneralHelper.GetDatePortionOnly(pePromo.StartDate).ToString("d");
            atPromo.PromoStartTime = pePromo.StartDate.Hour;

            atPromo.PromoEndDate = GeneralHelper.GetDatePortionOnly(pePromo.EndDate).ToString("d");
            atPromo.PromoEndTime = pePromo.EndDate.Hour;

            atPromo.IsRecurring = pePromo.Recurring;
            atPromo.RecurringDuration = pePromo.RecurrenceDurationHours == Constants.NULL_INT ? 0 : pePromo.RecurrenceDurationHours;
            atPromo.RecurrenceCronExpression = pePromo.RecurrenceCronExpression;
            SetRecurringPropertiesFromCronExpression(atPromo);

            atPromo.DefaultPlanId = pePromo.DefaultPlanID;
            atPromo.PageTitle = pePromo.PageTitle;
            atPromo.LegalNote = pePromo.LegalNote;
            atPromo.PageTemplateId = pePromo.UPSTemplateID;
            atPromo.AdminMemberId = pePromo.AdminMemberID;
            return atPromo;
        }

        private void PopulatePromoDetailLineItems(AT.Promo promo)
        {
            var brand = BrandConfigSA.Instance.GetBrandByID(promo.BrandId);
            promo.PromoDetailLineItems = new List<PromoDetailLineItem>();

            #region Basic Promo info

            if (promo.PromoId == Constants.NULL_INT)
            {
                promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                                   {LabelText = "Promotion ID", LineItemText = "not yet inserted"});
            }
            else
            {
                promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                                   {LabelText = "Promotion ID", LineItemText = promo.PromoId.ToString()});
                promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                                   {
                                                       LabelText = "Promotion(UPS)",
                                                       LineItemText = "<a target=\"_blank\" href=\"http://" +
                                                                      brand.Site.DefaultHost + "." +
                                                                      brand.Uri +
                                                                      "/applications/subscription/subscribe.aspx?PromoID=" +
                                                                      promo.PromoId.ToString() + "\">View</a>",
                                                       IsLiteral = true
                                                   });
            }

            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Name of Promotion",
                                                   LineItemText = promo.Description
                                               });

            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Website",
                                                   LineItemText = brand.Uri
                                               });

            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Approval Status",
                                                   LineItemText = promo.PromoApprovalStatus.ToString()
                                               });

            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Promo Type",
                                                   LineItemText = Enum.GetName(typeof (PromoType), promo.PromoType)
                                               });

            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Payment Type",
                                                   LineItemText = Enum.GetName(typeof (PaymentType), promo.PaymentType)
                                               });

            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Default plan selected",
                                                   LineItemText =
                                                       promo.DefaultPlanId == Constants.NULL_INT
                                                           ? "none"
                                                           : promo.DefaultPlanId.ToString()
                                               });

            #endregion

            #region Selected Plans

            var orderedPlans = (from PlanSelector aPlanS in promo.PlanSelectors
                                select aPlanS).OrderBy(p => p.ColumnNumber).ThenBy(p => p.RowNumber).ToList();

            for (int i = 0; i < orderedPlans.Count; i++)
            {
                var label = string.Empty;
                var textContent = string.Empty;

                if (orderedPlans[i].RowNumber == 1)
                {
                    label = string.Format("Selected plans - Column {0}", orderedPlans[i].ColumnNumber);
                    textContent = SubAdminHelper.GetPlanInfoString(orderedPlans[i].PlanId, promo.BrandId);
                }
                else
                {
                    label = string.Empty;
                    textContent = SubAdminHelper.GetPlanInfoString(orderedPlans[i].PlanId, promo.BrandId);
                }

                promo.PromoDetailLineItems.Add(new PromoDetailLineItem() {LabelText = label, LineItemText = textContent});
            }

            #endregion

            #region Promo Expressions

            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Subscription Status",
                                                   LineItemText =
                                                       SubAdminHelper.GetValueStringFromMaskType(
                                                           promo.SubscriptionStatus.ExpPromoAttributeId,
                                                           promo.SubscriptionStatus.GetPromoExpression())
                                               });
            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Days since subscription end date",
                                                   LineItemText = SubAdminHelper.GetValueStringFromRangeType(
                                                       promo.DaysTillTermination.ExpPromoAttributeId,
                                                       promo.DaysTillTermination.GetPromoExpression())
                                               });
            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Days since registration",
                                                   LineItemText = SubAdminHelper.GetValueStringFromRangeType(
                                                       promo.DaysSinceRegistration.ExpPromoAttributeId,
                                                       promo.DaysSinceRegistration.GetPromoExpression())
                                               });
            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Last digit of member ID",
                                                   LineItemText = SubAdminHelper.GetValueStringFromEqualType(
                                                       promo.MemberIdLastDigit.ExpPromoAttributeId,
                                                       promo.MemberIdLastDigit.GetPromoExpression(),
                                                       promo.BrandId,
                                                       brand.Site.LanguageID)
                                               });
            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Year of birth",
                                                   LineItemText =
                                                       SubAdminHelper.GetValueStringFromEqualType(
                                                           promo.YearOfBirth.ExpPromoAttributeId,
                                                           promo.YearOfBirth.GetPromoExpression(), promo.BrandId,
                                                           brand.Site.LanguageID)
                                               });
            promo.PromoDetailLineItems.Add(new PromoDetailLineItem
                                               {
                                                   LabelText = "PRM",
                                                   LineItemText =
                                                       SubAdminHelper.GetValueStringFromEqualType(
                                                           promo.PRM.ExpPromoAttributeId,
                                                           promo.PRM.GetPromoExpression(), promo.BrandId,
                                                           brand.Site.LanguageID)
                                               });
            promo.PromoDetailLineItems.Add(new PromoDetailLineItem
                                                {
                                                    LabelText = "Price Target ID",
                                                    LineItemText =
                                                        SubAdminHelper.GetValueStringFromEqualType(
                                                            promo.PriceTargetID.ExpPromoAttributeId,
                                                            promo.PriceTargetID.GetPromoExpression(), promo.BrandId,
                                                            brand.Site.LanguageID)
                                                });
            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Age",
                                                   LineItemText =
                                                       SubAdminHelper.GetValueStringFromRangeType(
                                                           promo.Age.ExpPromoAttributeId, promo.Age.GetPromoExpression())
                                               });
            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Birthday (days until / days since)",
                                                   LineItemText =
                                                       SubAdminHelper.GetValueStringFromRangeType(
                                                           promo.DaysSinceBirthday.ExpPromoAttributeId,
                                                           promo.DaysSinceBirthday.GetPromoExpression())
                                               });
            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Gender seeking gender",
                                                   LineItemText =
                                                       SubAdminHelper.GetValueStringFromMaskType(
                                                           promo.GenderSeekingGender.ExpPromoAttributeId,
                                                           promo.GenderSeekingGender.GetPromoExpression())
                                               });
            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "DMA",
                                                   LineItemText =
                                                       SubAdminHelper.GetValueStringFromEqualType(
                                                           promo.DMAs.ExpPromoAttributeId,
                                                           promo.DMAs.GetPromoExpression(), promo.BrandId,
                                                           brand.Site.LanguageID)
                                               });
            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Country",
                                                   LineItemText = SubAdminHelper.GetValueStringFromEqualType(
                                                       promo.Country.ExpPromoAttributeId,
                                                       promo.Country.GetPromoExpression(),
                                                       promo.BrandId,
                                                       brand.Site.LanguageID)
                                               });
            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "State / Region / Province",
                                                   LineItemText =
                                                       SubAdminHelper.GetValueStringFromEqualType(
                                                           promo.State.ExpPromoAttributeId,
                                                           promo.State.GetPromoExpression(),
                                                           promo.BrandId,
                                                           brand.Site.LanguageID)
                                               });

            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "City",
                                                   LineItemText =
                                                       SubAdminHelper.GetValueStringFromEqualType(
                                                           promo.City.ExpPromoAttributeId,
                                                           promo.City.GetPromoExpression(),
                                                           promo.BrandId,
                                                           brand.Site.LanguageID)
                                               });
            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Marital status",
                                                   LineItemText =
                                                       SubAdminHelper.GetValueStringFromEqualType(
                                                           promo.MaritalStatus.ExpPromoAttributeId,
                                                           promo.MaritalStatus.GetPromoExpression(),
                                                           promo.BrandId,
                                                           brand.Site.LanguageID)
                                               });
            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Education",
                                                   LineItemText =
                                                       SubAdminHelper.GetValueStringFromEqualType(
                                                           promo.Education.ExpPromoAttributeId,
                                                           promo.Education.GetPromoExpression(),
                                                           promo.BrandId,
                                                           brand.Site.LanguageID)
                                               });
            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Profession",
                                                   LineItemText =
                                                       SubAdminHelper.GetValueStringFromEqualType(
                                                           promo.Profession.ExpPromoAttributeId,
                                                           promo.Profession.GetPromoExpression(),
                                                           promo.BrandId,
                                                           brand.Site.LanguageID)
                                               });
            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                                {
                                                    LabelText = "Device/OS",
                                                    LineItemText =
                                                        SubAdminHelper.GetValueStringFromMaskType(
                                                            promo.DeviceOS.ExpPromoAttributeId,
                                                            promo.DeviceOS.GetPromoExpression())
                                                });

            #endregion

            #region Promo start/end date, recurring, the page title (Promo link URL if applicable), page template

            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Start date time of promotion",
                                                   LineItemText =
                                                       string.Format("{0} {1}", promo.PromoStartDate,
                                                                     promo.PromoStartTime)
                                               });
            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "End date time of promotion",
                                                   LineItemText =
                                                       string.Format("{0} {1}", promo.PromoEndDate, promo.PromoEndTime)
                                               });

            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Recurring",
                                                   LineItemText = promo.IsRecurring.ToString()
                                               });
            if (promo.RecurrenceCronExpression != null)
            {
                promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                                   {
                                                       LabelText = "Recurrence Cron Expression",
                                                       LineItemText = promo.RecurrenceCronExpression
                                                   });
            }
            if (promo.RecurringDuration != null && promo.RecurringDuration != Constants.NULL_INT)
            {
                promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                                   {
                                                       LabelText = "Recurrence Duration Hours",
                                                       LineItemText = promo.RecurringDuration.ToString()
                                                   });
            }

            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Subscription page title",
                                                   LineItemText = promo.PageTitle
                                               });

            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
            {
                LabelText = "Legal Note",
                LineItemText = promo.LegalNote
            });

            if (promo.PromoType == PromoType.EmailLink || promo.PromoType == PromoType.EmailLinkWithTargeting)
            {
                var promoUrl = string.Format("http://{0}.{1}/Applications/Subscription/Subscribe.aspx?promoplan={2}",
                                             g.Environment == string.Empty ? "www" : g.Environment,
                                             brand.Uri.ToLower(),
                                             GeneralHelper.Encrypt(promo.PromoId));
                promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                                   {
                                                       LabelText = "Promo link URL",
                                                       LineItemText = promoUrl
                                                   });
            }

            promo.PromoDetailLineItems.Add(new PromoDetailLineItem()
                                               {
                                                   LabelText = "Subscription page template",
                                                   LineItemText = promo.PageTemplateId.ToString()
                                               });

            #endregion
        }

        private StringBuilder AddErrorMessages(ValidationResult validationResult, StringBuilder sb)
        {
            if (!validationResult.IsValid)
            {
                foreach (var e in validationResult.Errors)
                {
                    sb.Append(e.ErrorMessage + "<br/>");
                }
            }

            return sb;
        }

        private void SetupPlanCells(AT.Promo promo)
        {
            if (promo.PlanSelectors == null)
                promo.PlanSelectors = new List<PlanSelector>(); // new promo

            var newPlanSelectors = new List<PlanSelector>();

            // since mvc wants us to rebind any type of SelectListItem list, we are only creating shells here and doing the
            // List<SelectListeItem> binding later
            foreach (var colHeader in promo.ColHeaders)
            {
                var tempColHeader = colHeader; // to avoid 'Access to modified closure' problem

                foreach (var rowHeader in promo.RowHeaders)
                {
                    var tempRowHeader = rowHeader; // to avoid 'Access to modified closure' problem

                    var aPlanSelector = new AT.PlanSelector()
                                            {
                                                ColumnNumber = tempColHeader.HeaderNumber,
                                                RowNumber = tempRowHeader.HeaderNumber,
                                                PlanId = Constants.NULL_INT,
                                                ResourceTemplateId = Constants.NULL_INT,
                                                BrandID = promo.BrandId
                                            };

                    var pSel = (from PlanSelector pSelector in promo.PlanSelectors
                                where
                                    pSelector.ColumnNumber == tempColHeader.HeaderNumber &&
                                    pSelector.RowNumber == tempRowHeader.HeaderNumber
                                select pSelector).SingleOrDefault();

                    if (pSel != null)
                    {
                        aPlanSelector.PlanId = pSel.PlanId;
                        aPlanSelector.ResourceTemplateId = pSel.ResourceTemplateId;
                    }

                    newPlanSelectors.Add(aPlanSelector);
                }
            }

            //promo.PlanSelectors.Clear();
            //promo.PlanSelectors.AddRange(newPlanSelectors);
            promo.PlanSelectors = newPlanSelectors;
        }

        private RowHeaderResourceTemplateCollection ModifyRowHeaderDimension(
            RowHeaderResourceTemplateCollection HeaderCollection, int NewCount)
        {
            var newCollection = new RowHeaderResourceTemplateCollection();
            if (HeaderCollection.Count > NewCount)
            {
                // Chop                
                for (int i = 1; i <= NewCount; i++) // Remember our ListOrder is 1-based
                {
                    newCollection.Add(HeaderCollection.FindByPosition(i));
                }
            }
            else
            {
                // Extending or brand new HeaderCollection or same dimensions as before. Only copy elements if this is an Extend.
                if (HeaderCollection.Count > 0)
                {
                    for (int i = 1; i <= HeaderCollection.Count; i++)
                    {
                        newCollection.Add(HeaderCollection.FindByPosition(i));
                    }
                }

                for (int i = HeaderCollection.Count + 1; i <= NewCount; i++)
                {
                    newCollection.Add(new RowHeaderResourceTemplate(Constants.NULL_INT, Constants.NULL_INT, (short) i,
                                                                    Constants.NULL_INT, DurationType.Month));
                }
            }

            return newCollection;
        }

        private ColHeaderResourceTemplateCollection ModifyColHeaderDimension(
            ColHeaderResourceTemplateCollection HeaderCollection, int NewCount)
        {
            var newCollection = new ColHeaderResourceTemplateCollection();
            if (HeaderCollection.Count > NewCount)
            {
                // Chop                
                for (int i = 1; i <= NewCount; i++) // Remember our ListOrder is 1-based
                {
                    newCollection.Add(HeaderCollection.FindByPosition(i));
                }
            }
            else
            {
                // Extending or brand new HeaderCollection or same dimensions as before. Only copy elements if this is an Extend.
                if (HeaderCollection.Count > 0)
                {
                    for (int i = 1; i <= HeaderCollection.Count; i++)
                    {
                        newCollection.Add(HeaderCollection.FindByPosition(i));
                    }
                }

                for (int i = HeaderCollection.Count + 1; i <= NewCount; i++)
                {
                    newCollection.Add(new ColHeaderResourceTemplate(Constants.NULL_INT, Constants.NULL_INT, (short) i,
                                                                    PlanType.Regular, PremiumType.None));
                }
            }

            return newCollection;
        }

        private void SetRecurringPropertiesFromCronExpression(AT.Promo promo)
        {
            if (string.IsNullOrEmpty(promo.RecurrenceCronExpression))
                return;

            var match = Regex.Match(promo.RecurrenceCronExpression, @"^(\d\s){4}\*");

            if (match.Success)
            {
                // this means it's monthly
                promo.RecurringPattern = "Monthly";
                if (match.Groups.Count == 2 && match.Groups[1].Captures.Count == 4)
                {
                    int outInt;
                    if (int.TryParse(match.Groups[1].Captures[2].Value, out outInt))
                    {
                        promo.RecurringStartHour = outInt;
                    }

                    if (int.TryParse(match.Groups[1].Captures[3].Value, out outInt))
                    {
                        promo.RecurringDayOfMonth = outInt;
                    }
                }
            }
            else
            {
                // this means it's weekly
                promo.RecurringPattern = "Weekly";
                match = Regex.Match(promo.RecurrenceCronExpression, @"^(\d\s){3}\?\s\*\s(.+)?");
                if (match.Success && match.Groups.Count == 3 && match.Groups[1].Captures.Count == 3 &&
                    match.Groups[2].Captures.Count == 1)
                {
                    int outInt;
                    if (int.TryParse(match.Groups[1].Captures[2].Value, out outInt))
                    {
                        promo.RecurringStartHour = outInt;
                    }

                    if (match.Groups[2].Captures[0].Value == "*")
                    {
                        promo.ReccuringDayOfWeek = new string[]
                                                       {
                                                           "SUN",
                                                           "MON",
                                                           "TUE",
                                                           "WED",
                                                           "THU",
                                                           "FRI",
                                                           "SAT"
                                                       };
                    }
                    else
                    {
                        promo.ReccuringDayOfWeek = match.Groups[2].Captures[0].Value.Split(new char[] {','},
                                                                                           StringSplitOptions.
                                                                                               RemoveEmptyEntries);
                    }
                }
            }
        }

        private void SetPromoExpression(PromoExpressionBase promoExpression, ExpressionCollection criteria)
        {
            var queryResults = (from Expression e in criteria
                                where e.Attribute.ID == promoExpression.ExpPromoAttributeId
                                select new ExpressionValue(e.Values, e.Operator)).ToList();

            if (queryResults.Count > 0)
                promoExpression.ExpValues = queryResults;
        }

        #endregion

        public AT.Promo GetBlankPromo()
        {
            var promo = new AT.Promo();
            // defaults
            promo.PromoId = Constants.NULL_INT;
            promo.BrandId = 1003;
            promo.PromoApprovalStatus = PromoApprovalStatus.Pending;
            promo.PromoStartDate = DateTime.Now.ToString("d");
            promo.PromoEndDate = DateTime.Now.AddDays(1).ToString("d");

            SubAdminHelper.PopulateSelectListItems(promo);

            return promo;
        }

        public AT.Promo ClonePromo(int promoId, int brandId)
        {
            var pePromo = PromoEngineSA.Instance.GetPromoByID(promoId, brandId, false);
            var atPromo = MapPromoEnginePromoToAdmintoolPromo(pePromo, brandId);
            
            atPromo.PromoId = Constants.NULL_INT;
            atPromo.PromoApprovalStatus = PromoApprovalStatus.Pending;

            SubAdminHelper.PopulateSelectListItems(atPromo);

            return atPromo;
        }

        public AT.Promo GetPromoDetail(int promoId, int brandId)
        {
            var pePromo = PromoEngineSA.Instance.GetPromoByID(promoId, brandId, false);
            var atPromo = MapPromoEnginePromoToAdmintoolPromo(pePromo, brandId);
            PopulatePromoDetailLineItems(atPromo);

            return atPromo;
        }

        public bool IsPromoEditable(AT.Promo promo, out ArrayList errorMessage)
        {
            errorMessage = new ArrayList();

            var today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            if (GeneralHelper.ConvertDateTimeString(promo.PromoEndDate) <= today)
            {
                errorMessage.Add("Promo end date cannot be modified for expired promos");
                return false;
            }

            return true;
        }

        public bool UpdatePromoEndDate(int promoId, int brandId, DateTime newEndDate, out ArrayList errors)
        {
            errors = new ArrayList();

            var pePromo = PromoEngineSA.Instance.GetPromoByID(promoId, brandId, false);
            if (pePromo == null)
            {
                errors.Add(string.Format("Promo with id:{0} does not exist", promoId));
                return false;
            }

            if (newEndDate <= DateTime.Today)
            {
                errors.Add(string.Format("Earliest end date is tomorrow: {0}", DateTime.Today.AddDays(1).ToString("d")));
                return false;
            }

            pePromo.EndDate = newEndDate;
            PromoEngineSA.Instance.SavePromotion(pePromo, brandId);

            return true;
        }
        
    }
}
