﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.MobileControls;
using AdminTool.Models.ModelViews.SubAdmin;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
using Spark.CommonLibrary.Logging;

namespace AdminTool.Models
{
    public class ResourceTemplatesManager
    {
        private Logger logger = new Logger(typeof(ResourceTemplatesManager));
        public List<SelectListItem> PopulateBrands()
        {
            var brandsList = new List<Brand>();
            Sites sites = BrandConfigSA.Instance.GetSites();

            foreach (Site s in sites)
            {

                int siteid = s.SiteID;
                int communityid = s.Community.CommunityID;

                bool active = Conversion.CBool(RuntimeSettings.GetSetting("SITE_ACTIVE_FLAG", communityid, siteid), true);
                if (!active)
                    continue;

                Brands brands = BrandConfigSA.Instance.GetBrandsBySite(s.SiteID);
                foreach (Brand b in brands)
                {
                    if (b.Uri.ToLower() == s.Name.ToLower())
                    {
                        brandsList.Add(b);
                        break;
                    }
                }
            }
            brandsList.Sort((item1, item2) => item1.Site.Name.CompareTo(item2.Site.Name));
            return brandsList.Select(brand => new SelectListItem {Text = brand.Uri, Value = brand.BrandID.ToString(), Selected = (brand.BrandID == 1003)}).ToList();
        }

        public List<SelectListItem> PopulateTemplateTypes(bool showSelect)
        {
            var templateTypes = new List<SelectListItem>
                                    {
                                       
                                        new SelectListItem {Text = "Row", Value = "1"},
                                        new SelectListItem {Text = "Column", Value = "2"},
                                        new SelectListItem {Text = "Plan", Value = "3"}
                                    };
            if(showSelect)
            {
                var selectItem = new SelectListItem {Text = "--Select--", Value = "0", Selected = true};
                templateTypes.Insert(0, selectItem);
            }
            return templateTypes;
        }

        public bool SaveResourceTemplate(ResourceTemplateDetail detail)
        {
            bool status = true;

            ResourceTemplate template = (detail.IsNew) ? new ResourceTemplate("", "") : ResourceTemplateSA.Instance.GetResourceTemplateByID(detail.TemplateId);
            if (template != null)
            {
                template.Content = detail.Content ?? "";
                template.Description = detail.Description;
                if(detail.BrandId > 0)
                    template.GroupID = detail.BrandId;
                if(detail.TemplateTypeId > 0)
                    template.ResourceTemplateType = (ResourceTemplateType)detail.TemplateTypeId;
                try
                {
                    ResourceTemplate savedtemplate = ResourceTemplateSA.Instance.SaveResourceTemplate(template);
                    if (savedtemplate != null)
                    {
                        detail.TemplateId = savedtemplate.ResourceTemplateID;
                        detail.BrandId = savedtemplate.GroupID;
                        detail.TemplateTypeId = (int)savedtemplate.ResourceTemplateType;
                    }

                }
                catch (Exception ex)
                {
                    status = false;
                    logger.Error("Error occured while saving Resource Template", ex);
                }
            }
            return status;
        }
    }
}