﻿using System;
using System.Collections.Generic;
using System.Linq;
using Spark.Common.FraudService;
using Spark.Common.OrderHistoryService;
using Spark.Common.DiscountService;
using Matchnet.Purchase.ValueObjects;
using Matchnet;
using System.Text;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using ContentBrand = Matchnet.Content.ValueObjects.BrandConfig;
using AdminTool.Models.ModelViews.Member;


namespace AdminTool.Models
{
    public class AccountHistory
    {
        # region Private Members

        private DateTime _insertDateInPST = DateTime.MinValue;
        private int _orderID = Constants.NULL_INT;
        private TransactionType _transactionType = TransactionType.None;
        private decimal _totalAmount = Constants.NULL_DECIMAL;
        private string _lastFourAccountNumber = Constants.NULL_STRING;
        private int _callingSystemID = Constants.NULL_INT;
        private Spark.Common.OrderHistoryService.PaymentType _paymentType = Spark.Common.OrderHistoryService.PaymentType.None;
        private OrderStatus _orderStatus = OrderStatus.None;
        private int _adminID = Constants.NULL_INT;
        private string _adminUserName = Constants.NULL_STRING;
        private int _promoID = Constants.NULL_INT;
        private int _primaryPackageID = Constants.NULL_INT;
        private string _packageIDList = Constants.NULL_STRING;
        private int _duration = Constants.NULL_INT;
        private DurationType _durationType = DurationType.None;
        private bool _isOneOffPurchase = false;
        private List<Spark.Common.AccessService.PrivilegeType> _privilegeTypeList = new List<Spark.Common.AccessService.PrivilegeType>();
        private int _adjustEmailCount = 0;
        private string _creditQueueURL;
        private Member _member;
        private ContentBrand.Brand _brand;
        private CurrencyType _currencyType;
        private Gift _giftPurchase = null;
        private Gift _giftRedemption = null;
        private decimal _deferredAmount = Constants.NULL_DECIMAL;
        private int _authorizationPaymentID = Constants.NULL_INT;
        private int _billingPaymentID = Constants.NULL_INT;
        private string _internalStatusMessage = Constants.NULL_STRING;
        private string _userPaymentGUID = Constants.NULL_STRING;
        public string _discountCode = Constants.NULL_STRING;
        public string _discountAmountForDisplay = null;
        private FraudDetailedResponse _fraudDetailedResponse = null;

        public FraudDetailedResponse FraudDetailedResponse
        {
            get
            {
                if (_fraudDetailedResponse == null)
                {
                    return new FraudDetailedResponse();
                }
                else
                {
                    return _fraudDetailedResponse;
                }
            }
            set { this._fraudDetailedResponse = value; }
        }

        private AccountHistoryType _accountHistoryType = AccountHistoryType.None;

        //private const string RESOURCE_CONSTANT = "_RC";

        #endregion

        #region Constructors

        public AccountHistory(OrderInfo orderInfo, ContentBrand.Brand brand, AccountHistoryType accountHistoryType, Member member)
        {
            this._insertDateInPST = ConvertUTCToPST(orderInfo.InsertDate);
            this._orderID = orderInfo.OrderID;
            // The transaction type is the first transaction type found  
            this._transactionType = GetAllUPSTransactionTypes(orderInfo)[0];
            this._totalAmount = orderInfo.TotalAmount;
            this._lastFourAccountNumber = orderInfo.LastFourAccountNumber;
            this._callingSystemID = orderInfo.CallingSystemID;
            this._paymentType = (Spark.Common.OrderHistoryService.PaymentType)Enum.Parse(typeof(Spark.Common.OrderHistoryService.PaymentType), Enum.GetName(typeof(Spark.Common.OrderHistoryService.PaymentType), orderInfo.PaymentType));
            this._orderStatus = (OrderStatus)orderInfo.OrderStatusID;
            this._adminID = orderInfo.AdminUserID;
            this._adminUserName = orderInfo.AdminUserName;
            this._promoID = orderInfo.PromoID;
            this._primaryPackageID = orderInfo.PrimaryPackageID;
            this._packageIDList = GetPackageIDList(orderInfo);
            this._duration = orderInfo.Duration;
            this._durationType = (DurationType)orderInfo.DurationTypeID;
            this._accountHistoryType = accountHistoryType;
            
            foreach (OrderDetailInfo orderDetailInfo in orderInfo.OrderDetail)
            {
                if (orderDetailInfo.PackageType == PackageType.OneOff)
                {
                    this._isOneOffPurchase = true;
                }
            }

            foreach (OrderUserPaymentInfo orderUserPaymentInfo in orderInfo.OrderUserPayment)
            {
                if (orderUserPaymentInfo.ChargeTypeID == 1)
                {
                    // Populate the billing charge ID for this order 
                    this._billingPaymentID = orderUserPaymentInfo.ChargeID;
                }
                else if (orderUserPaymentInfo.ChargeTypeID == 2)
                {
                    // Populate the authorization charge ID for this order 
                    this._authorizationPaymentID = orderUserPaymentInfo.ChargeID;            
                }                
            }

            this._creditQueueURL = GetCreditQueueBaseURL();
            this._member = member;
            this._brand = brand;
            this._currencyType = (CurrencyType)orderInfo.CurrencyID;
            this._internalStatusMessage = orderInfo.InternalStatusMessage;
        }

        public AccountHistory(Spark.Common.RenewalService.RenewalTransaction renewalTran, ContentBrand.Brand brand, AccountHistoryType accountHistoryType, Member member)
        {
            this._insertDateInPST = ConvertUTCToPST(renewalTran.InsertDateUTC);
            this._orderID = renewalTran.RenewalTransactionID;
            this._transactionType = (TransactionType)renewalTran.TransactionTypeID;
            this._totalAmount = 0.00m;
            this._lastFourAccountNumber = "";
            this._callingSystemID = renewalTran.CallingSystemID;
            this._paymentType = Spark.Common.OrderHistoryService.PaymentType.None;
            this._orderStatus = (OrderStatus)renewalTran.RenewalStatusID;
            this._adminID = renewalTran.AdminID;
            this._promoID = Constants.NULL_INT;
            this._primaryPackageID = Constants.NULL_INT;
            this._packageIDList = "";
            this._duration = 0;
            this._durationType = DurationType.Month;

            if (this._transactionType == TransactionType.AutoRenewalReopen || this.TransactionType == TransactionType.AutoRenewalTerminate)
            {
                //primary package will be the first one
                if (renewalTran.RenewalTransactionDetails != null && renewalTran.RenewalTransactionDetails.Length > 0)
                {
                    this._primaryPackageID = renewalTran.RenewalTransactionDetails[0].PackageID;

                    //a la carte packages
                    for (int i = 1; i < renewalTran.RenewalTransactionDetails.Length; i++)
                    {
                        if (String.IsNullOrEmpty(this.PackageIDList))
                            this._packageIDList += renewalTran.RenewalTransactionDetails[i].PackageID;
                        else
                            this._packageIDList += (";" + renewalTran.RenewalTransactionDetails[i].PackageID);
                    }
                }
            }
            else if (this._transactionType == TransactionType.AutoRenewalReopenALaCarte || this.TransactionType == TransactionType.AutoRenewalTerminateALaCarte)
            {
                if (renewalTran.RenewalTransactionDetails != null && renewalTran.RenewalTransactionDetails.Length > 0)
                {
                    //a la carte packages
                    for (int i = 0; i < renewalTran.RenewalTransactionDetails.Length; i++)
                    {
                        if (String.IsNullOrEmpty(this.PackageIDList))
                            this._packageIDList += renewalTran.RenewalTransactionDetails[i].PackageID;
                        else
                            this._packageIDList += (";" + renewalTran.RenewalTransactionDetails[i].PackageID);
                    }
                }
            }
            else if (this._transactionType == TransactionType.FreezeSubscriptionAccount)
            {
                this._totalAmount = -Math.Abs(renewalTran.DeferredAmount);
                if (renewalTran.PrimaryPackageID > 0)
                {
                    this._primaryPackageID = renewalTran.PrimaryPackageID;
                }

                // Frozen time is saved in minutes  
                decimal days = renewalTran.Duration / 1440;
                this._duration = -Math.Abs(Convert.ToInt32(Math.Ceiling(days)));
                this._durationType = Matchnet.DurationType.Day;
            }
            else if (this._transactionType == TransactionType.UnfreezeSubscriptionAccount)
            {
                this._totalAmount = Math.Abs(renewalTran.DeferredAmount);
                if (renewalTran.PrimaryPackageID > 0)
                {
                    this._primaryPackageID = renewalTran.PrimaryPackageID;
                }

                // Frozen time is saved in minutes  
                decimal days = renewalTran.Duration / 1440;
                this._duration = Math.Abs(Convert.ToInt32(Math.Ceiling(days)));
                this._durationType = Matchnet.DurationType.Day;
            }

            if (this._transactionType != TransactionType.FreezeSubscriptionAccount
                && this._transactionType != TransactionType.UnfreezeSubscriptionAccount)
            {
                this._duration = 0;
                this._durationType = DurationType.Month;
            }
            this._accountHistoryType = accountHistoryType;
            this._creditQueueURL = GetCreditQueueBaseURL();
            this._member = member;
            this._brand = brand;
            this._currencyType = CurrencyType.None;
        }

        //this is for redeemed gifts
        public AccountHistory(Spark.Common.AccessService.AccessTransaction accessTran, ContentBrand.Brand brand, Member member, Gift gift)
        {
            _giftRedemption = gift;
            _accountHistoryType = AccountHistoryType.GiftRedemption;
            
            this._insertDateInPST = ConvertUTCToPST(accessTran.InsertDateUTC);
            this._orderID = accessTran.AccessTransactionID;
            this._transactionType = (TransactionType)accessTran.UnifiedTransactionTypeID;
            this._totalAmount = 0.00m;
            this._lastFourAccountNumber = gift.GiftRedemption.LastFourAccountNumber;
            this._callingSystemID = gift.GiftRedemption.CallingSystemID;
            this._paymentType = (Spark.Common.OrderHistoryService.PaymentType)gift.GiftRedemption.PaymentType; this._orderStatus = OrderStatus.Successful;
            this._adminID = Constants.NULL_INT;
            this._promoID = gift.GiftRedemption.PromoID;
            this._primaryPackageID = gift.GiftRedemption.PackageID;

            if (accessTran.AccessTransactionDetails != null)
            {
                foreach (Spark.Common.AccessService.AccessTransactionDetail atd in accessTran.AccessTransactionDetails)
                {
                    if (this._durationType == DurationType.None && atd.Duration != Constants.NULL_INT)
                    {
                        this._duration = atd.Duration;
                        this._durationType = (DurationType)atd.DurationTypeID;
                    }

                    this._privilegeTypeList.Add((Spark.Common.AccessService.PrivilegeType)atd.UnifiedPrivilegeTypeID);
                    if (((Spark.Common.AccessService.PrivilegeType)atd.UnifiedPrivilegeTypeID) == Spark.Common.AccessService.PrivilegeType.AllAccessEmails)
                        _adjustEmailCount = atd.Count;
                }
            }

            this._packageIDList = gift.GiftRedemption.PackageID.ToString();
            this._creditQueueURL = GetCreditQueueBaseURL();
            this._member = member;
            this._brand = brand;
            this._currencyType = CurrencyType.None;
        }


        public AccountHistory(Spark.Common.AccessService.AccessTransaction accessTran, ContentBrand.Brand brand, AccountHistoryType accountHistoryType, Member member)
        {
            this._insertDateInPST = ConvertUTCToPST(accessTran.InsertDateUTC);
            this._orderID = accessTran.AccessTransactionID;
            this._transactionType = (TransactionType)accessTran.UnifiedTransactionTypeID;
            this._totalAmount = 0.00m;
            this._lastFourAccountNumber = "";
            this._callingSystemID = accessTran.CallingSystemID;
            this._paymentType = Spark.Common.OrderHistoryService.PaymentType.None;
            this._orderStatus = OrderStatus.Successful;
            this._adminID = accessTran.AdminUserID;
            this._promoID = Constants.NULL_INT;
            this._primaryPackageID = Constants.NULL_INT;
            this._duration = 0;
            this._durationType = DurationType.None;
            if (accessTran.AccessTransactionDetails != null)
            {
                foreach (Spark.Common.AccessService.AccessTransactionDetail atd in accessTran.AccessTransactionDetails)
                {
                    if (this._durationType == DurationType.None && atd.Duration != Constants.NULL_INT)
                    {
                        this._duration = atd.Duration;
                        this._durationType = (DurationType)atd.DurationTypeID;
                    }

                    this._privilegeTypeList.Add((Spark.Common.AccessService.PrivilegeType)atd.UnifiedPrivilegeTypeID);
                    if (((Spark.Common.AccessService.PrivilegeType)atd.UnifiedPrivilegeTypeID) == Spark.Common.AccessService.PrivilegeType.AllAccessEmails)
                        _adjustEmailCount = atd.Count;
                }
            }
            this._packageIDList = "";

            this._accountHistoryType = accountHistoryType;
            this._creditQueueURL = GetCreditQueueBaseURL();
            this._member = member;
            this._brand = brand;
            this._currencyType = CurrencyType.None;

        }

        public AccountHistory(MemberTran memberTran, ContentBrand.Brand brand, AccountHistoryType accountHistoryType, Member member)
        {
            this._insertDateInPST = memberTran.InsertDate;
            this._orderID = memberTran.MemberTranID;
            // The transaction type is the first transaction type found  
            this._transactionType = ConvertLegacyTransactionTypeToUPSTransactionType(memberTran.TranType, memberTran.Amount);
            this._totalAmount = memberTran.Amount;
            this._lastFourAccountNumber = GetLegacyLastFourAccountNumber(memberTran);
            this._callingSystemID = memberTran.SiteID;
            this._paymentType = ConvertLegacyPaymentTypeToUPSPaymentType(memberTran);
            this._orderStatus = ConvertLegacyTransactionStatusToUPSTransactionStatus(memberTran.MemberTranStatus);
            this._adminID = memberTran.AdminMemberID;
            this._promoID = memberTran.PromoID;
            this._primaryPackageID = memberTran.PlanID;
            this._packageIDList = memberTran.PlanIDList;
            this._duration = memberTran.Duration;
            this._durationType = ConvertLegacyDurationTypeToUPSDurationType(memberTran.DurationType);
            this._accountHistoryType = accountHistoryType;
            this._creditQueueURL = GetCreditQueueBaseURL();
            this._member = member;
            this._brand = brand;
            this._currencyType = memberTran.CurrencyType;

        }

       #endregion

        #region Properties

        public DateTime InsertDateInPST
        {
            get { return this._insertDateInPST; }
            set { this._insertDateInPST = value; }
        }

        public string InsertDateInPSTHtml
        {
            get { return Convert.ToString(this._insertDateInPST); }
        }

        public int OrderID
        {
            get { return this._orderID; }
            set { this._orderID = value; }
        }

        public TransactionType TransactionType
        {
            get { return this._transactionType; }
            set { this._transactionType = value; }
        }

        public string CreditQueueURL
        {
            get
            {
                return string.Format(_creditQueueURL, _member.MemberID.ToString(), _brand.Site.SiteID.ToString(),
                    _member.EmailAddress, _orderID.ToString(), _totalAmount.ToString(), _currencyType.ToString());
            }
        }

        public string BillingInfoURL
        {
            get
            {
                if (TransactionType == TransactionType.GiftRedeem && RedeemedGift != null)
                {
                    return string.Format("/MemberAPI/BillingInfoForGiftRedemption/{0}/{1}/{2}",
                                         RedeemedGift.Code, _brand.Site.SiteID.ToString(), _member.MemberID);
                }
                else if (TransactionType == TransactionType.Authorization)
                {
                    return string.Format("/MemberAPI/BillingInfoForFreeTrial/{0}/{1}/{2}",
                                         UserPaymentGUID, _brand.Site.SiteID.ToString(), _member.MemberID);
                }
                else
                {
                    return string.Format("/MemberAPI/BillingInfo/{0}/{1}/{2}",
                                            OrderID, _brand.Site.SiteID.ToString(), _member.MemberID);
                }
            }
        }

        public string TransactionTypeDisplay
        {
            get
            {
                string resourceConstant = Constants.NULL_STRING;

                switch (this._transactionType)
                {
                    case TransactionType.InitialSubscriptionPurchase:
                        resourceConstant = "Initial Buy";
                        break;
                    case TransactionType.Renewal:
                        resourceConstant = "Renewal";
                        break;
                    case TransactionType.Credit:
                        resourceConstant = "Credit";
                        break;
                    case TransactionType.Void:
                        resourceConstant = "Void";
                        break;
                    case TransactionType.AutoRenewalTerminate:
                        resourceConstant = "Termination";
                        break;
                    case TransactionType.Adjustment:
                        resourceConstant = "Administrative Adjustment";
                        break;
                    case TransactionType.Authorization:
                        resourceConstant = "Authorization Only";
                        break;
                    case TransactionType.PaymentProfileAuthorization:
                        resourceConstant = "Payment Profile Authorization";
                        break;
                    case TransactionType.TrialPayAdjustment:
                        resourceConstant = "Trial Pay Adjustment";
                        break;
                    case TransactionType.AutoRenewalReopen:
                        resourceConstant = "Reopen Auto Renewal";
                        break;
                    case TransactionType.AdditionalSubscriptionPurchase:
                        resourceConstant = "Additional Subscription Purchase";
                        break;
                    case TransactionType.AdditionalNonSubscriptionPurchase:
                        resourceConstant = "Additional Nonsubscription Purchase";
                        break;
                    case TransactionType.AutoRenewalReopenALaCarte:
                        resourceConstant = "Reopen Auto Renewal A La Carte";
                        break;
                    case TransactionType.AutoRenewalTerminateALaCarte:
                        resourceConstant = "Termination A La Carte";
                        break;
                    case TransactionType.LifetimeMemberAdjustment:
                        resourceConstant = "Lifetime Member Adjustment";
                        break;
                    case TransactionType.GiftRedeem:
                        resourceConstant = "Gift Redemption";
                        break;
                    case TransactionType.FreezeSubscriptionAccount:
                        resourceConstant = "Subscription Pause";
                        break;
                    case TransactionType.UnfreezeSubscriptionAccount:
                        resourceConstant = "Subscription Resume";
                        break;
                    case TransactionType.ManualPayment:
                        resourceConstant = "Manual Payment ";
                        break;
                    case TransactionType.ChargeBack:
                        resourceConstant = "Chargeback";
                        break;
                    case TransactionType.RenewalRecyclingTimeAdjustment:
                        resourceConstant = "Renewal Recycling Time Adjustment";
                        break;
                    default:
                        resourceConstant = "Unknown";
                        break;
                }

                //return Enum.GetName(typeof(TransactionType), this._transactionType) + RESOURCE_CONSTANT;

                return resourceConstant;
            }
        }

        public decimal TotalAmount
        {
            get { return this._totalAmount; }
            set { this._totalAmount = value; }
        }

        public string TotalAmountHtml
        {
            get { return String.Format("{0:#0.00}", TotalAmount); }
        }

        public string LastFourAccountNumber
        {
            get { return this._lastFourAccountNumber; }
            set { this._lastFourAccountNumber = value; }
        }

        public int CallingSystemID
        {
            get { return this._callingSystemID; }
            set { this._callingSystemID = value; }
        }

        public Spark.Common.OrderHistoryService.PaymentType PaymentType
        {
            get { return this._paymentType; }
            set { this._paymentType = value; }
        }

        public string PaymentTypeDisplay
        {
            get
            {
                string resourceConstant = Constants.NULL_STRING;

                switch (this._paymentType)
                {
                    case Spark.Common.OrderHistoryService.PaymentType.Check:
                        resourceConstant = "Check";
                        break;
                    case Spark.Common.OrderHistoryService.PaymentType.CreditCard:
                    case Spark.Common.OrderHistoryService.PaymentType.CreditCardShortFormMobileSite:
                    case Spark.Common.OrderHistoryService.PaymentType.CreditCardShortFormFullWebSite:
                        resourceConstant = "Credit Card";
                        break;
                    case Spark.Common.OrderHistoryService.PaymentType.SMS:
                        resourceConstant = "SMS";
                        break;
                    case Spark.Common.OrderHistoryService.PaymentType.PayPalLitle:
                        resourceConstant = "PayPal Litle";
                        break;
                    case Spark.Common.OrderHistoryService.PaymentType.PaypalDirect:
                        resourceConstant = "PayPal Direct";
                        break;
                    case Spark.Common.OrderHistoryService.PaymentType.Manual:
                        resourceConstant = "Manual";
                        break;
                    case Spark.Common.OrderHistoryService.PaymentType.ElectronicFundsTransfer:
                        resourceConstant = "Electronic Funds Transfer";
                        break;
                    case Spark.Common.OrderHistoryService.PaymentType.PaymentReceived:
                        resourceConstant = "Payment Received";
                        break;
                    case Spark.Common.OrderHistoryService.PaymentType.DebitCard:
                        resourceConstant = "Debit Card";
                        break;
                    case Spark.Common.OrderHistoryService.PaymentType.InApplicationPurchase:
                        resourceConstant = "iTunes Purchase";
                        break;
                    default:
                        resourceConstant = "Unknown";
                        break;
                }

                return resourceConstant;
            }
        }

        public OrderStatus OrderStatus
        {
            get { return this._orderStatus; }
            set { this._orderStatus = value; }
        }

        public string OrderStatusString
        {
            get { return this._orderStatus.ToString(); }
        }

        public Gift PurchasedGift
        {
            get { return this._giftPurchase; }
        }

        public string PurchasedGiftCode
        {
            get { return (PurchasedGift != null) ? PurchasedGift.Code : ""; }
        }

        public string PurchasedGiftType
        {
            get { return (PurchasedGift != null) ? PurchasedGift.GiftType.ToString() : ""; }
        }

        public string PurchasedGiftBeginValidDate
        {
            get { return (PurchasedGift != null) ? PurchasedGift.BeginValidDate.ToShortDateString() : ""; }
        }

        public string PurchasedGiftExpirationDate
        {
            get { return (PurchasedGift != null) ? PurchasedGift.ExpirationDate.ToShortDateString() : ""; }
        }

        public string PurchasedGiftIsValid
        {
            get { return (PurchasedGift != null) ? PurchasedGift.IsValid.ToYesNoString() : ""; }
        }

        public string PurchasedGiftStatus
        {
            get { return (PurchasedGift != null) ? PurchasedGift.Status.ToString() : ""; }
        }

        public bool PurchasedGiftRedemption
        {
            get { return (PurchasedGift != null && PurchasedGift.GiftRedemption != null && PurchasedGift.GiftRedemption.CustomerID > 0) ; }
        }

        public int PurchasedGiftRedemptionCallingSystemId
        {
            get { return (PurchasedGift != null && PurchasedGift.GiftRedemption != null) ? PurchasedGift.GiftRedemption.CallingSystemID : 0; }
        }

        public int PurchasedGiftRedemptionCustomerId
        {
            get
            {
                return (PurchasedGift != null && PurchasedGift.GiftRedemption != null)
                           ? PurchasedGift.GiftRedemption.CustomerID
                           : 0;
            }
        }

        public Gift RedeemedGift
        {
            get { return this._giftRedemption; }
        }

        public string RedeemedGiftCode
        {
            get { return (RedeemedGift != null) ? RedeemedGift.Code : ""; }
        }

        public string RedeemedGiftType
        {
            get { return (RedeemedGift != null) ? RedeemedGift.GiftType.ToString() : ""; }
        }

        public int RedeemedGiftCallingSystemId
        {
            get { return (RedeemedGift != null) ? RedeemedGift.CallingSystemID : 0; }
        }

        public int RedeemedGiftCustomerId
        {
            get { return (RedeemedGift != null) ? RedeemedGift.CustomerID : 0; }
        }

        public bool IsGiftPurchase
        {
            get { return (_giftPurchase != null); }
        }

        public bool IsGiftRedemption
        {
            get { return (_giftRedemption != null); }
        }

        public bool IsAdminAdjust
        {
            get { return TransactionType == TransactionType.Adjustment; }
        }

        public bool IsNotAdminAdjust
        {
            get { return TransactionType != TransactionType.Adjustment; }
        }

        public void AddPuchasedGift(Gift gift)
        {
            _giftPurchase = gift;
            _accountHistoryType = AccountHistoryType.FinancialWithGift;
        }

        public void AddRedeemedGift(Gift gift)
        {
            _giftRedemption = gift;
            _accountHistoryType = AccountHistoryType.GiftRedemption;
        }

        public string OrderStatusDisplay
        {
            get
            {
                string resourceConstant = Constants.NULL_STRING;

                switch (this._orderStatus)
                {
                    case OrderStatus.Pending:
                        resourceConstant = "Pending";
                        break;
                    case OrderStatus.Successful:
                        resourceConstant = "Success";
                        break;
                    case OrderStatus.Failed:
                        resourceConstant = "Failure";
                        break;
                    default:
                        resourceConstant = "Unknown";
                        break;
                }

                return resourceConstant;
            }
        }

        public int AdminID
        {
            get { return this._adminID; }
            set { this._adminID = value; }
        }

        public string AdminUsername
        {
            get { return this._adminUserName; }
            set { this._adminUserName = value; }
        }

        public string AdminDisplay
        {
            get
            {
                string adminDisplay = String.Empty;

                if (!String.IsNullOrEmpty(this._adminUserName))
                {
                    adminDisplay = this._adminUserName.Replace(@"MATCHNET\", "");
                }
                else if (this._adminID != Constants.NULL_INT && this._adminID > 0)
                {
                    Matchnet.Member.ServiceAdapters.Member member = MemberHelper.GetMember(this._adminID, MemberLoadFlags.None);

                    if (member != null)
                    {
                        adminDisplay = member.EmailAddress;
                    }
                }

                return adminDisplay;
            }
        }

        public string AdminDisplayHtml
        {
            get { return (string.IsNullOrEmpty(AdminDisplay) ? "&nbsp;" : "admin:" + AdminDisplay); }
        }
    
        public int PromoID
        {
            get { return this._promoID; }
            set { this._promoID = value; }
        }

        public string PromoIDHtml
        {
            get { return (_promoID > 0 ? Convert.ToString(PromoID) : String.Empty); }
        }

        public string PromoDescription { get; set; }

        public int PrimaryPackageID
        {
            get { return this._primaryPackageID; }
            set { this._primaryPackageID = value; }
        }

        public string PrimaryPackageIDHTML
        {
            get { return (_primaryPackageID > 0 ? Convert.ToString(_primaryPackageID) : String.Empty); }
        }

        public Plan PrimaryPlan
        {
            get
            {
                try
                {
                    Matchnet.Purchase.ValueObjects.Plan plan = Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(this._primaryPackageID, this._brand.BrandID);
                    return plan;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public bool HasPrimaryPlan
        {
            get { return PrimaryPlan != null; }
        }

        public string RenewalRate
        {
            get
            {
                if(PrimaryPlan != null)
                {
                    return String.Format("{0:#0.00}", PrimaryPlan.RenewCost);
                }
                return "N/A";
            }
        }

        public string RenewalDurationHtml
        {
            get
            {
                if(PrimaryPlan != null)
                {
                    return (PrimaryPlan.RenewDuration == 0)
                               ? PrimaryPlan.RenewDuration.ToString()
                               : PrimaryPlan.RenewDuration + " " + RenewalDurationTypeDisplay;
                }
                return "";
            }
        }

        public string PackageResourceConstant
        {
            get
            {
                try
                {
                    string resourceConstant = Constants.NULL_STRING;

                    if (this.IsOneOffPurchase)
                    {
                        resourceConstant = "ONEOFF_PACKAGE";
                    }
                    else
                    {
                        Matchnet.Purchase.ValueObjects.Plan plan = Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(this._primaryPackageID, this._brand.BrandID);
                        resourceConstant = plan.PlanResourceConstant;
                    }

                    return resourceConstant;
                }
                catch (Exception ex)
                {
                    return "MISSING_PACKAGEID_" + Convert.ToString(this._primaryPackageID);
                }
            }
        }

        public int MinValue
        {
            get { return int.MinValue; }
        }

        public string PackageIDList
        {
            get { return this._packageIDList; }
            set { this._packageIDList = value; }
        }

        public int Duration
        {
            get { return this._duration; }
            set { this._duration = value; }
        }

        public DurationType DurationType
        {
            get { return this._durationType; }
            set { this._durationType = value; }
        }

        public string DurationHtml
        {
            get { return (_duration == 0) ? _duration.ToString() : _duration + " " + DurationTypeDisplay; }
        }

        public bool IsOneOffPurchase
        {
            get { return this._isOneOffPurchase; }
            set { this._isOneOffPurchase = value; }
        }

        public string DurationTypeDisplay
        {
            get
            {
                string resourceConstant = Constants.NULL_STRING;

                switch (this._durationType)
                {
                    case DurationType.Minute:
                        resourceConstant = "Minutes";
                        break;
                    case DurationType.Hour:
                        resourceConstant = "Hour";
                        break;
                    case DurationType.Day:
                        resourceConstant = "Day";
                        break;
                    case DurationType.Week:
                        resourceConstant = "Week";
                        break;
                    case DurationType.Month:
                        resourceConstant = "Month";
                        break;
                    case DurationType.Year:
                        resourceConstant = "Year";
                        break;
                    default:
                        resourceConstant = "Unknown";
                        break;
                }

                return resourceConstant;
            }
        }

        public string RenewalDurationTypeDisplay
        {
            get
            {
                string resourceConstant = Constants.NULL_STRING;
                if (PrimaryPlan != null)
                {
                    switch (ConvertLegacyDurationTypeToUPSDurationType(this.PrimaryPlan.RenewDurationType))
                    {
                        case DurationType.Minute:
                            resourceConstant = "Minutes";
                            break;
                        case DurationType.Hour:
                            resourceConstant = "Hour";
                            break;
                        case DurationType.Day:
                            resourceConstant = "Day";
                            break;
                        case DurationType.Week:
                            resourceConstant = "Week";
                            break;
                        case DurationType.Month:
                            resourceConstant = "Month";
                            break;
                        case DurationType.Year:
                            resourceConstant = "Year";
                            break;
                        default:
                            resourceConstant = "Unknown";
                            break;
                    }
                }

                return resourceConstant;
            }
        }

        public AccountHistoryType AccountHistoryType
        {
            get { return this._accountHistoryType; }
            set { this._accountHistoryType = value; }
        }

        public string AccountHistoryTypeString
        {
            get { return this._accountHistoryType.ToString(); }
        }

        public string IncludedAlaCarteItemsDisplay
        {
            get
            {
                try
                {
                    List<string> arrDiscountPlanResourceConstants = new List<string>();
                    List<string> arrAlaCartePlanResourceConstants = new List<string>();
                    if (TransactionType != TransactionType.Adjustment
                        && TransactionType != TransactionType.RenewalRecyclingTimeAdjustment
                        && TransactionType != TransactionType.AutoRenewalReopenALaCarte
                        && TransactionType != TransactionType.AutoRenewalTerminateALaCarte)
                    {
                        string planIDList = this.PackageIDList;
                        string[] arrPlanID = planIDList.Split(';');
                        
                        foreach (string singlePlanID in arrPlanID)
                        {
                            Matchnet.Purchase.ValueObjects.Plan singlePlan = null;
                            try
                            {
                                singlePlan = Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(Convert.ToInt32(singlePlanID), this._brand.BrandID);
                            }
                            catch (Exception ex)
                            {
                                singlePlan = null;
                            }

                            if (singlePlan != null)
                            {
                                Matchnet.Purchase.ValueObjects.PlanServiceCollection planServiceCol = singlePlan.PlanServices;

                                // A single plan can be a regular or bundled plan, an ala carte plan, or a discount plan
                                // A plan cannot be both a bundled plan as well as a discount plan for example  
                                if ((singlePlan.PlanTypeMask & PlanType.ALaCarte) == PlanType.ALaCarte)
                                {
                                    foreach (PlanService singlePlanService in planServiceCol)
                                    {
                                        string planServiceResourceConstant = (Enum.GetName(typeof(PlanServiceDefinition), singlePlanService.PlanSvcDefinition));
                                        planServiceResourceConstant = planServiceResourceConstant.Replace("_", " ");

                                        if (!arrAlaCartePlanResourceConstants.Contains(planServiceResourceConstant))
                                        {
                                            arrAlaCartePlanResourceConstants.Add(planServiceResourceConstant);
                                        }
                                    }
                                }
                                else if ((singlePlan.PlanTypeMask & PlanType.PremiumPlan) == PlanType.PremiumPlan)
                                {
                                    foreach (int p in System.Enum.GetValues(typeof(PremiumType)))
                                    {
                                        if ((singlePlan.PremiumTypeMask & ((PremiumType)p)) > 0)
                                        {
                                            string premiumServiceDescription = System.Enum.GetName(typeof(PremiumType), p);
                                            if (!arrAlaCartePlanResourceConstants.Contains(premiumServiceDescription))
                                            {
                                                arrAlaCartePlanResourceConstants.Add(premiumServiceDescription);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (this.IsOneOffPurchase)
                        {
                            string planServiceResourceConstant = "Color Code Purchase";
                            arrAlaCartePlanResourceConstants.Add(planServiceResourceConstant);
                        }

                        if (arrAlaCartePlanResourceConstants.Count > 0)
                        {
                            StringBuilder alaCarteList = new StringBuilder();
                            foreach (string singleAlaCarte in arrAlaCartePlanResourceConstants)
                            {
                                if (alaCarteList.ToString().Length < 1)
                                {
                                    alaCarteList.Append(singleAlaCarte);
                                }
                                else
                                {
                                    alaCarteList.Append(" ," + singleAlaCarte);
                                }
                            }

                            return alaCarteList.ToString();
                        }
                        else
                        {
                            return "N/A";
                        }
                    }
                    else
                    {
                        if (TransactionType == TransactionType.AutoRenewalReopenALaCarte
                            || TransactionType == TransactionType.AutoRenewalTerminateALaCarte)
                        {
                            //convert plan to privilege list
                            if (PackageIDList != null)
                            {
                                string planIDList = PackageIDList;
                                string[] arrPlanID = planIDList.Split(';');

                                foreach (string singlePlanID in arrPlanID)
                                {
                                    Matchnet.Purchase.ValueObjects.Plan singlePlan = null;
                                    try
                                    {
                                        singlePlan = Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(Convert.ToInt32(singlePlanID), this._brand.BrandID);
                                    }
                                    catch (Exception ex)
                                    {
                                        singlePlan = null;
                                    }
                                    if (singlePlan != null)
                                    {
                                        Matchnet.Purchase.ValueObjects.PlanServiceCollection planServiceCol = singlePlan.PlanServices;

                                        // A single plan can be a regular or bundled plan, an ala carte plan, or a discount plan
                                        // A plan cannot be both a bundled plan as well as a discount plan for example  
                                        if ((singlePlan.PlanTypeMask & PlanType.ALaCarte) == PlanType.ALaCarte)
                                        {
                                            if ((singlePlan.PremiumTypeMask & PremiumType.HighlightedProfile) == PremiumType.HighlightedProfile)
                                            {
                                                PrivilegeTypeList.Add(Spark.Common.AccessService.PrivilegeType.HighlightedProfile);
                                            }
                                            else if ((singlePlan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember)
                                            {
                                                PrivilegeTypeList.Add(Spark.Common.AccessService.PrivilegeType.SpotlightMember);
                                            }
                                            else if ((singlePlan.PremiumTypeMask & PremiumType.JMeter) == PremiumType.JMeter)
                                            {
                                                PrivilegeTypeList.Add(Spark.Common.AccessService.PrivilegeType.JMeter);
                                            }
                                            else if ((singlePlan.PremiumTypeMask & PremiumType.AllAccess) == PremiumType.AllAccess)
                                            {
                                                PrivilegeTypeList.Add(Spark.Common.AccessService.PrivilegeType.AllAccess);
                                            }
                                            else if ((singlePlan.PremiumTypeMask & PremiumType.ReadReceipt) == PremiumType.ReadReceipt)
                                            {
                                                PrivilegeTypeList.Add(Spark.Common.AccessService.PrivilegeType.ReadReceipt);
                                            }
                                        }
                                    }
                                }
                            }

                        }

                        if (PrivilegeTypeList.Count > 0)
                        {
                            StringBuilder sbDetail = new StringBuilder();
                            foreach (Spark.Common.AccessService.PrivilegeType privilege in PrivilegeTypeList)
                            {
                                if (privilege == Spark.Common.AccessService.PrivilegeType.AllAccessEmails)
                                {
                                    sbDetail.Append(privilege.ToString() + "(" + _adjustEmailCount.ToString() + "), ");
                                }
                                else
                                {
                                    sbDetail.Append(privilege.ToString() + ", ");
                                }
                            }

                            if (sbDetail.ToString().Length > 3)
                                return sbDetail.ToString().Substring(0, sbDetail.ToString().Length - 2);

                        }
                    }

                    return "N/A";

                }
                catch (Exception ex)
                {
                    return "N/A";
                }
            }
        }

        public string IncludedDiscountItemsDisplay
        {
            get
            {
                try
                {
                    string planIDList = this.PackageIDList;
                    string[] arrPlanID = planIDList.Split(';');
                    List<string> arrDiscountPlanResourceConstants = new List<string>();
                    List<string> arrAlaCartePlanResourceConstants = new List<string>();

                    foreach (string singlePlanID in arrPlanID)
                    {
                        Matchnet.Purchase.ValueObjects.Plan singlePlan = Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(Convert.ToInt32(singlePlanID), this._brand.BrandID);
                        if (singlePlan != null)
                        {
                            Matchnet.Purchase.ValueObjects.PlanServiceCollection planServiceCol = singlePlan.PlanServices;

                            if ((singlePlan.PlanTypeMask & PlanType.Discount) == PlanType.Discount)
                            {
                                // Show the plan description for discounts  
                                string discountPlanResourceConstant = Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlanDescription(Convert.ToInt32(singlePlanID));
                                arrDiscountPlanResourceConstants.Add(discountPlanResourceConstant);
                            }
                        }
                    }

                    if (arrDiscountPlanResourceConstants.Count > 0)
                    {
                        StringBuilder discountList = new StringBuilder();
                        foreach (string singleDiscountResourceConstant in arrDiscountPlanResourceConstants)
                        {
                            if (discountList.ToString().Length < 1)
                            {
                                discountList.Append(singleDiscountResourceConstant);
                            }
                            else
                            {
                                discountList.Append(" ," + singleDiscountResourceConstant);
                            }
                        }

                        return discountList.ToString();
                    }
                    else
                    {
                        return "N/A";
                    }
                }
                catch (Exception ex)
                {
                    return "N/A";
                }
            }
        }

        public List<Spark.Common.AccessService.PrivilegeType> PrivilegeTypeList
        {
            get { return this._privilegeTypeList; }
            set { this._privilegeTypeList = value; }
        }

        public int AdjustEmailCount
        {
            get { return this._adjustEmailCount; }
            set { this._adjustEmailCount = value; }
        }

        public decimal DeferredAmount
        {
            get { return this._deferredAmount; }
            set { this._deferredAmount = value; }
        }

        public int AuthorizationPaymentID
        {
            get { return this._authorizationPaymentID; }
            set { this._authorizationPaymentID = value; }
        }

        public int BillingPaymentID
        {
            get { return this._billingPaymentID; }
            set { this._billingPaymentID = value; }
        }

        public string InternalStatusMessage
        {
            get { return this._internalStatusMessage; }
            set { this._internalStatusMessage = value; }
        }

        public string UserPaymentGUID
        {
            get { return this._userPaymentGUID; }
            set { this._userPaymentGUID = value; }
        }

        public string DiscountCode
        {
            get { return this._discountCode; }
            set { this._discountCode = value; }
        }

        public string DiscountAmountForDisplay
        {
            get { return this._discountAmountForDisplay; }
            set { this._discountAmountForDisplay = value; }
        }

        public bool HasDiscount
        {
            get;set;
        }

        #endregion

        #region Methods

        private string GetCreditQueueBaseURL()
        {
            return RuntimeSettings.GetSetting("MINGLE_CREDIT_QUEUE_URL") + "?memberid={0}&siteid={1}&email={2}&tranid={3}&amount={4}&currency={5}"; 
        }

        private List<TransactionType> GetAllUPSTransactionTypes(OrderInfo orderInfo)
        {
            List<TransactionType> arrOrderTransactionTypes = new List<TransactionType>();
            foreach (OrderDetailInfo orderDetailInfo in orderInfo.OrderDetail)
            {
                if (orderDetailInfo.OrderTypeID != Constants.NULL_INT)
                {
                    TransactionType orderDetailTransactionType = (TransactionType)orderDetailInfo.OrderTypeID;
                    if (!arrOrderTransactionTypes.Contains(orderDetailTransactionType))
                    {
                        arrOrderTransactionTypes.Add(orderDetailTransactionType);
                    }
                }
            }

            return arrOrderTransactionTypes;
        }

        private TransactionType ConvertLegacyTransactionTypeToUPSTransactionType(TranType legacyTranType, decimal amount)
        {
            TransactionType upsTransactionType = TransactionType.None;
            switch (legacyTranType)
            {
                case TranType.AdministrativeAdjustment:
                    if (Math.Abs(amount) > 0)
                    {
                        upsTransactionType = TransactionType.Credit;
                    }
                    else
                    {
                        upsTransactionType = TransactionType.Adjustment;
                    }
                    break;
                case TranType.AuthorizationNoTran:
                    upsTransactionType = TransactionType.Authorization;
                    break;
                case TranType.AuthorizationOnly:
                    upsTransactionType = TransactionType.Authorization;
                    break;
                case TranType.AuthPmtProfile:
                    upsTransactionType = TransactionType.PaymentProfileAuthorization;
                    break;
                case TranType.Discount:
                    upsTransactionType = TransactionType.None;
                    break;
                case TranType.InitialBuy:
                    upsTransactionType = TransactionType.InitialSubscriptionPurchase;
                    break;
                case TranType.ReceivedCheck:
                    upsTransactionType = TransactionType.None;
                    break;
                case TranType.Renewal:
                    upsTransactionType = TransactionType.Renewal;
                    break;
                case TranType.ReopenAutoRenewal:
                    upsTransactionType = TransactionType.AutoRenewalReopen;
                    break;
                case TranType.Termination:
                    upsTransactionType = TransactionType.AutoRenewalTerminate;
                    break;
                case TranType.TrialPayAdjustment:
                    upsTransactionType = TransactionType.TrialPayAdjustment;
                    break;
                case TranType.Void:
                    upsTransactionType = TransactionType.Void;
                    break;
                default:
                    break;
            }

            return upsTransactionType;
        }

        private string GetLegacyLastFourAccountNumber(MemberTran memberTran)
        {
            string legacyLastFourAccountNumber = Constants.NULL_STRING;

            if (memberTran.AccountNumber != null && memberTran.AccountNumber.Length > 0)
            {
                legacyLastFourAccountNumber = memberTran.AccountNumber;
            }
            else if (memberTran.LegacyLastFourAccountNumber != null && memberTran.LegacyLastFourAccountNumber.Length > 0)
            {
                legacyLastFourAccountNumber = memberTran.LegacyLastFourAccountNumber;
            }

            return legacyLastFourAccountNumber;
        }

        private Spark.Common.OrderHistoryService.PaymentType ConvertLegacyPaymentTypeToUPSPaymentType(MemberTran memberTran)
        {
            Spark.Common.OrderHistoryService.PaymentType upsPaymentType = Spark.Common.OrderHistoryService.PaymentType.None;

            switch (memberTran.PaymentType)
            {
                case Matchnet.Purchase.ValueObjects.PaymentType.None:
                    switch (memberTran.PaymentTypeResourceConstant)
                    {
                        case "CHECK":
                            upsPaymentType = Spark.Common.OrderHistoryService.PaymentType.Check;
                            break;
                        case "CREDIT_CARD":
                            upsPaymentType = Spark.Common.OrderHistoryService.PaymentType.CreditCard;
                            break;
                        case "SMS":
                            upsPaymentType = Spark.Common.OrderHistoryService.PaymentType.SMS;
                            break;
                        default:
                            upsPaymentType = Spark.Common.OrderHistoryService.PaymentType.None;
                            break;
                    }
                    break;
                case Matchnet.Purchase.ValueObjects.PaymentType.Check:
                    upsPaymentType = Spark.Common.OrderHistoryService.PaymentType.Check;
                    break;
                case Matchnet.Purchase.ValueObjects.PaymentType.CreditCard:
                    upsPaymentType = Spark.Common.OrderHistoryService.PaymentType.CreditCard;
                    break;
                case Matchnet.Purchase.ValueObjects.PaymentType.PaypalLitle:
                    upsPaymentType = Spark.Common.OrderHistoryService.PaymentType.PayPalLitle;
                    break;
                case Matchnet.Purchase.ValueObjects.PaymentType.Manual:
                    upsPaymentType = Spark.Common.OrderHistoryService.PaymentType.Manual;
                    break;
                case Matchnet.Purchase.ValueObjects.PaymentType.ElectronicFundsTransfer:
                    upsPaymentType = Spark.Common.OrderHistoryService.PaymentType.ElectronicFundsTransfer;
                    break;
                case Matchnet.Purchase.ValueObjects.PaymentType.PaymentReceived:
                    upsPaymentType = Spark.Common.OrderHistoryService.PaymentType.PaymentReceived;
                    break;
                default:
                    upsPaymentType = Spark.Common.OrderHistoryService.PaymentType.None;
                    break;
            }

            return upsPaymentType;
        }

        private OrderStatus ConvertLegacyTransactionStatusToUPSTransactionStatus(MemberTranStatus legacyTransactionStatus)
        {
            OrderStatus upsTransactionStatus = OrderStatus.None;

            switch (legacyTransactionStatus)
            {
                case MemberTranStatus.None:
                    upsTransactionStatus = OrderStatus.None;
                    break;
                case MemberTranStatus.Pending:
                    upsTransactionStatus = OrderStatus.Pending;
                    break;
                case MemberTranStatus.Success:
                    upsTransactionStatus = OrderStatus.Successful;
                    break;
                case MemberTranStatus.Failure:
                    upsTransactionStatus = OrderStatus.Failed;
                    break;
                default:
                    upsTransactionStatus = OrderStatus.None;
                    break;
            }

            return upsTransactionStatus;
        }

        private string GetPackageIDList(OrderInfo orderInfo)
        {
            List<int> arrPackageID = new List<int>();
            foreach (OrderDetailInfo orderDetailInfo in orderInfo.OrderDetail)
            {
                if (!arrPackageID.Contains(orderDetailInfo.PackageID))
                {
                    arrPackageID.Add(orderDetailInfo.PackageID);
                }
            }
            StringBuilder sb = new StringBuilder();
            foreach (int packageID in arrPackageID)
            {
                if (sb.ToString().Length > 0)
                {
                    sb.Append(";" + Convert.ToString(packageID));
                }
                else
                {
                    sb.Append(Convert.ToString(packageID));
                }
            }

            return sb.ToString();
        }


        private string GetPackageIDList(OrderHistory orderInfo)
        {
            List<int> arrPackageID = new List<int>();
            foreach (OrderDetail orderDetailInfo in orderInfo.OrderDetail)
            {
                if (!arrPackageID.Contains(orderDetailInfo.PackageID))
                {
                    arrPackageID.Add(orderDetailInfo.PackageID);
                }
            }
            StringBuilder sb = new StringBuilder();
            foreach (int packageID in arrPackageID)
            {
                if (sb.ToString().Length > 0)
                {
                    sb.Append(";" + Convert.ToString(packageID));
                }
                else
                {
                    sb.Append(Convert.ToString(packageID));
                }
            }

            return sb.ToString();
        }

        private DurationType ConvertLegacyDurationTypeToUPSDurationType(Matchnet.DurationType legacyDurationType)
        {
            DurationType upsDurationType = DurationType.None;

            switch (legacyDurationType)
            {
                case Matchnet.DurationType.Minute:
                    upsDurationType = DurationType.Minute;
                    break;
                case Matchnet.DurationType.Hour:
                    upsDurationType = DurationType.Hour;
                    break;
                case Matchnet.DurationType.Day:
                    upsDurationType = DurationType.Day;
                    break;
                case Matchnet.DurationType.Week:
                    upsDurationType = DurationType.Week;
                    break;
                case Matchnet.DurationType.Month:
                    upsDurationType = DurationType.Month;
                    break;
                case Matchnet.DurationType.Year:
                    upsDurationType = DurationType.Year;
                    break;
                default:
                    upsDurationType = DurationType.None;
                    break;
            }

            return upsDurationType;
        }

        public DateTime ConvertUTCToPST(DateTime transactionDate)
        {
            TimeZoneInfo timeZoneInfoInPST = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
            DateTime transactionInsertDateInPST = TimeZoneInfo.ConvertTimeFromUtc(transactionDate, timeZoneInfoInPST);

            return transactionInsertDateInPST;
        }

        

        public static DiscountTransaction GetDiscountInfo(OrderHistory oh)
        {
            DiscountTransaction dt = null;

            foreach (OrderDetail od in oh.OrderDetail)
            {
                if (od.DiscountAmount > 0.0 && !String.IsNullOrEmpty(od.DiscountCode))
                {
                    dt = new DiscountTransaction();
                    dt.DiscountAmount = od.DiscountAmount;
                    dt.DiscountCode = od.DiscountCode;
                    dt.DiscountType = od.DiscountType;
                    dt.OrderId = oh.OrderID;
                    break;
                }
            }

            return dt;
        }

        #endregion
    }

    public enum AccountHistoryType
    {
        None = 0,
        Financial = 1,
        NonFinancial = 2,
        FinancialWithGift=3,
        GiftRedemption=4
    }

    /// <summary>
    /// Order transaction types
    /// </summary>
    public enum TransactionType
    {
        None = 0,
        InitialSubscriptionPurchase = 1,
        Renewal = 2,
        Credit = 3,
        Void = 4,
        AutoRenewalTerminate = 5,
        Authorization = 6,
        PaymentProfileAuthorization = 10,
        PlanChange = 11,
        TrialPayAdjustment = 12,
        AutoRenewalReopen = 18,
        AdditionalSubscriptionPurchase = 23, //upgrade before
        AdditionalNonSubscriptionPurchase = 24, //downgrade before
        VirtualTerminalPurchase = 34,
        VirtualTerminalCredit = 35,
        VirtualTerminalVoid = 36,
        BatchRenewal = 37,
        LifetimeMemberAdjustment = 38,
        AutoRenewalTerminateALaCarte = 39,
        AutoRenewalReopenALaCarte = 40,
        FreezeSubscriptionAccount = 46,
        UnfreezeSubscriptionAccount = 47,
        ManualPayment = 48,		
        TrialTakenInitialSubscription = 1001,
        ChargeBack = 1004,
        CheckReversal = 1005,
        Adjustment = 1007,
        GiftPurchase = 1013,
        GiftRedeem = 1014,
        RenewalRecyclingTimeAdjustment = 1015
    }

    /// <summary>
    /// The status of the order    
    /// </summary>
    public enum OrderStatus : int
    {
        None = 0,
        Pending = 1,
        Successful = 2,
        Failed = 3
    }
}
