﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models
{
    public static class WebConstants
    {
        #region ENVIRONMENTS
        public const string ENV_DEV = "dev";
        public const string ENV_STAGE = "stage";
        public const string ENV_PROD = "prod";

        #endregion

        public const string ATTRIBUTE_NAME_COLORCODEQUIZANSWERS = "ColorCodeQuizAnswers";
        public const string ATTRIBUTE_NAME_COLORCODEPRIMARYCOLOR = "ColorCodePrimaryColor";
        public const string ATTRIBUTE_NAME_COLORCODESECONDARYCOLOR = "ColorCodeSecondaryColor";
        public const string ATTRIBUTE_NAME_COLORCODEBLUESCORE = "ColorCodeBlueScore";
        public const string ATTRIBUTE_NAME_COLORCODEYELLOWSCORE = "ColorCodeYellowScore";
        public const string ATTRIBUTE_NAME_COLORCODEWHITESCORE = "ColorCodeWhiteScore";
        public const string ATTRIBUTE_NAME_COLORCODEREDSCORE = "ColorCodeRedScore";
        public const string ATTRIBUTE_NAME_COLORCODEHIDDEN = "ColorCodeHidden";
        public const string ATTRIBUTE_NAME_COLORCODEQUIZCOMPLETEDATE = "ColorCodeQuizCompleteDate";
        public const string ATTRIBUTE_NAME_GLOBALSTATUSMASK = "GlobalStatusMask";
        public const string ATTRIBUTE_NAME_BIRTHDATE = "Birthdate";
        public const string ATTRIBUTE_NAME_GENDERMASK = "GenderMask";
        public const string ATTRIBUTE_NAME_BRANDLUGGAGE = "Luggage";
        public const string ATTRIBUTE_NAME_BRANDPROMOTIONID = "PromotionID";

        public const string ERROR_KEY = "ErrorPageMessage";
        public const int MAX_REGION_STRING_LENGTH = 40;
        public const int REGIONID_USA = 223;
        public const int GENDERID_FEMALE = 2;
        public const int GENDERID_FTM = 32;
        public const int GENDERID_MALE = 1;
        public const int GENDERID_MTF = 16;
        
        public const int GLOBALSTATUSMASK_ADMIN_SUSPEND = 1;
        public const int GLOBALSTATUSMASK_BAD_EMAIL_SUSPEND = 2;
        public const int GLOBALSTATUSMASK_EMAIL_VERIFIED = 4;
        public const int SELF_SUSPEND = 1;

        public const Int16 APPROVE_STATUS_IGNORE = 1;
        public const Int16 APPROVE_STATUS_INVALID = 8;

        public const int ADMIN_ACTION_MASK_APPROVED = 8;

        public const string URL_PARAMETER_NAME_SPARKWS_CONTEXT = "SparkWS"; // = true if it is SparkWS context - for API / SparkWS project.
        public const string URL_PARAMETER_NAME_SPARKWS_CYPHER_OUT = "SPWSCYPHER"; // carries a cypher on a redirect from login in web service API context.
        public const string SPARK_WS_CRYPT_KEY = "A877C90D";// used to encrypt session id for tranfer over to SparkWS.
        public const string SESSION_PARAMETER_NAME_MEMBER_MESSAGES = "MemberMessages";
        public const string SUBSCRIPTION_ENCRYPT_KEY = "m0nk$ies";
        public const string INVALID_IP_ADDRESS = "1.0.0.128";

        public const string OMNITURE_EVAR44_MEMBERID = "MemberID{0}";
        public const char ONE_WHITESPACE_CHAR = ' ';
        public const char COMMA_DELIMITER = ',';
    }
}
