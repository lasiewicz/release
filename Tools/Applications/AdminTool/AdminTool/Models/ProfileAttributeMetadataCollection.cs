﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Configuration.ServiceAdapters;

namespace AdminTool.Models
{
    public enum ControlType
    {
        All = 0,
        TextBox = 1,
        BoolCheckBox = 2,
        SingleSelect = 3,
        MultiSelect = 4
    }

    public enum DataType
    {
        None,
        FreeText,
        NumberText,
        DateTime,
        Boolean,
        SingleSelect,
        MultiSelect
    }

    public class ProfileAttributeMetadata
    {
        public int AttributeID { get; set; }
        public string AttributeName { get; set; }
        public DataType DataType { get; set; }
        public int AttributeMaxLength { get; set; }
    }

    public class ProfileAttributeMetadataCollection
    {
        public List<ProfileAttributeMetadata> ProfileAttributeMetadataCol = new List<ProfileAttributeMetadata>();

        public void GetMetadata(int controlType, Brand brand)
        {
            int profileSteps = Matchnet.Conversion.CInt(RuntimeSettings.GetSetting("REGISTRATION_STEPS", brand.Site.Community.CommunityID, brand.Site.SiteID));
            AttributeCollection attributes;
            Attributes contentAttributes = Matchnet.Content.ServiceAdapters.AttributeMetadataSA.Instance.GetAttributes();

            for (int i = 1; i <= profileSteps; i++)
            {
                attributes = AttributeMetadataSA.Instance.GetAttributeCollections().GetCollection("REGSTEP" + i.ToString(), brand);
                if (attributes != null)
                {
                    foreach (Matchnet.Content.ValueObjects.AttributeMetadata.AttributeCollectionAttribute a in attributes)
                    {
                        this.Add(a, (ControlType)controlType, brand, contentAttributes);
                    }
                }
            }
        }

        public void Add(Matchnet.Content.ValueObjects.AttributeMetadata.AttributeCollectionAttribute attributeColAttribute, ControlType controlType, Brand brand, Attributes attributes)
        {
            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = attributes.GetAttribute(attributeColAttribute.AttributeID);
            if (attribute == null)
                return;

            DataType dType = DataType.None;
            bool addToCollection = true;

            #region DataType overrides. Data type defined in the DB isn't how we actually use it on the sites anymore.
            // some number DataType attributes actually require TextBox
            if (attribute.Name.ToLower() == "desiredminage" || attribute.Name.ToLower() == "desiredmaxage")
            {
                if (controlType == ControlType.TextBox)
                {
                    dType = DataType.NumberText;
                }
                else
                {
                    addToCollection = false;
                }
            }
            else if (attribute.Name.ToLower() == "bodytype")
            {
                // mask in db, but we want to represent as single select
                if (controlType == ControlType.SingleSelect)
                {
                    dType = DataType.SingleSelect;
                }
                else
                {
                    addToCollection = false;
                }
            }
            else if (attribute.Name.ToLower() == "israelipersonalitytrait")
            {
                // number in db, but we want to represent it as mask
                if (controlType == ControlType.MultiSelect)
                {
                    dType = DataType.MultiSelect;
                }
                else
                {
                    addToCollection = false;
                }
            }
            #endregion

            if (dType == DataType.None)
            {
                switch (attribute.DataType)
                {
                    case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Text:
                        if (controlType == ControlType.TextBox)
                        {
                            dType = DataType.FreeText;
                        }
                        break;
                    case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Number:
                        if (controlType == ControlType.SingleSelect)
                        {
                            dType = DataType.SingleSelect;
                        }
                        break;
                    case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Date:
                        if (controlType == ControlType.TextBox)
                        {
                            dType = DataType.DateTime;
                        }
                        break;
                    case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Bit:
                        if (controlType == ControlType.BoolCheckBox)
                        {
                            dType = DataType.Boolean;
                        }
                        break;
                    case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Mask:
                        if (controlType == ControlType.MultiSelect)
                        {
                            dType = DataType.MultiSelect;
                        }
                        break;
                }
            }

            if (dType != DataType.None && addToCollection)
            {
                AttributeGroup ag = attributes.GetAttributeGroup(GeneralHelper.GetGroupIDForAttribute(brand, attribute), attribute.ID);

                ProfileAttributeMetadataCol.Add(new ProfileAttributeMetadata
                {
                    AttributeID=attribute.ID,
                    AttributeName=attribute.Name,
                    DataType = dType,
                    AttributeMaxLength = ag.Length <= 0 ? 0 : ag.Length
                });

            }
        }
    }
}
