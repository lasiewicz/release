﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models
{
    public class MatchMember
    {
        public int MemberID { get; set; }
        public string UserName { get; set; }
        public int Age { get; set; }
        public string GenderAndSeeking { get; set; }
        public string Location { get; set; }
        public double Distance { get; set; }
        public int PreferredMinAge { get; set; }
        public int PreferredMaxAge { get; set; }

    }
}
