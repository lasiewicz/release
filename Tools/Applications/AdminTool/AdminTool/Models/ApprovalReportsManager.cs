﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;

using AdminTool.Models.ModelViews.Member;
using AdminTool.Models.ModelViews.Review;
using Spark.CommonLibrary;
using AdminTool.Models.ModelViews;

using Matchnet;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ServiceAdapters;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Configuration.ServiceAdapters;
using AdminTool.Models.ModelViews.Approval;
using Matchnet.Session.ValueObjects;

namespace AdminTool.Models
{
    public class ApprovalReportsManager: ManagerBase
    {
        public ApprovalCountTotalsModelView GetCountTotals(DateTime startDate, DateTime endDate, string adminMemberID, CommunityIDs communityID, SiteIDs siteID, Enums.ApprovalCountReportSortType sortType, Enums.SortDirection sortDirection)
        {
            ApprovalCountTotalsModelView modelView = new ApprovalCountTotalsModelView();

            //to make the date range inclusive
            endDate = endDate.AddDays(1);

            int intCommunityID = (communityID == CommunityIDs.None) ? Constants.NULL_INT : (int)communityID;
            int intSiteID = (siteID == SiteIDs.None) ? Constants.NULL_INT: (int) siteID;
            int intAdminMemberID = string.IsNullOrEmpty(adminMemberID) ? Constants.NULL_INT : Convert.ToInt32(adminMemberID);

            List<ApprovalCountRecord> approvalRecords = DBApproveQueueSA.Instance.GetApprovalHistoryCounts(startDate, endDate, intAdminMemberID, intCommunityID, intSiteID);

            if (approvalRecords != null && approvalRecords.Count > 0)
            {
                List<ApprovalCountRecordExtended> extendedRecords = new List<ApprovalCountRecordExtended>();
                int totalPhotoCount = 0;
                int totalTextCount = 0;
                int totalQACount = 0;
                int totalFBLikesCount = 0;
                int totalCount = 0;

                foreach (ApprovalCountRecord record in approvalRecords)
                {
                    totalPhotoCount = totalPhotoCount + record.PhotoApprovalCount;
                    totalTextCount = totalTextCount + record.TextApprovalCount;
                    totalQACount = totalQACount + record.QAApprovalCount;
                    totalFBLikesCount = totalFBLikesCount + record.FacebookLikeApprovalCount;
                    totalCount = totalCount + record.PhotoApprovalCount + record.TextApprovalCount + record.QAApprovalCount + record.FacebookLikeApprovalCount;

                    Member member = MemberHelper.GetMember(record.AdminMemberID, MemberLoadFlags.None);
                    extendedRecords.Add(new ApprovalCountRecordExtended(record, member.EmailAddress));
                }

                modelView.Records = extendedRecords;
                modelView.TotalCountPhoto = totalPhotoCount;
                modelView.TotalCountRegularText = totalTextCount;
                modelView.TotalCountQA = totalQACount;
                modelView.TotalCountFacebookLikes = totalFBLikesCount;
                modelView.TotalCount = totalCount;

                switch (sortType)
                {
                    case Enums.ApprovalCountReportSortType.MemberID:
                        extendedRecords.Sort(new RecordComparerByMemberID(sortDirection));
                        break;
                    case Enums.ApprovalCountReportSortType.Email:
                        extendedRecords.Sort(new RecordComparerByEmail(sortDirection));
                        break;
                    case Enums.ApprovalCountReportSortType.TextCount:
                        extendedRecords.Sort(new RecordComparerByTextCount(sortDirection));
                        break;
                    case Enums.ApprovalCountReportSortType.QACount:
                        extendedRecords.Sort(new RecordComparerByQACount(sortDirection));
                        break;
                    case Enums.ApprovalCountReportSortType.PhotoCount:
                        extendedRecords.Sort(new RecordComparerByPhotoCount(sortDirection));
                        break;
                    case Enums.ApprovalCountReportSortType.FacebookLikesCount:
                        extendedRecords.Sort(new RecordComparerByFacebookLikesCount(sortDirection));
                        break;
                }
                SetNextSortDirections(modelView, sortType, sortDirection);
            }

            return modelView;
        }

        public ApprovalQueueCountTotalsModelView GetPhotoApprovalQueueCounts()
        {
            ApprovalQueueCountTotalsModelView modelView = new ApprovalQueueCountTotalsModelView();
            List<QueueCountRecord> communityRecords = null;
            List<QueueCountRecord> siteRecords = null;

            List<QueueCountRecord> records = DBApproveQueueSA.Instance.GetQueueCounts(QueueItemType.Photo);

            if (records != null && records.Count > 0)
            {
                communityRecords = (from r in records where r.GroupType == QueueCountGroupType.Community select r).ToList();
                siteRecords = (from r in records where r.GroupType == QueueCountGroupType.Site select r).ToList();
            }

            modelView.ItemType = QueueItemType.Photo;
            modelView.CountsByCommunity = communityRecords;
            modelView.CountsBySite = siteRecords;

            return modelView;

        }

        public ApprovalQueueCountTotalsModelView GetTextApprovalQueueCounts()
        {
            ApprovalQueueCountTotalsModelView modelView = new ApprovalQueueCountTotalsModelView();
            List<QueueCountRecord> communityRecords = null;
            List<QueueCountRecord> languageRecords = null;

            List<QueueCountRecord> records = DBApproveQueueSA.Instance.GetQueueCounts(QueueItemType.Text);

            if (records != null && records.Count > 0)
            {
                communityRecords = (from r in records where r.GroupType == QueueCountGroupType.Community select r).ToList();
                languageRecords = (from r in records where r.GroupType == QueueCountGroupType.Language select r).ToList();
            }

            modelView.ItemType = QueueItemType.Photo;
            modelView.CountsByCommunity = communityRecords;
            modelView.CountsByLanguage = languageRecords;

            return modelView;

        }

        public ApprovalQueueCountTotalsModelView GetQAApprovalQueueCounts()
        {
            ApprovalQueueCountTotalsModelView modelView = new ApprovalQueueCountTotalsModelView();
            List<QueueCountRecord> communityRecords = null;
            List<QueueCountRecord> siteRecords = null;
            List<QueueCountRecord> languageRecords = null;

            List<QueueCountRecord> records = DBApproveQueueSA.Instance.GetQueueCounts(QueueItemType.QuestionAnswer);

            if (records != null && records.Count > 0)
            {
                communityRecords = (from r in records where r.GroupType == QueueCountGroupType.Community select r).ToList();
                siteRecords = (from r in records where r.GroupType == QueueCountGroupType.Site select r).ToList();
                languageRecords = (from r in records where r.GroupType == QueueCountGroupType.Language select r).ToList();
            }

            modelView.ItemType = QueueItemType.Photo;
            modelView.CountsByCommunity = communityRecords;
            modelView.CountsByLanguage = languageRecords;
            modelView.CountsBySite = siteRecords;

            return modelView;
        }

        public ApprovalQueueCountTotalsModelView GetFacebookLikesApprovalQueueCounts()
        {
            ApprovalQueueCountTotalsModelView modelView = new ApprovalQueueCountTotalsModelView();
            List<QueueCountRecord> communityRecords = null;
            List<QueueCountRecord> siteRecords = null;

            List<QueueCountRecord> records = DBApproveQueueSA.Instance.GetQueueCounts(QueueItemType.FacebookLike);

            if (records != null && records.Count > 0)
            {
                communityRecords = (from r in records where r.GroupType == QueueCountGroupType.Community select r).ToList();
                siteRecords = (from r in records where r.GroupType == QueueCountGroupType.Site select r).ToList();
            }

            modelView.ItemType = QueueItemType.FacebookLike;
            modelView.CountsByCommunity = communityRecords;
            modelView.CountsBySite = siteRecords;

            return modelView;
        }

        public int GetTotalPhotoQueueCount(CommunityIDs communityID)
        {
            int count = 0;
            List<QueueCountRecord> records = DBApproveQueueSA.Instance.GetQueueCounts(QueueItemType.Photo);
            if (records != null && records.Count > 0)
            {
                List<QueueCountRecord> filteredRecords = (from r in records where r.GroupType == QueueCountGroupType.Community && r.GroupID == (int)communityID select r).ToList();
                count = filteredRecords.Sum(r => r.ItemCount);
            }

            return count;
        }

        public int GetTotalPhotoQueueCount(SiteIDs siteID)
        {
            int count = 0;
            List<QueueCountRecord> records = DBApproveQueueSA.Instance.GetQueueCounts(QueueItemType.Photo);
            if (records != null && records.Count > 0)
            {
                List<QueueCountRecord> filteredRecords = (from r in records where r.GroupType == QueueCountGroupType.Site && r.GroupID == (int)siteID select r).ToList();
                count = filteredRecords.Sum(r => r.ItemCount);
            }

            return count;
        }

        public int GetTotalTextQueueCount(QueueItemType itemType, CommunityIDs communityID)
        {
            int count = 0;
            List<QueueCountRecord> records = DBApproveQueueSA.Instance.GetQueueCounts(itemType);
            if (records != null && records.Count > 0)
            {
                List<QueueCountRecord> filteredRecords = (from r in records where r.GroupType == QueueCountGroupType.Community && r.GroupID == (int)communityID select r).ToList();
                count = filteredRecords.Sum(r => r.ItemCount);
            }

            return count;
        }

        public int GetTotalTextQueueCount(QueueItemType itemType, SiteIDs siteID)
        {
            int count = 0;
            List<QueueCountRecord> records = DBApproveQueueSA.Instance.GetQueueCounts(itemType);
            if (records != null && records.Count > 0)
            {
                List<QueueCountRecord> filteredRecords = (from r in records where r.GroupType == QueueCountGroupType.Site && r.GroupID == (int)siteID select r).ToList();
                count = filteredRecords.Sum(r => r.ItemCount);
            }

            return count;
        }

        public int GetTotalTextQueueCount(QueueItemType itemType, int languageID)
        {
            int count = 0;
            List<QueueCountRecord> records = DBApproveQueueSA.Instance.GetQueueCounts(itemType);
            if (records != null && records.Count > 0)
            {
                List<QueueCountRecord> filteredRecords = (from r in records where r.GroupType == QueueCountGroupType.Language && r.GroupID == (int)languageID select r).ToList();
                count = filteredRecords.Sum(r => r.ItemCount);
            }

            return count;
        }

        public int GetSessionQueueItemsProcessedCount()
        {
            int totalSessionQueueItemsProcessed = 0;
            if (HttpContext.Current.Session["TotalSessionQueueItemsProcessed"] != null)
            {
                totalSessionQueueItemsProcessed = Convert.ToInt32(HttpContext.Current.Session["TotalSessionQueueItemsProcessed"].ToString());
            }
            return totalSessionQueueItemsProcessed;
        }

        public void AddSessionQueueItemProcessedCount()
        {
            int totalSessionQueueItemsProcessed = GetSessionQueueItemsProcessedCount();
            totalSessionQueueItemsProcessed += 1;
            HttpContext.Current.Session["TotalSessionQueueItemsProcessed"] = totalSessionQueueItemsProcessed;
        }

        public int GetSessionQueueItemsProcessedCount(QueueItemType queueItemType)
        {
            // eventually all queue item type should have its own counter.  for this to work, the update portion needs to call the right method too.
            // this is beyond my scope at the moment, so will work on it later when i get a separate story for it.
            if (queueItemType == QueueItemType.MemberProfileDuplicate || queueItemType == QueueItemType.MemberProfileDuplicateReview)
            {
                int totalSessionQueueItemsProcessed = 0;
                var sessionCounterName = string.Format("{0}{1}{2}", "Total",
                                                       Enum.GetName(typeof (QueueItemType), queueItemType), "Processed");
                if (HttpContext.Current.Session[sessionCounterName] != null)
                {
                    totalSessionQueueItemsProcessed =
                        Convert.ToInt32(HttpContext.Current.Session[sessionCounterName].ToString());
                }
                return totalSessionQueueItemsProcessed;
            }
            else
            {
                return GetSessionQueueItemsProcessedCount();
            }
        }

        public void AddSessionQueueItemProcessedCount(QueueItemType queueItemType)
        {
            // eventually all queue item type should have its own counter.  for this to work, the update portion needs to call the right method too.
            // this is beyond my scope at the moment, so will work on it later when i get a separate story for it.
            if (queueItemType == QueueItemType.MemberProfileDuplicate || queueItemType == QueueItemType.MemberProfileDuplicateReview)
            {
                var sessionCounterName = string.Format("{0}{1}{2}", "Total",
                                                       Enum.GetName(typeof (QueueItemType), queueItemType), "Processed");
                int totalSessionQueueItemsProcessed = GetSessionQueueItemsProcessedCount(queueItemType);
                totalSessionQueueItemsProcessed += 1;
                HttpContext.Current.Session[sessionCounterName] = totalSessionQueueItemsProcessed;
            }
            else
            {
                AddSessionQueueItemProcessedCount();
            }
        }

        public List<PhotoApprovalData> GetPhotoQueueItems(int siteID)
        {
            var items =  DBApproveQueueSA.Instance.GetApprovalPhotoQueueItems(siteID);
            if(items == null)
                return new List<PhotoApprovalData>();
            return (from QueueItemPhoto photoItem in items
                    select new PhotoApprovalData
                               {
                                   MemberId = photoItem.MemberID, 
                                   CommunityId = (CommunityIDs) photoItem.CommunityID, 
                                   SiteId = (SiteIDs) photoItem.SiteID, 
                                   SiteName = BrandConfigSA.Instance.GetBrandByID(photoItem.BrandID).Site.Name,
                                   MemberPhotoId = photoItem.MemberPhotoID, 
                                   InsertDate = photoItem.InsertDate,
                                   PhotoAge = Convert.ToInt32(DateTime.Now.Subtract(photoItem.InsertDate).TotalHours)
                               }).ToList();
        }

        public ApprovalQueueCountTotalsModelView GetCrxReviewQueueCounts()
        {
            var modelView = new ApprovalQueueCountTotalsModelView();
            List<QueueCountRecord> communityRecords = null;
            List<QueueCountRecord> languageRecords = null;

            List<QueueCountRecord> records = DBApproveQueueSA.Instance.GetQueueCounts(QueueItemType.CrxSecondReview);

            if (records != null && records.Count > 0)
            {
                communityRecords = (from r in records where r.GroupType == QueueCountGroupType.Community select r).ToList();
                languageRecords = (from r in records where r.GroupType == QueueCountGroupType.Language select r).ToList();
            }

            modelView.ItemType = QueueItemType.CrxSecondReview;
            modelView.CountsByCommunity = communityRecords;
            modelView.CountsByLanguage = languageRecords;

            return modelView;
        }

        private void SetNextSortDirections(ApprovalCountTotalsModelView approvalCountTotalsModelView, Enums.ApprovalCountReportSortType currentSortType, Enums.SortDirection currentSortDirection)
        {
            approvalCountTotalsModelView.NextAdminMemberIDSortDirection = (currentSortType == Enums.ApprovalCountReportSortType.MemberID) ? currentSortDirection.Reverse() : Enums.SortDirection.ASC;
            approvalCountTotalsModelView.NextEmailAddressSortDirection = (currentSortType == Enums.ApprovalCountReportSortType.Email) ? currentSortDirection.Reverse() : Enums.SortDirection.ASC;
            approvalCountTotalsModelView.NextPhotoSortDirection = (currentSortType == Enums.ApprovalCountReportSortType.PhotoCount) ? currentSortDirection.Reverse() : Enums.SortDirection.ASC;
            approvalCountTotalsModelView.NextTextSortDirection = (currentSortType == Enums.ApprovalCountReportSortType.TextCount) ? currentSortDirection.Reverse() : Enums.SortDirection.ASC;
            approvalCountTotalsModelView.NextQASortDirection = (currentSortType == Enums.ApprovalCountReportSortType.QACount) ? currentSortDirection.Reverse() : Enums.SortDirection.ASC;
        }

        public ApprovalQueueCountTotalsModelView GetMemberProfileDuplicateQueueCounts()
        {
            ApprovalQueueCountTotalsModelView modelView = new ApprovalQueueCountTotalsModelView();
            List<QueueCountRecord> languageRecords = null;

            List<QueueCountRecord> records = DBApproveQueueSA.Instance.GetQueueCounts(QueueItemType.MemberProfileDuplicate);

            if (records != null && records.Count > 0)
            {
                languageRecords = (from r in records where r.GroupType == QueueCountGroupType.Language select r).ToList();
            }

            modelView.ItemType = QueueItemType.MemberProfileDuplicate;
            modelView.CountsByLanguage = languageRecords;

            return modelView;
        }


        public MemberProfileDuplicateCountsResult GetMemberProfileDuplicateHistoryCount(DateTime startDate, DateTime endDate, int adminMemberId)
        {
            var result = new MemberProfileDuplicateCountsResult();
            var historyCount = DBApproveQueueSA.Instance.GetMemberProfileDuplicateHistoryCount(startDate, endDate,
                                                                                           adminMemberId);
            
            if(historyCount == null)
                return result;
            
            var historyCountExtended = new List<MemberProfileDuplicateHistoryCountExtended>();
            foreach(var item in historyCount)
            {
                if (item.AdminId != Constants.NULL_INT)
                {
                    var member = MemberHelper.GetMember(item.AdminId, MemberLoadFlags.None);
                    historyCountExtended.Add(new MemberProfileDuplicateHistoryCountExtended
                                                 {
                                                     AdminId = item.AdminId,
                                                     ActionCount = item.ActionCount,
                                                     DuplicatesCount = item.DuplicatesCount,
                                                     MatchesCount = item.MatchesCount,
                                                     SuspendedCount = item.SuspendedCount,
                                                     AlreadySuspendedCount = item.AlreadySuspendedCount,
                                                     EmailAddress = member.EmailAddress
                                                 });
                }
            }

            result.CountsList = historyCountExtended;
            return result;
        }

        public ApprovalQueueCountTotalsModelView GetMemberProfileDuplicateReviewQueueCounts()
        {
            ApprovalQueueCountTotalsModelView modelView = new ApprovalQueueCountTotalsModelView();
            List<QueueCountRecord> languageRecords = null;

            List<QueueCountRecord> records = DBApproveQueueSA.Instance.GetQueueCounts(QueueItemType.MemberProfileDuplicateReview);

            if (records != null && records.Count > 0)
            {
                languageRecords = (from r in records where r.GroupType == QueueCountGroupType.Language select r).ToList();
            }

            modelView.ItemType = QueueItemType.MemberProfileDuplicateReview;
            modelView.CountsByLanguage = languageRecords;

            return modelView;
        }
    }
}
