﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Net;
using System.Drawing;
using AdminTool.Models.Helpers;
using AdminTool.Models.ModelViews.Member;
using Spark.CommonLibrary;
using Spark.CommonLibrary.Logging;
using AdminTool.Models.ModelViews;

using Matchnet;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ServiceAdapters;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Configuration.ServiceAdapters;
using AdminTool.Models.ModelViews.Approval;
using AdminTool.Models.SparkAPI;
using AdminTool.Models.SparkAPI.EndpointClients;
using Spark.Common.Adapter;
using Spark.Common.OrderHistoryService;
using Spark.Common.PaymentProfileService;
using Matchnet.File.ServiceAdapters;
using Spark.FacebookLike.ValueObjects;
using Spark.FacebookLike.ValueObjects.ServiceDefinitions;
using Spark.FacebookLike.ServiceAdapters;

namespace AdminTool.Models
{
    public class ApprovalManager : ManagerBase
    {
        private const string SHOW_COORDS = "&showCoords=1";
        
        public Logger _logger;

        public SiteIDs SiteID { get; private set; }
        public CommunityIDs CommunityID { get; private set; }
        public int MemberID { get; private set; }
        private Guid _debugguid;

        public ApprovalManager()
        {
            _logger = new Logger(this.GetType());
            _debugguid = new Guid();
        }

        public PhotoApprovalModelViewBase GetPhotoApprovalByMember(int memberID, CommunityIDs communityID)
        {
            MemberID = memberID;
            List<PhotoApprovalData> approvalData = PopulateMemberPhotoApprovals(memberID, communityID);

            PhotoApprovalMemberModelView modelView = new PhotoApprovalMemberModelView();
            MemberManager memberManager;

            if (approvalData != null && approvalData.Count > 0)
            {
                memberManager = new MemberManager(MemberID, (int)approvalData[0].SiteId, MemberLoadFlags.None);
                modelView.ApprovalItems = approvalData;
                modelView.Left = memberManager.GetMemberLeft();
                modelView.TopNav = memberManager.GetTopNav();
            }
            else
            {
                Member member = MemberHelper.GetMember(memberID, MemberLoadFlags.None);
                int siteID = (int)GeneralHelper.GetPrimarySiteForCommunity(communityID);
                memberManager = new MemberManager(MemberID, siteID, MemberLoadFlags.None);
                modelView.Left = memberManager.GetMemberLeft();
                modelView.TopNav = memberManager.GetTopNav();
                modelView.ApprovalItems = new List<PhotoApprovalData>();
            }

            return modelView;
        }

        public PhotoApprovalModelViewBase GetPhotoApprovalByCommunity(int maxCount, CommunityIDs communityId)
        {
            CommunityID = communityId;
            var approvalData = PopulatePhotoApprovals(QAApprovalType.QueuedApprovalByCommunity, maxCount);
            var modelView = new PhotoApprovalModelView
            {
                ApprovalItems = approvalData,
                TopNav = new SharedTopNav()
            };
            modelView.Left = modelView.Left = GetPhotoApprovalLeft(CommunityID);

            return modelView;
        }

        public PhotoApprovalModelViewBase GetPhotoApprovalBySite(int maxCount, SiteIDs siteID, CommunityIDs communityID)
        {
            SiteID = siteID;
            CommunityID = communityID;
            List<PhotoApprovalData> approvalData = PopulatePhotoApprovals(QAApprovalType.QueuedApprovalBySite, maxCount);

            PhotoApprovalModelView modelView = new PhotoApprovalModelView();
            modelView.ApprovalItems = approvalData;
            modelView.TopNav = new SharedTopNav();
            modelView.Left = GetPhotoApprovalLeft(SiteID);

            return modelView;
        }

        public void DeletePhotoQueueItem(int memberID, int communityID, int memberPhotoID)
        {
            DBApproveQueueSA.Instance.DeletePhotoQueueItem(memberPhotoID);
            AdminSA.Instance.AdminActionLogInsert(memberID, communityID, (int)AdminAction.DeletedPhotoFromQueue, g.AdminID, Matchnet.Member.ValueObjects.ServiceConstants.LANGUAGE_ENGLISH);
        }

        public bool CompletePhotoApproval(int disposition, int tx, int ty, int tw, int th, int cx, int cy, int cw, int ch, int captionDelete, int photoPrevApproved, int memberPhotoID, int fileID, int rot, int memberID, int adminMemberID, int communityID, int siteID, bool isApprovedForMain)
        {
            bool success = true;
            try
            {
                System.Diagnostics.Trace.WriteLine("Start ApprovalManager.CompletePhotoApproval()");
                if (disposition != WebConstants.APPROVE_STATUS_INVALID && disposition != WebConstants.APPROVE_STATUS_IGNORE)
                {
                    Rectangle thumbRect = new Rectangle(tx, ty, tw, th);
                    Rectangle cropRect = new Rectangle(cx, cy, cw, ch);
                    bool photoPrevApprovedFlag = Convert.ToBoolean(photoPrevApproved);
                    bool captionDeleteFlag = Convert.ToBoolean(captionDelete);

                    System.Diagnostics.Trace.WriteLine("ApprovalManager.CompletePhotoApproval() - Create ApproveInfo");
                    ApproveInfo approveInfo = new ApproveInfo(memberPhotoID,
                                (ApproveStatusType)Enum.Parse(typeof(ApproveStatusType), disposition.ToString()),
                                fileID,
                                0, // not used
                                Convert.ToInt16(rot),
                                Convert.ToInt16(cropRect.X),
                                Convert.ToInt16(cropRect.Y),
                                Convert.ToInt16(cropRect.Width),
                                Convert.ToInt16(cropRect.Height),
                                0, // not used
                                Convert.ToInt16(thumbRect.X),
                                Convert.ToInt16(thumbRect.Y),
                                Convert.ToInt16(thumbRect.Width),
                                Convert.ToInt16(thumbRect.Height),
                                Convert.ToInt32(adminMemberID),
                                Convert.ToInt32(communityID),
                                Convert.ToInt32(memberID), captionDeleteFlag, photoPrevApprovedFlag, siteID);

                    approveInfo.IsApprovedForMain = isApprovedForMain;

                    System.Diagnostics.Trace.WriteLine("ApprovalManager.CompletePhotoApproval() - Create ApproveInfoCollection");
                    ApproveInfoCollection approveInfoCollection = new ApproveInfoCollection();
                    approveInfoCollection.Add(approveInfo);
                    System.Diagnostics.Trace.WriteLine("ApprovalManager.CompletePhotoApproval() - Call DBApproveQueueSA.SendToPhotoApprove");
                    DBApproveQueueSA.Instance.SendToPhotoApprove(approveInfoCollection);

                    ApprovalHelper approvalHelper = new ApprovalHelper();
                    if (disposition == 0)
                    {
                        System.Diagnostics.Trace.WriteLine("ApprovalManager.CompletePhotoApproval() - PersistMemberMessage() with disposition of 0");
                        approvalHelper.PersistMemberMessage(g.AdminID, memberID, string.Format("Successfully approved member <a href='/Member/View/{1}/{0}' target='_blank'>{0}</a>", memberID.ToString(), siteID.ToString("d")), false);
                    }
                    else
                    {
                        System.Diagnostics.Trace.WriteLine("ApprovalManager.CompletePhotoApproval() - PersistMemberMessage() with disposition of not 0");
                        approvalHelper.PersistMemberMessage(g.AdminID, memberID, string.Format("Successfully rejected member <a href='/Member/View/{1}/{0}' target='_blank'>{0}</a>", memberID.ToString(), siteID.ToString("d")), false);
                    }
                    new ApprovalReportsManager().AddSessionQueueItemProcessedCount();
                }


                //Add member to Fraud Review Queue, when a member is marked as 'Possible Fraud'
                if (disposition == (int)ApproveStatusType.Approved_PossibleFraud)
                {
                    System.Diagnostics.Trace.WriteLine("ApprovalManager.CompletePhotoApproval() - Add member to Fraud Review Queue");
                    var queueItemMember = new QueueItemMember(communityID, siteID, Constants.NULL_INT, memberID,
                                                              adminMemberID);
                    DBApproveQueueSA.Instance.Enqueue(queueItemMember);
                }

                //Check if Member is in Sodb Watchlist. If yes, update sodb item status
                if (disposition == 0)
                    new SodbHelper().UpdateSodbMemberPhotoUploadStatus(memberID, siteID);

            }
            catch (Exception ex)
            {
                Logger logger = new Logger(this.GetType()); ;
                logger.Error("Error in ApprovalManager.CompletePhotoApproval().", ex);
                System.Diagnostics.Trace.WriteLine("Error in ApprovalManager.CompletePhotoApproval(): " + ex.Message);
                success = false;
            }

            System.Diagnostics.Trace.WriteLine("ApprovalManager.CompletePhotoApproval(): End successfully");
            return success;
        }

        private List<PhotoApprovalData> PopulateMemberPhotoApprovals(int memberId, CommunityIDs communityId)
        {
            var photoApprovals = new List<PhotoApprovalData>();

            var member = MemberHelper.GetMember(memberId, MemberLoadFlags.IngoreSACache);
            var photos = member.GetPhotos((int) communityId);
            var photoCount = 0;

            foreach (Photo photo in photos)
            {
                var queueItemPhoto = new QueueItemPhoto
                {
                    CommunityID = (int) communityId,
                    MemberID = memberId,
                    SiteID = (int) GeneralHelper.GetPrimarySiteForCommunity(communityId)
                };
                queueItemPhoto.BrandID = GeneralHelper.GetBrandForSite(queueItemPhoto.SiteID).BrandID;
                queueItemPhoto.MemberPhotoID = photo.MemberPhotoID;
                var photoData = GetLaPhotoApprovalDataFromQueueItem(queueItemPhoto, false);
                if (photoData == null) continue;
                photoCount++;
                photoData.Id = photoCount;
                photoApprovals.Add(photoData);
            }

            return photoApprovals;
        }

        private List<PhotoApprovalData> PopulatePhotoApprovals(QAApprovalType approvalType, int maxCount)
        {
            List<PhotoApprovalData> photoApprovals = new List<PhotoApprovalData>();
            int numPhotosFound = 0;
            int numLeftToFind = maxCount;
            bool queueEmpty = false;
            List<QueueItemBase> queueItemPhotos = null;

            while (!queueEmpty && numPhotosFound < maxCount)
            {
                switch (approvalType)
                {
                    case QAApprovalType.QueuedApprovalByCommunity:
                        queueItemPhotos = DBApproveQueueSA.Instance.DequeuePhotosByCommunity(numLeftToFind, (int)CommunityID);
                        string queueCount = queueItemPhotos == null ? "0" : queueItemPhotos.Count.ToString();
                        break;
                    case QAApprovalType.QueuedApprovalBySite:
                        queueItemPhotos = DBApproveQueueSA.Instance.DequeuePhotosBySite(numLeftToFind, (int)SiteID);
                        break;
                    case QAApprovalType.MemberApproval:
                        queueItemPhotos = DBApproveQueueSA.Instance.DequeuePhotosByMember(numLeftToFind, MemberID);
                        break;
                }

                if (queueItemPhotos == null || queueItemPhotos.Count < numLeftToFind) queueEmpty = true;

                if (queueItemPhotos == null) continue;
                foreach (var queueItem in queueItemPhotos)
                {
                    var queueItemPhoto = (QueueItemPhoto) queueItem;
                    PhotoApprovalData photoApprovalData;

                    switch (CommunityID)
                    {
                        // L.A. sites
                        case CommunityIDs.JDate:
                        case CommunityIDs.Spark:
                        case CommunityIDs.Cupid:
                        case CommunityIDs.BlackSingles:
                        case CommunityIDs.BBWPersonalsPlus:
                            photoApprovalData = GetLaPhotoApprovalDataFromQueueItem(queueItemPhoto,
                                approvalType != QAApprovalType.MemberApproval);
                            break;
                        default:
                            // Mingle sites
                            photoApprovalData = GetMinglePhotoApprovalDataFromQueueItem(queueItemPhoto,
                                approvalType != QAApprovalType.MemberApproval);
                            break;
                    }

                    if (photoApprovalData == null) continue;
                    numPhotosFound++;
                    numLeftToFind--;
                    photoApprovalData.Id = numPhotosFound;
                    photoApprovals.Add(photoApprovalData);
                }
            }

            return photoApprovals;
        }

        private PhotoApprovalData GetMinglePhotoApprovalDataFromQueueItem(QueueItemPhoto item, bool defaultIsApprovedForMain)
        {
            var photoData = new PhotoApprovalData();

            var brand = BrandConfigSA.Instance.GetBrandByID(item.BrandID);
            var member = MemberHelper.GetMember(item.MemberID, MemberLoadFlags.IngoreSACache);

            // basic profile data validation
            if (!ValidateMemberForApprovalOrCompleteApproval(member, item.SiteID,
                item.CommunityID, item.MemberPhotoID))
                return null;

            // does photo exist?
            var photo = GetMemberPhotoOrCompleteApproval(member, item.CommunityID, item.MemberPhotoID);
            if (photo == null) return null;

            // call UT API to retrieve fraud profile
            var siteId = GeneralHelper.GetPrimarySiteForCommunity((CommunityIDs)item.CommunityID);
            var fraudProfile =
                new ApiMingleMember(ApiClientMingle.GetInstance()).GetFraudProfileAttributeSet(member.MemberID, (int)siteId);

            photoData.MemberId = item.MemberID;
            photoData.MemberPhotoId = item.MemberPhotoID;
            photoData.Username = fraudProfile.UserName;
            photoData.HairColor = fraudProfile.Hair;
            photoData.Gender = fraudProfile.Gender;
            photoData.Ethnicity = fraudProfile.Ethnicity;
            photoData.BodyStyle = fraudProfile.BodyStyle;
            photoData.BillingPhoneNumber = fraudProfile.Billing_Phone;
            photoData.Age = fraudProfile.Age;
            photoData.SiteName = fraudProfile.Site;
            photoData.HairColor = fraudProfile.Hair;
            photoData.Height = fraudProfile.Height;
            photoData.AboutMe = fraudProfile.AboutMe;
            photoData.Location = fraudProfile.ProfileLocation;
            photoData.RegIpAddress = fraudProfile.IPAddress;
            photoData.SubscriptionStatus = fraudProfile.SubStatus;
            photoData.PassedFraud = fraudProfile.PassedFraud;
            photoData.IsApprovedForMain = defaultIsApprovedForMain || photo.IsApprovedForMain;
            photoData.SiteName = brand.Site.Name;
            photoData.CommunityId = (CommunityIDs)item.CommunityID;
            photoData.SiteId = (SiteIDs)item.SiteID;
            var diff = DateTime.Now.Subtract(item.InsertDate);
            photoData.PhotoAge = (photoData.InsertDate > DateTime.MinValue) ? Convert.ToInt32(diff.TotalHours) : -1;
            photoData.PhotoUrl = MemberPhotoHelper.GetPhotoDisplayUrl(PhotoType.Full, photo,
                item.CommunityID, item.SiteID);
            photoData.ThumbFileId = photo.ThumbFileID;
            photoData.FileId = photo.FileID;
            photoData.PrivateFlag = Convert.ToInt32(photo.IsPrivate);

            return photoData;
        }

        private bool ValidateMemberForApprovalOrCompleteApproval(Member member, int siteId, int communityId, int memberPhotoId)
        {
           if (MemberHelper.IsMemberValidForApproval(member, communityId)) return true;

            DBApproveQueueSA.Instance.CompletePhotoApproval(memberPhotoId,
                ApprovalStatus.Completed, g.AdminID, WebConstants.ADMIN_ACTION_MASK_APPROVED);

            new ApprovalHelper().PersistMemberMessage(g.AdminID, member.MemberID,
                string.Format(
                    "Member was possible fraud or suspended: <a href='/Member/View/{1}/{0}' target='_blank'>{0}</a>",
                    member.MemberID, siteId), false);

            return false;
        }

        private Photo GetMemberPhotoOrCompleteApproval(Member member, int communityId, int memberPhotoId)
        {
            var photo = member.GetPhotos(communityId).Find(memberPhotoId);

            if (photo != null) return photo;
            //photo has been deleted since it was put in the queue, so complete it and move on.
            DBApproveQueueSA.Instance.CompletePhotoApproval(memberPhotoId,
                ApprovalStatus.Completed, g.AdminID, WebConstants.ADMIN_ACTION_MASK_APPROVED);
            return null;
        }

        private PhotoApprovalData GetLaPhotoApprovalDataFromQueueItem(QueueItemPhoto item, bool defaultIsApprovedForMain)
        {
            var photoData = new PhotoApprovalData();

            try
            {
                var brand = BrandConfigSA.Instance.GetBrandByID(item.BrandID);
                var member = MemberHelper.GetMember(item.MemberID, MemberLoadFlags.IngoreSACache);

                // basic profile data validation
                if (!ValidateMemberForApprovalOrCompleteApproval(member, item.SiteID,
                    item.CommunityID, item.MemberPhotoID))
                    return null;

                // does photo exist?
                var photo = GetMemberPhotoOrCompleteApproval(member, item.CommunityID, item.MemberPhotoID);
                if (photo == null) return null;

                var ipAddress = member.GetAttributeInt(brand, "RegistrationIP");
                IPAddress regIpAddress = null;
                if (ipAddress != Constants.NULL_INT)
                {
                    regIpAddress = new IPAddress(BitConverter.GetBytes(ipAddress));
                }

                photoData.MemberId = item.MemberID;
                photoData.MemberPhotoId = item.MemberPhotoID;
                photoData.Age = Matchnet.Lib.Util.Util.Age(member.GetAttributeDate(brand, "Birthdate"));
                photoData.Username = member.GetUserName(brand);
                photoData.Gender = MemberHelper.GetMemberGender(member, brand).ToString();
                photoData.AboutMe = member.GetAttributeText(brand, "AboutMe");
                if (regIpAddress != null)
                {
                    photoData.RegIpAddress = regIpAddress.ToString();
                }
                photoData.FraudResult = MemberHelper.GetFraudDetailedResponse(member.MemberID, brand.Site.SiteID,3);
                photoData.ThumbFileId = photo.ThumbFileID;
                photoData.FileId = photo.FileID;
                photoData.PhotoUrl = MemberPhotoHelper.GetPhotoDisplayUrl(PhotoType.Full, photo,
                    item.CommunityID, item.SiteID);
                photoData.PrivateFlag = Convert.ToInt32(photo.IsPrivate);
                photoData.Caption = photo.Caption;
                photoData.CaptionApproved = Convert.ToInt32(photo.IsCaptionApproved);
                photoData.ApprovedFlag = Convert.ToInt32(photo.IsApproved);
                if (((CommunityIDs) item.CommunityID) == CommunityIDs.JDate)
                {
                    photoData.Ethnicity = MemberHelper.GetAttributeOptionDisplay(member, brand, "JDateEthnicity",
                        member.GetAttributeInt(brand, "JDateEthnicity"));
                }
                else
                {
                    photoData.Ethnicity = MemberHelper.GetAttributeOptionDisplay(member, brand, "Ethnicity",
                        member.GetAttributeInt(brand, "Ethnicity"));
                }
                photoData.HairColor = MemberHelper.GetAttributeOptionDisplay(member, brand, "HairColor",
                    member.GetAttributeInt(brand, "HairColor"));
                photoData.BodyStyle = MemberHelper.GetAttributeOptionDisplay(member, brand, "BodyType",
                    member.GetAttributeInt(brand, "BodyType"));
                photoData.Height = MemberHelper.GetHeightDisplay(member, brand);
                photoData.Location = MemberHelper.GetRegionDisplayString(member, brand);
                photoData.PassedFraud = Convert.ToBoolean(member.GetAttributeInt(brand, "PassedFraudCheckSite", 1));
                photoData.SiteName = brand.Site.Name;
                photoData.CommunityId = (CommunityIDs) item.CommunityID;
                photoData.SiteId = (SiteIDs) item.SiteID;
                photoData.SubscriptionStatus = MemberHelper.GetSubscriberStatusString(member, brand);
                photoData.InsertDate = item.InsertDate;
                var diff = DateTime.Now.Subtract(item.InsertDate);
                photoData.PhotoAge = (photoData.InsertDate > DateTime.MinValue) ? Convert.ToInt32(diff.TotalHours) : -1;
                photoData.IsApprovedForMain = defaultIsApprovedForMain || photo.IsApprovedForMain;
                photoData.BillingPhoneNumber = GetBillingPhoneNumber(member, brand);
            }
            catch (Exception ex)
            {
                //There was an error retrieving necessary attribute data, so mark the item as complete and move on
                DBApproveQueueSA.Instance.CompletePhotoApproval(item.MemberPhotoID, ApprovalStatus.Completed,
                    g.AdminID, WebConstants.ADMIN_ACTION_MASK_APPROVED);
                photoData = null;
                _logger.Error(
                    "Error in ApprovalManager.GetLAPhotoApprovalDataFromQueueItem() MemberPhotoID: " +
                    item.MemberPhotoID, ex);
            }

            return photoData;
        }

        private string GetBillingPhoneNumber(Member member, Brand brand)
        {
            var arrOrderInfo =
                OrderHistoryServiceWebAdapter.GetProxyInstanceForBedrock()
                    .GetMemberOrderHistoryByMemberID(member.MemberID, brand.Site.SiteID, 5000, 1);
            var paymentProfileResponse =
                PaymentProfileServiceWebAdapter.GetProxyInstance()
                    .GetMemberDefaultPaymentProfile(member.MemberID, brand.Site.SiteID);

            foreach (var orderInfo in arrOrderInfo)
            {
                if (orderInfo.OrderDetail.Length <= 0) continue;
                if (paymentProfileResponse == null ||
                    orderInfo.UserPaymentGuid != paymentProfileResponse.PaymentProfileID) continue;
                var obsfucatedpaymentProfileResponse =
                    PaymentProfileServiceWebAdapter.GetProxyInstance()
                        .GetObsfucatedPaymentProfileByOrderID(orderInfo.OrderID, member.MemberID,
                            brand.Site.SiteID);
                if (obsfucatedpaymentProfileResponse == null || obsfucatedpaymentProfileResponse.Code != "0" ||
                    !(obsfucatedpaymentProfileResponse.PaymentProfile is ObsfucatedCreditCardPaymentProfile))
                    continue;
                return
                    ((ObsfucatedCreditCardPaymentProfile)
                        obsfucatedpaymentProfileResponse.PaymentProfile).PhoneNumber;
            }
            return string.Empty;
        }

        private ApprovalLeft GetPhotoApprovalLeft(CommunityIDs communityID)
        {
            SiteIDs siteID = GeneralHelper.GetPrimarySiteForCommunity(communityID);
            ApprovalLeft left = new ApprovalLeft();
            ApprovalHelper approvalHelper = new ApprovalHelper();
            left.IsTextApproval = false;
            left.SiteID = siteID;

            ApprovalReportsManager manager = new ApprovalReportsManager();
            left.QueueCount = manager.GetTotalPhotoQueueCount(communityID);
            left.ItemsProcessed = manager.GetSessionQueueItemsProcessedCount();
            left.MemberMessages = approvalHelper.GetCurrentMemberMessages(g.AdminID);
            approvalHelper.ClearMemberMessages(g.AdminID);
            return left;
        }

        private ApprovalLeft GetPhotoApprovalLeft(SiteIDs siteID)
        {
            ApprovalLeft left = new ApprovalLeft();
            ApprovalHelper approvalHelper = new ApprovalHelper();
            left.IsTextApproval = false;
            left.SiteID = siteID;

            ApprovalReportsManager manager = new ApprovalReportsManager();
            left.QueueCount = manager.GetTotalPhotoQueueCount(siteID);
            left.ItemsProcessed = manager.GetSessionQueueItemsProcessedCount();
            left.MemberMessages = approvalHelper.GetCurrentMemberMessages(g.AdminID);
            approvalHelper.ClearMemberMessages(g.AdminID);
            return left;
        }

        private string GetGender(Int32 genderMask)
        {
            String gender;

            genderMask = genderMask & (int)Enums.Gender.Female;

            gender = (genderMask == (int)Enums.Gender.Female) ? "F" : "M";

            return gender;
        }

        public ApprovalFacebookLikeModelView GetFacebookLikesApprovalByCommunity(CommunityIDs communityID)
        {
            Member member = null;
            Brand brand = null;
            int brandID;
            bool hasContent = false;
            CommunityID = communityID;
            ApprovalFacebookLikeModelView modelView = new ApprovalFacebookLikeModelView();
            modelView.CommunityID = (int)communityID;
            modelView.ApprovalType = QAApprovalType.QueuedApprovalByCommunity;
            PopulateFacebookLikeApprovals(modelView, QAApprovalType.QueuedApprovalByCommunity);
            modelView.TopNav = new SharedTopNav();

            if (modelView.MemberID > 0 && modelView.FacebookLikes != null && modelView.FacebookLikes.Count > 0)
            {
                hasContent = true;
                member = MemberHelper.GetMember(modelView.MemberID, MemberLoadFlags.None);
                member.GetLastLogonDate(modelView.CommunityID, out brandID);
                brand = BrandConfigSA.Instance.GetBrandByID(brandID);
                modelView.ExpandedUsrInfo = ApprovalHelper.GetExpandedUserInfo(member, brand);
            }

            modelView.Left = GetFacebookLikeApprovalLeft(communityID, member, brand, hasContent);

            return modelView;
        }

        public ApprovalFacebookLikeModelView GetFacebookLikesApprovalBySite(SiteIDs siteID, CommunityIDs communityID)
        {
            Member member = null;
            Brand brand = null;
            int brandID;
            bool hasContent = false;
            SiteID = siteID;
            CommunityID = communityID;
            ApprovalFacebookLikeModelView modelView = new ApprovalFacebookLikeModelView();
            modelView.SiteID = (int)siteID;
            modelView.CommunityID = (int)communityID;
            modelView.ApprovalType = QAApprovalType.QueuedApprovalBySite;
            PopulateFacebookLikeApprovals(modelView, QAApprovalType.QueuedApprovalBySite);
            modelView.TopNav = new SharedTopNav();
            

            if (modelView.MemberID > 0 && modelView.FacebookLikes != null && modelView.FacebookLikes.Count > 0)
            {
                hasContent = true;
                member = MemberHelper.GetMember(modelView.MemberID, MemberLoadFlags.None);
                member.GetLastLogonDate(modelView.CommunityID, out brandID);
                brand = BrandConfigSA.Instance.GetBrandByID(brandID);
                modelView.ExpandedUsrInfo = ApprovalHelper.GetExpandedUserInfo(member, brand);
            }

            modelView.Left = GetFacebookLikeApprovalLeft(siteID, member, brand, hasContent);

            return modelView;
        }

        public ApprovalFacebookLikeMemberModelView GetFacebookLikesApprovalByMember(int memberID, SiteIDs siteID, CommunityIDs communityID)
        {
            SiteID = siteID;
            CommunityID = communityID;
            MemberID = memberID;
            ApprovalFacebookLikeMemberModelView modelView = new ApprovalFacebookLikeMemberModelView();
            modelView.CommunityID = (int)communityID;
            modelView.SiteID = (int)siteID;
            modelView.MemberID = memberID;
            modelView.ApprovalType = QAApprovalType.MemberApproval;
            PopulateFacebookLikeApprovals(modelView, QAApprovalType.MemberApproval);
            Member member = MemberHelper.GetMember(memberID, MemberLoadFlags.None);
            MemberManager memberManager = new MemberManager(MemberID, (int)siteID, MemberLoadFlags.None);
            modelView.Left = memberManager.GetMemberLeft();
            modelView.TopNav = memberManager.GetTopNav();

            return modelView;
        }

        public void DeleteFacebookLikeQueueItem(int memberID, int siteID)
        {
            DBApproveQueueSA.Instance.DeleteFacebookLikeQueueItem(memberID, siteID);
        }

        private void PopulateFacebookLikeApprovals(ApprovalFacebookLikeModelViewBase modelView, QAApprovalType approvalType)
        {
            List<FacebookLike> facebookLikes = new List<FacebookLike>();
            bool foundValidItem = false;
            bool queueEmpty = false;
            List<QueueItemBase> queueItemFBLikes = null;

            while (!queueEmpty && !foundValidItem)
            {
                switch (approvalType)
                {
                    case QAApprovalType.QueuedApprovalByCommunity:
                        queueItemFBLikes = DBApproveQueueSA.Instance.DequeueFacebookLikesByCommunity(1, (int)CommunityID);
                        string queueCount = queueItemFBLikes == null ? "0" : queueItemFBLikes.Count.ToString();
                        break;
                    case QAApprovalType.QueuedApprovalBySite:
                        queueItemFBLikes = DBApproveQueueSA.Instance.DequeueFacebookLikesBySite(1, (int)SiteID);
                        break;
                    case QAApprovalType.MemberApproval:
                        //Note: Member approval is like edit profile, it does not utilizing dequeue, so admins should always be able to edit without requiring member to be in queue
                        //queueItemFBLikes = DBApproveQueueSA.Instance.DequeueFacebookLikesByMember((int)SiteID, MemberID);
                        queueItemFBLikes = new List<QueueItemBase>();
                        QueueItemFacebookLike queueItem = new QueueItemFacebookLike();
                        queueItem.MemberID = MemberID;
                        queueItem.BrandID = Constants.NULL_INT;
                        queueItem.SiteID = (int)SiteID;
                        queueItem.CommunityID = (int)CommunityID;
                        queueItemFBLikes.Add(queueItem);
                        foundValidItem = true;
                        break;
                }

                if (queueItemFBLikes == null || queueItemFBLikes.Count == 0)
                {
                    //no items left in queue
                    queueEmpty = true;
                }
                else
                {
                    QueueItemFacebookLike queueItemFacebookLike = (QueueItemFacebookLike)queueItemFBLikes[0];
                    FacebookLikeList facebookLikeList = GetFacebookLikesFromQueueItem(queueItemFacebookLike);

                    if (facebookLikeList != null)
                    {
                        modelView.MemberID = facebookLikeList.MemberID;
                        modelView.SiteID = facebookLikeList.SiteID;
                        switch (approvalType)
                        {
                            case QAApprovalType.MemberApproval:
                                facebookLikes = facebookLikeList.FacebookLikes;
                                break;
                            default:
                                foreach (FacebookLike fbLike in facebookLikeList.FacebookLikes)
                                {
                                    if (fbLike.FacebookLikeStatus != FacebookLikeStatus.Approved)
                                    {
                                        facebookLikes.Add(fbLike);
                                    }
                                }
                                break;
                        }

                        if (approvalType != QAApprovalType.MemberApproval)
                        {
                            if (facebookLikes.Count > 0)
                            {
                                foundValidItem = true;
                            }
                            else
                            {
                                //Remove invalid queue item
                                DeleteFacebookLikeQueueItem(queueItemFacebookLike.MemberID, queueItemFacebookLike.SiteID);
                            }
                        }
                    }
                    else
                    {
                        //Remove invalid queue item
                        DeleteFacebookLikeQueueItem(queueItemFacebookLike.MemberID, queueItemFacebookLike.SiteID);
                    }
                }
            }
            modelView.FacebookLikes = facebookLikes;
        }

        private FacebookLikeList GetFacebookLikesFromQueueItem(QueueItemFacebookLike queueItemFBLike)
        {
            FacebookLikeList facebookLikeList = null;

            try
            {
                FacebookLikeParams facebookLikeParams = new FacebookLikeParams();
                facebookLikeParams.MemberId = queueItemFBLike.MemberID;
                facebookLikeParams.SiteId = queueItemFBLike.SiteID;

                facebookLikeList = Spark.FacebookLike.ServiceAdapters.FacebookLikeServiceSA.Instance.GetFacebookLikeListByMember(facebookLikeParams, true);
            }
            catch (Exception ex)
            {
                //There was an error retrieving necessary attribute data, so mark the item as complete and move on
                facebookLikeList = null;
                _logger.Error(string.Format("Error in ApprovalManager.GetFacebookLikesFromQueueItem() MemberID:{0}, SiteID:{1} ", queueItemFBLike.MemberID, queueItemFBLike.SiteID), ex);
            }

            return facebookLikeList;
        }

        private ApprovalLeft GetFacebookLikeApprovalLeft(CommunityIDs communityID, Member member, Brand brand, bool hasContent)
        {
            SiteIDs siteID = GeneralHelper.GetPrimarySiteForCommunity(communityID);
            ApprovalLeft left = new ApprovalLeft();
            ApprovalHelper approvalHelper = new ApprovalHelper();
            left.IsTextApproval = false;
            left.SiteID = siteID;

            if (member != null && hasContent)
            {
                left.MemberID = member.MemberID;
                left.Gender = MemberHelper.GetGender(member, brand);
                left.Age = MemberHelper.GetAge(member.GetAttributeDate(brand, "Birthdate"));
                left.SiteID = siteID;
                left.FraudResult = MemberHelper.GetFraudDetailedResponse(member.MemberID, brand.Site.SiteID,3);
                left.PhotoURL = MemberPhotoHelper.GetDefaultPhotoThumbnailDisplayURL(member, brand);
            }

            ApprovalReportsManager manager = new ApprovalReportsManager();
            left.QueueCount = left.QueueCount = manager.GetTotalTextQueueCount(QueueItemType.FacebookLike, communityID);
            left.ItemsProcessed = manager.GetSessionQueueItemsProcessedCount();
            left.MemberMessages = approvalHelper.GetCurrentMemberMessages(g.AdminID);
            approvalHelper.ClearMemberMessages(g.AdminID);
            return left;
        }

        private ApprovalLeft GetFacebookLikeApprovalLeft(SiteIDs siteID, Member member, Brand brand, bool hasContent)
        {
            ApprovalLeft left = new ApprovalLeft();
            ApprovalHelper approvalHelper = new ApprovalHelper();
            left.IsTextApproval = false;
            left.SiteID = siteID;

            if (member != null && hasContent)
            {
                left.MemberID = member.MemberID;
                left.Gender = MemberHelper.GetGender(member, brand);
                left.Age = MemberHelper.GetAge(member.GetAttributeDate(brand, "Birthdate"));
                left.SiteID = siteID;
                left.FraudResult = MemberHelper.GetFraudDetailedResponse(member.MemberID, brand.Site.SiteID, 3);
                left.PhotoURL = MemberPhotoHelper.GetDefaultPhotoThumbnailDisplayURL(member, brand);
            }

            ApprovalReportsManager manager = new ApprovalReportsManager();
            left.QueueCount = left.QueueCount = manager.GetTotalTextQueueCount(QueueItemType.FacebookLike, siteID);
            left.ItemsProcessed = manager.GetSessionQueueItemsProcessedCount();
            left.MemberMessages = approvalHelper.GetCurrentMemberMessages(g.AdminID);
            approvalHelper.ClearMemberMessages(g.AdminID);
            return left;
        }

        public void CompleteFacebookLikesApproval(List<FacebookLikeParams> fbLikeParamsObjects, int memberID,
            CommunityIDs communityID, SiteIDs siteID, QAApprovalType approvalType, int fbLikesDisplayedCount, bool suspend, AdminActionReasonID adminActionReasonID)
        {
            ApprovalHelper approvalHelper = new ApprovalHelper();
            Member member = null;
            Brand brand = null;
            MemberHelper.GetMemberAndBrand(out member, out brand, memberID, (int)communityID);
            int actionMask = (int)AdminAction.Default;

            //Update to FacebookLikeService
            FacebookLikeSaveResult saveResult = null;
            if (approvalType == QAApprovalType.MemberApproval)
            {
                if (fbLikeParamsObjects.Count > 0)
                {
                    actionMask = actionMask | (int)AdminAction.ApprovedFacebookLike;
                    if (fbLikesDisplayedCount > fbLikeParamsObjects.Count)
                    {
                        actionMask = actionMask | (int)AdminAction.DeletedFacebookLike;
                    }
                    saveResult = FacebookLikeServiceSA.Instance.AdminProcessFacebookLikeByMember(fbLikeParamsObjects);
                }
                else
                {
                    //need admin remove all api?
                    actionMask = actionMask | (int)AdminAction.DeletedFacebookLike;
                    FacebookLikeParams fbLikeParams = new FacebookLikeParams();
                    fbLikeParams.MemberId = memberID;
                    fbLikeParams.SiteId = (int)siteID;
                    saveResult = FacebookLikeServiceSA.Instance.RemoveAllFacebookLikes(fbLikeParams);
                }
            }
            else
            {
                foreach (FacebookLikeParams flp in fbLikeParamsObjects)
                {
                    if (((actionMask & (int)AdminAction.DeletedFacebookLike) == (int)AdminAction.DeletedFacebookLike)
                        && ((actionMask & (int)AdminAction.ApprovedFacebookLike) == (int)AdminAction.ApprovedFacebookLike))
                    {
                        break;
                    }

                    if (flp.FbLikeStatus == FacebookLikeStatus.Approved)
                        actionMask = actionMask | (int)AdminAction.ApprovedFacebookLike;
                    else
                        actionMask = actionMask | (int)AdminAction.DeletedFacebookLike;
                }
                saveResult = FacebookLikeServiceSA.Instance.AdminProcessFacebookLikes(fbLikeParamsObjects);
            }

            if (suspend)
            {
                actionMask = actionMask | (int)AdminAction.AdminSuspendMember;
                bool stoppedRenewals = ApprovalHelper.SuspendMember(member, brand, g.AdminID, adminActionReasonID, actionMask, communityID, brand.Site.LanguageID);
                MemberSA.Instance.SaveMember(member);
            }

            DBApproveQueueSA.Instance.CompleteFacebookLikesApproval(memberID, (int)siteID, ApprovalStatus.Completed, g.AdminID, actionMask);
            AdminSA.Instance.AdminActionLogInsert(memberID, (int)communityID, actionMask, g.AdminID);

            approvalHelper.PersistMemberMessage(g.AdminID, member.MemberID, string.Format("Successfully approved member <a href='/Member/View/{1}/{0}' target='_blank'>{0}</a>", member.MemberID.ToString(), siteID.ToString("d")), false);
            new ApprovalReportsManager().AddSessionQueueItemProcessedCount();
        }

        

    }
}
