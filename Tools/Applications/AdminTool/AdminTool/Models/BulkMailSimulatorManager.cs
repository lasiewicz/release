﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Search.ServiceAdapters;
using Matchnet.Search.ValueObjects;
using Matchnet.BulkMail.ServiceAdapters;
using Matchnet.BulkMail.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.DoNotEmail;
using Matchnet.Configuration.ServiceAdapters;
using AdminTool.Models.ModelViews.Tools;

using Spark.CommonLibrary;

namespace AdminTool.Models
{
    public class BulkMailSimulatorManager
    {
        private const int PAGE_SIZE = 106;
        private int _cfgRegisteredDaysCriteria = Convert.ToInt32(RuntimeSettings.GetSetting("BULKMAILSVC_REGISTERED_DAYS_CRITERIA"));
        private CommunityIDs _communityID;
        private MemberManager memberManager = null;
        private double _memberLatitude = 0;
        private double _memberLongitude = 0;

        public BulkMailSimulatorModelView GetSimulatorModelView(int memberID, int siteID)
        {
            _communityID = GeneralHelper.GetCommunityIDForSiteID((SiteIDs)siteID);
            BulkMailSimulatorModelView modelView = new BulkMailSimulatorModelView();
            try
            {
                modelView.UseAgeFilter = bool.TrueString.ToLower().Equals(RuntimeSettings.GetSetting("BULKMAIL_USE_AGE_FILTER"));
            }catch (Exception ignore)
            {
                modelView.UseAgeFilter = false;
            }

            try
            {
                modelView.KeywordSearchEnabled = bool.TrueString.ToLower().Equals(RuntimeSettings.GetSetting("ENABLE_KEYWORD_MATCHES_SEARCHPREF", (int)_communityID, siteID, Matchnet.Constants.NULL_INT));
            }
            catch (Exception e)
            {
                modelView.KeywordSearchEnabled = false;
            }
            memberManager = new MemberManager(memberID, siteID, MemberLoadFlags.None);
            modelView.TopNav = memberManager.GetTopNav();
            modelView.Left = memberManager.GetMemberLeft();


            Member member = null;
            Brand brand = null;
            int brandID;

            
            try
            {
                member = MemberHelper.GetMember(memberID, MemberLoadFlags.None);
                member.GetLastLogonDate((int)_communityID, out brandID);
                brand = BrandConfigSA.Instance.GetBrandByID(brandID);
            }
            catch(Exception ex)
            {
                modelView.Errored = true;
                modelView.ErrorMessage = "Error retrieving member/brand";
                return modelView;
            }

            
            if (member.EmailAddress == null || member.EmailAddress == string.Empty)
            {
                modelView.Errored = true;
                modelView.ErrorMessage = "Member had no email address";
                return modelView;
            }

            DoNotEmailEntry dneEntry = DoNotEmailSA.Instance.GetEntryByEmailAddress(member.EmailAddress);
            if (dneEntry != null)
            {
                modelView.Errored = true;
                modelView.ErrorMessage = "Member is on DNE list";
                return modelView;
            }

            modelView.ScheduleRecord = BulkMailReportingSA.Instance.GetBulkMailScheduleRecord(memberID, (int)_communityID, BulkMailType.MatchMail);
            
            if (modelView.ScheduleRecord == null)
            {
                modelView.Errored = true;
                modelView.ErrorMessage = "Member has no MatchMail schedule record.";
                return modelView;
            }

            SearchPreferenceCollection strictPreferences = SearchPreferencesSA.Instance.GetSearchPreferences(memberID, (int)_communityID, true);
            BulkMailQueueItem queueItem = new BulkMailQueueItem(memberID,
                (int)_communityID,
                BulkMailType.MatchMail,
                (Byte)modelView.ScheduleRecord.Frequency,
                modelView.ScheduleRecord.LastSentDate,
                modelView.ScheduleRecord.LastAttemptDate,
                modelView.ScheduleRecord.NextAttemptDate,
                modelView.ScheduleRecord.SentCount,
                modelView.ScheduleRecord.FrequencyUpdateDate,
                modelView.ScheduleRecord.InsertDate,
                null,
                strictPreferences);

            MatchResult result = BulkMailReportingSA.Instance.RunMatchSimulation(member.MemberID, queueItem, brand, new ScrubList(modelView.ScheduleRecord.SentMemberIDList));

            SearchPreferenceCollection initialPreferences = null;
            SearchPreferenceCollection initialPreferencesWithFilter = null;
            modelView.SearchPrefType = result.SearchPrefType;
            if (result.CreatedDefaultPreferences)
            {
                initialPreferences = result.DefaultSearchPreferences;
                initialPreferencesWithFilter = result.DefaultSearchPreferencesWithAgeFilter;
            }
            else
            {
                initialPreferences = result.StoredSearchPreferences;
                initialPreferencesWithFilter = result.StoredSearchPreferencesWithAgeFilter;
            }

            int totalMatchesFound = 0;
            modelView.InitialSearch.TotalSearchResults = 0;
            modelView.LaxSearch.TotalSearchResults = 0;
            modelView.InitialSearchWithAgeFilter.TotalSearchResults = 0;
            modelView.LaxSearchWithAgeFilter.TotalSearchResults = 0;

            modelView.InitialSearch.SearchPreferences = initialPreferences;
            modelView.InitialSearch.Preferences = TranslateSearchPreferences(initialPreferences, brand);
            if (modelView.UseAgeFilter)
            {
                modelView.InitialSearchWithAgeFilter.SearchPreferences = initialPreferencesWithFilter;
                modelView.InitialSearchWithAgeFilter.Preferences = TranslateSearchPreferences(initialPreferencesWithFilter, brand);
            }

            if (modelView.UseAgeFilter && result.InitialSearchResultsWithAgeFilter != null)
            {
                modelView.InitialSearchWithAgeFilter.TotalSearchResults = result.InitialSearchResultsWithAgeFilter.Items.Count;
                modelView.InitialSearchWithAgeFilter.AlreadyOnScrubList = result.InitialMatchResultsWithAgeFilter.MembersOnScrubList.Count;
                modelView.InitialSearchWithAgeFilter.AlreadyOnScrubListIds = result.InitialMatchResultsWithAgeFilter.MembersOnScrubList;
                modelView.InitialSearchWithAgeFilter.IneligibleMatches = result.InitialMatchResultsWithAgeFilter.InvalidMembers.Count;
                modelView.InitialSearchWithAgeFilter.Matches = getMatchMembers(result.InitialMatchResultsWithAgeFilter.MatchedMembers);
                modelView.InitialSearchSufficientWithAgeFilter = result.InitialMatchResultsWithAgeFilter.SearchSufficient;
                totalMatchesFound = modelView.InitialSearchWithAgeFilter.Matches.Count;
            }

            if (result.InitialSearchResults != null)
            {
//                result.InitialMatchResults.MembersOnScrubList = new int[] {}.ToList();
//                result.InitialMatchResults.MembersOnScrubList = new int[] { 8765 }.ToList();
//                result.InitialMatchResults.MembersOnScrubList = new int[] { 1234, 5678, 4321, 8765 }.ToList();

                modelView.InitialSearch.TotalSearchResults = result.InitialSearchResults.Items.Count;
                modelView.InitialSearch.AlreadyOnScrubList = result.InitialMatchResults.MembersOnScrubList.Count;
                modelView.InitialSearch.AlreadyOnScrubListIds = result.InitialMatchResults.MembersOnScrubList;
                modelView.InitialSearch.IneligibleMatches = result.InitialMatchResults.InvalidMembers.Count;
                modelView.InitialSearch.Matches = getMatchMembers(result.InitialMatchResults.MatchedMembers);
                modelView.InitialSearchSufficient = result.InitialMatchResults.SearchSufficient;
                totalMatchesFound += modelView.InitialSearch.Matches.Count;
            }

            if (modelView.UseAgeFilter && result.LaxSearchResultsWithAgeFilter != null)
            {
                modelView.LaxSearchWithAgeFilter.SearchPreferences = result.LaxSearchPreferencesWithAgeFilter;
                modelView.LaxSearchWithAgeFilter.Preferences = TranslateSearchPreferences(result.LaxSearchPreferencesWithAgeFilter, brand);
                modelView.LaxSearchWithAgeFilter.TotalSearchResults = result.LaxSearchResultsWithAgeFilter.Items.Count;
                modelView.LaxSearchWithAgeFilter.AlreadyOnScrubList = result.LaxMatchResultsWithAgeFilter.MembersOnScrubList.Count;
                modelView.LaxSearchWithAgeFilter.AlreadyOnScrubListIds = result.LaxMatchResultsWithAgeFilter.MembersOnScrubList;
                modelView.LaxSearchWithAgeFilter.IneligibleMatches = result.LaxMatchResultsWithAgeFilter.InvalidMembers.Count;
                modelView.LaxSearchWithAgeFilter.Matches = getMatchMembers(result.LaxMatchResultsWithAgeFilter.MatchedMembers);
                modelView.LaxSearchSufficientWithAgeFilter = result.LaxMatchResultsWithAgeFilter.SearchSufficient;
                totalMatchesFound += modelView.LaxSearchWithAgeFilter.Matches.Count;
            }

            if (result.LaxSearchResults != null)
            {
                modelView.LaxSearch.SearchPreferences = result.LaxSearchPreferences;
                modelView.LaxSearch.Preferences = TranslateSearchPreferences(result.LaxSearchPreferences, brand);
                modelView.LaxSearch.TotalSearchResults = result.LaxSearchResults.Items.Count;
                modelView.LaxSearch.AlreadyOnScrubList = result.LaxMatchResults.MembersOnScrubList.Count;
                modelView.LaxSearch.AlreadyOnScrubListIds = result.LaxMatchResults.MembersOnScrubList;
                modelView.LaxSearch.IneligibleMatches = result.LaxMatchResults.InvalidMembers.Count;
                modelView.LaxSearch.Matches = getMatchMembers(result.LaxMatchResults.MatchedMembers);
                modelView.LaxSearchSufficient = result.LaxMatchResults.SearchSufficient;
                totalMatchesFound += modelView.LaxSearch.Matches.Count;
            }

            modelView.MatchesFound = totalMatchesFound;
            
            /*SearchPreferenceCollection strictPreferences = SearchPreferencesSA.Instance.GetSearchPreferences(memberID, (int)_communityID);
            strictPreferences.Add("HasPhotoFlag", "1");
            ScrubList scrubList = new ScrubList(modelView.ScheduleRecord.SentMemberIDList);
            modelView.ScrubListCount = scrubList.Count;
            
            if (strictPreferences == null || strictPreferences.Count == 0)
            {
                modelView.Warnings.Add("Member has no stored search preferences.");
            }
            else
            {
                ValidationResult validationResult = strictPreferences.Validate();
                if (validationResult.Status == PreferencesValidationStatus.Failed)
                {
                    modelView.Warnings.Add("Member has no valid search preferences. Reason: " + validationResult.ValidationError.ToString());
                }
                else
                {
                    hasStoredSearchPrefs = true;
                }
            }
            if (!hasStoredSearchPrefs)
            {
                //to do - invent search prefs
            }

            modelView.InitialSearch.SearchPreferences = strictPreferences;
            modelView.InitialSearch.Preferences = TranslateSearchPreferences(strictPreferences, brand);

            MatchnetQueryResults initialResults = populateResultsCollection(strictPreferences, brand, memberID);
            if(initialResults != null)
            {
                modelView.InitialSearch.TotalSearchResults = initialResults.MatchesFound;

                numStrictMatchesFound = populateMatches(initialResults, brand, modelView.InitialSearch, modelView.FinalMatches, scrubList, numMatchesToSend, memberID);
                if (numStrictMatchesFound == numMatchesToSend)
                {
                    modelView.InitialSearchSufficient = true;
                }
            }

            if (hasStoredSearchPrefs && modelView.FinalMatches.Count < numMatchesToSend)
            {
                SearchPreferenceCollection laxPreferences = BulkMailReportingSA.Instance.GetLaxSearchPreference(memberID, brand, strictPreferences);
                modelView.LaxSearch.SearchPreferences = laxPreferences;
                modelView.LaxSearch.Preferences = TranslateSearchPreferences(laxPreferences, brand);
                scrubList = new ScrubList();

                MatchnetQueryResults laxResults = populateResultsCollection(laxPreferences, brand, memberID);
                modelView.LaxSearch.TotalSearchResults = laxResults.MatchesFound;

                numLaxMatchesFound = populateMatches(laxResults, brand, modelView.LaxSearch, modelView.FinalMatches, scrubList, numMatchesToSend, memberID);
                if (numStrictMatchesFound + numLaxMatchesFound == numMatchesToSend)
                {
                    modelView.LaxSearchSufficient = true;
                }
            }

            modelView.MatchesFound = numStrictMatchesFound + numLaxMatchesFound;*/
            
            return modelView;
        }

        private MatchnetQueryResults populateResultsCollection(SearchPreferenceCollection searchPrefs, Brand brand, int memberID)
        {
            MatchnetQueryResults searchResults = null;

            searchResults = MemberSearchSA.Instance.Search(searchPrefs,
                brand.Site.Community.CommunityID,
                brand.Site.SiteID,
                0,
                PAGE_SIZE,
                Matchnet.Search.Interfaces.SearchEngineType.FAST,
                Matchnet.Search.Interfaces.SearchType.MatchMail);
            return searchResults;
        }

        private int populateMatches(MatchnetQueryResults searchResults, Brand brand, MatchSearch matchSearch, List<MatchMember> allMatches, ScrubList scrubList, int numTotalMatchesToProcess, int memberID)
        {
            BulkMailSearchResultItem BulkMailResult;
            int numMatches = 0;
            Member member;

            if (searchResults.Items.Count > 0)
            {
                for (int resultIdx = 0; resultIdx < searchResults.Items.Count; resultIdx++)
                {
                    member = null;
                    MatchnetResultItem resultItem = (searchResults.Items[resultIdx] as MatchnetResultItem);
                    BulkMailResult = new BulkMailSearchResultItem(resultItem);

                    if (!scrubList.Contains(resultItem.MemberID) && resultItem.MemberID != memberID)
                    {
                        if (!memberAlreadyInMatches(resultItem.MemberID, allMatches))
                        {
                            member = getValidatedMember(resultItem.MemberID, brand);
                            if (member != null)
                            {
                                MatchMember matchMember = getMatchMember(member);
                                matchSearch.Matches.Add(matchMember);
                                allMatches.Add(matchMember);
                                numMatches++;
                                scrubList.Add(resultItem.MemberID);
                            }
                            else
                            {
                                matchSearch.IneligibleMatches++;
                            }
                        }
                    }
                    else
                    {
                        matchSearch.AlreadyOnScrubList++;
                    }

                    if (allMatches.Count == numTotalMatchesToProcess)
                    {
                        break;
                    }
                }
            }

            return numMatches;
        }

        private bool memberAlreadyInMatches(int memberID, List<MatchMember> matches)
        {
            MatchMember member = (from m in matches where m.MemberID == memberID select m).FirstOrDefault();
            return (member != null);
        }

        private Member getValidatedMember(int memberID, Brand brand)
        {
            Member member = MemberHelper.GetMember(memberID, MemberLoadFlags.None);

            DateTime registerDate = member.GetAttributeDate(brand, "BrandInsertDate", DateTime.MinValue);
            if (DateTime.Now.Subtract(registerDate).Days < _cfgRegisteredDaysCriteria)
            {
                member = null;
            }
            return member;
        }

        private MatchMember getMatchMember(Member member)
        {
            MatchMember matchMember = new MatchMember();
            try
            {
            SiteIDs siteID = GeneralHelper.GetPrimarySiteForCommunity(_communityID);
            Brand brand = GeneralHelper.GetBrandForSite((int)siteID);

            matchMember.MemberID = member.MemberID;
            matchMember.UserName = member.GetUserName(brand);
            matchMember.Age = MemberHelper.GetAge(member.GetAttributeDate(brand, "Birthdate"));
            matchMember.GenderAndSeeking = MemberHelper.GetMemberGender(member, brand).ToString() + " seeking " + MemberHelper.GetSeekingGender(member, brand).ToString();
            matchMember.Location = MemberHelper.GetRegionDisplayString(member, brand);

                SearchPreferenceCollection matchMemberPreferences = SearchPreferencesSA.Instance.GetSearchPreferences(member.MemberID, (int)_communityID, true);
                if (matchMemberPreferences != null)
                {
                    if (matchMemberPreferences.ContainsKey("MinAge"))
                    {
                        matchMember.PreferredMinAge = Convert.ToInt32(matchMemberPreferences["MinAge"]);
                    }

                    if (matchMemberPreferences.ContainsKey("MaxAge"))
                    {
                        matchMember.PreferredMaxAge = Convert.ToInt32(matchMemberPreferences["MaxAge"]);
                    }
                }

            //Calculate distance
            if (memberManager != null && memberManager.Member != null && memberManager.MemberID > 0)
            {
                //get member's longitude/latitude
                if (_memberLatitude <= 0 || _memberLongitude <= 0)
                {
                    int memberRegionID = memberManager.Member.GetAttributeInt(brand, "RegionID");
                    SearchPreferenceCollection memberPreferences = SearchPreferencesSA.Instance.GetSearchPreferences(memberManager.MemberID, (int)_communityID, true);
                        if (memberPreferences != null)
                        {
                            if(memberPreferences.ContainsKey("RegionID"))
                    {
                        memberRegionID = Convert.ToInt32(memberPreferences["RegionID"]);
                    }
                        }
                    Matchnet.Content.ValueObjects.Region.Region memberRegion = RegionSA.Instance.RetrieveRegionByID(Convert.ToInt32(memberRegionID), (int)Matchnet.Language.English);

                    if (null != memberRegion)
                    {
                        _memberLatitude = GeneralHelper.RadianToDegree(Convert.ToDouble(memberRegion.Latitude.ToString()));
                        _memberLongitude = GeneralHelper.RadianToDegree(Convert.ToDouble(memberRegion.Longitude.ToString()));
                    }
                }

                //get match member's longitude/latitude
                int matchMemberRegionID = member.GetAttributeInt(brand, "RegionID");
                Matchnet.Content.ValueObjects.Region.Region matchMemberRegion = RegionSA.Instance.RetrieveRegionByID(Convert.ToInt32(matchMemberRegionID), (int)Matchnet.Language.English);
                double matchMemberLatitude = 0;
                double matchMemberLongitude = 0;
                if (null != matchMemberRegion)
                {
                    matchMemberLatitude = GeneralHelper.RadianToDegree(Convert.ToDouble(matchMemberRegion.Latitude.ToString()));
                    matchMemberLongitude = GeneralHelper.RadianToDegree(Convert.ToDouble(matchMemberRegion.Longitude.ToString()));
                }

                //calculate distance
                matchMember.Distance = Math.Round(Lucene.Net.Spatial.Tier.DistanceUtils.GetInstance().GetDistanceMi(_memberLatitude, _memberLongitude, matchMemberLatitude, matchMemberLongitude), 2);
            }
            }
            catch (Exception e)
            {
            
            }
            return matchMember;
        }

        private List<MatchMember> getMatchMembers(List<int> matches)
        {
            List<MatchMember> matchMembers = new List<MatchMember>();
            
            foreach (int match in matches)
            {
                Member member = MemberHelper.GetMember(match, MemberLoadFlags.None);
                matchMembers.Add(getMatchMember(member));
            }

            return matchMembers;
        }

        private List<MatchSearchPreference> TranslateSearchPreferences(SearchPreferenceCollection originalPreferences, Brand brand)
        {
            List<MatchSearchPreference> translatedPreferences = new List<MatchSearchPreference>();
            if(originalPreferences!= null){

                foreach (DictionaryEntry entry in originalPreferences)
                {
                    MatchSearchPreference preference = new MatchSearchPreference();
                    preference.Name = entry.Key.ToString().ToLower();
                    switch (preference.Name)
                    {
                        case "gendermask":
                            preference.Value = GeneralHelper.GetGenderAndSeekingStringFromMask(Int32.Parse(entry.Value.ToString()));
                            break;
                        case "preferredage":
                            preference.Value = (entry.Value == null || string.IsNullOrEmpty(entry.Value.ToString())) ? "empty" : entry.Value.ToString();
                            preference.Name = "Age For Filter";
                            break;
                        case "distance":
                        case "minage":
                        case "maxage":
                        case "hasphotoflag":
                        case "schoolid":
                        case "areacodes":
                        case "memberid":
                        case "colorcode":
                        case "radius": 
                        case "keywordsearch":
                            preference.Value = entry.Value == null ? string.Empty : entry.Value.ToString();
                            break;
                        case "searchtypeid":
                            preference.Value = ((SearchTypeID)Enum.Parse(typeof(SearchTypeID), entry.Value.ToString())).ToString();
                            break;
                        case "searchorderby":
                            preference.Value = ((Matchnet.Search.Interfaces.QuerySorting)Enum.Parse(typeof(Matchnet.Search.Interfaces.QuerySorting), entry.Value.ToString())).ToString();
                            break;
                        case "domainid":
                        case "groupid":
                            preference.Value = ((CommunityIDs)Enum.Parse(typeof(CommunityIDs), entry.Value.ToString())).ToString();
                            break;
                        case "minheight":
                        case "maxheight":
                            int height = int.Parse(entry.Value.ToString());
                            if (height == Matchnet.Constants.NULL_INT)
                            {
                                preference.Value = "null";
                            }
                            else
                            {
                                preference.Value = GeneralHelper.GetHeightDisplay(int.Parse(entry.Value.ToString()));
                            }
                            break;
                        case "regionid":
                        case "countryregionid":
                            preference.Value = GeneralHelper.GetRegionString(int.Parse(entry.Value.ToString()), true, true, true, brand);
                            break;
                        case "jdatereligion":
                        case "jdateethnicity":
                        case "languagemask":
                        case "activitylevel":
                        case "religion":
                        case "ethnicity":
                        case "maritalstatus":
                        case "drinkinghabits":
                        case "educationlevel":
                        case "morechildrenflag":
                        case "relocateflag":
                        case "smokinghabits":
                        case "synagogueattendance":
                        case "keepkosher":
                        case "bodytype":
                        case "custody":
                            preference.Value = GeneralHelper.GetMaskContent(preference.Name, Int32.Parse(entry.Value.ToString()), brand);
                            break;
                        default:
                            preference.Value = "empty";
                            break;
                    }
                    translatedPreferences.Add(preference);
                }
            }
            
            return (from mp in translatedPreferences orderby mp.Name select mp).ToList();
        }
    }
}
