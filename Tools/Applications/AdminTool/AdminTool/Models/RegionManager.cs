﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet;
using Matchnet.Content.ValueObjects.BrandConfig;
using System.Web.Mvc;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Content.ServiceAdapters;
using AdminTool.Models.JSONResult;
using Matchnet.Lib;
using System.Text;
using System.Collections;

namespace AdminTool.Models
{
    public class RegionManager : ManagerBase
    {
        public const int TEMP_TRANSLATION_ID = 2;
        
        private Brand _brand;
        private int _languageId = Constants.NULL_INT;

        public int LanguageId
        {
            get
            {
                if (_brand != null)
                    return _brand.Site.LanguageID;

                return _languageId;
            }
            set { _languageId = value; }
        }

        public RegionManager(int siteID)
        {
            _brand = g.GetBrand(siteID);
        }

        public List<SelectListItem> GetCountries()
        {

            RegionCollection countries = RegionSA.Instance.RetrieveCountries(LanguageId);

            return LoadItems(countries);
        }

        public List<SelectListItem> GetChildRegions(int parentRegionId)
        {
            var childRegions = RegionSA.Instance.RetrieveChildRegions(parentRegionId, LanguageId);
            if(childRegions == null)
                return new List<SelectListItem>();

            return (from Region r in childRegions
                    select new SelectListItem() {Text = r.Description, Value = r.RegionID.ToString()}).ToList();
        }

        public int GetRegionID(int countryRegionID, int stateRegionID, string cityName)
        {
            int regionID = -1;
            RegionID rID = null;

            Matchnet.Content.ValueObjects.Region.Region regionInfo = RegionSA.Instance.RetrieveRegionByID(countryRegionID, LanguageId);
            if (regionInfo == null)
                return regionID;

            if (regionInfo.ChildrenDepth == 3)
            { 
                // no state countries
                rID = RegionSA.Instance.FindRegionIdByCity(countryRegionID, cityName, LanguageId);
            }
            else
            {
                // countries with states
                rID = RegionSA.Instance.FindRegionIdByCity(stateRegionID, cityName, LanguageId);
            }

            if (rID != null)
                regionID = rID.ID;

            return regionID;
        }

        public List<SelectListItem> GetRegionIDByZip(int countryRegionID, string zipCode, out int regionID, bool ignoreMultiCity)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            regionID = -1;

            // this method shouldn't called for countries outside US and Canada
            if (countryRegionID != ConstantsTemp.REGION_US && countryRegionID != ConstantsTemp.REGIONID_CANADA)
                return items;

            RegionCollection colCities = RegionSA.Instance.RetrieveCitiesByPostalCode(zipCode);
            if (colCities == null)
                return items;

            // for US only there is a possiblity of more cities per zipcode, so check for that
            if (countryRegionID == ConstantsTemp.REGION_US && colCities.Count > 1)
            {
                if (ignoreMultiCity)
                {
                    regionID = colCities[0].RegionID;
                }
                else
                {
                    // more cities returned for this zip. don't set the regionID and return the list of cities instead
                    foreach (Region r in colCities)
                    {
                        items.Add(new SelectListItem { Text = r.Description, Value = r.RegionID.ToString() });
                    }
                    return items;
                }
            }
            else
            {
                RegionID rID = RegionSA.Instance.FindRegionIdByPostalCode(countryRegionID, zipCode);

                if (rID != null)
                    regionID = rID.ID;
            }

            return items;
        }

        public RegionDisplay GetNextDataToCapture(int countryRegionID, string caller)
        {
            RegionDisplay ret = RegionDisplay.None;

            if (caller != "editsearch" && (countryRegionID == ConstantsTemp.REGION_US || countryRegionID == ConstantsTemp.REGIONID_CANADA))
                ret = RegionDisplay.Zip;
            else
            {
                Matchnet.Content.ValueObjects.Region.Region regionInfo = RegionSA.Instance.RetrieveRegionByID(countryRegionID, LanguageId);
                if (regionInfo != null && regionInfo.ChildrenDepth == 3)
                {
                    ret = RegionDisplay.City;
                }
                else
                {
                    ret = RegionDisplay.State;
                }
            }
            
            return ret;
        }

        public List<SelectListItem> GetStates(int countryRegionID)
        {
            RegionCollection regions;
            if (LanguageId == (int)Matchnet.Language.Hebrew)
            {
                regions = RegionSA.Instance.RetrieveChildRegions(countryRegionID, LanguageId, TEMP_TRANSLATION_ID, true);
            }
            else
            {
                regions = RegionSA.Instance.RetrieveChildRegions(countryRegionID, LanguageId, TEMP_TRANSLATION_ID, true);
            }

            return LoadItems(regions);
        }

        private List<SelectListItem> LoadItems(RegionCollection regions)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (Region r in regions)
            {
                items.Add(new SelectListItem { Text = r.Description, Value = r.RegionID.ToNullableString() });
            }

            return items;
        }

        internal List<SelectListItem> GetCityList(int countryRegionID, int stateRegionID, string cityPrefix)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            int regionIDToPass = countryRegionID;

            if (stateRegionID > 0)
                regionIDToPass = stateRegionID;

            RegionCollection rCol = RegionSA.Instance.RetrieveChildRegions(regionIDToPass, LanguageId);
            if (rCol == null)
                return items;

            foreach (Region r in rCol)
            {
                if (cityPrefix.ToUpper() == r.Description.Substring(0, 1).ToUpper())
                {
                    items.Add(new SelectListItem { Value = r.RegionID.ToString(), Text= r.Description });
                }
            }

            items.Sort((x, y) => x.Text.CompareTo(y.Text));

            return items;
        }

        internal List<SelectListItem> GetCityPrefix(int countryRegionID, int stateRegionID)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            int regionIDToPass = countryRegionID;

            if (stateRegionID > 0)
                regionIDToPass = stateRegionID;

            RegionCollection rCol = RegionSA.Instance.RetrieveChildRegions(regionIDToPass, LanguageId);
            if (rCol == null)
                return items;

            foreach (Region r in rCol)
            {
                if (items.Find(x => x.Value.ToUpper().Equals(r.Description.Substring(0, 1).ToUpper())) == null)
                {
                    items.Add(new SelectListItem { Value = r.Description.Substring(0, 1).ToUpper(), Text = r.Description.Substring(0, 1).ToUpper() });
                }

            }

            items.Sort((x, y) => x.Text.CompareTo(y.Text));

            return items;
        }

        internal RegionBindInfo GetRegionBindInfo(int regionID)
        {
            RegionBindInfo bindInfo = new RegionBindInfo();
            
            RegionLanguage regionLang = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, LanguageId);
            if (regionLang == null)
                return bindInfo;

            bindInfo.RegionID = regionID;

            // determine if state is required for this country
            Matchnet.Content.ValueObjects.Region.Region regionInfo = RegionSA.Instance.RetrieveRegionByID(regionLang.CountryRegionID, LanguageId);
            if (regionInfo.ChildrenDepth != 3)
            {
                // we need state
                bindInfo.StateRegionID = regionLang.StateRegionID;
            }
            
            // for every country, we have country code and city name so set these first
            bindInfo.CountryRegionID = regionLang.CountryRegionID;
            bindInfo.CityName = regionLang.CityName;

            // if US or Canada, we have postal code
            if (regionLang.PostalCode != null &&
                regionLang.PostalCode != string.Empty &&
                (regionLang.CountryRegionID == ConstantsTemp.REGIONID_USA || regionLang.CountryRegionID == ConstantsTemp.REGIONID_CANADA) )
            {
                bindInfo.ZipCode = regionLang.PostalCode;
                if (regionLang.CountryRegionID == ConstantsTemp.REGIONID_USA)
                {
                    // for US, check for multi zip city
                    RegionCollection colCities = RegionSA.Instance.RetrieveCitiesByPostalCode(regionLang.PostalCode);
                    if (colCities != null && colCities.Count > 1)
                    {
                        bindInfo.IsMutiCityZip = true;
                    }
                }
            }

            return bindInfo;
        }

        internal RegionString GetRegionString(int regionID)
        {
            RegionString regionString = new RegionString();
            StringBuilder sb = new StringBuilder();

            RegionLanguage regionLang = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, LanguageId);
            if (regionLang == null)
                return regionString;

            // if US or Canada, check for multi city zip possibility and just select the first city
            if (regionLang.PostalCode != null && (regionLang.CountryRegionID == ConstantsTemp.REGIONID_USA || regionLang.CountryRegionID == ConstantsTemp.REGIONID_CANADA) )
            {
                RegionCollection colCities = RegionSA.Instance.RetrieveCitiesByPostalCode(regionLang.PostalCode);
                if (colCities != null && colCities.Count > 1)
                {
                    sb.Append(colCities[0].Description);
                }
            }

            if(sb.Length == 0)
                sb.Append(regionLang.CityName);

            // determine if state is required for this country
            Matchnet.Content.ValueObjects.Region.Region regionInfo = RegionSA.Instance.RetrieveRegionByID(regionLang.CountryRegionID, LanguageId);
            if (regionInfo.ChildrenDepth != 3)
            {
                // we need state
                sb.Append(", " + regionLang.StateDescription);
            }
            else
            {
                // level 3 means no state so append the country name instead
                sb.Append(", " + regionLang.CountryName);
            }

            regionString.RegionDisplayString = sb.ToString();
            return regionString;
        }

        internal ResultBase ValidateAreaCodes(string areaCodes, int countryRegionID)
        {
            ResultBase result = new ResultBase();
            result.Status = (int)AdminTool.Models.Enums.Status.Failed;

            string[] splits = areaCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (splits == null)
            {
                return result;
            }

            int[] intAreaCodes = new int[splits.Length];
            int intValue;
            int i = 0;

            foreach (string s in splits)
            {
                if (int.TryParse(s, out intValue))
                {
                    intAreaCodes[i] = intValue;
                }
                else
                {
                    return result;
                }

                i++;
            }

            string invalidAreaCodes = Matchnet.Content.ServiceAdapters.RegionSA.Instance.IsValidAreaCodes(intAreaCodes, countryRegionID);

            if (invalidAreaCodes == string.Empty)
                result.Status = (int)AdminTool.Models.Enums.Status.Success;
            else
            {
                result.StatusMessage = invalidAreaCodes + " are invalid";
            }

            return result;
        }
    }
}
