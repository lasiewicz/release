﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminTool.Models.ModelViews
{
    public class GenderPicker : ModelViewBase
    {
        public int GenderMaskValue { get; set; }
        public int GenderValue {
            get {
                return (GenderMaskValue & 51);
            }
        }
        public int SeekingGenderValue {
            get {
                return (GenderMaskValue & 204);
            }
        }
        public List<SelectListItem> GenderOptions = new List<SelectListItem>();
        public List<SelectListItem> SeekingGenderOptions = new List<SelectListItem>();
        public string FormFieldName = "GenderPicker01";
    }
}
