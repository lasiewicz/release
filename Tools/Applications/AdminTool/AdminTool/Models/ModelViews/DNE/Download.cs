﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.ExternalMail.ValueObjects;
using Matchnet.ExternalMail.ValueObjects.DoNotEmail;

namespace AdminTool.Models.ModelViews.DNE
{
    public class Download : ModelViewBase
    {
        public ArrayList SiteSummary { get; set; }
        public SharedTopNav TopNav { get; set; }
        public List<bool> IsChecked { get; set; }

        public Download()
        {
            SiteSummary = new ArrayList();
            TopNav = new SharedTopNav();
            IsChecked = new List<bool>();
        }
    }
}