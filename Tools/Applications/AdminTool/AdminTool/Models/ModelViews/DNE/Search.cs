﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ValueObjects;

namespace AdminTool.Models.ModelViews.DNE
{
    public class Search : ModelViewBase
    {
        public const int PageSize = 20;
        private string emailAddress;

        public string EmailAddress
        {
            get { return emailAddress; }
            set { emailAddress = value; }
        }

        public int SiteId { get; set; }
        public int StartRow { get; set; }
        public int TotalRows { get; set; }
        public int EmailUploadCount { get; set; }
        public int PageNumber { get; set; }
        public int EndRow
        {
            get
            {
                int endRow = StartRow + PageSize - 1;
                return endRow > TotalRows ? TotalRows : endRow;
            }
        }
        public int TotalPages
        {
            get
            {
                int totalPages = TotalRows / PageSize;
                return TotalRows % PageSize > 0 ? totalPages + 1 : totalPages;
            }
        }

        public IEnumerable<SelectListItem> Sites { get; set; }
        public ArrayList SearchResults { get; set; }
        public SharedTopNav TopNav { get; set; }

        public Search()
        {
            TopNav = new SharedTopNav();
            SearchResults = new ArrayList();
            StartRow = 1;
            PageNumber = 1;
            TotalRows = -1;
        }
        
    }
}