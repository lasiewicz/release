﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews
{
    public class ModelViewBase
    {
        ActiveDirectoryHelper securityHelper = new ActiveDirectoryHelper();

        public ContextGlobal g
        {
            get { return HttpContext.Current.Items["g"] as ContextGlobal; }
        }

        public bool IsNewAgent
        {
            get
            {
                return securityHelper.CheckUserAccess(HttpContext.Current.User.Identity,
                                                      (int) Enums.Operations.NewAgentRights);
            }
        }

        public string BigUiMessage { get; set; }

        public bool CheckUIPermission(AdminTool.Models.Enums.Operations operation)
        {
            
            if (!securityHelper.CheckUserAccess(HttpContext.Current.User.Identity, (int)operation))
            {
                return false;
            }

            return true;
        }

        public string GetDisplayDate(DateTime date)
        {
            return date.ToString("MM/dd/yyyy hh:mm tt");
        }

        public string GetShortDisplayDate(DateTime date)
        {
            return date.ToString("MM/dd/yy hh:mm tt");
        }

        public string TruncateString(string StringToTruncate, int max)
        {
            if (String.IsNullOrEmpty(StringToTruncate) || StringToTruncate.Length <= max)
            {
                return StringToTruncate;
            }
            else
            {
                return StringToTruncate.Substring(0, max) + "...";
            }
        }
    }
}
