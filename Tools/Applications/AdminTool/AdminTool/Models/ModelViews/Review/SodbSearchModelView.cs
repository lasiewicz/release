﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spark.SODBQueue.ValueObjects;

namespace AdminTool.Models.ModelViews.Review
{
    public class SodbSearchModelView
    {
        [Required(ErrorMessage = "*")]
        public int? MemberId { get; set; }
        public int? SSPBlueId { get; set; }
        public int? SiteId { get; set; }
        public List<SodbItem> SodbMembers { get; set; }
        public SharedTopNav TopNav { get; set; }
        public List<SelectListItem> SodbSites { get; set; }

        public SodbSearchModelView()
        {
            TopNav = new SharedTopNav();
            SodbSites = ReviewManager.GetSodbSites();
            SodbMembers = new List<SodbItem>();
        }
    }
}