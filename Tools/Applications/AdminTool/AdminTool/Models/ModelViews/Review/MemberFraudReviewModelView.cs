﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models.ModelViews.Approval;

namespace AdminTool.Models.ModelViews.Review
{
    public class MemberFraudReviewModelView
    {
        public SharedTopNav TopNav { get; set; }
        public int? SiteId { get; set; }
        public List<MemberFraudReviewData> MemberItems { get; set; }

        private List<SelectListItem> sites;
        public List<SelectListItem> Sites
        {
            get { return sites; }
            set { sites = value; }
        }

        public MemberFraudReviewModelView()
        {
            TopNav = new SharedTopNav();
            MemberItems = new List<MemberFraudReviewData>();
        }
    }
}