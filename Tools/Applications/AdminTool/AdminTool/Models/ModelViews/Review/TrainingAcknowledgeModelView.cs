﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.Review
{
    public class TrainingAcknowledgeModelView
    {
        public SharedTopNav SharedTopNav { get; set; }
        public bool Success { get; set; }
    }
}