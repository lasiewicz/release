﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models.ModelViews.Member;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;

namespace AdminTool.Models.ModelViews.Review
{
    public class TextApprovalReviewModelView
    {
        public ReviewLeft Left { get; set; }
        public SharedTopNav TopNav { get; set; }
        public ExpandedUserInfo ExpandedUserInfo { get; set; }
        public List<SelectListItem> AdminSuspendReasons { get; private set; }
        public List<TextAttribute> Attributes { get; set; }
        public int MemberTextApprovalId { get; set; }
        public int CommunityId { get; set; }
        public int LanguageId { get; set; }
        public int AdminMemberId { get; set; }
        public int MemberId { get; set; }
        public bool IsOnDemand { get; set; }
        public string TrainingNotes { get; set; }
        public bool ModifyTextContent { get; set; }
        public string AdminNotes { get; set; }
        public bool Suspend { get; set; }
        public int? AdminActionReasonId { get; set; }
        public int SupervisorId { get; set; }
        public bool ProcessSingleAdmin { get; set; }
        public string Title
        {
            get { return (IsOnDemand) ? "FTA OnDemand Review Queue" : "FTA Review Queue"; }
        }

        public TextApprovalReviewModelView()
        {
            Attributes = new List<TextAttribute>();
            AdminSuspendReasons = GeneralHelper.GetAdminSuspendReasons();
        }
    }

    public class TextAttribute
    {
        public int AttributeGroupId { get; set; }
        public string AttributeName { get; set; }
        public string TextContent { get; set; }
        public string AdminTextContent { get; set; }
        public int StatusMask { get; set; }
        public int SupervisorStatusMask { get; set; }
        public bool CRXApproval { get; set; }
        public string CRXNotes { get; set; }
        public int CRXResponseId { get; set; }
        public DateTime CRXReviewDate { get; set; }
        public bool CRXTie { get; set; }
        public bool ToolCorrectionRequested { get; set; }
        public int ReviewStatus { get; set; }
        public int MaxLength { get; set; }
        public string SupervisorTextContent { get; set; }
        public string OldTextContent { get; set; }
        public List<SelectListItem> IndividualTextApprovalStatuses
        {
            get
            {
                var listItems = new List<SelectListItem>();
                List<IndividualTextApprovalStatus> statuses = DBApproveQueueSA.Instance.GetIndividualTextApprovalStatuses();

                foreach (IndividualTextApprovalStatus status in statuses)
                {
                    if (status.Display)
                    {
                        listItems.Add(new SelectListItem() { Text = status.Description, Value = status.StatusMask.ToString(), Selected = (StatusMask & status.StatusMask) == status.StatusMask });
                    }
                }
                return listItems;
            }
        }

        public string TextContentHtml
        {
            get { return AddHtmlTags(TextContent, OldTextContent); }
        }
        public string AdminTextContentHtml
        {
            get { return AddHtmlTags(AdminTextContent, TextContent); }
        }

        private string AddHtmlTags(string content, string oldContent)
        {
            if (!string.IsNullOrEmpty(content))
            {
                var regex = new Regex("<(.|\n)+?>", RegexOptions.IgnoreCase);
                var attributeCheckValue = new StringBuilder(regex.Replace(content, ""));
                attributeCheckValue.Replace("<", "&lt;");
                attributeCheckValue.Replace(">", "&gt;");

                //ensure all line breaks are saved as windows "\r\n" so it shows up correctly in admin tool
                string templinebreak = "{{br}}";
                attributeCheckValue.Replace("\r\n", templinebreak);
                attributeCheckValue.Replace("\n", templinebreak);
                attributeCheckValue.Replace("\r", templinebreak);
                attributeCheckValue.Replace(templinebreak, "\r\n");

                var textManager = new TextApprovalManager();
                var text = textManager.highlightBannedSuspectWords(attributeCheckValue.ToString());
                if (!string.IsNullOrEmpty(oldContent))
                {
                    text = textManager.boldChanges(oldContent, content);
                }
               
                text = text.Replace(Environment.NewLine, "<br />");
                return text;
            }
            return "";
        }
    }
}
