﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spark.SODBQueue.ValueObjects;

namespace AdminTool.Models.ModelViews.Review
{
    public class SodbReviewCountsModelView
    {
        public List<SodbQueueCounts> SodbReviewQueueCountsBySite { get; set; }
        public List<SodbQueueCounts> SodbSecondReviewQueueCountsBySite { get; set; }
        public List<SodbQueueCounts> SodbReviewQueueCountsByLevel { get; set; }
        public List<SodbQueueCounts> SodbSecondReviewQueueCountsByLevel { get; set; }
        public SharedTopNav TopNav { get; set; }

        public SodbReviewCountsModelView()
        {
            SodbReviewQueueCountsBySite = new List<SodbQueueCounts>();
            SodbSecondReviewQueueCountsBySite = new List<SodbQueueCounts>();
            SodbReviewQueueCountsByLevel = new List<SodbQueueCounts>();
            SodbSecondReviewQueueCountsByLevel = new List<SodbQueueCounts>();
            TopNav = new SharedTopNav();
        }
    }


    public class SodbReviewStatsModelView
    {
        [Required(ErrorMessage = "*")]
        [DateValidation(ErrorMessage = "Please select a valid start date.")]
        public DateTime StartDate { get; set; }
        [Required(ErrorMessage = "*")]
        [DateValidation(ErrorMessage = "Please select a valid end date.")]
        public DateTime EndDate { get; set; }
        public int? AdminMemberId { get; set; }
        public int? SiteId { get; set; }
        public List<SodbAgentReviewCounts> SodbAgentReviewCounts { get; set; }
        public List<SelectListItem> SodbSites { get; set; }
        public SharedTopNav TopNav { get; set; }

        public SodbReviewStatsModelView()
        {
            SodbAgentReviewCounts = new List<SodbAgentReviewCounts>();
            TopNav = new SharedTopNav();
            SodbSites = ReviewManager.GetSodbSites();
        }
    }

    public enum MingleBHSiteIds
    {
        JDateCoIL = 4,
        Cupid = 15,
        Spark = 101,
        JDate = 103,
        JDateFR = 105,
        JDateCoUK = 107,
        AdventistMingle = 9011,
        AsianMingle = 9021,
        BBW = 9041,
        BlackMingle = 9051,
        CanadaMingle = 9061,
        CatholicMingle = 9071,
        ChristianMingle = 9081,
        DeafMingle = 9111,
        IndianMingle = 9131,
        InterRacialMingle = 9151,
        JewishMingle = 9171,
        LatinSinglesConnection = 9181,
        LDSMingle = 9191,
        LDSSingles = 9281,
        MilitarySinglesConnection = 9221,
        SilverSingles = 9231,
        SingleParentsMingle = 9251,
        SingleSeniorsMeet = 9231,
        UKSinglesConnection = 9271
    }
}