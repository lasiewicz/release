﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;

namespace AdminTool.Models.ModelViews.Review
{
    public class MemberProfileDuplicateCountsResult
    {
        public List<MemberProfileDuplicateHistoryCountExtended> CountsList { get; set; }
    }

    public class MemberProfileDuplicateHistoryCountExtended : MemberProfileDuplicateHistoryCount
    {
        public string EmailAddress { get; set; }
        public int PercentSuspendedMatches
        {
            get
            {
                return (int) (((float)SuspendedCount/(float)MatchesCount)*100);
            }
        }
    }
}