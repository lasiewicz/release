﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Member.ServiceAdapters;

namespace AdminTool.Models.ModelViews.Review
{
    public class AdminSampleRatesModelView
    {
        public List<AdminSampleRate> AdminSampleRates { get; set; }
        public SharedTopNav TopNav { get; set; }
        public AdminSampleRatesModelView()
        {
            AdminSampleRates = new List<AdminSampleRate>();
            TopNav = new SharedTopNav();
        }
    }

    public class AdminSampleRate
    {
        public int AdminMemberId { get; set; }
        public int DailySampleRate { get; set; }
        public int OldDailySampleRate { get; set; }
        public string AdminEmail
        {
            get
            {
                if (AdminMemberId > 0)
                {
                    Matchnet.Member.ServiceAdapters.Member admin = MemberHelper.GetMember(AdminMemberId, MemberLoadFlags.None);
                    return admin.EmailAddress;
                }
                return "";
            }
        }
    }
}