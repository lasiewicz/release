﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.Review
{
    public class TextApprovalReviewCountsModelView
    {
        public int TotalFTASamples { get; set; }
        public DateTime LatestFTASample { get; set; }
        public DateTime OldestFTASample { get; set; }
        public SharedTopNav SharedTopNav { get; set; }
    }
}