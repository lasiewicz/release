﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminTool.Models.ModelViews.Approval;

namespace AdminTool.Models.ModelViews.Review
{
    public class ReviewLeft
    {
        public int SiteId { get; set; }
        public string ApprovalStatus { get; set; }
        public string ApprovalProcessedByEmail { get; set; }
        public DateTime ApprovalDate { get; set; }
        public int QueueCount { get; set; }
        public int ItemsProcessed { get; set; }
        public List<ApprovalMemberMessage> MemberMessages { get; set; }
        public bool IsOnDemand { get; set; }
    }
}
