﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet.Member.ServiceAdapters;

namespace AdminTool.Models.ModelViews.Review
{
    public class TextApprovalReviewReportModelView
    {
        [Required(ErrorMessage = "*")]
        [DateValidation(ErrorMessage = "Please select a valid start date.")]
        public DateTime StartDate { get; set; }
        [Required(ErrorMessage = "*")]
        [DateValidation(ErrorMessage = "Please select a valid end date.")]
        public DateTime EndDate { get; set; }
        //[RegularExpression(@"\d", ErrorMessage = "Please enter a valid ID")]
        public int? AdminMemberId { get; set; }
        public int? CommunityId { get; set; }
        public List<SelectListItem> Communities { get; set; }
        public SharedTopNav TopNav { get; set; }
        public List<ReviewCountRecord> Records { get; set; }

        public TextApprovalReviewReportModelView()
        {
            TopNav = new SharedTopNav();
            Records = new List<ReviewCountRecord>();
        }
    }

    public class ReviewCountRecord
    {
        public int AdminMemberId { get; set; }
        public int TotalTextProcessedCount { get; set; }
        public int TotalTextSampledCount { get; set; }
        public int TotalTextReviewedCount { get; set; }
        public int TotalTextApprovedCount { get; set; }
        public int TotalTextCorrectedCount { get; set; }
        public int SampledProcessedRate
        {
            get { return (TotalTextProcessedCount > 0) ? (TotalTextSampledCount * 100 / TotalTextProcessedCount) : 0; }
        }
        public int ReviewedSampledRate
        {
            get { return (TotalTextSampledCount > 0) ? (TotalTextReviewedCount * 100 / TotalTextSampledCount) : 0; }
        }
        public int ApprovedReviewedRate
        {
            get { return (TotalTextReviewedCount > 0) ? (TotalTextApprovedCount * 100 / TotalTextReviewedCount) : 0; }
        }
        public int CorrectedReviewedRate
        {
            get { return (TotalTextReviewedCount > 0) ? (TotalTextCorrectedCount * 100 / TotalTextReviewedCount) : 0; }
        }

        public string AdminEmail
        {
            get
            {
                if (AdminMemberId > 0)
                {
                    Matchnet.Member.ServiceAdapters.Member admin = MemberHelper.GetMember(AdminMemberId,
                                                                                               MemberLoadFlags.None);
                    return admin.EmailAddress;
                }
                return "";
            }
        }
    }
}