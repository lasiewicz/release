﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Member.ServiceAdapters;

namespace AdminTool.Models.ModelViews.Review
{
    public class MemberFraudReviewData
    {
        public int MemberId { get; set; }
        public int SiteId { get; set; }
        public string Site { get; set; }
        public int AdminMemberId { get; set; }
        public DateTime InsertDate { get; set; }
        public bool? IsFraud { get; set; }
        public string AdminEmail
        {
            get
            {
                Matchnet.Member.ServiceAdapters.Member member = MemberHelper.GetMember(AdminMemberId, MemberLoadFlags.None);
                return member.EmailAddress;
            }
        }
    }
}