﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Member.ServiceAdapters;

namespace AdminTool.Models.ModelViews.Review
{
    public class OnDemandQueueCountsModelView
    {
        public List<OnDemandQueueCount> OnDemandQueueCounts { get; set; }
        public SharedTopNav SharedTopNav { get; set; }
        public OnDemandQueueCountsModelView()
        {
            OnDemandQueueCounts = new List<OnDemandQueueCount>();
        }
    }

    public class OnDemandQueueCount
    {
        public int AdminMemberId { get; set; }
        public int SupervisorId { get; set; }
        public int TotalItems { get; set; }
        public DateTime DateQueueAdded { get; set; }
        public DateTime OldestItem { get; set; }
        public string AdminEmail
        {
            get 
            { 
                Matchnet.Member.ServiceAdapters.Member admin = MemberHelper.GetMember(AdminMemberId, MemberLoadFlags.None);
                return (admin != null) ? admin.EmailAddress : "";
            }
        }
        public string SupervisorEmail
        {
            get
            {
                Matchnet.Member.ServiceAdapters.Member supervisor = MemberHelper.GetMember(SupervisorId, MemberLoadFlags.None);
                return (supervisor != null) ? supervisor.EmailAddress : "";
            }
        }
    }
}