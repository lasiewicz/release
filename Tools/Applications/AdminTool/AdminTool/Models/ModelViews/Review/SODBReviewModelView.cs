﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spark.SODBQueue.ValueObjects;

namespace AdminTool.Models.ModelViews.Review
{
    public enum SODBDecision
    {
        None = 0,
        PositiveMatch = 1,
        NegativeMatch = 2,
        RequestMoreInfo = 3,
        RequestPhoto = 4,
        Escalate = 5
    }

    public class SODBReviewModelView
    {
        public int MemberId { get; set; }
        public int SiteId { get; set; }
        public int SSPBlueId { get; set; }
        public int SelectedDecision { get; set; }
        public string SodbImageUrls { get; set; }
        public bool IsSecondReview { get; set; }
        public string AdminNotes { get; set; }
        public int Level { get; set; }
        public ReviewLeft Left { get; set; }
        public SharedTopNav TopNav { get; set; }
        public bool IsWaitingforSSP { get; set; }
        public bool IsBHMember { get; set; }
        public string Email { get; set; }
        public List<SodbAdminAction> AdminActions { get; set; }
        

        // our info
        public SODBMemberInfo SparkInfo { get; set; }
        
        // sodb info
        public SODBMemberInfo SODBInfo { get; set; }

        public string Title { 
        get { return (IsSecondReview) ? "SODB Second Review Queue" : "SODB Review Queue"; } 
        }
    }

    public class SODBMemberInfo
    {
        public string InfoLabel { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string ZipCode { get; set; }
        public string State { get; set; }
        public string Gender { get; set; }
        public List<string> PhotoUrls { get; set; }
        public bool ShowViewMemberLink { get; set; }
        public string ViewMemberLink { get; set; }
        public int SiteId { get; set; }
        public int MemberId { get; set; }

        public SODBMemberInfo()
        {
            PhotoUrls = new List<string>();
        }
    }

    [Serializable]
    public class SodbMembaseItem
    {
        public int SSPBlueId { get; set; }
        public int MemberId { get; set; }
        public int SiteId { get; set; }
    }
}