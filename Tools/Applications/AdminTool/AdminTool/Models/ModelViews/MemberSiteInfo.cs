﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.CommonLibrary;

namespace AdminTool.Models.ModelViews
{
    public class MemberSiteInfo : IComparable
    {
        public SiteIDs SiteID { get; set; }
        public string Name { get; set; }
        public string Domain { get; set; }
        public string Host { get; set; }
        public string HostDomain { get; set; }
        public string URL { get; set; }
        public int MemberID { get; set; }
        public bool IsSubscriber { get; set; }
        public DateTime LastLoginDate { get; set; }
        public AdminTool.Models.Enums.ProfileStatus ProfileStatus { get; set; } 

        public MemberSiteInfo()
        {
            ProfileStatus = Enums.ProfileStatus.Active;
        }

        public int CompareTo(object obj)
        {
            if (obj is MemberSiteInfo)
            {
                MemberSiteInfo t = (MemberSiteInfo)obj;
                return t.LastLoginDate.CompareTo(LastLoginDate);
            }
            else
                throw new ArgumentException("Object is not a MemberSiteInfo.");
        }
    }
}
