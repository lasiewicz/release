﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Purchase.ValueObjects;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public class PlanDisplay
    {
        public Matchnet.Purchase.ValueObjects.Plan PurchasePlan { get; set; }
        public string IncrementalPrice { get; set; }
        public int? FreeTrialDuration { get; set; }
        public Spark.Common.CatalogService.DurationType FreeTrialDurationType { get; set; }
        public string BasicCost { get; set; }
        public string SpotLightCost { get; set; }
        public string HighlightCost { get; set; }
        public string ReadReceipts { get; set; }
        public string JMeterCost { get; set; }
        public string AllAccessCost { get; set; }
        public string AllAcessEmailCost { get; set; }
        

        public string PlanTypeDescription
        {
            get
            {
                var str = "Standard";
                if (((int) PurchasePlan.PlanTypeMask & (int) PlanType.PremiumPlan) == (int) PlanType.PremiumPlan)
                {
                    str = "Premium";
                }
                else if (((int) PurchasePlan.PlanTypeMask & (int) PlanType.ALaCarte) == (int) PlanType.ALaCarte)
                {
                    str = "A La Carte";
                }
                return str;
            }
        }

        public PlanDisplay(Matchnet.Purchase.ValueObjects.Plan plan)
        {
            PurchasePlan = plan;
        }
    }
}