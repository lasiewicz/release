﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public enum MultiSelectDataType
    {
        Number,
        SingleDigitNumber
    }

    public class PromoExpressionMultiSelectTextBox : PromoExpressionBase
    {
        public string TheValue { get; set; }
        public string PropertyFriendlyName { get; set; }
        public MultiSelectDataType DataType { get; set; }

        public override List<ExpressionValue> ExpValues
        {
            get
            {
                _expValues.Clear();

                if (TheValue == null)
                {
                    return _expValues;
                }

                var splits = TheValue.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries);
                var values = new ArrayList();

                foreach(var str in splits)
                {
                    int outInt;
                    if(int.TryParse(str, out outInt))
                    {
                        values.Add(outInt);
                    }
                }

                if(values.Count > 0)
                    _expValues.Add(new ExpressionValue(values, Matchnet.PromoEngine.ValueObjects.OperatorType.Equal));

                return _expValues;
            }
            set
            {
                _expValues = value;

                if (_expValues == null || _expValues.Count == 0)
                    return;
                
                var sb = new StringBuilder();
                for (int i = 0; i < _expValues[0].Values.Count; i++)
                {
                    if (i != 0)
                        sb.Append(",");

                    sb.Append(_expValues[0].Values[i].ToString());
                }

                TheValue = sb.ToString();
            }
        }

       
    }
}