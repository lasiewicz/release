﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet.Purchase.ValueObjects;

namespace AdminTool.Models.ModelViews.SubAdmin
{


    public class PromoExpressionCheckBoxList : PromoExpressionSelectListBase
    {
        public int[] TheValue { get; set; }
        public string PropertyName { get; set; }

        public bool ValueContains(string intValue)
        {
            if (TheValue == null)
                return false;

            return TheValue.Contains(int.Parse(intValue));
        }
        
        public override List<ExpressionValue> ExpValues
        {
            get
            {
                _expValues.Clear();

                if(TheValue == null)
                {
                    return _expValues;
                }

                foreach(var oneValue in TheValue)
                {
                    if(_expValues.Count == 0)
                    {
                        _expValues.Add(new ExpressionValue(oneValue, Matchnet.PromoEngine.ValueObjects.OperatorType.Equal));
                    }
                    else
                    {
                        _expValues[0].Values.Add(oneValue);
                    }
                }

                return _expValues;

            }
            set
            {
                _expValues = value;

                if(_expValues != null && _expValues.Count > 0)
                {
                    var tempArray = (int[])_expValues[0].Values.ToArray(typeof(int));
                    TheValue = (from int item in tempArray
                                select item).ToArray();
                }
            }
        }

        public override void PopulateSelectListItems()
        {
            if(ExpPromoAttributeId == 1000)
            {
                PossibleValues = GeneralHelper.GetOptionsFromEnum(typeof(SubscriptionStatus));    
            }
            else if(ExpPromoAttributeId == 69)
            {
                PossibleValues = GeneralHelper.GetOptionsFromEnum(typeof (GenderSeekingGender));
            }
            else if (ExpPromoAttributeId == 1012)
            {
                PossibleValues = GeneralHelper.GetOptionsFromEnumExcludeZeroOrLess(typeof(Matchnet.Member.ValueObjects.Enumerations.DeviceOS));
            }
            else
            {
                PossibleValues = base.GetAttributeOptions();
            }
        }
    }
}