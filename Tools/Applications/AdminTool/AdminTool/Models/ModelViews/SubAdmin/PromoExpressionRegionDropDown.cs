﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.PromoEngine.ValueObjects;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public enum RegionDepthType
    {
        Country,
        State,
        City
    };

    public class PromoExpressionRegionDropDown : PromoExpressionSelectListBase
    {
        private string _cssClassName = string.Empty;

        public int TheValue { get; set; }
        public RegionDepthType RegionDepth { get; set; }
        public int ParentRegionId { get; set; }
        public bool AddBlankSelection { get; set; }

        public string CssClassName
        {
            get { return _cssClassName + " " + "dropdown-autobind"; }
            set { _cssClassName = value; }
        }
        
        public int LanguageId
        {
            get
            {
                int ret = Constants.NULL_INT;
                if (BrandId != Constants.NULL_INT && BrandId != 0)
                    ret = BrandConfigSA.Instance.GetBrandByID(BrandId).Site.LanguageID;

                return ret;     
            }
        }

        public override List<ExpressionValue> ExpValues
        {
            get
            {
                _expValues.Clear();
                if(TheValue == Constants.NULL_INT)
                {
                    return _expValues;
                }

                _expValues.Add(new ExpressionValue(TheValue, OperatorType.Equal));

                return _expValues;
            }
            set
            {
                _expValues = value;
                if (_expValues != null && _expValues.Count > 0)
                {
                    TheValue = (int)_expValues[0].Values[0];
                }
            }
        }

        public override void PopulateSelectListItems()
        {
            if(RegionDepth == RegionDepthType.Country)
            {
                var countries = RegionSA.Instance.RetrieveCountries(LanguageId);
                if(countries != null)
                {
                    PossibleValues = (from Region r in countries
                                      select new SelectListItem() { Text = r.Description, Value = r.RegionID.ToString() }).ToList();
                }
            }
            else
            {
                //for state and city, we have parent region ID
                if (ParentRegionId == Constants.NULL_INT)
                {
                    PossibleValues = new List<SelectListItem>();
                    return;
                }

                var childRegions = RegionSA.Instance.RetrieveChildRegions(ParentRegionId, LanguageId);
                if (childRegions != null)
                {
                    PossibleValues = (from Region r in childRegions
                                      select new SelectListItem() { Text = r.Description, Value = r.RegionID.ToString() }).ToList();    
                }
            }

            if(PossibleValues == null)
                PossibleValues = new List<SelectListItem>();

            if(AddBlankSelection)
                PossibleValues.Insert(0, new SelectListItem() { Text = "", Value = Constants.NULL_INT.ToString()});
            
        }
    }
}