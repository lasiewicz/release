﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Spark.Common.DiscountService;
using Matchnet;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public class GiftDetailsModelView:  ModelViewBase
    {
        public Gift Gift { get; private set; }
        public List<GiftNotificationExtended> Notifications { get; private set; }
        public bool EnableSendNotifications { get; private set; }
        public bool EnableStatusToggle { get; private set; }

        public GiftDetailsModelView(Gift gift)
        {
            Gift = gift;
            if(gift.Notifications != null && gift.Notifications.Count > 0)
            {
                Notifications = new List<GiftNotificationExtended>();
                foreach(GiftNotification notification in gift.Notifications)
                {
                    Notifications.Add(new GiftNotificationExtended(notification));
                }
            }

            EnableSendNotifications = false;
            EnableStatusToggle = false;

            if(gift.Status != GiftStatus.Cancelled && gift.Status != GiftStatus.Redeemed && gift.ExpirationDate > DateTime.Now && gift.IsValid)
            {
                //gift is still good, enable send notifications
                EnableSendNotifications = true;
            }

            if(gift.Status != GiftStatus.Redeemed && gift.ExpirationDate > DateTime.Now && gift.IsValid)
            {
                EnableStatusToggle = true;
            }


        }
    }
}