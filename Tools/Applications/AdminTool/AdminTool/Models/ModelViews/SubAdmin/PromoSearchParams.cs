﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Configuration.ServiceAdapters;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public class PromoSearchParams : ModelViewBase
    {
        public List<SelectListItem> BrandIds;
        public List<SelectListItem> PaymentTypes;
        public List<SelectListItem> PromoTypes;
        public List<SelectListItem> PromoApprovalStatuses;
        public string PromoId { get; set; }
        public string PromoName { get; set; }
        public int BrandId { get; set; }
        public int PaymentType { get; set; }
        public int PromoType { get; set; }
        public int PromoApprovalStatus { get; set; }

        public PromoSearchParams()
        {
            BindSelectListItems();
        }

        private void BindSelectListItems()
        {
            GetBrands();
            GetPaymentTypes();
            GetPromoTypes();
            GetPromoApprovalStatuses();
        }

        private void GetPromoApprovalStatuses()
        {
            PromoApprovalStatuses = new List<SelectListItem>
                                        {
                                            new SelectListItem() { Text="All", Value="0"},
                                            new SelectListItem() { Text="Pending", Value="1"},
                                            new SelectListItem() { Text="Approved", Value="2"},
                                            new SelectListItem() { Text="Rejected", Value="3"}
                                        };
        }

        private void GetBrands()
        {
            BrandIds = GeneralHelper.GetActiveBrands();
        }

        private void GetPaymentTypes()
        {
            PaymentTypes = new List<SelectListItem>
                               {
                                   new SelectListItem() {Text="Credit", Value="1"},
                                   new SelectListItem() { Text = "Check", Value = "2" }
                               };
            
        }

        private void GetPromoTypes()
        {
            PromoTypes = new List<SelectListItem>
                             {
                                 new SelectListItem() {Text = "Member", Value = "0"},
                                 new SelectListItem() {Text = "Admin Only", Value = "1"},
                                 new SelectListItem() {Text = "Email / Link", Value = "2"},
                                 new SelectListItem() {Text = "Email with Targeting", Value = "3"},
                                 new SelectListItem() {Text = "Mobile", Value = "4"},
                                 new SelectListItem() {Text = "Supervisor Only", Value = "5"}
                             };

        }

    }
}
