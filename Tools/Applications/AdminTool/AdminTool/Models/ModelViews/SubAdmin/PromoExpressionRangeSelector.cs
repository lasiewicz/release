﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.PromoEngine.ValueObjects;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public enum RangeSelectorBehavior
    {
        Default,
        TextBoxPositionReversed,
        LessThanEqualOnly,
        TextBoxPositionAndSignReversed
    }

    public class PromoExpressionRangeSelector : PromoExpressionBase
    {
        public string PropertyFriendlyName { get; set; }
        public string FromLabel { get; set; }
        public string ToLabel { get; set; }
        public string FromValue { get; set; }
        public string ToValue { get; set; }
        public RangeSelectorBehavior Behavior { get; set; }

        public override List<ExpressionValue> ExpValues
        {
            get
            {
                _expValues.Clear();
                int outInt;
                switch(Behavior)
                {
                    case RangeSelectorBehavior.TextBoxPositionReversed:
                    // if reversed, we have to flip the operator as well as the number sign on the "To" box value
                        if(int.TryParse(FromValue, out outInt))
                            _expValues.Add(new ExpressionValue(outInt, OperatorType.LessThanEqual));

                        if(int.TryParse(ToValue, out outInt))
                            _expValues.Add(new ExpressionValue(-outInt, OperatorType.GreaterThanEqual));
                        break;
                    case RangeSelectorBehavior.LessThanEqualOnly:
                        if(int.TryParse(FromValue, out outInt))
                            _expValues.Add(new ExpressionValue(outInt, OperatorType.LessThanEqual));

                        if(int.TryParse(ToValue, out outInt))
                            _expValues.Add(new ExpressionValue(-outInt, OperatorType.LessThanEqual));
                        break;
                    case RangeSelectorBehavior.TextBoxPositionAndSignReversed:
                        if(int.TryParse(FromValue, out outInt))
                            _expValues.Add(new ExpressionValue(-outInt, OperatorType.LessThanEqual));

                        if(int.TryParse(ToValue, out outInt))
                            _expValues.Add(new ExpressionValue(-outInt, OperatorType.GreaterThanEqual));
                        break;
                    case RangeSelectorBehavior.Default:
                          if(int.TryParse(FromValue, out outInt))
                              _expValues.Add(new ExpressionValue(outInt, OperatorType.GreaterThanEqual));

                        if(int.TryParse(ToValue, out outInt))
                            _expValues.Add(new ExpressionValue(outInt, OperatorType.LessThanEqual));
                        break;
                }

                return _expValues;
            }
            set
            {
                _expValues = value;
                if (_expValues == null || _expValues.Count <= 0)
                    return;

                foreach (var aExpValue in _expValues)
                {
                    switch (Behavior)
                    {
                        case RangeSelectorBehavior.TextBoxPositionReversed:
                            switch (aExpValue.ValueOperator)
                            {
                                case OperatorType.GreaterThanEqual:
                                    ToValue = (-(int)aExpValue.Values[0]).ToString();
                                    break;
                                case OperatorType.LessThanEqual:
                                    FromValue = aExpValue.Values[0].ToString();
                                    break;
                            }                  
                            break;
                        case RangeSelectorBehavior.LessThanEqualOnly:
                            if (aExpValue.Values.Count > 0)
                            {
                                if ((int)aExpValue.Values[0] < 0)
                                    ToValue = (-(int)aExpValue.Values[0]).ToString();
                                else
                                    FromValue = aExpValue.Values[0].ToString();
                            }
                            break;
                        case RangeSelectorBehavior.TextBoxPositionAndSignReversed:
                            switch (aExpValue.ValueOperator)
                            {
                                case OperatorType.GreaterThanEqual:
                                    ToValue = (-(int)aExpValue.Values[0]).ToString();
                                    break;
                                case OperatorType.LessThanEqual:
                                    FromValue = (-(int)aExpValue.Values[0]).ToString();
                                    break;
                            }   
                            break;
                        case RangeSelectorBehavior.Default:
                            switch (aExpValue.ValueOperator)
                            {
                                case OperatorType.GreaterThanEqual:
                                    FromValue = aExpValue.Values[0].ToString();
                                    break;
                                case OperatorType.LessThanEqual:
                                    ToValue = aExpValue.Values[0].ToString();
                                    break;
                            }
                            break;
                    }
                }
            }
        }
    }
}