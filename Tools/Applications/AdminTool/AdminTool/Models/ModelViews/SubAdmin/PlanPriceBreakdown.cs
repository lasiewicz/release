﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public class PlanPriceBreakdown : ModelViewBase
    {
        public string LabelText { get; set; }
        public int PremiumPlanType { get; set; }

        [RegularExpression(@"^\d+(\.\d{2})?$", ErrorMessage = "Initial cost for one of price breakdowns is in incorrect format")]
        public decimal? InitialCost { get; set; }

        [RegularExpression(@"^\d+(\.\d{2})?$", ErrorMessage = "Renewal cost for one of price breakdowns is in incorrect format")]
        public decimal? RenewalCost { get; set; }

        public int? Count { get; set; }
        public bool IsDisburseCount { get; set; }
        public bool EnabledDisburseCount { get; set; }
    }
}