﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet;
using Matchnet.PromoEngine.ValueObjects;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public class PromoExpressionDropDown : PromoExpressionSelectListBase
    {
        public int TheValue { get; set; }

        public override List<ExpressionValue> ExpValues
        {
            get
            {
                _expValues.Clear();

                if (TheValue == Constants.NULL_INT)
                {
                    return _expValues;
                }
                
                _expValues.Add(new ExpressionValue(TheValue, OperatorType.Equal));
                return _expValues;
            }
            set
            {
                _expValues = value;
                if(_expValues.Count > 0)
                    TheValue = (int)(((ExpressionValue)_expValues[0]).Values[0]);
            }
        }

        public override void PopulateSelectListItems()
        {
            if(ExpPromoAttributeId == 1007) // DMA codes
            {
                PossibleValues = base.GetAllDMACodes();
            }
            else
            {
                PossibleValues = base.GetAttributeOptions();
            }
        }
    }
}