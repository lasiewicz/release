﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public class PlanDetailedDisplay
    {
        
        /// <summary>
        /// the price of the plan
        /// </summary>
        public string BasicCost { get; set; }

        /// <summary>
        /// is the plan premium or standard or etc
        /// </summary>
        public string PlanTypeDescription {get; set;}

        /// <summary>
        /// the cost to renew the plan.
        /// </summary>
        public decimal RenewCost { get; set; }
        
        /// <summary>
        /// e.g 1 month or 3 weeks etc
        /// </summary>
        public string RenewDuration { get; set; }
        
        /// <summary>
        /// e.g 1 month or 1 week etc
        /// </summary>
        public string InitialDuration { get; set; }
        
        /// <summary>
        /// e.g Basic with spotlight or high light etc
        /// </summary>
        public string PlanType { get; set; }

        
    }
}