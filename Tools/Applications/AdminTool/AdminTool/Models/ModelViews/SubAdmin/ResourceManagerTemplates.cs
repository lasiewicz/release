﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public class ResourceManagerTemplates
    {
        public SharedTopNav TopNav { get; set; }
        public List<SelectListItem> Brands { get; set; }
        public List<SelectListItem> TemplateTypes { get; set; }
        public int BrandId { get; set; }
        public int TemplateType { get; set; }

        public ResourceManagerTemplates()
        {
            TopNav = new SharedTopNav();
            Brands = new List<SelectListItem>();
            TemplateTypes = new List<SelectListItem>();
            BrandId = 1003;
        }
    }
}