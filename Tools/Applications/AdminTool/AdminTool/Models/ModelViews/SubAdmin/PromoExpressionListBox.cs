﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Matchnet;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public class PromoExpressionListBox : PromoExpressionSelectListBase
    {
        public int[] TheValue { get; set; }
        public string TheValueInString
        {
            get
            {
                if(TheValue == null)
                    return string.Empty;

                var sb = new StringBuilder();

                for(int i=0; i < TheValue.Length; i++)
                {
                    if (i != 0)
                        sb.Append(",");

                    sb.Append(TheValue[i].ToString());
                }

                return sb.ToString();
            }
        }

        public override List<ExpressionValue> ExpValues
        {
            get
            {
                _expValues.Clear();

                var selectedValues = GetSelectedValues();
                if (selectedValues != null && selectedValues.Count > 0)
                    _expValues.Add(new ExpressionValue(selectedValues, Matchnet.PromoEngine.ValueObjects.OperatorType.Equal));
                
                return _expValues;
            }
            set
            {
                _expValues = value;
                if (_expValues == null || _expValues.Count == 0)
                    return;

                TheValue = new int[_expValues[0].Values.Count];
                int i = 0;
                foreach(var intVal in _expValues[0].Values)
                {
                    TheValue[i++] = (int)intVal;
                }
            }
        }

        public override void PopulateSelectListItems()
        {
            PossibleValues = ExpPromoAttributeId == 1007 ? base.GetAllDMACodes() : base.GetAttributeOptions();
        }

        private ArrayList GetSelectedValues()
        {
            if(TheValue == null)
                return null;

            var selectedValues = new ArrayList();
            foreach (var sv in TheValue)
            {
                if(sv != Constants.NULL_INT)
                    selectedValues.Add(sv);
            }

            return selectedValues;
        }
    }
}