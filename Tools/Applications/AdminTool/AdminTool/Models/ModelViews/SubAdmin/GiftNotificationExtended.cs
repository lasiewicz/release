﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.Common.DiscountService;
using Matchnet;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public class GiftNotificationExtended
    {
        public string SentTo { get; set; }
        public GiftNotificationType NotificationType { get; set; }
        public DateTime SentDate { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public GiftNotificationStatus NotificationStatus { get; set; }
        public string AdminEmailAddress { get; set; }

        public GiftNotificationExtended(GiftNotification notification)
        {
            SentTo = notification.SentTo;
            NotificationType = notification.NotificationType;
            SentDate = notification.SentDate;
            CreateDate = notification.CreateDate;
            UpdateDate = notification.UpdateDate;
            NotificationStatus = notification.NotificationStatus;
            if(notification.AdminID != Constants.NULL_INT)
            {
                Matchnet.Member.ServiceAdapters.Member admin = MemberHelper.GetMember(notification.AdminID, MemberLoadFlags.None);
                if(admin != null)
                {
                    AdminEmailAddress = admin.EmailAddress;
                }
            }
        }
    }
}