﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using FluentValidation;

namespace AdminTool.Models.ModelViews.SubAdmin.Validators
{
    public class PEMultiSelectTextBoxValidator : AbstractValidator<PromoExpressionMultiSelectTextBox>
    {
        public PEMultiSelectTextBoxValidator()
        {
            RuleFor(m => m.TheValue).Matches(@"^\d+(,\d+)*$").When(m => m.DataType == MultiSelectDataType.Number).WithMessage("{0} is invalid", m => m.PropertyFriendlyName);
            RuleFor(m => m.TheValue).Matches(@"^\d(,\d)*$").When(m => m.DataType == MultiSelectDataType.SingleDigitNumber).WithMessage("{0} is invalid", m => m.PropertyFriendlyName);
        }
    }
}