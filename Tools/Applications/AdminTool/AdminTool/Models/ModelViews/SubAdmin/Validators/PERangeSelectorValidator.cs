﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;

namespace AdminTool.Models.ModelViews.SubAdmin.Validators
{
    public class PERangeSelectorValidator : AbstractValidator<PromoExpressionRangeSelector>
    {
        public PERangeSelectorValidator()
        {
            RuleFor(m => m.FromValue).Matches(@"^-?\d+$").WithMessage("From value for {0} is invalid", m => m.PropertyFriendlyName); ;
            RuleFor(m => m.ToValue).Matches(@"^-?\d+$").WithMessage("To value {0} is invalid", m => m.PropertyFriendlyName); ;
        }
    }
}