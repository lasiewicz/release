﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.Purchase.ValueObjects;

namespace AdminTool.Models.ModelViews.SubAdmin.Validators
{
    public class PromoValidator : AbstractValidator<Promo>
    {
        public PromoValidator()
        {
            // step 1
            RuleFor(m => m.Description).NotNull().When(m => m.Step == 1).WithMessage("Promo name is required");
            RuleFor(m => m.PaymentType).NotEqual(PaymentType.Check).When(m => m.PromoType != PromoType.Member).When(
                m => m.Step == 1).WithMessage("Check payment type is only for Member promo");

            // step 2 has no validation

            // step 3 - at least one plan selected and no repeated plan
            RuleFor(m => m.PlanSelectors).Must(
                (promo, planselector) => planselector.Exists(p => p.PlanId != Matchnet.Constants.NULL_INT)).When(
                    m => m.Step == 3).WithMessage("At least one plan must be selected");

            RuleFor(m => m.RepeatedPlans).Equal(string.Empty).When(m => m.Step == 3).WithMessage(
                "Same plan cannot be selected multiple times - {0}", m => m.RepeatedPlans);

            // step 4 - subcription page title is required
            RuleFor(m => m.PageTitle).NotNull().When(m => m.Step == 4).WithMessage("Subscription page title is required");
            RuleFor(m => m.SubscriptionStatus.TheValue).NotNull().When(
                m =>
                m.Step == 4 &&
                (m.PromoType == PromoType.Member || m.PromoType == PromoType.EmailLinkWithTargeting ||
                 m.PromoType == PromoType.Mobile)).WithMessage(
                     "At least one subscription status must be specified");

            RuleFor(m => m.RecurringPattern).NotNull().When(m => m.Step == 4).When(m => m.IsRecurring).WithMessage(
                "Recurrence pattern is required");
            RuleFor(m => m.RecurringDuration).NotNull().When(m => m.Step == 4).When(m => m.IsRecurring).WithMessage(
                "Recurrence duration is required");

            RuleFor(m => m.RecurringDayOfMonth).NotNull().When(m => m.Step == 4).When(
                m => m.RecurringPattern == "Monthly").WithMessage("Recurrence day of month is required");

            // step 4 - we have to make sure the promo end date plus time combo is in the feature
            RuleFor(m => m.PromoEndDateTime).GreaterThan(DateTime.Now).When(m => m.Step == 4).WithMessage(
                "Promo end date time must ben in the future");
        }
    }
}