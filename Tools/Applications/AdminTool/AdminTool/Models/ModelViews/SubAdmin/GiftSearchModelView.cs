﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public class GiftSearchModelView : ModelViewBase
    {
        public SharedTopNav TopNav { get; set; }
        public string GiftCode { get; set; }
        
        public GiftSearchModelView(SharedTopNav topNav)
        {
            TopNav = topNav;
        }

        public GiftSearchModelView(SharedTopNav topNav, string giftCode)
        {
            TopNav = topNav;
            GiftCode = giftCode;
        }
    }
}