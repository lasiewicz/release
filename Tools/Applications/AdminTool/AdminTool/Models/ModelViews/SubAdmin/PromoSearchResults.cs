﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PromoEngine = Matchnet.PromoEngine.ValueObjects;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public class PromoSearchResults : ModelViewBase
    {
        public PromoSearchResults()
        {
        }

        public List<PromoEngine.Promo> FoundPromos;
        public int BrandId { get; set; }
    }
}