﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.PromoEngine.ValueObjects;
using System.Collections;
using Matchnet;
using Matchnet.PromoEngine.ServiceAdapters;
using System.Web.Mvc;
using Matchnet.Content.ValueObjects.AttributeOption;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public class ExpressionValue
    {
        public ArrayList Values { get; set; }
        public OperatorType ValueOperator { get; set; }

        public ExpressionValue(int value, OperatorType valueOperator)
        {
            Values = new ArrayList();
            Values.Add(value);
            ValueOperator = valueOperator;
        }

        public ExpressionValue(ArrayList values, OperatorType valueOperator)
        {
            Values = values;
            ValueOperator = valueOperator;
        }
    }

    public abstract class PromoExpressionBase : ModelViewBase
    {
        protected List<ExpressionValue> _expValues = new List<ExpressionValue>();

        public virtual int ExpPromoAttributeId { get; set; }
        public virtual string ExpPromoAttributeName { get; set; }
        
        public abstract List<ExpressionValue> ExpValues  { get; set; }
        
        public List<Expression>  GetPromoExpression()
        {
            var expressions = new List<Expression>();
            Expression exp = null;

            if (ExpPromoAttributeId != Constants.NULL_INT && ExpValues != null && ExpValues.Count > 0)
            {
                var promoAttribute = PromoEngineSA.Instance.GetPromoAttributes().FindByID(ExpPromoAttributeId);
                if(promoAttribute != null)
                {
                    foreach (ExpressionValue aExpValue in ExpValues)
                    {
                        exp = new Expression(promoAttribute, aExpValue.ValueOperator, aExpValue.Values);
                        expressions.Add(exp);
                    }     
                }
            }

            return expressions;
        }
    }
}