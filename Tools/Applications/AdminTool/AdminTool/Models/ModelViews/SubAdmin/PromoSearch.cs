﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.PromoEngine.ValueObjects;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public class PromoSearch : ModelViewBase
    {
        public string ErrorMessage { get; set; }
        public bool AutoHideSearchParms { get; set; }
        public PromoSearchParams MPromoSearchParams = new PromoSearchParams();
        public PromoSearchResults MPromoSearchResults = new PromoSearchResults();

        public SharedTopNav TopNav { get; set; }

        public PromoSearch() : base()
        {
            TopNav = new SharedTopNav();
        }
    }
}