﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Matchnet.Content.ServiceAdapters;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.Purchase.ValueObjects;
using PromoEngine = Matchnet.PromoEngine.ValueObjects;
using System.Web.Mvc;
using Matchnet;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public class RowHeader
    {
        public int HeaderNumber { get; set; }
        public int Duration { get; set; }
        public int DurationType { get; set; }
        public int ResourceTemplateId { get; set; }
        public List<SelectListItem> DurationChoices { get; set; }
        public List<SelectListItem> DurationTypeChoices { get; set; }
        public List<SelectListItem> ResourceTemplateChoices { get; set; }
    }

    public class ColHeader
    {
        private int _planType = Constants.NULL_INT;
        private int _premiumType = Constants.NULL_INT;

        public int HeaderNumber { get; set; }

        public int PlanType
        {
            get { return IsMixedPlanType ? 0 : _planType; }
            set { _planType = value; }
        }

        public int PremiumType
        {
            get { return IsMixedPlanType ? 0 : _premiumType; }
            set { _premiumType = value; }
        }

        public int ResourceTemplateId { get; set; }
        public bool IsMixedPlanType { get; set; }
        public List<SelectListItem> PlanTypeChoices { get; set; }
        public List<SelectListItem> PremiumTypeChoices { get; set; }
        public List<SelectListItem> ResourceTemplateChoices { get; set; }
    }

    public class PlanSelector
    {
        public string Prefix
        {
            get { return (ColumnNumber*100 + RowNumber).ToString(); }
        }

        public int ColumnNumber { get; set; }
        public int RowNumber { get; set; }
        public int PlanId { get; set; }
        public int ResourceTemplateId { get; set; }
        public List<SelectListItem> PlanChoices { get; set; }
        public List<SelectListItem> ResourceTemplateChoices { get; set; }
        public int BrandID { get; set; }
    }

    public class PromoDetailLineItem
    {
        public string LabelText { get; set; }
        public string LineItemText { get; set; }
        public bool IsLiteral { get; set; }
    }

    public class Promo : ModelViewBase
    {
        public SharedTopNav TopNav { get; set; }

        public int Step { get; set; }
        public string Description { get; set; }
        public PaymentType PaymentType { get; set; }
        public PromoType PromoType { get; set; }
        public int BrandId { get; set; }
        public int RowDimension { get; set; }
        public int ColDimension { get; set; }
        public bool IsSingleColWithMultPlanTypes { get; set; }
        public List<RowHeader> RowHeaders { get; set; }
        public List<ColHeader> ColHeaders { get; set; }
        public List<PlanSelector> PlanSelectors { get; set; }
        public int DefaultPlanId { get; set; }
        public string PromoStartDate { get; set; }
        public int PromoStartTime { get; set; }
        public string PromoEndDate { get; set; }
        public int PromoEndTime { get; set; }

        public bool IsRecurring { get; set; }
        public string RecurringPattern { get; set; }
        public int RecurringStartHour { get; set; }
        public int? RecurringDuration { get; set; }
        public string[] ReccuringDayOfWeek { get; set; }
        public int? RecurringDayOfMonth { get; set; }

        public string PageTitle { get; set; }
        public string LegalNote { get; set; }
        public int PageTemplateId { get; set; }
        public PromoApprovalStatus PromoApprovalStatus { get; set; }
        public int PromoId { get; set; }
        public string RecurrenceCronExpression { get; set; }
        public int AdminMemberId { get; set; }

        public List<PromoDetailLineItem> PromoDetailLineItems { get; set; }

        #region Dropdown choices

        public List<SelectListItem> DefaultPlanChoices { get; set; }
        public List<SelectListItem> BrandIdChoices { get; set; }
        public List<SelectListItem> RowChoices { get; set; }
        public List<SelectListItem> ColChoices { get; set; }
        public List<SelectListItem> PaymentTypeChoices { get; set; }
        public List<SelectListItem> PromoTypeChoices { get; set; }
        public List<SelectListItem> PromoTimeChoices { get; set; }
        public List<SelectListItem> DayOfTheWeekChoices { get; set; }
        public List<SelectListItem> PageTemplateChoices { get; set; }

        #endregion

        #region Promo Expressions

        public PromoExpressionRegionDropDown Country { get; set; }
        public PromoExpressionRegionDropDown State { get; set; }
        public PromoExpressionRegionDropDown City { get; set; }
        public PromoExpressionRangeSelector DaysTillTermination { get; set; }
        public PromoExpressionRangeSelector DaysSinceRegistration { get; set; }
        public PromoExpressionRangeSelector Age { get; set; }
        public PromoExpressionRangeSelector DaysSinceBirthday { get; set; }
        public PromoExpressionMultiSelectTextBox MemberIdLastDigit { get; set; }
        public PromoExpressionMultiSelectTextBox YearOfBirth { get; set; }
        public PromoExpressionListBox DMAs { get; set; }
        public PromoExpressionCheckBoxList SubscriptionStatus { get; set; }
        public PromoExpressionCheckBoxList GenderSeekingGender { get; set; }
        public PromoExpressionDropDown MaritalStatus { get; set; }
        public PromoExpressionDropDown Education { get; set; }
        public PromoExpressionDropDown Profession { get; set; }
        public PromoExpressionMultiSelectTextBox PRM { get; set; }
        public PromoExpressionMultiSelectTextBox PriceTargetID { get; set; }
        public PromoExpressionCheckBoxList DeviceOS { get; set; }

        #endregion

        public int SiteId
        {
            get
            {
                var brand = BrandConfigSA.Instance.GetBrandByID(BrandId);
                return brand.Site.SiteID;
            }
        }

        public string RepeatedPlans
        {
            get
            {
                if (PlanSelectors == null)
                    return string.Empty;

                var result = (from ps in PlanSelectors
                              group ps by ps.PlanId
                              into g
                              where g.Count() > 1
                              select new {PlanId = g.Key, PlanCount = g.Count()}).ToList();

                if (result.Count == 0)
                {
                    return string.Empty;
                }
                else
                {
                    var sb = new StringBuilder();
                    foreach (var r in result)
                    {
                        // bypass the "none" plan selection
                        if(r.PlanId == Matchnet.Constants.NULL_INT)
                            continue;

                        sb.Append((sb.Length > 0 ? "," : string.Empty) + r.PlanId);
                    }
                    return sb.ToString();
                }
            }
        }

        public DateTime PromoEndDateTime
        {
            get
            {
                if(!string.IsNullOrEmpty(PromoEndDate))
                {
                    return GeneralHelper.ConvertDateTimeString(PromoEndDate).AddHours(PromoEndTime);
                }

                return DateTime.MinValue;
            }
        }

        public Promo() : base()
        {
            TopNav = new SharedTopNav();

            #region Promo expressions

            Country = new PromoExpressionRegionDropDown()
                          {
                              ExpPromoAttributeId = 1004,
                              ExpPromoAttributeName = "none",
                              RegionDepth = RegionDepthType.Country,
                              CssClassName = "create-promo-country",
                              AddBlankSelection = true,
                              TheValue = Constants.NULL_INT
                          };

            State = new PromoExpressionRegionDropDown()
                        {
                            ExpPromoAttributeId = 1005,
                            ExpPromoAttributeName = "none",
                            RegionDepth = RegionDepthType.State,
                            ParentRegionId = Constants.NULL_INT,
                            CssClassName = "create-promo-state",
                            AddBlankSelection = true,
                            TheValue = Constants.NULL_INT
                        };

            City = new PromoExpressionRegionDropDown()
                       {
                           ExpPromoAttributeId = 1006,
                           ExpPromoAttributeName = "none",
                           RegionDepth = RegionDepthType.City,
                           ParentRegionId = Constants.NULL_INT,
                           CssClassName = "create-promo-city",
                           AddBlankSelection = true,
                           TheValue = Constants.NULL_INT
                       };

            DaysTillTermination = new PromoExpressionRangeSelector()
                                      {
                                          ExpPromoAttributeId = 1001,
                                          ExpPromoAttributeName = "none",
                                          Behavior = RangeSelectorBehavior.TextBoxPositionAndSignReversed,
                                          FromLabel = "At least ",
                                          ToLabel = "but not more than ",
                                          PropertyFriendlyName = "Days SINCE renewal / termination"
                                      };
            DaysSinceRegistration = new PromoExpressionRangeSelector()
                                        {
                                            ExpPromoAttributeId = 1002,
                                            ExpPromoAttributeName = "none",
                                            Behavior = RangeSelectorBehavior.Default,
                                            FromLabel = " Great than or equal to ",
                                            ToLabel = " Equal to or less than ",
                                            PropertyFriendlyName = "Days since registration"
                                        };
            Age = new PromoExpressionRangeSelector()
                      {
                          ExpPromoAttributeId = 1003,
                          ExpPromoAttributeName = "none",
                          Behavior = RangeSelectorBehavior.Default,
                          FromLabel = " Between ",
                          ToLabel = " and ",
                          PropertyFriendlyName = "Age"
                      };

            DaysSinceBirthday = new PromoExpressionRangeSelector()
                                    {
                                        ExpPromoAttributeId = 1008,
                                        ExpPromoAttributeName = "none",
                                        Behavior = RangeSelectorBehavior.TextBoxPositionReversed,
                                        FromLabel = " Days until ",
                                        ToLabel = " since ",
                                        PropertyFriendlyName = "Birthday"
                                    };

            MemberIdLastDigit = new PromoExpressionMultiSelectTextBox()
                                    {
                                        ExpPromoAttributeId = 1009,
                                        ExpPromoAttributeName = "none",
                                        PropertyFriendlyName = "Last digit of memberID",
                                        DataType = MultiSelectDataType.SingleDigitNumber
                                    };

            YearOfBirth = new PromoExpressionMultiSelectTextBox()
                              {
                                  ExpPromoAttributeId = 1010,
                                  ExpPromoAttributeName = "none",
                                  PropertyFriendlyName = "Year of birth",
                                  DataType = MultiSelectDataType.Number
                              };
            PRM = new PromoExpressionMultiSelectTextBox
                      {
                          ExpPromoAttributeId = 79,
                          ExpPromoAttributeName = "none",
                          PropertyFriendlyName = "PRM",
                          DataType = MultiSelectDataType.Number
                      };

            PriceTargetID = new PromoExpressionMultiSelectTextBox
                                {
                                    ExpPromoAttributeId = 744,
                                    ExpPromoAttributeName = "none",
                                    PropertyFriendlyName = "Price Target ID",
                                    DataType = MultiSelectDataType.Number
                                };

            DMAs = new PromoExpressionListBox() {ExpPromoAttributeId = 1007, ExpPromoAttributeName = "none"};

            SubscriptionStatus = new PromoExpressionCheckBoxList()
                                     {
                                         ExpPromoAttributeId = 1000,
                                         ExpPromoAttributeName = "none",
                                         PropertyName = "SubscriptionStatus"
                                     };

            GenderSeekingGender = new PromoExpressionCheckBoxList()
                                      {
                                          ExpPromoAttributeId = 69,
                                          ExpPromoAttributeName = "none",
                                          PropertyName = "GenderSeekingGender"
                                      };

            MaritalStatus = new PromoExpressionDropDown()
                                {
                                    ExpPromoAttributeId = 32,
                                    ExpPromoAttributeName = "MaritalStatus",
                                    TheValue = Constants.NULL_INT
                                };
            Education = new PromoExpressionDropDown()
                            {
                                ExpPromoAttributeId = 89,
                                ExpPromoAttributeName = "EducationLevel",
                                TheValue = Constants.NULL_INT
                            };
            Profession = new PromoExpressionDropDown()
                             {
                                 ExpPromoAttributeId = 392,
                                 ExpPromoAttributeName = "IndustryType",
                                 TheValue = Constants.NULL_INT
                             };

            DeviceOS = new PromoExpressionCheckBoxList()
                {
                    ExpPromoAttributeId = 1012,
                    ExpPromoAttributeName = "none",
                    PropertyName = "DeviceOS"
                };
            #endregion
        }

        public bool ReccuringDayOfWeekContains(string stringValue)
        {
            if (ReccuringDayOfWeek == null)
                return false;

            return ReccuringDayOfWeek.Contains(stringValue);
        }
    }
}