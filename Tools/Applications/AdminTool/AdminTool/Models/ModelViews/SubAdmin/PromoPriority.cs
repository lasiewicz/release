﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public class PromoPriority : ModelViewBase
    {
        public int BrandId { get; set; }
        public int PaymentType { get; set; }
        public int PromoType { get; set; }
        public bool IsRefresh { get; set; }

        public List<SelectListItem> BrandIdChoices { get; set; }
        public List<SelectListItem> PaymentTypeChoices { get; set; }
        public List<SelectListItem> PromoTypeChoices { get; set; }

        public List<Matchnet.PromoEngine.ValueObjects.Promo> Promos { get; set; }

        public SharedTopNav TopNav { get; set; }

        public PromoPriority() : base()
        {
            TopNav = new SharedTopNav();
        }
    }
}