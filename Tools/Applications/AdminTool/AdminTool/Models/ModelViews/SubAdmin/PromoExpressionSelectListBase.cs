﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Content.ValueObjects.Region;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public abstract class PromoExpressionSelectListBase : PromoExpressionBase
    {
        public int BrandId { get; set; }
        public Hashtable ResourceBag { get; set; }
        public List<SelectListItem> PossibleValues { get; set; }

        public abstract void PopulateSelectListItems();

        protected List<SelectListItem> GetAttributeOptions()
        {
            var items = new List<SelectListItem>();

            if (ExpPromoAttributeName != Constants.NULL_STRING && BrandId != Constants.NULL_INT)
            {
                AttributeOptionCollection attOptions = AttributeOptionSA.Instance.GetAttributeOptionCollection(ExpPromoAttributeName, BrandId);

                items.Add(new SelectListItem() { Text = " All ", Value = Constants.NULL_INT.ToString() });

                foreach (AttributeOption aOption in attOptions)
                {
                    if(aOption.Value == 0 || aOption.Value == Constants.NULL_INT)
                        continue;

                    var item = new SelectListItem
                                   {
                                       Value = aOption.Value.ToString(),
                                       Text =
                                           ResourceBag == null
                                               ? aOption.Description
                                               : GeneralHelper.GetResource(ResourceBag, aOption.ResourceKey)
                                   };
                    items.Add(item);
                }
            }

            return items;
        }

        protected List<SelectListItem> GetAllDMACodes()
        {
            var items = new List<SelectListItem>
                            {new SelectListItem() {Text = " All ", Value = Constants.NULL_INT.ToString()}};

            var dmaCol = RegionSA.Instance.GetAllDMAS();
            items.AddRange(from DMA dma in dmaCol select new SelectListItem() { Text = dma.DMADescription, Value = dma.DMAID.ToString() });

            return items;
        }
    }
}