﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public class PlanDisplayList : ModelViewBase
    {
        public int BrandId { get; set; }
        public List<SelectListItem> BrandIdChoices { get; set; }

        public List<PlanDisplay> PlansToDisplays { get; set; }

        public SharedTopNav TopNav { get { return new SharedTopNav(); } }
    }
}