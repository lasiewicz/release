﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public class ResourceTemplateDetail
    {
        public SharedTopNav TopNav { get; set; }
        public List<SelectListItem> Brands { get; set; }
        public List<SelectListItem> TemplateTypes { get; set; }
        public int TemplateId  { get; set; }
        public string Content  { get; set; }
        [Required]
        public string Description { get; set; }
        public int BrandId { get; set; }
        public int TemplateTypeId { get; set; }
        public bool IsNew { get; set; }

        public ResourceTemplateDetail()
        {
            TopNav = new SharedTopNav();
            Brands = new List<SelectListItem>();
            TemplateTypes = new List<SelectListItem>();
        }

        public ResourceTemplateDetail(int templateId)
        {
            TopNav = new SharedTopNav();
            Brands = new List<SelectListItem>();
            TemplateTypes = new List<SelectListItem>();
            ResourceTemplate template = ResourceTemplateSA.Instance.GetResourceTemplateByID(templateId);
            Content = template.Content;
            Description = template.Description;
            BrandId = template.GroupID;
            TemplateTypeId = (int)template.ResourceTemplateType;
            TemplateId = template.ResourceTemplateID;
        }

    }
}