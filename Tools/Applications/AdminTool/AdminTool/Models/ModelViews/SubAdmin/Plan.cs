﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Matchnet.Purchase.ValueObjects;

namespace AdminTool.Models.ModelViews.SubAdmin
{
    public enum PlanState
    {
        EnterInfo = 0,
        ViewInfo = 1,
        Inserted = 2
    }

    public class Plan : ModelViewBase
    {
        public int BrandId { get; set; }
        public List<SelectListItem> BrandIds { get; set; } 
        public string CurrencyType { get; set; }
        
        [Required(ErrorMessage="Initial cost is required")]
        [RegularExpression(@"^\d+(\.\d{2})?$", ErrorMessage="Initial cost is in incorrect format")]
        public decimal? InitialCost { get; set; }

        [Required(ErrorMessage="Initial duration is required")]
        public int? Duration { get; set; }

        [Required(ErrorMessage="Initial duration type is required")]
        public int DurationType { get; set; }
        
        public List<SelectListItem> DurationTypes { get; set; }

        public decimal? CostPer
        {
            get
            {
                if(InitialCost != null && Duration != null)
                {
                    return Math.Round((InitialCost / Duration).GetValueOrDefault(), 2);
                }

                return 0;
            }
        }

        [RegularExpression(@"^\d+(\.\d{2})?$", ErrorMessage = "Renewal cost is in incorrect format")]
        public decimal? RenewalCost { get; set; }

        public int? RenewalDuration { get; set; }
        public int RenewalDurationType { get; set; }
        public List<SelectListItem> RenewalDurationTypes { get; set; }
        public int? FreeDuration { get; set; }
        public int FreeDurationType { get; set; }
        public List<SelectListItem> FreeDurationTypes { get; set; }
        public bool IsCheckPlan { get; set; }
        public int PlanType { get; set; }
        public List<SelectListItem> PlanTypes { get; set; }
        public int PremiumPlanTypeMask { get; set; }
        public PremiumType PremiumPlanTypeMaskNative
        {
            get { return (PremiumType) PremiumPlanTypeMask; }
        }
        public List<SelectListItem> PremiumPlanTypes { get; set; }
        public int ALaCartePlanType { get; set; }
        public List<SelectListItem> ALaCartePlanTypes { get; set; }
        public int AdditiveMask { get; set; }
        public PlanType AdditiveMaskNative
        {
            get { return (PlanType) AdditiveMask; }
        }
        public List<PlanPriceBreakdown> PremiumPlansPriceBreakdowns { get; set; }

        public SharedTopNav TopNav { get; set; }

        public Plan() : this(true)
        {
        }

        public Plan(bool populateSelectListItems)
        {
            TopNav = new SharedTopNav();
            if (!populateSelectListItems)
                return;
            
            SubAdminHelper.PopulateSelectListItems(this);

            PremiumPlansPriceBreakdowns = new List<PlanPriceBreakdown>()
                                                   {
                                                       new PlanPriceBreakdown() {EnabledDisburseCount=false, LabelText="Standard Subscription", PremiumPlanType=0 },
                                                       new PlanPriceBreakdown() {EnabledDisburseCount=false, LabelText="Highlighted Profile", PremiumPlanType=1},
                                                       new PlanPriceBreakdown() {EnabledDisburseCount=false, LabelText="Member Spotlight", PremiumPlanType=2},
                                                       new PlanPriceBreakdown() {EnabledDisburseCount=false, LabelText="J-Meter", PremiumPlanType=4},
                                                       new PlanPriceBreakdown() {EnabledDisburseCount=false, LabelText="All Access", PremiumPlanType=32},
                                                       new PlanPriceBreakdown() {EnabledDisburseCount=true, LabelText="All Access Email", PremiumPlanType=64},
                                                       new PlanPriceBreakdown() {EnabledDisburseCount=false, LabelText="Read Receipt", PremiumPlanType=128}
                                                   };
        }
    }
}