﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace AdminTool.Models.ModelViews.TrainingTool
{
    public class Constants
    {
        //// old constants - remove this later
        //public static string GROUP_NAME_BADGOODESSAY = "badgoodessay";
        //public static string GROUP_NAME_ESSAYVIOLATION = "essayviolation";
        //public const string GROUP_NAME_APPROVESUSPEND = "approvesuspend";
        //public const string GROUP_NAME_SENDWARNINGS = "sendwarnings";

        //public const string SUBGROUP_NAME_APPROVE = "approve";
        //public const string SUBGROUP_NAME_SUSPEND = "suspend";
        //public const string SUBGROUP_NAME_CONTACTINFO = "contactinfo";
        //public const string SUBGROUP_NAME_INAPPROPRIATE = "inappropriate";
        //public const string SUBGROUP_NAME_GENERALVIOLATION = "generalviolation";
        //public const string SUBGROUP_NAME_SENDNOWARNING = "sendnowarning";
        //public const string SUBGROUP_NAME_GOODESSAY = "goodessay";
        //public const string SUBGROUP_NAME_BADESSAY = "badessay";

        // new constants
        public static string GROUP_NAME_SUSPENSION = "suspension";
        public static string GROUP_NAME_EMAIL_WARNING = "emailwarning";
        public static string GROUP_NAME_CONTENT_REVIEW = "contentreview";

    }

    #region Old code - remove this later
    //public class TestResultLineItem
    //{
    //    private string _displayName;

    //    public string DisplayName
    //    {
    //        get
    //        {
    //            return string.IsNullOrEmpty(_displayName) ? Name : _displayName;
    //        }
    //        set { _displayName = value; }
    //    }

    //    public string Name { get; set; }
    //    public int CorrectCount { get; set; }
    //    public int IncorrectCount { get; set; }

    //    public int CorrectPercent
    //    {
    //        get
    //        {
    //            if (CorrectCount + IncorrectCount == 0)
    //                return 0;

    //            decimal correctDecimal = CorrectCount;
    //            decimal incorrectDecimal = IncorrectCount;

    //            var ret = correctDecimal / (correctDecimal + incorrectDecimal) * 100;

    //            return (int)ret ;
    //        }
    //    }
    //}

    //public class TestResultAnswerGroup
    //{
    //    private List<TestResultLineItem> _testResultLineItems;

    //    #region Properties
    //    public string GroupName { get; set; }

    //    public List<TestResultLineItem> TestResultLineItems
    //    {
    //        get
    //        {
    //            if (_testResultLineItems == null)
    //                _testResultLineItems = new List<TestResultLineItem>();

    //            return _testResultLineItems;
    //        }
    //    }

    //    public int TotalCorrectPercent
    //    {
    //        get
    //        {
    //            decimal totalCount = 0;
    //            decimal totalCorrectCount = 0;

    //            if (!TestResultLineItems.Any())
    //                return 0;

    //            foreach (TestResultLineItem item in TestResultLineItems)
    //            {
    //                totalCount += item.CorrectCount + item.IncorrectCount;
    //                totalCorrectCount += item.CorrectCount;
    //            }

    //            var ret = (totalCorrectCount / totalCount) * 100;

    //            return (int)ret;
    //        }
    //    }

    //    public TestResultLineItem this[string lineItemName]
    //    {
    //        get
    //        {
    //            return (from a in TestResultLineItems
    //                    where a.Name.ToLower() == lineItemName.ToLower()
    //                    select a).SingleOrDefault();
    //        }
    //    }

    //    public TestResultLineItem this[int index]
    //    {
    //        get
    //        {
    //            return TestResultLineItems[index];
    //        }
    //    }
    //    #endregion

    //    // methods
    //    public TestResultLineItem AddLineItem(string name)
    //    {

    //        var lineItem = (from a in TestResultLineItems
    //                                       where a.Name == name
    //                                       select a).SingleOrDefault();

    //        if (lineItem == null)
    //        {
    //            lineItem = new TestResultLineItem {Name = name.ToLower()};
    //            TestResultLineItems.Add(lineItem);
    //        }

    //        return lineItem;
    //    }

    //    public void AddToCount(string name, bool isCorrect, int countToAdd)
    //    {
    //        TestResultLineItem lineItem = AddLineItem(name);

    //        if (lineItem != null)
    //        {
    //            if (isCorrect)
    //            {
    //                lineItem.CorrectCount += countToAdd;
    //            }
    //            else
    //            {
    //                lineItem.IncorrectCount += countToAdd;
    //            }
    //        }
    //    }

    //    public int GetCount(string name, bool isCorrect)
    //    {
    //        var lineItem = (from a in TestResultLineItems
    //                        where a.Name == name
    //                        select a).SingleOrDefault();

    //        if (lineItem == null)
    //            return 0;

    //        return isCorrect ? lineItem.CorrectCount : lineItem.IncorrectCount;
    //    }
    //}
    #endregion

    public class TestResultAnswerGroup
    {
        public string GroupName { get; set; }
        public int PossiblePoints { get; set; }
        public int PointsCorrect { get; set; }
    }

    public class TTTestResult : ModelViewBase
    {
        public SharedTopNav TopNav { get; set; }
        /// <summary>
        /// Time is returned in minutes in decimal representation. If minutes and seconds need to be separated out
        /// check out TotalTestTimeMinutes and TotalTestTimeSeconds properties.
        /// </summary>
        public double TotalTestTime { get; set; }
        public int TotalTestTimeMinutes { get; set; }
        public int TotalTestTimeSeconds { get; set; }
        public string AgentEmail { get; set; }
        public int AgentMemberID { get; set; }
        public DateTime TimeStarted { get; set; }
        public DateTime TimeEnded { get; set; }
        public string GroupingName { get; set; }
        public int TotalTestItemCount { get; set; }
        public double ProfilesPerHour { get; set; }
        public bool TestFinished { get; set; }
        public List<TestResultAnswerGroup> TestAnswerGroups { get; set; }

        public TestResultAnswerGroup this[string answerGroupName]
        {
            get
            {
                return (from a in TestAnswerGroups
                        where a.GroupName.ToLower() == answerGroupName.ToLower()
                        select a).SingleOrDefault();
            }
        }
    }
}