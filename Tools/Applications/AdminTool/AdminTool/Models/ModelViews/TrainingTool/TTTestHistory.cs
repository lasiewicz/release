﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.TrainingTool
{
    public class TTTestHistory : ModelViewBase
    {
        public SharedTopNav TopNav { get; set; }
        public TTTestResults TestResults { get; set; }
    }
}