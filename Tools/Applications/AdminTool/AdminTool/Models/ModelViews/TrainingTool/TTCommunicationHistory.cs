﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.TrainingTool
{
    public class TTCommunicationHistory : TTMemberBase
    {
        #region Properties
        public int TTMemberProfileID { get; set; }
        public DateTime LastMessageSentDate { get; set; }
        public DateTime LastMatchMailSentDate { get; set; }
        public DateTime LastMatchMailAttemptDate { get; set; }
        public DateTime AllAcessExpireDate { get; set; }
        public int AllAccessRemainingCount { get; set; }
        public int UnreadMessageCount { get; set; }
        public int ContactInfoWarnings { get; set; }
        public int InappropriateContentWarnings { get; set; }
        public int GeneralViolationWarnings { get; set; }
        #endregion
    }
}