﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminTool.Models.ModelViews.TrainingTool
{
    public class TTTraineeStart : ModelViewBase
    {
        public SharedTopNav TopNav { get; set; }
        public string TraineeEmail { get; set; }
        public List<SelectListItem> GroupingList { get; set; }
        public int LastPausedTTTestInstanceID { get; set; }
        public TTTestResults TestResults { get; set; }
    }
}