﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet.ApproveQueue.ValueObjects.TrainingTool;

namespace AdminTool.Models.ModelViews.TrainingTool
{
    public class TTSetAnswers : ModelViewBase
    {
        public SharedTopNav TopNav { get; set; }
        public TTSuperTopNav SuperTopNav { get; set; }
        public List<SelectListItem> GroupingList { get; set; }
        public List<SelectListItem> SuspendReasons { get; set; }
        public List<string> SelectedSuspendReasons { get; set; } 
        public List<TTMemberAttributeTextUI> TTMemberAttributeTexts { get; set; }
        public string TTMemberAttributeTextIDsString { get; set; }
        public string CurrentBucketName { get; set; }
        public int TTMemberProfileID { get; set; }
        public int MemberID { get; set; }
        public bool Suspend { get; set; }
        public bool WarningContactInfo { get; set; }
        public bool WarningInappropriateContactInfo { get; set; }
        public bool WarningGeneralViolation { get; set; }
    }
}