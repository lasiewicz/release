﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models.ModelViews.Member;

namespace AdminTool.Models.ModelViews.TrainingTool
{
    public class TTTestPage : ModelViewBase
    {
        public SharedTopNav TopNav { get; set; }
        public ExpandedUserInfo ExpandedUserInfo { get; set; }
        public List<TTMemberAttributeTextUI> TTMemberAttributeTexts { get; set; }
        public List<SelectListItem> AdminSuspendReasons { get; set; }
        public string TTMemberAttributeTextIDsString { get; set; }
        public int TTTestInstanceID { get; set; }
        public int TTMemberProfileID { get; set; }
        public bool Suspend { get; set; }
        public bool WarningContactInfo { get; set; }
        public bool WarningInappropriateContent { get; set; }
        public bool WarningGeneralViolation { get; set; }
        public bool WarningNoWarning { get; set; }
        public bool IsLastQuestion { get; set; }
        public bool PossibleFraud { get; set; }
        public int TTAnswerMemberProfileVersion { get; set; }
        public int TTAnswerSuspendReasonsVersion { get; set; }
    }
}