﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminTool.Models.ModelViews.TrainingTool
{
    public class TTManageGrouping : ModelViewBase
    {
        public SharedTopNav TopNav { get; set; }
        public List<SelectListItem> GroupingList { get; set; }

        public List<SelectListItem> SingleAddSiteList { get; set; }
        public List<SelectListItem> MultiAddSiteList { get; set; }
        //public int SingleAddMemberID { get; set; }
        //public int SingleAddSelectedSiteID { get; set; }
        //public int SingleAddSelectedGroupingID { get; set; }

        public int PageNumber { get; set; }
        public int RowsPerPage
        {
            get { return Convert.ToInt32(ConfigurationManager.AppSettings["SearchResultPerPage"]); }
        }
    }
}