﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.TrainingTool
{
    public class TTViewMember : ModelViewBase
    {
        public TTSuperTopNav SuperTopNav { get; set; }
        public TTBasicUserInfo TtBasicUserInfo { get; set; }
        public TTGlobalProfile TtGlobalProfile { get; set; }
        public TTDateTimeStampInfo TtDateTimeStampInfo { get; set; }
        public TTMemberLeft TtMemberLeft { get; set; }
        public TTCommunicationHistory TtCommunicationHistory { get; set; }
    }
}