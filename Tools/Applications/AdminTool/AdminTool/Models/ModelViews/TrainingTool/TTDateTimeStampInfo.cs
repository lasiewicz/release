﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.TrainingTool
{
    public class TTDateTimeStampInfo : TTMemberBase
    {
        public DateTime RegisteredDate { get; set; }

        public bool IsMobileRegistration { get; set; }

        public DateTime LastUpdateDate { get; set; }

        public DateTime LastLoginDate { get; set; }

        public List<DateTime> LastLogons { get; set; }
    }
}