﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.TrainingTool
{
    public class TTBasicUserInfo : TTMemberBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public bool IsAdmin { get; set; }
        public string Email { get; set; }
        public Enums.ProfileStatus ProfileStatus { get; set; }
        public string SuspendReason { get; set; }
    }
}