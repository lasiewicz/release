﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.ApproveQueue.ValueObjects.TrainingTool;

namespace AdminTool.Models.ModelViews.TrainingTool
{
    public class TTMemberAttributeTextUI : TTMemberAttributeText
    {

        public int TextboxHeight { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> TextUnacceptableReasons { get; set; }

        public TTMemberAttributeTextUI(TTMemberAttributeText baseClassInstance)
        {
            TTMemberAttributeTextID = baseClassInstance.TTMemberAttributeTextID;
            TTMemberProfileID = baseClassInstance.TTMemberProfileID;
            TTAttributeID = baseClassInstance.TTAttributeID;
            FieldValue = baseClassInstance.FieldValue;
            IncludeInTest = baseClassInstance.IncludeInTest;
            MemberAttributeName = baseClassInstance.MemberAttributeName;
            IsGood = baseClassInstance.IsGood;
            BadReasonMask = baseClassInstance.BadReasonMask;
            AnswerVersion = baseClassInstance.AnswerVersion;
        }
    }
}