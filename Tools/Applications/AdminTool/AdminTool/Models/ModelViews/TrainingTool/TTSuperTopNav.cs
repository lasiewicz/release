﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.TrainingTool
{
    public class TTSuperTopNav
    {
        public int TargetAgentID { get; set; }
        public int GroupingID { get; set; }
    }
}