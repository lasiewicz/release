﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminTool.Models.ModelViews.Email;

namespace AdminTool.Models.ModelViews.TrainingTool
{
    public class TTEmailList : EmailList
    {
        public int TTMemberProfileID { get; set; }
        
    }
}