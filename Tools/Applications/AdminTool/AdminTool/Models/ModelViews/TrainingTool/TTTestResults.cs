﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace AdminTool.Models.ModelViews.TrainingTool
{
    public class TTTestResults : ModelViewBase
    {
        public List<TTTestResult> TTTestResultList { get; set; }
    }
}