﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet.Content.ValueObjects.Admin;

namespace AdminTool.Models.ModelViews.TrainingTool
{
    public class TTGlobalProfile : TTMemberBase
    {
        public bool AdminSuspended { get; set; }
        public bool BadEmail { get; set; }
        public bool EmailVerified { get; set; }
        public bool OnDNE { get; set; }
        public List<SelectListItem> AdminSuspendReasons { get; set; }
        public AdminActionReasonID LastSuspendedReasonID { get; set; }
    }
}