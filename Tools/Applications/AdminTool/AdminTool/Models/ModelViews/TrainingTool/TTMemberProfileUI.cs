﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.ApproveQueue.ValueObjects.TrainingTool;

namespace AdminTool.Models.ModelViews.TrainingTool
{
    public class TTMemberProfileUI : TTMemberProfile
    {
        public string AnswerCompleted
        {
            get { return HasAnswer ? "Yes" : "No"; }
        }

        public TTMemberProfileUI(TTMemberProfile baseClassInstance)
        {
            TTMemberProfileID = baseClassInstance.TTMemberProfileID;
            TTGroupingID = baseClassInstance.TTGroupingID;
            GroupingName = baseClassInstance.GroupingName;
            MemberID = baseClassInstance.MemberID;
            BrandID = baseClassInstance.BrandID;
            HasAnswer = baseClassInstance.HasAnswer;
        }
    }
}