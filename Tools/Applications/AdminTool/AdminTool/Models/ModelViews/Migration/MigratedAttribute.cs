﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.Migration
{
    public class MigratedAttribute
    {
        public int BHAttributeID { get; set; }
        public string BHAttributeName { get; set; }
        public string MingleAttributeName { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string ApprovedStatus { get; set; }
    }
}
