﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.Migration
{
    public class MigratedSearchPreference
    {
        public string MinglePreferenceName { get; set; }
        public string BHPreferenceName { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string Weight { get; set; }
    }
}