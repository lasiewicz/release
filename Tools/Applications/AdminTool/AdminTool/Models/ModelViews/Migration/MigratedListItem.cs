﻿#region

using Matchnet.List.ValueObjects;
using System.Collections.Generic;

#endregion

namespace AdminTool.Models.ModelViews.Migration
{
    public class ListCategoryModel
    {
        public int CategoryId { get; set; }
        public Dictionary<int, ListItemDetail> MemberIds { get; set; }
        public int RowCount { get; set; }
    }
}