﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminTool.Models.ModelViews.Member;
using AdminTool.Models.ModelViews.Migration;

namespace AdminTool.Models.ModelViews
{
    public class ListMigrationModelView : MemberBase
    {
        public TopNav TopNav { get; set; }
        public MemberLeft MemberLeft { get; set; }
        public Matchnet.Member.ServiceAdapters.Member Member { get; set; }
        public Matchnet.Content.ValueObjects.BrandConfig.Brand brand { get; set; }
        public List<ListCategoryModel> ListData { get; set; }
    }
}