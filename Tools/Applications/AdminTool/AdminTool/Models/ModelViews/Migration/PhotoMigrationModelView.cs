﻿#region

using System.Collections.Generic;
using System.Net.Configuration;
using AdminTool.Models.ModelViews.Member;

#endregion

namespace AdminTool.Models.ModelViews.Migration
{
    public class PhotoMigrationModelView : MemberBase
    {
        public TopNav TopNav { get; set; }
        public MemberLeft MemberLeft { get; set; }
        public Matchnet.Member.ServiceAdapters.Member Member { get; set; }
        public Matchnet.Content.ValueObjects.BrandConfig.Brand Brand { get; set; }
        public List<Photo> Photos { get; set; }
    }

    public class Photo
    {
        public int MemberPhotoId { get; set; }
        public string FullCloudPath { get; set; }
        public string ThumbCloudPath { get; set; }
        public bool IsApproved { get; set; }
        public int AdminMemberId { get; set; }
    }
}