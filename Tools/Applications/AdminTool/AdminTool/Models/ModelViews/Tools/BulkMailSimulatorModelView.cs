﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Search.ValueObjects;
using Matchnet.BulkMail.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using AdminTool.Models.ModelViews.Member;

namespace AdminTool.Models.ModelViews.Tools
{
    public class BulkMailSimulatorModelView : ModelViewBase
    {
        public List<MatchMember> FinalMatches { get; set; }
        public List<string> Warnings { get; set; }
        public BulkMailScheduleRecord ScheduleRecord { get; set; }
        public bool Errored { get; set; }
        public bool InitialSearchSufficient { get; set; }
        public bool LaxSearchSufficient { get; set; }
        public bool InitialSearchSufficientWithAgeFilter { get; set; }
        public bool LaxSearchSufficientWithAgeFilter { get; set; }
        public string ErrorMessage { get; set; }
        public int MatchesFound { get; set; }
        public MatchSearch InitialSearch { get; set; }
        public MatchSearch LaxSearch { get; set; }
        public MatchSearch InitialSearchWithAgeFilter { get; set; }
        public MatchSearch LaxSearchWithAgeFilter { get; set; }
        public int ScrubListCount { get; set; }
        public MemberLeft Left { get; set; }
        public TopNav TopNav { get; set; }
        public bool UseAgeFilter { get; set; }
        public bool KeywordSearchEnabled { get; set; }
        public Matchnet.BulkMail.ValueObjects.Enumerations.SearchPrefType SearchPrefType { get; set; }

        public BulkMailSimulatorModelView()
        {
            Warnings = new List<string>();
            InitialSearch = new MatchSearch();
            InitialSearchWithAgeFilter = new MatchSearch();
            LaxSearch = new MatchSearch();
            LaxSearchWithAgeFilter = new MatchSearch();
            FinalMatches = new List<MatchMember>();
            SearchPrefType = Enumerations.SearchPrefType.None;
        }
    }
}
