﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Matchnet.BulkMail.ValueObjects;

namespace AdminTool.Models.ModelViews.Reports
{
    public class BulkMailBatchModelView : ModelViewBase
    {
        public Dictionary<ProcessingStatusID, int> Totals { get; set; }
        public BulkMailBatch Batch { get; set; }
        public Dictionary<string, List<BulkMailBatchSummaryRecord>> GroupedSummaryRecords {get;set;}
    }
}
