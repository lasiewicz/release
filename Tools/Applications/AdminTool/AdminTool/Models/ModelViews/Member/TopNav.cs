﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.Member
{
    public class TopNav : MemberBase
    {
        #region Properties
        public string Email { get; set; }
        public bool UseAdminToolPhotoUpload { get; set; }
        public bool ShowEditPremiumServicesLink { get; set; }
        #endregion

        #region Constructor
        public TopNav()
            : base()
        {

        }
        #endregion
    }
}
