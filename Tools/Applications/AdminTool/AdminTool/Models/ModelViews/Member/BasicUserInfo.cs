﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.Member
{
    public class BasicUserInfo : MemberBase
    {
        #region Properties
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public AdminTool.Models.Enums.ProfileStatus ProfileStatus { get; set; }
        public string SuspendReason { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
        public List<Matchnet.Member.ValueObjects.LastEmail> LastEmails { get; set; }
        public bool IsNorbert { get; set; }
        #endregion

        #region Constructor
        public BasicUserInfo() : base()
        {
            FirstName = "";
            LastName = "";
            UserName = "";
            ProfileStatus = Enums.ProfileStatus.Active;
            SuspendReason = "";
            Email = "";
            Password = "";
            IsAdmin = false;
        }
        #endregion
    }
}
