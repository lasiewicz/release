﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminTool.Models.ModelViews.Member
{
    public class RenewalInfo : MemberBase
    {
        public bool CanReopen { get; set; }
        public bool CanAdjustRenewalDate { get; set; }
        public bool HasValidPlan { get; set; }
        public bool HasPremiumHighlight { get; set; }
        public bool RenewalEnabledPremiumHighlight { get; set; }
        public bool HasPremiumSpotlight { get; set; }
        public bool RenewalEnabledPremiumSpotlight { get; set; }
        public bool HasPremiumJMeter { get; set; }
        public bool RenewalEnabledPremiumJMeter { get; set; }
        public bool HasPremiumAllAccess { get; set; }
        public bool RenewalEnabledPremiumAllAccess { get; set; }
        public bool HasPremiumReadReceipts { get; set; }
        public bool RenewalEnabledReadReceipts { get; set; }
        public bool HasActiveFreeTrialSubscription { get; set; }
        public DateTime RenewDate { get; set; }

        public int RenewalPlanInitialDuration { get; set; }
        public int RenewalPlanInitialDurationType { get; set; }
        public decimal RenewalPlanInitialAmount { get; set; }
        public int RenewalPlanRenewalDuration { get; set; }
        public int RenewalPlanRenewalDurationType { get; set; }
        public decimal RenewalPlanRenewalAmount { get; set; }
        public List<SelectListItem> TerminationReasons = new List<SelectListItem>();
        public int SelectedTerminationReason { get; set; }

        public RenewalInfo() : base()
        {

        }
    }
}
