﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.Member
{
    public class SubscriptionStatusInfo : MemberBase
    {
        #region Properties
        public string SubscriberStatus { get; set; }
        public bool HasValidSubsciption { get; set; }
        public DateTime RenewalDate { get; set; }
        public DateTime? SubscriptionEndDate { get; set; }
        public decimal RenewalAmount { get; set; }
        public string BundledPremiumServices { get; set; }
        public string ALCPremiumServices { get; set; }
        public string ActivePremiumServices { get; set; }
        public bool HasSpotlightALCService { get; set; }
        public bool HasHighlightALCService { get; set; }
        public bool HasAllAccessALCService { get; set; }
        public bool HasJMeterALCService { get; set; }
        public bool HasReadReceiptALCService { get; set; }
        public DateTime? SubscriptionLastInitialPurchaseDate { get; set; }
        public bool HasPausedSubscription { get; set; }
        public bool IsPauseAllowed { get; set; }
        public bool IsIAP { get; set; } //In App Purchase

        public bool ActiveRenewal
        {
            get
            {
                if (RenewalDate > DateTime.Now && !SubscriptionEndDate.HasValue)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion

        #region Constructor
        public SubscriptionStatusInfo()
            : base()
        {
            IsPauseAllowed = true;
        }
        #endregion
    }
}
