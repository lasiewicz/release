﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;

namespace AdminTool.Models.ModelViews.Member
{
    public class DateTimeStampInfo : MemberBase
    {
        #region Properties
        public DateTime RegisteredDate { get; set; }
        public DateTime LastLoginDate { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public List<LastLogon> LastLogons { get; set; }
        public bool IsMobileRegistration { get; set; }
        public string SiteNameForLink { get; set; }
        #endregion

        #region Constructor
        public DateTimeStampInfo()
        {
        }
        #endregion

        public string GetLastLogonDisplay(LastLogon lastLogon) 
        {
            string logondisplay = string.Empty;
            if (lastLogon != null)
            {
                logondisplay = lastLogon.LastLogonDate.ToString("MM/dd/yyyy hh:mm tt");
                if (lastLogon.AdminMemberId != Constants.NULL_INT)
                {
                    var admin = MemberHelper.GetMember(lastLogon.AdminMemberId, MemberLoadFlags.None);
                    logondisplay += string.Format("<span style=\"padding-left:2px;\" title={0}> (A) </span>", admin.EmailAddress);
                }
            }
            return logondisplay;
        }
    }
}
