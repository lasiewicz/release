﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Text;
using System.IO;

namespace AdminTool.Models.ModelViews.Member
{
    public class QASupport : MemberBase
    {
        public TopNav TopNav { get; set; }
        public MemberLeft MemberLeft { get; set; }
        public Matchnet.Member.ServiceAdapters.Member Member { get; set; }
        public Matchnet.Content.ValueObjects.BrandConfig.Brand brand { get; set; }
        public string TrackingRegApplication {
            get
            {
                return Member.GetAttributeInt(brand, "TrackingRegApplication", 0).ToString();
            }
        }
        public string TrackingRegOS
        {
            get
            {
                return HttpContext.Current.Server.HtmlEncode(Member.GetAttributeText(brand, "TrackingRegOS", ""));
            }
        }
        public string TrackingRegFormFactor
        {
            get
            {
                string trackingRegFormFactor = Member.GetAttributeText(brand, "TrackingRegFormFactor", "");
                if (!string.IsNullOrEmpty(trackingRegFormFactor))
                {
                    try
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(trackingRegFormFactor);
                        StringBuilder sb = new StringBuilder();
                        StringWriter sw = new StringWriter(sb);
                        XmlTextWriter xmlw = new XmlTextWriter(sw);
                        xmlw.Formatting = Formatting.Indented;
                        doc.WriteTo(xmlw);
                        trackingRegFormFactor = sb.ToString();
                    }
                    catch
                    {

                    }
                }
                return HttpContext.Current.Server.HtmlEncode(trackingRegFormFactor);
            }
        }
        public string TrackingRegDevice
        {
            get
            {
                return HttpContext.Current.Server.HtmlEncode(Member.GetAttributeText(brand, "TrackingRegDevice", ""));
            }
        }
        public DateTime RegisteredDate
        {
            get
            {
                return Member.GetAttributeDate(brand, "BrandInsertDate", System.DateTime.MinValue);
            }
        }
        public string RegistrationScenarioID {
            get
            {
                return Member.GetAttributeInt(brand, "RegistrationScenarioID", 0).ToString();
            }
        }
        public string RegsistrationSessionID {
            get
            {
                return Member.GetAttributeText(brand, "RegsistrationSessionID", "");
            }
        }
        public string BlackBoardEatsPromo1
        {
            get
            {
                string value = Member.GetAttributeText(brand, "BlackBoardEatsPromo1", "");
                if (!string.IsNullOrEmpty(value))
                {
                    try
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(value);
                        StringBuilder sb = new StringBuilder();
                        StringWriter sw = new StringWriter(sb);
                        XmlTextWriter xmlw = new XmlTextWriter(sw);
                        xmlw.Formatting = Formatting.Indented;
                        doc.WriteTo(xmlw);
                        value = sb.ToString();
                    }
                    catch
                    {

                    }
                }
                return HttpContext.Current.Server.HtmlEncode(value);
            }
        }

        public DateTime BBE_PROMOTION_EXPIRATION { get; set; }
        public string ENABLE_BLACK_BOARD_EATS_PROMOTION { get; set; }
        public string IOS_IAP_Receipt_Original_TransactionID { get; set; }
        public bool HasRamahAlumBadge { get; set; }
        public int RamahStartYear { get; set; }
        public int RamahEndYear { get; set; }
        public string RamahCamp { get; set; }

        [Flags]
        public enum Camps
        {
            None = 0,
            Berkshires = 1,
            California = 2,
            Canada = 4,
            Chicago = 8,
            Connecticut = 16,
            Darom = 32,
            GlenSpey = 64,
            Jerusalem = 128,
            Mador = 256,
            Maine = 512,
            NewEngland = 1024,
            Nyack = 2048,
            Philadelphia = 4096,
            Poconos = 8192,
            Rockies = 16384,
            SEE = 32768,
            Seminar = 65536,
            TRYUSYHigh = 131072,
            Wisconsin = 262144

        }



    }
}