﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.Member
{
    public class MemberData : MemberBase
    {
        #region Properties
        //Model views
        public AdminActionLog AdminActionLog { get; set; }
        public BasicUserInfo BasicUserInfo { get; set; }
        public BillingInfo BillingInfo { get; set; }
        public CommunicationHistory CommunicationHistory { get; set; }
        public DateTimeStampInfo DateTimeStampInfo { get; set; }
        public FraudAbuseInfo FraudAbuseInfo { get; set; }
        public MemberLeft MemberLeft { get; set; }
        public OtherActioninfo OtherActionInfo { get; set; }
        public ProfileInfo ProfileInfo { get; set; }
        public ProfileStatusInfo ProfileStatusInfo { get; set; }
        public SubscriptionStatusInfo SubscriptionStatusInfo { get; set; }
        public TopNav TopNav { get; set; }
        public TransactionHistory TransactionHistory { get; set; }
        public GlobalProfile GlobalProfile { get; set; }
        public Dictionary<string, long> TimingData { get; set; }
        public OffSiteNotifications OffSiteNotifications { get; set; }
        public PremiumServiceSettings PremiumServiceSettings { get; set; }
        public AdminAdjustInfo AdminAdjustInfo { get; set; }
        public PauseSubscriptionInfo PauseSubscriptionInfo { get; set; }
        public RenewalInfo RenewalInfo { get; set; }
        public ProfileDisplaySettings ProfileDisplaySettings { get; set; }
        public JMeterEditable JMeterEditable { get; set; }
        public ILAuthenticated IlAuthenticated { get; set; }
        public LastFewClientIPs LastFewClientIPs { get; set; }
        #endregion

        #region Constructor
        public MemberData()
        {
            AdminActionLog = new AdminActionLog();
            BasicUserInfo = new BasicUserInfo();
            BillingInfo = new BillingInfo();
            CommunicationHistory = new CommunicationHistory();
            DateTimeStampInfo = new DateTimeStampInfo();
            FraudAbuseInfo = new FraudAbuseInfo();
            MemberLeft = new MemberLeft();
            OtherActionInfo = new OtherActioninfo();
            ProfileInfo = new ProfileInfo();
            SubscriptionStatusInfo = new SubscriptionStatusInfo();
            TopNav = new TopNav();
            TransactionHistory = new TransactionHistory();
            GlobalProfile = new GlobalProfile();
            AdminAdjustInfo = new AdminAdjustInfo();
            PauseSubscriptionInfo = new PauseSubscriptionInfo(false, true);
            RenewalInfo = new RenewalInfo();
        }

        #endregion
    }
}
