﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminTool.Models;


namespace AdminTool.Models.ModelViews.Member
{
    public class ProfileInfo : MemberBase
    {
        #region Properties
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public DateTime BirthDate { get; set; }
        public Enums.Gender Gender { get; set; }
        public Enums.SeekingGender SeekingGender { get; set; }
        public string RegionDisplay { get; set; }
        #endregion

        #region Constructor
        public ProfileInfo(): base()
        {

        }
        #endregion
    }
}
