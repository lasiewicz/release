﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.Common.PurchaseService;
//using Matchnet.Purchase.ValueObjects;

namespace AdminTool.Models.ModelViews.Member
{
    public class BillingInfo : MemberBase
    {
        #region Properties
        public string PaymentProfileId { get; set; }
        public int OrderID { get; set; }
        public PaymentType PaymentType { get; set; }
        public string CardType { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public int ExpMonth { get; set; }
        public int ExpYear { get; set; }
        public string FirstName { get; set; }
        public string LastFourDigitsCreditCardNumber { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string PostalCode { get; set; }
        public string State { get; set; }
        public string StreetAddress { get; set; }
        public string BankAccountNumberLastFour { get; set; }
        public string BankAccountType { get; set; }
        public string BankName { get; set; }
        public string BankState { get; set; }
        public string BankRoutingNumberLastFour { get; set; }
        public string EmailAddress { get; set; }
        public string PayerStatus { get; set; }
        public string PaypalToken { get; set; }
        public string BillingAgreementID { get; set; }
        public bool UsedForDefaultPaymentProfile { get; set; }
        public string GovernmentIssuedID { get; set; }
        public bool ShowGovernmentIssueID { get; set; }

        #endregion

        #region Constructor

        public BillingInfo() : base()
        {

        }

        #endregion
    }
}
