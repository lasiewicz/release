﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.Common.UPS;

namespace AdminTool.Models.ModelViews.Member
{
    public class TransactionHistory : MemberBase
    {
        #region Properties

        public List<AccountHistory> AllAccountHistory { get; set; }

        #endregion

        #region Constructor

        public TransactionHistory() : base()
        {

        }

        #endregion
    }
}
