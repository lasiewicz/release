﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.Member
{
    public class ProfileDisplaySettings : MemberBase
    {
        public bool ShowOnline { get; set; }
        public bool ShowInSearches { get; set; }
        public bool ShowActivityTrail { get; set; }
        public bool ShowPhotosToNonMembers { get; set; }

        public ProfileDisplaySettings(): base()
        {
            ShowOnline = true;
            ShowInSearches = true;
            ShowActivityTrail = true;
        }
    }
}
