﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.CommonLibrary;

namespace AdminTool.Models.ModelViews.Member
{
    public class MemberBase : ModelViewBase
    {
        #region Properties
        public int MemberID { get; set; }
        public MemberSiteInfo MemberSiteInfo { get; set; }
        public int BrandID { get; set; }
        public string MemberEmail { get; set; }
        #endregion

        #region Constructor
        public MemberBase() : base()
        {
        }
        #endregion

        public string GetConnectSiteName()
        {
            string siteName = "";

            if (MemberSiteInfo != null)
            {
                switch (MemberSiteInfo.SiteID)
                {
                    case SiteIDs.JDate:
                        siteName = "jdate";
                        break;
                    case SiteIDs.Spark:
                        siteName = "american";
                        break;
                    case SiteIDs.JDateUK:
                        siteName = "jdateuk";
                        break;
                    case SiteIDs.BlackMingle:
                        siteName = "blackbh";
                        break;
                    case SiteIDs.BBW:
                        siteName = "bbwbh";
                        break;
                    case SiteIDs.ItalianMingle:
                        siteName = "italianbh";
                        break;

                }
            }

            return siteName;
        }

        public string GetTemplateID()
        {
            string templateID = "";

            if (MemberSiteInfo != null)
            {
                switch (MemberSiteInfo.SiteID)
                {
                    case SiteIDs.NRGDating:
                        templateID = "11";
                        break;
                    default:
                        templateID = "15";
                        break;
                }
            }

            return templateID;
        }
    }
}
