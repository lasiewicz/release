﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.Admin;

namespace AdminTool.Models.ModelViews.Member
{
    public class AdminActionLog : MemberBase
    {
        #region Properties
        public AdminActivityContainerCollection ActivityContainerCollection { get; set; }

        #endregion

        #region Constructor
        public AdminActionLog(): base()
        { }
        #endregion
    }
}
