﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet;

namespace AdminTool.Models.ModelViews.Member
{
    public class EditProfileData : MemberBase
    {
        public RegionPicker RegionPickerData;
        public GenderPicker GenderPickerData;
        public bool DisplayRegionPicker { get; set; }
        public bool DisplayGenderPicker { get; set; }
        public int ControlType { get; set; }
        public string Message { get; set; }

        public List<FreeTextAttribute> FreeTextAttributes = new List<FreeTextAttribute>();
        public List<NumberTextAttribute> NumberTextAttributes = new List<NumberTextAttribute>();
        public List<DateTimeAttribute> DateTimeAttributes = new List<DateTimeAttribute>();
        public List<BooleanAttribute> BooleanAttributes = new List<BooleanAttribute>();
        public List<SingleSelectAttribute> SingleSelectAttributes = new List<SingleSelectAttribute>();
        public List<MultiSelectAttribute> MultiSelectAttributes = new List<MultiSelectAttribute>();
    }

    public abstract class ProfileAttribute
    {
        public int AttributeID { get; set; }
        public string AttributeName { get; set; }

        public ProfileAttribute(int attributeID, string attributeName)
        {
            this.AttributeID = attributeID;
            this.AttributeName = attributeName;
        }
    }

    public class FreeTextAttribute : ProfileAttribute
    {
        public string AttributeValue { get; set; }
        public int MaxLength { get; set; }

        public FreeTextAttribute(int attributeID, string attributeName, string attributeValue, int maxLength) : base(attributeID, attributeName)
        {
            this.AttributeValue = attributeValue;
            this.MaxLength = maxLength;
        }
    }

    public class NumberTextAttribute : ProfileAttribute
    {
        public int AttributeValue { get; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }

        public NumberTextAttribute(int attributeID, string attributeName, int attributeValue)
            : base(attributeID, attributeName)
        {
            this.AttributeValue = attributeValue;
            this.MinValue = Constants.NULL_INT;
            this.MaxValue = Constants.NULL_INT;
        }

    }

    public class DateTimeAttribute : ProfileAttribute
    {
        public DateTime AttributeValue { get; set; }

        public DateTimeAttribute(int attributeID, string attributeName, DateTime attributeValue)
            : base(attributeID, attributeName)
        {
            this.AttributeValue = attributeValue;
        }
    }

    public class BooleanAttribute : ProfileAttribute
    {
        public bool AttributeValue { get; set; }

        public BooleanAttribute(int attributeID, string attributeName, bool attributeValue)
            : base(attributeID, attributeName)
        {
            this.AttributeValue = attributeValue;
        }
    }

    public class SingleSelectAttribute : ProfileAttribute
    {
        public int AttributeValue { get; set; }
        public List<SelectListItem> OptionChoices = new List<SelectListItem>();

        public SingleSelectAttribute(int attributeID, string attributeName, int attributeValue)
            : base(attributeID, attributeName)
        {
            this.AttributeValue = attributeValue;
        }
    }

    public class MultiSelectAttribute : ProfileAttribute
    {
        public int AttributeValue { get; set; }
        public List<SelectListItem> OptionChoices = new List<SelectListItem>();

        public MultiSelectAttribute(int attributeID, string attributeName, int attributeValue)
            : base(attributeID, attributeName)
        {
            this.AttributeValue = attributeValue;
        }
    }

    
}
