﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.Member
{
    public class AdminAdjustInfo : MemberBase
    {
        public bool HasPremiumHighlightedProfile { get; set; }
        public bool HasPremiumMemberSpotlight { get; set; }
        public bool HasPremiumJMeter { get; set; }
        public bool HasPremiumAllAccess { get; set; }
        public bool HasPremiumReadReceipts { get; set; }

        public AdminAdjustInfo(): base()
        {

        }
    }


}
