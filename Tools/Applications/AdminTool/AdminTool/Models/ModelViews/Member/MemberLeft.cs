﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.CommonLibrary;

namespace AdminTool.Models.ModelViews.Member
{
    public class MemberLeft : MemberBase
    {
        #region Properties
        public string PhotoURL { get; set; }
        public List<MemberSiteInfo> OtherMemberSites { get; set; }
        public string PRM { get; set; }
        public string LuggageID { get; set; }
        public string LandingPageID { get; set; }
        public string BannerID { get; set; }
        public UserCapabilities UserCapabilities { get; set; }
        #endregion

        #region Constructor
        public MemberLeft() : base()
        {
            PhotoURL = "";
            OtherMemberSites = new List<MemberSiteInfo>();
        }

        #endregion
    }
}
