﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.Common.CatalogService;

namespace AdminTool.Models.ModelViews.Member
{
    public class PauseSubscriptionHistory : MemberBase
    {
        public int PlanDuration { get; set; }
        public int PlanDurationType { get; set; }
        public DateTime PauseDate { get; set; }
        public DateTime ResumeDate { get; set; }

        public PauseSubscriptionHistory() : base()
        {

        }

        public string PlanDescription
        {
            get
            {
                return Convert.ToString(PlanDuration) + " " + Enum.GetName(typeof(DurationType), PlanDurationType);
            }
        }
    }
}
