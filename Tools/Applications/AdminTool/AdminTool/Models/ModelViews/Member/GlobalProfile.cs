﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet.Content.ValueObjects.Admin;

namespace AdminTool.Models.ModelViews.Member
{
    public class GlobalProfile : MemberBase
    {
        #region Properties
        public bool AdminSuspended { get; set; }
        public bool BadEmail { get; set; }
        public bool EmailVerified { get; set; }
        public bool OnDNE { get; set; }
        public List<SelectListItem> AdminSuspendReasons { get; private set; }
        public AdminActionReasonID LastSuspendedReasonID { get; set; }
        #endregion

        #region Constructor
        public GlobalProfile()
            : base()
        {

        }

        public GlobalProfile(bool adminSuspended, AdminActionReasonID lastSuspendedReasonID)
            : base()
        {
            AdminSuspended = adminSuspended;
            BadEmail = false;
            EmailVerified = false;
            OnDNE = false;
            LastSuspendedReasonID = lastSuspendedReasonID;
            AdminSuspendReasons = GeneralHelper.GetAdminSuspendReasons(lastSuspendedReasonID, adminSuspended, false);
        }
        #endregion
    }
}
