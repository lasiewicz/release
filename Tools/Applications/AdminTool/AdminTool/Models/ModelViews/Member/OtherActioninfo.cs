﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.Member
{
    public class OtherActioninfo : MemberBase
    {
        #region Properties
        public bool CompletedColorQuiz { get; set; }
        public bool ColorAnalysisPurchase { get; set; }
        public bool Authenticated { get; set; }
        public bool ProfileHighlight { get; set; }
        public bool JMeterEditable { get; set; }
        public bool IsLifetimeMember { get; set; }
        public bool HasRamahBadge { get; set; }
        #endregion

        #region Constructor
        public OtherActioninfo(): base()
        {
        }
        #endregion
    }
}
