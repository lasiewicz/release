﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.Member
{
    public class ILAuthenticated : MemberBase
    {
        public string Username { get; set; }
        public string Status { get; set; }
        public DateTime ExpirationDate { get; set; }
        public bool Disable { get; set; }

        public ILAuthenticated() : base() 
        {}
    }
}