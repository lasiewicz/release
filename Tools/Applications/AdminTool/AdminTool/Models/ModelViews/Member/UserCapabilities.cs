﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.Member
{
    public class UserCapabilities
    {
        public string UserAgent { get; set; }
        public string FlashVersion { get; set; }
        public DateTime InformationCapturedDate { get; set; }

        public UserCapabilities() { }

        public UserCapabilities(string userAgent, string flashVersion, DateTime informationCapturedDate)
        {
            UserAgent = userAgent;
            FlashVersion = flashVersion;
            InformationCapturedDate = informationCapturedDate;
        }
    }
}
