﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminTool.Models.IPLocation;
using Spark.Common.FraudService;

namespace AdminTool.Models.ModelViews.Member
{
    public class FraudAbuseInfo : MemberBase
    {
        #region Properties
        public bool SubscriptionFraud { get; set; }
        public bool ChatBanned { get; set; }
        public bool EmailBanned { get; set; }
        public string IPAddress { get; set; }
        public bool SignedPledge { get; set; }
        public FraudDetailedResponse KountFraudResult { get; set; }
        public Location IPLocation { get; set; }
        public TimeSpan TimeBetweenRegAndFirstSub { get; set; }
        #endregion

        #region Constructor
        public FraudAbuseInfo(): base()
        {
            IPAddress = string.Empty;
        }
        #endregion
    }
}
