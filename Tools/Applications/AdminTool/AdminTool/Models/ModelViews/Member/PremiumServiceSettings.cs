﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminTool.Models.ModelViews.Member
{
    public class PremiumServiceSettings : MemberBase
    {
        #region Properties

        public bool HasHighlightProfile { get; set; }
        public bool HiglightEnabled { get; set; }
        public bool HasSpotlightProfile { get; set; }
        public bool SpotlightEnabled { get; set; }
        public int SpotlightAgeMin { get; set; }
        public int SpotlightAgeMax { get; set; }
        public int SpotlightDistance { get; set; }
        public Enums.Gender SpotlightGender { get; set; }
        public Enums.SeekingGender SpotlightSeekingGender { get; set; }
        public int SpotlightRegionID { get; set; }
        public int SpotlightDepth1RegionID { get; set; }
        public int SpotlightDepth2RegionID { get; set; }
        public int SpotlightDepth3RegionID { get; set; }
        public List<SelectListItem> SpotlightDistanceOptions { get; set; }
        public SearchRegionPicker SpotlightRegionPicker { get; set; }
        
        #endregion

        #region Constructor

        #endregion
    }
}
