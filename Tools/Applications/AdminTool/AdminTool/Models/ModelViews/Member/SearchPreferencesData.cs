﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.SAL;
using System.Web.Mvc;

namespace AdminTool.Models.ModelViews.Member
{
    public class SearchPreferencesData : MemberBase
    {
        public string UIMessage { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public GenderPicker GenderData { get; set; }

        public int Distance { get; set; }
        public List<SelectListItem> DistanceOptions = new List<SelectListItem>();
        public SearchRegionPicker SearchRegionPicker { get; set; }

        public bool HasPhotos { get; set; }

        public int MinHeight { get; set; }
        public int MaxHeight { get; set; }
        public string KeywordSearch { get; set; }
        public List<SelectListItem> MinHeightOptions = new List<SelectListItem>();
        public List<SelectListItem> MaxHeightOptions = new List<SelectListItem>();

        public List<SearchPreference> BasicSearchPrefs = new List<SearchPreference>();
        public List<SearchPreference> AdditionalSearchPrefs = new List<SearchPreference>();
    }

    public class SearchPreference
    {
        public string Name { get; set; }
        public string SectionName { get; set; }
        public string SubSectionName { get; set; }
        public string ResourceConstant { get; set; }
        public bool Collapsible { get; set; }

        public int SelectedValue { get; set; }
        public List<SelectListItem> Options = new List<SelectListItem>();
    }
}
