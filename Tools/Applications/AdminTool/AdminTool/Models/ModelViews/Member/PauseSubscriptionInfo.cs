﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.Common.AccessService;
using Spark.Common.RenewalService;
using System.Text;
using Spark.Common.CatalogService;

namespace AdminTool.Models.ModelViews.Member
{
    public class PauseSubscriptionInfo : MemberBase
    {
        // For both before pausing the subscription and while the subscription is frozen 
        public int remainingBasicSubscriptionMinutes { get; set; }
        // For both before pausing the subscription and while the subscription is frozen 
        public List<string> customerPrivilegeDescriptions { get; set; }
        
        /*        
        // For before the subscription is frozen 
        public List<AccessPrivilege> AllCustomerPrivileges { get; set; }
        // For while the subscription is frozen 
        public List<AccessFrozenPrivilege> AllCustomerFrozenPrivileges { get; set; }
        */

        // For both before pausing the subscription and while the subscription is frozen 
        public bool lastSavedRenewalIsOpen { get; set; }
        // For both before pausing the subscription and while the subscription is frozen 
        public List<PauseSubscriptionHistory> FrozenTransactionHistory { get; set; }
        // For both before pausing the subscription and while the subscription is frozen 
        public RenewalInfo CurrentRenewalInfo { get; set; }
        public bool HasPausedSubscription { get; set; }
        public int MaximumTimeLimitForPauseInMinutes { get; set; }
        public bool IsPauseAllowed { get; set; }

        public PauseSubscriptionInfo(bool hasPausedSubscription, bool isPausedAllowed) : base()
        {
            HasPausedSubscription = hasPausedSubscription;
            MaximumTimeLimitForPauseInMinutes = 525600; // One year by default 
            IsPauseAllowed = isPausedAllowed;
        }

        public int RemainingBasicSubscriptionDays 
        {
            get
            {
                decimal days = remainingBasicSubscriptionMinutes / 1440;
                return Convert.ToInt32(Math.Ceiling(days));
            }
        }

        public string CustomerPrivilegeDescriptionList
        {
            get
            {
                if (customerPrivilegeDescriptions == null)
                {
                    return String.Empty;
                }
                else if (customerPrivilegeDescriptions.Count <= 0)
                {
                    return String.Empty;
                }
                else
                {
                StringBuilder s = new StringBuilder();
                if (null != customerPrivilegeDescriptions)
                {
                foreach (string privilegeDescription in customerPrivilegeDescriptions)
                {
                    if (s.ToString().Length > 0)
                    {
                        s.Append(" <br/>");
                    }

                    s.Append(privilegeDescription);
                }
                }
                return s.ToString();               
            }
        }
        }

        public string CurrentRenewalPlanInformation
        {
            get
            {
                StringBuilder s = new StringBuilder();

                if (CurrentRenewalInfo != null)
                {
                    s.Append("Initial duration: ");
                    s.Append(Convert.ToString(CurrentRenewalInfo.RenewalPlanInitialDuration));
                    s.Append(" ");
                    s.Append(Enum.GetName(typeof(DurationType), (DurationType)CurrentRenewalInfo.RenewalPlanInitialDurationType));
                    s.Append("<br/>");
                    s.Append("Initial total cost: ");
                    s.Append(String.Format("{0:#0.00}", CurrentRenewalInfo.RenewalPlanInitialAmount));
                    s.Append("<br/>");
                    s.Append("Renewal duration: ");
                    s.Append(Convert.ToString(CurrentRenewalInfo.RenewalPlanRenewalDuration));
                    s.Append(" ");
                    s.Append(Enum.GetName(typeof(DurationType), (DurationType)CurrentRenewalInfo.RenewalPlanRenewalDurationType));
                    s.Append("<br/>");
                    s.Append("Renewal cost: ");
                    s.Append(String.Format("{0:#0.00}", CurrentRenewalInfo.RenewalPlanRenewalAmount));
                }
                else
                {
                    s.Append("There is no active renewal subscription");
                }

                return s.ToString();
            }
        }

        public string SubscriptionPauseEndsOn
        {
            get
            {
                return String.Format("{0:M/d/yyyy}", DateTime.Now.AddMinutes(MaximumTimeLimitForPauseInMinutes));
            }
        }
    }
}
