﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.ExternalMail.ValueObjects;

namespace AdminTool.Models.ModelViews.Member
{
    public class OffSiteNotifications : MemberBase
    {
        public int MatchMailFrequency { get; set; }
        public int ClickMailFrequency { get; set; }
        
        public int EmailAlertMaskValues { get; set; }

        public bool RequestEsssayAlert
        {
            get
            {
                return (EmailAlertMaskValues &
                        (int) EmailAlertMask.NoEssayRequestEmail) == (int) EmailAlertMask.NoEssayRequestEmail;
            }
            set
            {
                if (value)
                {
                    EmailAlertMaskValues |= (int) EmailAlertMask.NoEssayRequestEmail;
                }
                else
                {
                    EmailAlertMaskValues &= ~(int) EmailAlertMask.NoEssayRequestEmail;
                }
            }
        }
        
        public bool ClickAlert {
            get { return (EmailAlertMaskValues & (int)EmailAlertMask.GotClickAlert) == (int)EmailAlertMask.GotClickAlert; }
            set
            {
                if (value == true)
                {
                    EmailAlertMaskValues |= (int)EmailAlertMask.GotClickAlert;
                }
                else
                {
                    EmailAlertMaskValues &= ~(int)EmailAlertMask.GotClickAlert;
                }
            }
        }
        public bool EmailAlert {
            get { return (EmailAlertMaskValues & (int)EmailAlertMask.NewEmailAlert) == (int)EmailAlertMask.NewEmailAlert; }
            set
            {
                if (value == true)
                {
                    EmailAlertMaskValues |= (int)EmailAlertMask.NewEmailAlert;
                }
                else
                {
                    EmailAlertMaskValues &= ~(int)EmailAlertMask.NewEmailAlert;
                }
            }
        }
        public bool EcardAlert {
            get { return (EmailAlertMaskValues & (int)EmailAlertMask.ECardAlert) == (int)EmailAlertMask.ECardAlert; }
            set
            {
                if (value == true)
                {
                    EmailAlertMaskValues |= (int)EmailAlertMask.ECardAlert;
                }
                else
                {
                    EmailAlertMaskValues &= ~(int)EmailAlertMask.ECardAlert;
                }
            }
        }
        public bool HotlistAlert
        {
            get
            {
                return (EmailAlertMaskValues & (int) EmailAlertMask.HotListedAlert) ==
                       (int) EmailAlertMask.HotListedAlert;
            }
            set
            {
                if(value)
                {
                    EmailAlertMaskValues |= (int) EmailAlertMask.HotListedAlert;
                }
                else
                {
                    EmailAlertMaskValues &= ~(int)EmailAlertMask.HotListedAlert;
                }
            }
        }
        public bool ProfileViewedAlert
        {
            get { return (EmailAlertMaskValues & (int) EmailAlertMask.ProfileViewedAlertOptOut) == 0; }
            set
            {
                if(value)
                {
                    EmailAlertMaskValues &= ~(int)EmailAlertMask.ProfileViewedAlertOptOut;
                }
                else
                {
                    EmailAlertMaskValues |= (int)EmailAlertMask.ProfileViewedAlertOptOut;
                }
            }
        }

        public int NewsEventOfferMaskValues { get; set; }
        public bool NewsletterAlert {
            get { return (NewsEventOfferMaskValues & (int)NewsEventOfferMask.News) == (int)NewsEventOfferMask.News; }
            set
            {
                if (value == true)
                {
                    NewsEventOfferMaskValues |= (int)NewsEventOfferMask.News;
                }
                else
                {
                    NewsEventOfferMaskValues &= ~(int)NewsEventOfferMask.News;
                }
            }
        }
        public bool TravelEventsAlert {
            get { return (NewsEventOfferMaskValues & (int)NewsEventOfferMask.Events) == (int)NewsEventOfferMask.Events; }
            set
            {
                if (value == true)
                {
                    NewsEventOfferMaskValues |= (int)NewsEventOfferMask.Events;
                }
                else
                {
                    NewsEventOfferMaskValues &= ~(int)NewsEventOfferMask.Events;
                }
            }
        }
        public bool QualifiedPartnersAlert {
            get { return (NewsEventOfferMaskValues & (int)NewsEventOfferMask.Offers) == (int)NewsEventOfferMask.Offers; }
            set
            {
                if (value == true)
                {
                    NewsEventOfferMaskValues |= (int)NewsEventOfferMask.Offers;
                }
                else
                {
                    NewsEventOfferMaskValues &= ~(int)NewsEventOfferMask.Offers;
                }
            }
        }

        public OffSiteNotifications()
            : base()
        {
            
        }
    } 
}
