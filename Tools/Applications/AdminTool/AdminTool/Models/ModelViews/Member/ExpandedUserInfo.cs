﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminTool.Models.IPLocation;
using Spark.Common.FraudService;

namespace AdminTool.Models.ModelViews.Member
{
    public class ExpandedUserInfo : MemberBase
    {
        #region Properties
        //public int MemberId { get; set; }
        public int SiteId { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string GenderSeekingGender { get; set; }
        public string MaritalStatus { get; set; }
        public string Occupation { get; set; }
        public string Education { get; set; }
        public string Religion { get; set; }
        public string Ethnicity { get; set; }
        public string ProfileLocation { get; set; }
        public string IPAddress { get; set; }
        public Location IPLocation { get; set; }
        public string ThumbnailUrl { get; set; }
        //public MingleFraudResult FraudResult { get; set; }
        //JS-1326
        public FraudDetailedResponse FraudResult { get; set; }
        // non display properties
        public int ProfileRegionID { get; set; }
        public int GenderMask { get; set; }
        public int MaritalStatusIntValue { get; set; }
        public int EducationIntValue { get; set; }
        public int ReligionIntValue { get; set; }
        public int EthnicityIntValue { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string SubscriberStatus { get; set; }
        public int DaysSinceFirstSubscription { get; set; }
        public string BillingPhoneNumber { get; set; }

        #endregion

        public ExpandedUserInfo() : base()
        {
            DaysSinceFirstSubscription = -1;
        }
    }
}
