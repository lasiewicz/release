﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Session.ValueObjects;

namespace AdminTool.Models.ModelViews.Member
{
    public class LastFewClientIPs
    {
        public List<SessionTracking> SessionTrackings { get; set; } 
    }
}