﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace AdminTool.Models.ModelViews.PhotoUpload
{
    public class MemberPhotos : ModelViewBase
    {
        public List<MemberPhoto> PhotosList { get; set; }
        public int MaxPhotoCount { get; set; }
        public int MemberID { get; set; }
        public string Username { get; set; }
        public bool DisplayPhotosToGuests { get; set; }
        public string ValidationMessage { get; set; }


        public MemberPhotos()
        {
            PhotosList = new List<MemberPhoto>();
        }
    }

}
