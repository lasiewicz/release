﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace AdminTool.Models.ModelViews.PhotoUpload
{
    public class MemberPhoto
    {
        public int AdminMemberID { get; set; }
        public int AlbumID { get; set; }
        public string Caption { get; set; }
        public int FileID { get; set; }
        public string FilePath { get; set; }
        public string FileWebPath { get; set; }
        public bool IsApproved { get; set; }
        public bool IsCaptionApproved { get; set; }
        public bool IsDirty { get; set; }
        public string IsPrivateChecked { get; set; }
        public byte ListOrder { get; set; }
        public int MemberPhotoID { get; set; }
        public int ThumbFileID { get; set; }
        public string ThumbFilePath { get; set; }
        public string ThumbFileWebPath { get; set; }
        public List<ListItem> OrderList { get; set; }
        public int SiteID { get; set; }
        public int PhotosListID { get; set; }
        public int MemberID { get; set; }
        public bool IsPrivatePhotoVisible { get; set; }
        public string IsApprovedForMainChecked { get; set; }
        public string IsMainChecked { get; set; }

        public MemberPhoto()
        {
            OrderList = new List<ListItem>();
        }
    }
}
