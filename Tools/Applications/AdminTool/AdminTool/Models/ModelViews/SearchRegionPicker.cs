﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.SAL;

namespace AdminTool.Models.ModelViews
{
    public class SearchRegionPicker : ModelViewBase
    {
        public int RegionID { get; set; }
        public int SiteID { get; set; }
        public SearchType SearchType { get; set; }
        public string FormFieldName = "SearchRegionPicker01";
        public string CountryRegionIDFormFieldName = "SearchCountryRegionID01";
        public string SearchTypeFormFieldName = "SearchTypeFormField01";
        public int CountryRegionID { get; set; }
        public List<int> AreaCodes = new List<int>();
        public bool LockSearchType { get; set; }
    }
}
