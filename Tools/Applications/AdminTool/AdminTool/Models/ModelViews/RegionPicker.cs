﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews
{
    public class RegionPicker : ModelViewBase
    {
        public int RegionID { get; set; }
        public int SiteID { get; set; }
        public string FormFieldName = "RegionPicker01";
    }
}
