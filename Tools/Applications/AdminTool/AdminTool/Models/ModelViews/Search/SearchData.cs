﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using Spark.CommonLibrary;
using System.Web.Mvc;
using System.Text;
using System.Configuration;

namespace AdminTool.Models.ModelViews.Search
{
    public enum AdminSearchFilterType
    {
        None = 0,
        Active = 1,
        Subscriber = 2,
        NonSubscriber = 3,
        AdminSuspended = 4,
        SelfSuspended = 5
    }

    public class SearchData : ModelViewBase
    {
        private int _MemberID = 0;
        private int _PageNumber = 1;
        private bool _exactUsernameOnly = false;

        #region Properties
        public bool LegacyEnabled { get; set; }
        public bool PerformLegacySearch { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public int MemberID { get { return _MemberID; } set { _MemberID = value; } }
        public SiteIDs SiteID { get; set; }
        public int CommunityID { get; set; }
        public List<SelectListItem> CommunityIDChoices { get; set; } 
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ZipCode { get; set; }
        public string CityName { get; set; }
        public string PhoneNumber { get; set; }
        public IPAddress IPAddress
        {
            get
            {
                if (string.IsNullOrEmpty(IP1) || string.IsNullOrEmpty(IP2) || string.IsNullOrEmpty(IP3) || string.IsNullOrEmpty(IP4))
                    return null;

                var ipString = string.Format("{0}.{1}.{2}.{3}", IP1, IP2, IP3, IP4);
                return IPAddress.Parse(ipString);
            }
        }
        public int? IPAddressInt
        {
            get
            {
                if (IPAddress == null)
                    return null;

                return BitConverter.ToInt32(IPAddress.GetAddressBytes(),0);
            }
        }
        public string IP1 { get; set; }
        public string IP2 { get; set; }
        public string IP3 { get; set; }
        public string IP4 { get; set; }
        public int PageNumber { get { return _PageNumber; } set { _PageNumber = value; } }
        public int ResultsPerPage { get; set; }
        public bool ExactUsernameOnly { get { return _exactUsernameOnly; } set { _exactUsernameOnly = value; } }
        public string VirtualTerminalUrl { get; set; }

        //filter mask
        public AdminSearchFilterType FilterValue { get; set; }

        //model view
        public SearchResult SearchResult { get; set;}
        public SharedTopNav TopNav { get; set; }
        #endregion 

        #region Constructors
        public SearchData() : base()
        {
            Email = "";
            UserName = "";
            SiteID = SiteIDs.None;
            FirstName = "";
            LastName = "";
            ZipCode = "";
            CityName = "";
            PhoneNumber = "";
            TopNav = new SharedTopNav();
            SearchResult = new SearchResult();
            ResultsPerPage = Convert.ToInt32(ConfigurationManager.AppSettings["SearchResultPerPage"]);
            SearchResult.ResultsPerPage = ResultsPerPage;
            ExactUsernameOnly = false;
            VirtualTerminalUrl = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.Instance.GetSettingFromSingleton("VIRTUAL_TERMINAL_URL");
            LegacyEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.Instance.SettingExistsFromSingleton("ENVIRONMENT_TYPE", 0, 0);

            CommunityIDChoices = GeneralHelper.GetCommunityIDChoices("Any", "0");
        }
        #endregion

        #region Static Methods
        public static List<SelectListItem> GetSiteListItems(int? selectedID)
        {
            return GeneralHelper.GetSitesListItems(selectedID);
        }
        #endregion

        #region Public Methods
        public void PopulateSearchData(FormCollection formData)
        {
            if (!String.IsNullOrEmpty(formData["Page"])) { int.TryParse(formData["Page"], out _PageNumber); }
            if (!String.IsNullOrEmpty(formData["MemberID"])) { int.TryParse(formData["MemberID"], out _MemberID); }
            if (!String.IsNullOrEmpty(formData["Email"])) { Email = formData["Email"]; }
            if (!String.IsNullOrEmpty(formData["UserName"])) { UserName = formData["UserName"]; }
            if (!String.IsNullOrEmpty(formData["PhoneNumber"])) { PhoneNumber = formData["PhoneNumber"]; }
            if (!String.IsNullOrEmpty(formData["ExactUsernameOnly"])) { ExactUsernameOnly = (formData["ExactUsernameOnly"].ToLower() != "false"); }
            //if (!String.IsNullOrEmpty(formData["LegacyEnabled"])) { LegacyEnabled = (formData["LegacyEnabled"].ToLower() != "false"); }
            if (!String.IsNullOrEmpty(formData["PerformLegacySearch"])) { PerformLegacySearch = (formData["PerformLegacySearch"].ToLower() != "false"); }

            FirstName = String.IsNullOrEmpty(formData["FirstName"]) ? string.Empty : formData["FirstName"];
            LastName = String.IsNullOrEmpty(formData["LastName"]) ? string.Empty : formData["LastName"];
            ZipCode = String.IsNullOrEmpty(formData["ZipCode"]) ? string.Empty : formData["ZipCode"];
            CityName = String.IsNullOrEmpty(formData["CityName"]) ? string.Empty : formData["CityName"];
            IP1 = String.IsNullOrEmpty(formData["IP1"]) ? string.Empty : formData["IP1"];
            IP2 = String.IsNullOrEmpty(formData["IP2"]) ? string.Empty : formData["IP2"];
            IP3 = String.IsNullOrEmpty(formData["IP3"]) ? string.Empty : formData["IP3"];
            IP4 = String.IsNullOrEmpty(formData["IP4"]) ? string.Empty : formData["IP4"];
            CommunityID = String.IsNullOrEmpty(formData["CommunityID"]) ? 0 : int.Parse(formData["CommunityID"]);

            var searchFilter = formData["SearchFilter"];
            FilterValue = (AdminSearchFilterType)(searchFilter == null ? 0 : Convert.ToInt32(searchFilter));
            
            if (_PageNumber < 1)
            {
                _PageNumber = 1;
            }
        }

        public string GetSearchParameterQueryString(int PageNumber)
        {
            StringBuilder query = new StringBuilder();

            query.Append("Page=" + PageNumber.ToString());

            //search fields
            if (MemberID > 0) { query.Append("&MemberID=" + MemberID.ToString()); }
            if (!String.IsNullOrEmpty(Email)) { query.Append("&Email=" + HttpContext.Current.Server.UrlEncode(Email)); }
            if (!String.IsNullOrEmpty(UserName)) { query.Append("&UserName=" + HttpContext.Current.Server.UrlEncode(UserName)); }
            if (!String.IsNullOrEmpty(PhoneNumber)) { query.Append("&PhoneNumber=" + HttpContext.Current.Server.UrlEncode(PhoneNumber)); }
            query.Append("&ExactUsernameOnly=" + HttpContext.Current.Server.UrlDecode(ExactUsernameOnly.ToString()));
            //query.Append("&LegacyEnabled=" + HttpContext.Current.Server.UrlDecode(LegacyEnabled.ToString()));
            query.Append("&PerformLegacySearch=" + HttpContext.Current.Server.UrlDecode(PerformLegacySearch.ToString()));
            if (!string.IsNullOrEmpty(FirstName))
            {
                query.Append("&FirstName=" + HttpContext.Current.Server.UrlEncode(FirstName));
            }
            if (!string.IsNullOrEmpty(LastName))
            {
                query.Append("&LastName=" + HttpContext.Current.Server.UrlEncode(LastName));
            }
            if (!string.IsNullOrEmpty(ZipCode))
            {
                query.Append("&ZipCode=" + HttpContext.Current.Server.UrlEncode(ZipCode));
            }
            if (!string.IsNullOrEmpty(CityName))
            {
                query.Append("&CityName=" + HttpContext.Current.Server.UrlEncode(CityName));
            }
            if (!string.IsNullOrEmpty(IP1))
            {
                query.Append("&IP1=" + HttpContext.Current.Server.UrlEncode(IP1));
            }
            if (!string.IsNullOrEmpty(IP2))
            {
                query.Append("&IP2=" + HttpContext.Current.Server.UrlEncode(IP2));
            }
            if (!string.IsNullOrEmpty(IP3))
            {
                query.Append("&IP3=" + HttpContext.Current.Server.UrlEncode(IP3));
            }
            if (!string.IsNullOrEmpty(IP4))
            {
                query.Append("&IP4=" + HttpContext.Current.Server.UrlEncode(IP4));
            }
            query.Append("&CommunityID=" + HttpContext.Current.Server.UrlEncode(CommunityID.ToString()));
            query.Append("&SearchFilter="+ HttpContext.Current.Server.UrlEncode(((int)FilterValue).ToString()));

            return query.ToString();
        }

        #endregion

    }
}
