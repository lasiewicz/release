﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.Search
{
    public class SearchResult : ModelViewBase
    {
        #region Properties
        public List<SearchMember> SearchMembers { get; set; }
        public int TotalRows { get; set; }
        public int TotalPages { get; set; }
        public int ResultsPerPage { get; set; }
        public bool OnlyOneResultReturned { get; set; }
        public bool SearchResultEndReached { get; set; }
        public bool isSearched { get; set; } //differentiates empty results versus no search
        #endregion

        #region Constructors
        public SearchResult() : base()
        {
            isSearched = false;
            SearchMembers = new List<SearchMember>();
           
        }

        #endregion
    }
}
