﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.CommonLibrary;

namespace AdminTool.Models.ModelViews.Search
{
    public class SearchMember
    {
        #region Properties
        public string EmailAddress { get; set; }
        public string UserName { get; set; }
        public int MemberID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }
        public string Location { get; set; }
        public List<MemberSiteInfo> MemberSites { get; set; }

        #endregion

        #region Constructors
        public SearchMember()
        {
            MemberSites = new List<MemberSiteInfo>();

        }
        #endregion

    }
}
