﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Email.ValueObjects;
using AdminTool.Models.ModelViews.Member;
using System.Web.Mvc;

namespace AdminTool.Models.ModelViews.Email
{
    public enum EmailLogType
    {
        Both = 0,
        Inbox = 1,
        Sent = 2,
        ImHistory = 3
    }

    public class EmailList : MemberBase
    {
        public string EmailAddress { get; set; }
        public string JSMessageAttachmentURLPath { get; set; }
        public List<Email> Emails = new List<Email>();
        public int TotalEmailCount { get; set; }
        public int TotalPageCount { get; set; }
        public int TotalReturnedInPage { get; set; }
        public int FolderID { get; set; }
        public int PageSize { get; set; }
        public int RowIndex { get; set; }
        public List<SelectListItem> FolderIDChoices { get; set; }

        public int SiteID
        {
            get { return (int)this.MemberSiteInfo.SiteID; }
        }

        public EmailList()
        {
            FolderIDChoices = new List<SelectListItem>();
            FolderIDChoices.Add(new SelectListItem { Value = ((int)EmailLogType.Inbox).ToString(), Text = "Inbox" });
            FolderIDChoices.Add(new SelectListItem { Value = ((int)EmailLogType.Sent).ToString(), Text = "Sent" });
            FolderIDChoices.Add(new SelectListItem { Value = ((int)EmailLogType.Both).ToString(), Text = "Both" });
            FolderIDChoices.Add(new SelectListItem { Value = ((int)EmailLogType.ImHistory).ToString(), Text = "Im History" });
        }
    }

    public class Email
    {
        public int MessageID { get; set; }
        public MailType EmailType { get; set; }
        public DateTime MessageDate { get; set; }
        public int FromMemberID { get; set; }
        public int ToMemberID { get; set; }
        public string FromMemberString { get; set; }
        public string ToMemberString { get; set; }
        public bool IsVIP { get; set; }
        
        public string ViewText
        {
            get
            {
                return Enum.GetName(typeof(MailType), this.EmailType);
            }
        }

        public Email(int messageID, MailType emailType, DateTime messageDate, string fromString, string toString, int fromMemberID, int toMemberID, bool isVIP)
        {
            this.MessageID = messageID;
            this.EmailType = emailType;
            this.MessageDate = messageDate;
            this.FromMemberString = fromString;
            this.ToMemberString = toString;
            this.FromMemberID = fromMemberID;
            this.ToMemberID = toMemberID;
            this.IsVIP = isVIP;
        }
    }



    public class InstantMessageHistoryBase
    {
        public int GroupId { get; set; }
        public int MemberId { get; set; }
        public int MatchesFound { get; set; }
        public int code { get; set; }

    }

    public class InstantMessageHistoryMembersResponse : InstantMessageHistoryBase
    {
        public List<MailMember> MemberList { get; set; }
    }

   
    public class InstantMessageHistoryResponse : InstantMessageHistoryBase
    {
        public List<MailMessageV2> MessageList { get; set; }
    }


    public class MailMember
    {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public int FromMemberId { get; set; }
        public int ToMemberId { get; set; }
        public int GroupId { get; set; }
        public String InsertDate { get; set; }
        public int MessageStatus { get; set; }
        public string MailType { get; set; }
        public string Username { get; set; }
    }

    public class MailMessageV2
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public string MailType { get; set; }
        public string Body { get; set; }
        public string InsertDate { get; set; }
        public int MemberFolderId { get; set; }
        public bool IsAllAccess { get; set; }
        public int SenderId { get; set; }
        public int ToMemberId { get; set;}
        public int FromMemberId { get; set; }
        public string FromUserName { get; set; }
        public string ToUserName { get; set; }
    }

}
