﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Spark.CommonLibrary;

namespace AdminTool.Models.ModelViews.Approval
{
    public class MemberProfileDuplicateApprovalModelView : ModelViewBase
    {
        public string ErrorMessage { get; set; }
        public int MemberID { get; set; }
        public int LanguageID { get; set; }
        public int CommunityID { get; set; }
        public int SiteID { get; set; }
        public string SiteName { get; set; }
        public ApprovalLeft Left { get; set; }
        public SharedTopNav TopNav { get; set; }
        public QueueItemMemberProfileDuplicate QueueItem { get; set; }
        public List<MemberProfileDuplicateExtended> MemberProfileDuplicateExtended { get; set; }
    }

    public class MemberProfileDuplicateExtended : MemberProfileDuplicate
    {
        public MemberProfileDuplicate MemberProfileDuplicate { 
            set
            {
                this.MemberProfileDuplicateId = value.MemberProfileDuplicateId;
                this.MemberId = value.MemberId;
                this.CRXResponseId = value.CRXResponseId;
                this.FieldType = value.FieldType;
                this.FieldValue = value.FieldValue;
                this.FieldValueOriginationDate = value.FieldValueOriginationDate;
                this.DuplicateStatus = value.DuplicateStatus;
                this.OrdinalNumber = value.OrdinalNumber;
            }
        }
        public int TemplateID
        {
            get {
                var templateID = Constants.NULL_INT;
                switch (LastLogonSiteID)
                {
                    case (int)SiteIDs.NRGDating:
                        templateID = 11;
                        break;
                    default:
                        templateID = 15;
                        break;
                }
                return templateID;
            }
        }
        public int LastLogonSiteID { get; set; }
        public int LastBrandID
        {
            get
            {
                var brand = GeneralHelper.GetBrandForSite(LastLogonSiteID);
                return brand == null ? Constants.NULL_INT : brand.BrandID;
            }
        }
        public int TextboxHeight { get; set; }
        public bool AlreadySuspended { get; set; }
        public List<SelectListItem> AdminSuspendReasons { get; private set; }

        public MemberProfileDuplicateExtended()
        {
            AdminSuspendReasons = GeneralHelper.GetAdminSuspendReasons();
        }
    }
}