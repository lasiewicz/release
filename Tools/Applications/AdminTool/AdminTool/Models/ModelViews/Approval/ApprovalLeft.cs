﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.Common.FraudService;
using Spark.CommonLibrary;

namespace AdminTool.Models.ModelViews.Approval
{
    public class ApprovalLeft : ModelViewBase
    {
        public int MemberID { get; set; }
        public string PhotoURL { get; set; }
        public SiteIDs SiteID { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
       // public MingleFraudResult FraudResult { get; set; }
        //JS-1326
        public FraudDetailedResponse FraudResult { get; set; }
        public bool IsTextApproval { get; set; }
        public bool HasContent { get; set; }
        public List<ApprovalMemberMessage> MemberMessages {get;set;}
        public int QueueCount { get; set; }
        public int ItemsProcessed { get; set; }
        
        public ApprovalLeft() : base()
        {
            
        }
    }
}
