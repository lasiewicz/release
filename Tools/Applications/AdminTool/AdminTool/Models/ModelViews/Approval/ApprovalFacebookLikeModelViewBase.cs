﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.FacebookLike.ValueObjects;

namespace AdminTool.Models.ModelViews.Approval
{
    public class ApprovalFacebookLikeModelViewBase : ModelViewBase
    {
        public int ID { get; set; }
        public int MemberID { get; set; }
        public int SiteID { get; set; }
        public int CommunityID { get; set; }
        public List<FacebookLike> FacebookLikes { get; set; }
        public QAApprovalType ApprovalType { get; set; }
    }
}