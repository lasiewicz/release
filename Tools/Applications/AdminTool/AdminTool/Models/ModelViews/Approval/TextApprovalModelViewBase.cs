﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminTool.Models;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;

namespace AdminTool.Models.ModelViews.Approval
{
    public class TextApprovalModelViewBase : ModelViewBase
    {
        public int MemberID { get; set; }
        public int LanguageID { get; set; }
        public int CommunityID { get; set; }
        public int SiteID { get; set; }
        public TextType TextType { get; set; }
        public int MemberTextApprovalID { get; set; }

        public TextApprovalModelViewBase() { }

        public TextApprovalModelViewBase(int memberID, int languageID, int communityID, TextType textType)
        {
            MemberID = memberID;
            LanguageID = languageID;
            CommunityID = communityID;
            TextType = textType;
        }
    }
}
