﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using AdminTool.Models.ModelViews.Member;

namespace AdminTool.Models.ModelViews.Approval
{
    public class TextApprovalMemberModelView : TextApprovalModelViewBase
    {
        public MemberLeft Left { get; set; }
        public TopNav TopNav { get; set; }
        public List<TextApprovalAttribute> AttributesToApprove { get; set; }
        public List<SelectListItem> IndividualTextApprovalStatuses { get; private set; } 

        public TextApprovalMemberModelView(): base()
        {
            IndividualTextApprovalStatuses = GeneralHelper.GetIndividualTextStatuses();
        }
        public TextApprovalMemberModelView(int memberID, int languageID, int communityID, TextType textType, List<TextApprovalAttribute> attributesToApprove)
            : base(memberID, languageID, communityID, textType)
        {
            AttributesToApprove = attributesToApprove;
            IndividualTextApprovalStatuses = GeneralHelper.GetIndividualTextStatuses();
        }
    }
}
