﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.Approval
{
    public class PhotoApprovalModelViewBase : ModelViewBase
    {
        public List<PhotoApprovalData> ApprovalItems { get; set; }

        public PhotoApprovalModelViewBase()
            : base()
        {

        }

        public PhotoApprovalModelViewBase(List<PhotoApprovalData> approvalItems): base()
        {
            ApprovalItems = approvalItems;
        }
    }
}
