﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using AdminTool.Models.ModelViews.Member;

namespace AdminTool.Models.ModelViews.Approval
{
    public enum WarningReason
    {
        NoWarning = 0,
        ContactInfo = 1,
        InappropriateContent = 2,
        GeneralViolation = 3
    }

    public class TextApprovalModelView : TextApprovalModelViewBase
    {
        private List<WarningReason> _warningReasons;

        public List<TextApprovalAttribute> AttributesToApprove { get; set; }
        public ApprovalLeft Left {get;set;}
        public SharedTopNav TopNav { get; set; }
        public List<SelectListItem> AdminSuspendReasons { get; private set; }
        public List<SelectListItem> IndividualTextApprovalStatuses { get; private set; }
        public ExpandedUserInfo ExpandedUsrInfo { get; set; }
        public bool IsEnglish { get; set; }

        public List<WarningReason> WarningReasons
        {
            get
            {
                if (_warningReasons == null)
                {
                    _warningReasons = new List<WarningReason>();
                }
                return _warningReasons;
            }
            set { _warningReasons = value; }
        }

        public TextApprovalModelView(): base()
        {
            AdminSuspendReasons = GeneralHelper.GetAdminSuspendReasons();
            IndividualTextApprovalStatuses = GeneralHelper.GetIndividualTextStatuses();
        }
        public TextApprovalModelView(int memberID, int languageID, int communityID, TextType textType, List<TextApprovalAttribute> attributesToApprove): base(memberID, languageID, communityID, textType)
        {
            AttributesToApprove = attributesToApprove;
            AdminSuspendReasons = GeneralHelper.GetAdminSuspendReasons();
            IndividualTextApprovalStatuses = GeneralHelper.GetIndividualTextStatuses();
        }
    }
}
