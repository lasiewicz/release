﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;

namespace AdminTool.Models.ModelViews.Approval
{
    public class ApprovalCountTotalsModelView: ModelViewBase
    {
        public int TotalCountRegularText { get; set; }
        public int TotalCountPhoto { get; set; }
        public int TotalCountQA { get; set; }
        public int TotalCountFacebookLikes { get; set; }
        public int TotalCount { get; set; }
        public List<ApprovalCountRecordExtended> Records { get; set; }
        public Enums.ApprovalCountReportSortType SortType { get; set; }
        public Enums.SortDirection NextTextSortDirection { get; set; }
        public Enums.SortDirection NextPhotoSortDirection { get; set; }
        public Enums.SortDirection NextQASortDirection { get; set; }
        public Enums.SortDirection NextFacebookLikesSortDirection { get; set; }
        public Enums.SortDirection NextAdminMemberIDSortDirection { get; set; }
        public Enums.SortDirection NextEmailAddressSortDirection { get; set; }
        
        public ApprovalCountTotalsModelView() : base() { }
    }
}
