﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.Approval
{
    public class PhotoApprovalModelView : PhotoApprovalModelViewBase
    {
        public ApprovalLeft Left { get; set; }
        public SharedTopNav TopNav { get; set; }
        
        public PhotoApprovalModelView(): base()
        {
            
        }

        public PhotoApprovalModelView(List<PhotoApprovalData> approvalItems, ApprovalLeft left, SharedTopNav topNav)
            : base(approvalItems)
        {
            Left = left;
            TopNav = topNav;
        }
    }
}
