﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;

namespace AdminTool.Models.ModelViews.Approval
{
    public class ApprovalQueueCountTotalsModelView : ModelViewBase
    {
        public List<QueueCountRecord> CountsByCommunity { get; set; }
        public List<QueueCountRecord> CountsBySite { get; set; }
        public List<QueueCountRecord> CountsByLanguage { get; set; }
        public QueueItemType ItemType { get; set; }
        
        public ApprovalQueueCountTotalsModelView() : base() { }
    }
}
