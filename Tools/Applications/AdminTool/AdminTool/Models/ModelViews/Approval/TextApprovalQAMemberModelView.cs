﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models;
using Spark.CommonLibrary;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using AdminTool.Models.ModelViews.Member;

namespace AdminTool.Models.ModelViews.Approval
{
    public class TextApprovalQAMemberModelView : TextApprovalModelViewBase
    {
        public List<ApprovalQuestion> ApprovalQuestions { get; set; }
        public MemberLeft Left {get;set;}
        public TopNav TopNav { get; set; }
        public QAApprovalType ApprovalType { get; private set; }
        public bool ShowConfirmationMessage { get; set; }
                
        public TextApprovalQAMemberModelView(): base()
        {
            ApprovalType = QAApprovalType.MemberApproval;
        }
        public TextApprovalQAMemberModelView(int memberID, int languageID, int communityID, TextType textType, List<ApprovalQuestion> approvalQuestions)
            : base(memberID, languageID, communityID, textType)
        {
            ApprovalQuestions = approvalQuestions;
            ApprovalType = QAApprovalType.MemberApproval;
        }
    }
}
