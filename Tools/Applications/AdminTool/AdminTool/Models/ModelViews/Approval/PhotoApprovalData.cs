﻿#region

using System;
using Spark.Common.FraudService;
using Spark.CommonLibrary;

#endregion

namespace AdminTool.Models.ModelViews.Approval
{
    public class PhotoApprovalData : ModelViewBase
    {
        /// <summary>
        ///     Set some initial values to prevent possible null errors
        /// </summary>
        public PhotoApprovalData()
        {
            MemberId = 0;
            Age = 0;
            MemberPhotoId = 0;
            ThumbFileId = 0;
            ListOrder = 0;
            FileId = 0;
            PhotoUrl = string.Empty;
            Username = string.Empty;
            FraudResult = new FraudDetailedResponse();
            SiteName = string.Empty;
            Gender = string.Empty;
            Ethnicity = string.Empty;
            HairColor = string.Empty;
            BodyStyle = string.Empty;
            Height = string.Empty;
            Location = string.Empty;
            AboutMe = string.Empty;
            ProfileUrl = string.Empty;
            Caption = string.Empty;
            RegIpAddress = string.Empty;
            FileWebPath = string.Empty;
            SubscriptionStatus = string.Empty;
            CaptionApproved = 0;
            PrivateFlag = 0;
            ApprovedFlag = 0;
            PassedFraud = false;
            CommunityId = CommunityIDs.JDate;
            SiteId = SiteIDs.JDate;
            BillingPhoneNumber = string.Empty;
            InsertDate = DateTime.MinValue;
            PhotoAge = -1;
            IsApprovedForMain = false;
        }

        public int Id { get; set; }
        public int MemberId { get; set; }
        public int Age { get; set; }
        public int MemberPhotoId { get; set; }
        public int ThumbFileId { get; set; }
        public int ListOrder { get; set; }
        public int FileId { get; set; }
        public string PhotoUrl { get; set; }
        public string Username { get; set; }
        //public MingleFraudResult FraudResult { get; set; }
        //JS-1326
        //public KountFraudResult FraudResult { get; set; }
        public FraudDetailedResponse FraudResult { get; set; }
        public string SiteName { get; set; }
        public string Gender { get; set; }
        public string Ethnicity { get; set; }
        public string HairColor { get; set; }
        public string BodyStyle { get; set; }
        public string Height { get; set; }
        public string Location { get; set; }
        public string AboutMe { get; set; }
        public string ProfileUrl { get; set; }
        public string Caption { get; set; }
        public string RegIpAddress { get; set; }
        public string FileWebPath { get; set; }
        public string SubscriptionStatus { get; set; }
        public int CaptionApproved { get; set; }
        public int PrivateFlag { get; set; }
        public int ApprovedFlag { get; set; }
        public bool PassedFraud { get; set; }
        public CommunityIDs CommunityId { get; set; }
        public SiteIDs SiteId { get; set; }
        public string BillingPhoneNumber { get; set; }
        public DateTime InsertDate { get; set; }
        public int PhotoAge { get; set; }
        public bool IsApprovedForMain { get; set; }
    }
}