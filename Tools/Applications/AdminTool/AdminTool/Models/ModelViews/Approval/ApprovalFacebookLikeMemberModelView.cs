﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Spark.FacebookLike.ValueObjects;
using AdminTool.Models.ModelViews.Member;

namespace AdminTool.Models.ModelViews.Approval
{
    public class ApprovalFacebookLikeMemberModelView : ApprovalFacebookLikeModelViewBase
    {
        public MemberLeft Left { get; set; }
        public TopNav TopNav { get; set; }
                
        public ApprovalFacebookLikeMemberModelView(): base()
        {
        }

        public ApprovalFacebookLikeMemberModelView(int memberID, int siteID, int communityID, List<FacebookLike> fbLikes)
        {
            MemberID = memberID;
            SiteID = siteID;
            CommunityID = communityID;
            FacebookLikes = fbLikes;
        }
    }
}