﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminTool.Models.ModelViews.Member;

namespace AdminTool.Models.ModelViews.Approval
{
    public class PhotoApprovalMemberModelView : PhotoApprovalModelViewBase
    {
        public MemberLeft Left { get; set; }
        public TopNav TopNav { get; set; }
        
        public PhotoApprovalMemberModelView(): base()
        {
            
        }

        public PhotoApprovalMemberModelView(List<PhotoApprovalData> approvalItems, MemberLeft left, TopNav topNav)
            : base(approvalItems)
        {
            Left = left;
            TopNav = topNav;
        }
    }
}
