﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.ModelViews.Approval
{
    public class ApprovalMemberMessage
    {
        public int MemberID { get; set; }
        public string Message { get; set; }
        public bool ShowInRed { get; set; }

        public ApprovalMemberMessage() { }
        public ApprovalMemberMessage(int memberID, string message, bool showInRed)
        {
            MemberID = memberID;
            Message = message;
            ShowInRed = showInRed;
        }
    }
}
