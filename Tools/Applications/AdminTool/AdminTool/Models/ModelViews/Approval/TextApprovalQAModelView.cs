﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models;
using Spark.CommonLibrary;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;

namespace AdminTool.Models.ModelViews.Approval
{
    public class TextApprovalQAModelView : TextApprovalModelViewBase
    {
        public List<ApprovalQuestion> ApprovalQuestions { get; set; }
        public ApprovalLeft Left {get;set;}
        public SharedTopNav TopNav { get; set; }
        public List<SelectListItem> AdminSuspendReasons { get; private set; }
        public QAApprovalType ApprovalType { get; set; }
                
        public TextApprovalQAModelView(): base()
        {
            AdminSuspendReasons = GeneralHelper.GetAdminSuspendReasons();
        }
        public TextApprovalQAModelView(int memberID, int languageID, int communityID, TextType textType, List<ApprovalQuestion> approvalQuestions)
            : base(memberID, languageID, communityID, textType)
        {
            ApprovalQuestions = approvalQuestions;
            AdminSuspendReasons = GeneralHelper.GetAdminSuspendReasons();
        }
    }
}
