﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matchnet;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Spark.CommonLibrary;

namespace AdminTool.Models.ModelViews.Approval
{
    public class MemberProfileDuplicateReviewModelView : ModelViewBase
    {
        public string ErrorMessage { get; set; }
        public int MemberID { get; set; }
        public int LanguageID { get; set; }
        public int CommunityID { get; set; }
        public int SiteID { get; set; }
        public string SiteName { get; set; }
        public ApprovalLeft Left { get; set; }
        public SharedTopNav TopNav { get; set; }
        public QueueItemMemberProfileDuplicateReview QueueItem { get; set; }
        public List<MemberProfileDuplicateHistoryEntryExtended> MemberProfileDuplicateHistoryEntryExtended { get; set; }
    }

    public class MemberProfileDuplicateHistoryEntryExtended : MemberProfileDuplicateHistoryEntry
    {
        public MemberProfileDuplicateHistoryEntry MemberProfileDuplicateHistoryEntry
        {
            set
            {
                MemberId = value.MemberId;
                CRXResponseId = value.CRXResponseId;
                FieldType = value.FieldType;
                FieldValueOriginationDate = value.FieldValueOriginationDate;
                IsMatch = value.IsMatch;
                Suspended = value.Suspended;
                AlreadySuspended = value.AlreadySuspended;
                OrdinalNumber = value.OrdinalNumber;
                TextContent = value.TextContent;
            }
        }

        public bool CurrentlySuspended { get; set; }
        public int TemplateID
        {
            get {
                var templateID = Constants.NULL_INT;
                switch (LastLogonSiteID)
                {
                    case (int)SiteIDs.NRGDating:
                        templateID = 11;
                        break;
                    default:
                        templateID = 15;
                        break;
                }
                return templateID;
            }
        }
        public int LastLogonSiteID { get; set; }
        public int LastBrandID
        {
            get
            {
                var brand = GeneralHelper.GetBrandForSite(LastLogonSiteID);
                return brand == null ? Constants.NULL_INT : brand.BrandID;
            }
        }
        public int TextboxHeight { get; set; }
        public List<SelectListItem> AdminSuspendReasons { get; private set; }
        public int SuspendReasonValue { get; set; }

        public MemberProfileDuplicateHistoryEntryExtended()
        {
            AdminSuspendReasons = GeneralHelper.GetAdminSuspendReasons();
        }
    }
}