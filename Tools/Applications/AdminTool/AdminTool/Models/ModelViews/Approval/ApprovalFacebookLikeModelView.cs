﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models;
using Spark.CommonLibrary;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Spark.FacebookLike.ValueObjects;
using AdminTool.Models.ModelViews.Member;

namespace AdminTool.Models.ModelViews.Approval
{
    public class ApprovalFacebookLikeModelView : ApprovalFacebookLikeModelViewBase
    {
        public ApprovalLeft Left {get;set;}
        public SharedTopNav TopNav { get; set; }
        public List<SelectListItem> AdminSuspendReasons { get; private set; }
        public ExpandedUserInfo ExpandedUsrInfo { get; set; }
                
        public ApprovalFacebookLikeModelView(): base()
        {
            AdminSuspendReasons = GeneralHelper.GetAdminSuspendReasons();
        }
        public ApprovalFacebookLikeModelView(int memberID, int siteID, int communityID, List<FacebookLike> fbLikes)
        {
            MemberID = memberID;
            SiteID = siteID;
            CommunityID = communityID;
            FacebookLikes = fbLikes;
            AdminSuspendReasons = GeneralHelper.GetAdminSuspendReasons();
        }
    }
}
