﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminTool.Models
{
    public static class ExtensionMethods
    {
        public static string GetElementVisibility(this HtmlHelper htmlHelper, bool isVisible)
        {
            if (isVisible)
                return "display:inline";
            else
            {
                return "display:none";
            }
        }

        public static string ToYesNoString(this bool boolValue)
        {
            if (boolValue)
                return "Yes";
            else
                return "No";
        }

        public static string ToNullableString(this int value)
        {
            return value == Matchnet.Constants.NULL_INT ? "null" : value.ToString();
        }


        public static Enums.SortDirection Reverse(this Enums.SortDirection direction)
        {
            if (direction == Enums.SortDirection.ASC)
            {
                return Enums.SortDirection.DESC;
            }
            else
            {
                return Enums.SortDirection.ASC;
            }

        }
  
        public static string GetHoursMinutesString(this int totalMinutes)
        {
            int hours = (int)Math.Truncate(Convert.ToDecimal(totalMinutes / 60));
            int minutes = totalMinutes % 60;
            return string.Format("{0} Hours {1} Minutes", hours.ToString(), minutes.ToString());
        }

        public static string ToStringEmptyIfMin(this DateTime value)
        {
            if(value == DateTime.MinValue)
            {
                return string.Empty;
            }
            else
            {
                return value.ToString();
            }
        }
    }
}
