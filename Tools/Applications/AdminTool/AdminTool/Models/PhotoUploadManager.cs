﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using AdminTool.Models.ModelViews;
using AdminTool.Models.ModelViews.PhotoUpload;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Member.ValueObjects.Photos;
using Spark.CommonLibrary;
using Spark.CommonLibrary.Logging;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;

namespace AdminTool.Models
{
    public class PhotoUploadManager : ManagerBase
    {
        public int MAX_PHOTO_COUNT = 4;
        public const int MAX_FILE_SIZE = 5120000;

        public Logger _logger;
        private Guid _debugguid;

        private SiteIDs _siteID;
        private CommunityIDs _communityID;
        private int _memberID;


        public PhotoUploadManager(int memberID, SiteIDs siteID)
        {
            _logger = new Logger(this.GetType());
            _debugguid = new Guid();
            _siteID = siteID;
            _memberID = memberID;
            _communityID = GeneralHelper.GetCommunityIDForSiteID(siteID);
        }

        public MemberPhotos GetMemberPhotos()
        {
            return GetMemberPhotos(string.Empty, MemberLoadFlags.IngoreSACache);
        }

        public MemberPhotos GetMemberPhotos(string validationMessage, MemberLoadFlags memberLoadFlags)
        {
            MemberPhotos memberPhotos = new MemberPhotos();

            try
            {
                memberPhotos.ValidationMessage = validationMessage;
                var member = MemberHelper.GetMember(_memberID, memberLoadFlags);
                bool isPrivatePhotoVisible = false;
                memberPhotos.MemberID = member.MemberID;

                try
                {
                    MemberSiteInfo memberSiteInfo = MemberHelper.GetMemberSiteInfo(member, (int)_siteID);
                    Brand brand = g.GetBrand((int)memberSiteInfo.SiteID);
                    memberPhotos.MaxPhotoCount = int.Parse(RuntimeSettings.GetSetting("MAX_PHOTO_COUNT", brand.Site.Community.CommunityID, brand.Site.SiteID));
                    memberPhotos.DisplayPhotosToGuests = Convert.ToBoolean(member.GetAttributeInt(brand, "DisplayPhotosToGuests"));
                    memberPhotos.Username = member.GetUserName(brand);
                    isPrivatePhotoVisible = brand.GetStatusMaskValue(StatusType.PrivatePhotos);
                }
                catch (Exception ex)
                {
                    _logger.Error("Error in PhotoUploadManager.GetMemberPhotos() communityID: " + _communityID, ex);
                    memberPhotos.MaxPhotoCount = MAX_PHOTO_COUNT;
                }

                PhotoCommunity photoCommunity = member.GetPhotos((int)_communityID);
                int i;
                for (i = 0; i < photoCommunity.Count && i < memberPhotos.MaxPhotoCount; i++)
                {
                    Photo photo = photoCommunity[(byte)i];

                    memberPhotos.PhotosList.Add(new MemberPhoto()
                    {
                        AdminMemberID = photo.AdminMemberID,
                        AlbumID = photo.AlbumID,
                        Caption = photo.Caption,
                        FileID = photo.FileID,
                        FilePath = photo.FilePath,
                        FileWebPath = MemberPhotoHelper.GetPhotoDisplayUrl(PhotoType.Full, photo, (int)_communityID, (int)_siteID),
                        IsApproved = photo.IsApproved,
                        IsCaptionApproved = photo.IsCaptionApproved,
                        IsDirty = photo.IsDirty,
                        IsPrivateChecked = photo.IsPrivate ? "checked" : string.Empty,
                        ListOrder = photo.ListOrder,
                        MemberPhotoID = photo.MemberPhotoID,
                        ThumbFileID = photo.ThumbFileID,
                        ThumbFilePath = photo.ThumbFilePath,
                        ThumbFileWebPath = MemberPhotoHelper.GetPhotoDisplayUrl(PhotoType.Thumbnail, photo, (int)_communityID, (int)_siteID),
                        OrderList =
                            GetOrderListItems(memberPhotos.MaxPhotoCount,
                                              photo.ListOrder),
                        SiteID = (int)_siteID,
                        PhotosListID = i,
                        MemberID = _memberID,
                        IsPrivatePhotoVisible = isPrivatePhotoVisible,
                        IsApprovedForMainChecked = photo.IsApprovedForMain ? "checked" : string.Empty,
                        IsMainChecked = photo.IsMain ? "checked" : string.Empty
                    });
                }

                i--;

                while (i++ < memberPhotos.MaxPhotoCount - 1)
                {
                    memberPhotos.PhotosList.Add(new MemberPhoto()
                    {
                        OrderList = GetOrderListItems(memberPhotos.MaxPhotoCount, (byte)(i + 1)),
                        PhotosListID = i
                    });
                    //memberPhotos.OrderList.Add(new ListItem(GetOrderListItemValue(i), i));
                }


            }
            catch (Exception ex)
            {
                throw new Exception("Error in PhotoUploadManager.GetMemberPhotos()", ex);
            }

            return memberPhotos;
        }

        public void DeleteMemberPhoto(int memberPhotoID, byte listOrder)
        {
            try
            {
                ArrayList photoUpdates = new ArrayList();
                Member member = MemberHelper.GetMember(_memberID, MemberLoadFlags.None);
                MemberSiteInfo memberSiteInfo = MemberHelper.GetMemberSiteInfo(member, (int)_siteID);
                Brand brand = g.GetBrand((int)memberSiteInfo.SiteID);

                photoUpdates.Add(CreateDeletePhotoUpdate(memberPhotoID, listOrder));

                MemberSA.Instance.SavePhotos(brand.BrandID, (int)_siteID, (int)_communityID,_memberID, 
                    (PhotoUpdate[])photoUpdates.ToArray(typeof(PhotoUpdate)));
 
                AdminSA.Instance.AdminActionLogInsert(_memberID, brand.Site.Community.CommunityID, (int)AdminAction.DeletedMemberPhoto, g.AdminID);
            }
            catch (Exception ex)
            {
                throw new Exception("Error in PhotoUploadManager.DeleteMemberPhoto()", ex);
            }
        }

        private PhotoUpdate CreateDeletePhotoUpdate(int memberPhotoID, byte listOrder)
        {
            var member = MemberHelper.GetMember(_memberID, MemberLoadFlags.None);

            foreach (Photo photo in member.GetPhotos((int)_communityID))
            {
                // If we have a memberPhotoID by which to reference the photo to be deleted, then use it.
                // If not, we must rely on the listOrder of this control.
                if (memberPhotoID == photo.MemberPhotoID || photo.ListOrder == listOrder)
                {
                    return new PhotoUpdate(null,
                        true,
                        photo.MemberPhotoID,
                        Constants.NULL_INT,
                        Constants.NULL_STRING,
                        Constants.NULL_INT,
                        Constants.NULL_STRING,
                        listOrder,
                        false,
                        true,
                        Constants.NULL_INT,
                        Constants.NULL_INT);
                }
            }
            // If you get here, an error occured.
            throw new Exception("Error occured while trying to create a Delete PhotoUpdate object.");
        }

        private List<ListItem> GetOrderListItems(int photosCount, byte order)
        {
            List<ListItem> list = new List<ListItem>();

            for (int i = 1; i <= photosCount; i++)
            {
                list.Add(GetOrderListItem(i, order));
            }

            return list;
        }

        private ListItem GetOrderListItem(int i, byte order)
        {
            ListItem li = new ListItem();

            li.Text = GetOrderListItemValue(i);
            li.Value = i.ToString();
            if (order != null && (int)order == i)
            {
                li.Selected = true;
            }

            return li;
        }

        private string GetOrderListItemValue(int index)
        {
            switch (index)
            {
                case 1:
                    return "1st";
                case 2:
                    return "2nd";
                case 3:
                    return "3rd";
                case 4:
                    return "4th";
                default:
                    return index.ToString();
            }
        }

        internal void UpdateChanges(System.Web.Mvc.FormCollection formData)
        {

            MemberPhotos memberPhotos = new MemberPhotos();
            var member = MemberHelper.GetMember(_memberID, MemberLoadFlags.None);

            bool uploadedNewPhotos = false;

            memberPhotos.MemberID = member.MemberID;

            try
            {
                MemberSiteInfo memberSiteInfo = MemberHelper.GetMemberSiteInfo(member, (int)_siteID);
                Brand brand = g.GetBrand((int)memberSiteInfo.SiteID);
                memberPhotos.MaxPhotoCount = int.Parse(RuntimeSettings.GetSetting("MAX_PHOTO_COUNT", brand.Site.Community.CommunityID, brand.Site.SiteID));

                PhotoCommunity photoCommunity = member.GetPhotos((int)_communityID);
                ArrayList photoUpdates = new ArrayList(memberPhotos.MaxPhotoCount * 2);

                for (int i = 0; i < memberPhotos.MaxPhotoCount; i++)
                {
                    Photo p = new Photo(Constants.NULL_INT, Constants.NULL_INT, null, 0);
                    if (i < photoCommunity.Count)
                    {
                        p = photoCommunity[(byte)i];
                    }
                    bool saveResultedInNewPhoto = SavePhoto(i, p, ref photoUpdates, formData);
                    if (saveResultedInNewPhoto && uploadedNewPhotos == false) uploadedNewPhotos = true;
                }
                int displayPhotosToGuest = Convert.ToBoolean(formData["guestSearchRadioList"]) ? 1 : 0;
                if (/*Page.IsValid ||*/ photoUpdates.Count > 0 || (member.GetAttributeInt(brand, "DisplayPhotosToGuests") != displayPhotosToGuest))
                {
                    if (photoUpdates.Count > 0)
                    {
                        MemberSA.Instance.SavePhotos(brand.BrandID, brand.Site.SiteID,
                            brand.Site.Community.CommunityID, member.MemberID,
                            (PhotoUpdate[])photoUpdates.ToArray(typeof(PhotoUpdate)));
                    }

                    member.SetAttributeInt(brand, "DisplayPhotosToGuests", displayPhotosToGuest);
                    member.SetAttributeDate(brand, "LastUpdated", DateTime.Now);
                    // Bump up the registration step value on save. Only saved once the first time a photo is uploaded. This implementation is borrowed from RegistrationStepHanlder.cs setNextRegistrationStep() method.
                    int stepCount = Matchnet.Conversion.CInt(RuntimeSettings.GetSetting("REGISTRATION_STEPS", brand.Site.Community.CommunityID, brand.Site.SiteID));
                    stepCount++; // Photo reg step value
                    int stepValue = (int)Math.Pow((double)2, (double)(stepCount - 1));
                    int completedStepMask = member.GetAttributeInt(brand, "NextRegistrationActionPageID");
                    if ((completedStepMask & stepValue) != stepValue)
                    {	// current step not already marked complete.
                        completedStepMask += stepValue;
                        member.SetAttributeInt(brand, "NextRegistrationActionPageID", completedStepMask);
                    }

                    if (uploadedNewPhotos)
                    {
                        AdminSA.Instance.AdminActionLogInsert(_memberID, brand.Site.Community.CommunityID, (int)AdminAction.UploadMemberPhotos, g.AdminID);
                    }

                    MemberSA.Instance.SaveMember(member);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error in PhotoUploadManager.GetMemberPhotos() communityID: " + _communityID, ex);
            }
        }

        private bool SavePhoto(int i, Photo photo, ref ArrayList photoUpdates, NameValueCollection formData)
        {
            bool uploadedNewPhoto = false;

            HttpPostedFile newFile = HttpContext.Current.Request.Files["uploadedFile" + i];

            int fileLength = 0;

            if (newFile != null)
            {
                fileLength = newFile.ContentLength;
            }

            #region No new photo. No current photo. See ya.
            if ((fileLength == 0) && (photo.MemberPhotoID == Constants.NULL_INT))
            {
                return uploadedNewPhoto;
            }
            #endregion

            int selectedListOrder = Conversion.CInt(formData["listOrder" + i]);
            string txtCaption = formData["txtCaption" + i];
            bool privatePhoto = formData["chkPrivatePhoto" + i] != null ? true : false;
            bool isApprovedForMain = formData["chkApprovedForMain" + i] != null ? true : false;
            bool isMain = formData["chkIsMain" + i] != null ? true : false;

            #region Nothing has changed with existing photo. See ya.
            if ((fileLength == 0) && (photo.MemberPhotoID != Constants.NULL_INT)
                && (selectedListOrder == photo.ListOrder) && (privatePhoto == photo.IsPrivate) && txtCaption == photo.Caption
                && (isApprovedForMain == photo.IsApprovedForMain) && (isMain == photo.IsMain))
            {
                return uploadedNewPhoto;
            }
            #endregion

            // Update the _listOrder.
            //  photo.ListOrder = selectedListOrder;
            /*
                    // If there was an error with validation control then exit
                    if (!PhotoValidator.IsValid || !PhotoValidator2.IsValid)
                    {
                        if (_memberPhotoID == Constants.NULL_INT)
                        {
                            PopulateStatus.Value = "False";
                        }

                        return;
                    }
                        */

            int memberPhotoID = Constants.NULL_INT;
            int fileID = Constants.NULL_INT;
            string fileWebPath = Constants.NULL_STRING;
            string fileCloudPath = Constants.NULL_STRING;
            int thumbFileID = Constants.NULL_INT;
            string thumbFileWebPath = Constants.NULL_STRING;
            string thumbFileCloudPath = Constants.NULL_STRING;
            int albumID = Constants.NULL_INT;
            int adminMemberID = Constants.NULL_INT;
            Byte[] fileBytes = null;
            bool isApproved = false;
            string caption = "";
            bool isCaptionApproved = false;

            // From this point on we only have 3 types of photos:
            // 1)  Photo that has had listOrder and/or privacy changed.
            if ((fileLength == 0) && (photo.MemberPhotoID != Constants.NULL_INT))
            {
                memberPhotoID = photo.MemberPhotoID;
                fileID = photo.FileID;
                fileWebPath = photo.FileWebPath;
                fileCloudPath = photo.FileCloudPath;
                thumbFileID = photo.ThumbFileID;
                thumbFileWebPath = photo.ThumbFileWebPath;
                thumbFileCloudPath = photo.ThumbFileCloudPath;
                albumID = photo.AlbumID;
                adminMemberID = photo.AdminMemberID;
                isApproved = photo.IsApproved;
                if (txtCaption != photo.Caption)
                {
                    caption = txtCaption;
                    isCaptionApproved = false;
                    photo.Caption = caption;
                }
                else
                {
                    caption = photo.Caption;
                    isCaptionApproved = photo.IsCaptionApproved;
                }
            }
            else
            {
                // 2)  New photo that is not overwritting another photo.
                // 3)  New photo that is overwriting another photo.
                int length;

                // Convert to byte array
                Stream fileStream = newFile.InputStream;
                length = (int)newFile.ContentLength;
                fileStream.Position = 0;
                fileBytes = new Byte[length];
                fileStream.Read(fileBytes, 0, length);
                newFile.InputStream.Flush();
                caption = txtCaption;
                isCaptionApproved = false;
                // If overwriting another photo then delete the existing memberPhotoID.
                if ((fileLength != 0) && (photo.MemberPhotoID != Constants.NULL_INT))
                {
                    photoUpdates.Add(CreateDeletePhotoUpdate(photo.MemberPhotoID, photo.ListOrder));
                }
                uploadedNewPhoto = true;
            }

            photoUpdates.Add(new PhotoUpdate(fileBytes,
                false,
                memberPhotoID,
                fileID,
                fileWebPath,
                thumbFileID,
                thumbFileWebPath,
                Convert.ToByte(selectedListOrder),
                isApproved,
                privatePhoto,
                albumID,
                adminMemberID, caption, isCaptionApproved, fileCloudPath, thumbFileCloudPath, isApprovedForMain, isMain, true));

            return uploadedNewPhoto;
        }

        /// <summary>
        /// Checks the files validation.
        /// </summary>
        /// <returns>Returns string.Empty if all files are valid</returns>
        public string CheckFilesValidation()
        {
            foreach (string fileKey in HttpContext.Current.Request.Files.Keys)
            {
                HttpPostedFile file = HttpContext.Current.Request.Files[fileKey];
                if (file != null && file.ContentLength > 0)
                {
                    string result = IsValidImage(file);
                    if (!string.IsNullOrEmpty(result))
                        return result;
                }
            }
            return string.Empty;
        }

        private string IsValidImage(HttpPostedFile file)
        {
            // Size check
            if (file.ContentLength > MAX_FILE_SIZE)
            {
                return "Minimum file size exceeded";
            }

            // File format check
            try
            {
                System.Drawing.Image image = System.Drawing.Image.FromStream(file.InputStream);

                // Convert to byte array
                Stream fileStream = file.InputStream;
                int length = (int)file.ContentLength;
                fileStream.Position = 0;
                Byte[] fileBytes = new Byte[length];
                fileStream.Read(fileBytes, 0, length);
                file.InputStream.Flush();

                if (!IsAcceptedImageType(fileBytes))
                {
                    return "Invalid photo type";
                }
            }
            catch (ArgumentException)
            {
                return "Unable to upload photo";
            }
            catch
            {
                return "Invalid photo type";
            }

            return string.Empty;
        }

        private bool IsAcceptedImageType(byte[] input)
        {
            if (input == null) return false;
            if (input.Length < 32) return false;
            // JPG
            string signature;
            signature = BitConverter.ToString(input, 0, 4);
            /*
            47 49 46 38 37 61   GIF87a 
            47 49 46 38 39 61   GIF89a 
            */
            if (signature.StartsWith("47-49-46-38")) return true;
            if (signature.StartsWith("47-49-46-38")) return true;
            /*
             42 4D   BM 
                        BMP, DIB   Windows bitmap image
 
            */
            if (signature.StartsWith("42-4D")) return true;

            /*
			 
            FF D8 FF E0 xx xx 4A 46 49 46 00   ÿ??.JFIF. 
                                                JFIF, JPE, JPEG, JPG   JPEG/JFIF graphics file
                                                Trailer: FF D9 (..)
            FF D8 FF E1 xx xx 45 78 69 66 00   ÿ??.Exif. 

             */
            string subtype;
            subtype = BitConverter.ToString(input, 6, 4);
            // JFIF
            if (signature.StartsWith("FF-D8-FF-E0") && subtype == "4A-46-49-46") return true;
            // EXIF
            if (signature.StartsWith("FF-D8-FF-E1") && subtype == "45-78-69-66") return true;

            return false;
        }

    }
}