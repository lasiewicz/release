﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using AdminTool.Models.Helpers;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Lib;
using Spark.Common.FraudService;
using Spark.CommonLibrary.Logging;
using BrandConfig = Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ServiceAdapters;
using Matchnet;
using AdminTool.Models.ModelViews;
using Spark.CommonLibrary;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;
using Purchase = Matchnet.Purchase.ValueObjects;
using Matchnet.PremiumServiceSearch11.ServiceAdapters;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using System.Configuration;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.MatchTest.ValueObjects;
using Matchnet.MatchTest.ValueObjects.MatchMeter;
using Matchnet.MatchTest.ServiceAdapters;
using System.Web.Mvc;
using Spark.SAL;
using AdminTool.Models.ModelViews.Member;
using System.Collections;
using AdminTool.Models.SparkAPI;
using AdminTool.Models.SparkAPI.EndpointClients;
using Spark.Common.OrderHistoryService;
using Spark.Common.Adapter;
using Spark.Common.PaymentProfileService;
using PurchaseVO = Matchnet.Purchase.ValueObjects;

namespace AdminTool.Models
{
    public static class MemberHelper
    {
        public const int FREE_TEXT_MAX_LENGTH = 5000;

        public static void PopulateMemberSearchForDynamicPortion(MemberSearch memberSearch, FormCollection collection, List<SearchPreference> searchPrefs)
        {
            string valueHolder = string.Empty;

            foreach (var searchPref in searchPrefs)
            {
                if (searchPref.Name.ToLower() == "height")
                    continue;

                if (GeneralHelper.TryGetFromFormCollection(collection, searchPref.Name, out valueHolder))
                {
                    MemberSearchPreferenceInt prefInt = memberSearch[Preference.GetInstance(searchPref.Name)] as MemberSearchPreferenceInt;
                    if (prefInt != null)
                    {
                        prefInt.Value = Convert.ToInt32(valueHolder);
                    }
                }
                else
                {
                    MemberSearchPreferenceInt prefInt = memberSearch[Preference.GetInstance(searchPref.Name)] as MemberSearchPreferenceInt;
                    if (prefInt != null)
                    {
                        prefInt.Value = Constants.NULL_INT;
                    }
                }
            }
        }

        public static void PopulateEditProfileDataFromMember(Member member, BrandConfig.Brand brand, int controlType,
            EditProfileData data, ProfileAttributeMetadataCollection attributeMeta, string regionIDFormFieldName, string genderPickerFormFieldName)
        {
            //Get member to get updated attribute values
            member = GetMember(member.MemberID, MemberLoadFlags.None);

            // get the metadata first
            if (attributeMeta == null)
            {
                attributeMeta = new ProfileAttributeMetadataCollection();
                attributeMeta.GetMetadata(controlType, brand);
            }

            // get the resource bag if needed
            Hashtable resourceBag = null;

            if (controlType == (int)ControlType.SingleSelect || controlType == (int)ControlType.MultiSelect)
            {
                resourceBag = GeneralHelper.GetResourceCollection(GeneralHelper.DetermineResourceFileName(brand));
            }

            // now get the member data for the attributes
            foreach (ProfileAttributeMetadata aMeta in attributeMeta.ProfileAttributeMetadataCol)
            {
                switch (aMeta.DataType)
                {
                    case DataType.FreeText:
                        string freeTextValue = member.GetAttributeText(brand, aMeta.AttributeID);
                        bool includeThisAttribute = true;
                        if (aMeta.AttributeName.ToLower() == "emailaddress" && freeTextValue.ToLower().Contains("@spark.net"))
                        {
                            includeThisAttribute = false;
                        }

                        if (includeThisAttribute)
                        {
                            data.FreeTextAttributes.Add(new FreeTextAttribute(aMeta.AttributeID, aMeta.AttributeName, freeTextValue, aMeta.AttributeMaxLength));
                        }
                        break;
                    case DataType.NumberText:
                        NumberTextAttribute numAttr = new NumberTextAttribute(aMeta.AttributeID, aMeta.AttributeName, member.GetAttributeInt(brand, aMeta.AttributeID));
                        if (aMeta.AttributeName.ToLower() == "desiredminage")
                        {
                            numAttr.MinValue = Constants.MIN_AGE_RANGE;
                        }
                        else if (aMeta.AttributeName.ToLower() == "desiredmaxage")
                        {
                            numAttr.MaxValue = Constants.MAX_AGE_RANGE;
                        }

                        data.NumberTextAttributes.Add(numAttr);
                        break;
                    case DataType.DateTime:
                        data.DateTimeAttributes.Add(new DateTimeAttribute(aMeta.AttributeID, aMeta.AttributeName, member.GetAttributeDate(brand, aMeta.AttributeID)));
                        break;
                    case DataType.Boolean:
                        data.BooleanAttributes.Add(new BooleanAttribute(aMeta.AttributeID, aMeta.AttributeName, member.GetAttributeBool(brand, aMeta.AttributeID)));
                        break;
                    case DataType.SingleSelect:
                        if (aMeta.AttributeName.ToLower() == "regionid")
                        {
                            // set the region data
                            data.DisplayRegionPicker = true;
                            data.RegionPickerData = new RegionPicker { SiteID = brand.Site.SiteID, FormFieldName = regionIDFormFieldName, RegionID = member.GetAttributeInt(brand, aMeta.AttributeID) };
                        }
                        else
                        {
                            int selectedValue = member.GetAttributeInt(brand, aMeta.AttributeID);
                            SingleSelectAttribute singleSelAttribute = new SingleSelectAttribute(aMeta.AttributeID, aMeta.AttributeName, selectedValue);
                            singleSelAttribute.OptionChoices = GeneralHelper.GetAttributeOptionListItems(brand, aMeta.AttributeID, selectedValue, false, resourceBag, true, true);
                            data.SingleSelectAttributes.Add(singleSelAttribute);
                        }
                        break;
                    case DataType.MultiSelect:
                        if (aMeta.AttributeName.ToLower() == "gendermask")
                        {
                            data.DisplayGenderPicker = true;
                            data.GenderPickerData = MemberHelper.BindGenderMask(genderPickerFormFieldName, member.GetAttributeInt(brand, aMeta.AttributeID));
                        }
                        else
                        {
                            int multiSelectedValues = member.GetAttributeInt(brand, aMeta.AttributeID);
                            MultiSelectAttribute multiSelAttribute = new MultiSelectAttribute(aMeta.AttributeID, aMeta.AttributeName, multiSelectedValues);
                            multiSelAttribute.OptionChoices = GeneralHelper.GetAttributeOptionListItems(brand, aMeta.AttributeID, multiSelectedValues, true, resourceBag, false);
                            data.MultiSelectAttributes.Add(multiSelAttribute);
                        }
                        break;
                }
            }

        }


        public static void PopulateMemberDataForPrint(Member member, BrandConfig.Brand brand, MemberDataForPrint data)
        {
            //Get member to get updated attribute values
            member = GetMember(member.MemberID, MemberLoadFlags.None);


            var attributeMeta = new MemberDataForPrint.AttributeMetadataCollectionForPrint();
            attributeMeta.GetMetadata(brand);
            
            var resourceBag = GeneralHelper.GetResourceCollection(GeneralHelper.DetermineResourceFileName(brand));

           
            // now get the member data for the attributes
            foreach (MemberDataForPrint.AttributeMetadataForPrint aMeta in attributeMeta.ProfileAttributeMetadataCol)
            {
                switch (aMeta.DataType)
                {
                    case MemberDataForPrint.DataType.FreeText:
                        string freeTextValue = member.GetAttributeText(brand, aMeta.AttributeID);
                         
                            data.FreeTextAttributes.Add(new FreeTextAttribute(aMeta.AttributeID, aMeta.AttributeName, freeTextValue, aMeta.AttributeMaxLength));
                        
                        break;
                    case MemberDataForPrint.DataType.NumberText:
                        NumberTextAttribute numAttr = new NumberTextAttribute(aMeta.AttributeID, aMeta.AttributeName, member.GetAttributeInt(brand, aMeta.AttributeID));
                        if (aMeta.AttributeName.ToLower() == "desiredminage")
                        {
                            numAttr.MinValue = Constants.MIN_AGE_RANGE;
                        }
                        else if (aMeta.AttributeName.ToLower() == "desiredmaxage")
                        {
                            numAttr.MaxValue = Constants.MAX_AGE_RANGE;
                        }

                        data.NumberTextAttributes.Add(numAttr);
                        break;
                    case MemberDataForPrint.DataType.DateTime:
                        data.DateTimeAttributes.Add(new DateTimeAttribute(aMeta.AttributeID, aMeta.AttributeName, member.GetAttributeDate(brand, aMeta.AttributeID)));
                        break;
                    case MemberDataForPrint.DataType.Boolean:
                        data.BooleanAttributes.Add(new BooleanAttribute(aMeta.AttributeID, aMeta.AttributeName, member.GetAttributeBool(brand, aMeta.AttributeID)));
                        break;
                    case MemberDataForPrint.DataType.SingleSelect:
                        if (aMeta.AttributeName.ToLower() == "regionid")
                        {
                            
                        }
                        else
                        {
                            int selectedValue = member.GetAttributeInt(brand, aMeta.AttributeID);
                            SingleSelectAttribute singleSelAttribute = new SingleSelectAttribute(aMeta.AttributeID, aMeta.AttributeName, selectedValue);
                            singleSelAttribute.OptionChoices = GeneralHelper.GetAttributeOptionListItems(brand, aMeta.AttributeID, selectedValue, false, resourceBag, true, true);
                            data.SingleSelectAttributes.Add(singleSelAttribute);
                        }
                        break;
                    case MemberDataForPrint.DataType.MultiSelect:
                        if (aMeta.AttributeName.ToLower() == "gendermask")
                        {
                           
                        }
                        else
                        {
                            int multiSelectedValues = member.GetAttributeInt(brand, aMeta.AttributeID);
                            MultiSelectAttribute multiSelAttribute = new MultiSelectAttribute(aMeta.AttributeID, aMeta.AttributeName, multiSelectedValues);
                            multiSelAttribute.OptionChoices = GeneralHelper.GetAttributeOptionListItems(brand, aMeta.AttributeID, multiSelectedValues, true, resourceBag, false);
                            data.MultiSelectAttributes.Add(multiSelAttribute);
                        }
                        break;
                }
            }

        }



        public static void PopulateSearchPreferencesDataFromMemberSearch(SearchPreferencesData data, MemberSearch memberSearch, BrandConfig.Brand brand, int memberID,
            string regionIDFormFieldName, string countryRegionIDFormFieldName, string searchTypeFormFieldName, string genderPickerFormFieldName, bool dynamicSearchPrefsInitialized)
        {
            if (memberSearch == null)
                return;

            Hashtable resourceBag = GeneralHelper.GetResourceCollection(GeneralHelper.DetermineResourceFileName(brand));

            data.MinAge = memberSearch.Age.MinValue;
            data.MaxAge = memberSearch.Age.MaxValue;

            int genderMask = 0;
            genderMask |= (int)memberSearch.Gender;
            genderMask |= memberSearch.SeekingGender == Gender.Male ? (int)Enums.SeekingGender.Male : (int)Enums.SeekingGender.Female;
            data.GenderData = MemberHelper.BindGenderMask(genderPickerFormFieldName, genderMask);

            data.Distance = memberSearch.Distance;
            data.DistanceOptions = GeneralHelper.GetAttributeOptionListItems(brand, GeneralHelper.GetDistanceAttribute(brand), memberSearch.Distance, false, resourceBag);

            data.KeywordSearch = memberSearch.KeywordSearch;

            switch (memberSearch.SearchType)
            {
                case SearchType.PostalCode:
                case SearchType.Region:
                    data.SearchRegionPicker = new SearchRegionPicker { SiteID = brand.Site.SiteID,
                                                                       FormFieldName = regionIDFormFieldName,
                                                                       CountryRegionIDFormFieldName = countryRegionIDFormFieldName,
                                                                       SearchTypeFormFieldName = searchTypeFormFieldName,
                                                                        RegionID = memberSearch.RegionID,
                                                                        SearchType = memberSearch.SearchType };
                    break;
                case SearchType.AreaCode:
                    data.SearchRegionPicker = new SearchRegionPicker { SiteID = brand.Site.SiteID,
                                                                       FormFieldName = regionIDFormFieldName,
                                                                       CountryRegionIDFormFieldName = countryRegionIDFormFieldName,
                                                                       SearchTypeFormFieldName = searchTypeFormFieldName,
                                                                        SearchType = memberSearch.SearchType};
                    for (int i = 1; i <= 6; i++)
                    {
                        MemberSearchPreferenceInt areaCode = memberSearch[Preference.GetInstance("AreaCode" + i)] as MemberSearchPreferenceInt;
                        if (areaCode != null && Conversion.CInt(areaCode.Value) > 0)
                        {
                            data.SearchRegionPicker.AreaCodes.Add(areaCode.Value);
                            data.SearchRegionPicker.CountryRegionID = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrieveAreaCodes()[areaCode.Value];
                        }
                    }
                    break;
                case SearchType.College:
                    throw new Exception("College SearchType is no longer supported!");
            }

            data.HasPhotos = memberSearch.HasPhoto;

            // height
            MemberSearchPreferenceRange mspHeight = memberSearch[Preference.GetInstance("Height")] as MemberSearchPreferenceRange;
            data.MinHeight = mspHeight.MinValue;
            data.MaxHeight = mspHeight.MaxValue;
            data.MinHeightOptions = GeneralHelper.GetAttributeOptionListItems(brand, "height", mspHeight.MinValue, false, resourceBag);
            data.MinHeightOptions.Insert(0, new SelectListItem { Selected = mspHeight.MinValue == Constants.NULL_INT, Text = "Any", Value = Constants.NULL_INT.ToString() });
            data.MaxHeightOptions = GeneralHelper.GetAttributeOptionListItems(brand, "height", mspHeight.MaxValue, false, resourceBag);
            data.MaxHeightOptions.Insert(0, new SelectListItem { Selected = mspHeight.MaxValue == Constants.NULL_INT, Text = "Any", Value = Constants.NULL_INT.ToString() });

            // get the rest of search preferences from the xml file
            if(!dynamicSearchPrefsInitialized)
                GetDynamicSearchPreferences(data, brand.Site.SiteID);

            // bind the user's selected values
            MemberHelper.PopulateSearchPreferenceList(memberSearch, data.BasicSearchPrefs, resourceBag, brand, true);
            MemberHelper.PopulateSearchPreferenceList(memberSearch, data.AdditionalSearchPrefs, resourceBag, brand, true);
        }

        public static void GetDynamicSearchPreferences(SearchPreferencesData searchPrefs, int siteID)
        {
            List<SearchPreference> fromXml = GeneralHelper.GetSearchPreferencesSection(siteID);

            searchPrefs.BasicSearchPrefs.AddRange(fromXml.FindAll(x => x.SectionName == "basics"));
            searchPrefs.AdditionalSearchPrefs.AddRange(fromXml.FindAll(x => x.SectionName == "additional"));
        }

        public static void PopulateSearchPreferenceList(MemberSearch memberSearch, List<SearchPreference> searchPrefs, Hashtable resourceBag, BrandConfig.Brand brand, bool includeAnyOption)
        {
            foreach (SearchPreference pref in searchPrefs)
            {
                // if height, just skip
                if (pref.Name.ToLower() == "height")
                    continue;

                int selectedValue = -1;
                MemberSearchPreferenceInt prefInt = memberSearch[Preference.GetInstance(pref.Name)] as MemberSearchPreferenceInt;
                if (prefInt != null && Convert.ToInt32(prefInt.Value) > 0)
                {
                    selectedValue = Convert.ToInt32(prefInt.Value);
                    pref.SelectedValue = selectedValue;
                }

                pref.Options = GeneralHelper.GetAttributeOptionListItems(brand, pref.Name, selectedValue, true, resourceBag);

                if (includeAnyOption)
                {
                    pref.Options.Insert(0, new SelectListItem { Text = "Any", Value = Constants.NULL_INT.ToString(), Selected = (selectedValue == -1) });
                }
            }
        }

        public static GenderPicker BindGenderMask(string formFieldName, int genderMask)
        {
            GenderPicker picker = BindGenderMaskDropDowns(genderMask);

            picker.FormFieldName = formFieldName;
            picker.GenderMaskValue = genderMask;

            SelectListItem search = picker.GenderOptions.Find(x => (Convert.ToInt32(x.Value) & genderMask) == Convert.ToInt32(x.Value));
            if (search != null)
            {
                search.Selected = true;
            }

            search = picker.SeekingGenderOptions.Find(x => (Convert.ToInt32(x.Value) & genderMask) == Convert.ToInt32(x.Value));
            if (search != null)
            {
                search.Selected = true;
            }

            return picker;
        }

        public static GenderPicker BindGenderMaskDropDowns(int genderMask)
        {
            GenderPicker picker = new GenderPicker();

            picker.GenderOptions = GetGenderListItems(false);
            picker.SeekingGenderOptions = GetGenderListItems(true);

            return picker;
        }

        public static List<SelectListItem> GetGenderListItems(bool isSeeking)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            if (isSeeking)
            {
                items.Add(new SelectListItem { Text = "Male", Value = "4" });
                items.Add(new SelectListItem { Text = "Female", Value = "8" });
                //items.Add(new SelectListItem { Text = "male turned female", Value = "64" });
                //items.Add(new SelectListItem { Text = "female turned male", Value = "128" });
            }
            else
            {
                items.Add(new SelectListItem { Text = "Male", Value = "1" });
                items.Add(new SelectListItem { Text = "Female", Value = "2" });
                //items.Add(new SelectListItem { Text = "male turned female", Value = "16" });
                //items.Add(new SelectListItem { Text = "female turned male", Value = "32" });
            }

            return items;
        }

        public static List<SelectListItem> GetGenderListItems(bool isSeeking, int genderMask)
        {
            List<SelectListItem> items = GetGenderListItems(isSeeking);
            
            SelectListItem search = items.Find(x => (Convert.ToInt32(x.Value) & genderMask) == Convert.ToInt32(x.Value));
            if (search != null)
            {
                search.Selected = true;
            }

            return items;
        }

        public static void GetMemberAndBrand(out Matchnet.Member.ServiceAdapters.Member member, out BrandConfig.Brand brand, int memberID, int communityID)
        {
            member = GetMember(memberID, MemberLoadFlags.None);
            int brandID;

            member.GetLastLogonDate((int)communityID, out brandID);
            brand = BrandConfigSA.Instance.GetBrandByID(brandID);
        }
        
        public static int GetAge(DateTime birthDate)
        {
            if (birthDate == DateTime.MinValue) return 0;
            return (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
        }

        public static int GetAge(Matchnet.Member.ServiceAdapters.Member member, BrandConfig.Brand brand)
        {
            return GetAge(member.GetAttributeDate(brand, "BirthDate", DateTime.MinValue));
        }

        public static AdminActionReasonID GetLastSuspendedReasonID(Matchnet.Member.ServiceAdapters.Member member)
        {
            return AdminSA.Instance.GetLastAdminSuspendReasonForMember(member.MemberID);
        }

        public static string GetRegionString(Matchnet.Member.ServiceAdapters.Member member, BrandConfig.Brand brand, bool showPostalCode, bool showAbbreviatedState, bool showAbbreviatedCountry)
        {
            string regionString = string.Empty;
            int regionID = member.GetAttributeInt(brand, "regionid");

            if(regionID > 0)
            {
                regionString = GeneralHelper.GetRegionString(regionID, showPostalCode, showAbbreviatedState, showAbbreviatedCountry, brand);
            }
           
            return regionString;
        }

        public static string GetGender(Matchnet.Member.ServiceAdapters.Member member, BrandConfig.Brand brand)
        {
            string genderStr = string.Empty;
            int genderMask = member.GetAttributeInt(brand, "gendermask", 0);

            if (genderMask < 0)
                return genderStr;

            if ((genderMask & WebConstants.GENDERID_MALE) == WebConstants.GENDERID_MALE)
                return "Male";
            if ((genderMask & WebConstants.GENDERID_FEMALE) == WebConstants.GENDERID_FEMALE)
                return "Female";
            if ((genderMask & WebConstants.GENDERID_MTF) == WebConstants.GENDERID_MTF)
                return "MTF";
            if ((genderMask & WebConstants.GENDERID_FTM) == WebConstants.GENDERID_FTM)
                return "FTM";

            return genderStr;
        }

        public static string GetGender(int genderMask)
        {
            string genderStr = string.Empty;

            if (genderMask < 0)
                return genderStr;

            if ((genderMask & WebConstants.GENDERID_MALE) == WebConstants.GENDERID_MALE)
                return "Male";
            if ((genderMask & WebConstants.GENDERID_FEMALE) == WebConstants.GENDERID_FEMALE)
                return "Female";
            if ((genderMask & WebConstants.GENDERID_MTF) == WebConstants.GENDERID_MTF)
                return "MTF";
            if ((genderMask & WebConstants.GENDERID_FTM) == WebConstants.GENDERID_FTM)
                return "FTM";

            return genderStr;
        }

        public static bool IsSubscriber(Matchnet.Member.ServiceAdapters.Member member, BrandConfig.Brand brand)
        {
            return (GetSubscriptionExpirationDate(member, brand) > DateTime.Now);
        }

        public static DateTime GetSubscriptionExpirationDate(Matchnet.Member.ServiceAdapters.Member member, BrandConfig.Brand brand)
        {
            return member.GetAttributeDate(Constants.NULL_INT, brand.Site.SiteID, Constants.NULL_INT, "SubscriptionExpirationDate", System.DateTime.MinValue);
        }

        public static MemberSiteInfo GetMemberSiteInfo(Member member, int siteID)
        {
            MemberSiteInfo siteInfo = siteInfo = new MemberSiteInfo();
            siteInfo.SiteID = (SiteIDs)Enum.Parse(typeof(SiteIDs), siteID.ToString());

            int checkSiteID = 0;
            if (int.TryParse(siteInfo.SiteID.ToString(), out checkSiteID))
            {
                siteInfo = null;
            }
            else
            {
                siteInfo.MemberID = member.MemberID;

                BrandConfig.Brand brand = ((ContextGlobal)HttpContext.Current.Items["g"]).GetBrand(siteID);
                siteInfo.Name = brand.Site.Name;
                siteInfo.Domain = brand.Uri;
                siteInfo.Host = ConfigurationManager.AppSettings["DefaultHost"];
                siteInfo.HostDomain = siteInfo.Host + "." + brand.Uri;
                siteInfo.URL = brand.Site.SSLUrl.Replace("https", "http");
                siteInfo.IsSubscriber = MemberHelper.IsSubscriber(member, brand);
                siteInfo.LastLoginDate = member.GetAttributeDate(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, "BrandLastLogonDate", System.DateTime.MinValue);

                siteInfo.ProfileStatus = GetProfileStatus(member, brand);
            }
            return siteInfo;
        }

        public static List<MemberSiteInfo> GetMemberSiteInfoList(Member member)
        {
            int[] siteIDList = member.GetSiteIDList();
            List<MemberSiteInfo> memberSiteList = new List<MemberSiteInfo>();

            if (siteIDList.Length > 0)
            {
                foreach (int s in siteIDList)
                {
                    //only add supported sites
                    MemberSiteInfo siteInfo = MemberHelper.GetMemberSiteInfo(member, s);
                    if (siteInfo != null)
                        memberSiteList.Add(siteInfo);
                }
            }
            memberSiteList.Sort();

            return memberSiteList;
        }

        public static int GetMemberLastLogonSite(Member member)
        {
            return GetMemberLastLogonSite(GetMemberSiteInfoList(member));
        }

        public static int GetMemberLastLogonSite(List<MemberSiteInfo> siteInfo)
        {
            var ret = Constants.NULL_INT;

            if (siteInfo != null)
            {
                var siteId = (from MemberSiteInfo i in siteInfo
                                orderby i.LastLoginDate descending
                                select i.SiteID).First();
                ret = (int) siteId;
            }

            return ret;
        }

        public static void SetLastUpdateDateForAllSites(Member member)
        {
            int[] siteIDList = member.GetSiteIDList();
            if (siteIDList.Length > 0)
            {
                foreach (int siteID in siteIDList)
                {
                    BrandConfig.Brand brand = ((ContextGlobal)HttpContext.Current.Items["g"]).GetBrand(siteID);
                    member.SetAttributeDate(brand, "LastUpdated", DateTime.Now);
                }
            }
        }

        public static string GetMemberUsername(int memberID, BrandConfig.Brand brand)
        {
            string ret = string.Empty;

            var member = GetMember(memberID, MemberLoadFlags.None);
            if (member != null)
            {
                ret = member.GetUserName(brand);
            }

            return ret;
        }

        public static Member GetMember(int memberID, MemberLoadFlags loadFlag)
        {
            return MemberSA.Instance.GetMember(memberID, loadFlag);
        }

        public static Member GetMigratedMember(int memberID)
        {
            return Spark.MingleMigration.ServiceAdapters.MingleMemberSA.Instance.GetMember(memberID);
        }

        public static Purchase.MemberSub GetMemberSub(int memberID, int siteID)
        {
            //Note: Deprecated - Use GetRenewalSubscription() instead
            throw new Exception("Deprecated - Use GetRenewalSubscription() instead");
            return PurchaseSA.Instance.GetSubscription(memberID, siteID);
        }

        public static Spark.Common.RenewalService.RenewalSubscription GetRenewalSubscription(int memberID, int siteID)
        {
            return Spark.Common.Adapter.RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetCurrentRenewalSubscription(memberID, siteID);
        }

        public static DateTime GetLastMessageSentDate(int memberID, int communityID)
        {
            bool useNewEmailService = Convert.ToBoolean(ConfigurationManager.AppSettings["UseNewEmailService"]);

            if (useNewEmailService)
            {
                DateTime? lastSentDate = EmailLogSA.Instance.RetrieveEmailLastSentDate(memberID, communityID);
                if (lastSentDate.HasValue)
                {
                    return lastSentDate.Value;
                }
                else
                {
                    return DateTime.MinValue;
                }
            }
            else
            {
                DateTime lastSentDateSorted = DateTime.MinValue;
                MemberMailLog memberMailLog = EmailLogSA.Instance.RetrieveEmailLog(memberID, communityID);
                if (memberMailLog.Sent != null && memberMailLog.Sent.Count > 0)
                {
                    memberMailLog.Sent.Sort();
                    lastSentDateSorted = memberMailLog.Sent[0].InsertDate;
                }

                return lastSentDateSorted;
            }
        }

        public static string GetSubscriberStatusString(Member member, BrandConfig.Brand brand)
        {
            int memberSubscriptionStatus = Constants.NULL_INT;

            bool evaluateSubscriptionStatusAgain = false;

            memberSubscriptionStatus = member.GetAttributeInt(brand, "SubscriptionStatus", 0);
            Matchnet.Purchase.ValueObjects.SubscriptionStatus enumSubscriptionStatus = (Matchnet.Purchase.ValueObjects.SubscriptionStatus)memberSubscriptionStatus;
            DateTime dtSubendDate = member.GetAttributeDate(brand, "SubscriptionExpirationDate", DateTime.MinValue);

            if (enumSubscriptionStatus == Matchnet.Purchase.ValueObjects.SubscriptionStatus.PremiumSubscriber)
            {
                if (dtSubendDate != DateTime.MinValue)
                {
                    // Member is currently identified as a premium subscriber
                    if ((PremiumServiceSA.Instance.GetMemberActivePremiumServices(member.MemberID, brand).Count < 1)
                        || dtSubendDate < DateTime.Now)
                    {
                        // Member does not have an active premium service or the member does not have
                        // subscription privilege.  
                        // Reevaluate the subscription status of this member
                        evaluateSubscriptionStatusAgain = true;
                    }
                }
            }
            else if (enumSubscriptionStatus != Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberExSubscriber
                     && enumSubscriptionStatus != Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberFreeTrial
                     && enumSubscriptionStatus != Matchnet.Purchase.ValueObjects.SubscriptionStatus.NonSubscriberNeverSubscribed)
            {
                if (dtSubendDate != DateTime.MinValue)
                {
                    // Member is currently identified as a subscriber
                    if (dtSubendDate < DateTime.Now)
                    {
                        // Member does not have subscription privilege.  
                        // Reevaluate the subscription status of this member
                        evaluateSubscriptionStatusAgain = true;
                    }
                }
            }

            if (evaluateSubscriptionStatusAgain)
            {
                MemberSaveResult result = null;

                memberSubscriptionStatus = Convert.ToInt16(PurchaseSA.Instance.GetMemberSubStatus(member.MemberID, brand.Site.SiteID, dtSubendDate, false).Status);
                member.SetAttributeInt(brand, "SubscriptionStatus", memberSubscriptionStatus);
                result = MemberSA.Instance.SaveMember(member);
            }

            string memberStatusString = string.Empty;

            switch (memberSubscriptionStatus)
            {
                case 2:
                    memberStatusString = "Ex Subscriber";
                    break;
                case 1:
                    memberStatusString = "Never Subscribed";
                    break;
                case 5:
                    memberStatusString = "Renew Subscriber";
                    break;
                case 6:
                    memberStatusString = "Renew Winback";
                    break;
                case 3:
                    memberStatusString = "First Time Subscriber";
                    break;
                case 4:
                    memberStatusString = "Subscriber Winback";
                    break;
                case 9:
                    memberStatusString = "Premium";
                    break;
            }

            return memberStatusString;
        }

        public static List<Spark.Common.AccessService.AccessFrozenPrivilege> GetCustomerFrozenPrivilegeList(int memberID, int siteID)
        {
            List<Spark.Common.AccessService.AccessFrozenPrivilege> frozenPrivileges = new List<Spark.Common.AccessService.AccessFrozenPrivilege>();
            Spark.Common.AccessService.AccessFrozenPrivilege[] arrFrozenPrivileges = Spark.Common.Adapter.AccessServiceWebAdapter.GetProxyInstanceForBedrock().GetCustomerFrozenPrivilegeList(memberID, siteID);

            foreach (Spark.Common.AccessService.AccessFrozenPrivilege frozenPrivilege in arrFrozenPrivileges)
            {
                frozenPrivileges.Add(frozenPrivilege);
            }

            return frozenPrivileges;
        }

        public static AdminTool.Models.Enums.ProfileStatus GetProfileStatus(Matchnet.Member.ServiceAdapters.Member member, BrandConfig.Brand brand)
        {
            AdminTool.Models.Enums.ProfileStatus profileStatus = Enums.ProfileStatus.Active;

            int globalStatusMask = member.GetAttributeInt(brand, "GlobalStatusMask");
            if ((globalStatusMask & (int)GlobalStatusMask.AdminSuspended) == (int)GlobalStatusMask.AdminSuspended)
            {
                profileStatus = Enums.ProfileStatus.AdminSuspend;
            }
            else if (member.GetAttributeInt(brand, "SelfSuspendedFlag") == WebConstants.SELF_SUSPEND)
            {
                profileStatus = Enums.ProfileStatus.SelfSuspend;
            }

            return profileStatus;
        }

        public static bool HasMemberCompletedColorQuiz(Member member, BrandConfig.Brand brand)
        {
            //It is safe to assume that a member will only have a valid primary color after completing the quiz.

            AdminTool.Models.Enums.Color color = AdminTool.Models.Enums.Color.none;

            if (member != null && brand != null)
            {
                string attributeText = member.GetAttributeText(brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID, brand.Site.LanguageID, WebConstants.ATTRIBUTE_NAME_COLORCODEPRIMARYCOLOR, "");
                if (!String.IsNullOrEmpty(attributeText))
                {
                    color = (AdminTool.Models.Enums.Color)Enum.Parse(typeof(AdminTool.Models.Enums.Color), attributeText);
                }
            }

            if (color != AdminTool.Models.Enums.Color.none)
                return true;
            else
                return false;
        }

        public static bool IsMemberPremiumAuthenticated(Member member, BrandConfig.Brand brand)
        {
            //return true;

            try
            {
                DateTime dtePremiumAuthenticatedExpirationDate = member.GetAttributeDate(brand, "PremiumAuthenticatedExpirationDate", DateTime.MinValue);
                
                if (dtePremiumAuthenticatedExpirationDate == DateTime.MinValue)
                {
                    // Occurs when the member has no PremiumAuthenticatedExpirationDate attribute
                    // because they never subscribed to the Premium plan
                    return false;
                }
                else
                {
                    if (DateTime.Now < dtePremiumAuthenticatedExpirationDate)
                    {
                        // Member has subscribed to the premium plan and the time has
                        // not yet expired
                        return true;
                    }
                    else
                    {
                        // Expired premium plan subscription
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                // Member may not exists or attribute may not exists for this member
                return false;
            }
        }

        public static bool IsJMeterEditable(Member member, BrandConfig.Brand brand)
        {
            bool isEditable = true;

            try
            {
                MatchTestStatus mts = MatchMeterSA.Instance.GetMatchTestStatus(member.MemberID, false, brand);

                if (mts == MatchTestStatus.MinimumCompleted || mts == MatchTestStatus.Completed)
                {
                    isEditable = false;
                }
            }
            catch
            {
                isEditable = false;
            }

            return isEditable;

            
        }

        public static Enums.Gender GetMemberGender(Member member, BrandConfig.Brand brand)
        {
            int genderMask = member.GetAttributeInt(brand, "GenderMask");
            if ((genderMask & (int)Enums.Gender.Male) == (int)Enums.Gender.Male)
                return Enums.Gender.Male;
            else
                return Enums.Gender.Female;

        }

        public static Enums.SeekingGender GetSeekingGender(Member member, BrandConfig.Brand brand)
        {
            int genderMask = member.GetAttributeInt(brand, "GenderMask");
            if ((genderMask & (int)Enums.SeekingGender.Male) == (int)Enums.SeekingGender.Male)
                return Enums.SeekingGender.Male;
            else
                return Enums.SeekingGender.Female;
        }

        public static Enums.SeekingGender GetSeekingGender(int genderMask)
        {
            if ((genderMask & (int)Enums.SeekingGender.Male) == (int)Enums.SeekingGender.Male)
                return Enums.SeekingGender.Male;
            else
                return Enums.SeekingGender.Female;
        }

        public static string GetRegionDisplayString(Member member, BrandConfig.Brand brand)
        {
            return Matchnet.OmnitureHelper.OmnitureHelper.GetRegionString(member, brand, brand.Site.LanguageID, true, true, true);
        }

        public static string GetAttributeOptionDisplay(Member member, BrandConfig.Brand brand, string attributeName, int attributeValue)
        {
            string display = string.Empty;
            
            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);

            if (attributeOptionCollection != null)
            {
                AttributeOption attributeOption = attributeOptionCollection[attributeValue];
                if (attributeOption != null)
                {
                    display = attributeOption.Description;
                }
            }

            return display;
        }


        public static DateTime GetMemberFirstSubscriptionDate(Member member, BrandConfig.Brand brand)
        {
            DateTime subscriptionDate = DateTime.MinValue;

            List<AccountHistory> colAccountHistory = new List<AccountHistory>();
            OrderInfo[] arrOrderInfo = OrderHistoryServiceWebAdapter.GetProxyInstanceForBedrock().GetMemberOrderHistoryByMemberID(member.MemberID, brand.Site.SiteID, 5000, 1);

            foreach (OrderInfo orderInfo in arrOrderInfo)
            {
                if (orderInfo.OrderDetail.Length > 0)
                {
                    // Only add orders that have order details  
                    AccountHistory accountHistory = new AccountHistory(orderInfo, brand, AccountHistoryType.Financial, member);
                    colAccountHistory.Add(accountHistory);
                }
            }

            AccountHistory first = (from AccountHistory ah in colAccountHistory
                         where ah.TransactionType == TransactionType.InitialSubscriptionPurchase
                         && ah.OrderStatus == OrderStatus.Successful
                         orderby ah.InsertDateInPST ascending
                         select ah).Take(1).FirstOrDefault();

            if (first != null)
            {
                subscriptionDate = first.InsertDateInPST;
            }

            return subscriptionDate;
        }

        public static string GetHeightDisplay(Member member, BrandConfig.Brand brand)
        {
            string heightAttribute = null;
            string heightDisplay = string.Empty;
            heightAttribute = member.GetAttributeInt(brand, "Height").ToString();
            if (heightAttribute != null && !"".Equals(heightAttribute) && heightAttribute != Constants.NULL_INT.ToString())
            {
                int height = int.Parse(heightAttribute);
                heightDisplay = GeneralHelper.GetHeightDisplay(height);
            }
            return heightDisplay;
        }

        /// <summary>
        ///     Validates the member by the last brand, brand logon date, birthdate, admin suspended status
        /// </summary>
        /// <param name="member"></param>
        /// <param name="communityId"></param>
        /// <returns></returns>
        public static bool IsMemberValidForApproval(Member member, int communityId)
        {
            if (member == null) return false;
            if (communityId <= 0) return false;

            switch (communityId)
            {
                // L.A. sites
                case (int)CommunityIDs.JDate:
                case (int)CommunityIDs.Spark:
                case (int)CommunityIDs.Cupid:
                case (int)CommunityIDs.BlackSingles:
                case (int)CommunityIDs.BBWPersonalsPlus:
                    return IsLaMemberValidForApproval(member, communityId);
                default: 
                    // Mingle sites
                    return IsMingleMemberValidForApproval(member, communityId);

            }
        }

        /// <summary>
        ///     Determines if a Mingle member is valid for photo approval
        ///     based on a set of attributes
        /// </summary>
        /// <param name="member"></param>
        /// <param name="communityId"></param>
        /// <returns></returns>
        public static bool IsLaMemberValidForApproval(Member member, int communityId)
        {
            var isValid = true;

            try
            {
                int brandId;
                member.GetLastLogonDate(communityId, out brandId);
                var brand = BrandConfigSA.Instance.GetBrandByID(brandId);

                member.GetAttributeDate(brand, "BrandLastLogonDate");
                member.GetAttributeDate(brand, "Birthdate");

                var globalStatusMask = member.GetAttributeInt(brand, "GlobalStatusMask");

                if ((globalStatusMask & WebConstants.GLOBALSTATUSMASK_ADMIN_SUSPEND) ==
                    WebConstants.GLOBALSTATUSMASK_ADMIN_SUSPEND)
                {
                    isValid = false;
                }
            }
                // this type of exception is caught while trying to pull null attributes
            catch (NullReferenceException exception)
            {
                isValid = false;
                var logger = new Logger(typeof (MemberHelper));
                logger.Error(
                    string.Format("Error retrieving brand level attributes for Member:{0} CommnityId:{1}",
                        member.MemberID, communityId), exception);
            }

            return isValid;
        }

        /// <summary>
        ///     Determines if a Mingle member is valid for photo approval
        ///     based on a set of attributes
        /// </summary>
        /// <param name="member"></param>
        /// <param name="communityId"></param>
        /// <returns></returns>
        public static bool IsMingleMemberValidForApproval(Member member, int communityId)
        {
            var siteId = GeneralHelper.GetPrimarySiteForCommunity((CommunityIDs) communityId);
            var attributeSet =
                new ApiMingleMember(ApiClientMingle.GetInstance()).GetFullProfileAttributeSet(member.MemberID,
                    (int) siteId);

            // apply similar checks as the LA method above

            DateTime lastLoggedIn;
            if (!DateTime.TryParse(attributeSet.lastLoggedIn, out lastLoggedIn))
                return false;

            DateTime registrationDate;
            if (!DateTime.TryParse(attributeSet.registrationDate, out registrationDate))
                return false;

            if (attributeSet.age <= 0) return false;

            if (attributeSet.adminSuspendedFlag) return false;

            return true;
        }

        public static bool IsMemberHighlighted(Member member, BrandConfig.Brand brand)
        {
            bool highlighted = false;
            
            try
            {
                DateTime dteHighlightedExpirationDate = member.GetAttributeDate(brand, "HighlightedExpirationDate", DateTime.MinValue);

                /*
                Returns a boolean value of whether this member currently has the highlighted profile
                premium service and whether it is turned on.  This is independent of whether this
                member has an active subscription.				
                True if the member currently has the highlighted profile and is turned on or false if the member does not have the highlighted profile or has it but turned it off.  
                */
                if (dteHighlightedExpirationDate == DateTime.MinValue)
                {
                    // Member does not have the highlighted profile
                    highlighted = false;
                }
                else
                {
                    if (DateTime.Now < dteHighlightedExpirationDate)
                    {
                        // Highlighted profile purchase has not yet expired
                        // Check whether the highlighted profile is turned on
                        if ((member.GetAttributeInt(brand, "HighlightedFlag", 0) == 1))
                        {
                            // Highlighted profile is turned on
                            highlighted = true;
                        }
                        else
                        {
                            // Highlighted profile is turned off
                            highlighted =  false;
                        }
                    }
                    else
                    {
                        // Highlighted profile purchase has expired  
                        highlighted = false;
                    }
                }
            }
            catch (Exception ex)
            {
                highlighted = false;
            }

            return highlighted;
        }

        public static bool NotifyIovation(Member member, bool adminSuspend, AdminActionReasonID adminActionReasonId)
        {
            bool success = false;
            string banurl = RuntimeSettings.GetSetting("MINGLE_ROLF_BANISH_URL");
            string unbanurl = RuntimeSettings.GetSetting("MINGLE_ROLF_UNBANISH_URL");
            string rolfKey = RuntimeSettings.GetSetting("MINGLE_ROLF_KEY");
            string url = (adminSuspend) ? banurl : unbanurl;
            
            try
            {
                WebRequest iovationRequest = WebRequest.Create(url);
                iovationRequest.Method = "POST";
                iovationRequest.Timeout = 2000;
                var parameters = new ArrayList();

                //get siteids of member
                int[] membersites = member.GetSiteIDList();
                string siteids = "";
                for (int i = 0; i < membersites.Length; i++)
                {
                    siteids += (i == (membersites.Length - 1)) ? membersites[i].ToString() : membersites[i] + ",";
                }
                siteids = (siteids.Length > 0) ? string.Format("[{0}]", siteids) : "";
                    //send siteIds in [103,101] format

                parameters.Add(String.Format("{0}={1}", "auth", HttpUtility.UrlEncode(rolfKey)));
                parameters.Add(String.Format("{0}={1}", "memberid", HttpUtility.UrlEncode(member.MemberID.ToString())));
                parameters.Add(String.Format("{0}={1}", "siteids", HttpUtility.UrlEncode(siteids)));
                if (adminSuspend)
                    parameters.Add(String.Format("{0}={1}", "reasonid",
                                                 HttpUtility.UrlEncode(((int) adminActionReasonId).ToString())));

                // Set the encoding type
                iovationRequest.ContentType = "application/x-www-form-urlencoded";

                // Build a string containing all the parameters
                string parametersData = String.Join("&", (String[]) parameters.ToArray(typeof (string)));
                iovationRequest.ContentLength = parametersData.Length;

                // We write the parameters into the request
                var sw = new StreamWriter(iovationRequest.GetRequestStream());
                sw.Write(parametersData);
                sw.Close();

                // Execute the query
                var iovationResponse = (HttpWebResponse) iovationRequest.GetResponse();

                var sr = new StreamReader(iovationResponse.GetResponseStream());
                string response = sr.ReadToEnd();

                int result = 0;
                Int32.TryParse(response, out result);
                if (result == 1)
                    success = true;
            }
            catch(Exception ex)
            {
                success = false;
                var logger = new Logger(typeof(Controller));
                logger.Error("Error calling ROLF. NotifyIovation(). MemberID: " + member.MemberID + " Url: " + url ,ex);
            }
            return success;
        }

        public static bool HasPaymentProfile(int memberId, int siteId)
        {
            var hasPmtProfile = true;

            var renewalSub = Spark.Common.Adapter.RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetCurrentRenewalSubscription(memberId, siteId);

            if (renewalSub == null || renewalSub.RenewalSubscriptionID <= 0)
            {
                // Default payment profile may exists from a free trial subscription 
                // If the member took a free trial subscription, than show the details of the billing information used to take the free trial subscription. 
                var defaultPaymentProfileInfo =
                    Spark.Common.Adapter.PaymentProfileServiceWebAdapter.GetProxyInstance().
                        GetMemberDefaultPaymentProfile(memberId, siteId);

                hasPmtProfile = defaultPaymentProfileInfo != null;
            }

            return hasPmtProfile;
        }


        public static ExpandedUserInfo GetExpandedUserInfo(Member member, Brand brand)
        {
            ExpandedUserInfo info = new ExpandedUserInfo();

            // populate the model
            info.MemberSiteInfo = MemberHelper.GetMemberSiteInfo(member, brand.Site.SiteID);
            info.MemberID = member.MemberID;
            info.SiteId = brand.Site.SiteID;
            info.Email = member.EmailAddress;
            info.Username = member.GetUserName(brand);
            info.FirstName = member.GetAttributeText(brand, "SiteFirstName");
            info.LastName = member.GetAttributeText(brand, "SiteLastName");
            info.Age = MemberHelper.GetAge(member, brand);
            info.GenderSeekingGender = MemberHelper.GetGender(member, brand) + " seeking " +
                Enum.GetName(typeof(Enums.SeekingGender), MemberHelper.GetSeekingGender(member, brand));

            Hashtable resourceBag = GeneralHelper.GetResourceCollection(GeneralHelper.DetermineResourceFileName(brand));
            info.MaritalStatus = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, member, brand, "MaritalStatus");
            info.Occupation = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, member, brand, "IndustryType");
            info.Education = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, member, brand, "EducationLevel");
            info.Religion = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, member, brand, "Religion");

            if (brand.Site.Community.CommunityID == (int)CommunityIDs.JDate)
            {
                info.Ethnicity = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, member, brand, "JDateEthnicity");
            }
            else
            {
                info.Ethnicity = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, member, brand, "Ethnicity");
            }

            info.ProfileLocation = MemberHelper.GetRegionDisplayString(member, brand);

            // for non view purposes but we need these
            info.ProfileRegionID = member.GetAttributeInt(brand, "RegionID");
            info.GenderMask = member.GetAttributeInt(brand, "GenderMask");
            info.MaritalStatusIntValue = member.GetAttributeInt(brand, "MaritalStatus");
            info.EducationIntValue = member.GetAttributeInt(brand, "EducationLevel");
            info.ReligionIntValue = member.GetAttributeInt(brand, "Religion");
            info.EthnicityIntValue = member.GetAttributeInt(brand, "Ethnicity");
            // ===

            //ip address
            int intIPAddress = member.GetAttributeInt(brand, "RegistrationIP");
            IPAddress regIPAddress = null;
            if (intIPAddress != Constants.NULL_INT)
            {
                regIPAddress = new IPAddress(BitConverter.GetBytes(intIPAddress));
                info.IPAddress = regIPAddress.ToString();
            }

            //ip location
            if (regIPAddress != null)
            {
                info.IPLocation = new IPLocation.LookupService().getLocation(regIPAddress.ToString());
            }

            info.ThumbnailUrl = MemberPhotoHelper.GetDefaultPhotoThumbnailDisplayURL(member, brand);

            // fraud score
            info.FraudResult = GetFraudDetailedResponse( member.MemberID, brand.Site.SiteID,3);
            info.RegistrationDate = member.GetAttributeDate(brand, "BrandInsertDate");
            info.SubscriberStatus = MemberHelper.GetSubscriberStatusString(member, brand);

            //first sub date and billing phone number
            List<AccountHistory> colAccountHistory = new List<AccountHistory>();
            OrderInfo[] arrOrderInfo = OrderHistoryServiceWebAdapter.GetProxyInstanceForBedrock().GetMemberOrderHistoryByMemberID(member.MemberID, brand.Site.SiteID, 5000, 1);
            PaymentProfileInfo paymentProfileResponse = Spark.Common.Adapter.PaymentProfileServiceWebAdapter.GetProxyInstance().GetMemberDefaultPaymentProfile(member.MemberID, brand.Site.SiteID);
            int orderIDForDefaultPaymentProfile = 0;

            foreach (OrderInfo orderInfo in arrOrderInfo)
            {
                if (orderInfo.OrderDetail.Length > 0)
                {
                    // Only add orders that have order details  
                    AccountHistory accountHistory = new AccountHistory(orderInfo, brand, AccountHistoryType.Financial, member);
                    colAccountHistory.Add(accountHistory);

                    if (paymentProfileResponse != null && orderInfo.UserPaymentGuid == paymentProfileResponse.PaymentProfileID)
                    {
                        if (orderIDForDefaultPaymentProfile <= 0)
                        {
                            ObsfucatedPaymentProfileResponse obsfucatedpaymentProfileResponse = Spark.Common.Adapter.PaymentProfileServiceWebAdapter.GetProxyInstance().GetObsfucatedPaymentProfileByOrderID(orderInfo.OrderID, member.MemberID, brand.Site.SiteID);
                            if (obsfucatedpaymentProfileResponse != null && obsfucatedpaymentProfileResponse.Code == "0"
                                && obsfucatedpaymentProfileResponse.PaymentProfile != null && obsfucatedpaymentProfileResponse.PaymentProfile is ObsfucatedCreditCardPaymentProfile)
                            {
                                info.BillingPhoneNumber = ((ObsfucatedCreditCardPaymentProfile)obsfucatedpaymentProfileResponse.PaymentProfile).PhoneNumber;
                            }
                        }
                    }
                }
            }

            AccountHistory first = (from AccountHistory ah in colAccountHistory
                                    where ah.TransactionType == TransactionType.InitialSubscriptionPurchase
                                    && ah.OrderStatus == OrderStatus.Successful
                                    orderby ah.InsertDateInPST ascending
                                    select ah).Take(1).FirstOrDefault();

            if (first != null)
            {
                info.DaysSinceFirstSubscription = DateTime.Now.Subtract(first.InsertDateInPST).Days;
            }

            return info;
        }

        /// <summary>
        /// This method sets the AdminSuspend GlobalStatusMask bit for the caller needs to call Save() on method still. This method will
        /// update the admin action log, admin notes, call Iovation, and disable auto renewal.
        /// </summary>
        /// <param name="member"></param>
        /// <param name="brand"></param>
        /// <param name="adminActionReasonID"></param>
        /// <param name="actionMask"></param>
        /// <param name="communityID"></param>
        /// <param name="languageID"></param>
        /// <param name="adminID"></param>
        /// <returns></returns>
        public static bool SuspendMember(Member member, Brand brand, AdminActionReasonID adminActionReasonID, int actionMask, CommunityIDs communityID, int languageID, int adminID)
        {
            bool stoppedRenewals = false;

            int globalStatusMask = member.GetAttributeInt(brand, WebConstants.ATTRIBUTE_NAME_GLOBALSTATUSMASK);
            globalStatusMask = globalStatusMask | Matchnet.Lib.ConstantsTemp.ADMIN_SUSPEND;
            member.SetAttributeInt(brand, WebConstants.ATTRIBUTE_NAME_GLOBALSTATUSMASK, globalStatusMask);

            AdminSA.Instance.AdminActionLogInsert(member.MemberID, (int)communityID, actionMask | (int)AdminAction.AdminSuspendMember,
                            adminID, languageID);

            AdminSA.Instance.UpdateAdminNote(adminActionReasonID.ToString(), member.MemberID, adminID, (int)communityID, ConstantsTemp.ADMIN_NOTE_MAX_LENGTH);

            //Update legacy renewal
            //PurchaseVO.SubscriptionResult sr = PurchaseSA.Instance.EndSubscription(member.MemberID, g.AdminID, brand.Site.SiteID, 20);
            //if (sr.ReturnValue == Constants.RETURN_OK)
            //{
            //    int confirmationID = sr.MemberTranID;


            //}

            //Notify Iovation on suspend
            MemberHelper.NotifyIovation(member, true, adminActionReasonID);

            //Update UPS Renewal
            Spark.Common.RenewalService.RenewalResponse renewalResponse = Spark.Common.Adapter.RenewalServiceWebAdapter.GetProxyInstanceForBedrock().DisableAutoRenewal(member.MemberID,
                brand.Site.SiteID, 1, adminID, "", 20);
            if (renewalResponse.InternalResponse.responsecode != "0")
                throw new Exception("Failed to update with Unified Renewal to disable auto-renewal for MemberID: " + member.MemberID.ToString() + ", please try again or contact Software Engineering.");
            else
            {
                int confirmationID = renewalResponse.TransactionID;
                stoppedRenewals = true;
            }

            return stoppedRenewals;
        }

        /// <summary>
        /// This method sets the AdminSuspend GlobalStatusMask bit for the caller needs to call Save() on method still. This method will
        /// update the admin action log, call Iovation, and disable auto renewal. This method does not update the admin notes.
        /// </summary>
        /// <param name="member"></param>
        /// <param name="brand"></param>
        /// <param name="adminActionReasonID"></param>
        /// <param name="actionMask"></param>
        /// <param name="communityID"></param>
        /// <param name="languageID"></param>
        /// <param name="adminID"></param>
        /// <param name="useGlobalScopeForAdminLogAndNote"></param>
        /// <returns></returns>
        public static bool SuspendMember(Member member, Brand brand, AdminActionReasonID adminActionReasonID, int actionMask, CommunityIDs communityID, int languageID, int adminID, bool useGlobalScopeForAdminLogAndNote)
        {
            bool stoppedRenewals = false;

            int globalStatusMask = member.GetAttributeInt(brand, WebConstants.ATTRIBUTE_NAME_GLOBALSTATUSMASK);
            globalStatusMask = globalStatusMask | Matchnet.Lib.ConstantsTemp.ADMIN_SUSPEND;
            member.SetAttributeInt(brand, WebConstants.ATTRIBUTE_NAME_GLOBALSTATUSMASK, globalStatusMask);

            AdminSA.Instance.AdminActionLogInsert(member.MemberID,
                                                  useGlobalScopeForAdminLogAndNote
                                                      ? Constants.GROUP_PERSONALS
                                                      : (int) communityID,
                                                  actionMask | (int) AdminAction.AdminSuspendMember,
                                                  adminID, AdminMemberIDType.AdminProfileMemberID, adminActionReasonID);

            //Update legacy renewal
            //PurchaseVO.SubscriptionResult sr = PurchaseSA.Instance.EndSubscription(member.MemberID, g.AdminID, brand.Site.SiteID, 20);
            //if (sr.ReturnValue == Constants.RETURN_OK)
            //{
            //    int confirmationID = sr.MemberTranID;


            //}

            //Notify Iovation on suspend
            MemberHelper.NotifyIovation(member, true, adminActionReasonID);

            //Update UPS Renewal
            Spark.Common.RenewalService.RenewalResponse renewalResponse = Spark.Common.Adapter.RenewalServiceWebAdapter.GetProxyInstanceForBedrock().DisableAutoRenewal(member.MemberID,
                brand.Site.SiteID, 1, adminID, "", 20);
            if (renewalResponse.InternalResponse.responsecode != "0")
                throw new Exception("Failed to update with Unified Renewal to disable auto-renewal for MemberID: " + member.MemberID.ToString() + ", please try again or contact Software Engineering.");
            else
            {
                int confirmationID = renewalResponse.TransactionID;
                stoppedRenewals = true;
            }

            return stoppedRenewals;
        }

        public static DateTime GetFirstBrandInsertDate(Member member)
        {
            var ret = DateTime.MaxValue;

            var activeCommunityList = new int[] {1, 3, 10, 23, 24};
            foreach(int communityId in activeCommunityList)
            {
                var firstBrandInsertDate = member.GetInsertDate(communityId);
                if(firstBrandInsertDate != DateTime.MinValue && firstBrandInsertDate < ret)
                {
                    ret = firstBrandInsertDate;
                }
            }

            return ret == DateTime.MaxValue ? DateTime.MinValue : ret;
        }


        public static FraudDetailedResponse GetFraudDetailedResponse(int memberID, int siteId, int riskInquiryTypeId, string userpaymentID = "")
        {
            ApiFraudInfo apiPaymentUIPSON = new ApiFraudInfo(ApiClient.GetInstance());
            var fraudInfo = apiPaymentUIPSON.GetMemberFraudInfo(memberID, riskInquiryTypeId, siteId, userpaymentID);

            if (fraudInfo == null)
                throw new Exception("Unable to get payment data from Subcription. CreditCard page");
            return fraudInfo;
        }

    }


}
