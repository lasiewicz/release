﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using AdminTool.Models.ModelViews.SubAdmin;
using Matchnet;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using AT = AdminTool.Models.ModelViews.SubAdmin;

namespace AdminTool.Models
{
    public static class SubAdminHelper
    {
        public static void PopulateSelectListItems(PromoPriority promoPriority)
        {
            promoPriority.BrandIdChoices = GeneralHelper.GetActiveBrands();
            promoPriority.PaymentTypeChoices = GeneralHelper.GetPaymentTypeChoices();
            promoPriority.PromoTypeChoices = GeneralHelper.GetTargetablePromoTypeChoices();
        }

        public static void PopulateSelectListItems(PlanDisplayList plansDisplay)
        {
            plansDisplay.BrandIdChoices = GeneralHelper.GetActiveBrands();
            plansDisplay.BrandIdChoices.Insert(0, new SelectListItem() {Text = "--Select a site--", Value="0"});
        }

        public static void PopulateSelectListItems(AT.Promo promo)
        {
            promo.BrandIdChoices = GeneralHelper.GetActiveBrands();
            promo.RowChoices = GeneralHelper.GetPromoRowChoices();
            promo.ColChoices = GeneralHelper.GetPromoColChoices();
            promo.PaymentTypeChoices = GeneralHelper.GetPaymentTypeChoices();
            promo.PromoTypeChoices = GeneralHelper.GetPromoTypeChoices();

            #region Header SelectListItems
            if (promo.RowHeaders != null)
            {
                foreach (var h in promo.RowHeaders)
                {
                    var tempH = h;

                    h.DurationChoices = GeneralHelper.GetNumberedSelectListItems(1, 12,
                                                                                 new List<int>()
                                                                                     {
                                                                                         8,
                                                                                         9,
                                                                                         10,
                                                                                         11
                                                                                     });
                    h.DurationTypeChoices = GeneralHelper.GetDurationTypes(1);
                    if(h.DurationType != Constants.NULL_INT)
                    {
                        var query = (from SelectListItem item in h.DurationTypeChoices
                                     where item.Value == (tempH.DurationType).ToString()
                                     select item).Single();
                        query.Selected = true;
                    }

                    h.ResourceTemplateChoices = GeneralHelper.GetResourceTemplates(ResourceTemplateType.Row,
                                                                                   promo.BrandId);
                }
            }

            if (promo.ColHeaders != null)
            {
                foreach (var c in promo.ColHeaders)
                {
                    c.PlanTypeChoices = new List<SelectListItem>()
                                            {
                                                new SelectListItem() {Text = "Premium service plan", Value = "256"},
                                                new SelectListItem() {Text = "Standard plan", Value = "1"}
                                            };
                    c.PremiumTypeChoices = new List<SelectListItem>()
                                               {
                                                   new SelectListItem() {Text = "None or All", Value = "3"},
                                                   new SelectListItem() {Text = "Highlight Only", Value = "1"},
                                                   new SelectListItem() {Text = "Spotlight Only", Value = "2"},
                                                   new SelectListItem() {Text = "J-Meter Only", Value = "4"},
                                                   new SelectListItem() {Text = "All Access Only", Value = "96"},
                                                   new SelectListItem() {Text = "All Access, JMeter", Value = "100"}
                                               };

                    c.ResourceTemplateChoices =
                        GeneralHelper.GetResourceTemplates(ResourceTemplateType.Column,
                                                           promo.BrandId);
                }
            }
            #endregion

            #region PlansSelector SelectListItems
            if(promo.PlanSelectors != null)
            {
                var toRemove = new List<PlanSelector>();

                foreach(var pSel in promo.PlanSelectors)
                {
                    var tempPsel = pSel;
                    
                    var rowHeader = (from RowHeader rHeader in promo.RowHeaders
                                     where rHeader.HeaderNumber == tempPsel.RowNumber
                                     select rHeader).SingleOrDefault();
                    var colHeader = (from ColHeader cHeader in promo.ColHeaders
                                     where cHeader.HeaderNumber == tempPsel.ColumnNumber
                                     select cHeader).SingleOrDefault();

                    // bind the possible plans given the constraints set from step 2
                    if (rowHeader != null && colHeader != null)
                    {
                        // if this is a mixed column; both standard and premium
                        // colHeader.PlanType will actually return PlanType.None so that we get both types of plans
                        pSel.PlanChoices = GetSelectablePlans(promo.BrandId, (PlanType) colHeader.PlanType,
                                                              promo.PaymentType,
                                                              rowHeader.Duration, (DurationType) rowHeader.DurationType,
                                                              (PremiumType) colHeader.PremiumType);

                        pSel.ResourceTemplateChoices = GeneralHelper.GetResourceTemplates(ResourceTemplateType.Plan,
                                                                                          promo.BrandId);
                    }
                    else
                    {
                        // this means the dimensions are reduced and we no longer need this PlanSelector
                        toRemove.Add(tempPsel);
                    }
                }

                foreach(var pSelRemove in toRemove)
                {
                    promo.PlanSelectors.Remove(pSelRemove);
                }
            }
            #endregion

            #region Promo Expression
            // putting all these expression types in a generic list would be nice, but MVC is terrible at handling
            // editable collections so it would save us a lot of pain for doing this one by one for a finite number of elements
            var brand = BrandConfigSA.Instance.GetBrandByID(promo.BrandId);
            Hashtable resourceBag = GeneralHelper.GetResourceCollection(GeneralHelper.DetermineResourceFileName(brand));
            
            PopulateSelectListItems(promo.Country, resourceBag, promo.BrandId);
            PopulateSelectListItems(promo.State, resourceBag, promo.BrandId);
            PopulateSelectListItems(promo.City, resourceBag, promo.BrandId);
            PopulateSelectListItems(promo.DMAs, resourceBag, promo.BrandId);
            PopulateSelectListItems(promo.SubscriptionStatus, resourceBag, promo.BrandId);
            PopulateSelectListItems(promo.GenderSeekingGender, resourceBag, promo.BrandId);
            PopulateSelectListItems(promo.MaritalStatus, resourceBag, promo.BrandId);
            PopulateSelectListItems(promo.Education, resourceBag, promo.BrandId);
            PopulateSelectListItems(promo.Profession, resourceBag, promo.BrandId);
            PopulateSelectListItems(promo.DeviceOS, resourceBag, promo.BrandId);

            #endregion

            #region Default Plan Dropdown
            promo.DefaultPlanChoices = new List<SelectListItem>();
            promo.DefaultPlanChoices.Add(new SelectListItem() {Text = "None", Value = Constants.NULL_INT.ToString()});
            if(promo.PlanSelectors != null)
            {
                foreach(var planSel in promo.PlanSelectors)
                {
                    if(planSel.PlanId != Constants.NULL_INT)
                    {
                        promo.DefaultPlanChoices.Add(new SelectListItem()
                                                         {
                                                             Text = GetSimplePlanInfoString(planSel.PlanId, promo.BrandId),
                                                             Value = planSel.PlanId.ToString()
                                                         });
                    }
                }
            }
            #endregion

            promo.PromoTimeChoices = GeneralHelper.GetHourlyTimeChoices();
            promo.DayOfTheWeekChoices = GeneralHelper.GetDayOfTheWeekChoices();
            promo.PageTemplateChoices = GeneralHelper.GetUPSTemplates(promo.BrandId);
        }

        public static string GetSimplePlanInfoString(int planId, int brandId)
        {
            string ret = string.Empty;

            var plan = PlanSA.Instance.GetPlan(planId, brandId);
            if (plan == null)
                return ret;

            ret = string.Format("{0}: {1}, {2}, {3}",
                                plan.PlanID,
                                plan.InitialCost,
                                plan.RenewCost,
                                plan.PremiumTypeMask == PremiumType.None ? "STD" : "PREM");

            return ret;
        }

        public static string GetPlanInfoString(int planId, int brandId)
        {
            var plan = PlanSA.Instance.GetPlan(planId, brandId);

            if (plan == null)
                return string.Empty;

            string formatString = "N2";
            Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandId);

            //Not sure why we needed to do specific formating for IL
            //if (targetBrand.Site.LanguageID == (int)Matchnet.Language.Hebrew)  
            //    formatString = "N0";


            return string.Format("{0} ({5}) - {1} {2} @ {3}, {4} ren", 
                            plan.PlanID.ToString(), plan.InitialDuration.ToString(), 
                            Enum.GetName(typeof(DurationType), plan.InitialDurationType), 
                            plan.InitialCost.ToString(formatString),
                            plan.RenewCost.ToString(formatString), 
                            (plan.PlanTypeMask & PlanType.PremiumPlan) > 0 ? "PREM" : "STRD"
                          );
        }

        public static void PopulateSelectListItems(PromoExpressionBase promoExpression, Hashtable resourceBag, int brandId)
        {
            if (promoExpression is PromoExpressionSelectListBase)
            {
                var promoExpressionSelectListBase = (PromoExpressionSelectListBase)promoExpression;

                promoExpressionSelectListBase.BrandId = brandId;
                promoExpressionSelectListBase.ResourceBag = resourceBag;
                promoExpressionSelectListBase.PopulateSelectListItems();
            }
        }

        public static List<SelectListItem> GetSelectablePlans(int brandId, PlanType planType, PaymentType paymentType,
            int duration, DurationType durationType, PremiumType premiumType)
        {

            PremiumType modifiedPremiumType = PremiumType.None;
            if (planType == PlanType.PremiumPlan)
                modifiedPremiumType = premiumType;

            PlanCollection planCollection = PlanSA.Instance.GetPlans(brandId,
                                                                        planType,
                                                                        paymentType,
                                                                        duration,
                                                                        durationType,
                                                                        modifiedPremiumType);

            // this means we don't have any plan that matches the filters passed in
            if (planCollection == null)
            {
                return new List<SelectListItem>()
                           {
                               new SelectListItem() {Text = "None", Value = Constants.NULL_INT.ToString()}
                           };
            }

            PlanCollection filteredPlans = null;

            filteredPlans = planType == PlanType.Regular ? GetOnlyStandardPlans(planCollection, brandId) : planCollection;

            var plans = filteredPlans.Cast<Matchnet.Purchase.ValueObjects.Plan>().OrderBy(x => x.PlanID).ToList();
            var plansSelectList = (from Matchnet.Purchase.ValueObjects.Plan plan in plans
                                   select new SelectListItem() { Text = plan.PlanID.ToString(), Value = plan.PlanID.ToString() }).ToList();

            plansSelectList.Insert(0, new SelectListItem() {Text = "None", Value = Constants.NULL_INT.ToString()});

            return plansSelectList;
        }

        public static PlanCollection GetOnlyStandardPlans(PlanCollection plans, int brandId)
        {
            PlanCollection stdPlans = null;

            if (plans != null)
            {
                stdPlans = new PlanCollection(brandId);

                // exclude records that have premium plan type flag
                var standardPlans = from Matchnet.Purchase.ValueObjects.Plan plan in plans
                                    where ((int)plan.PlanTypeMask & (int)PlanType.PremiumPlan) == 0
                                    select plan;

                foreach (Matchnet.Purchase.ValueObjects.Plan stdPlan in standardPlans)
                {
                    stdPlans.Add(stdPlan);
                }
            }

            return stdPlans;
        }

        public static void PopulateSelectListItems(AT.Plan plan)
        {
            plan.BrandIds = GeneralHelper.GetActiveBrands();
            plan.DurationTypes = GeneralHelper.GetDurationTypes(2); // 2=hour
            plan.RenewalDurationTypes = GeneralHelper.GetDurationTypes(3); // 3=day
            plan.FreeDurationTypes = GeneralHelper.GetDurationTypes(0); // 0=none
            plan.PlanTypes = GeneralHelper.GetPlanTypes();
            plan.PremiumPlanTypes = GeneralHelper.GetPremiumPlanTypes();
            plan.ALaCartePlanTypes = GeneralHelper.GetALaCartePlanTypes();
        }

        public static string GetValueStringFromMaskType(int promoAttributeId, List<Expression> expressions)
        {
            string ret = "All";
            if (expressions == null || expressions.Count == 0)
                return ret;

            if (promoAttributeId == 1000 || promoAttributeId == 69 || promoAttributeId == 1012)
            {
                Type enumType = null;

                if (promoAttributeId == 1000)
                    enumType = typeof(SubscriptionStatus);
                else if (promoAttributeId == 1012)
                    enumType = typeof(Matchnet.Member.ValueObjects.Enumerations.DeviceOS);
                else
                    enumType = typeof(GenderSeekingGender);

                // This field is not a mask field really although it appears that way on the UI
                // It's captured as 1 criteria expression with multiple values
                if (expressions.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (int value in expressions[0].Values)
                    {
                        sb.Append(Enum.GetName(enumType, value).Replace("_", " ") + ", ");
                    }

                    if (sb.Length > 0)
                        sb.Remove(sb.Length - 2, 2);

                    ret = sb.ToString();
                }
            }

            return ret;
        }

        public static string GetValueStringFromRangeType(int promoAttributeId, List<Expression> expressions)
        {
            string ret = "All";
            if (expressions == null || expressions.Count == 0)
                return ret;

            // Let's identify the behavior of this range attribute
            RangeSelectorBehavior beh = GetBehaviorForAttribute(promoAttributeId);

            // We know the behavior, act accordingly
            if (beh == RangeSelectorBehavior.Default)
            {
                // this is the simple case, just figure out which value is the lower one of the two
                string lessVal = string.Empty;
                string higherVal = string.Empty;

                foreach (Expression exp in expressions)
                {
                    if (exp.Values != null && exp.Values.Count > 0)
                    {
                        if (exp.Operator == OperatorType.LessThanEqual)
                            higherVal = exp.Values[0].ToString();
                        else if (exp.Operator == OperatorType.GreaterThanEqual)
                            lessVal = exp.Values[0].ToString();
                    }
                }

                if (lessVal == string.Empty || higherVal == string.Empty)
                {
                    if (lessVal == string.Empty)
                        ret = string.Format("Less than {0}", higherVal);
                    else
                        ret = string.Format("Greater than {0}", lessVal);
                }
                else
                {
                    ret = lessVal + " - " + higherVal;
                }
            }
            else if (beh == RangeSelectorBehavior.TextBoxPositionAndSignReversed)
            {
                string atLeast = string.Empty;
                string noMoreThan = string.Empty;

                foreach (Expression exp in expressions)
                {
                    if (exp.Values != null && exp.Values.Count > 0)
                    {
                        if (exp.Operator == OperatorType.LessThanEqual)
                            atLeast = (-(int)exp.Values[0]).ToString();
                        else if (exp.Operator == OperatorType.GreaterThanEqual)
                            noMoreThan = (-(int)exp.Values[0]).ToString();
                    }
                }

                if (atLeast == string.Empty || noMoreThan == string.Empty)
                {
                    if (noMoreThan == string.Empty)
                        ret = string.Format("At least {0}", atLeast);
                    else
                        ret = string.Format("No more than {0}", noMoreThan);
                }
                else
                {
                    ret = atLeast + " - " + noMoreThan;
                }
            }
            else
            {
                // for both LessThanEqualOnly and TextBoxPositionReversed cases, positive number indicates the UNTIL number and the negative
                // number indicates the SINCE number
                string untilNum = string.Empty;
                string sinceNum = string.Empty;

                foreach (Expression exp in expressions)
                {
                    if (exp.Values != null && exp.Values.Count > 0)
                    {
                        if ((int)exp.Values[0] < 0)
                            sinceNum = (-(int)exp.Values[0]).ToString();
                        else
                            untilNum = exp.Values[0].ToString();
                    }
                }

                ret = (untilNum == string.Empty ? "-" : untilNum) + " / " + (sinceNum == string.Empty ? "-" : sinceNum);

            }

            return ret;
        }

        public static string GetValueStringFromEqualType(int promoAttributeID, List<Expression> expressions, int brandId, int languageId)
        {
            string ret = "All";
            if (expressions == null || expressions.Count == 0)
                return ret;

            if (expressions != null && expressions.Count > 0 && expressions[0].Values != null && expressions[0].Values.Count > 0)
            {
                switch (promoAttributeID)
                {
                    case 1007:  // DMA
                        // Loop through DMA values since multi select is allowed now
                        var sb = new StringBuilder();
                        for (int i = 0; i < expressions[0].Values.Count; i++)
                        {
                            var dma = RegionSA.Instance.GetDMAByDMAID((int)expressions[0].Values[i]);
                            if (dma != null)
                            {
                                sb.Append(string.Format("{0} ({1})", dma.DMADescription, dma.DMAID.ToString()));
                                if (i < expressions[0].Values.Count - 1)
                                    sb.Append(",");
                            }
                        }

                        if (sb.Length > 0)
                            ret = sb.ToString();
                        break;
                    case 1004:  // Country
                    case 1005:  // State
                    case 1006:  // City
                        ret = RegionSA.Instance.RetrieveRegionByID((int)expressions[0].Values[0], languageId).Description;
                        break;
                    case 32:    // marital status
                    case 89:    // education
                    case 392:   // profession
                        ret = GetPromoAttributeText(promoAttributeID, (int)expressions[0].Values[0], brandId);
                        break;
                    default:
                        ret = GetAppendedStringForExpressionValues(expressions[0].Values);
                        break;
                }
            }

            return ret;
        }

        public static string GetPromoAttributeText(int promoAttributeId, int value, int brandId)
        {
            string attributeName = AttributeMetadataSA.Instance.GetAttributes().GetAttribute(promoAttributeId).Name;
            string ret = string.Empty;

            var attOptions = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, brandId);

            var query = (from AttributeOption option in attOptions
                         where option.Value == value
                         select option).FirstOrDefault();

            if (query != null)
                ret = query.Description;

            return ret;
        }

        public static string GetAppendedStringForExpressionValues(ArrayList expValues)
        {
            var sb = new StringBuilder();

            for (int i = 0; i < expValues.Count; i++)
            {
                sb.Append(expValues[i].ToString());

                if (i < expValues.Count - 1)
                    sb.Append(",");
            }

            return sb.ToString();
        }

        public static RangeSelectorBehavior GetBehaviorForAttribute(int attributeId)
        {
            var bh = RangeSelectorBehavior.Default;

            switch (attributeId)
            {
                case 1001:
                    bh = RangeSelectorBehavior.TextBoxPositionAndSignReversed;
                    break;
                case 1008:
                    bh = RangeSelectorBehavior.TextBoxPositionReversed;
                    break;
            }

            return bh;
        }

        public static void AddPromoExpression(PromoExpressionBase promoExpression, Matchnet.PromoEngine.ValueObjects.Promo promo)
        {
            var expList = promoExpression.GetPromoExpression();
            foreach (var expression in expList)
            {
                promo.Criteria.Add(expression);
            }
        }
    }
}