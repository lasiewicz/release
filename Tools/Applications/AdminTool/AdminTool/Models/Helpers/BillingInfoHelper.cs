﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminTool.Models.ModelViews.Member;
using Spark.Common.DiscountService;
using Spark.Common.PaymentProfileService;
using Spark.CommonLibrary;
using Purchase = Matchnet.Purchase.ValueObjects;

namespace AdminTool.Models
{
    public class BillingInfoHelper
    {
        public BillingInfo GetBillingInfoFromOrder(int orderID, int siteID, int memberID)
        {
            BillingInfo billingInfoModel = new BillingInfo();
            billingInfoModel.UsedForDefaultPaymentProfile = false;
            billingInfoModel.OrderID = orderID;

            ObsfucatedPaymentProfileResponse paymentProfile = Spark.Common.Adapter.PaymentProfileServiceWebAdapter.GetProxyInstance().GetObsfucatedPaymentProfileByOrderID(orderID, memberID, siteID);

            if (paymentProfile != null
                && paymentProfile.Code == "0"
                && paymentProfile.PaymentProfile != null)
            {
                populateBillingModel(billingInfoModel, paymentProfile);
                if (siteID == (int)SiteIDs.JDateCoIL || siteID == (int)SiteIDs.Cupid)
                    billingInfoModel.ShowGovernmentIssueID = true;
                else
                    billingInfoModel.ShowGovernmentIssueID = false;
            }
            else
            {
                billingInfoModel.PaymentType = Spark.Common.PurchaseService.PaymentType.None;
            }

            return billingInfoModel;
        }

        public BillingInfo GetBillingInfoFromGift(string giftCode, int siteID, int memberID)
        {
            BillingInfo billingInfoModel = new BillingInfo();
            billingInfoModel.UsedForDefaultPaymentProfile = false;

            Gift gift = new GiftManager().GetGift(giftCode);
            if(gift != null && gift.GiftRedemption != null && gift.GiftRedemption.CustomerID == memberID)
            {
                ObsfucatedPaymentProfileResponse paymentProfile = Spark.Common.Adapter.PaymentProfileServiceWebAdapter.GetProxyInstance().GetObsfucatedPaymentProfileByUserPaymentID(gift.GiftRedemption.UserPaymentGUID, gift.GiftRedemption.CallingSystemID);
                if (paymentProfile != null
                && paymentProfile.Code == "0"
                && paymentProfile.PaymentProfile != null)
                {
                    populateBillingModel(billingInfoModel, paymentProfile);
                }
                else
                {
                    billingInfoModel.PaymentType = Spark.Common.PurchaseService.PaymentType.None;
                }
            }
            else
            {
                billingInfoModel.PaymentType = Spark.Common.PurchaseService.PaymentType.None;
            }

            return billingInfoModel;
        }

        public BillingInfo GetBillingInfoFromFreeTrial(string userpaymentGUID, int siteID, int memberID)
        {
            BillingInfo billingInfoModel = new BillingInfo();
            billingInfoModel.UsedForDefaultPaymentProfile = false;

            ObsfucatedPaymentProfileResponse paymentProfile = Spark.Common.Adapter.PaymentProfileServiceWebAdapter.GetProxyInstance().GetObsfucatedPaymentProfileByUserPaymentID(userpaymentGUID, siteID);

            if (paymentProfile != null
                && paymentProfile.Code == "0"
                && paymentProfile.PaymentProfile != null)
            {
                populateBillingModel(billingInfoModel, paymentProfile);
            }
            else
            {
                billingInfoModel.PaymentType = Spark.Common.PurchaseService.PaymentType.None;
            }

            return billingInfoModel;
        }

        private void populateBillingModel(BillingInfo billingInfoModel, ObsfucatedPaymentProfileResponse paymentProfile)
        {
            if (paymentProfile.PaymentProfile is ObsfucatedCreditCardPaymentProfile)
            {
                billingInfoModel.PaymentType = Spark.Common.PurchaseService.PaymentType.CreditCard;
                ObsfucatedCreditCardPaymentProfile creditCardPaymentProfile = (ObsfucatedCreditCardPaymentProfile)paymentProfile.PaymentProfile;
                billingInfoModel.PaymentProfileId = creditCardPaymentProfile.PaymentProfileID.ToString();
                billingInfoModel.CardType = creditCardPaymentProfile.CardType;
                billingInfoModel.City = creditCardPaymentProfile.City;
                billingInfoModel.Country = creditCardPaymentProfile.Country;
                billingInfoModel.ExpMonth = creditCardPaymentProfile.ExpMonth;
                billingInfoModel.ExpYear = creditCardPaymentProfile.ExpYear;
                billingInfoModel.FirstName = creditCardPaymentProfile.FirstName;
                billingInfoModel.LastFourDigitsCreditCardNumber = creditCardPaymentProfile.LastFourDigitsCreditCardNumber;
                billingInfoModel.LastName = creditCardPaymentProfile.LastName;
                billingInfoModel.PhoneNumber = creditCardPaymentProfile.PhoneNumber;
                billingInfoModel.PostalCode = creditCardPaymentProfile.PostalCode;
                billingInfoModel.State = creditCardPaymentProfile.State;
                billingInfoModel.StreetAddress = creditCardPaymentProfile.StreetAddress;
                billingInfoModel.GovernmentIssuedID = creditCardPaymentProfile.GovernmentIssuedID;
            }
            else if (paymentProfile.PaymentProfile is ObfuscatedCreditCardShortFormMobileSitePaymentProfile)
            {
                billingInfoModel.PaymentType = Spark.Common.PurchaseService.PaymentType.CreditCardShortFormMobileSite;
                ObfuscatedCreditCardShortFormMobileSitePaymentProfile creditCardShortFormMobileSitePaymentProfile = (ObfuscatedCreditCardShortFormMobileSitePaymentProfile)paymentProfile.PaymentProfile;
                billingInfoModel.PaymentProfileId = creditCardShortFormMobileSitePaymentProfile.PaymentProfileID.ToString();
                billingInfoModel.CardType = creditCardShortFormMobileSitePaymentProfile.CardType;
                billingInfoModel.ExpMonth = creditCardShortFormMobileSitePaymentProfile.ExpMonth;
                billingInfoModel.ExpYear = creditCardShortFormMobileSitePaymentProfile.ExpYear;
                billingInfoModel.LastFourDigitsCreditCardNumber = creditCardShortFormMobileSitePaymentProfile.LastFourDigitsCreditCardNumber;
                billingInfoModel.FirstName = creditCardShortFormMobileSitePaymentProfile.FirstName;
                billingInfoModel.LastName = creditCardShortFormMobileSitePaymentProfile.LastName;
                billingInfoModel.PostalCode = creditCardShortFormMobileSitePaymentProfile.PostalCode;
            }
            else if (paymentProfile.PaymentProfile is ObfuscatedCreditCardShortFormFullWebSitePaymentProfile)
            {
                billingInfoModel.PaymentType = Spark.Common.PurchaseService.PaymentType.CreditCardShortFormFullWebSite;
                ObfuscatedCreditCardShortFormFullWebSitePaymentProfile creditCardShortFormFullWebSitePaymentProfile = (ObfuscatedCreditCardShortFormFullWebSitePaymentProfile)paymentProfile.PaymentProfile;
                billingInfoModel.PaymentProfileId = creditCardShortFormFullWebSitePaymentProfile.PaymentProfileID.ToString();
                billingInfoModel.CardType = creditCardShortFormFullWebSitePaymentProfile.CardType;
                billingInfoModel.ExpMonth = creditCardShortFormFullWebSitePaymentProfile.ExpMonth;
                billingInfoModel.ExpYear = creditCardShortFormFullWebSitePaymentProfile.ExpYear;
                billingInfoModel.LastFourDigitsCreditCardNumber = creditCardShortFormFullWebSitePaymentProfile.LastFourDigitsCreditCardNumber;
                billingInfoModel.FirstName = creditCardShortFormFullWebSitePaymentProfile.FirstName;
                billingInfoModel.LastName = creditCardShortFormFullWebSitePaymentProfile.LastName;
                billingInfoModel.PostalCode = creditCardShortFormFullWebSitePaymentProfile.PostalCode;
            }
            else if (paymentProfile.PaymentProfile is ObsfucatedCheckPaymentProfile)
            {
                billingInfoModel.PaymentType = Spark.Common.PurchaseService.PaymentType.Check;
                ObsfucatedCheckPaymentProfile checkPaymentProfile = (ObsfucatedCheckPaymentProfile)paymentProfile.PaymentProfile;
                billingInfoModel.PaymentProfileId = checkPaymentProfile.PaymentProfileID.ToString();
                billingInfoModel.BankAccountNumberLastFour = checkPaymentProfile.BankAccountNumberLastFour;
                billingInfoModel.BankAccountType = checkPaymentProfile.BankAccountType;
                billingInfoModel.BankName = checkPaymentProfile.BankName;
                billingInfoModel.BankState = checkPaymentProfile.BankState;
                billingInfoModel.City = checkPaymentProfile.City;
                billingInfoModel.EmailAddress = checkPaymentProfile.EmailAddress;
                billingInfoModel.FirstName = checkPaymentProfile.FirstName;
                billingInfoModel.LastName = checkPaymentProfile.LastName;
                billingInfoModel.PhoneNumber = checkPaymentProfile.Phone;
                billingInfoModel.PostalCode = checkPaymentProfile.Zip;
                billingInfoModel.BankRoutingNumberLastFour = checkPaymentProfile.BankRoutingNumberLastFour;
                billingInfoModel.State = checkPaymentProfile.State;
                billingInfoModel.StreetAddress = checkPaymentProfile.Street;
            }
            else if (paymentProfile.PaymentProfile is ObsfucatedPaypalPaymentProfile)
            {
                billingInfoModel.PaymentType = Spark.Common.PurchaseService.PaymentType.PayPalLitle;
                ObsfucatedPaypalPaymentProfile paypalPaymentProfile = (ObsfucatedPaypalPaymentProfile)paymentProfile.PaymentProfile;
                billingInfoModel.PaymentProfileId = paypalPaymentProfile.PaymentProfileID.ToString();
                billingInfoModel.FirstName = paypalPaymentProfile.FirstName;
                billingInfoModel.LastName = paypalPaymentProfile.LastName;
                billingInfoModel.EmailAddress = paypalPaymentProfile.Email;
                billingInfoModel.Country = paypalPaymentProfile.Country;
                billingInfoModel.PayerStatus = paypalPaymentProfile.PayerStatus;
                billingInfoModel.PaypalToken = paypalPaymentProfile.PaypalToken;
                billingInfoModel.BillingAgreementID = paypalPaymentProfile.BillingAgreementID;
            }
            else if (paymentProfile.PaymentProfile is ObfuscatedManualPaymentProfile)
            {
                billingInfoModel.PaymentType = Spark.Common.PurchaseService.PaymentType.PaypalDirect;
                ObfuscatedManualPaymentProfile manualPaymentProfile = (ObfuscatedManualPaymentProfile)paymentProfile.PaymentProfile;
                billingInfoModel.PaymentProfileId = manualPaymentProfile.PaymentProfileID.ToString();
                billingInfoModel.FirstName = manualPaymentProfile.FirstName;
                billingInfoModel.LastName = manualPaymentProfile.LastName;
            }
            else if (paymentProfile.PaymentProfile is ObfuscatedElectronicFundsTransferProfile)
            {
                billingInfoModel.PaymentType = Spark.Common.PurchaseService.PaymentType.ElectronicFundsTransfer;
                ObfuscatedElectronicFundsTransferProfile electronicFundsTransferProfile = (ObfuscatedElectronicFundsTransferProfile)paymentProfile.PaymentProfile;
                billingInfoModel.PaymentProfileId = electronicFundsTransferProfile.PaymentProfileID.ToString();
                billingInfoModel.BankAccountNumberLastFour = electronicFundsTransferProfile.BankAccountNumberLastFour;
                billingInfoModel.BankAccountType = electronicFundsTransferProfile.BankAccountType;
                billingInfoModel.City = electronicFundsTransferProfile.City;
                billingInfoModel.EmailAddress = electronicFundsTransferProfile.EmailAddress;
                billingInfoModel.FirstName = electronicFundsTransferProfile.FirstName;
                billingInfoModel.LastName = electronicFundsTransferProfile.LastName;
                billingInfoModel.PhoneNumber = electronicFundsTransferProfile.Phone;
                billingInfoModel.PostalCode = electronicFundsTransferProfile.Zip;
                billingInfoModel.BankRoutingNumberLastFour = electronicFundsTransferProfile.BankRoutingNumberLastFour;
                billingInfoModel.State = electronicFundsTransferProfile.State;
                billingInfoModel.StreetAddress = electronicFundsTransferProfile.Street;
            }
            else if (paymentProfile.PaymentProfile is ObfuscatedPaymentReceivedProfile)
            {
                billingInfoModel.PaymentType = Spark.Common.PurchaseService.PaymentType.PaymentReceived;
                ObfuscatedPaymentReceivedProfile paymentReceivedProfile = (ObfuscatedPaymentReceivedProfile)paymentProfile.PaymentProfile;
                billingInfoModel.PaymentProfileId = paymentReceivedProfile.PaymentProfileID.ToString();
                billingInfoModel.FirstName = paymentReceivedProfile.FirstName;
                billingInfoModel.LastName = paymentReceivedProfile.LastName;
            }
            else if (paymentProfile.PaymentProfile is ObfuscatedDebitCardPaymentProfile)
            {
                billingInfoModel.PaymentType = Spark.Common.PurchaseService.PaymentType.DebitCard;
                ObfuscatedDebitCardPaymentProfile debitCardPaymentProfile = (ObfuscatedDebitCardPaymentProfile)paymentProfile.PaymentProfile;
                billingInfoModel.CardType = debitCardPaymentProfile.CardType;
                billingInfoModel.City = debitCardPaymentProfile.City;
                billingInfoModel.Country = debitCardPaymentProfile.Country;
                billingInfoModel.ExpMonth = debitCardPaymentProfile.ExpMonth;
                billingInfoModel.ExpYear = debitCardPaymentProfile.ExpYear;
                billingInfoModel.FirstName = debitCardPaymentProfile.FirstName;
                billingInfoModel.LastFourDigitsCreditCardNumber = debitCardPaymentProfile.LastFourDigitsCreditCardNumber;
                billingInfoModel.LastName = debitCardPaymentProfile.LastName;
                billingInfoModel.PhoneNumber = debitCardPaymentProfile.PhoneNumber;
                billingInfoModel.PostalCode = debitCardPaymentProfile.PostalCode;
                billingInfoModel.State = debitCardPaymentProfile.State;
                billingInfoModel.StreetAddress = debitCardPaymentProfile.StreetAddress;
                billingInfoModel.GovernmentIssuedID = debitCardPaymentProfile.GovernmentIssuedID;
            }
            else
            {
                billingInfoModel.PaymentType = Spark.Common.PurchaseService.PaymentType.None;
            }
        }
    }
}