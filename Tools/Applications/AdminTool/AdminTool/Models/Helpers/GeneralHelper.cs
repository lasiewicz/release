﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using Spark.CommonLibrary;
using AdminTool.Models;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.Region;
using BrandConfig = Matchnet.Content.ValueObjects.BrandConfig;
using Purchase = Matchnet.Purchase.ValueObjects;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Security;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using System.Text;
using Matchnet.PremiumServiceSearch11.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Member.ServiceAdapters;
using System.Resources;
using AdminTool.Models.ModelViews.Member;
using System.Xml;
using System.Text.RegularExpressions;
using Spark.Common.CatalogService;
using Spark.Common.Adapter;
using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;

namespace AdminTool.Models
{
    public enum GenderSeekingGender
    {
        Male_Seeking_Female = 9,
        Male_Seeking_Male = 5,
        Female_Seeking_Male = 6,
        Female_Seeking_Female = 10
    }

    public class MyPolicy : ICertificatePolicy
    {
        public bool CheckValidationResult(
            ServicePoint srvPoint
            , X509Certificate certificate
            , WebRequest request
            , int certificateProblem)
        {

            //Return True to force the certificate to be accepted.
            return true;

        } // end CheckValidationResult
    } // class MyPolicy

    public class UPSTemplate
    {
        public int TemplateID { get; set; }
        public string Description { get; set; }
    }

    public static class GeneralHelper
    {
        private const Double RadiansToDegrees = 180 / Math.PI;

        public static int GetTextApprovalStatusMask(string rawFormValue)
        {
            if (string.IsNullOrEmpty(rawFormValue))
                return 0;

            int statusMask = 0;
            
            string[] statuses = rawFormValue.Split(',');

            foreach (string status in statuses)
            {
                statusMask = statusMask + Convert.ToInt32(status);
            }

            return statusMask;
        }

        public static string RemoveHtmlTags(string newValue)
        {
            if (newValue == null || newValue.Length < 1)
            {
                return String.Empty;
            }

            // Strip any HTML junk out...
            newValue = Regex.Replace(newValue, "<(.|\n)+?>", "", RegexOptions.IgnoreCase);
            newValue = Regex.Replace(newValue, "&lt;(.|\n)+?>", "", RegexOptions.IgnoreCase);

            return newValue;
        }

        public static int GetGroupIDForAttribute(BrandConfig.Brand brand, Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute)
        {
            int groupID = 0;

            switch (attribute.Scope)
            {
                case Matchnet.Content.ValueObjects.AttributeMetadata.ScopeType.Brand:
                    groupID = brand.BrandID;
                    break;
                case Matchnet.Content.ValueObjects.AttributeMetadata.ScopeType.Community:
                    groupID = brand.Site.Community.CommunityID;
                    break;
                case Matchnet.Content.ValueObjects.AttributeMetadata.ScopeType.Site:
                    groupID = brand.Site.SiteID;
                    break;
                case Matchnet.Content.ValueObjects.AttributeMetadata.ScopeType.EnterpriseGlobal:
                    // should never hit this, but just in case we have an attribute of this scope later
                    // currently in the database, we don't even have a groupID defined for this
                    break;
                case Matchnet.Content.ValueObjects.AttributeMetadata.ScopeType.Personals:
                    groupID = 8383;
                    break;

            }

            return groupID;
        }

        public static bool TryGetFromFormCollection(FormCollection collection, string formFieldName, out string fieldValue)
        {
            bool doesExists = false;
            fieldValue = string.Empty;

            if (collection[formFieldName] != null)
            {
                doesExists = true;
                fieldValue = collection[formFieldName];

                if (fieldValue.ToLower().Contains("true") || fieldValue.ToLower().Contains("false"))
                    return doesExists;

                if (fieldValue.Contains(","))
                {
                    // combine the mask values separated by commas
                    string[] splits = fieldValue.Split(',');
                    int combinedMaskValues = 0;
                    foreach (string s in splits)
                    {
                        combinedMaskValues |= int.Parse(s);
                    }

                    fieldValue = combinedMaskValues.ToString();
                }
            }

            return doesExists;
        }

        public static Hashtable GetResourceCollection(string resourceFilePath)
        {
            Hashtable resourceCollection = new Hashtable();

            if (!System.IO.File.Exists(resourceFilePath))
            {
                return resourceCollection; // can't find the file
            }

            try
            {
                using (ResXResourceReader reader = new ResXResourceReader(resourceFilePath))
                {
                    try
                    {
                        foreach (DictionaryEntry entry in reader)
                        {
                            try
                            {
                                resourceCollection.Add(entry.Key.ToString(), entry.Value.ToString());
                            }
                            catch (Exception ex)
                            {
                                // This is to ignore object not found error when no proper <value/> is present within a <data/> tag
                            }
                        }
                        return resourceCollection;
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to parse the resources from " + resourceFilePath + ".", ex);
            }
        }

        public static string GetResource(Hashtable resourceBag, string resourceKey)
        {
            if (resourceBag != null && resourceBag.ContainsKey(resourceKey))
                return resourceBag[resourceKey].ToString();
            else
                return resourceKey;
        }

        public static string DetermineResourceFileName(BrandConfig.Brand brand)
        {
            string fileLocation = string.Empty;

            if (brand != null)
            {
                fileLocation = "/Content/XML/Global." + brand.Site.SiteID.ToString() + "." + brand.Site.CultureInfo.Name + ".resx";
            }

            return HttpContext.Current.Server.MapPath(fileLocation.ToLower());
        }

        public static string DetermineSearchXMLFileName(int siteID)
        {
            string fileLocation = string.Empty;

            fileLocation = "/Content/XML/Search." + siteID.ToString() + ".xml";

            return HttpContext.Current.Server.MapPath(fileLocation.ToLower());
        }

        public static List<SearchPreference> GetSearchPreferencesSection(int siteID)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(DetermineSearchXMLFileName(siteID));

            List<SearchPreference> searchPrefs = new List<SearchPreference>();
            foreach (XmlNode node in doc.ChildNodes[0].ChildNodes)
            {
                if (node.Name.ToLower() != "section")
                {
                    throw new Exception("Invalid node name: " + node.Name + ".  Expected 'Section'.");
                }

                if (node.Attributes["Name"].Value.ToLower() == "basics")
                {
                    searchPrefs.AddRange(GetChildNodesForASection(node, "basics", string.Empty));
                }
                else if (node.Attributes["Name"].Value.ToLower() == "additional")
                {
                    // for Additional section, we have subsection names below the "Additional" node
                    foreach (XmlNode subSectionNode in node.ChildNodes)
                    {
                        if (subSectionNode.Name.ToLower() != "section")
                        {
                            throw new Exception("Invalid node name: " + subSectionNode.Name + ".  Expected 'Section'.");
                        }

                        searchPrefs.AddRange(GetChildNodesForASection(subSectionNode, "additional", subSectionNode.Attributes["Name"].Value.ToLower()));
                    }
                }
            }

            return searchPrefs;
        }

        public static List<SearchPreference> GetChildNodesForASection(XmlNode topNode, string sectionName, string subSectionName)
        {
            List<SearchPreference> searchPrefs = new List<SearchPreference>();

            foreach (XmlNode node in topNode)
            {
                if (node.Name.ToLower() != "preference")
                {
                    throw new Exception("Invalid node name: " + node.Name + ".  Expected 'preference'.");
                }

                searchPrefs.Add(new SearchPreference { Name = node.Attributes["Name"].Value.ToLower(),
                                                       ResourceConstant = node.Attributes["ResourceConstant"].Value.ToLower(),
                                                       Collapsible = Convert.ToBoolean(node.Attributes["Collapsible"].Value),
                                                       SectionName = sectionName,
                                                       SubSectionName = subSectionName
                });
            }

            return searchPrefs;
        }

        public static string GetAttributeOptionResourceValue(Hashtable resourceBag, Member member, BrandConfig.Brand brand, string attributeName)
        {
            string ret = string.Empty;
            string resourceKey = string.Empty;

            int attributeValue = member.GetAttributeInt(brand, attributeName);
            if (attributeValue < 0)
                return ret;

            AttributeOptionCollection attributeOptions = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, brand.Site.Community.CommunityID,
                brand.Site.SiteID, brand.BrandID);

            foreach (AttributeOption option in attributeOptions)
            {
                if (option.Value == attributeValue)
                {
                    resourceKey = option.ResourceKey;
                    break;
                }
            }

            if (resourceKey != string.Empty)
                ret = GetResource(resourceBag, resourceKey);

            return ret;
        }

        public static string GetAttributeOptionResourceValue(Hashtable resourceBag, BrandConfig.Brand brand, string attributeName, int attributeValue)
        {
            string ret = string.Empty;
            string resourceKey = string.Empty;

            if (attributeValue < 0)
                return ret;

            AttributeOptionCollection attributeOptions = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, brand.Site.Community.CommunityID,
                brand.Site.SiteID, brand.BrandID);

            foreach (AttributeOption option in attributeOptions)
            {
                if (option.Value == attributeValue)
                {
                    resourceKey = option.ResourceKey;
                    break;
                }
            }

            if (resourceKey != string.Empty)
                ret = GetResource(resourceBag, resourceKey);

            return ret;
        }

        public static string GetAttributeOptionDescription(Member member, BrandConfig.Brand brand, string attributeName)
        {
            string ret = string.Empty;

            int relStatusValue = member.GetAttributeInt(brand, attributeName);
            if (relStatusValue >= 0)
            {
                AttributeOptionCollection attributeOptions = AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, brand.Site.Community.CommunityID,
                    brand.Site.SiteID, brand.BrandID);

                ret = GetAttributeOptionDescription(relStatusValue, attributeOptions);

            }

            return ret;
        }

        public static string GetAttributeOptionDescription(int attributeValue, AttributeOptionCollection attributeOptions)
        {
            string ret = string.Empty;

            if (attributeValue < 0 || attributeOptions == null)
                return ret;

            foreach (AttributeOption option in attributeOptions)
            {
                if (option.Value == attributeValue)
                {
                    ret = option.Description;
                    break;
                }
            }

            return ret;
        }

        public static List<SelectListItem> GetSitesListItems(int? selectedID)
        {
            return GetSitesListItems(true, selectedID);
        }

        public static List<SelectListItem> GetSitesListItems(bool includeAllSites, int? selectedID)
        {
            List<SelectListItem> listItems = new List<SelectListItem>();

            if (includeAllSites)
            {
                listItems.Add(new SelectListItem() {Text = "All Sites", Value = "0"});
            }

            foreach (int val in Enum.GetValues(typeof(SiteIDs)))
            {
                if (val > 0)
                {

                    SelectListItem newItem = new SelectListItem() { Text = Enum.GetName(typeof(SiteIDs), val), Value = val.ToString() };
                    if (selectedID.HasValue && val == selectedID.Value)
                    {
                        newItem.Selected = true;
                    }
                    listItems.Add(newItem);
                }
            }

            return listItems;
        }

        public static List<SelectListItem> GetIndividualTextStatuses()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            List<IndividualTextApprovalStatus> statuses = DBApproveQueueSA.Instance.GetIndividualTextApprovalStatuses();

            foreach (IndividualTextApprovalStatus status in statuses)
            {
                if (status.Display)
                {
                    listItems.Add(new SelectListItem() { Text = status.Description, Value = status.StatusMask.ToString() });
                }
            }

            return listItems;
        }

        public static List<SelectListItem> GetIndividualTextStatuses(int selectedReasons)
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            List<IndividualTextApprovalStatus> statuses = DBApproveQueueSA.Instance.GetIndividualTextApprovalStatuses();

            foreach (IndividualTextApprovalStatus status in statuses)
            {
                if (!status.Display)
                    continue;

                var itemToAdd = new SelectListItem()
                                    {Text = status.Description, Value = status.StatusMask.ToString()};

                itemToAdd.Selected = (selectedReasons & status.StatusMask) == status.StatusMask;
                listItems.Add(itemToAdd);
            }

            return listItems;
        }

        public static string GetHeightDisplay(int heightInCM)
        {
            string heightDisplay = string.Empty;

            if (heightInCM != Constants.NULL_INT)
            {
                int feet = 1;
                int inches = 1;

                CentimetersToInches(heightInCM, out feet, out inches);
                heightDisplay = feet.ToString() + "' " + inches.ToString() + "\" (" + heightInCM.ToString() + "cm)";
            }

            return heightDisplay;
        }

        private static void CentimetersToInches(int cm, out int feet, out int inches)
        {
            inches = Matchnet.Conversion.CInt(cm * 0.3937);
            feet = (inches / 12);
            inches = inches % 12;
        }

        public static double RoundWeight(int grams)
        {
            double kg;
            double offset;

            offset = (grams % 1000);
            kg = ((grams - offset) / 1000);
            if (offset < 250)
            {

            }
            else if (offset < 750)
            {
                kg += 0.5;
            }
            else
            {
                kg += 1;
            }
            return kg;
        }

        public static List<SelectListItem> GetAttributeOptionListItems(BrandConfig.Brand brand, string attributeName, int selectedValue, bool isMask, Hashtable resourceBag)
        {
            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = AttributeMetadataSA.Instance.GetAttributes().GetAttribute(attributeName);
            if (attribute == null)
                return new List<SelectListItem>();

            return GetAttributeOptionListItems(brand, attribute.ID, selectedValue, isMask, resourceBag, false);
        }

        public static List<SelectListItem> GetAttributeOptionListItems(BrandConfig.Brand brand, int attributeID, int selectedValue, bool isMask, Hashtable resourceBag, bool includeZeroValueItem)
        {
            return GetAttributeOptionListItems(brand, attributeID, selectedValue, isMask, resourceBag, includeZeroValueItem, false);
        }

        public static List<SelectListItem> GetAttributeOptionListItems(BrandConfig.Brand brand, int attributeID, int selectedValue, bool isMask, Hashtable resourceBag, bool includeZeroValueItem, bool isEditProfile)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            // first get the attribute
            Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = AttributeMetadataSA.Instance.GetAttributes().GetAttribute(attributeID);
            if (attribute == null)
                return items;

            // reroute any specific logic ones
            switch (attribute.Name.ToLower())
            {
                case "weight":
                    return GetWeightListItems(brand, selectedValue, false, resourceBag);
                case "height":
                    return GetHeightListItems(brand, selectedValue);
                case "birthcountryregionid":
                    return GetCountryRegionIDListItems(brand, selectedValue);
                case "morechildrenflag":
                    if (isEditProfile)
                        return GetMoreChildrenFlagListItems(brand, selectedValue);
                    break;
            }

            // now get the attributeoptions
            AttributeOptionCollection attributeOptions = AttributeOptionSA.Instance.GetAttributeOptionCollection(attribute.Name, brand.Site.Community.CommunityID,
                    brand.Site.SiteID, brand.BrandID);
            if (attributeOptions == null)
                return items;

            // copy the attributeoptions so that we can sort
            List<AttributeOption> aOptions = new List<AttributeOption>(attributeOptions.Cast<AttributeOption>());
          
            // sort
            aOptions.Sort((x, y) => x.ListOrder.CompareTo(y.ListOrder));

            // now load our listitems
            //Hashtable resourceBag = GetResourceCollection(DetermineResourceFileName(brand));
            foreach (AttributeOption ao in aOptions)
            {
                if (!includeZeroValueItem)
                {
                    if(ao.Value > 0)
                        items.Add(new SelectListItem { Value = ao.Value.ToString(), Text = GetResource(resourceBag, ao.ResourceKey) });
                }
                else
                {
                    items.Add(new SelectListItem { Value = ao.Value.ToString(), Text = GetResource(resourceBag, ao.ResourceKey) });
                }
            }

            // set the selected
            if (selectedValue > -1)
            {
                if (isMask)
                {
                    List<SelectListItem> search = items.FindAll(x => (Convert.ToInt32(x.Value) & selectedValue) == Convert.ToInt32(x.Value));
                    foreach (SelectListItem i in search)
                    {
                        i.Selected = true;
                    }
                }
                else
                {
                    SelectListItem search = items.Find(x => x.Value == selectedValue.ToString());
                    if (search != null)
                        search.Selected = true;
                }
            }

            return items;
        }

        //gets the distance attribute (depends on site and setting)
        public static string GetDistanceAttribute(BrandConfig.Brand brand)
        {
            //distance
            string useE2Distance = "false";
            try
            {
                useE2Distance = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USE_E2_DISTANCE", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                useE2Distance = "false";
            }

            string useMingleDistance = "false";
            try
            {
                useMingleDistance = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USE_MINGLE_DISTANCE", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting missing
                useMingleDistance = "false";
            }

            string distanceAttribute = useE2Distance.ToLower().Trim() == "true" ? "E2Distance" : "Distance";
            if (useMingleDistance.ToLower().Trim() == "true")
            {
                distanceAttribute = "MingleDistance";
            }
            return distanceAttribute;
        }

        public static List<SelectListItem> GetMoreChildrenFlagListItems(BrandConfig.Brand brand, int selectedValue)
        {
            //these are hard coded values to match with edit profile on the web
            List<SelectListItem> listItems = new List<SelectListItem>();

            SelectListItem blankItem = new SelectListItem() { Text = "", Value = Constants.NULL_INT.ToString() };
            if (selectedValue < 0)
            {
                blankItem.Selected = true;
            }
            listItems.Add(blankItem);

            SelectListItem noItem = new SelectListItem() { Text = "No", Value = "0" };
            if (selectedValue == 0)
            {
                noItem.Selected = true;
            }
            listItems.Add(noItem);

            SelectListItem yesItem = new SelectListItem() { Text = "Yes", Value = "1" };
            if (selectedValue == 1)
            {
                yesItem.Selected = true;
            }
            listItems.Add(yesItem);

            SelectListItem notsureItem = new SelectListItem() { Text = "Not Sure", Value = "2" };
            if (selectedValue == 2)
            {
                notsureItem.Selected = true;
            }
            listItems.Add(notsureItem);

            return listItems;
        }

        public static List<SelectListItem> GetCountryRegionIDListItems(BrandConfig.Brand brand, int selectedValue)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            Matchnet.Content.ValueObjects.Region.RegionCollection col = Matchnet.Content.ServiceAdapters.RegionSA.Instance.RetrieveBirthCountries(brand.Site.LanguageID);
            if (col == null)
                return items;

            foreach (Matchnet.Content.ValueObjects.Region.Region region in col)
            {
                items.Add(new SelectListItem() { Text = region.Description,
                    Value = region.RegionID.ToString(),
                    Selected = region.RegionID == selectedValue ? true : false});
            }

            return items;
        }

        public static List<SelectListItem> GetWeightListItems(BrandConfig.Brand brand, int selectedValue, bool withAnyAttribute, Hashtable resourceBag)
        {
            double grams;
            int pounds;
            double kg;
            string display = "";
            string hebrewDisplay = "{0} " + GetResource(resourceBag, "KG");
            bool siteUsesPounds = false;
            List<SelectListItem> items = new List<SelectListItem>();
            SelectListItem listItem;

            // Empty item at top of list
            if (withAnyAttribute)
            {
                listItem = new SelectListItem() { Text = GetResource(resourceBag, "ANY_"), Value = Constants.NULL_INT.ToString() };
                items.Add(listItem);
            }
            else
            {
                items.Add(new SelectListItem() { Text = "", Value = Constants.NULL_INT.ToString() });
            }

            if (Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USE_US_WEIGHT", brand.Site.Community.CommunityID, brand.Site.SiteID) == "true")
            {
                siteUsesPounds = true;
            }

            if (siteUsesPounds || brand.Site.CultureInfo.LCID == Constants.LOCALE_US_ENGLISH)
            {
                for (grams = Constants.WEIGHT_MIN; grams <= Constants.WEIGHT_MAX; grams += 907.2)
                {
                    pounds = Convert.ToInt32(grams / 453.6);
                    kg = Convert.ToDouble(RoundWeight(Convert.ToInt32(grams)));
                    display = String.Format("{0} lbs ({1:F2} kg)", pounds.ToString(), kg.ToString());

                    listItem = new SelectListItem() { Text = display, Value = Convert.ToString(Convert.ToInt32(grams)) }; 
                    items.Add(listItem);
                }
            }
            else
            {
                // default the starting weight to +500kg to offset WEIGHT_MIN at .5 kg
                for (grams = Constants.WEIGHT_MIN + 500; grams <= Constants.WEIGHT_MAX; grams += 1000)
                {
                    kg = Convert.ToDouble(RoundWeight(Convert.ToInt32(grams)));
                    if (brand.Site.LanguageID == (int)Matchnet.Language.Hebrew)
                    {
                        display = String.Format(hebrewDisplay, kg.ToString());
                    }
                    else
                    {
                        display = String.Format("{0} kg", kg.ToString());
                    }
                    
                    listItem = new SelectListItem() { Text = display, Value = Convert.ToString(Convert.ToInt32(grams)) };
                    items.Add(listItem);
                }
            }

            // set the selected
            SelectListItem search = items.Find(x => x.Value == selectedValue.ToString());
            if (search != null)
                search.Selected = true;

            return items;
        }

        public static List<SelectListItem> GetHeightListItems(BrandConfig.Brand brand, int selectedValue)
        {
            double height;
            int heightUS;
            int inches;
            int feet;
            int cmPrev;
            string display = "";
            List<SelectListItem> items = new List<SelectListItem>();
            SelectListItem listItem;

            #region less than min height
            height = Constants.HEIGHT_MIN - 2.54;
            if (brand.Site.CultureInfo.LCID == Constants.LOCALE_US_ENGLISH)
            {
                heightUS = Convert.ToInt32(Constants.HEIGHT_MIN / 2.54);
                inches = heightUS % 12;
                feet = (heightUS - inches) / 12;
                display = String.Format("< {0}\'{1}\"({2:F0} cm)", feet, inches, Constants.HEIGHT_MIN);
            }
            else
            {
                display = String.Format("< {0:F0} cm", Constants.HEIGHT_MIN.ToString());
            }
            listItem = new SelectListItem();
            listItem.Text = display;
            listItem.Value = Convert.ToString(Convert.ToInt32(height));
            items.Add(listItem);

            #endregion

            for (height = Constants.HEIGHT_MIN; height <= Constants.HEIGHT_MAX; height += 2.54)
            {
                if (brand.Site.CultureInfo.LCID == Constants.LOCALE_US_ENGLISH)
                {
                    heightUS = Convert.ToInt32(height / 2.54);
                    inches = heightUS % 12;
                    feet = (heightUS - inches) / 12;
                    display = String.Format("{0}\'{1}\"({2:F0} cm)", feet, inches, height);
                }
                else
                {
                    display = String.Format("{0:F0} cm", height.ToString());
                }
                cmPrev = Convert.ToInt32(height - 2.54);
                listItem = new SelectListItem();
                listItem.Text = display;
                listItem.Value = Convert.ToString(Convert.ToInt32(height));
                items.Add(listItem);
            }

            #region more than max height
            height = Constants.HEIGHT_MAX + 2.54;
            if (brand.Site.CultureInfo.LCID == Constants.LOCALE_US_ENGLISH)
            {
                heightUS = Convert.ToInt32(Constants.HEIGHT_MAX / 2.54);
                inches = heightUS % 12;
                feet = (heightUS - inches) / 12;
                display = String.Format("> {0}\'{1}\"({2:F0} cm)", feet, inches, Constants.HEIGHT_MAX);
            }
            else
            {
                display = String.Format("> {0:F0} cm", Constants.HEIGHT_MAX.ToString());
            }
            listItem = new SelectListItem();
            listItem.Text = display;
            listItem.Value = Convert.ToString(Convert.ToInt32(height));
            items.Add(listItem);
            #endregion

            // set the selected
            SelectListItem search = items.Find(x => x.Value == selectedValue.ToString());
            if (search != null)
                search.Selected = true;

            return items;
        }
        
        public static List<SelectListItem> GetGenderListItems(Enums.Gender selectedGender)
        {
            List<SelectListItem> listItems = new List<SelectListItem>();

            foreach (int val in Enum.GetValues(typeof(Enums.Gender)))
            {
                SelectListItem newItem = new SelectListItem() { Text = Enum.GetName(typeof(Enums.Gender), val), Value = val.ToString()};
                if ((int)selectedGender == val)
                {
                    newItem.Selected = true;
                }

                listItems.Add(newItem);
            }

            return listItems;
        }

        public static List<SelectListItem> GetSeekingGenderListItems(Enums.SeekingGender selectedGender)
        {
            List<SelectListItem> listItems = new List<SelectListItem>();

            foreach (int val in Enum.GetValues(typeof(Enums.SeekingGender)))
            {
                SelectListItem newItem = new SelectListItem() { Text = Enum.GetName(typeof(Enums.SeekingGender), val), Value = val.ToString() };
                if ((int)selectedGender == val)
                {
                    newItem.Selected = true;
                }

                listItems.Add(newItem);
            }

            return listItems;
        }

        public static List<SelectListItem> GetAdminAdjustDirections()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();

            foreach (int val in Enum.GetValues(typeof(Enums.AdminAdjustDirection)))
            {
                SelectListItem newItem = new SelectListItem() { Text = Enum.GetName(typeof(Enums.AdminAdjustDirection), val), Value = val.ToString() };
                listItems.Add(newItem);
            }
            
            return listItems;
        }

        public static List<SelectListItem> GetAdminAdjustPeriods()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();

            foreach (int val in Enum.GetValues(typeof(Matchnet.DurationType)))
            {
                SelectListItem newItem = new SelectListItem() { Text = Enum.GetName(typeof(Matchnet.DurationType), val), Value = val.ToString() };
                listItems.Add(newItem);
            }

            return listItems;
        }

        public static List<SelectListItem> GetAdminSuspendReasons()
        {
            return GetAdminSuspendReasons(AdminActionReasonID.None, false, true);
        }

        public static List<SelectListItem> GetAdminSuspendReasons(AdminActionReasonID selectedReasonID, bool isSuspended, bool includeUnknown)
        {
            bool foundSelected = false;
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem() { Text = "", Value = "0" });

            AdminActionReasonCollection reasons = AdminSA.Instance.GetAdminActionReasonsByType(AdminActionReasonType.AdminSuspend);
            foreach (AdminActionReason reason in reasons)
            {
                SelectListItem newItem = new SelectListItem() { Text = reason.Description, Value = reason.AdminActionReasonID.ToString("d") };
                if (isSuspended && reason.AdminActionReasonID == selectedReasonID)
                {
                    newItem.Selected = true;
                    foundSelected = true;
                }
                listItems.Add(newItem);
            }

            if ((isSuspended && !foundSelected) || includeUnknown)
            {
                SelectListItem newItem = new SelectListItem() { Text = "Unknown", Value = AdminActionReasonID.Unknown.ToString("d") };
                newItem.Selected = (isSuspended && !foundSelected) || (selectedReasonID == AdminActionReasonID.Unknown);
                listItems.Add(newItem);
            }

            return listItems;
        }

        public static string GetGenderAndSeekingStringFromMask(int genderMask)
        {
            string genderString = string.Empty;

            if ((genderMask & WebConstants.GENDERID_MALE) == WebConstants.GENDERID_MALE)
                genderString = "Male";
            if ((genderMask & WebConstants.GENDERID_FEMALE) == WebConstants.GENDERID_FEMALE)
                genderString = "Female";

            if ((genderMask & (int)Enums.SeekingGender.Male) == (int)Enums.SeekingGender.Male)
                genderString = genderString + " seeking Male";
            if ((genderMask & (int)Enums.SeekingGender.Female) == (int)Enums.SeekingGender.Female)
                genderString = genderString + " seeking Female";

            return genderString;
        }

        public static Enums.Gender GetGenderFromMask(int genderMask)
        {
            if ((genderMask & (int)Enums.Gender.Male) == (int)Enums.Gender.Male)
                return Enums.Gender.Male;
            else
                return Enums.Gender.Female;

        }

        public static Enums.SeekingGender GetSeekingGenderFromMask(int genderMask)
        {
            if ((genderMask & (int)Enums.SeekingGender.Male) == (int)Enums.SeekingGender.Male)
                return Enums.SeekingGender.Male;
            else
                return Enums.SeekingGender.Female;
        }

        public static bool IsSuspendReasonAllowed(AdminActionReasonID reasonID)
        {
            if (reasonID == AdminActionReasonID.Unknown) return false;

            bool allowed = true;
            
            AdminActionReasonCollection reasons = AdminSA.Instance.GetAdminActionReasonsByType(AdminActionReasonType.AdminSuspend);
            foreach (AdminActionReason reason in reasons)
            {
                if (reason.AdminActionReasonID == reasonID && reason.Display == false) allowed = false;
            }

            return allowed;
        }

        public static string GetConnectLink(ContextGlobal g, BrandConfig.Brand brand, string page, bool addEncryptedSessionIDFlag)
        {
            string devSubdomain = String.Empty;
            if (g.Environment == String.Empty || g.Environment == WebConstants.ENV_PROD)
                devSubdomain = "";
            else
                devSubdomain = g.Environment + ".";

            string link = String.Format("http://connect.{0}{1}/{2}", devSubdomain, brand.Uri, page);
            if (addEncryptedSessionIDFlag)
                return AppendEncryptedSessionID(link);
            else
                return link;
        }

        private static string AppendEncryptedSessionID(string url)
        {
            if (url.IndexOf(WebConstants.URL_PARAMETER_NAME_SPARKWS_CYPHER_OUT) > -1)
            {
                return url;
            }

            string encryptedSessionID = Crypto.Encrypt(WebConstants.SPARK_WS_CRYPT_KEY, "1234");

            url += ((url.IndexOf("?") > 1) ? "&" : "?") + WebConstants.URL_PARAMETER_NAME_SPARKWS_CYPHER_OUT + "=" + HttpUtility.UrlEncode(encryptedSessionID);
            

            return url;
        }

        public static string GetPlanTypeString(Purchase.Plan plan)
        {
            string sPlanType = "None";
            if ((plan.PlanTypeMask & Purchase.PlanType.PremiumPlan) > 0)
            {
                StringBuilder sbPlanType = new StringBuilder();
                foreach (int p in System.Enum.GetValues(typeof(Purchase.PremiumType)))
                {
                    if ((plan.PremiumTypeMask & ((Purchase.PremiumType)p)) > 0)
                    {
                        sbPlanType.Append((System.Enum.GetName(typeof(Purchase.PremiumType), p) + ", "));
                    }
                }

                if (sbPlanType.ToString() == "")
                    sPlanType = "Unspecified Premium Type)";
                else
                    sPlanType = sbPlanType.ToString().Substring(0, sbPlanType.ToString().Length - 2);
            }
            return sPlanType;
        }

        public static string GetPlanTypeString(int planTypeValue)
        {
            string planType = "None";

            try
            {
                planType = Enum.GetName(typeof (Purchase.PremiumType), planTypeValue);
                if(planType.ToLower() == "none")
                {
                    planType = "Standard";
                }
            }
            catch
            {
                // do nothing
            }

            return planType;
        }

        public static string GetMemberActivePremiumServicesString(int memberID, BrandConfig.Brand brand)
        {
            StringBuilder sbPlanString = new StringBuilder();
            ArrayList arrActivePremiumServices = PremiumServiceSA.Instance.GetMemberActivePremiumServices(memberID, brand);

            foreach (string service in arrActivePremiumServices)
            {
                if (sbPlanString.Length > 0) sbPlanString.Append(",");
                sbPlanString.Append(service);
            }

            if (sbPlanString.Length == 0)
            {
                return "None";
            }
            else
            {
                return sbPlanString.ToString();
            }
        }

        public static string GetPlanTypeString(Spark.Common.RenewalService.RenewalSubscriptionDetail[] renewalSubDetails, BrandConfig.Brand brand)
        {
            StringBuilder sbPlanString = new StringBuilder();
            Purchase.Plan msePlan = null;
            foreach (Spark.Common.RenewalService.RenewalSubscriptionDetail mse in renewalSubDetails)
            {
                if (!mse.IsPrimaryPackage)
                {
                    if (sbPlanString.Length > 0) sbPlanString.Append(",");
                    msePlan = PlanSA.Instance.GetPlan(mse.PackageID, brand.BrandID);
                    if (msePlan != null)
                    {
                        if ((msePlan.PremiumTypeMask & Purchase.PremiumType.HighlightedProfile) == Purchase.PremiumType.HighlightedProfile)
                        {
                            sbPlanString.Append(Purchase.PremiumType.HighlightedProfile.ToString());
                        }
                        else if ((msePlan.PremiumTypeMask & Purchase.PremiumType.SpotlightMember) == Purchase.PremiumType.SpotlightMember)
                        {
                            sbPlanString.Append(Purchase.PremiumType.SpotlightMember.ToString());
                        }
                        else if ((msePlan.PremiumTypeMask & Purchase.PremiumType.JMeter) == Purchase.PremiumType.JMeter)
                        {
                            sbPlanString.Append(Purchase.PremiumType.JMeter.ToString());
                        }
                        else if ((msePlan.PremiumTypeMask & Purchase.PremiumType.ColorCode) == Purchase.PremiumType.ColorCode)
                        {
                            sbPlanString.Append(Purchase.PremiumType.ColorCode.ToString());
                        }
                        else if ((msePlan.PremiumTypeMask & Purchase.PremiumType.ReadReceipt) == Purchase.PremiumType.ReadReceipt)
                        {
                            sbPlanString.Append(Purchase.PremiumType.ReadReceipt.ToString());
                        }
                    }
                }
            }

            if (sbPlanString.Length == 0)
            {
                return "None";
            }
            else
            {
                return sbPlanString.ToString();
            }
        }

        public static int GetPlanFromMemberSubExtended(Purchase.MemberSubExtended[] memberSubExtendeds, Purchase.PremiumType planType, BrandConfig.Brand brand, out Matchnet.Purchase.ValueObjects.MemberSubExtended mseReturn)
        {
            int planID = Constants.NULL_INT;
            mseReturn = null;
            if (memberSubExtendeds == null)
            {
                return planID;
            }

            foreach (Purchase.MemberSubExtended mse in memberSubExtendeds)
            {
                Purchase.Plan msePlan = PlanSA.Instance.GetPlan(mse.PlanID, brand.BrandID);
                if (msePlan != null && ((msePlan.PremiumTypeMask & planType) == planType))
                {
                    planID = msePlan.PlanID;
                    mseReturn = mse;
                    break;
                }
            }

            return planID;
        }

        public static int GetPlanFromRenewalSubscriptionDetail(Spark.Common.RenewalService.RenewalSubscriptionDetail[] renewalSubscriptionDetails, Purchase.PremiumType planType, BrandConfig.Brand brand, out Spark.Common.RenewalService.RenewalSubscriptionDetail rseReturn)
        {
            int planID = Constants.NULL_INT;
            rseReturn = null;
            if (renewalSubscriptionDetails == null || renewalSubscriptionDetails.Length <= 1)
            {
                return planID;
            }

            foreach (Spark.Common.RenewalService.RenewalSubscriptionDetail rse in renewalSubscriptionDetails)
            {
                if (!rse.IsPrimaryPackage)
                {
                    Purchase.Plan msePlan = PlanSA.Instance.GetPlan(rse.PackageID, brand.BrandID);
                    if (msePlan != null && ((msePlan.PremiumTypeMask & planType) == planType))
                    {
                        planID = msePlan.PlanID;
                        rseReturn = rse;
                        break;
                    }
                }
            }

            return planID;
        }

        public static int ConvertPageToStartRow(int pageNumber, int resultsPerPage)
        {
            int startRow = 1;
            if (pageNumber > 1)
            {
                startRow = (pageNumber - 1) * resultsPerPage;
                startRow += 1; //startRow starts with index of 1 (used in db proc)
            }

            return startRow;
        }

        public static int ConvertTotalRowToTotalPage(int totalRows, int resultsPerPage)
        {
            int totalPages = 0;
            if (totalRows > 0 && totalRows <= resultsPerPage)
            {
                totalPages = 1;
            }
            else
            {
                totalPages = totalRows / resultsPerPage; //this always round down to nearest int
                if ((totalRows % resultsPerPage) > 0)
                {
                    totalPages += 1;
                }
            }

            return totalPages;
        }

        public static bool IsMatchMeterEnabled(BrandConfig.Brand brand)
        {
            int mmsett = Int32.Parse(RuntimeSettings.GetSetting("MatchTest_App_Settings", brand.Site.Community.CommunityID, brand.Site.SiteID));
            int mmenumval = (Int32)AdminTool.Models.Enums.MatchMeterFlags.EnableApp;
            if ((mmsett & mmenumval) == mmenumval)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// This determines whether Unified Access is enabled for the site, used to determine whether we 
        /// read privileges from attributes or from Access svc.
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="siteID"></param>
        /// <param name="communityID"></param>
        /// <returns></returns>
        public static bool IsUPSAccessEnabled(int brandID, int siteID, int communityID)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_UPS_ACCESS_ENABLED", communityID, siteID, brandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        /// <summary>
        /// This is a kill switch so Bedrock systems will stop reading Access data
        /// </summary>
        /// <returns></returns>
        public static bool IsUPSAccessMasterReadKillSwitchEnabled()
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_UPS_ACCESS_MASTER_READ_KILLSWITCH_ENABLED");
            }
            catch (Exception ex)
            {

                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        /// <summary>
        /// This setting determins whether Bedrock systems will update Access Service with changes to privileges (e.g. Admin Adjust)
        /// </summary>
        /// <param name="brandID"></param>
        /// <param name="siteID"></param>
        /// <param name="communityID"></param>
        /// <returns></returns>
        public static bool IsUPSAccessUpdateEnabled(int brandID, int siteID, int communityID)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_UPS_ACCESS_UPDATE_PRIVILEGE_ENABLED", communityID, siteID, brandID);
            }
            catch (Exception ex)
            {

                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        public static bool IsUPSRenewalEnabled(int brandID, int siteID, int communityID)
        {
            string isEnabled = "false";
            try
            {
                isEnabled = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("IS_UPS_RENEWAL_ENABLED", communityID, siteID, brandID);
            }
            catch (Exception ex)
            {
                //setting missing
                isEnabled = "false";
            }

            if (isEnabled.ToLower().Trim() == "true")
                return true;
            else
                return false;

        }

        public static List<SelectListItem> GetCommunityIDChoices(string firstChoiceText, string firstChoiceValue)
        {
            var communityIds = new List<SelectListItem>
                                   {
                                       new SelectListItem {Text = firstChoiceText, Value = firstChoiceValue},
                                       new SelectListItem {Text = "Jdate", Value = "3"},
                                       new SelectListItem {Text = "Spark", Value = "1"},
                                       new SelectListItem {Text = "BBW", Value="23"},
                                       new SelectListItem {Text = "BlackSingles", Value="24"},
                                       new SelectListItem {Text = "Cupid", Value="10"}
                                   };

            return communityIds;
        }

        public static CommunityIDs GetCommunityIDForSiteID(SiteIDs siteID)
        {
            BrandConfig.Sites sites = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetSites();

            var selectedSite = sites.Cast<BrandConfig.Site>().Where(site => site.SiteID == (int)siteID).First();
            return (CommunityIDs)selectedSite.Community.CommunityID;
        }

        public static SiteIDs GetPrimarySiteForCommunity(CommunityIDs communityID)
        {
            // wt...
            switch (communityID)
            {
                case CommunityIDs.JDate:
                    return SiteIDs.JDate;
                case CommunityIDs.Spark:
                    return SiteIDs.Spark;
                case CommunityIDs.Cupid:
                    return SiteIDs.Cupid;
                case CommunityIDs.ItalianSinglesConnection:
                    return SiteIDs.ItalianMingle;
                case CommunityIDs.InterRacialSingles:
                    return SiteIDs.InterRacialMingle;
                case CommunityIDs.BBWPersonalsPlus:
                    return SiteIDs.BBW;
                case CommunityIDs.BlackSingles:
                    return SiteIDs.BlackMingle;
                case CommunityIDs.ChristianMingle:
                    return SiteIDs.ChristianMingle;
                case CommunityIDs.CatholicMingle:
                    return SiteIDs.CatholicMingle;
                case CommunityIDs.DeafSinglesConnection:
                    return SiteIDs.DeafSinglesConnection;
                case CommunityIDs.MilitarySinglesConnection:
                    return SiteIDs.MilitarySinglesConnection;
                case CommunityIDs.LDSMingle:
                    return SiteIDs.LDSMingle;
                case CommunityIDs.LDSSingles:
                    return SiteIDs.LDSSingles;
                case CommunityIDs.SingleSeniorsMeet:
                    return SiteIDs.SingleSeniorsMeet;
                default:
                    return SiteIDs.JDate;
            }
        }

        public static BrandConfig.Site GetSiteByID(int siteID)
        {
            BrandConfig.Sites sites = BrandConfigSA.Instance.GetSites();
            return (from BrandConfig.Site s in sites where s.SiteID == siteID select s).FirstOrDefault();
        }

        public static BrandConfig.Brand GetBrandForSite(int siteID)
        {
            BrandConfig.Brands brands = BrandConfigSA.Instance.GetBrandsBySite(siteID);

            //default brand should have the smallest brandID
            BrandConfig.Brand defaultBrand = null;
            foreach (BrandConfig.Brand b in brands)
            {
                if (defaultBrand == null)
                {
                    defaultBrand = b;
                }
                else if (b.BrandID < defaultBrand.BrandID)
                {
                    defaultBrand = b;
                }
            }

            return defaultBrand;
        }

        public static DateTime ConvertUTCToPST(DateTime transactionDate)
        {
            TimeZoneInfo timeZoneInfoInPST = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
            DateTime transactionInsertDateInPST = TimeZoneInfo.ConvertTimeFromUtc(transactionDate, timeZoneInfoInPST);

            return transactionInsertDateInPST;
        }

        public static string GetMaskContent(string pAttributeName, int pMaskValue, BrandConfig.Brand brand)
        {
            AttributeOptionCollection attributeOptionCollection = AttributeOptionSA.Instance.GetAttributeOptionCollection(pAttributeName, brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);

            if (attributeOptionCollection == null)
                return string.Empty;

            StringBuilder sb = new StringBuilder();

            foreach (AttributeOption attributeOption in attributeOptionCollection)
            {
                if (attributeOption.MaskContains(pMaskValue) && attributeOption.Description != "_blank" && attributeOption.Description != "blank")
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(", ");
                    }
                    sb.Append(attributeOption.Description);
                }
            }

            return sb.ToString();
        }

        public static string GetRegionString(int regionID, bool showPostalCode, bool showAbbreviatedState, bool showAbbreviatedCountry, BrandConfig.Brand brand)
        {
            string regionString = string.Empty;
            int languageID = brand.Site.LanguageID;

            if (regionID > 0)
            {
                RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(regionID, languageID);

                regionString = regionLanguage.CityName;

                string stateString = string.Empty;

                if (isDefaultRegion(brand, regionLanguage))
                {
                    if (showAbbreviatedState && regionLanguage.CountryRegionID == WebConstants.REGIONID_USA)
                    {
                        stateString = regionLanguage.StateAbbreviation;
                    }
                    else
                    {
                        stateString = regionLanguage.StateDescription;
                    }
                }

                if (!String.IsNullOrEmpty(regionString) && !String.IsNullOrEmpty(stateString))
                {
                    regionString += ", ";
                }
                regionString += stateString;

                if (!isDefaultRegion(brand, regionLanguage))
                {
                    string countryString = string.Empty;
                    if (showAbbreviatedCountry)
                    {
                        countryString = regionLanguage.CountryAbbreviation;
                    }
                    else
                    {
                        countryString = regionLanguage.CountryName;
                    }
                    if (!String.IsNullOrEmpty(regionString) && !String.IsNullOrEmpty(countryString))
                    {
                        regionString += ", ";
                    }
                    regionString += countryString;
                }

                if (regionString != null && regionString.Length > WebConstants.MAX_REGION_STRING_LENGTH)
                {
                    regionString = regionString.Substring(0, WebConstants.MAX_REGION_STRING_LENGTH - 3) + "...";
                }
            }
            else
            {
                regionString = "N/A";
            }
            return regionString;
        }

        private static bool isDefaultRegion(BrandConfig.Brand brand, RegionLanguage regionLanguage)
        {
            if (brand != null && regionLanguage.CountryRegionID == brand.Site.DefaultRegionID)
            {
                return true;
            }
            return false;
        }

        public static double RadianToDegree(double angle)
        {
            return angle * RadiansToDegrees;
        }

        public static double[] ConvertRadiansToDegrees(double latRadians, double lngRadians)
        {
            double[] degrees = new double[2];
            degrees[0] = RadianToDegree(latRadians);
            degrees[1] = RadianToDegree(lngRadians);
            return degrees;
        }
        public static List<SelectListItem> GetActiveBrands()
        {
            var brandIds = new List<SelectListItem>();

            var sites = BrandConfigSA.Instance.GetSites();

            foreach (BrandConfig.Site s in sites)
            {
                bool active = CheckSiteSetting("SITE_ACTIVE_FLAG", s.Community.CommunityID, s.SiteID, true);
                if (!active)
                    continue;

                var brands = BrandConfigSA.Instance.GetBrandsBySite(s.SiteID);
                foreach (BrandConfig.Brand b in brands)
                {
                    if (b.Uri.ToLower() == s.Name.ToLower())
                    {
                        brandIds.Add(new SelectListItem() { Text = b.Site.Name, Value = b.BrandID.ToString() });
                        break;
                    }
                }
            }

            brandIds.Sort((i1, i2) => i1.Text.CompareTo(i2.Text));

            return brandIds;
        }

        public static bool CheckSiteSetting(string settingConstant, int communityId, int siteId, bool defaultValue)
        {
            try
            {
                return Convert.ToBoolean(RuntimeSettings.GetSetting(settingConstant, communityId, siteId));
            }
            catch
            {
                return defaultValue;
            }
        }

        public static List<SelectListItem> GetDurationTypes(int cutoffLevel)
        {
            return (from int val in Enum.GetValues(typeof(Matchnet.DurationType)) where val >= cutoffLevel select new SelectListItem() { Text = Enum.GetName(typeof(Matchnet.DurationType), val), Value = val.ToString() }).ToList();
        }

        public static List<SelectListItem> GetPremiumPlanTypes()
        {
            var items = new List<SelectListItem>
                            {
                                new SelectListItem() {Text = "Highlighted Profile", Value = "1"},
                                new SelectListItem() {Text = "Spotlight Member", Value = "2"},
                                new SelectListItem() {Text = "J-Meter", Value = "4"},
                                new SelectListItem() {Text = "All Access", Value = "32"},
                                new SelectListItem() {Text = "All Access Email", Value = "64"},
                                new SelectListItem() {Text = "Read Receipt", Value = "128"}
                            };

            return items;
        }

        public static List<SelectListItem> GetPlanTypes()
        {
            var items = new List<SelectListItem>
                            {
                                new SelectListItem() {Text = "Standard plan", Value = "1"},
                                new SelectListItem() {Text = "Premium service plan", Value = "256"},
                                new SelectListItem() {Text = "A La Carte plan", Value = "1024"}
                            };

            return items;
        }

        public static List<SelectListItem>  GetALaCartePlanTypes()
        {
            var items = new List<SelectListItem>
                            {
                                new SelectListItem() {Text="Hightlighted Profile", Value="1"},
                                new SelectListItem() {Text="Spotlight Member", Value="2"},
                                new SelectListItem() {Text="J-Meter", Value="4"},
                                new SelectListItem() {Text="Color Code", Value="8"},
                                new SelectListItem() {Text="Read Receipt", Value="128"}
                            };

            return items;
        }

        public static string GetCurrencyString(int brandId)
        {
            var brand = BrandConfigSA.Instance.GetBrandByID(brandId);
            string ret = string.Empty;

            switch(brand.Site.CurrencyID)
            {
                case 1:
                    ret = "USD";
                    break;
                case 2:
                    ret = "EU";
                    break;
                case 3:
                    ret = "CAD";
                    break;
                case 4:
                    ret = "GBP";
                    break;
                case 5:
                    ret = "AUD";
                    break;
                case 6:
                    ret = "ILS";
                    break;
            }

            return ret;
        }

        public static int GetCurrencyIdFromString(string currencyString)
        {
            int currencyID = Constants.NULL_INT;

            switch (currencyString)
            {
                case "USD":
                    currencyID = 1;
                    break;
                case "EU":
                    currencyID = 2;
                    break;
                case "CAD":
                    currencyID = 3;
                    break;
                case "GBP":
                    currencyID = 4;
                    break;
                case "ILS":
                    currencyID = 6;
                    break;
            }

            return currencyID;
        }

        public static Purchase.PlanServiceDefinition MapPremiumTypeToPlanServiceDefinition(Purchase.PremiumType pType)
        {
            Purchase.PlanServiceDefinition ret = Purchase.PlanServiceDefinition.Basic_Subscription;
            
            switch(pType)
            {
                case Purchase.PremiumType.None:
                    ret = Purchase.PlanServiceDefinition.Basic_Subscription;
                    break;
                case Purchase.PremiumType.HighlightedProfile:
                    ret = Purchase.PlanServiceDefinition.Profile_Highlighting;
                    break;
                case Purchase.PremiumType.SpotlightMember:
                    ret = Purchase.PlanServiceDefinition.SpotLight;
                    break;
                case Purchase.PremiumType.JMeter:
                    ret = Purchase.PlanServiceDefinition.JMeter;
                    break;
                case Purchase.PremiumType.ColorCode:
                    ret = Purchase.PlanServiceDefinition.ColorCode;
                    break;
                case Purchase.PremiumType.ServiceFee:
                    ret = Purchase.PlanServiceDefinition.ServiceFee;
                    break;
                case Purchase.PremiumType.AllAccess:
                    ret = Purchase.PlanServiceDefinition.AllAccess;
                    break;
                case Purchase.PremiumType.AllAccessEmail:
                    ret = Purchase.PlanServiceDefinition.AllAccessEmail;
                    break;
                case Purchase.PremiumType.ReadReceipt:
                    ret = Purchase.PlanServiceDefinition.ReadReceipt;
                    break;
                default:
                    throw new Exception("Unsupported PremiumType");
                    break;
            }

            return ret;
        }

        public static List<SelectListItem> GetPromoRowChoices()
        {
            return GetNumberedSelectListItems(1, 60, null);
        }

        public static List<SelectListItem> GetPromoColChoices()
        {
            return GetNumberedSelectListItems(1, 5, null);
        }

        public static List<SelectListItem> GetPaymentTypeChoices()
        {
            return new List<SelectListItem>()
                       {
                           new SelectListItem() {Text="Credit Card", Value="1"},
                           new SelectListItem() {Text="Check", Value="2"}
                       };
            
        }

        public static List<SelectListItem> GetPromoTypeChoices()
        {
            return new List<SelectListItem>()
                       {
                           new SelectListItem() {Text="Member", Value="0"},
                           new SelectListItem() {Text="Admin Only", Value="1"},
                           new SelectListItem() {Text="Email / Banner", Value="2"},
                           new SelectListItem() {Text="Email with Targeting", Value="3"},
                           new SelectListItem() {Text="Mobile", Value="4"},
                           new SelectListItem() {Text="Supervisor Only", Value="5"}
                       };
        }

        public static List<SelectListItem> GetTargetablePromoTypeChoices()
        {
            return new List<SelectListItem>()
                       {
                           new SelectListItem() {Text="Member", Value="0"},
                           new SelectListItem() {Text="Mobile", Value="4"}
                       };
        }

        public static List<SelectListItem> GetNumberedSelectListItems(int startNumber, int endNumber, List<int> excludeNumbers )
        {
            var items = new List<SelectListItem>();

            for(int i=startNumber; i <= endNumber; i++)
            {
                if(excludeNumbers== null || !excludeNumbers.Exists(x=>x ==i))
                    items.Add(new SelectListItem() {Text=i.ToString(), Value=i.ToString()});
            }

            return items;
        }

        public static List<SelectListItem> GetResourceTemplates(Matchnet.PromoEngine.ValueObjects.ResourceTemplate.ResourceTemplateType resourceTemplateType, int brandId)
        {
            ResourceTemplateCollection col = ResourceTemplateSA.Instance.GetResourceTemplateCollection();
            List<SelectListItem> items = (from ResourceTemplate resTemplate in col
                                          where resTemplate.ResourceTemplateType == resourceTemplateType &&
                                                resTemplate.GroupID == brandId
                                          select
                                              new SelectListItem()
                                                  {
                                                      Text = resTemplate.Description,
                                                      Value = resTemplate.ResourceTemplateID.ToString()
                                                  }).ToList();

            //sorting the items by name. (alphabetically)
            items.Sort((x, y) => string.Compare(x.Text, y.Text));

            return items;
        }

        public static List<SelectListItem> GetOptionsFromEnum(Type enumType)
        {
            return (from int value in System.Enum.GetValues(enumType) select new SelectListItem() {Value = value.ToString(), Text = Enum.GetName(enumType, value).Replace("_", " ")}).ToList();
        }

        public static List<SelectListItem> GetOptionsFromEnumExcludeZeroOrLess(Type enumType)
        {
            return (from int value in System.Enum.GetValues(enumType) where value > 0 select new SelectListItem() { Value = value.ToString(), Text = Enum.GetName(enumType, value).Replace("_", " ") }).ToList();
        }

        public static List<SelectListItem> GetHourlyTimeChoices()
        {
            var hours = new List<SelectListItem>();

            for (int i = 0; i < 24; i++)
            {
                if (i == 0)
                {
                    hours.Add(new SelectListItem() {Text = "12:00AM", Value = "0"});
                }
                else
                {
                    hours.Add(new SelectListItem() {Text=string.Format("{0}:00 {1}", i, i>=12 ? "PM" : "AM"), Value=i.ToString()});
                }
            }

            return hours;
        }

        public static List<SelectListItem> GetDayOfTheWeekChoices()
        {
            return new List<SelectListItem>()
                       {
                           new SelectListItem() { Text="Sunday", Value="SUN"},
                           new SelectListItem() { Text="Monday", Value="MON"},
                           new SelectListItem() { Text="Tuesday", Value="TUE"},
                           new SelectListItem() { Text="Wednesday", Value="WED"},
                           new SelectListItem() { Text="Thursday", Value="THU"},
                           new SelectListItem() { Text="Friday", Value="FRI"},
                           new SelectListItem() { Text="Saturday", Value="SAT"}
                       };
        }

        public static List<SelectListItem> GetUPSTemplates(int brandId)
        {
            var brand = BrandConfigSA.Instance.GetBrandByID(brandId);

            var paymentUITemplateAPIURL =
                (RuntimeSettings.GetSetting("UPS_PAYMENT_UI_TEMPLATE_API", brand.Site.Community.CommunityID,
                                            brand.Site.SiteID, brand.BrandID));

            paymentUITemplateAPIURL += "?Callingsystemid=" + brand.Site.SiteID;

            ServicePointManager.CertificatePolicy = new MyPolicy();

            var request =
                WebRequest.Create(paymentUITemplateAPIURL) as HttpWebRequest;

            using (var response = request.GetResponse() as HttpWebResponse)
            {
                var reader = new System.IO.StreamReader(response.GetResponseStream());

                var json = new Newtonsoft.Json.JsonSerializer();

                var upsTemplates = new List<UPSTemplate>();

                var deserialized = json.Deserialize(reader, upsTemplates.GetType()) as List<UPSTemplate>;

                return deserialized.Select(template => new SelectListItem()
                                                           {
                                                               Text = template.Description + "(" + template.TemplateID + ")", Value = template.TemplateID.ToString()
                                                           }).ToList();
            }

            return null;
        }

        public static string Encrypt(int promoID)
        {
            // TODO: We need to have some mechanism to not store this in code
            const string SUBSCRIPTION_ENCRYPT_KEY = "m0nk$ies";
            var encryptor = new Matchnet.Lib.Encryption.SymmetricalEncryption(Matchnet.Lib.Encryption.SymmetricalEncryption.Provider.DES);

            return System.Web.HttpUtility.UrlEncode(encryptor.Encrypt(promoID.ToString(), SUBSCRIPTION_ENCRYPT_KEY));
        }

        public static DateTime ConvertDateTimeString(string dateTimeString)
        {
            DateTime ret = DateTime.MinValue;
            if (dateTimeString == null)
                return ret;

            var splits = dateTimeString.Split(new char[] {'/'});
            if(splits.Length == 3)
            {
                ret = new DateTime(int.Parse(splits[2]), int.Parse(splits[0]), int.Parse(splits[1]));
            }

            return ret;
        }

        public static DateTime GetDatePortionOnly(DateTime datetime)
        {
            return new DateTime(datetime.Year, datetime.Month, datetime.Day);
        }
        public static int GetLanguageIdFromBrandId(int brandId)
        {
            Matchnet.Content.ValueObjects.BrandConfig.Brand brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandId);
            if(brand != null)
                return brand.Site.LanguageID;

            return -1;
        }

        public static string GetDurationStringForDurationType(int durationType)
        {
            string duration = string.Empty;  

            switch (durationType)
	        {   
                    case 0:
                    duration = "None";
                        break;
                    case 1:
                        duration = "Minute";
                        break;
                    case 2:
                        duration = "Hour";
                        break;
                    case 3:
                        duration = "Day";
                        break;
                    case 4:
                        duration = "Week";
                        break;
                    case 5:
                        duration = "Month";
                        break;
                    case 6:
                        duration = "Year";
                        break;
		       
	        } 
            
            return duration;
        }

        public static string ConvertIntArrayToString(int[] intArray, char? delimiter)
        {
            string myDelim = (null != delimiter) ? delimiter.ToString() : string.Empty;
            StringBuilder sb = intArray.Aggregate(new StringBuilder(),
                                                            (s, i) =>
                                                            s.Append(WebConstants.ONE_WHITESPACE_CHAR).Append(i).Append(myDelim));
            return (sb.Length > 0 && (null != delimiter && sb[sb.Length - 1] == delimiter)) ? sb.ToString(0, sb.Length - 1) : sb.ToString();
        }

        public static int GetTextboxHeight(string text)
        {
            int height = 0;
            int lineBreaks = text.Split(new string[] { "\r\n" }, StringSplitOptions.None).Length - 1;

            if (text.Length <= 100)
            {
                height = 55 + (lineBreaks * 40);
            }

            if (text.Length >= 100)
            {
                int rows = (int)(text.Length / 100);
                height = (20 * rows) + 60 + (lineBreaks * 25);
            }

            return height;
        }

        public static string AddSpaceBeforeEachCapitalLetter(string text)
        {
            if (string.IsNullOrEmpty(text))
                return string.Empty;
            var newText = new StringBuilder(text.Length*2);
            newText.Append(text[0]);
            for (int i = 1; i < text.Length; i++)
            {
                if (char.IsUpper(text[i]) && text[i - 1] != ' ')
                    newText.Append(' ');
                newText.Append(text[i]);
            }
            return newText.ToString();
        }
    }
}
