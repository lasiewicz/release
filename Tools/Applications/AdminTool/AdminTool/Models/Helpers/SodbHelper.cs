﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using AdminTool.Models.ModelViews.Review;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Spark.SODBQueue.ValueObjects;
using Spark.CommonLibrary.Logging;

namespace AdminTool.Models
{
    public class SodbHelper
    {
        private const string MEMBASE_WEB_BUCKET_NAME = "sodb";
        private const string SodbMemberPhotoUploadCacheKey = "sodb_member_photo_upload_watchlist";
        private Logger logger = new Logger(typeof(SodbHelper));

        public void UpdateSodbMemberPhotoUploadStatus(int memberId, int siteId)
        {
            try
            {
            
                //Check if member is in Sodb photo upload watchlist
                MembaseConfig membaseConfig = RuntimeSettings.GetMembaseConfigByBucket(MEMBASE_WEB_BUCKET_NAME);
                if (membaseConfig != null)
                {
                    ICaching cache = Spark.Caching.MembaseCaching.GetSingleton(membaseConfig);
                    var watchList = cache.Get(SodbMemberPhotoUploadCacheKey) as List<SodbMembaseItem>;
                    if (watchList == null)
                    {
                        //Get Sodb watchlist from DB
                        List<SodbMembaseItem> membersWithNoPhotos = GetSodbMembersWithNoPhotoFromDB();
                        //Update Membase Cache
                        cache.Insert(SodbMemberPhotoUploadCacheKey, membersWithNoPhotos);
                        logger.Debug(
                            "Sodb watchlist does not exist on membase. Get sodb watch list from DB and update membase cache");
                        watchList = membersWithNoPhotos;
                    }
                    
                    //Check if member is in the watchlist
                    SodbMembaseItem item =
                        watchList.Where(a => a.MemberId == memberId && a.SiteId == siteId).FirstOrDefault();
                    if (item != null)
                    {
                        //Update review status of the item, so that the item can appear in the Sodb Review Queue
                        UpdateSodbQueueItemStatus(item.MemberId, item.SiteId, item.SSPBlueId);
                        //Remove Item from Membase watchlist
                        watchList.Remove(item);
                        cache.Insert(SodbMemberPhotoUploadCacheKey, watchList);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Unable to update sodb member photo upload status", ex);
            }
            
        }

        public void UpdateMembaseSodbWatchList(int memberId, int siteId, int sspBlueId)
        {
            MembaseConfig membaseConfig = RuntimeSettings.GetMembaseConfigByBucket(MEMBASE_WEB_BUCKET_NAME);
            if (membaseConfig != null)
            {
                ICaching cache = Spark.Caching.MembaseCaching.GetSingleton(membaseConfig);
                var watchList = cache.Get(SodbMemberPhotoUploadCacheKey) as List<SodbMembaseItem>;
                if (watchList == null)
                {
                    //Get Sodb watchlist from DB
                    watchList = GetSodbMembersWithNoPhotoFromDB();
                    //Update Membase Cache
                    cache.Insert(SodbMemberPhotoUploadCacheKey, watchList);
                    logger.Debug(
                        "Sodb watchlist does not exist on membase. Get sodb watch list from DB and update membase cache");
                }
                else
                {
                    SodbMembaseItem item =
                        watchList.Where(a => a.MemberId == memberId && a.SiteId == siteId && a.SSPBlueId == sspBlueId).FirstOrDefault();
                    if (item == null)
                    {
                        watchList.Add(new SodbMembaseItem {MemberId = memberId, SiteId = siteId, SSPBlueId = sspBlueId});
                        cache.Insert(SodbMemberPhotoUploadCacheKey, watchList);
                    }
                }
            }
        }

        private List<SodbMembaseItem> GetSodbMembersWithNoPhotoFromDB()
        {
            var items = new List<SodbItem>();
            var membersWithNoPhoto = new List<SodbMembaseItem>();
            try
            {
                //Get data from Internal API
                string sodbApiUrl = ConfigurationManager.AppSettings["SodbMembersWithNoPhoto"];
                if (!string.IsNullOrEmpty(sodbApiUrl))
                {
                    var request = (HttpWebRequest)WebRequest.Create(sodbApiUrl);
                    using (var response = request.GetResponse() as HttpWebResponse)
                    {
                        var reader = new StreamReader(response.GetResponseStream());

                        string content = reader.ReadToEnd();
                        items =
                            Newtonsoft.Json.JsonConvert.DeserializeObject(content, typeof(List<SodbItem>)) as
                            List<SodbItem>;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            if (items != null && items.Count > 0)
            {
                membersWithNoPhoto.AddRange(items.Select(sodbItem => new SodbMembaseItem {MemberId = sodbItem.MemberId, SiteId = sodbItem.SiteId, SSPBlueId = sodbItem.SSPBlueId}));
            }
                
            return membersWithNoPhoto;
        }

        private void UpdateSodbQueueItemStatus(int memberId, int siteId, int sspBlueId)
        {
            try
            {
                //Get data from Internal API
                string sodbApiUrl = ConfigurationManager.AppSettings["SodbUpdatePhotoUploadStatus"];
                sodbApiUrl = sodbApiUrl.Replace("{memberId}", memberId.ToString()).Replace("{siteId}", siteId.ToString()).Replace("{sspblueId}", sspBlueId.ToString());
                if (!string.IsNullOrEmpty(sodbApiUrl))
                {
                    var request = (HttpWebRequest)WebRequest.Create(sodbApiUrl);
                    using (var response = request.GetResponse() as HttpWebResponse)
                    {
                        var reader = new StreamReader(response.GetResponseStream());

                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public  void SendSodbPhotoUploadEmail(SODBReviewModelView model, int brandId)
        {
            string memberEmail = model.Email;
            try
            {
                if (model.IsBHMember) //For non-BH member, we get email address from DB.
                {
                    Member member = MemberHelper.GetMember(model.MemberId, MemberLoadFlags.None);
                    memberEmail = member.EmailAddress;
                }

                //Overwrite emailaddress on dev and stage env
                string env = ConfigurationManager.AppSettings["Environment"] ?? "dev";
                if (env.ToLower() != "prod")
                {
                    memberEmail = ConfigurationManager.AppSettings["SodbSupervisorEmail"];
                }

                if (!string.IsNullOrEmpty(memberEmail))
                {
                    ExternalMailSA.Instance.SendSodbPhotoUploadEmail(model.MemberId, brandId, memberEmail,
                                                                     model.IsBHMember);
                    logger.Debug(
                        string.Format(
                            "Sodb photo upload email sent to Member. MemberId : {0} SiteId: {1} BrandId: {2}",
                            model.MemberId, model.SiteId, brandId));
                }
                else
                {
                    logger.Error(string.Format("Unable to send sodb photo upload email. Invalid email. MemberId: {0} SiteId: {1}", model.MemberId, model.SiteId));
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("Error sending Sodb photo upload email to member. MemberId: {0} SiteId: {1} Email: {2}", model.MemberId, model.SiteId, memberEmail), ex);
            }
        }
    }
    
}