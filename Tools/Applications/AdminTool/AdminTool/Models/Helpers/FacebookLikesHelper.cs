﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.FacebookLike.ValueObjects;
using System.Text;
using Spark.FacebookLike.ServiceAdapters;

namespace AdminTool.Models
{
    public class FacebookLikesHelper
    {
        public static bool HasFacebookLikes(List<FacebookLike> facebookLikesList)
        {
            if (facebookLikesList != null && facebookLikesList.Count > 0)
                return true;
            else
                return false;
        }

        public static string GenerateSavedLikesJS(List<FacebookLike> facebookLikesList)
        {
            //Generate JS Facebook Saved Likes
            StringBuilder sbFacebookSavedLikesJS = new StringBuilder();
            if (facebookLikesList != null && facebookLikesList.Count > 0)
            {
                //var fbLikeObj = new FBLikeObj();
                //fbLikeObj.id = obj.id;
                //fbLikeObj.name = obj.name;
                //fbLikeObj.category = obj.category;
                //fbLikeObj.link = obj.link;
                //fbLikeObj.picture = obj.picture ? obj.picture :  'http://profile.ak.fbcdn.net/static-ak/rsrc.php/v1/yr/r/fwJFrO5KjAQ.png';
                //fbLikeObj.type = obj.type;
                //fbLikeObj.chkEditID = 'chk' + obj.id;
                //fbLikeObj.categoryGroupID
                //fbLikeObj.categoryID
                //fbLikeObj.selected
                //fbLikeObj.saved
                //fbLikeObj.approved
                //fbLikeManager.addFBLike(fbLikeObj);

                foreach (FacebookLike fbLike in facebookLikesList)
                {
                    string varFBLikeObj = "fbLikeObj" + fbLike.FacebookId.ToString();
                    sbFacebookSavedLikesJS.AppendLine("var " + varFBLikeObj + " = new FBLikeObj();");
                    sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".id = " + fbLike.FacebookId.ToString() + ";");
                    sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".name = \"" + fbLike.FacebookName + "\";");
                    //sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".category = \"" + fbLike.FacebookName + "\";");
                    sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".link = \"" + fbLike.FacebookLinkHref + "\";");
                    sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".picture = \"" + fbLike.FacebookImageUrl + "\";");
                    sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".type = \"" + fbLike.FacebookType + "\";");
                    sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".chkEditID = \"chk" + fbLike.FacebookId.ToString() + "\";");
                    sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".categoryID = " + fbLike.FacebookLikeCategoryId.ToString() + ";");
                    sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".categoryGroupID = " + fbLike.FacebookLikeCategoryGroupId.ToString() + ";");
                    sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".selected = true;");
                    sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".saved = true;");
                    if (fbLike.FacebookLikeStatus == Spark.FacebookLike.ValueObjects.ServiceDefinitions.FacebookLikeStatus.Approved)
                    {
                        sbFacebookSavedLikesJS.AppendLine(varFBLikeObj + ".approved = true;");
                    }
                    sbFacebookSavedLikesJS.AppendLine("fbLikeManager.addFBLike(" + varFBLikeObj + ");");

                }
            }

            return sbFacebookSavedLikesJS.ToString();
        }

        public static string GenerateCategoryJS()
        {
            //Generate JS Facebook Likes Category Groups
            StringBuilder sbFacebookLikesCategoryGroupsJS = new StringBuilder();
            FacebookLikeCategoryGroupList fbGroupList = FacebookLikeServiceSA.Instance.GetFacebookLikeCategoryGroupList();
            if (fbGroupList != null && fbGroupList.FacebookLikeCategoryGroups != null)
            {
                foreach (FacebookLikeCategoryGroup fbGroup in fbGroupList.FacebookLikeCategoryGroups)
                {
                    //category group
                    string varFbLikeCategoryGroupObj = "fbLikeCategoryGroupObj" + fbGroup.FacebookLikeCategoryGroupId.ToString();
                    sbFacebookLikesCategoryGroupsJS.AppendLine("var " + varFbLikeCategoryGroupObj + " = new FBLikeCategoryGroupObj();");
                    sbFacebookLikesCategoryGroupsJS.AppendLine(varFbLikeCategoryGroupObj + ".categoryGroupID = " + fbGroup.FacebookLikeCategoryGroupId.ToString() + ";");
                    sbFacebookLikesCategoryGroupsJS.AppendLine(varFbLikeCategoryGroupObj + ".categoryGroupName = \"" + fbGroup.FacebookCategoryGroupName + "\";");
                    //sbFacebookLikesCategoryGroupsJS.AppendLine(varFbLikeCategoryGroupObj + ".categoryGroupName = \"" + g.GetResource(fbGroup.FacebookCategoryGroupNameResourceKey, this) + "\";");
                    sbFacebookLikesCategoryGroupsJS.AppendLine(varFbLikeCategoryGroupObj + ".containerID = \"fbLikesGroup" + fbGroup.FacebookLikeCategoryGroupId.ToString() + "\";");
                    sbFacebookLikesCategoryGroupsJS.AppendLine(varFbLikeCategoryGroupObj + ".containerEditID = \"fbLikesGroupEdit" + fbGroup.FacebookLikeCategoryGroupId.ToString() + "\";");
                    sbFacebookLikesCategoryGroupsJS.AppendLine("spark_util_addItem(fbLikeManager.fbLikeCategoryGroupObjList, " + varFbLikeCategoryGroupObj + ");");

                    if (fbGroup.FacebookLikeCategories != null)
                    {
                        foreach (FacebookLikeCategory fbCategory in fbGroup.FacebookLikeCategories)
                        {
                            //categories within a group
                            string varFbLikeCategoryObj = "fbLikeCategoryObj" + fbCategory.FacebookLikeCategoryId.ToString();
                            sbFacebookLikesCategoryGroupsJS.AppendLine("var " + varFbLikeCategoryObj + " = new FBLikeCategoryObj();");
                            sbFacebookLikesCategoryGroupsJS.AppendLine(varFbLikeCategoryObj + ".categoryID = " + fbCategory.FacebookLikeCategoryId.ToString() + ";");
                            sbFacebookLikesCategoryGroupsJS.AppendLine(varFbLikeCategoryObj + ".categoryName = \"" + fbCategory.FacebookLikeCategoryName + "\";");
                            sbFacebookLikesCategoryGroupsJS.AppendLine("spark_util_addItem(" + varFbLikeCategoryGroupObj + ".fbLikeCategoryObjList, " + varFbLikeCategoryObj + ");");
                        }
                    }
                }
            }

            return sbFacebookLikesCategoryGroupsJS.ToString();
        }
    }
}