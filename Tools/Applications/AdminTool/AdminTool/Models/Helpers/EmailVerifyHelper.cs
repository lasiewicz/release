﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using AdminTool.Models.ModelViews.Email;
using Matchnet.Email.ValueObjects;

namespace AdminTool.Models
{
    public class EmailVerifyHelper
    {
        public const string SESSION_EMAIL_VERIFY_NOTIFICATION_DISPLAY = "DISPLAY_EMAIL_VERIFIED_MSG";
        public const string RESX_EMAIL_VERIFY_CONFIRMATION = "EMAIL_VERIFIED_MSG";
        public const string SESSION_EMAIL_VERIFY_JUST_REGISTERED = "EMAIL_VERIFIED_JUST_REGISTERED";
        public const string SESSION_EMAIL_VERIFY_JUST_VERIFIED = "EMAIL_VERIFIED_JUST_VERIFIED";


        public enum HashCodeMask : int
        {
            fromMemberID = 0,
            fromEmailAddress = 1
        }

        bool _enableEmailVerifyFlag;
        AdminTool.Models.Enums.ForcedBlockingMask _blockingMaskSetting = AdminTool.Models.Enums.ForcedBlockingMask.NotBlocked;

        int _communityid;
        int _siteid;
        int _brandid;
        Brand _brand;
        HashCodeMask _hashCodeMask = HashCodeMask.fromMemberID;

        public EmailVerifyHelper(Brand brand)
        {
            try
            {
                _communityid = brand.Site.Community.CommunityID;
                _siteid = brand.Site.SiteID;
                _brandid = brand.BrandID;
                _brand = brand;

                _enableEmailVerifyFlag = Conversion.CBool(RuntimeSettings.GetSetting("EMAIL_VERIFICATION_ENABLE_FLAG", _communityid, _siteid, _brandid), false);
                _blockingMaskSetting = (AdminTool.Models.Enums.ForcedBlockingMask)Enum.Parse(typeof(AdminTool.Models.Enums.ForcedBlockingMask), RuntimeSettings.GetSetting("EMAIL_VERIFICATION_BLOCK_MASK", _communityid, _siteid, _brandid));
                _hashCodeMask = (HashCodeMask)Enum.Parse(typeof(HashCodeMask), RuntimeSettings.GetSetting("EMAIL_VERIFICATION_HASH_MASK", _communityid, _siteid, _brandid));

            }
            catch (Exception ex)
            { }


        }

        public bool EnableEmailVerification
        {
            get
            { return _enableEmailVerifyFlag; }

        }

        public AdminTool.Models.Enums.ForcedBlockingMask EmailVerificationBlocking
        {
            get
            { return _blockingMaskSetting; }

        }

        public HashCodeMask HashCodeGenerationMask
        {
            get
            { return _hashCodeMask; }

        }

        public Brand Brand
        {
            get
            { return _brand; }
            set { _brand = value; }
        }

        public bool IsMemberBlocked(Member member)
        {
            AdminTool.Models.Enums.ForcedBlockingMask blockingmask;
            bool ret = false;
            try
            {
                if (member != null)
                {
                    blockingmask = GetBlockingMaskAttr(member);
                    ret = IsMemberBlocked(blockingmask);

                }

                return ret;

            }
            catch (Exception ex)
            { return ret; }

        }

        public bool IsMemberBlocked(AdminTool.Models.Enums.ForcedBlockingMask mask)
        {
            bool ret = false;
            if ((mask & AdminTool.Models.Enums.ForcedBlockingMask.BlockedAfterReg) == AdminTool.Models.Enums.ForcedBlockingMask.BlockedAfterReg && IfMustBlock(AdminTool.Models.Enums.EmailVerificationSteps.AfterRegistration))
                ret = true;

            if ((mask & AdminTool.Models.Enums.ForcedBlockingMask.BlockedAfterEmailChange) == AdminTool.Models.Enums.ForcedBlockingMask.BlockedAfterEmailChange && IfMustBlock(AdminTool.Models.Enums.EmailVerificationSteps.AfterEmailChange))
                ret = true;


            return ret;

        }

        public void SetVerifiedMemberAttributes(Member member)
        {
            AdminTool.Models.Enums.ForcedBlockingMask blockingmask;
            GlobalStatusMask globalstatusmask;
            int hideMask;
            if (member == null)
                return;


            blockingmask = GetBlockingMaskAttr(member);
            globalstatusmask = GetGlobalStatusMaskAttr(member);
            if ((blockingmask & AdminTool.Models.Enums.ForcedBlockingMask.BlockedAfterEmailChange) == AdminTool.Models.Enums.ForcedBlockingMask.BlockedAfterEmailChange)
            { blockingmask = blockingmask & (~AdminTool.Models.Enums.ForcedBlockingMask.BlockedAfterEmailChange); }

            if ((blockingmask & AdminTool.Models.Enums.ForcedBlockingMask.BlockedAfterReg) == AdminTool.Models.Enums.ForcedBlockingMask.BlockedAfterReg)
            { blockingmask = blockingmask & (~AdminTool.Models.Enums.ForcedBlockingMask.BlockedAfterReg); }

            globalstatusmask = globalstatusmask | GlobalStatusMask.VerifiedEmail;


            hideMask = member.GetAttributeInt(_brand, "HideMask", 0);

            if ((blockingmask & AdminTool.Models.Enums.ForcedBlockingMask.HideFromMOL) == AdminTool.Models.Enums.ForcedBlockingMask.HideFromMOL)
            {
                hideMask = hideMask & ~(int)AdminTool.Models.Enums.AttributeOptionHideMask.HideMembersOnline;
                blockingmask = blockingmask & ~AdminTool.Models.Enums.ForcedBlockingMask.HideFromMOL;
            }
            if ((blockingmask & AdminTool.Models.Enums.ForcedBlockingMask.HideFromSearch) == AdminTool.Models.Enums.ForcedBlockingMask.HideFromSearch)
            {
                hideMask = hideMask & ~(int)AdminTool.Models.Enums.AttributeOptionHideMask.HideSearch;
                blockingmask = blockingmask & ~AdminTool.Models.Enums.ForcedBlockingMask.HideFromSearch;
            }
            member.SetAttributeInt(_brand, "HideMask", hideMask);
            member.SetAttributeInt(_brand, "GlobalStatusMask", (int)globalstatusmask);
            member.SetAttributeInt(_brand, "BlockingMask", (int)blockingmask);
            member.SetAttributeDate(_brand, "EmailVerificationDate", DateTime.Now);

        }

        public void SetNotVerifiedMemberAttributes(Member member, AdminTool.Models.Enums.EmailVerificationSteps emailVerifyStep)
        {
            GlobalStatusMask globalstatusmask;
            AdminTool.Models.Enums.ForcedBlockingMask blockingmask;
            int hideMask;
            int hideMaskOld;
            if (member == null)
                return;

            blockingmask = GetBlockingMaskAttr(member);
            globalstatusmask = GetGlobalStatusMaskAttr(member);
            globalstatusmask = globalstatusmask & (~GlobalStatusMask.VerifiedEmail);

            if (IfMustBlock(emailVerifyStep))
            {

                if (emailVerifyStep == AdminTool.Models.Enums.EmailVerificationSteps.AfterEmailChange)
                    blockingmask = blockingmask | AdminTool.Models.Enums.ForcedBlockingMask.BlockedAfterEmailChange;

                if (emailVerifyStep == AdminTool.Models.Enums.EmailVerificationSteps.AfterRegistration)
                    blockingmask = blockingmask | AdminTool.Models.Enums.ForcedBlockingMask.BlockedAfterReg;
            }

            hideMask = member.GetAttributeInt(_brand, "HideMask", 0);
            hideMaskOld = hideMask;
            if (IfMustHide(emailVerifyStep))
            {
                if (IfMustHide(AdminTool.Models.Enums.ForcedBlockingMask.HideFromSearch))
                {
                    hideMask = hideMask | (int)AdminTool.Models.Enums.AttributeOptionHideMask.HideMembersOnline;
                    if ((hideMaskOld & (int)AdminTool.Models.Enums.AttributeOptionHideMask.HideMembersOnline) != (int)AdminTool.Models.Enums.AttributeOptionHideMask.HideMembersOnline)
                    {
                        blockingmask = blockingmask | AdminTool.Models.Enums.ForcedBlockingMask.HideFromMOL;
                    }

                }
                if (IfMustHide(AdminTool.Models.Enums.ForcedBlockingMask.HideFromMOL))
                {
                    hideMask = hideMask | (int)AdminTool.Models.Enums.AttributeOptionHideMask.HideSearch;

                    if ((hideMaskOld & (int)AdminTool.Models.Enums.AttributeOptionHideMask.HideSearch) != (int)AdminTool.Models.Enums.AttributeOptionHideMask.HideSearch)
                    {
                        blockingmask = blockingmask | AdminTool.Models.Enums.ForcedBlockingMask.HideFromSearch;
                    }
                }

                member.SetAttributeInt(_brand, "HideMask", hideMask);
            }

            member.SetAttributeInt(_brand, "GlobalStatusMask", (int)globalstatusmask);

            member.SetAttributeInt(_brand, "BlockingMask", (int)blockingmask);

        }



        public bool IfMustBlock(AdminTool.Models.Enums.EmailVerificationSteps blockMask)
        {
            bool ret = false;
            if (((int)_blockingMaskSetting & (int)blockMask) == (int)blockMask)
                ret = true;

            return ret;

        }

        public bool IfMustHide(AdminTool.Models.Enums.EmailVerificationSteps blockMask)
        {
            bool ret = false;
            if ((blockMask & AdminTool.Models.Enums.EmailVerificationSteps.AfterEmailChange) == AdminTool.Models.Enums.EmailVerificationSteps.AfterEmailChange)
                return false;
            if (IfMustHide(AdminTool.Models.Enums.ForcedBlockingMask.HideFromMOL) || IfMustHide(AdminTool.Models.Enums.ForcedBlockingMask.HideFromSearch))
                ret = true;

            return ret;
        }
        public bool IfMustHide(AdminTool.Models.Enums.ForcedBlockingMask blockMask)
        {
            bool ret = false;
            if (((int)_blockingMaskSetting & (int)blockMask) == (int)blockMask)
                ret = true;

            return ret;

        }


        public AdminTool.Models.Enums.ForcedBlockingMask GetBlockingMaskAttr(Member member)
        {
            AdminTool.Models.Enums.ForcedBlockingMask mask = AdminTool.Models.Enums.ForcedBlockingMask.NotBlocked;
            if (member != null)
            {
                mask = (AdminTool.Models.Enums.ForcedBlockingMask)Enum.Parse(typeof(AdminTool.Models.Enums.ForcedBlockingMask), member.GetAttributeInt(_brand, "BlockingMask", 0).ToString());
            }
            return mask;
        }

        public GlobalStatusMask GetGlobalStatusMaskAttr(Member member)
        {
            GlobalStatusMask mask = GlobalStatusMask.Default;
            if (member != null)
            {
                mask = (GlobalStatusMask)Enum.Parse(typeof(GlobalStatusMask), member.GetAttributeInt(_brand, "GlobalStatusMask", 0).ToString());
            }
            return mask;
        }

        public static bool IsMemberVerified(Member member, Brand brand)
        {
            bool ret = false;
            GlobalStatusMask mask = GlobalStatusMask.Default;

            if (member != null)
            {
                mask = (GlobalStatusMask)Enum.Parse(typeof(GlobalStatusMask), member.GetAttributeInt(brand, "GlobalStatusMask", 0).ToString());
            }
            if ((mask & GlobalStatusMask.VerifiedEmail) == GlobalStatusMask.VerifiedEmail)
                ret = true;


            return ret;


        }


        public static bool IsMemberHiddenFromSearch(Member member, Brand brand, AdminTool.Models.Enums.AttributeOptionHideMask hideMaskOption)
        {
            bool ret = false;
            int mask = 0;

            if (member != null)
            {
                mask = Conversion.CInt(member.GetAttributeInt(brand, "HideMask", 0).ToString());
            }
            if ((mask & (int)hideMaskOption) == (int)hideMaskOption)
                ret = true;


            return ret;


        }

         
    }

}
