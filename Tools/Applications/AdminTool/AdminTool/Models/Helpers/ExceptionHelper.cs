﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace AdminTool.Models
{
    public class ExceptionHelper
    {
        public enum ExceptionMode
        {
            Html = 0,
            Text = 1
        }
        public static string GetExceptionMessage(Exception ex, ExceptionMode exMode)
        {
            string lineBreak = "\n\r";
            if (exMode == ExceptionMode.Html)
            {
                lineBreak = "<br/>";
            }

            StringBuilder s = new StringBuilder();

            s.Append("Message: " + ex.Message + lineBreak);
            s.Append("StackTrace: " + ex.StackTrace + lineBreak + lineBreak);

            Exception innerEx = ex.InnerException;
            while (innerEx != null)
            {
                s.Append("Inner Message: " + innerEx.Message + lineBreak);
                s.Append("Inner StackTrace: " + innerEx.StackTrace + lineBreak + lineBreak);
                innerEx = innerEx.InnerException;
            }


            return s.ToString();

        }
    }
}
