﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Web;

namespace AdminTool.Models
{
    public class TimingHelper
    {
        Stopwatch _stopwatch = new Stopwatch();
        Dictionary<string, long> _values = new Dictionary<string, long>();
        public bool _timing = false;

        public void AddTask(string task)
        {
            _values.Add(task, 0);
        }

        public void StartTaskTiming(string task)
        {
            if (!_timing)
            {
                _timing = true;
                _stopwatch.Start();
            }
        }

        public void StopTaskTiming(string task)
        {
            if (_timing)
            {
                _timing = false;
                _stopwatch.Stop();
                _values[task] = _stopwatch.ElapsedMilliseconds;
                _stopwatch.Reset();
            }
        }

        public Dictionary<string, long> GetTimingValues()
        {
            return _values;
        }

    }
}
