﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ValueObjects.Photos;
using Spark.CloudStorage;

namespace AdminTool.Models
{
    public enum PhotoType
    {
        Full = 1,
        Thumbnail
    }
    
    public class MemberPhotoHelper
    {
        public static string GetPhotoDisplayUrl(PhotoType photoType, Photo photo, int communityId, int siteId, bool forceLocalLoad = false)
        {
            var showPhotosFromCloud = !forceLocalLoad && Convert.ToBoolean(RuntimeSettings.GetSetting("ENABLE_MEMBER_PHOTOS_DISPLAY_FROM_CLOUD"));

            var memberPhotoUrl = string.Empty;
            var host = string.Empty;

            if (!showPhotosFromCloud)
            {
                host = "http://" + System.Configuration.ConfigurationManager.AppSettings["DefaultHost"] + ".jdate.com";
            }

            switch (photoType)
            {
                case PhotoType.Full:
                    if (photo.FileWebPath != null) //17243 - some members have null FileWebPath but shouldn't 
                    {
                        memberPhotoUrl = host + (showPhotosFromCloud ? photo.FileCloudPath : photo.FileWebPath);
                    }
                    break;
                case PhotoType.Thumbnail:
                    if (photo.ThumbFileWebPath != null)
                        // some members have null ThumbFileWebPath (before approval for example)
                    {
                        memberPhotoUrl = host +
                                         (showPhotosFromCloud ? photo.ThumbFileCloudPath : photo.ThumbFileWebPath);
                    }
                    break;
            }

            if (showPhotosFromCloud && memberPhotoUrl != string.Empty)
            {
                var cloudClient = new Client(communityId, siteId, RuntimeSettings.Instance);
                memberPhotoUrl = cloudClient.GetFullCloudImagePath(memberPhotoUrl, FileType.MemberPhoto, false,
                    communityId, siteId);
            }

            return memberPhotoUrl;
        }

        public static Photo GetDefaultPhoto(Matchnet.Member.ServiceAdapters.Member member, Brand brand)
        {
            PhotoCommunity photos = member.GetPhotos(brand.Site.Community.CommunityID);
            Photo photo;
            Photo selectedPhoto = null;

            for (byte photoNum = 0; photoNum < photos.Count; photoNum++)
            {
                photo = photos[photoNum];
                if (photo.IsApproved)
                {
                    if (photo.IsPrivate)
                    {
                        selectedPhoto = photo;  //This private photo will get returned if no non-private approved photos are found
                    }
                    else
                    {
                        selectedPhoto = photo; //We found a good photo, get out
                        break;
                    }
                }
            }

            if (selectedPhoto == null && photos.Count > 0)
            {
                //there must be an unapproved photo, so get that
                for (byte photoNum = 0; photoNum < photos.Count; photoNum++)
                {
                    photo = photos[photoNum];
                    if (photo.IsPrivate)
                    {
                        selectedPhoto = photo;  //This private photo will get returned if no non-private approved photos are found
                    }
                    else
                    {
                        selectedPhoto = photo; //We found a good photo, get out
                        break;
                    }
                }
            }

            return selectedPhoto;
        }

        public static string GetDefaultPhotoThumbnailDisplayURL(Matchnet.Member.ServiceAdapters.Member member, Brand brand)
        {
            string photoURL = string.Empty;

            Photo photo = GetDefaultPhoto(member, brand);

            if(photo != null)
            {
                photoURL = GetPhotoDisplayUrl(PhotoType.Thumbnail, photo, brand.Site.Community.CommunityID,
                                              brand.Site.SiteID);
            }

            return photoURL;
        }
        
    }
}