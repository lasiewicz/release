﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using AdminTool.Models.ModelViews.Member;
using Matchnet;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.OmnitureHelper;
using Spark.CloudStorage;
using Spark.Common.FraudService;
using Spark.CommonLibrary;

namespace AdminTool.Models.Helpers
{
    public class TrainingToolHelper
    {
        #region TT Attribute Name Consts
        public const string TTATTRIBUTENAME_MemberID = "MemberID";
        public const string TTATTRIBUTENAME_EmailAddress = "EmailAddress";
        public const string TTATTRIBUTENAME_UserName = "UserName";
        public const string TTATTRIBUTENAME_SiteFirstName = "SiteFirstName";
        public const string TTATTRIBUTENAME_SiteLastName = "SiteLastName";
        public const string TTATTRIBUTENAME_MaritalStatus = "MaritalStatus";
        public const string TTATTRIBUTENAME_GenderMask = "GenderMask";
        public const string TTATTRIBUTENAME_EducationLevel = "EducationLevel";
        public const string TTATTRIBUTENAME_Religion = "Religion";
        public const string TTATTRIBUTENAME_Ethnicity = "Ethnicity";
        public const string TTATTRIBUTENAME_JDateEthnicity = "JDateEthnicity";
        public const string TTATTRIBUTENAME_RegionID = "RegionID";
        public const string TTATTRIBUTENAME_RegistrationIP = "RegistrationIP";
        #endregion

        public static Spark.CloudStorage.BasicClient GetBasicCloudClient()
        {
            string accessKey = RuntimeSettings.GetSetting("AWS_ADMINTOOL_ACCESSKEY");
            string secretKey = RuntimeSettings.GetSetting("AWS_ADMINTOOL_SECRETKEY");
            string bucketName = RuntimeSettings.GetSetting("AWS_ADMINTOOL_PHOTO_BUCKET_NAME");
            string cloudUrl = RuntimeSettings.GetSetting("AWS_ROOT_CLOUD_URL");
            bool appendBucketName = true;

            return new BasicClient(accessKey, secretKey, bucketName, cloudUrl, true);
        }

        public static ExpandedUserInfo GetTTExpandedUserInfo(int ttMemberProfileID)
        {
            var info = new ExpandedUserInfo();

            var ttMemberProfile = DBApproveQueueSA.Instance.GetTTMemberAttributes(ttMemberProfileID);
            info.MemberID = ttMemberProfile.MemberAtributeInts.Find(m => m.MemberAttributeName == TTATTRIBUTENAME_MemberID).FieldValue;
            info.Email =
                ttMemberProfile.MemberAttributeTexts.Find(m => m.MemberAttributeName == TTATTRIBUTENAME_EmailAddress)
                               .FieldValue;
            info.Username =
                ttMemberProfile.MemberAttributeTexts.Find(m => m.MemberAttributeName == TTATTRIBUTENAME_UserName)
                               .FieldValue;
            info.FirstName =
                ttMemberProfile.MemberAttributeTexts.Find(m => m.MemberAttributeName == TTATTRIBUTENAME_SiteFirstName)
                               .FieldValue;
            info.LastName =
                ttMemberProfile.MemberAttributeTexts.Find(m => m.MemberAttributeName == TTATTRIBUTENAME_SiteLastName)
                               .FieldValue;


            var brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(ttMemberProfile.BrandID);
            var resourceBag = GeneralHelper.GetResourceCollection(GeneralHelper.DetermineResourceFileName(brand));

            info.MaritalStatus = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, brand, "MaritalStatus",
                                                                               ttMemberProfile.MemberAtributeInts.Find(
                                                                                   m =>
                                                                                   m.MemberAttributeName ==
                                                                                   TTATTRIBUTENAME_MaritalStatus)
                                                                                              .FieldValue);

            var genderMask =
                ttMemberProfile.MemberAtributeInts.Find(m => m.MemberAttributeName == TTATTRIBUTENAME_GenderMask)
                               .FieldValue;

            info.GenderSeekingGender = MemberHelper.GetGender(genderMask) + " seeking " +
                Enum.GetName(typeof(Enums.SeekingGender), MemberHelper.GetSeekingGender(genderMask));


            info.Education = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, brand, "EducationLevel",
                                                                           ttMemberProfile.MemberAtributeInts.Find(
                                                                               m =>
                                                                               m.MemberAttributeName ==
                                                                               TTATTRIBUTENAME_EducationLevel)
                                                                                          .FieldValue);

            info.Religion = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, brand, "Religion",
                                                                          ttMemberProfile.MemberAtributeInts.Find(
                                                                              m =>
                                                                              m.MemberAttributeName ==
                                                                              TTATTRIBUTENAME_Religion)
                                                                                         .FieldValue);

            bool isJdate = (brand.Site.Community.CommunityID == (int) CommunityIDs.JDate);
            info.Ethnicity = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, brand,
                                                                           isJdate
                                                                               ? TTATTRIBUTENAME_JDateEthnicity
                                                                               : TTATTRIBUTENAME_Ethnicity,
                                                                           ttMemberProfile.MemberAtributeInts.Find(
                                                                               m =>
                                                                               m.MemberAttributeName ==
                                                                               (isJdate
                                                                                    ? TTATTRIBUTENAME_JDateEthnicity
                                                                                    : TTATTRIBUTENAME_Ethnicity))
                                                                                          .FieldValue);

            info.ProfileLocation =
                OmnitureHelper.GetRegionString(ttMemberProfile.MemberAtributeInts.Find(
                    m =>
                    m.MemberAttributeName ==
                    TTATTRIBUTENAME_RegionID).FieldValue, brand,
                                               brand.Site.LanguageID, true, true, true);

            //ip address
            int intIPAddress = ttMemberProfile.MemberAtributeInts.Find(
                    m =>
                    m.MemberAttributeName ==
                    TTATTRIBUTENAME_RegistrationIP).FieldValue;

            IPAddress regIPAddress = null;
            if (intIPAddress != Constants.NULL_INT)
            {
                regIPAddress = new IPAddress(BitConverter.GetBytes(intIPAddress));
                info.IPAddress = regIPAddress.ToString();
            }

            //ip location
            if (regIPAddress != null)
            {
                info.IPLocation = new IPLocation.LookupService().getLocation(regIPAddress.ToString());
            }

            info.FraudResult = new FraudDetailedResponse();

            return info;
        }
    }
}