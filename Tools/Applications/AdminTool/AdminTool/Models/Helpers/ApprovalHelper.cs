﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Net;
using System.Text.RegularExpressions;
using System.Text;
using System.Web.Caching;
using System.Configuration;
using AdminTool.Models.Helpers;
using AdminTool.Models.ModelViews.Member;
using Spark.CommonLibrary;
using AdminTool.Models.ModelViews;

using Matchnet;
using Matchnet.Lib;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ServiceAdapters;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.Content.ServiceAdapters;
using MCA = Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.DifferenceEngine;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Purchase.ServiceAdapters;
using PurchaseVO = Matchnet.Purchase.ValueObjects;
using Matchnet.PremiumServiceSearch.ServiceAdapters;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using AdminTool.Models.ModelViews.Approval;
using Spark.Common.Adapter;
using Spark.Common.OrderHistoryService;
using Spark.Common.PaymentProfileService;

namespace AdminTool.Models
{
    public class ApprovalHelper
    {
        private const string MEMBER_MESSAGES_CACHE_KEY = "MemberMessages";

        public ApprovalHelper() { }
        
        public List<ApprovalMemberMessage> GetCurrentMemberMessages(int adminMemberID)
        {
            object messages = HttpContext.Current.Cache[MEMBER_MESSAGES_CACHE_KEY + adminMemberID.ToString()];
            if (messages != null)
            {
                return (List<ApprovalMemberMessage>)messages;
            }
            else
            {
                return null;
            }
        }

        public void ClearMemberMessages(int adminMemberID)
        {
            HttpContext.Current.Cache.Remove(MEMBER_MESSAGES_CACHE_KEY + adminMemberID.ToString());
        }

        public void PersistMemberMessage(int adminMemberID, int memberID, string message, bool showInRed)
        {
            List<ApprovalMemberMessage> messages = null;
            object cachedMessages = HttpContext.Current.Cache[MEMBER_MESSAGES_CACHE_KEY + adminMemberID.ToString()];

            if (cachedMessages == null)
            {
                messages = new List<ApprovalMemberMessage>();
            }
            else
            {
                messages = (List<ApprovalMemberMessage>)cachedMessages;
            }

            messages.Add(new ApprovalMemberMessage(memberID, message, showInRed));

            HttpContext.Current.Cache.Add(MEMBER_MESSAGES_CACHE_KEY + adminMemberID.ToString(), messages, null, DateTime.Now.AddDays(1), Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
        }

        public static String cleanUpTags(String newValue)
        {
            if (newValue == null || newValue.Length < 1)
            {
                return String.Empty;
            }

            //TL: These newline manipulation is to support line breaks for profile essays (as well as any text on our site that support line breaks)
            //Note: RichTextEdit control shows text in <p></p> tags and each when submitted, automatically has new line at the end of each paragraph
            //We will simulate a newline as two new lines for paragraphs
            string templinebreak = "{{br}}";
            newValue = newValue.Replace("\r\n", templinebreak);
            newValue = newValue.Replace(templinebreak, "\r\n\r\n");

            //Note: RichTextEdit control shows <br /> without newline added
            newValue = newValue.Replace("<br />", "\r\n");
            newValue = newValue.Replace("<br/>", "\r\n");

            // Strip any HTML junk out...
            newValue = Regex.Replace(newValue, "<(.|\n)+?>", "", RegexOptions.IgnoreCase);
            newValue = Regex.Replace(newValue, "&lt;(.|\n)+?>", "", RegexOptions.IgnoreCase);
            newValue = Regex.Replace(newValue, "&nbsp;", "", RegexOptions.IgnoreCase);

            //convert special html characters codes to their original value characters
            //newValue = Regex.Replace(newValue, "&amp;", "&", RegexOptions.IgnoreCase);

            //remove style text inserted by the RichTextEdit control.
            newValue = Regex.Replace(newValue, "<P><FONT style=\"BACKGROUND-COLOR: #c0dcc0\">", "", RegexOptions.IgnoreCase);
            newValue = Regex.Replace(newValue, "</FONT></P>", "", RegexOptions.IgnoreCase);

            // convert the remaining html encoded characters to their original state
            newValue = HttpUtility.HtmlDecode(newValue);

            return newValue;
        }
        public static bool SuspendMember(Member member, Brand brand, int adminID, AdminActionReasonID adminActionReasonID, int actionMask, CommunityIDs communityID, int languageID)
        {
            bool stoppedRenewals = false;

            int globalStatusMask = member.GetAttributeInt(brand, WebConstants.ATTRIBUTE_NAME_GLOBALSTATUSMASK);
            globalStatusMask = globalStatusMask | Matchnet.Lib.ConstantsTemp.ADMIN_SUSPEND;
            member.SetAttributeInt(brand, WebConstants.ATTRIBUTE_NAME_GLOBALSTATUSMASK, globalStatusMask);

            AdminSA.Instance.AdminActionLogInsert(member.MemberID, (int)communityID, actionMask | (int)AdminAction.AdminSuspendMember,
                            adminID, languageID);

            AdminSA.Instance.UpdateAdminNote(adminActionReasonID.ToString(), member.MemberID, adminID, (int)communityID, ConstantsTemp.ADMIN_NOTE_MAX_LENGTH);

            //Notify Iovation on suspend
            MemberHelper.NotifyIovation(member, true, adminActionReasonID);

            //Update UPS Renewal
            Spark.Common.RenewalService.RenewalResponse renewalResponse = Spark.Common.Adapter.RenewalServiceWebAdapter.GetProxyInstanceForBedrock().DisableAutoRenewal(member.MemberID,
                brand.Site.SiteID, 1, adminID, "", 20);
            if (renewalResponse.InternalResponse.responsecode != "0")
                throw new Exception("Failed to update with Unified Renewal to disable auto-renewal for MemberID: " + member.MemberID.ToString() + ", please try again or contact Software Engineering.");
            else
            {
                int confirmationID = renewalResponse.TransactionID;
                stoppedRenewals = true;
            }

            return stoppedRenewals;
        }

        public static ExpandedUserInfo GetExpandedUserInfo(Member member, Brand brand)
        {
            ExpandedUserInfo info = new ExpandedUserInfo();

            // populate the model
            info.MemberSiteInfo = MemberHelper.GetMemberSiteInfo(member, brand.Site.SiteID);
            info.MemberID = member.MemberID;
            info.SiteId = brand.Site.SiteID;
            info.Email = member.EmailAddress;
            info.Username = member.GetUserName(brand);
            info.FirstName = member.GetAttributeText(brand, "SiteFirstName");
            info.LastName = member.GetAttributeText(brand, "SiteLastName");
            info.Age = MemberHelper.GetAge(member, brand);
            info.GenderSeekingGender = MemberHelper.GetGender(member, brand) + " seeking " +
                Enum.GetName(typeof(Enums.SeekingGender), MemberHelper.GetSeekingGender(member, brand));

            Hashtable resourceBag = GeneralHelper.GetResourceCollection(GeneralHelper.DetermineResourceFileName(brand));
            info.MaritalStatus = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, member, brand, "MaritalStatus");
            info.Occupation = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, member, brand, "IndustryType");
            info.Education = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, member, brand, "EducationLevel");
            info.Religion = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, member, brand, "Religion");

            if (brand.Site.Community.CommunityID == (int)CommunityIDs.JDate)
            {
                info.Ethnicity = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, member, brand, "JDateEthnicity");
            }
            else
            {
                info.Ethnicity = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, member, brand, "Ethnicity");
            }

            info.ProfileLocation = MemberHelper.GetRegionDisplayString(member, brand);

            // for non view purposes but we need these
            info.ProfileRegionID = member.GetAttributeInt(brand, "RegionID");
            info.GenderMask = member.GetAttributeInt(brand, "GenderMask");
            info.MaritalStatusIntValue = member.GetAttributeInt(brand, "MaritalStatus");
            info.EducationIntValue = member.GetAttributeInt(brand, "EducationLevel");
            info.ReligionIntValue = member.GetAttributeInt(brand, "Religion");
            info.EthnicityIntValue = member.GetAttributeInt(brand, "Ethnicity");
            // ===

            //ip address
            int intIPAddress = member.GetAttributeInt(brand, "RegistrationIP");
            IPAddress regIPAddress = null;
            if (intIPAddress != Constants.NULL_INT)
            {
                regIPAddress = new IPAddress(BitConverter.GetBytes(intIPAddress));
                info.IPAddress = regIPAddress.ToString();
            }

            //ip location
            if (regIPAddress != null)
            {
                info.IPLocation = new IPLocation.LookupService().getLocation(regIPAddress.ToString());
            }

            info.ThumbnailUrl = MemberPhotoHelper.GetDefaultPhotoThumbnailDisplayURL(member, brand);

            // fraud score
            info.FraudResult = MemberHelper.GetFraudDetailedResponse(member.MemberID, brand.Site.SiteID, 3); 
            info.RegistrationDate = member.GetAttributeDate(brand, "BrandInsertDate");
            info.SubscriberStatus = MemberHelper.GetSubscriberStatusString(member, brand);

            //first sub date and billing phone number
            List<AccountHistory> colAccountHistory = new List<AccountHistory>();
            OrderInfo[] arrOrderInfo = OrderHistoryServiceWebAdapter.GetProxyInstanceForBedrock().GetMemberOrderHistoryByMemberID(member.MemberID, brand.Site.SiteID, 5000, 1);
            PaymentProfileInfo paymentProfileResponse = Spark.Common.Adapter.PaymentProfileServiceWebAdapter.GetProxyInstance().GetMemberDefaultPaymentProfile(member.MemberID, brand.Site.SiteID);
            int orderIDForDefaultPaymentProfile = 0;

            foreach (OrderInfo orderInfo in arrOrderInfo)
            {
                if (orderInfo.OrderDetail.Length > 0)
                {
                    // Only add orders that have order details  
                    AccountHistory accountHistory = new AccountHistory(orderInfo, brand, AccountHistoryType.Financial, member);
                    colAccountHistory.Add(accountHistory);

                    if (paymentProfileResponse != null && orderInfo.UserPaymentGuid == paymentProfileResponse.PaymentProfileID)
                    {
                        if (orderIDForDefaultPaymentProfile <= 0)
                        {
                            ObsfucatedPaymentProfileResponse obsfucatedpaymentProfileResponse = Spark.Common.Adapter.PaymentProfileServiceWebAdapter.GetProxyInstance().GetObsfucatedPaymentProfileByOrderID(orderInfo.OrderID, member.MemberID, brand.Site.SiteID);
                            if (obsfucatedpaymentProfileResponse != null && obsfucatedpaymentProfileResponse.Code == "0"
                                && obsfucatedpaymentProfileResponse.PaymentProfile != null && obsfucatedpaymentProfileResponse.PaymentProfile is ObsfucatedCreditCardPaymentProfile)
                            {
                                info.BillingPhoneNumber = ((ObsfucatedCreditCardPaymentProfile)obsfucatedpaymentProfileResponse.PaymentProfile).PhoneNumber;
                            }
                        }
                    }
                }
            }

            AccountHistory first = (from AccountHistory ah in colAccountHistory
                                    where ah.TransactionType == TransactionType.InitialSubscriptionPurchase
                                    && ah.OrderStatus == OrderStatus.Successful
                                    orderby ah.InsertDateInPST ascending
                                    select ah).Take(1).FirstOrDefault();

            if (first != null)
            {
                info.DaysSinceFirstSubscription = DateTime.Now.Subtract(first.InsertDateInPST).Days;
            }

            return info;
        }
    }
}
