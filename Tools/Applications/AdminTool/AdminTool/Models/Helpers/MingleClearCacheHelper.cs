﻿using System;
using System.IO;
using System.Net;
using System.Web;
using Matchnet.Configuration.ServiceAdapters;
using Spark.CommonLibrary.Logging;

namespace AdminTool.Models
{
    public class MingleClearCacheHelper
    {
        private string _ClearCacheURL;
        private string _ClearCacheKey;
        private int _MemberID;

        public MingleClearCacheHelper(int memberID)
        {
            _ClearCacheURL = RuntimeSettings.GetSetting("MINGLE_CLEAR_CACHE_URL");
            _ClearCacheKey = RuntimeSettings.GetSetting("MINGLE_CLEAR_CACHE_KEY");
            _MemberID = memberID;
        }

        public string GetClearCacheResponse()
        {
            string response = null;

            string siteIDs = RuntimeSettings.GetSetting("MINGLE_CLEAR_CACHE_SITE_IDS");
            if (string.IsNullOrEmpty(siteIDs.Trim()))
            {
                return string.Empty;
            }

            string[] sitesArray = siteIDs.Split(new char[] { ',' });

            foreach (string siteID in sitesArray)
            {
                string baseUri = _ClearCacheURL +
                                 string.Format("?key={0}&siteid={1}&memberid={2}", _ClearCacheKey, siteID, _MemberID);
                Uri serviceUri = new Uri(baseUri, UriKind.Absolute);
                HttpWebRequest webRequest = (HttpWebRequest)System.Net.WebRequest.Create(serviceUri);
                webRequest.Timeout = 2000;
                webRequest.ContentType = "application/x-www-form-urlencoded";

                try
                {
                    HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                    StreamReader sr = new StreamReader(webResponse.GetResponseStream());
                    response += GetTextFromResponse(sr.ReadToEnd()) + " siteID=" + siteID + "\n";
                }
                catch (Exception ex)
                {
                    new Logger(this.GetType()).Error("Error in MingleClearCacheHelper.GetClearCacheResponse().", ex);

                    response += "An error has occurred while trying to clear Mingle's cache." + " siteID=" + siteID + "\n";
                }
            }

            return response;
        }

        private string GetTextFromResponse(string webResponse)
        {
            string result = string.Empty;

            switch (webResponse)
            {
                case "-1": result = "You didn't pass in all of the GET parameters or the key didn't match."; break;
                case "0": result = "An error occurred on Mingle's end."; break;
                case "1": result = "The cache was cleared for this member."; break;
                case "An application error has occurred": result = "A system failure has occured on Mingle."; break;
                default:
                    result = "Unknown response: " + webResponse; break;
            }

            return result;
        }
    }
}
