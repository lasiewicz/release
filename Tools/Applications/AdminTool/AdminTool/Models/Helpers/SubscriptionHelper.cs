﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ServiceAdapters;
using Spark.Common.Adapter;
using Spark.Common.AuthorizationService;
using Spark.Common.OrderHistoryService;
using Spark.Common.PaymentProfileService;
using Spark.CommonLibrary.Logging;

namespace AdminTool.Models
{
    public class SubscriptionHelper
    {
        static Logger _logger = new Logger(typeof(Controller));
        public static string ClearMingleMemberCache(Member targetMember, Brand targetbrand)
        {
            string response = string.Empty;
            int siteID = 0;
            int memberID = 0;

            try
            {
                string clearCacheURL = RuntimeSettings.GetSetting("MINGLE_CLEAR_CACHE_URL");
                string clearCacheKey = RuntimeSettings.GetSetting("MINGLE_CLEAR_CACHE_KEY");

                var brands = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandsByCommunity(targetbrand.Site.Community.CommunityID);

                foreach (Brand brand in brands)
                {
                    siteID = brand.Site.SiteID;
                    memberID = targetMember.MemberID;

                    string baseUri = clearCacheURL +
                                     string.Format("?key={0}&siteid={1}&memberid={2}", clearCacheKey, brand.Site.SiteID, targetMember.MemberID);
                    var serviceUri = new Uri(baseUri, UriKind.Absolute);
                    var webRequest = (HttpWebRequest)System.Net.WebRequest.Create(serviceUri);
                    webRequest.Timeout = 2000;
                    webRequest.ContentType = "application/x-www-form-urlencoded";

                    try
                    {
                        var webResponse = (HttpWebResponse)webRequest.GetResponse();
                        var sr = new StreamReader(webResponse.GetResponseStream());
                        response += sr.ReadToEnd();
                    }
                    catch (Exception ex)
                    {
                        response += "An error has occurred while trying to clear Mingle's cache." + " siteID=" + brand.Site.SiteID + "\n";
                        _logger.Error(ex.Message + response, ex);
                    }
                }
            }
            catch (Exception ex)
            {
                // do nothing for exceptions because we want the sub confirmation page to load even if there was a problem
                // busting Mingle's member cache
                _logger.Error("Error from ClearMingleMemberCache() siteID: " + siteID + " memberID: " + memberID + " ex: " + ex.Message, ex);
            }

            return response;
        }

        public static string[] GetColorCodeArgs(Member targetMember, Brand targetBrand, int orderId)
        {
            string userName = targetMember.GetUserName(targetBrand);
            string confirmationNumber = orderId.ToString();
            string transactionDate = DateTime.Now.ToString();

            return new string[3]
				{
					userName,
					confirmationNumber, 
					transactionDate
				};
        }

        public static Matchnet.Purchase.ValueObjects.Plan UPSGetPrimaryPlan(OrderInfo orderInfo, Brand brand)
        {
            int legacyPlanIDFound = Constants.NULL_INT;
            int alaCartePlanIDFound = Constants.NULL_INT;
            int discountPlanIDFound = Constants.NULL_INT;


            if (orderInfo.OrderDetail.Length == 1)
            {
                return Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(orderInfo.OrderDetail[0].PackageID, brand.BrandID);
            }
            else
            {
                foreach (OrderDetailInfo odi in orderInfo.OrderDetail)
                {
                    Matchnet.Purchase.ValueObjects.Plan plan = Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(odi.PackageID, brand.BrandID);

                    if (plan != null)
                    {
                        if ((plan.PlanTypeMask & Matchnet.Purchase.ValueObjects.PlanType.ALaCarte) == Matchnet.Purchase.ValueObjects.PlanType.ALaCarte)
                        {
                            // An ala carte item exists in the order
                            if (alaCartePlanIDFound == Constants.NULL_INT)
                            {
                                // Only set this once so the first PlanID found will be used as the
                                // primary PlanID
                                alaCartePlanIDFound = odi.PackageID;
                            }
                        }
                        else if ((plan.PlanTypeMask & Matchnet.Purchase.ValueObjects.PlanType.Discount) == Matchnet.Purchase.ValueObjects.PlanType.Discount)
                        {
                            // A discount item exists in the order
                            if (discountPlanIDFound == Constants.NULL_INT)
                            {
                                // Only set this once so the first PlanID found will be used as the
                                // primary PlanID
                                discountPlanIDFound = odi.PackageID;
                            }
                        }
                        else
                        {
                            // The primary PlanID will be the item that is not an ala carte 
                            // or discount package  
                            if (legacyPlanIDFound == Constants.NULL_INT)
                            {
                                // Only set this once so the first PlanID found will be used as the
                                // primary PlanID
                                legacyPlanIDFound = odi.PackageID;
                                break;
                            }
                        }
                    }
                }

                if (legacyPlanIDFound != Constants.NULL_INT)
                {
                    // Return any PlanID as the primary PlanID as long as this is not an ala carte 
                    // package or a discount package
                    return Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(legacyPlanIDFound, brand.BrandID);
                }
                else if (alaCartePlanIDFound != Constants.NULL_INT)
                {
                    // If the member only purchased an ala carte package and no base subscription packages,
                    // than return the first ala carte package found
                    return Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(alaCartePlanIDFound, brand.BrandID);
                }
                else
                {
                    // If the order did not have any base subscriptions or ala carte packages, than there
                    // no primary PlanID was found
                    // Do not return the discount PlanID since a discount cannot exists without a base 
                    // subscription or ala carte purchase  
                    return null;
                }
            }
        }

        public static string[] GetFreeTrialArgs(Member member, Brand brand, AuthorizationSubscription freeTrialSubscription)
        {
            string userName = member.GetUserName(brand);
            string confirmationNumber = Convert.ToString(freeTrialSubscription.AuthorizationSubscriptionID);
            string creditCardNumber = Constants.NULL_STRING;
            string creditCardType = Constants.NULL_STRING;
            string transactionDate = DateTime.Now.ToString();

            try
            {
                if (freeTrialSubscription != null)
                {
                    ObsfucatedPaymentProfileResponse paymentProfileResponse = PaymentProfileServiceWebAdapter.GetProxyInstance().GetObsfucatedPaymentProfileByUserPaymentID(freeTrialSubscription.PaymentProfileID, freeTrialSubscription.CallingSystemID);

                    if (paymentProfileResponse.Code == "0")
                    {
                        ObsfucatedCreditCardPaymentProfile creditCardPaymentProfile = paymentProfileResponse.PaymentProfile as ObsfucatedCreditCardPaymentProfile;
                        ObfuscatedCreditCardShortFormMobileSitePaymentProfile creditCardShortFormMobileSitePaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedCreditCardShortFormMobileSitePaymentProfile;
                        ObfuscatedCreditCardShortFormFullWebSitePaymentProfile creditCardShortFormFullWebSitePaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedCreditCardShortFormFullWebSitePaymentProfile;
                        ObfuscatedDebitCardPaymentProfile debitCardPaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedDebitCardPaymentProfile;

                        if (creditCardPaymentProfile != null)
                        {
                            creditCardNumber = creditCardPaymentProfile.LastFourDigitsCreditCardNumber;

                            creditCardType = creditCardPaymentProfile.CardType;
                            try
                            {
                                Matchnet.Purchase.ValueObjects.CreditCardCollection ccc = Matchnet.Purchase.ServiceAdapters.PurchaseSA.Instance.GetCreditCardTypes(brand.Site.SiteID);

                                if (ccc != null)
                                {
                                    Matchnet.Purchase.ValueObjects.CreditCardType cardType = UPSGetMatchnetCreditCardType(creditCardType);

                                    if (cardType != Matchnet.Purchase.ValueObjects.CreditCardType.None)
                                    {
                                        //creditCardType = g.GetResource(ccc.FindByID(
                                        //    Convert.ToInt32(cardType)).ResourceConstant, null); //TODO
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                creditCardType = creditCardPaymentProfile.CardType;
                            }
                        }
                        else if (creditCardShortFormMobileSitePaymentProfile != null)
                        {
                            creditCardNumber = creditCardShortFormMobileSitePaymentProfile.LastFourDigitsCreditCardNumber;

                            creditCardType = creditCardShortFormMobileSitePaymentProfile.CardType;
                            try
                            {
                                Matchnet.Purchase.ValueObjects.CreditCardCollection ccc = Matchnet.Purchase.ServiceAdapters.PurchaseSA.Instance.GetCreditCardTypes(brand.Site.SiteID);

                                if (ccc != null)
                                {
                                    Matchnet.Purchase.ValueObjects.CreditCardType cardType = UPSGetMatchnetCreditCardType(creditCardType);

                                    if (cardType != Matchnet.Purchase.ValueObjects.CreditCardType.None)
                                    {
                                        //creditCardType = g.GetResource(ccc.FindByID(
                                        //    Convert.ToInt32(cardType)).ResourceConstant, null); //TODO
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                creditCardType = creditCardShortFormMobileSitePaymentProfile.CardType;
                            }
                        }
                        else if (creditCardShortFormFullWebSitePaymentProfile != null)
                        {
                            creditCardNumber = creditCardShortFormFullWebSitePaymentProfile.LastFourDigitsCreditCardNumber;

                            creditCardType = creditCardShortFormFullWebSitePaymentProfile.CardType;
                            try
                            {
                                Matchnet.Purchase.ValueObjects.CreditCardCollection ccc = Matchnet.Purchase.ServiceAdapters.PurchaseSA.Instance.GetCreditCardTypes(brand.Site.SiteID);

                                if (ccc != null)
                                {
                                    Matchnet.Purchase.ValueObjects.CreditCardType cardType = UPSGetMatchnetCreditCardType(creditCardType);

                                    if (cardType != Matchnet.Purchase.ValueObjects.CreditCardType.None)
                                    {
                                        //creditCardType = g.GetResource(ccc.FindByID(
                                        //    Convert.ToInt32(cardType)).ResourceConstant, null); //TODO
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                creditCardType = creditCardShortFormFullWebSitePaymentProfile.CardType;
                            }
                        }
                        else if (debitCardPaymentProfile != null)
                        {
                            creditCardNumber = debitCardPaymentProfile.LastFourDigitsCreditCardNumber;

                            creditCardType = debitCardPaymentProfile.CardType;
                            try
                            {
                                Matchnet.Purchase.ValueObjects.CreditCardCollection ccc = Matchnet.Purchase.ServiceAdapters.PurchaseSA.Instance.GetCreditCardTypes(brand.Site.SiteID);

                                if (ccc != null)
                                {
                                    Matchnet.Purchase.ValueObjects.CreditCardType cardType = UPSGetMatchnetCreditCardType(creditCardType);

                                    if (cardType != Matchnet.Purchase.ValueObjects.CreditCardType.None)
                                    {
                                        //creditCardType = g.GetResource(ccc.FindByID(
                                        //    Convert.ToInt32(cardType)).ResourceConstant, null); //TODO
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                creditCardType = debitCardPaymentProfile.CardType;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Debug("Error in getting the free trial subscription details, Error message: " + ex.Message);
            }
            finally
            {
                PaymentProfileServiceWebAdapter.CloseProxyInstance();
            }

            return new string[5]
				{
					userName,
					confirmationNumber, 
					creditCardType,
					creditCardNumber,
					transactionDate
				};
        }


        private static Matchnet.Purchase.ValueObjects.CreditCardType UPSGetMatchnetCreditCardType(string upsCreditCardType)
        {
            Matchnet.Purchase.ValueObjects.CreditCardType cardType = Matchnet.Purchase.ValueObjects.CreditCardType.None;
            //this is the UPS credit card type text
            switch (upsCreditCardType)
            {
                case "Visa":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.Visa;
                    break;
                case "Mastercard":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.MasterCard;
                    break;
                case "AmericanExpress":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.AmericanExpress;
                    break;
                case "Discover":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.Discover;
                    break;
                case "Switch":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.Switch;
                    break;
                case "CarteBlanche":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.CarteBlanche;
                    break;
                case "Solo":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.Solo;
                    break;
                case "DirectDebit":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.DirectDebit;
                    break;
                case "Delta":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.Delta;
                    break;
                case "Electron":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.Electron;
                    break;
                case "DinersClub":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.DinersClub;
                    break;
                case "YoterClubLeumiCard":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.YoterClubLeumiCard;
                    break;
                case "LeumiCardStudentCard":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.LeumiCardStudentCard;
                    break;
                case "YoterClubIsraCard":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.YoterClubIsraCard;
                    break;
                case "IsraCardCampusCardStudent":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.IsraCardCampusCardStudent;
                    break;
                case "VisaCalSoldiersClub":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.VisaCalSoldiersClub;
                    break;
                case "IsraCard":
                    cardType = Matchnet.Purchase.ValueObjects.CreditCardType.IsraCard;
                    break;
            }

            return cardType;
        }

        public static string GetKountMerchantID(Brand brand)
        {
            string merchantID = "";
            try
            {
                merchantID = RuntimeSettings.GetSetting("KOUNT_MERCHANT_ID", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID);
            }
            catch (Exception ex)
            {
                //setting probably doesn't exist
            }

            return merchantID;
        }

        /// <summary>
        /// returns the gender string
        /// </summary>
        /// <param name="genderMask"></param>
        /// <returns></returns>
        public static string GetGender(int genderMask)
        {
            genderMask = genderMask & Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE;
            string gender = (genderMask == Matchnet.Lib.ConstantsTemp.GENDERID_FEMALE) ? "Female" : "Male";
            return gender;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="attributeName"></param>
        /// <param name="attributeValue"></param>
        /// <param name="brand"></param>
        /// <returns></returns>
        public static string GetDescription(string attributeName, int attributeValue, Brand brand)
        {
            var attributeOptionCollection =
                AttributeOptionSA.Instance.GetAttributeOptionCollection(attributeName, brand.Site.Community.CommunityID,
                    brand.Site.SiteID, brand.BrandID);

            if (attributeOptionCollection == null)
                return string.Empty;

            var attributeOption = attributeOptionCollection[attributeValue];

            if (attributeOption != null)
                return attributeOption.Description;

            return string.Empty;
        }

    }
}