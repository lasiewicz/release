﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Runtime.Serialization.Json;
using System.Xml;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Configuration;
using Spark.CommonLibrary.Logging;

namespace AdminTool.Models
{

    public class MingleFraudHelper
    {
        private string _fraudScoreURL;

        public static readonly MingleFraudHelper Instance = new MingleFraudHelper();

        private MingleFraudHelper()
        {
            _fraudScoreURL = RuntimeSettings.GetSetting("MINGLE_FRAUD_SCORE_URL");
        }

        public MingleFraudResult GetFraudResult(int siteID, string username)
        {
            string fraudScore = string.Empty;
            string grade = string.Empty;
            string color = string.Empty;
            bool isError = false;
            string formattedUri = String.Format(_fraudScoreURL, username, siteID.ToString());

            Uri serviceUri = new Uri(formattedUri, UriKind.Absolute);
            HttpWebRequest webRequest = (HttpWebRequest)System.Net.WebRequest.Create(serviceUri);
            webRequest.Timeout = 2000;

            try
            {
                HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();

                XmlDictionaryReader xmlReader = JsonReaderWriterFactory.CreateJsonReader(response.GetResponseStream(), new System.Xml.XmlDictionaryReaderQuotas());

                while (xmlReader.Read())
                {
                    if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name.ToLower() == "score")
                    {
                        fraudScore = xmlReader.ReadElementContentAsString();
                    }
                    if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name.ToLower() == "grade")
                    {
                        grade = xmlReader.ReadElementContentAsString().ToLower();
                    }
                    if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name.ToLower() == "color")
                    {
                        color = xmlReader.ReadElementContentAsString();
                    }
                }
                if (fraudScore.ToLower() == "unknown")
                {
                    grade = "u";
                }
            }
            catch (Exception ex)
            {
                isError = true;
                new Logger(this.GetType()).Error("Error in MingleFraudScoreHelper.GetFraudScore().", ex);
            }

            return new MingleFraudResult(grade, color, isError);
        }

    }
}
