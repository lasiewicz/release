﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;


namespace AdminTool.Models
{
    public class DefaultPaymentProfileCache
    {
        static System.Web.Caching.Cache _cache = HttpContext.Current.Cache;

        public static void Add(int memberID, int siteID, string paymentProfileID)
        {
            string cacheKey = getCacheKey(memberID, siteID);
            _cache.Add(cacheKey, paymentProfileID, null, DateTime.Now.AddHours(1), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
        }

        public static void Remove(int memberID, int siteID)
        {
            string cacheKey = getCacheKey(memberID, siteID);
            _cache.Remove(cacheKey);
        }

        public static string GetPaymentProfileID(int memberID, int siteID)
        {
            object paymentProfileID = _cache.Get(getCacheKey(memberID, siteID));
            if (paymentProfileID != null)
                return paymentProfileID.ToString();
            else
                return string.Empty;
        }


        private static string getCacheKey(int memberID, int siteID)
        {
            return memberID.ToString() + "-" + siteID.ToString();
        }

    }
}