﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI.MobileControls;
using System.Windows.Forms;
using AdminTool.Models.ModelViews;
using AdminTool.Models.ModelViews.Migration;
using Matchnet;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Search.ServiceAdapters;
using Spark.MingleMigration.ServiceAdapters;
using Spark.MingleMigration.ValueObjects;
using Spark.SAL;
using CVO=Matchnet.Content.ValueObjects.AttributeMetadata;
using Constants = Matchnet.Constants;
using Matchnet.List.ServiceAdapters;
using Matchnet.List.ValueObjects;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Photos;
using RestSharp.Extensions;
using Photo = Matchnet.Member.ValueObjects.Photos.Photo;

namespace AdminTool.Models
{
    public class MigrationManager
    {
        private readonly List<MingleAttribute> _mingleAttributes;

        public MigrationManager()
        {
            _mingleAttributes = AttributeMapperSA.Instance.GetMingleAttributeMapper().GetAllMingleAttributes();
        }

        /// <summary>
        /// Use the GetListMembers method to get a list of added members broken down by category
        /// </summary>
        /// <param name="member">The member.</param>
        /// <param name="brand">The brand.</param>
        /// <param name="startRow">The start row.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns></returns>
        public List<ListCategoryModel> GetMigratedListItemsByCategory(Member member,
            Brand brand, int startRow, int pageSize )
        {
            var siteId = brand.Site.SiteID;
            var communityId = brand.Site.Community.CommunityID;
            var list = ListSA.Instance.GetList(member.MemberID, ListLoadFlags.IngoreSACache);

            var categories = new List<ListCategoryModel>();


            foreach (var category in GetEnumerationValues<HotListCategory>())
            {
                var categoryModel = new ListCategoryModel { CategoryId = (int)category, MemberIds = new Dictionary<int, ListItemDetail>() };
                int rowCount;

                foreach (var item in list.GetListMembers(category, communityId, siteId, startRow, pageSize, out rowCount))
                {
                    categoryModel.MemberIds.Add((int)item, list.GetListItemDetail(category, communityId, siteId, (int)item));                   
                }

                categoryModel.RowCount = rowCount;
                categories.Add(categoryModel);
            }

            return categories;
        }

        /// <summary>
        ///     Get all community level photos for the member
        /// </summary>
        public List<Photo> GetPhotos(Member member, Brand brand)
        {
            return member.GetPhotos(brand.Site.Community.CommunityID).GetList();
        }

        public static IEnumerable<T> GetEnumerationValues<T>()
        {
            return Enum.GetValues(typeof (T)).Cast<T>();
        }


        public List<MigratedAttribute> GetMigratedIntegerAttributes(Member member, Brand brand)
        {
            var migratedAttributes = new List<MigratedAttribute>();

            var allAttributes = AttributeMetadataSA.Instance.GetAttributes().GetAllAttributes();

            foreach (var attribute in allAttributes.Keys.Cast<object>().Select(attributeId => (CVO.Attribute) allAttributes[attributeId])
                                                                        .Where(attribute => attribute.DataType == CVO.DataType.Bit 
                                                                                || attribute.DataType == CVO.DataType.Number 
                                                                                || attribute.DataType == CVO.DataType.Mask))
            {
                try
                {
                    var attributeGroup = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(attribute, brand);
                    if (attributeGroup == null) continue;
                    var value = member.GetAttributeInt(brand, attribute.ID);
                    if (value == Constants.NULL_INT) continue;
                    var attributeDescription = string.Empty;

                    if (attribute.DataType == CVO.DataType.Mask && value != 0)
                    {
                        var optionDescription = new StringBuilder();

                        var options = AttributeOptionSA.Instance.GetAttributeOptionCollection(
                            attribute.Name, Constants.NULL_INT);

                        if (options != null)
                        {
                            foreach (var option in options.GetList().Where(option => option.Value != 0 && ((option.Value & value) == option.Value)))
                            {
                                if (optionDescription.Length > 0)
                                {
                                    optionDescription.Append(",");
                                }
                                optionDescription.Append(option.Description);
                            }
                            attributeDescription = optionDescription.ToString();
                        }
                    }
                    if (attribute.DataType == CVO.DataType.Number && value != 0)
                    {
                        var options = AttributeOptionSA.Instance.GetAttributeOptionCollection(attribute.Name, Constants.NULL_INT);

                        if (options != null)
                        {
                            foreach (var option in options.GetList().Where(option => option.Value == value))
                            {
                                attributeDescription = option.Description;
                                break;
                            }
                        }
                    }

                    var mingleAttribute = (from a in _mingleAttributes where a.BHAttributeID == attribute.ID select a).FirstOrDefault();
                    var mingleAttributeName = mingleAttribute == null ? string.Empty : mingleAttribute.AttributeName;

                    if (attribute.Name.ToLower() == "registrationip" || attribute.Name.ToLower() == "lastip")
                    {
                        migratedAttributes.Add(new MigratedAttribute
                        {
                            BHAttributeID = attribute.ID,
                            BHAttributeName = attribute.Name,
                            Value = new IPAddress(BitConverter.GetBytes(value)).ToString(),
                            Description = attributeDescription,
                            MingleAttributeName = mingleAttributeName
                        });
                    }
                    else
                    {
                        migratedAttributes.Add(new MigratedAttribute
                        {
                            BHAttributeID = attribute.ID,
                            BHAttributeName = attribute.Name,
                            Value = value.ToString(CultureInfo.InvariantCulture),
                            Description = attributeDescription,
                            MingleAttributeName = mingleAttributeName
                        });
                    }
                }
                catch (Exception ex)
                {
                    var y = ex.Message;
                }
            }
            return migratedAttributes;
        }

        public List<MigratedAttribute> GetMigratedTextAttributes(Member member, Brand brand)
        {
            var migratedAttributes = new List<MigratedAttribute>();
            var allAttributes = AttributeMetadataSA.Instance.GetAttributes().GetAllAttributes();

            foreach (var attribute in allAttributes.Keys.Cast<object>().Select(attributeId => (CVO.Attribute) allAttributes[attributeId])
                                                                        .Where(attribute => attribute.DataType == CVO.DataType.Text))
            {
                try
                {
                    var attributeGroup = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(attribute, brand);
                    if (attributeGroup == null) continue;
                    TextStatusType textStatusType;
                    var value = member.GetAttributeText(brand.Site.Community.CommunityID,
                        brand.Site.SiteID,
                        brand.BrandID,
                        2,
                        attribute.Name,
                        String.Empty,
                        out textStatusType);

                    if (string.IsNullOrEmpty(value)) continue;
                    var mingleAttribute = (from a in _mingleAttributes where a.BHAttributeID == attribute.ID select a).FirstOrDefault();
                    var mingleAttributeName = mingleAttribute == null ? string.Empty : mingleAttribute.AttributeName; 
                    migratedAttributes.Add(new MigratedAttribute
                    {
                        BHAttributeID = attribute.ID,
                        BHAttributeName = attribute.Name,
                        MingleAttributeName = mingleAttributeName,
                        Value = value,
                        Description = string.Empty,
                        ApprovedStatus = textStatusType.ToString()
                    });
                }
                catch (Exception ex)
                {
                    var y = ex.Message;
                    throw ex;
                }
            }
            return migratedAttributes;
        }

        public List<MigratedAttribute> GetMigratedDateAttributes(Member member, Brand brand)
        {
            var migratedAttributes = new List<MigratedAttribute>();

            var allAttributes = AttributeMetadataSA.Instance.GetAttributes().GetAllAttributes();

            foreach (var attribute in allAttributes.Keys.Cast<object>().Select(attributeId => (CVO.Attribute) allAttributes[attributeId])
                                                                       .Where(attribute => attribute.DataType == CVO.DataType.Date))
            {
                try
                {
                    var attributeGroup = AttributeMetadataSA.Instance.GetAttributes().GetAttributeGroup(attribute, brand);
                    if (attributeGroup == null) continue;
                    var value = member.GetAttributeDate(brand, attribute.ID);

                    if (value == DateTime.MinValue) continue;
                    var mingleAttribute = (from a in _mingleAttributes where a.BHAttributeID == attribute.ID select a).FirstOrDefault();
                    var mingleAttributeName = mingleAttribute == null ? string.Empty : mingleAttribute.AttributeName;

                    migratedAttributes.Add(new MigratedAttribute
                    {
                        BHAttributeID = attribute.ID,
                        BHAttributeName = attribute.Name,
                        MingleAttributeName = mingleAttributeName,
                        Value = value.ToString(CultureInfo.InvariantCulture)
                    });
                }
                catch (Exception ex)
                {
                    var y = ex.Message;
                }
            }
            return migratedAttributes;
        }

        public List<MigratedSearchPreference> GetMigratedSearchPreferences(Member member, Brand brand)
        {
            var migratedPreferences = new List<MigratedSearchPreference>();

            var allAttributes = AttributeMetadataSA.Instance.GetAttributes().GetAllAttributes();
            var searchPreferenceMapper = AttributeMapperSA.Instance.GetMingleSearchPreferenceMapper();
            var attributeMapper = AttributeMapperSA.Instance.GetMingleAttributeMapper();

            var mappedPreferences = searchPreferenceMapper.GetAllPreferencesForGroup(brand.Site.SiteID);
            var memberSearchCollection = MemberSearchPreferencesSA.Instance.GetMemberSearchCollection(member.MemberID, brand.Site.Community.CommunityID, true);

            if (memberSearchCollection == null || memberSearchCollection.GetPrimarySearch() == null) 
                return migratedPreferences;
            var memberSearchPreferences = memberSearchCollection.GetPrimarySearch().SearchPreferenceCollection;

            foreach (var memberSearchPreference in memberSearchPreferences)
            {
                var description = string.Empty;
                var preference = mappedPreferences.FirstOrDefault(p => memberSearchPreference.Name.ToLower() == p.BHPreferenceName.ToLower());

                if (preference != null)
                {
                    if (!string.IsNullOrEmpty(preference.RelatedMingleAttributeName) && preference.IsMaskValue)
                    {
                        var mingleAttribute = 
                            attributeMapper.GetMingleAttributeByNameAndGroup(preference.RelatedMingleAttributeName, brand.Site.SiteID);

                        var bhAttribute = (CVO.Attribute) allAttributes[mingleAttribute.BHAttributeID];

                        var optionDescription = new StringBuilder();

                        var options = AttributeOptionSA.Instance.GetAttributeOptionCollection(bhAttribute.Name, Constants.NULL_INT);

                        if (options != null)
                        {
                            var searchPreference = memberSearchPreference;
                            foreach (var option in options.GetList().Where(option => option.Value != 0 && ((option.Value & Convert.ToInt32(searchPreference.Value)) == option.Value)))
                            {
                                if (optionDescription.Length > 0)
                                {
                                    optionDescription.Append(",");
                                }
                                optionDescription.Append(option.Description);
                            }
                            description = optionDescription.ToString();
                        }
                    }

                    migratedPreferences.Add(new MigratedSearchPreference
                    {
                        BHPreferenceName = preference.BHPreferenceName,
                        MinglePreferenceName = preference.MinglePreferenceName,
                        Description = description,
                        Value = memberSearchPreference.Value,
                        Weight = memberSearchPreference.Weight.ToString(CultureInfo.InvariantCulture)
                    });
                }
                else
                {
                    migratedPreferences.Add(new MigratedSearchPreference
                    {
                        BHPreferenceName = memberSearchPreference.Name,
                        Value = memberSearchPreference.Value
                    });
                }
            }

            return migratedPreferences;
        }
    }
}