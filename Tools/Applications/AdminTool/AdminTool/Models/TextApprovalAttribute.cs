﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Member.ValueObjects;

namespace AdminTool.Models
{
    public class TextApprovalAttribute
    {
        public string AttributeName { get; set; }
        public string Value { get; set; }
        public string OldValue { get; set; }
        public TextStatusType Status { get; set; }
        public int MaxLength { get; set; }
        public int TextboxHeight { get; set; }
        public string StatusIconSource { get; set; }
        public string DisplayValue { get; set; }
        public int Number { get; set; }
        public int AttributeGroupID { get; set; }
        public int LanguageID {get;set;}
        public bool? CRXApproval { get; set; }
        public bool ShowNoCRXResponseMessage { get; set; }
        
        public TextApprovalAttribute()
        {

        }

        public TextApprovalAttribute(string attributeName, string value, string oldValue, TextStatusType status, int maxLength)
        {
            AttributeName = attributeName;
            Value = value;
            OldValue = oldValue;
            Status = status;
            MaxLength = maxLength;
        }
    }
}
