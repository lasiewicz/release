﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models.ModelViews.DNE;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ServiceAdapters;

namespace AdminTool.Models
{
    public class DNEManager : ManagerBase
    {
        private const int PageSize = 20;

        public List<SelectListItem> GetSites()
        {
            var sitesList = new List<SelectListItem>();
            Sites sites = BrandConfigSA.Instance.GetSites();

            foreach (Site s in sites)
            {
                int siteid = s.SiteID;
                int communityid = s.Community.CommunityID;

                bool active = Conversion.CBool(RuntimeSettings.GetSetting("SITE_ACTIVE_FLAG", communityid, siteid), true);
                if (!active)
                    continue;
                sitesList.Add(new SelectListItem() { Text = s.Name, Value = s.SiteID.ToString() });
            }

            sitesList.Sort((item1, item2) => item1.Text.CompareTo(item2.Text));
            return sitesList;
        }

        public ArrayList Search(int siteId, string emailAddress, int startRow, ref int totalRows)
        {
            return DoNotEmailSA.Instance.Search(siteId, emailAddress, startRow, PageSize, ref totalRows);
        }

        public bool RemoveMember(int siteId, string email)
        {
           return DoNotEmailSA.Instance.RemoveEmailAddress(siteId, email);
        }

        public ArrayList GetSitesSummary()
        {
            return DoNotEmailSA.Instance.GetSiteSummary();
        }

        public int UploadEmailAddress(string fileContent, int siteId)
        {
            int count = 0;
            string[] entries = fileContent.Split('\n');
            foreach (var entry in entries)
            {
                bool success = DoNotEmailSA.Instance.AddEmailAddressBySiteID(siteId, entry.Trim());
                if (success)
                    count++;
            }
            return count;
        }
    }
}