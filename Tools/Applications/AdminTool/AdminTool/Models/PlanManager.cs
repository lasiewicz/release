﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminTool.Models.ModelViews.SubAdmin;
using Matchnet.PromoEngine.ValueObjects;
using AT = AdminTool.Models.ModelViews.SubAdmin;
using Matchnet.Purchase.ValueObjects;
using Matchnet.Content.ServiceAdapters;
using Matchnet;
using Spark.Common.Adapter;
using Spark.Common.CatalogService;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Purchase.ServiceAdapters;
using System.Transactions;
using System.Web.Mvc;
using Plan = Matchnet.Purchase.ValueObjects.Plan;

namespace AdminTool.Models
{
    public class PlanManager : ManagerBase
    {
        public PlanManager()
        {
        }

        /// <summary>
        /// Returns a blank plan with defaults if any.
        /// </summary>
        /// <returns></returns>
        public AT.Plan GetBlankPlan(bool setDefaults)
        {
            var plan = new AT.Plan();

            if (setDefaults)
            {
                plan.BrandId = 1003;
                plan.DurationType = 5;
                plan.RenewalDurationType = 5;
                plan.FreeDurationType = 5;
            }

            return plan;
        }

        public string CompleteCreatePlan(AT.Plan plan)
        {
            Plan planToInsert = MapAdminToolPlanToPurchasePlan(plan);

            int planID = Constants.NULL_INT;

            TransactionOptions options = new TransactionOptions();

            options.Timeout = new TimeSpan(0, 0, 20);

            // This transaction is probably useless since the db writes are going through Purchase service which is in 1.1.
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, options))
            {
                planID = PlanSA.Instance.InsertPlan(plan.BrandId, planToInsert);
                CreatePlanOnUPSCatalog(plan.BrandId, planToInsert, planID);
            }

            return string.Format("Plan creation successful - PlanID: {0}", planID);
        }

        private void CreatePlanOnUPSCatalog(int brandID, Plan plan, int planID)
        {
            try
            {
                var brand = BrandConfigSA.Instance.GetBrandByID(brandID);

                //var connector = new Matchnet.Web.Applications.Admin.SubscriptionAdmin.UnifiedPaymentSystem.CatalogServiceConnector();

                if (CatalogServiceWebAdapter.GetProxyInstanceForBedrock().IsPackageInCatalog(planID))
                {
                    throw new Exception("This PlanID, " + planID.ToString() +
                                        " already exists on UPS Catalog system. Look for descrepancies between Bedrock and UPS Catalog.");
                }

                #region Create a new package (plan)

                // Package Type is NOT a mask value on the UPS side.
                PackageType packageType = PackageType.None;

                #region Map Plan type to UPS Package Type 2/2/10

                if ((plan.PlanTypeMask & PlanType.Regular) == PlanType.Regular)
                {
                    packageType = PackageType.Basic;
                }

                if ((plan.PlanTypeMask & PlanType.PremiumPlan) == PlanType.PremiumPlan)
                {
                    packageType = PackageType.Bundled;
                }

                if ((plan.PlanTypeMask & PlanType.ALaCarte) == PlanType.ALaCarte)
                {
                    packageType = PackageType.ALaCarte;
                }

                if ((plan.PlanTypeMask & PlanType.OneTimeOnly) == PlanType.OneTimeOnly)
                {
                    packageType = PackageType.OneOff;
                }

                if ((plan.PlanTypeMask & PlanType.Discount) == PlanType.Discount)
                {
                    packageType = PackageType.Discount;
                }

                // Color Code Case. ALC with stand alone no renewal option.
                if (((plan.PlanTypeMask & PlanType.OneTimeOnly) == PlanType.OneTimeOnly)
                    && ((plan.PlanTypeMask & PlanType.ALaCarte) == PlanType.ALaCarte))
                {
                    packageType = PackageType.OneOff;
                }

                if ((plan.PlanTypeMask & PlanType.FreeTrialWelcome) == PlanType.FreeTrialWelcome)
                {
                    packageType = PackageType.FreeTrial;
                }

                #endregion

                CatalogServiceWebAdapter.GetProxyInstanceForBedrock().CreatePackage(
                    planID,
                    brand.Site.Name + " - " + plan.InitialCost + " " + plan.InitialDuration + " " +
                    plan.InitialDurationType,
                    brand.Site.SiteID,
                    1,
                    DateTime.Now, // TBD
                    DateTime.MinValue,
                    plan.CurrencyType.ToString(),
                    packageType);

                #endregion

                #region Create new items inside the new package (plan services)

                foreach (PlanService planService in plan.PlanServices)
                {
                    int packageItem = (int)planService.PlanSvcDefinition;

                    if (planService.PlanSvcDefinition == PlanServiceDefinition.AllAccess)
                    {
                        packageItem = 1020;
                    }
                    else if (planService.PlanSvcDefinition == PlanServiceDefinition.AllAccessEmail)
                    {
                        packageItem = 1021;
                    }

                    CatalogServiceWebAdapter.GetProxyInstanceForBedrock().AddItemToPackage(
                        packageItem,
                        (int)plan.InitialDurationType,
                        plan.InitialDuration,
                        planService.BasePrice,
                        (int) plan.CurrencyType,
                        (int) plan.RenewDurationType,
                        plan.RenewDuration,
                        planService.RenewalPrice,
                        (int) plan.CurrencyType,
                        planID,
                        ((plan.PlanTypeMask & PlanType.Installment) == PlanType.Installment) ? true : false,
                        planService.DisburseCount,
                        planService.DisburseDuration,
                        planService.DisburseDurationTypeID,
                        (int) plan.FreeTrialDurationType,
                        plan.FreeTrialDuration);
                }

                #endregion

                //connector.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred creating a new package/item(s) on UPS Catalog system for" +
                                    "plan ID, " + planID.ToString() +
                                    " Please sync this plan records from Bedrock to UPS manually. Ensure that UPS Catalog system is functional before running this tool again.",
                                    ex);
            }
        }

        private Plan MapAdminToolPlanToPurchasePlan(AT.Plan plan)
        {
            var p = new Plan();
            p.CurrencyType =
                (Matchnet.Purchase.ValueObjects.CurrencyType)
                BrandConfigSA.Instance.GetBrandByID(plan.BrandId).Site.CurrencyID;
            p.InitialCost = plan.InitialCost ?? 0;
            p.InitialDuration = plan.Duration ?? 0;
            p.InitialDurationType = (Matchnet.DurationType) plan.DurationType;

            p.RenewCost = plan.RenewalCost ?? 0;
            p.RenewDuration = plan.RenewalDuration ?? 0;
            p.RenewDurationType = plan.RenewalCost == null
                                      ? Matchnet.DurationType.None
                                      : (Matchnet.DurationType) plan.RenewalDurationType;

            p.FreeTrialDuration = plan.FreeDuration ?? Constants.NULL_INT;
            p.FreeTrialDurationType = plan.FreeDuration == null
                                          ? Matchnet.DurationType.None
                                          : (Matchnet.DurationType) plan.FreeDurationType;

            p.PaymentTypeMask = plan.IsCheckPlan ? PaymentType.Check | PaymentType.CreditCard : PaymentType.CreditCard;
            p.PlanTypeMask = (PlanType) (plan.PlanType | plan.AdditiveMask);
            p.RenewAttempts = 3;
            p.StartDate = DateTime.Now;

            // deal with the PlanService collection
            if (p.PlanServices == null)
            {
                p.PlanServices = new PlanServiceCollection();
            }
            else
            {
                p.PlanServices.Clear();
            }

            switch (plan.PlanType)
            {
                case (int) PlanType.PremiumPlan:
                    p.PremiumTypeMask = (PremiumType) plan.PremiumPlanTypeMask;
                    foreach (var pb in plan.PremiumPlansPriceBreakdowns)
                    {
                        if (pb.InitialCost == null)
                            continue;

                        // AllAccessEmail types requires some special handling
                        if (pb.PremiumPlanType == (int) PremiumType.AllAccessEmail)
                        {
                            var ps = new PlanService(PlanServiceDefinition.AllAccessEmail, pb.InitialCost ?? 0,
                                                     pb.RenewalCost ?? 0);
                            ps.DisburseCount = pb.Count ?? 0;
                            if (ps.DisburseCount <= 0)
                                ps.DisburseCount = Constants.NULL_INT;

                            ps.DisburseDuration = 0;
                            ps.DisburseDurationTypeID = (int) Matchnet.DurationType.None;
                            if (pb.Count != null && pb.Count > 0 && pb.IsDisburseCount)
                            {
                                ps.DisburseDuration = 1;
                                ps.DisburseDurationTypeID = plan.DurationType;
                            }

                            p.PlanServices.Add(ps);
                        }
                        else
                        {
                            p.PlanServices.Add(
                                new PlanService(
                                    GeneralHelper.MapPremiumTypeToPlanServiceDefinition((PremiumType) pb.PremiumPlanType),
                                    pb.InitialCost ?? 0, pb.RenewalCost ?? 0));
                        }
                    }
                    break;
                case (int) PlanType.ALaCarte:
                    p.PremiumTypeMask = (PremiumType) plan.ALaCartePlanType;
                    p.PlanServices.Add(
                        new PlanService(
                            GeneralHelper.MapPremiumTypeToPlanServiceDefinition((PremiumType) plan.ALaCartePlanType),
                            plan.InitialCost ?? 0,
                            plan.RenewalCost ?? 0));
                    break;
                default:
                    p.PlanServices.Add(new PlanService(PlanServiceDefinition.Basic_Subscription, plan.InitialCost ?? 0,
                                                       plan.RenewalCost ?? 0));
                    p.PremiumTypeMask = PremiumType.None;
                    break;
            }

            return p;
        }

        public bool Validate(AT.Plan plan, System.Web.Mvc.ModelStateDictionary modelState)
        {
            if (plan.PlanType == 1024)
            {
                // a la carte validation
                if (plan.ALaCartePlanType == 0)
                    modelState.AddModelError("ALaCartePlanType", "A La Carte plan type must be selected");
            }
            else if (plan.PlanType == 256)
            {
                // premium plan type
                if (plan.PremiumPlanTypeMask == 0)
                {
                    modelState.AddModelError("PremiumPlanTypeMask", "At least one premium plan type must be selected");
                }

                // if disburse type was selected, make sure the count field was filled out
                if ((plan.PremiumPlanTypeMask & (int) PremiumType.AllAccessEmail) == (int) PremiumType.AllAccessEmail)
                {
                    var q = (from pb in plan.PremiumPlansPriceBreakdowns
                             where pb.PremiumPlanType == (int) PremiumType.AllAccessEmail
                             select pb).ToList();
                    if (q.Count() > 0)
                    {
                        if (q.First().IsDisburseCount && (q.First().Count == null || q.First().Count == 0))
                        {
                            modelState.AddModelError("PremiumPlansPriceBreakdowns",
                                                     "Count must be filled out if one of the premium types is a disburse type");
                        }
                    }
                }

                // combine price test
                decimal breakdownInitCost = 0;
                decimal breakdownRenewalCost = 0;
                decimal initCost = 0;
                decimal renewalCost = 0;

                foreach (var pb in plan.PremiumPlansPriceBreakdowns)
                {
                    if (pb.InitialCost != null)
                        breakdownInitCost += (decimal) pb.InitialCost;

                    if (pb.RenewalCost != null)
                        breakdownRenewalCost += (decimal) pb.RenewalCost;
                }

                if (plan.InitialCost != null)
                    initCost = (decimal) plan.InitialCost;

                if (plan.RenewalCost != null)
                    renewalCost = (decimal) plan.RenewalCost;

                if (initCost != breakdownInitCost || renewalCost != breakdownRenewalCost)
                    modelState.AddModelError("InitialCost",
                                             "Cost total(s) from the breakdown of costs do not match the plan cost(s)");
            }

            return modelState.IsValid;
        }

        public void GetPlanList(PlanDisplayList plansDisplay)
        {
            if (plansDisplay.PlansToDisplays == null)
                plansDisplay.PlansToDisplays = new List<PlanDisplay>();

            var planCollection = PlanAdminSA.Instance.GetPlans(plansDisplay.BrandId);

            if (planCollection == null)
                return;

            var plans = (from Plan plan in planCollection
                         orderby plan.PlanID descending ,
                             plan.PlanResourcePaymentTypeID ,
                             plan.UpdateDate descending
                         select plan).ToList();

            foreach (var p in plans)
            {
                plansDisplay.PlansToDisplays.Add(ConvertPurchasePlanToPlanDisplay(p, plansDisplay.BrandId));
            }
        }

        public PlanDisplay ConvertPurchasePlanToPlanDisplay(Plan plan, int brandId)
        {
            var pDisplay = new PlanDisplay(plan);

            // incremental cost calculation
            //var languageId = GeneralHelper.GetLanguageIdFromBrandId(brandId);
            var formatString = "F2";
            if (plan.InitialDuration > 0)
            {
                var incCost = (plan.InitialCost/plan.InitialDuration);
                pDisplay.IncrementalPrice = incCost.ToString(formatString);
            }

            // free trial info
            if (((plan.PlanTypeMask & PlanType.AuthOnly) == PlanType.AuthOnly)
                && ((plan.PlanTypeMask & PlanType.FreeTrialWelcome) == PlanType.FreeTrialWelcome))
            {
                var package = CatalogServiceWebAdapter.GetProxyInstance().GetPackageDetails(plan.PlanID);

                if (package != null)
                {
                    if (package.PackageType == PackageType.FreeTrial)
                    {
                        pDisplay.FreeTrialDuration = package.Items[0].FreeTrialDuration;
                        pDisplay.FreeTrialDurationType = package.Items[0].FreeTrialDurationType;
                    }
                }
            }

            // various costs
            foreach (PlanService planService in plan.PlanServices)
            {
                switch (planService.PlanSvcDefinition)
                {
                    case PlanServiceDefinition.Basic_Subscription:
                        pDisplay.BasicCost = planService.BasePrice.ToString(formatString) + " / " +
                                             planService.RenewalPrice.ToString(formatString);
                        break;
                    case PlanServiceDefinition.SpotLight:
                        pDisplay.SpotLightCost = planService.BasePrice.ToString(formatString) + " / " +
                                                 planService.RenewalPrice.ToString(formatString);
                        break;
                    case PlanServiceDefinition.Profile_Highlighting:
                        pDisplay.HighlightCost = planService.BasePrice.ToString(formatString) + " / " +
                                                 planService.RenewalPrice.ToString(formatString);
                        break;
                    case PlanServiceDefinition.JMeter:
                        pDisplay.JMeterCost = planService.BasePrice.ToString(formatString) + " / " +
                                              planService.RenewalPrice.ToString(formatString);
                        break;
                    case PlanServiceDefinition.AllAccess:
                        pDisplay.AllAccessCost = planService.BasePrice.ToString(formatString) + " / " +
                                                 planService.RenewalPrice.ToString(formatString);
                        break;
                    case PlanServiceDefinition.AllAccessEmail:
                        pDisplay.AllAcessEmailCost = planService.BasePrice.ToString(formatString) + " / " +
                                                     planService.RenewalPrice.ToString(formatString);
                        break;
                    case PlanServiceDefinition.ReadReceipt:
                        pDisplay.ReadReceipts = planService.BasePrice.ToString(formatString) + " / " +
                                                     planService.RenewalPrice.ToString(formatString);
                        break;
                }
            }

            return pDisplay;
        }


        public PlanDisplay GetPlan(int planID, int brandID)
        {

            var thePlan = PlanSA.Instance.GetPlan(planID, brandID);

            if (thePlan != null)
                return ConvertPurchasePlanToPlanDisplay(thePlan, brandID);
            else
                return null;
        }
    }
}
