﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models
{
    public class Enums
    {
        public enum Operations
        {
            none = 0,
            Searching = 100,
            MemberPageView = 200,
            PasswordView = 201,
            PasswordEdit =202,
            PasswordSend = 203,
            EmailEdit = 204, 
            GlobalProfileUpdate = 205,
            AutoRenewUpdate = 206,
            BuySubscription = 207,
            BuyAlaCarte = 208,
            AdminAdjust = 209,
            ChangePaymentType = 210,
            ViewEmailActivity = 211,
            UpdateFraudStatus = 212,
            ProfileInformationSave = 213,
            BillingInformationView = 214,
            ColorCodeReset = 215,
            ColorAnalysisPurchase = 216,
            ILAuthenticatedUpdate = 217,
            ILProfileHighlightUpdate = 218,
            ILJmeterUpdate = 219,
            AddAdminNote = 220,
            PromoSearch = 221,
            PhotoApprovalQueue = 222,
            CreatePlan = 223,
            CreatePromo = 224,
            PauseSubscription = 225,
            ResumeSubscription = 226,
            SupervisorPromo = 227,
            StealthLogin = 228,
            SODBAccess = 229,
            NewAgentRights = 230,
            CRXDuplicatesSupervisor = 231,
            FTATrainingToolSupervisor = 232,
            LifetimeMembershipUpdate = 233
        }

        [Flags]
        public enum AttributeOptionHideMask : int
        {
            HideSearch = 1,
            HideHotLists = 2,
            HideMembersOnline = 4,
            HidePhotos = 8
        }

        public enum BillingType
        {
            Order=1,
            GiftRedemption=2
        }


        public enum Status : int
        {
            Success = 2,
            Failed = 3,
            APIAdminMappingFailed=4
        }

        public enum ProfileStatus : int
        {
            Active = 1,
            SelfSuspend = 2,
            AdminSuspend = 3
        }

        public enum ForcedBlockingMask
        {
            NotBlocked = 0,
            BlockedAfterReg = 1,
            BlockedAfterEmailChange = 2,
            HideFromSearch = 8,
            HideFromMOL = 16
        }

        public enum EmailVerificationSteps
        {
            None = 0,
            AfterRegistration = 1,
            AfterEmailChange = 2

        }

        public enum Color
        {
            none = 0,
            red = 1,
            blue = 2,
            yellow = 3,
            white = 4
        }

        [Flags]
        public enum MatchMeterFlags : int
        {
            DEFAULT_NoChange = 0,
            EnableApp = 1,
            ShowBestMatches = 2,
            PayFor1On1 = 4,
            PayForBestMatches = 8,
            PremiumFor1On1 = 16,
            PremiumForBestMatches = 32
        }

        public enum Gender
        {
            Male = 1,
            Female = 2
        }

        public enum SeekingGender
        {
            Male = 4,
            Female = 8
        }

        public enum ApprovalCountReportSortType
        {
            MemberID=0,
            Email=1,
            TextCount=2,
            PhotoCount=3,
            QACount=4,
            FacebookLikesCount=5
        }

        public enum SortDirection
        {
            ASC=0,
            DESC=1
        }

        public enum Language
        {
            English=2,
            Hebrew = 262144,
            French = 8,
            Russian = 256
        }

        public enum AdminAdjustDirection
        {
            Add=1,
            Subtract=2
        }

        public enum HideMask
        {
            ShowInSearches=1,
            ShowActivityTrail=2,
            ShowOnline=4
        }

    }
}
