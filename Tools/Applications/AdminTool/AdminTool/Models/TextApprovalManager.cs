﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Text.RegularExpressions;
using System.Text;
using System.Web.Caching;
using System.Configuration;
using System.Web.Mvc;
using AdminTool.Models.Helpers;
using AdminTool.Models.ModelViews.Member;
using AdminTool.Models.SparkAPI.EndpointClients;
using Spark.CommonLibrary;
using AdminTool.Models.ModelViews;

using Matchnet;
using Matchnet.Lib;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Matchnet.Member.ServiceAdapters;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.Content.ServiceAdapters;
using Spark.CommonLibrary.Logging;
using MCA = Matchnet.Content.ValueObjects.AttributeMetadata;
using BrandConfig = Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.DifferenceEngine;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Purchase.ServiceAdapters;
using PurchaseVO = Matchnet.Purchase.ValueObjects;
using Matchnet.PremiumServiceSearch.ServiceAdapters;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using AdminTool.Models.ModelViews.Approval;
using AdminTool.Models.IPLocation;
using AdminTool.Models.SparkAPI;
using Spark.Common.OrderHistoryService;
using Spark.Common.Adapter;
using Spark.Common.PaymentProfileService;
using Matchnet.Content.ValueObjects.BrandConfig;


namespace AdminTool.Models
{

    public enum RegularApprovalType
    {
        QueuedApproval = 0,
        MemberApproval = 1
    }

    public enum QAApprovalType
    {
        QueuedApprovalByLanguage = 0,
        QueuedApprovalBySite = 1,
        QueuedApprovalByCommunity = 2,
        MemberApproval = 3,
        MemberProfileDuplicate = 4
    }

    public class TextApprovalManager : ManagerBase
    {
        private const int REFRESH_INTERVAL_MINUTES = 180;
        private const string FREETEXTAPPROVAL_SUSPECTWORDS_CACHE_KEY = "FREETEXTAPPROVAL_SUSPECTWORDS_CACHE_KEY";
        private const string FREETEXTAPPROVAL_BANNEDWORDS_CACHE_KEY = "FREETEXTAPPROVAL_BANNEDWORDS_CACHE_KEY";
        private const string FREETEXTAPPROVAL_CURRENT_QUEUEITEM = "FREETEXTAPPROVAL_CURRENT_QUEUEITEM";
        private const string CRXEnabled = "ENABLE_CRX_APPROVAL";
        private const string BOLDSPAN_BEGIN_TAG = "<b>";
        private const string BOLDSPAN_END_TAG = "</b>";
        private Logger _logger;


        public CommunityIDs CommunityID { get; private set; }
        public int MemberID { get; private set; }
        public int LanguageID { get; private set; }
        public TextType TextType { get; private set; }
        public Logger Logger
        {
            get
            {
                if(_logger == null)
                    _logger = new Logger(this.GetType());

                return _logger;
            }
        }

        public TextApprovalManager() { }

        public TextApprovalManager(int languageID, TextType textType)
        {
            LanguageID = languageID;
            TextType = textType;
        }

        public TextApprovalManager(TextType textType, int memberID, CommunityIDs communityID)
        {
            MemberID = memberID;
            TextType = textType;
            CommunityID = communityID;
        }

        public TextApprovalManager(int memberID, int languageID, CommunityIDs communityID)
        {
            MemberID = memberID;
            LanguageID = languageID;
            CommunityID = communityID;
        }


        public TextApprovalMemberModelView GetMemberApproval(SiteIDs siteID)
        {
            TextApprovalMemberModelView textApprovalModelView = new TextApprovalMemberModelView();
            MemberManager memberManager = new MemberManager(MemberID, (int)siteID, MemberLoadFlags.IngoreSACache);

            textApprovalModelView.MemberID = MemberID;
            textApprovalModelView.CommunityID = (int)CommunityID;
            textApprovalModelView.TextType = TextType;

            Member member = MemberHelper.GetMember(MemberID, MemberLoadFlags.None);
            //int brandID;
            //member.GetLastLogonDate((int)CommunityID, out brandID);
            //Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);
            //Fix for MPR-2727 - FTA shows text from most recent logged on site
            //GetBrand gives the default brand associated with the site.
            Brand brand = g.GetBrand((int) siteID);

            textApprovalModelView.LanguageID = brand.Site.LanguageID;
            textApprovalModelView.AttributesToApprove = GetApprovalAttributes(member, textApprovalModelView.LanguageID, brand, RegularApprovalType.MemberApproval, null);

            textApprovalModelView.Left = memberManager.GetMemberLeft();
            textApprovalModelView.TopNav = memberManager.GetTopNav();
            return textApprovalModelView;
        }


        public TextApprovalQAModelView GetQAApprovalByCommunity(CommunityIDs communityID)
        {
            TextApprovalQAModelView textApprovalQAModelView = PopulateQATextApproval(QAApprovalType.QueuedApprovalByCommunity, (int)communityID);
            textApprovalQAModelView.ApprovalType = QAApprovalType.QueuedApprovalByCommunity;
            return textApprovalQAModelView;
        }

        public TextApprovalQAModelView GetQAApprovalByLanguage(int languageID)
        {
            TextApprovalQAModelView textApprovalQAModelView = PopulateQATextApproval(QAApprovalType.QueuedApprovalByLanguage, languageID);

            textApprovalQAModelView.ApprovalType = QAApprovalType.QueuedApprovalByLanguage;
            return textApprovalQAModelView;
        }

        public TextApprovalQAModelView GetQAApprovalBySite(SiteIDs siteID)
        {
            TextApprovalQAModelView textApprovalQAModelView = PopulateQATextApproval(QAApprovalType.QueuedApprovalBySite, (int)siteID); 
            textApprovalQAModelView.ApprovalType = QAApprovalType.QueuedApprovalBySite;
            return textApprovalQAModelView;
        }

        public TextApprovalQAMemberModelView GetQAApprovalByMember(int memberID, SiteIDs siteID)
        {
            CommunityIDs communityID = GeneralHelper.GetCommunityIDForSiteID(siteID);
            Member member;
            Brand brand;
            BrandConfig.Site site = GeneralHelper.GetSiteByID((int)siteID);
            MemberHelper.GetMemberAndBrand(out member, out brand, memberID, (int)communityID);

            QueueItemText queueItem = new QueueItemText((int)communityID, (int)siteID, brand.BrandID, memberID, site.LanguageID, TextType.QA);

            TextApprovalQAMemberModelView textApprovalQAMemberModelView = GetQAMemberApproval(queueItem);

            return textApprovalQAMemberModelView;
        }

        private TextApprovalQAModelView PopulateQATextApproval(QAApprovalType approvalType, int ID)
        {
            TextApprovalQAModelView textApprovalQAModelView = new TextApprovalQAModelView();

            bool queueEmpty = false;
            bool foundValidItem = false;
            QueueItemText queueItem = null;

            while (!queueEmpty && !foundValidItem)
            {
                switch (approvalType)
                {
                    case QAApprovalType.QueuedApprovalByCommunity:
                        queueItem = DBApproveQueueSA.Instance.DequeueTextByCommunity(ID, TextType.QA);
                        break;
                    case QAApprovalType.QueuedApprovalBySite:
                        queueItem = DBApproveQueueSA.Instance.DequeueTextBySite(ID, TextType.QA);
                        break;
                    case QAApprovalType.QueuedApprovalByLanguage:
                        queueItem = DBApproveQueueSA.Instance.DequeueTextByLanguage(ID, TextType.QA);
                        break;
                }

                if (queueItem == null)
                {
                    queueEmpty = true;
                    queueItem = new QueueItemText();

                    switch (approvalType)
                    {
                        case QAApprovalType.QueuedApprovalByCommunity:
                            queueItem.CommunityID = ID;
                            break;
                        case QAApprovalType.QueuedApprovalBySite:
                            queueItem.SiteID = ID;
                            break;
                        case QAApprovalType.QueuedApprovalByLanguage:
                            queueItem.LanguageID = ID;
                            break;
                    }

                    textApprovalQAModelView = BuildBlankQAModelView(queueItem, approvalType);
                }
                else
                {
                    Member member = MemberHelper.GetMember(queueItem.MemberID, MemberLoadFlags.None);
                    if (!MemberHelper.IsMemberValidForApproval(member, queueItem.CommunityID))
                    {
                        DBApproveQueueSA.Instance.CompleteTextApproval(queueItem.MemberID, queueItem.CommunityID, queueItem.LanguageID, TextType.QA, ApprovalStatus.Completed, g.AdminID, (int)AdminAction.ApprovedFreeText);
                        new ApprovalHelper().PersistMemberMessage(g.AdminID, queueItem.MemberID, string.Format("Member was possible fraud or suspended: <a href='/Member/View/{1}/{0}' target='_blank'>{0}</a>", queueItem.MemberID.ToString(), queueItem.SiteID.ToString()), false);
                    }
                    else
                    {
                        List<ApprovalQuestion> approvalQuestions = GetApprovalQuestions(QuestionAnswerSA.Instance.GetMemberQuestions(queueItem.MemberID, queueItem.SiteID), approvalType);
                        if (approvalQuestions != null && approvalQuestions.Count > 0)
                        {
                            foundValidItem = true;
                            textApprovalQAModelView = BuildQAModelView(queueItem, approvalType, approvalQuestions);
                        }
                        else
                        {
                            DBApproveQueueSA.Instance.CompleteTextApproval(queueItem.MemberID, queueItem.CommunityID, queueItem.LanguageID, TextType.QA, ApprovalStatus.Completed, g.AdminID, (int)AdminAction.ApprovedFreeText);
                            new ApprovalHelper().PersistMemberMessage(g.AdminID, queueItem.MemberID, string.Format("Member had no questions to approve: <a href='/Member/View/{1}/{0}' target='_blank'>{0}</a>", queueItem.MemberID.ToString(), queueItem.SiteID.ToString()), false);
                        }
                    }
                }
            }

            return textApprovalQAModelView;
        }

        private TextApprovalQAModelView BuildQAModelView(QueueItemText queueItem, QAApprovalType approvalType, List<ApprovalQuestion> approvalQuestions)
        {
            TextApprovalQAModelView textApprovalQAModelView = new TextApprovalQAModelView();
            ApprovalLeft left;
            Member member = null;
            Brand brand = null;
            bool hasContent = true;
            textApprovalQAModelView.MemberID = queueItem.MemberID;
            textApprovalQAModelView.LanguageID = queueItem.LanguageID;
            textApprovalQAModelView.CommunityID = queueItem.CommunityID;
            textApprovalQAModelView.SiteID = queueItem.SiteID;
            textApprovalQAModelView.MemberTextApprovalID = queueItem.MemberTextApprovalID;
            textApprovalQAModelView.TextType = TextType.QA;
            textApprovalQAModelView.ApprovalQuestions = approvalQuestions;

            MemberHelper.GetMemberAndBrand(out member, out brand, queueItem.MemberID, queueItem.CommunityID);

            if (approvalType == QAApprovalType.QueuedApprovalByLanguage)
            {
                left = GetApprovalLeft(member, brand, hasContent, QueueItemType.QuestionAnswer, approvalType, queueItem.LanguageID);
            }
            else
            {
                left = GetApprovalLeft(member, brand, hasContent, QueueItemType.QuestionAnswer, approvalType);
            }

            textApprovalQAModelView.Left = left;
            textApprovalQAModelView.TopNav = new SharedTopNav();

            return textApprovalQAModelView;
        }

        private TextApprovalQAModelView BuildBlankQAModelView(QueueItemText queueItem, QAApprovalType approvalType)
        {
            TextApprovalQAModelView textApprovalQAModelView = new TextApprovalQAModelView();

            ApprovalLeft left;
            Member member = null;
            BrandConfig.Brand brand = null;
            bool hasContent = false;

            if (approvalType == QAApprovalType.QueuedApprovalBySite)
            {
                brand = GeneralHelper.GetBrandForSite(queueItem.SiteID);
            }
            if (approvalType == QAApprovalType.QueuedApprovalByCommunity)
            {
                SiteIDs siteID = GeneralHelper.GetPrimarySiteForCommunity((CommunityIDs)queueItem.CommunityID);
                brand = GeneralHelper.GetBrandForSite((int)siteID);
            }

            if (approvalType == QAApprovalType.QueuedApprovalByLanguage)
            {
                left = GetApprovalLeft(member, brand, hasContent, QueueItemType.QuestionAnswer, approvalType, queueItem.LanguageID);
            }
            else
            {
                left = GetApprovalLeft(member, brand, hasContent, QueueItemType.QuestionAnswer, approvalType);
            }

            textApprovalQAModelView.Left = left;
            textApprovalQAModelView.TopNav = new SharedTopNav();

            return textApprovalQAModelView;
        }

        /// <summary>
        /// This method will return the next queue item or a specific queue item depending on the value of memberProfileDuplicateQueueId.
        /// </summary>
        /// <param name="languageId"></param>
        /// <param name="memberProfileDuplicateQueueId">Set to Constants.NULL_INT for next item in the queue or queue ID to retrieve a specific one.</param>
        /// <returns></returns>
        public MemberProfileDuplicateApprovalModelView GetMemberProfileDuplicate(int languageId, int memberProfileDuplicateQueueId, out bool crxTextNotFound)
        {
            crxTextNotFound = false;
            var modelView = new MemberProfileDuplicateApprovalModelView();
            modelView.TopNav = new SharedTopNav();
            modelView.QueueItem = memberProfileDuplicateQueueId == Constants.NULL_INT ? DBApproveQueueSA.Instance.DequeueMemberProfileDuplicateByLanguage(languageId) :
                DBApproveQueueSA.Instance.GetMemberProfileDuplicateQueueItem(memberProfileDuplicateQueueId);

            // since we are identifying duplicates, if less than 2 something is wrong.
            if (modelView.QueueItem == null || modelView.QueueItem.MemberProfileDuplicates.Count < 2)
            {
                modelView.Left = GetApprovalLeft(null, null, false, QueueItemType.MemberProfileDuplicate, QAApprovalType.MemberProfileDuplicate, languageId);
                return modelView;
            }

            // ordinal number 1 is the member we are looking at, and the rest are other members who have similar profile text field value
            // as the member of ordinal number 1.
            modelView.LanguageID = modelView.QueueItem.LanguageID;
            modelView.MemberID = (from MemberProfileDuplicate d in modelView.QueueItem.MemberProfileDuplicates
                                  where d.OrdinalNumber == 1
                                  select d.MemberId).SingleOrDefault();
            var member = MemberHelper.GetMember(modelView.MemberID, MemberLoadFlags.None);

            var test = MemberHelper.GetMemberSiteInfoList(member);

            #region Determine the brand based on the CRXResponse record for the hero/top member
            var crxResponseId = (from MemberProfileDuplicate d in modelView.QueueItem.MemberProfileDuplicates
                                 where d.OrdinalNumber == 1
                                 select d.CRXResponseId).SingleOrDefault();
            
            var crxText = DBApproveQueueSA.Instance.LocateCRXText(crxResponseId);

            // if CRXText isn't found, log this information and return right away
            if(crxText == null)
            {
                DBApproveQueueSA.Instance.DeleteMemberProfileDuplicateQueueItem(modelView.QueueItem.QueueItemMemberProfileDuplicateID);
                Logger.Error(string.Format("CRX text with MemberID: {1} CRXResponseID: {0} was not found in our system", crxResponseId, modelView.MemberID));
                crxTextNotFound = true;
                return modelView;
            }

            int brandId = Constants.NULL_INT;
            if(crxText.BrandId != Constants.NULL_INT)
            {
                brandId = crxText.BrandId;
            }
            else if(crxText.SiteId != Constants.NULL_INT)
            {
                brandId = g.GetBrand(crxText.SiteId) == null ? Constants.NULL_INT : g.GetBrand(crxText.SiteId).BrandID;
            }
            else
            {
                if (crxText.CommunityId == Constants.NULL_INT)
                    throw new Exception(
                        "GetMemberProfileDuplicate() error. Community could not be determined by the data in the database. CRXResponseID: " + crxResponseId.ToString());

                member.GetLastLogonDate(crxText.CommunityId, out brandId);
            }

            var brand = BrandConfigSA.Instance.GetBrandByID(brandId);
            #endregion
            
            // for each of the MemberProfileDuplicate record, let's figure out the siteID to use when building the member view page link
            // we already know the hero/first member since we went through an elaborate way to determining it
            bool isFirst = true;
            foreach (MemberProfileDuplicate d in modelView.QueueItem.MemberProfileDuplicates)
            {
                if (isFirst)
                {
                    modelView.MemberProfileDuplicateExtended = new List<MemberProfileDuplicateExtended>();
                }

                var dMember = MemberHelper.GetMember(d.MemberId, MemberLoadFlags.None);
                var dSiteId = isFirst ? brand.Site.SiteID : MemberHelper.GetMemberLastLogonSite(dMember);

                var globalStatusMask = dMember.GetAttributeInt(brand, "GlobalStatusMask");
                var alreadySuspended = (globalStatusMask & WebConstants.GLOBALSTATUSMASK_ADMIN_SUSPEND) == WebConstants.GLOBALSTATUSMASK_ADMIN_SUSPEND;

                modelView.MemberProfileDuplicateExtended.Add(new MemberProfileDuplicateExtended()
                                                                 {
                                                                     LastLogonSiteID = dSiteId,
                                                                     MemberProfileDuplicate = d,
                                                                     TextboxHeight = GeneralHelper.GetTextboxHeight(d.FieldValue),
                                                                     AlreadySuspended = alreadySuspended
                                                                 });

                isFirst = false;
            }

            modelView.SiteName = brand.Site.Name;
            modelView.Left = GetApprovalLeft(member, brand, false, QueueItemType.MemberProfileDuplicate, QAApprovalType.MemberProfileDuplicate, languageId);

            return modelView;

        }

        public TextApprovalModelView GetTextApprovalByLanguage()
        {
            TextApprovalModelView textApprovalModelView = new TextApprovalModelView();
            textApprovalModelView.IsEnglish = LanguageID == 2;
            bool foundValidMember = false;
            bool queueEmpty = false;

            QueueItemText queueItem = null;
            Member member = null;
            Brand brand = null;
            int brandID;
            bool hasContent = false;
            int languageID = Constants.NULL_INT;

            while (!foundValidMember && !queueEmpty)
            {
                queueItem = DBApproveQueueSA.Instance.DequeueTextByLanguage(LanguageID, TextType.Regular);
                if (queueItem != null)
                {
                    member = MemberHelper.GetMember(queueItem.MemberID, MemberLoadFlags.IngoreSACache);

                    if (!MemberHelper.IsMemberValidForApproval(member, queueItem.CommunityID))
                    {
                        //this member doesn't have valid attributes for the community on the queue item
                        DBApproveQueueSA.Instance.CompleteTextApproval(member.MemberID, queueItem.CommunityID, queueItem.LanguageID, TextType.Regular, ApprovalStatus.CompletedPossibleFraud, g.AdminID, (int)AdminAction.ApprovedFreeText);
                        SiteIDs siteID = GeneralHelper.GetPrimarySiteForCommunity((CommunityIDs)queueItem.CommunityID);
                        new ApprovalHelper().PersistMemberMessage(g.AdminID, member.MemberID, string.Format("Member was possible fraud or suspended: <a href='/Member/View/{1}/{0}' target='_blank'>{0}</a>", member.MemberID.ToString(), siteID.ToString("d")), false);
                        continue;
                    }

                    member.GetLastLogonDate(queueItem.CommunityID, out brandID);
                    brand = BrandConfigSA.Instance.GetBrandByID(brandID);

                    textApprovalModelView.MemberID = queueItem.MemberID;
                    textApprovalModelView.LanguageID = queueItem.LanguageID;
                    textApprovalModelView.CommunityID = queueItem.CommunityID;
                    textApprovalModelView.MemberTextApprovalID = queueItem.MemberTextApprovalID;
                    textApprovalModelView.TextType = TextType.Regular;

                    textApprovalModelView.ExpandedUsrInfo = MemberHelper.GetExpandedUserInfo(member, brand);
                                        
                    textApprovalModelView.AttributesToApprove = GetApprovalAttributes(member, queueItem.LanguageID, brand, RegularApprovalType.QueuedApproval, queueItem.MemberTextApprovalID);

                    if (textApprovalModelView.AttributesToApprove == null || textApprovalModelView.AttributesToApprove.Count < 1)
                    {
                        //this member has no attributes to approve, so go ahead and complete the queue item
                        DBApproveQueueSA.Instance.CompleteTextApproval(member.MemberID, queueItem.CommunityID, queueItem.LanguageID, TextType.Regular, ApprovalStatus.Completed, g.AdminID, (int)AdminAction.ApprovedFreeText);
                        new ApprovalHelper().PersistMemberMessage(g.AdminID, member.MemberID, string.Format("Member had no attributes to approve: <a href='/Member/View/{1}/{0}' target='_blank'>{0}</a>", member.MemberID.ToString(), brand.Site.SiteID.ToString()), false);
                    }
                    else
                    {
                        languageID = queueItem.LanguageID;
                        hasContent = true;
                        foundValidMember = true;
                    }
                }
                else
                {
                    textApprovalModelView.AttributesToApprove = new List<TextApprovalAttribute>();
                    queueEmpty = true;
                }
            }

            textApprovalModelView.Left = GetApprovalLeft(member, brand, hasContent, QueueItemType.Text, QAApprovalType.QueuedApprovalByLanguage, languageID); ;
            textApprovalModelView.TopNav = new SharedTopNav();
            return textApprovalModelView;
        }

        public TextApprovalModelView GenerateModelView( int memberId, int communityId,int languageId,int? memberTextApprovalId)
        {
            var textApprovalModelView = new TextApprovalModelView { TopNav = new SharedTopNav()};
            if (memberTextApprovalId.HasValue)
            {
                textApprovalModelView.IsEnglish = languageId == 2;

                textApprovalModelView.MemberID = memberId;
                textApprovalModelView.LanguageID = languageId;
                textApprovalModelView.CommunityID = communityId;
                textApprovalModelView.MemberTextApprovalID = memberTextApprovalId.Value;
                textApprovalModelView.TextType = TextType.Regular;

                Member member = MemberHelper.GetMember(memberId, MemberLoadFlags.None);
                int branId;
                member.GetLastLogonDate(communityId, out branId);
                Brand brand = BrandConfigSA.Instance.GetBrandByID(branId);
                textApprovalModelView.ExpandedUsrInfo = MemberHelper.GetExpandedUserInfo(member, brand);

                textApprovalModelView.AttributesToApprove = GetApprovalAttributes(member, languageId, brand,
                                                                                  RegularApprovalType.QueuedApproval,
                                                                                  memberTextApprovalId);

                textApprovalModelView.Left = GetApprovalLeft(member, brand, true, QueueItemType.Text,
                                                             QAApprovalType.QueuedApprovalByLanguage, languageId);
            }
            return textApprovalModelView;
        }

        private TextApprovalQAMemberModelView GetQAMemberApproval(QueueItemText queueItem)
        {
            TextApprovalQAMemberModelView textApprovalQAMemberModelView = new TextApprovalQAMemberModelView();
            MemberManager manager = new MemberManager(queueItem.MemberID, queueItem.SiteID, MemberLoadFlags.None);

            textApprovalQAMemberModelView.MemberID = queueItem.MemberID;
            textApprovalQAMemberModelView.LanguageID = queueItem.LanguageID;
            textApprovalQAMemberModelView.CommunityID = queueItem.CommunityID;
            textApprovalQAMemberModelView.SiteID = queueItem.SiteID;
            textApprovalQAMemberModelView.TextType = TextType.QA;
            textApprovalQAMemberModelView.ApprovalQuestions = GetApprovalQuestions(QuestionAnswerSA.Instance.GetMemberQuestions(queueItem.MemberID, queueItem.SiteID), QAApprovalType.MemberApproval);

            textApprovalQAMemberModelView.Left = manager.GetMemberLeft();
            textApprovalQAMemberModelView.TopNav = manager.GetTopNav();

            return textApprovalQAMemberModelView;
        }

        private ApprovalLeft GetApprovalLeft(Member member, Brand brand, bool hasContent, QueueItemType itemType, QAApprovalType approvalType)
        {
            return GetApprovalLeft(member, brand, hasContent, itemType, approvalType, Constants.NULL_INT);
        }

        private ApprovalLeft GetApprovalLeft(Member member, Brand brand, bool hasContent, QueueItemType itemType, QAApprovalType approvalType, int languageID)
        {
            ApprovalLeft left = new ApprovalLeft();
            ApprovalHelper approvalHelper = new ApprovalHelper();
            left.IsTextApproval = true;
            left.HasContent = hasContent;

            if (hasContent)
            {
                SiteIDs siteID = GeneralHelper.GetPrimarySiteForCommunity((CommunityIDs)brand.Site.Community.CommunityID);

                left.MemberID = member.MemberID;
                left.Gender = MemberHelper.GetGender(member, brand);
                left.Age = MemberHelper.GetAge(member.GetAttributeDate(brand, "Birthdate"));
                left.SiteID = siteID;
                left.FraudResult = MemberHelper.GetFraudDetailedResponse(member.MemberID, brand.Site.SiteID, 3);
                left.PhotoURL = MemberPhotoHelper.GetDefaultPhotoThumbnailDisplayURL(member, brand);
            }

            ApprovalReportsManager manager = new ApprovalReportsManager();

            switch (approvalType)
            {
                case QAApprovalType.QueuedApprovalByCommunity:
                    left.QueueCount = manager.GetTotalTextQueueCount(itemType, (CommunityIDs)brand.Site.Community.CommunityID);
                    break;
                case QAApprovalType.QueuedApprovalByLanguage:
                case QAApprovalType.MemberProfileDuplicate:
                    left.QueueCount = manager.GetTotalTextQueueCount(itemType, languageID);
                    break;
                case QAApprovalType.QueuedApprovalBySite:
                    left.QueueCount = manager.GetTotalTextQueueCount(QueueItemType.QuestionAnswer, (SiteIDs)brand.Site.SiteID);
                    break;
            }

            left.ItemsProcessed = manager.GetSessionQueueItemsProcessedCount(itemType);
            left.MemberMessages = approvalHelper.GetCurrentMemberMessages(g.AdminID);
            approvalHelper.ClearMemberMessages(g.AdminID);
            return left;
        }


        public void CompleteTextApproval(List<TextApprovalAttributeUpdate> attributeUpdates, int? memberTextApprovalId)
        {
            ApprovalHelper approvalHelper = new ApprovalHelper();
            Member member = null;
            Brand brand = null;
            bool updateUsername = false;
            bool addToCRXReviewQueue = false;
            MemberHelper.GetMemberAndBrand(out member, out brand, MemberID, (int)CommunityID);

            int actionMask = (int)AdminAction.ApprovedFreeText;

            actionMask = UpdateMemberAttributes(attributeUpdates, member, brand, actionMask, memberTextApprovalId, ref updateUsername, ref addToCRXReviewQueue);
            LogIndividualTextAttributeUpdates(attributeUpdates, member.MemberID, g.AdminID, memberTextApprovalId);
            SaveMemberAndCompleteTextApproval(member, actionMask, CommunityID, LanguageID, false, string.Empty, updateUsername, addToCRXReviewQueue);

            approvalHelper.PersistMemberMessage(g.AdminID, member.MemberID, string.Format("Successfully approved member <a href='/Member/View/{1}/{0}' target='_blank'>{0}</a>", member.MemberID.ToString(), brand.Site.SiteID.ToString()), false);
            new ApprovalReportsManager().AddSessionQueueItemProcessedCount();
            new ReviewManager().AddAdminForFTAReview(g.AdminID);
        }

        public void CompleteTextApprovalMemberApproval(List<TextApprovalAttributeUpdate> attributeUpdates,
                                                       int languageID, int communityID)
        {
            ApprovalHelper approvalHelper = new ApprovalHelper();
            Member member = null;
            Brand brand = null;
            bool updateUsername = false;
            bool addToCRXReviewQueue = false;
            MemberHelper.GetMemberAndBrand(out member, out brand, MemberID, (int) CommunityID);

            int actionMask = (int) AdminAction.ApprovedFreeText;
            int? memberTextApprovalID = null;

            // to handle some of race conditions logic, we need this
            var textAttributesCurrentlyInQueue = DBApproveQueueSA.Instance.GetTextAttributesForMember(MemberID,
                                                                                                     communityID,
                                                                                                     languageID,
                                                                                                     TextType.Regular);

            actionMask = UpdateMemberAttributesMemberApproval(attributeUpdates, member, brand, actionMask, 
                                                ref updateUsername, textAttributesCurrentlyInQueue);
            LogIndividualTextAttributeUpdatesMemberApproval(attributeUpdates, member.MemberID, g.AdminID,
                                                            ref memberTextApprovalID, textAttributesCurrentlyInQueue);
            SaveMemberAndCompleteTextApprovalMemberApproval(member, actionMask, CommunityID, LanguageID, false,
                                                            updateUsername, memberTextApprovalID);

            approvalHelper.PersistMemberMessage(g.AdminID, member.MemberID,
                                                string.Format(
                                                    "Successfully approved member <a href='/Member/View/{1}/{0}' target='_blank'>{0}</a>",
                                                    member.MemberID.ToString(), brand.Site.SiteID.ToString()), false);
            new ApprovalReportsManager().AddSessionQueueItemProcessedCount();
            new ReviewManager().AddAdminForFTAReview(g.AdminID);
        }

        public void CompleteTextApprovalWithSuspend(List<TextApprovalAttributeUpdate> attributeUpdates, AdminActionReasonID adminActionReasonID, int? memberTextApprovalId)
        {
            ApprovalHelper approvalHelper = new ApprovalHelper();
            Member member = null;
            Brand brand = null;
            bool updateUsername = false;
            bool addToCRXReviewQueue = false;
            MemberHelper.GetMemberAndBrand(out member, out brand, MemberID, (int)CommunityID);

            int actionMask = (int)AdminAction.AdminSuspendMember;

            actionMask = UpdateMemberAttributes(attributeUpdates, member, brand, actionMask, memberTextApprovalId, ref updateUsername, ref addToCRXReviewQueue);
            bool stoppedRenewals = MemberHelper.SuspendMember(member, brand, adminActionReasonID, actionMask, CommunityID, LanguageID,g.AdminID);
            LogIndividualTextAttributeUpdates(attributeUpdates, member.MemberID, g.AdminID, memberTextApprovalId);
            SaveMemberAndCompleteTextApproval(member, actionMask, CommunityID, LanguageID, true, adminActionReasonID.ToString(), updateUsername, addToCRXReviewQueue);

            approvalHelper.PersistMemberMessage(g.AdminID, member.MemberID, string.Format("Suspended member <a href='/Member/View/{1}/{0}' target='_blank'>{0}</a>", member.MemberID.ToString(), brand.Site.SiteID.ToString()), true);
            if (stoppedRenewals)
            {
                approvalHelper.PersistMemberMessage(g.AdminID, member.MemberID, "Renewals have been terminated.", true);
            }
            else
            {
                approvalHelper.PersistMemberMessage(g.AdminID, member.MemberID, "No active renewal found.", true);
            }
            new ApprovalReportsManager().AddSessionQueueItemProcessedCount();
            new ReviewManager().AddAdminForFTAReview(g.AdminID);
        }

        public void CompleteQATextApproval(List<ApprovalQuestionUpdate> questionUpdates, int memberID,
            CommunityIDs communityID, SiteIDs siteID, int languageID)
        {
            ApprovalHelper approvalHelper = new ApprovalHelper();
            Member member = null;
            Brand brand = null;
            MemberHelper.GetMemberAndBrand(out member, out brand, memberID, (int)communityID);

            int actionMask = (int)AdminAction.ApprovedQuestionAnswer;
            actionMask = UpdateQuestions(questionUpdates, memberID, brand, false, actionMask);

            DBApproveQueueSA.Instance.CompleteTextApproval(memberID, (int)communityID, languageID, TextType.QA, ApprovalStatus.Completed, g.AdminID, actionMask);
            AdminSA.Instance.AdminActionLogInsert(memberID, (int)communityID, actionMask, g.AdminID, languageID);

            approvalHelper.PersistMemberMessage(g.AdminID, member.MemberID, string.Format("Successfully approved member <a href='/Member/View/{1}/{0}' target='_blank'>{0}</a>", member.MemberID.ToString(), siteID.ToString("d")), false);
            new ApprovalReportsManager().AddSessionQueueItemProcessedCount();
        }

        public void CompleteQATextApprovalWithSuspend(List<ApprovalQuestionUpdate> questionUpdates, int memberID,
            CommunityIDs communityID, SiteIDs siteID, int languageID, AdminActionReasonID adminActionReasonID)
        {
            ApprovalHelper approvalHelper = new ApprovalHelper();
            Member member = null;
            Brand brand = null;
            MemberHelper.GetMemberAndBrand(out member, out brand, memberID, (int)communityID);

            int actionMask = (int)AdminAction.AdminSuspendMember;

            actionMask = UpdateQuestions(questionUpdates, memberID, brand, true, actionMask);
            bool stoppedRenewals = MemberHelper.SuspendMember(member, brand, adminActionReasonID, actionMask, communityID, languageID,g.AdminID);
            MemberSA.Instance.SaveMember(member);

            DBApproveQueueSA.Instance.CompleteTextApproval(memberID, (int)communityID, languageID, TextType.QA, ApprovalStatus.Completed, g.AdminID, actionMask);

            approvalHelper.PersistMemberMessage(g.AdminID, member.MemberID, string.Format("Suspended member <a href='/Member/View/{1}/{0}' target='_blank'>{0}</a>", member.MemberID.ToString(), siteID.ToString("d")), true);
            if (stoppedRenewals)
            {
                approvalHelper.PersistMemberMessage(g.AdminID, member.MemberID, "Renewals have been terminated.", true);
            }
            else
            {
                approvalHelper.PersistMemberMessage(g.AdminID, member.MemberID, "No active renewal found.", true);
            }
            new ApprovalReportsManager().AddSessionQueueItemProcessedCount();
        }

        
        private void LogIndividualTextAttributeUpdates(List<TextApprovalAttributeUpdate> attributeUpdates, int memberID, int adminMemberID, int? memberTextApprovalId)
        {
            foreach (TextApprovalAttributeUpdate update in attributeUpdates)
            {
                string originalText = ApprovalHelper.cleanUpTags(update.OriginalValue);
                string updatedText = ApprovalHelper.cleanUpTags(update.UpdatedValue);
                bool isEdit = (originalText != updatedText);

                if (update.AttributedUnapproved || isEdit)
                {
                    //If SaveAttribute is true, CRX Response and admin response are same, text will go live. 
                    //If SaveAttribute is false, CRX Response and admin response are not same, approval item will go into CRX Review Queue. 
                    ApprovalStatus reviewStatus = (update.SaveAttribute)
                                                     ? ApprovalStatus.Completed
                                                     : ApprovalStatus.Pending;
                    if (update.TextAcceptable)
                    {
                        DBApproveQueueSA.Instance.SaveIndividualTextAttributeApprovalExpanded(memberID,
                            update.AttributeGroupID, update.LanguageID, (int)IndividualTextApprovalStatusMask.Approve, adminMemberID, originalText.Trim(), false,
                            update.IP, update.ProfileRegionID, update.GenderMask, update.MaritalStatus, update.FirstName, update.LastName, update.Email,
                            update.OccupationDescription, update.EducationLevel, update.Religion, update.Ethnicity, update.DaysSinceFirstSubscription,
                            update.BillingPhoneNumber, update.SubscriberStatus, update.RegistrationDate, memberTextApprovalId, updatedText.Trim(), reviewStatus);
                    }
                    else
                    {
                        DBApproveQueueSA.Instance.SaveIndividualTextAttributeApprovalExpanded(memberID,
                            update.AttributeGroupID, update.LanguageID, update.StatusMask, adminMemberID, originalText.Trim(), isEdit,
                            update.IP, update.ProfileRegionID, update.GenderMask, update.MaritalStatus, update.FirstName, update.LastName, update.Email,
                            update.OccupationDescription, update.EducationLevel, update.Religion, update.Ethnicity, update.DaysSinceFirstSubscription,
                            update.BillingPhoneNumber, update.SubscriberStatus, update.RegistrationDate, memberTextApprovalId, updatedText.Trim(), reviewStatus);

                        //Both old content and updated content will stored in the same record.
                        //No need to add a new row for updated content
                        //if (isEdit)
                        //{
                        //    //log edited, approved text
                        //    DBApproveQueueSA.Instance.SaveIndividualTextAttributeApprovalExpanded(memberID,
                        //        update.AttributeGroupID, update.LanguageID, (int)IndividualTextApprovalStatusMask.Approve, adminMemberID, updatedText, true,
                        //        update.IP, update.ProfileRegionID, update.GenderMask, update.MaritalStatus, update.FirstName, update.LastName, update.Email,
                        //        update.OccupationDescription, update.EducationLevel, update.Religion, update.Ethnicity, update.DaysSinceFirstSubscription, update.BillingPhoneNumber, update.SubscriberStatus, update.RegistrationDate);
                        //}
                    }
                }
            }
        }

        private void LogIndividualTextAttributeUpdatesMemberApproval(List<TextApprovalAttributeUpdate> attributeUpdates,
                                                                     int memberID, int adminMemberID,
                                                                     ref int? refMemberTextApprovalID,
                                                                     List<QueueItemText> textAttributesCurrentlyInQueue)
        {
            foreach (TextApprovalAttributeUpdate update in attributeUpdates)
            {
                string originalText = ApprovalHelper.cleanUpTags(update.OriginalValue);
                string updatedText = ApprovalHelper.cleanUpTags(update.UpdatedValue);
                bool isEdit = (originalText != updatedText);
                int? memberTextApprovalID = null;

                if (update.AttributedUnapproved || isEdit)
                {
                    //If SaveAttribute is true, CRX Response and admin response are same, text will go live. 
                    //If SaveAttribute is false, CRX Response and admin response are not same, approval item will go into CRX Review Queue. 
                    ApprovalStatus reviewStatus = (update.SaveAttribute)
                                                     ? ApprovalStatus.Completed
                                                     : ApprovalStatus.Pending;

                    /* For Member Approval, we want to include the MemberTextApprovalID only if this attribute is an attribute that's currently in the
                     FTA approval queue. For the existing FTA that Admin edited, we don't want to pass this in because that would wrongly represent the
                     relationship.*/
                    var textAttributeInQueue = textAttributesCurrentlyInQueue == null
                                                   ? null
                                                   : (from a in textAttributesCurrentlyInQueue
                                                      where a.MemberAttributeGroupID == update.AttributeGroupID
                                                      select a).SingleOrDefault();

                    memberTextApprovalID = textAttributeInQueue == null
                                               ? (int?)null
                                               : textAttributeInQueue.MemberTextApprovalID;

                    if(!refMemberTextApprovalID.HasValue)
                    {
                        refMemberTextApprovalID = memberTextApprovalID.HasValue
                                                      ? memberTextApprovalID.Value
                                                      : (int?) null;
                    }

                    /* Regarding the very last parameter "deleteMovedRows": In the queue case, MemberTextApprovalAttribute and CRXResponse table rows are
                     deleted in one shot at the end of the queue item completion cycle. Although the way it deletes those rows are wrong, the chances of that
                     causing problems are relatively low. In the Member Approval case, this way of deleting rows causes major problems because Member Approval
                     ignores the 15-minute wait time rule; it ends up deleting many rows that it shouldn't delete. To solve this problem, I am deleting these
                     rows individually as I moved each row to IndividualTextAttributeApproval table. For this reason, I createdt his overloaded version
                     of the method.*/

                    if (update.TextAcceptable)
                    {
                        DBApproveQueueSA.Instance.SaveIndividualTextAttributeApprovalExpanded(memberID,
                            update.AttributeGroupID, update.LanguageID, (int)IndividualTextApprovalStatusMask.Approve, adminMemberID, originalText.Trim(), false,
                            update.IP, update.ProfileRegionID, update.GenderMask, update.MaritalStatus, update.FirstName, update.LastName, update.Email,
                            update.OccupationDescription, update.EducationLevel, update.Religion, update.Ethnicity, update.DaysSinceFirstSubscription,
                            update.BillingPhoneNumber, update.SubscriberStatus, update.RegistrationDate, memberTextApprovalID, updatedText.Trim(), reviewStatus, true);
                    }
                    else
                    {
                        DBApproveQueueSA.Instance.SaveIndividualTextAttributeApprovalExpanded(memberID,
                            update.AttributeGroupID, update.LanguageID, update.StatusMask, adminMemberID, originalText.Trim(), isEdit,
                            update.IP, update.ProfileRegionID, update.GenderMask, update.MaritalStatus, update.FirstName, update.LastName, update.Email,
                            update.OccupationDescription, update.EducationLevel, update.Religion, update.Ethnicity, update.DaysSinceFirstSubscription,
                            update.BillingPhoneNumber, update.SubscriberStatus, update.RegistrationDate, memberTextApprovalID, updatedText.Trim(), reviewStatus, true);
                    }
                }
            }
        }

        private int UpdateQuestions(List<ApprovalQuestionUpdate> questionUpdates, int memberID, Brand brand, bool suspend, int actionMask)
        {
            MemberQuestionList memberQuestionList = QuestionAnswerSA.Instance.GetMemberQuestions(memberID, brand.Site.SiteID);

            foreach (ApprovalQuestionUpdate questionUpdate in questionUpdates)
            {
                Answer answer = getAnswerInQuestionsByAnswerID(memberQuestionList, questionUpdate.AnswerID);
                string newValue = ApprovalHelper.cleanUpTags(questionUpdate.Answer);

                if (answer.AnswerStatus != QuestionAnswerEnums.AnswerStatusType.Approved || answer.AnswerValue.Trim() != newValue.Trim())
                {
                    if (questionUpdate.Delete || questionUpdate.Answer.Trim() == string.Empty || suspend)
                    {
                        if (questionUpdate.Delete || questionUpdate.Answer.Trim() == string.Empty) { actionMask = actionMask | (int)AdminAction.DeletedQuestionAnswer; }
                        answer.AnswerValue = string.Empty;
                        answer.AnswerStatus = QuestionAnswerEnums.AnswerStatusType.Rejected;
                    }
                    else
                    {

                        if (newValue.Length > 4000)
                        {
                            newValue = newValue.Substring(0, 4000);
                        }

                        answer.AnswerValue = newValue;
                        answer.AnswerValuePending = string.Empty;
                        answer.AnswerStatus = QuestionAnswerEnums.AnswerStatusType.Approved;
                    }

                    answer.AnswerValuePending = string.Empty;
                    answer.AdminMemberID = g.AdminID;
                    new ApiQuestionAnswerJson(ApiClient.GetInstance()).AdminUpdate(answer,brand.BrandID);
                }
            }

            return actionMask;
        }

        private Answer getAnswerInQuestionsByAnswerID(MemberQuestionList memberQuestionList, int answerID)
        {
            foreach (Question question in memberQuestionList.Questions)
            {
                foreach (Answer answer in question.Answers)
                {
                    if (answer.AnswerID == answerID)
                    {
                        return answer;
                    }
                }
            }

            throw new Exception("Answer not found in question collection. Answer ID: " + answerID.ToString());
        }

        private int UpdateMemberAttributes(List<TextApprovalAttributeUpdate> attributeUpdates, Member member,
                                           Brand brand, int actionMask, int? memberTextApprovalId,
                                           ref bool updateUsername, ref bool addToCRXReviewQueue)
        {
            //Get actual brand based on languageId where the attribute is updated. 
            if (brand.Site.LanguageID != LanguageID)
            {
                var updatedBrand = GetLastUpdatedBrand(member);
                if (updatedBrand != null)
                    brand = updatedBrand;
            }

            //Get CRX Response for member attributes
            var crxResponse = new List<QueueItemText>();
            //Check if CRX is enabled.
            bool isCrxEnabled = false;
            bool.TryParse(RuntimeSettings.Instance.GetSettingFromSingleton(CRXEnabled), out isCrxEnabled);
            if (isCrxEnabled && memberTextApprovalId.HasValue)
                crxResponse = DBApproveQueueSA.Instance.GetApprovalTextAttributesCRXResponse(memberTextApprovalId.Value);

            foreach (TextApprovalAttributeUpdate attributeUpdate in attributeUpdates)
            {
                 //Compare CRX Response with Agent's approval decision. If matches, text can go live. Otherwise, this should go to CRX second review Queue.
                bool isDifferent = CompareCRXResponse(crxResponse, attributeUpdate);
                if (isDifferent)
                {
                    attributeUpdate.SaveAttribute = false;
                    addToCRXReviewQueue = true;
                }
                else
                {
                    attributeUpdate.SaveAttribute = true;
                    string attributeValue = null;

                    if (attributeUpdate.UpdatedValue.Trim() == string.Empty)
                    {
                        if (attributeUpdate.Name.ToLower() == "username")
                        {
                            attributeValue = MemberID.ToString();
                        }
                        actionMask = actionMask | (int) AdminAction.DeletedFreeTextAttribute;
                    }
                     else
                    {
                        attributeValue = ApprovalHelper.cleanUpTags(attributeUpdate.UpdatedValue);

                        if (attributeUpdate.Name.ToLower() == "username" && attributeValue.Trim() == string.Empty)
                        {
                            attributeValue = MemberID.ToString();
                        }
                    }

                    //update username 
                    if (attributeUpdate.Name.ToLower() == "username")
                        updateUsername = attributeValue != ApprovalHelper.cleanUpTags(attributeUpdate.OriginalValue);

                    SetAttributeTextTrim(member, brand, attributeUpdate.Name, attributeValue, TextStatusType.Human);
                }

            }

            return actionMask;
        }

        private int UpdateMemberAttributesMemberApproval(List<TextApprovalAttributeUpdate> attributeUpdates,
                                                         Member member,
                                                         Brand brand, int actionMask, ref bool updateUsername,
                                                         List<QueueItemText>
                                                             textAttributesCurrentlyInQueue)
        {
            //Get actual brand based on languageId where the attribute is updated. 
            if (brand.Site.LanguageID != LanguageID)
            {
                var updatedBrand = GetLastUpdatedBrand(member);
                if (updatedBrand != null)
                    brand = updatedBrand;
            }

            foreach (TextApprovalAttributeUpdate attributeUpdate in attributeUpdates)
            {

                // We have to make sure a FTA field wasn't added to the FTA queue after the admin loaded up the FTA Member Approval page.
                // If we don't safeguard against this situation, it will end up writing the attribute's approval status as approved when
                // it should be in pending since when the page was loaded that attribute was marked as approved already.
                if (!attributeUpdate.AttributedUnapproved && textAttributesCurrentlyInQueue != null)
                {
                    var textAttribute = (from a in textAttributesCurrentlyInQueue
                                         where a.MemberAttributeGroupID == attributeUpdate.AttributeGroupID
                                         select a).SingleOrDefault();

                    if(textAttribute != null)
                    {
                        // This SaveAttribute seems to only matter if the attributeUpdate.AttributedUnapproved = true, but
                        // I am setting it anyways.
                        attributeUpdate.SaveAttribute = false;
                        continue;
                    }
                }

                attributeUpdate.SaveAttribute = true;
                string attributeValue = null;

                if (attributeUpdate.UpdatedValue.Trim() == string.Empty)
                {
                    if (attributeUpdate.Name.ToLower() == "username")
                    {
                        attributeValue = MemberID.ToString();
                    }
                    actionMask = actionMask | (int) AdminAction.DeletedFreeTextAttribute;
                }
                else
                {
                    attributeValue = ApprovalHelper.cleanUpTags(attributeUpdate.UpdatedValue);

                    if (attributeUpdate.Name.ToLower() == "username" && attributeValue.Trim() == string.Empty)
                    {
                        attributeValue = MemberID.ToString();
                    }
                }

                //update username 
                if (attributeUpdate.Name.ToLower() == "username")
                    updateUsername = attributeValue != ApprovalHelper.cleanUpTags(attributeUpdate.OriginalValue);

                SetAttributeTextTrim(member, brand, attributeUpdate.Name, attributeValue, TextStatusType.Human);
            }

            return actionMask;
        }

        private void SaveMemberAndCompleteTextApproval(Member member, int actionMask, CommunityIDs communityID, int languageID, bool suspend, string reason, bool updateUsername, bool addToCRXReviewQueue)
        {
            if(updateUsername)
                MemberSA.Instance.SaveMember(member, (int)CommunityID); //Use overloaded SaveMember method to update username
            else
            MemberSA.Instance.SaveMember(member);

            ApprovalStatus status = ApprovalStatus.Completed;
            //Check if CRX is enabled.
            bool isCrxEnabled = false;
            bool.TryParse(RuntimeSettings.Instance.GetSettingFromSingleton(CRXEnabled), out isCrxEnabled);
            if (isCrxEnabled && addToCRXReviewQueue)
            {
                //If admin response conflicts with CRX Response, item will be added to CRX Second Review Queue
                status = ApprovalStatus.Pending;
            }
            DBApproveQueueSA.Instance.CompleteTextApproval(member.MemberID, (int)communityID, languageID, TextType.Regular, status, g.AdminID, actionMask);

            if (!suspend)
            {
                AdminSA.Instance.AdminActionLogInsert(member.MemberID, (int)communityID, actionMask, g.AdminID, languageID);
            }
        }

        private void SaveMemberAndCompleteTextApprovalMemberApproval(Member member, int actionMask,
                                                                     CommunityIDs communityID, int languageID,
                                                                     bool suspend, bool updateUsername, int? memberTextApprovalID)
        {
            if (updateUsername)
                MemberSA.Instance.SaveMember(member, (int) CommunityID);
                    //Use overloaded SaveMember method to update username
            else
                MemberSA.Instance.SaveMember(member);

            // Only call the Complete method if there was a queue item at the time where the admin look at this member's FTA fields.
            // If there was no queue item at the time of the admin's viewing, no need to do anything to the DB queue
            if (memberTextApprovalID.HasValue)
            {
                DBApproveQueueSA.Instance.CompleteTextApprovalMemberApproval(member.MemberID, memberTextApprovalID.Value,
                                                                             ApprovalStatus.Completed,
                                                                             g.AdminID, actionMask);
            }

            if (!suspend)
            {
                AdminSA.Instance.AdminActionLogInsert(member.MemberID, (int) communityID, actionMask, g.AdminID,
                                                      languageID);
            }
        }

        private void SetLastUpdateDateForAllSites(Member member)
        {
            int[] siteIDList = member.GetSiteIDList();
            if (siteIDList.Length > 0)
            {
                foreach (int siteID in siteIDList)
                {
                    Matchnet.Content.ValueObjects.BrandConfig.Brand brand = GeneralHelper.GetBrandForSite(siteID);
                    member.SetAttributeDate(brand, "LastUpdated", DateTime.Now);
                }
            }
        }

        private void SetAttributeTextTrim(Member member, Brand brand, string attributeName, string newValue, TextStatusType textStatType)
        {
            string finalValue = newValue;

            if (newValue != null)
                finalValue = newValue.Trim();

            if (attributeName.ToLower() == "username")
            {
                member.SetUsername(newValue, brand, textStatType);
            }
            else
            {
                member.SetAttributeText(brand.Site.Community.CommunityID,
                    brand.Site.SiteID,
                    brand.BrandID,
                    LanguageID,
                    attributeName,
                    finalValue,
                    textStatType);
            }
        }

        
        private List<ApprovalQuestion> GetApprovalQuestions(MemberQuestionList memberQuestionList, QAApprovalType approvalType)
        {
            List<ApprovalQuestion> approvalQuestions = new List<ApprovalQuestion>();
            int index = 0;

            foreach (Question question in memberQuestionList.Questions)
            {
                if (approvalType == QAApprovalType.MemberApproval || question.Answers[0].AnswerStatus == QuestionAnswerEnums.AnswerStatusType.Pending)
                {
                    index++;
                    ApprovalQuestion approvalQuestion = new ApprovalQuestion();
                    Answer answer = question.Answers[0];

                    approvalQuestion.Question = question.Text;
                    approvalQuestion.QuestionID = question.QuestionID;
                    approvalQuestion.AnswerID = answer.AnswerID;

                    if (answer.AnswerValuePending.Trim() == String.Empty)
                    {
                        approvalQuestion.Answer = highlightBannedSuspectWords(answer.AnswerValue);
                    }
                    else
                    {
                        approvalQuestion.Answer = highlightBannedSuspectWords(answer.AnswerValuePending);
                    }

                    approvalQuestion.StatusIconSource = GetStatusIconSource(answer.AnswerStatus);
                    approvalQuestion.TextboxHeight = GeneralHelper.GetTextboxHeight(approvalQuestion.Answer);
                    approvalQuestion.Number = index;

                    approvalQuestions.Add(approvalQuestion);
                }
            }

            return approvalQuestions;

        }

        private List<TextApprovalAttribute> GetApprovalAttributes(Member member, int languageID, Brand brand, RegularApprovalType approvalType, int? memberTextApprovalId)
        {
            //Get CRX Response for member attributes
            var crxResponse = new List<QueueItemText>();
            //Check if CRX is enabled.
            bool isCrxEnabled = false;
            bool.TryParse(RuntimeSettings.Instance.GetSettingFromSingleton(CRXEnabled), out isCrxEnabled);
            if (isCrxEnabled && memberTextApprovalId.HasValue)
                crxResponse = DBApproveQueueSA.Instance.GetApprovalTextAttributesCRXResponse(memberTextApprovalId.Value);

            List<TextApprovalAttribute> approvalAttributes = new List<TextApprovalAttribute>();

            MCA.AttributeCollection attributeCollection =
                    AttributeMetadataSA.Instance.GetAttributeCollections().GetCollection
                    ("FREETEXT", brand.Site.Community.CommunityID, Constants.NULL_INT, Constants.NULL_INT);

            Regex regex = new Regex("<(.|\n)+?>", RegexOptions.IgnoreCase);
            int number = 0;

            foreach (MCA.AttributeCollectionAttribute aca in attributeCollection)
            {
                TextApprovalAttribute approvalAttribute = new TextApprovalAttribute();

                TextStatusType textStatusType = TextStatusType.None;
                MCA.Attribute attribute = AttributeMetadataSA.Instance.GetAttributes().GetAttribute(aca.AttributeID);
                approvalAttribute.AttributeName = attribute.Name;

                if (attribute.DataType == MCA.DataType.Text)
                {
                    string attributeValue = member.GetAttributeText(brand.Site.Community.CommunityID,
                            brand.Site.SiteID,
                            brand.BrandID,
                            languageID,
                            attribute.Name,
                            String.Empty,
                            out textStatusType);

                    if (attributeValue != null && ((approvalType == RegularApprovalType.QueuedApproval && (textStatusType == TextStatusType.None || textStatusType == TextStatusType.Pending)) ||
                        (approvalType == RegularApprovalType.MemberApproval)))
                    {
                        if (attributeValue.Trim().Length > 0 || approvalType == RegularApprovalType.MemberApproval)
                        {
                            number++;
                            MCA.AttributeGroup attributeGroup = member.getAttributeGroup(brand.Site.Community.CommunityID,
                                        brand.Site.SiteID,
                                        brand.BrandID,
                                        attribute);

                            //Setting the languageID from brand always sets it to the member lastlogon brand languageId.
                            //This is different from actual languageID when the member has accounts in different sites on same community.
                            //languageID = brand.Site.LanguageID;


                            StringBuilder attributeCheckValue = new StringBuilder(regex.Replace(attributeValue, ""));                            
                            attributeCheckValue.Replace("<", "&lt;");
                            attributeCheckValue.Replace(">", "&gt;");

                            //ensure all line breaks are saved as windows "\r\n" so it shows up correctly in admin tool
                            string templinebreak = "{{br}}";
                            attributeCheckValue.Replace("\r\n", templinebreak);
                            attributeCheckValue.Replace("\n", templinebreak);
                            attributeCheckValue.Replace("\r", templinebreak);
                            attributeCheckValue.Replace(templinebreak, "\r\n");

                            approvalAttribute.MaxLength = attributeGroup.Length;
                            approvalAttribute.Value = attributeCheckValue.ToString();
                            approvalAttribute.Status = textStatusType;
                            approvalAttribute.StatusIconSource = GetStatusIconSource(textStatusType);
                            approvalAttribute.AttributeGroupID = attributeGroup.ID;
                            approvalAttribute.LanguageID = languageID;

                            if (attribute.OldValueContainerID != Constants.NULL_INT)
                            {
                                string oldAttributeValue = member.GetAttributeText(brand.Site.Community.CommunityID,
                                                                brand.Site.SiteID,
                                                                brand.BrandID,
                                                                languageID,
                                                                attribute.OldValueContainerID,
                                                                String.Empty);

                                if (oldAttributeValue != string.Empty)
                                    approvalAttribute.OldValue = oldAttributeValue;
                            }

                            if (!string.IsNullOrEmpty(approvalAttribute.OldValue))
                            {
                                // boldChanges will call the highLightBannedWords and highLightSuspectWords internally
                                approvalAttribute.DisplayValue = boldChanges(approvalAttribute.OldValue, approvalAttribute.Value);
                            }
                            else
                            {
                                approvalAttribute.DisplayValue = highLightBannedWords(approvalAttribute.Value);
                                approvalAttribute.DisplayValue = highLightSuspectWords(approvalAttribute.DisplayValue);

                                // If we don't have an old value, and the TextStatus is not one of the approved enums,
                                // we know this is the first time the user entered this FreeText value
                                if (textStatusType == TextStatusType.None || textStatusType == TextStatusType.Pending)
                                    approvalAttribute.DisplayValue = BOLDSPAN_BEGIN_TAG + approvalAttribute.DisplayValue + BOLDSPAN_END_TAG;
                            }

                            //TL: These newline manipulation is to support line breaks for profile essays (as well as any text on our site that support line breaks)
                            //need to create <br /> tags to display correctly in rich text control and also to maintain correct newline
                            if (approvalAttribute.DisplayValue.Contains(Environment.NewLine))
                            {
                                approvalAttribute.DisplayValue = approvalAttribute.DisplayValue.Replace(Environment.NewLine, "<br />");
                            }

                            approvalAttribute.TextboxHeight = GeneralHelper.GetTextboxHeight(approvalAttribute.Value);
                            approvalAttribute.Number = number;

                            bool? crxApprovalResponse = GetCRXResponse(crxResponse,
                                                                           approvalAttribute.AttributeGroupID);
                            //This means there is no CRXResponse available for this item. 
                            if(isCrxEnabled && !crxApprovalResponse.HasValue)
                                System.Diagnostics.EventLog.WriteEntry("Admintool",string.Format("No CRXResponse found. AttributeGroupID : {0} " +
                                                                                                 "MemberTextApprovalID : {1} ", approvalAttribute.AttributeGroupID,
                                                                                                 (memberTextApprovalId.HasValue ? memberTextApprovalId.Value : 0)),System.Diagnostics.EventLogEntryType.Warning);
                            approvalAttribute.ShowNoCRXResponseMessage = isCrxEnabled && !crxApprovalResponse.HasValue;
                            approvalAttribute.CRXApproval = crxApprovalResponse;

                            approvalAttributes.Add(approvalAttribute);
                        }
                    }
                }
            }

            return approvalAttributes;
        }

        private Regex getSuspectWordsPattern()
        {
            object suspectWordsPattern = HttpContext.Current.Cache.Get(FREETEXTAPPROVAL_SUSPECTWORDS_CACHE_KEY);

            if (suspectWordsPattern != null)
            {
                return (Regex)suspectWordsPattern;
            }

            Matchnet.Lib.Util.FreeTextApproval approval = Matchnet.Lib.Util.FreeTextApproval.GetInstance();
            approval.Load(HttpContext.Current.Server.MapPath("/Content/XML/FreeTextApproval.xml"));
            suspectWordsPattern = approval.GetRegex("suspectWords");

            HttpContext.Current.Cache.Insert(FREETEXTAPPROVAL_SUSPECTWORDS_CACHE_KEY,
                suspectWordsPattern,
                null,
                DateTime.Now.AddMinutes(REFRESH_INTERVAL_MINUTES),
                TimeSpan.Zero,
                CacheItemPriority.Normal,
                null);

            System.Diagnostics.Trace.WriteLine("TA: SuspectWords list reloaded into cache.");

            return (Regex)suspectWordsPattern;
        }
        private Regex getBannedWordsPattern()
        {
            object bannedWordsPattern = HttpContext.Current.Cache.Get(FREETEXTAPPROVAL_BANNEDWORDS_CACHE_KEY);

            if (bannedWordsPattern != null)
            {
                return (Regex)bannedWordsPattern;
            }

            Matchnet.Lib.Util.FreeTextApproval approval = Matchnet.Lib.Util.FreeTextApproval.GetInstance();
            approval.Load(HttpContext.Current.Server.MapPath("/Content/XML/FreeTextApproval.xml"));
            bannedWordsPattern = approval.GetRegex("bannedWords");

            HttpContext.Current.Cache.Insert(FREETEXTAPPROVAL_BANNEDWORDS_CACHE_KEY,
                bannedWordsPattern,
                null,
                DateTime.Now.AddMinutes(REFRESH_INTERVAL_MINUTES),
                TimeSpan.Zero,
                CacheItemPriority.Normal,
                null);

            System.Diagnostics.Trace.WriteLine("TA: BannedWords list reloaded into cache.");

            return (Regex)bannedWordsPattern;
        }

        public string highlightBannedSuspectWords(string textValue)
        {
            string returnText = string.Empty;

            returnText = highLightBannedWords(textValue);
            returnText = highLightSuspectWords(returnText);

            return returnText;
        }

        private String highLightSuspectWords(String textValue)
        {
            String highlight = String.Empty;
            highlight = textValue;

            Regex suspectWordsPattern = getSuspectWordsPattern();
            highlight = suspectWordsPattern.Replace(highlight, "<span style=background-color:#FFFF00;>$&</span>");

            return highlight;
        }

        private String highLightBannedWords(String textValue)
        {
            String highlight = String.Empty;
            highlight = textValue;

            Regex bannedWordsPattern = getBannedWordsPattern();
            highlight = bannedWordsPattern.Replace(highlight, "<span style=background-color:#FF0033;>$&</span>");

            return highlight;
        }

        private String cleanBannedWords(String textValue)
        {
            String cleanedValue = String.Empty;

            Regex bannedWordsPattern = getBannedWordsPattern();

            cleanedValue = bannedWordsPattern.Replace(textValue, String.Empty);

            return cleanedValue;
        }

        /// <summary>
        /// Takes 2 string values and bolds the edited or new words.  DiffList_StringData implementation uses spaces
        /// to separate out the words and allows for word-based comparisons. 
        /// </summary>
        /// <param name="origText"></param>
        /// <param name="editedText"></param>
        /// <returns></returns>
        public String boldChanges(String origText, String editedText)
        {
            DiffList_StringData sData = new DiffList_StringData(origText);
            DiffList_StringData dData = new DiffList_StringData(editedText);

            double time = 0;
            DiffEngine de = new DiffEngine();
            time = de.ProcessDiff(sData, dData, DiffEngineLevel.SlowPerfect);

            return addHtmlBoldSpanTags(dData, de.DiffReport());
        }

        /// <summary>
        /// Looks through the DiffResultSpan list and outputs the html required to bold the new/edited words.  Due to the
        /// fact that the bold tags get caught with banned/suspect words' regular expressions, we must call the highLight
        /// methods here before we add any bold tags.
        /// </summary>
        /// <param name="destData"></param>
        /// <param name="diffResultSpanList"></param>
        /// <returns></returns>
        private String addHtmlBoldSpanTags(DiffList_StringData destData, ArrayList diffResultSpanList)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder chunk = new StringBuilder();
            int i;

            foreach (DiffResultSpan drs in diffResultSpanList)
            {
                chunk.Remove(0, chunk.Length);
                switch (drs.Status)
                {
                    case DiffResultSpanStatus.DeleteSource: // we don't care about deleted data
                        break;
                    case DiffResultSpanStatus.NoChange: // nothing changed, no html needed
                        for (i = 0; i < drs.Length; i++)
                        {
                            chunk.Append(destData.GetByIndex(drs.DestIndex + i));
                        }
                        sb.Append(HighlightBannedSuspectWords(chunk.ToString()));
                        break;
                    case DiffResultSpanStatus.AddDestination: // added words, bold this
                        sb.Append(BOLDSPAN_BEGIN_TAG);
                        for (i = 0; i < drs.Length; i++)
                        {
                            chunk.Append(destData.GetByIndex(drs.DestIndex + i));
                        }
                        sb.Append(HighlightBannedSuspectWords(chunk.ToString()));
                        sb.Append(BOLDSPAN_END_TAG);
                        break;
                    case DiffResultSpanStatus.Replace: // changed words, bold this
                        sb.Append(BOLDSPAN_BEGIN_TAG);
                        for (i = 0; i < drs.Length; i++)
                        {
                            chunk.Append(destData.GetByIndex(drs.DestIndex + i));
                        }
                        sb.Append(HighlightBannedSuspectWords(chunk.ToString()));
                        sb.Append(BOLDSPAN_END_TAG);
                        break;
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Highlights the banned then the suspect words.
        /// </summary>
        /// <param name="textValue"></param>
        /// <returns></returns>
        private string HighlightBannedSuspectWords(string textValue)
        {
            string returnText = string.Empty;

            returnText = highLightBannedWords(textValue);
            returnText = highLightSuspectWords(returnText);

            return returnText;
        }

        // Moved to GeneralHelper class
        //private int GetTextboxHeight(string text)
        //{
        //    int height = 0;
        //    int lineBreaks = text.Split(new string[] { "\r\n" }, StringSplitOptions.None).Length - 1;

        //    if (text.Length <= 100)
        //    {
        //        height = 55 + (lineBreaks * 40);
        //    }

        //    if (text.Length >= 100)
        //    {
        //        int rows = (int)(text.Length / 100);
        //        height = (20 * rows) + 60 + (lineBreaks * 25);
        //    }

        //    return height;
        //}

        private string GetStatusIconSource(TextStatusType status)
        {
            string imageSource = string.Empty;

            switch (status)
            {
                case TextStatusType.None:
                    imageSource = "notapproved.gif";
                    break;
                case TextStatusType.Pending:
                    imageSource = "notapproved.gif";
                    break;
                case TextStatusType.Auto:
                    imageSource = "autoapproved.gif";
                    break;
                case TextStatusType.Human:
                    imageSource = "humanapproved.gif";
                    break;
            }

            return imageSource;
        }

        private string GetStatusIconSource(QuestionAnswerEnums.AnswerStatusType answerStatus)
        {
            string imageSource = string.Empty;

            switch (answerStatus)
            {
                case QuestionAnswerEnums.AnswerStatusType.None:
                    imageSource = "notapproved.gif";
                    break;
                case QuestionAnswerEnums.AnswerStatusType.Pending:
                    imageSource = "notapproved.gif";
                    break;
                case QuestionAnswerEnums.AnswerStatusType.Approved:
                    imageSource = "humanapproved.gif";
                    break;
            }

            return imageSource;
        }

        private Brand GetLastUpdatedBrand(Member member)
        {
            //Get actual brand based on languageId where the attribute is updated. 
            //Brand info is retrieved based on 'lastlogondate' which is not same as actual brand where the member updated the attribute.
            Brand brand = null;
            //get all sites from member's community
            int[] siteIds = member.GetSiteIDList();
            foreach (var siteId in siteIds)
            {
                Brand updatedbrand = g.GetBrand((int)siteId);
                if (updatedbrand.Site.LanguageID == LanguageID)
                {
                    brand = updatedbrand;
                    break;
                }
            }
            return brand;
        }

        /// <summary>
        /// Returns true if CRXResponse and the admin's response are different. If no CRXResponse record is found, the two responses are
        /// considered the same thus returning false.
        /// </summary>
        /// <param name="crxResponse"></param>
        /// <param name="processedAttribute"></param>
        /// <returns></returns>
        private bool CompareCRXResponse(IEnumerable<QueueItemText> crxResponse, TextApprovalAttributeUpdate processedAttribute)
        {
            foreach (var queueItemText in crxResponse)
            {
                if (queueItemText.MemberAttributeGroupID == processedAttribute.AttributeGroupID && queueItemText.CRXApproval != processedAttribute.TextAcceptable)
                    return true;
            }
            return false;
        }

        private bool? GetCRXResponse(IEnumerable<QueueItemText> crxResponse, int attributeGroupId)
        {
            foreach (var queueItemText in crxResponse)
            {
                if (queueItemText.MemberAttributeGroupID == attributeGroupId)
                    return queueItemText.CRXApproval;
            }
            return null; 
        }

        public string CompleteMemberProfileDuplicate(FormCollection collection, int languageId, int memberProfileDuplicateQueueId, out bool invalidQueueItem)
        {
            
            invalidQueueItem = false;

            var queueItem = DBApproveQueueSA.Instance.GetMemberProfileDuplicateQueueItem(memberProfileDuplicateQueueId);
            if(queueItem == null)
            {
                invalidQueueItem = true;
                return "Invalid queue item - No action was taken as a result. Please repeat your changes.";
            }

            var queueHistoryItem = new MemberProfileDuplicateHistory()
                                       {
                                           AdminId = g.AdminID,
                                           LanguageId = queueItem.LanguageID,
                                           MemberProfileDuplicateHistoryId = Constants.NULL_INT,
                                           MemberProfileDuplicateHistoryEntries =
                                               new List<MemberProfileDuplicateHistoryEntry>(),
                                           QueueInsertDate = queueItem.InsertDate
                                       };

            // loop through and prepare to call SaveMemberProfileDuplicateHistory with MemberProfileDuplicateHistory object
            // also detect the members to suspend during the loop
            var membersToSuspend = new ArrayList();
            foreach(var dup in queueItem.MemberProfileDuplicates)
            {
                // determine if IsMatch
                var matching = collection["Matching" + dup.CRXResponseId.ToString()] ?? string.Empty;
                var isMatching = matching.ToLower().Contains("true");
                
                // determine if we need to suspend this member
                var suspend = collection["Suspend" + dup.MemberId] ?? string.Empty;
                var isSuspend = suspend.ToLower().Contains("true");

                var alreadySuspended = collection["AlreadySuspended" + dup.MemberId] ?? string.Empty;
                var isAlreadySuspended = alreadySuspended.ToLower() == "true";

                if(isSuspend && !isAlreadySuspended)
                {
                    if(!isMatching)
                    {
                        return "Match checkbox must be checked before a member can be suspended.";
                    }

                    var suspendReason = collection["SuspendReason" + dup.MemberId] ?? string.Empty;
                    if (suspendReason == string.Empty || suspendReason == "0")
                    {
                        return "When suspending someone, suspend reason must be specified.";
                    }

                    membersToSuspend.Add(new int[2] { dup.MemberId, Convert.ToInt32(suspendReason) });
                }

                // add an entry
                queueHistoryItem.MemberProfileDuplicateHistoryEntries.Add(new MemberProfileDuplicateHistoryEntry()
                                                                              {
                                                                                  CRXResponseId = dup.CRXResponseId,
                                                                                  FieldType = dup.FieldType,
                                                                                  FieldValueOriginationDate = dup.FieldValueOriginationDate,
                                                                                  IsMatch = isMatching,
                                                                                  MemberId = dup.MemberId,
                                                                                  OrdinalNumber = dup.OrdinalNumber,
                                                                                  Suspended = isSuspend && !isAlreadySuspended,
                                                                                  AlreadySuspended = isAlreadySuspended,
                                                                                  TextContent = dup.FieldValue
                                                                              });
            }

            DBApproveQueueSA.Instance.SaveMemberProfileDuplicateHistory(queueHistoryItem, memberProfileDuplicateQueueId);
            var approvalReportMan = new ApprovalReportsManager();
            approvalReportMan.AddSessionQueueItemProcessedCount(QueueItemType.MemberProfileDuplicate);

            // suspend the members captured in the loop
            foreach(int[] entry in membersToSuspend)
            {
                var memToSuspend = MemberHelper.GetMember(entry[0], MemberLoadFlags.None);
                var siteId = MemberHelper.GetMemberLastLogonSite(memToSuspend);
                var brand = GeneralHelper.GetBrandForSite(siteId);
                MemberHelper.SuspendMember(memToSuspend, brand, (AdminActionReasonID)entry[1], (int)AdminAction.AdminSuspendMember,
                                           (CommunityIDs) brand.Site.Community.CommunityID,
                                           languageId, g.AdminID, true);
                MemberSA.Instance.SaveMember(memToSuspend);
            }

            return string.Empty;
        }

        public MemberProfileDuplicateApprovalModelView GetEmptyMemberProfileDuplicateApprovalModelView(int languageId)
        {
            var modelView = new MemberProfileDuplicateApprovalModelView();
            modelView.TopNav = new SharedTopNav();
            modelView.Left = GetApprovalLeft(null, null, false, QueueItemType.MemberProfileDuplicate, QAApprovalType.MemberProfileDuplicate, languageId);

            return modelView;
        }

        public MemberProfileDuplicateReviewModelView GetMemberProfileDuplicateReview(int languageId, int memberProfileDuplicateHistoryID)
        {
            var modelView = new MemberProfileDuplicateReviewModelView();
            modelView.TopNav = new SharedTopNav();
            // dequeue the next item or looking for a specific queue item for some processing
            modelView.QueueItem = memberProfileDuplicateHistoryID == Constants.NULL_INT
                                      ? DBApproveQueueSA.Instance.DequeueMemberProfileDuplicateReview(languageId)
                                      : DBApproveQueueSA.Instance.GetMemberProfileDuplicateReviewQueueItem(
                                          memberProfileDuplicateHistoryID);

            if (modelView.QueueItem == null)
            {
                modelView.Left = GetApprovalLeft(null, null, false, QueueItemType.MemberProfileDuplicateReview,
                                             QAApprovalType.MemberProfileDuplicate, languageId);

                return modelView;
            }

            modelView.LanguageID = modelView.QueueItem.MemberProfileDuplicateHistory.LanguageId;
            // ordinal 1 member is the member we are interested in
            modelView.MemberID =
                (from MemberProfileDuplicateHistoryEntry e in
                     modelView.QueueItem.MemberProfileDuplicateHistory.MemberProfileDuplicateHistoryEntries
                 where e.OrdinalNumber == 1
                 select e.MemberId).SingleOrDefault();
            var member = MemberHelper.GetMember(modelView.MemberID, MemberLoadFlags.None);

            #region Determine the brand based on the CRXResponse record for the hero/top member
            var crxResponseID = (from MemberProfileDuplicateHistoryEntry e in
                                     modelView.QueueItem.MemberProfileDuplicateHistory.MemberProfileDuplicateHistoryEntries
                                 where e.OrdinalNumber == 1
                                 select e.CRXResponseId).SingleOrDefault();
            var crxText = DBApproveQueueSA.Instance.LocateCRXText(crxResponseID);
            int brandID = Constants.NULL_INT;
            if (crxText.BrandId != Constants.NULL_INT)
            {
                brandID = crxText.BrandId;
            }
            else if(crxText.SiteId != Constants.NULL_INT)
            {
                brandID = g.GetBrand(crxText.SiteId) == null ? Constants.NULL_INT : g.GetBrand(crxText.SiteId).BrandID;
            }
            else
            {
                if (crxText.CommunityId == Constants.NULL_INT)
                    throw new Exception(
                        "GetMemberProfileDuplicateReview() error. Community could not be determined by the data in the database. CRXResponseID: " +
                        crxResponseID.ToString());
                
                member.GetLastLogonDate(crxText.CommunityId, out brandID);
            }
            var brand = BrandConfigSA.Instance.GetBrandByID(brandID);

            #endregion

            // let's prepare the List of MemberProfileDuplicateHistoryEntryExtended objects
            var isFirst = true;
            foreach(MemberProfileDuplicateHistoryEntry e in modelView.QueueItem.MemberProfileDuplicateHistory.MemberProfileDuplicateHistoryEntries)
            {
                if(isFirst)
                {
                    modelView.MemberProfileDuplicateHistoryEntryExtended =
                        new List<MemberProfileDuplicateHistoryEntryExtended>();
                }

                var eMember = MemberHelper.GetMember(e.MemberId, MemberLoadFlags.None);
                var eSiteID = isFirst ? brand.Site.SiteID : MemberHelper.GetMemberLastLogonSite(eMember);

                var globalStatusMask = eMember.GetAttributeInt(brand, "GlobalStatusMask");
                var currentlySuspended = (globalStatusMask & WebConstants.GLOBALSTATUSMASK_ADMIN_SUSPEND) ==
                                         WebConstants.GLOBALSTATUSMASK_ADMIN_SUSPEND;
                var lastSuspendReasonID = currentlySuspended
                                              ? (int) MemberHelper.GetLastSuspendedReasonID(eMember)
                                              : Constants.NULL_INT;

                modelView.MemberProfileDuplicateHistoryEntryExtended.Add(new MemberProfileDuplicateHistoryEntryExtended()
                                                                             {
                                                                                 MemberProfileDuplicateHistoryEntry = e,
                                                                                 CurrentlySuspended = currentlySuspended,
                                                                                 LastLogonSiteID = eSiteID,
                                                                                 TextboxHeight = GeneralHelper.GetTextboxHeight(e.TextContent),
                                                                                 SuspendReasonValue = lastSuspendReasonID
                                                                             });
                isFirst = false;
            }

            modelView.SiteName = brand.Site.Name;
            modelView.Left = GetApprovalLeft(member, brand, false, QueueItemType.MemberProfileDuplicateReview,
                                             QAApprovalType.MemberProfileDuplicate, languageId);

            return modelView;
        }

        public string CompleteMemberProfileDuplicateReview(FormCollection collection, int languageId, int memberProfileDuplicateHistoryID, out bool invalidQueueItem)
        {
            var queueItem = DBApproveQueueSA.Instance.GetMemberProfileDuplicateReviewQueueItem(memberProfileDuplicateHistoryID);
            invalidQueueItem = (queueItem == null);
            if(invalidQueueItem)
            {
                return "Invalid queue item";
            }

            var reviewToSubmit = new MemberProfileDuplicateReview()
                                     {
                                         AdminID = queueItem.MemberProfileDuplicateHistory.AdminId,
                                         LanguageID = languageId,
                                         MemberProfileDuplicateHistoryID = memberProfileDuplicateHistoryID,
                                         SupervisorID = g.AdminID,
                                         Entries = new List<MemberProfileDuplicateReviewEntry>()
                                     };

            foreach(var entry in queueItem.MemberProfileDuplicateHistory.MemberProfileDuplicateHistoryEntries)
            {
                // grab the form elements
                var isMatching = collection["Matching" + entry.CRXResponseId];
                var suspend = collection["Suspend" + entry.MemberId];
                var alreadySuspended = collection["AlreadySuspended" + entry.MemberId];
                var suspendReason = collection["SuspendReason" + entry.MemberId];

                if(isMatching == null || suspend == null || alreadySuspended == null)
                {
                    invalidQueueItem = true;
                    return "Invalid queue item";
                }

                var boolIsMatching = isMatching.ToLower().Contains("true");
                var boolSuspend = suspend.ToLower().Contains("true");
                var boolAlreadySuspended = alreadySuspended.ToLower().Contains("true");
                var intSuspendReason = suspendReason == null ? Constants.NULL_INT : Convert.ToInt32(suspendReason);
                
                // double checking what the UI should have blocked already
                var suspendNow = boolSuspend && !boolAlreadySuspended;
                if (suspendNow)
                {
                    // suspend the member
                    var memToSuspend = MemberHelper.GetMember(entry.MemberId, MemberLoadFlags.None);
                    var siteId = MemberHelper.GetMemberLastLogonSite(memToSuspend);
                    var brand = GeneralHelper.GetBrandForSite(siteId);

                    MemberHelper.SuspendMember(memToSuspend, brand, (AdminActionReasonID)intSuspendReason, (int)AdminAction.AdminSuspendMember,
                                           (CommunityIDs)brand.Site.Community.CommunityID,
                                           languageId, g.AdminID, true);
                    MemberSA.Instance.SaveMember(memToSuspend);
                }

                // create a record for the DB insert
                reviewToSubmit.Entries.Add(new MemberProfileDuplicateReviewEntry()
                                               {
                                                   IsMatch = boolIsMatching,
                                                   Ordinal = entry.OrdinalNumber,
                                                   Suspended = boolSuspend
                                               });
            }

            DBApproveQueueSA.Instance.SaveMemberProfileDuplicateReview(reviewToSubmit);
            var approvalReportMan = new ApprovalReportsManager();
            approvalReportMan.AddSessionQueueItemProcessedCount(QueueItemType.MemberProfileDuplicateReview);

            return string.Empty;
        }

        public MemberProfileDuplicateReviewModelView GetEmptyMemberProfileDuplicateReviewModelView(int languageId)
        {
            var modelView = new MemberProfileDuplicateReviewModelView();
            modelView.TopNav = new SharedTopNav();
            modelView.Left = GetApprovalLeft(null, null, false, QueueItemType.MemberProfileDuplicateReview, QAApprovalType.MemberProfileDuplicate, languageId);

            return modelView;
        }
    }
}

