﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models
{
    public class MingleFraudResult
    {
        public string Grade { get; private set; }
        public string ColorHex { get; private set; }
        public bool IsError { get; private set; }

        public MingleFraudResult(string grade, string colorHex, bool isError)
        {
            Grade = grade;
            ColorHex = colorHex;
            IsError = isError;
        }
    }
}
