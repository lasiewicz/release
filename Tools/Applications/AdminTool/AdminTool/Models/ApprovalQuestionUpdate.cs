﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models
{
    public class ApprovalQuestionUpdate
    {
        public string Answer { get; set; }
        public int AnswerID { get; set; }
        public bool Delete { get; set; }

        public ApprovalQuestionUpdate() { }

        public ApprovalQuestionUpdate(int answerID, string answer, bool delete)
        {
            AnswerID = answerID;
            Answer = answer;
            Delete = delete;
        }
    }
}
