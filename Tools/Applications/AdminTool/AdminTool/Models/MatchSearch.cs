﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Search.ValueObjects;
using System.Text;

namespace AdminTool.Models
{
    public class MatchSearch
    {
        public SearchPreferenceCollection SearchPreferences { get; set; }
        public List<MatchSearchPreference> Preferences { get; set; }
        public int TotalSearchResults { get; set; }
        public int AlreadyOnScrubList { get; set; }
        public List<int> AlreadyOnScrubListIds { get; set; }
        public int IneligibleMatches { get; set; }
        public List<MatchMember> Matches { get; set; }
        public bool SearchSufficient { get; set; }

        public string AlreadyOnScrubListIdsToString()
        {
            if (null != AlreadyOnScrubListIds)
            {
                return GeneralHelper.ConvertIntArrayToString(AlreadyOnScrubListIds.ToArray(),
                                                             WebConstants.COMMA_DELIMITER);
            } 
            return string.Empty;
        }

        public MatchSearch()
        {
            Matches = new List<MatchMember>();
            Preferences = new List<MatchSearchPreference>();
        }
    }

    public class MatchSearchPreference
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public MatchSearchPreference() { }
        public MatchSearchPreference(string name, string value)
        {
            Name = name;
            Value = value;
        }
    }
}
