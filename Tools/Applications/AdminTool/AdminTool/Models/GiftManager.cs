﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminTool.Models.ModelViews.SubAdmin;
using Matchnet.Member.ServiceAdapters;
using Spark.Common.Adapter;
using Spark.Common.DiscountService;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.EmailNotifier.ServiceAdapters;
using Matchnet.EmailNotifier.ValueObjects;
using Matchnet;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;


namespace AdminTool.Models
{
    public class GiftManager
    {
        public const string SCHEDULE_NAME = "BOGO";
        
        public GiftDetailsModelView GetGiftDetailsModelView(string giftCode)
        {
            GiftDetailsModelView modelView = null;

            Gift gift = null;

            try
            {
                gift = DiscountServiceWebAdapter.GetProxyInstanceForBedrock().GetGiftByCode(giftCode);
                if(gift!=null)
                {
                    modelView  = new GiftDetailsModelView(gift);
                }
            }
            catch (Exception ex)
            {

            }

            return modelView;
        }

        public Gift GetGift(string giftCode)
        {
            Gift gift = null;

            try
            {
                gift = DiscountServiceWebAdapter.GetProxyInstanceForBedrock().GetGiftByCode(giftCode);
                DiscountServiceWebAdapter.CloseProxyInstance();
            }
            catch (Exception ex)
            {

            }
            return gift;
        }

        public bool IsEmailAddressEligibleForGift(string emailAddress, int siteID)
        {
            bool isEligible = true;
            
            int existingMemberID = MemberSA.Instance.GetMemberIDByEmail(emailAddress);
            if (existingMemberID != Constants.NULL_INT)
            {
                Matchnet.Member.ServiceAdapters.Member member = MemberHelper.GetMember(existingMemberID, MemberLoadFlags.None);
                if(member.IsPayingMember(siteID)) isEligible = false;
            }

            return isEligible;
        }

        public bool CancelGift(string giftCode, int adminID)
        {
            bool success = true;
            
            try
            {
                bool pendingNotifications = false;

                GiftResponse response = DiscountServiceWebAdapter.GetProxyInstanceForBedrock().UpdateGiftStatus(giftCode, GiftStatus.Cancelled);
                if(response.InternalResponse.responsecode != "0") success = false;

                Gift gift = DiscountServiceWebAdapter.GetProxyInstanceForBedrock().GetGiftByCode(giftCode);
                if(gift != null && gift.Notifications != null && gift.Notifications.Count > 0)
                {
                    //check for pending notifications and cancel them
                    foreach(GiftNotification notification in gift.Notifications)
                    {
                        if(notification.NotificationStatus == GiftNotificationStatus.Pending)
                        {
                            pendingNotifications = true;
                            response = DiscountServiceWebAdapter.GetProxyInstanceForBedrock().UpdateGiftNotificationStatus(
                                notification.GiftNotificationID, GiftNotificationStatus.Cancelled, adminID);
                            if (response.InternalResponse.responsecode != "0") success = false;
                        }
                    }

                    if (pendingNotifications && success)
                    {
                        Brand brand = GeneralHelper.GetBrandForSite(gift.CallingSystemID);
                        //since we had pending notifications, should be a scheduled email that needs to be deleted
                        ScheduledEvent scheduledBogoNotification = WCFEmailNotifierSA.Instance.GetScheduledEventByMember(brand.BrandID,
                                                                                                                 gift.CustomerID,
                                                                                                                 SCHEDULE_NAME);

                        if (scheduledBogoNotification != null)
                        {
                            WCFEmailNotifierSA.Instance.DeleteScheduledEvent(scheduledBogoNotification.ScheduleGUID);
                        }
                    }
                }


            }
            catch (Exception)
            {
                success = false;
            }
            finally
            {
                DiscountServiceWebAdapter.CloseProxyInstance();
            }

            return success;
        }

        public bool ReopenGift(string giftCode)
        {
            bool success = true;

            try
            {
                GiftResponse response = DiscountServiceWebAdapter.GetProxyInstanceForBedrock().UpdateGiftStatus(giftCode, GiftStatus.Created);
                if (response.InternalResponse.responsecode != "0")
                {
                    success = false;
                }
            }
            catch (Exception)
            {
                success = false;
            }
            finally
            {
                DiscountServiceWebAdapter.CloseProxyInstance();
            }

            return success;
        }

        public bool SendNotification(int giftID, int purchaserMemberID, int purchaserSiteID, string emailAddress, int adminID)
        {
            bool success = true;

            Gift gift = DiscountServiceWebAdapter.GetProxyInstanceForBedrock().GetGiftByID(giftID);
            if (gift != null)
            {
                Brand brand = GeneralHelper.GetBrandForSite(purchaserSiteID);
                if (gift.BeginValidDate >= DateTime.Now)
                {
                    success = SendScheduledNotification(gift, emailAddress, purchaserMemberID, brand, adminID);
                }
                else
                {
                    success = SendRegularNotification(gift, emailAddress, purchaserMemberID, brand, adminID);
                }
            }

            return success;
        }

        private bool SendRegularNotification(Gift gift, string emailAddress, int purchaserMemberID, Brand brand, int adminID)
        {
            bool success = true;
            try
            {
                GiftResponse response = DiscountServiceWebAdapter.GetProxyInstanceForBedrock().AddGiftNotification(gift.GiftID,
                                                                GiftNotificationType.SentByAdmin,
                                                                emailAddress, GiftNotificationStatus.Sent,
                                                               adminID);
                if (response.InternalResponse.responsecode == "0")
                {
                    string encryptedPromo = getEncryptedBogoPromo(brand);

                    success = ExternalMailSA.Instance.SendBOGONotificationEmail(purchaserMemberID, brand.BrandID,
                                                                      emailAddress, encryptedPromo, gift.Code);
                }
                else
                {
                    success = false;
                }
            }
            catch (Exception)
            {
                success = false; 
            }
            finally
            {
                DiscountServiceWebAdapter.CloseProxyInstance();
            }
            

            return success;
        }

        private bool SendScheduledNotification(Gift gift, string emailAddress, int purchaserMemberID, Brand brand, int adminID)
        {

            bool success = true;

            try
            {
                GiftResponse response = DiscountServiceWebAdapter.GetProxyInstanceForBedrock().AddGiftNotification(gift.GiftID,
                                                                            GiftNotificationType.SentByAdmin,
                                                                            emailAddress, GiftNotificationStatus.Pending,
                                                                            adminID);
                
                if (response.InternalResponse.responsecode == "0")
                {
                    //First, see if there's another pending notification for this 
                    ScheduledEvent scheduledBogoNotification = WCFEmailNotifierSA.Instance.GetScheduledEventByMember(brand.BrandID,
                                                                                                             purchaserMemberID,
                                                                                                             SCHEDULE_NAME);
                    
                    if (scheduledBogoNotification != null)
                    {
                        WCFEmailNotifierSA.Instance.DeleteScheduledEvent(scheduledBogoNotification.ScheduleGUID);
                    }

                    int giftNotificationID = response.ReturnID;
                    string encryptedPromo = getEncryptedBogoPromo(brand);

                    ScheduledEvent notificationEvent = new ScheduledEvent();
                    notificationEvent.EmailAddress = emailAddress;
                    notificationEvent.ScheduleName = SCHEDULE_NAME;
                    notificationEvent.GroupID = brand.BrandID;
                    notificationEvent.NextAttemptDate = gift.BeginValidDate;
                    notificationEvent.MemberID = purchaserMemberID;
                    notificationEvent.ScheduleDetails = new ScheduleDetail();
                    notificationEvent.ScheduleDetails.Add("GiftCode", gift.Code);
                    notificationEvent.ScheduleDetails.Add("GiftPromo", encryptedPromo);
                    notificationEvent.ScheduleDetails.Add("GiftNotificationID", giftNotificationID.ToString());
                    WCFEmailNotifierSA.Instance.SaveScheduledEvent(notificationEvent);
                }
                else
                {
                    success = false;
                }
            }
            catch (Exception)
            {
                success = false;
            }
            finally
            {
                DiscountServiceWebAdapter.CloseProxyInstance();
            }
            
            return success;
        }

        private static string getEncryptedBogoPromo(Brand brand)
        {
            int bogoPromoID = Convert.ToInt32(RuntimeSettings.GetSetting("BOGO_PROMOID", brand.Site.Community.CommunityID,
                                                                brand.Site.SiteID, brand.BrandID));

            Matchnet.Lib.Encryption.SymmetricalEncryption encryptMemberTranID = new Matchnet.Lib.Encryption.SymmetricalEncryption(Matchnet.Lib.Encryption.SymmetricalEncryption.Provider.DES);
            return HttpContext.Current.Server.UrlEncode(encryptMemberTranID.Encrypt(bogoPromoID.ToString(), WebConstants.SUBSCRIPTION_ENCRYPT_KEY));
        }
    }
}
