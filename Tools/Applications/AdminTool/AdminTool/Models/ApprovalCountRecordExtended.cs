﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;

namespace AdminTool.Models
{
    public class ApprovalCountRecordExtended
    {
        public ApprovalCountRecord ApprovalCountRecord {get;set;}
        public string AdminEmailAddress {get;set;}

        public ApprovalCountRecordExtended() { }

        public ApprovalCountRecordExtended(ApprovalCountRecord approvalCountRecord, string adminEmailAddress)
        {
            ApprovalCountRecord = approvalCountRecord;
            AdminEmailAddress = adminEmailAddress;
        }
    }

    public class RecordComparerByMemberID: IComparer<ApprovalCountRecordExtended>
    {
        private Enums.SortDirection _sortDirection;
        
        public RecordComparerByMemberID(Enums.SortDirection sortDirection)
        {
            _sortDirection = sortDirection;
        }

        public int Compare(ApprovalCountRecordExtended x, ApprovalCountRecordExtended y)
        {
            if (_sortDirection == Enums.SortDirection.ASC)
            {
                return (x.ApprovalCountRecord.AdminMemberID - y.ApprovalCountRecord.AdminMemberID);
            }
            else
            {
                return (y.ApprovalCountRecord.AdminMemberID - x.ApprovalCountRecord.AdminMemberID);
            }
        }
    }

    public class RecordComparerByEmail : IComparer<ApprovalCountRecordExtended>
    {
        private Enums.SortDirection _sortDirection;

        public RecordComparerByEmail(Enums.SortDirection sortDirection)
        {
            _sortDirection = sortDirection;
        }
        
        public int Compare(ApprovalCountRecordExtended x, ApprovalCountRecordExtended y)
        {
            if (_sortDirection == Enums.SortDirection.ASC)
            {
                return string.Compare(x.AdminEmailAddress, y.AdminEmailAddress);
            }
            else
            {
                return string.Compare(y.AdminEmailAddress, x.AdminEmailAddress);
            }
        }
    }

    public class RecordComparerByTextCount : IComparer<ApprovalCountRecordExtended>
    {
        private Enums.SortDirection _sortDirection;

        public RecordComparerByTextCount(Enums.SortDirection sortDirection)
        {
            _sortDirection = sortDirection;
        }

        public int Compare(ApprovalCountRecordExtended x, ApprovalCountRecordExtended y)
        {
            if (_sortDirection == Enums.SortDirection.ASC)
            {
                return x.ApprovalCountRecord.TextApprovalCount - y.ApprovalCountRecord.TextApprovalCount;
            }
            else
            {
                return y.ApprovalCountRecord.TextApprovalCount - x.ApprovalCountRecord.TextApprovalCount;
            }
        }
    }

    public class RecordComparerByPhotoCount : IComparer<ApprovalCountRecordExtended>
    {
        private Enums.SortDirection _sortDirection;

        public RecordComparerByPhotoCount(Enums.SortDirection sortDirection)
        {
            _sortDirection = sortDirection;
        }
        
        public int Compare(ApprovalCountRecordExtended x, ApprovalCountRecordExtended y)
        {
            if (_sortDirection == Enums.SortDirection.ASC)
            {
                return x.ApprovalCountRecord.PhotoApprovalCount - y.ApprovalCountRecord.PhotoApprovalCount;
            }
            else
            {
                return y.ApprovalCountRecord.PhotoApprovalCount - x.ApprovalCountRecord.PhotoApprovalCount;
            }
        }
    }

    public class RecordComparerByQACount : IComparer<ApprovalCountRecordExtended>
    {
        private Enums.SortDirection _sortDirection;

        public RecordComparerByQACount(Enums.SortDirection sortDirection)
        {
            _sortDirection = sortDirection;
        }
        
        public int Compare(ApprovalCountRecordExtended x, ApprovalCountRecordExtended y)
        {
            if (_sortDirection == Enums.SortDirection.ASC)
            {
                return x.ApprovalCountRecord.QAApprovalCount - y.ApprovalCountRecord.QAApprovalCount;
            }
            else
            {
                return y.ApprovalCountRecord.QAApprovalCount - x.ApprovalCountRecord.QAApprovalCount;
            }
        }
    }

    public class RecordComparerByFacebookLikesCount : IComparer<ApprovalCountRecordExtended>
    {
        private Enums.SortDirection _sortDirection;

        public RecordComparerByFacebookLikesCount(Enums.SortDirection sortDirection)
        {
            _sortDirection = sortDirection;
        }

        public int Compare(ApprovalCountRecordExtended x, ApprovalCountRecordExtended y)
        {
            if (_sortDirection == Enums.SortDirection.ASC)
            {
                return x.ApprovalCountRecord.FacebookLikeApprovalCount - y.ApprovalCountRecord.FacebookLikeApprovalCount;
            }
            else
            {
                return y.ApprovalCountRecord.FacebookLikeApprovalCount - x.ApprovalCountRecord.FacebookLikeApprovalCount;
            }
        }
    }
}
