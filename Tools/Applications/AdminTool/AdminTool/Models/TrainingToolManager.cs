﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using AdminTool.Models.Helpers;
using AdminTool.Models.Helpers.Csv;
using AdminTool.Models.ModelViews;
using AdminTool.Models.ModelViews.Approval;
using AdminTool.Models.ModelViews.Email;
using AdminTool.Models.ModelViews.TrainingTool;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.ApproveQueue.ValueObjects.TrainingTool;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.Registration;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Email.ValueObjects;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.ExternalMail.ValueObjects.DoNotEmail;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Spark.Common.AccessService;

namespace AdminTool.Models
{
    public class TrainingToolManager : ManagerBase
    {
        public TTManageGrouping GetTTManageGrouping()
        {
            var viewModel = new TTManageGrouping();
            viewModel.TopNav = new SharedTopNav();
            viewModel.SingleAddSiteList = GeneralHelper.GetSitesListItems(false, 0);
            viewModel.MultiAddSiteList = GeneralHelper.GetSitesListItems(false, 0);
            viewModel.GroupingList = GetGroupingList(false);
            viewModel.PageNumber = 1;

            return viewModel;
        }

        public string AddSingleMember(int groupingId, int memberId, int siteId)
        {
            string returnMesssage = string.Empty;
            Brand brand = GeneralHelper.GetBrandForSite(siteId);

            if (CanAddMember(memberId, brand))
            {
                var profilesToAdd = new List<TTMemberProfile>();
                profilesToAdd.Add(new TTMemberProfile {BrandID = brand.BrandID, MemberID = memberId});
                insertTTMemberProfiles(groupingId, profilesToAdd);
            }
            else
            {
                returnMesssage = string.Format("Member {0} does not belong to the site you selected.", memberId);
            }

            return returnMesssage;
        }

        public bool CanAddMember(int memberID, Brand brand)
        {
            Member member = MemberHelper.GetMember(memberID, MemberLoadFlags.None);

            if (member == null)
                return false;

            int[] memberSiteIDList = member.GetSiteIDList();

            if (memberSiteIDList == null || memberSiteIDList.Count() == 0)
            {
                return false;
            }

            return memberSiteIDList.Contains(brand.Site.SiteID);
        }

        public List<TTMemberProfileUI> GetMemberProfiles(int groupingId, int pageNumber, int rowsPerPage, out int totalRows)
        {
            totalRows = 0;
            var memProfiles = DBApproveQueueSA.Instance.GetTTMemberProfiles(groupingId, pageNumber, rowsPerPage, out totalRows);

            if (memProfiles == null || memProfiles.Count == 0)
                return null;

            return (from a in memProfiles
                    select new TTMemberProfileUI(a)).ToList();
        }

        public string CreateNewGrouping(string groupingName)
        {
            string returnMessage = string.Empty;

            try
            {
                DBApproveQueueSA.Instance.CreateNewGrouping(groupingName);
            }
            catch (Exception ex)
            {
                string exceptionMessage = ExceptionHelper.GetExceptionMessage(ex, ExceptionHelper.ExceptionMode.Text);
                if (exceptionMessage.ToLower().Contains("duplicate key"))
                {
                    returnMessage = "Training Profile with the same name exists already.";
                }
                else
                {
                    throw ex;
                }
            }

            return returnMessage;
        }

        public TTSetAnswers GetTTSetAnswers(int memberID, int groupingID)
        {
            var ttSetAnswers = new TTSetAnswers();
            ttSetAnswers.TopNav = new SharedTopNav();
            ttSetAnswers.SuperTopNav = new TTSuperTopNav() {GroupingID = groupingID, TargetAgentID=memberID};
            ttSetAnswers.MemberID = memberID;
            
            var groupingList = GetGroupingList(false);
            var itemToRemove = (from g in groupingList
                                where g.Value == groupingID.ToString()
                                select g).SingleOrDefault();
            if (itemToRemove != null)
            {
                ttSetAnswers.CurrentBucketName = itemToRemove.Text;
                groupingList.Remove(itemToRemove);
            }
            ttSetAnswers.GroupingList = groupingList;
            
            // master record first
            var masterAnsRecords =
                DBApproveQueueSA.Instance.GetTTAnswerMemberProfiles(memberID,
                                                                   groupingID, false, null);
            var masterAnswerRecord = masterAnsRecords == null || masterAnsRecords.Count == 0
                                         ? null
                                         : masterAnsRecords[0];

            ttSetAnswers.SuspendReasons = GeneralHelper.GetAdminSuspendReasons();

            // if this null, it means it's the first time setting the answer for this profile
            if (masterAnswerRecord != null)
            {
                ttSetAnswers.TTMemberProfileID = masterAnswerRecord.TTMemberProfileID;
                ttSetAnswers.Suspend = masterAnswerRecord.Suspend;
                ttSetAnswers.WarningContactInfo = masterAnswerRecord.WarningContactInfo;
                ttSetAnswers.WarningGeneralViolation = masterAnswerRecord.WarningGeneralViolation;
                ttSetAnswers.WarningInappropriateContactInfo = masterAnswerRecord.WarningInappropriateContent;
                
                // bind the suspend reasons that supervisor selected previously
                if (masterAnswerRecord.Suspend && masterAnswerRecord.SuspendReasons != null && masterAnswerRecord.SuspendReasons.Count > 0)
                {
                    ttSetAnswers.SelectedSuspendReasons = new List<string>();

                    foreach (SelectListItem item in ttSetAnswers.SuspendReasons)
                    {
                        if(masterAnswerRecord.SuspendReasons.Exists(x => x.ReasonID == Convert.ToInt32(item.Value)))
                        {
                            ttSetAnswers.SelectedSuspendReasons.Add(item.Value);   
                        }
                    }
                }
            }
            
            // get all the text attributes to display
            List<TTMemberAttributeText> attributeWithAnswer =
                DBApproveQueueSA.Instance.GetAllTTMemberTextAttributesWithAnswer(
                    memberID, groupingID, false, null);

            // this shouldn't happen but just in case
            if (attributeWithAnswer == null)
                return ttSetAnswers;

            ttSetAnswers.TTMemberAttributeTexts = new List<TTMemberAttributeTextUI>();
            foreach (TTMemberAttributeText a in attributeWithAnswer)
            {
                if (a.DisplayOnly)
                    continue;

                var converted = new TTMemberAttributeTextUI(a);
                converted.TextboxHeight = GeneralHelper.GetTextboxHeight(converted.FieldValue);
                converted.TextUnacceptableReasons = GeneralHelper.GetIndividualTextStatuses(a.BadReasonMask);
                ttSetAnswers.TTMemberAttributeTexts.Add(converted);
            }

            // prepare some extra parameters for the UI so that during the save command, it  will save some trips to the DB
            ttSetAnswers.TTMemberAttributeTextIDsString = string.Join(",",
                                                                      (from a in ttSetAnswers.TTMemberAttributeTexts
                                                                       select a.TTMemberAttributeTextID.ToString())
                                                                          .ToArray());
            ttSetAnswers.TTMemberProfileID = attributeWithAnswer.First().TTMemberProfileID;

            return ttSetAnswers;
        }

        public void SaveTTAnswer(TTSetAnswers model, FormCollection collection)
        {
            var masterRecord = new TTAnswerMemberProfile
                {
                    TTMemberProfileID = model.TTMemberProfileID,
                    Suspend = model.Suspend,
                    WarningContactInfo = model.WarningContactInfo,
                    WarningGeneralViolation = model.WarningGeneralViolation,
                    WarningInappropriateContent = model.WarningInappropriateContactInfo
                };

            // suspend reasons?
            if (model.Suspend)
            {
                var suspendReasons = collection["SelectedSuspendReasons"];

                if (suspendReasons != null)
                {
                    masterRecord.SuspendReasons = new List<TTAnswerSuspendReason>();
                    var strReasons = suspendReasons.ToString();
                    var arrayReasonsSeparated = strReasons.Split(',');

                    foreach (var aReason in arrayReasonsSeparated)
                    {
                        masterRecord.SuspendReasons.Add(new TTAnswerSuspendReason() {ReasonID = Convert.ToInt32(aReason)});
                    }
                }
            }

            // loop through what was displayed on the screen and collect all the user inputs for the text attributes
            string[] ttMemberAttributeTextIDs = model.TTMemberAttributeTextIDsString.Split(',');
            var textAttributes = new List<TTMemberAttributeText>();
            foreach (string ttMemberAttributeTextID in ttMemberAttributeTextIDs)
            {
                var textAttribute = new TTMemberAttributeText();

                textAttribute.TTMemberAttributeTextID = Convert.ToInt32(ttMemberAttributeTextID);
                textAttribute.IncludeInTest = collection["IncludeInTest" + ttMemberAttributeTextID] == null
                                                  ? false
                                                  : collection["IncludeInTest" + ttMemberAttributeTextID].
                                                        ToLower().Contains("true");
                textAttribute.FieldValue = collection["Attribute" + ttMemberAttributeTextID] == null
                                               ? string.Empty
                                               : ApprovalHelper.cleanUpTags(
                                                   collection["Attribute" + ttMemberAttributeTextID]);
                textAttribute.IsGood = Convert.ToBoolean(collection["TextAcceptable" + ttMemberAttributeTextID]);
                textAttribute.BadReasonMask = textAttribute.IsGood
                                                  ? 0
                                                  : GeneralHelper.GetTextApprovalStatusMask(
                                                      collection["TextUnacceptableReasons" + ttMemberAttributeTextID]);

                textAttributes.Add(textAttribute);
            }

            DBApproveQueueSA.Instance.SaveTTAnswer(masterRecord, textAttributes);
        }

        public TTTraineeStart GetTTTraineeStart(int memberId)
        {
            var viewModel = new TTTraineeStart();
            viewModel.TopNav = new SharedTopNav();
            Member member = MemberHelper.GetMember(memberId, MemberLoadFlags.None);
            viewModel.TraineeEmail = member.EmailAddress;
            viewModel.GroupingList = GetGroupingList(true);

            // let's see if the trainee has any paused sessions
            viewModel.LastPausedTTTestInstanceID = -1;
            List<TTTestInstance> prevSessions = DBApproveQueueSA.Instance.GetTTTestInstances(memberId);
            if (prevSessions != null)
            {
                var pausedSes = (from a in prevSessions
                                 where a.Status == TTTestInstanceStatus.Paused
                                 select a).OrderByDescending(x => x.InsertDate).FirstOrDefault();

                viewModel.LastPausedTTTestInstanceID = pausedSes == null ? -1 : pausedSes.TTTestInstanceID;
            }

            // get the past test results
            viewModel.TestResults = new TTTestResults()
                {
                    TTTestResultList = GetTTTestResultsForAdmin(memberId)
                };
            
            return viewModel;
        }

        public int CreateNewTestInstance(int groupingId, int adminId)
        {
            int testInstanceID = DBApproveQueueSA.Instance.CreateNewTestInstance(groupingId, adminId);
            return testInstanceID;
        }

        public TTTestPage GetTTTestPage(int testInstanceId, int ttMemberProfileIDToExclude)
        {
            var ttTestPage = new TTTestPage();
            ttTestPage.TopNav = new SharedTopNav();
            ttTestPage.TTTestInstanceID = testInstanceId;
            int remainingCount = 0, ttAnswerMemberProfileVer= 0, ttAnswerSuspendReasonsVer = 0;

            List<TTMemberAttributeText> textAttributesToTest = DBApproveQueueSA.Instance
                                                                               .GetNextTTMemberProfileQuestion(
                                                                                   testInstanceId,
                                                                                   ttMemberProfileIDToExclude > 0
                                                                                       ? ttMemberProfileIDToExclude
                                                                                       : (int?) null, out remainingCount,
                                                                                   out ttAnswerMemberProfileVer,
                                                                                   out ttAnswerSuspendReasonsVer);

            if (textAttributesToTest == null || textAttributesToTest.Count == 0)
                return ttTestPage;

            int ttMemberProfileID = textAttributesToTest.First().TTMemberProfileID;
            ttTestPage.ExpandedUserInfo = TrainingToolHelper.GetTTExpandedUserInfo(ttMemberProfileID);

            ttTestPage.TTMemberAttributeTexts = new List<TTMemberAttributeTextUI>();
            foreach (TTMemberAttributeText a in textAttributesToTest)
            {
                var converted = new TTMemberAttributeTextUI(a);
                converted.TextboxHeight = GeneralHelper.GetTextboxHeight(converted.FieldValue);
                converted.TextUnacceptableReasons = GeneralHelper.GetIndividualTextStatuses();
                ttTestPage.TTMemberAttributeTexts.Add(converted);
            }

            // prepare some extra parameters for the UI so that during the save command, it  will save some trips to the DB
            ttTestPage.TTMemberAttributeTextIDsString = string.Join(",", (from a in ttTestPage.TTMemberAttributeTexts
                                                                          select a.TTMemberAttributeTextID.ToString())
                                                                             .ToArray());
            ttTestPage.TTMemberProfileID = ttMemberProfileID;
            ttTestPage.AdminSuspendReasons = GeneralHelper.GetAdminSuspendReasons();
            ttTestPage.IsLastQuestion = remainingCount == 1;
            ttTestPage.TTAnswerMemberProfileVersion = ttAnswerMemberProfileVer;
            ttTestPage.TTAnswerSuspendReasonsVersion = ttAnswerSuspendReasonsVer;

            // let's call the keepalive pulse code
            DBApproveQueueSA.Instance.TrainingToolKeepalivePulse(testInstanceId);

            return ttTestPage;
        }

        public void SaveAdminAnswer(TTTestPage model, FormCollection collection)
        {
            // master record properties
            var adminAnswer = new TTAdminAnswer();
            adminAnswer.TTTestInstanceID = model.TTTestInstanceID;
            adminAnswer.TTMemberProfileID = model.TTMemberProfileID;
            adminAnswer.SuspendReason = collection["AdminActionReasonID"] == null
                                            ? 0
                                            : Convert.ToInt32(collection["AdminActionReasonID"]);

            adminAnswer.Suspend = model.Suspend;
            adminAnswer.WContactInfo = model.WarningContactInfo;
            adminAnswer.WInappropriateContent = model.WarningInappropriateContent;
            adminAnswer.WGeneralViolation = model.WarningGeneralViolation;
            adminAnswer.WNoWarning = (!model.WarningContactInfo && !model.WarningInappropriateContent &&
                                      !model.WarningGeneralViolation);
            adminAnswer.PossibleFraud = model.PossibleFraud;
            adminAnswer.TTAnswerMemberProfileVersion = model.TTAnswerMemberProfileVersion;
            adminAnswer.TTAnswerSuspendReasonsVersion = model.TTAnswerSuspendReasonsVersion;


            // now record the individual answers for the fta fields
           adminAnswer.AdminAnswerEssays = new List<TTAdminAnswerEssay>();

            var atLeastOneEssayBad = false;
            string[] ttMemberAttributeTextIDs = model.TTMemberAttributeTextIDsString.Split(new[] { ',' });
            foreach (string s in ttMemberAttributeTextIDs)
            {
                var adminEssay = new TTAdminAnswerEssay();
                adminEssay.TTMemberAttributeTextID = Convert.ToInt32(s);
                adminEssay.IsGood = Convert.ToBoolean(collection["TextAcceptable" + adminEssay.TTMemberAttributeTextID]);
                adminEssay.BadReasonMask = 0;
                adminEssay.TTAnswerMemberAttributeTextVersion = Convert.ToInt32(collection["TextAnswerVersion" + adminEssay.TTMemberAttributeTextID]);

                // if the FTA field is marked as bad, capture the bad reason mask value
                if (!adminEssay.IsGood)
                {
                    adminEssay.BadReasonMask = GeneralHelper.GetTextApprovalStatusMask(
                        collection["TextUnacceptableReasons" + adminEssay.TTMemberAttributeTextID]);

                }

                adminAnswer.AdminAnswerEssays.Add(adminEssay);
            } // end of the main for loop for text attributes

            // db write will execute in asynch fashion unless it's the last question we are answering here.
            DBApproveQueueSA.Instance.SaveAdminAnswer(adminAnswer, model.IsLastQuestion);

            // if this is the last question, update the status of the test instance to done
            if (model.IsLastQuestion)
            {
                DBApproveQueueSA.Instance.UpdateTestStatus(model.TTTestInstanceID, TTTestInstanceStatus.Ended);
            }
        }

        private List<SelectListItem> GetGroupingList(bool completedOnly)
        {
            var list = new List<SelectListItem>();
            List<TTGrouping> groupingList = completedOnly
                                                ? DBApproveQueueSA.Instance.GetCompletedGroupingList()
                                                : DBApproveQueueSA.Instance.GetGroupingList();

            if (groupingList != null)
            {
                foreach (TTGrouping grouping in groupingList)
                {
                    list.Add(new SelectListItem
                        {
                            Selected = false,
                            Text = grouping.TTGroupingName,
                            Value = grouping.TTGroupingID.ToString()
                        });
                }
            }

            list.Sort((x,y) => x.Text.CompareTo(y.Text));

            return list;
        }

        public List<TTTestResult> GetTTTestResultsForAdmin(int adminMemberID)
        {
            var testInstances = DBApproveQueueSA.Instance.GetTTTestInstances(adminMemberID);

            if (testInstances == null)
                return null;

            var finishedTests = testInstances.Where(x => x.EndDate != DateTime.MinValue).ToList().OrderByDescending(x=>x.StartDate);

            return finishedTests.Select(tInstance => GetTTTestResults(tInstance.TTTestInstanceID)).ToList();
        }
        
        public TTTestResult GetTTTestResults(int testInstanceId)
        {
            // this method has to be rewritten because the test grading logic requirement changed, so deal with it later
            var testResults = new TTTestResult();
            testResults.TopNav = new SharedTopNav();
            testResults.TestAnswerGroups = new List<TestResultAnswerGroup>();

            #region test information
            var testInstance = DBApproveQueueSA.Instance.GetTTTestInstance(testInstanceId);
            var ttGrouping = DBApproveQueueSA.Instance.GetTTGrouping(testInstance.TTGroupingID);
            // only answered profiles come back in this count
            var totalItemCount = DBApproveQueueSA.Instance.GetTTMemberProfileCount(testInstanceId, true);
            // this is in seconds
            bool testFinished;
            var totalTestTime = DBApproveQueueSA.Instance.GetTotalTimeSpentOnTest(testInstanceId, out testFinished);
            testResults.TestFinished = testFinished;

            var agent = MemberHelper.GetMember(testInstance.AdminMemberID, MemberLoadFlags.None);
            testResults.AgentEmail = agent.EmailAddress;
            testResults.AgentMemberID = agent.MemberID;
            testResults.TimeStarted = testInstance.StartDate;
            testResults.TimeEnded = testInstance.EndDate;
            testResults.GroupingName = ttGrouping.TTGroupingName;
            testResults.TotalTestItemCount = totalItemCount;
            // UI expects in minutes so divide by 60 since totalTestTime is in seconds
            testResults.TotalTestTime = totalTestTime / 60;
            testResults.TotalTestTimeMinutes = (int)(totalTestTime/60);
            testResults.TotalTestTimeSeconds = (int) (totalTestTime%60);
            
            testResults.ProfilesPerHour = (totalItemCount/totalTestTime)*3600;
            #endregion

            // Admin's scantron entries
            TTAdminAnswerReport adminsAnswers = DBApproveQueueSA.Instance.GetTTAdminAnswerReport(testInstanceId);

            // Answer key set by a supervisor
            var answerMemProfiles = DBApproveQueueSA.Instance.GetTTAnswerMemberProfiles(testInstanceId);
            var answerTextAttrs = DBApproveQueueSA.Instance.GetTTAnswerMemberAttributeTexts(testInstanceId);

            int emailWarnPossiblePoints = 0,
                emailWarnPoints = 0,
                suspendPossiblePoints = 0,
                suspendPoints = 0,
                contentReviewPossiblePoints = 0,
                contentReviewPoints = 0;

            #region Suspend status and email warnings grading
            // Loop through the answerMemProfiles and grade the suspend status and email warnings sections.
            // We will handle the content review grading in a different loop.
            foreach (var answerKeyProfile in answerMemProfiles)
            {
                var pointsForSuspend = 0;
                var adminAnswer = (from a in adminsAnswers.TtAdminAnswers
                                   where a.TTMemberProfileID == answerKeyProfile.TTMemberProfileID
                                   select a).SingleOrDefault();

                if (answerKeyProfile.Suspend != adminAnswer.Suspend)
                {
                    // if the suspend statuses do not match, it's usually 0 pt but there is a special case where we give 5
                    if (answerKeyProfile.Suspend &&
                        answerKeyProfile.SuspendReasons.Exists(x => x.ReasonID == (int) AdminActionReasonID.FraudScammer)
                        && adminAnswer.PossibleFraud)
                    {
                        pointsForSuspend = 5;
                    }
                    else
                    {
                        pointsForSuspend = 0;
                    }
                }
                else
                {
                    // Inside this bracket, we can assume the answer key and admin's answer for suspend status match.
                    // Both answer key and admin said Approve; if admin selects possible fraud in this case, we deduct 5 pts
                    // otherwise give full 10.
                    if (!answerKeyProfile.Suspend)
                    {
                        pointsForSuspend = adminAnswer.PossibleFraud ? 5 : 10;

                        // Email warnings need to be correct if the answer key says to approve this profile.
                        // Exact match gets 1 pt.
                        emailWarnPossiblePoints++;
                        if (answerKeyProfile.WarningContactInfo == adminAnswer.WContactInfo &&
                            answerKeyProfile.WarningGeneralViolation == adminAnswer.WGeneralViolation &&
                            answerKeyProfile.WarningInappropriateContent == adminAnswer.WInappropriateContent &&
                            answerKeyProfile.WarningNoWarning == adminAnswer.WNoWarning)
                        {
                            emailWarnPoints++;
                        }
                    }
                    
                    // If we got this far and the grade hasn't been given yet, we can assume both the answer key and the admin's answer
                    // said suspend status is to suspend.
                    if (pointsForSuspend == 0)
                    {
                        // Suspend reason has to be right to get 10 pts and if wrong 0 pt.
                        pointsForSuspend =
                            answerKeyProfile.SuspendReasons.Exists(x => x.ReasonID == adminAnswer.SuspendReason)
                                ? 10
                                : 0;
                    }
                }
                
                suspendPoints += pointsForSuspend;
                // each profile is worth 10 points max
                suspendPossiblePoints += 10;
            }
            #endregion

            // Grade the content review accuracy
            foreach (var ansKeyTextAttr in answerTextAttrs)
            {
                var adminTextAttr = (from x in adminsAnswers.TtAdminAnswerEssays
                                     where x.TTMemberAttributeTextID == ansKeyTextAttr.TTMemberAttributeTextID
                                     select x).SingleOrDefault();

                // it's all or nothing for grading.  1 point per correct answer.
                if (ansKeyTextAttr.IsGood == adminTextAttr.IsGood &&
                    ansKeyTextAttr.BadReasonMask == adminTextAttr.BadReasonMask)
                {
                    contentReviewPoints++;
                }

                contentReviewPossiblePoints++;
            }

            testResults.TestAnswerGroups = new List<TestResultAnswerGroup>();
            testResults.TestAnswerGroups.Add(new TestResultAnswerGroup()
                {
                    GroupName = Constants.GROUP_NAME_SUSPENSION,
                    PointsCorrect = suspendPoints,
                    PossiblePoints = suspendPossiblePoints
                });
            testResults.TestAnswerGroups.Add(new TestResultAnswerGroup()
                {
                    GroupName = Constants.GROUP_NAME_EMAIL_WARNING,
                    PointsCorrect = emailWarnPoints,
                    PossiblePoints = emailWarnPossiblePoints
                });
            testResults.TestAnswerGroups.Add(new TestResultAnswerGroup()
            {
                GroupName = Constants.GROUP_NAME_CONTENT_REVIEW,
                PointsCorrect = contentReviewPoints,
                PossiblePoints = contentReviewPossiblePoints
            });


            #region Old grading logic (Remove later)
            //// essay answer breakdown
            //TTAdminAnswerReport dbResults = DBApproveQueueSA.Instance.GetTTAdminAnswerReport(testInstanceId);

            //#region *the Approve or Suspend section of the report
            //var approveGroupBy = (from a in dbResults.TtAdminAnswers
            //                           group a by a.SuspendAnswerKeyValue
            //                           into ag
            //                           select
            //                               new
            //                                   {
            //                                       SupendKeyValue = ag.Key,
            //                                       RightWrongGroups =
            //                               from a in ag
            //                               group a by a.SuspendApproveIsCorrect
            //                               into rwg select new {IsRight = rwg.Key, RowCount = rwg.Count()}
            //                                   });


            //testResults.TestAnswerGroups.Add(new TestResultAnswerGroup() {GroupName=Constants.GROUP_NAME_APPROVESUSPEND});
            //var approveEnum = approveGroupBy.GetEnumerator();
            //while (approveEnum.MoveNext())
            //{
            //    foreach (var subGroup in approveEnum.Current.RightWrongGroups)
            //    {
            //        testResults[Constants.GROUP_NAME_APPROVESUSPEND].AddToCount(
            //            approveEnum.Current.SupendKeyValue ? Constants.SUBGROUP_NAME_SUSPEND : Constants.SUBGROUP_NAME_APPROVE,
            //            subGroup.IsRight,
            //            subGroup.RowCount);
            //    }
            //}

            //// let's make sure both types of the line items are created so that UI can be dumb about it
            //testResults[Constants.GROUP_NAME_APPROVESUSPEND].AddLineItem(Constants.SUBGROUP_NAME_SUSPEND);
            //testResults[Constants.GROUP_NAME_APPROVESUSPEND].AddLineItem(Constants.SUBGROUP_NAME_APPROVE);
            //#endregion

            //#region *Send warnings section
            //testResults.TestAnswerGroups.Add(new TestResultAnswerGroup() {GroupName=Constants.GROUP_NAME_SENDWARNINGS});
            //var warningsGroupBy = (from a in dbResults.TtAdminAnswers
            //                       group a by a.WContactInfoIsCorrect
            //                       into ag
            //                       select new {WarningKey = ag.Key, Count = ag.Count()});

            //var warningsEnum = warningsGroupBy.GetEnumerator();
            //while (warningsEnum.MoveNext())
            //{
            //    testResults[Constants.GROUP_NAME_SENDWARNINGS].AddToCount(Constants.SUBGROUP_NAME_CONTACTINFO,
            //        warningsEnum.Current.WarningKey, warningsEnum.Current.Count);
            //}

            //warningsGroupBy = (from a in dbResults.TtAdminAnswers
            //                   group a by a.WInappropriateContentIsCorrect
            //                       into ag
            //                       select new { WarningKey = ag.Key, Count = ag.Count() });

            //warningsEnum = warningsGroupBy.GetEnumerator();
            //while (warningsEnum.MoveNext())
            //{
            //    testResults[Constants.GROUP_NAME_SENDWARNINGS].AddToCount(Constants.SUBGROUP_NAME_INAPPROPRIATE,
            //        warningsEnum.Current.WarningKey, warningsEnum.Current.Count);
            //}

            //warningsGroupBy = (from a in dbResults.TtAdminAnswers
            //                   group a by a.WGeneralViolationIsCorrect
            //                       into ag
            //                       select new { WarningKey = ag.Key, Count = ag.Count() });

            //warningsEnum = warningsGroupBy.GetEnumerator();
            //while (warningsEnum.MoveNext())
            //{
            //    testResults[Constants.GROUP_NAME_SENDWARNINGS].AddToCount(Constants.SUBGROUP_NAME_GENERALVIOLATION,
            //        warningsEnum.Current.WarningKey, warningsEnum.Current.Count);
            //}

            //warningsGroupBy = (from a in dbResults.TtAdminAnswers
            //                   group a by a.WNoWarningIsCorrect
            //                       into ag
            //                       select new { WarningKey = ag.Key, Count = ag.Count() });

            //warningsEnum = warningsGroupBy.GetEnumerator();
            //while (warningsEnum.MoveNext())
            //{
            //    testResults[Constants.GROUP_NAME_SENDWARNINGS].AddToCount(Constants.SUBGROUP_NAME_SENDNOWARNING,
            //        warningsEnum.Current.WarningKey, warningsEnum.Current.Count);
            //}
            //#endregion

            //#region Good from bad essays
            //var essayGroupBy = (from a in dbResults.TtAdminAnswerEssays
            //                    group a by a.IsGoodAnswerKeyValue
            //                    into ag
            //                    select new
            //                        {
            //                            IsGoodKeyValue = ag.Key,
            //                            RightWrongGroups = from a in ag
            //                                               group a by a.IsGoodIsCorrect
            //                                               into rwg
            //                                               select new {IsRight = rwg.Key, RowCount = rwg.Count()}
            //                        });

            //testResults.TestAnswerGroups.Add(new TestResultAnswerGroup() { GroupName = Constants.GROUP_NAME_BADGOODESSAY });
            //var essayEnum = essayGroupBy.GetEnumerator();
            //while (essayEnum.MoveNext())
            //{
            //    foreach (var subGroup in essayEnum.Current.RightWrongGroups)
            //    {
            //        testResults[Constants.GROUP_NAME_BADGOODESSAY].AddToCount(
            //            essayEnum.Current.IsGoodKeyValue ? Constants.SUBGROUP_NAME_GOODESSAY : Constants.SUBGROUP_NAME_BADESSAY,
            //            subGroup.IsRight,
            //            subGroup.RowCount);
            //    }
            //}
            //testResults[Constants.GROUP_NAME_BADGOODESSAY].AddLineItem(Constants.SUBGROUP_NAME_GOODESSAY).DisplayName = "Good Essays";
            //testResults[Constants.GROUP_NAME_BADGOODESSAY].AddLineItem(Constants.SUBGROUP_NAME_BADESSAY).DisplayName = "Bad Essays";
            //#endregion

            //#region Essay Violations

            //var vioGroupBy = (from a in dbResults.TtAdminAnswerEssayViolations
            //                  group a by a.BadReasonID
            //                  into ag
            //                  select new
            //                      {
            //                          ViolationKey = ag.Key,
            //                          RightWrongGroups = from a in ag
            //                                             group a by a.BadReasonIsCorrect
            //                                             into rwg
            //                                             select new {IsRight = rwg.Key, RowCount = rwg.Count()}
            //                      }
            //                 );

            //testResults.TestAnswerGroups.Add(new TestResultAnswerGroup() { GroupName = Constants.GROUP_NAME_ESSAYVIOLATION });
            //var vioEnum = vioGroupBy.GetEnumerator();
            //while (vioEnum.MoveNext())
            //{
            //    // skip the 0 essay violation type
            //    if (vioEnum.Current.ViolationKey == 0)
            //        continue;

            //    foreach (var subGroup in vioEnum.Current.RightWrongGroups)
            //    {
            //        testResults[Constants.GROUP_NAME_ESSAYVIOLATION].AddToCount(
            //            vioEnum.Current.ViolationKey.ToString(),
            //            subGroup.IsRight,
            //            subGroup.RowCount);
            //    }
            //}

            //// for the sub group name, we used the mask numerical value, so we need to map the display name somehow here
            //var violationList = GeneralHelper.GetIndividualTextStatuses();
            //foreach (var violationBitMask in violationList)
            //{
            //    var lineItem = testResults[Constants.GROUP_NAME_ESSAYVIOLATION][violationBitMask.Value];

            //    if (lineItem != null)
            //    {
            //        lineItem.DisplayName = violationBitMask.Text;
            //    }
            //}
            //#endregion
            #endregion

            return testResults;
        }

        public void PauseTestSession(int testInstanceId)
        {
            DBApproveQueueSA.Instance.UpdateTestStatus(testInstanceId, TTTestInstanceStatus.Paused);
        }

        internal List<TTMemberProfileUI> GetMemberProfiles(int memberID)
        {
            var memProfiles = DBApproveQueueSA.Instance.GetTTMemberProfiles(memberID);

            if (memProfiles == null || memProfiles.Count == 0)
                return null;

            return (from a in memProfiles
                    select new TTMemberProfileUI(a)).ToList();
        }

        public TTEditProfile GetTTEditProfile(int memberId, int groupingId)
        {
            var ttEditProfile = new TTEditProfile();
            ttEditProfile.SuperTopNav = new TTSuperTopNav() { GroupingID = groupingId, TargetAgentID = memberId };

            return ttEditProfile;
        }

        public ArrayList AddMemberProfilesFromCsv(Stream inputStream, int groupingId)
        {
            var reader = new CsvFileReader(inputStream);

            var profilesToAdd = new List<TTMemberProfile>();
            var row = new CsvRow();

            ArrayList errorItems = new ArrayList();
            var rowNumber = 0;

            while (reader.ReadRow(row))
            {
                rowNumber++;

                if (row.Count != 2)
                {
                    errorItems.Add(string.Format("Row number {0}: Missing argument.", rowNumber));
                    continue;
                }

                var errorString = string.Empty;
                var memberId = 0;
                var brandId = 0;
                if (!int.TryParse(row[0], out memberId) || !int.TryParse(row[1], out brandId))
                {
                    errorItems.Add(string.Format("Row number {0}: Both arguments must be numeric.", rowNumber));
                    continue;
                }

                var brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(brandId);
                if (brand == null)
                {
                    errorItems.Add(string.Format("Row number {0}: Invalid brandID {1}.", rowNumber, brandId));
                    continue;
                }
                var member = MemberHelper.GetMember(memberId, MemberLoadFlags.None);
                if (member == null)
                {
                    errorItems.Add(string.Format("Row number {0}: Member {1} was not found.", rowNumber, memberId));
                    continue;
                }
                if (!CanAddMember(memberId, brand))
                {
                    errorItems.Add(string.Format("Row number {0}: Member {1} does not belong to brand {2}", rowNumber, memberId,
                                                 brandId));
                    continue;
                }

                profilesToAdd.Add(new TTMemberProfile() {MemberID = memberId, BrandID = brandId});
            }

            insertTTMemberProfiles(groupingId, profilesToAdd);
            return errorItems;
        }

        public TTViewMember GetTTViewMember(int memberId, int groupingId, bool isFttSupervisor)
        {
            var ttViewMember = new TTViewMember();

            if (isFttSupervisor)
            {
                ttViewMember.SuperTopNav = new TTSuperTopNav() { GroupingID = groupingId, TargetAgentID = memberId };
            }

            // let's obtain the frozen member data
            var ttMemberProfile = DBApproveQueueSA.Instance.GetTTMemberProfile(memberId, groupingId);
            var ttMemberAttributes = DBApproveQueueSA.Instance.GetTTMemberAttributes(ttMemberProfile.TTMemberProfileID);

            #region Basic info
            ttViewMember.TtBasicUserInfo = new TTBasicUserInfo();
            ttViewMember.TtBasicUserInfo.MemberId = memberId;
            ttViewMember.TtBasicUserInfo.FirstName = ttMemberAttributes.GetMemberAttributeText("SiteFirstName");
            ttViewMember.TtBasicUserInfo.LastName = ttMemberAttributes.GetMemberAttributeText("SiteLastName");
            ttViewMember.TtBasicUserInfo.UserName = ttMemberAttributes.GetMemberAttributeText("Username");
            ttViewMember.TtBasicUserInfo.Email = ttMemberAttributes.GetMemberAttributeText("EmailAddress");
            ttViewMember.TtBasicUserInfo.ProfileStatus = getProfileStatus(ttMemberAttributes);
            // The actual Admintool doesn't have this property populated either, but I am throwing in the attribute here so I can set it later
            // when it's implemented.
            ttViewMember.TtBasicUserInfo.SuspendReason = string.Empty;
            // We are going to assume the user is not an admin always.
            ttViewMember.TtBasicUserInfo.IsAdmin = false;
            #endregion

            // Photo url for the TTMemberLeft
            ttViewMember.TtMemberLeft = new TTMemberLeft();
            var ttMemberPhotos = DBApproveQueueSA.Instance.GetTTMemberPhotos(ttMemberProfile.TTMemberProfileID);
            if (ttMemberPhotos != null && ttMemberPhotos.Count > 0)
            {
                var basicClient = TrainingToolHelper.GetBasicCloudClient();
                
                ttViewMember.TtMemberLeft.PhotoThumbUrl =
                    basicClient.GetFullCloudImagePath(ttMemberPhotos[0].ThumbnailCloudPath, false);
            }

            #region GlobalProfile info
            ttViewMember.TtGlobalProfile = new TTGlobalProfile();
            var globalStatusMask = ttMemberAttributes.GetMemberAttributeInt("GlobalStatusMask");
            ttViewMember.TtGlobalProfile.MemberId = memberId;
            ttViewMember.TtGlobalProfile.AdminSuspended = (globalStatusMask & WebConstants.GLOBALSTATUSMASK_ADMIN_SUSPEND) == WebConstants.GLOBALSTATUSMASK_ADMIN_SUSPEND;
            ttViewMember.TtGlobalProfile.BadEmail =  (globalStatusMask & WebConstants.GLOBALSTATUSMASK_BAD_EMAIL_SUSPEND) == WebConstants.GLOBALSTATUSMASK_BAD_EMAIL_SUSPEND;
            ttViewMember.TtGlobalProfile.EmailVerified = (globalStatusMask & WebConstants.GLOBALSTATUSMASK_EMAIL_VERIFIED) == WebConstants.GLOBALSTATUSMASK_EMAIL_VERIFIED;

            // dne and last suspend reason info
            if (ttViewMember.TtGlobalProfile.AdminSuspended)
            {
                var lastSuspendReasonID = ttMemberAttributes.GetMemberAttributeInt("LastSuspendReasonID") ==
                                          Matchnet.Constants.NULL_INT
                                              ? 0
                                              : ttMemberAttributes.GetMemberAttributeInt("LastSuspendReasonID");
                ttViewMember.TtGlobalProfile.LastSuspendedReasonID = (AdminActionReasonID)lastSuspendReasonID;
                ttViewMember.TtGlobalProfile.AdminSuspendReasons = GeneralHelper.GetAdminSuspendReasons(ttViewMember.TtGlobalProfile.LastSuspendedReasonID, true, false);    
            }
            else
            {
                ttViewMember.TtGlobalProfile.LastSuspendedReasonID = AdminActionReasonID.None;
                ttViewMember.TtGlobalProfile.AdminSuspendReasons = GeneralHelper.GetAdminSuspendReasons(AdminActionReasonID.None, false, false);    
            }

            ttViewMember.TtGlobalProfile.OnDNE = Convert.ToBoolean(ttMemberAttributes.GetMemberAttributeInt("IsOnDNE") == Matchnet.Constants.NULL_INT ? 0 : ttMemberAttributes.GetMemberAttributeInt("IsOnDNE"));
            #endregion

            #region DateTimeStampInfo
            ttViewMember.TtDateTimeStampInfo = new TTDateTimeStampInfo();
            ttViewMember.TtDateTimeStampInfo.RegisteredDate =
                convertToDateTime(
                    (from a in ttMemberAttributes.MemberAttributeDates
                     where a.MemberAttributeName == "BrandInsertDate"
                     select a).SingleOrDefault());
            ttViewMember.TtDateTimeStampInfo.LastUpdateDate =
                convertToDateTime(
                    (from a in ttMemberAttributes.MemberAttributeDates
                     where a.MemberAttributeName == "LastUpdated"
                     select a).SingleOrDefault());
            ttViewMember.TtDateTimeStampInfo.LastLoginDate =
                convertToDateTime(
                    (from a in ttMemberAttributes.MemberAttributeDates
                     where a.MemberAttributeName == "BrandLastLogonDate"
                     select a).SingleOrDefault());

            ttViewMember.TtDateTimeStampInfo.LastLogons = new List<DateTime>();
            for (int i = 1; i < 6; i++)
            {
                ttViewMember.TtDateTimeStampInfo.LastLogons.Add(convertToDateTime(
                    (from a in ttMemberAttributes.MemberAttributeDates
                     where a.MemberAttributeName == "LastLogon" + i
                     select a).SingleOrDefault()));   
            }

            // remember normally we work with frozen member data, so this member object shouldn't be used so freely unless you
            // know what you are doing. it's currently being used to determine the first ever BrandInsertDate which know
            // should never change, so it's safe to use this live member data in this case.
            var member = MemberHelper.GetMember(memberId, MemberLoadFlags.None);
            var firstEverBrandInsertDate = MemberHelper.GetFirstBrandInsertDate(member);
            if (firstEverBrandInsertDate == ttViewMember.TtDateTimeStampInfo.RegisteredDate)
            {
                var regSceneId = ttMemberAttributes.GetMemberAttributeInt("RegistrationScenarioID");

                if (regSceneId != Matchnet.Constants.NULL_INT)
                {
                    var regScenarios =
                        Matchnet.Content.ServiceAdapters.RegistrationMetadataSA.Instance.GetScenariosForAdmin();
                    var memberRegScenario = (from r in regScenarios
                                             where r.ID == regSceneId
                                             select r).SingleOrDefault();
                    ttViewMember.TtDateTimeStampInfo.IsMobileRegistration = (memberRegScenario != null) && (memberRegScenario.DeviceType == DeviceType.Handeheld);
                }
            }
            #endregion

            #region Communication history section
            ttViewMember.TtCommunicationHistory = new TTCommunicationHistory();
            ttViewMember.TtCommunicationHistory.TTMemberProfileID = ttMemberProfile.TTMemberProfileID;
            ttViewMember.TtCommunicationHistory.UnreadMessageCount =
                ttMemberAttributes.GetMemberAttributeInt("UnreadMessageCount");

            ttViewMember.TtCommunicationHistory.LastMessageSentDate = convertToDateTime(
                    (from a in ttMemberAttributes.MemberAttributeDates
                     where a.MemberAttributeName == "LastSentDate"
                     select a).SingleOrDefault());
            ttViewMember.TtCommunicationHistory.AllAcessExpireDate = convertToDateTime(
                    (from a in ttMemberAttributes.MemberAttributeDates
                     where a.MemberAttributeName == "AllAccessPrivilegeEndDate"
                     select a).SingleOrDefault());

            ttViewMember.TtCommunicationHistory.AllAccessRemainingCount = ttMemberAttributes.GetMemberAttributeInt("AllAccessEmailsRemaining");
            ttViewMember.TtCommunicationHistory.ContactInfoWarnings = ttMemberAttributes.GetMemberAttributeInt("FTWarningContactInfo");
            ttViewMember.TtCommunicationHistory.InappropriateContentWarnings = ttMemberAttributes.GetMemberAttributeInt("FTWarningInappropriateContent");
            ttViewMember.TtCommunicationHistory.GeneralViolationWarnings = ttMemberAttributes.GetMemberAttributeInt("FTWarningGeneralViolation");
            #endregion

            return ttViewMember;
        }

        private Enums.ProfileStatus getProfileStatus(TTMemberProfile ttMemberProfile)
        {
            var profileStatus = Enums.ProfileStatus.Active;

            int globalStatusMask = ttMemberProfile.GetMemberAttributeInt("GlobalStatusMask");
            if ((globalStatusMask & (int)GlobalStatusMask.AdminSuspended) == (int)GlobalStatusMask.AdminSuspended)
            {
                profileStatus = Enums.ProfileStatus.AdminSuspend;
            }
            else if (ttMemberProfile.GetMemberAttributeInt("SelfSuspendedFlag") == WebConstants.SELF_SUSPEND)
            {
                profileStatus = Enums.ProfileStatus.SelfSuspend;
            }

            return profileStatus;
        }

        private void insertTTMemberProfiles(int groupingId, List<TTMemberProfile> profilesToAdd)
        {
            // ApproveQueue service will copy all the attributes, but we need to populate the non-attribute information before sending the request in.
            foreach (TTMemberProfile profile in profilesToAdd)
            {
                var member = MemberHelper.GetMember(profile.MemberID, MemberLoadFlags.None);
                var brand = BrandConfigSA.Instance.GetBrandByID(profile.BrandID);

                if (profile.MemberAtributeInts == null)
                {
                    profile.MemberAtributeInts = new List<TTMemberAttributeInt>();
                }

                if (profile.MemberAttributeDates == null)
                {
                    profile.MemberAttributeDates = new List<TTMemberAttributeDate>();
                }
                
                #region IsOnDNE and LastSuspendReasonID are calculated attributes, not member attributes, so set the values here.
                AdminActionReasonID lastSuspendedReasonID = MemberHelper.GetLastSuspendedReasonID(member);
                var dneEntry = DoNotEmailSA.Instance.GetEntryBySiteAndEmailAddress(brand.Site.SiteID,
                                                                                               member.EmailAddress);

                profile.MemberAtributeInts.Add(new TTMemberAttributeInt
                    {
                        MemberAttributeName = "IsOnDNE",
                        TTAttributeDataType = TTAttributeDataType.SpecialNumber,
                        FieldValue = dneEntry != null ? 1 : 0
                    });
                profile.MemberAtributeInts.Add(new TTMemberAttributeInt
                    {
                        MemberAttributeName = "LastSuspendReasonID",
                        TTAttributeDataType = TTAttributeDataType.SpecialNumber,
                        FieldValue = (int) lastSuspendedReasonID
                    });
                #endregion

                #region unread message count and FTA warning counts
                var unreadMessageCount = EmailMessageSA.Instance.GetMessageCountByStatus(member.MemberID, brand,
                    (int)SystemFolders.Inbox, Matchnet.Email.ValueObjects.MessageStatus.Read, true, false);
                unreadMessageCount += EmailMessageSA.Instance.GetMessageCountByStatus(member.MemberID, brand,
                    (int)SystemFolders.VIPInbox, Matchnet.Email.ValueObjects.MessageStatus.Read, true, false);
                profile.MemberAtributeInts.Add(new TTMemberAttributeInt
                {
                    MemberAttributeName = "UnreadMessageCount",
                    TTAttributeDataType = TTAttributeDataType.SpecialNumber,
                    FieldValue = unreadMessageCount
                });

                var dict = MemberSA.Instance.GetCommunicationWarningCounts(member.MemberID, brand.Site.SiteID);
                if (dict != null)
                {
                    int countVal;
                    var isInDict = dict.TryGetValue((int) WarningReason.ContactInfo, out countVal);
                    profile.MemberAtributeInts.Add(new TTMemberAttributeInt
                        {
                            MemberAttributeName = "FTWarningContactInfo",
                            TTAttributeDataType = TTAttributeDataType.SpecialNumber,
                            FieldValue = isInDict ? countVal : 0
                        });

                    isInDict = dict.TryGetValue((int) WarningReason.InappropriateContent, out countVal);
                    profile.MemberAtributeInts.Add(new TTMemberAttributeInt
                        {
                            MemberAttributeName = "FTWarningInappropriateContent",
                            TTAttributeDataType = TTAttributeDataType.SpecialNumber,
                            FieldValue = isInDict ? countVal : 0
                        });

                    isInDict = dict.TryGetValue((int) WarningReason.GeneralViolation, out countVal);
                    profile.MemberAtributeInts.Add(new TTMemberAttributeInt
                        {
                            MemberAttributeName = "FTWarningGeneralViolation",
                            TTAttributeDataType = TTAttributeDataType.SpecialNumber,
                            FieldValue = isInDict ? countVal : 0
                        });
                }
                else
                {
                    profile.MemberAtributeInts.Add(new TTMemberAttributeInt
                    {
                        MemberAttributeName = "FTWarningContactInfo",
                        TTAttributeDataType = TTAttributeDataType.SpecialNumber,
                        FieldValue = 0
                    });

                    profile.MemberAtributeInts.Add(new TTMemberAttributeInt
                    {
                        MemberAttributeName = "FTWarningInappropriateContent",
                        TTAttributeDataType = TTAttributeDataType.SpecialNumber,
                        FieldValue = 0
                    });

                    profile.MemberAtributeInts.Add(new TTMemberAttributeInt
                    {
                        MemberAttributeName = "FTWarningGeneralViolation",
                        TTAttributeDataType = TTAttributeDataType.SpecialNumber,
                        FieldValue = 0
                    });
                }

                #endregion

                #region All Access remaining count and expiration date
                var allAccessCount = 0;
                var allAccessExpireDate = DateTime.MinValue;

                AccessPrivilege priv = member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.AllAccessEmails, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                if (priv != null)
                {
                    allAccessCount = priv.RemainingCount;
                }
                profile.MemberAtributeInts.Add(new TTMemberAttributeInt
                {
                    MemberAttributeName = "AllAccessEmailsRemaining",
                    TTAttributeDataType = TTAttributeDataType.SpecialNumber,
                    FieldValue = allAccessCount
                });

                priv = member.GetUnifiedAccessPrivilege(Spark.Common.AccessService.PrivilegeType.AllAccess, brand.BrandID, brand.Site.SiteID, brand.Site.Community.CommunityID);
                if (priv != null)
                {
                    allAccessExpireDate = priv.EndDatePST;
                }

                bool isFutureTimespan;
                var convertedDate = convertToTimespan(allAccessExpireDate, out isFutureTimespan);
                profile.MemberAttributeDates.Add(new TTMemberAttributeDate()
                {
                    MemberAttributeName = "AllAccessPrivilegeEndDate",
                    TTAttributeDataType = TTAttributeDataType.SpecialTimespan,
                    FieldValue = convertedDate,
                    IsFutureTimespan = isFutureTimespan
                });
                #endregion

                #region Last message sent date
                var lastMessageSentDate = MemberHelper.GetLastMessageSentDate(member.MemberID, brand.Site.Community.CommunityID);
                convertedDate = convertToTimespan(lastMessageSentDate, out isFutureTimespan);
                profile.MemberAttributeDates.Add(new TTMemberAttributeDate()
                {
                    MemberAttributeName = "LastSentDate",
                    TTAttributeDataType = TTAttributeDataType.SpecialTimespan,
                    FieldValue = convertedDate,
                    IsFutureTimespan = isFutureTimespan
                });
                #endregion

                #region Set last 5 logons here because these aren't stored as member attributes
                var lastLogons = member.GetMemberLastLogons(brand.BrandID, MemberLoadFlags.IngoreSACache);

                int i = 1;
                foreach (var date in lastLogons)
                {
                    // The metadata info in the database only has up to 5 of these, so break out of this if for some reason we get more than 5 back
                    // from the GetMemberLastLogons() call.
                    if (i > 5)
                        break;

                    convertedDate = convertToTimespan(date.LastLogonDate, out isFutureTimespan);

                    profile.MemberAttributeDates.Add(new TTMemberAttributeDate()
                    {
                        MemberAttributeName = "LastLogon" + i.ToString(),
                        TTAttributeDataType = TTAttributeDataType.SpecialTimespan,
                        FieldValue = convertedDate,
                        IsFutureTimespan = isFutureTimespan
                    });

                    i++;
                }
                #endregion

            }
            
            DBApproveQueueSA.Instance.InsertTTMemberProfiles(groupingId,
                                                                 profilesToAdd);
        }

        /// <summary>
        /// Given a TTMemberAttributeDate, examine the data type and determine the end result DateTime value.
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        private DateTime convertToDateTime(TTMemberAttributeDate attribute)
        {
            if (attribute == null)
                return DateTime.MinValue;

            var retValue = DateTime.MinValue;

            if (attribute.TTAttributeDataType == TTAttributeDataType.Date)
            {
                // Very simple case here. No calculation needed so just return the value.
                retValue =  attribute.FieldValue;
            }
            else if (attribute.TTAttributeDataType == TTAttributeDataType.Timespan ||
                attribute.TTAttributeDataType == TTAttributeDataType.SpecialTimespan)
            {
                var timeSpan = attribute.FieldValue - DateTime.MinValue;
                retValue = DateTime.Now - (attribute.IsFutureTimespan ? timeSpan.Negate() : timeSpan.Duration());
            }

            return retValue;
        }

        /// <summary>
        /// Given an actual date, caculate the timespan from today and then convert it to DateTime.MinValue based DateTime value.
        /// </summary>
        /// <param name="valueToConvert"></param>
        /// <param name="isFutureTimespan"></param>
        /// <returns></returns>
        private DateTime convertToTimespan(DateTime valueToConvert, out bool isFutureTimespan)
        {
            var timeSpanDiff = DateTime.Now - valueToConvert;
            isFutureTimespan = timeSpanDiff.CompareTo(TimeSpan.Zero) < 0;

            return DateTime.MinValue + timeSpanDiff.Duration();
        }

        public TTEmailList GetTTEmailList(int ttMemberProfileID, int folderID, int rowIndex)
        {
            var emailList = new TTEmailList();

            var ttMemberProfile = DBApproveQueueSA.Instance.GetTTMemberAttributes(ttMemberProfileID);
            emailList.EmailAddress = ttMemberProfile.GetMemberAttributeText("EmailAddress");
            emailList.FolderID = folderID;
            emailList.TTMemberProfileID = ttMemberProfileID;

            var brand = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetBrandByID(ttMemberProfile.BrandID);
            
            // get the emails for the given folder we want
            List<Email> emails = convertToEmailList(DBApproveQueueSA.Instance.GetTTMessages(ttMemberProfileID, false),
                ttMemberProfile.GetMemberAttributeInt("MemberID"),    
                folderID,
                ttMemberProfile,
                brand);

            // sort
            emails.Sort((x, y) => (y.MessageDate.CompareTo(x.MessageDate)));

            // do pagination here
            doPagination(emailList, emails, rowIndex, 10);

            return emailList;
        }

        private void doPagination(EmailList emailList, List<Email> allEmail, int rowIndex, int pageSize)
        {
            // first calculate the total number of pages
            int totalPages = 0;
            totalPages = allEmail.Count / pageSize;
            if (allEmail.Count % pageSize > 0)
            {
                totalPages++;
            }
            emailList.TotalPageCount = totalPages;
            emailList.PageSize = pageSize;
            emailList.TotalEmailCount = allEmail.Count;
            emailList.RowIndex = rowIndex;

            // load the EmailList object with the actual emails
            if (rowIndex < allEmail.Count)
            {
                // we have a full page worth or not
                if (rowIndex + pageSize - 1 < allEmail.Count)
                {
                    emailList.Emails = allEmail.GetRange(rowIndex, pageSize);
                    emailList.TotalReturnedInPage = pageSize;
                }
                else
                {
                    emailList.Emails = allEmail.GetRange(rowIndex, allEmail.Count - rowIndex);
                    emailList.TotalReturnedInPage = allEmail.Count - rowIndex;
                }

            }
        }

        private List<Email> convertToEmailList(List<TTMessage> ttMessages,
            int memberID,
            int folderID,
            TTMemberProfile ttMemberProfile,
            Brand brand)
        {
            if (ttMessages == null)
                return null;

            List<Email> emails = null;

            switch ((EmailLogType)folderID)
            {
                case EmailLogType.Both:
                    emails = getBothInboxSentMessages(ttMessages, memberID, ttMemberProfile, brand);
                    break;
                case EmailLogType.Inbox:
                    emails = getInboxMessages(ttMessages, memberID, ttMemberProfile, brand);
                    break;
                case EmailLogType.Sent:
                    emails = getSentMessages(ttMessages, memberID, ttMemberProfile, brand);
                    break;
            }

            return emails;
        }

        private List<Email> getBothInboxSentMessages(List<TTMessage> ttMessages, int memberID,
                                                     TTMemberProfile ttMemberProfile, Brand brand)
        {
            var emails = new List<Email>();

            foreach (TTMessage message in ttMessages)
            {
                // If either the "to" or "from" member is the member who owns the messages, grab the
                // frozen username from the attribute bag passed in.  Otherwise just grab it from the current
                // member data.
                emails.Add(new Email(message.TTMessageID, (MailType) message.MailTypeID, message.InsertDate,
                                     message.FromMemberID == memberID
                                         ? ttMemberProfile.GetMemberAttributeText("UserName")
                                         : MemberHelper.GetMemberUsername(message.FromMemberID, brand),
                                     message.ToMemberID == memberID
                                         ? ttMemberProfile.GetMemberAttributeText("UserName")
                                         : MemberHelper.GetMemberUsername(message.ToMemberID, brand),
                                     message.FromMemberID,
                                     message.ToMemberID,
                                     (message.MailOption & (int) MailOption.VIP) == (int) MailOption.VIP));
            }

            return emails;
        }

        private List<Email> getInboxMessages(List<TTMessage> ttMessages, int memberID, TTMemberProfile ttMemberProfile,
                                             Brand brand)
        {
            var emails = new List<Email>();

            foreach (TTMessage message in ttMessages)
            {
                if (message.ToMemberID == memberID)
                {
                    emails.Add(new Email(message.TTMessageID, (MailType) message.MailTypeID, message.InsertDate,
                                         MemberHelper.GetMemberUsername(message.FromMemberID, brand),
                                         ttMemberProfile.GetMemberAttributeText("UserName"),
                                         message.FromMemberID,
                                         message.ToMemberID,
                                         (message.MailOption & (int) MailOption.VIP) == (int) MailOption.VIP));
                }
            }

            return emails;
        }

        private List<Email> getSentMessages(List<TTMessage> ttMessages, int memberID, TTMemberProfile ttMemberProfile,
                                            Brand brand)
        {
            var emails = new List<Email>();

            foreach (TTMessage message in ttMessages)
            {
                if (message.FromMemberID == memberID)
                {
                    emails.Add(new Email(message.TTMessageID, (MailType) message.MailTypeID, message.InsertDate,
                                         ttMemberProfile.GetMemberAttributeText("UserName"),
                                         MemberHelper.GetMemberUsername(message.ToMemberID, brand),
                                         message.FromMemberID,
                                         message.ToMemberID,
                                         (message.MailOption & (int) MailOption.VIP) == (int) MailOption.VIP));
                }
            }

            return emails;
        }

        public TTMessage RetrieveEmailMessage(int ttMessageID)
        {
            return DBApproveQueueSA.Instance.GetTTMessage(ttMessageID);
        }

        public ArrayList GetMemberEmailsByEmailSubstring(string emailString, bool adminsOnly)
        {
            var emailList = new ArrayList();
            int totalRows = 0;

            var cachedMembers = MemberSA.Instance.GetMembersByEmail(emailString, 0, 100, ref totalRows);
            if (cachedMembers == null)
                return emailList;

            foreach (object t in cachedMembers)
            {
                var cachedMem = (CachedMember) t;
                if (MemberPrivilegeSA.Instance.IsAdminByPrivilegesOnly(cachedMem.MemberID))
                {
                    emailList.Add(new {MemberID = cachedMem.MemberID, EmailAddress = cachedMem.EmailAddress});
                }
            }

            return emailList;
        }

        public TTTestHistory GetTTTestHistory(int adminMemberID)
        {
            var ttTestHistory = new TTTestHistory();
            ttTestHistory.TopNav = new SharedTopNav();
            ttTestHistory.TestResults = new TTTestResults()
                {
                    TTTestResultList = GetTTTestResultsForAdmin(adminMemberID)
                };

            return ttTestHistory;
        }
    }
}