﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Matchnet.BulkMail.ValueObjects;
using Matchnet.BulkMail.ServiceAdapters;
using AdminTool.Models.ModelViews.Reports;

namespace AdminTool.Models
{
    public class ReportsManager
    {
        public BulkMailBatchModelView GetBulkMailBatchForDate(DateTime batchDate)
        {
            BulkMailBatchModelView modelView = null;

            BulkMailBatch batch = BulkMailReportingSA.Instance.GetBulkMailBatchForDate(batchDate);
            if (batch != null)
            {
                modelView = new BulkMailBatchModelView();
                List<ProcessingStatusID> processingStatuses = (from sr in batch.SummaryRecords select sr.ProcessingStatus).Distinct().ToList();
                List<string> processingServers = (from sr in batch.SummaryRecords select sr.ProcessingServer).Distinct().ToList();

                Dictionary<ProcessingStatusID, int> totals = new Dictionary<ProcessingStatusID, int>();
                Dictionary<string, List<BulkMailBatchSummaryRecord>> groupedSummaryRecords = new Dictionary<string, List<BulkMailBatchSummaryRecord>>();

                foreach (ProcessingStatusID status in processingStatuses)
                {
                    int statusCount = (from sr in batch.SummaryRecords where sr.ProcessingStatus == status select sr.RecordCount).Sum();
                    totals.Add(status, statusCount);
                }

                foreach (string server in processingServers)
                {
                    List<BulkMailBatchSummaryRecord> summaryRecords = (from sr in batch.SummaryRecords where sr.ProcessingServer == server select sr).ToList();
                    groupedSummaryRecords.Add(server, summaryRecords);
                }

                modelView.Batch = batch;
                modelView.Totals = totals;
                modelView.GroupedSummaryRecords = groupedSummaryRecords;
            }

            return modelView;
        }
    }
}
