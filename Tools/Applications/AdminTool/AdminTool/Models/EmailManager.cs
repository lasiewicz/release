﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminTool.Models.ModelViews.Email;
using Matchnet.Email.ValueObjects;
using Matchnet.Email.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using AdminTool.Models.ModelViews;
using System.Collections;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Configuration.ServiceAdapters;
using System.Configuration;

namespace AdminTool.Models
{
    public class EmailManager : ManagerBase
    {
        private const int PAGE_SIZE = 10;
        
        private Member _member;
        private MemberSiteInfo _memberSiteInfo;
        
        public int SiteID { get; set; }
        public int MemberID { get; set; }
        public Member Member { get { return _member; } }
        public MemberSiteInfo MemberSiteInfo { get { return _memberSiteInfo; } }
        
        public EmailManager(int siteID, int memberID)
        {
            try
            {
                this.SiteID = siteID;
                this.MemberID = memberID;

                _member = MemberHelper.GetMember(MemberID, MemberLoadFlags.None);
                if (_member == null)
                {
                    throw new Exception("Member not found: " + memberID.ToString());
                }
                _memberSiteInfo = MemberHelper.GetMemberSiteInfo(_member, SiteID);
            }
            catch (Exception ex)
            {
                throw new Exception("Error in EmailManager.EmailManager().", ex);
            }

        }

        private void DoPagination(EmailList emailList, List<Email> allEmail, int rowIndex, int pageSize)
        {
            // first calculate the total number of pages
            int totalPages = 0;
            totalPages = allEmail.Count / pageSize;
            if (allEmail.Count % pageSize > 0)
            {
                totalPages++;
            }
            emailList.TotalPageCount = totalPages;
            emailList.PageSize = pageSize;
            emailList.TotalEmailCount = allEmail.Count;
            emailList.RowIndex = rowIndex;

            // load the EmailList object with the actual emails
            if (rowIndex < allEmail.Count)
            {
                // we have a full page worth or not
                if (rowIndex + pageSize - 1 < allEmail.Count)
                {
                    emailList.Emails = allEmail.GetRange(rowIndex, pageSize);
                    emailList.TotalReturnedInPage = pageSize;
                }
                else
                {
                    emailList.Emails = allEmail.GetRange(rowIndex, allEmail.Count - rowIndex);
                    emailList.TotalReturnedInPage = allEmail.Count - rowIndex;
                }
                
            }
        }

        private void MergeEmails(int folderID, List<Email> allEmails)
        {
            MemberMailLog mailLog = EmailLogSA.Instance.RetrieveEmailLog(MemberID, g.GetBrand(SiteID).Site.Community.CommunityID);
            EmailLogEntryCollection col = null;

            if (mailLog != null)
            {
                switch ((EmailLogType)folderID)
                {
                    case EmailLogType.Both:
                        col = mailLog.Both;
                        break;
                    case EmailLogType.Inbox:
                        col = mailLog.Received;
                        break;
                    case EmailLogType.Sent:
                        col = mailLog.Sent;
                        break;
                }

                if (col != null)
                {
                    foreach (EmailLogEntry entry in col)
                    {
                        allEmails.Add(new Email(entry.MailID, (MailType)entry.MailType, entry.InsertDate,
                            MemberHelper.GetMemberUsername(entry.FromMemberID, g.GetBrand(SiteID)),
                            MemberHelper.GetMemberUsername(entry.ToMemberID, g.GetBrand(SiteID)),
                            entry.FromMemberID, entry.ToMemberID, (entry.MailOption & (int)MailOption.VIP) == (int)MailOption.VIP));
                    }
                }
            }
       
        }

        public EmailList GetEmails(int rowIndex, int folderID)
        {
            EmailList emailList = new EmailList();
            emailList.MemberID = MemberID;
            emailList.MemberSiteInfo = _memberSiteInfo;
            emailList.BrandID = g.GetBrand(SiteID).BrandID;
            emailList.EmailAddress = Member.EmailAddress;
            emailList.FolderID = folderID;
            emailList.JSMessageAttachmentURLPath = ConfigurationManager.AppSettings["MessageAttachmentURLPath"];
            
            // let's merge all the emails we are suppose to get include the corresponding vip folder
            List<Email> allEmails = new List<Email>();
            MergeEmails(folderID, allEmails);

            // sort InsertDate desc
            allEmails.Sort((x, y) => (y.MessageDate.CompareTo(x.MessageDate)));

            // pagination
            DoPagination(emailList, allEmails, rowIndex, PAGE_SIZE);
            

            return emailList;
        }

        public static bool IsReadRecieptPremiumFeatureEnabled(Brand brand)
        {
            try
            {
                return Convert.ToBoolean((RuntimeSettings.GetSetting("ENABLE_READ_RECEIPTS_PREMIUM_SERVICE_FEATURE", brand.Site.Community.CommunityID, brand.Site.SiteID, brand.BrandID)));
            }
            catch
            {
                return false; //likely setting doesn't exist in this environment
            }
        }



        public static MailMember ConvertMessageMemberToMailMember(int memberID, MessageMember message, Brand brand)
        {
            //int memberToFetch = 0;

            //if (memberID != message.FromMemberID)
            //    memberToFetch = message.FromMemberID;
            //else
            //    memberToFetch = message.ToMemberID;

            //var member = MemberSA.Instance.GetCachedMember(memberToFetch, MemberLoadFlags.None);

            var mailMember = new MailMember
            {
                Id = message.MessageListID,
                FromMemberId = message.FromMemberID,
                ToMemberId = message.ToMemberID,
                GroupId = message.GroupID,
                InsertDate = message.InsertDate.ToString(),
                MessageStatus = (int)message.StatusMask,
                MailType = Enum.GetName(typeof(MailType), message.MailTypeID),
                Username = ""// member.Username
            };

            return mailMember;
        }


        public static List<MailMessageV2> ConvertEmailMessageListToMailMessageList(int memberID, EmailMessageList emailMessageList)
        {
            List<MailMessageV2> messageHistoryList = new List<MailMessageV2>();

            if (emailMessageList != null && emailMessageList.Count > 0)
            {
                foreach (EmailMessage message in emailMessageList)
                {
                    
                    var mailMessage = new MailMessageV2
                    {
                        Id = message.MessageList.MessageListID,
                        Body = message.Message.MessageBody,
                        Subject = message.Message.MessageHeader,
                        InsertDate = message.Message.InsertDate.ToString(),
                        MailType = Enum.GetName(typeof(MailType), message.MailTypeID),
                        SenderId = message.FromMemberID,
                        MemberFolderId = message.MemberFolderID,
                        FromMemberId = message.FromMemberID,
                        ToMemberId = message.ToMemberID,
                        FromUserName = message.MessageList.FromUsername,
                        ToUserName = message.MessageList.ToUsername
                    };
                    messageHistoryList.Add(mailMessage);
                }
            }

            return messageHistoryList;
        }

        
    }
}
