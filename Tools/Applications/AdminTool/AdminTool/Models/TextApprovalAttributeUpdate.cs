﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;

namespace AdminTool.Models
{
    public class TextApprovalAttributeUpdate
    {
        public string Name { get; set; }
        public int AttributeGroupID { get; set; }
        public string OriginalValue { get; set; }
        public string UpdatedValue { get; set; }
        public bool TextAcceptable { get; set; }
        public int StatusMask { get; set; }
        public int LanguageID { get; set; }
        public bool AttributedUnapproved { get; set; }
        // values at the time of approving the free text attribute
        public string IP { get; set; }
        public int ProfileRegionID { get; set; }
        public int GenderMask { get; set; }
        public int MaritalStatus { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string OccupationDescription { get; set; }
        public int EducationLevel { get; set; }
        public int Religion { get; set; }
        public int Ethnicity { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string SubscriberStatus { get; set; }
        public int DaysSinceFirstSubscription { get; set; }
        public string BillingPhoneNumber { get; set; }
        public bool SaveAttribute { get; set; }
                        
        public TextApprovalAttributeUpdate() { }

        public TextApprovalAttributeUpdate(string name, int attributeGroupID, string originalValue, string updatedValue, bool textAcceptable, int statusMask, int languageID, bool attributedUnapproved)
        {
            Name = name;
            AttributeGroupID = attributeGroupID;
            OriginalValue = originalValue;
            UpdatedValue = updatedValue;
            TextAcceptable = textAcceptable;
            StatusMask = statusMask;
            LanguageID = languageID;
            AttributedUnapproved = attributedUnapproved;
        }

        public TextApprovalAttributeUpdate(string name, int attributeGroupID, string originalValue, string updatedValue, bool textAcceptable, int statusMask, int languageID, bool attributedUnapproved, string ip, int profileRegionID, int genderMask, int maritalStatus, string firstName, string lastName, string email, string occupationDescription, int educationLevel, int religion, int ethnicity)
        {
            Name = name;
            AttributeGroupID = attributeGroupID;
            OriginalValue = originalValue;
            UpdatedValue = updatedValue;
            TextAcceptable = textAcceptable;
            StatusMask = statusMask;
            LanguageID = languageID;
            AttributedUnapproved = attributedUnapproved;
            IP = ip;
            ProfileRegionID = profileRegionID;
            GenderMask = genderMask;
            MaritalStatus = maritalStatus;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            OccupationDescription = occupationDescription;
            EducationLevel = educationLevel;
            Religion = religion;
            Ethnicity = ethnicity;
        }

        public TextApprovalAttributeUpdate(string name, int attributeGroupID, string originalValue, string updatedValue, bool textAcceptable, int statusMask, int languageID, bool attributedUnapproved, string ip, int profileRegionID, int genderMask, int maritalStatus, string firstName, string lastName, string email, string occupationDescription, int educationLevel, int religion, int ethnicity,
            DateTime registrationDate, string subscriberStatus, int daysSinceFirstSubscription, string billingPhoneNumber)
        {
            Name = name;
            AttributeGroupID = attributeGroupID;
            OriginalValue = originalValue;
            UpdatedValue = updatedValue;
            TextAcceptable = textAcceptable;
            StatusMask = statusMask;
            LanguageID = languageID;
            AttributedUnapproved = attributedUnapproved;
            IP = ip;
            ProfileRegionID = profileRegionID;
            GenderMask = genderMask;
            MaritalStatus = maritalStatus;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            OccupationDescription = occupationDescription;
            EducationLevel = educationLevel;
            Religion = religion;
            Ethnicity = ethnicity;
            RegistrationDate  = registrationDate;
            SubscriberStatus  = subscriberStatus;
            DaysSinceFirstSubscription  = daysSinceFirstSubscription;
            BillingPhoneNumber = billingPhoneNumber;
        }


    }
}
