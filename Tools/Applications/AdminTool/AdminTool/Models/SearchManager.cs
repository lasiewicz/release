﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminTool.Models.ModelViews.Search;
using System.Collections;
using Matchnet;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ValueObjects;
using Spark.CommonLibrary;
using System.Web.Mvc;

namespace AdminTool.Models
{
    public class SearchManager : ManagerBase
    {
        public SearchResult ProcessSearch(SearchData searchData)
        {
            SearchResult searchResult = new SearchResult();
            searchResult.isSearched = true;

            try
            {
                //int startRow = GeneralHelper.ConvertPageToStartRow(searchData.PageNumber, searchData.ResultsPerPage);

                #region delete later
                // searching by MemberID will go through our old method
                //if (searchData.MemberID > 0) // don't have to worry about sorting because this is a exact memberID match only
                //{
                //    results = Matchnet.Member.ServiceAdapters.MemberSA.Instance.GetMembersByMemberID(searchData.MemberID, ref totalRows);

                //    searchResult.TotalRows = totalRows;
                //    searchResult.TotalPages = GeneralHelper.ConvertTotalRowToTotalPage(totalRows, searchData.ResultsPerPage);

                //    //get member information
                //    if (results != null)
                //    {
                //        foreach (Matchnet.Member.ValueObjects.CachedMember cacheMember in results)
                //        {
                //            searchResult.SearchMembers.Add(PopulateSearchMember(cacheMember.MemberID));
                //        }
                //    }

                //    return searchResult;
                //}
                #endregion

                // our new method of searching
                // single field search first; check in this order MemberID, Email, Username, Phone, IP
                #region Setting search parameters
                string email = string.Empty;
                string userName = string.Empty;
                string memberId = string.Empty;
                string phone = string.Empty;
                string firstName = string.Empty;
                string lastName = string.Empty;
                string zipcode = string.Empty;
                string city = string.Empty;
                string ipAddress = string.Empty;
                int communityId = searchData.CommunityID == 0 ? Constants.NULL_INT : searchData.CommunityID;
                bool exactUsername = false;

                if(searchData.MemberID > 0)
                {
                    memberId = searchData.MemberID.ToString();
                }
                else if(!string.IsNullOrEmpty(searchData.Email))
                {
                    email = searchData.Email;
                }
                else if(!string.IsNullOrEmpty(searchData.UserName))
                {
                    userName = searchData.UserName;
                    exactUsername = searchData.ExactUsernameOnly;
                }
                else if(!string.IsNullOrEmpty(searchData.PhoneNumber))
                {
                    phone = searchData.PhoneNumber;
                }
                else if(searchData.IPAddressInt != null)
                {
                    ipAddress = searchData.IPAddressInt.ToString();
                }
                else
                {
                    // this means, we should perform a combination search
                    firstName = searchData.FirstName;
                    lastName = searchData.LastName;
                    zipcode = searchData.ZipCode;
                    city = searchData.CityName;
                }
                #endregion

                // make sure we have at least 1 search parameter so we don't cause critical error in the DB
                if (
                    string.IsNullOrEmpty(
                        string.Concat(new string[]
                                          {
                                              email, userName, memberId, phone, firstName, lastName, zipcode, city,
                                              ipAddress
                                          })))
                {
                    searchResult.TotalRows = 0;
                    searchResult.TotalPages = 0;
                    return searchResult;
                }

                var returnedMemberIds = searchData.PerformLegacySearch
                                            ? PerformLegacySearch(memberId, email, userName, exactUsername,
                                                                  searchData.PageNumber, searchData.ResultsPerPage)
                                            : MemberSA.Instance.PerformAdminSearchPagination(email, userName, memberId,
                                                                                             phone, firstName,
                                                                                             lastName, zipcode, city,
                                                                                             ipAddress, communityId,
                                                                                             exactUsername,
                                                                                             (int)
                                                                                             searchData.FilterValue,
                                                                                             searchData.PageNumber,
                                                                                             searchData.ResultsPerPage);

                if(returnedMemberIds == null && searchData.PageNumber == 1)
                {
                    searchResult.TotalRows = 0;
                    searchResult.TotalPages = 0;
                    return searchResult;
                }
                
                // if we got no row back but we are not on the first page, take the admin back to the previous page with a message.
                if(returnedMemberIds == null && searchData.PageNumber > 1)
                {
                    searchData.PageNumber = searchData.PageNumber - 1;
                    returnedMemberIds = searchData.PerformLegacySearch
                                            ? PerformLegacySearch(memberId, email, userName, exactUsername,
                                                                  searchData.PageNumber, searchData.ResultsPerPage)
                                            : MemberSA.Instance.PerformAdminSearchPagination(email, userName, memberId,
                                                                                             phone, firstName,
                                                                                             lastName, zipcode, city,
                                                                                             ipAddress, communityId,
                                                                                             exactUsername,
                                                                                             (int)
                                                                                             searchData.FilterValue,
                                                                                             searchData.PageNumber,
                                                                                             searchData.ResultsPerPage);
                    searchResult.SearchResultEndReached = true;

                }

                // we shouldn't have a null here, but let's check once more to guard against Member Service having problems.
                if (returnedMemberIds == null)
                {
                    searchResult.TotalRows = 0;
                    searchResult.TotalPages = 0;
                    return searchResult;
                }

                if(searchData.PageNumber == 1 && returnedMemberIds.Count() == 1)
                {
                    searchResult.OnlyOneResultReturned = true;
                }

                // We are not using TotalRows property. We are using it now to indicate how many we returned for this current page.
                searchResult.TotalRows = returnedMemberIds.Count();
                    
                //searchResult.TotalPages = GeneralHelper.ConvertTotalRowToTotalPage(returnedMemberIds.Count(), searchData.ResultsPerPage);
                // This number doesn't matter; we just want to indicate that we got some results back. With our new method, since
                // don't know the total rows, we don't know the total pages either.
                searchResult.TotalPages = 1;

                #region Old way of grabbing one page from max rows returned
                // startRow is 1-based so let's make it 0-based
                //startRow--;
                //var endIndex = startRow + searchData.ResultsPerPage > returnedMemberIds.Count()
                //                   ? returnedMemberIds.Count()
                //                   : startRow + searchData.ResultsPerPage;
                
                //// take the time to populate member data only for the requested page
                //for(int i=startRow; i<endIndex; i++)
                //{
                //    searchResult.SearchMembers.Add(PopulateSearchMember(returnedMemberIds[i]));
                //}
                #endregion

                foreach(var oneMemberId in returnedMemberIds)
                {
                    searchResult.SearchMembers.Add(PopulateSearchMember(oneMemberId));
                }

                return searchResult;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in SearchManager.ProcessSearch()", ex);
            }

            return searchResult;
        }

        private int[] PerformLegacySearch(string memberId, string email, string userName, bool exactUsername, int pageNumber, int pageSize)
        {
            int totalRows = 0;
            var startRow = GeneralHelper.ConvertPageToStartRow(pageNumber, pageSize);

            if(!string.IsNullOrEmpty(memberId))
            {
                var cachedMembers = MemberSA.Instance.GetMembersByMemberID(int.Parse(memberId), ref totalRows);
                return GetMemberIdFromCachedMember(cachedMembers);
            }

            if(!string.IsNullOrEmpty(email))
            {
                var cachedMembers = MemberSA.Instance.GetMembersByEmail(email, startRow, pageSize, ref totalRows, true);
                return GetMemberIdFromCachedMember(cachedMembers);
            }

            if(!string.IsNullOrEmpty(userName))
            {
                var cachedMembers = MemberSA.Instance.GetMembersByUserName(userName, startRow, pageSize, ref totalRows,
                                                                           true, exactUsername);
                return GetMemberIdFromCachedMember(cachedMembers);
            }

            return null;
        }

        private int[] GetMemberIdFromCachedMember(ArrayList cachedMembers)
        {
            if (cachedMembers == null)
                return null;

            int[] memberIds = new int[cachedMembers.Count];
            for(int i=0; i<cachedMembers.Count; i++)
            {
                var cachedMember = (CachedMember) cachedMembers[i];
                memberIds[i] = cachedMember.MemberID;
            }

            return memberIds;
        }

        private SearchMember PopulateSearchMember(int memberID)
        {
            SearchMember searchMember = new SearchMember();
            Member member = MemberHelper.GetMember(memberID, MemberLoadFlags.None);

            //non-site specific info
            searchMember.MemberID = member.MemberID;
            searchMember.EmailAddress = member.EmailAddress;

            //sites
            searchMember.MemberSites = MemberHelper.GetMemberSiteInfoList(member);
            if (searchMember.MemberSites.Count > 0)
            {
                int defaultSiteID = (int)searchMember.MemberSites[0].SiteID;

                Brand brand = g.GetBrand(defaultSiteID);

                //site specific info for default site
                searchMember.FirstName = member.GetAttributeText(brand, "SiteFirstName");
                searchMember.Age = MemberHelper.GetAge(member, brand);
                searchMember.Location = MemberHelper.GetRegionString(member, brand, false, true, false);
                searchMember.Gender = MemberHelper.GetGender(member, brand);
                searchMember.LastName = member.GetAttributeText(brand, "SiteLastName");
                searchMember.UserName = member.GetUserName(brand); //replace username with brand specific one

            }

            return searchMember;
        }

        
    }
}
