﻿#region

using System.Runtime.Serialization;
using RestSharp;

#endregion

namespace AdminTool.Models.SparkAPI
{
    public class ApiResult : StatusCodeResult
    {
        [DataMember(Name = "data")]
        public JsonObject Data { get; set; }
    }
}