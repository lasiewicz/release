﻿#region

using RestSharp;
using Spark.Common.RestConsumer.V2.Models;
using System;
using System.Collections.Generic;
using System.Net;

#endregion

namespace AdminTool.Models.SparkAPI
{
    public abstract class ApiBase
    {
        private readonly ApiClientBase _apiClient;

        protected ApiBase(ApiClientBase apiClient)
        {
            _apiClient = apiClient;
        }

        protected ResponseWrapper<T> Get<T>(string url, Dictionary<string, string> urlParams, Dictionary<string, string> data, string version="V2.1") where T : new()
        {
            return Execute<T>(url, urlParams, data, Method.GET);
        }

        protected ResponseWrapper<T> Post<T>(string url, Dictionary<string, string> urlParams, Dictionary<string, string> data, string version = "V2.1") where T : new()
        {
            return Execute<T>(url, urlParams, data, Method.POST);
        }

        protected ResponseWrapper<T> Put<T>(string url, Dictionary<string, string> urlParams, Dictionary<string, string> data, string version = "V2.1") where T : new()
        {
            return Execute<T>(url, urlParams, data, Method.PUT);
        }

        private ResponseWrapper<T> Execute<T>(string url, Dictionary<string, string> urlParams, Dictionary<string, string> data, Method method, string version = "V2.1")
        {
            var response = _apiClient.Execute<ResponseWrapper<T>>(url, urlParams, data, method, version);

            // Check to see if unauthorize call to API is attempted 
            // (could be access token required, expired, etc.)
            if (response.Code == (int)HttpStatusCode.Unauthorized)
                throw new AuthorizationException(response.Error.Code, ParseError(response.Error));
            if (response.Code != 0 && response.Code != (int)HttpStatusCode.OK)
                throw new ApiException(ParseError(response.Error))
                {
                    ErrorCode = response.Error.Code,
                    HttpStatusCode = response.Code,
                    Type = this.GetType()
                };

            return response;
        }

        private static string ParseError(Error error)
        {
            return string.Format("{0} - {1}", error.Code, error.Message);
        }


        public class AuthorizationException : Exception
        {
            public int ErrorCode { get; set; }

            public AuthorizationException(string message)
                : base(message)
            {

            }

            public AuthorizationException(int errorCode, string message)
                : base(message)
            {
                ErrorCode = errorCode;
            }
        }

        public class ApiException : Exception
        {
            public int HttpStatusCode { get; set; }
            public int ErrorCode { get; set; }
            public Type Type { get; set; }

            public ApiException(string message)
                : base(message)
            {

            }
        }
    }
}