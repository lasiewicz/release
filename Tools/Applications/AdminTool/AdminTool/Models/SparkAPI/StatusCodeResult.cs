﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AdminTool.Models.SparkAPI
{
    public class StatusCodeResult
    {
        [DataMember(Name = "code")]
        public int Code { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }
    }
}