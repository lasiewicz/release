﻿#region

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using RestSharp;

#endregion

namespace AdminTool.Models.SparkAPI
{
    public abstract class ApiClientBase
    {
        private readonly int _applicationId;
        private readonly string _baseUrl;
        private readonly string _clientSecret;

        protected ApiClientBase(string baseUrl, int applicationId, string clientSecret)
        {
            _baseUrl = baseUrl;
            _applicationId = applicationId;
            _clientSecret = clientSecret;
        }

        /// <summary>
        ///     non generic type call
        /// </summary>
        /// <param name="url"></param>
        /// <param name="urlParams"></param>
        /// <param name="data"></param>
        /// <param name="method"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public string Execute(string url, Dictionary<string, string> urlParams, Dictionary<string, string> data,
            Method method, string version = "V2.1")
        {
            var client = new RestClient(_baseUrl);
            var request = new RestRequest(url, method);
            request.AddHeader("Accept", version);

            AddParameters(request, data);
            AddUrlSegments(request, urlParams);
            AddApplicationId(request, method);
            AddClientSecret(request, method);

            var response = client.Execute(request);
            return response.Content;
        }

        /// <summary>
        ///     with generic type support call
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="urlParams"></param>
        /// <param name="data"></param>
        /// <param name="method"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public T Execute<T>(string url, Dictionary<string, string> urlParams, Dictionary<string, string> data,
            Method method, string version = "V2.1") where T : new()
        {
            var client = new RestClient(_baseUrl);
            var request = new RestRequest(url, method);

            // ignore ssl errors from using self ssl in dev/stage envs.
            if (ConfigurationManager.AppSettings["Environment"] != "prod" && ConfigurationManager.AppSettings["Environment"] != "preprod")
            {
                ServicePointManager.ServerCertificateValidationCallback +=
                    (sender, certificate, chain, sslPolicyErrors) => true;
            }

            request.AddHeader("Accept", version);

            client.AddHandler("application/json", new RestDeserializer());

            AddParameters(request, data);
            AddUrlSegments(request, urlParams);
            AddApplicationId(request, method);
            AddClientSecret(request, method);

            try
            {
                var response = client.Execute<T>(request);
                return response.Data;
            }
            catch (Exception exception)
            {
                throw new Exception(
                    string.Format("Error communicating with Mingle API. " +
                                  "url:{0} endpoint:{1} applicationId:{2} clientSecret:{3}",
                    _baseUrl, url, _applicationId, _clientSecret), exception);
            }
        }

        private static void AddParameters(IRestRequest request, Dictionary<string, string> data)
        {
            if (data == null)
                return;

            foreach (var param in data)
                request.AddParameter(param.Key, param.Value);
        }

        private static void AddUrlSegments(IRestRequest request, Dictionary<string, string> urlParams)
        {
            if (urlParams == null)
                return;

            foreach (var param in urlParams)
                request.AddUrlSegment(param.Key, param.Value);
        }

        private void AddApplicationId(IRestRequest request, Method method)
        {
            var parameter = request.Parameters.Find(
                param => (string.Compare(param.Name, "access_token", StringComparison.OrdinalIgnoreCase) == 0 ||
                          string.Compare(param.Name, "accesstoken", StringComparison.OrdinalIgnoreCase) == 0));

            if (parameter != null)
                return;

            request.AddParameter("applicationId", _applicationId);
            // some Mingle API endpoints inconsistently expect client_Id or clientId instead of applicationId 
            // todo: have them clean up those endpoints.. this is a bit ridiculous
            request.AddParameter("clientId", _applicationId);
            request.AddParameter("client_Id", _applicationId);

            if (method != Method.POST && method != Method.PUT) return;
            var url = request.Resource + "?applicationId=" + _applicationId;

            request.Parameters.RemoveAt(request.Parameters.Count - 1);
            request.Resource = url;
            request.AddUrlSegment("applicationId", _applicationId.ToString());
        }

        private void AddClientSecret(IRestRequest request, Method method)
        {
            var parameter = request.Parameters.Find(
                param => (String.Compare(param.Name, "client_secret", StringComparison.OrdinalIgnoreCase) == 0 ||
                          String.Compare(param.Name, "client_secret", StringComparison.OrdinalIgnoreCase) == 0));

            if (parameter != null)
                return;

            request.AddParameter("client_secret", _clientSecret);
            if (method != Method.POST && method != Method.PUT) return;
            var url = request.Resource + "&client_secret=" + _clientSecret;

            request.Parameters.RemoveAt(request.Parameters.Count - 1);
            request.Resource = url;
            request.AddUrlSegment("client_secret", _clientSecret);
        }
    }
}