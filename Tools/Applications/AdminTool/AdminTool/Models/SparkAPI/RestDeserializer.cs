﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.SparkAPI
{
    public class RestDeserializer : IDeserializer
    {
        public string RootElement { get; set; }
        public string Namespace { get; set; }
        public string DateFormat { get; set; }
        //public CultureInfo Culture { get; set; }

        public RestDeserializer()
        {
            //Culture = CultureInfo.InvariantCulture;
        }

        public T Deserialize<T>(IRestResponse response)
        {
            var temp = JsonConvert.DeserializeObject<T>(response.Content);
            return temp;
        }
    }
}