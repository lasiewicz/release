﻿#region

using System;
using System.Configuration;

#endregion

namespace AdminTool.Models.SparkAPI
{
    /// <summary>
    ///     For making LA API Calls
    /// </summary>
    internal sealed class ApiClient : ApiClientBase
    {
        private static readonly ApiClient Instance = new ApiClient();
        // todo: shouldn't we get the rest Url from Settings?
        private ApiClient()
            : base(ConfigurationManager.AppSettings["RESTAuthority"],
                Convert.ToInt32(ConfigurationManager.AppSettings["ApplicationID"]),
                ConfigurationManager.AppSettings["ClientSecret"])
        {
        }

        public static ApiClient GetInstance()
        {
            return Instance;
        }
    }
}