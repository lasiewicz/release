﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Spark.Common.FraudService;

namespace AdminTool.Models.SparkAPI.EndpointClients
{
    public class ApiFraudInfo : ApiBase
    {
        public ApiFraudInfo(ApiClientBase apiClient)
            : base(apiClient)
        {
        }

        public FraudDetailedResponse GetMemberFraudInfo(int memberId, int riskInquiryTypeID, int siteId, string bindingID)
        {
            var fraudInfoRequestDict = new Dictionary<string, string>
            {
                {"memberid", memberId.ToString()},
                {"riskinquirytypeid", riskInquiryTypeID.ToString()},
                {"siteid", siteId.ToString()},
                {"bindingid", bindingID}
            };


            //return Post<FraudDetailedResponse>("/brandId/" + brandId.ToString() + RestUrls.MEMBER_FRAUD_INFO, null, fraudInfoRequestDict).Data;
            return Post<FraudDetailedResponse>("/brandId/" + "1003" + RestUrls.MEMBER_FRAUD_INFO, null, fraudInfoRequestDict).Data;
           
        }


     
    }
}