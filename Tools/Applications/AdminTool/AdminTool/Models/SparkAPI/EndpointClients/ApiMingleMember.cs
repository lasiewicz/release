﻿#region

using System.Collections.Generic;

#endregion

namespace AdminTool.Models.SparkAPI.EndpointClients
{
    public class ApiMingleMember : ApiBase
    {
        public ApiMingleMember(ApiClientBase apiClient)
            : base(apiClient)
        {
        }

        public MingleFullProfile GetFullProfileAttributeSet(int memberId, int siteId)
        {
            var url = string.Format("v3/{0}/profile/attributesetinternal/fullprofile/{1}",
                siteId, memberId);

            return Get<MingleFullProfile>(url, null, null).Data;
        }

        public MingleFraudProfile GetFraudProfileAttributeSet(int memberId, int siteId)
        {
            var url = string.Format("v3/{0}/profile/fraudProfile/{1}",
                siteId, memberId);

            return Get<MingleFraudProfile>(url, null, null).Data;
        }
    }

    /// <summary>
    ///     Instead of using Spark.Common.RestConsumer.V2.Models.Mingle.Profile.FullProfile
    ///     which generates too many JSON conversion errors. refactor later
    /// </summary>
    public class MingleFullProfile
    {
        public int memberId { get; set; }
        public string username { get; set; }
        public object primaryPhoto { get; set; }
        public int photoCount { get; set; }
        public string maritalStatus { get; set; }
        public string gender { get; set; }
        public string seekingGender { get; set; }
        public List<object> lookingFor { get; set; }
        public int age { get; set; }
        public string location { get; set; }
        public string zipCode { get; set; }
        public string lastLoggedIn { get; set; }
        public string lastUpdated { get; set; }
        public string registrationDate { get; set; }
        public string isPayingMember { get; set; }
        public bool isOnline { get; set; }
        public int matchRating { get; set; }
        public int distance { get; set; }
        public bool isHighlighted { get; set; }
        public string secret_admirer { get; set; }
        public bool selfSuspendedFlag { get; set; }
        public bool adminSuspendedFlag { get; set; }
        public object blockContact { get; set; }
        public object blockContactByTarget { get; set; }
        public object hideSearch { get; set; }
        public object hideSearchByTarget { get; set; }
        public bool isViewersFavorite { get; set; }
        public string height { get; set; }
        public string body_type { get; set; }
        public string hair { get; set; }
        public string eyes { get; set; }
        public string about_children { get; set; }
        public string children { get; set; }
        public string children_at_home { get; set; }
        public string smoke { get; set; }
        public string drink { get; set; }
        public string church_importance { get; set; }
        public string church_activity { get; set; }
        public string education { get; set; }
        public string occupation { get; set; }
        public List<string> ethnicity { get; set; }
        public List<object> language { get; set; }
        public string self_description { get; set; }
        public List<object> music { get; set; }
        public string favorite_musicians { get; set; }
        public List<object> movie { get; set; }
        public string favorite_movies { get; set; }
        public string favorite_tv { get; set; }
        public List<object> outdoors { get; set; }
        public List<object> indoors { get; set; }
        public string season { get; set; }
        public string great_trip { get; set; }
        public List<object> food { get; set; }
        public string favorite_restaurants { get; set; }
        public string looks { get; set; }
        public string mentality { get; set; }
        public string political { get; set; }
        public string schools_attended { get; set; }
        public string living_quarters { get; set; }
        public string prefer_to_live { get; set; }
        public string punctual { get; set; }
        public string planning { get; set; }
        public string trendy { get; set; }
        public string wants_age { get; set; }
        public string wants_distance { get; set; }
        public string wants_height { get; set; }
        public List<object> wants_body_type { get; set; }
        public List<object> wants_education { get; set; }
        public List<object> wants_marital_status { get; set; }
        public List<object> wants_church_importance { get; set; }
        public List<object> wants_church_activity { get; set; }
        public List<object> wants_ethnicity { get; set; }
        public List<object> wants_smoke { get; set; }
        public List<object> wants_drink { get; set; }
        public string greeting { get; set; }
        public string first_date { get; set; }
        public string past_relationships { get; set; }
        public string christian_meaning { get; set; }
        public string five_years { get; set; }
        public string favorite_scripture { get; set; }
        public string anythingelse { get; set; }
    }

    public class MingleFraudProfile
    {
        public string UserName { get; set; }
        public string Gender { get; set; }
        public string Ethnicity { get; set; }
        public string BodyStyle { get; set; }
        public string Billing_Phone { get; set; }
        public int Age { get; set; }
        public string Site { get; set; }
        public string Hair { get; set; }
        public string Height { get; set; }
        public string AboutMe { get; set; }
        public string ProfileLocation { get; set; }
        public string IPAddress { get; set; }
        public string SubStatus { get; set; }
        public bool PassedFraud { get; set; }
        public string FraudScore { get; set; }
        public string IovationResult { get; set; }
    }
}