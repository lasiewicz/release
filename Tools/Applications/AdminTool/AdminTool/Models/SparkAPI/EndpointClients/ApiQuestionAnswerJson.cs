﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.PremiumServiceSearch.ValueObjects.QuestionAnswer;
using Spark.Common.RestConsumer.V2.Models.Subscription;

namespace AdminTool.Models.SparkAPI.EndpointClients
{
    public class ApiQuestionAnswerJson : ApiBase
    {

        public StatusCodeResult AdminUpdate(Answer answer, int brandId)
        {
            
            var questionAnswerDict = new Dictionary<string, string>
            {
                   {"answerid", answer.AnswerID.ToString()},
                   {"questionid", answer.QuestionID.ToString()},
                   {"memberid", answer.MemberID.ToString()},
                   {"answervalue", answer.AnswerValue},
                   {"answervaluepending",answer.AnswerValuePending}, 
                   {"adminmemberid",answer.AdminMemberID.ToString()}, 
                   {"answertype", answer.AnswerType.ToString()},
                   {"answerstatus", answer.AnswerStatus.ToString() },
                   {"isdirty", answer.IsDirty.ToString() },
                   {"gendermask", answer.GenderMask.ToString()},
                   {"globalstatusmask", answer.GlobalStatusMask.ToString()},
                   {"selfsuspendedflag", answer.SelfSuspendedFlag.ToString()},
                   {"hasapprovedphotos", answer.HasApprovedPhotos.ToString()},
            };

            return Put<StatusCodeResult>("/brandId/" + brandId.ToString() + RestUrls.ADMIN_QUESTIONANSWER_UPDATE, null, questionAnswerDict).Data;
        }

        public ApiQuestionAnswerJson(ApiClientBase apiClient) : base(apiClient)
        {
        }
    }
}