﻿#region

using System;
using System.Configuration;
using Matchnet.Configuration.ServiceAdapters;

#endregion

namespace AdminTool.Models.SparkAPI
{
    /// <summary>
    ///     For making Mingle API calls
    /// </summary>
    internal sealed class ApiClientMingle : ApiClientBase
    {
        private static readonly ApiClientMingle Instance = new ApiClientMingle();

        private ApiClientMingle()
            : base(ConfigurationManager.AppSettings["Mingle_Api_Url"],
                Convert.ToInt32(ConfigurationManager.AppSettings["Mingle_Api_ApplicationId"]),
                ConfigurationManager.AppSettings["Mingle_Api_Client_Secret"])
        {
        }

        public static ApiClientMingle GetInstance()
        {
            return Instance;
        }
    }
}