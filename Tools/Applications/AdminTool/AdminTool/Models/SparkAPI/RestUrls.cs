﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.SparkAPI
{
    public static class RestUrls
    {
        public const string VALIDATE_ACCESS_TOKEN = "/oauth2/accesstoken/validate";
        public const string MEMBERS_ONLINE_COUNT = "/membersonline/count";
        public const string MEMBERS_ONLINE_LIST = "/membersonline/list";
        public const string MAIL_FOLDER = "/mail/folder/{folderId}/pagesize/{pageSize}/page/{page}";
        public const string MAIL_MESSAGE = "/mail/message/{messageListId}";
        public const string PROFILE_ATTRIBUTES = "/profile/attributeset/{attributeSetName}/{targetMemberId}";
        public const string HOTLIST = "/hotlist/{hotListCategory}/pagesize/{pageSize}/pagenumber/{page}";
        public const string MATCH_RESULT = "/match/results/pagesize/{pageSize}/page/{page}";
        public const string MATCH_PREFERENCES = "/match/preferences";
        public const string SEARCH_RESULTS = "/search/results";
        public const string SECRET_ADMIRERS = "/search/secretadmirer";
        public const string PHOTOS_LIST = "/profile/photos/{targetMemberId}";
        public const string POST_PAYMENT = "/subscription/PaymentUiRedirectData";
        public const string TRANSACTION_INFO = "/subscription/transactionInfo/{orderId}";
        public const string ADMIN_POST_PAYMENT = "/subscription/adminpaymentuiredirectdata";
        public const string ADMIN_QUESTIONANSWER_UPDATE = "/premium/questionanswer/admin";
        public const string GETMEMBERORDERHISTORYBYMEMBERID = "GetMemberOrderHistoryByMemberID";
        public const string MEMBER_FRAUD_INFO = "/fraud/memberfraudinfo";
    }
}
