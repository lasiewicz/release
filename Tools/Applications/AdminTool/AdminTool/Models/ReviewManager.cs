﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using AdminTool.Models.ModelViews;
using AdminTool.Models.ModelViews.Approval;
using AdminTool.Models.ModelViews.Review;
using Matchnet;
using Matchnet.ApproveQueue.ServiceAdapters;
using Matchnet.ApproveQueue.ValueObjects.DBQueue;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Admin;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.ExternalMail.ServiceAdapters;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects;
using Matchnet.Member.ValueObjects.Photos;
using Spark.CommonLibrary;
using Spark.CommonLibrary.Logging;
using Spark.SODBQueue.ValueObjects;

namespace AdminTool.Models
{
    public class ReviewManager : ManagerBase
    {
        const string TrainingReportEmailTemplatePath = @"\Models\EmailTemplates\ApprovalReviewReportEmail.htm";
        const string SodbEmailTemplatePath = @"\Models\EmailTemplates\SodbRequestMoreInfoFromSSP.htm";
        const string EmailTextAttributeHtmlFilePath = @"\Models\EmailTemplates\ApprovalReviewReportAttribute.htm";
        private const string DefaultDailySampleRate = "ADMIN_REVIEW_SAMPLE_RATE";
        private const string AdminReviewCacheKey = "AdminAddedForFTAReview";
        private const string SodbReviewMessagesCacheKey = "SodbReviewMessage";
        public Logger _logger = new Logger(typeof(ReviewManager));

        public MemberFraudReviewModelView GetFraudReviewQueueItems(int siteId)
        {
            var data = new MemberFraudReviewModelView { SiteId = siteId, Sites = GetSites(siteId) };
            var memberItems = DBApproveQueueSA.Instance.DequeueMemberBySite(siteId);
            foreach (var queueItemMember in memberItems)
            {
                data.MemberItems.Add(new MemberFraudReviewData
                                         {
                                             AdminMemberId = queueItemMember.AdminMemberId,
                                             MemberId = queueItemMember.MemberID,
                                             SiteId = queueItemMember.SiteID,
                                             Site = ((SiteIDs) Enum.Parse(typeof (SiteIDs), queueItemMember.SiteID.ToString())).ToString(),
                                             InsertDate = queueItemMember.InsertDate
                                         });
            }
            return data;
        }

        public MemberFraudReviewModelView CompleteFraudReview(MemberFraudReviewModelView model)
        {
            var items = new List<QueueItemMember>();
            foreach (var memberItem in model.MemberItems)
            {
                if (memberItem.IsFraud.HasValue)
                {
                    items.Add(new QueueItemMember(Matchnet.Constants.NULL_INT,
                                                        memberItem.SiteId,
                                                        Matchnet.Constants.NULL_INT,
                                                        memberItem.MemberId,
                                                        memberItem.AdminMemberId,
                                                        g.AdminID,
                                                        memberItem.IsFraud.Value));
                }
            }

            DBApproveQueueSA.Instance.CompleteMemberFraudReview(items);

            var data = new MemberFraudReviewModelView { SiteId = model.SiteId, Sites = GetSites(model.SiteId) };
            if (model.SiteId.HasValue)
            {
                var memberItems = DBApproveQueueSA.Instance.DequeueMemberBySite(model.SiteId.Value);
                foreach (var queueItemMember in memberItems)
                {
                    data.MemberItems.Add(new MemberFraudReviewData
                                             {
                                                 AdminMemberId = queueItemMember.AdminMemberId,
                                                 MemberId = queueItemMember.MemberID,
                                                 SiteId = queueItemMember.SiteID,
                                                 Site =
                                                     ((SiteIDs)
                                                      Enum.Parse(typeof (SiteIDs), queueItemMember.SiteID.ToString())).
                                                     ToString(),
                                                 InsertDate = queueItemMember.InsertDate
                                             });
                }
            }
            return data;
        }

        public TextApprovalReviewModelView GetTextApprovalReview(bool isOnDemand, int? supervisorId, int? adminMemberId, bool? perAdmin)
        {
            var textApprovalReviewModelView = new TextApprovalReviewModelView { TopNav = new SharedTopNav(), IsOnDemand = isOnDemand};
            if (isOnDemand && !supervisorId.HasValue)
                supervisorId = g.AdminID;
            if (perAdmin.HasValue && perAdmin.Value)
            {
                textApprovalReviewModelView.ProcessSingleAdmin = true;
                textApprovalReviewModelView.SupervisorId = (supervisorId.HasValue) ? supervisorId.Value : 0;
            }
            ReviewText reviewText = DBApproveQueueSA.Instance.GetTextToReview(isOnDemand, supervisorId, adminMemberId);
            if (reviewText != null)
            {
                textApprovalReviewModelView.Left = GetReviewLeft(reviewText.CommunityId, reviewText.AdminActionMask, reviewText.AdminMemberId, reviewText.StatusDate,false, isOnDemand);

                if (reviewText.MemberId > 0)
                {
                    var member = MemberHelper.GetMember(reviewText.MemberId, MemberLoadFlags.IngoreSACache);
                    //TODO get this info from IndividualTextApprovalExpanded table
                    int brandId = 0;
                    member.GetLastLogonDate(reviewText.CommunityId, out brandId);
                    var brand = BrandConfigSA.Instance.GetBrandByID(brandId);
                    textApprovalReviewModelView.ExpandedUserInfo = MemberHelper.GetExpandedUserInfo(member, brand);

                    textApprovalReviewModelView.Attributes = GetReviewTextAttributes(reviewText.TextAttributes, brand,
                                                                                     member);
                    textApprovalReviewModelView.CommunityId = reviewText.CommunityId;
                    textApprovalReviewModelView.AdminMemberId = reviewText.AdminMemberId;
                    textApprovalReviewModelView.MemberTextApprovalId = reviewText.MemberTextApprovalId;
                    textApprovalReviewModelView.IsOnDemand = reviewText.IsOnDemand;
                    textApprovalReviewModelView.MemberId = reviewText.MemberId;
                }
            }
            return textApprovalReviewModelView;
        }

        public void PopulateOnDemandTextApprovalReview(DateTime startDate, DateTime endDate, int? communityId, string rates)
        {
            //to make the date range inclusive
            endDate = endDate.AddDays(1);

            var adminRateSettings = new List<AdminReviewSetting>();
            string[] adminRates = rates.Split(',');
            if(adminRates.Length > 0)
            {
                foreach (var adminRate in adminRates)
                {
                    string[] memberIdRates = adminRate.Split(':');
                    if(memberIdRates.Length > 0)
                    {
                        int adminMemberId = Convert.ToInt32(memberIdRates[0]);
                        int sampleRate = Convert.ToInt32(memberIdRates[1]);
                        if(sampleRate > 0)
                            adminRateSettings.Add(new AdminReviewSetting { AdminMemberId = adminMemberId, OnDemandSampleRate = sampleRate});
                    }
                } 
            }
            //Populate OnDemand Review Queue
            if(adminRateSettings.Count > 0)
                DBApproveQueueSA.Instance.PopulateOnDemandTextReview(startDate,endDate,communityId,g.AdminID,adminRateSettings);

            //Return Queue Item
            //return GetTextApprovalReview(true, g.AdminID, null, null);
        }

        public void CompleteAdminTextApprovalReview(FormCollection collection, TextApprovalReviewModelView model, string action)
        {
            var reviewText = new ReviewText
            {
                MemberTextApprovalId = model.MemberTextApprovalId,
                CommunityId = model.CommunityId,
                SupervisorId = g.AdminID,
                SupervisorReviewStatusId = action.Contains("training") ? SupervisorReviewStatus.Corrected : SupervisorReviewStatus.Approved,
                SupervisorNotes = model.TrainingNotes,
                AdminMemberId = model.AdminMemberId
            };

            if (action.Contains("training"))
            {
                reviewText.TextAttributes = new List<ReviewTextAttribute>();
                foreach (TextAttribute textAttribute in model.Attributes)
                {
                    int supervisorStatusMask = 0;
                    bool textStatus = Convert.ToBoolean(collection["TextAcceptable" + textAttribute.AttributeGroupId.ToString()]);
                    if (!textStatus)
                    {
                        string maskValue =
                            collection["multiselect_TextUnacceptableReasons" + textAttribute.AttributeGroupId.ToString()
                                ];
                        if (!string.IsNullOrEmpty(maskValue))
                        {
                            string[] statuses = maskValue.Split(',');
                            foreach (string status in statuses)
                            {
                                supervisorStatusMask = supervisorStatusMask + Convert.ToInt32(status);
                            }
                            textAttribute.SupervisorStatusMask = supervisorStatusMask;
                        }
                    }
                    if (supervisorStatusMask != textAttribute.StatusMask)
                    {
                        reviewText.TextAttributes.Add(new ReviewTextAttribute
                        {
                            AttributeGroupId = textAttribute.AttributeGroupId,
                            SupervisorStatusMask = supervisorStatusMask
                        });
                    }
                }

                SendTrainingReport(model);
            }

            DBApproveQueueSA.Instance.CompleteAdminTextApprovalReview(reviewText);
            if(model.IsOnDemand) 
                IncrementOnDemandTextApprovalReviewQueueItemsProcessedCount();
            else
                IncrementTextApprovalReviewQueueItemsProcessedCount();
        }

        public ReviewLeft GetReviewLeft(int communityId, int approvalStatus, int adminMemberId, DateTime approvalDate, bool CrxReview, bool? isOnDemand)
        {
            var approvalHelper = new ApprovalHelper();
            var textApprovalReviewCounts = (CrxReview) ? 
                DBApproveQueueSA.Instance.GetCRXSecondReviewCounts() :
                DBApproveQueueSA.Instance.GetTextApprovalReviewCounts(isOnDemand.HasValue && isOnDemand.Value);
            var reviewLeft = new ReviewLeft();
            reviewLeft.QueueCount = textApprovalReviewCounts.TotalItems;
            if (CrxReview)
                reviewLeft.ItemsProcessed = GetCRXTextApprovalReviewQueueItemsProcessedCount();
            else
            {
                reviewLeft.ItemsProcessed = (isOnDemand.HasValue && isOnDemand.Value)
                                                ? GetOnDemandTextApprovalReviewQueueItemsProcessedCount()
                                                : GetTextApprovalReviewQueueItemsProcessedCount();
            }
            reviewLeft.MemberMessages = approvalHelper.GetCurrentMemberMessages(g.AdminID);
            approvalHelper.ClearMemberMessages(g.AdminID);

            if (communityId > 0 && adminMemberId > 0)
            {
                SiteIDs siteId = GeneralHelper.GetPrimarySiteForCommunity((CommunityIDs)communityId);
                reviewLeft.SiteId = (int)siteId;

                //Approval Status
                if ((approvalStatus & (int)AdminAction.ApprovedFreeText) == (int)AdminAction.ApprovedFreeText)
                {
                    reviewLeft.ApprovalStatus = "Approved";
                }
                else if ((approvalStatus & (int)AdminAction.AdminSuspendMember) == (int)AdminAction.AdminSuspendMember)
                    reviewLeft.ApprovalStatus = "Suspended";

                //Admin email
                Member admin = MemberHelper.GetMember(adminMemberId, MemberLoadFlags.None);
                reviewLeft.ApprovalProcessedByEmail = admin.EmailAddress;

                reviewLeft.ApprovalDate = approvalDate;
                reviewLeft.IsOnDemand = isOnDemand.HasValue && isOnDemand.Value;
            }
            return reviewLeft;
        }

        public OnDemandQueueCountsModelView GetOnDemandQueueCounts()
        {
            var modelView = new OnDemandQueueCountsModelView {SharedTopNav = new SharedTopNav()};
            var reviewCounts = DBApproveQueueSA.Instance.GetOnDemandReviewQueueCounts();
            foreach (TextApprovalReviewCount textApprovalReviewCount in reviewCounts)
            {
                modelView.OnDemandQueueCounts.Add(new OnDemandQueueCount
                                                      {
                                                          AdminMemberId = textApprovalReviewCount.AdminMemberId,
                                                          SupervisorId = textApprovalReviewCount.SupervisorId,
                                                          DateQueueAdded = textApprovalReviewCount.DateQueueAdded,
                                                          TotalItems = textApprovalReviewCount.TotalItems,
                                                          OldestItem = textApprovalReviewCount.OldRecordDate
                                                      });
            }
            return modelView;
        }

        public bool PurgeOndemandReviewQueue(int adminMemberId, int supervisorId)
        {
            bool success = true;
            try
            {
                DBApproveQueueSA.Instance.PurgeOnDemandReviewQueue(adminMemberId, supervisorId);
            }
            catch (Exception ex)
            {
                success = false;
                _logger.Error("Exception occured while purging Ondemand Review Queue",ex);
            }
            return success;
        }

        public void SendTrainingReport(TextApprovalReviewModelView reviewText)
        {
            int adminMemberId = reviewText.AdminMemberId;
            Member admin = MemberHelper.GetMember(adminMemberId, MemberLoadFlags.None);
            string toEmailAddress = admin.EmailAddress;
            int supervisorId = g.AdminID;
            Member supervisor = MemberHelper.GetMember(supervisorId, MemberLoadFlags.None);
            string fromEmailAddress = supervisor.EmailAddress;
            string subject = "Free Text Approval Training Report";
            string body = GenerateEmailBody(TrainingReportEmailTemplatePath);

            if (reviewText.ExpandedUserInfo == null)
            {
                var member = MemberHelper.GetMember(reviewText.MemberId, MemberLoadFlags.IngoreSACache);
                int brandId = 0;
                member.GetLastLogonDate(reviewText.CommunityId, out brandId);
                var brand = BrandConfigSA.Instance.GetBrandByID(brandId);
                reviewText.ExpandedUserInfo = MemberHelper.GetExpandedUserInfo(member, brand);
            }

            ListDictionary replacements = GenerateEmailBodyReplacements(reviewText, fromEmailAddress);
            SendEmail(fromEmailAddress, toEmailAddress, subject, body, replacements);
        }

        public void SendEmail(string fromAddress, string toAddress, string subject, string body, ListDictionary replacements)
        {
            try
            {
                var mailDefinition = new MailDefinition();
                mailDefinition.From = fromAddress;
                mailDefinition.IsBodyHtml = true;
                mailDefinition.Subject = subject;
                MailMessage message = mailDefinition.CreateMailMessage(toAddress, replacements, body, new System.Web.UI.Control());
                var client = new SmtpClient("smtp.matchnet.com");
                client.UseDefaultCredentials = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(message);
            }
            catch (Exception ex)
            {
                System.Diagnostics.EventLog.WriteEntry("AdminTool", string.Format("Failure sending training report to agent. From: {0} To {1} {2} {3}", fromAddress, toAddress,ex.Message,ex.StackTrace));
            }
        }

        public int GetCRXTextApprovalReviewQueueItemsProcessedCount()
        {
            int totalTextApprovalReviewQueueItemsProcessed = 0;
            if (HttpContext.Current.Session["TotalCRXTextApprovalReviewQueueItemsProcessed"] != null)
            {
                totalTextApprovalReviewQueueItemsProcessed = Convert.ToInt32(HttpContext.Current.Session["TotalCRXTextApprovalReviewQueueItemsProcessed"].ToString());
            }
            return totalTextApprovalReviewQueueItemsProcessed;
        }

        public void IncrementCRXTextApprovalReviewQueueItemsProcessedCount()
        {
            int totalSessionQueueItemsProcessed = GetCRXTextApprovalReviewQueueItemsProcessedCount();
            totalSessionQueueItemsProcessed += 1;
            HttpContext.Current.Session["TotalCRXTextApprovalReviewQueueItemsProcessed"] = totalSessionQueueItemsProcessed;
        }

        public int GetTextApprovalReviewQueueItemsProcessedCount()
        {
            int totalTextApprovalReviewQueueItemsProcessed = 0;
            if (HttpContext.Current.Session["TotalTextApprovalReviewQueueItemsProcessed"] != null)
            {
                totalTextApprovalReviewQueueItemsProcessed = Convert.ToInt32(HttpContext.Current.Session["TotalTextApprovalReviewQueueItemsProcessed"].ToString());
            }
            return totalTextApprovalReviewQueueItemsProcessed;
        }

        public void IncrementTextApprovalReviewQueueItemsProcessedCount()
        {
            int totalSessionQueueItemsProcessed = GetTextApprovalReviewQueueItemsProcessedCount();
            totalSessionQueueItemsProcessed += 1;
            HttpContext.Current.Session["TotalTextApprovalReviewQueueItemsProcessed"] = totalSessionQueueItemsProcessed;
        }

        public int GetSodbReviewQueueItemsProcessedCount()
        {
            int totalSodbReviewQueueItemsProcessed = 0;
            if (HttpContext.Current.Session["TotalSodbReviewQueueItemsProcessed"] != null)
            {
                totalSodbReviewQueueItemsProcessed = Convert.ToInt32(HttpContext.Current.Session["TotalSodbReviewQueueItemsProcessed"].ToString());
            }
            return totalSodbReviewQueueItemsProcessed;
        }

        public void IncrementSodbReviewQueueItemsProcessedCount()
        {
            int totalSessionQueueItemsProcessed = GetSodbReviewQueueItemsProcessedCount();
            totalSessionQueueItemsProcessed += 1;
            HttpContext.Current.Session["TotalSodbReviewQueueItemsProcessed"] = totalSessionQueueItemsProcessed;
        }

        public int GetSodbSecondReviewQueueItemsProcessedCount()
        {
            int totalSodbSecondReviewQueueItemsProcessed = 0;
            if (HttpContext.Current.Session["TotalSodbSecondReviewQueueItemsProcessed"] != null)
            {
                totalSodbSecondReviewQueueItemsProcessed = Convert.ToInt32(HttpContext.Current.Session["TotalSodbSecondReviewQueueItemsProcessed"].ToString());
            }
            return totalSodbSecondReviewQueueItemsProcessed;
        }

        public void IncrementSodbSecondReviewQueueItemsProcessedCount()
        {
            int totalSessionQueueItemsProcessed = GetSodbSecondReviewQueueItemsProcessedCount();
            totalSessionQueueItemsProcessed += 1;
            HttpContext.Current.Session["TotalSodbSecondReviewQueueItemsProcessed"] = totalSessionQueueItemsProcessed;
        }

        public int GetOnDemandTextApprovalReviewQueueItemsProcessedCount()
        {
            int totalTextApprovalReviewQueueItemsProcessed = 0;
            if (HttpContext.Current.Session["TotalOnDemandTextApprovalReviewQueueItemsProcessed"] != null)
            {
                totalTextApprovalReviewQueueItemsProcessed = Convert.ToInt32(HttpContext.Current.Session["TotalOnDemandTextApprovalReviewQueueItemsProcessed"].ToString());
            }
            return totalTextApprovalReviewQueueItemsProcessed;
        }

        public void IncrementOnDemandTextApprovalReviewQueueItemsProcessedCount()
        {
            int totalSessionQueueItemsProcessed = GetOnDemandTextApprovalReviewQueueItemsProcessedCount();
            totalSessionQueueItemsProcessed += 1;
            HttpContext.Current.Session["TotalOnDemandTextApprovalReviewQueueItemsProcessed"] = totalSessionQueueItemsProcessed;
        }

        public TrainingAcknowledgeModelView AcknowledgeTrainingReport(int memberTextApprovalId, int adminMemberId, int supervisorId, bool isOnDemand)
        {

            bool success = true;
            try
            {
                DBApproveQueueSA.Instance.AcknowledgeTrainingReport(memberTextApprovalId, adminMemberId, supervisorId, isOnDemand);
            }
            catch (Exception ex)
            {
                success = false;
            }
            var acknowledgeModelView = new TrainingAcknowledgeModelView { SharedTopNav = new SharedTopNav(), Success = success };
            return acknowledgeModelView;
        }

        public TextApprovalReviewReportModelView GetTextApprovalReviewReportCounts(TextApprovalReviewReportModelView model)
        {
            var modelView = new TextApprovalReviewReportModelView { Communities = ReviewManager.GetCommunities(model.CommunityId) };
            List<TextApprovalReviewHistoryCount> counts = DBApproveQueueSA.Instance.GetTextApprovalReviewHistoryCounts(model.StartDate, model.EndDate,
                                                                      model.AdminMemberId,
                                                                      (model.CommunityId > 0) ? model.CommunityId : null);
            modelView.Records = new List<ReviewCountRecord>();
            foreach (var textApprovalReviewHistoryCount in counts)
            {
                modelView.Records.Add(new ReviewCountRecord
                {
                    AdminMemberId = textApprovalReviewHistoryCount.AdminMemberId,
                    TotalTextProcessedCount = textApprovalReviewHistoryCount.TotalFTAProcessedCount,
                    TotalTextSampledCount = textApprovalReviewHistoryCount.TotalSampledCount,
                    TotalTextReviewedCount = textApprovalReviewHistoryCount.TotalReviewedCount,
                    TotalTextApprovedCount = textApprovalReviewHistoryCount.TotalApprovedCount,
                    TotalTextCorrectedCount = textApprovalReviewHistoryCount.TotalCorrectedCount
                });
            }
            return modelView;
        }

        public AdminSampleRatesModelView GetAdminSampleRates()
        {
            var modelView = new AdminSampleRatesModelView();
            var reviewSettings = DBApproveQueueSA.Instance.GetAdminReviewSampleRates();
            foreach (AdminReviewSetting adminReviewSetting in reviewSettings)
            {
                modelView.AdminSampleRates.Add(new AdminSampleRate
                {
                    AdminMemberId = adminReviewSetting.AdminMemberId,
                    DailySampleRate = (adminReviewSetting.DailySampleRate.HasValue) ? adminReviewSetting.DailySampleRate.Value : 0,
                    OldDailySampleRate = (adminReviewSetting.DailySampleRate.HasValue) ? adminReviewSetting.DailySampleRate.Value : 0
                });
            }
            return modelView;
        }

        public AdminSampleRatesModelView SaveAdminSampleRates(AdminSampleRatesModelView model)
        {
            var adminSettings = new List<AdminReviewSetting>();
            foreach (var adminReviewRate in model.AdminSampleRates)
            {
                if (adminReviewRate.DailySampleRate != adminReviewRate.OldDailySampleRate)
                {
                    adminSettings.Add(new AdminReviewSetting
                    {
                        AdminMemberId = adminReviewRate.AdminMemberId,
                        DailySampleRate = adminReviewRate.DailySampleRate
                    });
                }
            }
            DBApproveQueueSA.Instance.SaveAdminReviewSettings(adminSettings);
            var modelView = GetAdminSampleRates();
            return modelView;
        }

        public void AddAdminForFTAReview(int adminMemberId)
        {
            string cacheKey = AdminReviewCacheKey + adminMemberId.ToString();
            var adminAddedForReview = HttpContext.Current.Cache[cacheKey];
            if (adminAddedForReview == null)
            {
                //Get default daily sample rate
                int dailySampleRate = 0;
                Int32.TryParse(RuntimeSettings.GetSetting(DefaultDailySampleRate), out dailySampleRate);
                DBApproveQueueSA.Instance.SaveAdminReviewSettings(adminMemberId, dailySampleRate, null, false);
                HttpContext.Current.Cache.Add(cacheKey, "true", null, DateTime.Now.AddHours(2),
                                              Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
            }
        }

        public TextApprovalReviewModelView GetCRXSecondReviewText()
        {
            var modelView = new TextApprovalReviewModelView { TopNav = new SharedTopNav(), ModifyTextContent = true};
            //Shows only those attributes where admin's decision contradicts with CRX decision. (ReviewStatus - Pending)
            ReviewText reviewText = DBApproveQueueSA.Instance.GetCRXTextForSecondReview();
            if (reviewText != null)
            {
                modelView.Left = GetReviewLeft(reviewText.CommunityId, reviewText.AdminActionMask, reviewText.AdminMemberId, reviewText.StatusDate, true, null);

                if (reviewText.MemberId > 0)
                {
                    var member = MemberHelper.GetMember(reviewText.MemberId, MemberLoadFlags.IngoreSACache);
                    //TODO get this info from IndividualTextApprovalExpanded table
                    int brandId = 0;
                    member.GetLastLogonDate(reviewText.CommunityId, out brandId);
                    var brand = BrandConfigSA.Instance.GetBrandByID(brandId);
                    modelView.ExpandedUserInfo = MemberHelper.GetExpandedUserInfo(member, brand);

                    modelView.Attributes = GetSecondReviewTextAttributes(reviewText.TextAttributes, brand,
                                                                                     member);
                    modelView.CommunityId = reviewText.CommunityId;
                    modelView.LanguageId = reviewText.LanguageId;
                    modelView.AdminMemberId = reviewText.AdminMemberId;
                    modelView.MemberTextApprovalId = reviewText.MemberTextApprovalId;
                    modelView.IsOnDemand = reviewText.IsOnDemand;
                    modelView.MemberId = reviewText.MemberId;
                }
            }
            return modelView;
        }

        public void CompleteCRXSecondReview(TextApprovalReviewModelView textApprovalReviewModelView, FormCollection collection)
        {
            var reviewText = new ReviewText { MemberTextApprovalId = textApprovalReviewModelView.MemberTextApprovalId,
                                              ReviewerId = g.AdminID,
                                              ReviewerAccount = g.AdminDomainAccount};
            Brand brand = null;
            Member member = GetMemberAndBrand(textApprovalReviewModelView.MemberId, textApprovalReviewModelView.CommunityId,
                              textApprovalReviewModelView.LanguageId, out brand);

            bool updateUsername = false;
            int actionMask;
            reviewText.TextAttributes = UpdateMemberAttributes(textApprovalReviewModelView, collection, member, brand,
                                                               out actionMask, out updateUsername);
            reviewText.ReviewerActionMask = actionMask;
            if (!textApprovalReviewModelView.Suspend)
            {
                AdminSA.Instance.AdminActionLogInsert(member.MemberID, textApprovalReviewModelView.CommunityId, actionMask, g.AdminID, textApprovalReviewModelView.LanguageId);
            }
            else
            {
                var approvalHelper = new ApprovalHelper();
                var adminActionReasonId = textApprovalReviewModelView.AdminActionReasonId.HasValue ? 
                    (AdminActionReasonID)textApprovalReviewModelView.AdminActionReasonId.Value : AdminActionReasonID.None;
                bool stoppedRenewals = MemberHelper.SuspendMember(member, brand, adminActionReasonId, actionMask,
                                                                  (CommunityIDs) textApprovalReviewModelView.CommunityId,
                                                                  textApprovalReviewModelView.LanguageId, g.AdminID);
                approvalHelper.PersistMemberMessage(g.AdminID, member.MemberID, string.Format("Suspended member <a href='/Member/View/{1}/{0}' target='_blank'>{0}</a>", member.MemberID.ToString(), brand.Site.SiteID.ToString()), true);
                approvalHelper.PersistMemberMessage(g.AdminID, member.MemberID,
                                                    stoppedRenewals
                                                        ? "Renewals have been terminated."
                                                        : "No active renewal found.", true);
                new ApprovalReportsManager().AddSessionQueueItemProcessedCount();
            }

            //Update MTAH status
            DBApproveQueueSA.Instance.CompleteCRXSecondReview(reviewText);
            //Save Member
            if (updateUsername)
                MemberSA.Instance.SaveMember(member, (int)textApprovalReviewModelView.CommunityId); //Use overloaded SaveMember method to update username
            else
                MemberSA.Instance.SaveMember(member);

            IncrementCRXTextApprovalReviewQueueItemsProcessedCount();
            // process admin notes
            if (!string.IsNullOrEmpty(textApprovalReviewModelView.AdminNotes))
            {
                AdminSA.Instance.UpdateAdminNote(textApprovalReviewModelView.AdminNotes.Replace("<p>", string.Empty).Replace("</p>", string.Empty),
                    textApprovalReviewModelView.MemberId, g.AdminID, textApprovalReviewModelView.CommunityId, Matchnet.Lib.ConstantsTemp.ADMIN_NOTE_MAX_LENGTH);
            }
        }

        public void CompleteSODBReview(SODBReviewModelView model)
        {
            //post sodb decision to api
            try
            {
                string sodbApiUrl = ConfigurationManager.AppSettings["SodbPostDecisionUrl"];
                if (!string.IsNullOrEmpty(sodbApiUrl))
                {
                    var request = (HttpWebRequest)WebRequest.Create(sodbApiUrl);
                    request.Method = "POST";
                    request.ContentType = "application/json; charset:utf-8";
                    var postData = new
                    {
                        SSPBlueId = model.SSPBlueId,
                        CustomerId = model.MemberId,
                        CallingSystemId = model.SiteId,
                        AdminId = g.AdminID,
                        AdminActionId = model.SelectedDecision,
                        AdminNotes = model.AdminNotes
                    };

                    string json = Newtonsoft.Json.JsonConvert.SerializeObject(postData);
                    var writer = new StreamWriter(request.GetRequestStream());
                    writer.Write(json);
                    writer.Close();

                    using (var response = request.GetResponse() as HttpWebResponse)
                    {
                        var reader = new StreamReader(response.GetResponseStream());
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            //When SodbDecision is 'Request more info from SSP, then generate an email to supervisor
            if (model.SelectedDecision == (int)SODBDecision.RequestMoreInfo)
                SendSodbEmailToSupervisor(model);

            //When SodbDecision is 'Request photos from Member', then add the member to Sodb photo upload watch list
            if (model.SelectedDecision == (int)SODBDecision.RequestPhoto)
            {
                Brand brand = GeneralHelper.GetBrandForSite(model.SiteId);
                int brandId = (brand != null) ? brand.BrandID : 0;
                var sodbHelper = new SodbHelper();
                //Send email to member to upload photo 
                //For now, send emails to only BH members. Right now, Sodb Email is set up on YesMail service which won't work for sending emails to 
                //Utah platform members from BH. Once Mingle Mail is set up for this, we should be able to send emails to both platform members
                if(model.IsBHMember)
                    sodbHelper.SendSodbPhotoUploadEmail(model, brandId);

                sodbHelper.UpdateMembaseSodbWatchList(model.MemberId, model.SiteId, model.SSPBlueId);
            }

            if(model.IsSecondReview)
                IncrementSodbSecondReviewQueueItemsProcessedCount();
            else
                IncrementSodbReviewQueueItemsProcessedCount();
            PersistReviewMessage(g.AdminID, model.MemberId, string.Format("Reviewed member <a href='{1}' target='_blank'>{0}</a>", model.MemberId.ToString(), GetViewMemberLink(model.IsBHMember,model.SiteId,model.MemberId)), false);
        }

        public SODBReviewModelView GetItemFromSODBQueue(bool isSecondReview)
        {
            Spark.SODBQueue.ValueObjects.SodbItem memberInfo = null;
            try
            {
                //Get data from Internal API
                string sodbApiUrl = isSecondReview ? ConfigurationManager.AppSettings["SodbSecondReviewQueueUrl"].Replace("{adminId}",g.AdminID.ToString()) 
                                    : ConfigurationManager.AppSettings["SodbGetMemberUrl"];
                if (!string.IsNullOrEmpty(sodbApiUrl))
                {
                    var request = (HttpWebRequest)WebRequest.Create(sodbApiUrl);
                    using (var response = request.GetResponse() as HttpWebResponse)
                    {
                        var reader = new StreamReader(response.GetResponseStream());

                        string content = reader.ReadToEnd();
                        memberInfo =
                            Newtonsoft.Json.JsonConvert.DeserializeObject(content, typeof(Spark.SODBQueue.ValueObjects.SodbItem)) as
                            Spark.SODBQueue.ValueObjects.SodbItem;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            var modelView = GetSodbModel(memberInfo, isSecondReview);
            return modelView;
        }

        public SODBReviewModelView GetSodbMember(int memberId, int sspBlueId, int siteId)
        {
            Spark.SODBQueue.ValueObjects.SodbItem memberInfo = null;
            try
            {
                //Get data from Internal API
                string sodbApiUrl = string.Format("{0}/{1}/{2}/{3}",ConfigurationManager.AppSettings["SodbGetMemberUrl"], memberId, siteId, sspBlueId);
                if (!string.IsNullOrEmpty(sodbApiUrl))
                {
                    var request = (HttpWebRequest)WebRequest.Create(sodbApiUrl);
                    using (var response = request.GetResponse() as HttpWebResponse)
                    {
                        var reader = new StreamReader(response.GetResponseStream());

                        string content = reader.ReadToEnd();
                        memberInfo =
                            Newtonsoft.Json.JsonConvert.DeserializeObject(content, typeof(Spark.SODBQueue.ValueObjects.SodbItem)) as
                            Spark.SODBQueue.ValueObjects.SodbItem;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            var modelView = GetSodbModel(memberInfo, false);
            modelView.IsWaitingforSSP = true;
            return modelView;
        }

        public SodbReviewCountsModelView GetSodbQueueCounts()
        {
            var model = new SodbReviewCountsModelView();
            var queueCounts = new List<SodbQueueCounts>();
            try
            {
                //Get data from Internal API
                string sodbApiUrl = ConfigurationManager.AppSettings["SodbGetQueueStatsUrl"];
                if (!string.IsNullOrEmpty(sodbApiUrl))
                {
                    var request = (HttpWebRequest)WebRequest.Create(sodbApiUrl);
                    using (var response = request.GetResponse() as HttpWebResponse)
                    {
                        var reader = new StreamReader(response.GetResponseStream());

                        string content = reader.ReadToEnd();
                        queueCounts = Newtonsoft.Json.JsonConvert.DeserializeObject(content, typeof(List<SodbQueueCounts>)) as
                            List<SodbQueueCounts>;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            if(queueCounts != null && queueCounts.Count > 0)
            {
                model.SodbReviewQueueCountsBySite = queueCounts.Where(a => a.SodbQueueType == QueueType.ReviewQueue && a.QueueCountType == (int)QueueCountType.Site).ToList();
                model.SodbSecondReviewQueueCountsBySite = queueCounts.Where(a => a.SodbQueueType == QueueType.SecondReviewQueue && a.QueueCountType == (int)QueueCountType.Site).ToList();
                model.SodbReviewQueueCountsByLevel = queueCounts.Where(a => a.SodbQueueType == QueueType.ReviewQueue && a.QueueCountType == (int)QueueCountType.Level).ToList();
                model.SodbSecondReviewQueueCountsByLevel = queueCounts.Where(a => a.SodbQueueType == QueueType.SecondReviewQueue && a.QueueCountType == (int)QueueCountType.Level).ToList();
                var comparer = Comparer<int>.Default;
                model.SodbReviewQueueCountsBySite.Sort((x, y) => comparer.Compare(y.Count, x.Count));
                model.SodbReviewQueueCountsBySite.Sort((x, y) => comparer.Compare(y.Count, x.Count));
            }
            return model;
        }

        public SodbReviewStatsModelView GetSodbReviewStats(SodbReviewStatsModelView model)
        {
            try
            {
                //Get data from Internal API
                string sodbApiUrl = ConfigurationManager.AppSettings["SodbAgentStatsUrl"];
                sodbApiUrl += string.Format("?startDate={0}&endDate={1}&siteId={2}", model.StartDate.ToString(),
                    model.EndDate.AddDays(1).ToString(), model.SiteId.HasValue && model.SiteId.Value > 0 ? model.SiteId.ToString() : "");
                if (!string.IsNullOrEmpty(sodbApiUrl))
                {
                    var request = (HttpWebRequest)WebRequest.Create(sodbApiUrl);
                    using (var response = request.GetResponse() as HttpWebResponse)
                    {
                        var reader = new StreamReader(response.GetResponseStream());

                        string content = reader.ReadToEnd();
                        model.SodbAgentReviewCounts = Newtonsoft.Json.JsonConvert.DeserializeObject(content, typeof(List<SodbAgentReviewCounts>)) as
                            List<SodbAgentReviewCounts>;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            if (model.SodbAgentReviewCounts != null && model.SodbAgentReviewCounts.Count > 0 && model.AdminMemberId.HasValue)
            {
                model.SodbAgentReviewCounts =
                    model.SodbAgentReviewCounts.Where(a => a.AdminId == model.AdminMemberId.Value).ToList();
            }
            return model;
        }

        public SodbSearchModelView SearchSodbMember(int memberId, int? siteId, int? sspBlueId)
        {
            var model = new SodbSearchModelView {MemberId = memberId, SiteId = siteId, SSPBlueId = sspBlueId};
            string sodbApiUrl = ConfigurationManager.AppSettings["SodbSearch"];
            try
            {
                //Get data from Internal API
                var request = (HttpWebRequest) WebRequest.Create(sodbApiUrl);
                request.Method = "POST";
                request.ContentType = "application/json; charset:utf-8";
                var postData = new
                                   {
                                       CustomerId = memberId,
                                       CallingSystemId = siteId,
                                       SspBlueId = sspBlueId
                                   };

                string json = Newtonsoft.Json.JsonConvert.SerializeObject(postData);
                var writer = new StreamWriter(request.GetRequestStream());
                writer.Write(json);
                writer.Close();

                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    var reader = new StreamReader(response.GetResponseStream());
                    string content = reader.ReadToEnd();
                    model.SodbMembers = Newtonsoft.Json.JsonConvert.DeserializeObject(content, typeof(List<SodbItem>)) as
                        List<SodbItem>;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return model;
        }

        public static List<SelectListItem> GetSodbSites()
        {
            var listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "--Select--", Value = "0", Selected = true });
            var sitesList = Enum.GetValues(typeof (MingleBHSiteIds));
            foreach (object siteId in sitesList)
            {
                if (Convert.ToInt32(siteId) != 0)
                {
                    listItems.Add(new SelectListItem{ Text = System.Enum.GetName(typeof(MingleBHSiteIds), siteId), Value = ((int)siteId).ToString()});
                }
            }
            return listItems;
        }


        public static List<SelectListItem> GetSites(int? siteId)
        {
            var listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "--Select--", Value = "0", Selected = siteId == 0 });
            listItems.Add(new SelectListItem { Text = "Spark", Value = "101", Selected = siteId == 101 });
            listItems.Add(new SelectListItem { Text = "Cupid", Value = "15", Selected = siteId == 15 });
            listItems.Add(new SelectListItem { Text = "BlackSingles", Value = "9051", Selected = siteId == 9051 });
            listItems.Add(new SelectListItem { Text = "BBW", Value = "9041", Selected = siteId == 9041 });
            listItems.Add(new SelectListItem { Text = "JDate", Value = "103", Selected = siteId == 103 });
            listItems.Add(new SelectListItem { Text = "JDate.co.il", Value = "4", Selected = siteId == 4 });
            listItems.Add(new SelectListItem { Text = "JDate.co.uk", Value = "107", Selected = siteId == 107 });
            listItems.Add(new SelectListItem { Text = "JDate.fr", Value = "105", Selected = siteId == 105 });
            return listItems;
        }

        public static List<SelectListItem> GetCommunities(int? communityId)
        {
            var listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "All Sites", Value = "0", Selected = communityId == 0 });
            listItems.Add(new SelectListItem { Text = "BBWPersonalsPlus.com", Value = "23", Selected = communityId == 101 });
            listItems.Add(new SelectListItem { Text = "BlackSingles.com", Value = "24", Selected = communityId == 15 });
            listItems.Add(new SelectListItem { Text = "ChristianMingle.com", Value = "25", Selected = communityId == 9051 });
            listItems.Add(new SelectListItem { Text = "Cupid.co.il", Value = "10", Selected = communityId == 9041 });
            listItems.Add(new SelectListItem { Text = "InterracialSingles.com", Value = "22", Selected = communityId == 103 });
            listItems.Add(new SelectListItem { Text = "ItalianSingles.com", Value = "21", Selected = communityId == 4 });
            listItems.Add(new SelectListItem { Text = "JDate", Value = "3", Selected = communityId == 107 });
            listItems.Add(new SelectListItem { Text = "Spark.com", Value = "1", Selected = communityId == 105 });
            return listItems;
        }

        public static string GetMemberEmail(int memberId)
        {
            string email = "";
            Member member = MemberHelper.GetMember(memberId, MemberLoadFlags.None);
            if (member != null)
                email = member.EmailAddress;
            return email;
        }

        public void PersistReviewMessage(int adminMemberId, int memberId, string message, bool showInRed)
        {
            List<ApprovalMemberMessage> messages = null;
            object cachedMessages = HttpContext.Current.Cache[SodbReviewMessagesCacheKey + adminMemberId.ToString()];

            if (cachedMessages == null)
            {
                messages = new List<ApprovalMemberMessage>();
            }
            else
            {
                messages = (List<ApprovalMemberMessage>)cachedMessages;
            }

            messages.Add(new ApprovalMemberMessage(memberId, message, showInRed));

            HttpContext.Current.Cache.Add(SodbReviewMessagesCacheKey + adminMemberId.ToString(), messages, null, DateTime.Now.AddDays(1), Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
        }

        public void ClearMemberMessages(int adminMemberId)
        {
            HttpContext.Current.Cache.Remove(SodbReviewMessagesCacheKey + adminMemberId.ToString());
        }

        public List<ApprovalMemberMessage> GetCurrentMemberMessages(int adminMemberId)
        {
            object messages = HttpContext.Current.Cache[SodbReviewMessagesCacheKey + adminMemberId.ToString()];
            if (messages != null)
            {
                return (List<ApprovalMemberMessage>)messages;
            }
            else
            {
                return null;
            }
        }

        private List<ReviewTextAttribute> UpdateMemberAttributes(TextApprovalReviewModelView textApprovalReviewModelView, FormCollection collection, Member member, Brand brand, out int actionMask, out bool updateUsername)
        {
            updateUsername = false;
            actionMask = ( textApprovalReviewModelView.Suspend) ? (int)AdminAction.AdminSuspendMember : (int)AdminAction.ApprovedFreeText;
            var textAttributes = new List<ReviewTextAttribute>();
            foreach (TextAttribute attributeUpdate in textApprovalReviewModelView.Attributes)
            {
                string attributeValue = "";
                string supervisorTextContent = collection["Attribute" + attributeUpdate.AttributeGroupId.ToString()];
                if (supervisorTextContent.Trim() == string.Empty)
                {
                    if (attributeUpdate.AttributeName.ToLower() == "username")
                    {
                        attributeValue = textApprovalReviewModelView.MemberId.ToString();
                    }
                    actionMask = actionMask | (int)AdminAction.DeletedFreeTextAttribute;
                }
                else
                {
                    attributeValue = ApprovalHelper.cleanUpTags(supervisorTextContent);
                    if (attributeUpdate.AttributeName.ToLower() == "username" && attributeValue.Trim() == string.Empty)
                    {
                        attributeValue = textApprovalReviewModelView.MemberId.ToString();
                    }
                }

                TextStatusType textStatusType;
                string newValue = member.GetAttributeText(brand.Site.Community.CommunityID,
                            brand.Site.SiteID,
                            brand.BrandID,
                            textApprovalReviewModelView.LanguageId,
                            attributeUpdate.AttributeName,
                            String.Empty,
                            out textStatusType);
                string oldValue = attributeUpdate.TextContent;//Value before supervisor and admin update
                //If this text is different from what supervisor reviewed, then we should not save the essay, since there will be a queue item already added for this essay.
                if (newValue.Trim() == oldValue.Trim())
                {
                    if (attributeUpdate.AttributeName.ToLower() == "username")
                    {
                        //update username 
                        updateUsername = attributeValue != ApprovalHelper.cleanUpTags(attributeUpdate.TextContent);
                        member.SetUsername(attributeValue.Trim(), brand, TextStatusType.Human);
                    }
                    else
                    {
                        member.SetAttributeText(brand.Site.Community.CommunityID,
                                                brand.Site.SiteID,
                                                brand.BrandID,
                                                textApprovalReviewModelView.LanguageId,
                                                attributeUpdate.AttributeName,
                                                attributeValue.Trim(),
                                                TextStatusType.Human);
                    }
                }

                //Reviewer Status Mask
                int reviewerStatusMask = 0;
                bool textStatus = Convert.ToBoolean(collection["TextAcceptable" + attributeUpdate.AttributeGroupId.ToString()]);
                if (!textStatus)
                {
                    string maskValue =
                        collection["multiselect_TextUnacceptableReasons" + attributeUpdate.AttributeGroupId.ToString()
                            ];
                    if (!string.IsNullOrEmpty(maskValue))
                    {
                        string[] statuses = maskValue.Split(',');
                        foreach (string status in statuses)
                        {
                            reviewerStatusMask = reviewerStatusMask + Convert.ToInt32(status);
                        }
                    }
                }

                textAttributes.Add(new ReviewTextAttribute
                {
                    AttributeGroupId = attributeUpdate.AttributeGroupId,
                    ReviewerStatusMask = reviewerStatusMask,
                    ReviewerTextContent = attributeValue.Trim(),
                    ReviewStatus = (int)ApprovalStatus.Completed,
                    CRXTie = attributeUpdate.CRXTie,
                    ToolCorrectionRequested = attributeUpdate.ToolCorrectionRequested
                });
            }
            return textAttributes;
        }

        private List<TextAttribute> GetReviewTextAttributes(IEnumerable<ReviewTextAttribute> reviewTextAttributes, Brand brand, Member member)
        {
            Matchnet.Content.ValueObjects.AttributeMetadata.AttributeCollection attributeCollection =
               AttributeMetadataSA.Instance.GetAttributeCollections().GetCollection
               ("FREETEXT", brand.Site.Community.CommunityID, Constants.NULL_INT, Constants.NULL_INT);

            var textAttributes = new List<TextAttribute>();
            foreach (ReviewTextAttribute reviewTextAttribute in reviewTextAttributes)
            {
                int maxLength = 0;
                string attributeName = GetAttributeName(attributeCollection, member, brand,
                                                        reviewTextAttribute.AttributeGroupId, out maxLength);
                textAttributes.Add(new TextAttribute
                {
                    AdminTextContent = reviewTextAttribute.AdminTextContent,
                    AttributeGroupId = reviewTextAttribute.AttributeGroupId,
                    TextContent = reviewTextAttribute.TextContent,
                    AttributeName = attributeName,
                    MaxLength = maxLength,
                    StatusMask = reviewTextAttribute.StatusMask,
                    CRXApproval = reviewTextAttribute.CRXApproval,
                    CRXNotes = reviewTextAttribute.CRXNotes,
                    CRXResponseId = reviewTextAttribute.CRXResponseId,
                    CRXReviewDate = reviewTextAttribute.CRXReviewDate,
                    ReviewStatus = reviewTextAttribute.ReviewStatus
                });
            }
            return textAttributes;
        }

        private List<TextAttribute> GetSecondReviewTextAttributes(IEnumerable<ReviewTextAttribute> reviewTextAttributes, Brand brand, Member member)
        {
            string oldAttributeValue = "";
            Matchnet.Content.ValueObjects.AttributeMetadata.AttributeCollection attributeCollection =
               AttributeMetadataSA.Instance.GetAttributeCollections().GetCollection
               ("FREETEXT", brand.Site.Community.CommunityID, Constants.NULL_INT, Constants.NULL_INT);

            var textAttributes = new List<TextAttribute>();
            foreach (ReviewTextAttribute reviewTextAttribute in reviewTextAttributes)
            {
                int maxLength = 0;
                string attributeName = GetAttributeName(attributeCollection, member, brand,
                                                        reviewTextAttribute.AttributeGroupId, out maxLength);
                Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = AttributeMetadataSA.Instance.GetAttributes().GetAttribute(attributeName);

                if (attribute.OldValueContainerID != Constants.NULL_INT)
                {
                    oldAttributeValue = member.GetAttributeText(brand.Site.Community.CommunityID,
                                                                       brand.Site.SiteID,
                                                                       brand.BrandID,
                                                                       brand.Site.LanguageID,
                                                                       attribute.OldValueContainerID,
                                                                       String.Empty);
                }

                textAttributes.Add(new TextAttribute
                {
                    AdminTextContent = reviewTextAttribute.AdminTextContent,
                    AttributeGroupId = reviewTextAttribute.AttributeGroupId,
                    TextContent = reviewTextAttribute.TextContent,
                    AttributeName = attributeName,
                    MaxLength = maxLength,
                    StatusMask = reviewTextAttribute.StatusMask,
                    CRXApproval = reviewTextAttribute.CRXApproval,
                    CRXNotes = reviewTextAttribute.CRXNotes,
                    CRXResponseId = reviewTextAttribute.CRXResponseId,
                    CRXReviewDate = reviewTextAttribute.CRXReviewDate,
                    ReviewStatus = reviewTextAttribute.ReviewStatus,
                    OldTextContent = oldAttributeValue
                });
            }
            return textAttributes;
        }

        private string GenerateEmailBody(string filePath)
        {
            string emailBody = "";
            using (StreamReader reader = File.OpenText(HttpContext.Current.Server.MapPath(filePath)))
            {
                emailBody = reader.ReadToEnd();
            }
            return emailBody;
        }

        private ListDictionary GenerateEmailBodyReplacements(TextApprovalReviewModelView reviewText, string supervisorEmail)
        {
            var replacements = new ListDictionary();
            replacements.Add("<%DateReviewed%>", DateTime.Now.ToShortDateString());
            replacements.Add("<%ReviewedByEmail%>", supervisorEmail);
            replacements.Add("<%ReviewNotes%>", (string.IsNullOrEmpty(reviewText.TrainingNotes)) ? "" : reviewText.TrainingNotes);
            replacements.Add("<%MemberId%>", reviewText.ExpandedUserInfo.MemberID.ToString());
            replacements.Add("<%Email%>", (string.IsNullOrEmpty(reviewText.ExpandedUserInfo.Email)) ? "" : reviewText.ExpandedUserInfo.Email);
            replacements.Add("<%FirstName%>", (string.IsNullOrEmpty(reviewText.ExpandedUserInfo.FirstName)) ? "" : reviewText.ExpandedUserInfo.FirstName);
            replacements.Add("<%LastName%>", (string.IsNullOrEmpty(reviewText.ExpandedUserInfo.LastName)) ? "" : reviewText.ExpandedUserInfo.LastName);
            replacements.Add("<%RegistrationDate%>", reviewText.ExpandedUserInfo.RegistrationDate.ToString());
            replacements.Add("<%SubStatus%>", (string.IsNullOrEmpty(reviewText.ExpandedUserInfo.SubscriberStatus)) ? "" : reviewText.ExpandedUserInfo.SubscriberStatus);
            replacements.Add("<%Username%>", (string.IsNullOrEmpty(reviewText.ExpandedUserInfo.Username)) ? "" : reviewText.ExpandedUserInfo.Username);
            replacements.Add("<%MaritalStatus%>", (string.IsNullOrEmpty(reviewText.ExpandedUserInfo.MaritalStatus)) ? "" : reviewText.ExpandedUserInfo.MaritalStatus);
            replacements.Add("<%Basics%>", (string.IsNullOrEmpty(reviewText.ExpandedUserInfo.GenderSeekingGender)) ? "" : reviewText.ExpandedUserInfo.GenderSeekingGender);
            replacements.Add("<%Occupation%>", (string.IsNullOrEmpty(reviewText.ExpandedUserInfo.Occupation)) ? "" : reviewText.ExpandedUserInfo.Occupation);
            replacements.Add("<%Education%>", (string.IsNullOrEmpty(reviewText.ExpandedUserInfo.Education)) ? "" : reviewText.ExpandedUserInfo.Education);
            replacements.Add("<%Religion%>", (string.IsNullOrEmpty(reviewText.ExpandedUserInfo.Religion)) ? "" : reviewText.ExpandedUserInfo.Religion);
            replacements.Add("<%Ethnicity%>", (string.IsNullOrEmpty(reviewText.ExpandedUserInfo.Ethnicity)) ? "" : reviewText.ExpandedUserInfo.Ethnicity);
            replacements.Add("<%Location%>", (string.IsNullOrEmpty(reviewText.ExpandedUserInfo.ProfileLocation)) ? "" : reviewText.ExpandedUserInfo.ProfileLocation);
            replacements.Add("<%BillingPhone%>", (string.IsNullOrEmpty(reviewText.ExpandedUserInfo.BillingPhoneNumber)) ? "" : reviewText.ExpandedUserInfo.BillingPhoneNumber);
            replacements.Add("<%IPAddress%>", (string.IsNullOrEmpty(reviewText.ExpandedUserInfo.IPAddress)) ? "" : reviewText.ExpandedUserInfo.IPAddress);
            replacements.Add("<%CountryName%>", (reviewText.ExpandedUserInfo.IPLocation != null && reviewText.ExpandedUserInfo.IPLocation.countryName != null) ? reviewText.ExpandedUserInfo.IPLocation.countryName : "");
            replacements.Add("<%RegionName%>", reviewText.ExpandedUserInfo.IPLocation != null && reviewText.ExpandedUserInfo.IPLocation.regionName != null ? reviewText.ExpandedUserInfo.IPLocation.regionName : "");
            replacements.Add("<%City%>", reviewText.ExpandedUserInfo.IPLocation != null && reviewText.ExpandedUserInfo.IPLocation.city != null ? reviewText.ExpandedUserInfo.IPLocation.city : "");
            //replacements.Add("<%Grade%>", (reviewText.ExpandedUserInfo.FraudResult != null && reviewText.ExpandedUserInfo.FraudResult.Grade != null) ? reviewText.ExpandedUserInfo.FraudResult.Grade.ToUpper() : "");
            //replacements.Add("<%ColorHex%>", (reviewText.ExpandedUserInfo.FraudResult != null && reviewText.ExpandedUserInfo.FraudResult.ColorHex != null) ? reviewText.ExpandedUserInfo.FraudResult.ColorHex : "");

            replacements.Add("<%Score%>", (reviewText.ExpandedUserInfo.FraudResult != null ) ? reviewText.ExpandedUserInfo.FraudResult.RiskInquiryScore : "0");
            replacements.Add("<%Status%>", (reviewText.ExpandedUserInfo.FraudResult != null ) ? reviewText.ExpandedUserInfo.FraudResult.AutomateRiskInquiryStatus.ToString() : "");

            replacements.Add("<%Attributes%>", GetEmailReportTextAttributes(reviewText));
            replacements.Add("<%AcknowledgeURL%>", string.Format("http://{0}/Review/AcknowledgeTrainingReport?mtaid={1}&adminId={2}&supervisorId={3}&isOnDemand={4}",
                HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port > 0 ? ":" + HttpContext.Current.Request.Url.Port : ""), reviewText.MemberTextApprovalId, reviewText.AdminMemberId, g.AdminID, reviewText.IsOnDemand));
            return replacements;
        }

        private string GetEmailReportTextAttributes(TextApprovalReviewModelView reviewText)
        {
            string attributesHtmlTemplate = "";
            string attributesHtml = "";
            using (StreamReader reader = File.OpenText(HttpContext.Current.Server.MapPath(EmailTextAttributeHtmlFilePath)))
            {
                attributesHtmlTemplate = reader.ReadToEnd();
            }
            foreach (TextAttribute attribute in reviewText.Attributes)
            {
                var attrHtmlContent = new StringBuilder();
                attrHtmlContent.Append(attributesHtmlTemplate);
                attrHtmlContent.Replace("<%AttributeName%>", attribute.AttributeName);
                attrHtmlContent.Replace("<%TextContent%>", attribute.TextContentHtml);
                attrHtmlContent.Replace("<%OldGoodChecked%>", attribute.StatusMask == 0 ? "checked=\"checked\"" : "");
                attrHtmlContent.Replace("<%OldBadChecked%>", attribute.StatusMask > 0 ? "checked=\"checked\"" : "");
                attrHtmlContent.Replace("<%OldBadOptions%>", (attribute.StatusMask > 0) ? GetFTABadReasonsText(attribute.StatusMask) : "");
                attrHtmlContent.Replace("<%AdminTextContent%>", attribute.AdminTextContentHtml);
                attrHtmlContent.Replace("<%NewGoodChecked%>", attribute.SupervisorStatusMask == 0 ? "checked=\"checked\"" : "");
                attrHtmlContent.Replace("<%NewBadChecked%>", attribute.SupervisorStatusMask > 0 ? "checked=\"checked\"" : "");
                attrHtmlContent.Replace("<%NewBadOptions%>", (attribute.SupervisorStatusMask > 0) ? GetFTABadReasonsText(attribute.SupervisorStatusMask) : "");
                attributesHtml += attrHtmlContent.ToString();
            }
            return attributesHtml;
        }

        private string GetFTABadReasonsText(int statusMask)
        {
            string reasons = "";
            var statuses = DBApproveQueueSA.Instance.GetIndividualTextApprovalStatuses();
            foreach (IndividualTextApprovalStatus status in statuses)
            {
                if (status.Display && (statusMask & status.StatusMask) == status.StatusMask)
                {
                    reasons += status.Description + "<br/>";
                }
            }
            return reasons;
        }

        private string GetAttributeName(Matchnet.Content.ValueObjects.AttributeMetadata.AttributeCollection attributeCollection, Member member, Brand brand, int attributeGroupId, out int maxLength)
        {
            maxLength = 0;
            foreach (Matchnet.Content.ValueObjects.AttributeMetadata.AttributeCollectionAttribute aca in attributeCollection)
            {
                Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute =
                    AttributeMetadataSA.Instance.GetAttributes().GetAttribute(aca.AttributeID);
                if (attribute.DataType == Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Text)
                {
                    Matchnet.Content.ValueObjects.AttributeMetadata.AttributeGroup attributeGroup =
                        member.getAttributeGroup(brand.Site.Community.CommunityID,
                                                 brand.Site.SiteID,
                                                 brand.BrandID,
                                                 attribute);
                    if (attributeGroup.ID == attributeGroupId)
                    {
                        maxLength = attributeGroup.Length;
                        return attribute.Name;
                    }
                }
            }
            return "";
        }

        private Member GetMemberAndBrand(int memberId, int communityId, int languageId, out Brand brand)
        {
            Member member = MemberHelper.GetMember(memberId, MemberLoadFlags.None);
            //Get brand based on languageid
            brand = null;
            //get all sites from member's community
            int[] siteIds = member.GetSiteIDList();
            foreach (var siteId in siteIds)
            {
                Brand updatedbrand = g.GetBrand((int)siteId);
                if (updatedbrand.Site.LanguageID == languageId && updatedbrand.Site.Community.CommunityID == communityId)
                {
                    brand = updatedbrand;
                    break;
                }
            }

            if(brand == null)
            {
                int brandId;
                member.GetLastLogonDate((int)communityId, out brandId);
                brand = BrandConfigSA.Instance.GetBrandByID(brandId);
            }
            return member;
        }

        private void SendSodbEmailToSupervisor(SODBReviewModelView model)
        {
            Member admin = MemberHelper.GetMember(g.AdminID, MemberLoadFlags.None);
            string fromEmailAddress = admin.EmailAddress;
            string toEmailAddress = ConfigurationManager.AppSettings["SodbSupervisorEmail"];
            string subject = "SODB - Request more info from SSP";
            string body = GenerateEmailBody(SodbEmailTemplatePath);
            
            var replacements = new ListDictionary();
            replacements.Add("<%SSPBlueID%>", model.SSPBlueId.ToString());
            replacements.Add("<%MemberID%>", model.MemberId.ToString());
            replacements.Add("<%SiteID%>", ((MingleBHSiteIds)model.SiteId).ToString());
            replacements.Add("<%AdminEmail%>", fromEmailAddress);
            replacements.Add("<%PhotoUrls%>", (!string.IsNullOrEmpty(model.SodbImageUrls) ? model.SodbImageUrls.Replace(";","<br/>") : ""));
            
            SendEmail(fromEmailAddress, toEmailAddress, subject, body, replacements);
        }

        private SODBReviewModelView GetSodbModel(Spark.SODBQueue.ValueObjects.SodbItem memberInfo, bool isSecondReview)
        {
            var modelView = new SODBReviewModelView { IsSecondReview = isSecondReview};
            modelView.SelectedDecision = (int)SODBDecision.PositiveMatch;
            modelView.TopNav = new SharedTopNav();
            modelView.Left = GetSODBReviewLeft(isSecondReview);
            if (memberInfo != null && memberInfo.MemberId > 0)
            {
                modelView.MemberId = memberInfo.MemberId;
                modelView.SiteId = memberInfo.SiteId;
                modelView.SSPBlueId = memberInfo.SSPBlueId;
                modelView.Level = memberInfo.Level;
                modelView.SparkInfo = new SODBMemberInfo();
                modelView.SparkInfo.ShowViewMemberLink = true;
                modelView.SparkInfo.MemberId = memberInfo.MemberId;
                modelView.SparkInfo.SiteId = memberInfo.SiteId;
                modelView.SparkInfo.FirstName = memberInfo.SparkFirstName;
                modelView.SparkInfo.LastName = memberInfo.SparkLastName;
                modelView.SparkInfo.DateOfBirth = memberInfo.SparkDateOfBirth;
                modelView.SparkInfo.ZipCode = memberInfo.SparkZip;
                modelView.SparkInfo.State = memberInfo.SparkState;
                modelView.SparkInfo.Gender = memberInfo.SparkGender;
                modelView.IsBHMember = memberInfo.IsBHMember;
                modelView.Email = memberInfo.Email;
                modelView.SparkInfo.ViewMemberLink = GetViewMemberLink(memberInfo.IsBHMember, memberInfo.SiteId, memberInfo.MemberId); 
                if (!memberInfo.IsBHMember && memberInfo.UtahImagesCount > 0)
                {
                    if (memberInfo.UtahPhotoUrls != null)
                        modelView.SparkInfo.PhotoUrls = memberInfo.UtahPhotoUrls.Split(';').ToList();
                }
                else
                {
                    List<string> photoUrls = GetBHMemberPhotoUrls(memberInfo);
                    if(photoUrls.Count > 0)
                        modelView.SparkInfo.PhotoUrls.AddRange(photoUrls);
                }

                modelView.SODBInfo = new SODBMemberInfo();
                modelView.SODBInfo.FirstName = memberInfo.SodbFirstName;
                modelView.SODBInfo.LastName = memberInfo.SodbLastName;
                modelView.SODBInfo.DateOfBirth = memberInfo.SodbDateOfBirth;
                modelView.SODBInfo.ZipCode = memberInfo.SodbZip.ToString();
                modelView.SODBInfo.State = memberInfo.SodbState;
                modelView.SODBInfo.Gender = memberInfo.SodbGender;
                if (memberInfo.SodbImagesCount > 0)
                {
                    modelView.SODBInfo.PhotoUrls = memberInfo.SodbImageLink.Split(';').ToList();
                    modelView.SodbImageUrls = memberInfo.SodbImageLink;
                }
                modelView.AdminNotes = memberInfo.AdminNotes;
                modelView.AdminActions = memberInfo.AdminActions;
            }
            return modelView;
        }

        private List<string> GetBHMemberPhotoUrls(Spark.SODBQueue.ValueObjects.SodbItem memberInfo)
        {
            var urls = new List<string>();
            if (memberInfo != null && memberInfo.MemberId > 0)
            {
                //Show photos of BH Member
                Member bhmember = MemberHelper.GetMember(memberInfo.MemberId, MemberLoadFlags.IngoreSACache);
                Brand brand = g.GetBrand((int) memberInfo.SiteId);
                if (bhmember != null)
                {
                    Sites sites = Matchnet.Content.ServiceAdapters.BrandConfigSA.Instance.GetSites();
                    List<Photo> photos = bhmember.GetApprovedPhotos(brand.Site.Community.CommunityID, false); //TODO 
                    if (photos != null && photos.Count > 0)
                    {
                        foreach (var photo in photos)
                        {
                            string url = MemberPhotoHelper.GetPhotoDisplayUrl(PhotoType.Full, photo,
                                                                              brand.Site.Community.CommunityID,
                                                                              memberInfo.SiteId, true);
                            if (!string.IsNullOrEmpty(url))
                                urls.Add(url);
                        }
                    }
                    else
                    {
                        //Get ThumbNail photo
                        string thumbNailurl = MemberPhotoHelper.GetDefaultPhotoThumbnailDisplayURL(bhmember, brand);
                        if (!string.IsNullOrEmpty(thumbNailurl))
                        {
                            urls.Add(thumbNailurl);
                        }
                    }
                }
            }
            return urls;
        }

        private ReviewLeft GetSODBReviewLeft(bool isSecondReview)
        {
            var left = new ReviewLeft();
            left.SiteId = (int)SiteIDs.None;
            var queueCounts = new List<SodbQueueCounts>();
             try
            {
                //Get data from Internal API
                string sodbApiUrl = ConfigurationManager.AppSettings["SodbGetQueueStatsUrl"];
                if (!string.IsNullOrEmpty(sodbApiUrl))
                {
                    var request = (HttpWebRequest)WebRequest.Create(sodbApiUrl);
                    using (var response = request.GetResponse() as HttpWebResponse)
                    {
                        var reader = new StreamReader(response.GetResponseStream());

                        string content = reader.ReadToEnd();
                        queueCounts = Newtonsoft.Json.JsonConvert.DeserializeObject(content, typeof(List<SodbQueueCounts>)) as
                            List<SodbQueueCounts>;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            left.QueueCount = (queueCounts != null && queueCounts.Count > 0)
                ? ((isSecondReview) ? queueCounts.Where(a => a.SodbQueueType == QueueType.SecondReviewQueue && a.QueueCountType == 1).Select(a => a.Count).Sum() : queueCounts.Where(a => a.SodbQueueType == QueueType.ReviewQueue && a.QueueCountType == 1).Select(a => a.Count).Sum())
                : 0;
            left.ItemsProcessed = isSecondReview ? GetSodbSecondReviewQueueItemsProcessedCount() : GetSodbReviewQueueItemsProcessedCount();
            left.MemberMessages = GetCurrentMemberMessages(g.AdminID);
            ClearMemberMessages(g.AdminID);
            return left;
        }

        public string GetViewMemberLink(bool isBHMember, int siteId, int memberId)
        {
            if(isBHMember)
            {
                return string.Format("http://admintool.matchnet.com/Member/View/{0}/{1}", siteId.ToString(), memberId.ToString());
            }
            else
            {
                string siteName = "";
                switch (siteId)
                {
                    case 9011:
                        siteName = "adventist";
                        break;
                    case 9021:
                        siteName = "asian";
                        break;
                    case 9061:
                        siteName = "canada";
                        break;
                    case 9071:
                        siteName = "catholic";
                        break;
                    case 9081:
                        siteName = "christian";
                        break;
                    case 9111:
                        siteName = "deaf";
                        break;
                    case 9121:
                        siteName = "greek";
                        break;
                    case 9131:
                        siteName = "indian";
                        break;
                    case 9151:
                        siteName = "interracial";
                        break;
                    case 9171:
                        siteName = "jewish";
                        break;
                    case 9181:
                        siteName = "latin";
                        break;
                    case 9191:
                        siteName = "lds";
                        break;
                    case 9281:
                        siteName = "ldsso";
                        break;
                    case 9221:
                        siteName = "military";
                        break;
                    case 9251:
                        siteName = "singleparent";
                        break;
                    case 113:
                        siteName = "prime";
                        break;
                    case 9271:
                        siteName = "UK";
                        break;
                }
                return (!string.IsNullOrEmpty(siteName)) 
                    ? string.Format("https://www.securemingle.com/admin/sites/{0}/admin_billing.html?id={1}",siteName,memberId.ToString())
                    : "";
            }
        }
    }
}
