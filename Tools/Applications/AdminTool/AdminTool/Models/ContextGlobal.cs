﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ServiceAdapters;
using System.Collections;
using Spark.CommonLibrary;
using AdminTool.Models.ModelViews;
using Matchnet.Member.ServiceAdapters;
using Matchnet;
using Matchnet.Member.ValueObjects.Admin;
using System.Configuration;
using Matchnet.Session.ServiceAdapters;
using Matchnet.Session.ValueObjects;
using Matchnet.Configuration.ServiceAdapters;
using Spark.CommonLibrary.Logging;
using System.Text;

namespace AdminTool.Models
{
    public class ContextGlobal
    {
        private Dictionary<int, Brand> _brands = new Dictionary<int, Brand>();

        #region Properties
        public string AdminDomainAccount { get; set; } //matchnet account
        public int AdminID { get; set; } //bedrock memberID
        public string DefaultBedrockAdminHost { get; set; }
        public string DefaultHost { get; set; }
        public string Environment { get; private set; }
        public bool IsDevMode { get; private set; }
        public UserSession Session { get; private set; }
        #endregion

        #region Constructor

        public ContextGlobal(string adminName)
        {
            IsDevMode = Boolean.Parse(RuntimeSettings.GetSetting("IS_DEVELOPMENT_MODE"));
            Session = SessionSA.Instance.GetSession(IsDevMode);
            //get admin domain account and bedrock ID
            AdminDomainAccount = adminName;
            AdminID = Constants.NULL_INT;
            AdminMemberDomainMapperCollection adminMapperList = MemberSA.Instance.GetAdminMemberDomainMappers();
            if (adminMapperList != null && adminMapperList.Count > 0)
            { 
                AdminMemberDomainMapper mapper = adminMapperList.GetAdminMemberDomainMapper(AdminDomainAccount);
                if (mapper != null)
                {
                    AdminID = mapper.MemberID;
                }
            }

            //default host for when we want all links to go to common existing web admin
            DefaultBedrockAdminHost = ConfigurationManager.AppSettings["DefaultBedrockAdminHost"];
            DefaultHost = ConfigurationManager.AppSettings["DefaultHost"];

            Environment = "";
            try
            {
                string env = Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("ENVIRONMENT_TYPE");
                if (env != null)
                {
                    Environment = env.Trim().ToLower();
                }
            }
            catch (Exception ex)
            {
                Environment = "";
            }
        }

        public ContextGlobal(): this(HttpContext.Current.User.Identity.Name) { }
        #endregion

        #region Public Methods
        public Brand GetBrand(int siteID)
        {
            Brand brand = null;
            if (!_brands.ContainsKey(siteID))
            {
                Brands brands = BrandConfigSA.Instance.GetBrandsBySite(siteID);

                //default brand should have the smallest brandID
                Brand defaultBrand = null;
                foreach (Brand b in brands)
                {
                    if (defaultBrand == null)
                    {
                        defaultBrand = b;
                    }
                    else if (b.BrandID < defaultBrand.BrandID)
                    {
                        defaultBrand = b;
                    }
                }
                _brands.Add(siteID, defaultBrand);

            }

            brand = _brands[siteID];

            return brand;
        }
       
        #endregion
    }
}
