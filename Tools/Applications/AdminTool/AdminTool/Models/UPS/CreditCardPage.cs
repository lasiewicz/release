﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using AdminTool.Models.SparkAPI;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Lib;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Admin;
using Matchnet.PromoEngine.ServiceAdapters;
using Matchnet.PromoEngine.ServiceAdapters.TokenReplacement;
using Matchnet.PromoEngine.ValueObjects;
using Matchnet.PromoEngine.ValueObjects.ResourceTemplate;
using Matchnet.Purchase.ValueObjects;
using Spark.Common.Adapter;
using Spark.Common.UPS;
using Language = Matchnet.Language;
using Matchnet.OmnitureHelper;
using Spark.CommonLibrary;
using Spark.Common.RestConsumer.V2.Models.Subscription;
using Spark.Common.RestConsumer.V2.Models.OAuth2;

namespace AdminTool.Models.UPS
{
    public class CreditCardPage : UserControl, IPaymentUIJSON
    {
        public const string SUBSCRIPTION_ENCRYPT_KEY = "m0nk$ies";

        private int version = Constants.NULL_INT;
        private int templateID = Constants.NULL_INT;
        private PaymentType paymentType;
        private int upsLegacyDataID = Constants.NULL_INT;
        private int UPSGlobalLogID = Constants.NULL_INT;
        private bool saveLegacyData = true;
        private Member targetMember = null;
        private string _clientIP;
        private StringDictionary _expansionTokens = new StringDictionary();
        private Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrand = null;

        public HttpContext Context { get; set; }
        public Dictionary<String, String> OrderAttributes { get; set; }
        public int PromoID { get; set; }
        public int SelectedPlanID { get; set; }
        public Member TargetMember { get { return targetMember; } }
        public Matchnet.Content.ValueObjects.BrandConfig.Brand TargetBrand { get { return targetBrand; } }
        public bool PromoPreviewOnly { get; set; }
        public Promo PreviewPromo { get; set; }
        public bool AdminOnly { get; set; }

        public CreditCardPage(int memberID, int brandID, bool adminOnly)
        {
            targetMember = MemberHelper.GetMember(memberID, MemberLoadFlags.IngoreSACache);
            targetBrand = BrandConfigSA.Instance.GetBrands().GetBrand(brandID);
            AdminOnly = adminOnly;
        }

        #region Context Global Properties

        public string ClientIP
        {
            get
            {
                if (_clientIP == null)
                {
                    _clientIP = HttpContext.Current.Request.Headers["client-ip"];
                    if (_clientIP == null)
                    {
                        _clientIP = HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];
                    }

                    Int32 commaPos = _clientIP.IndexOf(",");
                    if (commaPos > -1)
                    {
                        _clientIP = _clientIP.Substring(0, commaPos);
                    }
                }

                return _clientIP;
            }
        }

        public StringDictionary ExpansionTokens
        {
            get { return _expansionTokens; }
        }

        //Omniture Property
        public string S_LinkInternalFilters 
        {
            get
            {
                if (Convert.ToBoolean((RuntimeSettings.GetSetting("IS_UPS_INTEGRATION_ENABLED", TargetBrand.Site.Community.CommunityID,
                    TargetBrand.Site.SiteID, TargetBrand.BrandID))))
                {
                    return "javascript:," + TargetBrand.Site.Name + ",secure.spark.net";
                }
                return "javascript:," + TargetBrand.Site.Name;
            }
        }

        #endregion

        #region IPaymentUIJSON Members

        bool IPaymentUIJSON.SaveLegacyData
        {
            get
            {
                return saveLegacyData;
            }
            set
            {
                saveLegacyData = true;
            }
        }

        int IPaymentUIJSON.Version
        {
            get
            {
                return version;
            }
            set
            {
                version = value;
            }
        }

        int IPaymentUIJSON.TemplateID
        {
            get
            {
                return templateID;
            }
            set
            {
                templateID = value;
            }
        }

        PaymentType IPaymentUIJSON.PaymentType
        {
            get
            {
                return paymentType;
            }
            set
            {
                paymentType = value;
            }
        }

        int IPaymentUIJSON.UPSLegacyDataID
        {
            get
            {
                return upsLegacyDataID;
            }
            set
            {
                upsLegacyDataID = value;
            }
        }

        int IPaymentUIJSON.UPSGlobalLogID
        {
            get
            {
                return UPSGlobalLogID;
            }
            set
            {
                UPSGlobalLogID = value;
            }
        }

        private bool UseAPIForPaymentJSON()
        {
            return Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USE_API_FOR_UPS_SUBSCRIPTION_PAYMENT_JSON", TargetBrand.Site.Community.CommunityID, TargetBrand.Site.SiteID, TargetBrand.BrandID));
        }

        object IPaymentUIJSON.GetJSONObject()
        {

            if (UseAPIForPaymentJSON())
                return GetAPIPaymentJson();
            else
                return GetPaymentJson();
        }


        private PaymentUiPostData GetAPIPaymentJson()
        {


            //version and template are set in payment ui connector and paymenttype by subscribenew page. 
            if (version == Constants.NULL_INT)
                throw new Exception("Version cannot be null.");

            if (paymentType == null)
                throw new Exception("PaymentType cannot be null.");


            try
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetAPIPaymentJson", "ENTER PaymentUIConnector jump page", null, 1, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);

                 
                PaymentUiPostData paymentData = null;
                


                string cancelUrl = string.Empty;


                if (HttpContext.Current.Request.UrlReferrer != null)
                cancelUrl = HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                else
                cancelUrl = "http://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port > 0 ? ":" + HttpContext.Current.Request.Url.Port : "");

                // Legacy Data Info
                int adminID = Constants.NULL_INT;
                string adminDomainName = String.Empty;

                AdminMemberDomainMapperCollection adminMapperList = MemberSA.Instance.GetAdminMemberDomainMappers();
                if (adminMapperList != null && adminMapperList.Count > 0)
                {
                AdminMemberDomainMapper mapper = adminMapperList.GetAdminMemberDomainMapper(HttpContext.Current.User.Identity.Name);
                    if (mapper != null)
                    {
                        adminID = mapper.MemberID;
                        adminDomainName = mapper.DomainAccount;
                    }
                }
                    
                var request = new PaymentUiPostDataRequest
                {
                PaymentUiPostDataType = "1",
                CancelUrl = cancelUrl,
                ConfirmationUrl = "http://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port > 0 ? ":" + HttpContext.Current.Request.Url.Port : "") + "/Subscription/Confirmation",
                DestinationUrl = string.Empty,
                ReturnUrl = "http://" + HttpContext.Current.Request.Url.Host,
                ClientIp = ClientIP,
                PrtTitle = string.Empty,
                SrId = (Context.Request["srid"] != null) ? Convert.ToString(Context.Request["srid"]).Replace(",", ";") : string.Empty,
                PrtId = ((int)PurchaseReasonType.AdminPurchasesOnBehalfOfMember).ToString(),
                OmnitureVariables = new Dictionary<string, string>(),
                MemberLevelTrackingLastApplication = "1",
                MemberLevelTrackingIsMobileDevice = "false",
                MemberLevelTrackingIsTablet = "false",
                MemberID = TargetMember.MemberID,
                AdminDomainName = adminDomainName,
                AdminMemberID = adminID,
                PaymentType = (int)paymentType
                };

                if (Context.Request["PromoID"] != null)
                {
                request.PromoID = Convert.ToInt32(Context.Request["PromoID"]);
                }

                if (AdminOnly)
                    request.PromoType = "1";
                else
                    request.PromoType = "0";


               


                #region Omniture Variables

                request.OmnitureVariables.Add("linkInternalFilters", S_LinkInternalFilters);
                request.OmnitureVariables.Add("pageName", "Subscribe - SubscribeNew");
                request.OmnitureVariables.Add("prop17", OmnitureHelper.GetProfileCompetionPercentage(TargetMember, TargetBrand));
                request.OmnitureVariables.Add("prop18", OmnitureHelper.GetGender(TargetMember.GetAttributeInt(TargetBrand, WebConstants.ATTRIBUTE_NAME_GENDERMASK)));
                request.OmnitureVariables.Add("prop19", OmnitureHelper.GetAge(TargetMember.GetAttributeDate(TargetBrand, WebConstants.ATTRIBUTE_NAME_BIRTHDATE)).ToString());

                string attributeName = "Ethnicity";
                if (TargetBrand.Site.Community.CommunityID == (int)CommunityIDs.JDate)
                {
                attributeName = "JDateEthnicity";
                }
                Hashtable resourceBag = GeneralHelper.GetResourceCollection(GeneralHelper.DetermineResourceFileName(TargetBrand));
                string prop20 = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, TargetMember, TargetBrand, attributeName);
                request.OmnitureVariables.Add("prop20", prop20);

                request.OmnitureVariables.Add("prop21", OmnitureHelper.GetRegionString(TargetMember, TargetBrand, TargetBrand.Site.LanguageID, false, true, false).Replace("\"", string.Empty));
                request.OmnitureVariables.Add("prop23", TargetMember.MemberID.ToString());
                request.OmnitureVariables.Add("events", "event9");

                request.OmnitureVariables.Add("eVar44", string.Format(WebConstants.OMNITURE_EVAR44_MEMBERID, (TargetMember.MemberID % 10).ToString()));
                request.OmnitureVariables.Add("eVar6", ((int)PurchaseReasonType.AdminPurchasesOnBehalfOfMember).ToString());
                request.OmnitureVariables.Add("eVar7", string.Empty);
                
                //JS-958 using web.config to retrieve PUI url
                //string paymentUIURL = (RuntimeSettings.GetSetting("UPS_PAYMENT_UI_URL", TargetBrand.Site.Community.CommunityID,
                //        TargetBrand.Site.SiteID, TargetBrand.BrandID));

                string paymentUIURL = ConfigurationManager.AppSettings["UPS_PAYMENT_UI_URL"];

                paymentUIURL += TargetBrand.Site.Name.Replace(".", String.Empty).ToLower();
                request.OmnitureVariables.Add("prop10", paymentUIURL);
                request.OmnitureVariables.Add("prop14", "Subscribe");
                string prop29 = (HttpContext.Current != null && HttpContext.Current.Request.UrlReferrer != null)
                                ? HttpContext.Current.Request.UrlReferrer.ToString()
                                : string.Empty;
                request.OmnitureVariables.Add("prop29", prop29);

                if (Context.Request.QueryString["eid"] != null)
                request.OmnitureVariables.Add("campaign", Context.Request.QueryString["eid"]);

                // passing the mbox name if it's present for this site
                string mBoxNamePrefix = (RuntimeSettings.GetSetting("T_N_T_SUB_CONFIRM_MBOX_NAME_PREFIX", TargetBrand.Site.Community.CommunityID,
                        TargetBrand.Site.SiteID, TargetBrand.BrandID));

                request.OmnitureVariables.Add("mboxname", mBoxNamePrefix == null ? string.Empty : mBoxNamePrefix + "_sub_new");

                #endregion

                APIPaymentUIJSON apiPaymentUIPSON = new APIPaymentUIJSON(ApiClient.GetInstance());
                paymentData = apiPaymentUIPSON.PostPayment(request,TargetBrand.BrandID.ToString());

                if (paymentData == null)
                throw new Exception("Unable to get payment data from Subcription. CreditCard page");

                 

                return paymentData;
            }
            catch (Exception ex)
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetAPIPaymentJson", "ERROR DETECTED: " + ex.Message, null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.ERROR, DateTime.Now);

                throw new Exception("GetAPIPaymentJson Error in geting JSON from API. MemberID:" + TargetMember.MemberID.ToString(), ex);
            }
            finally
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetAPIPaymentJson", "EXIT PaymentUIConnector jump page", null, 0, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
            }	

           
        }

        private PaymentJson GetPaymentJson()
        {
            if (version == Constants.NULL_INT)
                throw new Exception("Version cannot be null.");

            if (templateID == Constants.NULL_INT)
                throw new Exception("TemplateID cannot be null.");

            if (paymentType == null)
                throw new Exception("PaymentType cannot be null.");

            if (upsLegacyDataID == Constants.NULL_INT)
                throw new Exception("UPS Legacy Data ID cannot be null.");

            var colDataAttributes = new Dictionary<string, string>();
            try
            {
                if (TargetMember == null)
                    throw new Exception("Member cannot null.");

                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "ENTER PaymentUIConnector jump page", null, 1, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);

                int memberID = TargetMember.MemberID;
                PromoPlansHelper PromoPlan = null;
                Promo promo;
                if (Convert.ToBoolean(RuntimeSettings.GetSetting("PROMOENGINE_ENABLE", TargetBrand.Site.Community.CommunityID, TargetBrand.Site.SiteID)))
                {
                    PromoPlan = new PromoPlansHelper(SUBSCRIPTION_ENCRYPT_KEY, TargetBrand, memberID);
                }

                if (PromoPreviewOnly)
                {
                    promo = PreviewPromo;
                    PromoID = PreviewPromo.PromoID;
                }
                else
                {
                    if (PromoID != 0)
                    {
                        promo = PromoEngineSA.Instance.GetPromoByID(PromoID, TargetBrand.BrandID, memberID);
                    }
                    else if (PromoPlan != null && PromoPlan.Promo != null)
                    {
                        if (PromoPlan.FailedValidation)
                        {
                            throw new Exception("Promo Plan Expired.");
                        }
                        promo = PromoPlan.Promo;
                    }
                    else if (Context.Request["PromoID"] == null)
                    {
                        promo = PromoEngineSA.Instance.GetMemberPromo(memberID, TargetBrand, paymentType);
                    }
                    else
                    {
                        promo = PromoEngineSA.Instance.GetPromoByID(Convert.ToInt32(Context.Request["PromoID"]), TargetBrand.BrandID, memberID, false);
                    }
                }

                if (promo == null)
                {
                    throw new Exception("Promo cannot be null.");
                }

                PaymentJson payment = new PaymentJson();
                payment.UPSLegacyDataID = upsLegacyDataID;
                payment.UPSGlobalLogID = UPSGlobalLogID;
                payment.GetCustomerPaymentProfile = (paymentType == PaymentType.CreditCard) ? 1 : 0;
                payment.Version = version;
                payment.CallingSystemID = TargetBrand.Site.SiteID;
                payment.IsSubscriptionConfirmationEmailEnabled = Convert.ToBoolean((RuntimeSettings.GetSetting("ENABLE_SUBSCRIPTION_CONFIRMATION_EMAIL", TargetBrand.Site.Community.CommunityID, TargetBrand.Site.SiteID)));

                if ((promo.UPSTemplateID != Constants.NULL_INT) && (promo.UPSTemplateID != 0))
                    payment.TemplateID = promo.UPSTemplateID;
                else
                    payment.TemplateID = templateID;
                payment.TimeStamp = DateTime.Now;
                if (HttpContext.Current.Request.UrlReferrer != null)
                    payment.Data.Navigation.CancelURL = HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                else
                    payment.Data.Navigation.CancelURL = "http://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port > 0 ? ":" + HttpContext.Current.Request.Url.Port : "");
                // Temporary fix until SLL cert is deployed
                payment.Data.Navigation.ConfirmationURL = "http://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port > 0 ? ":" + HttpContext.Current.Request.Url.Port : "") + "/Subscription/Confirmation";
                payment.Data.Navigation.DestinationURL = "";

                // Order Attributes
                OrderAttributes = new Dictionary<string, string>();

                OrderAttributes.Add("PRTID", ((int)PurchaseReasonType.AdminPurchasesOnBehalfOfMember).ToString()); //PurchaseReasonType 19 - AdminPurchasesOnBehalfOfMember
                OrderAttributes.Add("SRID", (Context.Request["srid"] != null) ? Convert.ToString(Context.Request["srid"]).Replace(",", ";") : string.Empty);
                OrderAttributes.Add("PricingGroupID", promo.PromoID.ToString());

                foreach (string key in OrderAttributes.Keys)
                {
                    if (OrderAttributes[key] != string.Empty)
                        payment.Data.OrderAttributes += key + "=" + OrderAttributes[key] + ",";
                }

                if (payment.Data.OrderAttributes.Length > 0)
                    payment.Data.OrderAttributes = payment.Data.OrderAttributes.TrimEnd(',');

                // Member Info
                payment.Data.MemberInfo.CustomerID = memberID;
                payment.Data.MemberInfo.RegionID = TargetMember.GetAttributeInt(TargetBrand, "RegionID");
                payment.Data.MemberInfo.IPAddress = ClientIP;
                payment.Data.MemberInfo.Language = ((Language)Enum.Parse(typeof(Language), TargetBrand.Site.LanguageID.ToString())).ToString();
                RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(payment.Data.MemberInfo.RegionID, (int)Language.English);
                if (regionLanguage.CountryRegionID == ConstantsTemp.REGIONID_USA)
                    payment.Data.MemberInfo.State = regionLanguage.StateAbbreviation.Trim().ToUpper();

                //LC: Adding extra fields needed for Kount RIS
                payment.Data.MemberInfo.EmailAddress = TargetMember.GetAttributeText(TargetBrand, "EmailAddress");
                payment.Data.MemberInfo.GenderMask = SubscriptionHelper.GetGender(TargetMember.GetAttributeInt(TargetBrand, "gendermask"));
                payment.Data.MemberInfo.BirthDate = TargetMember.GetAttributeDate(TargetBrand, "birthdate", DateTime.MinValue);
                payment.Data.MemberInfo.BrandInsertDate = TargetMember.GetAttributeDate(TargetBrand, "BrandInsertDate", DateTime.MinValue);

                #region UDFS
                //JS-1228 LC: Adding UDF fields needed for Kount
                payment.Data.MemberInfo.UserName = TargetMember.GetUserName(TargetBrand);
                payment.Data.MemberInfo.MaritalStatus = SubscriptionHelper.GetDescription("MaritalStatus", TargetMember.GetAttributeInt(TargetBrand, "MaritalStatus"), TargetBrand);
                payment.Data.MemberInfo.Occupation = TargetMember.GetAttributeText(TargetBrand, "OccupationDescription");
                payment.Data.MemberInfo.Education = SubscriptionHelper.GetDescription("EducationLevel", TargetMember.GetAttributeInt(TargetBrand, "EducationLevel"), TargetBrand);
                payment.Data.MemberInfo.PromotionID = TargetMember.GetAttributeInt(TargetBrand, "PromotionID").ToString();
                payment.Data.MemberInfo.Eyes = SubscriptionHelper.GetDescription("EyeColor", TargetMember.GetAttributeInt(TargetBrand, "EyeColor"), TargetBrand);
                payment.Data.MemberInfo.Hair = SubscriptionHelper.GetDescription("HairColor", TargetMember.GetAttributeInt(TargetBrand, "HairColor"), TargetBrand);
                payment.Data.MemberInfo.Height = TargetMember.GetAttributeInt(TargetBrand, "Height").ToString();
                //TODO: define webconstant to check Jdate as 3 
                if (TargetBrand.Site.Community.CommunityID == 3)
                {
                    payment.Data.MemberInfo.Ethnicity = SubscriptionHelper.GetDescription("JDateEthnicity", TargetMember.GetAttributeInt(TargetBrand, "JDateEthnicity"), TargetBrand);
                    payment.Data.MemberInfo.Religion = SubscriptionHelper.GetDescription("JDateReligion", TargetMember.GetAttributeInt(TargetBrand, "JDateReligion"), TargetBrand);
                }
                else
                {
                    payment.Data.MemberInfo.Ethnicity = SubscriptionHelper.GetDescription("Ethnicity", TargetMember.GetAttributeInt(TargetBrand, "Ethnicity"), TargetBrand);
                    payment.Data.MemberInfo.Religion = SubscriptionHelper.GetDescription("Religion", TargetMember.GetAttributeInt(TargetBrand, "Religion"), TargetBrand);
                }

                string aboutMe = TargetMember.GetAttributeText(TargetBrand, "AboutMe");
                payment.Data.MemberInfo.AboutMe = aboutMe.Length < 251 ? aboutMe : aboutMe.Substring(0, 250);

                #endregion UDF

                // Promo Info
                var promoInfo = new PaymentJsonArray();
                promoInfo.Add("PromoID", promo.PromoID);
                promoInfo.Add("PageTitle", promo.PageTitle);
                promoInfo.Add("LegalNote", promo.LegalNote);
                payment.Data.PromoInfo.Add(promoInfo);

                // Page Info
                var pageInfo = new PaymentJsonArray();
                pageInfo.Add("PRTTitle", "");//Title text
                payment.Data.PageInfo.Add(pageInfo);

                #region Omniture Variables

                var omnitureVariables = new PaymentJsonArray();
                omnitureVariables.Add("linkInternalFilters", S_LinkInternalFilters);
                omnitureVariables.Add("pageName", "Subscribe - SubscribeNew");
                omnitureVariables.Add("prop17", OmnitureHelper.GetProfileCompetionPercentage(TargetMember, TargetBrand));
                omnitureVariables.Add("prop18", OmnitureHelper.GetGender(TargetMember.GetAttributeInt(TargetBrand, WebConstants.ATTRIBUTE_NAME_GENDERMASK)));
                omnitureVariables.Add("prop19", OmnitureHelper.GetAge(TargetMember.GetAttributeDate(TargetBrand, WebConstants.ATTRIBUTE_NAME_BIRTHDATE)).ToString());

                string attributeName = "Ethnicity";
                if (TargetBrand.Site.Community.CommunityID == (int)CommunityIDs.JDate)
                {
                    attributeName = "JDateEthnicity";
                }
                Hashtable resourceBag = GeneralHelper.GetResourceCollection(GeneralHelper.DetermineResourceFileName(TargetBrand));
                string prop20 = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, TargetMember, TargetBrand, attributeName);
                omnitureVariables.Add("prop20", prop20);

                omnitureVariables.Add("prop21", OmnitureHelper.GetRegionString(TargetMember, TargetBrand, TargetBrand.Site.LanguageID, false, true, false).Replace("\"", string.Empty));
                omnitureVariables.Add("prop23", TargetMember.MemberID.ToString());
                omnitureVariables.Add("events", "event9");
                if (promo.PromoPlans == null)
                    throw new Exception("PromoPlanCollection cannot be null. PromoID:" + promo.PromoID.ToString());
                omnitureVariables.Add("eVar3", GetPlanIDListForOmniture(promo.PromoPlans));
                omnitureVariables.Add("eVar4", promo.PromoID);
                omnitureVariables.Add("eVar44", string.Format(WebConstants.OMNITURE_EVAR44_MEMBERID, (TargetMember.MemberID % 10).ToString()));
                omnitureVariables.Add("eVar6", ((int)PurchaseReasonType.AdminPurchasesOnBehalfOfMember).ToString());
                omnitureVariables.Add("eVar7", string.Empty);
                
                //Js-958
                //string paymentUIURL = (RuntimeSettings.GetSetting("UPS_PAYMENT_UI_URL", TargetBrand.Site.Community.CommunityID,
                //            TargetBrand.Site.SiteID, TargetBrand.BrandID));
                string paymentUIURL = ConfigurationManager.AppSettings["UPS_PAYMENT_UI_URL"];

                paymentUIURL += TargetBrand.Site.Name.Replace(".", String.Empty).ToLower();
                omnitureVariables.Add("prop10", paymentUIURL);
                omnitureVariables.Add("prop14", "Subscribe");
                string prop29 = (HttpContext.Current != null && HttpContext.Current.Request.UrlReferrer != null)
                                    ? HttpContext.Current.Request.UrlReferrer.ToString()
                                    : string.Empty;
                omnitureVariables.Add("prop29", prop29);

                if (Context.Request.QueryString["eid"] != null)
                    omnitureVariables.Add("campaign", Context.Request.QueryString["eid"]);

                // passing the mbox name if it's present for this site
                string mBoxNamePrefix = (RuntimeSettings.GetSetting("T_N_T_SUB_CONFIRM_MBOX_NAME_PREFIX", TargetBrand.Site.Community.CommunityID,
                            TargetBrand.Site.SiteID, TargetBrand.BrandID));

                omnitureVariables.Add("mboxname", mBoxNamePrefix == null ? string.Empty : mBoxNamePrefix + "_sub_new");

                payment.Data.OmnitureVariables.Add(omnitureVariables);

                #endregion

                // Legacy Data Info
                int adminID = Constants.NULL_INT;
                AdminMemberDomainMapperCollection adminMapperList = MemberSA.Instance.GetAdminMemberDomainMappers();
                if (adminMapperList != null && adminMapperList.Count > 0)
                {
                    AdminMemberDomainMapper mapper = adminMapperList.GetAdminMemberDomainMapper(HttpContext.Current.User.Identity.Name);
                    if (mapper != null)
                    {
                        adminID = mapper.MemberID;
                    }
                }
                payment.Data.LegacyDataInfo.AdminMemberID = adminID;
                payment.Data.LegacyDataInfo.Member = TargetMember.MemberID;
                payment.Data.LegacyDataInfo.BrandID = TargetBrand.BrandID;
                payment.Data.LegacyDataInfo.SiteID = TargetBrand.Site.SiteID;

                #region Header Template Collections

                // Column Header Template Collection
                foreach (HeaderResourceTemplate template in promo.ColHeaderResourceTemplateCollection)
                {
                    var headerTemplate = new PaymentJsonArray();

                    string content = ResourceTemplateSA.Instance.GetResource(template.ResourceTemplateID, new ColumnHeaderTokenReplacer());
                    headerTemplate.Add("ResourceTemplateContent", content);
                    headerTemplate.Add("HeaderNumber", template.HeaderNumber);

                    payment.Data.ColHeaderTemplates.Add(headerTemplate);
                }

                // Row Header Template Collection
                foreach (RowHeaderResourceTemplate template in promo.RowHeaderResourceTemplateCollection)
                {
                    PaymentJsonArray headerTemplate = new PaymentJsonArray();

                    string content = ResourceTemplateSA.Instance.GetResource(template.ResourceTemplateID, new RowHeaderTokenReplacer(template.Duration));
                    headerTemplate.Add("ResourceTemplateContent", content);
                    headerTemplate.Add("HeaderNumber", template.HeaderNumber);

                    payment.Data.RowHeaderTemplates.Add(headerTemplate);
                }

                #endregion

                #region Add Promo Plans

                // Promo Plans
                foreach (PromoPlan promoPlan in promo.PromoPlans)
                {
                    // Admin Selected
                    if (SelectedPlanID > 0)
                    {
                        if (SelectedPlanID != promoPlan.PlanID)
                        {
                            // Not Available Package...
                            var dummy = new PaymentJsonPackage();
                            dummy.ID = 0;
                            dummy.Description = "";
                            dummy.Items.Add("EnabledForMember", false);
                            dummy.Items.Add("ResourceTemplateContent",
                                            "<div class=\"cell-plan sub-none\"><p>Not available</p></div>");
                            dummy.Items.Add("ListOrder", promoPlan.ListOrder);
                            dummy.Items.Add("ColumnGroup", promoPlan.ColumnGroup);

                            payment.Data.Packages.Add(dummy);

                            continue;
                        }
                    }
                    var package = new PaymentJsonPackage();

                    package.ID = promoPlan.PlanID;
                    package.Description = "";
                    package.SytemID = TargetBrand.Site.SiteID;
                    package.StartDate = promoPlan.StartDate;
                    package.ExpiredDate = promoPlan.EndDate;
                    package.CurrencyType = PaymentUIConnector.GetCurrencyAbbreviation(promoPlan.CurrencyType);

                    string content = ResourceTemplateSA.Instance.GetResource(promoPlan.ResourceTemplateID, new PlanTokenReplacer(promoPlan, 2));
                    package.Items.Add("ResourceTemplateContent", content);
                    package.Items.Add("EnabledForMember", promoPlan.EnabledForMember.ToString());
                    package.Items.Add("InitialCost", promoPlan.InitialCost);
                    package.Items.Add("InitialCostPerDuration", promoPlan.InitialCostPerDuration);
                    package.Items.Add("ListOrder", promoPlan.ListOrder);
                    package.Items.Add("ColumnGroup", promoPlan.ColumnGroup);
                    package.Items.Add("PlanID", promoPlan.PlanID);
                    package.Items.Add("CurrencyType", PaymentUIConnector.GetCurrencyAbbreviation(promoPlan.CurrencyType));
                    package.Items.Add("BestValueFlag", promoPlan.BestValueFlag);
                    package.Items.Add("DefaultPackage", (promoPlan.PlanID == promo.DefaultPlanID));
                    package.Items.Add("PaymentTypeMask", promoPlan.PaymentTypeMask);
                    package.Items.Add("PlanResourcePaymentTypeID", promoPlan.PlanResourcePaymentTypeID);
                    if (promoPlan.CreditAmount > Constants.NULL_DECIMAL)
                        package.Items.Add("CreditAmount", promoPlan.CreditAmount);
                    package.Items.Add("PurchaseMode", promoPlan.PurchaseMode.ToString());
                    package.Items.Add("PlanServices", promoPlan.PlanServices);
                    package.Items.Add("IsPurchaseAllowedWithRemainingCredit", promoPlan.EnabledForMember);
                    package.Items.Add("RenewCost", promoPlan.RenewCost);
                    payment.Data.Packages.Add(package);
                }

                #endregion

                foreach (PaymentJsonArray col in payment.Data.ColHeaderTemplates)
                {
                    int colNumber = Convert.ToInt32(col["HeaderNumber"]);

                    foreach (PaymentJsonArray row in payment.Data.RowHeaderTemplates)
                    {
                        int rowNumber = Convert.ToInt32(row["HeaderNumber"]);

                        if (!DoesPackageExistAtRowCol(rowNumber, colNumber, payment.Data.Packages))
                        {
                            // Not Available Package...
                            PaymentJsonPackage dummy = new PaymentJsonPackage();
                            dummy.ID = 0;
                            dummy.Description = "";
                            dummy.Items.Add("EnabledForMember", false);
                            dummy.Items.Add("ResourceTemplateContent",
                                            "<div class=\"cell-plan sub-none\"><p>Not available</p></div>");
                            dummy.Items.Add("ListOrder", rowNumber);
                            dummy.Items.Add("ColumnGroup", colNumber);

                            payment.Data.Packages.Add(dummy);
                        }
                    }
                }

                colDataAttributes.Add("UPSLegacyDataID", Convert.ToString(upsLegacyDataID));
                colDataAttributes.Add("CustomerID", Convert.ToString(memberID));
                colDataAttributes.Add("CallingSystemID", Convert.ToString(TargetBrand.Site.SiteID));
                colDataAttributes.Add("TransactionType", "1");
                colDataAttributes.Add("PRTID", OrderAttributes["PRTID"]);
                colDataAttributes.Add("SRID", OrderAttributes["SRID"]);
                colDataAttributes.Add("PricingGroupID", OrderAttributes["PricingGroupID"]);
                colDataAttributes.Add("RegionID", Convert.ToString(payment.Data.MemberInfo.RegionID));
                colDataAttributes.Add("IPAddress", payment.Data.MemberInfo.IPAddress);
                colDataAttributes.Add("TemplateID", Convert.ToString(payment.TemplateID));
                colDataAttributes.Add("CancelURL", payment.Data.Navigation.CancelURL);
                colDataAttributes.Add("ConfirmationURL", payment.Data.Navigation.ConfirmationURL);
                colDataAttributes.Add("DestinationURL", payment.Data.Navigation.DestinationURL);
                colDataAttributes.Add("PlanIDList", GetPlanIDListForOmniture(promo.PromoPlans));

                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "Save data attributes", colDataAttributes, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "NO ERROR", null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.OK, DateTime.Now);

                return payment;

            }
            catch (Exception ex)
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "Save data attributes", colDataAttributes, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "ERROR DETECTED: " + ex.Message, null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.ERROR, DateTime.Now);

                throw new Exception("Error in building JSON for PaymentUI. MemberID:" + TargetMember.MemberID.ToString(), ex);
            }
            finally
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "EXIT PaymentUIConnector jump page", null, 0, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
            }	
        }


        #endregion

        #region Private Methods

        private bool DoesPackageExistAtRowCol(int row, int col, PaymentJsonPackages packages)
        {
            foreach (PaymentJsonPackage package in packages)
            {
                if (Convert.ToInt32(package.Items["ColumnGroup"]) == col &&
                    Convert.ToInt32(package.Items["ListOrder"]) == row)
                {
                    return true;
                }
            }

            return false;
        }

        private string GetPlanIDListForOmniture(Matchnet.PromoEngine.ValueObjects.PromoPlanCollection plans)
        {
            var planIDList = new System.Text.StringBuilder();

            foreach (Matchnet.Purchase.ValueObjects.Plan displayedPlan in plans)
            {
                if (displayedPlan != null)
                {
                    if (displayedPlan.PlanID > 0)
                    {
                        planIDList.Append(displayedPlan.PlanID.ToString() + ",");
                    }
                }
            }

            return planIDList.ToString().TrimEnd(',');
        }

        #endregion
    }
}
