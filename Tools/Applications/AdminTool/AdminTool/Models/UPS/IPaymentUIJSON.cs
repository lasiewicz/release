﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Matchnet.Purchase.ValueObjects;

namespace AdminTool.Models.UPS
{
    public interface IPaymentUIJSON
    {
        /// <summary>
        /// Version number used in UPS - PaymentUI.
        /// </summary>
        int Version { get; set; }

        /// <summary>
        /// Template ID used in UPS - Payment UI.
        /// </summary>
        int TemplateID { get; set; }

        /// <summary>
        /// Indicate Credit || Check.
        /// </summary>
        PaymentType PaymentType { get; set; }

        /// <summary>
        /// Each call is saved as "UPSLegacyData" in mnSubscription. This is the unique ID per call.
        /// </summary>
        int UPSLegacyDataID { get; set; }

        /// <summary>
        /// Each call has a unique global log ID. 
        /// </summary>
        int UPSGlobalLogID { get; set; }

        /// <summary>
        /// Indicate whether this call should be saved on the Bedrock site.
        /// </summary>
        bool SaveLegacyData { get; set; }

        /// <summary>
        /// For populating data required for the JSON string.
        /// </summary>
        /// <returns></returns>
        object GetJSONObject();
    }
}