﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Matchnet;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Content.ValueObjects.PagePixel;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Lib;
using Matchnet.Member.ServiceAdapters;
using  Spark.CommonLibrary.Logging;

namespace AdminTool.Models.UPS
{
    public class PagePixelControl
    {
        private int pageID = Constants.NULL_INT;
        private Logger _logger = new Logger(typeof(Controller));
        RegionLanguage region;
        public Brand TargetBrand { get; set; }
        public Member TargetMember { get; set; }
        public string SubAmt { get; set; }
        public string SubDuration { get; set; }

        public int PageID
        {
            get { return pageID; }
            set { pageID = value; }
        }

        public PagePixelControl(Member targetMember,Brand targetBrand)
        {
            TargetBrand = targetBrand;
            TargetMember = targetMember;
        }

        public string WritePixel()
        {
            if (HttpContext.Current.Request["purchasedUpsale"] != null)
            {
                if (HttpContext.Current.Request["purchasedUpsale"] == "true")
                {
                    // Affiliate tracking pixel has already been rendered in the upsale page 
                    // so do not render another affiliate tracking pixel 
                    return String.Empty;
                }
            }
            var sb = new StringBuilder();
            int siteID = TargetBrand.Site.SiteID; 
            PagePixelCollection pixels = null;
            // There is a time out set in content service to prevent pixels from taking the site down.
            try
            {
                pixels = PagePixelSA.Instance.RetrievePagePixels(pageID, siteID);
            }
            catch (System.Exception ex)
            {
                _logger.Error("Page pixel error retrieving page pixels.", ex); 
                return String.Empty;
            }
            if (pixels.Count > 0)
            {
                sb.Append(System.Environment.NewLine);
                sb.Append(@"<!-- Affiliate Pixels -->");
                sb.Append(System.Environment.NewLine);
            }
            foreach (PagePixel pixel in pixels)
            {
                try
                {
                    if (pixel != null)
                    {
                        var p = new PagePixel();
                        p.Code = pixel.Code;
                        p.PageID = pixel.PageID;
                        p.SiteID = pixel.SiteID;
                        p.TargetingConditionCollection = pixel.TargetingConditionCollection;
                        sb.Append(EvaluatePixel(p));
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error("Error processing a pixel.", ex);
                }
            }
            if (pixels.Count > 0)
            {
                sb.Append(@"<!-- End Affiliate Pixels -->");
                sb.Append(System.Environment.NewLine);
            }
            return sb.ToString();
        }


        /// <summary>
        /// Apply filtering and adding arguments.
        /// </summary>
        /// <param name="pixel"></param>
        /// <returns></returns>
        private string EvaluatePixel(PagePixel pixel)
        {
            Nullable<Boolean> showPixel = true;
            Nullable<Boolean> showcountryid = null, showlgid = false, showgenderid = false, showpromoid = null, showage = null, showpageid = null;
            Dictionary<String, Nullable<Boolean>> checkList = new Dictionary<string, Nullable<Boolean>>();

            #region Targeting

            TargetingConditionCollection conditions = pixel.TargetingConditionCollection;
            bool temp = false;
            foreach (TargetingCondition condition in conditions)
            {
                switch (condition.Name)
                {
                    case "countryid":
                        InitializeRegion();
                        temp = FilterCompare(condition.ConditionValue, region.CountryRegionID.ToString(), condition.Comparison);
                        UpdateDictionaryItem(checkList, "countryid", temp);
                        break;
                    case "lgid":
                        string lgid = Constants.NULL_STRING;
                        if (TargetMember != null && TargetMember.GetAttributeText(TargetBrand, WebConstants.ATTRIBUTE_NAME_BRANDLUGGAGE) != Constants.NULL_STRING && TargetMember.GetAttributeText(TargetBrand, WebConstants.ATTRIBUTE_NAME_BRANDLUGGAGE) != "")
                        {
                            lgid = TargetMember.GetAttributeText(TargetBrand, WebConstants.ATTRIBUTE_NAME_BRANDLUGGAGE);
                        }
                        temp = FilterCompare(condition.ConditionValue, lgid, condition.Comparison);
                        UpdateDictionaryItem(checkList, "lgid", temp);
                        break;
                    case "genderid":
                        if (TargetMember != null)
                        {
                            temp = FilterCompare(condition.ConditionValue, TargetMember.GetAttributeInt(TargetBrand, "GenderMask").ToString(), condition.Comparison);
                            UpdateDictionaryItem(checkList, "genderid", temp);
                        }
                        break;
                    case "promoid":
                        string promoID = Constants.NULL_STRING;
                        if (TargetMember != null && TargetMember.GetAttributeInt(TargetBrand, WebConstants.ATTRIBUTE_NAME_BRANDPROMOTIONID, Constants.NULL_INT) != Constants.NULL_INT)
                        {
                            promoID = Convert.ToString(TargetMember.GetAttributeInt(TargetBrand, WebConstants.ATTRIBUTE_NAME_BRANDPROMOTIONID));
                        }
                        temp = FilterCompare(condition.ConditionValue, promoID, condition.Comparison);
                        UpdateDictionaryItem(checkList, "promoid", temp);
                        break;
                    case "age":
                        if (TargetMember != null)
                        {
                            temp = FilterCompare(condition.ConditionValue, GetAge(TargetMember.GetAttributeDate(TargetBrand, "BirthDate")).ToString(), condition.Comparison);
                            UpdateDictionaryItem(checkList, "age", temp);
                        }
                        break;
                    case "pageid":
                        //Using PageID of Upsale.aspx from Bedrock. Hardcoding this value since PagePixelControl is used only from Upsale page.
                        temp = FilterCompare(condition.ConditionValue, "549260", condition.Comparison);
                        UpdateDictionaryItem(checkList, "pageid", temp);
                        break;
                    default:
                        _logger.Error("Page pixel unknown targeting " + condition.Name);
                        break;
                }
            }

            if (conditions.Count > 0)
            {
                foreach (KeyValuePair<string, Nullable<Boolean>> hmm in checkList)
                {
                    if (hmm.Value == null)
                        continue;
                    showPixel &= hmm.Value;
                }
            }
            else
            {
                showPixel = true;
            }

            // None of the filters met, do not render anything for this pixel.
            if (showPixel == false)
            {
                return String.Empty;
            }

            #endregion // Targeting

            #region Arguments

            MatchCollection matchCollection = Regex.Matches(pixel.Code, @"{(.*?)}");

            var fields = new string[matchCollection.Count];
            for (int i = 0; i < fields.Length; i++)
            {
                string argumentName = matchCollection[i].Groups[1].Value.ToString();
                switch (argumentName.ToLower())
                {
                    case "birthdate":
                        try
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{birthdate}", TargetMember.GetAttributeDate(TargetBrand, "Birthdate").ToString("yyyyMMdd"));
                        }
                        catch
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{birthdate}", string.Empty);
                        }
                        break;
                    case "gender":
                        try
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{gender}", TargetMember.GetAttributeInt(TargetBrand, "GenderMask").ToString());
                        }
                        catch
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{gender}", String.Empty);
                        }
                        break;
                    case "memberid":
                        pixel.Code = ReplaceString(pixel.Code, "{memberid}", TargetMember.MemberID.ToString());
                        break;
                    case "country":
                        try
                        {
                            InitializeRegion();

                            pixel.Code = ReplaceString(pixel.Code, "{country}", region.CountryRegionID.ToString());
                        }
                        catch
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{country}", string.Empty);
                        }
                        break;
                    case "referring_url":
                        pixel.Code = ReplaceString(pixel.Code, "{referring_url}", HttpUtility.UrlEncode(Convert.ToString(HttpContext.Current.Request.ServerVariables["HTTP_REFERER"])));
                        break;
                    case "state":
                        try
                        {
                            InitializeRegion();

                            pixel.Code = ReplaceString(pixel.Code, "{state}", region.StateRegionID.ToString());
                        }
                        catch
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{state}", string.Empty);
                        }
                        break;
                    case "dma":
                        try
                        {
                            int rid = TargetMember.GetAttributeInt(TargetBrand, "RegionID", 0);

                            DMA dma = RegionSA.Instance.GetDMAByZipRegionID(rid);

                            pixel.Code = ReplaceString(pixel.Code, "{dma}", dma.DMAID.ToString());
                        }
                        catch
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{dma}", string.Empty);
                        }
                        break;
                    case "age":
                        try
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{age}", GetAge(TargetMember.GetAttributeDate(TargetBrand, "BirthDate")).ToString());
                        }
                        catch
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{age}", string.Empty);
                        }
                        break;
                    case "age_group":
                        try
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{age_group}", GetAgeGroup(TargetMember.GetAttributeDate(TargetBrand, "BirthDate")).ToString());
                        }
                        catch
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{age_group}", string.Empty);
                        }
                        break;
                    case "prm":
                        try
                        {
                            //09252008 TL MPR-457, Updating to ensure the promotion ID rendered for page pixel is based on member's registered promotionID
                            if (TargetMember != null && TargetMember.GetAttributeInt(TargetBrand, WebConstants.ATTRIBUTE_NAME_BRANDPROMOTIONID) > 0 && TargetMember.GetAttributeInt(TargetBrand, WebConstants.ATTRIBUTE_NAME_BRANDPROMOTIONID) != Constants.NULL_INT)
                            {
                                pixel.Code = ReplaceString(pixel.Code, "{prm}", TargetMember.GetAttributeInt(TargetBrand, WebConstants.ATTRIBUTE_NAME_BRANDPROMOTIONID).ToString());
                            }
                        }
                        catch
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{prm}", string.Empty);
                        }
                        break;
                    case "lgid":
                        if (TargetMember != null && TargetMember.GetAttributeText(TargetBrand, WebConstants.ATTRIBUTE_NAME_BRANDLUGGAGE) != Constants.NULL_STRING && TargetMember.GetAttributeText(TargetBrand, WebConstants.ATTRIBUTE_NAME_BRANDLUGGAGE) != "")
                        {
                            pixel.Code = ReplaceString(pixel.Code, "{lgid}", TargetMember.GetAttributeText(TargetBrand, WebConstants.ATTRIBUTE_NAME_BRANDLUGGAGE));
                        }
                        break;
                    case "subscription_amount":
                        pixel.Code = ReplaceString(pixel.Code, "{subscription_amount}", HttpUtility.UrlEncode(SubAmt));
                        break;
                    case "subscription_duration":
                        pixel.Code = ReplaceString(pixel.Code, "{subscription_duration}", HttpUtility.UrlEncode(SubDuration));
                        break;
                    case "timestamp":
                        pixel.Code = ReplaceString(pixel.Code, "{timestamp}", DateTime.Now.ToString("yyyyMMddHHmmss"));
                        break;
                    default:
                        // This line was throwing massive exceptions due to wrong arg names being passed in. 12/28/09
                        //_logger.Error("Page Pixel argument " + argumentName + " not found.");
                        break;
                }

            #endregion // Populate Arguments
            }
            return pixel.Code + Environment.NewLine;
        }

        private void InitializeRegion()
        {
            if (region == null)
            {
                region = new RegionLanguage();

                region = RegionSA.Instance.RetrievePopulatedHierarchy(TargetMember.GetAttributeInt(TargetBrand, "RegionID"), TargetBrand.Site.LanguageID);
            }
        }

        private bool FilterCompare(string value1, string value2, string op)
        {
            try
            {
                int value1int = Convert.ToInt32(value1);
                int value2int = Convert.ToInt32(value2);

                switch (op)
                {
                    case "1":
                        return (value1int == value2int);
                    case "2":
                        return (value1int != value2int);
                    case "3":
                        return (value1int < value2int);
                    case "4":
                        return (value1int <= value2int);
                    case "5":
                        return (value1int > value2int);
                    case "6":
                        return (value1int >= value2int);
                    case "7":
                        return (value2.IndexOf(value1) != -1);
                    default:
                        _logger.Error("Page pixel unknown filter operation. " + op.ToString());
                        return false;
                }
            }
            catch (System.Exception ex)
            {
                _logger.Error("Page pixel error running comparison.", ex);
                return false;
            }
        }

        private void UpdateDictionaryItem(Dictionary<String, Nullable<Boolean>> checkList, string key, bool temp)
        {
            if (!checkList.ContainsKey(key))
            {
                checkList.Add(key, temp);
            }
            else
            {
                temp |= checkList[key].Value;
                checkList.Remove(key);
                checkList.Add(key, temp);
            }
        }

        private int GetAge(DateTime birthDate)
        {
            if (birthDate == DateTime.MinValue) return 0;
            return (int)((double)DateTime.Now.Subtract(birthDate).Days / 365.25);
        }

        /// <summary>
        /// Case insensitive string replacer.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="find"></param>
        /// <param name="replace"></param>
        /// <returns></returns>
        private string ReplaceString(string source, string find, string replace)
        {
            return Regex.Replace(source, find, replace, RegexOptions.IgnoreCase);
        }

        private int GetAgeGroup(DateTime DOB)
        {
            int age = GetAge(DOB);
            return GetAgeGroup(age);
        }

        private int GetAgeGroup(int Age)
        {
            if (Age >= 18 && Age <= 25)
                return ConstantsTemp.AGE_GROUP_18_TO_25;
            else if (Age >= 26 && Age <= 34)
                return ConstantsTemp.AGE_GROUP_26_TO_34;
            else if (Age >= 35 && Age <= 45)
                return ConstantsTemp.AGE_GROUP_35_TO_45;
            else if (Age >= 46 && Age <= 60)
                return ConstantsTemp.AGE_GROUP_46_TO_60;
            else if (Age >= 60)
                return ConstantsTemp.AGE_GROUP_60_TO_99;
            else
                return 0;
        }

    }
}