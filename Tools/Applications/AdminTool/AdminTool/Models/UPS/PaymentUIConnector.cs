﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Newtonsoft.Json;
using Spark.Common;
using Spark.Common.UPS;
using Spark.CommonLibrary.Logging;
using Spark.Common.RestConsumer.V2.Models.Subscription;
using Newtonsoft.Json.Converters;


namespace AdminTool.Models.UPS
{
    /// <summary>
    /// Redirects subscription requests to PaymentUI using POST
    /// </summary>
    public class PaymentUIConnector : ManagerBase
    {
        #region Constants

        private const string PRIMARYKEY_UPSLEGACYDATAID = "UPSLegacyDataID";
        private const string PRIMARYKEY_UPSGLOBALLOGID = "UPSGlobalLogID";
        private Logger _logger = new Logger(typeof(Controller));
        #endregion

        /// <summary>
        /// Generates a POST form on the client browser and submits the form on body load.
        /// </summary>
        /// <param name="paymentUIJSON"></param>
        public string RedirectToPaymentUI(IPaymentUIJSON paymentUIJSON,int memberID, int brandID)
        {
            string html = "";
            try
            {
                // Get Version & Template ID.
                paymentUIJSON = PUI_PageData.Hydrate(paymentUIJSON, HttpContext.Current.Server.MapPath("/Models/UPS/PUI_Page.xml"));

                int newLegacyDataID = Constants.NULL_INT;
                int newGlobalLogID = Constants.NULL_INT;

                Matchnet.Content.ValueObjects.BrandConfig.Brand brand = BrandConfigSA.Instance.GetBrandByID(brandID);
                string paymentUIURL = (RuntimeSettings.GetSetting("UPS_PAYMENT_UI_URL", brand.Site.Community.CommunityID,
                                           brand.Site.SiteID, brand.BrandID));

                paymentUIURL += brand.Site.Name.Replace(".", String.Empty).ToLower();

                if (paymentUIJSON.SaveLegacyData == true)
                {
                    newLegacyDataID = KeySA.Instance.GetKey(PRIMARYKEY_UPSLEGACYDATAID);
                    newGlobalLogID = KeySA.Instance.GetKey(PRIMARYKEY_UPSGLOBALLOGID);
                    paymentUIJSON.UPSLegacyDataID = newLegacyDataID;
                    paymentUIJSON.UPSGlobalLogID = newGlobalLogID;
                }

                object payment = paymentUIJSON.GetJSONObject();
                PaymentJson paymentJSON = (PaymentJson)payment;
                string currentPaymentUIURL = "";
                if(payment is PaymentUiPostData)
                {
                    var paymentUiPostData = (PaymentUiPostData)payment;
                    currentPaymentUIURL = paymentUiPostData.PaymentUiUrl;
                    newGlobalLogID = paymentUiPostData.GlobalLogId;
                    paymentJSON = JsonConvert.DeserializeObject<PaymentJson>(paymentUiPostData.Json.Decrypt());
                   // html = POSTData(paymentUiPostData.Json, currentPaymentUIURL, newGlobalLogID);
                }
                else
                {
                     paymentJSON = (PaymentJson)payment;
                    //JS-958
                     //currentPaymentUIURL = (RuntimeSettings.GetSetting("UPS_PAYMENT_UI_URL", brand.Site.Community.CommunityID,brand.Site.SiteID, brand.BrandID));
                     currentPaymentUIURL = ConfigurationManager.AppSettings["UPS_PAYMENT_UI_URL"];
                     currentPaymentUIURL += brand.Site.Name.Replace(".", String.Empty).ToLower();
                }

               
                if (paymentJSON.Data.Packages.Count <= 0)
                {
                    bool ignoreForChangingBillingInformation = paymentJSON.TemplateID != Constants.NULL_INT && paymentJSON.TemplateID == 110;
                    if (!ignoreForChangingBillingInformation)
                    {
                        // Since there are no packages for upsale, than return back to the confirmation page 
                        string href = "";
                        if (HttpContext.Current.Request["oid"] != null)
                        {
                            href = string.Format("/Subscription/Confirmation?oid={0}&impmid={1}&imppid={2}",HttpContext.Current.Request["oid"],memberID,brandID) ;
                            _logger.Debug("No upsale packages exists so go back to the following URL: " + href);
                            HttpContext.Current.Response.Redirect(href);
                            return string.Empty;
                        }

                        return "No valid package to sell exists for this member.";
                    }
                }

                //Add Kount MerchantID
                paymentJSON.KountMerchantID = SubscriptionHelper.GetKountMerchantID(brand);

                string postString = JsonConvert.SerializeObject(paymentJSON, new IsoDateTimeConverter());
                _logger.Debug("UPS PostString:" + postString); 
                if (paymentUIJSON.SaveLegacyData)
                {
                    // Using the text compression method from Spark.Common. Cuts from 12kb to 2kb.
                    PurchaseSA.Instance.UPSLegacyDataSave(postString.Compress(), newLegacyDataID);
                }
                // Redirect to user's browser which will then POST to PUI on load.
                html = POSTData(postString.Encrypt(), currentPaymentUIURL, newGlobalLogID);
                
            }
            catch (Exception ex)
            {
                _logger.Error("Error redirecting to Payment UI.", ex);
            }
            return html;
        }

        /// <summary>
        /// This renders a POST form on the clients browser which then triggers a submit on body onload.
        /// Not the greatest idea.. Ugh.
        /// </summary>
        /// <param name="postString"></param>
        /// <param name="url"></param>
        private string POSTData(string postString, string url, int newGlobalLogID)
        {
            try
            {
                _logger.Debug("Begin posting to URL: " + url);

                var sb = new StringBuilder();
                sb.Append("<html><head>" + Environment.NewLine);
                sb.Append(string.Format("</head><body onload=\"document.{0}.submit()\">", "form1"));
                sb.Append(string.Format("<form id=\"form1\" name=\"{0}\" method=\"{1}\" action=\"{2}\" >" + Environment.NewLine, "form1", "POST", url));
                sb.Append(string.Format("<input type=\"hidden\" name=\"{0}\" ID=\"{0}\" value=\"{1}\" />" + Environment.NewLine, "requestData", postString));
                sb.Append(string.Format("<input type=\"hidden\" name=\"{0}\" ID=\"{0}\" value=\"{1}\" />" + Environment.NewLine, "globalLogID", Convert.ToString(newGlobalLogID)));
                sb.Append("</form>" + Environment.NewLine);
                sb.Append("</body></html>");

                _logger.Debug("Post data: " + sb.ToString());
                _logger.Debug("Created a form to submit to URL: " + url);
                return sb.ToString();
            }
            catch (Exception ex)
            {
                _logger.Debug("LogException  Error message   \r\n :" + ex.Message);
                _logger.Debug("LogException  Error   \r\n :" + ex.ToString());

                throw new Exception("Error in POST to Payment UI. Error Message: " + ex.Message + ", Error: " + ex + ", PostString:" + postString
                    + "URL:" + url, ex);
            }
        }

        /// <summary>
        /// This is a hack(woohoo) to map Currency Enum to the proper abbreviation.
        /// </summary>
        /// <param name="description"></param>
        public static string GetCurrencyAbbreviation(CurrencyType currencyType)
        {
            switch (currencyType)
            {
                case CurrencyType.USDollar:
                    return "USD";
                case CurrencyType.Euro:
                    return "EUR";
                case CurrencyType.CanadianDollar:
                    return "CAD";
                case CurrencyType.Pound:
                    return "GBP";
                case CurrencyType.AustralianDollar:
                    return "AUD";
                case CurrencyType.Shekels:
                    return "NIS";
                case CurrencyType.VerifiedByVisa:
                    return "VBV";
                default:
                    return "None";
            }
        }
    }
}