﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Matchnet;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Lib;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Spark.Common.UPS;
using Language = Matchnet.Language;

namespace AdminTool.Models.UPS
{
    public class PaymentProfilePage : UserControl, IPaymentUIJSON
    {
        private int version = Constants.NULL_INT;
        private int templateID = Constants.NULL_INT;
        private PaymentType paymentType;
        private int upsLegacyDataID = Constants.NULL_INT;
        private int UPSGlobalLogID = Constants.NULL_INT;
        private bool saveLegacyData = true;

        #region IPaymentUIJSON Members

        #region Properties
        bool IPaymentUIJSON.SaveLegacyData
        {
            get
            {
                return saveLegacyData;
            }
            set
            {
                saveLegacyData = true;
            }
        }

        int IPaymentUIJSON.Version
        {
            get
            {
                return version;
            }
            set
            {
                version = value;
            }
        }

        int IPaymentUIJSON.TemplateID
        {
            get
            {
                return templateID;
            }
            set
            {
                templateID = value;
            }
        }

        PaymentType IPaymentUIJSON.PaymentType
        {
            get
            {
                return paymentType;
            }
            set
            {
                paymentType = value;
            }
        }

        int IPaymentUIJSON.UPSLegacyDataID
        {
            get
            {
                return upsLegacyDataID;
            }
            set
            {
                upsLegacyDataID = value;
            }
        }

        int IPaymentUIJSON.UPSGlobalLogID
        {
            get
            {
                return UPSGlobalLogID;
            }
            set
            {
                UPSGlobalLogID = value;
            }
        }
        #endregion

        public Member TargetMember { get; set; }
        public Matchnet.Content.ValueObjects.BrandConfig.Brand TargetBrand { get; set; }

        public PaymentProfilePage(int memberID, int brandID)
        {
            TargetMember = MemberHelper.GetMember(memberID, MemberLoadFlags.IngoreSACache);
            TargetBrand = BrandConfigSA.Instance.GetBrands().GetBrand(brandID);
        }

        object IPaymentUIJSON.GetJSONObject()
        {
            if (version == Constants.NULL_INT)
                throw new Exception("Version cannot be null.");

            if (templateID == Constants.NULL_INT)
                throw new Exception("TemplateID cannot be null.");

            try
            {
                if (TargetMember == null)
                    throw new Exception("Member cannot null.");

                var payment = new PaymentJson();

                payment.Version = version;
                payment.CallingSystemID = TargetBrand.Site.SiteID;
                payment.TemplateID = templateID;
                payment.TimeStamp = DateTime.Now;
                if (HttpContext.Current.Request.UrlReferrer != null)
                    payment.Data.Navigation.CancelURL = HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                else
                    payment.Data.Navigation.CancelURL = "http://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port > 0 ? ":" + HttpContext.Current.Request.Url.Port : "");
                payment.Data.Navigation.ReturnURL = "http://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port > 0 ? ":" + HttpContext.Current.Request.Url.Port : "");
                payment.GetCustomerPaymentProfile = 1;

                // Member Info
                payment.Data.MemberInfo.CustomerID = TargetMember.MemberID;
                payment.Data.MemberInfo.RegionID = TargetMember.GetAttributeInt(TargetBrand, "RegionID");
                payment.Data.MemberInfo.Language = ((Language)Enum.Parse(typeof(Language), TargetBrand.Site.LanguageID.ToString())).ToString();
                var regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(payment.Data.MemberInfo.RegionID, (int)Language.English);
                if (regionLanguage.CountryRegionID == ConstantsTemp.REGIONID_USA)
                    payment.Data.MemberInfo.State = regionLanguage.StateAbbreviation;

                //LC: Adding extra fields needed for Kount RIS
                payment.Data.MemberInfo.EmailAddress = TargetMember.GetAttributeText(TargetBrand, "EmailAddress");
                payment.Data.MemberInfo.GenderMask = SubscriptionHelper.GetGender(TargetMember.GetAttributeInt(TargetBrand, "gendermask"));
                payment.Data.MemberInfo.BirthDate = TargetMember.GetAttributeDate(TargetBrand, "birthdate", DateTime.MinValue);
                payment.Data.MemberInfo.BrandInsertDate = TargetMember.GetAttributeDate(TargetBrand, "BrandInsertDate", DateTime.MinValue);

                #region UDFS
                //JS-1228 LC: Adding UDF fields needed for Kount
                payment.Data.MemberInfo.UserName = TargetMember.GetUserName(TargetBrand);
                payment.Data.MemberInfo.MaritalStatus = SubscriptionHelper.GetDescription("MaritalStatus", TargetMember.GetAttributeInt(TargetBrand, "MaritalStatus"), TargetBrand);
                payment.Data.MemberInfo.Occupation = TargetMember.GetAttributeText(TargetBrand, "OccupationDescription");
                payment.Data.MemberInfo.Education = SubscriptionHelper.GetDescription("EducationLevel", TargetMember.GetAttributeInt(TargetBrand, "EducationLevel"), TargetBrand);
                payment.Data.MemberInfo.PromotionID = TargetMember.GetAttributeInt(TargetBrand, "PromotionID").ToString();
                payment.Data.MemberInfo.Eyes = SubscriptionHelper.GetDescription("EyeColor", TargetMember.GetAttributeInt(TargetBrand, "EyeColor"), TargetBrand);
                payment.Data.MemberInfo.Hair = SubscriptionHelper.GetDescription("HairColor", TargetMember.GetAttributeInt(TargetBrand, "HairColor"), TargetBrand);
                payment.Data.MemberInfo.Height = TargetMember.GetAttributeInt(TargetBrand, "Height").ToString();
                //TODO: define webconstant to check Jdate as 3 
                if (TargetBrand.Site.Community.CommunityID == 3)
                {
                    payment.Data.MemberInfo.Ethnicity = SubscriptionHelper.GetDescription("JDateEthnicity", TargetMember.GetAttributeInt(TargetBrand, "JDateEthnicity"), TargetBrand);
                    payment.Data.MemberInfo.Religion = SubscriptionHelper.GetDescription("JDateReligion", TargetMember.GetAttributeInt(TargetBrand, "JDateReligion"), TargetBrand);
                }
                else
                {
                    payment.Data.MemberInfo.Ethnicity = SubscriptionHelper.GetDescription("Ethnicity", TargetMember.GetAttributeInt(TargetBrand, "Ethnicity"), TargetBrand);
                    payment.Data.MemberInfo.Religion = SubscriptionHelper.GetDescription("Religion", TargetMember.GetAttributeInt(TargetBrand, "Religion"), TargetBrand);
                }

                string aboutMe = TargetMember.GetAttributeText(TargetBrand, "AboutMe");
                payment.Data.MemberInfo.AboutMe = aboutMe.Length < 251 ? aboutMe : aboutMe.Substring(0, 250);

                #endregion UDF
                var renewalSub = Spark.Common.Adapter.RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetCurrentRenewalSubscription(TargetMember.MemberID, TargetBrand.Site.SiteID);
                if (renewalSub == null || renewalSub.RenewalSubscriptionID <= 0)
                {
                    var defaultPaymentProfileInfo = Spark.Common.Adapter.PaymentProfileServiceWebAdapter.GetProxyInstance().GetMemberDefaultPaymentProfile(TargetMember.MemberID, TargetBrand.Site.SiteID);
                    if (defaultPaymentProfileInfo != null)
                    {
                        // Default payment profile may exists from a free trial subscription 
                        // If the member took a free trial subscription, than show the details of the billing information used to take the free trial subscription. 
                        payment.Data.MemberInfo.MonthlyRenewalRate = 0.0m;
                        payment.Data.MemberInfo.NextRenewalDate = String.Empty;
                    }
                    else
                    {
                        throw new Exception("MemberSub cannot be null. Member must have an active or a past subsription.");
                    }
                }
                else
                {
                    var plan = Matchnet.Purchase.ServiceAdapters.PlanSA.Instance.GetPlan(renewalSub.PrimaryPackageID, TargetBrand.BrandID);
                    payment.Data.MemberInfo.MonthlyRenewalRate = plan.RenewCost;
                    payment.Data.MemberInfo.NextRenewalDate = renewalSub.RenewalDatePST.ToShortDateString();
                }

                return payment;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in building JSON for PaymentUI. MemberID:" +
                    TargetMember.MemberID.ToString(), ex);
            }
        }

        #endregion
    }
}