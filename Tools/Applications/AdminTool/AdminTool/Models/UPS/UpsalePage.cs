﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AdminTool.Models.SparkAPI;
using Matchnet;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ServiceAdapters.Links;
using Matchnet.Content.ValueObjects.AttributeOption;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Lib;
using Matchnet.Member.ValueObjects.Admin;
using Matchnet.OmnitureHelper;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Newtonsoft.Json;
using MPVO = Matchnet.Purchase.ValueObjects;
using Matchnet.Content.ValueObjects.PagePixel;
using Matchnet.Member.ServiceAdapters;
using Spark.Common.Adapter;
using Spark.Common.OrderHistoryService;
using Spark.Common.PaymentProfileService;
using Spark.Common.UPS;
using Spark.CommonLibrary;
using Spark.CommonLibrary.Logging;
using Language = Matchnet.Language;
using Spark.Common.RestConsumer.V2.Models.Subscription;

namespace AdminTool.Models.UPS
{
    public class UpsalePage : IPaymentUIJSON
    {
        private int version = Constants.NULL_INT;
        private int templateID = Constants.NULL_INT;
        private Matchnet.Purchase.ValueObjects.PaymentType paymentType;
        private int upsLegacyDataID = Constants.NULL_INT;
        private int UPSGlobalLogID = Constants.NULL_INT;
        private bool saveLegacyData = true;
         private Member targetMember = null;
         private string _clientIP;
        private Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrand = null;
        private Logger _logger = new Logger(typeof(Controller));

        public Dictionary<String, String> OrderAttributes { get; set; }
        public Member TargetMember { get { return targetMember; } }
        public Matchnet.Content.ValueObjects.BrandConfig.Brand TargetBrand { get { return targetBrand; } }
        public HttpContext Context { get; set; }

        public UpsalePage(int memberID, int brandID)
        {
            targetMember = MemberHelper.GetMember(memberID, MemberLoadFlags.IngoreSACache);
            targetBrand = BrandConfigSA.Instance.GetBrands().GetBrand(brandID);
        }

        public string ClientIP
        {
            get
            {
                if (_clientIP == null)
                {
                    _clientIP = HttpContext.Current.Request.Headers["client-ip"];
                    if (_clientIP == null)
                    {
                        _clientIP = HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];
                    }

                    Int32 commaPos = _clientIP.IndexOf(",");
                    if (commaPos > -1)
                    {
                        _clientIP = _clientIP.Substring(0, commaPos);
                    }
                }

                return _clientIP;
            }
        }
        #region IPaymentUIJSON Members

        bool IPaymentUIJSON.SaveLegacyData
        {
            get
            {
                return saveLegacyData;
            }
            set
            {
                saveLegacyData = true;
            }
        }

        int IPaymentUIJSON.Version
        {
            get
            {
                return version;
            }
            set
            {
                version = value;
            }
        }

        int IPaymentUIJSON.TemplateID
        {
            get
            {
                return templateID;
            }
            set
            {
                templateID = value;
            }
        }

        Matchnet.Purchase.ValueObjects.PaymentType IPaymentUIJSON.PaymentType
        {
            get
            {
                return paymentType;
            }
            set
            {
                paymentType = value;
            }
        }

        int IPaymentUIJSON.UPSLegacyDataID
        {
            get
            {
                return upsLegacyDataID;
            }
            set
            {
                upsLegacyDataID = value;
            }
        }

        int IPaymentUIJSON.UPSGlobalLogID
        {
            get
            {
                return UPSGlobalLogID;
            }
            set
            {
                UPSGlobalLogID = value;
            }
        }

        private bool UseAPIForPaymentJSON()
        {
            return Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USE_API_FOR_UPS_SUBSCRIPTION_PAYMENT_JSON", TargetBrand.Site.Community.CommunityID, TargetBrand.Site.SiteID, TargetBrand.BrandID));
        }

        object IPaymentUIJSON.GetJSONObject()
        {
            if (UseAPIForPaymentJSON())
                return GetAPIPaymentJson();
            else
                return GetPaymentJson();
        }

        private PaymentUiPostData GetAPIPaymentJson()
        {


            if (version == Constants.NULL_INT)
                throw new Exception("Version cannot be null.");

            if (templateID == Constants.NULL_INT)
                throw new Exception("TemplateID cannot be null.");

            if (paymentType == null)
                throw new Exception("PaymentType cannot be null.");

            if (upsLegacyDataID == Constants.NULL_INT)
                throw new Exception("UPS Legacy Data ID cannot be null.");


            try
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetAPIPaymentJson", "ENTER PaymentUIConnector jump page", null, 1, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);


                PaymentUiPostData paymentData = null;

                int adminID = Constants.NULL_INT;
                string adminDomainName = String.Empty;

                AdminMemberDomainMapperCollection adminMapperList = MemberSA.Instance.GetAdminMemberDomainMappers();
                if (adminMapperList != null && adminMapperList.Count > 0)
                {
                    AdminMemberDomainMapper mapper = adminMapperList.GetAdminMemberDomainMapper(HttpContext.Current.User.Identity.Name);
                    if (mapper != null)
                    {
                        adminID = mapper.MemberID;
                        adminDomainName = mapper.DomainAccount;
                    }
                }

                var request = new PaymentUiPostDataRequest
                {
                    PaymentUiPostDataType = "2",
                    CancelUrl = "http://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port > 0 ? ":" + HttpContext.Current.Request.Url.Port : ""),
                    ConfirmationUrl = "http://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port > 0 ? ":" + HttpContext.Current.Request.Url.Port : "") + "/Subscription/Confirmation",
                    DestinationUrl = string.Empty,
                    ReturnUrl = "http://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port > 0 ? ":" + HttpContext.Current.Request.Url.Port : ""),
                    ClientIp = ClientIP,
                    PrtTitle = string.Empty,
                    SrId = (Context.Request["srid"] != null) ? Convert.ToString(Context.Request["srid"]).Replace(",", ";") : string.Empty,
                    PrtId = ((int)PurchaseReasonType.AdminPurchasesOnBehalfOfMember).ToString(),
                    OmnitureVariables = new Dictionary<string, string>(),
                    MemberLevelTrackingLastApplication = "1",
                    MemberLevelTrackingIsMobileDevice = "false",
                    MemberLevelTrackingIsTablet = "false",
                    PromoType = "0",
                    MemberID = TargetMember.MemberID,
                    AdminDomainName = adminDomainName,
                    AdminMemberID = adminID,
                    PaymentType = (int)paymentType,
                    TemplateId = templateID.ToString()
                };

                if (Context.Request["PromoID"] != null)
                {
                    request.PromoID = Convert.ToInt32(Context.Request["PromoID"]);
                }



                #region Omniture Variables
                request.OmnitureVariables.Add("pageName", "Ala Carte - Select Product");
                request.OmnitureVariables.Add("prop17", OmnitureHelper.GetProfileCompetionPercentage(TargetMember, TargetBrand));
                request.OmnitureVariables.Add("prop18", OmnitureHelper.GetGender(TargetMember.GetAttributeInt(TargetBrand, WebConstants.ATTRIBUTE_NAME_GENDERMASK)));
                request.OmnitureVariables.Add("prop19", OmnitureHelper.GetAge(TargetMember.GetAttributeDate(TargetBrand, WebConstants.ATTRIBUTE_NAME_BIRTHDATE)).ToString());

                string attributeName = "Ethnicity";
                if (TargetBrand.Site.Community.CommunityID == (int)CommunityIDs.JDate)
                {
                    attributeName = "JDateEthnicity";
                }
                Hashtable resourceBag = GeneralHelper.GetResourceCollection(GeneralHelper.DetermineResourceFileName(TargetBrand));
                string prop20 = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, TargetMember, TargetBrand, attributeName);
                request.OmnitureVariables.Add("prop20", prop20);

                request.OmnitureVariables.Add("prop21", OmnitureHelper.GetRegionString(TargetMember, TargetBrand, TargetBrand.Site.LanguageID, false, true, false).Replace("\"", string.Empty));
                request.OmnitureVariables.Add("prop23", TargetMember.MemberID.ToString());
                request.OmnitureVariables.Add("eVar44", string.Format(WebConstants.OMNITURE_EVAR44_MEMBERID, (TargetMember.MemberID % 10).ToString()));
                string prop29 = (HttpContext.Current != null && HttpContext.Current.Request.UrlReferrer != null)
                                    ? HttpContext.Current.Request.UrlReferrer.ToString()
                                    : string.Empty;
                request.OmnitureVariables.Add("prop29", prop29);
                request.OmnitureVariables.Add("eVar6", ((int)PurchaseReasonType.AdminPurchasesOnBehalfOfMember).ToString());
               

                #endregion

                APIPaymentUIJSON apiPaymentUIPSON = new APIPaymentUIJSON(ApiClient.GetInstance());
                paymentData = apiPaymentUIPSON.PostPayment(request, TargetBrand.BrandID.ToString());

                if (paymentData == null)
                    throw new Exception("Unable to get payment data from Subcription. CreditCard page");



                return paymentData;
            }
            catch (Exception ex)
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetAPIPaymentJson", "ERROR DETECTED: " + ex.Message, null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.ERROR, DateTime.Now);

                throw new Exception("GetAPIPaymentJson Error in geting JSON from API. MemberID:" + TargetMember.MemberID.ToString(), ex);
            }
            finally
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetAPIPaymentJson", "EXIT PaymentUIConnector jump page", null, 0, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
            }


        }

        private PaymentJson GetPaymentJson()
        {
            if (version == Constants.NULL_INT)
                throw new Exception("Version cannot be null.");

            if (templateID == Constants.NULL_INT)
                throw new Exception("TemplateID cannot be null.");

            if (paymentType == null)
                throw new Exception("PaymentType cannot be null.");

            if (upsLegacyDataID == Constants.NULL_INT)
                throw new Exception("UPS Legacy Data ID cannot be null.");

            var colDataAttributes = new Dictionary<string, string>();

            try
            {
                if (TargetMember == null)
                    throw new Exception("Member cannot null.");

                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "ENTER PaymentUIConnector jump page", null, 1, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);

                int memberID = TargetMember.MemberID;
                _logger.Debug("Begin gathering JSON data in the upsale page");

                var payment = new PaymentJson();
                string affiliateTrackingPixel = Constants.NULL_STRING;

                // Only show the confirmation section in the upsale page if going directly to the upsale
                // page without previously showing the confirmation page after a subscription purchase 
                if (Convert.ToBoolean((RuntimeSettings.GetSetting("UPS_DIRECT_TO_UPSALE_WITH_NO_CONFIRMATION_PAGE", TargetBrand.Site.Community.CommunityID,
                                                    TargetBrand.Site.SiteID, TargetBrand.BrandID))))
                {
                    int orderID = Constants.NULL_INT;
                    OrderInfo orderInfo = null;
                    ObsfucatedPaymentProfileResponse paymentProfileResponse = null;

                    if (Context.Request["oid"] != null)
                    {
                        orderID = Convert.ToInt32(Context.Request["oid"]);
                        _logger.Debug("Get order information for Order ID [" + Convert.ToString(orderID) + "]");

                        //get UPS order info
                        orderInfo = OrderHistoryServiceWebAdapter.GetProxyInstance().GetOrderInfo(orderID);
                        OrderHistoryServiceWebAdapter.CloseProxyInstance();

                        // Get the affiliate tracking pixel for the subscription confirmation page 
                        var ucPixel = new PagePixelControl(TargetMember, TargetBrand);
                        ucPixel.PageID = 3500;
                        ucPixel.SubAmt = (orderInfo != null) ? orderInfo.TotalAmount.ToString() : "";
                        ucPixel.SubDuration = (orderInfo != null) ? orderInfo.Duration.ToString() : "";
                        affiliateTrackingPixel = ucPixel.WritePixel();

                        if (orderInfo != null)
                        {
                            _logger.Debug("Successfully retrieved order information for Order ID [" + Convert.ToString(orderID) + "]");

                            //get payment profile info
                            paymentProfileResponse =
                                PaymentProfileServiceWebAdapter.GetProxyInstance().GetObsfucatedPaymentProfileByOrderID(orderID, orderInfo.CustomerID, orderInfo.CallingSystemID);
                            PaymentProfileServiceWebAdapter.CloseProxyInstance();

                            string orderCustomerID = Constants.NULL_STRING;
                            string orderConfirmationNumber = Constants.NULL_STRING;
                            string orderCreditCardNumber = Constants.NULL_STRING;
                            string orderCreditCardType = Constants.NULL_STRING;
                            string orderTransactionDate = DateTime.Now.ToString();
                            string orderPaymentType = Constants.NULL_STRING;
                            string orderTotalAmount = Constants.NULL_STRING;

                            if (paymentProfileResponse != null && orderInfo != null)
                            {
                                _logger.Debug("Successfully retrieved payment profile information for Order ID [" + Convert.ToString(orderID) + "]");

                                orderConfirmationNumber = orderID.ToString();
                                orderCustomerID = TargetMember.GetAttributeText(TargetBrand, "UserName");
                                orderPaymentType = paymentProfileResponse.PaymentProfile.PaymentType;
                                orderTotalAmount = orderInfo.TotalAmount.ToString();
                                orderTransactionDate = DateTime.Now.ToString();

                                if (paymentProfileResponse.PaymentProfile is ObsfucatedCreditCardPaymentProfile)
                                {
                                    var ccPaymentProfile = paymentProfileResponse.PaymentProfile as ObsfucatedCreditCardPaymentProfile;
                                    orderCreditCardType = ccPaymentProfile.CardType;
                                    orderCreditCardNumber = ccPaymentProfile.LastFourDigitsCreditCardNumber;
                                }
                                else if (paymentProfileResponse.PaymentProfile is ObfuscatedCreditCardShortFormMobileSitePaymentProfile)
                                {
                                    var creditCardShortFormMobileSitePaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedCreditCardShortFormMobileSitePaymentProfile;
                                    orderCreditCardType = creditCardShortFormMobileSitePaymentProfile.CardType;
                                    orderCreditCardNumber = creditCardShortFormMobileSitePaymentProfile.LastFourDigitsCreditCardNumber;
                                }
                                else if (paymentProfileResponse.PaymentProfile is ObfuscatedCreditCardShortFormFullWebSitePaymentProfile)
                                {
                                    var creditCardShortFormFullWebSitePaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedCreditCardShortFormFullWebSitePaymentProfile;
                                    orderCreditCardType = creditCardShortFormFullWebSitePaymentProfile.CardType;
                                    orderCreditCardNumber = creditCardShortFormFullWebSitePaymentProfile.LastFourDigitsCreditCardNumber;
                                }
                                else if (paymentProfileResponse.PaymentProfile is ObfuscatedDebitCardPaymentProfile)
                                {
                                    var debitCardPaymentProfile = paymentProfileResponse.PaymentProfile as ObfuscatedDebitCardPaymentProfile;
                                    orderCreditCardType = debitCardPaymentProfile.CardType;
                                    orderCreditCardNumber = debitCardPaymentProfile.LastFourDigitsCreditCardNumber;
                                }

                                PaymentJsonArray orderConfirmation = new PaymentJsonArray();

                                orderConfirmation.Add("orderCustomerID", orderCustomerID);
                                orderConfirmation.Add("orderConfirmationNumber", orderConfirmationNumber);
                                orderConfirmation.Add("orderCreditCardNumber", orderCreditCardNumber);
                                orderConfirmation.Add("orderCreditCardType", orderCreditCardType);
                                orderConfirmation.Add("orderTransactionDate", orderTransactionDate);
                                orderConfirmation.Add("orderPaymentType", orderPaymentType);
                                orderConfirmation.Add("orderTotalAmount", orderTotalAmount);

                                payment.Data.OrderConfirmation.Add(orderConfirmation);
                            }
                            else
                            {
                                throw new Exception("Cannot get payment profile details for the order confirmation.");
                            }
                        }
                        else
                        {
                            throw new Exception("Cannot get order details for the order confirmation.");
                        }
                    }
                }
                // Get the most recent Plan ID that the member is on  
                Spark.Common.RenewalService.RenewalSubscription renewalSub = Spark.Common.Adapter.RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetCurrentRenewalSubscription(TargetMember.MemberID, TargetBrand.Site.SiteID);
                if (renewalSub == null || renewalSub.RenewalSubscriptionID <= 0 || (renewalSub.RenewalDatePST < DateTime.Now))
                {
                    _logger.Debug("Cannot find RenewalSubscription for Member ID [" + Convert.ToString(memberID) + "]");
                    _logger.Debug("Redirect to the subscription page since the member does not have a basic subscription privilege");

                    // Basic subscription privilege is required
                    // If the member does not have basic subscription privilege, redirect the user to the subscription
                    // page to purchase a basic subscription 
                    string url = string.Format("/Subscription/Credit?impmid={0}&imppid={1}&prtid={2}", TargetMember.MemberID,
                                                      TargetBrand.BrandID, (int)Matchnet.Content.ServiceAdapters.Links.PurchaseReasonType.AttemptToAccessUpsalePackagesWithNoSubscription);
                    Context.Response.Redirect(url);
                    return null;
                }

                int activePlanID = renewalSub.PrimaryPackageID;

                // Get all the privileges that have not expired for this member for this site  
                Spark.Common.AccessService.AccessPrivilege[] accessPrivileges = Spark.Common.Adapter.AccessServiceWebAdapter.GetProxyInstanceForBedrock().GetCustomerPrivilegeListAllWithIgnoreCache(memberID);
                var activePrivileges = new List<Spark.Common.AccessService.AccessPrivilege>();
                foreach (Spark.Common.AccessService.AccessPrivilege checkAccessPrivilege in accessPrivileges)
                {
                    if (checkAccessPrivilege.CallingSystemID == TargetBrand.Site.SiteID)
                    {
                        if (checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.BasicSubscription
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.HighlightedProfile
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.SpotlightMember
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.JMeter
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.AllAccess
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.ReadReceipt)
                        {
                            if (checkAccessPrivilege.EndDateUTC > DateTime.Now)
                            {
                                activePrivileges.Add(checkAccessPrivilege);
                            }
                        }
                    }
                }
                // Get the details of the most recent plan that the member is on  
                Spark.Common.CatalogService.Package activeSubscriptionPackage = Spark.Common.Adapter.CatalogServiceWebAdapter.GetProxyInstance().GetPackageDetails(activePlanID);
                Spark.Common.Adapter.CatalogServiceWebAdapter.CloseProxyInstance();
                string upsaleFromType = "Standard";
                if (activeSubscriptionPackage.PackageType == Spark.Common.CatalogService.PackageType.Bundled)
                {
                    upsaleFromType = "BundledWithPremiumServices";
                }
                else if (activeSubscriptionPackage.PackageType == Spark.Common.CatalogService.PackageType.Basic)
                {
                    // Check to see if the member had purchased any a la carte premium service in addition to the
                    // standard plan that the member has most recently purchased
                    bool hasAtLeastOneActivePremiumService = false;

                    foreach (Spark.Common.AccessService.AccessPrivilege checkAccessPrivilege in activePrivileges)
                    {
                        if (checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.HighlightedProfile
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.SpotlightMember
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.JMeter
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.AllAccess
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.ReadReceipt)
                        {
                            hasAtLeastOneActivePremiumService = true;
                        }
                    }
                    if (hasAtLeastOneActivePremiumService)
                    {
                        upsaleFromType = "StandardWithAlacarte";
                    }
                    else
                    {
                        upsaleFromType = "Standard";
                    }
                }
                // Get the packages allowed for upsale for this member 
                Spark.Common.CatalogService.Item activeSubscriptionPackageBasicItem = null;
                foreach (Spark.Common.CatalogService.Item item in activeSubscriptionPackage.Items)
                {
                    var itemPrivileges = new List<Spark.Common.CatalogService.PrivilegeType>();
                    itemPrivileges.AddRange(item.PrivilegeType);
                    if (itemPrivileges.Contains(Spark.Common.CatalogService.PrivilegeType.BasicSubscription))
                    {
                        // Use the basic subscription item in the package to determine the duration and duration type of the package  
                        activeSubscriptionPackageBasicItem = item;
                        break;
                    }
                }

                var upsalePackages = new List<int>();
                if (activeSubscriptionPackageBasicItem != null)
                {
                    int[] arrUpsalePackages =
                        Spark.Common.Adapter.CatalogServiceWebAdapter.GetProxyInstance().GetUpsalePackages(
                            activeSubscriptionPackageBasicItem.Duration,
                            Enum.GetName(typeof(DurationType), activeSubscriptionPackageBasicItem.DurationType),
                            TargetBrand.Site.SiteID, upsaleFromType, activeSubscriptionPackageBasicItem.RenewalDuration,
                            Enum.GetName(typeof(DurationType), activeSubscriptionPackageBasicItem.RenewalDurationType));

                    Spark.Common.Adapter.CatalogServiceWebAdapter.CloseProxyInstance();
                    upsalePackages.AddRange(arrUpsalePackages);
                }

                var colPlansAvailableForUpsale = new List<MPVO.Plan>();
                foreach (int planID in upsalePackages)
                {
                    MPVO.Plan upsalePlan = PlanSA.Instance.GetPlan(planID, TargetBrand.BrandID);

                    if (upsalePlan != null)
                    {
                        colPlansAvailableForUpsale.Add(upsalePlan);
                    }
                }

                // Go through the upsale package list and remove the ones that are not allowed for purchase
                // since the member already has the maximum privilege time for upsale package  
                var allowedUpsalePlanCollection = new List<Matchnet.Purchase.ValueObjects.Plan>();

                // There is an upsale package that contains both all access and renewable all access emails 
                // This package is only available for upsale if the all access expiration date is before the 
                // basic subscription expiration date 
                // There is no need to check the count for the all access email part of the package 
                // If the user wants to purchase more all access emails, they can do that as a fixed pricing upgrade 
                // In the fixed pricing upgrade purchase, there will be a check to see what the limit
                // of the all access emails are 
                foreach (MPVO.Plan plan in colPlansAvailableForUpsale)
                {
                    if ((plan.PremiumTypeMask & PremiumType.HighlightedProfile) == PremiumType.HighlightedProfile)
                    {
                        if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.HighlightedProfile, renewalSub.RenewalDatePST))
                        {
                            allowedUpsalePlanCollection.Add(plan);
                        }
                    }
                    else if ((plan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember)
                    {
                        if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.SpotlightMember, renewalSub.RenewalDatePST))
                        {
                            allowedUpsalePlanCollection.Add(plan);
                        }
                    }
                    else if ((plan.PremiumTypeMask & PremiumType.JMeter) == PremiumType.JMeter)
                    {
                        if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.JMeter, renewalSub.RenewalDatePST))
                        {
                            allowedUpsalePlanCollection.Add(plan);
                        }
                    }
                    else if ((plan.PremiumTypeMask & PremiumType.AllAccess) == PremiumType.AllAccess)
                    {
                        if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.AllAccess, renewalSub.RenewalDatePST))
                        {
                            allowedUpsalePlanCollection.Add(plan);
                        }
                    }
                    else if ((plan.PremiumTypeMask & PremiumType.ReadReceipt) == PremiumType.ReadReceipt)
                    {
                        if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.ReadReceipt, renewalSub.RenewalDatePST))
                        {
                            allowedUpsalePlanCollection.Add(plan);
                        }
                    }
                    //else if ((plan.PremiumTypeMask & PremiumType.AllAccessEmail) == PremiumType.AllAccessEmail)
                    //{
                    //    if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.AllAccessEmails, memberSub.RenewDate))
                    //    {
                    //        allowedUpsalePlanCollection.Add(plan);
                    //    }
                    //}
                }

                payment.UPSLegacyDataID = upsLegacyDataID;
                payment.UPSGlobalLogID = UPSGlobalLogID;
                payment.GetCustomerPaymentProfile = 1;
                payment.Version = version;
                payment.CallingSystemID = TargetBrand.Site.SiteID;
                payment.TemplateID = templateID;
                payment.TimeStamp = DateTime.Now;
                payment.Data.Navigation.CancelURL = "http://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port > 0 ? ":" + HttpContext.Current.Request.Url.Port : "");
                payment.Data.Navigation.ReturnURL = "http://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port > 0 ? ":" + HttpContext.Current.Request.Url.Port : "");
                payment.Data.Navigation.ConfirmationURL = "http://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port > 0 ? ":" + HttpContext.Current.Request.Url.Port : "") + "/Subscription/Confirmation";
                payment.Data.Navigation.DestinationURL = "";

                // Order Attributes
                OrderAttributes = new Dictionary<string, string>();

                OrderAttributes.Add("PRTID", ((int)PurchaseReasonType.AdminPurchasesOnBehalfOfMember).ToString());
                OrderAttributes.Add("SRID", (Context.Request["srid"] != null) ? Convert.ToString(Context.Request["srid"]).Replace(",", ";") : string.Empty);
                foreach (string key in OrderAttributes.Keys)
                {
                    if (OrderAttributes[key] != string.Empty)
                        payment.Data.OrderAttributes += key + "=" + OrderAttributes[key] + ",";
                }

                if (payment.Data.OrderAttributes.Length > 0)
                    payment.Data.OrderAttributes = payment.Data.OrderAttributes.TrimEnd(',');

                // Member Info
                payment.Data.MemberInfo.CustomerID = TargetMember.MemberID;
                payment.Data.MemberInfo.RegionID = TargetMember.GetAttributeInt(TargetBrand, "RegionID");
                payment.Data.MemberInfo.Language = ((Language)Enum.Parse(typeof(Language), TargetBrand.Site.LanguageID.ToString())).ToString();

                RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(payment.Data.MemberInfo.RegionID, (int)Language.English);
                if (regionLanguage.CountryRegionID == ConstantsTemp.REGIONID_USA)
                    payment.Data.MemberInfo.State = regionLanguage.StateAbbreviation.Trim().ToUpper();

                //LC: Adding extra fields needed for Kount RIS
                payment.Data.MemberInfo.EmailAddress = TargetMember.GetAttributeText(TargetBrand, "EmailAddress");
                payment.Data.MemberInfo.GenderMask = SubscriptionHelper.GetGender(TargetMember.GetAttributeInt(TargetBrand, "gendermask"));
                payment.Data.MemberInfo.BirthDate = TargetMember.GetAttributeDate(TargetBrand, "birthdate", DateTime.MinValue);
                payment.Data.MemberInfo.BrandInsertDate = TargetMember.GetAttributeDate(TargetBrand, "BrandInsertDate", DateTime.MinValue);

                #region UDFS
                //JS-1228 LC: Adding UDF fields needed for Kount
                payment.Data.MemberInfo.UserName = TargetMember.GetUserName(TargetBrand);
                payment.Data.MemberInfo.MaritalStatus = SubscriptionHelper.GetDescription("MaritalStatus", TargetMember.GetAttributeInt(TargetBrand, "MaritalStatus"), TargetBrand);
                payment.Data.MemberInfo.Occupation = TargetMember.GetAttributeText(TargetBrand, "OccupationDescription");
                payment.Data.MemberInfo.Education = SubscriptionHelper.GetDescription("EducationLevel", TargetMember.GetAttributeInt(TargetBrand, "EducationLevel"), TargetBrand);
                payment.Data.MemberInfo.PromotionID = TargetMember.GetAttributeInt(TargetBrand, "PromotionID").ToString();
                payment.Data.MemberInfo.Eyes = SubscriptionHelper.GetDescription("EyeColor", TargetMember.GetAttributeInt(TargetBrand, "EyeColor"), TargetBrand);
                payment.Data.MemberInfo.Hair = SubscriptionHelper.GetDescription("HairColor", TargetMember.GetAttributeInt(TargetBrand, "HairColor"), TargetBrand);
                payment.Data.MemberInfo.Height = TargetMember.GetAttributeInt(TargetBrand, "Height").ToString();
                //TODO: define webconstant to check Jdate as 3 
                if (TargetBrand.Site.Community.CommunityID == 3)
                {
                    payment.Data.MemberInfo.Ethnicity = SubscriptionHelper.GetDescription("JDateEthnicity", TargetMember.GetAttributeInt(TargetBrand, "JDateEthnicity"), TargetBrand);
                    payment.Data.MemberInfo.Religion = SubscriptionHelper.GetDescription("JDateReligion", TargetMember.GetAttributeInt(TargetBrand, "JDateReligion"), TargetBrand);
                }
                else
                {
                    payment.Data.MemberInfo.Ethnicity = SubscriptionHelper.GetDescription("Ethnicity", TargetMember.GetAttributeInt(TargetBrand, "Ethnicity"), TargetBrand);
                    payment.Data.MemberInfo.Religion = SubscriptionHelper.GetDescription("Religion", TargetMember.GetAttributeInt(TargetBrand, "Religion"), TargetBrand);
                }

                string aboutMe = TargetMember.GetAttributeText(TargetBrand, "AboutMe");
                payment.Data.MemberInfo.AboutMe = aboutMe.Length < 251 ? aboutMe : aboutMe.Substring(0, 250);

                #endregion UDF

                //// Omniture Variables
                var omnitureVariables = new PaymentJsonArray();

                omnitureVariables.Add("pageName", "Ala Carte - Select Product");
                omnitureVariables.Add("prop17", OmnitureHelper.GetProfileCompetionPercentage(TargetMember, TargetBrand));
                omnitureVariables.Add("prop18", OmnitureHelper.GetGender(TargetMember.GetAttributeInt(TargetBrand, WebConstants.ATTRIBUTE_NAME_GENDERMASK)));
                omnitureVariables.Add("prop19", OmnitureHelper.GetAge(TargetMember.GetAttributeDate(TargetBrand, WebConstants.ATTRIBUTE_NAME_BIRTHDATE)).ToString());

                string attributeName = "Ethnicity";
                if (TargetBrand.Site.Community.CommunityID == (int)CommunityIDs.JDate)
                {
                    attributeName = "JDateEthnicity";
                }
                Hashtable resourceBag = GeneralHelper.GetResourceCollection(GeneralHelper.DetermineResourceFileName(TargetBrand));
                string prop20 = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, TargetMember, TargetBrand, attributeName);
                omnitureVariables.Add("prop20", prop20);

                omnitureVariables.Add("prop21", OmnitureHelper.GetRegionString(TargetMember, TargetBrand, TargetBrand.Site.LanguageID, false, true, false).Replace("\"", string.Empty));
                omnitureVariables.Add("prop23", TargetMember.MemberID.ToString());
                if (payment.Data.OrderConfirmation.Count > 0)
                {
                    omnitureVariables.Add("events", "purchase,event48,event10");
                }
                else
                {
                    omnitureVariables.Add("events", "purchase,event48");
                }
                omnitureVariables.Add("eVar44", string.Format(WebConstants.OMNITURE_EVAR44_MEMBERID, (TargetMember.MemberID % 10).ToString()));
                string prop29 = (HttpContext.Current != null && HttpContext.Current.Request.UrlReferrer != null)
                                    ? HttpContext.Current.Request.UrlReferrer.ToString()
                                    : string.Empty;
                omnitureVariables.Add("prop29", prop29);
                omnitureVariables.Add("eVar6", ((int)PurchaseReasonType.AdminPurchasesOnBehalfOfMember).ToString());
                payment.Data.OmnitureVariables.Add(omnitureVariables);

                if (!string.IsNullOrEmpty(affiliateTrackingPixel))
                {
                    payment.Data.AffiliateTrackingPixel = affiliateTrackingPixel;
                }
                // Legacy Data Info
                int adminID = Constants.NULL_INT;
                AdminMemberDomainMapperCollection adminMapperList = MemberSA.Instance.GetAdminMemberDomainMappers();
                if (adminMapperList != null && adminMapperList.Count > 0)
                {
                    AdminMemberDomainMapper mapper = adminMapperList.GetAdminMemberDomainMapper(HttpContext.Current.User.Identity.Name);
                    if (mapper != null)
                    {
                        adminID = mapper.MemberID;
                    }
                }
                payment.Data.LegacyDataInfo.AdminMemberID = adminID;
                payment.Data.LegacyDataInfo.Member = TargetMember.MemberID;
                payment.Data.LegacyDataInfo.BrandID = TargetBrand.BrandID;
                payment.Data.LegacyDataInfo.SiteID = TargetBrand.Site.SiteID;

                var planIDList = new System.Text.StringBuilder();
                // A La Carte Plans
                // Should only be two. One of each premium service.
                foreach (MPVO.Plan plan in allowedUpsalePlanCollection)
                {
                    if (plan.EndDate != DateTime.MinValue)
                        continue;

                    PaymentJsonPackage package;
                    package = new PaymentJsonPackage();

                    package.ID = plan.PlanID;
                    package.Description = "";
                    package.SytemID = TargetBrand.Site.SiteID;
                    package.StartDate = renewalSub.RenewalDatePST; // memberSub.EndDate;
                    package.ExpiredDate = renewalSub.RenewalDatePST; // memberSub.EndDate;
                    package.CurrencyType = PaymentUIConnector.GetCurrencyAbbreviation(plan.CurrencyType);

                    package.Items.Add("InitialCostPerDuration", plan.InitialCost);
                    decimal proratedAmount = GetMemberProratedAmount(plan.InitialCost, renewalSub.RenewalDatePST,
                                                                     GetStartDateForUpsalePlan(plan, activePrivileges));
                    package.Items.Add("ProratedAmount", proratedAmount);
                    package.Items.Add("DurationType", "Day");
                    package.Items.Add("Duration",
                                      GetDuration(renewalSub.RenewalDatePST,
                                                  GetStartDateForUpsalePlan(plan, activePrivileges)));
                    package.Items.Add("PlanID", plan.PlanID);
                    package.Items.Add("CurrencyType", PaymentUIConnector.GetCurrencyAbbreviation(plan.CurrencyType));
                    package.Items.Add("PaymentTypeMask", plan.PaymentTypeMask);
                    package.Items.Add("CreditAmount", (plan.CreditAmount < 0) ? 0 : plan.CreditAmount);
                    package.Items.Add("PurchaseMode", PurchaseMode.New.ToString());
                    package.Items.Add("PremiumType", plan.PremiumTypeMask.ToString());
                    package.Items.Add("RenewCost", plan.RenewCost);

                    if (proratedAmount > 0)
                    {
                        // If the package for upsale has amount of 0, do not display this package. 
                        payment.Data.Packages.Add(package);

                        if (package.ID > 0)
                        {
                            planIDList.Append(package.ID.ToString() + ",");
                        }
                    }
                }
                _logger.Debug("JSON data to be sent: " + JsonConvert.SerializeObject(payment));

                colDataAttributes.Add("UPSLegacyDataID", Convert.ToString(upsLegacyDataID));
                colDataAttributes.Add("CustomerID", Convert.ToString(memberID));
                colDataAttributes.Add("CallingSystemID", Convert.ToString(TargetBrand.Site.SiteID));
                colDataAttributes.Add("TransactionType", "23");
                colDataAttributes.Add("PRTID", OrderAttributes["PRTID"]);
                colDataAttributes.Add("SRID", OrderAttributes["SRID"]);
                //colDataAttributes.Add("PricingGroupID", OrderAttributes["PricingGroupID"]);
                colDataAttributes.Add("RegionID", Convert.ToString(payment.Data.MemberInfo.RegionID));
                colDataAttributes.Add("IPAddress", payment.Data.MemberInfo.IPAddress);
                colDataAttributes.Add("TemplateID", Convert.ToString(payment.TemplateID));
                colDataAttributes.Add("CancelURL", payment.Data.Navigation.CancelURL);
                colDataAttributes.Add("ConfirmationURL", payment.Data.Navigation.ConfirmationURL);
                colDataAttributes.Add("DestinationURL", payment.Data.Navigation.DestinationURL);
                colDataAttributes.Add("PlanIDList", planIDList.ToString().TrimEnd(','));

                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "Save data attributes", colDataAttributes, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "NO ERROR", null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.OK, DateTime.Now);

                return payment;
            }
            catch (Exception ex)
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "Save data attributes", colDataAttributes, Constants.NULL_INT,
                                                                          Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "ERROR DETECTED: " + ex.Message, null,
                                                                          Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.ERROR,
                                                                          DateTime.Now);

                throw new Exception("Error in building JSON for PaymentUI. MemberID:" + TargetMember.MemberID, ex);
            }
            finally
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "EXIT PaymentUIConnector jump page", null, 0,
                                                                          Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
            }
           
        }
        #endregion

        private bool IsAllowedToPurchaseUpsalePlan(List<Spark.Common.AccessService.AccessPrivilege> activePrivileges, Spark.Common.AccessService.PrivilegeType checkPrivilegeType, DateTime renewalDate)
        {
            bool isAllowedToPurchaseUpsalePlan = false;

            Spark.Common.AccessService.AccessPrivilege checkPrivilege = activePrivileges.Find(delegate(Spark.Common.AccessService.AccessPrivilege privilege) { return privilege.UnifiedPrivilegeType == checkPrivilegeType; });
            if (checkPrivilege == null)
            {
                // Member can purchase this privilege since he does not have this privilege yet
                isAllowedToPurchaseUpsalePlan = true;
            }
            else
            {
                if (checkPrivilege.EndDatePST == DateTime.MinValue
                    || checkPrivilege.EndDatePST < DateTime.Now)
                {
                    // Member can purchase this privilege since he had this privilege but it expired  
                    isAllowedToPurchaseUpsalePlan = true;
                }
                else
                {
                    // The start date of the premium service should be either earlier than the renewal date 
                    // or the same as the renewal date. If the start date of the premium service is past the
                    // renewal date, then set the start date of the premium service to the renewal date. 
                    if (checkPrivilege.EndDatePST > renewalDate)
                    {
                        isAllowedToPurchaseUpsalePlan = false;
                    }
                    else
                    {
                        System.TimeSpan dateDifference = checkPrivilege.EndDatePST.Subtract(renewalDate);
                        if (Math.Abs(dateDifference.TotalMinutes) > 20)
                        {
                            // There is still additional time that member can purchase for this privilege
                            isAllowedToPurchaseUpsalePlan = true;
                        }
                        else
                        {
                            // The end date of the privilege is the same as the end date of the basic subscription  
                            // so do not allow the member to purchase this privilege  
                            isAllowedToPurchaseUpsalePlan = false;
                        }
                    }
                }
            }

            return isAllowedToPurchaseUpsalePlan;
        }

        private DateTime GetStartDateForUpsalePlan(MPVO.Plan plan, List<Spark.Common.AccessService.AccessPrivilege> activePrivileges)
        {
            Spark.Common.AccessService.PrivilegeType checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.None;

            if ((plan.PremiumTypeMask & PremiumType.HighlightedProfile) == PremiumType.HighlightedProfile)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.HighlightedProfile;
            }
            else if ((plan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.SpotlightMember;
            }
            else if ((plan.PremiumTypeMask & PremiumType.JMeter) == PremiumType.JMeter)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.JMeter;
            }
            else if ((plan.PremiumTypeMask & PremiumType.AllAccess) == PremiumType.AllAccess)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.AllAccess;
            }
            else if ((plan.PremiumTypeMask & PremiumType.ReadReceipt) == PremiumType.ReadReceipt)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.ReadReceipt;
            }
            else
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.None;
            }

            Spark.Common.AccessService.AccessPrivilege checkPrivilege = activePrivileges.Find(delegate(Spark.Common.AccessService.AccessPrivilege privilege) { return privilege.UnifiedPrivilegeType == checkPrivilegeType; });

            if (checkPrivilege == null)
            {
                return DateTime.Now.AddMinutes(60);
            }
            else
            {
                if (checkPrivilege.EndDatePST <= DateTime.Now)
                {
                    return DateTime.Now.AddMinutes(60);
                }
                else
                {
                    return checkPrivilege.EndDatePST;
                }
            }
        }

        private decimal GetMemberProratedAmount(decimal initialCost, DateTime endDate, DateTime startDate)
        {
            if (endDate < DateTime.Now)
            {
                throw new Exception("Subscription end date must be a future date. Enddate:" +
                                    endDate.ToString());
            }

            decimal amount = Constants.NULL_DECIMAL;
            decimal pricePerDay = Constants.NULL_DECIMAL;

            // The start date of the premium service should be either earlier than the renewal date 
            // or the same as the renewal date. If the start date of the premium service is past the
            // renewal date, then set the start date of the premium service to the renewal date. 
            if (startDate > endDate)
            {
                amount = 0.0m;
            }
            else
            {
                int monthsUntilRenewalDate = GetExactMonthsUntilRenewal(startDate, endDate);
                decimal subTotalFromMonths = (monthsUntilRenewalDate) * (initialCost);
                DateTime dteAfterMonthsAdded = startDate.AddMonths(monthsUntilRenewalDate);

                // Calculate cost per day
                DateTime dteEndDateOfRemainingDaysWithMonthAdded = dteAfterMonthsAdded.AddMonths(1);
                TimeSpan span = dteEndDateOfRemainingDaysWithMonthAdded.Subtract(dteAfterMonthsAdded);
                pricePerDay = (initialCost) / Convert.ToDecimal(span.Days);

                decimal subTotalFromRemainingDays = (pricePerDay) * (Convert.ToDecimal(GetDuration(endDate, dteAfterMonthsAdded)));

                amount = subTotalFromMonths + subTotalFromRemainingDays;
            }

            if (amount < 0)
                throw new Exception("Prorated Amount cannot be less then 0.");

            _logger.Debug("InitialCost:" + initialCost + "EndDate:" + endDate + "PricePerDay:" + pricePerDay + "Amount:" + amount);

            return amount;
        }

        private int GetExactMonthsUntilRenewal(DateTime startDate, DateTime endDate)
        {
            int monthDifference = System.Data.Linq.SqlClient.SqlMethods.DateDiffMonth(startDate, endDate);

            if (monthDifference > 0)
            {
                // Check to see if the number of months in between these two dates should be one less
                DateTime dteMonthDifferenceAdded = startDate.AddMonths(monthDifference);

                if (endDate < dteMonthDifferenceAdded)
                {
                    monthDifference = (monthDifference - 1);
                }
            }

            return monthDifference;
        }

        private int GetDuration(DateTime endDate, DateTime startDate)
        {
            // Get remaining days left on this plan
            // Whenever there is any remaining time in a day, give that full day of remaining credit to the 
            // member.  
            // Round 2.1 to 3 days
            // Round 2.9 to 3 days
            TimeSpan span = endDate.Subtract(startDate);
            Int32 remainingDays = Convert.ToInt32(Math.Ceiling(span.TotalDays));

            _logger.Debug("RemainingDays:" + remainingDays);

            return remainingDays;
        }
        
    }
}