﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.UPS
{
    public class SubscriptionData
    {
        public string MBoxScript { get; set; }
        //public string FreeTrialConfirmationText { get; set; }
        public string HeaderText { get; set; }
        //public string ColorCodeSubscriptionConfirmationText { get; set; }
        public string RecordInfoText { get; set; }
        public string UserName { get; set; }
        public string ConfirmationNumber { get; set; }
        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public string TransactionDate { get; set; }
        public string Cost { get; set; }
        public bool ShowOmnitureMboxSubscriptionConfirmation { get; set; }
        public bool ShowFreeTrialConfirmation { get; set; }
        public bool ShowFreeTrialConfirmationAdditional { get; set; }
        public bool ShowOtherStuff { get; set; }
        public bool ShowFreeTrialConfirmationButton { get; set; }
        public bool ShowColorCodeConfirmation { get; set;  }
        
    }
}