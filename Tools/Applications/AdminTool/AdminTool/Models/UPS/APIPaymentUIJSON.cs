﻿#region

using AdminTool.Models.SparkAPI;
using Newtonsoft.Json;
using Spark.Common.RestConsumer.V2.Models.Subscription;
using System.Collections.Generic;

#endregion

namespace AdminTool.Models.UPS
{
    public class APIPaymentUIJSON : ApiBase
    {
        public PaymentUiPostData PostPayment(PaymentUiPostDataRequest data, string brandID)
        {
            string prtid = null;
            var customprtid = prtid;


            var paymentDictionary = new Dictionary<string, string>
            {
                   {"AdminMemberID", data.AdminMemberID.ToString()},
                   {"AdminDomainName", data.AdminDomainName},
                   {"MemberID", data.MemberID.ToString()},
                   {"PaymentUiPostDataType", data.PaymentUiPostDataType},
                   {"PromoType",data.PromoType}, 
                   {"PaymentType",data.PaymentType.ToString()}, 
                   {"CancelUrl", data.CancelUrl},
                   {"ConfirmationUrl", data.ConfirmationUrl },
                   {"DestinationUrl", data.DestinationUrl },
                   {"ReturnUrl", data.ReturnUrl},
                   {"ClientIp", data.ClientIp},
                   {"PrtTitle", data.PrtTitle},
                   {"SrId", data.SrId},
                   {"PrtId",data.PrtId},
                   {"MemberLevelTrackingLastApplication", "1"},
                   {"MemberLevelTrackingIsMobileDevice", data.MemberLevelTrackingIsMobileDevice},
                   {"MemberLevelTrackingIsTablet", data.MemberLevelTrackingIsTablet},
                   {"OrderID",data.OrderID},
                   {"TemplateId", data.TemplateId},
                   
            };

            if (data.PromoID > 0)
                paymentDictionary.Add("PromoID", data.PromoID.ToString());

            paymentDictionary.Add("OmnitureVariables", JsonConvert.SerializeObject(data.OmnitureVariables));

            var response = Post<PaymentUiPostData>("/brandId/" + brandID + RestUrls.ADMIN_POST_PAYMENT, null, paymentDictionary);
            return response.Data;
        }

        public APIPaymentUIJSON(ApiClientBase apiClient) : base(apiClient)
        {
        }
    }
}