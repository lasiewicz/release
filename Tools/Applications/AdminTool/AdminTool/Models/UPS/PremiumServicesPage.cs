﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using AdminTool.Models.SparkAPI;
using Matchnet;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.Region;
using Matchnet.Lib;
using Matchnet.Member.ServiceAdapters;
using Matchnet.Member.ValueObjects.Admin;
using Matchnet.OmnitureHelper;
using Matchnet.Purchase.ServiceAdapters;
using Matchnet.Purchase.ValueObjects;
using Spark.Common.Adapter;
using Spark.Common.UPS;
using Spark.CommonLibrary;
using Language = Matchnet.Language;
using MPVO = Matchnet.Purchase.ValueObjects;
using Spark.Common.RestConsumer.V2.Models.Subscription;

namespace AdminTool.Models.UPS
{
    public class PremiumServicesPage : UserControl, IPaymentUIJSON
    {
        private int version = Constants.NULL_INT;
        private int templateID = Constants.NULL_INT;
        private PaymentType paymentType;
        private int upsLegacyDataID = Constants.NULL_INT;
        private int UPSGlobalLogID = Constants.NULL_INT;
        private bool saveLegacyData = true;
        private Member targetMember = null;
        private Matchnet.Content.ValueObjects.BrandConfig.Brand targetBrand = null;
        private string _clientIP;

        public Dictionary<String, String> OrderAttributes { get; set; }
        public Member TargetMember { get { return targetMember; } }
        public Matchnet.Content.ValueObjects.BrandConfig.Brand TargetBrand { get { return targetBrand; } }
        public HttpContext Context { get; set; }

        public string ClientIP
        {
            get
            {
                if (_clientIP == null)
                {
                    _clientIP = HttpContext.Current.Request.Headers["client-ip"];
                    if (_clientIP == null)
                    {
                        _clientIP = HttpContext.Current.Request.ServerVariables["REMOTE_HOST"];
                    }

                    Int32 commaPos = _clientIP.IndexOf(",");
                    if (commaPos > -1)
                    {
                        _clientIP = _clientIP.Substring(0, commaPos);
                    }
                }

                return _clientIP;
            }
        }

        /// <summary>
        /// Constructor needed because we no longer have access to ContextGlobal aka "g" like the imported code so memberID and brandID must be passed in
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="brandID"></param>
        public PremiumServicesPage(int memberID, int brandID)
        {
            targetMember = MemberHelper.GetMember(memberID, MemberLoadFlags.IngoreSACache);
            targetBrand = BrandConfigSA.Instance.GetBrands().GetBrand(brandID);
        }

        #region IPaymentUIJSON Members

        #region Properties
        bool IPaymentUIJSON.SaveLegacyData
        {
            get
            {
                return saveLegacyData;
            }
            set
            {
                saveLegacyData = true;
            }
        }

        int IPaymentUIJSON.Version
        {
            get
            {
                return version;
            }
            set
            {
                version = value;
            }
        }

        int IPaymentUIJSON.TemplateID
        {
            get
            {
                return templateID;
            }
            set
            {
                templateID = value;
            }
        }

        PaymentType IPaymentUIJSON.PaymentType
        {
            get
            {
                return paymentType;
            }
            set
            {
                paymentType = value;
            }
        }

        int IPaymentUIJSON.UPSLegacyDataID
        {
            get
            {
                return upsLegacyDataID;
            }
            set
            {
                upsLegacyDataID = value;
            }
        }

        int IPaymentUIJSON.UPSGlobalLogID
        {
            get
            {
                return UPSGlobalLogID;
            }
            set
            {
                UPSGlobalLogID = value;
            }
        }
        #endregion

        
        private bool UseAPIForPaymentJSON()
        {
            return Convert.ToBoolean(Matchnet.Configuration.ServiceAdapters.RuntimeSettings.GetSetting("USE_API_FOR_UPS_SUBSCRIPTION_PAYMENT_JSON", TargetBrand.Site.Community.CommunityID, TargetBrand.Site.SiteID, TargetBrand.BrandID));
        }


        object IPaymentUIJSON.GetJSONObject()
        {
            if (UseAPIForPaymentJSON())
                return GetAPIPaymentJson();
            else
                return GetPaymentJson();
        }

        #endregion

        private PaymentUiPostData GetAPIPaymentJson()
        {


            if (version == Constants.NULL_INT)
                throw new Exception("Version cannot be null.");

            if (templateID == Constants.NULL_INT)
                throw new Exception("TemplateID cannot be null.");

            if (paymentType == null)
                throw new Exception("PaymentType cannot be null.");

            if (upsLegacyDataID == Constants.NULL_INT)
                throw new Exception("UPS Legacy Data ID cannot be null.");


            try
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetAPIPaymentJson", "ENTER PaymentUIConnector jump page", null, 1, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);


                PaymentUiPostData paymentData = null;

                int adminID = Constants.NULL_INT;
                string adminDomainName = String.Empty;
                string cancelURL = string.Empty;

                AdminMemberDomainMapperCollection adminMapperList = MemberSA.Instance.GetAdminMemberDomainMappers();
                if (adminMapperList != null && adminMapperList.Count > 0)
                {
                    AdminMemberDomainMapper mapper = adminMapperList.GetAdminMemberDomainMapper(HttpContext.Current.User.Identity.Name);
                    if (mapper != null)
                    {
                        adminID = mapper.MemberID;
                        adminDomainName = mapper.DomainAccount;
                    }
                }

               
                if (HttpContext.Current.Request.UrlReferrer != null)
                    cancelURL = HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                else
                    cancelURL = "http://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port > 0 ? ":" + HttpContext.Current.Request.Url.Port : "");

                
                var request = new PaymentUiPostDataRequest
                {
                    PaymentUiPostDataType = "3",
                    CancelUrl = cancelURL,
                    ConfirmationUrl = "http://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port > 0 ? ":" + HttpContext.Current.Request.Url.Port : "") + "/Subscription/Confirmation",
                    DestinationUrl = string.Empty,
                    ReturnUrl = "http://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port > 0 ? ":" + HttpContext.Current.Request.Url.Port : ""),
                    ClientIp = ClientIP,
                    PrtTitle = string.Empty,
                    SrId = (Context.Request["srid"] != null) ? Convert.ToString(Context.Request["srid"]).Replace(",", ";") : string.Empty,
                    PrtId = (Context.Request["prtid"] != null) ? Convert.ToString(Context.Request["prtid"]).Replace(",", ";") : string.Empty,
                    OmnitureVariables = new Dictionary<string, string>(),
                    MemberLevelTrackingLastApplication = "1",
                    MemberLevelTrackingIsMobileDevice = "false",
                    MemberLevelTrackingIsTablet = "false",
                    PromoType = "0",
                    MemberID = TargetMember.MemberID,
                    AdminDomainName = adminDomainName,
                    AdminMemberID = adminID,
                    PaymentType = (int)paymentType,
                    TemplateId = templateID.ToString()
                };

                if (Context.Request["PromoID"] != null)
                {
                    request.PromoID = Convert.ToInt32(Context.Request["PromoID"]);
                }



                #region Omniture Variables
                request.OmnitureVariables.Add("pageName", "Ala Carte - Select Product");
                request.OmnitureVariables.Add("prop17", OmnitureHelper.GetProfileCompetionPercentage(TargetMember, TargetBrand));
                request.OmnitureVariables.Add("prop18", OmnitureHelper.GetGender(TargetMember.GetAttributeInt(TargetBrand, WebConstants.ATTRIBUTE_NAME_GENDERMASK)));
                request.OmnitureVariables.Add("prop19", OmnitureHelper.GetAge(TargetMember.GetAttributeDate(TargetBrand, WebConstants.ATTRIBUTE_NAME_BIRTHDATE)).ToString());

                string attributeName = "Ethnicity";
                if (TargetBrand.Site.Community.CommunityID == (int)CommunityIDs.JDate)
                {
                    attributeName = "JDateEthnicity";
                }
                var resourceBag = GeneralHelper.GetResourceCollection(GeneralHelper.DetermineResourceFileName(TargetBrand));
                var prop20 = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, TargetMember, TargetBrand, attributeName);
                request.OmnitureVariables.Add("prop20", prop20);

                request.OmnitureVariables.Add("prop21", OmnitureHelper.GetRegionString(TargetMember, TargetBrand, TargetBrand.Site.LanguageID, false, true, false).Replace("\"", string.Empty));
                request.OmnitureVariables.Add("prop23", TargetMember.MemberID.ToString());
                request.OmnitureVariables.Add("events", "purchase");
                request.OmnitureVariables.Add("eVar44", string.Format(WebConstants.OMNITURE_EVAR44_MEMBERID, (TargetMember.MemberID % 10).ToString()));
                var prop29 = (HttpContext.Current != null && HttpContext.Current.Request.UrlReferrer != null)
                                   ? HttpContext.Current.Request.UrlReferrer.ToString()
                                   : string.Empty;
                request.OmnitureVariables.Add("prop29", prop29);
                request.OmnitureVariables.Add("eVar6",
                                      (Context.Request["prtid"] != null)
                                          ? Convert.ToString(Context.Request["prtid"])
                                          : string.Empty);
               
                #endregion

                APIPaymentUIJSON apiPaymentUIPSON = new APIPaymentUIJSON(ApiClient.GetInstance());
                paymentData = apiPaymentUIPSON.PostPayment(request, TargetBrand.BrandID.ToString());

                if (paymentData == null)
                    throw new Exception("Unable to get payment data from Subcription. CreditCard page");



                return paymentData;
            }
            catch (Exception ex)
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetAPIPaymentJson", "ERROR DETECTED: " + ex.Message, null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.ERROR, DateTime.Now);

                throw new Exception("GetAPIPaymentJson Error in geting JSON from API. MemberID:" + TargetMember.MemberID.ToString(), ex);
            }
            finally
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetAPIPaymentJson", "EXIT PaymentUIConnector jump page", null, 0, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
            }


        }




        private PaymentJson GetPaymentJson()
        {
            if (version == Constants.NULL_INT)
                throw new Exception("Version cannot be null.");

            if (templateID == Constants.NULL_INT)
                throw new Exception("TemplateID cannot be null.");

            if (paymentType == null)
                throw new Exception("PaymentType cannot be null.");

            if (upsLegacyDataID == Constants.NULL_INT)
                throw new Exception("UPS Legacy Data ID cannot be null.");

            var colDataAttributes = new Dictionary<string, string>();

            try
            {
                if (TargetMember == null)
                    throw new Exception("Member cannot null.");

                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "ENTER PaymentUIConnector jump page", null, 1, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);

                int memberID = TargetMember.MemberID;

                PlanCollection originalCollection = PlanSA.Instance.GetPlans(TargetBrand.BrandID, PlanType.ALaCarte, PaymentType.CreditCard);

                if (originalCollection == null)
                    throw new Exception("Plancollection cannot be null.");

                var planCollection = new List<Matchnet.Purchase.ValueObjects.Plan>();

                var highlightPlan = (from MPVO.Plan p in originalCollection
                                     where p.PremiumTypeMask == PremiumType.HighlightedProfile
                                     orderby p.UpdateDate descending
                                     select p).Take(1).ToList();

                if (highlightPlan.Count == 1)
                {
                    planCollection.Add(highlightPlan[0]);
                }

                var spotlightPlan = (from MPVO.Plan p in originalCollection
                                     where p.PremiumTypeMask == PremiumType.SpotlightMember
                                     orderby p.UpdateDate descending
                                     select p).Take(1).ToList();

                if (spotlightPlan.Count == 1)
                {
                    planCollection.Add(spotlightPlan[0]);
                }

                if (EmailManager.IsReadRecieptPremiumFeatureEnabled(TargetBrand))
                {
                    var readReceiptPlan = (from MPVO.Plan p in originalCollection
                                           where p.PremiumTypeMask == PremiumType.ReadReceipt
                                           orderby p.UpdateDate descending
                                           select p).Take(1).ToList();

                    if (readReceiptPlan.Count == 1)
                    {
                        planCollection.Add(readReceiptPlan[0]);
                    }
                }

                var renewalSub = Spark.Common.Adapter.RenewalServiceWebAdapter.GetProxyInstanceForBedrock().GetCurrentRenewalSubscription(TargetMember.MemberID, TargetBrand.Site.SiteID);
                if (renewalSub == null || renewalSub.RenewalSubscriptionID <= 0)
                {
                    throw new Exception("Member does not have a subscription. Renewal Subscription cannot be null. MemberID:" + TargetMember.MemberID);
                }

                int activePlanID = renewalSub.PrimaryPackageID;

                // Get all the privileges that have not expired for this member for this site  
                Spark.Common.AccessService.AccessPrivilege[] accessPrivileges = Spark.Common.Adapter.AccessServiceWebAdapter.GetProxyInstanceForBedrock().GetCustomerPrivilegeListAll(memberID);
                var activePrivileges = new List<Spark.Common.AccessService.AccessPrivilege>();
                foreach (Spark.Common.AccessService.AccessPrivilege checkAccessPrivilege in accessPrivileges)
                {
                    if (checkAccessPrivilege.CallingSystemID == TargetBrand.Site.SiteID)
                    {
                        if (checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.BasicSubscription
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.HighlightedProfile
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.SpotlightMember
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.JMeter
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.AllAccess
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.ReadReceipt)
                        {
                            if (checkAccessPrivilege.EndDateUTC > DateTime.Now)
                            {
                                activePrivileges.Add(checkAccessPrivilege);
                            }
                        }
                    }
                }

                // Get the details of the most recent plan that the member is on  
                Spark.Common.CatalogService.Package activeSubscriptionPackage = Spark.Common.Adapter.CatalogServiceWebAdapter.GetProxyInstance().GetPackageDetails(activePlanID);
                Spark.Common.Adapter.CatalogServiceWebAdapter.CloseProxyInstance();
                //Matchnet.Web.CatalogServiceReference.Package activeSubscriptionPackage = connector.Client.GetPackageDetails(activePlanID);
                string upsaleFromType = "Standard";
                if (activeSubscriptionPackage.PackageType == Spark.Common.CatalogService.PackageType.Bundled)
                {
                    upsaleFromType = "BundledWithPremiumServices";
                }
                else if (activeSubscriptionPackage.PackageType == Spark.Common.CatalogService.PackageType.Basic)
                {
                    // Check to see if the member had purchased any a la carte premium service in addition to the
                    // standard plan that the member has most recently purchased
                    //Spark.Common.AccessService.AccessPrivilege[] accessPrivileges = Spark.Common.Adapter.AccessServiceWebAdapter.GetProxyInstanceForBedrock().GetCustomerPrivilegeListAll(memberID);
                    bool hasAtLeastOneActivePremiumService = false;

                    foreach (Spark.Common.AccessService.AccessPrivilege checkAccessPrivilege in activePrivileges)
                    {
                        if (checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.HighlightedProfile
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.SpotlightMember
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.JMeter
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.AllAccess
                            || checkAccessPrivilege.UnifiedPrivilegeType == Spark.Common.AccessService.PrivilegeType.ReadReceipt)
                        {
                            hasAtLeastOneActivePremiumService = true;
                        }

                    }

                    if (hasAtLeastOneActivePremiumService)
                    {
                        upsaleFromType = "StandardWithAlacarte";
                    }
                    else
                    {
                        upsaleFromType = "Standard";
                    }
                }

                // Get the packages allowed for upsale for this member 
                Spark.Common.CatalogService.Item activeSubscriptionPackageBasicItem = null;
                //Matchnet.Web.CatalogServiceReference.Item activeSubscriptionPackageBasicItem = null;
                foreach (Spark.Common.CatalogService.Item item in activeSubscriptionPackage.Items)
                {
                    var itemPrivileges = new List<Spark.Common.CatalogService.PrivilegeType>();
                    itemPrivileges.AddRange(item.PrivilegeType);
                    if (itemPrivileges.Contains(Spark.Common.CatalogService.PrivilegeType.BasicSubscription))
                    {
                        // Use the basic subscription item in the package to determine the duration and duration type of the package  
                        activeSubscriptionPackageBasicItem = item;
                        break;
                    }
                }
                int[] arrUpsalePackages = Spark.Common.Adapter.CatalogServiceWebAdapter.GetProxyInstance().GetUpsalePackages(activeSubscriptionPackageBasicItem.Duration, Enum.GetName(typeof(DurationType), activeSubscriptionPackageBasicItem.DurationType), TargetBrand.Site.SiteID, upsaleFromType, activeSubscriptionPackageBasicItem.RenewalDuration, Enum.GetName(typeof(DurationType), activeSubscriptionPackageBasicItem.RenewalDurationType));
                Spark.Common.Adapter.CatalogServiceWebAdapter.CloseProxyInstance();
                var upsalePackages = new List<int>();
                upsalePackages.AddRange(arrUpsalePackages);

                var colPlansAvailableForUpsale = new List<MPVO.Plan>();
                foreach (int planID in upsalePackages)
                {
                    MPVO.Plan upsalePlan = PlanSA.Instance.GetPlan(planID, TargetBrand.BrandID);

                    if (upsalePlan != null)
                    {
                        colPlansAvailableForUpsale.Add(upsalePlan);
                    }
                }

                // There is an upsale package that contains both all access and renewable all access emails 
                // This package is only available for upsale if the all access expiration date is before the 
                // basic subscription expiration date 
                // There is no need to check the count for the all access email part of the package 
                // If the user wants to purchase more all access emails, they can do that as a fixed pricing upgrade 
                // In the fixed pricing upgrade purchase, there will be a check to see what the limit
                // of the all access emails are 
                foreach (MPVO.Plan plan in colPlansAvailableForUpsale)
                {
                    if ((plan.PremiumTypeMask & PremiumType.AllAccess) == PremiumType.AllAccess)
                    {
                        if (IsAllowedToPurchaseUpsalePlan(activePrivileges, Spark.Common.AccessService.PrivilegeType.AllAccess, renewalSub.RenewalDatePST))
                        {
                            planCollection.Add(plan);
                        }
                    }

                }


                PaymentJson payment = new PaymentJson();

                payment.UPSLegacyDataID = upsLegacyDataID;
                payment.UPSGlobalLogID = UPSGlobalLogID;
                payment.GetCustomerPaymentProfile = 1;
                payment.Version = version;
                payment.CallingSystemID = TargetBrand.Site.SiteID;
                payment.TemplateID = templateID;
                payment.TimeStamp = DateTime.Now;
                if (HttpContext.Current.Request.UrlReferrer != null)
                    payment.Data.Navigation.CancelURL = HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                else
                    payment.Data.Navigation.CancelURL = "http://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port > 0 ? ":" + HttpContext.Current.Request.Url.Port : "");

                payment.Data.Navigation.ReturnURL = "http://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port > 0 ? ":" + HttpContext.Current.Request.Url.Port : "");
                payment.Data.Navigation.ConfirmationURL = "http://" + HttpContext.Current.Request.Url.Host + (HttpContext.Current.Request.Url.Port > 0 ? ":" + HttpContext.Current.Request.Url.Port : "") + "/Subscription/Confirmation";
                payment.Data.Navigation.DestinationURL = string.Empty;

                // Order Attributes
                OrderAttributes = new Dictionary<string, string>();

                OrderAttributes.Add("PRTID", (Context.Request["prtid"] != null) ? Convert.ToString(Context.Request["prtid"]).Replace(",", ";") : string.Empty);
                OrderAttributes.Add("SRID", (Context.Request["srid"] != null) ? Convert.ToString(Context.Request["srid"]).Replace(",", ";") : string.Empty);
                //OrderAttributes.Add("PricingGroupID", promo.PromoID.ToString());

                foreach (string key in OrderAttributes.Keys)
                {
                    if (OrderAttributes[key] != string.Empty)
                        payment.Data.OrderAttributes += key + "=" + OrderAttributes[key] + ",";
                }

                if (payment.Data.OrderAttributes.Length > 0)
                    payment.Data.OrderAttributes = payment.Data.OrderAttributes.TrimEnd(',');

                // Member Info
                payment.Data.MemberInfo.CustomerID = TargetMember.MemberID;
                payment.Data.MemberInfo.RegionID = TargetMember.GetAttributeInt(TargetBrand, "RegionID");
                if (payment.Data.MemberInfo.RegionID == Constants.NULL_INT
                    || payment.Data.MemberInfo.RegionID < 0)
                {
                    // There must be a valid RegionID to price the cart so get it from the database if the cache does not have a valid RegionID  
                    var refreshedMember = MemberHelper.GetMember(35, MemberLoadFlags.IngoreSACache);
                    payment.Data.MemberInfo.RegionID = refreshedMember.GetAttributeInt(TargetBrand, "RegionID");
                }
                payment.Data.MemberInfo.Language = ((Language)Enum.Parse(typeof(Language), TargetBrand.Site.LanguageID.ToString())).ToString();

                RegionLanguage regionLanguage = RegionSA.Instance.RetrievePopulatedHierarchy(payment.Data.MemberInfo.RegionID, (int)Language.English);
                if (regionLanguage.CountryRegionID == ConstantsTemp.REGIONID_USA)
                    payment.Data.MemberInfo.State = regionLanguage.StateAbbreviation.Trim().ToUpper();

                //LC: Adding extra fields needed for Kount RIS
                payment.Data.MemberInfo.EmailAddress = TargetMember.GetAttributeText(TargetBrand, "EmailAddress");
                payment.Data.MemberInfo.GenderMask = SubscriptionHelper.GetGender(TargetMember.GetAttributeInt(TargetBrand, "gendermask"));
                payment.Data.MemberInfo.BirthDate = TargetMember.GetAttributeDate(TargetBrand, "birthdate", DateTime.MinValue);
                payment.Data.MemberInfo.BrandInsertDate = TargetMember.GetAttributeDate(TargetBrand, "BrandInsertDate", DateTime.MinValue);

                #region UDFS
                //JS-1228 LC: Adding UDF fields needed for Kount
                payment.Data.MemberInfo.UserName = TargetMember.GetUserName(TargetBrand);
                payment.Data.MemberInfo.MaritalStatus = SubscriptionHelper.GetDescription("MaritalStatus", TargetMember.GetAttributeInt(TargetBrand, "MaritalStatus"), TargetBrand);
                payment.Data.MemberInfo.Occupation = TargetMember.GetAttributeText(TargetBrand, "OccupationDescription");
                payment.Data.MemberInfo.Education = SubscriptionHelper.GetDescription("EducationLevel", TargetMember.GetAttributeInt(TargetBrand, "EducationLevel"), TargetBrand);
                payment.Data.MemberInfo.PromotionID = TargetMember.GetAttributeInt(TargetBrand, "PromotionID").ToString();
                payment.Data.MemberInfo.Eyes = SubscriptionHelper.GetDescription("EyeColor", TargetMember.GetAttributeInt(TargetBrand, "EyeColor"), TargetBrand);
                payment.Data.MemberInfo.Hair = SubscriptionHelper.GetDescription("HairColor", TargetMember.GetAttributeInt(TargetBrand, "HairColor"), TargetBrand);
                payment.Data.MemberInfo.Height = TargetMember.GetAttributeInt(TargetBrand, "Height").ToString();
                //TODO: define webconstant to check Jdate as 3 
                if (TargetBrand.Site.Community.CommunityID == 3)
                {
                    payment.Data.MemberInfo.Ethnicity = SubscriptionHelper.GetDescription("JDateEthnicity", TargetMember.GetAttributeInt(TargetBrand, "JDateEthnicity"), TargetBrand);
                    payment.Data.MemberInfo.Religion = SubscriptionHelper.GetDescription("JDateReligion", TargetMember.GetAttributeInt(TargetBrand, "JDateReligion"), TargetBrand);
                }
                else
                {
                    payment.Data.MemberInfo.Ethnicity = SubscriptionHelper.GetDescription("Ethnicity", TargetMember.GetAttributeInt(TargetBrand, "Ethnicity"), TargetBrand);
                    payment.Data.MemberInfo.Religion = SubscriptionHelper.GetDescription("Religion", TargetMember.GetAttributeInt(TargetBrand, "Religion"), TargetBrand);
                }

                string aboutMe = TargetMember.GetAttributeText(TargetBrand, "AboutMe");
                payment.Data.MemberInfo.AboutMe = aboutMe.Length < 251 ? aboutMe : aboutMe.Substring(0, 250);

                #endregion UDF
                //// Omniture Variables
                PaymentJsonArray omnitureVariables = new PaymentJsonArray();

                omnitureVariables.Add("pageName", "Ala Carte - Select Product");
                omnitureVariables.Add("prop17", OmnitureHelper.GetProfileCompetionPercentage(TargetMember, TargetBrand));
                omnitureVariables.Add("prop18", OmnitureHelper.GetGender(TargetMember.GetAttributeInt(TargetBrand, WebConstants.ATTRIBUTE_NAME_GENDERMASK)));
                omnitureVariables.Add("prop19", OmnitureHelper.GetAge(TargetMember.GetAttributeDate(TargetBrand, WebConstants.ATTRIBUTE_NAME_BIRTHDATE)).ToString());

                string attributeName = "Ethnicity";
                if (TargetBrand.Site.Community.CommunityID == (int)CommunityIDs.JDate)
                {
                    attributeName = "JDateEthnicity";
                }
                var resourceBag = GeneralHelper.GetResourceCollection(GeneralHelper.DetermineResourceFileName(TargetBrand));
                var prop20 = GeneralHelper.GetAttributeOptionResourceValue(resourceBag, TargetMember, TargetBrand, attributeName);
                omnitureVariables.Add("prop20", prop20);

                omnitureVariables.Add("prop21", OmnitureHelper.GetRegionString(TargetMember, TargetBrand, TargetBrand.Site.LanguageID, false, true, false).Replace("\"", string.Empty));
                omnitureVariables.Add("prop23", TargetMember.MemberID.ToString());
                omnitureVariables.Add("events", "purchase");
                omnitureVariables.Add("eVar44", string.Format(WebConstants.OMNITURE_EVAR44_MEMBERID, (TargetMember.MemberID % 10).ToString()));
                var prop29 = (HttpContext.Current != null && HttpContext.Current.Request.UrlReferrer != null)
                                   ? HttpContext.Current.Request.UrlReferrer.ToString()
                                   : string.Empty;
                omnitureVariables.Add("prop29", prop29);
                omnitureVariables.Add("eVar6",
                                      (Context.Request["prtid"] != null)
                                          ? Convert.ToString(Context.Request["prtid"])
                                          : string.Empty);
                payment.Data.OmnitureVariables.Add(omnitureVariables);

                // Legacy Data Info
                var adminID = Constants.NULL_INT;
                var adminMapperList = MemberSA.Instance.GetAdminMemberDomainMappers();
                if (adminMapperList != null && adminMapperList.Count > 0)
                {
                    var mapper = adminMapperList.GetAdminMemberDomainMapper(HttpContext.Current.User.Identity.Name);
                    if (mapper != null)
                    {
                        adminID = mapper.MemberID;
                    }
                }
                payment.Data.LegacyDataInfo.AdminMemberID = adminID;
                payment.Data.LegacyDataInfo.Member = TargetMember.MemberID;
                payment.Data.LegacyDataInfo.BrandID = TargetBrand.BrandID;
                payment.Data.LegacyDataInfo.SiteID = TargetBrand.Site.SiteID;

                var actives =
                    Matchnet.PremiumServiceSearch11.ServiceAdapters.PremiumServiceSA.Instance.GetMemberActivePremiumServices(
                        memberID, TargetBrand);

                var planIDList = new StringBuilder();

                // A La Carte Plans
                // Should only be two. One of each premium service.
                foreach (var plan in planCollection)
                {
                    if (plan.EndDate != DateTime.MinValue)
                        continue;

                    PaymentJsonPackage package;
                    package = new PaymentJsonPackage();

                    package.ID = plan.PlanID;
                    package.Description = "";
                    package.SytemID = TargetBrand.Site.SiteID;
                    package.StartDate = renewalSub.RenewalDatePST;// memberSub.EndDate;
                    package.ExpiredDate = renewalSub.RenewalDatePST;// memberSub.EndDate;
                    package.CurrencyType = PaymentUIConnector.GetCurrencyAbbreviation(plan.CurrencyType);

                    package.Items.Add("InitialCostPerDuration", plan.InitialCost);
                    //package.Items.Add("ProratedAmount", GetMemberProratedAmount(plan.InitialCost, memberSub.RenewDate));
                    package.Items.Add("DurationType", "Day");
                    //package.Items.Add("Duration", GetDuration(memberSub.RenewDate));
                    package.Items.Add("PlanID", plan.PlanID);
                    package.Items.Add("CurrencyType", PaymentUIConnector.GetCurrencyAbbreviation(plan.CurrencyType));
                    package.Items.Add("PaymentTypeMask", plan.PaymentTypeMask);
                    package.Items.Add("CreditAmount", (plan.CreditAmount < 0) ? 0 : plan.CreditAmount);
                    package.Items.Add("PurchaseMode", PurchaseMode.New.ToString());
                    package.Items.Add("PremiumType", plan.PremiumTypeMask.ToString());
                    package.Items.Add("RenewCost", plan.RenewCost);

                    if ((plan.PremiumTypeMask & PremiumType.HighlightedProfile) == PremiumType.HighlightedProfile
                        || (plan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember
                        || (plan.PremiumTypeMask & PremiumType.JMeter) == PremiumType.JMeter
                        || (plan.PremiumTypeMask & PremiumType.ReadReceipt) == PremiumType.ReadReceipt)
                    {
                        package.Items.Add("ProratedAmount", GetMemberProratedAmount(plan.InitialCost, renewalSub.RenewalDatePST, GetStartDateForUpsalePlan(plan, activePrivileges)));
                        package.Items.Add("Duration", GetDuration(renewalSub.RenewalDatePST, GetStartDateForUpsalePlan(plan, activePrivileges)));

                        bool found = false;
                        foreach (string premium in actives)
                        {
                            if (plan.PremiumTypeMask.ToString().ToLower().Substring(0, 5) ==
                                premium.ToLower().Substring(0, 5))
                            {
                                found = true;
                                break;
                            }
                        }

                        if (!found)
                        {
                            payment.Data.Packages.Add(package);
                            if (package.ID > 0)
                            {
                                planIDList.Append(package.ID.ToString() + ",");
                            }
                        }
                    }
                    else if ((plan.PremiumTypeMask & PremiumType.AllAccess) == PremiumType.AllAccess)
                    {
                        decimal proratedAmount = GetMemberProratedAmount(plan.InitialCost, renewalSub.RenewalDatePST, GetStartDateForUpsalePlan(plan, activePrivileges));
                        package.Items.Add("ProratedAmount", proratedAmount);

                        package.Items.Add("Duration", GetDuration(renewalSub.RenewalDatePST, GetStartDateForUpsalePlan(plan, activePrivileges)));

                        if (proratedAmount > 0)
                        {
                            // If the package for upsale has amount of 0, do not display this package. 
                            payment.Data.Packages.Add(package);

                            if (package.ID > 0)
                            {
                                planIDList.Append(package.ID.ToString() + ",");
                            }
                        }
                    }
                };

                colDataAttributes.Add("UPSLegacyDataID", Convert.ToString(upsLegacyDataID));
                colDataAttributes.Add("CustomerID", Convert.ToString(memberID));
                colDataAttributes.Add("CallingSystemID", Convert.ToString(TargetBrand.Site.SiteID));
                colDataAttributes.Add("TransactionType", "23");
                colDataAttributes.Add("PRTID", OrderAttributes["PRTID"]);
                colDataAttributes.Add("SRID", OrderAttributes["SRID"]);
                //colDataAttributes.Add("PricingGroupID", OrderAttributes["PricingGroupID"]);
                colDataAttributes.Add("RegionID", Convert.ToString(payment.Data.MemberInfo.RegionID));
                colDataAttributes.Add("IPAddress", payment.Data.MemberInfo.IPAddress);
                colDataAttributes.Add("TemplateID", Convert.ToString(payment.TemplateID));
                colDataAttributes.Add("CancelURL", payment.Data.Navigation.CancelURL);
                colDataAttributes.Add("ConfirmationURL", payment.Data.Navigation.ConfirmationURL);
                colDataAttributes.Add("DestinationURL", payment.Data.Navigation.DestinationURL);
                colDataAttributes.Add("PlanIDList", planIDList.ToString().TrimEnd(','));

                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "Save data attributes", colDataAttributes, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "NO ERROR", null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.OK, DateTime.Now);

                return payment;
            }
            catch (Exception ex)
            {
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "Save data attributes", colDataAttributes, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "ERROR DETECTED: " + ex.Message, null, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.ERROR, DateTime.Now);

                throw new Exception("Error in building JSON for PaymentUI. MemberID:" +
                    TargetMember.MemberID, ex);
            }
            finally
            {
                //TraceLogConnectorServiceWebAdapter.GetProxyInstance().InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "Save data attributes", colDataAttributes, Constants.NULL_INT, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
                TraceLogConnectorServiceWebAdapter.InsertEndpointTraceLog(UPSGlobalLogID, "PaymentUIConnector", "GetJSONObject", "EXIT PaymentUIConnector jump page", null, 0, Spark.Common.TraceLogConnectorService.UPSEndpointProcessingStatus.NONE, DateTime.Now);
            }
        }


        private int GetExactMonthsUntilRenewal(DateTime startDate, DateTime endDate)
        {
            int monthDifference = System.Data.Linq.SqlClient.SqlMethods.DateDiffMonth(startDate, endDate);

            if (monthDifference > 0)
            {
                // Check to see if the number of months in between these two dates should be one less
                DateTime dteMonthDifferenceAdded = startDate.AddMonths(monthDifference);

                if (endDate < dteMonthDifferenceAdded)
                {
                    monthDifference = (monthDifference - 1);
                }
            }

            return monthDifference;
        }

        /// <summary>
        /// Copied from Purchase
        /// </summary>
        /// <param name="endDate"></param>
        /// <returns></returns>
        private int GetDuration(DateTime endDate)
        {
            // Get remaining days left on this plan
            // Whenever there is any remaining time in a day, give that full day of remaining credit to the 
            // member.  
            // Round 2.1 to 3 days
            // Round 2.9 to 3 days
            TimeSpan span = endDate.Subtract(DateTime.Now);
            Int32 remainingDays = Convert.ToInt32(Math.Ceiling(span.TotalDays));

            System.Diagnostics.Trace.WriteLine("RemainingDays:" + remainingDays);

            return remainingDays;
        }

        private int GetDuration(DateTime endDate, DateTime startDate)
        {
            // Get remaining days left on this plan
            // Whenever there is any remaining time in a day, give that full day of remaining credit to the 
            // member.  
            // Round 2.1 to 3 days
            // Round 2.9 to 3 days
            TimeSpan span = endDate.Subtract(startDate);
            Int32 remainingDays = Convert.ToInt32(Math.Ceiling(span.TotalDays));

            System.Diagnostics.Trace.WriteLine("RemainingDays:" + remainingDays);

            return remainingDays;
        }

        private bool IsAllowedToPurchaseUpsalePlan(List<Spark.Common.AccessService.AccessPrivilege> activePrivileges, Spark.Common.AccessService.PrivilegeType checkPrivilegeType, DateTime renewalDate)
        {
            bool isAllowedToPurchaseUpsalePlan = false;

            Spark.Common.AccessService.AccessPrivilege checkPrivilege = activePrivileges.Find(delegate(Spark.Common.AccessService.AccessPrivilege privilege) { return privilege.UnifiedPrivilegeType == checkPrivilegeType; });
            if (checkPrivilege == null)
            {
                // Member can purchase this privilege since he does not have this privilege yet
                isAllowedToPurchaseUpsalePlan = true;
            }
            else
            {
                if (checkPrivilege.EndDatePST == DateTime.MinValue
                    || checkPrivilege.EndDatePST < DateTime.Now)
                {
                    // Member can purchase this privilege since he had this privilege but it expired  
                    isAllowedToPurchaseUpsalePlan = true;
                }
                else
                {
                    // The start date of the premium service should be either earlier than the renewal date 
                    // or the same as the renewal date. If the start date of the premium service is past the
                    // renewal date, then set the start date of the premium service to the renewal date. 
                    if (checkPrivilege.EndDatePST > renewalDate)
                    {
                        isAllowedToPurchaseUpsalePlan = false;
                    }
                    else
                    {
                        TimeSpan dateDifference = checkPrivilege.EndDatePST.Subtract(renewalDate);
                        if (Math.Abs(dateDifference.TotalMinutes) > 20)
                        {
                            // There is still additional time that member can purchase for this privilege
                            isAllowedToPurchaseUpsalePlan = true;
                        }
                        else
                        {
                            // The end date of the privilege is the same as the end date of the basic subscription  
                            // so do not allow the member to purchase this privilege  
                            isAllowedToPurchaseUpsalePlan = false;
                        }
                    }
                }
            }

            return isAllowedToPurchaseUpsalePlan;
        }

        private DateTime GetStartDateForUpsalePlan(MPVO.Plan plan, List<Spark.Common.AccessService.AccessPrivilege> activePrivileges)
        {
            Spark.Common.AccessService.PrivilegeType checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.None;

            if ((plan.PremiumTypeMask & PremiumType.HighlightedProfile) == PremiumType.HighlightedProfile)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.HighlightedProfile;
            }
            else if ((plan.PremiumTypeMask & PremiumType.SpotlightMember) == PremiumType.SpotlightMember)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.SpotlightMember;
            }
            else if ((plan.PremiumTypeMask & PremiumType.JMeter) == PremiumType.JMeter)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.JMeter;
            }
            else if ((plan.PremiumTypeMask & PremiumType.AllAccess) == PremiumType.AllAccess)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.AllAccess;
            }
            else if ((plan.PremiumTypeMask & PremiumType.ReadReceipt) == PremiumType.ReadReceipt)
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.ReadReceipt;
            }
            else
            {
                checkPrivilegeType = Spark.Common.AccessService.PrivilegeType.None;
            }

            Spark.Common.AccessService.AccessPrivilege checkPrivilege = activePrivileges.Find(delegate(Spark.Common.AccessService.AccessPrivilege privilege) { return privilege.UnifiedPrivilegeType == checkPrivilegeType; });

            if (checkPrivilege == null)
            {
                return DateTime.Now.AddMinutes(60);
            }
            else
            {
                if (checkPrivilege.EndDatePST <= DateTime.Now)
                {
                    return DateTime.Now.AddMinutes(60);
                }
                else
                {
                    return checkPrivilege.EndDatePST;
                }
            }
        }

        private decimal GetMemberProratedAmount(decimal initialCost, DateTime endDate, DateTime startDate)
        {
            if (endDate < DateTime.Now)
            {
                throw new Exception("Subscription end date must be a future date. Enddate:" +
                                    endDate.ToString());
            }

            decimal amount = Constants.NULL_DECIMAL;
            decimal pricePerDay = Constants.NULL_DECIMAL;

            // The start date of the premium service should be either earlier than the renewal date 
            // or the same as the renewal date. If the start date of the premium service is past the
            // renewal date, then set the start date of the premium service to the renewal date. 
            if (startDate > endDate)
            {
                amount = 0.0m;
            }
            else
            {
                int monthsUntilRenewalDate = GetExactMonthsUntilRenewal(startDate, endDate);
                decimal subTotalFromMonths = (monthsUntilRenewalDate) * (initialCost);
                DateTime dteAfterMonthsAdded = startDate.AddMonths(monthsUntilRenewalDate);

                // Calculate cost per day
                DateTime dteEndDateOfRemainingDaysWithMonthAdded = dteAfterMonthsAdded.AddMonths(1);
                TimeSpan span = dteEndDateOfRemainingDaysWithMonthAdded.Subtract(dteAfterMonthsAdded);
                pricePerDay = (initialCost) / Convert.ToDecimal(span.Days);

                decimal subTotalFromRemainingDays = (pricePerDay) * (Convert.ToDecimal(GetDuration(endDate, dteAfterMonthsAdded)));

                //Make sure the prorated amount for remaining days is not greater than monthly price
                if (subTotalFromRemainingDays > initialCost)
                {
                    subTotalFromRemainingDays = initialCost;
                }

                amount = subTotalFromMonths + subTotalFromRemainingDays;
            }

            if (amount < 0)
                throw new Exception("Prorated Amount cannot be less then 0.");

            System.Diagnostics.Trace.WriteLine("InitialCost:" + initialCost +
                "EndDate:" + endDate + "PricePerDay:" + pricePerDay + "Amount:" + amount);

            return amount;
        }
    }
}