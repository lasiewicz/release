﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminTool.Models.ModelViews.Email;
using AdminTool.Models.ModelViews.Member;
using Matchnet.Configuration.ServiceAdapters;
using Matchnet.Content.ServiceAdapters;
using Matchnet.Content.ValueObjects.AttributeMetadata;
using Matchnet.Content.ValueObjects.BrandConfig;
using Matchnet.Member.ValueObjects;
using Matchnet.Content.ValueObjects.Admin;

namespace AdminTool.Models
{
    public class MemberDataForPrint : BasicUserInfo
    {
        public enum DataType
        {
            None,
            FreeText,
            NumberText,
            DateTime,
            Boolean,
            SingleSelect,
            MultiSelect
        }
        public List<FreeTextAttribute> FreeTextAttributes = new List<FreeTextAttribute>();
        public List<NumberTextAttribute> NumberTextAttributes = new List<NumberTextAttribute>();
        public List<DateTimeAttribute> DateTimeAttributes = new List<DateTimeAttribute>();
        public List<BooleanAttribute> BooleanAttributes = new List<BooleanAttribute>();
        public List<SingleSelectAttribute> SingleSelectAttributes = new List<SingleSelectAttribute>();
        public List<MultiSelectAttribute> MultiSelectAttributes = new List<MultiSelectAttribute>();

        public int TheSiteID { get; set; }

        public string MDPBrandInfo { get; set; }

        public string MDPEmailAddress { get; set; }

        public List<AdminActionNotesMiniForPrint> Notes { get; set; }

        public bool HasPhoto { get; set; }

        public List<MemberDataForPrint.LoginData> LastLogons { get; set; }

        public class AttributeMetadataForPrint
        {
            public int AttributeID { get; set; }
            public string AttributeName { get; set; }
            public int AttributeMaxLength { get; set; }
            public DataType DataType { get; set; }
        }

        public ProfileInfo ProfileInfo { get; set; }

        public List<Email> AllSentEmails { get; set; }


        public class AttributeMetadataCollectionForPrint
        {
            public List<Models.MemberDataForPrint.AttributeMetadataForPrint> ProfileAttributeMetadataCol = new List<Models.MemberDataForPrint.AttributeMetadataForPrint>();

            public void GetMetadata(Brand brand)
            {
                int profileSteps = Matchnet.Conversion.CInt(RuntimeSettings.GetSetting("REGISTRATION_STEPS", brand.Site.Community.CommunityID, brand.Site.SiteID));
                AttributeCollection attributes;
                Attributes contentAttributes = Matchnet.Content.ServiceAdapters.AttributeMetadataSA.Instance.GetAttributes();

                for (int i = 1; i <= profileSteps; i++)
                {
                    attributes = AttributeMetadataSA.Instance.GetAttributeCollections().GetCollection("REGSTEP" + i.ToString(), brand);
                    if (attributes != null)
                    {
                        foreach (Matchnet.Content.ValueObjects.AttributeMetadata.AttributeCollectionAttribute a in attributes)
                        {
                            this.Add(a, brand, contentAttributes);
                        }
                    }
                }
            }

            public void Add(Matchnet.Content.ValueObjects.AttributeMetadata.AttributeCollectionAttribute attributeColAttribute, Brand brand, Attributes attributes)
            {
                Matchnet.Content.ValueObjects.AttributeMetadata.Attribute attribute = attributes.GetAttribute(attributeColAttribute.AttributeID);
                if (attribute == null)
                    return;

                DataType dType = DataType.None;


                #region DataType overrides. Data type defined in the DB isn't how we actually use it on the sites anymore.
                // some number DataType attributes actually require TextBox
                if (attribute.Name.ToLower() == "desiredminage" || attribute.Name.ToLower() == "desiredmaxage")
                {

                    dType = DataType.NumberText;

                }
                else if (attribute.Name.ToLower() == "bodytype")
                {




                }
                else if (attribute.Name.ToLower() == "israelipersonalitytrait")
                {
                    // number in db, but we want to represent it as mask

                    dType = DataType.MultiSelect;

                }
                #endregion

                if (dType == DataType.None)
                {
                    switch (attribute.DataType)
                    {
                        case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Text:

                            dType = DataType.FreeText;

                            break;
                        case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Number:

                            dType = DataType.SingleSelect;

                            break;
                        case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Date:

                            dType = DataType.DateTime;

                            break;
                        case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Bit:

                            dType = DataType.Boolean;

                            break;
                        case Matchnet.Content.ValueObjects.AttributeMetadata.DataType.Mask:

                            dType = DataType.MultiSelect;

                            break;
                    }
                }

                AttributeGroup ag = attributes.GetAttributeGroup(GeneralHelper.GetGroupIDForAttribute(brand, attribute), attribute.ID);

                ProfileAttributeMetadataCol.Add(new Models.MemberDataForPrint.AttributeMetadataForPrint
                {
                    AttributeID = attribute.ID,
                    AttributeName = attribute.Name,
                    DataType = dType,
                    AttributeMaxLength = ag.Length <= 0 ? 0 : ag.Length
                });
            }
        }


        public class LoginData
        {
            public int count { get; set; }
            public DateTime Login { get; set; }
        }


        public class AdminActionNotesMiniForPrint
        {
            public string Note { get; set; }
            public DateTime InsertDate { get; set; }
        }


    }
}