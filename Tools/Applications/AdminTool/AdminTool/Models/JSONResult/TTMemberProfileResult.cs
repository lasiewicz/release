﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdminTool.Models.ModelViews.TrainingTool;
using Matchnet.ApproveQueue.ValueObjects.TrainingTool;

namespace AdminTool.Models.JSONResult
{
    public class TTMemberProfileResult : APIResult
    {
        public int TotalRows { get; set; }
        public List<TTMemberProfileUI> MemberProfiles { get; set; } 
    }
}