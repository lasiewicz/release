﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.JSONResult
{
    [Serializable]
    public class MemberUpdateResult : MemberResultBase
    {
        //list of properties for all possible values returned that may be needed to update other UI as part of this Ajax update
        public string ProfileStatus { get; set; }
        public string SuspendReason { get; set; }



        public MemberUpdateResult()
        {
            SuspendReason = "";
        }
    }
}
