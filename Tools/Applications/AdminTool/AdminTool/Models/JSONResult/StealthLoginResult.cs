﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.JSONResult
{
    [Serializable]
    public class StealthLoginResult : ResultBase
    {
        public string StealthLoginLink { get; set; }
    }
}