﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.JSONResult
{
    public class ArrayResult : APIResult
    {
        public ArrayList Array { get; set; }
    }
}