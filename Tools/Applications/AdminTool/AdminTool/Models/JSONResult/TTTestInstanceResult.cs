﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.JSONResult
{
    public class TTTestInstanceResult : APIResult
    {
        public int TTTestInstanceID { get; set; }
    }
}