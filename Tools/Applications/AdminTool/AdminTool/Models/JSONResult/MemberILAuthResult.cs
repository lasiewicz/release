﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.JSONResult
{
    [Serializable]
    public class MemberILAuthResult : MemberResultBase
    {
        public string ILAuthStatus { get; set; }
    }
}