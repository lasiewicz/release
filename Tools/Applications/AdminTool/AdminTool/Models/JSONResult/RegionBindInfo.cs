﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.JSONResult
{
    [Serializable]
    public class RegionBindInfo : ResultBase
    {
        private int _countryRegionID = -1;
        private int _stateRegionID = -1;
        private int _regionID = -1;
        private string _zipcode = string.Empty;
        private string _cityName = string.Empty;
        private bool _isMultiCityZip = false;

        public int CountryRegionID
        {
            get { return _countryRegionID; }
            set { _countryRegionID = value; }
        }

        public int StateRegionID
        {
            get { return _stateRegionID; }
            set { _stateRegionID = value; }
        }

        public int RegionID
        {
            get { return _regionID; }
            set { _regionID = value; }
        }

        public string ZipCode
        {
            get { return _zipcode; }
            set { _zipcode = value; }
        }

        public string CityName
        {
            get { return _cityName; }
            set { _cityName = value; }
        }

        public bool IsMutiCityZip
        {
            get { return _isMultiCityZip; }
            set { _isMultiCityZip = value; }
        }

    }
}
