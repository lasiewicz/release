﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminTool.Models.JSONResult
{
    public enum RegionDisplay
    {
        None = 0,
        Zip = 1,
        State = 2,
        City = 3
    }

    [Serializable]
    public class RegionResult : ResultBase
    {
        private int _regionID = -1;

        public int RegionID
        {
            get { return _regionID; }
            set { _regionID = value; }
        }
        public RegionDisplay NextToDisplay { get; set; }
        public List<SelectListItem> SelectListItems = new List<SelectListItem>();
    }
}
