﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.JSONResult
{
    /// <summary>
    /// Result class to return HTML for a Partial View; e.g. used in lazy load API calls
    /// </summary>
    [Serializable]
    public class PartialViewResult : ResultBase
    {
        public string HTML { get; set; }
    }
}
