﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.JSONResult
{
    [Serializable]
    public class MemberResultBase : ResultBase
    {
        public int MemberID { get; set; }
        public int SiteID { get; set; }
    }
}
