﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.JSONResult
{
    [Serializable]
    public class RegionString : ResultBase
    {
        public string RegionDisplayString { get; set; }
    }
}
