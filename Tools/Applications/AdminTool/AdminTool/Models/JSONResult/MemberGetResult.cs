﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.JSONResult
{
    [Serializable]
    public class MemberGetResult : MemberResultBase
    {
        //list of properties for all possible values returned
        public string Password { get; set; }
    }
}
