﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminTool.Models.JSONResult
{
    /// <summary>
    /// Base class for returning JSON results for API calls
    /// </summary>
    [Serializable]
    public class ResultBase
    {
        public int Status { get; set; }
        public string StatusMessage { get; set; }

        public ResultBase()
        {
            Status = (int)Models.Enums.Status.Failed;
            StatusMessage = "";
        }
    }
}
