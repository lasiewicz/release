﻿
var attributeIDArray = [];

function newMemberWindow(siteId, memberId) {
    window.open('../../Member/View/' + siteId + '/' + memberId);
}

$(document).ready(function () {


    //$.each(attributeIDArray, function (index, value) {
    //   setup(value);
    //});


    $('input[type=checkbox]').click(function () {

        var clickedCB = $(this).attr('id');
        $(this).closest('div').children('input[type=checkbox]').each(function (index) {

            if (clickedCB != $(this).attr('id')) {
                $(this).removeAttr('checked');
            }

        });

    });


    if (typeof tinyMCE != 'undefined') {
        tinyMCE.init({
            mode: "textareas",
            theme: "advanced",
            theme_advanced_buttons1: "undo,redo",
            theme_advanced_buttons2: "",
            theme_advanced_buttons3: "",
            theme_advanced_toolbar_location: "bottom",
            theme_advanced_toolbar_align: "left"
        });
    }

    //$('.review_attr .old select option').attr('disabled', 'disabled');

    $('select.reasonsDisabledStyle').multiselect({
        header: false
    });

    $('select.reasonsStyle').multiselect();

    $(':radio').click(function () {

        var radioId = $(this).attr("id");
        var ddId = radioId.substring(14);

        var firstRValue = $('input[name=OldTextAcceptable' + ddId + ']:checked').val();
        var secondRValue = $('input[name=TextAcceptable' + ddId + ']:checked').val();
        var crxSelector = $('#CrxReview' + ddId);
        var redBox = $('#redCopy' + ddId);
        var blueBox = $('#blueCopy' + ddId);



        updateCrxReview(firstRValue, secondRValue, crxSelector, redBox, blueBox);



        //this part shows the drop down
        if ($(this).val() == 'false') {
            $("#divTextUnacceptableReasons" + ddId).show();
        }
        else {
            $("#divTextUnacceptableReasons" + ddId).hide();
        }

    });



    //function validateRadio() {
    //$("form").submit(function () {
    $('#btnSendTrainingReport').click(function (e) {
        var valid = true;
        $('input:radio').each(function () {
            if ($(this).is(':checked') && $(this).val() == "false") {
                if ($(this).attr('id').indexOf('Old') != 0) {
                    var index = $(this).attr('id').substring(14);
                    if ($('input[name=multiselect_TextUnacceptableReasons' + index + '][aria-selected=true]').length == 0) {
                        $('#TextUnAcceptableValidation' + index).show();
                        valid = false;
                        e.preventDefault();
                    }
                }
            }
        });
        return valid;
    });

    $('#btnApprove').click(function (e) {
        var valid = true;
        $('input:radio[id^="TextAcceptable"]').each(function () {
            var index = $(this).attr('id').substring(14);

            if ($(this).is(':checked') && $(this).val() == "false") {
                $('#TextUnAcceptableValidation' + index).hide();
                if ($('input[name=multiselect_TextUnacceptableReasons' + index + '][aria-selected=true]').length == 0) {
                    $('#TextUnAcceptableValidation' + index).show();
                    valid = false;
                    e.preventDefault();
                }
            }

            $('#ValidationMessage' + index).hide();
            var valueLength = $.trim($(tinyMCE.get('Attribute' + index).getContent()).text()).length;
            var maxLength = $('#MaxLength' + index).val();
            if (valueLength > maxLength) {
                $('#CharactersTooLong' + index).text(valueLength - maxLength);
                $('#ValidationMessage' + index).show();
                valid = false;
                e.preventDefault();
            }

            if ($('#redCopy' + index).is(":visible")) {

                //check to see if one of the checkbox's is selected. 
                var isAnyCheckBoxChecked = false;

                $('#CrxReview' + index).children('input[type=checkbox]').each(function (index) {

                    if ($(this).is(":checked")) {
                        isAnyCheckBoxChecked = true;
                    }
                });

                if (isAnyCheckBoxChecked)
                    valid = true;
                else {
                    valid = false;
                    $("#validateCopy" + index).show();
                }
            }


        });
        return valid;
    });

    $('#btnSuspend').click(function (e) {
        if ($('#SuspendReasons').is(':visible')) {
            $('#SuspendReasons').hide();
        }
        else {
            $('#SuspendReasons').show();
        }
    });

    $('#btnUndoEdits').click(function (e) {
        $('.attr-text-value').each(function () {
            var attrGroupId = $(this).attr('id');
            var originalValue = $('#OriginalValue' + attrGroupId).val();
            tinyMCE.getInstanceById('Attribute' + attrGroupId).execCommand('mceSetContent', false, originalValue);
        });
    });

    $('#btnConfirmSuspend').click(function (e) {
        $('#Suspend').val('true');
    });


    $.each(attributeIDArray, function (index, value) {

        //alert($('#TextAcceptable' + value).val() + ' ' + value + ' this is the value');
        //alert($('#TextAcceptable' + value).is(':checked') +' '+  value + 'this is the value');

        $('input[name=TextAcceptable'+value+']').removeAttr('checked');

    });
     

});

function setup(value) {

    var firstRValue = $('input[name=OldTextAcceptable' + value + ']:checked').val();
    var secondRValue = $('input[name=TextAcceptable' + value + ']:checked').val();
    var crxSelector = $('#CrxReview' + value);

    var redBox = $('#redCopy' + value);
    var blueBox = $('#blueCopy' + value);

    updateCrxReview(firstRValue, secondRValue, crxSelector, redBox, blueBox);

}



function updateCrxReview(firstRValue, secondRValue, crxSelector, redBox, blueBox) {

    turnOffCopy(redBox, blueBox);

    if (firstRValue == "false" && secondRValue == "true") {
        greyBoxStyle(crxSelector);
    }

    if (firstRValue == "false" && secondRValue == "false") {
        redBoxStyle(crxSelector, redBox);
    }

    if (firstRValue == "true" && secondRValue == "true") {
        redBoxStyle(crxSelector,redBox);
    }

    if (firstRValue == "true" && secondRValue == "false") {
        blueBoxStyle(crxSelector, blueBox);
    }


}


function redBoxStyle(crxselector,redBox) {

    crxselector.attr("style", "border:solid 1px red;");

    redBox.show();

    crxselector.children('input[type=checkbox]').each(function (index) {

        $(this).removeAttr('disabled');

    });

}

function greyBoxStyle(crxselector) {

    crxselector.attr("style", "border:solid 1px grey;background-color:#EDEDED;color:#919191");

    crxselector.children('input[type=checkbox]').each(function (index) {

        $(this).attr('disabled', 'disabled');

        });

}

function blueBoxStyle(crxselector,blueBox) {

    blueBox.show();

    crxselector.attr("style", "border:solid 1px blue;");

    crxselector.children('input[type=checkbox]').each(function (index) {

        $(this).removeAttr('disabled');

    });
}

function turnOffCopy(redBox, blueBox) {
    redBox.hide();
    blueBox.hide();
}
