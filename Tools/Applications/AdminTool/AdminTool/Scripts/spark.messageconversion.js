﻿var spark = spark || {};
spark.messageconversion = spark.messageconversion || {
    messageAttachmentURLPath: 'http://share.sparksites.com',

    /**Templates**/
    //photo
    sparkTagPhoto: '<sparktag type="photo" alt="{{alt}}" src="{{src}}" id="{{id}}" data-originalExt="{{originalExt}}"/>',
    htmlPhoto: '<a href="{{href}}" target="_blank"><img alt="{{alt}}" src="{{src}}" id="{{id}}"/></a>',
    htmlPhotoPreview: '<img alt="{{alt}}" src="{{src}}" id="{{id}}" data-originalExt="{{originalExt}}"/>&nbsp;',

    //emoticon
    sparkTagEmoticon: '<sparktag type="emoticon" class="{{styleClass}}"/>',
    htmlEmoticon: '<span class="{{styleClass}}" data-type="emoticon"></span>',
    htmlEmoticonPreview: '<span class="{{styleClass}}" data-type="emoticon"></span>',

    /**Conversion functions**/
    convertHtmlToCustomTag: function (message) {
        //remove paragraphs, nbsp, and divs that we don't care for
        message = spark.messageconversion.removeHtmlParagraphs(message);
        message = spark.messageconversion.removeHtmlNoise(message);
        message = $.trim(message);
        var rebuiltHtml = '';
        var html = $('<div>').append(message).contents();
        $.each(html, function (i, el) {
            switch (el.nodeName.toLowerCase()) {
                case "#text":
                    //text
                    rebuiltHtml += spark.messageconversion.htmlEncode($(el).text());
                    break;
                case "img":
                    //photo
                    var dtoPhoto = {
                        src: String($(el).attr('src')).replace(spark.messageconversion.messageAttachmentURLPath, ''),
                        id: $(el).attr('id'),
                        alt: $(el).attr('alt'),
                        originalExt: $(el).attr('data-originalExt')
                    };
                    rebuiltHtml += Mustache.to_html(spark.messageconversion.sparkTagPhoto, dtoPhoto);
                    break;
                case "span":
                    var type = $(el).attr('data-type');
                    //emoticon
                    if (type == "emoticon") {
                        var dtoEmoticon = {
                            styleClass: $(el).attr('class')
                        };
                        rebuiltHtml += Mustache.to_html(spark.messageconversion.sparkTagEmoticon, dtoEmoticon);
                    }
                    break;
            }
        });

        return $.trim(rebuiltHtml);
    },

    convertCustomTagToHtml: function (message) {
        var rebuiltHtml = '';
        var html = $('<div>').append(message).contents();
        $.each(html, function (i, el) {
            switch (el.nodeName.toLowerCase()) {
                case "#text":
                    //text
                    rebuiltHtml += spark.messageconversion.htmlEncode($(el).text());
                    break;
                case "sparktag":
                    var type = $(el).attr('type');
                    if (type == "photo") {
                        //photo
                        var dtoPhoto = {
                            src: spark.messageconversion.messageAttachmentURLPath + $(el).attr('src'),
                            id: $(el).attr('id'),
                            alt: $(el).attr('alt'),
                            href: spark.messageconversion.messageAttachmentURLPath + String($(el).attr('src')).replace("preview", "original").replace('.jpg', '.' + $(el).attr('data-originalExt'))
                        };
                        rebuiltHtml += Mustache.to_html(spark.messageconversion.htmlPhoto, dtoPhoto);
                    }
                    else if (type == "emoticon") {
                        //emoticon
                        var dtoEmoticon = {
                            styleClass: $(el).attr('class')
                        };
                        rebuiltHtml += Mustache.to_html(spark.messageconversion.htmlEmoticon, dtoEmoticon);
                    }
                    break;
            }
        });

        return $.trim(rebuiltHtml);
    },

    /**Encode/Decode which also strips white spaces **/
    htmlEncode: function (value) {
        //create a in-memory div, set it's inner text(which jQuery automatically encodes)
        //then grab the encoded contents back out.  The div never exists on the page.
        return $('<div/>').text(value).html();
    },

    htmlDecode: function (value) {
        return $('<div/>').html(value).text();
    },

    /**Encode/Decode without stripping white spaces **/
    htmlEscape: function (str) {
        return String(str)
		.replace(/&/g, '&amp;')
		.replace(/"/g, '&quot;')
		.replace(/'/g, '&#39;')
		.replace(/</g, '&lt;')
		.replace(/>/g, '&gt;');
    },

    htmlUnescape: function (value) {
        return String(value)
		.replace(/&quot;/g, '"')
		.replace(/&#39;/g, "'")
		.replace(/&lt;/g, '<')
		.replace(/&gt;/g, '>')
		.replace(/&amp;/g, '&');
    },

    removeHtmlParagraphs: function (value) {
        return String(value)
			.replace(/<p>&nbsp;<\/p>/g, "")
			.replace(/<p>/g, "")
			.replace(/<\/p>/g, "")
			.replace(/<br\/>/g, "")
			.replace(/<br>/g, "")
			.replace(/<\/br>/g, "");
    },
    removeParagraphs: function (value) {
        return String(value)
			.replace("\n", "")
			.replace("\r", "");
    },
    removeHtmlNoise: function (value) {
        return String(value)
			.replace(/&nbsp;/g, ' ')
			.replace(/<div>/g, "")
			.replace(/<\/div>/g, "");
    },

};