﻿$(document).ready(function () {
    $('input.btnPurge').click(function (e) {
        var adminId = $(this).attr('data-adminmemberid');
        var supervisorId = $(this).attr('data-supervisorid');
        $.ajax({
            type: "GET",
            url: '/Review/PurgeOnDemandQueue?adminMemberID=' + adminId + '&supervisorId=' + supervisorId,
            dataType: 'html',
            success: function (result) {
                window.location.href = '/Review/OnDemandReviewQueueManagement';
            }
        });
    });

    $('input.btnProcess').click(function (e) {
        var adminId = $(this).attr('data-adminmemberid');
        var supervisorId = $(this).attr('data-supervisorid');
        window.location.href = '/Review/TextReview?isOnDemand=true&supervisorId=' + supervisorId + '&adminMemberId=' + adminId + '&perAdmin=true';
    });
});