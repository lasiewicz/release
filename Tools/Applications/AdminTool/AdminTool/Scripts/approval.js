﻿function submitPhotoApprovals() {

    var photoCount = $('#PhotoCount').val();
    var i = 0;
    for (i = 1; i <= photoCount; i++) {
        var disposition = $('#FlashApprove' + i + '_rb').val();
        var tx = $('#FlashApprove' + i + '_tx').val();
        var ty = $('#FlashApprove' + i + '_ty').val();
        var tw = $('#FlashApprove' + i + '_tw').val();
        var th = $('#FlashApprove' + i + '_th').val();
        var cx = $('#FlashApprove' + i + '_cx').val();
        var cy = $('#FlashApprove' + i + '_cy').val();
        var cw = $('#FlashApprove' + i + '_cw').val();
        var ch = $('#FlashApprove' + i + '_ch').val();
        var cd = $('#FlashApprove' + i + '_cd').val();
        var photoPrevApproved = $('#FlashApprove' + i + '_PhotoPrevApproved').val();
        var memberPhotoID = $('#FlashApprove' + i + '_MemberPhotoID').val();
        var fileID = $('#FlashApprove' + i + '_FileID').val();
        var rot = $('#FlashApprove' + i + '_rot').val();
        var adminMemberID = $('#FlashApprove' + i + '_AdminMemberID').val();
        var communityID = $('#FlashApprove' + i + '_CommunityID').val();
        var siteID = $('#FlashApprove' + i + '_SiteID').val();
        var memberID = $('#FlashApprove' + i + '_MemberID').val();
        var deleted = $('#FlashApprove' + i + '_Deleted').val();
        var isApprovedForMain = $('#FlashApprove' + i + '_IsApprovedForMain').is(':checked');
        
        $.ajax({
            type: "POST",
            url: "/ApprovalAPI/ApprovePhoto",
            data: { disposition: disposition,
                tx: tx,
                ty: ty,
                tw: tw,
                th: th,
                cx: cx,
                cy: cy,
                cw: cw,
                ch: ch,
                captionDelete: cd,
                photoPrevApproved: photoPrevApproved,
                memberPhotoID: memberPhotoID,
                fileID: fileID,
                rot: rot,
                memberID: memberID,
                adminMemberID: adminMemberID,
                communityID: communityID,
                siteID: siteID,
                deleted: deleted,
                isApprovedForMain: isApprovedForMain
            },
            cache: false,
            async: false,
            success: function(result) {

            }
        });
    }
    location.reload(true);
}

function deletePhotoQueueItem(photoNumber, memberID) {
    var memberPhotoID = $('#FlashApprove' + photoNumber + '_MemberPhotoID').val();
    var communityID = $('#FlashApprove' + photoNumber + '_CommunityID').val();

    $.ajax({
        type: "POST",
        url: "/ApprovalAPI/DeletePhotoQueueItem",
        data: { memberID: memberID,
            communityID: communityID,
            memberPhotoID: memberPhotoID
        },
        cache: false,
        async: false,
        success: function(result) {
            //alert(result.Status);
            if (result.Status == 2)
                $('#photoDiv_' + memberID).css("background-color", "grey");
        }
    });
   $('#FlashApprove' + photoNumber + '_Deleted').val('true');
}

function changePossibleFraud(index) {
    var isChecked = $('#PossibleFraud' + index).is(':checked');
    if (isChecked == true) {
        $('#FlashApprove' + index + '_rb').val('9');
    }
    else {
        var originalValue = $('#FlashApprove' + index + '_rb_original').val();
        $('#FlashApprove' + index + '_rb').val(originalValue);
    }
}

function changeIsAlreadyApproved(index) {
    var isChecked = $('#Reapprove' + index).is(':checked');
    if (isChecked == true) {
        $('#FlashApprove' + index + '_PhotoPrevApproved').val('0');
    }
    else {
        var originalValue = $('#FlashApprove' + index + '_PhotoPrevApproved_Original').val();
        $('#FlashApprove' + index + '_PhotoPrevApproved').val(originalValue);
    }
}

function ValidateFTA() {
    var index = 1;
    var value = $('#Attribute' + index).val();
    var validated = true;
    var wasOneBadSelected = false;
    

    while (value != undefined) {
        $('#ValidationMessage' + index).hide();
        $('#RadioValidation' + index).hide();
        $('#TextUnAcceptableValidation' + index).hide();
        var valueLength = $.trim($(tinyMCE.get('Attribute' + index).getContent()).text()).length;
        var maxLength = $('#MaxLength' + index).val();
        if (valueLength > maxLength) {
            $('#CharactersTooLong' + index).text(valueLength - maxLength);
            $('#ValidationMessage' + index).show();
            validated = false;
        }
        //Validate radio buttons
        if ($('input[name=TextAcceptable' + index + ']:checked').size() == 0) {
            $('#RadioValidation' + index).show();
            validated = false;
        }
        //Selecting Bad option, atleast one option from the reasons list should be selected
        else if ($('input[name=TextAcceptable' + index + ']:checked').val() == "false" && $('input[name=multiselect_TextUnacceptableReasons' + index + '][aria-selected=true]').length == 0) {
            $('#TextUnAcceptableValidation' + index).show();
            validated = false;
        }
        
        if ($('input[name=TextAcceptable' + index + ']:checked').val() == "false") {
            wasOneBadSelected = true;
        }

        index++;
        value = undefined;
        value = $('#Attribute' + index).val();
    }

    //handle the warning validation
    if (IsEnglish) {

        var isWarningChecked = false;
        $('#WarningValidation').hide();
        if (wasOneBadSelected) {

            //check to make sure a warning was selected
            $('#divWarning input[type=checkbox]').each(function () {
                if (this.checked) {
                    isWarningChecked = true;
                }
            });

            if (!isWarningChecked) {
                validated = false;
                $('#warningValidation').show();
            }
        }

        
    }

    return validated;
}

function resetApprovalForm() {
    $(".possible-fraud-cb").removeAttr("checked");
}



function ValidateFTAForSuspend() {
    var index = 1;
    var value = $('#Attribute' + index).val();
    var validated = true;
    var badCount = 0;
    // reset any previous validation messages
    $("#AdminActionReasonIDValidation").hide();
    $("#AtLeastOneBadFTASelected").hide();

    while (value != undefined) {
        $('#ValidationMessage' + index).hide();
        $('#RadioValidation' + index).hide();
        $('#TextUnAcceptableValidation' + index).hide();
        var valueLength = $.trim($(tinyMCE.get('Attribute' + index).getContent()).text()).length;
        var maxLength = $('#MaxLength' + index).val();
        if (valueLength > maxLength) {
            $('#CharactersTooLong' + index).text(valueLength - maxLength);
            $('#ValidationMessage' + index).show();
            validated = false;
        }
        //Validate radio buttons
        if ($('input[name=TextAcceptable' + index + ']:checked').size() == 0) {
            $('#RadioValidation' + index).show();
            validated = false;
        }
        //Selecting Bad option, atleast one option from the reasons list should be selected
        else if ($('input[name=TextAcceptable' + index + ']:checked').val() == "false") {
            badCount++;
            if ($('input[name=multiselect_TextUnacceptableReasons' + index + '][aria-selected=true]').length == 0) {
                $('#TextUnAcceptableValidation' + index).show();
                validated = false;
            }
        }

        index++;
        value = undefined;
        value = $('#Attribute' + index).val();
    }

    var selectedAdminActionReason = $("#AdminActionReasonID :selected").val();

    if (selectedAdminActionReason <= 0) {
        $("#AdminActionReasonIDValidation").show();
        validated = false;
    }

    if (badCount == 0) {
        $("#AtLeastOneBadFTASelected").show();
        validated = false;
    }

    if (validated) {
        $('#Suspend').val('true');
    }

    return validated;
}