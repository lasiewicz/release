﻿
function spark_util_addItem(arr, item) {
    arr[arr.length] = item;
    return arr;
}

//initial definitions for manager and likes
function FBLikeObj() {
    this.id = 0;
    this.name = '';
    this.category = '';
    this.categoryID = 0;
    this.categoryGroupID = 0;
    this.picture = '';
    this.link = '';
    this.type = '';
    this.selected = false;
    this.chkEditID = '';
    this.saved = false;
    this.approved = false;
    this.checkedHTML = function(){
        if (this.selected){
            return "checked=\"checked\"";
        }
        else{
            return "";
        }
    }
    this.selectedClass = function(){
        if (this.selected){
            return fbLikeManager.cssClassLikeItemSelected;
        }
        else{
            return "";
        }
    }
    this.approvedClass = function(){
        if (this.approved){
            return fbLikeManager.cssClassLikeItemApproved;
        }
        else{
            return "";
        }
    }
}

function FBLikeCategoryObj(){
    this.categoryName = '';
    this.categoryID = 0;
}

function FBLikeCategoryGroupObj(){
    this.categoryGroupID = 0;
    this.categoryGroupName = '';
    this.containerID = '';
    this.containerEditID = '';
    this.fbLikeCategoryObjList = [];
    this.fbLikeObjList = [];
}

FBLikeCategoryGroupObj.prototype.getFBCategory = function(category) {
    var categoryObj = null;
    if (this.fbLikeCategoryObjList.length > 0) {
        for (var i = 0; i < this.fbLikeCategoryObjList.length; i++) {
            if (this.fbLikeCategoryObjList[i].categoryName.toLowerCase() == category.toLowerCase()) {
                categoryObj = this.fbLikeCategoryObjList[i];
                break;
            }
        }
    }

    return categoryObj;
}
FBLikeCategoryGroupObj.prototype.getFBCategoryByID = function(categoryID) {
    var categoryObj = null;
    if (this.fbLikeCategoryObjList.length > 0) {
        for (var i = 0; i < this.fbLikeCategoryObjList.length; i++) {
            if (this.fbLikeCategoryObjList[i].categoryID == categoryID) {
                categoryObj = this.fbLikeCategoryObjList[i];
                break;
            }
        }
    }

    return categoryObj;
}
FBLikeCategoryGroupObj.prototype.addFBLike = function(fbLikeObj) {
    if (fbLikeObj != null && fbLikeObj.id > 0) {
        var existingFBLikeObj = null;
        if (fbLikeObj.saved != true){
            existingFBLikeObj = this.getFBLike(fbLikeObj.id);
        }
        if (existingFBLikeObj == null){
            var categoryObj = null;
            if (fbLikeObj.categoryID > 0){
                categoryObj = this.getFBCategoryByID(fbLikeObj.categoryID);
            }
            else{
                categoryObj = this.getFBCategory(fbLikeObj.category);
            }

            if (categoryObj != null){
                fbLikeObj.categoryID = categoryObj.categoryID;
                //fbLikeObj.category = categoryObj.categoryName;
                spark_util_addItem(this.fbLikeObjList, fbLikeObj);
                return true;
            }
            else{
                return false;
            }
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}
FBLikeCategoryGroupObj.prototype.getFBLike = function(id) {
    var fbLikeObj = null;
    if (id != null && id > 0){
        if (this.fbLikeObjList.length > 0) {
            for (var i = 0; i < this.fbLikeObjList.length; i++) {
                if (this.fbLikeObjList[i].id == id) {
                    fbLikeObj = this.fbLikeObjList[i];
                    break;
                }
            }
        }
    }

    return fbLikeObj;
}
FBLikeCategoryGroupObj.prototype.getFBLikeCountAll = function() {
    return this.fbLikeObjList.length;
}
FBLikeCategoryGroupObj.prototype.getFBLikeCountSaved = function() {
    var savedCount = 0;
    if (this.fbLikeObjList.length > 0) {
        for (var j = 0; j < this.fbLikeObjList.length; j++) {
            if (this.fbLikeObjList[j].saved == true) {
                savedCount++;
            }
        }
    }
    return savedCount;
}
FBLikeCategoryGroupObj.prototype.getFBLikeCountApproved = function() {
    var approvedCount = 0;
    if (this.fbLikeObjList.length > 0) {
        for (var j = 0; j < this.fbLikeObjList.length; j++) {
            if (this.fbLikeObjList[j].approved == true) {
                approvedCount++;
            }
        }
    }
    return approvedCount;
}

function FBLikeManager() {
    this.memberID = 0;
    this.isOwnProfile = true;
    this.fbConnectStatusID = 0;
    this.fbUserID = 0;
    this.fbHasSavedLikes = true;
    this.fbLikeCategoryGroupObjList = [];
    this.likesWidgetControlContainerID = '';
    this.likesContainerEditID = '';
    this.widgetHasDataEditContainerID = '';
    this.fbSubmitDataID = '';
    this.fbRemoveAllDataID = '';
    this.isBlocked = false;
    this.blockDivId = '';
    this.scrollToDiv = null;
    this.updateInProgress = false;
    this.isEdit = true;
    this.isVerbose = true;
    this.cssClassLikeItemSelected = "selected";
    this.cssClassLikeItemSaved = "saved";
    this.cssClassLikeItemApproved = "approved";
    this.templateGroupEdit = "<li id=\"{{containerEditID}}\" class=\"clearfix\"><h3>{{categoryGroupName}}</h3><ol></ol></li>";
    this.templateLikeEdit = "<li class=\"{{approvedClass}} {{selectedClass}}\"><span class=\"spr s-icon-check-active\"></span><a title=\"{{name}}\" href=\"{{link}}\" target=\"_blank\"><span class=\"image\"><img src=\"{{picture}}\"></span><span class=\"name\">{{name}}</span></a><input id=\"{{chkEditID}}\" type=\"checkbox\" {{checkedHTML}} onclick=\"fbLikeManager.updateLikeSelect('{{chkEditID}}','{{id}}', '{{categoryGroupID}}');\"></input></li>";
    this.addFBLike = function(fbLikeObj) {
        var fbLikeCategoryGroupObj = null;
        if (fbLikeObj.categoryGroupID > 0){
            fbLikeCategoryGroupObj = fbLikeManager.getFBLikeCategoryGroupByID(fbLikeObj.categoryGroupID);
        }
        else{
            fbLikeCategoryGroupObj = fbLikeManager.getFBLikeCategoryGroup(fbLikeObj.category);
        }

        if (fbLikeCategoryGroupObj != null) {
            fbLikeObj.categoryGroupID = fbLikeCategoryGroupObj.categoryGroupID;
            return fbLikeCategoryGroupObj.addFBLike(fbLikeObj);
        }
        else {
            return false;
        }
    }
    this.getFBLike = function(id) {
        var fbLikeObj = null;
        if (this.fbLikeCategoryGroupObjList.length > 0) {
            for (var i = 0; i < this.fbLikeCategoryGroupObjList.length; i++) {
                fbLikeObj = this.fbLikeCategoryGroupObjList[i].getFBLike(id);
                if (fbLikeObj != null){
                    break;
                }
            }
        }
        return fbLikeObj;
    }
    this.getFBLikeCategoryGroup = function(category){
        var fbLikeCategoryGroupObj = null;
        if (category != null && category != ''){
            if (this.fbLikeCategoryGroupObjList.length > 0) {
                for (var i = 0; i < this.fbLikeCategoryGroupObjList.length; i++) {
                    if (this.fbLikeCategoryGroupObjList[i].getFBCategory(category) != null) {
                        fbLikeCategoryGroupObj = this.fbLikeCategoryGroupObjList[i];
                        break;
                    }
                }
            }
        }

        return fbLikeCategoryGroupObj;
    }
    this.getFBLikeCategoryGroupByID = function(id){
        var fbLikeCategoryGroupObj = null;
        if (id > 0){
            if (this.fbLikeCategoryGroupObjList.length > 0) {
                for (var i = 0; i < this.fbLikeCategoryGroupObjList.length; i++) {
                    if (this.fbLikeCategoryGroupObjList[i].categoryGroupID == id) {
                        fbLikeCategoryGroupObj = this.fbLikeCategoryGroupObjList[i];
                        break;
                    }
                }
            }
        }

        return fbLikeCategoryGroupObj;
    }
    this.ajaxBlockLoading = function(){
        this.ajaxBlock('<div class="loading spinner-only" /><blockquote>Loading...</blockquote>');
    }
    this.ajaxBlockSaving = function(){
        this.ajaxBlock('<div class="loading spinner-only" /><blockquote>Saving...</blockquote>');
    }
    this.ajaxBlock= function (_message) {
        if(!this.isBlocked) {
            this.isBlocked = true;
            var blockParams = {
                message: _message,
                centerY: false,
                css: { backgroundColor: 'transparent', border: 'none', height: '85px', top: '30%' },
                overlayCSS: { backgroundColor: '#fff', opacity: 0.8 }
            };
            var blockObj = $('#' + this.blockDivId);
            blockObj.block(blockParams);                
        }
    }
    this.ajaxUnBlock= function () {
        if (this.isBlocked){
            var blockObj = $('#' + this.blockDivId);
            blockObj.unblock();
        }
        this.isBlocked = false;
    }
    this.GoToByScroll = function(){
        if(null != this.scrollToDiv) {
            $('html,body').animate({ scrollTop: this.scrollToDiv.offset().top }, 'fast');
        }
    }
    this.unselectAll = function () {
        fbLikeManager.ajaxBlockLoading();
        if (fbLikeManager.fbLikeCategoryGroupObjList.length > 0) {
            for (var i = 0; i < fbLikeManager.fbLikeCategoryGroupObjList.length; i++) {
                var fbLikeCategoryGroupObj = fbLikeManager.fbLikeCategoryGroupObjList[i];
                if (fbLikeCategoryGroupObj.fbLikeObjList.length > 0) {
                    for (var j = 0; j < fbLikeCategoryGroupObj.fbLikeObjList.length; j++) {
                        var fbLikeObj = fbLikeCategoryGroupObj.fbLikeObjList[j];
                        fbLikeObj.selected = false;
                    }
                }
            }
        }
        fbLikeManager.loadEdit();
    }
    this.selectAll = function () {
        fbLikeManager.ajaxBlockLoading();
        if (fbLikeManager.fbLikeCategoryGroupObjList.length > 0) {
            for (var i = 0; i < fbLikeManager.fbLikeCategoryGroupObjList.length; i++) {
                var fbLikeCategoryGroupObj = fbLikeManager.fbLikeCategoryGroupObjList[i];
                if (fbLikeCategoryGroupObj.fbLikeObjList.length > 0) {
                    for (var j = 0; j < fbLikeCategoryGroupObj.fbLikeObjList.length; j++) {
                        var fbLikeObj = fbLikeCategoryGroupObj.fbLikeObjList[j];
                        fbLikeObj.selected = true;
                    }
                }
            }
        }
        fbLikeManager.loadEdit();
    }
    this.loadEdit = function () {
        fbLikeManager.ajaxBlockLoading();
        var likesContainerEdit = document.getElementById(fbLikeManager.likesContainerEditID);
        if (fbLikeManager.isOwnProfile == true) {
            this.isEdit = true;
            likesContainerEdit.innerHTML = "";

            // generate HTML holder for each category defined in categories
            for (var i = 0; i < fbLikeManager.fbLikeCategoryGroupObjList.length; i++) {
                likesContainerEdit.innerHTML += Mustache.to_html(fbLikeManager.templateGroupEdit, fbLikeManager.fbLikeCategoryGroupObjList[i]);
            }

            // load current likes
            if (fbLikeManager.fbLikeCategoryGroupObjList.length > 0) {
                for (var i = 0; i < fbLikeManager.fbLikeCategoryGroupObjList.length; i++) {
                    var fbLikeCategoryGroupObj = fbLikeManager.fbLikeCategoryGroupObjList[i];
                    if (fbLikeCategoryGroupObj.fbLikeObjList.length > 0) {
                        for (var j = 0; j < fbLikeCategoryGroupObj.fbLikeObjList.length; j++) {
                            var fbLikeObj = fbLikeCategoryGroupObj.fbLikeObjList[j];
                            target = document.getElementById(fbLikeCategoryGroupObj.containerEditID).childNodes[1];
                            target.innerHTML += Mustache.to_html(this.templateLikeEdit, fbLikeObj);
                        }
                    }
                }
            }

            $('#' + fbLikeManager.widgetHasDataEditContainerID).show();
            fbLikeManager.uiControl();
        }
    }
    this.getCategoryIndex = function (category){
        var indexVal = 0;
        if (category != null && category != ''){
            if (this.categories.length > 0) {
                for (var i = 0; i < this.categories.length; i++) {
                    if (this.categories[i].toLowerCase() == category.toLowerCase()) {
                        indexVal = i;
                        break;
                    }
                }
            }
        }
        return indexVal;
    }
    this.isAllowedCategory = function(category){
        var isAllowed = false;
        if (this.getFBLikeCategoryGroup(category) != null){
            isAllowed = true;
        }
        return isAllowed;
    }
    this.changePublishStatusClick = function(){
        var radGroupName = $("#<%=radioFBPublishShow.ClientID%>").attr('name');
        if (radGroupName != ''){
            var selectedVal = $("input:radio[name=" + radGroupName + "]:checked").val();
            if (selectedVal == 'show'){
                fbLikeManager.updateFBConnectStatus(fbLikeManager.connectStatusIDShowData);
                $('#<%=fbNotPublishedStatusContainer.ClientID%>').hide();
                $('#<%=fbPublishedStatusContainer.ClientID%>').show();
            }
            else{
                fbLikeManager.updateFBConnectStatus(fbLikeManager.connectStatusIDHideData);
                $('#<%=fbPublishedStatusContainer.ClientID%>').hide();
                $('#<%=fbNotPublishedStatusContainer.ClientID%>').show();
            }
        }
    }
    this.updateLikeSelect = function(chkID, likeID, categoryGroupID){
        //select or unselect a specific like            
        var categoryGroupObj = fbLikeManager.getFBLikeCategoryGroupByID(categoryGroupID);
        if (categoryGroupObj != null){
            var likesObj = categoryGroupObj.getFBLike(likeID);
            if (likesObj != null){
                //if ($("#" + chkID).is(':checked')){
                if ($("#" + chkID).closest('li').hasClass('selected')){
                    likesObj.selected = false;
                    if (fbLikeManager.isVerbose){
                        try{
                            console.log('Unselected like - ' + chkID + ', ' + likeID + ', ' + categoryGroupID);
                        }
                        catch(e){}
                    }
                }
                else{
                    likesObj.selected = true;
                    if (fbLikeManager.isVerbose){
                        try{
                            console.log('Selected like - ' + chkID + ', ' + likeID + ', ' + categoryGroupID);
                        }
                        catch(e){}
                    }
                }
            }
        }
    }
    this.prepareRejectAllLikes = function () {
        fbLikeManager.ajaxBlockSaving();

        //reject all data
        var fbLikesData = "";
        var fbLikesSelected = 0;
        var fbObjSeparator = "sparkobj&&";
        var fbObjNVSeparator = "==";
        var fbObjParamSeparator = "&&";
        if (fbLikeManager.fbLikeCategoryGroupObjList.length > 0) {
            for (var i = 0; i < fbLikeManager.fbLikeCategoryGroupObjList.length; i++) {
                var fbLikeCategoryGroupObj = fbLikeManager.fbLikeCategoryGroupObjList[i];
                if (fbLikeCategoryGroupObj.fbLikeObjList.length > 0) {
                    for (var j = 0; j < fbLikeCategoryGroupObj.fbLikeObjList.length; j++) {
                        var fbLikeObj = fbLikeCategoryGroupObj.fbLikeObjList[j];
                        fbLikesData = fbLikesData + "id" + fbObjNVSeparator + fbLikeObj.id + fbObjParamSeparator;
                        fbLikesData = fbLikesData + "name" + fbObjNVSeparator + fbLikeObj.name + fbObjParamSeparator;
                        fbLikesData = fbLikesData + "categoryID" + fbObjNVSeparator + fbLikeObj.categoryID + fbObjParamSeparator;
                        fbLikesData = fbLikesData + "categoryGroupID" + fbObjNVSeparator + fbLikeObj.categoryGroupID + fbObjParamSeparator;
                        fbLikesData = fbLikesData + "picture" + fbObjNVSeparator + fbLikeObj.picture + fbObjParamSeparator;
                        fbLikesData = fbLikesData + "link" + fbObjNVSeparator + fbLikeObj.link + fbObjParamSeparator;
                        fbLikesData = fbLikesData + "type" + fbObjNVSeparator + fbLikeObj.type + fbObjParamSeparator;
                        fbLikesData = fbLikesData + "approved" + fbObjNVSeparator + "false" + fbObjParamSeparator;
                        fbLikesData = fbLikesData + fbObjSeparator;
                    }
                }
            }
        }

        $('#' + this.fbSubmitDataID).val(fbLikesData);
    }
    this.prepareSaveLikesByMember = function () {
        fbLikeManager.ajaxBlockSaving();

        //save all selected likes
        var fbLikesData = "";
        var fbLikesSelected = 0;
        var fbObjSeparator = "sparkobj&&";
        var fbObjNVSeparator = "==";
        var fbObjParamSeparator = "&&";
        if (fbLikeManager.fbLikeCategoryGroupObjList.length > 0) {
            for (var i = 0; i < fbLikeManager.fbLikeCategoryGroupObjList.length; i++) {
                var fbLikeCategoryGroupObj = fbLikeManager.fbLikeCategoryGroupObjList[i];
                if (fbLikeCategoryGroupObj.fbLikeObjList.length > 0) {
                    for (var j = 0; j < fbLikeCategoryGroupObj.fbLikeObjList.length; j++) {
                        var fbLikeObj = fbLikeCategoryGroupObj.fbLikeObjList[j];
                        if (fbLikeObj.selected == true) {
                            fbLikesData = fbLikesData + "id" + fbObjNVSeparator + fbLikeObj.id + fbObjParamSeparator;
                            fbLikesData = fbLikesData + "name" + fbObjNVSeparator + fbLikeObj.name + fbObjParamSeparator;
                            fbLikesData = fbLikesData + "categoryID" + fbObjNVSeparator + fbLikeObj.categoryID + fbObjParamSeparator;
                            fbLikesData = fbLikesData + "categoryGroupID" + fbObjNVSeparator + fbLikeObj.categoryGroupID + fbObjParamSeparator;
                            fbLikesData = fbLikesData + "picture" + fbObjNVSeparator + fbLikeObj.picture + fbObjParamSeparator;
                            fbLikesData = fbLikesData + "link" + fbObjNVSeparator + fbLikeObj.link + fbObjParamSeparator;
                            fbLikesData = fbLikesData + "type" + fbObjNVSeparator + fbLikeObj.type + fbObjParamSeparator;
                            fbLikesData = fbLikesData + "approved" + fbObjNVSeparator + "true" + fbObjParamSeparator;
                            fbLikesData = fbLikesData + fbObjSeparator;
                            fbLikesSelected++;
                        }
                    }
                }
            }
        }

        if (fbLikeManager.isVerbose) {
            try {
                console.log('Approving likes - ' + fbLikesData);
            }
            catch (e) { }
        }
        $('#' + this.fbSubmitDataID).val(fbLikesData);
        if (fbLikesSelected <= 0) {
            $('#' + this.fbRemoveAllDataID).val('true');
        }
    }
    this.prepareSaveLikesBySiteCommunity = function () {
        fbLikeManager.ajaxBlockSaving();

        //save all selected likes
        var fbLikesData = "";
        var fbLikesSelected = 0;
        var fbObjSeparator = "sparkobj&&";
        var fbObjNVSeparator = "==";
        var fbObjParamSeparator = "&&";
        if (fbLikeManager.fbLikeCategoryGroupObjList.length > 0) {
            for (var i = 0; i < fbLikeManager.fbLikeCategoryGroupObjList.length; i++) {
                var fbLikeCategoryGroupObj = fbLikeManager.fbLikeCategoryGroupObjList[i];
                if (fbLikeCategoryGroupObj.fbLikeObjList.length > 0) {
                    for (var j = 0; j < fbLikeCategoryGroupObj.fbLikeObjList.length; j++) {
                        var fbLikeObj = fbLikeCategoryGroupObj.fbLikeObjList[j];
                        fbLikesData = fbLikesData + "id" + fbObjNVSeparator + fbLikeObj.id + fbObjParamSeparator;
                        fbLikesData = fbLikesData + "name" + fbObjNVSeparator + fbLikeObj.name + fbObjParamSeparator;
                        fbLikesData = fbLikesData + "categoryID" + fbObjNVSeparator + fbLikeObj.categoryID + fbObjParamSeparator;
                        fbLikesData = fbLikesData + "categoryGroupID" + fbObjNVSeparator + fbLikeObj.categoryGroupID + fbObjParamSeparator;
                        fbLikesData = fbLikesData + "picture" + fbObjNVSeparator + fbLikeObj.picture + fbObjParamSeparator;
                        fbLikesData = fbLikesData + "link" + fbObjNVSeparator + fbLikeObj.link + fbObjParamSeparator;
                        fbLikesData = fbLikesData + "type" + fbObjNVSeparator + fbLikeObj.type + fbObjParamSeparator;
                        if (fbLikeObj.selected == true) {
                            fbLikesData = fbLikesData + "approved" + fbObjNVSeparator + "true" + fbObjParamSeparator;
                            fbLikesSelected++;
                        }
                        else {
                            fbLikesData = fbLikesData + "approved" + fbObjNVSeparator + "false" + fbObjParamSeparator;
                        }
                        fbLikesData = fbLikesData + fbObjSeparator;
                    }
                }
            }
        }

        if (fbLikeManager.isVerbose) {
            try {
                console.log('Approving likes - ' + fbLikesData);
            }
            catch (e) { }
        }
        $('#' + this.fbSubmitDataID).val(fbLikesData);

    }
    // jQuery ui manipulation
    this.uiControl = function () {
        // collapse & expand - display counter
        var fbLikeCategoryGroupObjListWithLikes = [],
            fbLikeColNum = 6,
            fbLikeGroupNum = 5,
            $widgetContainer = $('#' + fbLikeManager.likesContainerEditID);

        for (var i = 0; i < fbLikeManager.fbLikeCategoryGroupObjList.length; i++) {
            var fbLikeCategoryGroupObj = fbLikeManager.fbLikeCategoryGroupObjList[i],
                fbLikeTotalItemNum = fbLikeCategoryGroupObj.getFBLikeCountAll(),
                fbLikeDisplayItemNum = fbLikeTotalItemNum - fbLikeColNum;

            //remove empty categories
            if (fbLikeTotalItemNum <= 0) {
                $('#' + fbLikeCategoryGroupObj.containerEditID).remove();
            }
            else {
                spark_util_addItem(fbLikeCategoryGroupObjListWithLikes, fbLikeCategoryGroupObj);
            }
        }

        // select/unselect fb-likes
        $('#' + fbLikeManager.likesContainerEditID + ' ol > li').click(function (e) {
            var $target = $(e.target),
                $this = $(this),
                $input = $this.find('input');

            e.stopPropagation();

            if ($target.is('input')) {
                $this.toggleClass(fbLikeManager.cssClassLikeItemSelected);
            } else if ($target.hasClass('name')) {
                //it goes to the fan page
            } else {
                e.preventDefault();
                $input.trigger("click");
            }
        });

        //alert('UIControl manipulation done');
        fbLikeManager.ajaxUnBlock();
    }
}
