var PremiumTypes = {
    None: { value: 0, name: "None" },
    HighlightedProfile: { value: 1, name: "HighlightedProfile" },
    SpotlightMember: { value: 2, name: "SpotlightMember" },
    JMeter: { value: 4, name: "JMeter" },
    ColorCode: { value: 8, name: "ColorCode" },
    ServiceFee: { value: 16, name: "ServiceFee" },
    AllAccess: { value: 32, name: "AllAccess" },
    AllAccessEmail: { value: 64, name: "AllAccessEmail" },
    ReadReceipt: { value: 128, name: "ReadReceipt" }
};

$(document).ready(function() {
    attachEventsGlobal();

    if ($('.Search.Index').length > 0) {
        attachEventsMemberSearchPage();
    }
    if ($('.Member.View').length > 0) {
        attachEventsResultPage();
    }

    if ($('.Tools.ViewBulkMailSimulator').length > 0) {
        attachEventsResultPage();
    }

});

function attachEventsGlobal() {
    $('ul.sf-menu').superfish({
        speed: 'fast',
        delay: 150,                // the delay in milliseconds that the mouse can remain outside a submenu without it closing 
        animation: { opacity: 'show', height: 'show'}  // fade-in and slide-down animation 
    });
    //$("#member-search-nav .sf-menu li li").css({ opacity: .95 });

    // Make links pop up in a new window
    $('.pop-link').click(function(e) {
        e.preventDefault();
        var specialClass = $(this).attr('class').split(" ")[1];
        if (specialClass == "photo-approve") {
            popmeup(this.href, 1060);
        } else {
            popmeup(this.href, 790, 780);
        }
    });
}   

function attachEventsMemberSearchPage() {
    // Member Search - Search Results Table		
    $('#search-results-table tbody tr:odd').addClass('stripe-results');
    $('#search-results-table tbody tr').hover(function() {
        $(this).toggleClass('results-hover');
    });


    $("#search-by-billing").click(
           function (e) {
               var virtualTerminalUrl = $("#VirtualTerminalUrl").val() + "CreditCardSearch";
               window.open(virtualTerminalUrl, 'name', 'height=500,width=400,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,status=yes');
           });

       // Legend popup
       $('#legend').click(function(e) {
           ieSafePreventDefault(e);
           $('#legend-container').fadeIn(100);
       });

       // Legend close
       $('.close-x, .cancel-overlay').click(function (e) {
           ieSafePreventDefault(e);
           if ($(this).parent().attr('id') == "legend-container") {
               $(this).parent().fadeOut(300);
           } else {
               if ($.browser.msie) {
                   $(this).parent().slideUp('fast');
               }
               else {
                   $(this).parent().hide('slide', { direction: 'down' }, 300);
               }

           }
       });

       // Make the currently selected site in the site list display in the correct color
       $('.site-list').each(function() {
           var thisId = '#' + $(this).attr('id');
           $(this).addClass($(thisId + ' option:selected').attr('class'));
       });

       // Change the 'View' link based on the site selected in the drop down
       $('.site-list').change(function() {
           var userId = $(this).attr('class').split(" ")[1];
           var siteID = $(this).val();
           var viewLink = $('#view-member-' + userId);
           var thisId = '#' + $(this).attr('id');

           viewLinkHref = viewLink.attr('href').split("/");
           viewLinkHref[3] = siteID;
           viewLink.attr('href', viewLinkHref.join("/"));
           $(this)
    .removeClass('site-status-Active site-status-AdminSuspend site-status-SelfSuspend')
    .addClass($(thisId + ' option:selected').attr('class'));

       });
     
   $('input[value="Clear"]').click(function() {
       clearFormElements($(this).parents('form'));
   });



}

function attachEventsResultPage() {
// Overlays
    $('.trigger-overlay').click(function(e) {
        ieSafePreventDefault(e);
        showOverlay($(this).attr('id'), $(this).closest('div[id]').attr('id'));
    });

    $('.close-x, .cancel-overlay').click(function(e) {
        ieSafePreventDefault(e);
        if ($(this).parent().attr('id') == "legend-container") {
            $(this).parent().fadeOut(300);
        } else {
            if ($.browser.msie) {
                $(this).parent().slideUp('fast');
            }
            else {
                $(this).parent().hide('slide', { direction: 'down' }, 300);
            }

        }
    });

    $('input[name="AdminSuspended"]').click(function() {
        var adminSuspend = $(this).val();
        if (adminSuspend == 'false') {
            $('#AdminActionReasonID')
                    .attr('disabled', 'disabled')
                    .val('0');
        }
        else {
            //$('#AdminActionReasonID').attr('disabled', '');
            $('#AdminActionReasonID').removeAttr('disabled');
        }
    });

    $('.more-link').live('click', function(e) {

        ieSafePreventDefault(e);
        var self = $(this);
        var moreText = self.parent().parent().next('.more-text');
        moreText.toggleClass('hide')

        if (self.html() == "[more]") {
            self.html("[less]");
        } else {
            self.html("[more]");
        }
    });


    $('#expandALL').click(function(e) {
        ieSafePreventDefault(e);
        
    $('.more-link').each(function(e) {
        ieSafePreventDefault(e);
            var self = $(this);
            if (self.html() != "[less]") {
                self.html("[less]");
            } 
        });

        $('tr.more-text').each(function(e) {
            ieSafePreventDefault(e);
             $(this).removeClass('hide')
             //$(this).addClass('hide');
            //$(this).toggleClass('hide');
        });

    });

    $('#contractALL').click(function(e) {
          ieSafePreventDefault(e);

        $('.more-link').each(function(e) {
           ieSafePreventDefault(e);
            var self = $(this);
            if (self.html() != "[more]") {
                self.html("[more]");
            }
        });

        $('tr.more-text').each(function(e) {
            ieSafePreventDefault(e);
            $(this).addClass('hide');
            
        });

    });

}

/* User Admin functions */

function SendKeepalivePulse(testInstanceID) {
    // url: /TrainingToolAPI/SendKeepalivePulse

    $.ajax({
        type: "POST",
        url: "/TrainingToolAPI/SendKeepalivePulse",
        data: { TestInstanceID: testInstanceID },
        success: function (result) {
            // do nothing for now
        }
    });
}

function MoveTTMemberProfile(e, target, ttMemberProfileID, memberID, newTTGroupingID) {
    // url: /TrainingToolAPI/MoveMemberProfile
    ieSafePreventDefault(e);
    var isValid = true;

    if (isValid) {
        showHideLoadingAnimation(target, 'show');

        $.ajax({
            type: "POST",
            url: "/TrainingToolAPI/MoveMemberProfile",
            data: { TtMemberProfileID: ttMemberProfileID, MemberID: memberID, TtGroupingID: newTTGroupingID },
            success: function (result) {
                showHideLoadingAnimation(target, 'hide');
                alertMessage(result.StatusMessage, result.Status);

                // if success move the user away from this page. this page values are no longer valid.
                if (result.Status == 2) {
                    setTimeout(function () {
                        window.location = "/TrainingTool/TTManageGrouping";
                    }, 6000);
                }
            }
        });
    }
}

function PauseTestSession(e, target, testInstanceID, memberID) {
    // url: /TrainingToolAPI/PauseTestSession
    ieSafePreventDefault(e);
    var isValid = true;
    
    if (isValid) {
        showHideLoadingAnimation(target, 'show');

        $.ajax({
            type: "POST",
            url: "/TrainingToolAPI/PauseTestSession",
            data: { TestInstanceID: testInstanceID },
            success: function (result) {
                alertMessage(result.StatusMessage, result.Status);
                setTimeout(function () {
                    window.location = "/TrainingTool/TTTraineeStart/" + memberID;
                }, 6000)
            }
        });
    }
}

function CreateNewTTTestInstance(e, target, groupingID, adminID) {
    // url: /TrainingToolAPI/CreateNewTestInstance
    ieSafePreventDefault(e);
    var isValid = true;

    if (isValid) {
        showHideLoadingAnimation(target, 'show');

        $.ajax({
            type: "POST",
            url: "/TrainingToolAPI/CreateNewTestInstance",
            data: { GroupingID: groupingID, AdminID: adminID },
            success: function (result) {
                alertMessage(result.StatusMessage, result.Status);
                showHideLoadingAnimation(target, 'hide');
                $('#HiddenTestInstanceID').val(result.TTTestInstanceID).trigger('change');
            }
        });
    }
}

function AddSingleMemberToTTGrouping(e, target, groupingID, groupingName, memberID, siteID) {
    // url: /TrainingToolAPI/AddSingleMember
    ieSafePreventDefault(e);
    var isValid = true;

    if(isValid) {
        showHideLoadingAnimation(target, 'show');

        $.ajax({
            type: "POST",
            url: "/TrainingToolAPI/AddSingleMember",
            data: { GroupingID: groupingID, GroupingName: groupingName, MemberID: memberID, SiteID: siteID },
            success: function (result) {
                alertMessage(result.StatusMessage, result.Status);
                showHideLoadingAnimation(target, 'hide');
            }
        });
    }

}

function CreateNewTTGrouping(e, target, newGroupingName) {
    // url: /TrainingToolAPI/CreateNewGrouping
    ieSafePreventDefault(e);
    var isValid = true;

    if (isValid) {
        showHideLoadingAnimation(target, 'show');

        $.ajax({
            type: "POST",
            url: "/TrainingToolAPI/CreateNewGrouping",
            data: { GroupingName: newGroupingName },
            success: function (result) {
                alertMessage(result.StatusMessage, result.Status);
                showHideLoadingAnimation(target, 'hide');

                //reload the page so the dropdown refreshes
                window.location.reload(true);
            }
        });
    }
}

function GetMemberEmailsByEmailSubstring(e, target, emailString, callBack, adminsOnly) {
    // url: /TrainingToolAPI/GetMembersByEmail
    ieSafePreventDefault(e);
    var isValid = true;

    if (isValid) {
        showHideLoadingAnimation(target, 'show');

        $.ajax({
            type: "POST",
            url: "/TrainingToolAPI/GetMemberEmailsByEmailSubstring",
            data: { EmailString: emailString, AdminsOnly: adminsOnly },
            success: function (result) {
                showHideLoadingAnimation(target, 'hide');
                callBack(result);
            }
        });
    }
}

function GetTTMemberProfilesByMemberID(e, target, memberID, callBack) {
    // url: /TrainingToolAPI/GetMemberProfilesByMemberID
    ieSafePreventDefault(e);
    var isValid = true;

    if (isValid) {
        showHideLoadingAnimation(target, 'show');

        $.ajax({
            type: "POST",
            url: "/TrainingToolAPI/GetMemberProfilesByMemberID",
            data: { MemberID: memberID },
            success: function (result) {
                showHideLoadingAnimation(target, 'hide');
                var found = false;
                if (result.TotalRows > 0) {
                    found = true;
                }

                callBack(result.MemberProfiles, found);
            }
        });
    }
}

function GetTTMemberProfilesForGroupingID(e, target, groupingID, pageNumber, rowsPerPage) {
    // url: /TrainingToolAPI/GetMemberProfiles
    ieSafePreventDefault(e);
    var isValid = true;

    if (isValid) {
        showHideLoadingAnimation(target, 'show');

        $.ajax({
            type: "POST",
            url: "/TrainingToolAPI/GetMemberProfiles",
            data: { GroupingID: groupingID, PageNumber: pageNumber, RowsPerPage: rowsPerPage },
            success: function (result) {
                showHideLoadingAnimation(target, 'hide');

                if (result.MemberProfiles == null) {
                    $('#MemberProfilesContainer').html("This bucket is currently empty.");
                    return;
                }

                var template = $('#ttMemberProfileTemplate').html();
                var finalHtml = "<table><tr><th>MemberID</th><th>Answer Sheet Answer</th></tr>";
                for (var i = 0; i < result.MemberProfiles.length; i++) {
                    var memProfile = {
                        MemberID: result.MemberProfiles[i].MemberID,
                        AnswerCompleted: result.MemberProfiles[i].AnswerCompleted,
                        GroupingID: groupingID
                    };
                    var htmlString = Mustache.to_html(template, memProfile);
                    finalHtml += htmlString;
                }
                finalHtml += "</table>";
                $('#MemberProfilesContainer').html(finalHtml);

                // pagination code
                var numOfPages = Math.ceil(result.TotalRows / rowsPerPage);
                var prevLink = "";
                var nextLink = "";

                if (pageNumber > 1) {
                    prevLink = "<input type=\"button\" onclick=\"GetTTMemberProfilesForGroupingID(event, $(this), $('#GroupingID').val()," + (pageNumber - 1) + ", $('#RowsPerPage').val())\" value=\"Prev\" >";
                }

                if (pageNumber < numOfPages) {
                    nextLink = "<input type=\"button\" onclick=\"GetTTMemberProfilesForGroupingID(event, $(this), $('#GroupingID').val()," + (pageNumber + 1) + ", $('#RowsPerPage').val())\" value=\"Next\" >";
                }

                var combinedHtml = prevLink + "&nbsp;" + pageNumber + "&nbsp;" + nextLink;
                $('#MemberProfilesPagination').html(combinedHtml);
            }
        });
    }
}

function UpdateEmail(e, target, memberID, siteID) {
    //url: /MemberAPI/UpdateEmail/[siteID]/[MemberID]
    ieSafePreventDefault(e);    
    var email = $('#Email').val();
    var isValid = isValidEmailAddress(email);

    if (isValid) {
        showHideLoadingAnimation(target, 'show');

        $.ajax({
            type: "POST",
            url: "/MemberAPI/UpdateEmail/" + siteID + "/" + memberID, 
            data: { Email: email },
            success: function(result) {
                alertMessage(result.StatusMessage, result.Status);
                showHideLoadingAnimation(target, 'hide');
            }
        });
    } else {
        alertMessage("Invalid email address", 3);
    }
}

function SendGiftNotification(e, target, siteID, memberID, giftID, giftCode) {
    ieSafePreventDefault(e);
    var emailAddress = $('#EmailAddress').val();
    var isValid = isValidEmailAddress(emailAddress);
    
    if (isValid) {

        $.ajax({
            type: "POST",
            url: "/SubAdminAPI/SendGiftNotification/" + memberID + "/" + siteID,
            data: { giftID: giftID, giftCode: giftCode, emailAddress: emailAddress },
            success: function (result) {
                alertMessage(result.StatusMessage, result.Status);

                if (typeof GetGiftDetails == 'function') {
                    GetGiftDetails();
                }
            }
        });
    } else {
        alertMessage("Invalid email address", 3);
    }
}

function CancelGift(e, target, giftCode) {
    ieSafePreventDefault(e);

    $.ajax({
        type: "POST",
        url: "/SubAdminAPI/CancelGift",
        data: { giftCode: giftCode },
        success: function (result) {
            
            alertMessage(result.StatusMessage, result.Status);
             
            if (typeof GetGiftDetails == 'function') {
                 
                GetGiftDetails();
            }
        }
    });

}

function ReopenGift(e, target, giftCode) {
    ieSafePreventDefault(e);

    $.ajax({
        type: "POST",
        url: "/SubAdminAPI/ReopenGift",
        data: { giftCode: giftCode },
        success: function (result) {
            alertMessage(result.StatusMessage, result.Status);
             
            if (typeof GetGiftDetails == 'function') {
                
                GetGiftDetails();
            }
        }
    });

}

function SendPassword(e, memberID, siteID) {
    //url: /MemberAPI/SendPassword/[siteID]/[MemberID]
    ieSafePreventDefault(e);
       
        var isValid = true;

        if (isValid) {
            $.ajax({
                type: "POST",
                url: "/MemberAPI/SendPassword/" + siteID + "/" + memberID,
                data: { data: 'data' }, //Dummy data so the server doesn't throw a 411 error
                success: function(result) { 
                    alertMessage(result.StatusMessage, result.Status);
            }
        });
    }
    }

    function GenerateStealthLoginLink(e, memberID, siteID, adminMemberID) {
        //url: /MemberAPI/GenerateStealthLoginLink/[siteID]/[MemberID]
        ieSafePreventDefault(e);

        var isValid = true;

        if (isValid) {
            $.ajax({
                type: "POST",
                url: "/MemberAPI/GenerateStealthLoginLink/" + siteID + "/" + memberID,
                data: { AdminMemberID: adminMemberID }, //Dummy data so the server doesn't throw a 411 error
                success: function (result) {
                    HandleStealthLoginLink(result.StealthLoginLink, result.StatusMessage, result.Status);
                }
            });
        }
    }
    
    function HandleStealthLoginLink(stealthLoginLink, statusMessage, status) {
        if (status == 2) {
            popmeupWithAddressBar(stealthLoginLink, 790, 780);
        }
        else {
            alertMessage("Unable to generate a stealth link.", status);
        }
    }
    
    function SendVerificationEmail(e, memberID, siteID) {
        //url: /MemberAPI/SendVerificationEmail/[siteID]/[MemberID]
        ieSafePreventDefault(e);

        $.ajax({
            type: "POST",
            url: "/MemberAPI/SendVerificationEmail/" + siteID + "/" + memberID,
            data: { data: 'data' }, //Dummy data so the server doesn't throw a 411 error
            success: function (result) {
                alertMessage(result.StatusMessage, result.Status);
            }
        });
    }

function UpdatePassword(e,target, memberID, siteID) {
    //url: /MemberAPI/UpdatePassword/[siteID]/[MemberID]
    ieSafePreventDefault(e);

    //TODO: validate password format
    var isValid = true;
    var newPassword = $('#NewPassword').val();

    if (isValid) {
        showHideLoadingAnimation(target,'show');
        $.ajax({
            type: "POST",
            url: "/MemberAPI/UpdatePassword/" + siteID + "/" + memberID, 
            data: { NewPassword: newPassword },  
            success: function(result) {  
                alertMessage(result.StatusMessage, result.Status);
                $('#NewPassword').val('');
                showHideLoadingAnimation(target, 'hide');

            }
        });
    }
}

function UpdateRenewalInfo(e, target, memberID, siteID) {
    ieSafePreventDefault(e);

    var reopenSub = false;
    var reopenPremiumHighlight =false;
    var reopenPremiumSpotlight = false;
    var reopenPremiumJMeter = false;
    var reopenPremiumAllAccess = false;
    var reopenPremiumReadReceipt = false;
    
    if ($("#ReopenSub").length > 0){
        reopenSub = $('#ReopenSub').is(':checked');
    }
    if ($("#ReopenPremiumHighlight").length > 0){
        reopenPremiumHighlight = $('#ReopenPremiumHighlight').is(':checked');
    }
    if ($("#ReopenPremiumSpotlight").length > 0){
        reopenPremiumSpotlight = $('#ReopenPremiumSpotlight').is(':checked');
    }
    if ($("#ReopenPremiumJMeter").length > 0){
        reopenPremiumJMeter = $('#ReopenPremiumJMeter').is(':checked');
    }
    if ($("#ReopenPremiumAllAccess").length > 0) {
        reopenPremiumAllAccess = $('#ReopenPremiumAllAccess').is(':checked');
    }
    if ($("#ReopenPremiumReadReceipt").length > 0) {
        reopenPremiumAllAccess = $('#ReopenPremiumReadReceipt').is(':checked');
    }

    $.ajax({
        type: "POST",
        url: "/MemberAPI/UpdateRenewalInfo/" + siteID + "/" + memberID,
        data: { reopenRenewal: reopenSub, reopenPremiumHighlight: reopenPremiumHighlight, reopenPremiumSpotlight: reopenPremiumSpotlight, reopenPremiumJMeter: reopenPremiumJMeter, reopenPremiumAllAccess: reopenPremiumAllAccess, reopenReadReceipt: reopenPremiumReadReceipt }, //Dummy data so the server doesn't throw a 411 error
        success: function(result) {
            if (result.Status == 3) {
                alertMessage(result.StatusMessage, 3);
            }
            if (result.Status == 2) {
                alertMessage('Changes have been saved', 2);
                showHideLoadingAnimation(target, 'hide');

                if ($("#promptRenewal").length > 0)
                    closeOverLayRenewal();
                
            }
        }
    });
}

function UpdateALaCarteRenewal(e, target, memberID, siteID, enable) {
    ieSafePreventDefault(e);

    var premTypes = new Array();
    var count = 0;

    if ($("#StopPremiumHighlight").length > 0 && $('#StopPremiumHighlight').is(':checked')) {
        premTypes[count] = PremiumTypes.HighlightedProfile.value;
        count++;
    }
    if ($("#StopPremiumSpotlight").length > 0 && $('#StopPremiumSpotlight').is(':checked')) {
        premTypes[count] = PremiumTypes.SpotlightMember.value;
        count++;
    }
    if ($("#StopPremiumJMeter").length > 0 && $('#StopPremiumJMeter').is(':checked')) {
        premTypes[count] = PremiumTypes.JMeter.value;
        count++;
    }
    if ($("#StopPremiumAllAccess").length > 0 && $('#StopPremiumAllAccess').is(':checked')) {
        premTypes[count] = PremiumTypes.AllAccess.value;
        count++;
    }
    if ($("#StopPremiumReadReceipts").length > 0 && $('#StopPremiumReadReceipts').is(':checked')) {
        premTypes[count] = PremiumTypes.ReadReceipt.value;
        count++;
    }

    $.ajax({
        type: "POST",
        traditional: true,
        dataType: "json",
        url: "/MemberAPI/UpdateALaCarteRenewal/" + memberID + "/" + siteID,
        data: { premiumTypes: premTypes, enable: enable }, 
        success: function (result) {
            if (result.Status == 3) {
                alertMessage(result.StatusMessage, 3);
            }
            if (result.Status == 2) {
                alertMessage('Changes have been saved', 2);
                showHideLoadingAnimation(target, 'hide');

                if ($("#promptRenewal").length > 0)
                    closeOverLayRenewal();

            }
        }
    });
}


function cancelTrialForMember(e, target, memberID, siteID) {
    ieSafePreventDefault(e);

    $.ajax({
        type: "POST",
        traditional: true,
        dataType: "json",
        url: "/MemberAPI/CancelFreeTrial/" + memberID + "/" + siteID,
        data: {},
        success: function (result) {
            if (result.Status == 3) {
                alertMessage(result.StatusMessage, 3);
            }
            if (result.Status == 2) {
                alertMessage('Changes have been saved', 2);
                showHideLoadingAnimation(target, 'hide');

                if ($("#promptRenewal").length > 0)
                    closeOverLayRenewal();

            }
        }
    });
}



function UpdateProfileDisplaySettings(e, target, memberID, siteID) {
    ieSafePreventDefault(e);

    var showOnline = $("#divProfileSettingsShowOnline").find("input:checked").val();
    var showInSearches = $("#divProfileSettingsShowinSearches").find("input:checked").val();
    var showActivity = $("#divProfileSettingsShowActivityTrail").find("input:checked").val();
    var showPhotos = $("#divProfileSettingsShowPhotosToNonMembers").find("input:checked").val();

    $.ajax({
        type: "POST",
        url: "/MemberAPI/UpdateProfileDisplaySettings/" + siteID + "/" + memberID,
        data: { showOnline: showOnline, showInSearches: showInSearches, showPhotosToNonMembers: showPhotos, showActivityTrail: showActivity },
        success: function(result) {
            if (result.Status == 3) {
                alertMessage(result.StatusMessage, 3);
            }
            if (result.Status == 2) {
                alertMessage('Changes have been saved', 2);
                showHideLoadingAnimation(target, 'hide');

                if ($("#promptProfileDisplaySettings").length > 0)
                    closeOverLayProfileDisplaySettings();
            }
        }
    });
    
}

function UpdateNorbertSetting(e, target, memberID, siteID) {
    ieSafePreventDefault(e);

    var isNorbert = $("#divShowNorbert").find("input:checked").val();

    $.ajax({
        type: "POST",
        url: "/MemberAPI/UpdateNorbertSetting/" + siteID + "/" + memberID,
        data: { isNorbert: isNorbert },
        success: function (result) {
            if (result.Status == 3) {
                alertMessage(result.StatusMessage, 3);
            }
            if (result.Status == 2) {
                alertMessage('Changes have been saved', 2);
                if (isNorbert == "true") {
                    $('.change-norbert').text('[Norbert]');
                }
                else {
                    $('.change-norbert').text('[Bedrock]');
                }
                showHideLoadingAnimation(target, 'hide');

                if ($("#oChangeNorbert").length > 0)
                    closeChangeNorbert();
            }
        }
    });

}

function UpdateRenewalDate(e, memberID, siteID) {
    ieSafePreventDefault(e);

    $.ajax({
        type: "POST",
        url: "/MemberAPI/UpdateRenewalDate/" + siteID + "/" + memberID,
        data: { data: 'data' }, //Dummy data so the server doesn't throw a 411 error
        success: function(result) {
            if (result.Status == 3) {
                alertMessage(result.StatusMessage, 3);
            }
            if (result.Status == 2) {
                alertMessage('Changes have been saved', 2);
                showHideLoadingAnimation(target, 'hide');

                if ($("#promptRenewal").length > 0)
                    closeOverlayRenewal();
            }
        }
    });
}

function AdminAdjust(e, target, memberID, siteID) {
    //url: /MemberAPI/AdminAdjust/[siteID]/[MemberID]
    ieSafePreventDefault(e);

    var isValid = ValidateAdminAdjust(target);

    if (!isValid)
        return;

    var hasPremiumHighlighted = $('#PremiumHighlightedProfile').is(':checked');
    var hasPremiumSpotlight = $('#PremiumMemberSpotlight').is(':checked');
    var hasPremiumJMeter = $('#PremiumJMeter').is(':checked');
    var hasPremiumAllAccess = $('#PremiumAllAccess').is(':checked');
    var hasReadReceipts = $('#PremiumReadReceipts').is(':checked');
    var direction = $('#DDLDirection').val();
    var duration = $('#Duration').val();
    var period = $('#DDLPeriod').val();

    showHideLoadingAnimation(target, 'show');
    
    $.ajax({
        type: "POST",
        url: "/MemberAPI/AdminAdjust/" + siteID + "/" + memberID,
        data: { adjustDirection: direction, adjustPeriod: period, duration: duration, includePremiumHighlight: hasPremiumHighlighted, includePremiumSpotlight: hasPremiumSpotlight, includePremiumJMeter: hasPremiumJMeter, includePremiumAllAccess: hasPremiumAllAccess, includePremiumReadReceipts: hasReadReceipts }, //Dummy data so the server doesn't throw a 411 error
        success: function(result) {
            if (result.Status == 3) {
                alertMessage(result.StatusMessage, 3);
            }
            if (result.Status == 2) {
                alertMessage('Changes have been saved', 2);
                showHideLoadingAnimation(target, 'hide');

                if ($("#promptAdminAdjust").length > 0)
                    closeOverlayAdminAdjust();
            }
        }
    });
    
}

function ValidateAdminAdjust(target) {
    var isValid = true;
    $('#AdminAdjustMessage').html('');

    if ($(target).attr("id") != "admin-adjust") {
        return true;
    }

    var durationLimit = [2622240, 43680, 1820, 260, 60, 5], //2622240 Minutes, 43680 Hours, 1820 Days, 260 Weeks, 60 Months, 5 Years.
        durationType = ['Minutes', 'Hours', 'Days', 'Weeks', 'Months', 'Years'],
        durationValue = Math.abs(parseInt($('#Duration').val(), 10)),
        periodValue = Math.abs(parseInt($('#DDLPeriod').val(), 10)),
        $adminAdjustMessage = $('#AdminAdjustMessage');

    if (durationValue && periodValue) {
        if (durationValue > durationLimit[periodValue - 1]) {
            isValid = false;
            $adminAdjustMessage.html('Please input less than ' + durationLimit[periodValue - 1] + ' ' + durationType[periodValue - 1] + ' of privileged time.');
        }
    } else {
        isValid = false;
        if (!durationValue) {
            $adminAdjustMessage.html('Please input duration value.');
        } else{
            $adminAdjustMessage.html('Please select a valid duration type.');
        }
    }

    return isValid;
}

function PauseOrResumeSubscription(e, target, memberID, siteID, hasPausedSubscription) {
    ieSafePreventDefault(e);

//    var hasPremiumHighlighted = $('#PremiumHighlightedProfile').is(':checked');
//    var hasPremiumSpotlight = $('#PremiumMemberSpotlight').is(':checked');
//    var hasPremiumJMeter = $('#PremiumJMeter').is(':checked');
//    var hasPremiumAllAccess = $('#PremiumAllAccess').is(':checked');
//    var direction = $('#DDLDirection').val();
//    var duration = $('#Duration').val();
//    var period = $('#DDLPeriod').val();

    $(target).attr('disabled', 'disabled');

    $.ajax({
        type: "POST",
        url: "/MemberAPI/PauseOrResumeSubscription/" + memberID + "/" + siteID,
        data: { hasPausedSubscription: hasPausedSubscription },
        success: function (result) {

            alert(result.StatusMessage);

            if (result.Status == 3) {
                alertMessage(result.StatusMessage, 3);
            }
            if (result.Status == 2) {
                alertMessage(result.StatusMessage, 2);
                //alertMessage('Changes have been saved', 2);
                showHideLoadingAnimation(target, 'hide');

                if ($("#promptPauseSubscriptionConfirmation").length > 0)
                    closeOverlayPauseSubscriptionConfirmation();

                //GetTransactionHistory(siteID, memberID);
                window.location.reload();
            }
        }
    });
}

function AdminAdjustAllAccessEmail(e, target, memberID, siteID) {
    //url: /MemberAPI/UpdatePassword/[siteID]/[MemberID]
    ieSafePreventDefault(e);

    var isValid = ValidateAdminAdjust(target);

    if (!isValid)
        return;

    var emailCount = $('#AllAcessEmailAdjustCount').val();


    showHideLoadingAnimation(target, 'show');

    $.ajax({
        type: "POST",
        url: "/MemberAPI/AdjustAllAccessEmails/" + siteID + "/" + memberID,
        data: { emailCount: emailCount   }, 
        success: function(result) {
            if (result.Status == 3) {
                alertMessage(result.StatusMessage, 3);
            }
            if (result.Status == 2) {
                alertMessage('Changes have been saved', 2);
                showHideLoadingAnimation(target, 'hide');

                if ($("#promptAdminAdjust").length > 0)
                    closeOverlayAdminAdjust();
            }
        }
    });
    
}

function GetPassword(e, memberID, siteID) {
    //url: /MemberAPI/GetPassword/[siteID]/[MemberID]
    ieSafePreventDefault(e);

    var isValid = true;

    if (isValid) {
        $.ajax({
            type: "POST",
            url: "/MemberAPI/GetPassword/" + siteID + "/" + memberID,
            data: { data: 'data' }, //Dummy data so the server doesn't throw a 411 error
            success: function(result) {  
                $('#lookupPasswordResult').text(result.Password);
                if(result.Status == 3){
                    alertMessage(result.StatusMessage, 3);
                }
            }
        });
    }
}

function AddAdminNote(e, memberID, siteID) {
    ieSafePreventDefault(e);

    var note = $('#Note').val(),
        reg = /<(.|\n)*?>/g; // matches all HTML tags pairs including attributes in the tags

    if (note == "") {
        alertMessage('Please enter a note', 3);
    } else if (note.length >= 3900) {
        alertMessage('The character limit is 3900. Please enter a shorter note.', 3);
    } else if (reg.test(note)) {
        alertMessage('No HTML tags are allowed!');
    } else {

        $.ajax({
            type: "POST",
            url: "/MemberAPI/AddAdminNote/" + siteID + "/" + memberID,
            data: { Note: note },
            success: function(result) {
                alertMessage(result.StatusMessage, 2);
                $('#Note').val('');
                $('#new-note')
                        .render({timestamp:getTimeStamp(), note:note})
                        .insertAfter($('#admin-notes-table tr:first'))
                        .css({ display: 'none' })
                        .fadeIn('slow');
            }
        });
    }
}

function GetProfileStatusDisplayText(profileStatus, suspendReason) {
    var displayText = '';
    if (profileStatus == 'Active') {
        displayText = 'Active';
    }
    else if (profileStatus == 'AdminSuspend') {
        displayText = 'Admin-Suspended';
    }
    else if (profileStatus == 'SelfSuspend') {
        displayText = 'Self-Suspended';
    }
    $('#profile-status').text(displayText);
    $('#profile-status').addClass(profileStatus);
   
}

function UpdateGlobalProfileInfo(e, target, confirmed, memberID, siteID) {
    //url: /MemberAPI/UpdateEmail/[siteID]/[MemberID]
    ieSafePreventDefault(e);
    var isValid = true;
    var adminSuspended = $("#AdminSuspendedDiv").find("input:checked").val();
    var badEmail = $("#BadEmailDiv").find("input:checked").val();
    var emailVerified = $("#EmailVerifiedDiv").find("input:checked").val();
    var onDne = $("#DneDiv").find("input:checked").val();
    var adminActionReasonID = $('#AdminActionReasonID').val();

    if (adminSuspended == "true" && adminActionReasonID == 0) {
        alertMessage('Reason is required to Admin Suspend. Please select from the dropdown.', 3);
    }

    else if (isValid) {
        showHideLoadingAnimation(target, 'show');
        $.ajax({
            type: "POST",
            url: "/MemberAPI/UpdateGlobalProfileInfo/" + siteID + "/" + memberID,
            data: { AdminSuspended: adminSuspended, BadEmail: badEmail, EmailVerified: emailVerified, OnDNE: onDne, adminActionReasonID: adminActionReasonID },
            success: function(result) {
                if (result.Status == 2) {
                    alertMessage(result.StatusMessage, 2);
                    showHideLoadingAnimation(target, 'hide');
                    if (adminSuspended == "true") {
                        $('#profile-status')
                        .addClass(result.ProfileStatus)
                    } else {
                        $('#profile-status')
                        .removeClass();
                    }
                    //update profile status in BasicUserInfo
                    var text = GetProfileStatusDisplayText(result.ProfileStatus, result.SuspendReason);
                    $('#profileStatusSpan').text(text);
                }
                if (result.Status == 3) {
                    alertMessage(result.StatusMessage, 3);
                    showHideLoadingAnimation(target, 'hide');
                    if (adminSuspended == "true") {
                        $('#profile-status')
                        .addClass(result.ProfileStatus)
                    } else {
                        $('#profile-status')
                        .removeClass();
                    }
                }
            }
        });
    } 
    
    
   }

   function UpdateFraudInfo(e, target, memberID, siteID) {
        //url: /MemberAPI/UpdateFraudInfo/[siteID]/[MemberID]
        ieSafePreventDefault(e);
        var isValid = true;
        var subscriptionFraud = $("#subscription-fraud").find("input:checked").val();
        var chatBanned = $("#chat-banned").find("input:checked").val();
        var emailBanned = $("#email-banned").find("input:checked").val();
        var unfraudEmails = $('#unfraud-emails').find("input:checked").length > 0;

        if (isValid) {
            showHideLoadingAnimation(target, 'show');
            $.ajax({
                type: "POST",
                url: "/MemberAPI/UpdateFraudAbuseInfo/" + siteID + "/" + memberID,
                data: { SubscriptionFraud: subscriptionFraud, ChatBanned: chatBanned, EmailBanned: emailBanned, UnfraudEmails: unfraudEmails },
                success: function(result) {
                    if (result.Status == 2) {
                        alertMessage(result.StatusMessage, 2);
                        showHideLoadingAnimation(target, 'hide');
                    }
                }
            });
        }
    }


    function UpdatePremiumServiceSettings(e, target, memberID, siteID) {
        ieSafePreventDefault(e);
        var isValid = true;

        var highlightEnabled = $("#highlight-enabled").find("input:checked").val();
        if (highlightEnabled == null) highlightEnabled = false;
        
        var spotlightGender;
        var spotlightSeekingGender;
        var spotlightMinAge;
        var spotlightMaxAge;
        var spotlightDistance;
        var spotlightRegionID;
        var spotlightEnabled = $("#spotlight-enabled").find("input:checked").val();

        if (spotlightEnabled == null) {
            spotlightEnabled = false;
            spotlightGender = 0;
            spotlightSeekingGender = 0;
            spotlightMinAge = 0;
            spotlightMaxAge = 0;
            spotlightDistance = 0;
            spotlightRegionID = 0;
        }
        else {
            spotlightGender = $('#DDLGenderPSS').val();
            spotlightSeekingGender = $('#DDLSeekingGenderPSS').val();
            spotlightMinAge = $('#SpotlightAgeMin').val();
            spotlightMaxAge = $('#SpotlightAgeMax').val();
            spotlightDistance = $('#DistancePSS').val();
            spotlightRegionID = $('[name=PSSRegionPickerRegionID]').val();
        }
       
        if (isValid) {
            showHideLoadingAnimation(target, 'show');
            $.ajax({
                type: "POST",
                url: "/MemberAPI/UpdatePremiumServiceSettings/" + siteID + "/" + memberID,
                data: { HighlightEnabled: highlightEnabled, SpotlightEnabled: spotlightEnabled, SpotlightGender: spotlightGender, SpotlightSeekingGender: spotlightSeekingGender, SpotlightMinAge: spotlightMinAge, SpotlightMaxAge: spotlightMaxAge, SpotlightDistance: spotlightDistance, SpotlightRegionID: spotlightRegionID},
                success: function(result) {
                    if (result.Status == 2) {
                        alertMessage('Changes have been saved', 2);
                        showHideLoadingAnimation(target, 'hide');

                        if ($("#promptPSS").length > 0)
                            closeOverLay();

                    }
                }
            });
        }
        
        
    }

    function UpdateOffSiteNotifications(e, target, memberID, siteID) {
        //url: /MemberAPI/UpdateOffSiteNotifications/[siteID]/[MemberID]
        
        
        ieSafePreventDefault(e);
        var isValid = true;
        var clickAlerts = $("#click-alerts").find("input:checkbox").is(":checked");
        var emailAlerts = $("#email-alerts").find("input:checkbox").is(":checked");
        var ecardAlerts = $("#ecard-alerts").find("input:checkbox").is(":checked");

        var hotlistAlerts = $("#hotlist-alerts").find("input:checkbox").is(":checked");
        var profileviewedAlerts = $("#profileviewed-alerts").find("input:checkbox").is(":checked");
        var essayRequestAlerts = $("#requestessay-alerts").find("input:checkbox").is(":checked");

        var newsletterAlerts = $("#newsletter-alerts").find("input:checkbox").is(":checked");
        var travelAlerts = $("#travel-alerts").find("input:checkbox").is(":checked");
        var partnersAlerts = $("#partners-alerts").find("input:checkbox").is(":checked");

        var matchmailFrequency = $("#matchmail-frequency").find("input:checked").val();
        var clickmailFrequency = $("#clickmail-frequency").find("input:checked").val();

        /*
        alert(clickAlerts);
        alert(emailAlerts);
        alert(ecardAlerts);
        alert(newsletterAlerts);
        alert(travelAlerts);
        alert(partnersAlerts);
        alert(matchmailFrequency);
        alert(clickmailFrequency);
        */

        if (isValid) {
            showHideLoadingAnimation(target, 'show');
            $.ajax({
                type: "POST",
                url: "/MemberAPI/UpdateOffSiteNotifications/" + siteID + "/" + memberID,
                data: { MatchmailFrequency: matchmailFrequency, ClickmailFrequency: clickmailFrequency, ClickAlert: clickAlerts,
                    EmailAlert: emailAlerts, EcardAlert: ecardAlerts, NewsletterAlert: newsletterAlerts, TraveleventsAlert: travelAlerts,
                    QualifiedpartnersAlert: partnersAlerts, HotlistAlert: hotlistAlerts, ProfileviewedAlert: profileviewedAlerts,
                    EssayRequestAlert: essayRequestAlerts },
                success: function(result) {
                    if (result.Status == 2) {
                        alertMessage('Changes have been saved', 2);
                        showHideLoadingAnimation(target, 'hide');

                        if ($("#prompt").length > 0)
                            closeOverLay();
                        
                    }
                }
            });
        }
           
    }

function UpdateSubscriptionLastInitialPurchaseDate(e, target, memberID, siteID) {
    ieSafePreventDefault(e);
    var newDate = $('#SubscriptionLastInitialPurchaseDate').val();
    $.ajax({
        type: "POST",
        url: "/MemberAPI/UpdateSubscriptionLastInitialPurchaseDate/" + siteID + "/" + memberID + "/",
        data: { newDate: newDate },
        success: function(result) {
            if (result.Status == 2) {
                alertMessage(result.StatusMessage, 2);
            }
        }
    });
}


function UpdateOtherActions(e, target, memberID, siteID) {
   //url: /MemberAPI/UpdateOtherActions/[siteID]/[MemberID]
   ieSafePreventDefault(e);
   var isValid = true;
   var colorAnalysisPurchase = $("#ColorAnalysisDiv").find("input:checked").val();

   
   if ((typeof (colorAnalysisPurchase) != "undefined"))
   {
       if (isValid) {
           showHideLoadingAnimation(target, 'show');
           $.ajax({
               type: "POST",
               url: "/MemberAPI/UpdateOtherActions/" + siteID + "/" + memberID,
               data: { ColorAnalysisPurchase: colorAnalysisPurchase, RamahDate: ramahDate },
               success: function (result) {
                   if (result.Status == 2) {
                       alertMessage(result.StatusMessage, 2);
                       showHideLoadingAnimation(target, 'hide');
                   }
               }
           });
       }
   }
   

   UpdateRamahAction(e, target, memberID, siteID);
}


function UpdateRamahAction(e, target, memberID, siteID) {
    ieSafePreventDefault(e);
    var isValid = true;
    var ramahDate = $("#ramahDiv").find("input:checked").val();

   
    
        showHideLoadingAnimation(target, 'show');
        $.ajax({
            type: "POST",
            url: "/MemberAPI/UpdateRamahAction/" + siteID + "/" + memberID,
            data: { RamahDate: ramahDate },
            success: function (result) {
                if (result.Status == 2) {
                    alertMessage(result.StatusMessage, 2);
                    showHideLoadingAnimation(target, 'hide');
                }
            }
        });
    
}




function UpdateProfileInfo(e, target, memberID, siteID) {
    //url: /MemberAPI/UpdateProfileInfo/[siteID]/[MemberID]
    ieSafePreventDefault(e);
    var isValid = true;
    var firstName = $('#FirstName').val();
    var lastName = $('#LastName').val();
    var birthdate = $('#Birthdate').val();
    var gender = $('#DDLGender').val();
    var seekingGender = $('#DDLSeekingGender').val();
    var checkName = new RegExp(/^[a-z\'\u00c0-\u00fe\u0591-\u05f4\s-]*$/i);
    
    if (firstName.length > 100 || lastName.length > 100) {
        alertMessage('The character limit is 100. Please enter a shorter name.', 3);
    }
    else if(!(checkName.test(firstName)) || !(checkName.test(lastName))){
        alertMessage('Name can contain only letters.', 3);
    }
    else if (isValid) {
        showHideLoadingAnimation(target, 'show');
        $.ajax({
            type: "POST",
            url: "/MemberAPI/UpdateProfileInfo/" + siteID + "/" + memberID,
            data: { FirstName: firstName, LastName: lastName, Birthdate: birthdate, Gender: gender, SeekingGender: seekingGender },
            success: function(result) {
                    alertMessage(result.StatusMessage, result.Status, target);
            }
        });
    }
}

function ResetJMeter(e, target, memberID, siteID) {
    showHideLoadingAnimation(target, 'show');

    $.ajax({
        type: "POST",
        url: "/MemberAPI/ResetJMeter/" + siteID + "/" + memberID,
        data: {},
        success: function (result) {
            alertMessage(result.StatusMessage, result.Status, target);
            showHideLoadingAnimation(target, 'hide');
        }
    });
}

function SyncWithZoozamen(e, target, memberID, siteID) {
    showHideLoadingAnimation(target, 'show');
    $.ajax({
        type: "POST",
        url: "/MemberAPI/SyncWithZoozamen/" + siteID + "/" + memberID,
        data: {},
        success: function (result) {
            alertMessage(result.StatusMessage, result.Status, target);
            showHideLoadingAnimation(target, 'hide');
        }
    });
}

function UpdateILAuthentication(e, target, memberID, siteID) {
    var disILAuth = $("#DisableILAuth").attr("checked");
    var expDate = $("#ILAuthExpDate").val();

    if (!disILAuth && expDate == "") {
        alertMessage("Either expiration date or the disable checkbox must be selected.");
        return;
    }

    showHideLoadingAnimation(target, 'show');

    if (expDate == "")
        expDate = "01/01/0001";

    $.ajax({
        type: "POST",
        url: "/MemberAPI/UpdateILAuthentication/" + siteID + "/" + memberID,
        data: { DisableILAuth: disILAuth, ILAuthExpDate: expDate },
        success: function (result) {
            alertMessage(result.StatusMessage, result.Status, target);
            showHideLoadingAnimation(target, 'hide');

            if ($.isFunction(closeOverLayILAuthenticated)) {
                closeOverLayILAuthenticated();
            }
            if (result.Status == 2) {
                $(".il-auth-status").html(result.ILAuthStatus);
                if (disILAuth) {
                    $("#ILAuthExpDate").val("");
                }
            }
        }
    });
}

function EndAutoRenewal(e, target, memberID, siteID) {
    //url: /MemberAPI/EndAutoRenewal/[siteID]/[MemberID]
    ieSafePreventDefault(e);
    var isValid = true;
    if (isValid) {
        $.ajax({
            type: "POST",
            url: "/MemberAPI/EndAutoRenewal/" + siteID + "/" + memberID,
            success: function(result) {
                if (result.Status == 3) {
                    alertMessage(result.StatusMessage, 3);
                }
                if (result.Status == 2) {
                    alertMessage('Changes have been saved', 2);
                    showHideLoadingAnimation(target, 'hide');

                    if ($("#promptRenewal").length > 0)
                        closeOverLayRenewal();
                }

            }
        });
    } 
}

function EndAutoRenewalWithReason(e, target, memberID, siteID, terminationReasonID) {
    //url: /MemberAPI/EndAutoRenewalWithReason/[siteID]/[MemberID]
    ieSafePreventDefault(e);
    var isValid = true;
    if (isValid) {
        $.ajax({
            type: "POST",
            url: "/MemberAPI/EndAutoRenewalWithReason/" + siteID + "/" + memberID,
            data: {TerminationReasonID: terminationReasonID},
            success: function (result) {
                if (result.Status == 3) {
                    alertMessage(result.StatusMessage, 3);
                }
                if (result.Status == 2) {
                    alertMessage('Changes have been saved', 2);
                    showHideLoadingAnimation(target, 'hide');

                    if ($("#promptRenewal").length > 0)
                        closeOverLayRenewal();
                }

            }
        });
    }
}

function ReopenAutoRenewal(e, target, memberID, siteID) {
    //url: /MemberAPI/ReopenAutoRenewal/[siteID]/[MemberID]
    ieSafePreventDefault(e);
    var isValid = true;
    var reopenMainSub = true;
    var reopenHighlight = $('#chkReopenHighlight').is(':checked');
    var reopenSpotlight = $('#chkReopenSpotlight').is(':checked');
    var reopenAllAccess = $('#chkReopenAllAccess').is(':checked');
    var reopenReadReceipt = $('#chkReopenReadReceipt').is(':checked');

    if (isValid) {
        showHideLoadingAnimation(target, 'show');
        $.ajax({
            type: "POST",
            url: "/MemberAPI/ReopenAutoRenewal/" + siteID + "/" + memberID,
            data: { ReopenMainSubscription: reopenMainSub, ReopenHighlightALC: reopenHighlight, ReopenSpotlightALC: reopenSpotlight, ReopenAllAccessALC: reopenAllAccess, reopenReadReceiptALC: reopenReadReceipt },
            success: function(result) {
                if (result.Status == 2) {
                    alertMessage(result.StatusMessage, 2);
                    showHideLoadingAnimation(target, 'hide');
                    var renewalStatus = $('#renewal-status');
                    renewalStatus.html('Renewal Status: Open <a href="#" onclick="EndAutoRenewal(event, $(this), false, ' + siteID + ', ' + memberID + ')">[Turn off Renewals]</a>')
                } else {
                    alertMessage(result.StatusMessage, result.Status);
                    showHideLoadingAnimation(target, 'hide');
                }
            }
        });
    }
}


function ClearColorCodeResults(e, target, confirmed, memberID, siteID) {
   //url: /MemberAPI/ClearColorCodeResults/[siteID]/[MemberID]
    ieSafePreventDefault(e);
    var isValid = true;
    if (isValid && confirmed) {
       showHideLoadingAnimation(target, 'show');
       $.ajax({
           type: "POST",
           url: "/MemberAPI/ClearColorCodeResults/" + siteID + "/" + memberID,
           success: function(result) {
               if (result.Status == 2 || result.Status == 3) {
                   alertMessage(result.StatusMessage, result.Status);
                   showHideLoadingAnimation(target, 'hide');
               }
           }
       });
   } else {
       showOverlay('clear-color-code', 'other-actions');
   }

}

function GiveLifetimeMembership(e, target, siteID, memberID, confirmed) {
    //url: /MemberAPI/GiveLifetimeMembership/[siteID]/[MemberID]
    ieSafePreventDefault(e);
    var isValid = true;
    if (isValid && confirmed) {
        showHideLoadingAnimation(target, 'show');
        $.ajax({
            type: "POST",
            url: "/MemberAPI/GiveLifetimeMembership/" + memberID + "/" + siteID,
            success: function(result) {
                alertMessage(result.StatusMessage, result.Status);
                showHideLoadingAnimation(target, 'hide');
            }
        });
    } else {
        showOverlay('DivGiveLifetime', 'other-actions');
    }
}

function showOverlay(overlayID, container) {
   var containerID = '#' + container;
   var position = $(containerID).offset();
   var overlayWidth = $(containerID).width();
   $('#' + overlayID + '-over')
    .css({ position: "absolute", left: position.left, top: position.top, width: overlayWidth });
    
    if ($.browser.msie){
        $('#' + overlayID + '-over').slideDown('fast');
    }
    else{
        $('#' + overlayID + '-over').show('slide', { direction: 'down' }, 300);
    }
}

function GetTransactionHistory(siteID, memberID) {

    $('#transaction-container').empty();

    $.ajax({
        type: "GET",
        url: '/MemberAPI/GetTransactionHistory?siteID=' + siteID + '&memberID=' + memberID,
        dataType: 'html',
        success: function (result) {
            var template = $('#transactionHistory').html();
            //var data = { AllAccountHistory: jQuery.parseJSON(result) };
            var data = jQuery.parseJSON(result);
            var html = Mustache.to_html(template, data);
            $('#transaction-container').html(html);
            initTabs();
            $('#transaction-container').removeClass('loading-bg');

            //BillingInfo
            DisplayBillingInfo(data);
            //GetBillingInfo(siteID, memberID);
        }
    });
}

function DisplayBillingInfo(result) {
    var templateId = 'billinginfo-none';
    if (result != null && result.billing != null) {
        var paymentType = result.billing.PaymentType;
        switch (paymentType) {
            case 0:
                templateId = 'billinginfo-none';
                break;
            case 1:
                templateId = 'billinginfo-creditcard';
                break;
            case 2:
                templateId = 'billinginfo-check';
                break;
            case 8:
                templateId = 'billinginfo-paypallitle';
                break;
            case 64:
                templateId = 'billinginfo-manual';
                break;
            case 128:
                templateId = 'billinginfo-electronictransfer';
                break;
            case 256:
                templateId = 'billinginfo-paymentreceived';
                break; 
            case 512:
                templateId = 'billinginfo-debitcard';
                break;
            case 1024:
                templateId = 'billinginfo-inapplicationpurchase';
                break;
            case 2048:
            case 4096:
                templateId = 'billinginfo-creditcardshortform';
                break;
        }
    }
    var template = $('#' + templateId).html();
    var html = Mustache.to_html(template, result.billing);
    $('#billing-info').html(html);
    $('#billing-info').removeClass('loading-bg');   

}

function closeOverLayPSS() {
    $('#closeOverlayPSS').lightbox_me();
    $('#closeOverlayPSS').trigger('close');
}

function GetBillingInfo(siteID, memberID) {
    $.ajax({
        type: "GET",
        url: '/MemberAPI/GetBillingInfo?siteID=' + siteID + '&memberID=' + memberID,
        dataType: 'html',
        success: function(result) {
            $('#billing-info').html(result);
            $('#billing-info').removeClass('loading-bg');           
        }
    });

}

function GetBillingInfoForFreeTrial(userPaymentGUID, siteID, memberID) {
    $.ajax({
        type: "GET",
        url: '/MemberAPI/BillingInfoForFreeTrial?userPaymentGUID=' + userPaymentGUID + '&siteID=' + siteID + '&memberID=' + memberID,
        dataType: 'html',
        success: function (result) {
            $('#billing-info').html(result);
            $('#billing-info').removeClass('loading-bg');
        }
    });

}


function ArchivePaymentProfile(siteId, memberId, paymentProfileId) {
    
    $.ajax({
        type: "POST",
        url: '/MemberAPI/ArchivePaymentProfile?siteID=' + siteId + '&memberID=' + memberId,
        data: { PaymentProfileId: paymentProfileId },
        success: function (result) {
            $('#billing-info').html(result);
            $('#billing-info').removeClass('loading-bg');
        }
    });
}

function initTabs() {

    $(".transaction-history-item")
        .tabs({ spinner: 'Retrieving data...' });
       // .removeClass("ui-corner-all ui-corner-bottom")
  		//.addClass("ui-corner-top");

//    $(".tabs-bottom .ui-tabs-nav, .tabs-bottom .ui-tabs-nav > *")
// 		.removeClass("ui-corner-all ui-corner-top")
//  		.addClass("ui-corner-bottom");
}




/* Generic functions */
function alertMessage(message, status, target) {
    var target = target || null;
    if ($('#alertMessage').length > 0) {
        $('#alertMessage').remove();
    }
    
    $('<div id="alertMessage"><p></p></div>').appendTo("body");
    
    if (status == 2) {
        $('#alertMessage').removeClass('error'); // remove 'error' class on success
        $('.overlay:visible').hide('slide', { direction: 'down' }, 300);
    } else {
        $('#alertMessage').addClass('error');
    }

    if(target) showHideLoadingAnimation(target, 'hide');
    $('#alertMessage').hide();
    $('#alertMessage p').html(message);
    $('#alertMessage')
            .css({top:$(window).scrollTop() + 15})
            .fadeIn(300)
            .delay(6000)
            .fadeOut(300)
}

function ieSafePreventDefault(e) {
    if (e.preventDefault) {
        e.preventDefault();
    } else {
        e.returnValue = false;
    }
}

function popmeup(url, width, height) {
    windowHeight = height || 780;
    windowWidth = width || 790;
    var newwindow = window.open(url, 'popWindow', 'height=' + windowHeight + ',width=' + windowWidth + ',resizable=yes,scrollbars=yes');
    if (window.focus) {
        newwindow.focus()
    }
}

function popmeupWithAddressBar(url, width, height) {
    windowHeight = height || 780;
    windowWidth = width || 790;
    var newwindow = window.open(url, 'popWindow', 'height=' + windowHeight + ',width=' + windowWidth + ',resizable=yes,scrollbars=yes,location=yes');
    if (window.focus) {
        newwindow.focus();
    }
}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

function showHideLoadingAnimation(target, action) {
    var targetWidth = $(target).width();
    var loadingMargins = (targetWidth - 20) / 2;
    var loadingPosition = $(target).offset();

    if ($('.ajax-loader').length == 0) {
        var loaderGraphic = $('<img src="/Images/ajax-loader.gif" class="ajax-loader" />');
        $(loaderGraphic).appendTo('body');
    } else {
        var loaderGraphic = $('.ajax-loader');
    }

    if (action == "show") {
        $(target).hide(); // Hide the button
        var marginLeft = loadingPosition.left + loadingMargins; // Center icon in the space where the button was
        $(loaderGraphic)
            .css({ position: "absolute", left: marginLeft, top: loadingPosition.top, marginRight:loadingMargins})
            .show();
    } else {
        $(target).show();
        $(loaderGraphic).hide();
    }
}

function getTimeStamp() {
    var a_p = "";

    var rightNow = new Date();
    var curr_date = rightNow.getDate();
    var curr_month = rightNow.getMonth();
    curr_month++;
    var curr_year = rightNow.getFullYear();
    var curr_hour = rightNow.getHours();
    var curr_min = rightNow.getMinutes();
    var curr_sec = rightNow.getSeconds();
    if (curr_sec < 10) curr_sec = String("0" + curr_sec);
    
    if (curr_hour < 12) {
        a_p = "AM";
    }
    else {
        a_p = "PM";
    }
    if (curr_hour == 0) {
        curr_hour = 12;
    }
    if (curr_hour > 12) {
        curr_hour = curr_hour - 12;
    }

    var currentDateTime = curr_month + "/" + curr_date + "/" + curr_year + " " + curr_hour + ":" + curr_min + ":" + curr_sec + " " + a_p;
    return currentDateTime;
}

function clearFormElements(ele) {
    $(ele).find(':input').each(function() {
        switch (this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'text':
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
        }
    });
}


function delPhoto(photo_id, member_id) {
    confirm(photo_id, member_id);
}

function confirm(photo_id, member_id) {
    $('#confirm').modal({
        closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
        position: ["20%", ],
        overlayId: 'confirm-overlay',
        containerId: 'confirm-container',
        onShow: function(dialog) {
            var modal = this;

            $('.message', dialog.data[0]).append("Are you sure you want to delete this photo?");

            // if the user clicks "yes"
            $('.yes', dialog.data[0]).click(function() {

                deletePhotoQueueItem(photo_id, member_id);
                modal.close(); 
            });
        }
    });
}

function getAge(dateString) {
    var today = new Date(),
        birthDate = new Date(dateString),
        age = today.getFullYear() - birthDate.getFullYear(),
        m = today.getMonth() - birthDate.getMonth();

    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }

    return age;
}
//Use the next 3 functions if you have a need for some sort of popup in overlay fashion.
//The caller of these functions needs to maintain the popupStatus variable and pass it each time.
function loadPopup(popupDivID, popupBackgroundDivID, popupStatus) {
    //loads popup only if it is disabled
    if (popupStatus == 0) {
        $(popupBackgroundDivID).css({
            "opacity": "0.7"
        });
        $(popupBackgroundDivID).fadeIn("slow");
        $(popupDivID).fadeIn("slow");
        popupStatus = 1;
    }
    return popupStatus;
}

function disablePopup(popupDivID, popupBackgroundDivID, popupStatus) {
    //disables popup only if it is enabled
    if (popupStatus == 1) {
        $(popupBackgroundDivID).fadeOut("slow");
        $(popupDivID).fadeOut("slow");
        popupStatus = 0;
    }
    return popupStatus;
}

function disablePopupfast(popupDivID, popupBackgroundDivID, popupStatus) {
    //disables popup only if it is enabled
    if (popupStatus == 1) {
        $(popupBackgroundDivID).fadeOut("fast");
        $(popupDivID).fadeOut("fast");
        popupStatus = 0;
    }
    return popupStatus;
}



function centerPopup(popupDivID, popupBackgroundDivID) {
    //request data for centering
    var windowWidth = document.documentElement.clientWidth;
    var windowHeight = document.documentElement.clientHeight;
    var popupHeight = $(popupDivID).height();
    var popupWidth = $(popupDivID).width();
    //centering
    $(popupDivID).css({
        "position": "absolute",
        "top": windowHeight / 2 - popupHeight / 2,
        "left": windowWidth / 2 - popupWidth / 2
    });
    //only need force for IE6

    $(popupBackgroundDivID).css({
        "height": windowHeight
    });

}

function BuildDropdownOptions(listItems, dropdownToAppendTo, insertBlank) {
    // wipe everything
    dropdownToAppendTo.html("");

    // insert blank as the first choice if wanted
    if (insertBlank) {
        var blank = new Option("", "");
        $(blank).html("");
        $(dropdownToAppendTo).append(blank);
    }

    for (i = 0; i < listItems.length; i++) {
        var o = new Option(listItems[i].Text, listItems[i].Value);
        $(o).html(listItems[i].Text);
        $(dropdownToAppendTo).append(o);
    }

}


function GetPlan(brandID, planID) {
     if (parseInt(planID) > 0) {
        $.ajax({
            type: "POST",
            url: "/SubAdminAPI/GetPlan/" + brandID + "/" + planID,
            data: { brandID: brandID, planID: planID }, //Dummy data so the server doesn't throw a 411 error
            success: function (result) {

                var thehtml = "<ul style=''>";

                thehtml += "<li class='list-item-edit'><label><b>Plan ID:</label><span style='padding-left:5px;'>" + planID + "</b><span/></span></li>";
                thehtml += "<li class='list-item-edit'><label>Basic Cost:</label><span style='padding-left:5px;'>" + result.BasicCost + "</span></li>";
                thehtml += "<li class='list-item-edit'><label>Renew Cost</label><span style='padding-left:5px;'>" + result.RenewCost + "</span></li>";
                thehtml += "<li class='list-item-edit'><label>Plan Type:</label><span style='padding-left:5px;'>" + result.PlanTypeDescription + "</span></li>";
                thehtml += "<li class='list-item-edit'><label>Initial Duration</label><span style='padding-left:5px;'>" + result.InitialDuration + "</span></li>";
                thehtml += "<li class='list-item-edit'><label>Renew Duration</label><span style='padding-left:5px;'>" + result.RenewDuration + "</span></li>";
                thehtml += "<li class='list-item-edit'><label>Plan Type</label><span style='padding-left:5px;'>" + result.PlanType + "</span></li>";

                thehtml += "</ul>";

                $("#planDesc").html(thehtml);
            }
        });
    }
    else {
        $("#planDesc").html('');
    }

}
