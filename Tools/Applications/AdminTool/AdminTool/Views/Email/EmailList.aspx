﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Email.EmailList>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<%--EmailList Title--%>
    Spark Networks Web Admin - Email List
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">

  //SETTING UP OUR POPUP
  //0 means disabled; 1 means enabled;
  var emailListPopupStatus = 0;

  //User clicked on a message to view
  function LoadEmailMessage(memberID, messageID, siteID) {

    // $("#popupEmailMessage").find("#emailMessageArea").html(memberID + "____" + messageID + "____" + siteID);
      $.ajax({
          type: "POST",
          url: "/EmailAPI/RetrieveEmailMessage/" + memberID + "/" + siteID,
          data: { MessageID: messageID },
          success: function (result) {
              if (result.Status == 2) {
                  $("#popupEmailMessage").find("#emailMessageArea").html(result.HTML);
              }
              else {
                  $("#popupEmailMessage").find("#emailMessageArea").html(result.StatusMessage);
              }
          }
      });

    //centering with css
    //centerPopup("#popupEmailMessage", "#backgroundPopup");
    //load popup
    emailListPopupStatus = loadPopup("#popupEmailMessage", "#backgroundPopup", emailListPopupStatus);
  }

  $(document).ready(function() {

      // hide imhistory
      $('#imhistoryContent').hide();

      //Load individual email message 
      $("div.view-message-click-1").click(function() {
          var messageID = $(this).find("input:hidden").val();
          var memberID = $("#memberID").val();
          var siteID = $("#siteID").val();
          LoadEmailMessage(memberID, messageID, siteID);
      });

      //FolderID dropdown changes, then we need to refresh the list of emails
      $("span.folder-selection").change(function() {
          var folderID = $(this).find("select option:selected").val();

          if(folderID != 3)
          {
              $('#title').html('Email activity');
              $('#emailContent').show();
              $('#imhistoryContent').hide();
              

              var memberID = $("#memberID").val();
              var siteID = $("#siteID").val();
              var theNewLocation = "/Email/List/" + siteID + "/" + memberID + "/" + folderID + "/0";
              window.location.href=theNewLocation;
          }
          else if(folderID == 3)
          {
              $('#title').html('Instant message activity');
              $('#imhistoryContent').show();
              $('#emailContent').hide();
              if (!spark.imHistoryManager.initialized) {
                  $(function () {
                      spark.messageconversion.messageAttachmentURLPath = '<%=Model.JSMessageAttachmentURLPath%>';
                      spark.imHistoryManager.initialize();
                      spark.imHistoryManager.initialized = true;

                  });
              }
          }

          
      });

      //Click the x event!
      $("#popupClose").click(function() {
      emailListPopupStatus = disablePopupfast("#popupEmailMessage", "#backgroundPopup", emailListPopupStatus);
      });

      //Click out event!
      $("#backgroundPopup").click(function() {
      emailListPopupStatus = disablePopupfast("#popupEmailMessage", "#backgroundPopup", emailListPopupStatus);
      });

  });
</script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Content/imHistory.css")%>" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%--Hidden input to hold some required parameters--%>
<input type="hidden" id="memberID" value="<%= Model.MemberID %>" />
<input type="hidden" id="siteID" value="<%= Model.SiteID.ToString() %>" />

<div style="width:100%;text-align:center;margin:5px;padding-top:10px;">
 <span id="title">Email activity</span> for <a href="mailto:<%=Model.EmailAddress %>"><%=Model.EmailAddress %></a>: 
 <span class="folder-selection">
 <%= Html.DropDownListFor(m => m.FolderID, Model.FolderIDChoices)%>
 </span>
</div>

<%if (Model.TotalReturnedInPage > 0)
  { %>

<div id="emailContent">
<table id="hor-minimalist-b">
<thead>
<tr>
   <th scope="col">View</th>
   <th scope="col"></th>
   <th scope="col">Date</th>
   <th scope="col">From</th>
   <th scope="col">To</th> 
</tr>
</thead>
<tbody>
<% foreach (var item in Model.Emails)
   { %>
<tr>
   <td>
        <div class="view-message-click-1"><a>View&nbsp;<%=item.ViewText%></a><input type="hidden" value="<%=item.MessageID %>" /></div>
   </td>
  <td>
    <% if (item.IsVIP) { %> *AA* <% } %>
  </td>
  <td>
    <%=item.MessageDate%>
  </td>
  <td>
    <a href="javascript:window.open('http://<%= Model.g.DefaultHost ?? "www" %>.jdate.com/Applications/MemberProfile/ViewProfile.aspx?MemberID=<%= item.FromMemberID %>&impmid=<%= item.FromMemberID %>&imppid=<%= Model.BrandID %>&EntryPoint=8&LayoutTemplateID=<%= Model.GetTemplateID() %>&admin=true','','width=1000,height=550,scrollbars=yes');void('');" class="more-link"><%=item.FromMemberString%></a>
  </td>
  <td>
    <a href="javascript:window.open('http://<%= Model.g.DefaultHost ?? "www" %>.jdate.com/Applications/MemberProfile/ViewProfile.aspx?MemberID=<%= item.ToMemberID %>&impmid=<%= item.ToMemberID %>&imppid=<%= Model.BrandID %>&EntryPoint=8&LayoutTemplateID=<%= Model.GetTemplateID() %>&admin=true','','width=1000,height=550,scrollbars=yes');void('');" styleClass="buttonLink"><%=item.ToMemberString%></a>
  </td>
</tr>
<% } %>
</tbody>
</table>
 
<div id="tnt_pagination" style="padding-top:10px;">
<% for (int i = 0; i < Model.TotalPageCount; i++)
   {
   if (i * Model.PageSize == Model.RowIndex)
       {
         %>
            <span class="disabled_tnt_pagination"><%= (i + 1)%></span>
        <% 
       }
    else
       {
        %>
            <span><a  href="/Email/List/<%= ((int)(Model.MemberSiteInfo.SiteID)) %>/<%= Model.MemberID %>/<%= Model.FolderID %>/<%= i * Model.PageSize %>"><%= (i + 1)%></a> </span>
        <% 
      }
   }
 %>
<span class="hor-minimalist-label">Showing: <%=Model.RowIndex + 1%>&nbsp;to&nbsp;<%=Model.RowIndex + Model.TotalReturnedInPage%>&nbsp;of&nbsp;<%=Model.TotalEmailCount%></span>
</div>
 <br /><br /><br />
 <div id="popupEmailMessage" style="width:70%; height:150px; overflow:auto; margin-left:100px; margin-top:5px;">
  <a id="popupClose">x</a>
  <p id="emailMessageArea"></p>
</div>

<% }
  else
  { %>
  No email to view
<%} %>
</div>


<div id="imhistoryContent">
<div id="listCont" >
    <ul class="imHistory">
        <li>View</li>
        <li>Date</li>
        <li>From</li>
        <li>To</li>
    </ul>
    <ul id="standardList">
     
   
    </ul>
</div>

<!--messages-->
<div id="messageCont" class="message-cont">
    <div id="chatPopUp">
        <div id="message_pagination"></div>
        <ul id="chatterMessages" class="chat_messages body custom-scroll"></ul>
        <div id="message_pagination2"></div>
    </div>
</div>

<!-- HELPER TEMPLATES START -->

<!-- member list item template-->
<div id="memberlistItemTemplate" style="display:none;">
        <li><div><a id="{{TargetMemberID}}" class="view-message-click-1">{{MailType}}</a><input type="hidden" value="{{ID}}" /></div></li>
        <li>{{LastMessageDateString}}</li>
        <li><a href="javascript:window.open('http://<%= Model.g.DefaultHost ?? "www" %>.jdate.com/Applications/MemberProfile/ViewProfile.aspx?MemberID={{FromMemberId}}&impmid={{FromMemberId}}&imppid=<%= Model.BrandID %>&EntryPoint=8&LayoutTemplateID=<%= Model.GetTemplateID() %>&admin=true','','width=1000,height=550,scrollbars=yes');void('');" class="more-link">{{FromMemberId}}</a></li>
    <li><a href="javascript:window.open('http://<%= Model.g.DefaultHost ?? "www" %>.jdate.com/Applications/MemberProfile/ViewProfile.aspx?MemberID={{ToMemberId}}&impmid={{ToMemberId}}&imppid=<%= Model.BrandID %>&EntryPoint=8&LayoutTemplateID=<%= Model.GetTemplateID() %>&admin=true','','width=1000,height=550,scrollbars=yes');void('');" class="more-link">{{ToMemberId}}</a></li>
</div>
<!-- end of member item template -->


<!-- Message templates -->
<div id="chatMessageMeContainer" style="display:none;">
    <li class="chat_message me" id="chat_message_me">
        <div class="bubble_and_profile clearfix">
            <div class="profile">
                <span class="timestamp">{{InsertDate}}</span>
            </div>
            <p class="chat_bubble bc-17">
                {{{Body}}}
            </p>
        </div>
    </li>
</div>

<div id="chatMessageOtherContainer" style="display:none;">
    <li class="chat_message other" id="chat_message_other">
        <div class="bubble_and_profile clearfix">
            <div class="profile">
                <span class="timestamp">{{InsertDate}}</span>
            </div>
            <p class="chat_bubble">
                {{{Body}}}
            </p>
        </div>
    </li>
</div>

<!-- End of Message templates -->

<!-- HELPER TEMPLATES END -->

<%--javascript im history manager--%>
<script src="<%= ResolveUrl("~/Scripts/mustache.min.js")%>" type="text/javascript"></script>
<script src="<%= ResolveUrl("~/Scripts/spark.messageconversion.js")%>" type="text/javascript"></script>
<script src="<%= ResolveUrl("~/Scripts/jquery.lightbox_me.js")%>" type="text/javascript" ></script>
    
<script type="text/javascript">
    var spark = spark || {};
    spark.imHistoryManager = spark.imHistoryManager ||  {
        initialized: false,
        isBlocked:  false,
        brandID: <%=Model.BrandID%>,
        memberID: <%=Model.MemberID%>,
        noPhotoMale: '',
        noPhotoFemale: '',
        currentTargetMemberID: 0,
        previousTargetMemberID: 0,
        memberListStartRow: 1,
        memberListPageSize: 1000,
        memberListTotalResultCount: 0,
        memberListUpdateInProgress: false,
        membersList: [],
        
        memberListObjTemplate: null,
        memberListTarget : null,
        memberListContainer : null,
        memberListTargetUiReady : false,

        memberProfileTemplate : null,
        memberProfileTarget: null,
        memberProfileContainer: null,
        memberProfileTargetUiReady : false,

        memberMyMessageTemplate : null,
        memberOtherMessageTemplate : null,
        memberMessageTarget: null,
        memberMessageContainer: null,
        memberMessageTargetUiReady: false,

        mainMessageContainer: null,

        messageListStartRow: 1,
        messageListPageSize: 50,
        messageListTotalResultCount: 0,
        messageListUpdateInProgress: false,
        messgeListInitialLoad:false,
        messagesList: null,
        initialize: function () {
            
            //initialze manager
            this.setUpTemplatePointers();

            //load member list
            if (this.membersList.length <= 0) {
                //data was not prefetched so go get data
                this.loadConversationMembersAPI();
            }

        },
        onConversationMemberClick: function(targetMemberID){
                this.log('onCnversationMemberClick ' + targetMemberID);
                //set selected target memberID
                this.previousTargetMemberID = this.currentTargetMemberID;
                this.currentTargetMemberID = targetMemberID;

                //reset and clear existing conversation messages
                this.messageListStartRow = 1;
                spark.imHistoryManager.messgeListInitialLoad = true;
                this.memberMessageTarget.empty();

                //get and load messages
                this.loadConversationMessages();

                //reset message ui status
                this.memberMessageTargetUiReady = false;

                

            
        },
        onConversationMembersNextPage: function(){
            this.memberListStartRow = this.memberListStartRow + this.memberListPageSize;
            this.loadConversationMembers();
        },
        onConversationMembersMessageNextPage: function(currentLink){
            this.messageListStartRow = currentLink;
            this.log('onConversationMESSAGENextPage from scrolling - new message startRow: ' + currentLink);
            this.loadConversationMessages();
        },
        loadConversationMembersAPI: function() {  
            
            //retrieve members list
            if (this.memberListStartRow <= 1 || this.memberListStartRow <= this.memberListTotalResultCount){
                if (!this.memberListUpdateInProgress) {

                    var existingStartRow = this.memberListStartRow;
                    if (this.memberListStartRow <= 0){
                        this.memberListStartRow = 1;
                    }
                    
                    $.ajax({
                        type: "GET",
                        url: "/EmailAPI/GetImHistoryList?memberID="+spark.imHistoryManager.memberID + "&brandID=" +spark.imHistoryManager.brandID,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            spark.imHistoryManager.memberListUpdateInProgress = true;
                            
                        },
                        complete: function () {
                            spark.imHistoryManager.memberListUpdateInProgress = false;
                        },
                        success: function (result) {
                          
                            var InstantMessageHistoryMembersResponse =  result;

                            if (result.code != 200) {
                                spark.imHistoryManager.log('Code: ' + result.code);
                          
                            }
                            else {

                                spark.imHistoryManager.log('Total found: ' + InstantMessageHistoryMembersResponse.MatchesFound + ', Batch returned: ' + InstantMessageHistoryMembersResponse.MemberList.length);

                                //load members
                                var existingTotalResultCount = spark.imHistoryManager.memberListTotalResultCount;
                                spark.imHistoryManager.memberListTotalResultCount = InstantMessageHistoryMembersResponse.MatchesFound;
                                if (InstantMessageHistoryMembersResponse.MemberList.length > 0) {

                                    var batchMemberList = [];
                                    var targetMemberID = 0;
                                    for (var i = 0; i < InstantMessageHistoryMembersResponse.MemberList.length; i++) {

                                        if(spark.imHistoryManager.memberID == InstantMessageHistoryMembersResponse.MemberList[i].ToMemberId)
                                            targetMemberID = InstantMessageHistoryMembersResponse.MemberList[i].FromMemberId
                                        else
                                            targetMemberID = InstantMessageHistoryMembersResponse.MemberList[i].ToMemberId;

                                        console.log(spark.imHistoryManager.memberID  + "    " +   targetMemberID);

                                        var username = InstantMessageHistoryMembersResponse.MemberList[i].Username;
                                        
                                        if(username == null)
                                            username = InstantMessageHistoryMembersResponse.MemberList[i].MemberId;

                                        var dtoMemberListItem = {
                                            ID: InstantMessageHistoryMembersResponse.MemberList[i].Id,
                                            ToMemberId: InstantMessageHistoryMembersResponse.MemberList[i].ToMemberId,
                                            FromMemberId: InstantMessageHistoryMembersResponse.MemberList[i].FromMemberId,
                                            UserName: username,
                                            LastMessageDateString: spark.imHistoryManager.getDateDisplay(new Date(InstantMessageHistoryMembersResponse.MemberList[i].InsertDate)),
                                            MailType: InstantMessageHistoryMembersResponse.MemberList[i].MailType,
                                            TargetMemberID: targetMemberID
                                        };

                                        spark.imHistoryManager.membersList.push(InstantMessageHistoryMembersResponse.MemberList[i].MemberId);
                                        batchMemberList.push(dtoMemberListItem);
                                    }

                                    
                                    //render members
                                    spark.imHistoryManager.log('Rendering members page ' + spark.imHistoryManager.getCurrentMemberListPageNumber());
                                    spark.imHistoryManager.renderConversationMembers(batchMemberList);  
                                }
                                else{
                                    //no results or no more results
                                    spark.imHistoryManager.log('No more member results found');
                                    if (spark.imHistoryManager.membersList.length <= 0){
                                        spark.imHistoryManager.memberListTarget.append("<li>No buddies found for YOU, maybe you should go make some.</li>");
                                    }
                                }
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            spark.imHistoryManager.log('Error getting member list: ' + textStatus);
                            spark.imHistoryManager.memberListStartRow = existingStartRow;
                       
                            
                        }
                    });
                }
            }
            else{
                //no more results
                this.log('Already on last member page');
              
            }

        },
        loadConversationMessages: function () { //Note: This loads via Bedrock asmx
            //retrieve members list
            if (this.messageListStartRow <= 1 || this.messageListStartRow <= this.messageListTotalResultCount){
                if (!this.messageListUpdateInProgress) {

                    var existingStartRow = this.messageListStartRow;
                    if (this.messageListStartRow <= 0)
                    {
                        this.messageListStartRow = 1;
                    }
                    
                    var ignoreCache = false;

                    if(this.messageListPageSize == 1)
                        ignoreCache = true;


                    $.ajax({
                        type: "GET",
                        url: "/EmailAPI/GetImHistoryMessageList?memberID="+spark.imHistoryManager.memberID+"&targetMemberID="+spark.imHistoryManager.currentTargetMemberID+"&brandID="+spark.imHistoryManager.brandID+"&pageNumber="+spark.imHistoryManager.messageListStartRow+"&pageSize="+spark.imHistoryManager.messageListPageSize+"&ignoreCache="+ignoreCache,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            spark.imHistoryManager.messageListUpdateInProgress = true;
                            
                        },
                        complete: function () {
                            spark.imHistoryManager.messageListUpdateInProgress = false;
                        },
                        success: function (result) {
                            var imHistoryMessageListResult = result;
                            if (imHistoryMessageListResult.code != 200) {
                                spark.imHistoryManager.log('StatusID: ' + imHistoryMessageListResult.code);
                             }
                            else {

                                spark.imHistoryManager.log('Total found: ' + imHistoryMessageListResult.MatchesFound + ', Batch returned: ' + imHistoryMessageListResult.MatchesFound);

                                //load messages
                                var existingTotalResultCount = spark.imHistoryManager.messageListTotalResultCount;
                                spark.imHistoryManager.messageListTotalResultCount = imHistoryMessageListResult.MatchesFound;
                                if (imHistoryMessageListResult.MatchesFound > 0) {
                                    //render messages
                                    spark.imHistoryManager.log('Rendering messages page ' + spark.imHistoryManager.getCurrentMessageListPageNumber());
                                    spark.imHistoryManager.renderConversationMessages(imHistoryMessageListResult);
                                }
                                else{
                                    //no results or no more results
                                    spark.imHistoryManager.log('No more results found');
                                }
                            }

                             
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            spark.imHistoryManager.log('Error getting next batch: ' + textStatus);
                            spark.imHistoryManager.messageListStartRow = existingStartRow;
                         }
                    });
                }
            }
            else{
                //no more results
                this.log('Already on last page');
            }

        },
        renderConversationMembers: function (IMHistoryMemberList) {  
            var htmlForAll = '';

            if (IMHistoryMemberList.length > 0) {
                IMHistoryMemberList.forEach(function (entry) {
                    
                    var html = Mustache.to_html(spark.imHistoryManager.memberListObjTemplate, entry);

                    htmlForAll += html; // reduce DOM rendering
                });

                spark.imHistoryManager.memberListTarget.append(htmlForAll);

                if(!spark.imHistoryManager.memberListTargetUiReady){
                    spark.imHistoryManager.uiBindingMember();
                }

               

            } else {
                this.log('WARNING Empty IMHistoryMemberList sent');
            }
                
        },
        renderConversationMessages: function (imHistoryMessageListResult) {
            //render list of messages for selected member via templating

            if (imHistoryMessageListResult != null && imHistoryMessageListResult.MatchesFound > 0) {
                

                this.renderConversationMessages2(imHistoryMessageListResult.MessageList);
            } else {
                this.log('WARNING Empty imHistoryMessageListResult sent');
            }   

        },
        renderConversationMessages2: function(IMHistoryMessageList) {
            
            if(IMHistoryMessageList.length > 0) {

                //let blank this target out 
                spark.imHistoryManager.memberMessageTarget.html('');

                    IMHistoryMessageList.forEach(function (entry) {
                        //spark.imHistoryManager.log(entry);
                        entry.Body = spark.messageconversion.convertCustomTagToHtml(entry.Body);  //clean up the message 
 
                    
                        var htmlForMessages = null;
                        //alert(entry.MemberID + "   " + spark.imHistoryManager.memberID);
                        if(entry.SenderId == spark.imHistoryManager.memberID){
                            htmlForMessages = Mustache.to_html(spark.imHistoryManager.memberMyMessageTemplate, entry);
                        } else {
                            htmlForMessages = Mustache.to_html(spark.imHistoryManager.memberOtherMessageTemplate, entry);
                        }

                        spark.imHistoryManager.memberMessageTarget.prepend(htmlForMessages);
                    
                    });

                    spark.imHistoryManager.uiBindingMessage();
                    this.renderMessagePagination();
                
                    if(spark.imHistoryManager.messgeListInitialLoad == true)
                    {
                        spark.imHistoryManager.messgeListInitialLoad = false;

                        $("#chatPopUp").lightbox_me({
                            centered: false,
                            overlayCSS :{background: '#FFFFFF', opacity: .9},
                        });
                    }
                    
                
                }

        },
        setUpTemplatePointers: function() {

            this.memberListObjTemplate = $('#memberlistItemTemplate').html();
            this.memberListTarget = $('#standardList');
            this.memberListContainer = $("#listCont");

            this.memberProfileTarget = $('#chatterMiniProfile');
            
            this.memberMyMessageTemplate = $('#chatMessageMeContainer').html();
            this.memberOtherMessageTemplate = $('#chatMessageOtherContainer').html();
            this.memberMessageTarget = $('#chatterMessages');

            this.mainMessageContainer =  $('#messageCont');

        },
        uiBindingMember: function(){
            if(!spark.imHistoryManager.memberListTargetUiReady){
                var memberList = spark.imHistoryManager.memberListTarget.find('.view-message-click-1');
                
                $(".view-message-click-1" ).each(function( index ) {

                    $(this).click(function(e){
                        spark.imHistoryManager.onConversationMemberClick($(this).attr('id'));
                    });
                });

                spark.imHistoryManager.memberListTargetUiReady = true;
            }
        },
        uiBindingMessage: function(){
            var messageList = spark.imHistoryManager.memberMessageTarget,
                numBottom = messageList[0].scrollHeight,
                numPage = messageList[0].scrollHeight / spark.imHistoryManager.getCurrentMessageListPageNumber();

            if(!spark.imHistoryManager.memberMessageTargetUiReady){
                // scroll to the bottom of the list at initial load
                messageList.scrollTop(numBottom);
                spark.imHistoryManager.memberMessageTargetUiReady = true;
            } else {
                // scroll to the top of the last messages
                messageList.scrollTop(numPage);
            }
        },
        log: function(message) {
            if (typeof(console) !== "undefined") {
                console.log(message);
            }
        },
        getCurrentMemberListPageNumber: function() {
            var currentPage = 1;
            if (this.membersList.length > this.memberListPageSize){
                currentPage = Math.ceil(this.membersList.length / this.memberListPageSize);
            }
            
            return currentPage;
        },
        getCurrentMessageListPageNumber: function(){
            var currentPage = 1;
            if ((this.messageListStartRow +  this.messageListPageSize - 1) > this.messageListPageSize){
                currentPage = Math.ceil((this.messageListStartRow +  this.messageListPageSize - 1) / this.messageListPageSize);
            }
            return currentPage;
        },
        getDateDisplay: function (date) {

            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;

        },
        renderMessagePagination: function(){
            
            $("#message_pagination").hide();
            $("#message_pagination2").hide();

            if(this.messageListTotalResultCount > this.messageListPageSize)
            {
                var number_of_pages = Math.ceil(this.messageListTotalResultCount/this.messageListPageSize);  
                var current_link = 1;  
                var navigation_html = "";
                while(number_of_pages >= current_link){  
                    navigation_html += '<span><a href="javascript:spark.imHistoryManager.onConversationMembersMessageNextPage('+current_link+')">'+current_link+'</a></span>';  
                    current_link++;  
                }  

                $("#message_pagination").html(navigation_html);
                $("#message_pagination2").html(navigation_html);
                

                $("#message_pagination").show();
                $("#message_pagination2").show();
            }
            
        }
    }

    </script>

</div>
</asp:Content>




