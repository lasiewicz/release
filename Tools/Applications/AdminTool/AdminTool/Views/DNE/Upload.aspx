﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.DNE.Search>" %>

<html>
<head>
      <script src="<%= ResolveUrl("~/Scripts/jquery-1.4.2.min.js")%>" type="text/javascript"></script>
      <%--<script src="<%= ResolveUrl("~/Scripts/jquery-2.0.3.min.js")%>" type="text/javascript"></script>
     <script src="<%= ResolveUrl("~/Scripts/jquery-migrate-1.2.1-min.js")%>" type="text/javascript"></script>--%>
</head>
<title>
    Spark Networks Web Admin - DNE Upload
</title>
<style>
   body { font-size:11px; }
</style>
<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        $('#btnUpload').click(function () {
            if ($('input:file').val().length == 0) {
                alert('Please select a file to upload');
                return false;
            }
            return true;
        });
    });
</script>
<body>
<% using (Html.BeginForm("Upload", "DNE", FormMethod.Post, new { enctype = "multipart/form-data" }))
   { %>
        <input type="file" name="dneFile" />
        <br/><br/>
        <%=Html.DropDownListFor(m => m.SiteId, Model.Sites)%>
        <br/><br/>
        <input id="btnUpload" type="submit" class="button" value="Import" />
        <br/><br/>
        <% if (Model.EmailUploadCount > 0)
            { %>
            <%=Model.EmailUploadCount + " records added."%>
        <% } %>
<% } %>
</body>
</html>
