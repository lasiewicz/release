﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.DNE.Search>" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="TitleContent" runat="server">
	Spark Networks Web Admin - DNE Search
</asp:Content>

<asp:Content ID="ContentHead" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="ContentHeader" ContentPlaceHolderID="HeaderContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/TopNav", this.ViewData.Model.TopNav); %>
</asp:Content>

<asp:Content ID="ContentMain" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm("Search", "DNE"))
   {%>
<!-- Search Query -->
<div id="dne-search" >
    Search the list by a full or partial email address.<br /><br />
    <%=Html.TextBoxFor(model => model.EmailAddress)%><br /><br />
    <%=Html.DropDownListFor(m => m.SiteId, Model.Sites)%>
    <input type="submit" class="button" value="Search" name=""/> 
    <br /><br />
<!-- Search Results -->
<%if (Model.TotalRows == 0)
  { %>
    <div class="dne-no-results">No Results found.</div>      
  <% } %>
<% if (Model.SearchResults.Count > 0)
   { %>   
    <table class="dne-results-table">
        <% if (Model.TotalPages > 1)
           { %>
        <tr>
            <td colspan="4" class="dne-results-text"> 
                <% if (Model.PageNumber > 1)
                   { %>
                    <a href="<%=Url.Action("Search", "DNE",
                                 new {pageNumber = Model.PageNumber - 1, siteId = Model.SiteId, email = Model.EmailAddress})%>"><<</a>
                <% } %>
                Results <%=Model.StartRow%> to <%=Model.EndRow%> of <%=Model.TotalRows%>
                <% if (Model.PageNumber < Model.TotalPages)
                   { %>
                    <a href="<%=Url.Action("Search", "DNE",
                                 new {pageNumber = Model.PageNumber + 1, siteId = Model.SiteId, email = Model.EmailAddress})%>">>></a>
                <% } %>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="dne-results-text paging">
                <div>
                Page:
                 <% for (int i = 1; i <= Model.TotalPages; i++)
                    {%>
                    <% if (Model.PageNumber == i)
                       { %>
                        [<%=i.ToString()%>]
                    <% } %>
                    <% else
                       { %>
                    <a href="<%=Url.Action("Search", "DNE",
                                 new {pageNumber = i, siteId = Model.SiteId, email = Model.EmailAddress})%>"><%=i%></a>
                    <% } %>
                 <% } %>
                 </div>
            </td>
        </tr>
        <% } %>
       <tr>
            <td><strong>Email</strong></td>
            <td><strong>MemberID</strong></td>
            <td><strong>Insert Date</strong></td>
        </tr>
    <% foreach (Matchnet.ExternalMail.ValueObjects.DoNotEmail.DoNotEmailEntry entry in Model.SearchResults)
       { %>
        <tr>
            <td> <%=entry.EmailAddress%> </td>
            <td> <%= (entry.MemberID > 0 ? entry.MemberID.ToString() : "None")%> </td>
            <td> <%=entry.InsertDate.ToString("MM/dd/yyyy")%> </td>
            <td><a href="<%= Url.Action("Search","DNE",new { siteId = entry.SiteID, rEmail = entry.EmailAddress, Email = Model.EmailAddress }) %>">Remove</a></td>
        </tr>
    <% } %>
     <% if (Model.TotalPages > 1)
        { %>
        <tr>
            <td colspan="4" class="dne-results-text"> 
                <% if (Model.PageNumber > 1)
                   { %>
                    <a href="<%=Url.Action("Search", "DNE",
                                 new {pageNumber = Model.PageNumber - 1, siteId = Model.SiteId, email = Model.EmailAddress})%>"><<</a>
                <% } %>
                Results <%=Model.StartRow%> to <%=Model.EndRow%> of <%=Model.TotalRows%>
                <% if (Model.PageNumber < Model.TotalPages)
                   { %>
                    <a href="<%=Url.Action("Search", "DNE",
                                 new {pageNumber = Model.PageNumber + 1, siteId = Model.SiteId, email = Model.EmailAddress})%>">>></a>
                <% } %>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="dne-results-text paging">
                <div>
                Page:
                 <% for (int i = 1; i <= Model.TotalPages; i++)
                    {%>
                    <% if (Model.PageNumber == i)
                       { %>
                        [<%=i.ToString()%>]
                    <% } %>
                    <% else
                       { %>
                    <a href="<%=Url.Action("Search", "DNE",
                                 new {pageNumber = i, siteId = Model.SiteId, email = Model.EmailAddress})%>"><%=i%></a>
                    <% } %>
                 <% } %>
                 </div>
            </td>
        </tr>
        <% } %>

    </table>
<% } %>

</div>
<% } %>
</asp:Content>