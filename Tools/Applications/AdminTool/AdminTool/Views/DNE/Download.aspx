﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.DNE.Download>" %>
<%@ Import Namespace="Matchnet.ExternalMail.ValueObjects.DoNotEmail" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="TitleContent" runat="server">
	Spark Networks Web Admin - DNE Download
</asp:Content>

<asp:Content ID="ContentHead" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $('#btnDownload').click(function () {
                if ($("input:checked").length == 0) {
                    alert('Please select a site to download');
                    return false;
                }
                return true;
            });
        });
    </script>
</asp:Content>

<asp:Content ID="ContentHeader" ContentPlaceHolderID="HeaderContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/TopNav", this.ViewData.Model.TopNav); %>
</asp:Content>

<asp:Content ID="ContentMain" ContentPlaceHolderID="MainContent" runat="server">
<% if (Model.SiteSummary.Count > 0)
   { %>   
    <table id="dneDownload-table">
        <tr>
            <td><strong>Include in download</strong></td>
            <td><strong>Sites</strong></td>
            <td><strong>Number of emails on file</strong></td>
        </tr>
        <% for (int i = 0; i < Model.SiteSummary.Count;  i++)
{
    var siteSummary = (DoNotEmailSiteSummary)Model.SiteSummary[i];%>
        <tr>
            <td><input type="checkbox" name="dneDownload" value="<%=siteSummary.SiteID.ToString() %>" <%= (Model.IsChecked[i]) ? "checked=\"checked\"" : ""%> /></td>
            <td><%= siteSummary.SiteName %></td>
            <td><%= siteSummary.Count%></td>
        </tr>
    <% } %>
    <tr>
        <td class="dne-download"> <input id="btnDownload" type="submit" class="button" value="Download" name=""/>  </td>
    </tr>
    </table>
<% } %>

</asp:Content>