﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Review.SodbReviewStatsModelView>" %>
<%@ Import Namespace="Spark.SODBQueue.ValueObjects" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Sodb Agent Review Stats
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%Html.BeginForm("ViewSodbReviewStats", "Reports"); %>
<script type="text/javascript">
    $(document).ready(function () {
        $('#StartDate').datepicker({
            changeMonth: true,
            changeYear: true
        });
        $('#EndDate').datepicker({
            changeMonth: true,
            changeYear: true
        });
    });
</script>
<div class="approval_count">
    <br/>
    <h2>Sodb Agent Review Stats</h2>
    <br/>
    <table class="form">
        <tr>
            <td>Start Date:</td>
            <td><%= Html.TextBox("StartDate", "", new { id = "StartDate" })%>
                <%= Html.ValidationMessageFor(model => model.StartDate) %>
             </td>
        </tr>
        <tr>
            <td>End Date:</td>
            <td>
                <%= Html.TextBox("EndDate", "", new { id = "EndDate" })%>
                <%= Html.ValidationMessageFor(model => model.EndDate)%>
            </td>
        </tr>
        <tr>
            <td>Admin Member ID:</td>
            <td>
                <%= Html.TextBoxFor(model => model.AdminMemberId)%>
                <%= Html.ValidationMessageFor(model => model.AdminMemberId)%>
            </td>
        </tr>
        <tr>
            <td>Site:</td>
            <td>
                <%=Html.DropDownListFor(model => model.SiteId, Model.SodbSites)%>
	        </td>
        </tr>
    </table>
    <br/>
    <input type="submit" id="btnRunReport" value="Run Report"/><br /><br />
    <% if (Model.SodbAgentReviewCounts.Count > 0)
   { %>
     <table class="rows">
        <tr style="font-size:12px;color:#ffffff;">
            <th>Admin MemberID</th>
            <th>Email</th>
            <th>Total Processed</th>
            <th>Positive Match</th>
            <th>100% Not a Match</th>
            <th>Request more info (from SSP)</th>
            <th>Request Photo from Member</th>
            <th>Escalate</th>
        </tr>
        <% foreach (SodbAgentReviewCounts record in Model.SodbAgentReviewCounts)
           { %>
            <tr>
                <td><%= record.AdminId%></td>
                <td><%= ReviewManager.GetMemberEmail(record.AdminId)%></td>
                <td><%= (record.PositiveMatchCount + record.NegativeMatchCount + record.RequestSSPCount + record.RequestPhotoCount + record.EscalateCount) %></td>
                <td><%= record.PositiveMatchCount%></td>
                <td><%= record.NegativeMatchCount%></td>
                <td><%= record.RequestSSPCount%></td>
                <td><%= record.RequestPhotoCount%></td>
                <td><%= record.EscalateCount%></td>
            </tr>
        <% } %>
     </table>  
  <% }%>
</div>
<%Html.EndForm(); %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<style>
    input[type=text]
    {
        width:174px;
    }
</style>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
 <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
<%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
