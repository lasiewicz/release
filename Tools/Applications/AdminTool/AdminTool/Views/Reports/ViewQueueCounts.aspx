﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.SharedTopNav>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	ViewQueueCounts
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<script type="text/javascript">
    function GetQueueTotals() {

        var itemType = $("#MainForm input[type='radio']:checked").val();
        try {
            console.log('Calling /ReportsAPI/GetQueueCountTotals?itemType=' + itemType);
        }
        catch (e) {
        }

        $.ajax({
            type: "GET",
            url: '/ReportsAPI/GetQueueCountTotals?itemType=' + itemType,
            dataType: 'html',
            success: function (result) {
                $('#Totals').html(result);
                try {
                    console.log(result);
                }
                catch (e) {
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus);
            }
        });
    }
	
	</script>

<div class="approval_count">
<br />
    <h2>Approval Queue Stats</h2>
    <form id="MainForm" action="" >
    <table class="form">
        <tr>
            <td><input type="radio" name="QueueType" value="<%=QueueItemType.Photo.ToString("d") %>" title="Photo Approval" /></td>
            <td>Photo Approval</td>
        </tr>
        <tr>
            <td><input type="radio" name="QueueType" value="<%=QueueItemType.Text.ToString("d") %>" title="Free Text Approval" /></td>
            <td>Free Text Approval</td>
        </tr>
        <tr>
            <td><input type="radio" name="QueueType" value="<%=QueueItemType.QuestionAnswer.ToString("d") %>" title="Q & A Approval" /></td>
            <td>Q & A Approval</td>
        </tr>
        <tr>
            <td><input type="radio" name="QueueType" value="<%=QueueItemType.FacebookLike.ToString("d") %>" title="Facebook Likes Approval" /></td>
            <td>Facebook Likes Approval</td>
        </tr>
        <tr>
            <td><input type="radio" name="QueueType" value="<%=QueueItemType.MemberProfileDuplicate.ToString("d") %>" title="Similar Text Review" /></td>
            <td>Similar Text Review</td>
        </tr>
        <tr>
            <td><input type="radio" name="QueueType" value="<%=QueueItemType.CrxSecondReview.ToString("d") %>" title="Second Review – Free Text Approval" /></td>
            <td>Second Review – Free Text Approval</td>
        </tr>
        <tr>
            <td><input type="radio" name="QueueType" value="<%=QueueItemType.MemberProfileDuplicateReview.ToString("d") %>" title="Second Review – Similar Text Review" /></td>
            <td>Second Review – Similar Text Review</td>
        </tr>
        <tr>
            <td colspan="2"><input type="button" id="btnRunReport" value="Run Report" onclick="GetQueueTotals();"/></td>
        </tr>
    </table>
    </form>
    <br />
    <div id="Totals"></div>

</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<link href="/Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Member Top navigation--%>
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
