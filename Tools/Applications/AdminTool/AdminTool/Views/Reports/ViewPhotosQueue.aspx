﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<PhotoApprovalModelView>" %>

<asp:Content ID="ContentTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Spark Networks Web Admin - Photos Queue
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../../Content/superfish-members-search.css" rel="stylesheet" type="text/css"/>
</asp:Content>

<asp:Content ID="ContentHeader" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Member Top navigation--%>
    <% Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <% Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function() {
            jQuery('#btnPhotosQueue').click(function() {

                var siteId = jQuery('#SiteID').val();
                if (siteId == 0) {
                    alert('Please select a site');
                    return false;
                }

                var resultDiv = jQuery('.queue_count_result');
                resultDiv.html('');
                resultDiv.addClass('loading-bg');

                jQuery.ajax({
                    type: "GET",
                    url: '/ReportsAPI/GetPhotosQueue?siteID=' + siteId,
                    dataType: 'json',
                    success: function(result) {
                        resultDiv.removeClass('loading-bg');
                        if (result.length > 0) {
                            var htmlResult = '<table class="rows">';
                            htmlResult += '<tr>';
                            htmlResult += '<th>MemberID</th><th>Site</th><th>Insert Date</th><th>Age (hours)</th>';
                            htmlResult += '</tr>';
                            for (var i = 0; i < result.length; i++) {
                                var date = new Date(+result[i].InsertDate.replace(/\/Date\((\d+)\)\//, '$1'));;
                                var insDateStr = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
                                htmlResult += '<tr>';
                                htmlResult += '<td>' + result[i].MemberID + '</td><td>' + result[i].SiteName + '</td><td>' + insDateStr + '</td><td>' + result[i].PhotoAge + '</td>';
                                htmlResult += '</tr>';
                            }
                            htmlResult += '</table>';

                            resultDiv.html(htmlResult);
                        } else {
                            resultDiv.html('Queue is empty.');
                        }
                    }
                });

            });
        });
    </script>

    <% using (Html.BeginForm("ViewPhotosQueue", "Reports"))
       { %>
        <div class="approval_count">
            <br/>
            <h2>Stuck Photo Finder</h2>
            <div style="font-size: 12px; margin-top: 6px;">Select a site and run the report to see the Member ID's with the oldest photos awaiting approval.</div>
            <br/>
            <div>
                <select id="SiteID">
                    <option selected="selected" value="0">-- Select --</option>
                    <option value="101">Spark</option>
                    <option value="15">Cupid</option>
                    <option value="9161">Italian</option>
                    <option value="9041">BBW</option>
                    <option value="9051">BlackSingles</option>
                    <option value="103">JDate.com</option>
                    <option value="4">JDate.co.il</option>
                    <option value="107">JDate.co.uk</option>
                    <option value="105">JDate.fr</option>
                    <option value="9011">AdventistSinglesConnection.com</option>
                    <option value="9071">CatholicMingle.com</option>
                    <option value="9081">ChristianMingle.com</option>
                    <option value="9111">DeafSinglesConnection.com</option>
                    <option value="9191">LDSMingle.com</option>
                    <option value="9221">MilitarySinglesConnection.com</option>
                    <option value="9231">SingleSeniorsMeet.com</option>
                    <option value="9311">DSSingles.com</option>
                </select>
                <input id="btnPhotosQueue" type="button" value="Run Report"/>
            </div>

            <div class="queue_count_result" style="float: left; min-height: 35px; min-width: 35px;">
            </div>
        </div>
    <% } %>
</asp:Content>