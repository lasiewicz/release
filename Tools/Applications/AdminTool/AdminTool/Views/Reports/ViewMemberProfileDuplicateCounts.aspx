﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Review.MemberProfileDuplicateCountsModelView>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	ViewMemberProfileDuplicateCounts
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
    $(function () {
        $('#StartDate').datepicker({
            changeMonth: true,
            changeYear: true
        });
        $('#EndDate').datepicker({
            changeMonth: true,
            changeYear: true
        });
    });

    function GetTotals() {

        var startDate = $('#StartDate').val();
        var endDate = $('#EndDate').val();
        var adminMemberID = $('#AdminMemberID').val();
        var requestTime = new Date().getTime(); //Used to force ajax update and override the browser's cache

        var errorMsg = '';
        errorMsg += (startDate != '') ? '' : 'Please select a valid "Start Date".\n\n';
        errorMsg += (endDate != '') ? '' : 'Please select a valid "End Date".\n\n';
        errorMsg += (adminMemberID == '' || (adminMemberID == parseInt(adminMemberID) && parseInt(adminMemberID) > 0)) ? '' : 'Please select a valid "Admin Member ID".\n\n';

        if (errorMsg != '') {
            alert(errorMsg);
            return;
        }

        $.ajax({
            type: "GET",
            url: '/ReportsAPI/GetMemberProfileDuplicateHistoryCount?startDate=' + startDate + '&endDate=' + endDate + '&adminMemberID=' + adminMemberID + '&requestTime=' + requestTime,
            dataType: 'html',
            success: function (result) {
                $('#Totals').html(result);
            }
        });
    }
    </script>
    
<div class="approval_count">
    <h2>ViewMemberProfileDuplicateCounts</h2>
    <table class="form">
    <tr>
        <td>Start Date:</td>
        <td><input type="text" id="StartDate" name="StartDate" /></td>
    </tr>
    <tr>
        <td>End Date:</td>
        <td><input type="text" id="EndDate" name="EndDate" /></td>
    </tr>
    <tr>
        <td>Admin Member ID:</td>
        <td><input type="text" id="AdminMemberID" name="AdminMemberID" /></td>
    </tr>
    </table>
    <br/>
    <input type="button" id="btnRunReport" value="Run Report" onclick="GetTotals();"/><br /><br />
    <div id="Totals" name="Totals"></div>
</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="/Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
     <%--Member Top navigation--%>
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.SharedTopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
