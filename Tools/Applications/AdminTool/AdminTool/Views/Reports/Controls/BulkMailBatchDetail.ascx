﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Reports.BulkMailBatchModelView>" %>

<%if (Model == null) { %>
    No data available for selected date. 
<%} else {%>
    <h2>Run time: <%= Model.Batch.RunTime.GetHoursMinutesString() %></h2>
    <br /><h2>Totals:</h2>
    <br />
    <table class="rows">
    <tr>
        <td><strong>Status</strong></td>
        <td><strong>Record Count</strong></td>
    </tr>
    <%foreach(var entry in Model.Totals){ %>
        <tr>
            <td><strong><%=entry.Key.ToString() %></strong></td>
            <td><%=entry.Value.ToString() %></td>
        </tr>
    <%} %>
    </table>
    <br />
    <h2>By server:</h2>
    <br />
    <table class="rows">
        <tr>
            <td><strong>Server</strong></td>
            <td><strong>Status</strong></td>
            <td><strong>Record Count</strong></td>
        </tr>
         <%foreach(var entry in Model.GroupedSummaryRecords){ %>
            <tr>
                <td><strong><%=entry.Key %></strong></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <%foreach (var summary in entry.Value){ %>
                <tr>
                    <td>&nbsp;</td>
                    <td><strong><%=summary.ProcessingStatus.ToString() %></strong></td>
                    <td><%=summary.RecordCount.ToString() %></td>
                </tr>
            <%} %>
        <%} %>
     </table>
    
<%} %>