﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ApprovalQueueCountTotalsModelView>" %>

<% if(Model.CountsByCommunity != null && Model.CountsByCommunity.Count > 0){ %>
    <div class="queue_count_result">
        <strong>By Community</strong>
        <table class="rows">
            <tr>
                <th>Community</th>
                <th>Count</th>
                <th>Oldest by Hours</th>
                <th>Average by Hours</th>
            </tr>
            <%foreach(QueueCountRecord record in Model.CountsByCommunity) { %>
                <tr>
                    <td><%=((CommunityIDs)record.GroupID).ToString() %></td>
                    <td><%=record.ItemCount.ToString() %></td>
                    <td><%=record.MaxHours.ToString() %></td>
                    <td><%=record.AverageHours.ToString() %></td>
                </tr>
                <%} %>
        </table>
    </div>
<%} %>
<% if (Model.CountsBySite != null && Model.CountsBySite.Count > 0) { %>
    <div class="queue_count_result">
        <strong>By Site</strong>
        <table class="rows">
            <tr>
                <th>Site</th>
                <th>Count</th>
                <th>Oldest by Hours</th>
                <th>Average by Hours</th>
            </tr>
            <%foreach(QueueCountRecord record in Model.CountsBySite) { %>
                <tr>
                    <td><%=((SiteIDs)record.GroupID).ToString() %></td>
                    <td><%=record.ItemCount.ToString() %></td>
                    <td><%=record.MaxHours.ToString()%></td>
                    <td><%=record.AverageHours.ToString() %></td>
                    
                </tr>
                <%} %>
        </table>
    </div>
<%} %>
<% if (Model.CountsByLanguage != null && Model.CountsByLanguage.Count > 0)
   { %>
   <div class="queue_count_result">
        <strong>By Language</strong>
        <table class="rows">
            <tr>
                <th>Language</th>
                <th>Count</th>
                <%if (Model.ItemType == QueueItemType.MemberProfileDuplicate)
                  {%>
                  <th>Duplicates</th>
                <% } %>
                <th>Oldest</th>
                <th>Average</th>
            </tr>
            <%foreach (QueueCountRecord record in Model.CountsByLanguage) { %>
                <tr>
                    <td><%=((Enums.Language)record.GroupID).ToString() %></td>
                    <td><%=record.ItemCount.ToString() %></td>
                    <%if (Model.ItemType == QueueItemType.MemberProfileDuplicate)
                    {%>
                    <td><%=record.ChildCount %></td>
                    <% } %>
                    <td><%=record.MaxHours.ToString()%></td>
                    <td><%=record.AverageHours.ToString() %></td>
                </tr>
                <%} %>
        </table>
    </div>
<%} %>
<%if (Model.CountsByLanguage == null && Model.CountsBySite == null && Model.CountsByCommunity == null) { %>
<p class="no_more_item">Queue is empty</p>
<%} %>