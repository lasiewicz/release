﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ApprovalCountTotalsModelView>" %>

<table class="totals">
    <tr>
        <th># Free Text Items:</th>
        <td><%=Model.TotalCountRegularText.ToString() %></td>
    </tr>
    <tr>
        <th># Photo Items:</th>
        <td><%=Model.TotalCountPhoto.ToString() %></td>
    </tr>
    <tr>
        <th># QA Items:</th>
        <td><%=Model.TotalCountQA.ToString() %></td>
    </tr>
    <tr>
        <th># Facebook Likes Items:</th>
        <td><%=Model.TotalCountFacebookLikes.ToString()%></td>
    </tr>
    <tr>
        <th>Total # of Items:</th>
        <td><%=Model.TotalCount.ToString() %></td>
    </tr>
</table>
<br /><br />
<table class="rows">
    <tr>
        <th><a href="#" onclick="GetTotals(<%=Enums.ApprovalCountReportSortType.MemberID.ToString("d") %>,<%=Model.NextAdminMemberIDSortDirection.ToString("d") %>);return false;">MemberID</a></th>
        <th class="emailcolumn"><a href="#" onclick="GetTotals(<%=Enums.ApprovalCountReportSortType.Email.ToString("d") %>,<%=Model.NextEmailAddressSortDirection.ToString("d") %>);return false;">Email</a></th>
        <th><a href="#" onclick="GetTotals(<%=Enums.ApprovalCountReportSortType.TextCount.ToString("d") %>,<%=Model.NextTextSortDirection.ToString("d") %>);return false;">TextCount</a></th>
        <th><a href="#" onclick="GetTotals(<%=Enums.ApprovalCountReportSortType.PhotoCount.ToString("d") %>,<%=Model.NextPhotoSortDirection.ToString("d") %>);return false;">PhotoCount</a></th>
        <th><a href="#" onclick="GetTotals(<%=Enums.ApprovalCountReportSortType.QACount.ToString("d") %>,<%=Model.NextQASortDirection.ToString("d") %>);return false;">QACount</a></th>
        <th><a href="#" onclick="GetTotals(<%=Enums.ApprovalCountReportSortType.FacebookLikesCount.ToString("d") %>,<%=Model.NextFacebookLikesSortDirection.ToString("d") %>);return false;">FBLikesCount</a></th>
        <th style="font-size: 12px; color:#ffffff;">On-Demand Sample %</th>
    </tr>
    <%if (Model.Records != null && Model.Records.Count > 0) { %>
        <%foreach(ApprovalCountRecordExtended record in Model.Records){ %>
            <tr>
                <td><%=record.ApprovalCountRecord.AdminMemberID.ToString() %></td>
                <td><%=record.AdminEmailAddress %></td>
                <td><span id="textCount<%= record.ApprovalCountRecord.AdminMemberID.ToString() %>"><%=record.ApprovalCountRecord.TextApprovalCount.ToString() %></span></td>
                <td><%=record.ApprovalCountRecord.PhotoApprovalCount.ToString() %></td>
                <td><%=record.ApprovalCountRecord.QAApprovalCount.ToString() %></td>
                <td><%=record.ApprovalCountRecord.FacebookLikeApprovalCount.ToString() %></td>
                <td style="width:125px;">
                    <input class="review-ondemand-sample" id="<%=record.ApprovalCountRecord.AdminMemberID.ToString() %>" type="text"/> % 
                    <span id="error<%=record.ApprovalCountRecord.AdminMemberID.ToString() %>" class="samplerate-errormsg" style="display: none;">*</span>
                </td>
            </tr>
        <%} %>
        
    <%} %>
</table>
<%if (Model.Records != null && Model.Records.Count > 0) { %>
<br/><br/>
<div class="review-ondemand-Queue">
    <span class="number-error-Message"> * Invalid Sample Rate. Please enter a number between 1 - 100. </span><br />
    <span class="error-Message"> Please provide sample rate for atleast one Admin </span> <br/>
    <input id="btnOnDemandReviewQueue" type="button" value="Generate FTA Queue for Selected Date Range" style="float: right; margin-right: 330px;">
</div>
<%} %>

