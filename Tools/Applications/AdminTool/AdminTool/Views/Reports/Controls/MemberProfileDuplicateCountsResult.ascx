﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.Review.MemberProfileDuplicateCountsResult>" %>
<%@ Import Namespace="AdminTool.Models.ModelViews.Review" %>

<%if (Model.CountsList == null)
  { %>
    No record in the date range specified
<%}
  else
  {%>
    <table class="rows">
        <tr>
            <th style="font-size: 12px; color:#ffffff;">AdminID</th>
            <th style="font-size: 12px; color:#ffffff;">Email</th>
            <th style="font-size: 12px; color:#ffffff;">Pages Processed</th>
            <th style="font-size: 12px; color:#ffffff;">Duplicates Processed</th>
            <th style="font-size: 12px; color:#ffffff;">Matches Identified</th>
            <th style="font-size: 12px; color:#ffffff;">Suspended Members</th>
            <th style="font-size: 12px; color:#ffffff;">Previously Suspended</th>
            <th style="font-size: 12px; color:#ffffff;">% of Suspended Matches</th>
        </tr>
    <% foreach (MemberProfileDuplicateHistoryCountExtended item in Model.CountsList)
       { %>
       <tr>
        <td><%= item.AdminId %></td>
        <td><%= item.EmailAddress%></td>
        <td><%= item.ActionCount %></td>
        <td><%= item.DuplicatesCount %></td>
        <td><%= item.MatchesCount %></td>
        <td><%= item.SuspendedCount %></td>
        <td><%= item.AlreadySuspendedCount %></td>
        <td><%= item.PercentSuspendedMatches %></td>
       </tr> 
    <% } %>
    </table>
<% } %>