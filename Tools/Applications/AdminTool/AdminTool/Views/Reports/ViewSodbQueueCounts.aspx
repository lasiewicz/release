﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Review.SodbReviewCountsModelView>" %>
<%@ Import Namespace="AdminTool.Models.ModelViews.Review" %>
<%@ Import Namespace="Spark.SODBQueue.ValueObjects" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Sodb Queue Stats
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="approval_count">
    <br/>
    <h2>Sodb Queue Stats</h2>
<table>
    <tr>
        <td>
            <div class="queue_count_result">
                <strong>SODB Queue - by Site</strong>
                <table class="rows">
                    <tr>
                        <th>Site</th>
                        <th>Count</th>
                        <th>Oldest</th>
                        <th>Average</th>
                    </tr>
                    <%foreach (SodbQueueCounts record in Model.SodbReviewQueueCountsBySite)
                      { %>
                        <tr>
                            <td><%=((MingleBHSiteIds)record.SiteId).ToString()%></td>
                            <td><%=record.Count.ToString() %></td>
                            <td><%=record.MaxHours.ToString()%></td>
                            <td><%=record.AverageHours.ToString() %></td>
                    
                        </tr>
                        <%} %>
                </table>
            </div>
        </td>
        <td style="padding-left: 20px;">
            <div class="queue_count_result">
                <strong>SODB Queue - by Level</strong>
                <table class="rows">
                    <tr>
                        <th>Level</th>
                        <th>Count</th>
                        <th>Oldest</th>
                        <th>Average</th>
                    </tr>
                    <%foreach (SodbQueueCounts record in Model.SodbReviewQueueCountsByLevel)
                      { %>
                        <tr>
                            <td><%=(record.Level).ToString()%></td>
                            <td><%=record.Count.ToString() %></td>
                            <td><%=record.MaxHours.ToString()%></td>
                            <td><%=record.AverageHours.ToString() %></td>
                    
                        </tr>
                        <%} %>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="queue_count_result">
                <strong>SODB Second Review Queue - by Site</strong>
                <table class="rows">
                    <tr>
                        <th>Site</th>
                        <th>Count</th>
                        <th>Oldest</th>
                        <th>Average</th>
                    </tr>
                    <%foreach (SodbQueueCounts record in Model.SodbSecondReviewQueueCountsBySite)
                      { %>
                        <tr>
                            <td><%=((MingleBHSiteIds)record.SiteId).ToString()%></td>
                            <td><%=record.Count.ToString() %></td>
                            <td><%=record.MaxHours.ToString()%></td>
                            <td><%=record.AverageHours.ToString() %></td>
                    
                        </tr>
                        <%} %>
                </table>
            </div>
        </td>
        <td style="padding-left: 20px;">
             <div class="queue_count_result">
                <strong>SODB Second Review Queue - by Level</strong>
                <table class="rows">
                    <tr>
                        <th>Level</th>
                        <th>Count</th>
                        <th>Oldest</th>
                        <th>Average</th>
                    </tr>
                    <%foreach (SodbQueueCounts record in Model.SodbSecondReviewQueueCountsByLevel)
                      { %>
                        <tr>
                            <td><%=(record.Level).ToString()%></td>
                            <td><%=record.Count.ToString() %></td>
                            <td><%=record.MaxHours.ToString()%></td>
                            <td><%=record.AverageHours.ToString() %></td>
                    
                        </tr>
                        <%} %>
                </table>
            </div>
        </td>
    </tr>
</table>
</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<link href="/Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
<%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
<%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
