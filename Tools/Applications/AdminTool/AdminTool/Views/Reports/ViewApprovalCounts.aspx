﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Review.TextApprovalReviewCountsModelView>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Admin Approval Stats and Review
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#StartDate').datepicker({
                changeMonth: true,
                changeYear: true
            });
            $('#EndDate').datepicker({
                changeMonth: true,
                changeYear: true
            });

            //Disabling enter key press
            $("form").bind("keypress", function (e) {
                if (e.keyCode == 13) return false;
            });
            
            $('#btnSampleRates').click(function () {
                window.location.href = "/Review/AdminSampleRates";
            });

            $('#btnOnDemandReviewQueue').live("click", function () {
                $('.number-error-Message').css('display', 'none');
                $('.error-Message').css('display', 'none');
                $('.samplerate-errormsg').css('display', 'none');

                var startDate = $('#StartDate').val();
                var endDate = $('#EndDate').val();
                var communityId = $('#Community').val();
                var rates = '';
                var displayErrorMsg = true;
                var showQueue = true;
                $('input.review-ondemand-sample').each(function () {
                    var id = $(this).attr('id');
                    var val = $(this).val();
                    var textCount = $('#textCount' + id).text();
                    $('#error' + id).css('display', 'none');
                    //Validate
                    if (val.length > 0 && val <= 0) {
                        $('#error' + id).css('display', 'inline');
                        $('.number-error-Message').css('display', 'block');
                        displayErrorMsg = false;
                        showQueue = false;
                    }
                    if (val.length > 0 && val > 0) {
                        if (rates.length == 0) {
                            rates = $(this).attr('id') + ":" + $(this).val();
                        }
                        else {
                            rates = rates + ',' + $(this).attr('id') + ":" + $(this).val();
                        }
                    }
                });
                if (rates.length == 0 && displayErrorMsg) {
                    $('.error-Message').css('display', 'block');
                    showQueue = false;
                }
                if(showQueue) {
                    window.location.href = '/Review/OnDemandTextReview?startDate=' + startDate + '&endDate=' + endDate + '&communityID=' + communityId + '&rates=' + rates;
                }
            });

        });

        function GetTotals(sortType, sortDirection) {

            var startDate = $('#StartDate').val();
            var endDate = $('#EndDate').val();
            var adminMemberID = $('#AdminMemberID').val();
            var communityID = $('#Community').val();
            var siteID = $('#Site').val();
            var requestTime = new Date().getTime(); //Used to force ajax update and override the browser's cache
            
            var errorMsg = '';
            errorMsg += (startDate != '') ? '' : 'Please select a valid "Start Date".\n\n';
            errorMsg += (endDate != '') ? '' : 'Please select a valid "End Date".\n\n';
            errorMsg += (adminMemberID == '' || (adminMemberID == parseInt(adminMemberID) && parseInt(adminMemberID) > 0)) ? '' : 'Please select a valid "Admin Member ID".\n\n';

            if (errorMsg != '') {
                alert(errorMsg);
                return;
            }

            $.ajax({
            type: "GET",
                url: '/ReportsAPI/GetApprovalCountTotals?startDate=' + startDate + '&endDate=' + endDate + '&adminMemberID=' + adminMemberID + '&communityID=' + communityID + '&siteID=' + siteID + '&sortType=' + sortType + '&sortDirection=' + sortDirection + '&requestTime=' + requestTime,
                dataType: 'html',
                success: function(result) {
                    $('#Totals').html(result);
                }
            });
        }
	
	</script>
	<br />
<% using(Html.BeginForm("TextReview","Review")) { %>
<div class="approval_count">
	
	<h2>Admin Approval Stats</h2><br />
	
<div class="review_count">
    <ul>
        <li>
            <div class="title"> Total FTA Samples: </div>
            <div> <strong><%= Model.TotalFTASamples %></strong></div>
        </li>
        <li>
            <div class="title">Last Insert:</div> 
            <div><%= (Model.LatestFTASample == DateTime.MinValue) ? "" : Model.LatestFTASample.ToShortDateString() %></div>
        </li>
        <li>
            <div class="title">Oldest:</div> 
            <div> <%= (Model.OldestFTASample == DateTime.MinValue) ? "" : Model.OldestFTASample.ToShortDateString()%> </div>
        </li>
    </ul>
    <div class="action">
        <input type="submit" name="ReviewQueue" id="btnTextReviewQueue" value="FTA Review Queue" />
        <br/> <br/>
        <input type="button" name="ReviewQueue" id="btnSampleRates" value="FTA Sample Rates" />
    </div>
</div>
<table class="form">
    <tr>
        <td>Start Date:</td>
        <td><input type="text" id="StartDate" name="StartDate" /></td>
    </tr>
    <tr>
        <td>End Date:</td>
        <td><input type="text" id="EndDate" name="EndDate" /></td>
    </tr>
    <tr>
        <td>Admin Member ID:</td>
        <td><input type="text" id="AdminMemberID" name="AdminMemberID" /></td>
    </tr>
    <tr>
        <td>Community:</td>
        <td>
            <select id="Community">
			<option selected="selected" value="0">All Sites</option>
			<option value="23">BBWPersonalsPlus.com</option>
			<option value="24">BlackSingles.com</option>
			<option value="25">ChristianMingle.com</option>
			<option value="10">Cupid.co.il</option>
			<option value="22">InterracialSingles.com</option>
			<option value="21">ItalianSingles.com</option>
			<option value="3">JDate</option>
			<option value="1">Spark.com</option>
			</select>
        </td>
    </tr>
    <tr>
        <td>Site:</td>
        <td>
            <select id="Site">
			<option selected="selected" value="0">All Sites</option>
			<option value="9041">BBWPersonalsPlus.com</option>
			<option value="9051">BlackSingles.com</option>
			<option value="9081">ChristianMingle.com</option>
			<option value="15">Cupid.co.il</option>
			<option value="9151">InterracialSingles.com</option>
			<option value="9161">ItalianSingles.com</option>
			<option value="4">JDate.co.il</option>
			<option value="107">JDate.co.uk</option>
			<option value="103">JDate.com</option>
			<option value="105">JDate.fr</option>
			<option value="19">NRGDating.com</option>
			<option value="101">Spark.com</option>
			</select>
	    </td>
    </tr>
</table>
<br />
<input type="hidden" id="SortDirection" value=""/>
<input type="button" id="btnRunReport" value="Run Report" onclick="GetTotals(<%=Enums.ApprovalCountReportSortType.MemberID.ToString("d") %>,<%=Enums.SortDirection.ASC.ToString("d") %>);"/><br /><br />


<div id="Totals" name="Totals"></div>

</div>
<% } %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<link href="/Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Member Top navigation--%>
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.SharedTopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
