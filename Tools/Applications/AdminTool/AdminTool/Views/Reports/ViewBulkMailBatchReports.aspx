﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.SharedTopNav>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	ViewBulkMailBatchReports
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(function() {
            $('#BatchDate').datepicker({
		            changeMonth: true,
		            changeYear: true
		        });

		    });

		    function GetReport() {

		        var batchDate = $('#BatchDate').val();
		        
		        $.ajax({
		            type: "GET",
		            url: '/ReportsAPI/GetBulkMailBatchForDate?batchDate=' + batchDate,
		            dataType: 'html',
		            success: function(result) {
		                $('#Report').html(result);
		            }
		        });
		    }
        
    </script>
        
<div class="approval_count">
    <br />
    <h2>BulkMail Batch Report</h2>
    <form id="MainForm" action="" >
    <table class="form">
    <tr>
        <td>Batch Date:</td>
        <td><input type="text" id="BatchDate" name="BatchDate" /></td>
    </tr>
    </table>
    <input type="button" id="btnRunReport" value="Run Report" onclick="GetReport();"/><br /><br />
    </form>
    <div id="Report" name="Report"></div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
<link href="/Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%--Member Top navigation--%>
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%--Member Left content--%>
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
