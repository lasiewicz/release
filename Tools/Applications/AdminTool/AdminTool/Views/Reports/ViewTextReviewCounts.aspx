﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/TwoColumns.Master" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.Review.TextApprovalReviewReportModelView>" %>
<%@ Import Namespace="AdminTool.Models.ModelViews.Review" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	FTA Review Report
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%Html.BeginForm("ViewTextReviewCounts", "Reports"); %>

 <script type="text/javascript">
     $(function () {
         $('#StartDate').datepicker({
             changeMonth: true,
             changeYear: true
         });
         $('#EndDate').datepicker({
             changeMonth: true,
             changeYear: true
         });

     });
</script>
<div class="approval_count">
    <br/>
    <h2>FTA Review Counts</h2>
    <br/>
    <table class="form">
        <tr>
            <td>Start Date:</td>
            <td>
                <%= Html.TextBox("StartDate", "", new { id = "StartDate" })%>
                <%= Html.ValidationMessageFor(model => model.StartDate) %>
            </td>
        </tr>
        <tr>
            <td>End Date:</td>
            <td>
                <%= Html.TextBox("EndDate", "", new { id = "EndDate" })%>
                <%= Html.ValidationMessageFor(model => model.EndDate)%>
            </td>
        </tr>
        <tr>
            <td>Admin Member ID:</td>
            <td>
                <%= Html.TextBoxFor(model => model.AdminMemberId)%>
                <%= Html.ValidationMessageFor(model => model.AdminMemberId)%>
            </td>
        </tr>
        <tr>
            <td>Community:</td>
            <td>
               <%=Html.DropDownListFor(model => model.CommunityId, Model.Communities)%>
            </td>
        </tr>
       
    </table>
    <br/>

<input type="submit" id="btnRunReport" value="Run Report" /><br /><br /><br />
<% if(Model.Records.Count > 0)
   { %>
     <table class="rows">
        <tr style="font-size:12px;color:#ffffff;">
            <th>MemberID</th>
            <th>Email</th>
            <th>Total Processed</th>
            <th>Total Sampled</th>
            <th>% Sampled/<br />Processed</th>
            <th>Total Reviewed</th>
            <th>% Reviewed/<br />Sampled</th>
            <th>Total Approved</th>
            <th>% Approved/<br />Reviewed</th>
            <th>Total Corrected</th>
            <th>% Corrected/<br />Reviewed</th>
        </tr>
        <% foreach(ReviewCountRecord record in Model.Records) { %>
            <tr>
                <td><%= record.AdminMemberId%></td>
                <td><%= record.AdminEmail%></td>
                <td><%= record.TotalTextProcessedCount%></td>
                <td><%= record.TotalTextSampledCount%></td>
                <td><%= record.SampledProcessedRate%></td>
                <td><%= record.TotalTextReviewedCount%></td>
                <td><%= record.ReviewedSampledRate%></td>
                <td><%= record.TotalTextApprovedCount%></td>
                <td><%= record.ApprovedReviewedRate%></td>
                <td><%= record.TotalTextCorrectedCount%></td>
                <td><%= record.CorrectedReviewedRate%></td>
            </tr>
        <% } %>
     </table>  
  <% }%>
</div>
<%Html.EndForm(); %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="/Content/superfish-members-search.css" rel="stylesheet" type="text/css" />
    <script src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js" type="text/javascript" ></script>
    <script src="/Scripts/MicrosoftMvcJQueryValidation.js" type="text/javascript" ></script>
    <script src="/Scripts/MicrosoftMvcValidation.js" type="text/javascript" ></script>
    <script src="/Scripts/MicrosoftMvcAjax.js" type="text/javascript" ></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="HeaderContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/TopNav", Model.TopNav); %>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="LeftContent" runat="server">
    <%Html.RenderPartial("../Shared/Controls/GenericLeft"); %>
</asp:Content>
