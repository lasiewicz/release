﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.PhotoUpload.MemberPhoto>" %>
<tr valign="top">
    <td colspan="2" valign="top">
        <table class="frame" cellspacing="0" cellpadding="4" width="99%">
            <tr>
                <td valign="top" width="100">
                    <select id="listOrder<%= Model.PhotosListID %>" name="listOrder<%= Model.PhotosListID %>" class="listCss">
                        <%foreach (ListItem li in Model.OrderList)
                          {%>
                        <option value="<%= li.Value %>" <%if (li.Selected)
                                  { %> selected="true" <%} %>>
                            <%= li.Text %>
                        </option>
                        <%} %>
                    </select>
                    <br />
                    <span class="default">photo</span>
                </td>
                <td align="center" width="125">
                    <table cellspacing="0" cellpadding="0" background="/img/Community/JDate/noPhoto.gif"
                        align="center" class="noPhoto" style="cursor: default;" id="_ctl0__ctl2_5_ucNoPhoto_tblNoPhoto">
                        <tbody>
                            <tr>
                                <td align="center">
                                    Upload a photo now
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td valign="top" align="center" width="175">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td>
                                <input type="file" id="uploadedFile<%=Model.PhotosListID %>" name="uploadedFile<%=Model.PhotosListID %>">
                            </td>
                        </tr>
                        <tr valign="top" height="50">
                            <td>
                            </td>
                        </tr>
                        <tr valign="bottom">
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top" width="100">
                </td>
            </tr>
        </table>
    </td>
</tr>
