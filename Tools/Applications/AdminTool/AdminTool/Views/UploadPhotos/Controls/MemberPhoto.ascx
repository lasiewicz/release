﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<AdminTool.Models.ModelViews.PhotoUpload.MemberPhoto>" %>
<tr>
    <td colspan="2">
        <table class="frame" cellspacing="0" cellpadding="4" width="99%" border='0'>
            <tr valign="top">
                <td valign="top" width="100">
                    <select id="listOrder<%= Model.PhotosListID %>" name="listOrder<%= Model.PhotosListID %>" class="listCss">
                        <%foreach (ListItem li in Model.OrderList)
                          {%>
                        <option value="<%= li.Value %>" <%if (li.Selected) { %> selected="true" <%} %>>
                            <%= li.Text %>
                        </option>
                        <%} %>
                    </select>
                    <br />
                    <span class="default">photo</span>
                </td>
                <td align="center" width="125">
                    <a onclick="launchPhotoWindow('<%=Model.FileWebPath %>?siteID=<%=Model.SiteID %>');">
                        <%if (Model.IsApproved) { %>
                            <img style="border-width: 0px; border-style: none; height: 104px; width: 80px;" src="<%=Model.ThumbFileWebPath %>">
                        <% } else { %>
                            <img style="border-width: 0px; border-style: none; height: 104px; width: 80px;" src="<%=Model.FileWebPath %>">
                        <% } %>
                    </a>
                </td>
                <td valign="top" align="center" width="175">
                    <table cellspacing="0" cellpadding="0" border="0" class=".photoContentCss">
                        <tr>
                            <td>
                                <input type="file" id="uploadedFile<%=Model.PhotosListID %>" name="uploadedFile<%=Model.PhotosListID %>">
                            </td>
                        </tr>
                        <tr valign="top" height="50">
                            <td>
                                <div>
                                    <div class="editor-label">
                                        <label for="txtCaption<%=Model.PhotosListID %>">
                                            Caption</label>
                                    </div>
                                    <div class="editor-field">
                                        <textarea name="txtCaption<%=Model.PhotosListID %>" class="txtCaption" cols="30"
                                            rows="5" id="Caption"><%=Model.Caption %></textarea>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%if (Model.IsPrivatePhotoVisible)
                                  { %>
                                <div style="display: block">
                                    <%}
                                  else
                                  { %>
                                    <div style="display: none">
                                        <%} %>
                                        <div class="editor-label"> 
                                            <label for="chkPrivatePhoto<%=Model.PhotosListID %>">
                                                This is a private photo</label>
                                        </div>
                                        <div class="editor-field">
                                            <input type="checkbox" <%=Model.IsPrivateChecked %> id="chkPrivatePhoto<%=Model.PhotosListID%>"
                                                name="chkPrivatePhoto<%=Model.PhotosListID%>" />
                                        </div>
                                    </div>
                            </td>
                        </tr> 
                        <tr>
                            <td>
                                <label for="chkApprovedForMain<%=Model.PhotosListID%>">Main Approved</label>
                                <input type="checkbox" <%=Model.IsApprovedForMainChecked %> id="chkApprovedForMain<%=Model.PhotosListID%>"
                                    name="chkApprovedForMain<%=Model.PhotosListID%>" class="isMainApprovedCss" />
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <div style="display:none;">
                                <label for="chkIsMain<%=Model.PhotosListID%>">Is Main Photo</label>
                                <input type="checkbox" <%=Model.IsMainChecked %> id="chkIsMain<%=Model.PhotosListID%>"
                                    name="chkIsMain<%=Model.PhotosListID%>" class="isMainCss"/>

                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top" width="100">
                    <p>
                        <span class="default">System Status</span><br />
                        <%if (Model.IsApproved)
                          { %>
                        <span class="bold">Approved</span>
                        <%}
                          else
                          { %>
                        <span class="bold">Awaiting Approval</span>
                        <%} %>
                    </p>
                    <p>
                        <a class="buttonAlink activityButton" href="javascript:window.location.href='<%=
                          Url.Action("DeleteMemberPhoto", "UploadPhotos",
                                     new { memberID = Model.MemberID,
                                           siteID = Model.SiteID,
                                           memberPhotoID = Model.MemberPhotoID,
                                           listOrder = Model.ListOrder})%>'" onclick="javascript:return confirm('Are you sure you want to delete this photo?')">
                            Delete Photo </a>
                    </p>
                </td>
            </tr>
        </table>
    </td>
</tr>
