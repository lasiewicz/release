﻿<%@ Page Title="Photo Upload" Language="C#" Inherits="System.Web.Mvc.ViewPage<AdminTool.Models.ModelViews.PhotoUpload.MemberPhotos>" %>

<html>
<title>JDate.com - The Leading Jewish Singles Network! Explore the possibilities.</title>
<meta name="copyright" content="http://www.spark.net">
<meta name="title" content="Jewish Dating and Jewish Singles @ JDate">
<meta name="keywords" content="Jewish dating, JDate, Jewish, chat rooms, love, free personals, Jewish singles, personal ads, romance, relationships, marriage, friendship, Jewish single women, online personals, date, relationship, Jewish matchmaking, photo personal ads, meet people, Jewish single men, soul mates, shiduch, Jewish match maker service, reform, conservative, orthodox, hupa, TRI religious, beshert, shidduchim, secular, traditional, synagogues, yeshiva, events, parties">
<meta name="description" content="Meet Jewish singles in your area for dating and romance @ JDate.com - the most popular online Jewish dating community.">
<meta name="classification" content="JDate, Jewish, singles, Jewish singles, chat rooms, love, free personals, dating, personal ads, romance, relationships, marriage, friendship, single women, men, internet, online personals, date, relationship, matchmaking, match, photo personal ads, IM buddies, meet people, soul mates, shiduch, many high quality professionals, match maker service, reform, conservative, orthodox, hupa, TRI religious, beshert, shidduchim, secular, traditional, synagogues, yeshiva, best travel, events, parties, fun jews, clean anonymous">
<meta name="author" content="http://www.JDate.com">
<meta name="robots" content="all">
<meta name="Rating" content="general">
<meta name="Distribution" content="GLOBAL">
<link rel="stylesheet" type="text/css" href="../../../Content/upload.css"/>
<link href="../../../Content/custom-theme/jquery-ui-1.8.1.custom.css" rel="stylesheet" type="text/css" />

<style type="text/css">
.isMainPhotoContent
{
    display:none;
}
</style>



    <script src="<%= ResolveUrl("~/Scripts/jquery-1.4.2.min.js")%>" type="text/javascript"></script>
     <%--<script src="<%= ResolveUrl("~/Scripts/jquery-2.0.3.min.js")%>" type="text/javascript"></script>
     <script src="<%= ResolveUrl("~/Scripts/jquery-migrate-1.2.1-min.js")%>" type="text/javascript"></script>--%>

<script type="text/javascript">
    var MemberPhotoWindow;

    function launchPhotoWindow(strURL, Caption) {
        if (typeof (MemberPhotoWindow) == 'object') {
            MemberPhotoWindow.close();
        }
        MemberPhotoWindow = window.open(strURL, 'MemberPhotoWindow', 'status=yes,scrollbars=yes,location=no,resizable=no,menubar=no,width=400,height=400,title=Caption'); MemberPhotoWindow.opener = self;
        MemberPhotoWindow.name = 'MemberPhotoWindow';
        return 0;
    }

</script>

<body>
    <% using (Html.BeginForm(null, null, null, FormMethod.Post, new { enctype = "multipart/form-data" }))
       { %>
    <div class="rightNewAlignOverride">
        <div id="rightNew">
            <h1>
                Photos for
                <%=Model.Username %>
            </h1>
            <br />
            <span>
                <%=Model.ValidationMessage %>
            </span>
            <div id="prefborder">
                <table cellspacing="0" cellpadding="4" border="0" class="lightest" width="592">
                    <tr>
                        <td>
                            <table cellpadding="2" border="0" width="100%">
                                <tr>
                                    <td colspan="2">
                                        Show my photos in guest search:
                                        <%=Html.RadioButton("guestSearchRadioList", Model.DisplayPhotosToGuests, Model.DisplayPhotosToGuests)%>
                                        <label for='guestSearchRadioList0'>
                                            Yes</label>
                                        <%=Html.RadioButton("guestSearchRadioList", !Model.DisplayPhotosToGuests, !Model.DisplayPhotosToGuests)%>
                                        <label for='guestSearchRadioList1'>
                                            No</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Click "Save/Upload" button to complete your upload.
                                    </td>
                                    <td class="alignRTL">
                                        <input type="submit" value="Save/Upload" class="activityButton" />
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                     <tr>
                                    <td>
                                        <b>To set a different Main Photo:</b>
                                            <ul>
                                                <li>1. Make sure the photo you want to be Main is makred "Main Approved".</li>
                                                <li>2. User the dropdown to make it the #1 photo.</li>
                                                <li>3. Save the changes.</li>
                                            </ul>
                                    </td>   
                    </tr>
                    <%foreach (AdminTool.Models.ModelViews.PhotoUpload.MemberPhoto photo in Model.PhotosList)
                      {%>
                    <% if (!string.IsNullOrEmpty(photo.FileWebPath))
                       {%>
                    <%Html.RenderPartial("Controls/MemberPhoto", photo); %>
                    <%}
                       else
                       {%>
                    <%Html.RenderPartial("Controls/NoPhoto", photo); %>
                    <%} %>
                    <%} %>
                    <tr>
                        <td class="alignRTL">
                            <input type="submit" value="Save/Upload" class="activityButton" />
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <hr class="hr-thin" />
            <!-- Photo Upload Tip -->
            <a name="Tips"></a>
            <div style="float: right; width: 158px; margin: 8px;">
                <a href="http://jdate.lookbetteronline.com/Home/Page.asp?page=jhome" target="_blank">
                    <img src="/img/Promo/bnr_jd_lbo_158x400_2.jpg" border="0"></a>
            </div>
            <h2>
                <a name="guidelines"></a>Basic Photo Guidelines</h2>
            <p>
                <strong>Digital photo files:</strong></p>
            <ul>
                <li>Please send JPG, GIF or BMP images </li>
                <li>Minimum: 350 x 350 pixels at 72 DPI</li>
                <li>Maximum file size: 5MB</li>
            </ul>
            <p>
                <strong>General guidelines:</strong></p>
            <ul>
                <li>Your first photo should be a clear headshot</li>
                <li>You should be in all photos</li>
                <li>If it's a group photo, you must distinguish yourself</li>
                <li>No copyrighted material</li>
                <li>No blurry photos</li>
                <li>No suggestive photos</li>
                <li>No collages or composites</li>
            </ul>
            <p>
                <strong>Submitting photos by email or regular mail</strong><br />
                When submitting photos by email or regular mail, please include your member number,
                username and email address. Once we receive your photos, it usually takes approximately
                24 - 48 hours to post them, and we will send you an email confirmation once they
                are up.</p>
            <p>
                <strong>Email photos to:</strong> <a href="mailto:photos@JDate.com">photos@JDate.com</a></p>
            <p>
                <strong>Mail photos to:</strong><br />
                JDate.com<br />
                c/o Spark Networks&reg; Limited<br />
                8383 Wilshire Blvd, Suite 800<br />
                Beverly Hills, CA 90211<br />
                USA</p>
            <p>
                Please include a self-addressed stamped envelope if you&#8217;d like your photos
                returned.</p>
        </div>
    </div> 
    <%} %>


<script type="text/javascript">
    $(document).ready(function () {


        var currentFirstID = '-1'
        var selectedValue = '-1';

        //cycle thru and grab the first slots ID. 
        $('.listCss').each(function () {

            if ($(this).find(':selected').val() == '1') {
                currentFirstID = $(this).attr('id');
            }

        });


        //store the selected value
        $('.listCss').focus(function () {

            selectedValue = $(this).find(':selected').val();

        });


        $('.listCss').unbind('click').click(function (event) {

            //check to see if was set to "1" slot. 
            if (isListDDSetToOne($(this))) {

                if (currentFirstID != $(this).attr('id') && selectedValue != '1') {
                    //set the current 1st with my value and make me the first  
                    setCurrentOneWithMyOrginalValue(selectedValue, currentFirstID);

                    //updated the currentFirstID to my id
                    currentFirstID = $(this).attr('id');

                    var mainTable = $(this).closest('table');

                    var mainPhotoTable = mainTable.find('table');

                    killAllIsMains();

                    if (mainPhotoTable.find('.isMainApprovedCss').is(':checked')) {

                        //make the current one main. 
                        mainPhotoTable.find('.isMainCss').attr('checked', true);
                    }


                }

            }
            

        });

        function setCurrentOneWithMyOrginalValue(selectedValue, currentFirstID) {
            $("#" + currentFirstID).val(selectedValue);

        }

        function isListDDSetToOne(myDD) {
            if (myDD.find(':selected').val() == "1")
                return true;
            else
                return false;
        }


        $('.isMainApprovedCss').unbind('click').click(function (event) {

            var theID = $(this).closest('.frame').find(".listCss ").find(':selected');

            if (theID.val() == "1") {

                if ($(this).is(':checked')) {

                    killAllIsMains();
                    
                    $(this).closest('table').find(".isMainCss").attr('checked', true);
                }
                else {
                    $(this).closest('table').find(".isMainCss").attr('checked', true);
                }
            }

        });


        function killAllIsMains() {
            $('.isMainCss').each(function () {
                $(this).removeAttr('checked');
            });
        }
    });
</script>
</body>
</html>
